﻿using CT.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Services.Protocols;
using System.Xml;
using UAPIdll.Air46;

namespace CT.BookingEngine.GDS
{
    public class UAPICredentials
    {
        public string UserName = string.Empty;
        public string Password = string.Empty;
        public string TargetBranch = string.Empty;
    }
    public class UAPI : IDisposable
    {
        // Flag: Has Dispose already been called?
        bool disposed = false;
        #region Credential
        public string UserName = string.Empty;
        public string Password = string.Empty;
        string urlAir = string.Empty;
        string urlUR = string.Empty;
        public string TargetBranch = string.Empty;
        string originalApplication = "UAPI";
        public string agentBaseCurrency;
        public Dictionary<string, decimal> ExchangeRates;
        double rateOfExchange;
        public string EndpointURL = string.Empty;

        /// <summary>
        ///This amount will be always in AED currency. 
        ///ROE will be applied before deducting from Total Amount. 
        ///In Case of Pax Type Fare Breakdown, Adult Tax will be applied discount.
        /// </summary>
        double baseDiscount = 0;
        double baseDiscountROE = 1;

        public string xmlPath = string.Empty;
        string xmlSoapPath = string.Empty;//path to save soap logs
        public string AppUserId = "1";//to avoid exception
        public string SessionId;
        public bool AvoidFullCancelPenalty;//Assign this value to "true" if Corp Agent from MSE
        #endregion

        #region Added for CombineSearch
        List<SearchRequest> searchRequests = new List<SearchRequest>();
        private bool searchBySegments;
        public bool SearchBySegments { get => searchBySegments; set => searchBySegments = value; }
        public string sSeatAvailResponseXSLT { get; private set; }
        /// <summary>
        /// Allow any airlines in the search using the airline code
        /// </summary>
        public string AllowedAirlinesForGDS { get => allowedAirlinesForGDS; set => allowedAirlinesForGDS = value; }

        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        private string suapisessionid;
        List<SearchResult> finalResult = new List<SearchResult>();
        #endregion

        public const string LINK_AVAILABILITY = "LinkAvailability";
        public const string AVAILABILITY_SOURCE = "AvailabilitySource";
        public const string PROVIDER_CODE = "ProviderCode";
        public const string SEGMENT_REF = "SegmentRef";
        public const string POLLED_AVAILABILITY_OPTION = "PolledAvailabilityOption";
        public const string PARENT_AGENT_LOCATION = "ParentAgentLocation";
        public const string PARENT_AGENT_PHONE = "ParentAgentPhone";
        public const string ProviderCode = "1G";
        public const string UAPI_TERMINAL_HOST_TOKEN = "UAPITerminalHostToken";
        bool formOfPaymentIsShown = true;
        public string PrivateFareCodes;//Added by lokesh for passing private fare codes configured for the agent.
        bool isOneWayCombinationSearch = false;//Added by lokesh for resetting the thread event
        private string allowedAirlinesForGDS;
        public bool IsTerminalAccessAllowed;
        public string TerminalHostToken;
        string urlTerminal = string.Empty;
        #region Constructor

        public UAPI()
        {
            Connection();
        }

        ~UAPI()
        {
            Dispose(true);
        }

        public void Connection()
        {
            # region LiVe HC Credentials
            //userName = "Universal API/uAPI7280978342-0fbaffdc";
            //password = "6Hz}$9KeJi";
            //urlAir = "https://emea.universal-api.travelport.com/B2BGateway/connect/uAPI/AirService";
            //urlUR = "https://emea.universal-api.travelport.com/B2BGateway/connect/uAPI/UniversalRecordService";
            //targetBranch = "P1544552";
            //Audit.Add(EventType.Search, Severity.Normal, 0, " Live userName:" + userName, string.Empty);
            # endregion
            # region Live 
            //userName = CT.Configuration.ConfigurationSystem.UAPIConfig["Userid"];
            //password = CT.Configuration.ConfigurationSystem.UAPIConfig["Password"];
            //password = "M}y2&6Tt7D";
            if (!string.IsNullOrEmpty(EndpointURL))
            {
                urlAir = EndpointURL + "AirService";
                urlUR = EndpointURL + "UniversalRecordService";
                urlTerminal = EndpointURL + "TerminalService";
            }
            else
            {
                urlAir = CT.Configuration.ConfigurationSystem.UAPIConfig["Url"] + "AirService";
                urlUR = CT.Configuration.ConfigurationSystem.UAPIConfig["Url"] + "UniversalRecordService";
                urlTerminal = CT.Configuration.ConfigurationSystem.UAPIConfig["Url"] + "TerminalService";
            }
            //targetBranch = CT.Configuration.ConfigurationSystem.UAPIConfig["InternationalHAP"];
            # endregion
            # region Test Credentials
            //userName = "Universal API/uAPI7997403705-43d54fd1";
            //password = "x-5Y6P}c!4";
            //urlAir = "https://emea.copy-webservices.travelport.com/B2BGateway/connect/uAPI/AirService";
            //urlUR = "https://emea.copy-webservices.travelport.com/B2BGateway/connect/uAPI/UniversalRecordService";
            //targetBranch = "P7005785";
            # endregion
            xmlPath = CT.Configuration.ConfigurationSystem.UAPIConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
            sSeatAvailResponseXSLT = CT.Configuration.ConfigurationSystem.XslconfigPath + "\\UAPISeatMap.xslt";
            try
            {
                if (!System.IO.Directory.Exists(xmlPath))
                {
                    System.IO.Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
            try
            {
                xmlSoapPath = CT.Configuration.ConfigurationSystem.UAPIConfig["XmlSoapPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
                if (!System.IO.Directory.Exists(xmlSoapPath))
                {
                    System.IO.Directory.CreateDirectory(xmlSoapPath);
                }
            }
            catch
            {
                xmlSoapPath = xmlPath;
                Audit.Add(EventType.Exception, Severity.High, 0, "(UAPI)Key not found XmlSoapPath in UAPI.Config.xml.", "");
            }

            //Check whether any key is specified
            if (ConfigurationManager.AppSettings["UAPIDiscount"] != null)
            {
                baseDiscount = Convert.ToDouble(ConfigurationManager.AppSettings["UAPIDiscount"]);
            }
            else
            {
                baseDiscount = 20;//If no value found in the Web.Config or Key missing assign default discount
            }
        }
        # endregion

        # region Search
        public SearchResult[] Search(SearchRequest request, string sessionId)
        {

            Dictionary<string, int> readySources = new Dictionary<string, int>();
            Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
            Airline.GetAllAirlinesList();

            searchBySegments = request.SearchBySegments;
            if (searchBySegments && request.Type == SearchType.Return)//Round Trip Combination Search
            {
                suapisessionid = sessionId;
                listOfThreads.Add("UAO1", new WaitCallback(SearchRoutingReturn));
                listOfThreads.Add("UAO2", new WaitCallback(SearchRoutingReturn));

                SearchRequest onwardRequest = request.Copy();
                onwardRequest.Type = SearchType.OneWay;
                onwardRequest.Segments = new FlightSegment[1];
                onwardRequest.Segments[0] = request.Segments[0];

                SearchRequest returnRequest = request.Copy();
                returnRequest.Type = SearchType.Return;
                returnRequest.Segments = new FlightSegment[1];
                returnRequest.Segments[0] = request.Segments[1];

                searchRequests.Add(onwardRequest);
                searchRequests.Add(returnRequest);

                eventFlag = new AutoResetEvent[2];
                readySources.Add("UAO1", 1200);
                readySources.Add("UAO2", 1200);

                int t = 0;
                //Start each fare search within a Thread which will automatically terminate after the results are received.
                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                {
                    if (readySources.ContainsKey(deThread.Key))
                    {
                        ThreadPool.QueueUserWorkItem(deThread.Value, t);
                        eventFlag[t] = new AutoResetEvent(false);
                        t++;
                    }
                }

                if (t != 0)
                {
                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 2, 0), true))
                    {
                        //TODO: audit which thread is timed out                
                    }
                }
            }
            //Added by lokesh on 24/04/2019
            //===============START:ONE WAY COMBINATION SEARCH==============/
            else if (searchBySegments && request.Type == SearchType.OneWay)//One Way Combination Search
            {
                suapisessionid = sessionId;
                SearchRequest onwardRequest = request.Copy();
                onwardRequest.Type = SearchType.OneWay;
                onwardRequest.Segments = new FlightSegment[1];
                onwardRequest.Segments[0] = request.Segments[0];
                searchRequests.Add(onwardRequest);
                isOneWayCombinationSearch = true;
                SearchRoutingReturn(0);
            }
            //===============END:ONE WAY COMBINATION SEARCH==============/
            else
            {
                //Trace.TraceInformation("UAPI.Search() entered.");

                SearchResult[] result = new SearchResult[0];
                //SearchResult[] icSearchResult = new SearchResult[0];
                // bool isDomestic = true;
                //List<int> flightNotFound = new List<int>();
                //Dictionary<int, List<FlightInfo>> outboundflightList = new Dictionary<int, List<FlightInfo>>();
                //Dictionary<int, List<FlightInfo>> inboundflightList = new Dictionary<int, List<FlightInfo>>();


                //isDomestic = false;
                //Connection connection = new Connection(isDomestic);
                result = LowFareSearch(request, sessionId);
                finalResult.AddRange(result);

                //Trace.TraceInformation("UAPI.Search() exiting.");
            }
            Basket.FlightBookingSession[sessionId].Log.Add("UAPI Search complete. Result count = " + finalResult.Count.ToString());
            ///Check for onward and return flights in case routing search and search type is return
            if (searchBySegments && request.Type == SearchType.Return)
            {
                bool onwardFound = finalResult.Any(r => r.Flights.ToList().All(s => s.ToList().All(f => f.Group == 0)));
                bool returnFound = finalResult.Any(r => r.Flights.ToList().All(s => s.ToList().All(f => f.Group == 1)));
                if (!onwardFound && !returnFound)
                {
                    finalResult = new List<SearchResult>();
                }
            }

            return finalResult.ToArray();
        }

        public void SearchRoutingReturn(object eventNumber)
        {
            try
            {
                SearchRequest request = searchRequests[Convert.ToInt32(eventNumber)];
                SearchResult[] result = new SearchResult[0];
                result = LowFareSearch(request, suapisessionid);

                finalResult.AddRange(result);
                if (!isOneWayCombinationSearch)
                {
                    eventFlag[(int)eventNumber].Set();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to get UAPI SOAP Search response. Error: " + ex.ToString(), "");
                if (!isOneWayCombinationSearch)
                {
                    eventFlag[(int)eventNumber].Set();
                }
            }
        }

        private SearchResult[] LowFareSearch(SearchRequest request, string sessionId)
        {
            //Dictionary<int, List<FlightInfo>> outboundflightList = new Dictionary<int, List<FlightInfo>>();
            //Dictionary<int, List<FlightInfo>> inboundflightList = new Dictionary<int, List<FlightInfo>>();
            //List<FlightInfo> outbound = new List<FlightInfo>();
            //List<FlightInfo> inbound = new List<FlightInfo>();

            Dictionary<string, List<FlightInfo>> outbound = new Dictionary<string, List<FlightInfo>>();
            Dictionary<string, List<FlightInfo>> inbound = new Dictionary<string, List<FlightInfo>>();

            SearchResult[] result = new SearchResult[0];
            //NetworkCredential nCredential=new NetworkCredential(this
            LowFareSearchRsp lfResponse = GenerateSearch(request);//Original Search Request
            //GenerateSearch(request);//For Normal xml request log
            //LowFareSearchRsp lfResponse = new LowFareSearchRsp();
            try
            {
                #region SOAP Search

                //string response = GetSearchResponse(request);
                //if (!string.IsNullOrEmpty(response))
                //{
                //    XmlDocument doc = new XmlDocument();
                //    doc.LoadXml(response);
                //    //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchRes.xml");
                //    string filePath = xmlSoapPath + "FlightSOAPSearchResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                //    doc.Save(filePath);
                //    //Audit.Add(EventType.Search, Severity.Low, Convert.ToInt32(AppUserId), "UAPI Search response wrote into SOAP", "");
                //    XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                //    nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");

                //    XmlNode root = doc.DocumentElement;
                //    XmlNode searchResponse = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild;

                //    //Use StringBuilder instead of string to avoid OutOfMemory Exception
                //    //StringBuilder sb = new StringBuilder(searchResponse.OuterXml);
                //    //sb = sb.Replace("air:", "");
                //    ////Add namespaces inorder to synchronize with LowFareSearchResponse object when Deserialized
                //    //sb = sb.Replace("xmlns:air=\"http://www.travelport.com/schema/air_v33_0\"", "");
                //    //sb = sb.Replace("<FlightDetailsList", "<FlightDetailsList xmlns=\"http://www.travelport.com/schema/air_v33_0\"");
                //    //sb = sb.Replace("<AirSegmentList", "<AirSegmentList xmlns=\"http://www.travelport.com/schema/air_v33_0\"");
                //    //sb = sb.Replace("<FareInfoList", "<FareInfoList xmlns=\"http://www.travelport.com/schema/air_v33_0\"");
                //    //sb = sb.Replace("<RouteList", "<RouteList xmlns=\"http://www.travelport.com/schema/air_v33_0\"");
                //    //sb = sb.Replace("<AirPricingSolution", "<AirPricingSolution xmlns=\"http://www.travelport.com/schema/air_v33_0\"");

                //    //StringReader reader = new StringReader(sb.ToString());
                //    //FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                //    try
                //    {
                //        SoapAttributeOverrides mySoapAttributeOverrides = new SoapAttributeOverrides();
                //        SoapAttributes soapAtts = new SoapAttributes();
                //        SoapTypeAttribute soapType = new SoapTypeAttribute();
                //        soapType.TypeName = "LowFareSearchRsp";
                //        soapType.IncludeInSchema = false;
                //        soapType.Namespace = "http://www.travelport.com/schema/air_v33_0";
                //        soapAtts.SoapType = soapType;
                //        soapAtts.SoapIgnore = true;

                //        mySoapAttributeOverrides.Add(typeof(CT.BookingEngine.GDS.SOAP.LowFareSearchRsp), soapAtts);

                //        XmlTypeMapping myMapping = (new SoapReflectionImporter(mySoapAttributeOverrides)).ImportTypeMapping(typeof(CT.BookingEngine.GDS.SOAP.LowFareSearchRsp));
                //        XmlSerializer mySerializer = new XmlSerializer(myMapping);

                //        TextReader reader = new StreamReader(filePath);

                //        XmlTextReader xmlReader = new XmlTextReader(reader);
                //        xmlReader.ReadStartElement();

                //        CT.BookingEngine.GDS.SOAP.LowFareSearchRsp searchRsp = (CT.BookingEngine.GDS.SOAP.LowFareSearchRsp)mySerializer.Deserialize(xmlReader);
                //        xmlReader.ReadEndElement();

                //        xmlReader.Close();
                //        reader.Close();
                //        //Audit.Add(EventType.Search, Severity.Low, Convert.ToInt32(AppUserId), "UAPI Search response xml Deserialize start", "");
                //        //XmlSerializer mySerializer = new XmlSerializer(typeof(LowFareSearchRsp));
                //        //lfResponse = (LowFareSearchRsp)mySerializer.Deserialize(reader);
                //        //Audit.Add(EventType.Search, Severity.Low, Convert.ToInt32(AppUserId), "UAPI Search response xml Deserialize end", "");
                //    }
                //    catch(Exception ex) { }
                //    finally
                //    {
                //        //reader.Close();
                //    }
                //}
                //else
                //{
                //    Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to get UAPI SOAP Search response. Response was empty or null." , "");
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to get UAPI SOAP Search response. Error: " + ex.ToString(), "");
            }
            //Basket.FlightBookingSession[sessionId].XmlMessage.Add(searchXml);


            if (request != null && lfResponse != null && lfResponse.FlightDetailsList != null && lfResponse.FlightDetailsList.Length > 0)
            {
                ReadFlightInfoResponse(lfResponse, ref outbound, ref inbound, ref request);
                if (outbound.Count > 0)
                {
                    result = ReadFareResponse(lfResponse, request, outbound, inbound);
                }

            }
            //if (searchXml.Length > 0)
            //{
            //    string responseXml = connection.MultiSubmitXml(searchXml);
            //    //Basket.FlightBookingSession[sessionId].XmlMessage.Add(responseXml);
            //    Audit.Add(EventType.Search, Severity.Normal, 0, "Search XML 1G. req : " + searchXml + " resp : " + responseXml, string.Empty);
            //    ReadFlightInfoResponse(responseXml, request, ref outboundflightList, ref inboundflightList);
            //    if (outboundflightList.Count > 0)
            //    {
            //        result = ReadFareResponse(responseXml, request, outboundflightList, inboundflightList);
            //    }
            //}
            return result;
        }
        private LowFareSearchRsp GenerateSearch(SearchRequest request)
        {

            //Trace.TraceInformation("UAPI.GenerateSearchMessage entered");
            //bool isDomestic = true;
            if ((request.AdultCount + request.ChildCount + request.SeniorCount + request.InfantCount) > 9)
            {
                throw new BookingEngineException("Total number of passengers should not exceed 9");
            }
            if (request.InfantCount > (request.AdultCount + request.SeniorCount))
            {
                throw new BookingEngineException("Number of infants should not exceed the number of Adult and Senior");
            }
            int paxCount = request.AdultCount + request.ChildCount + request.InfantCount + request.SeniorCount;

            Connection();
            LowFareSearchReq lfRequest = new LowFareSearchReq();
            lfRequest.TargetBranch = TargetBranch;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            lfRequest.BillingPointOfSaleInfo = pos;

            AirSearchModifiers airModifiers = new AirSearchModifiers();
            lfRequest.AirSearchModifiers = airModifiers;

            //Provider Code 1G-Galileo need to be specified for getting Inventory
            airModifiers.PreferredProviders = new Provider[1];
            airModifiers.PreferredProviders[0] = new Provider();
            airModifiers.PreferredProviders[0].Code = "1G";

            SearchPassenger[] passAdult = new SearchPassenger[paxCount + 1];
            int paxIncrement = 0;
            for (int i = 0; i < request.AdultCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "ADT";
                paxIncrement++;

            }
            for (int i = 0; i < request.ChildCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "CNN";
                passAdult[paxIncrement].Age = "5";
                paxIncrement++;

            }
            for (int i = 0; i < request.SeniorCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "SRC";
                passAdult[paxIncrement].Age = "65";
                paxIncrement++;

            }
            for (int i = 0; i < request.InfantCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "INF";
                paxIncrement++;

            }
            lfRequest.SearchPassenger = passAdult;
            //lfRequest.SearchPassenger = passAdult;

            //lfRequest.SearchAirLeg = new SearchAirLeg[2];
            //lfRequest.SearchAirLeg[0] = new SearchAirLeg();
            //SearchAirLeg leg = lfRequest.SearchAirLeg[0];

            //Air20.Airport origin = new Air20.Airport();
            //origin.Code = request.Segments[0].Origin;

            //Air20.Airport destination = new Air20.Airport();
            //destination.Code = request.Segments[0].Destination;

            //leg.SearchOrigin = new typeSearchLocation[1];
            //leg.SearchOrigin[0] = new typeSearchLocation();
            //leg.SearchOrigin[0].Item= origin;

            //leg.SearchDestination = new typeSearchLocation[1];
            //leg.SearchDestination[0] = new typeSearchLocation();
            //leg.SearchDestination[0].Item= destination;

            //leg.Items = new typeFlexibleTimeSpec[1];
            //leg.Items[0] = new typeFlexibleTimeSpec();
            //leg.Items[0].PreferredTime = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");

            // PReferred Cabin

            CabinClass cabinType = CabinClass.Economy;
            switch (request.Segments[0].flightCabinClass)
            {
                case CabinClass.Business:
                    cabinType = CabinClass.Business;
                    break;
                case CabinClass.Economy:
                    cabinType = CabinClass.Economy;
                    break;
                case CabinClass.First:
                    cabinType = CabinClass.First;
                    break;
                //Added by Shiva
                case CabinClass.PremiumEconomy:
                    cabinType = CabinClass.PremiumEconomy;
                    break;
                    //case CabinClass.PremiumBusiness:
                    //    cabinType = CabinClass.PremiumBusiness;
                    //    break;  

            }
            Airline.GetAllAirlinesList();

            Dictionary<string, System.Collections.ArrayList> AllAirlines = CT.Configuration.CacheData.AirlineList;
            List<string> RestrictedAirlines = new List<string>();

            foreach (KeyValuePair<string, System.Collections.ArrayList> airline in AllAirlines)
            {
                try
                {
                    if (airline.Value[22]!=null && !Convert.ToBoolean(airline.Value[22]))//UapiAllowed column value
                    {
                        RestrictedAirlines.Add(airline.Key);
                    }
                }
                catch
                { //Audit.Add(EventType.Search, Severity.High, 1, "Error in Airline Code UAPI restircted: " + airline.Value[0] ,"");
                }
            }
            if (request.Type == SearchType.MultiWay)
            {

                lfRequest.Items = new SearchAirLeg[request.Segments.Length];
                int segIndex = 0;
                foreach (FlightSegment segment in request.Segments)
                {
                    lfRequest.Items[segIndex] = new SearchAirLeg();
                    SearchAirLeg leg = lfRequest.Items[segIndex] as SearchAirLeg;
                    // origin
                    UAPIdll.Air46.CityOrAirport origin = new UAPIdll.Air46.CityOrAirport();
                    origin.Code = request.Segments[segIndex].Origin;
                    origin.PreferCity = request.Segments[segIndex].NearByOriginPort;

                    leg.SearchOrigin = new typeSearchLocation[1];
                    leg.SearchOrigin[0] = new typeSearchLocation();
                    leg.SearchOrigin[0].Item = origin;

                    // destination
                    UAPIdll.Air46.CityOrAirport destination = new UAPIdll.Air46.CityOrAirport();
                    destination.Code = request.Segments[segIndex].Destination;
                    destination.PreferCity = true;

                    leg.SearchDestination = new typeSearchLocation[1];
                    leg.SearchDestination[0] = new typeSearchLocation();
                    leg.SearchDestination[0].Item = destination;

                    //For implementing Calendar search or -/+ days search, you need to use the code below
                    /* Max days can be 3 only
                     * leg.Items[0].Item = new typeFlexibleTimeSpecSearchExtraDays();
                    (leg.Items[0].Item as typeFlexibleTimeSpecSearchExtraDays).DaysAfter = 3;
                    (leg.Items[0].Item as typeFlexibleTimeSpecSearchExtraDays).DaysAfterSpecified = true;
                    (leg.Items[0].Item as typeFlexibleTimeSpecSearchExtraDays).DaysBefore = 3;
                    (leg.Items[0].Item as typeFlexibleTimeSpecSearchExtraDays).DaysBeforeSpecified = true;
                     * */
                    leg.Items = new typeFlexibleTimeSpec[1];
                    leg.Items[0] = new typeFlexibleTimeSpec();
                    leg.Items[0].PreferredTime = request.Segments[segIndex].PreferredDepartureTime.ToString("yyyy-MM-dd");
                    AirLegModifiers legModifiers = new AirLegModifiers();
                    if (request.Segments[0].flightCabinClass != CabinClass.All)
                    {
                        PreferredCabins prefCabin = new PreferredCabins();
                        UAPIdll.Air46.CabinClass airCabin = new UAPIdll.Air46.CabinClass();
                        airCabin.Type = cabinType.ToString();
                        prefCabin.CabinClass = airCabin;
                        legModifiers.PreferredCabins = new PreferredCabins();
                        legModifiers.PreferredCabins = prefCabin;
                    }

                    if (request.RestrictAirline)
                    {

                        if (request.Segments[segIndex].PreferredAirlines != null && request.Segments[segIndex].PreferredAirlines.Length > 0)
                        {
                            // For Preffered Airlines
                            //Carrier[] tmpCarrierList = new Carrier[1];
                            List<Carrier> CarrierList = new List<Carrier>();
                            foreach (string carrier in request.Segments[segIndex].PreferredAirlines)
                            {
                                Carrier tmpCarrier = new Carrier();
                                tmpCarrier.Code = carrier;
                                CarrierList.Add(tmpCarrier);
                            }
                            legModifiers.PreferredCarriers = CarrierList.ToArray();
                        }
                    }

                    // Preferred Cabin for Outbound
                    leg.AirLegModifiers = legModifiers;

                    segIndex++;
                }
            }
            else  // one way and round trip
            {


                lfRequest.Items = new SearchAirLeg[2];
                lfRequest.Items[0] = new SearchAirLeg();
                SearchAirLeg leg = lfRequest.Items[0] as SearchAirLeg;

                UAPIdll.Air46.CityOrAirport origin = new UAPIdll.Air46.CityOrAirport();
                origin.Code = request.Segments[0].Origin;
                origin.PreferCity = request.Segments[0].NearByOriginPort;

                UAPIdll.Air46.CityOrAirport destination = new UAPIdll.Air46.CityOrAirport();
                destination.Code = request.Segments[0].Destination;
                destination.PreferCity = true;

                leg.SearchOrigin = new typeSearchLocation[1];
                leg.SearchOrigin[0] = new typeSearchLocation();
                leg.SearchOrigin[0].Item = origin;

                leg.SearchDestination = new typeSearchLocation[1];
                leg.SearchDestination[0] = new typeSearchLocation();
                leg.SearchDestination[0].Item = destination;

                leg.Items = new typeFlexibleTimeSpec[1];
                leg.Items[0] = new typeFlexibleTimeSpec();
                leg.Items[0].PreferredTime = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");
                AirLegModifiers legModifiers = new AirLegModifiers();
                if (request.Segments[0].flightCabinClass != CabinClass.All)
                {
                    PreferredCabins prefCabin = new PreferredCabins();
                    UAPIdll.Air46.CabinClass airCabin = new UAPIdll.Air46.CabinClass();
                    airCabin.Type = cabinType.ToString();
                    prefCabin.CabinClass = airCabin;
                    legModifiers.PreferredCabins = new PreferredCabins();
                    legModifiers.PreferredCabins = prefCabin;
                }
                if (request.RestrictAirline)
                {

                    if (request.Segments[0].PreferredAirlines != null && request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        // For Preffered Airlines
                        //Carrier[] tmpCarrierList = new Carrier[1];
                        //Carrier tmpCarrier = new Carrier();
                        //tmpCarrier.Code = request.Segments[0].PreferredAirlines[0].ToString();
                        //tmpCarrierList[0] = new Carrier();
                        //tmpCarrierList[0] = tmpCarrier;
                        //legModifiers.PreferredCarriers = tmpCarrierList;

                        Carrier[] tmpCarrierList = new Carrier[request.Segments[0].PreferredAirlines.Length];

                        for (int i = 0; i < request.Segments[0].PreferredAirlines.Length; i++)
                        {
                            if (!RestrictedAirlines.Exists(delegate (string airline) { return airline == request.Segments[0].PreferredAirlines[i]; }))
                            {
                                tmpCarrierList[i] = new Carrier();
                                tmpCarrierList[i].Code = request.Segments[0].PreferredAirlines[i];
                            }
                        }
                        legModifiers.PreferredCarriers = tmpCarrierList;
                    }
                }

                //If Non Stop filter applied then MaxStops will be 0 in B2C
                //If Direct filter applied then MaxStops will be 0 in B2B
                if (!string.IsNullOrEmpty(request.MaxStops))
                {
                    if (Convert.ToInt32(request.MaxStops) >= 0)
                    {
                        legModifiers.FlightType = new FlightType();
                        legModifiers.FlightType.MaxStops = request.MaxStops;
                    }

                }


                // Preferred Cabin for Outbound
                leg.AirLegModifiers = legModifiers;
                //lfRequest.SearchAirLeg[0].AirLegModifiers = legModifiers;


                // Return Leg
                if (request.Type == SearchType.Return && !searchBySegments)
                {
                    lfRequest.Items[1] = new SearchAirLeg();
                    SearchAirLeg legReturn = lfRequest.Items[1] as SearchAirLeg;

                    UAPIdll.Air46.CityOrAirport retOrigin = new UAPIdll.Air46.CityOrAirport();
                    retOrigin.Code = request.Segments[0].Destination;
                    retOrigin.PreferCity = request.Segments[0].NearByOriginPort;

                    UAPIdll.Air46.CityOrAirport retDestination = new UAPIdll.Air46.CityOrAirport();
                    retDestination.Code = request.Segments[0].Origin;
                    retDestination.PreferCity = true;

                    legReturn.SearchOrigin = new typeSearchLocation[1];
                    legReturn.SearchOrigin[0] = new typeSearchLocation();
                    legReturn.SearchOrigin[0].Item = retOrigin;

                    legReturn.SearchDestination = new typeSearchLocation[1];
                    legReturn.SearchDestination[0] = new typeSearchLocation();
                    legReturn.SearchDestination[0].Item = retDestination;


                    legReturn.Items = new typeFlexibleTimeSpec[1];
                    legReturn.Items[0] = new typeFlexibleTimeSpec();
                    legReturn.Items[0].PreferredTime = request.Segments[1].PreferredDepartureTime.ToString("yyyy-MM-dd");


                    // Preferred Cabin for Inbound
                    legReturn.AirLegModifiers = legModifiers;

                }
            }

            //// For No Of Stops
            if (!string.IsNullOrEmpty(request.MaxStops))
            {
                if (Convert.ToInt32(request.MaxStops) > 0)
                {
                    airModifiers.PreferNonStop = false;
                }
                else
                {
                    airModifiers.PreferNonStop = true;
                }
                //airModifiers.MaxStops = request.MaxStops;
            }
            // Fare Type (Refundable /Non Refundable) AirPricingModifiers 

            if (request.RefundableFares)
            {
                AirPricingModifiers priceModifier = new AirPricingModifiers();
                priceModifier.ProhibitNonRefundableFares = request.RefundableFares;// All Refundable will be shown
                lfRequest.AirPricingModifiers = priceModifier;
                //AirLowFareSearchAsynchBinding binding = new AirLowFareSearchAsynchBinding();

            }
            //For AirlinePrivateFares Pass Account Codes 31 May 2016
            //Modified by lokesh on 14Feb2019
            if (!string.IsNullOrEmpty(PrivateFareCodes))
            {
                string[] codes = PrivateFareCodes.Split(',');
                AirPricingModifiers priceModifier = null;
                if (lfRequest.AirPricingModifiers != null)
                {
                    priceModifier = lfRequest.AirPricingModifiers;
                }
                else
                {
                    priceModifier = new AirPricingModifiers();
                }
                priceModifier.AccountCodeFaresOnly = false;
                priceModifier.AccountCodeFaresOnlySpecified = true;
                priceModifier.AccountCodes = new AccountCode[codes.Length];
                for (int i = 0; i < codes.Length; i++)
                {
                    priceModifier.AccountCodes[i] = new AccountCode();
                    priceModifier.AccountCodes[i].Code = codes[i];
                    priceModifier.AccountCodes[i].ProviderCode = "1G";
                    //priceModifier.AccountCodes[i].SupplierCode = "";
                    //priceModifier.AccountCodes[i].Type = "";
                }
                priceModifier.FaresIndicatorSpecified = true;
                priceModifier.FaresIndicator = typeFaresIndicator.PublicAndPrivateFares;
                //priceModifier.FaresIndicator = typeFaresIndicator.AllFares;

                lfRequest.AirPricingModifiers = priceModifier;
            }            
                

            if (RestrictedAirlines.Count > 0)
            {
                int restrictedCount = 0;
                //Check whether FlyDubai is restricted by default
                if (RestrictedAirlines.Contains("FZ"))
                {
                    airModifiers.DisfavoredCarriers = new Carrier[RestrictedAirlines.Count];
                }
                else//If not restricted, add in the restriction list
                {
                    restrictedCount = RestrictedAirlines.Count + 1;
                    airModifiers.DisfavoredCarriers = new Carrier[restrictedCount];
                }

                for (int i = 0; i < RestrictedAirlines.Count; i++)
                {
                    airModifiers.DisfavoredCarriers[i] = new Carrier();
                    airModifiers.DisfavoredCarriers[i].Code = RestrictedAirlines[i];
                }
                if (airModifiers.DisfavoredCarriers.Length > RestrictedAirlines.Count)
                {
                    airModifiers.DisfavoredCarriers[airModifiers.DisfavoredCarriers.Length - 1] = new Carrier();
                    airModifiers.DisfavoredCarriers[airModifiers.DisfavoredCarriers.Length - 1].Code = "FZ";
                }
            }
            else 
            {
                airModifiers.DisfavoredCarriers = new Carrier[1];
                airModifiers.DisfavoredCarriers[0] = new Carrier();
                airModifiers.DisfavoredCarriers[0].Code = "FZ";
            }


            if (!string.IsNullOrEmpty(allowedAirlinesForGDS) && allowedAirlinesForGDS.Contains("FZ"))
            {
                string[] airlines = allowedAirlinesForGDS.Split(',');
                airlines.ToList().ForEach(a =>
                {
                    List<Carrier> carriers = airModifiers.DisfavoredCarriers.ToList();
                    carriers.RemoveAt(carriers.FindIndex(x => x.Code == a));
                    airModifiers.DisfavoredCarriers = carriers.ToArray();
                });
            }
                

            lfRequest.SolutionResult = true;

            //Assign this value to get additional details regarding Penalties and fare is Refundable/NonRefundable 
            //Implemented by Shiva 06 Nov 2019. 
            //TODO: You need to add two files in the Uapidll project from SVN (Air46.cs, Universal46.cs) to get the desired response
            lfRequest.FareRulesFilterCategory = new typeFareRuleCategoryCode[1];
            lfRequest.FareRulesFilterCategory[0] = new typeFareRuleCategoryCode();
            lfRequest.FareRulesFilterCategory[0] = typeFareRuleCategoryCode.CHG;

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(LowFareSearchReq));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, lfRequest); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object                
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchReq.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightSearchRequest_" + request.Segments[0].Origin + "_"
                    + request.Segments[0].Destination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            catch { }



            //Air20.AirLowFareSearchBinding binding = new Air20.AirLowFareSearchBinding();
            //binding.Url = urlAir;
            //binding.Credentials = new NetworkCredential(UserName, Password);
            UAPIdll.Air46.AirLowFareSearchBinding binding = new UAPIdll.Air46.AirLowFareSearchBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(UserName, Password);
            binding.PreAuthenticate = true;//to implement GZIP



            LowFareSearchRsp lfResponse = new LowFareSearchRsp();
            try
            {
                //Audit.Add(EventType.Search, Severity.Low, Convert.ToInt32(AppUserId), "UAPI Proxy search started", "");
                lfResponse = binding.service(lfRequest); //ziyad for local test Stop sending web request as we are calling soap request               
                //Audit.Add(EventType.Search, Severity.Low, Convert.ToInt32(AppUserId), "UAPI Proxy search ended", "");
            }
            catch (SoapException se)
            {
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            ////----------------------Loading previous xml-------------------------------
            //System.Xml.Serialization.XmlSerializer serLoc = new System.Xml.Serialization.XmlSerializer(typeof(LowFareSearchRsp));
            //string staticpath = ConfigurationManager.AppSettings["TestStaticXmlLoadPath"];
            //System.IO.StreamReader writerLoc = new StreamReader(@staticpath + "FlightSearchResponse_6f1bdba7-abbc-413d-8eaf-233aee464c39_4008_20062018_065819.xml");
            //lfResponse = serLoc.Deserialize(writerLoc) as LowFareSearchRsp;
            //writerLoc.Close();
            return lfResponse;
        }

        private string GenerateSoapRequest(SearchRequest request)
        {
            StringBuilder searchString = new StringBuilder();
            searchString.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>");
            searchString.Append("<soapenv:Header/>");
            searchString.Append("<soapenv:Body>");
            searchString.Append("<air:LowFareSearchReq xmlns:air='http://www.travelport.com/schema/air_v33_0' xmlns:com='http://www.travelport.com/schema/common_v33_0' AuthorizedBy='UAPI' SolutionResult='true' TargetBranch='" + TargetBranch + "' TraceId='trace'>");
            searchString.Append("<com:BillingPointOfSaleInfo xmlns:com='http://www.travelport.com/schema/common_v33_0' OriginApplication='UAPI'/>");

            Airline.GetAllAirlinesList();

            Dictionary<string, System.Collections.ArrayList> AllAirlines = CT.Configuration.CacheData.AirlineList;
            List<string> RestrictedAirlines = new List<string>();

            foreach (KeyValuePair<string, System.Collections.ArrayList> airline in AllAirlines)
            {
                if (!Convert.ToBoolean(airline.Value[22]))//UapiAllowed column value
                {
                    RestrictedAirlines.Add(airline.Key);
                }
            }

            if (request.Type == SearchType.OneWay || request.Type == SearchType.Return)
            {
                searchString.Append("<air:SearchAirLeg>");
                searchString.Append("<air:SearchOrigin>");
                searchString.Append("<com:Airport xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + request.Segments[0].Origin + "'/>");
                searchString.Append("</air:SearchOrigin>");
                searchString.Append("<air:SearchDestination>");
                searchString.Append("<com:Airport xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + request.Segments[0].Destination + "'/>");
                searchString.Append("</air:SearchDestination>");
                searchString.Append("<air:SearchDepTime PreferredTime='" + request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd") + "'/>");
                searchString.Append("<air:AirLegModifiers>");
                if (request.Segments[0].flightCabinClass != CabinClass.All)
                {
                    searchString.Append("<air:PreferredCabins>");
                }
                switch (request.Segments[0].flightCabinClass)
                {
                    case CabinClass.Business:
                        searchString.Append("<com:CabinClass Type='" + CabinClass.Business + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                        break;
                    case CabinClass.Economy:
                        searchString.Append("<com:CabinClass Type='" + CabinClass.Economy + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                        break;
                    case CabinClass.First:
                        searchString.Append("<com:CabinClass Type='" + CabinClass.First + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                        break;
                    case CabinClass.PremiumEconomy:
                        searchString.Append("<com:CabinClass Type='" + CabinClass.PremiumEconomy + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                        break;
                }
                if (request.Segments[0].flightCabinClass != CabinClass.All)
                {
                    searchString.Append("</air:PreferredCabins>");
                }
                if (request.RestrictAirline)
                {
                    if (request.Segments[0].PreferredAirlines != null && request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        searchString.Append("<air:PreferredCarriers>");
                        foreach (string carrier in request.Segments[0].PreferredAirlines)
                        {
                            if (!RestrictedAirlines.Exists(delegate (string airline) { return airline == carrier; }))
                            {
                                searchString.Append("<com:Carrier xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + carrier + "'/>");
                            }
                        }
                        searchString.Append("</air:PreferredCarriers>");
                    }
                }
                if (!string.IsNullOrEmpty(request.MaxStops) && Convert.ToInt32(request.MaxStops) >= 0)
                {
                    searchString.Append("<air:FlightType MaxStops='" + request.MaxStops + "' />");
                }
                searchString.Append("</air:AirLegModifiers>");
                searchString.Append("</air:SearchAirLeg>");
            }
            if (request.Type == SearchType.Return)
            {
                searchString.Append("<air:SearchAirLeg>");
                searchString.Append("<air:SearchOrigin>");
                searchString.Append("<com:Airport xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + request.Segments[0].Destination + "'/>");
                searchString.Append("</air:SearchOrigin>");
                searchString.Append("<air:SearchDestination>");
                searchString.Append("<com:Airport xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + request.Segments[0].Origin + "'/>");
                searchString.Append("</air:SearchDestination>");
                searchString.Append("<air:SearchDepTime PreferredTime='" + request.Segments[1].PreferredDepartureTime.ToString("yyyy-MM-dd") + "'/>");
                searchString.Append("<air:AirLegModifiers>");
                if (request.Segments[0].flightCabinClass != CabinClass.All)
                {
                    searchString.Append("<air:PreferredCabins>");
                }
                switch (request.Segments[0].flightCabinClass)
                {
                    case CabinClass.Business:
                        searchString.Append("<com:CabinClass Type='" + CabinClass.Business + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                        break;
                    case CabinClass.Economy:
                        searchString.Append("<com:CabinClass Type='" + CabinClass.Economy + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                        break;
                    case CabinClass.First:
                        searchString.Append("<com:CabinClass Type='" + CabinClass.First + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                        break;
                    case CabinClass.PremiumEconomy:
                        searchString.Append("<com:CabinClass Type='" + CabinClass.PremiumEconomy + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                        break;
                }
                if (request.Segments[0].flightCabinClass != CabinClass.All)
                {
                    searchString.Append("</air:PreferredCabins>");
                }
                if (request.RestrictAirline)
                {
                    if (request.Segments[0].PreferredAirlines != null && request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        searchString.Append("<air:PreferredCarriers>");
                        foreach (string carrier in request.Segments[0].PreferredAirlines)
                        {
                            if (!RestrictedAirlines.Exists(delegate (string airline) { return airline == carrier; }))
                            {
                                searchString.Append("<com:Carrier xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + carrier + "'/>");
                            }
                        }
                        searchString.Append("</air:PreferredCarriers>");
                    }
                }
                if (!string.IsNullOrEmpty(request.MaxStops) && Convert.ToInt32(request.MaxStops) >= 0)
                {
                    searchString.Append("<air:FlightType MaxStops='" + request.MaxStops + "' />");
                }
                searchString.Append("</air:AirLegModifiers>");
                searchString.Append("</air:SearchAirLeg>");
            }
            else if (request.Type == SearchType.MultiWay)
            {
                foreach (FlightSegment segment in request.Segments)
                {
                    searchString.Append("<air:SearchAirLeg>");
                    searchString.Append("<air:SearchOrigin>");
                    searchString.Append("<com:Airport xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + segment.Origin + "'/>");
                    searchString.Append("</air:SearchOrigin>");
                    searchString.Append("<air:SearchDestination>");
                    searchString.Append("<com:Airport xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + segment.Destination + "'/>");
                    searchString.Append("</air:SearchDestination>");
                    searchString.Append("<air:SearchDepTime PreferredTime='" + segment.PreferredDepartureTime.ToString("yyyy-MM-dd") + "'/>");
                    searchString.Append("<air:AirLegModifiers>");
                    if (segment.flightCabinClass != CabinClass.All)
                    {
                        searchString.Append("<air:PreferredCabins>");
                    }
                    switch (segment.flightCabinClass)
                    {
                        case CabinClass.Business:
                            searchString.Append("<com:CabinClass Type='" + CabinClass.Business + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                            break;
                        case CabinClass.Economy:
                            searchString.Append("<com:CabinClass Type='" + CabinClass.Economy + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                            break;
                        case CabinClass.First:
                            searchString.Append("<com:CabinClass Type='" + CabinClass.First + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                            break;
                        case CabinClass.PremiumEconomy:
                            searchString.Append("<com:CabinClass Type='" + CabinClass.PremiumEconomy + "' xmlns:com='http://www.travelport.com/schema/common_v33_0' />");
                            break;
                    }
                    if (segment.flightCabinClass != CabinClass.All)
                    {
                        searchString.Append("</air:PreferredCabins>");
                    }
                    if (request.RestrictAirline)
                    {
                        if (segment.PreferredAirlines != null && segment.PreferredAirlines.Length > 0)
                        {
                            searchString.Append("<air:PreferredCarriers>");
                            foreach (string carrier in segment.PreferredAirlines)
                            {
                                if (!RestrictedAirlines.Exists(delegate (string airline) { return airline == carrier; }))
                                {
                                    searchString.Append("<com:Carrier xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + carrier + "'/>");
                                }
                            }
                            searchString.Append("</air:PreferredCarriers>");
                        }
                    }
                    if (!string.IsNullOrEmpty(request.MaxStops) && Convert.ToInt32(request.MaxStops) >= 0)
                    {
                        searchString.Append("<air:FlightType MaxStops='" + request.MaxStops + "' />");
                    }
                    searchString.Append("</air:AirLegModifiers>");
                    searchString.Append("</air:SearchAirLeg>");
                }
            }
            if (!string.IsNullOrEmpty(request.MaxStops) && Convert.ToInt32(request.MaxStops) >= 0)
            {
                searchString.Append("<air:AirSearchModifiers PreferNonStop='true' xmlns:air='http://www.travelport.com/schema/air_v33_0'>");
            }
            else
            {
                searchString.Append("<air:AirSearchModifiers>");
            }
            searchString.Append("<air:PreferredProviders>");
            searchString.Append("<com:Provider xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='1G'/>");
            searchString.Append("</air:PreferredProviders>");



            searchString.Append("<air:DisfavoredCarriers>");
            if (RestrictedAirlines.Count > 0)
            {
                foreach (string airline in RestrictedAirlines)
                {
                    searchString.Append("<com:Carrier xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='" + airline + "' />");
                }
                if (!RestrictedAirlines.Contains("FZ"))
                {
                    searchString.Append("<com:Carrier xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='FZ' />");
                }
            }
            else
            {
                searchString.Append("<com:Carrier xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='FZ' />");
            }
            searchString.Append("</air:DisfavoredCarriers>");
            searchString.Append("</air:AirSearchModifiers>");
            searchString.Append("<com:SearchPassenger xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='ADT'/>");
            if (request.ChildCount > 0)
            {
                searchString.Append("<com:SearchPassenger xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='CNN' Age='5'/>");
            }
            if (request.InfantCount > 0)
            {
                searchString.Append("<com:SearchPassenger xmlns:com='http://www.travelport.com/schema/common_v33_0' Code='INF'/>");
            }


            if (ConfigurationManager.AppSettings["UAPIAccountCodes"] != null && request.RefundableFares)
            {
                searchString.Append("<air:AirPricingModifiers FaresIndicator='AllFares' ProhibitNonRefundableFares='true' xmlns:air='http://www.travelport.com/schema/air_v33_0'>");
                string[] AccountCodes = ConfigurationManager.AppSettings["UAPIAccountCodes"].Split(',');
                if (AccountCodes.Length > 0)
                {
                    searchString.Append("<air:AccountCodes>");
                    foreach (string code in AccountCodes)
                    {
                        searchString.Append("<com:AccountCode Code='" + code + "' ProviderCode='1G' xmlns:com='http://www.travelport.com/schema/common_v33_0'/>");
                    }
                    searchString.Append("</air:AccountCodes>");
                }
                searchString.Append("</air:AirPricingModifiers>");
            }
            else if (ConfigurationManager.AppSettings["UAPIAccountCodes"] != null && !request.RefundableFares)
            {
                string[] AccountCodes = ConfigurationManager.AppSettings["UAPIAccountCodes"].Split(',');
                if (AccountCodes.Length > 0)
                {
                    searchString.Append("<air:AirPricingModifiers FaresIndicator='AllFares' xmlns:air='http://www.travelport.com/schema/air_v33_0'>");
                    searchString.Append("<air:AccountCodes>");
                    foreach (string code in AccountCodes)
                    {
                        searchString.Append("<com:AccountCode Code='" + code + "' ProviderCode='1G' xmlns:com='http://www.travelport.com/schema/common_v33_0'/>");
                    }
                    searchString.Append("</air:AccountCodes>");
                    searchString.Append("</air:AirPricingModifiers>");
                }
            }
            else if (request.RefundableFares)
            {
                searchString.Append("<air:AirPricingModifiers ProhibitNonRefundableFares='true' xmlns:air='http://www.travelport.com/schema/air_v33_0'/>");
            }

            searchString.Append("</air:LowFareSearchReq>");
            searchString.Append("</soapenv:Body>");
            searchString.Append("</soapenv:Envelope>");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(searchString.ToString());
            //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchReq.xml");
            string filePath = xmlSoapPath + "FlightSOAPSearchRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
            doc.Save(filePath);

            return searchString.ToString();
        }

        private string GetSearchResponse(SearchRequest srequest)
        {
            string responseFromServer = string.Empty;
            try
            {
                Connection();
                const string contentType = "text/xml";
                Version _protocolVersion = HttpVersion.Version11;
                //ServicePointManager.MaxServicePoints = 4;
                //ServicePointManager.SetTcpKeepAlive(true, 5000, 1000);                

                ServicePoint sp = ServicePointManager.FindServicePoint(new Uri(urlAir));
                sp.Expect100Continue = false;
                sp.UseNagleAlgorithm = false;
                //sp.ReceiveBufferSize = 52428800; //50 MB
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAir);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateSoapRequest(srequest);//Gets the XML Payment Request String.

                request.ContentType = contentType;
                request.KeepAlive = true;
                request.ProtocolVersion = HttpVersion.Version11;

                //Basic Authentication required to process the request.               
                string _auth = string.Format("{0}:{1}", UserName, Password);
                string _enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));
                string _cred = string.Format("{0} {1}", "Basic", _enc);
                request.Headers.Add(HttpRequestHeader.Authorization, _cred);
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                byte[] lbPostBuffer = Encoding.Default.GetBytes(postData);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = lbPostBuffer.Length;
                Stream PostStream = request.GetRequestStream();
                PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                PostStream.Close();

                //Audit.Add(EventType.Search, Severity.Low, Convert.ToInt32(AppUserId), "UAPI SOAP Search hit", "");
                //Get the response
                using (WebResponse response = request.GetResponse())
                {
                    //Audit.Add(EventType.Search, Severity.Low, Convert.ToInt32(AppUserId), "UAPI Got SOAP response stream", "");
                    HttpWebResponse httpResponse = response as HttpWebResponse;
                    // handle if response is a compressed stream
                    using (Stream dataStream = GetStreamForResponse(httpResponse))
                    {
                        Encoding enc = System.Text.Encoding.GetEncoding(1252);
                        using (StreamReader reader = new StreamReader(dataStream, enc))
                        {
                            responseFromServer = reader.ReadToEnd();
                            //Audit.Add(EventType.Search, Severity.Low, Convert.ToInt32(AppUserId), "UAPI Search SOAP response read", "");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            return responseFromServer;
        }

        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }

        //private void ReadFlightInfoResponse(LowFareSearchRsp lfResponse, ref List<FlightInfo> outboundflightList, ref List<FlightInfo> inboundflightList)
        private void ReadFlightInfoResponse(LowFareSearchRsp lfResponse, ref Dictionary<string, List<FlightInfo>> outboundflightList, ref Dictionary<string, List<FlightInfo>> inboundflightList, ref SearchRequest request)
        {
            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(LowFareSearchRsp));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, lfResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchRes.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightSearchResponse_" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            catch { }
            //List<FlightInfo> outBound = new List<FlightInfo>();
            //List<FlightInfo> inBound = new List<FlightInfo>();
            outboundflightList = new Dictionary<string, List<FlightInfo>>();
            inboundflightList = new Dictionary<string, List<FlightInfo>>();
            bool isError = false;
            //ResponseMessage[] lfMessage=lfResponse.ResponseMessage;
            //for (int x = 0; x < lfMessage.Length; x++)
            //{
            //    if (lfMessage[x].Type == ResponseMessageType.Error)
            //    {isError = true;break;}
            //}

            FlightDetails[] lfFlightDetails = lfResponse.FlightDetailsList;
            typeBaseAirSegment[] lfAirSegment = lfResponse.AirSegmentList;

            if (!isError)
            {
                int segGroup = 0;
                for (int x = 0; x < lfAirSegment.Length; x++)
                {
                    //if (lfAirSegment[x].Carrier != "FZ")
                    {
                        segGroup = lfAirSegment[x].Group;

                        string segmentKey = lfAirSegment[x].Key;//keyGen(lfAirSegment[x].Key);
                        string flightKey = "";



                        FlightDetails lfFlight = null;
                        //All FlightDetails List
                        List<FlightDetails> Flights = new List<FlightDetails>(lfFlightDetails);
                        //Segment wise FlightDetails reference
                        List<FlightDetailsRef> FlightDetails = new List<FlightDetailsRef>(lfAirSegment[x].FlightDetailsRef);
                        //Store all details of the connection for each AirSegment
                        List<FlightInfo> FlightInfo = new List<BookingEngine.FlightInfo>();

                        //Loop thru each segment wise FlightDetailsRef list to match against FlightDetails List
                        for (int i = 0; i < FlightDetails.Count; i++)
                        {
                            lfFlight = Flights.Find(delegate (FlightDetails fd) { return fd.Key == FlightDetails[i].Key; });
                            flightKey = FlightDetails[i].Key;
                            FlightInfo flight = new FlightInfo();
                            flight.UapiSegmentRefKey = segmentKey;
                            //for (int i = 0; i < lfFlightDetails.Length; i++)
                            //{
                            //    string tempflightKey = lfFlightDetails[i].Key;//keyGen(lfFlightDetails[i].Key);
                            //    //FlightDetails[] lfFlightLit = lfFlightDetails[0].Key;
                            //    //for (int f = 0; f < lfFlightDetails; f++)
                            //    //{

                            //    //}
                            //    if (tempflightKey == lfAirSegment[x].FlightDetailsRef[0].Key) //keyGen(lfAirSegment[x].FlightDetailsRef[0].Key))
                            //    {
                            //        //  lfFlight=lfAirSegment[x].
                            //        flightKey = lfAirSegment[x].FlightDetailsRef[0].Key;//keyGen(lfAirSegment[x].FlightDetailsRef[0].Key);
                            //        lfFlight = lfFlightDetails[i];
                            //        break;

                            //    }
                            //    //FlightDetails lfFlight=lfFlightDetails[i].ke
                            //}

                            //flight.FlightId = flightKey;


                            //flight.Origin = new Airport(lfAirSegment[x].Origin);
                            flight.Origin = new Airport(lfFlight.Origin);
                            //flight.Destination = new Airport(lfAirSegment[x].Destination);
                            flight.Destination = new Airport(lfFlight.Destination);
                            flight.Airline = lfAirSegment[x].Carrier;
                            flight.FlightNumber = lfAirSegment[x].FlightNumber;

                            //Local Time
                            //string GMTdiff = lfAirSegment[x].DepartureTime.IndexOf("+") != -1 ? "+" : "-";
                            string GMTdiff = lfFlight.DepartureTime.IndexOf("+") != -1 ? "+" : "-";
                            //string depTime = lfAirSegment[x].DepartureTime.Substring(0, lfAirSegment[x].DepartureTime.LastIndexOf(GMTdiff));
                            string depTime = lfFlight.DepartureTime.Substring(0, lfFlight.DepartureTime.LastIndexOf(GMTdiff));
                            //GMTdiff = lfAirSegment[x].ArrivalTime.IndexOf("+") != -1 ? "+" : "-";
                            GMTdiff = lfFlight.ArrivalTime.IndexOf("+") != -1 ? "+" : "-";
                            //string arrTime = lfAirSegment[x].ArrivalTime.Substring(0, lfAirSegment[x].ArrivalTime.LastIndexOf(GMTdiff));
                            string arrTime = lfFlight.ArrivalTime.Substring(0, lfFlight.ArrivalTime.LastIndexOf(GMTdiff));
                            //Assign CodeShareInfo
                            if (lfAirSegment[x].CodeshareInfo != null && lfAirSegment[x].CodeshareInfo.OperatingCarrier != null && lfAirSegment[x].CodeshareInfo.OperatingFlightNumber != null)
                            {
                                flight.OperatingCarrier = lfAirSegment[x].CodeshareInfo.OperatingCarrier + "-" + lfAirSegment[x].CodeshareInfo.OperatingFlightNumber + " " + lfAirSegment[x].CodeshareInfo.Value;
                            }

                            flight.DepartureTime = Convert.ToDateTime(depTime);
                            flight.ArrivalTime = Convert.ToDateTime(arrTime);

                            //GMt Time
                            //flight.DepartureTime = Convert.ToDateTime(lfAirSegment[x].DepartureTime);
                            //flight.ArrivalTime= Convert.ToDateTime(lfAirSegment[x].ArrivalTime);
                            // For UAPI REservation Request
                            //flight.UapiDepartureTime = lfAirSegment[x].DepartureTime;
                            flight.UapiDepartureTime = lfFlight.DepartureTime;
                            //flight.UapiArrivalTime = lfAirSegment[x].ArrivalTime;
                            flight.UapiArrivalTime = lfFlight.ArrivalTime;


                            //flight.Craft = lfAirSegment[x].Equipment;
                            flight.Craft = lfFlight.Equipment;
                            //int duration = Convert.ToInt32(lfAirSegment[x].FlightTime);
                            int duration = Convert.ToInt32(lfFlight.FlightTime);
                            flight.Duration = new TimeSpan(0, duration, 0);
                            flight.DepTerminal = lfFlight.OriginTerminal;
                            flight.ArrTerminal = lfFlight.DestinationTerminal;
                            flight.ETicketEligible = lfAirSegment[x].ETicketabilitySpecified;
                            if (searchBySegments)
                                flight.Group = request.Type == SearchType.OneWay ? 0 : 1;
                            else
                                flight.Group = segGroup;
                            if (flight.UAPIReservationValues == null)
                            {
                                flight.UAPIReservationValues = new System.Collections.Hashtable();
                            }
                            flight.UAPIReservationValues[LINK_AVAILABILITY] = lfAirSegment[x].LinkAvailability;
                            if (lfAirSegment[x].AvailabilitySource != null)
                            {
                                flight.UAPIReservationValues[AVAILABILITY_SOURCE] = lfAirSegment[x].AvailabilitySource;
                            }
                            else
                            {
                                flight.UAPIReservationValues[AVAILABILITY_SOURCE] = "";
                            }
                            flight.UAPIReservationValues[POLLED_AVAILABILITY_OPTION] = lfAirSegment[x].PolledAvailabilityOption;
                            flight.UAPIReservationValues[PROVIDER_CODE] = lfAirSegment[x].AirAvailInfo[0].ProviderCode;
                            flight.UAPIReservationValues["AirSegment"] = lfAirSegment[x];//Storing the AirSegment for UAPI Repricing request

                            FlightInfo.Add(flight);
                        }

                        if (segGroup == 0)
                        {
                            outboundflightList.Add(segmentKey, FlightInfo);
                        }
                        else
                        {
                            inboundflightList.Add(segmentKey, FlightInfo);
                        }

                    }
                }
            }
        }

        private SearchResult[] ReadFareResponse(LowFareSearchRsp lfResponse, SearchRequest request, Dictionary<string, List<FlightInfo>> outboundflightList, Dictionary<string, List<FlightInfo>> inboundflightList)
        {

            List<SearchResult> resultarray = new List<SearchResult>();
            List<string> resultKey = new List<string>();
            //List<FlightInfo> outbound = outboundflightList;
            //List<FlightInfo> inbound = inboundflightList;

            Dictionary<string, List<FlightInfo>> outbound = outboundflightList;
            Dictionary<string, List<FlightInfo>> inbound = inboundflightList;

            Dictionary<string, FareInfo> fareBasisCodeRef = new Dictionary<string, FareInfo>();
            Dictionary<string, string> fareBasisBaggage = new Dictionary<string, string>();
            //Dictionary<int, Air20.FareInfo> fareInfoDic = new Dictionary<int, Air20.FareInfo>();
            FareInfo[] fareInfoList = lfResponse.FareInfoList;
            for (int f = 0; fareInfoList.Length > f; f++)
            {
                FareInfo fareInfo = fareInfoList[f];
                string fareInfoKey = fareInfo.Key;//keyGen(fareInfo.Key);
                fareBasisCodeRef.Add(fareInfoKey, fareInfo);
                if (fareInfo.BaggageAllowance != null)
                {
                    if (fareInfo.BaggageAllowance.MaxWeight != null && !string.IsNullOrEmpty(fareInfo.BaggageAllowance.MaxWeight.Value) && fareInfo.BaggageAllowance.MaxWeight.Value.Length > 0 && fareInfo.BaggageAllowance.MaxWeight.Value != "0")
                    {
                        fareBasisBaggage.Add(fareInfoKey, fareInfo.BaggageAllowance.MaxWeight.Value + "Kg");//Append Kg for default baggage weight
                    }
                    else if (!string.IsNullOrEmpty(fareInfo.BaggageAllowance.NumberOfPieces) && fareInfo.BaggageAllowance.NumberOfPieces.Length > 0)
                    {
                        fareBasisBaggage.Add(fareInfoKey, fareInfo.BaggageAllowance.NumberOfPieces + " piece");
                    }
                    else
                    {
                        fareBasisBaggage.Add(fareInfoKey, "Airline Norms");
                    }
                }
            }


            string currency = lfResponse.CurrencyType;

            if (ExchangeRates.ContainsKey(currency))
            {
                rateOfExchange = Convert.ToDouble(ExchangeRates[currency]);
            }
            else
            {
                rateOfExchange = 1;
            }

            baseDiscountROE = (double)ExchangeRates["AED"];

            //AirPricingSolution[] pricingSolutionList = (AirPricingSolution[])lfResponse.Items;
            Fare[] fare = new Fare[0];
            bool allowResult = true;//If true then result is added to the collection otherwise ommitted
            foreach (AirPricingSolution pricingSolution in lfResponse.Items) // AirPricingSolution
            {
                //AirPricingSolution RRpricingSolution = new AirPricingSolution();// TO Pass with Air Create Reservation Request
                //RRpricingSolution.airse
                //If CancelPenalty is 100 % chargeable then avoid those results for corp
                //if (AvoidFullCancelPenalty && pricingSolution.AirPricingInfo.ToList().Any(p => p.CancelPenalty.Percentage != null && p.CancelPenalty.Percentage == "100.00"))
                //if (AvoidFullCancelPenalty && pricingSolution.AirPricingInfo.ToList().Any(p => p.CancelPenalty.Percentage != null && p.Refundable))
                if (AvoidFullCancelPenalty && pricingSolution.AirPricingInfo.ToList().Any(p => p.Refundable == false))
                {
                    allowResult = false;
                }
                else
                    allowResult = true;

                bool nonRefundable = false;
                double totalResultFare = 0;
                double totalbaseFare = 0;
                double totalSupplierFare = 0;
                string lastTktDate = string.Empty;
                //pricingSolution.Journey[0].
                //int fareKey = 0;
                double baseFare = 0;
                double totalFare = 0;
                double supplierFare = 0;

                fare = new Fare[pricingSolution.AirPricingInfo.Length];
                List<TaxBreakup> taxBreakups = new List<TaxBreakup>();
                BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                int infoIndex = 0;
                List<FlightInfo> flightLists = new List<FlightInfo>();
                foreach (AirPricingInfo pricingInfo in pricingSolution.AirPricingInfo) //AirPricingInfo
                {
                    baseFare = getCurrAmount(pricingInfo.ApproximateBasePrice) * rateOfExchange;

                    totalFare = getCurrAmount(pricingInfo.TotalPrice) * rateOfExchange;
                    supplierFare = getCurrAmount(pricingInfo.TotalPrice);
                    //assigning local Time
                    //For APAC this node will not come so check for it.
                    if (!string.IsNullOrEmpty(pricingInfo.LatestTicketingTime))
                    {
                        string GMTdiff = pricingInfo.LatestTicketingTime.IndexOf("+") != -1 ? "+" : "-";
                        lastTktDate = pricingInfo.LatestTicketingTime.Substring(0, pricingInfo.LatestTicketingTime.LastIndexOf(GMTdiff));
                    }
                    //lastTktDate = pricingInfo.LatestTicketingTime;
                    //nonRefundable = !pricingInfo.Refundable;

                    //Identifying whether the fare is Refundable or Non refundable based on the following conditions
                    //If CancelPenalty applies=Anytime Percent=100 then Non refundable OR  CancelPenalty applies = After Departure Percent = 100 then Non refundable
                    //If CancelPenalty is null and if FareRulesFilter.CHG.Refundability.Value is "Refundable" then we will treat as Refundable fare
                    //otherwise we will treat as Non refundable fare
                    if (pricingInfo.CancelPenalty != null && pricingInfo.CancelPenalty.Length > 0 && pricingInfo.CancelPenalty[0].PenaltyApplies != typeFarePenaltyPenaltyApplies.BeforeDeparture && !string.IsNullOrEmpty(pricingInfo.CancelPenalty[0].Percentage) && pricingInfo.CancelPenalty[0].Percentage == "100.00")
                    {
                        nonRefundable = true;
                    }
                    else
                    {
                        nonRefundable = (pricingInfo.FareRulesFilter != null && pricingInfo.FareRulesFilter.CHG != null && pricingInfo.FareRulesFilter.Refundability.Value.ToUpper() == "REFUNDABLE") ? false : true;
                    }

                    pricingInfo.FareInfo = new FareInfo[pricingInfo.FareInfoRef.Length];
                    int indexFareInfo = 0;
                    foreach (FareInfoRef tempFareInfo in pricingInfo.FareInfoRef)
                    {
                        pricingInfo.FareInfo[indexFareInfo] = fareBasisCodeRef[tempFareInfo.Key];
                        indexFareInfo++;

                    }

                    if (pricingInfo.TaxInfo != null)
                    {
                        foreach (typeTaxInfo tax in pricingInfo.TaxInfo)
                        {
                            if (taxBreakups.Exists(t => t.TaxCode == tax.Category))
                            {
                                TaxBreakup breakup = taxBreakups.Find(t => t.TaxCode == tax.Category);
                                breakup.TaxValue += (decimal)(getCurrAmount(tax.Amount) * pricingInfo.PassengerType.Length * rateOfExchange);

                            }
                            else
                            {
                                TaxBreakup breakup = new TaxBreakup();
                                breakup.TaxCode = tax.Category;
                                breakup.TaxValue = (decimal)(getCurrAmount(tax.Amount) * pricingInfo.PassengerType.Length * rateOfExchange);
                                taxBreakups.Add(breakup);
                            }
                        }
                    }

                    fare[infoIndex] = new Fare();

                    if (pricingInfo.PassengerType[0].Code == "ADT") // Adult
                    {
                        fare[infoIndex].PassengerType = PassengerType.Adult;
                        fare[infoIndex].PassengerCount = request.AdultCount;
                        fare[infoIndex].BaseFare = baseFare * request.AdultCount;
                        fare[infoIndex].TotalFare = totalFare * request.AdultCount;
                        /*****************************************************************
                        * Discouting AED 20 for all users (both B2B & B2C) applying ROE
                        * in respective currency and deducting from Total Fare. In case 
                        * of Pax wise Fare BreakDown only Adult Fare will be affected.
                        * ***************************************************************/
                        fare[infoIndex].TotalFare -= (baseDiscount * baseDiscountROE);
                        /************************END Applying Discount*******************/
                        fare[infoIndex].SupplierFare = (supplierFare) * request.AdultCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "CNN")// Children
                    {
                        fare[infoIndex].PassengerType = PassengerType.Child;
                        fare[infoIndex].PassengerCount = request.ChildCount;
                        fare[infoIndex].BaseFare = baseFare * request.ChildCount;
                        fare[infoIndex].TotalFare = totalFare * request.ChildCount;
                        fare[infoIndex].SupplierFare = (supplierFare) * request.ChildCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "INF")// Infant
                    {
                        fare[infoIndex].PassengerType = PassengerType.Infant;
                        fare[infoIndex].PassengerCount = request.InfantCount;
                        fare[infoIndex].BaseFare = baseFare * request.InfantCount;
                        fare[infoIndex].TotalFare = totalFare * request.InfantCount;
                        fare[infoIndex].SupplierFare = (supplierFare) * request.InfantCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "SRC")// Senior citizen
                    {
                        fare[infoIndex].PassengerType = PassengerType.Senior;
                        fare[infoIndex].PassengerCount = request.SeniorCount;
                        fare[infoIndex].BaseFare = baseFare * request.SeniorCount;
                        fare[infoIndex].TotalFare = totalFare * request.SeniorCount;
                        fare[infoIndex].SupplierFare = (supplierFare) * request.SeniorCount;
                    }
                    totalbaseFare += baseFare * fare[infoIndex].PassengerCount;
                    totalResultFare += totalFare * fare[infoIndex].PassengerCount;
                    totalSupplierFare += (supplierFare) * fare[infoIndex].PassengerCount;
                    infoIndex++;
                }// Air Pricing Info End

                /*****************************************************************
                    * Discouting AED 20 for all users (both B2B & B2C) applying ROE
                    * in respective currency and deducting from Total Fare. In case 
                    * of Pax wise Fare BreakDown only Adult Fare will be affected.
                    * ***************************************************************/
                totalResultFare -= (baseDiscount * baseDiscountROE);
                /************************END Applying Discount********************/


                // Adding segments in single result
                List<FlightInfo[]> outFlights = new List<FlightInfo[]>();
                List<FlightInfo[]> inFlights = new List<FlightInfo[]>();

                // Adding Connection Details
                Connection[] connectionList = pricingSolution.Connection;
                //Adding Jopurney Details
                Journey[] journeyList = pricingSolution.Journey;
                int jIndex = 0;
                int segmentIndex = 0;
                foreach (Journey tempJourney in journeyList) // Journey Array
                {
                    FlightInfo[] flights = new FlightInfo[0];
                    flightLists = new List<FlightInfo>();//recreate flights list
                    AirSegmentRef[] jSegRefList = tempJourney.AirSegmentRef;
                    int segRefIndex = 0;
                    foreach (AirSegmentRef jSegRef in jSegRefList)
                    {
                        string jSegRefKey = (jSegRef.Key);
                        if (jIndex == 0)//outbound flights
                        {
                            flights = new FlightInfo[outbound[jSegRefKey].Count];
                        }
                        else//inbound flights
                        {
                            flights = new FlightInfo[inbound[jSegRefKey].Count];
                        }

                        //string jSegRefKey = (jSegRef.Key);

                        if (!flightLists.Exists(delegate (FlightInfo fi) { return fi.UapiSegmentRefKey == jSegRefKey; }))
                        {
                            if (jIndex == 0)// outbound
                            {
                                //outbound[jSegRefKey].Group = "0";
                                try
                                {
                                    if (!flightLists.Exists(delegate (FlightInfo fi) { return fi.UapiSegmentRefKey == jSegRefKey; }))
                                    {
                                        //Read all outbound connections
                                        for (int i = 0; i < flights.Length; i++)
                                        {
                                            flights[i] = outbound[jSegRefKey][i];
                                        }

                                        flightLists.AddRange(outbound[jSegRefKey]);
                                    }
                                }
                                catch { continue; }
                            }
                            else // return
                            {
                                //outbound[jSegRefKey].Group = "1";
                                try
                                {
                                    if (!flightLists.Exists(delegate (FlightInfo fi) { return fi.UapiSegmentRefKey == jSegRefKey; }))
                                    {
                                        //Read all inbound connections
                                        for (int i = 0; i < flights.Length; i++)
                                        {
                                            flights[i] = inbound[jSegRefKey][i];
                                        }
                                        flightLists.AddRange(inbound[jSegRefKey]);
                                    }
                                }
                                catch { continue; }
                            }
                            string bookingClass = string.Empty;
                            string cabinClass = string.Empty;
                            string fareInfoKey = "";
                            string segmentRef = string.Empty;
                            // pricingSolution.AirSegment = new AirSegment[bookingInfoList.Length];// to assign airsegment in Create Reservation Pricing Info
                            //int bkInfoIndex = 0;
                            foreach (BookingInfo bookingInfo in bookingInfoList)
                            {
                                if ((bookingInfo.SegmentRef) == jSegRefKey)
                                {
                                    bookingClass = bookingInfo.BookingCode;
                                    //typeCabinClass tempCabinClass = bookingInfo.CabinClass;
                                    cabinClass = bookingInfo.CabinClass.ToString();
                                    fareInfoKey = (bookingInfo.FareInfoRef);
                                    segmentRef = bookingInfo.SegmentRef;
                                    break;
                                    //Air20.FareInfo tempfareInfo = fareInfoDic[keyGen(bookingInfo.FareInfoRef)];
                                    //fareBasisCodeRef.Add(keyGen(bookingInfo.SegmentRef), tempfareInfo);
                                }
                            }
                            for (int i = 0; i < flights.Length; i++)
                            {
                                flights[i].TourCode = fareBasisCodeRef[fareInfoKey].TourCode;//Adding Tour Code for Negotiated Fare
                                flights[i].UAPIReservationValues[SEGMENT_REF] = segmentRef;
                                flights[i].BookingClass = bookingClass;
                                flights[i].CabinClass = cabinClass;
                                flights[i].FareInfoKey = fareInfoKey;

                                //Adding Connection Details
                                if (connectionList != null && connectionList.Length > 0)
                                {
                                    foreach (Connection tempConnection in connectionList) // Connection Array
                                    {
                                        int conSegIndex = Convert.ToInt32(tempConnection.SegmentIndex);
                                        if (conSegIndex == segmentIndex)
                                        {
                                            flights[i].StopOver = true; // Connection Flight
                                            break;
                                        }
                                        //else
                                        //{
                                        //    flights[segRefIndex].StopOver = false;
                                        //}
                                    }
                                }
                            }

                            segRefIndex += 1;

                            if (segRefIndex == jSegRefList.Length)
                            {
                                if (jIndex == 0)// outbound
                                {
                                    outFlights.Add(flightLists.ToArray());
                                }
                                else // return
                                {
                                    inFlights.Add(flightLists.ToArray());
                                }

                            }
                            segmentIndex++;
                        }
                    }
                    jIndex++;


                }// Journey Array End


                if (inFlights.Count > 0 && allowResult) // return
                {

                    SearchResult result = new SearchResult();
                    result.UapiPricingSolution = pricingSolution;// for UAPI Create Reservatiobn Req.... to get Airfare with response
                                                                 //result.UapiPricingSolution = new object();
                    result.FareBreakdown = fare;
                    result.TotalFare = totalResultFare;
                    result.BaseFare = totalbaseFare;
                    result.Price = new PriceAccounts();
                    result.Price.SupplierPrice = (decimal)totalSupplierFare;
                    result.Price.SupplierCurrency = currency;
                    result.Price.TaxBreakups = taxBreakups;
                    result.Price.K3Tax = taxBreakups.Where(x => x.TaxCode == "K3").Sum(y => y.TaxValue);
                    result.Tax = totalResultFare - totalbaseFare;
                    result.LastTicketDate = lastTktDate;
                    result.Currency = agentBaseCurrency;
                    result.NonRefundable = nonRefundable;
                    //result.ResultBookingSource = BookingSource.Galileo;
                    result.ResultBookingSource = BookingSource.UAPI;
                    result.FareRules = new List<FareRule>();
                    result.Flights = new FlightInfo[2][];
                    //AirSegments need to be updated in UAPIPricingSolution for UAPI Repricing
                    List<typeBaseAirSegment> airSegments = new List<typeBaseAirSegment>();
                    result.Flights[0] = new FlightInfo[outFlights[0].Length];
                    for (int k = 0; k < outFlights[0].Length; k++)
                    {
                        result.Flights[0][k] = new FlightInfo();
                        outFlights[0][k].Stops = outFlights[0].Length - 1;
                        result.Flights[0][k] = FlightInfo.Copy(outFlights[0][k]);
                        //Adding outbound AirSegments to the list 
                        //Avoid adding connection segments, only direct segments should be sent in further requests ex: Reprice
                        if (!airSegments.Exists(delegate (typeBaseAirSegment ba) { return ba.Key == ((typeBaseAirSegment)outFlights[0][k].UAPIReservationValues["AirSegment"]).Key; }))
                        {
                            airSegments.Add((typeBaseAirSegment)result.Flights[0][k].UAPIReservationValues["AirSegment"]);
                        }
                    }
                    // including mutli search as well

                    int inFlightLength = 0;
                    for (int f = 0; f < inFlights.Count; f++)
                    {
                        inFlightLength = inFlightLength + inFlights[f].Length;
                    }

                    result.Flights[1] = new FlightInfo[inFlightLength];
                    inFlightLength = 0;// reassigning values
                    for (int f = 0; f < inFlights.Count; f++)
                    {
                        for (int k = 0; k < inFlights[f].Length; k++)
                        {
                            result.Flights[1][inFlightLength] = new FlightInfo();
                            inFlights[f][k].Stops = inFlights[f].Length - 1;
                            result.Flights[1][inFlightLength] = FlightInfo.Copy(inFlights[f][k]);
                            inFlightLength++;
                            //Avoid adding connection segments, only direct segments should be sent in further requests ex: Reprice
                            if (!airSegments.Exists(delegate (typeBaseAirSegment ba) { return ba.Key == ((typeBaseAirSegment)inFlights[f][k].UAPIReservationValues["AirSegment"]).Key; }))
                            {
                                airSegments.Add((typeBaseAirSegment)inFlights[f][k].UAPIReservationValues["AirSegment"]);
                            }
                        }
                    }
                    //Assigning all AirSegments to the UAPIPricingSolution 
                    result.UapiPricingSolution.AirSegment = airSegments.ToArray();
                    //result.Flights[1] = new FlightInfo[inFlights[0].Length];
                    //for (int k = 0; k < inFlights[0].Length; k++)
                    //{
                    //    result.Flights[1][k] = new FlightInfo();
                    //    inFlights[0][k].Stops = inFlights[0].Length - 1;
                    //    result.Flights[1][k] = FlightInfo.Copy(inFlights[0][k]);
                    //}

                    FlightInfo[] flightSegments = SearchResult.GetSegments(result);
                    for (int s = 0; s < flightSegments.Length; s++)
                    {
                        if (fareBasisCodeRef.ContainsKey(flightSegments[s].FareInfoKey))
                        {
                            flightSegments[s].TourCode = fareBasisCodeRef[flightSegments[s].FareInfoKey].TourCode;// Assigning Tourcode for Corporate Negotiated Fare

                            FareRule fareRule = new FareRule();
                            fareRule.Airline = flightSegments[s].Airline;
                            fareRule.Destination = flightSegments[s].Destination.AirportCode;
                            fareRule.Origin = flightSegments[s].Origin.AirportCode;

                            FareInfo tempFareInfo = fareBasisCodeRef[flightSegments[s].FareInfoKey];
                            fareRule.FareBasisCode = tempFareInfo.FareBasis;

                            FareRuleKey tempFareRuleKey = tempFareInfo.FareRuleKey;
                            fareRule.FareRuleKeyValue = tempFareRuleKey.Value;
                            fareRule.FareInfoRef = (tempFareRuleKey.FareInfoRef);
                            if (result.BaggageIncludedInFare != null && result.BaggageIncludedInFare.Length > 0)
                            {
                                result.BaggageIncludedInFare += "," + fareBasisBaggage[flightSegments[s].FareInfoKey];
                            }
                            else
                            {
                                result.BaggageIncludedInFare = fareBasisBaggage[flightSegments[s].FareInfoKey];
                            }
                            // fareRule.FareBasisCode = fareBasisCodeRef[s + 1]; todo ziya
                            result.FareRules.Add(fareRule);
                        }
                    }
                    string key = BuildResultKey(result);
                    if (!resultKey.Contains(key))
                    {
                        //Commented out by Shiva for allowing Near by search 28 Nov 2018
                        //if (IsValidResult(result, request))
                        {
                            resultarray.Add(result);
                            resultKey.Add(key);
                        }
                    }

                }
                else if (allowResult) // One Way
                {
                    SearchResult result = new SearchResult();
                    result.UapiPricingSolution = pricingSolution;// for UAPI Create Reservatiobn Req.... to get Airfare with response
                                                                 //result.UapiPricingSolution = new object();
                    result.FareBreakdown = fare;
                    result.TotalFare = totalResultFare;
                    result.BaseFare = totalbaseFare;
                    result.Price = new PriceAccounts();
                    result.Price.SupplierPrice = (decimal)totalSupplierFare;
                    result.Price.SupplierCurrency = currency;
                    result.Price.TaxBreakups = taxBreakups;
                    result.Tax = totalResultFare - totalbaseFare;
                    result.LastTicketDate = lastTktDate;
                    result.Currency = agentBaseCurrency;
                    result.NonRefundable = nonRefundable;
                    //result.ResultBookingSource = BookingSource.Galileo;
                    result.ResultBookingSource = BookingSource.UAPI;
                    result.FareRules = new List<FareRule>();
                    result.Flights = new FlightInfo[1][];
                    result.Flights[0] = new FlightInfo[outFlights[0].Length];
                    result.Price.K3Tax = taxBreakups.Where(x => x.TaxCode == "K3").Sum(y => y.TaxValue);
                    result.UapiPricingSolution.AirSegment = new typeBaseAirSegment[outFlights[0].Length];
                    for (int j = 0; j < outFlights[0].Length; j++)
                    {
                        result.Flights[0][j] = new FlightInfo();
                        result.Flights[0][j] = FlightInfo.Copy(outFlights[0][j]);
                        result.Flights[0][j].Stops = outFlights[0].Length - 1;
                        //Adding outbound segments directly to UAPIPricingSolution for Repricing
                        result.UapiPricingSolution.AirSegment[j] = outFlights[0][j].UAPIReservationValues["AirSegment"] as typeBaseAirSegment;
                    }

                    FlightInfo[] flightSegments = SearchResult.GetSegments(result);
                    for (int s = 0; s < flightSegments.Length; s++)
                    {
                        if (fareBasisCodeRef.ContainsKey(flightSegments[s].FareInfoKey))
                        {
                            flightSegments[s].TourCode = fareBasisCodeRef[flightSegments[s].FareInfoKey].TourCode;
                            FareRule fareRule = new FareRule();
                            fareRule.Airline = flightSegments[s].Airline;
                            fareRule.Destination = flightSegments[s].Destination.AirportCode;
                            fareRule.Origin = flightSegments[s].Origin.AirportCode;

                            FareInfo tempFareInfo = fareBasisCodeRef[flightSegments[s].FareInfoKey];
                            fareRule.FareBasisCode = tempFareInfo.FareBasis;

                            FareRuleKey tempFareRuleKey = tempFareInfo.FareRuleKey;
                            fareRule.FareRuleKeyValue = tempFareRuleKey.Value;
                            fareRule.FareInfoRef = (tempFareRuleKey.FareInfoRef);

                            //result.BaggageIncludedInFare =  fareBasisBaggage[flightSegments[s].FareInfoKey];
                            result.BaggageIncludedInFare = !string.IsNullOrEmpty(result.BaggageIncludedInFare) ?
                                result.BaggageIncludedInFare + "," + fareBasisBaggage[flightSegments[s].FareInfoKey] : fareBasisBaggage[flightSegments[s].FareInfoKey];
                            // fareRule.FareBasisCode = fareBasisCodeRef[s + 1]; todo ziya
                            result.FareRules.Add(fareRule);
                        }
                    }
                    string key = BuildResultKey(result);
                    if (!resultKey.Contains(key))
                    {
                        //Commented out by Shiva for allowing Near by search 28 Nov 2018
                        //if (IsValidResult(result, request))
                        {
                            resultarray.Add(result);
                            resultKey.Add(key);
                        }
                    }
                }

                flightLists.Clear();

            } // Air Pricing Solution End



            return resultarray.ToArray();
        }

        private string BuildResultKey(SearchResult result)
        {
            int obLength = result.Flights[0].Length;
            StringBuilder key = new StringBuilder();
            key.Append(result.Flights[0][0].DepartureTime.DayOfYear.ToString());
            key.Append(result.Flights[0][0].DepartureTime.ToString("HHmm"));
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.DayOfYear.ToString());
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.ToString("HHmm"));
            if (result.Flights.Length > 1)
            {
                int ibLength = result.Flights[1].Length;
                key.Append(result.Flights[1][0].DepartureTime.DayOfYear.ToString());
                key.Append(result.Flights[1][0].DepartureTime.ToString("HHmm"));
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.DayOfYear.ToString());
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.ToString("HHmm"));
            }
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    key.Append(result.Flights[i][j].FlightNumber);
                    key.Append(result.Flights[i][j].BookingClass);
                }
            }
            return key.ToString();
        }
        private bool IsValidResult(SearchResult result, SearchRequest request)
        {
            bool isValid = false;
            for (int i = 0; i < result.Flights.Length; i++)
            {
                if (i == 0)
                {
                    if (((result.Flights[i][0].Origin.CityCode == request.Segments[0].Origin) || (result.Flights[i][0].Origin.AirportCode == request.Segments[0].Origin)) && ((result.Flights[i][result.Flights[i].Length - 1].Destination.CityCode == request.Segments[0].Destination) || (result.Flights[i][result.Flights[i].Length - 1].Destination.AirportCode == request.Segments[0].Destination)))
                    {
                        isValid = true;
                    }
                }
                else
                {
                    if (((result.Flights[i][0].Origin.CityCode == request.Segments[0].Destination) || (result.Flights[i][0].Origin.AirportCode == request.Segments[0].Destination)) && ((result.Flights[i][result.Flights[i].Length - 1].Destination.CityCode == request.Segments[0].Origin) || (result.Flights[i][result.Flights[i].Length - 1].Destination.AirportCode == request.Segments[0].Origin)))
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }



        /// <summary>
        /// This method returns the Seat Availability response based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public SeatAvailabilityResp GetSeatAvailableSSR(SearchResult result, string sOrigin, string sDestination,
            SeatAvailabilityResp clsSeatAvailabilityResp, string session, string sPaxNames)
        {
            SegmentSeats clsSegmentSeats = new SegmentSeats();

            try
            {
                SeatMapReq clsSeatMapReq = new SeatMapReq();
                clsSeatMapReq.AuthorizedBy = "user";
                clsSeatMapReq.ReturnSeatPricing = true;
                clsSeatMapReq.TargetBranch = TargetBranch;
                clsSeatMapReq.TraceId = "trace";
                clsSeatMapReq.BillingPointOfSaleInfo = new BillingPointOfSaleInfo();
                clsSeatMapReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";

                clsSeatMapReq.AirSegment = new typeBaseAirSegment[1];
                clsSeatMapReq.AirSegment[0] = new typeBaseAirSegment();

                var seg = result.UapiPricingSolution.AirSegment.Where(x => x.Origin == sOrigin && x.Destination == sDestination).FirstOrDefault();

                clsSeatMapReq.AirSegment[0] = seg;
                clsSeatMapReq.AirSegment[0].Status = "HK";

                clsSeatMapReq.SearchTraveler = new SearchTraveler[sPaxNames.Split(',').Length];

                var Passengers = sPaxNames.Split(',');
                for (int i = 0; i < Passengers.Length; i++)
                {
                    clsSeatMapReq.SearchTraveler[i] = new SearchTraveler();
                    clsSeatMapReq.SearchTraveler[i].Key = i.ToString();
                    clsSeatMapReq.SearchTraveler[i].Code = Passengers[i].Contains("-A") ? "ADT" : "CNN";
                    clsSeatMapReq.SearchTraveler[i].Name = new Name();
                    clsSeatMapReq.SearchTraveler[i].Name.Prefix = Convert.ToString(Passengers[i].Split('-')[0]).Trim();
                    clsSeatMapReq.SearchTraveler[i].Name.First = Convert.ToString(Passengers[i].Split('-')[1]).Trim();
                    clsSeatMapReq.SearchTraveler[i].Name.Last = Convert.ToString(Passengers[i].Split('-')[2]).Trim();
                }

                SeatMapBinding clsSeatMapBinding = new SeatMapBinding();
                clsSeatMapBinding.Url = urlAir;
                clsSeatMapBinding.Credentials = new NetworkCredential(UserName, Password);
                clsSeatMapBinding.PreAuthenticate = true;

                GenericStatic.WriteLogFile(typeof(SeatMapReq), clsSeatMapReq,
                    xmlPath + SessionId + "_" + AppUserId + "_" + sOrigin + "_" + sDestination + "_" + "UAPI_SeatMapReq_");

                SeatMapRsp clsSeatMapRsp = clsSeatMapBinding.service(clsSeatMapReq);

                string filePath = GenericStatic.WriteLogFile(typeof(SeatMapRsp), clsSeatMapRsp,
                    xmlPath + SessionId + "_" + AppUserId + "_" + sOrigin + "_" + sDestination + "_" + "UAPI_SeatMapRsp_");
                //string filePath = "C:\\Developments\\Dotnet\\UAPI\\CozmoAppXml\\UAPI\\09Apr2019\\a27ee51c-bb8a-4d44-83e0-84a6fe1dee59_1_DXB_DEL_UAPI_SeatMapRsp_20190409_041025.xml";

                clsSegmentSeats = GenericStatic.TransformXMLAndDeserialize(clsSegmentSeats, File.ReadAllText(filePath), sSeatAvailResponseXSLT);
                clsSegmentSeats.SeatInfoDetails = clsSegmentSeats.SeatInfoDetails.GroupBy(x => x.SeatNo).Select(y => y.First()).ToList();

                var itemIndex = clsSeatAvailabilityResp.SegmentSeat.FindIndex(x => x.Origin == sOrigin && x.Destination == sDestination);
                clsSeatAvailabilityResp.SegmentSeat.RemoveAt(itemIndex);
                clsSeatAvailabilityResp.SegmentSeat.Insert(itemIndex, clsSegmentSeats);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "(UAPI)Failed to get seat availability response. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return clsSeatAvailabilityResp;
        }
        #endregion

        #region Fare Rules

        public List<FareRule> GetFareRuleList(List<FareRule> fareRuleList)
        {
            Connection();

            AirFareRulesReq fareRulereq = new AirFareRulesReq();
            fareRulereq.TargetBranch = TargetBranch;
            fareRulereq.FareRuleType = typeFareRuleType.@long;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            fareRulereq.BillingPointOfSaleInfo = pos;

            fareRulereq.Items = new FareRuleKey[fareRuleList.Count];
            for (int i = 0; i < fareRuleList.Count; i++)
            {



                FareRuleKey fRuleKey = new FareRuleKey();

                string tempFareBasisCode = fareRuleList[i].FareBasisCode;
                string tempFareInfoRef = string.Empty;
                string tempFareRuleKeyValue = string.Empty;
                if (tempFareBasisCode.IndexOf('-') != -1) // From UAPI - FareInfo Key & FareInfo Key Value
                {
                    string[] fareInfo = tempFareBasisCode.Split('-');

                    if (fareInfo.Length > 1)
                    {
                        //fRule.FareBasisCode = fareInfo[0];
                        tempFareInfoRef = fareInfo[1];
                        tempFareRuleKeyValue = fareInfo[2];
                    }
                }
                else
                {
                    tempFareInfoRef = Convert.ToString(fareRuleList[i].FareInfoRef);
                    tempFareRuleKeyValue = fareRuleList[i].FareRuleKeyValue;

                    //fRule.FareBasisCode = tempFareBasisCode;
                }
                //Guid result;
                //using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
                //{
                //    byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(tempFareInfoRef + "T"));
                //    result = new Guid(hash);                    
                //}
                fRuleKey.FareInfoRef = tempFareInfoRef;//result.ToString(); //tempFareInfoRef + "T";
                fRuleKey.ProviderCode = "1G"; // TODO-- add Dynamic Provider
                fRuleKey.Value = tempFareRuleKeyValue;

                fareRulereq.Items[i] = new FareRuleKey();
                fareRulereq.Items[i] = fRuleKey;





                //fRuleKey.FareInfoRef = Convert.ToString(fareRuleList[i].FareInfoRef)+"T";
                //fRuleKey.ProviderCode = "1G"; // TODO-- add Dynamic Provider
                //fRuleKey.Value = fareRuleList[i].FareRuleKeyValue;

                //fareRulereq.FareRuleKey[i] = new FareRuleKey();
                //fareRulereq.FareRuleKey[i] = fRuleKey;

            }

            // To remove
            try
            {
                System.Xml.Serialization.XmlSerializer serReq = new System.Xml.Serialization.XmlSerializer(fareRulereq.GetType());
                System.Text.StringBuilder sbReq = new System.Text.StringBuilder();
                System.IO.StringWriter writerReq = new System.IO.StringWriter(sbReq);
                serReq.Serialize(writerReq, fareRulereq); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docReq = new XmlDocument();
                docReq.LoadXml(sbReq.ToString());
                //docReq.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirFareRulesReq .xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightFareRuleRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docReq.Save(filePath);
            }
            catch { }

            AirFareRulesBinding binding = new AirFareRulesBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(UserName, Password);
            binding.PreAuthenticate = true;
            AirFareRulesRsp fareRuleResponse = binding.service(fareRulereq);

            // startTo Remove
            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(fareRuleResponse.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, fareRuleResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());

                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirFareRulesRsp.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightFareRuleResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
                // end To Remove
            }
            catch { }
            // Fare Rule Response
            UAPIdll.Air46.FareRule[] fareRulesResultList = fareRuleResponse.FareRule;
            foreach (UAPIdll.Air46.FareRule fareRulesResult in fareRulesResultList)
            {
                //string category = string.Empty;
                string fareruleValue = string.Empty;
                //int farInfoRef = keyGen(fareRulesResult.FareInfoRef);
                FareRuleLong[] fareRuleslongList = fareRulesResult.FareRuleLong;
                if (fareRuleslongList != null && fareRuleslongList.Length > 0)
                {
                    //foreach (FareRuleLong fareRuleLong in fareRuleslongList)
                    //{
                    //    fareruleValue += "<br>" + fareRuleLong.Value;
                    //}
                    /****************************************************************************************
                     *    Loading Fare Rules based on Category. Default Penalties will be loaded first.
                     * **************************************************************************************/
                    try
                    {
                        List<FareRuleLong> fareRules = new List<FareRuleLong>();
                        fareRules.AddRange(fareRuleslongList);
                        FareRuleLong rule = fareRules.Find(delegate (FareRuleLong f) { return f.Category == "16"; });//Penalties

                        fareruleValue = "<p style='display:block;float:left;font-style:normal;	background:#18407B;	color:#fff;	padding:1px 2px 0 2px;margin-top:-1px;margin-left:0;margin-right:5px;border-radius:3px;'><b>" + rule.Category + "</b>&nbsp;<b>" + rule.Value.Substring(0, rule.Value.IndexOf("\n")) + "</b></p><br/>";
                        fareruleValue += "<p style='border:1px solid #18407B;margin-right:10px;'>" + rule.Value.Substring(rule.Value.IndexOf("\n") + 1) + "</p>";
                        fareRules = null;
                    }
                    catch { }

                    foreach (FareRuleLong fareRuleLong in fareRuleslongList)
                    {
                        if (fareRuleLong.Category != "16")//Avoid repeating
                        {
                            if (fareruleValue.Length > 0)
                            {
                                fareruleValue += "<p style='display:block;float:left;font-style:normal;	background:#18407B;	color:#fff;	padding:1px 2px 0 2px;margin-top:-1px;margin-left:0;margin-right:5px;border-radius:3px;'><b>" + fareRuleLong.Category + "</b>&nbsp;<b>" + fareRuleLong.Value.Substring(0, fareRuleLong.Value.IndexOf("\n")) + "</b></p><br/>";
                                fareruleValue += "<p style='border:1px solid #18407B;margin-right:10px;'>" + fareRuleLong.Value.Substring(fareRuleLong.Value.IndexOf("\n") + 1) + "</p>";
                            }
                            else
                            {
                                fareruleValue = "<p style='display:block;float:left;font-style:normal;	background:#18407B;	color:#fff;	padding:1px 2px 0 2px;margin-top:-1px;margin-left:0;width:100%;	text-align:center;	margin-right:5px;border-radius:3px;'><em>" + fareRuleLong.Category + "</em><b>" + fareRuleLong.Value.Substring(0, fareRuleLong.Value.IndexOf("\n")) + "</b></p>";
                                fareruleValue += "<p>" + fareRuleLong.Value.Substring(fareRuleLong.Value.IndexOf("\n") + 1) + "</p>";
                            }
                        }
                    }


                    for (int i = 0; i < fareRuleList.Count; i++)
                    {
                        //Guid result;
                        //System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
                        //byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(fareRuleList[i].FareInfoRef + "T"));
                        //result = new Guid(hash);

                        if (fareRuleList[i].FareInfoRef == fareRulesResult.FareInfoRef)
                        {
                            fareRuleList[i].FareRuleDetail = fareruleValue;
                        }
                    }
                }
            }

            return fareRuleList;
        }
        # endregion

        # region Book

        /// <summary>
        /// Method to book a selected Itinerary
        /// </summary>
        /// <param name="itinerary">An object of FlightItinerary</param>
        /// <returns>BookingResponse</returns>
        public BookingResponse Book(CT.BookingEngine.FlightItinerary itinerary, string sessionId, out List<SegmentPTCDetail> ptcDetail, UAPICredentials upaiCredentials)
        {


            BookingResponse bookingResponse = new BookingResponse();
            bool isDomestic = itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
            UAPIdll.Universal46.AirCreateReservationRsp reservationRsp = new UAPIdll.Universal46.AirCreateReservationRsp();
            reservationRsp = CreateReservation(itinerary, upaiCredentials);
            ptcDetail = new List<SegmentPTCDetail>();

            // To Remove
            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Universal46.AirCreateReservationRsp));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, reservationRsp); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirReservationRes .xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightReservationResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmssFFF") + ".xml";
                doc.Save(filePath);
                //
            }
            catch { }
            //Audit.Add(EventType.Book, Severity.Normal, Convert.ToInt32(AppUserId), "UAPI Create Reservation Response. : ", string.Empty);
            bool booked = true;



            //Checking Error
            UAPIdll.Universal46.AirSegmentError[] sellFailureErr = reservationRsp.AirSegmentSellFailureInfo;
            if (sellFailureErr != null)
            {
                booked = false;

                Basket.FlightBookingSession[sessionId].Log.Add("UAPI Sell Failure Error.");
                bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "SEAT NOT AVAILABLE -" + sellFailureErr[0].ErrorMessage, "");
                Audit.Add(EventType.Book, Severity.High, Convert.ToInt32(AppUserId), "Booking Failed UAPI. Sell Failure Error :" + sellFailureErr[0].ErrorMessage, string.Empty);
            }
            else if (reservationRsp.AirSolutionChangedInfo != null && reservationRsp.AirSolutionChangedInfo.Length > 0) // Price/Schedule Changed - if yes, pnr will not generate
            {
                booked = false;

                Basket.FlightBookingSession[sessionId].Log.Add("UAPI Air Soulution Changed Info.ReasonCode:" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString());
                if (reservationRsp.ResponseMessage != null)
                {
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, reservationRsp.ResponseMessage[0].Value, string.Empty);
                    Audit.Add(EventType.Book, Severity.High, Convert.ToInt32(AppUserId), "Booking Failed UAPI:(" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString() + "):" + reservationRsp.ResponseMessage[0].Value, string.Empty);
                }
                else
                {
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "Price/Schedule Changed", string.Empty);
                    Audit.Add(EventType.Book, Severity.High, Convert.ToInt32(AppUserId), "Booking Failed UAPI:Price/Schedule Changed:" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString(), string.Empty);
                }


            }
            else
            {

                UAPIdll.Universal46.UniversalRecord unRecord = reservationRsp.UniversalRecord;
                UAPIdll.Universal46.AirReservation[] airReservationList = unRecord.Items;
                if (airReservationList == null)
                    booked = false;
                foreach (UAPIdll.Universal46.AirReservation airReservation in airReservationList)
                {
                    if (airReservation.AirSegment != null)
                    {
                        booked = ReadBookingStatus(airReservation);
                    }
                    else//If AirSegment details not returned
                    {
                        Basket.FlightBookingSession[sessionId].Log.Add("UAPI Booking Failed.");
                        bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "Segment details not returned for this PNR!", "");
                        Audit.Add(EventType.Book, Severity.High, Convert.ToInt32(AppUserId), "(UAPI)Booking Failed UAPI. Booked Status : false", string.Empty);
                        booked = false;
                        break;

                    }
                }
                if (booked)
                {
                    bookingResponse = ReadBookResponse(reservationRsp, itinerary, out ptcDetail);
                    Audit.Add(EventType.Book, Severity.High, Convert.ToInt32(AppUserId), "(UAPI) PNR: " + itinerary.PNR + ". UAPI booking Response from ReadBookResponse", string.Empty);
                }
                else
                {
                    if (string.IsNullOrEmpty(bookingResponse.Error))
                    {
                        Basket.FlightBookingSession[sessionId].Log.Add("UAPI Booking Failed.");
                        bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "SEAT NOT AVAILABLE", "");
                        Audit.Add(EventType.Book, Severity.High, Convert.ToInt32(AppUserId), "Booking Failed UAPI. Booked Status : false", string.Empty);
                    }
                }

            }

            if (booked == false)
            {
                if (reservationRsp.UniversalRecord != null)
                {
                    if (!string.IsNullOrEmpty(reservationRsp.UniversalRecord.LocatorCode))
                    {
                        CancelItinerary(reservationRsp.UniversalRecord.LocatorCode, "UR");
                    }
                }
            }

            return bookingResponse;

        }

        private UAPIdll.Universal46.AirCreateReservationRsp CreateReservation(FlightItinerary itinerary, UAPICredentials _uapiCredentials)
        {
            UAPIdll.Universal46.AirCreateReservationRsp reservationRsp = new UAPIdll.Universal46.AirCreateReservationRsp();
            try
            {
                Connection();
                List<KeyValuePair<string, SSR>> ssrList = GenerateSSRPaxList(itinerary);

                // for mutli city booking 
                UAPIdll.Universal46.AirCreateReservationReq createRequest = new UAPIdll.Universal46.AirCreateReservationReq();
                UAPIdll.Universal46.ContinuityCheckOverride continuityCheck = new UAPIdll.Universal46.ContinuityCheckOverride();
                continuityCheck.Key = "Yes";
                continuityCheck.Value = "Yes";
                createRequest.ContinuityCheckOverride = continuityCheck;


                // createRequest.TargetBranch = TargetBranch;
                createRequest.TargetBranch = _uapiCredentials.TargetBranch;// MOD by ziyad for target branch clash
                createRequest.RetainReservation = UAPIdll.Universal46.typeRetainReservation.None;// If schedule/Price Change.... PNR should not be created                

                UAPIdll.Universal46.BillingPointOfSaleInfo pos = new UAPIdll.Universal46.BillingPointOfSaleInfo();
                pos.OriginApplication = originalApplication;
                createRequest.BillingPointOfSaleInfo = pos;

                createRequest.BookingTraveler = new UAPIdll.Universal46.BookingTraveler[itinerary.Passenger.Length];
                int paxIndex = 0; List<UAPIdll.Universal46.AccountingRemark> liDIinfo = new List<UAPIdll.Universal46.AccountingRemark>();

                foreach (FlightPassenger passenger in itinerary.Passenger)
                {
                    string passengerType = string.Empty;
                    // Asigning Pax Type
                    if (passenger.Type == PassengerType.Adult)
                        passengerType = "ADT";
                    else if (passenger.Type == PassengerType.Child)
                        passengerType = "CNN";
                    else if (passenger.Type == PassengerType.Infant)
                        passengerType = "INF";
                    else if (passenger.Type == PassengerType.Senior)
                        passengerType = "SRC";
                    UAPIdll.Universal46.BookingTraveler tmpBkTraveler = new UAPIdll.Universal46.BookingTraveler();
                    tmpBkTraveler.Key = paxIndex.ToString();
                    tmpBkTraveler.TravelerType = passengerType;

                    string tmpGender = "M";
                    if (passenger.Gender == Gender.Male)
                        tmpGender = "M";
                    else if (passenger.Gender == Gender.Female)
                        tmpGender = "F";
                    tmpBkTraveler.Gender = tmpGender;
                    tmpBkTraveler.DOB = passenger.DateOfBirth;
                    tmpBkTraveler.DOBSpecified = true;
                    // adding SSR
                    tmpBkTraveler.SSR = new UAPIdll.Universal46.SSR[ssrList.Count];
                    //We need to pass SSR DOCS for all Pax Types for some airlines, so uncommented by Shiva 05-May-2017
                    //if (passenger.Type != PassengerType.Child && passenger.Type != PassengerType.Infant)
                    {
                        int ssrPaxIndex = 0;
                        for (int x = 0; x < ssrList.Count; x++)
                        {
                            KeyValuePair<string, SSR> ssrPax = ssrList[x];

                            //if (!string.IsNullOrEmpty(ssrList[paxIndex].Key))
                            if (ssrPax.Key == passenger.PaxKey)
                            {
                                //KeyValuePair<string, SSR> ssrPax = ssrPaxList[paxIndex];
                                SSR tmpSSR = ssrPax.Value;
                                UAPIdll.Universal46.SSR airSSR = new UAPIdll.Universal46.SSR();
                                airSSR.Type = tmpSSR.SsrCode;
                                airSSR.FreeText = tmpSSR.Detail;
                                if (tmpSSR.SsrCode.Contains("GST"))
                                {
                                    airSSR.Status = "NN";
                                    airSSR.Carrier = itinerary.Segments[0].Airline;
                                    airSSR.Key = (x + 1).ToString();
                                }
                                else //if(airSSR.Type=="DOCS")
                                {
                                    airSSR.Status = "HK";
                                    airSSR.Carrier = "YY";
                                }
                                //else if(airSSR.Type.Contains("CTC"))
                                //    airSSR.Carrier = "YY";
                                tmpBkTraveler.SSR[ssrPaxIndex] = airSSR;
                                ssrPaxIndex++;
                            }

                            //List<SSR> ssrPaxList = new List<SSR>();
                        }
                    }
                    //ssrPaxList.Add(

                    //Air20.SSR[1] airSSRList = new Air20.SSR();




                    UAPIdll.Universal46.BookingTravelerName tmpTravelrName = new UAPIdll.Universal46.BookingTravelerName();
                    tmpTravelrName.Prefix = passenger.Title;
                    tmpTravelrName.First = passenger.FirstName;
                    tmpTravelrName.Last = passenger.LastName;
                    tmpBkTraveler.BookingTravelerName = tmpTravelrName;

                    UAPIdll.Universal46.PhoneNumber tmpPhone = new UAPIdll.Universal46.PhoneNumber();
                    tmpPhone.Type = UAPIdll.Universal46.PhoneNumberType.Mobile;
                    tmpPhone.TypeSpecified = true;
                    //tmpPhone.Number = passenger.CellPhone;
                    tmpPhone.Number = itinerary.Passenger[0].CellPhone;
                    tmpBkTraveler.PhoneNumber = new UAPIdll.Universal46.PhoneNumber[1];
                    tmpBkTraveler.PhoneNumber[0] = new UAPIdll.Universal46.PhoneNumber();
                    tmpBkTraveler.PhoneNumber[0] = tmpPhone;


                    //New Node added on 17 Nov 2015 as informed by Ziyad
                    UAPIdll.Universal46.AgencyContactInfo agencyContact = new UAPIdll.Universal46.AgencyContactInfo();
                    agencyContact.PhoneNumber = new UAPIdll.Universal46.PhoneNumber[1];
                    agencyContact.PhoneNumber[0] = new UAPIdll.Universal46.PhoneNumber();
                    agencyContact.PhoneNumber[0].Number = (itinerary.Segments[0].UAPIReservationValues.ContainsKey(PARENT_AGENT_PHONE) ? itinerary.Segments[0].UAPIReservationValues[PARENT_AGENT_PHONE].ToString() : "");
                    agencyContact.PhoneNumber[0].Type = UAPIdll.Universal46.PhoneNumberType.Agency;
                    agencyContact.PhoneNumber[0].TypeSpecified = true;
                    agencyContact.PhoneNumber[0].Location = (itinerary.Segments[0].UAPIReservationValues.ContainsKey(PARENT_AGENT_LOCATION) ? itinerary.Segments[0].UAPIReservationValues[PARENT_AGENT_LOCATION].ToString() : "");
                    //agencyContact.PhoneNumber[0].Text = "COZMO TRAVEL/pax " + itinerary.Passenger[0].CellPhone;
                    agencyContact.PhoneNumber[0].Text = "COZMO TRAVEL/pax " + itinerary.Passenger[0].CellPhone.Replace(@"+", "");
                    agencyContact.PhoneNumber[0].Key = "125155";

                    createRequest.AgencyContactInfo = agencyContact;

                    if (!string.IsNullOrEmpty(itinerary.Passenger[0].Email))
                    {
                        UAPIdll.Universal46.Email tmpEmail = new UAPIdll.Universal46.Email();
                        tmpEmail.Type = "Home";
                        tmpEmail.EmailID = itinerary.Passenger[0].Email;
                        tmpBkTraveler.Email = new UAPIdll.Universal46.Email[1];
                        tmpBkTraveler.Email[0] = new UAPIdll.Universal46.Email();
                        tmpBkTraveler.Email[0] = tmpEmail;
                    }
                    //if (paxIndex == 0)
                    //{
                    // adding pax Addres 
                    if (passenger.IsLeadPax) //Assigning Address Only for Lead Passenger
                    {
                        //Address is allowed upto 50 characters so if the length is greater then restrict it
                        string address = passenger.AddressLine1.Length > 50 ? passenger.AddressLine1.Substring(0, 50) : passenger.AddressLine1;
                        UAPIdll.Universal46.typeStructuredAddress tmpAdds = new UAPIdll.Universal46.typeStructuredAddress();
                        tmpAdds.AddressName = address; //passenger.AddressLine1;
                        //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                        //{
                        //    tmpAdds.Street = new string[1];
                        //    tmpAdds.Street[0] = passenger.AddressLine2;
                        //}
                        //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                        //{
                        //    tmpAdds.City = passenger.City;
                        //}
                        //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                        //{
                        //    State tmpState = new State();
                        //    tmpState.Value = passenger.City;
                        //    tmpAdds.State = tmpState;
                        //}  For timebeing assiginh addresline1

                        if (!string.IsNullOrEmpty(passenger.AddressLine1))
                        {
                            tmpAdds.Street = new string[1];
                            tmpAdds.Street[0] = address;//passenger.AddressLine1;
                        }
                        if (!string.IsNullOrEmpty(passenger.AddressLine1) || !string.IsNullOrEmpty(passenger.City))
                        {
                            if (!string.IsNullOrEmpty(passenger.City)) tmpAdds.City = passenger.City;
                            else tmpAdds.City = address;//passenger.AddressLine1;
                        }
                        if (!string.IsNullOrEmpty(passenger.AddressLine1) || !string.IsNullOrEmpty(passenger.City))
                        {
                            UAPIdll.Universal46.State tmpState = new UAPIdll.Universal46.State();
                            if (!string.IsNullOrEmpty(passenger.City)) tmpState.Value = passenger.City;
                            else tmpState.Value = address;//passenger.AddressLine1;
                            tmpAdds.State = tmpState;
                        }

                        //tmpAdds.PostalCode = "0097160";// To DO ziyad-- validating UAPI based on Country
                        if (passenger.Country != null) //Regarding APIS Mandate Fields
                        {
                            tmpAdds.Country = passenger.Country.CountryCode;
                        }

                        tmpBkTraveler.Address = new UAPIdll.Universal46.typeStructuredAddress[1];
                        tmpBkTraveler.Address[0] = new UAPIdll.Universal46.typeStructuredAddress();
                        tmpBkTraveler.Address[0] = tmpAdds;
                    }


                    // adding pax Loyalty Card details
                    if (!string.IsNullOrEmpty(passenger.FFAirline) && !string.IsNullOrEmpty(passenger.FFNumber))
                    {
                        string[] tmpFFAirlines = passenger.FFAirline.Split(',');
                        string[] tmpFFNumbers = passenger.FFNumber.Split(',');
                        UAPIdll.Universal46.LoyaltyCard[] lyCardList = new UAPIdll.Universal46.LoyaltyCard[tmpFFAirlines.Length];
                        for (int x = 0; x < tmpFFAirlines.Length; x++)
                        {
                            if (!string.IsNullOrEmpty(tmpFFNumbers[x]))
                            {
                                UAPIdll.Universal46.LoyaltyCard lyCard = new UAPIdll.Universal46.LoyaltyCard();
                                lyCard.SupplierCode = tmpFFAirlines[x];
                                lyCard.CardNumber = tmpFFNumbers[x];
                                lyCardList[x] = lyCard;
                            }

                        }
                        tmpBkTraveler.LoyaltyCard = lyCardList;

                    }

                    //}
                    createRequest.BookingTraveler[paxIndex] = new UAPIdll.Universal46.BookingTraveler();
                    createRequest.BookingTraveler[paxIndex] = tmpBkTraveler;

                    /* To pass DI info to supplier based on flex information added by user */
                    if (passenger.FlexDetailsList != null && passenger.FlexDetailsList.Count > 0)
                    {
                        passenger.FlexDetailsList.Where(y => !string.IsNullOrEmpty(y.FlexGDSprefix) || !string.IsNullOrEmpty(y.FlexData)).ToList().ForEach(x =>
                        {

                            string sFlexData = string.IsNullOrEmpty(x.FlexData) ? string.Empty : GenericStatic.ReplaceNonASCII(x.FlexData.Replace("@", "//").Replace("_", "--").Replace("–", "-"));
                            liDIinfo.Add(new UAPIdll.Universal46.AccountingRemark
                            {
                                RemarkData = (x.FlexGDSprefix + sFlexData).Length < 84 ? x.FlexGDSprefix + sFlexData :
                                    (x.FlexGDSprefix + sFlexData).Substring(0, 84),
                                Category = "FT",
                                TypeInGds = "Other",
                                ProviderCode = "1G"
                            });
                        });
                    }

                    paxIndex++;
                }

                createRequest.AccountingRemark = liDIinfo.Count() > 0 ? liDIinfo.ToArray() : createRequest.AccountingRemark;

                createRequest.SpecificSeatAssignment = new UAPIdll.Universal46.SpecificSeatAssignment[50];
                int cnt = 0;
                itinerary.Passenger.Where(y => !string.IsNullOrEmpty(y.SeatInfo) && y.liPaxSeatInfo != null && y.liPaxSeatInfo.Count() > 0).ToList().ForEach(x =>
                {
                    x.liPaxSeatInfo.Where(s => !string.IsNullOrEmpty(s.SeatNo) && s.SeatNo != "NoSeat").ToList().ForEach(m =>
                    {
                        var seg = itinerary.UapiPricingSolution.AirSegment.Where(z => z.Origin + "-" + z.Destination == m.Segment).FirstOrDefault();
                        if (seg != null)
                        {
                            createRequest.SpecificSeatAssignment[cnt] = new UAPIdll.Universal46.SpecificSeatAssignment();
                            createRequest.SpecificSeatAssignment[cnt].BookingTravelerRef = m.PaxNo.ToString();
                            createRequest.SpecificSeatAssignment[cnt].SegmentRef = seg.Key;
                            createRequest.SpecificSeatAssignment[cnt].SeatId = m.SeatNo;
                            cnt++;
                        }
                    });
                });

                createRequest.SpecificSeatAssignment = createRequest.SpecificSeatAssignment.Where(x => x != null).ToArray();

                /********************************************************
                //Calculate Pax Type Count for updating in PricingInfo  *
                *********************************************************/
                int adults = 0, childs = 0, infants = 0;

                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    if (pax.Type == PassengerType.Adult)
                    {
                        adults++;
                    }
                    if (pax.Type == PassengerType.Child)
                    {
                        childs++;
                    }
                    if (pax.Type == PassengerType.Infant)
                    {
                        infants++;
                    }
                }
                /***********************END**************************/

                itinerary.UapiPricingSolution.Journey = null;
                itinerary.UapiPricingSolution.LegRef = null;
                int paxRef = 0;
                foreach (UAPIdll.Air46.AirPricingInfo tempPriceinfo in itinerary.UapiPricingSolution.AirPricingInfo)
                {
                    tempPriceinfo.FareInfoRef = null;

                    foreach (UAPIdll.Air46.FareInfo tempfareInfo in tempPriceinfo.FareInfo)
                    {
                        tempfareInfo.FareSurcharge = null;
                        tempfareInfo.FareRuleKey = null;
                        tempfareInfo.BaggageAllowance = null;

                    }
                    //int count = 0;
                    foreach (UAPIdll.Air46.BookingInfo tempBookingInfo in tempPriceinfo.BookingInfo)
                    {
                        //tempBookingInfo.SegmentRef= itinerary.Segments[count].UAPIReservationValues[SEGMENT_REF].ToString();
                        //count++;
                    }

                    /********************************************************************************************************
                    //Update Pax Type wise count for corresponding AirPricingInfo. If count doesn't match add missing nodes *
                    //until matching the pax type count  - added by shiva 12 Sep 2017                                       *
                    /********************************************************************************************************/
                    if (tempPriceinfo.PassengerType[0].Code == "ADT" && tempPriceinfo.PassengerType.Length < adults)
                    {
                        int count = tempPriceinfo.PassengerType.Length;
                        List<UAPIdll.Air46.PassengerType> PaxTypes = new List<UAPIdll.Air46.PassengerType>();
                        PaxTypes.AddRange(tempPriceinfo.PassengerType);
                        for (int i = 0; i < (adults - count); i++)
                        {
                            UAPIdll.Air46.PassengerType tempPaxType = new UAPIdll.Air46.PassengerType();
                            tempPaxType.Code = "ADT";
                            PaxTypes.Add(tempPaxType);
                        }
                        tempPriceinfo.PassengerType = PaxTypes.ToArray();
                    }

                    if (tempPriceinfo.PassengerType[0].Code == "CNN" && tempPriceinfo.PassengerType.Length < childs)
                    {
                        int count = tempPriceinfo.PassengerType.Length;
                        List<UAPIdll.Air46.PassengerType> PaxTypes = new List<UAPIdll.Air46.PassengerType>();
                        PaxTypes.AddRange(tempPriceinfo.PassengerType);
                        for (int i = 0; i < (childs - count); i++)
                        {
                            UAPIdll.Air46.PassengerType tempPaxType = new UAPIdll.Air46.PassengerType();
                            tempPaxType.Code = "CNN";
                            tempPaxType.Age = "5";
                            PaxTypes.Add(tempPaxType);
                        }
                        tempPriceinfo.PassengerType = PaxTypes.ToArray();
                    }
                    if (tempPriceinfo.PassengerType[0].Code == "INF" && tempPriceinfo.PassengerType.Length < infants)
                    {
                        int count = tempPriceinfo.PassengerType.Length;
                        List<UAPIdll.Air46.PassengerType> PaxTypes = new List<UAPIdll.Air46.PassengerType>();
                        PaxTypes.AddRange(tempPriceinfo.PassengerType);
                        for (int i = 0; i < (infants - count); i++)
                        {
                            UAPIdll.Air46.PassengerType tempPaxType = new UAPIdll.Air46.PassengerType();
                            tempPaxType.Code = "INF";
                            PaxTypes.Add(tempPaxType);
                        }
                        tempPriceinfo.PassengerType = PaxTypes.ToArray();
                    }
                    /********************************************END**********************************************************/
                    foreach (UAPIdll.Air46.PassengerType tempPaxType in tempPriceinfo.PassengerType)
                    {
                        tempPaxType.BookingTravelerRef = paxRef.ToString();
                        paxRef++;

                    }
                }

                //Eliminate duplicate segments and pass original segments returned in Search response
                List<FlightInfo> Segments = new List<FlightInfo>(itinerary.Segments);
                List<FlightInfo> BookingSegments = new List<FlightInfo>();
                BookingInfo[] bkgInfo = itinerary.UapiPricingSolution.AirPricingInfo[0].BookingInfo;
                //Match with BookingInfo segmentRef against FlightInfo uapiSegmentKey
                foreach (BookingInfo bkg in bkgInfo)
                {
                    List<FlightInfo> duplicates = Segments.FindAll(delegate (FlightInfo fi) { return fi.UapiSegmentRefKey == bkg.SegmentRef; });
                    FlightInfo finfo = FlightInfo.Copy(duplicates[0]);
                    finfo.Origin = duplicates[0].Origin;
                    finfo.Destination = duplicates[duplicates.Count - 1].Destination;
                    finfo.ArrivalTime = duplicates[duplicates.Count - 1].ArrivalTime;
                    finfo.UapiArrivalTime = duplicates[duplicates.Count - 1].UapiArrivalTime;
                    BookingSegments.Add(finfo);
                }

                itinerary.UapiPricingSolution.AirSegment = new typeBaseAirSegment[itinerary.Segments.Length];
                int segIndex = 0;
                foreach (FlightInfo flight in BookingSegments)
                {
                    string airSegKey = Convert.ToString(flight.UapiSegmentRefKey);// + "T";

                    typeBaseAirSegment tmpAirSegment = new typeBaseAirSegment();
                    tmpAirSegment.Key = airSegKey;
                    tmpAirSegment.Group = flight.Group;
                    tmpAirSegment.Carrier = flight.Airline;
                    tmpAirSegment.FlightNumber = flight.FlightNumber;
                    tmpAirSegment.Origin = flight.Origin.AirportCode;
                    tmpAirSegment.Destination = flight.Destination.AirportCode;
                    //tmpAirSegment.DepartureTime = flight.DepartureTime.ToString("yyyy-MM-dd");  To get Schedule Error
                    //tmpAirSegment.DepartureTime = flight.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss");
                    //tmpAirSegment.ArrivalTime = flight.ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"); 
                    tmpAirSegment.DepartureTime = flight.UapiDepartureTime;
                    tmpAirSegment.ArrivalTime = flight.UapiArrivalTime;

                    //==============Set values based on the search response====================//
                    tmpAirSegment.LinkAvailability = flight.UAPIReservationValues.ContainsKey(LINK_AVAILABILITY) && flight.UAPIReservationValues[LINK_AVAILABILITY] != null ? Convert.ToBoolean(flight.UAPIReservationValues[LINK_AVAILABILITY]) : false;
                    tmpAirSegment.LinkAvailabilitySpecified = true;
                    tmpAirSegment.PolledAvailabilityOption = flight.UAPIReservationValues.ContainsKey(POLLED_AVAILABILITY_OPTION) && flight.UAPIReservationValues[POLLED_AVAILABILITY_OPTION] != null ? flight.UAPIReservationValues[POLLED_AVAILABILITY_OPTION].ToString() : null;
                    tmpAirSegment.AvailabilitySource = flight.UAPIReservationValues.ContainsKey(AVAILABILITY_SOURCE) && flight.UAPIReservationValues[AVAILABILITY_SOURCE] != null ? flight.UAPIReservationValues[AVAILABILITY_SOURCE].ToString() : null;
                    //tmpAirSegment.AvailabilitySourceSpecified = true;
                    if (tmpAirSegment.PolledAvailabilityOption == null)
                        tmpAirSegment.PolledAvailabilityOption = "Polled avail exists";
                    tmpAirSegment.ProviderCode = flight.UAPIReservationValues.ContainsKey(PROVIDER_CODE) && flight.UAPIReservationValues[PROVIDER_CODE] != null ? flight.UAPIReservationValues[PROVIDER_CODE].ToString() : ProviderCode;
                    //=================================End=====================================//

                    tmpAirSegment.ClassOfService = flight.BookingClass;

                    itinerary.UapiPricingSolution.AirSegment[segIndex] = new typeBaseAirSegment();
                    itinerary.UapiPricingSolution.AirSegment[segIndex] = tmpAirSegment;
                    segIndex++;
                    if (flight.StopOver)
                    {
                        tmpAirSegment.Connection = new Connection();

                    }
                }
                itinerary.UapiPricingSolution.OptionalServices = null;
                itinerary.UapiPricingSolution.PricingDetails = null;
                itinerary.UapiPricingSolution.AvailableSSR = null;
                createRequest.AirPricingSolution = new UAPIdll.Universal46.AirPricingSolution[1];
                createRequest.AirPricingSolution[0] = new UAPIdll.Universal46.AirPricingSolution();

                //createRequest.AirPricingSolution = (UAPIdll.Universal46.AirPricingSolution)UAPIdll.Converter.Convert(itinerary.UapiPricingSolution, typeof(UAPIdll.Universal46.AirPricingSolution));
                PropertiesCopier.CopyProperties(itinerary.UapiPricingSolution, createRequest.AirPricingSolution[0]);
                PropertiesCopier.CheckRecursiveDifferentTypes(itinerary.UapiPricingSolution, createRequest.AirPricingSolution[0]);

                //Assign Airline AccountCodes for AirlinePrivateFare booking if AccountCode is returned from Search Response
                UAPIdll.Universal46.AirPricingInfo pricingInfo = createRequest.AirPricingSolution[0].AirPricingInfo[0];//Take always first Adult PricingInfo
                if (pricingInfo != null && pricingInfo.PricingMethod == UAPIdll.Universal46.typePricingMethod.GuaranteedUsingAirlinePrivateFare && pricingInfo.FareInfo != null && pricingInfo.FareInfo.Length > 0 && pricingInfo.FareInfo[0].AccountCode != null && pricingInfo.FareInfo[0].AccountCode.Length > 0)
                {

                    pricingInfo.AirPricingModifiers = new UAPIdll.Universal46.AirPricingModifiers();
                    pricingInfo.AirPricingModifiers.AccountCodes = new UAPIdll.Universal46.AccountCode[1];
                    pricingInfo.AirPricingModifiers.AccountCodes[0] = new UAPIdll.Universal46.AccountCode();
                    pricingInfo.AirPricingModifiers.AccountCodes[0].Code = pricingInfo.FareInfo[0].AccountCode[0].Code;
                    pricingInfo.AirPricingModifiers.AccountCodes[0].ProviderCode = pricingInfo.ProviderCode;
                    if (pricingInfo.FareInfo[0].AccountCode[0].SupplierCode != null && pricingInfo.FareInfo[0].AccountCode[0].SupplierCode.Length > 0)
                    {
                        pricingInfo.AirPricingModifiers.AccountCodes[0].SupplierCode = pricingInfo.FareInfo[0].AccountCode[0].SupplierCode;
                    }
                    if (pricingInfo.FareInfo[0].AccountCode[0].Type != null && pricingInfo.FareInfo[0].AccountCode[0].Type.Length > 0)
                    {
                        pricingInfo.AirPricingModifiers.AccountCodes[0].Type = pricingInfo.FareInfo[0].AccountCode[0].Type;
                    }


                    //string[] codes = ConfigurationManager.AppSettings["UAPIAccountCodes"].Split(',');
                    //pricingInfo.AirPricingModifiers.AccountCodeFaresOnly = false;
                    //pricingInfo.AirPricingModifiers.AccountCodeFaresOnlySpecified = false;
                    //pricingInfo.AirPricingModifiers.AccountCodes = new UAPIdll.Universal46.AccountCode[codes.Length];
                    //for (int i = 0; i < codes.Length; i++)
                    //{
                    //    pricingInfo.AirPricingModifiers.AccountCodes[i+1] = new UAPIdll.Universal46.AccountCode();
                    //    pricingInfo.AirPricingModifiers.AccountCodes[i+1].Code = codes[i];
                    //    pricingInfo.AirPricingModifiers.AccountCodes[i+1].ProviderCode = "1G";
                    //    //priceModifier.AccountCodes[i].SupplierCode = "";
                    //    //priceModifier.AccountCodes[i].Type = "";
                    //}
                    //pricingInfo.AirPricingModifiers.FaresIndicator = UAPIdll.Universal46.typeFaresIndicator.PrivateFaresOnly;
                }

                createRequest.AirPricingSolution[0].AirSegmentRef = new UAPIdll.Universal46.AirSegmentRef[0];//Clear AirSegmentRef if RePricing done

                UAPIdll.Universal46.ActionStatus actionStatus = new UAPIdll.Universal46.ActionStatus();
                actionStatus.ProviderCode = itinerary.UapiPricingSolution.AirSegment[0].ProviderCode;// "1G";// To do -- should be assigned dynamically
                actionStatus.Type = UAPIdll.Universal46.ActionStatusType.ACTIVE;

                createRequest.ActionStatus = new UAPIdll.Universal46.ActionStatus[1];
                createRequest.ActionStatus[0] = new UAPIdll.Universal46.ActionStatus();
                createRequest.ActionStatus[0] = actionStatus;

                // For Payment Information
                UAPIdll.Universal46.FormOfPayment paymentForm = new UAPIdll.Universal46.FormOfPayment();
                paymentForm.Key = "123";
                paymentForm.Type = "Cash";
                createRequest.FormOfPayment = new UAPIdll.Universal46.FormOfPayment[1];
                createRequest.FormOfPayment[0] = new UAPIdll.Universal46.FormOfPayment();
                createRequest.FormOfPayment[0] = paymentForm;

                createRequest.AirPricingSolution[0].AvailableSSR = null;
                createRequest.AirPricingSolution[0].OptionalServices = null;
                createRequest.AirPricingSolution[0].PricingDetails = null;
                //No need to pass in the reservation
                createRequest.AirPricingSolution[0].AirPricingInfo.ToList().ForEach(price => price.FareRulesFilter = null);

                //Passing LeadPax Destination Phone Number.
                //Added by lokesh on 5-Oct-2017.
                if (itinerary.Passenger[0].IsLeadPax)
                {
                    //Added Email and phone to OSI 02 Sep 2020 by Shiva as per Shweta.Rath@travelport.com
                    List<UAPIdll.Universal46.OSI> OSIList = new List<UAPIdll.Universal46.OSI>();
                    if (!string.IsNullOrEmpty(itinerary.Passenger[0].DestinationPhone))
                    {
                        //createRequest.OSI = new UAPIdll.Universal46.OSI[1];
                        //createRequest.OSI[0] = new UAPIdll.Universal46.OSI();
                        //createRequest.OSI[0].ProviderCode = "1G";
                        //createRequest.OSI[0].Carrier = "YY";// as adviced by revenue Team - YY (generic) carrier code
                        //createRequest.OSI[0].Text = "PAX DEST CTC " + itinerary.Passenger[0].DestinationPhone;


                        UAPIdll.Universal46.OSI osi = new UAPIdll.Universal46.OSI();
                        osi.ProviderCode = "1G";
                        osi.Carrier = "YY";// as adviced by revenue Team - YY (generic) carrier code
                        osi.Text = "PAX DEST CTC " + itinerary.Passenger[0].DestinationPhone;
                        OSIList.Add(osi);
                    }
                    if (!string.IsNullOrEmpty(itinerary.Passenger[0].CellPhone))
                    {
                        UAPIdll.Universal46.OSI osi = new UAPIdll.Universal46.OSI();
                        osi.ProviderCode = "1G";
                        osi.Carrier = "YY";// as adviced by revenue Team - YY (generic) carrier code
                        osi.Text = "DEL CTCM " + GenericStatic.FilterAlphaNumeric(itinerary.Passenger[0].CellPhone);
                        OSIList.Add(osi);
                    }
                    if (!string.IsNullOrEmpty(itinerary.Passenger[0].Email))
                    {
                        UAPIdll.Universal46.OSI osi = new UAPIdll.Universal46.OSI();
                        osi.ProviderCode = "1G";
                        osi.Carrier = "YY";// as adviced by revenue Team - YY (generic) carrier code
                        osi.Text = "DEL CTCE " + (itinerary.Passenger[0].Email).Replace("@", "//").Replace("_", "..").Replace("-", "./");
                        OSIList.Add(osi);
                    }
                    //SI Entry for Deal Code by Shiva 11 Nov 2020
                    if(pricingInfo.FareInfo.Length > 0 && pricingInfo.FareInfo[0].AccountCode != null && pricingInfo.FareInfo[0].AccountCode.Length > 0)
                    {
                        UAPIdll.Universal46.OSI osi = new UAPIdll.Universal46.OSI();
                        osi.ProviderCode = "1G";
                        osi.Carrier = itinerary.AirlineCode;
                        osi.Text = "DEAL " + pricingInfo.FareInfo[0].AccountCode;
                        OSIList.Add(osi);
                    }

                    createRequest.OSI = OSIList.ToArray();
                }


                // To Remove

                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Universal46.AirCreateReservationReq));
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    ser.Serialize(writer, createRequest);   // Here Classes are converted to XML String. 
                                                            // This can be viewed in SB or writer.
                                                            // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirReservationReq .xml");
                    string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightReservationRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmssFFF") + ".xml";
                    doc.Save(filePath);
                    //
                }
                catch { }





                //Start Temp comment for manual load
                UAPIdll.Universal46.AirCreateReservationBinding binding = new UAPIdll.Universal46.AirCreateReservationBinding();
                binding.Url = urlAir;
                binding.Credentials = new NetworkCredential(_uapiCredentials.UserName, _uapiCredentials.Password);// MOD by ziyad as clashjing UAPI crdentails
                binding.PreAuthenticate = true;//to implement GZIP
                reservationRsp = binding.service(createRequest); //ziyad for local test comment




                ////----------------------Loading previous xml-------------------------------
                //System.Xml.Serialization.XmlSerializer serLoc = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Universal46.AirCreateReservationRsp));
                //string staticpath = ConfigurationManager.AppSettings["TestStaticXmlLoadPath"];
                //System.IO.StreamReader writerLoc = new StreamReader(@staticpath + "FlightReservationResponse_Test.xml");
                //reservationRsp = serLoc.Deserialize(writerLoc) as UAPIdll.Universal46.AirCreateReservationRsp;
                //writerLoc.Close();
                // ----------------------------- end test ----------------
            }
            catch (SoapException se)
            {
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, Convert.ToInt32(AppUserId), "(UAPI)Failed to Create Reservation. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return reservationRsp;
        }


        private bool ReadBookingStatus(UAPIdll.Universal46.AirReservation airReservation)
        {
            bool booked = true;
            List<string> availabilityStatus = new List<string>();
            availabilityStatus.Add("HS");
            availabilityStatus.Add("SS");
            availabilityStatus.Add("HK");
            availabilityStatus.Add("KK");
            availabilityStatus.Add("TK");

            UAPIdll.Universal46.typeBaseAirSegment[] airSegmentList = airReservation.AirSegment;
            if (airSegmentList != null)
            {
                foreach (UAPIdll.Universal46.typeBaseAirSegment airSegment in airSegmentList)
                {
                    string status = airSegment.Status;
                    if (!availabilityStatus.Contains(status))
                    {
                        booked = false;
                    }
                }
            }
            else
                booked = false;


            return booked;
        }

        private BookingResponse ReadBookResponse(UAPIdll.Universal46.AirCreateReservationRsp reservationRsp, FlightItinerary itinerary, out List<SegmentPTCDetail> ptcDetail)
        {
            ptcDetail = new List<SegmentPTCDetail>();
            BookingResponse booking = new BookingResponse();
            UAPIdll.Universal46.UniversalRecord unRecord = reservationRsp.UniversalRecord;
            string ssrMessage = string.Empty;
            // IF SSR is required for Pax 
            if (unRecord != null)
            {
                UAPIdll.Universal46.BookingTraveler[] tmpBktravelerList = unRecord.BookingTraveler;
                if (tmpBktravelerList != null)
                {
                    bool bHasSeats = itinerary.Passenger.Where(x => !string.IsNullOrEmpty(x.SeatInfo)).Count() > 0;
                    if (bHasSeats && tmpBktravelerList.Where(x => x.AirSeatAssignment != null).Count() == 0)
                    {
                        bHasSeats = false;
                        itinerary.Passenger.ToList().ForEach(m =>
                        {
                            m.liPaxSeatInfo.Where(c => !string.IsNullOrEmpty(c.SeatNo)).ToList().ForEach(x =>
                            {
                                string msg = "Failed to assign seat for passenger " + m.FirstName + " " + m.LastName + " for segment (" + x.Segment + ")";
                                booking.Error = string.IsNullOrEmpty(booking.Error) ? msg : booking.Error + "@" + msg;
                                x.SeatNo = string.Empty;
                            });
                        });
                    }

                    foreach (UAPIdll.Universal46.BookingTraveler tmpBktraveler in tmpBktravelerList)
                    {
                        UAPIdll.Universal46.SSR[] tempSSRList = tmpBktraveler.SSR;

                        if (tempSSRList != null)
                        {
                            foreach (UAPIdll.Universal46.SSR tempSSR in tempSSRList)
                            {
                                ssrMessage += " (" + tmpBktraveler.BookingTravelerName.First + ") SSR type: " + tempSSR.Type + ",Status:" + tempSSR.Status + ", Free Text:" + tempSSR.FreeText;
                            }
                        }

                        /* To identify the seat assignment in the response */
                        if (bHasSeats)
                        {
                            if (tmpBktraveler.AirSeatAssignment == null)
                                tmpBktraveler.AirSeatAssignment = new UAPIdll.Universal46.AirSeatAssignment[1];

                            tmpBktraveler.AirSeatAssignment.ToList().ForEach(x =>
                            {
                                var seg = unRecord.Items[0].AirSegment.Where(s => s.FlightDetails[0].Key == x.FlightDetailsRef).FirstOrDefault();
                                x.SegmentRef = seg.Origin + "-" + seg.Destination;
                            });

                            bool islastloop = tmpBktraveler.Equals(tmpBktravelerList.Last());

                            itinerary.Passenger.ToList().ForEach(m =>
                            {
                                m.liPaxSeatInfo.Where(c => !string.IsNullOrEmpty(c.SeatNo) && c.SeatStatus != "S").ToList().ForEach(x =>
                                {
                                    string seatno = x.SeatNo.Replace("-", "").Length == 3 ? "0" + x.SeatNo.Replace("-", "") : x.SeatNo.Replace("-", "");
                                    if (tmpBktraveler.AirSeatAssignment.Where(y => !string.IsNullOrEmpty(y.Status) && y.Status == "HK"
                                    && y.Seat == seatno && x.Segment == y.SegmentRef).Count() == 0)
                                    {
                                        x.SeatStatus = "F";
                                        if (islastloop)
                                        {
                                            string msg = "Failed to assign seat for passenger " + m.FirstName + " " + m.LastName + " for segment (" + x.Segment + ")";
                                            booking.Error = string.IsNullOrEmpty(booking.Error) ? msg : booking.Error + "@" + msg;
                                            x.SeatNo = string.Empty;
                                        }
                                    }
                                    else
                                        x.SeatStatus = "S";
                                });
                            });
                        }
                    }
                }

                // end SSR
                string URlocatorCode = unRecord.LocatorCode;// UR
                string providerLocatorCode = unRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test
                // PNR ( supplier)
                string reservationLocatorCode = string.Empty;
                itinerary.ProductType = ProductType.Flight;//To Avoid Serialization failure
                itinerary.BookingMode = BookingMode.Auto;//To Avoid Serialization failure
                //itinerary.PaymentMode = ModeOfPayment.Credit;//To Avoid Serialization failure
                itinerary.ProductTypeId = 1;

                UAPIdll.Universal46.AirReservation[] airReservationList = unRecord.Items;
                if (airReservationList != null)
                {
                    List<FlightInfo> Segments = new List<FlightInfo>(itinerary.Segments);
                    foreach (UAPIdll.Universal46.AirReservation airReservation in airReservationList)
                    {
                        UAPIdll.Universal46.AirPricingInfo[] airPricingList = airReservation.AirPricingInfo;

                        reservationLocatorCode = airReservation.LocatorCode;// TO do with multiple Airline Test
                        UAPIdll.Universal46.typeBaseAirSegment[] airSegmentList = airReservation.AirSegment;
                        int airSegIndex = 0;
                        if (airSegmentList != null)
                        {
                            foreach (UAPIdll.Universal46.typeBaseAirSegment airSegment in airSegmentList)
                            {
                                string status = airSegment.Status;

                                //Update Terminal info
                                if (airSegment.FlightDetails != null)
                                {
                                    foreach (UAPIdll.Universal46.FlightDetails fd in airSegment.FlightDetails)
                                    {
                                        FlightInfo seg = Segments.Find(delegate (FlightInfo fi) { return fd.Origin == fi.Origin.AirportCode && fd.Destination == fi.Destination.AirportCode; });
                                        try
                                        {
                                            if (!string.IsNullOrEmpty(fd.OriginTerminal))
                                                seg.DepTerminal = fd.OriginTerminal;
                                            else
                                            {
                                                string sSellMsg = airSegment.SellMessage.ToList().Find(x => x.Contains("TERMINAL"));
                                                if (!string.IsNullOrEmpty(sSellMsg))
                                                {
                                                    sSellMsg = sSellMsg.Replace("DEPARTS " + airSegment.Origin + " TERMINAL ", "");
                                                    sSellMsg = sSellMsg.Replace("ARRIVES " + airSegment.Destination + " TERMINAL ", "");
                                                    seg.DepTerminal = sSellMsg.Split('-').Length > 0 ? sSellMsg.Split('-')[0] : "";
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(fd.DestinationTerminal))
                                                seg.ArrTerminal = fd.DestinationTerminal;
                                            else
                                            {
                                                string sSellMsg = airSegment.SellMessage.ToList().Find(x => x.Contains("TERMINAL"));
                                                if (!string.IsNullOrEmpty(sSellMsg))
                                                {
                                                    sSellMsg = sSellMsg.Replace("DEPARTS " + airSegment.Origin + " TERMINAL ", "");
                                                    sSellMsg = sSellMsg.Replace("ARRIVES " + airSegment.Destination + " TERMINAL ", "");
                                                    seg.ArrTerminal = sSellMsg.Split('-').Length > 1 ? sSellMsg.Split('-')[1] : "";
                                                }
                                            }
                                        }
                                        catch { }
                                        //if ((airSegIndex + 1) == Convert.ToInt32(airSegment.TravelOrder)) // To cross check the Segments/Flights
                                        if (itinerary.Segments[airSegIndex].Group == Convert.ToInt32(airSegment.Group) || (airSegment.FlightDetails != null && airSegment.FlightDetails.ToList().Exists(x => x != null && x.Origin == itinerary.Segments[airSegIndex].Origin.AirportCode && x.Destination == itinerary.Segments[airSegIndex].Destination.AirportCode)))//Match with Group since we are storing connection details as segments
                                        {
                                            itinerary.Segments[airSegIndex].Status = status;
                                            if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                                            {
                                                itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;

                                                UAPIdll.Universal46.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                                                if (supLocatorList != null)
                                                {
                                                    foreach (UAPIdll.Universal46.SupplierLocator supLocator in supLocatorList)
                                                    {
                                                        if (airSegment.Carrier == supLocator.SupplierCode)
                                                        {
                                                            itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;
                                                        }
                                                    }
                                                }
                                                if (airPricingList != null)
                                                {
                                                    //******************************** Adding PTC Details ********************************//
                                                    foreach (UAPIdll.Universal46.AirPricingInfo price in airPricingList)
                                                    {
                                                        UAPIdll.Universal46.FareInfo[] fares = price.FareInfo;

                                                        foreach (UAPIdll.Universal46.FareInfo fare in fares)
                                                        {
                                                            if (airSegment.Origin.ToUpper() == fare.Origin.ToUpper() && airSegment.Destination.ToUpper() == fare.Destination.ToUpper())
                                                            {
                                                                SegmentPTCDetail ptc = new SegmentPTCDetail();
                                                                ptc.SegmentId = airSegment.Group;
                                                                ptc.Baggage = "";//We are showing up "Airline Norms" in the ETicket and in email's if baggage is empty
                                                                if (fare.BaggageAllowance != null)
                                                                {
                                                                    if (fare.BaggageAllowance.MaxWeight != null && !string.IsNullOrEmpty(fare.BaggageAllowance.MaxWeight.Value) && fare.BaggageAllowance.MaxWeight.Value.Length > 0 && fare.BaggageAllowance.MaxWeight.Value != "0")
                                                                    {
                                                                        ptc.Baggage = fare.BaggageAllowance.MaxWeight.Value + "Kg";
                                                                    }
                                                                    else if (!string.IsNullOrEmpty(fare.BaggageAllowance.NumberOfPieces) && fare.BaggageAllowance.NumberOfPieces.Length > 0)
                                                                    {
                                                                        ptc.Baggage = fare.BaggageAllowance.NumberOfPieces + " piece";
                                                                    }
                                                                    else
                                                                    {
                                                                        ptc.Baggage = "Airline Norms";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    ptc.Baggage = "Airline Norms";
                                                                }
                                                                ptc.FlightKey = seg.FlightKey;//itinerary.Segments[airSegIndex].FlightKey;
                                                                ptc.FareBasis = fare.FareBasis;
                                                                ptc.PaxType = fare.PassengerTypeCode;
                                                                ptc.NVA = "";
                                                                ptc.NVB = "";
                                                                ptcDetail.Add(ptc);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    booking.Error = "Pricing details not returned for this PNR!";
                                                    booking.Status = BookingResponseStatus.Failed;
                                                    return booking;
                                                }
                                            }
                                        }
                                        airSegIndex++;
                                    }
                                }
                                else
                                {
                                    booking.Error = "Flight details not returned for this PNR!";
                                    booking.Status = BookingResponseStatus.Failed;
                                    return booking;
                                }
                            }
                        }
                        else
                        {
                            booking.Error = "Flight details not returned for this PNR!";
                            booking.Status = BookingResponseStatus.Failed;
                            return booking;
                        }
                    }

                    itinerary.UniversalRecord = URlocatorCode;// Assigning UR to Itinerary
                    itinerary.PNR = providerLocatorCode;// //Provider PNR is Actual Pnr
                    itinerary.AirLocatorCode = reservationLocatorCode;//Reservation PNR is for issue the Ticket
                    booking.PNR = providerLocatorCode;//reservationLocatorCode;
                    itinerary.FareType = "PUB";
                    booking.Status = BookingResponseStatus.Successful;
                    // SSR Details
                }
                else
                {
                    booking.Status = BookingResponseStatus.Failed;
                    booking.Error = "NO VALID FARE FOR INPUT CRITERIA";
                }
            }
            else
            {
                booking.Status = BookingResponseStatus.Failed;
                booking.Error = "UniversalRecord is null";
            }
            if (ssrMessage.Length > 0)
            {
                booking.SSRMessage = ssrMessage;
                booking.SSRDenied = true;
            }

            return booking;
        }
        # endregion

        # region Cancel Itinerary
        public string CancelItinerary(string pnr, string pnrType)
        {
            Connection();
            //bool isCancel = false;
            string tempPNR = pnr;
            string cancelledPNR = string.Empty;
            string cancelledProvider = string.Empty;
            bool isCancelled = false;

            if (pnrType != "UR")
            {
                FlightItinerary itinerary = new FlightItinerary(FlightItinerary.GetFlightId(pnr));
                pnr = itinerary.UniversalRecord;

            }


            //CT.Core.Audit.Add(EventType.CancellBooking, Severity.Normal, 0, "UAPI Cancell Itinerary : Request UR PNR = " + pnr, "0");

            UAPIdll.Universal46.UniversalRecordCancelReq urCancelRequest = new UAPIdll.Universal46.UniversalRecordCancelReq();
            urCancelRequest.TargetBranch = TargetBranch;
            urCancelRequest.UniversalRecordLocatorCode = pnr;
            urCancelRequest.Version = "18";// TO DO
            urCancelRequest.AuthorizedBy = string.Empty;

            UAPIdll.Universal46.BillingPointOfSaleInfo pos = new UAPIdll.Universal46.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            urCancelRequest.BillingPointOfSaleInfo = pos;

            UAPIdll.Universal46.UniversalRecordCancelServiceBinding binding = new UAPIdll.Universal46.UniversalRecordCancelServiceBinding();

            binding.Url = urlUR;
            binding.Credentials = new NetworkCredential(UserName, Password);
            binding.PreAuthenticate = true;//to implement GZIP

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(urCancelRequest.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, urCancelRequest); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\UniversalRecordCancelReq.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightURCancelRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            catch { }

            UAPIdll.Universal46.UniversalRecordCancelRsp urCancelResponse = null;

            try
            {
                urCancelResponse = binding.service(urCancelRequest);
            }
            catch (SoapException se)
            {
                Audit.Add(EventType.CancellBooking, Severity.High, 0, "UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed. Erro msg:" + se.Message, string.Empty);
                cancelledPNR = se.Message;

                return cancelledPNR;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CancellBooking, Severity.High, 0, "UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed. Erro msg:" + ex.Message, string.Empty);
                cancelledPNR = ex.Message;

                return cancelledPNR;
            }


            // TO Remove
            try
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(urCancelResponse.GetType());
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, urCancelResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docRes = new XmlDocument();
                docRes.LoadXml(sbRes.ToString());
                //docRes.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\UniversalRecordCancelRsp.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightURCancelResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docRes.Save(filePath);
            }
            catch { }
            UAPIdll.Universal46.ProviderReservationStatus[] reservationStatusList = urCancelResponse.ProviderReservationStatus;
            if (reservationStatusList != null)
            {
                foreach (UAPIdll.Universal46.ProviderReservationStatus reservationStatus in reservationStatusList)
                {
                    isCancelled = reservationStatus.Cancelled;
                    cancelledPNR = reservationStatus.LocatorCode;
                    cancelledProvider = reservationStatus.ProviderCode;
                }

            }
            if (isCancelled)
            {
                if (pnrType != "UR")
                    cancelledPNR = tempPNR;



            }
            else
            {
                //Trace.TraceError("UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed.");
                throw new BookingEngineException("Cancellation of PNR " + pnr + " failed.");
            }






            return cancelledPNR;
        }
        # endregion

        # region Retrieve Itinerary
        public FlightItinerary RetrieveItinerary(string pnr, UAPICredentials _uapiCredentials)
        {
            FlightItinerary itinerary = new FlightItinerary();
            Ticket[] ticket;
            //Trace.TraceInformation("UAPI.RetrieveItinerary entered WITH PNR Param : PNR= " + pnr);
            UAPIdll.Universal46.UniversalRecordImportRsp URimportResponse = new UAPIdll.Universal46.UniversalRecordImportRsp();
            itinerary = RetrieveItinerary(pnr, out ticket, _uapiCredentials, ref URimportResponse);
            return itinerary;
        }

        /// <summary>
        /// This method retrieves Universal Record for the PNR.
        /// </summary>
        /// <param name="pnr">PNR of the Itinerary to be retrieved</param>
        /// <param name="ticket">Ticket array</param>
        /// <param name="_uapiCredentials">UAPICredentials for authentication</param>
        /// <param name="URimportResponse">UniversalRecordImportRsp reference</param>
        /// <returns>FlightItnerary object</returns>
        public FlightItinerary RetrieveItinerary(string pnr, out Ticket[] ticket, UAPICredentials _uapiCredentials, ref UAPIdll.Universal46.UniversalRecordImportRsp URimportResponse)
        {
            FlightItinerary itinerary = new FlightItinerary();
            //Trace.TraceInformation("UAPI.RetrieveItinerary entered : PNR = " + pnr);
            try
            {
                // UAPIdll.Universal46.UniversalRecordRetrieveRsp pnrResponse = GenerateRetrievePNRObject(pnr);
                URimportResponse = ImportUR(pnr, _uapiCredentials);
                try
                {
                    itinerary = MakeBookingObject(URimportResponse, out ticket);
                }
                catch (BookingEngineException ex)
                {
                    // throw ex;
                    string errorMess = ex.Message;
                    string restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                    if (errorMess.IndexOf(restrictedBf) >= 0)
                    {
                        //connection = new Connection(true);

                        //pnrResponse = connection.SubmitXml(request);
                        try
                        {
                            itinerary = MakeBookingObject(URimportResponse, out ticket);
                        }
                        catch (BookingEngineException excep)
                        {
                            throw excep;
                            //errorMess = excep.Message;
                            //restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                            //if (errorMess.IndexOf(restrictedBf) >= 0)
                            //{
                            //    string pcc = ConfigurationSystem.GalileoConfig["DomesticPCC"];
                            //    itinerary.PNR = pnr;
                            //    GetTicketref itinerary, out ticket, pcc);
                            //}
                            //else
                            //{
                            //    throw ex;
                            //}
                        }
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            catch (SoapException se)
            {
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return itinerary;
        }
        public FlightItinerary RetrieveItinerary(FlightItinerary itinerary, out Ticket[] ticket, UAPICredentials _uapiCredentials)
        {
            //FlightItinerary itinerary = new FlightItinerary();
            //Trace.TraceInformation("UAPI.RetrieveItinerary entered : PNR = " + itinerary.PNR);

            try
            {
                UAPIdll.Universal46.UniversalRecordImportRsp URimportResponse = ImportUR(itinerary.PNR, _uapiCredentials);
                try
                {
                    itinerary = MakeBookingObject(URimportResponse, out ticket);
                }
                catch (BookingEngineException ex)
                {
                    // throw ex;
                    string errorMess = ex.Message;
                    string restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                    if (errorMess.IndexOf(restrictedBf) >= 0)
                    {
                        //connection = new Connection(true);

                        //pnrResponse = connection.SubmitXml(request);
                        try
                        {
                            itinerary = MakeBookingObject(URimportResponse, out ticket);
                        }
                        catch (BookingEngineException excep)
                        {
                            throw excep;
                            //errorMess = excep.Message;
                            //restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                            //if (errorMess.IndexOf(restrictedBf) >= 0)
                            //{
                            //    string pcc = ConfigurationSystem.GalileoConfig["DomesticPCC"];
                            //    itinerary.PNR = pnr;
                            //    GetTicketref itinerary, out ticket, pcc);
                            //}
                            //else
                            //{
                            //    throw ex;
                            //}
                        }
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            catch (SoapException se)
            {
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return itinerary;
        }

        private UAPIdll.Universal46.UniversalRecordRetrieveRsp GenerateRetrievePNRObject(string pnr)// Not using this method since we are using Import UR
        {
            Connection();
            //Trace.TraceInformation("UApi.GenerateRetrievePNRObject entered");
            UAPIdll.Universal46.UniversalRecordRetrieveReq request = new UAPIdll.Universal46.UniversalRecordRetrieveReq();

            request.TargetBranch = TargetBranch;
            request.Item = pnr;

            UAPIdll.Universal46.BillingPointOfSaleInfo pos = new UAPIdll.Universal46.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            request.BillingPointOfSaleInfo = pos;

            UAPIdll.Universal46.UniversalRecordRetrieveServiceBinding binding = new UAPIdll.Universal46.UniversalRecordRetrieveServiceBinding();
            binding.Url = urlUR;
            binding.Credentials = new NetworkCredential(UserName, Password);
            binding.PreAuthenticate = true;//to implement GZIP

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(request.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URRetrieveReq.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightURRetrieveRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            catch { }

            UAPIdll.Universal46.UniversalRecordRetrieveRsp response = null;
            try
            {
                response = binding.service(request);

            }
            catch { throw; }
            //{
            //    throw ex;
            //}

            try
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(response.GetType());
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URRetrieveRes.xml");
            }
            catch { }
            //Trace.TraceInformation("UApi.GenerateRetrievePNRObject exiting");
            return response;
        }

        /* private FlightItinerary MakeBookingObject(UAPIdll.Universal46.UniversalRecordRetrieveRsp response, out Ticket[] ticket)// this method is not using currently,its for retrive pnr.. refer overloaded method
         {
             ticket = new Ticket[0];
             FlightItinerary itinerary = new FlightItinerary();
             UAPIdll.Universal46.UniversalRecord uRecord=response.UniversalRecord;
             itinerary.UniversalRecord = uRecord.LocatorCode;

             itinerary.PNR = uRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test


             // Passenger Information 
             UAPIdll.Universal46.BookingTraveler[] bookingTravelerList = uRecord.BookingTraveler;

             Dictionary<long, FlightPassenger> passengerDict = new Dictionary<long, FlightPassenger>();// To Stor Passnger Details
             FlightPassenger[] tempPassengerList = new FlightPassenger[bookingTravelerList.Length];
             Dictionary<long, List<KeyValuePair<string, decimal>>> taxBreakUpPax = new Dictionary<long, List<KeyValuePair<string, decimal>>>(); // To Store Pax wise Tax Break Up
             int paxIndex = 0;
             foreach (UAPIdll.Universal46.BookingTraveler bookingTraveler in bookingTravelerList)
             {
                 //bookingTraveler.TravelerType
                 tempPassengerList[paxIndex] = new FlightPassenger();
                 if (bookingTraveler.TravelerType == "ADT")
                     tempPassengerList[paxIndex].Type = PassengerType.Adult;
                 if (bookingTraveler.TravelerType == "CNN")
                     tempPassengerList[paxIndex].Type = PassengerType.Child;
                 if (bookingTraveler.TravelerType == "INF")
                     tempPassengerList[paxIndex].Type = PassengerType.Infant;
                 if (bookingTraveler.TravelerType == "SRC")
                     tempPassengerList[paxIndex].Type = PassengerType.Senior;

                 UAPIdll.Universal46.BookingTravelerName travelerName = new UAPIdll.Universal46.BookingTravelerName();
                 travelerName = bookingTraveler.BookingTravelerName;
                 tempPassengerList[paxIndex].FirstName = travelerName.First;
                 tempPassengerList[paxIndex].LastName = travelerName.Last;
                 tempPassengerList[paxIndex].Title = travelerName.Prefix;


                 UAPIdll.Universal46.PhoneNumber[] phone = bookingTraveler.PhoneNumber;
                 tempPassengerList[paxIndex].CellPhone = phone[0].Number;
                 UAPIdll.Universal46.typeStructuredAddress[] adds = bookingTraveler.Address;
                 tempPassengerList[paxIndex].AddressLine1 = adds[0].AddressName;
                 if (adds[0].Street != null)
                 {
                     string[] street = adds[0].Street;
                     tempPassengerList[paxIndex].AddressLine2 = street[0].ToString();
                 }

                 if(adds[0].City!=null)tempPassengerList[paxIndex].City = adds[0].City;

                 Country tempCountry=new Country();
                 tempCountry.CountryCode= adds[0].Country;
                 tempPassengerList[paxIndex].Country=tempCountry;
                 passengerDict.Add(Convert.ToInt64(bookingTraveler.Key), tempPassengerList[paxIndex]);
                 paxIndex++;

                 // Segment Information


             }
             // Air Reservation Details
             string reservationLocatorCode = string.Empty;
             UAPIdll.Universal46.AirReservation[] airReservationList = response.UniversalRecord.AirReservation;
             if (airReservationList != null)
             {

                 foreach (UAPIdll.Universal46.AirReservation airReservation in airReservationList)
                 {
                     reservationLocatorCode = airReservation.LocatorCode;// storing the PNR for issue the ticket for UAPI-TO do with multiple Airline Test

                     itinerary.CreatedOn = Convert.ToDateTime(airReservation.CreateDate);
                     //itinerary.Ticketed = true; TO check after Ticket
                     itinerary.FareType = "PUB";
                     itinerary.FlightBookingSource = BookingSource.UAPI;
                     itinerary.Segments = new FlightInfo[airReservation.AirSegment.Length];

                     // Air Segment Details
                      int airSegIndex = 0;
                      foreach (UAPIdll.Universal46.AirSegment tempAirSegment in airReservation.AirSegment)
                     {

                         itinerary.Segments[airSegIndex] = new FlightInfo();
                         itinerary.Segments[airSegIndex].Status = tempAirSegment.Status;
                         if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                             itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                         itinerary.Segments[airSegIndex].Airline = tempAirSegment.Carrier;
                         itinerary.Segments[airSegIndex].Origin = new Airport(tempAirSegment.Origin);
                         //itinerary.Segments[airSegIndex].Origin.AirportCode= tempAirSegment.Origin;
                         itinerary.Segments[airSegIndex].Destination= new Airport(tempAirSegment.Destination);
                         //itinerary.Segments[airSegIndex].Destination.AirportCode = tempAirSegment.Destination;

                          // Local Time
                          string depTime = tempAirSegment.DepartureTime.Substring(0,tempAirSegment.DepartureTime.IndexOf("+"));
                          string arrTime = tempAirSegment.ArrivalTime.Substring(0, tempAirSegment.ArrivalTime.IndexOf("+"));


                         itinerary.Segments[airSegIndex].DepartureTime = Convert.ToDateTime(depTime);
                         itinerary.Segments[airSegIndex].ArrivalTime = Convert.ToDateTime(arrTime);
                          // GMT Time
                         //itinerary.Segments[airSegIndex].ArrivalTime= Convert.ToDateTime(tempAirSegment.ArrivalTime);
                         //itinerary.Segments[airSegIndex].DepartureTime= Convert.ToDateTime(tempAirSegment.DepartureTime);
                         itinerary.Segments[airSegIndex].BookingClass = tempAirSegment.ClassOfService;
                         itinerary.Segments[airSegIndex].CabinClass = tempAirSegment.ClassOfService;
                         itinerary.Segments[airSegIndex].FlightNumber= tempAirSegment.FlightNumber;
                         itinerary.Segments[airSegIndex].UapiSegmentRefKey= Convert.ToInt32(tempAirSegment.Key);
                         itinerary.Segments[airSegIndex].AirlinePNR = airReservation.LocatorCode;
                         UAPIdll.Universal46.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                         if (supLocatorList != null)
                         {
                             foreach (UAPIdll.Universal46.SupplierLocator supLocator in supLocatorList)
                             {

                                 if (tempAirSegment.Carrier == supLocator.SupplierCode)
                                 {
                                     itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                 }
                             }
                         }



                         //UAPIdll.Universal46.typeEticketability ticketability = new UAPIdll.Universal46.typeEticketability();
                         //ticketability = tempAirSegment.ETicketability;
                         if (tempAirSegment.ETicketability == UAPIdll.Universal46.typeEticketability.Yes)
                             itinerary.Segments[airSegIndex].ETicketEligible = true;
                         else itinerary.Segments[airSegIndex].ETicketEligible = false;


                         itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                         airSegIndex++;

                     }


                     // Air Pricing Info Details
                      bool nonRefundable = false;
                      //double totalResultFare = 0;
                      //double totalbaseFare = 0;
                      string lastTktDate = string.Empty;
                      //pricingSolution.Journey[0].
                      //int fareKey = 0;
                      decimal baseFare = 0;
                      decimal totalFare = 0;
                      //fare = new Fare[pricingSolution.AirPricingInfo.Length];

                     // Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                      //int infoIndex = 0;
                     Dictionary<string, Dictionary<string, UAPIdll.Universal46.FareInfo>> fareInfoPaxtDict = new Dictionary<string, Dictionary<string, UAPIdll.Universal46.FareInfo>>();
                      foreach (UAPIdll.Universal46.AirPricingInfo pricingInfo in airReservation.AirPricingInfo) //AirPricingInfo
                      {
                          baseFare = (decimal)getCurrAmount(pricingInfo.BasePrice);
                          totalFare = (decimal)getCurrAmount(pricingInfo.TotalPrice);
                          lastTktDate = pricingInfo.LatestTicketingTime;
                          nonRefundable = !pricingInfo.Refundable;
                          //Fare Info Details
                          Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoDict = new Dictionary<string, UAPIdll.Universal46.FareInfo>();
                          Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoSegmentDict = new Dictionary<string, UAPIdll.Universal46.FareInfo>();

                          foreach (UAPIdll.Universal46.FareInfo tempFareInfo in pricingInfo.FareInfo)
                          {
                              fareInfoDict.Add(tempFareInfo.Key, tempFareInfo);

                          }

                          foreach(UAPIdll.Universal46.BookingInfo tempBookingInfo in pricingInfo.BookingInfo)
                          {
                              fareInfoSegmentDict.Add(tempBookingInfo.SegmentRef,fareInfoDict[tempBookingInfo.FareInfoRef]);
                          }


                          UAPIdll.Universal46.PassengerType[] tempPassTypeList = pricingInfo.PassengerType;
                          foreach (UAPIdll.Universal46.PassengerType tempPassType in tempPassTypeList)
                          {


                              fareInfoPaxtDict.Add(tempPassType.BookingTravelerRef, fareInfoSegmentDict);
                              // Price Details

                              PriceAccounts price = new PriceAccounts();
                              price.PublishedFare = baseFare;
                              price.Tax = totalFare - baseFare;

                              FlightPassenger tempPassenger = passengerDict[Convert.ToInt64(tempPassType.BookingTravelerRef)];
                              tempPassenger.Price = price;
                              // For Tax Break Up -- not yet implementd( only after Ticket)
                              List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
                              UAPIdll.Universal46.typeTaxInfo[] tempTaxTypeList = pricingInfo.TaxInfo;
                              if (tempTaxTypeList != null && tempTaxTypeList.Length > 0)
                              {
                                  foreach (UAPIdll.Universal46.typeTaxInfo tempTaxType in tempTaxTypeList)
                                  {
                                      string taxType = tempTaxType.Category;
                                      decimal taxValue = (decimal)getCurrAmount(tempTaxType.Amount);
                                      taxBreakUp.Add(new KeyValuePair<string, decimal>(taxType, taxValue));
                                  }
                                  taxBreakUpPax.Add(Convert.ToInt64(tempPassType.BookingTravelerRef), taxBreakUp);
                              }


                          }

                      }// Air Pricing Info End



                     // Checking whether ticketed or not
                     List<FareRule> fareRuleList = new List<FareRule>();
                      if (airReservation.DocumentInfo != null)
                      {
                          UAPIdll.Universal46.DocumentInfo docInfo = airReservation.DocumentInfo;

                          if (docInfo.TicketInfo != null && docInfo.TicketInfo.Length > 0)
                          {

                              UAPIdll.Universal46.TicketInfo[] ticketInfoList= docInfo.TicketInfo;
                              // Assiging Ticket Count
                              ticket = new Ticket[ticketInfoList.Length];
                              List<Ticket> ticketArray = new List<Ticket>();
                              int ticketIndex=0;
                              foreach (UAPIdll.Universal46.TicketInfo ticketInfo in ticketInfoList)
                              {
                                  //if(UAPIdll.Universal46.typeTicketStatus
                                  if (!string.IsNullOrEmpty(ticketInfo.Number)) // TODO replace Tic,et Number to Status, once we get status Descritption
                                  {
                                      long travelerRef = Convert.ToInt64(ticketInfo.BookingTravelerRef);
                                      Ticket tempTicket = new Ticket();


                                      tempTicket.TicketNumber = ticketInfo.Number;
                                      //tempTicket.ValidatingAriline = ticketNumNode.InnerText.Substring(0, 3);


                                      itinerary.Ticketed = true;

                                      //Assigning Pax Details
                                      FlightPassenger tempPassenger = passengerDict[travelerRef];
                                      //tempPassenger.p
                                      tempTicket.PaxFirstName = tempPassenger.FirstName;
                                      tempTicket.PaxLastName = tempPassenger.LastName;
                                      tempTicket.PaxType = tempPassenger.Type;
                                      tempTicket.Title= tempPassenger.Title;
                                      tempTicket.ETicket = true;
                                      tempTicket.IssueDate = DateTime.UtcNow;
                                      tempTicket.LastModifiedOn = itinerary.LastModifiedOn;
                                      tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                      // Adding Tax Details for Pax

                                      if(taxBreakUpPax.ContainsKey(travelerRef))
                                      {
                                          tempTicket.TaxBreakup = new List<KeyValuePair<string, decimal>>();

                                          tempTicket.TaxBreakup = taxBreakUpPax[travelerRef];
                                      }

                                      // Adding PTCDetail 

                                      for (int k = 0; k < itinerary.Segments.Length; k++)
                                      {


                                          Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoSegmentTemp= fareInfoPaxtDict[ticketInfo.BookingTravelerRef];
                                          UAPIdll.Universal46.FareInfo tempFareInfo = fareInfoSegmentTemp[Convert.ToString(itinerary.Segments[k].UapiSegmentRefKey)];
                                          // Fare Rules details
                                          if (ticketIndex == 0)
                                          {
                                              FareRule fareRule = new FareRule();
                                              fareRule.FareBasisCode = tempFareInfo.FareBasis;
                                              fareRule.Origin = itinerary.Segments[k].Origin.AirportCode;
                                              fareRule.Destination = itinerary.Segments[k].Destination.AirportCode;
                                              fareRule.Airline = itinerary.Segments[k].Airline;
                                              fareRuleList.Add(fareRule);

                                          }
                                          // Baagege Details
                                          SegmentPTCDetail ptcDtl = new SegmentPTCDetail();
                                          ptcDtl.Baggage = tempFareInfo.BaggageAllowance.MaxWeight.Value;
                                          string flightKey = string.Empty;
                                          flightKey = itinerary.Segments[k].Airline;
                                          //if (itinerary.Segments[k].FlightNumber.Length == 3)
                                          //{
                                          //    flightKey += "0" + itinerary.Segments[k].FlightNumber;
                                          //}
                                          //else if (itinerary.Segments[k].FlightNumber.Length == 4)
                                          //{
                                          //    flightKey += itinerary.Segments[k].FlightNumber;
                                          //}
                                          flightKey += itinerary.Segments[k].FlightNumber.PadLeft(4, '0');
                                          flightKey += itinerary.Segments[k].DepartureTime.ToString("ddMMMyyyyHHmm").ToUpper();
                                          ptcDtl.FlightKey = flightKey;
                                          ptcDtl.NVA = string.Empty;
                                          ptcDtl.NVB = string.Empty;
                                          //ptcDtl.FareBasis = itinerary.FareRules[k].FareBasisCode;
                                          ptcDtl.FareBasis = tempFareInfo.FareBasis;
                                          tempTicket.PtcDetail.Add(ptcDtl);

                                          //ticket[paxIndex].PtcDetail.Add(ptcDtl);
                                      }

                                      //tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                      ticketArray.Add(tempTicket);
                                  }
                                  ticketIndex++;
                              }
                              ticket = ticketArray.ToArray();
                          }
                          else
                              ticket = new Ticket[0];
                      }


                      //if (airReservation.TicketingModifiers != null && airReservation.TicketingModifiers.Length > 0)
                      //{
                      //    UAPIdll.Universal46.TicketingModifiers[] ticketModifierList = airReservation.TicketingModifiers;
                      //    foreach (UAPIdll.Universal46.TicketingModifiers ticketModifier in ticketModifierList)
                      //    {
                      //        itinerary.Ticketed = ticketModifier.DocumentSelect.IssueElectronicTicket;
                      //    }
                      //}


                     if(fareRuleList.Count>0)itinerary.FareRules = fareRuleList;
                 }

             }
             // Adding Temp FLight Passenger List To Itinerary
             itinerary.Passenger = new FlightPassenger[tempPassengerList.Length];

             itinerary.Passenger = tempPassengerList;
             itinerary.AirLocatorCode = reservationLocatorCode;// store the air reservation code to issue the ticket

             // TO DO

             return itinerary;


         }*/
        private FlightItinerary MakeBookingObject(UAPIdll.Universal46.UniversalRecordImportRsp response, out Ticket[] ticket)
        {
            ticket = new Ticket[0];
            FlightItinerary itinerary = new FlightItinerary();
            UAPIdll.Universal46.UniversalRecord uRecord = response.UniversalRecord;
            itinerary.UniversalRecord = uRecord.LocatorCode;
            itinerary.PNR = uRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test
            // Passenger Information 
            UAPIdll.Universal46.BookingTraveler[] bookingTravelerList = uRecord.BookingTraveler;

            Dictionary<string, FlightPassenger> passengerDict = new Dictionary<string, FlightPassenger>();// To Stor Passnger Details
            FlightPassenger[] tempPassengerList = new FlightPassenger[bookingTravelerList.Length];
            Dictionary<string, List<KeyValuePair<string, decimal>>> taxBreakUpPax = new Dictionary<string, List<KeyValuePair<string, decimal>>>(); // To Store Pax wise Tax Break Up
            int paxIndex = 0;
            foreach (UAPIdll.Universal46.BookingTraveler bookingTraveler in bookingTravelerList)
            {
                //bookingTraveler.TravelerType
                tempPassengerList[paxIndex] = new FlightPassenger();
                if (bookingTraveler.TravelerType == "ADT" || bookingTraveler.TravelerType == null)
                    tempPassengerList[paxIndex].Type = PassengerType.Adult;
                if (bookingTraveler.TravelerType == "CNN")
                    tempPassengerList[paxIndex].Type = PassengerType.Child;
                if (bookingTraveler.TravelerType == "INF")
                    tempPassengerList[paxIndex].Type = PassengerType.Infant;
                if (bookingTraveler.TravelerType == "SRC")
                    tempPassengerList[paxIndex].Type = PassengerType.Senior;

                UAPIdll.Universal46.BookingTravelerName travelerName = new UAPIdll.Universal46.BookingTravelerName();
                travelerName = bookingTraveler.BookingTravelerName;
                tempPassengerList[paxIndex].FirstName = travelerName.First;
                tempPassengerList[paxIndex].LastName = travelerName.Last;
                tempPassengerList[paxIndex].Title = travelerName.Prefix;
                tempPassengerList[paxIndex].DateOfBirth = Convert.ToDateTime(bookingTraveler.DOB);
                // for passport
                try
                {
                    UAPIdll.Universal46.SSR[] SSRList = bookingTraveler.SSR;
                    if (SSRList != null)
                    {
                        string passport = string.Empty;
                        foreach (UAPIdll.Universal46.SSR ssrPassport in SSRList)
                        {
                            if (ssrPassport.Type == "DOCS")
                            {
                                //string passport = "P/AE/00002345/AE/0000000/M/02FEB17/MHD/ZIYAD -1ZIYAD/MHDMR";
                                passport = ssrPassport.FreeText;
                                passport = passport.Substring(passport.IndexOf("/", 2) + 1, passport.Length - passport.IndexOf("/", 2) - 1);
                                passport = passport.Substring(0, passport.IndexOf("/", 1));
                                break;
                            }

                        }
                        tempPassengerList[paxIndex].PassportNo = passport;

                    }
                }
                catch { }
                if (bookingTraveler.Gender == "M")
                    tempPassengerList[paxIndex].Gender = Gender.Male;
                else if (bookingTraveler.Gender == "F")
                    tempPassengerList[paxIndex].Gender = Gender.Female;



                UAPIdll.Universal46.PhoneNumber[] phone = bookingTraveler.PhoneNumber;
                if (phone != null)
                {
                    tempPassengerList[paxIndex].CellPhone = phone[0].Number;
                }
                UAPIdll.Universal46.Email[] mail = bookingTraveler.Email;
                if (mail != null)
                {
                    tempPassengerList[paxIndex].Email = mail[0].EmailID;
                }
                UAPIdll.Universal46.typeStructuredAddress[] adds = bookingTraveler.Address;
                if (adds != null)
                {
                    tempPassengerList[paxIndex].AddressLine1 = adds[0].AddressName;
                    if (adds[0].Street != null)
                    {
                        string[] street = adds[0].Street;
                        tempPassengerList[paxIndex].AddressLine2 = street[0].ToString();
                    }

                    if (adds[0].City != null) tempPassengerList[paxIndex].City = adds[0].City;
                    if (adds[0].Country != null)
                    {
                        Country tempCountry = new Country();
                        tempCountry.CountryCode = adds[0].Country;
                        tempPassengerList[paxIndex].Country = tempCountry;
                    }
                }
                passengerDict.Add((bookingTraveler.Key), tempPassengerList[paxIndex]);
                paxIndex++;

                // Segment Information


            }
            // Air Reservation Details
            string reservationLocatorCode = string.Empty;
            UAPIdll.Universal46.AirReservation[] airReservationList = response.UniversalRecord.Items;
            if (airReservationList != null)
            {
                if (airReservationList[0].AirSegment == null)
                {
                    throw new BookingEngineException("Segment details missing for this PNR!");
                }
                foreach (UAPIdll.Universal46.AirReservation airReservation in airReservationList)
                {
                    reservationLocatorCode = airReservation.LocatorCode;// storing the PNR for issue the ticket for UAPI-TO do with multiple Airline Test
                    itinerary.CreatedOn = Convert.ToDateTime(airReservation.CreateDate);
                    //itinerary.Ticketed = true; TO check after Ticket
                    itinerary.FareType = "PUB";
                    itinerary.FlightBookingSource = BookingSource.UAPI;
                    itinerary.Segments = new FlightInfo[airReservation.AirSegment.Length];
                    itinerary.ProductTypeId = 1;
                    itinerary.BookingMode = BookingMode.Auto;//To Avoid Serialization failure;
                    //itinerary.PaymentMode = ModeOfPayment.Credit;//To Avoid Serialization failure;
                    itinerary.ProductType = ProductType.Flight;//To Avoid Serialization failure;

                    //Create segments for Connection flights also
                    int segCount = 0;
                    foreach (UAPIdll.Universal46.typeBaseAirSegment tempAirSegment in airReservation.AirSegment)
                    {
                        //Sum all segment flight details
                        segCount += tempAirSegment.FlightDetails.Length;
                    }
                    itinerary.Segments = new FlightInfo[segCount];

                    // Air Segment Details
                    int airSegIndex = 0;
                    foreach (UAPIdll.Universal46.typeBaseAirSegment tempAirSegment in airReservation.AirSegment)
                    {
                        foreach (UAPIdll.Universal46.FlightDetails fd in tempAirSegment.FlightDetails)
                        {
                            itinerary.Segments[airSegIndex] = new FlightInfo();
                            itinerary.Segments[airSegIndex].Status = tempAirSegment.Status;
                            if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                                itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                            else
                                itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Waitlisted;
                            itinerary.Segments[airSegIndex].Airline = tempAirSegment.Carrier;
                            itinerary.Segments[airSegIndex].Origin = new Airport(fd.Origin);
                            //itinerary.Segments[airSegIndex].Origin.AirportCode= tempAirSegment.Origin;
                            itinerary.Segments[airSegIndex].Destination = new Airport(fd.Destination);
                            //itinerary.Segments[airSegIndex].Destination.AirportCode = tempAirSegment.Destination;

                            // Local Time
                            string GMTdiff = fd.DepartureTime.IndexOf("+") != -1 ? "+" : "-";
                            string depTime = fd.DepartureTime.Substring(0, fd.DepartureTime.LastIndexOf(GMTdiff));
                            GMTdiff = fd.ArrivalTime.IndexOf("+") != -1 ? "+" : "-";
                            string arrTime = fd.ArrivalTime.Substring(0, fd.ArrivalTime.LastIndexOf(GMTdiff));


                            itinerary.Segments[airSegIndex].DepartureTime = Convert.ToDateTime(depTime);
                            itinerary.Segments[airSegIndex].ArrivalTime = Convert.ToDateTime(arrTime);
                            // GMT Time
                            //itinerary.Segments[airSegIndex].ArrivalTime= Convert.ToDateTime(tempAirSegment.ArrivalTime);
                            //itinerary.Segments[airSegIndex].DepartureTime= Convert.ToDateTime(tempAirSegment.DepartureTime);
                            itinerary.Segments[airSegIndex].BookingClass = tempAirSegment.ClassOfService;
                            itinerary.Segments[airSegIndex].CabinClass = tempAirSegment.ClassOfService;
                            itinerary.Segments[airSegIndex].FlightNumber = tempAirSegment.FlightNumber;
                            itinerary.Segments[airSegIndex].UapiSegmentRefKey = tempAirSegment.Key;//Convert.ToInt32(tempAirSegment.Key);
                            itinerary.Segments[airSegIndex].AirlinePNR = airReservation.LocatorCode;
                            itinerary.Segments[airSegIndex].Group = tempAirSegment.Group;
                            //Update Terminal info  
                            try
                            {
                                if (!string.IsNullOrEmpty(fd.OriginTerminal))
                                    itinerary.Segments[airSegIndex].DepTerminal = fd.OriginTerminal;
                                else
                                {
                                    string sSellMsg = tempAirSegment.SellMessage.ToList().Find(x => x.Contains("TERMINAL"));
                                    if (!string.IsNullOrEmpty(sSellMsg))
                                    {
                                        sSellMsg = sSellMsg.Replace("DEPARTS " + tempAirSegment.Origin + " TERMINAL ", "");
                                        sSellMsg = sSellMsg.Replace("ARRIVES " + tempAirSegment.Destination + " TERMINAL ", "");
                                        itinerary.Segments[airSegIndex].DepTerminal = sSellMsg.Split('-').Length > 0 ? sSellMsg.Split('-')[0] : "";
                                    }
                                }
                                if (!string.IsNullOrEmpty(fd.DestinationTerminal))
                                    itinerary.Segments[airSegIndex].ArrTerminal = fd.DestinationTerminal;
                                else
                                {
                                    string sSellMsg = tempAirSegment.SellMessage.ToList().Find(x => x.Contains("TERMINAL"));
                                    if (!string.IsNullOrEmpty(sSellMsg))
                                    {
                                        sSellMsg = sSellMsg.Replace("DEPARTS " + tempAirSegment.Origin + " TERMINAL ", "");
                                        sSellMsg = sSellMsg.Replace("ARRIVES " + tempAirSegment.Destination + " TERMINAL ", "");
                                        itinerary.Segments[airSegIndex].ArrTerminal = sSellMsg.Split('-').Length > 1 ? sSellMsg.Split('-')[1] : "";
                                    }
                                }
                            }
                            catch { }
                            //itinerary.Segments[airSegIndex].ArrTerminal = fd.DestinationTerminal;
                            //itinerary.Segments[airSegIndex].DepTerminal = fd.OriginTerminal;
                            int duration = Convert.ToInt32(fd.FlightTime);
                            itinerary.Segments[airSegIndex].Duration = new TimeSpan(0, duration, 0);
                            UAPIdll.Universal46.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                            if (supLocatorList != null)
                            {
                                foreach (UAPIdll.Universal46.SupplierLocator supLocator in supLocatorList)
                                {

                                    if (tempAirSegment.Carrier == supLocator.SupplierCode)
                                    {
                                        itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                    }
                                }
                            }



                            //UAPIdll.Universal46.typeEticketability ticketability = new UAPIdll.Universal46.typeEticketability();
                            //ticketability = tempAirSegment.ETicketability;
                            if (tempAirSegment.ETicketability == UAPIdll.Universal46.typeEticketability.Yes)
                                itinerary.Segments[airSegIndex].ETicketEligible = true;
                            else itinerary.Segments[airSegIndex].ETicketEligible = false;


                            itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                            airSegIndex++;
                        }
                    }


                    // Air Pricing Info Details
                    bool nonRefundable = false;
                    //double totalResultFare = 0;
                    //double totalbaseFare = 0;
                    string lastTktDate = string.Empty;
                    //pricingSolution.Journey[0].
                    //int fareKey = 0;
                    decimal baseFare = 0;
                    decimal totalFare = 0;




                    //fare = new Fare[pricingSolution.AirPricingInfo.Length];

                    // Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                    //int infoIndex = 0;
                    Dictionary<string, Dictionary<string, UAPIdll.Universal46.FareInfo>> fareInfoPaxtDict = new Dictionary<string, Dictionary<string, UAPIdll.Universal46.FareInfo>>();
                    if (airReservation.AirPricingInfo == null)
                    {
                        throw new BookingEngineException("Pricing Info is not assigned to this PNR!");
                    }
                    foreach (UAPIdll.Universal46.AirPricingInfo pricingInfo in airReservation.AirPricingInfo) //AirPricingInfo
                    {
                        string currency = getCurrency(pricingInfo.TotalPrice);

                        if (ExchangeRates.ContainsKey(currency))
                        {
                            rateOfExchange = Convert.ToDouble(ExchangeRates[currency]);
                        }
                        else
                        {
                            rateOfExchange = 1;
                        }
                        baseFare = (decimal)getCurrAmount(pricingInfo.ApproximateBasePrice) * Convert.ToDecimal(rateOfExchange);
                        totalFare = (decimal)getCurrAmount(pricingInfo.TotalPrice) * Convert.ToDecimal(rateOfExchange);
                        lastTktDate = pricingInfo.LatestTicketingTime;
                        nonRefundable = !pricingInfo.Refundable;
                        //Fare Info Details
                        Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoDict = new Dictionary<string, UAPIdll.Universal46.FareInfo>();
                        Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoSegmentDict = new Dictionary<string, UAPIdll.Universal46.FareInfo>();

                        foreach (UAPIdll.Universal46.FareInfo tempFareInfo in pricingInfo.FareInfo)
                        {
                            fareInfoDict.Add(tempFareInfo.Key, tempFareInfo);
                        }

                        foreach (UAPIdll.Universal46.BookingInfo tempBookingInfo in pricingInfo.BookingInfo)
                        {
                            fareInfoSegmentDict.Add(tempBookingInfo.SegmentRef, fareInfoDict[tempBookingInfo.FareInfoRef]);
                        }


                        UAPIdll.Universal46.PassengerType[] tempPassTypeList = pricingInfo.PassengerType;
                        foreach (UAPIdll.Universal46.PassengerType tempPassType in tempPassTypeList)
                        {
                            fareInfoPaxtDict.Add(tempPassType.BookingTravelerRef, fareInfoSegmentDict);
                            // Price Details

                            PriceAccounts price = new PriceAccounts();
                            price.PublishedFare = baseFare;
                            price.Tax = totalFare - baseFare;
                            /*=========================Price details updated by shiva=========================*/
                            price.RateOfExchange = (decimal)rateOfExchange;
                            price.Currency = agentBaseCurrency;
                            price.CurrencyCode = agentBaseCurrency;
                            price.SupplierPrice = (decimal)getCurrAmount(pricingInfo.TotalPrice);
                            price.SupplierCurrency = pricingInfo.BasePrice.Substring(0, 3);
                            price.AccPriceType = PriceType.PublishedFare;//To Avoid Serialization failure;                            
                            /*=========================Price details=========================*/
                            FlightPassenger tempPassenger = passengerDict[(tempPassType.BookingTravelerRef)];

                            List<TaxBreakup> taxBreakups = new List<TaxBreakup>();
                            // For Tax Break Up -- not yet implementd( only after Ticket)
                            List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
                            UAPIdll.Universal46.typeTaxInfo[] tempTaxTypeList = pricingInfo.TaxInfo;
                            if (tempTaxTypeList != null && tempTaxTypeList.Length > 0)
                            {
                                foreach (UAPIdll.Universal46.typeTaxInfo tempTaxType in tempTaxTypeList)
                                {
                                    string taxType = tempTaxType.Category;
                                    decimal taxValue = (decimal)getCurrAmount(tempTaxType.Amount);
                                    taxBreakUp.Add(new KeyValuePair<string, decimal>(taxType, (decimal)((double)taxValue * rateOfExchange)));
                                    TaxBreakup breakup = new TaxBreakup();
                                    breakup.TaxCode = tempTaxType.Category;
                                    breakup.TaxValue = (decimal)((double)taxValue * rateOfExchange);
                                    taxBreakups.Add(breakup);

                                }
                                taxBreakUpPax.Add((tempPassType.BookingTravelerRef), taxBreakUp);
                                price.TaxBreakups = taxBreakups;
                            }
                            tempPassenger.Price = price;
                        }
                    }// Air Pricing Info End

                    // Checking whether ticketed or not
                    List<FareRule> fareRuleList = new List<FareRule>();
                    if (airReservation.DocumentInfo != null)
                    {
                        UAPIdll.Universal46.DocumentInfo docInfo = airReservation.DocumentInfo;

                        if (docInfo.TicketInfo != null && docInfo.TicketInfo.Length > 0)
                        {

                            UAPIdll.Universal46.TicketInfo[] ticketInfoList = docInfo.TicketInfo;
                            // Assiging Ticket Count
                            ticket = new Ticket[ticketInfoList.Length];
                            List<Ticket> ticketArray = new List<Ticket>();
                            int ticketIndex = 0;
                            foreach (UAPIdll.Universal46.TicketInfo ticketInfo in ticketInfoList)
                            {
                                //if(UAPIdll.Universal46.typeTicketStatus
                                if (!string.IsNullOrEmpty(ticketInfo.Number)) // TODO replace Tic,et Number to Status, once we get status Descritption
                                {
                                    string travelerRef = (ticketInfo.BookingTravelerRef);
                                    Ticket tempTicket = new Ticket();


                                    tempTicket.TicketNumber = ticketInfo.Number;
                                    //tempTicket.ValidatingAriline = ticketNumNode.InnerText.Substring(0, 3);


                                    itinerary.Ticketed = true;

                                    //Assigning Pax Details
                                    FlightPassenger tempPassenger = passengerDict[travelerRef];
                                    //tempPassenger.p
                                    tempTicket.PaxFirstName = tempPassenger.FirstName;
                                    tempTicket.PaxLastName = tempPassenger.LastName;
                                    tempTicket.PaxType = tempPassenger.Type;
                                    tempTicket.Title = tempPassenger.Title;
                                    tempTicket.ETicket = true;
                                    tempTicket.IssueDate = DateTime.UtcNow;
                                    tempTicket.LastModifiedOn = itinerary.LastModifiedOn;
                                    //Assign TourCode, UAPI AccountCode and MasterAccountCode to Ticket for saving in DB
                                    if (airReservationList[0].AirPricingInfo != null && airReservationList[0].AirPricingInfo[0].PricingMethod == UAPIdll.Universal46.typePricingMethod.GuaranteedUsingAirlinePrivateFare)
                                    {
                                        if (airReservationList[0].TicketingModifiers != null && airReservationList[0].TicketingModifiers.Length > 0 && airReservationList[0].TicketingModifiers[0].TourCode != null)
                                        {
                                            //Save Tour Code, TicketDesignator & AccountCode
                                            tempTicket.TourCode = airReservationList[0].TicketingModifiers[0].TourCode.Value;//Tour Code
                                        }
                                        else
                                        {
                                            tempTicket.TourCode = "1";// For Without Airline PrivateFare
                                        }
                                        List<UAPIdll.Universal46.AirPricingInfo> pricingInfo = new List<UAPIdll.Universal46.AirPricingInfo>();
                                        pricingInfo.AddRange(airReservationList[0].AirPricingInfo);
                                        UAPIdll.Universal46.AirPricingInfo price = pricingInfo.Find(delegate (UAPIdll.Universal46.AirPricingInfo p) { return p.Key == ticketInfo.AirPricingInfoRef; });

                                        if (price != null && price.AirPricingModifiers != null && price.AirPricingModifiers.AccountCodes != null && price.AirPricingModifiers.AccountCodes.Length > 0)
                                        {
                                            tempTicket.TicketDesignator = price.AirPricingModifiers.AccountCodes[0].Code;//Our Account Code                                            
                                        }
                                        else
                                        {
                                            tempTicket.TicketDesignator = "0";//For Without Airline PrivateFare
                                        }
                                        if (price != null && price.FareInfo != null && price.FareInfo.Length > 0 && price.FareInfo[0].AccountCode != null && price.FareInfo[0].AccountCode.Length > 0)
                                        {
                                            tempTicket.StockType = price.FareInfo[0].AccountCode[0].Code;//Segmentwise Account Code
                                        }
                                        else
                                        {
                                            tempTicket.StockType = "1";//For Without Airline PrivateFare
                                        }
                                    }
                                    tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                    // Adding Tax Details for Pax

                                    if (taxBreakUpPax.ContainsKey(travelerRef))
                                    {
                                        tempTicket.TaxBreakup = new List<KeyValuePair<string, decimal>>();

                                        tempTicket.TaxBreakup = taxBreakUpPax[travelerRef];
                                    }

                                    // Adding PTCDetail 

                                    for (int k = 0; k < itinerary.Segments.Length; k++)
                                    {


                                        Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoSegmentTemp = fareInfoPaxtDict[ticketInfo.BookingTravelerRef];
                                        UAPIdll.Universal46.FareInfo tempFareInfo = fareInfoSegmentTemp[Convert.ToString(itinerary.Segments[k].UapiSegmentRefKey)];
                                        // Fare Rules details
                                        if (ticketIndex == 0)
                                        {
                                            FareRule fareRule = new FareRule();
                                            fareRule.FareBasisCode = tempFareInfo.FareBasis;
                                            fareRule.Origin = itinerary.Segments[k].Origin.AirportCode;
                                            fareRule.Destination = itinerary.Segments[k].Destination.AirportCode;
                                            fareRule.Airline = itinerary.Segments[k].Airline;
                                            fareRuleList.Add(fareRule);

                                        }
                                        // Baagege Details
                                        SegmentPTCDetail ptcDtl = new SegmentPTCDetail();
                                        if (tempFareInfo.BaggageAllowance != null)
                                        {
                                            if (tempFareInfo.BaggageAllowance.MaxWeight != null && !string.IsNullOrEmpty(tempFareInfo.BaggageAllowance.MaxWeight.Value) && tempFareInfo.BaggageAllowance.MaxWeight.Value.Length > 0 && tempFareInfo.BaggageAllowance.MaxWeight.Value != "0")
                                            {
                                                ptcDtl.Baggage = tempFareInfo.BaggageAllowance.MaxWeight.Value + "Kg";
                                            }
                                            else if (!string.IsNullOrEmpty(tempFareInfo.BaggageAllowance.NumberOfPieces) && tempFareInfo.BaggageAllowance.NumberOfPieces.Length > 0)
                                            {
                                                ptcDtl.Baggage = tempFareInfo.BaggageAllowance.NumberOfPieces + " piece";
                                            }
                                            else
                                            {
                                                ptcDtl.Baggage = "Airline Norms";
                                            }
                                        }
                                        else
                                        {
                                            ptcDtl.Baggage = "Airline Norms";
                                        }
                                        string flightKey = string.Empty;
                                        flightKey = itinerary.Segments[k].Airline;
                                        //if (itinerary.Segments[k].FlightNumber.Length == 3)
                                        //{
                                        //    flightKey += "0" + itinerary.Segments[k].FlightNumber;
                                        //}
                                        //else if (itinerary.Segments[k].FlightNumber.Length == 4)
                                        //{
                                        //    flightKey += itinerary.Segments[k].FlightNumber;
                                        //}
                                        flightKey += itinerary.Segments[k].FlightNumber.PadLeft(4, '0');
                                        flightKey += itinerary.Segments[k].DepartureTime.ToString("ddMMMyyyyHHmm").ToUpper();
                                        ptcDtl.FlightKey = flightKey;
                                        ptcDtl.NVA = string.Empty;
                                        ptcDtl.NVB = string.Empty;
                                        //ptcDtl.FareBasis = itinerary.FareRules[k].FareBasisCode;
                                        ptcDtl.FareBasis = tempFareInfo.FareBasis;
                                        tempTicket.PtcDetail.Add(ptcDtl);

                                        //ticket[paxIndex].PtcDetail.Add(ptcDtl);
                                    }

                                    //tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                    ticketArray.Add(tempTicket);
                                }
                                ticketIndex++;
                            }
                            ticket = ticketArray.ToArray();
                        }
                        else
                            ticket = new Ticket[0];
                    }


                    //if (airReservation.TicketingModifiers != null && airReservation.TicketingModifiers.Length > 0)
                    //{
                    //    UAPIdll.Universal46.TicketingModifiers[] ticketModifierList = airReservation.TicketingModifiers;
                    //    foreach (UAPIdll.Universal46.TicketingModifiers ticketModifier in ticketModifierList)
                    //    {
                    //        itinerary.Ticketed = ticketModifier.DocumentSelect.IssueElectronicTicket;
                    //    }
                    //}


                    if (fareRuleList.Count > 0) itinerary.FareRules = fareRuleList;
                }

            }
            // Adding Temp FLight Passenger List To Itinerary
            itinerary.Passenger = new FlightPassenger[tempPassengerList.Length];

            itinerary.Passenger = tempPassengerList;
            itinerary.AirLocatorCode = reservationLocatorCode;// store the air reservation code to issue the ticket
            // TO DO

            //Added by shiva 21 Sep 2015
            //If FormOfPayment is nor returned or if returned with Type other than "Cash" then add this node in AirTicketingModifiers request of Ticket request.
            //So set the following variable to "false". The same variable will be checked in 'GenerateETicketMessage' method of line 3012
            if (response.UniversalRecord.FormOfPayment == null || response.UniversalRecord.FormOfPayment.Length == 0 || (response.UniversalRecord.FormOfPayment.Length > 0 && response.UniversalRecord.FormOfPayment[0].Type != "Cash"))
            {
                formOfPaymentIsShown = false;
            }
            return itinerary;


        }

        /// <summary>
        /// This method is used to generate Tickets from the AirTicketingRsp and UniversalRecordImportRsp. Tickets will generated from AirTicketingRsp. UniversalRecordImportRsp will be used to update the Itinerary.
        /// </summary>
        /// <param name="response">UniversalRecordImportRsp object for updating Itinerary</param>
        /// <param name="airTicketingRsp">AirTicketingRsp object for generating Tickets</param>
        /// <param name="ticket">Generated Tickets for saving</param>
        /// <returns></returns>
        private FlightItinerary MakeBookingObject(UAPIdll.Universal46.UniversalRecordImportRsp response, AirTicketingRsp airTicketingRsp, out Ticket[] ticket)
        {
            ticket = new Ticket[0];
            FlightItinerary itinerary = new FlightItinerary();
            UAPIdll.Universal46.UniversalRecord uRecord = response.UniversalRecord;
            itinerary.UniversalRecord = uRecord.LocatorCode;
            itinerary.PNR = uRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test
            // Passenger Information 
            UAPIdll.Universal46.BookingTraveler[] bookingTravelerList = uRecord.BookingTraveler;

            Dictionary<string, FlightPassenger> passengerDict = new Dictionary<string, FlightPassenger>();// To Stor Passnger Details
            FlightPassenger[] tempPassengerList = new FlightPassenger[bookingTravelerList.Length];
            Dictionary<string, List<KeyValuePair<string, decimal>>> taxBreakUpPax = new Dictionary<string, List<KeyValuePair<string, decimal>>>(); // To Store Pax wise Tax Break Up
            int paxIndex = 0;
            foreach (UAPIdll.Universal46.BookingTraveler bookingTraveler in bookingTravelerList)
            {
                //bookingTraveler.TravelerType
                tempPassengerList[paxIndex] = new FlightPassenger();
                if (bookingTraveler.TravelerType == "ADT" || bookingTraveler.TravelerType == null)
                    tempPassengerList[paxIndex].Type = PassengerType.Adult;
                if (bookingTraveler.TravelerType == "CNN")
                    tempPassengerList[paxIndex].Type = PassengerType.Child;
                if (bookingTraveler.TravelerType == "INF")
                    tempPassengerList[paxIndex].Type = PassengerType.Infant;
                if (bookingTraveler.TravelerType == "SRC")
                    tempPassengerList[paxIndex].Type = PassengerType.Senior;

                UAPIdll.Universal46.BookingTravelerName travelerName = new UAPIdll.Universal46.BookingTravelerName();
                travelerName = bookingTraveler.BookingTravelerName;
                tempPassengerList[paxIndex].FirstName = travelerName.First;
                tempPassengerList[paxIndex].LastName = travelerName.Last;
                tempPassengerList[paxIndex].Title = travelerName.Prefix;
                tempPassengerList[paxIndex].DateOfBirth = Convert.ToDateTime(bookingTraveler.DOB);
                // for passport
                try
                {
                    UAPIdll.Universal46.SSR[] SSRList = bookingTraveler.SSR;
                    if (SSRList != null)
                    {
                        string passport = string.Empty;
                        foreach (UAPIdll.Universal46.SSR ssrPassport in SSRList)
                        {
                            if (ssrPassport.Type == "DOCS")
                            {
                                //string passport = "P/AE/00002345/AE/0000000/M/02FEB17/MHD/ZIYAD -1ZIYAD/MHDMR";
                                passport = ssrPassport.FreeText;
                                passport = passport.Substring(passport.IndexOf("/", 2) + 1, passport.Length - passport.IndexOf("/", 2) - 1);
                                passport = passport.Substring(0, passport.IndexOf("/", 1));
                                break;
                            }

                        }
                        tempPassengerList[paxIndex].PassportNo = passport;

                    }
                }
                catch { }
                if (bookingTraveler.Gender == "M")
                    tempPassengerList[paxIndex].Gender = Gender.Male;
                else if (bookingTraveler.Gender == "F")
                    tempPassengerList[paxIndex].Gender = Gender.Female;



                UAPIdll.Universal46.PhoneNumber[] phone = bookingTraveler.PhoneNumber;
                if (phone != null)
                {
                    tempPassengerList[paxIndex].CellPhone = phone[0].Number;
                }
                UAPIdll.Universal46.Email[] mail = bookingTraveler.Email;
                if (mail != null)
                {
                    tempPassengerList[paxIndex].Email = mail[0].EmailID;
                }
                UAPIdll.Universal46.typeStructuredAddress[] adds = bookingTraveler.Address;
                if (adds != null)
                {
                    tempPassengerList[paxIndex].AddressLine1 = adds[0].AddressName;
                    if (adds[0].Street != null)
                    {
                        string[] street = adds[0].Street;
                        tempPassengerList[paxIndex].AddressLine2 = street[0].ToString();
                    }

                    if (adds[0].City != null) tempPassengerList[paxIndex].City = adds[0].City;
                    if (adds[0].Country != null)
                    {
                        Country tempCountry = new Country();
                        tempCountry.CountryCode = adds[0].Country;
                        tempPassengerList[paxIndex].Country = tempCountry;
                    }
                }
                passengerDict.Add((bookingTraveler.Key), tempPassengerList[paxIndex]);
                paxIndex++;

                // Segment Information


            }
            // Air Reservation Details
            string reservationLocatorCode = string.Empty;
            UAPIdll.Universal46.AirReservation[] airReservationList = response.UniversalRecord.Items;
            if (airReservationList != null)
            {
                if (airReservationList[0].AirSegment == null)
                {
                    throw new BookingEngineException("Segment details missing for this PNR!");
                }
                Dictionary<string, Dictionary<string, UAPIdll.Universal46.FareInfo>> fareInfoPaxtDict = new Dictionary<string, Dictionary<string, UAPIdll.Universal46.FareInfo>>();
                foreach (UAPIdll.Universal46.AirReservation airReservation in airReservationList)
                {
                    reservationLocatorCode = airReservation.LocatorCode;// storing the PNR for issue the ticket for UAPI-TO do with multiple Airline Test
                    itinerary.CreatedOn = Convert.ToDateTime(airReservation.CreateDate);
                    //itinerary.Ticketed = true; TO check after Ticket
                    itinerary.FareType = "PUB";
                    itinerary.FlightBookingSource = BookingSource.UAPI;
                    itinerary.Segments = new FlightInfo[airReservation.AirSegment.Length];
                    itinerary.ProductTypeId = 1;
                    itinerary.BookingMode = BookingMode.Auto;//To Avoid Serialization failure;
                    //itinerary.PaymentMode = ModeOfPayment.Credit;//To Avoid Serialization failure;
                    itinerary.ProductType = ProductType.Flight;//To Avoid Serialization failure;

                    //Create segments for Connection flights also
                    int segCount = 0;
                    foreach (UAPIdll.Universal46.typeBaseAirSegment tempAirSegment in airReservation.AirSegment)
                    {
                        //Sum all segment flight details
                        segCount += tempAirSegment.FlightDetails.Length;
                    }
                    itinerary.Segments = new FlightInfo[segCount];

                    // Air Segment Details
                    int airSegIndex = 0;
                    foreach (UAPIdll.Universal46.typeBaseAirSegment tempAirSegment in airReservation.AirSegment)
                    {
                        foreach (UAPIdll.Universal46.FlightDetails fd in tempAirSegment.FlightDetails)
                        {
                            itinerary.Segments[airSegIndex] = new FlightInfo();
                            itinerary.Segments[airSegIndex].Status = tempAirSegment.Status;
                            if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                                itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                            else
                                itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Waitlisted;
                            itinerary.Segments[airSegIndex].Airline = tempAirSegment.Carrier;
                            itinerary.Segments[airSegIndex].Origin = new Airport(fd.Origin);
                            //itinerary.Segments[airSegIndex].Origin.AirportCode= tempAirSegment.Origin;
                            itinerary.Segments[airSegIndex].Destination = new Airport(fd.Destination);
                            //itinerary.Segments[airSegIndex].Destination.AirportCode = tempAirSegment.Destination;

                            // Local Time
                            string GMTdiff = fd.DepartureTime.IndexOf("+") != -1 ? "+" : "-";
                            string depTime = fd.DepartureTime.Substring(0, fd.DepartureTime.LastIndexOf(GMTdiff));
                            GMTdiff = fd.ArrivalTime.IndexOf("+") != -1 ? "+" : "-";
                            string arrTime = fd.ArrivalTime.Substring(0, fd.ArrivalTime.LastIndexOf(GMTdiff));


                            itinerary.Segments[airSegIndex].DepartureTime = Convert.ToDateTime(depTime);
                            itinerary.Segments[airSegIndex].ArrivalTime = Convert.ToDateTime(arrTime);
                            // GMT Time
                            //itinerary.Segments[airSegIndex].ArrivalTime= Convert.ToDateTime(tempAirSegment.ArrivalTime);
                            //itinerary.Segments[airSegIndex].DepartureTime= Convert.ToDateTime(tempAirSegment.DepartureTime);
                            itinerary.Segments[airSegIndex].BookingClass = tempAirSegment.ClassOfService;
                            itinerary.Segments[airSegIndex].CabinClass = tempAirSegment.ClassOfService;
                            itinerary.Segments[airSegIndex].FlightNumber = tempAirSegment.FlightNumber;
                            itinerary.Segments[airSegIndex].UapiSegmentRefKey = tempAirSegment.Key;//Convert.ToInt32(tempAirSegment.Key);
                            itinerary.Segments[airSegIndex].AirlinePNR = airReservation.LocatorCode;
                            itinerary.Segments[airSegIndex].Group = tempAirSegment.Group;
                            //Update Terminal info                           
                            try
                            {
                                if (!string.IsNullOrEmpty(fd.OriginTerminal))
                                    itinerary.Segments[airSegIndex].DepTerminal = fd.OriginTerminal;
                                else
                                {
                                    string sSellMsg = tempAirSegment.SellMessage.ToList().Find(x => x.Contains("TERMINAL"));
                                    if (!string.IsNullOrEmpty(sSellMsg))
                                    {
                                        sSellMsg = sSellMsg.Replace("DEPARTS " + tempAirSegment.Origin + " TERMINAL ", "");
                                        sSellMsg = sSellMsg.Replace("ARRIVES " + tempAirSegment.Destination + " TERMINAL ", "");
                                        itinerary.Segments[airSegIndex].DepTerminal = sSellMsg.Split('-').Length > 0 ? sSellMsg.Split('-')[0] : "";
                                    }
                                }
                                if (!string.IsNullOrEmpty(fd.DestinationTerminal))
                                    itinerary.Segments[airSegIndex].ArrTerminal = fd.DestinationTerminal;
                                else
                                {
                                    string sSellMsg = tempAirSegment.SellMessage.ToList().Find(x => x.Contains("TERMINAL"));
                                    if (!string.IsNullOrEmpty(sSellMsg))
                                    {
                                        sSellMsg = sSellMsg.Replace("DEPARTS " + tempAirSegment.Origin + " TERMINAL ", "");
                                        sSellMsg = sSellMsg.Replace("ARRIVES " + tempAirSegment.Destination + " TERMINAL ", "");
                                        itinerary.Segments[airSegIndex].ArrTerminal = sSellMsg.Split('-').Length > 1 ? sSellMsg.Split('-')[1] : "";
                                    }
                                }
                            }
                            catch { }
                            //itinerary.Segments[airSegIndex].ArrTerminal = fd.DestinationTerminal;
                            //itinerary.Segments[airSegIndex].DepTerminal = fd.OriginTerminal;
                            int duration = Convert.ToInt32(fd.FlightTime);
                            itinerary.Segments[airSegIndex].Duration = new TimeSpan(0, duration, 0);
                            UAPIdll.Universal46.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                            if (supLocatorList != null)
                            {
                                foreach (UAPIdll.Universal46.SupplierLocator supLocator in supLocatorList)
                                {

                                    if (tempAirSegment.Carrier == supLocator.SupplierCode)
                                    {
                                        itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                    }
                                }
                            }



                            //UAPIdll.Universal46.typeEticketability ticketability = new UAPIdll.Universal46.typeEticketability();
                            //ticketability = tempAirSegment.ETicketability;
                            if (tempAirSegment.ETicketability == UAPIdll.Universal46.typeEticketability.Yes)
                                itinerary.Segments[airSegIndex].ETicketEligible = true;
                            else itinerary.Segments[airSegIndex].ETicketEligible = false;


                            itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                            airSegIndex++;
                        }
                    }


                    // Air Pricing Info Details
                    bool nonRefundable = false;
                    //double totalResultFare = 0;
                    //double totalbaseFare = 0;
                    string lastTktDate = string.Empty;
                    //pricingSolution.Journey[0].
                    //int fareKey = 0;
                    decimal baseFare = 0;
                    decimal totalFare = 0;




                    //fare = new Fare[pricingSolution.AirPricingInfo.Length];

                    // Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                    //int infoIndex = 0;

                    if (airReservation.AirPricingInfo == null)
                    {
                        throw new BookingEngineException("Pricing Info is not assigned to this PNR!");
                    }
                    foreach (UAPIdll.Universal46.AirPricingInfo pricingInfo in airReservation.AirPricingInfo) //AirPricingInfo
                    {
                        string currency = getCurrency(pricingInfo.TotalPrice);

                        if (ExchangeRates.ContainsKey(currency))
                        {
                            rateOfExchange = Convert.ToDouble(ExchangeRates[currency]);
                        }
                        else
                        {
                            rateOfExchange = 1;
                        }
                        baseFare = (decimal)getCurrAmount(pricingInfo.ApproximateBasePrice) * Convert.ToDecimal(rateOfExchange);
                        totalFare = (decimal)getCurrAmount(pricingInfo.TotalPrice) * Convert.ToDecimal(rateOfExchange);
                        lastTktDate = pricingInfo.LatestTicketingTime;
                        nonRefundable = !pricingInfo.Refundable;
                        //Fare Info Details
                        Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoDict = new Dictionary<string, UAPIdll.Universal46.FareInfo>();
                        Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoSegmentDict = new Dictionary<string, UAPIdll.Universal46.FareInfo>();

                        foreach (UAPIdll.Universal46.FareInfo tempFareInfo in pricingInfo.FareInfo)
                        {
                            fareInfoDict.Add(tempFareInfo.Key, tempFareInfo);

                        }

                        foreach (UAPIdll.Universal46.BookingInfo tempBookingInfo in pricingInfo.BookingInfo)
                        {
                            fareInfoSegmentDict.Add(tempBookingInfo.SegmentRef, fareInfoDict[tempBookingInfo.FareInfoRef]);
                        }


                        UAPIdll.Universal46.PassengerType[] tempPassTypeList = pricingInfo.PassengerType;
                        foreach (UAPIdll.Universal46.PassengerType tempPassType in tempPassTypeList)
                        {


                            fareInfoPaxtDict.Add(tempPassType.BookingTravelerRef, fareInfoSegmentDict);
                            // Price Details

                            PriceAccounts price = new PriceAccounts();
                            price.PublishedFare = baseFare;
                            price.Tax = totalFare - baseFare;
                            /*=========================Price details updated by shiva=========================*/
                            price.RateOfExchange = (decimal)rateOfExchange;
                            price.Currency = agentBaseCurrency;
                            price.CurrencyCode = agentBaseCurrency;
                            price.SupplierPrice = (decimal)getCurrAmount(pricingInfo.TotalPrice);
                            price.SupplierCurrency = pricingInfo.BasePrice.Substring(0, 3);
                            price.AccPriceType = PriceType.PublishedFare;//To Avoid Serialization failure;                            
                            /*=========================Price details=========================*/
                            FlightPassenger tempPassenger = passengerDict[(tempPassType.BookingTravelerRef)];
                            tempPassenger.Price = price;
                            // For Tax Break Up -- not yet implementd( only after Ticket)
                            List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
                            UAPIdll.Universal46.typeTaxInfo[] tempTaxTypeList = pricingInfo.TaxInfo;
                            if (tempTaxTypeList != null && tempTaxTypeList.Length > 0)
                            {
                                foreach (UAPIdll.Universal46.typeTaxInfo tempTaxType in tempTaxTypeList)
                                {
                                    string taxType = tempTaxType.Category;
                                    decimal taxValue = (decimal)getCurrAmount(tempTaxType.Amount);
                                    taxBreakUp.Add(new KeyValuePair<string, decimal>(taxType, (decimal)((double)taxValue * rateOfExchange)));
                                }
                                taxBreakUpPax.Add((tempPassType.BookingTravelerRef), taxBreakUp);
                            }


                        }

                    }// Air Pricing Info End

                }

                // Checking whether ticketed or not
                List<FareRule> fareRuleList = new List<FareRule>();
                if (airTicketingRsp != null && airTicketingRsp.Items != null && airTicketingRsp.Items.Length > 0)
                {
                    //Assign tickets length
                    ticket = new Ticket[airTicketingRsp.Items.Length];
                    List<Ticket> tickets = new List<Ticket>();
                    List<Ticket> ticketArray = new List<Ticket>();
                    int ticketIndex = 0;
                    //loop thru ETickets for each pax
                    foreach (object etr in airTicketingRsp.Items)
                    {
                        ETR eTicket = etr as ETR;

                        if (eTicket.Ticket != null && eTicket.Ticket.Length > 0)
                        {
                            UAPIdll.Air46.Ticket ticketInfo = eTicket.Ticket[0];
                            if (!string.IsNullOrEmpty(ticketInfo.TicketNumber)) // TODO replace Ticket Number to Status, once we get status Descritption
                            {
                                string travelerRef = (eTicket.BookingTraveler.Key);
                                Ticket tempTicket = new Ticket();
                                tempTicket.TicketNumber = ticketInfo.TicketNumber;

                                itinerary.Ticketed = true;

                                //Assigning Pax Details
                                FlightPassenger tempPassenger = passengerDict[travelerRef];
                                //tempPassenger.p
                                tempTicket.PaxFirstName = tempPassenger.FirstName;
                                tempTicket.PaxLastName = tempPassenger.LastName;
                                tempTicket.PaxType = tempPassenger.Type;
                                tempTicket.Title = tempPassenger.Title;
                                tempTicket.ETicket = true;
                                tempTicket.IssueDate = DateTime.UtcNow;
                                tempTicket.LastModifiedOn = itinerary.LastModifiedOn;
                                //Assign TourCode, UAPI AccountCode and MasterAccountCode to Ticket for saving in DB
                                if (airReservationList[0].AirPricingInfo != null && airReservationList[0].AirPricingInfo[0].PricingMethod == UAPIdll.Universal46.typePricingMethod.GuaranteedUsingAirlinePrivateFare)
                                {
                                    if (airReservationList[0].TicketingModifiers != null && airReservationList[0].TicketingModifiers.Length > 0 && airReservationList[0].TicketingModifiers[0].TourCode != null)
                                    {
                                        //Save Tour Code, TicketDesignator & AccountCode
                                        tempTicket.TourCode = airReservationList[0].TicketingModifiers[0].TourCode.Value;//Tour Code
                                    }
                                    else
                                    {
                                        tempTicket.TourCode = "1";// For Without Airline PrivateFare
                                    }
                                    List<UAPIdll.Universal46.AirPricingInfo> pricingInfo = new List<UAPIdll.Universal46.AirPricingInfo>();
                                    pricingInfo.AddRange(airReservationList[0].AirPricingInfo);
                                    UAPIdll.Universal46.AirPricingInfo price = pricingInfo.Find(delegate (UAPIdll.Universal46.AirPricingInfo p) { return p.Key == eTicket.AirPricingInfo.Key; });

                                    if (price != null && price.AirPricingModifiers != null && price.AirPricingModifiers.AccountCodes != null && price.AirPricingModifiers.AccountCodes.Length > 0)
                                    {
                                        tempTicket.TicketDesignator = price.AirPricingModifiers.AccountCodes[0].Code;//Our Account Code                                            
                                    }
                                    else
                                    {
                                        tempTicket.TicketDesignator = "0";//For Without Airline PrivateFare
                                    }
                                    if (price != null && price.FareInfo != null && price.FareInfo.Length > 0 && price.FareInfo[0].AccountCode != null && price.FareInfo[0].AccountCode.Length > 0)
                                    {
                                        tempTicket.StockType = price.FareInfo[0].AccountCode[0].Code;//Segmentwise Account Code
                                    }
                                    else
                                    {
                                        tempTicket.StockType = "1";//For Without Airline PrivateFare
                                    }
                                }
                                tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                // Adding Tax Details for Pax

                                if (taxBreakUpPax.ContainsKey(travelerRef))
                                {
                                    tempTicket.TaxBreakup = new List<KeyValuePair<string, decimal>>();
                                    tempTicket.TaxBreakup = taxBreakUpPax[travelerRef];
                                }

                                // Adding PTCDetail 
                                for (int k = 0; k < itinerary.Segments.Length; k++)
                                {
                                    Dictionary<string, UAPIdll.Universal46.FareInfo> fareInfoSegmentTemp = fareInfoPaxtDict[eTicket.BookingTraveler.Key];
                                    UAPIdll.Universal46.FareInfo tempFareInfo = fareInfoSegmentTemp[Convert.ToString(itinerary.Segments[k].UapiSegmentRefKey)];
                                    // Fare Rules details
                                    if (ticketIndex == 0)
                                    {
                                        FareRule fareRule = new FareRule();
                                        fareRule.FareBasisCode = tempFareInfo.FareBasis;
                                        fareRule.Origin = itinerary.Segments[k].Origin.AirportCode;
                                        fareRule.Destination = itinerary.Segments[k].Destination.AirportCode;
                                        fareRule.Airline = itinerary.Segments[k].Airline;
                                        fareRuleList.Add(fareRule);
                                    }
                                    // Baagege Details
                                    SegmentPTCDetail ptcDtl = new SegmentPTCDetail();
                                    if (tempFareInfo.BaggageAllowance != null)
                                    {
                                        if (tempFareInfo.BaggageAllowance.MaxWeight != null && !string.IsNullOrEmpty(tempFareInfo.BaggageAllowance.MaxWeight.Value) && tempFareInfo.BaggageAllowance.MaxWeight.Value.Length > 0 && tempFareInfo.BaggageAllowance.MaxWeight.Value != "0")
                                        {
                                            ptcDtl.Baggage = tempFareInfo.BaggageAllowance.MaxWeight.Value + "Kg";
                                        }
                                        else if (!string.IsNullOrEmpty(tempFareInfo.BaggageAllowance.NumberOfPieces) && tempFareInfo.BaggageAllowance.NumberOfPieces.Length > 0)
                                        {
                                            ptcDtl.Baggage = tempFareInfo.BaggageAllowance.NumberOfPieces + " piece";
                                        }
                                        else
                                        {
                                            ptcDtl.Baggage = "Airline Norms";
                                        }
                                    }
                                    else
                                    {
                                        ptcDtl.Baggage = "Airline Norms";
                                    }
                                    string flightKey = string.Empty;
                                    flightKey = itinerary.Segments[k].Airline;

                                    flightKey += itinerary.Segments[k].FlightNumber.PadLeft(4, '0');
                                    flightKey += itinerary.Segments[k].DepartureTime.ToString("ddMMMyyyyHHmm").ToUpper();
                                    ptcDtl.FlightKey = flightKey;
                                    ptcDtl.NVA = string.Empty;
                                    ptcDtl.NVB = string.Empty;

                                    ptcDtl.FareBasis = tempFareInfo.FareBasis;
                                    tempTicket.PtcDetail.Add(ptcDtl);
                                }

                                tickets.Add(tempTicket);
                            }
                            ticketIndex++;
                        }
                    }
                    ticket = tickets.ToArray();
                }
                else
                    ticket = new Ticket[0];

                if (fareRuleList.Count > 0) itinerary.FareRules = fareRuleList;
            }
            // Adding Temp FLight Passenger List To Itinerary
            itinerary.Passenger = new FlightPassenger[tempPassengerList.Length];

            itinerary.Passenger = tempPassengerList;
            itinerary.AirLocatorCode = reservationLocatorCode;// store the air reservation code to issue the ticket
            // TO DO

            //Added by shiva 21 Sep 2015
            //If FormOfPayment is not returned or if returned with Type other than "Cash" then add this node in AirTicketingModifiers request of Ticket request.
            //So set the following variable to "false". The same variable will be checked in 'GenerateETicketMessage' method of line 3012
            if (response.UniversalRecord.FormOfPayment == null || response.UniversalRecord.FormOfPayment.Length == 0 || (response.UniversalRecord.FormOfPayment.Length > 0 && response.UniversalRecord.FormOfPayment[0].Type != "Cash"))
            {
                formOfPaymentIsShown = false;
            }
            return itinerary;


        }
        # endregion

        # region Ticket
        public TicketingResponse Ticket(FlightItinerary itineraryDB, int member, Dictionary<string, string> ticketData, string ipAddr, UAPICredentials _uapiCredentials, UAPIdll.Universal46.UniversalRecordImportRsp URimportResponse, out Ticket[] tickets)
        {
            TicketingResponse response = new TicketingResponse();
            bool isDomestic = itineraryDB.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
            //bool isYatra = false;
            //bool isVia = false;
            //Connection con = new Connection();
            string hap = string.Empty;
            string username = string.Empty;
            string password = string.Empty;
            if (isDomestic)
            {
                hap = Configuration.ConfigurationSystem.GalileoConfig["DomesticHAP"].ToString();
            }
            else
            {
                hap = Configuration.ConfigurationSystem.GalileoConfig["InternationalHAP"].ToString();
            }
            //AirlineHAP airHap = new AirlineHAP();
            //airHap.Load(itineraryDB.ValidatingAirline, "1G");
            //if (isDomestic)
            //{
            //    //if (airHap.DomesticHAP != null && airHap.DomesticHAP.Length > 0)
            //    //{
            //    //    string supName = Supplier.GetSupplierNameById(airHap.SupplierId);
            //    //    if (supName.ToUpper().Contains("YATRA"))
            //    //    {
            //    //        isYatra = true;
            //    //    }
            //    //    else
            //    //    {
            //    //        if (supName.ToUpper().Contains("VIA"))
            //    //        {
            //    //            isVia = true;
            //    //            username = Configuration.ConfigurationSystem.GalileoConfig["ViaUsername"].ToString();
            //    //            password = Configuration.ConfigurationSystem.GalileoConfig["ViaPassword"].ToString();
            //    //        }
            //    //    }
            //    //    hap = airHap.DomesticHAP;
            //    //    string rtResponse = con.SubmitTerminalTransaction("*" + itineraryDB.PNR);
            //    //    rtResponse = con.SubmitTerminalTransaction("QEB/" + airHap.DomesticPCC + "/66+67");
            //    //    con.EndSession();
            //    //    Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Queueing XML 1G. req : QEB/" + airHap.DomesticPCC + "/66+67" + rtResponse, string.Empty);
            //    //}
            //}
            //else
            //{
            //    if (airHap.InternationalHAP != null && airHap.InternationalHAP.Length > 0)
            //    {
            //        //string supName = Supplier.GetSupplierNameById(airHap.SupplierId);
            //        //if (supName.ToUpper().Contains("YATRA"))
            //        //{
            //        //    isYatra = true;
            //        //}
            //        //hap = airHap.InternationalHAP;
            //        //string rtResponse = con.SubmitTerminalTransaction("*" + itineraryDB.PNR);
            //        //rtResponse = con.SubmitTerminalTransaction("QEB/" + airHap.InternationalPCC + "/66+67");
            //        //con.EndSession();
            //        //Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Queueing XML 1G. req : QEB/" + airHap.InternationalPCC + "/66+67" + rtResponse, string.Empty);
            //    }
            //}


            string endorsement = string.Empty;
            string tourCode = string.Empty;
            string corporateCode = string.Empty;
            if (ticketData != null && ticketData.ContainsKey("corporateCode") && ticketData["corporateCode"].Length > 0)
            {
                corporateCode = ticketData["corporateCode"];
            }
            if (itineraryDB.Endorsement != null && itineraryDB.Endorsement.Length > 0)
            {
                endorsement = itineraryDB.Endorsement;
            }
            else if (ticketData != null && ticketData.ContainsKey("endorsement") && ticketData["endorsement"].Length > 0)
            {
                endorsement = ticketData["endorsement"];
            }
            if (corporateCode != null && corporateCode.Trim().Length > 0)
            {
                endorsement += " " + corporateCode.Trim();
            }
            if (itineraryDB.TourCode != null && itineraryDB.TourCode.Length > 0)
            {
                tourCode = itineraryDB.TourCode;
            }
            else if (ticketData != null && ticketData.ContainsKey("tourCode") && ticketData["tourCode"].Length > 0)
            {
                tourCode = ticketData["tourCode"];
            }
            //try //RetrieveItinerary is  Already calling in Metasearch.TicketUAPI.
            //{
            //    Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Retrieve Before Ticket XML UAPI. ", string.Empty);
            //    FlightItinerary retrievItinerary = RetrieveItinerary(itineraryDB.UniversalRecord);
            //}
            //catch
            //{
            //    Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Retrieve PNR Failed before Ticketing . Execption. ", string.Empty);
            //    throw;
            //}


            //UAPIdll.Air46.ResponseMessage responseMessage = null;
            string resMessageText = string.Empty;
            try
            {


                //Air20.AirTicketingRsp ticketingRsp = GenerateETicketMessage(itineraryDB.PNR);
                UAPIdll.Air46.AirTicketingRsp ticketingRsp = GenerateETicketMessage(itineraryDB.AirLocatorCode, _uapiCredentials);// in UAPI to issue the ticket, should pass the airreservationlocatorcode

                //If ResponseMessage count is 1 then Items will be TicketFailureInfo 
                //otherwise Items will be ETR. For TicketFailureInfo we need to check in ZERO index
                //as per schema v33.0 because only single TicketFailureIno node will be there in case of error.
                if (ticketingRsp.Items != null && ticketingRsp.Items.Length > 0 && ticketingRsp.ResponseMessage.Length <= 1)
                {
                    response.PNR = itineraryDB.PNR;
                    response.Status = TicketingResponseStatus.OtherError;

                    string erroMessage = (ticketingRsp.Items[0] as TicketFailureInfo).Message;
                    response.Message = erroMessage;
                    throw new Exception(erroMessage);
                    //Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Ticketing Failed. Execption :- " + erroMessage, string.Empty);

                }
                else
                {
                    //Generate ETickets from the URImport Response and Ticketing Response
                    itineraryDB = MakeBookingObject(URimportResponse, ticketingRsp, out tickets);
                }
                if (ticketingRsp.ResponseMessage[0] != null)
                {
                    foreach (ResponseMessage msg in ticketingRsp.ResponseMessage)
                    {
                        resMessageText += msg.Type.ToString() + ":" + msg.Value + ",";
                    }
                }

                // responseMessage = ticketingRsp.ResponseMessage[0];
                //eticketResponse = connection.SubmitXmlOnSession(request);
                // Audit.Add(EventType.Ticketing, Severity.Normal, Convert.ToInt32(AppUserId), "UAPI Ticket Response Message: " + resMessageText, string.Empty);
            }
            catch (SoapException se)
            {
                Audit.Add(EventType.Ticketing, Severity.High, Convert.ToInt32(AppUserId), "UAPI Ticketing Failed. Execption :- " + se.ToString(), string.Empty);
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Ticketing, Severity.High, Convert.ToInt32(AppUserId), "UAPI Ticketing Failed. Execption :- " + ex.ToString(), string.Empty);
                throw ex;//for writing in FailedBooking and sending email
            }
            string ticketSuccessPattern = "ELECTRONIC TKT GENERATED";
            //Match match = Regex.Match(responseMessage.Value, ticketSuccessPattern);
            Match match = Regex.Match((!string.IsNullOrEmpty(resMessageText)) ? resMessageText : string.Empty, ticketSuccessPattern);
            if (match.Success)
            {
                response.Status = TicketingResponseStatus.Successful;
                response.PNR = itineraryDB.PNR;
            }
            else
            {
                response.Status = TicketingResponseStatus.NotCreated;
                //response.Message = "Ticketing Failed";                
            }
            return response;
        }

        private UAPIdll.Air46.AirTicketingRsp GenerateETicketMessage(string pnr, UAPICredentials _uapiCredentials)
        {
            Connection();
            //Trace.TraceInformation("UApi.GenerateETicket entered");

            UAPIdll.Air46.AirTicketingReq request = new UAPIdll.Air46.AirTicketingReq();

            //request.TargetBranch = TargetBranch;
            request.TargetBranch = _uapiCredentials.TargetBranch;// Mod By ziya for targetbranch clash

            request.ReturnInfoOnFail = true;
            request.BulkTicket = false;
            request.AuthorizedBy = "ONLINE";

            UAPIdll.Air46.BillingPointOfSaleInfo pos = new UAPIdll.Air46.BillingPointOfSaleInfo();
            pos.OriginApplication = "UAPI";
            request.BillingPointOfSaleInfo = pos;


            UAPIdll.Air46.AirReservationLocatorCode locatorCode = new UAPIdll.Air46.AirReservationLocatorCode();
            locatorCode.Value = pnr;

            request.AirReservationLocatorCode = locatorCode;

            //if (airReservation.TicketingModifiers != null && airReservation.TicketingModifiers.Length > 0)
            //{
            //    UAPIdll.Universal46.TicketingModifiers[] ticketModifierList = airReservation.TicketingModifiers;
            //    foreach (UAPIdll.Universal46.TicketingModifiers ticketModifier in ticketModifierList)
            //    {
            //        itinerary.Ticketed = ticketModifier.DocumentSelect.IssueElectronicTicket;
            //    }
            //}
            UAPIdll.Air46.AirTicketingModifiers[] ticketModifierList = new AirTicketingModifiers[1];
            ticketModifierList[0] = new AirTicketingModifiers();
            //If FormOfPayment node is missing from Import UR or FormOfPayment, Type is other than "Cash" 
            //then add FormOfPayment here in AirTicketingModifiers to get the Ticket. Added by shiva - 21 Sep 2015
            if (!formOfPaymentIsShown)
            {
                ticketModifierList[0].FormOfPayment = new FormOfPayment[1];
                ticketModifierList[0].FormOfPayment[0] = new FormOfPayment();
                ticketModifierList[0].FormOfPayment[0].Key = "123";
                ticketModifierList[0].FormOfPayment[0].Type = "Cash";
            }
            UAPIdll.Air46.DocumentModifiers docModifier = new DocumentModifiers();
            //docModifier=new DocumentModifiers();
            //docModifier.GenerateAccountingInterface = true;
            docModifier.GenerateAccountingInterface = false;// Switching Off MIR File
            ticketModifierList[0].DocumentModifiers = docModifier;
            request.AirTicketingModifiers = ticketModifierList;

            //ticketModifierList[0].
            //request.AirTicketingModifiers


            UAPIdll.Air46.AirTicketingBinding binding = new UAPIdll.Air46.AirTicketingBinding();
            binding.Url = urlAir;
            //binding.Credentials = new NetworkCredential(UserName, Password);
            binding.Credentials = new NetworkCredential(_uapiCredentials.UserName, _uapiCredentials.Password);// Mod by ziya for credentials clash
            binding.PreAuthenticate = true;//to implement GZIP

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AirTicketingReq));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirTicketingReq.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightTicketingRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            catch { }
            UAPIdll.Air46.AirTicketingRsp response = null;
            try
            {
                response = binding.service(request);
                //Static xml loading
                //System.Xml.Serialization.XmlSerializer serLoc = new System.Xml.Serialization.XmlSerializer(typeof(AirTicketingRsp));
                //string staticpath = ConfigurationManager.AppSettings["TestStaticXmlLoadPath"];
                //System.IO.StreamReader writerLoc = new StreamReader(@staticpath + "FlightTicketingResponse_6f1bdba7-abbc-413d-8eaf-233aee464c39_4008_20062018_070806.xml");
                //response = serLoc.Deserialize(writerLoc) as AirTicketingRsp;
                //writerLoc.Close();
            }
            catch { throw; }
            //{
            //    throw ex;
            //}

            try
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(AirTicketingRsp));
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                //docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirTicketingRes.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightTicketingResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docres.Save(filePath);
            }
            catch { }
            ////Trace.TraceInformation("UApi.GenerateRetrievePNRObject exiting");
            return response;
        }
        # endregion

        # region Import UR 
        public UAPIdll.Universal46.UniversalRecordImportRsp ImportUR(string pnr, UAPICredentials _uapiCredentials)
        {
            Connection();
            //Trace.TraceInformation("UApi.ImportUR entered");
            //UAPIdll.Universal46.UniversalRecordRetrieveReq request = new UAPIdll.Universal46.UniversalRecordRetrieveReq();

            //request.TargetBranch = targetBranch;
            //request.UniversalRecordLocatorCode = pnr;

            //UAPIdll.Universal46.BillingPointOfSaleInfo pos = new UAPIdll.Universal46.BillingPointOfSaleInfo();
            //pos.OriginApplication = originalApplication;
            //request.BillingPointOfSaleInfo = pos;

            //UAPIdll.Universal46.UniversalRecordRetrieveServiceBinding binding = new UAPIdll.Universal46.UniversalRecordRetrieveServiceBinding();
            //binding.Url = urlUR;
            //binding.Credentials = new NetworkCredential(userName, password);


            UAPIdll.Universal46.UniversalRecordImportReq request = new UAPIdll.Universal46.UniversalRecordImportReq();

            //request.TargetBranch = TargetBranch;
            request.TargetBranch = _uapiCredentials.TargetBranch;// MOd by ziya uapi credntials clash

            //request.UniversalRecordLocatorCode = pnr;

            request.ProviderCode = "1G";// TODO-- add Dynamic Provider
            request.ProviderLocatorCode = pnr;


            UAPIdll.Universal46.BillingPointOfSaleInfo pos = new UAPIdll.Universal46.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            request.BillingPointOfSaleInfo = pos;

            UAPIdll.Universal46.UniversalRecordImportServiceBinding binding = new UAPIdll.Universal46.UniversalRecordImportServiceBinding();
            binding.Url = urlUR;
            //binding.Credentials = new NetworkCredential(UserName, Password);
            binding.Credentials = new NetworkCredential(_uapiCredentials.UserName, _uapiCredentials.Password);// MOd by ziya uapi credntials clash
            binding.PreAuthenticate = true;//to implement GZIP


            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Universal46.UniversalRecordImportReq));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URImportReq.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightURImportRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            catch { }
            UAPIdll.Universal46.UniversalRecordImportRsp response = null;

            try
            {
                response = binding.service(request);
                //Load static xml
                //System.Xml.Serialization.XmlSerializer serLoc = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Universal46.UniversalRecordImportRsp));
                //string staticpath = ConfigurationManager.AppSettings["TestStaticXmlLoadPath"];
                //System.IO.StreamReader writerLoc = new StreamReader(@staticpath + "FlightURImportResponse_6f1bdba7-abbc-413d-8eaf-233aee464c39_4008_20062018_070758.xml");
                //response = serLoc.Deserialize(writerLoc) as UAPIdll.Universal46.UniversalRecordImportRsp;
                //writerLoc.Close();
            }
            catch (SoapException se)
            {
                throw se;
            }
            catch (Exception ex)
            { throw ex; }
            //{
            //    throw ex;
            //}

            try
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Universal46.UniversalRecordImportRsp));
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                //docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URimportRes.xml");
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightURImportResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docres.Save(filePath);
            }
            catch { }
            ////Trace.TraceInformation("UApi.ImportUR exiting");
            return response;
        }
        # endregion

        #region Void Ticket

        public void VoidTicket(string airLocatorCode, string PNR, string universalRecord)
        {
            FlightItinerary itinerary = new FlightItinerary(FlightItinerary.GetFlightId(PNR));

            AirVoidDocumentReq voidReq = new AirVoidDocumentReq();
            voidReq.AirReservationLocatorCode = new AirReservationLocatorCode();
            voidReq.AirReservationLocatorCode.Value = airLocatorCode;
            voidReq.BillingPointOfSaleInfo = new BillingPointOfSaleInfo();
            voidReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
            voidReq.ProviderCode = "1G";
            voidReq.ProviderLocatorCode = PNR;
            voidReq.ShowETR = true;
            voidReq.TargetBranch = TargetBranch;

            try
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Air46.AirVoidDocumentReq));
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, voidReq); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightVoidTicketRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docres.Save(filePath);
            }
            catch { }

            AirVoidDocumentBinding binding = new AirVoidDocumentBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(UserName, Password);
            binding.PreAuthenticate = true;
            AirVoidDocumentRsp voidResp = binding.service(voidReq);

            try
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Air46.AirVoidDocumentRsp));
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, voidResp); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightVoidTicketResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docres.Save(filePath);
            }
            catch { }

            if (voidResp.VoidResultInfo[0].ResultType == "Success")
            {
                try
                {
                    UAPIdll.Universal46.UniversalRecordCancelReq urCancelReq = new UAPIdll.Universal46.UniversalRecordCancelReq();
                    urCancelReq.BillingPointOfSaleInfo = new UAPIdll.Universal46.BillingPointOfSaleInfo();
                    urCancelReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                    urCancelReq.RetrieveProviderReservationDetails = true;
                    urCancelReq.TargetBranch = TargetBranch;
                    urCancelReq.UniversalRecordLocatorCode = universalRecord;
                    urCancelReq.Version = "33";

                    try
                    {
                        System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Universal46.UniversalRecordCancelReq));
                        System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                        System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                        serRes.Serialize(writerRes, urCancelReq); 	// Here Classes are converted to XML String. 
                        // This can be viewed in SB or writer.
                        // Above XML in SB can be loaded in XmlDocument object
                        XmlDocument docres = new XmlDocument();
                        docres.LoadXml(sbRes.ToString());

                        string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightURCancelRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        docres.Save(filePath);
                    }
                    catch { }

                    UAPIdll.Universal46.UniversalRecordCancelServiceBinding cancelBinding = new UAPIdll.Universal46.UniversalRecordCancelServiceBinding();
                    cancelBinding.Url = urlUR;
                    cancelBinding.Credentials = new NetworkCredential(UserName, Password);
                    cancelBinding.PreAuthenticate = true;

                    UAPIdll.Universal46.UniversalRecordCancelRsp urCancelResp = cancelBinding.service(urCancelReq);

                    try
                    {
                        System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Universal46.UniversalRecordCancelRsp));
                        System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                        System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                        serRes.Serialize(writerRes, urCancelResp); 	// Here Classes are converted to XML String. 
                        // This can be viewed in SB or writer.
                        // Above XML in SB can be loaded in XmlDocument object
                        XmlDocument docres = new XmlDocument();
                        docres.LoadXml(sbRes.ToString());
                        string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightURCancelResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        docres.Save(filePath);
                    }
                    catch { }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Cancel UR after Void. Reason: " + ex.ToString(), "");
                }
            }

        }
        #endregion

        # region common Methods

        private int keyGen(string keyValue)
        {
            int key = 0;
            string newKey = keyValue.Substring(0, keyValue.Length - 1);
            key = Convert.ToInt32(newKey);
            return key;


            //int key=
        }
        private double getCurrAmount(string keyValue)
        {
            double amount = 0;
            string newAmont = keyValue.Remove(0, 3);
            amount = Convert.ToDouble(newAmont);
            return amount;
        }

        private string getCurrency(string keyValue)// to get Currency Code (retrieve PNR)
        {
            string currencyCode = "AED";
            currencyCode = keyValue.Substring(0, 3);
            return currencyCode;
        }

        public List<KeyValuePair<string, SSR>> GenerateSSRPaxList(FlightItinerary itinerary)
        {
            //Trace.TraceInformation("UAPI.GenerateSSR entered");
            List<KeyValuePair<string, SSR>> ssrPaxList = new List<KeyValuePair<string, SSR>>();
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                if (i == 0 && !string.IsNullOrEmpty(itinerary.GstNumber))
                {
                    /*
                     * (Use // (double slash) in place of @ (at sign), use “..” (double dot) in place of “_” (underscore), and use “./” in place of a “-“ in email addresses.)
                     * */
                    string gstEmail = itinerary.GstCompanyEmail.Replace("@", "//").Replace("_", "..").Replace("-", "./");
                    string gstPhone = GenericStatic.FilterAlphaNumeric(itinerary.GstCompanyContactNumber);// itinerary.GstCompanyContactNumber.Replace("+", "").Replace("-", "").Replace(".", "");
                    string gstAddress = GenericStatic.FilterAlphaNumeric(itinerary.GstCompanyAddress); //itinerary.GstCompanyAddress.Replace(", ", " ");
                    string gstName = string.IsNullOrEmpty(itinerary.GstCompanyName) ? string.Empty : GenericStatic.FilterAlphaNumeric(itinerary.GstCompanyName);
                    SSR ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    ssr.SsrCode = "GSTN";//Max Length 20 alpha numerics for GST Number, 35 alpha numerics for GST Company name. Company Name is Mandatory
                    ssr.Detail = string.IsNullOrEmpty(gstName) ? "IND/" + itinerary.GstNumber + "/CTW" : "IND/" + itinerary.GstNumber + "/" + (gstName.Length > 31 ? gstName.Substring(0, 30) : gstName);
                    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                    ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    ssr.SsrCode = "GSTA";//Max Length 35 alpha numerics
                    ssr.Detail = "IND/" + (gstAddress.Length > 31 ? gstAddress.Substring(0, 30) : gstAddress);
                    ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                    ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    ssr.SsrCode = "GSTP";//Max Length 25 numerics for First Business Telephone
                    ssr.Detail = "IND/" + (gstPhone.Length > 15 ? gstPhone.Substring(0, 14) : gstPhone);
                    ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                    ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    ssr.SsrCode = "GSTE";//Max Length 35 alpha numerics
                    ssr.Detail = "IND/" + (gstEmail.Length > 31 ? gstEmail.Substring(0, 30) : gstEmail);
                    ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                }
                //if (itinerary.Passenger[i].Meal.Code != null || itinerary.Passenger[i].Meal.Code == string.Empty)
                //{
                //    //ssrList.Add("3SAN" + (i + 1) + itinerary.Passenger[i].Meal.Code);
                //    SSR ssr = new SSR();
                //    ssr.PaxId = itinerary.Passenger[i].PaxId;
                //    ssr.SsrCode = "MEAL";
                //    ssr.Detail = itinerary.Passenger[i].Meal.Code;
                //    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                //    ssrPaxList.Add(ssrPax);
                //}
                //if ((itinerary.Passenger[i].FFAirline != null && itinerary.Passenger[i].FFAirline.Length > 0) && (itinerary.Passenger[i].FFNumber != null && itinerary.Passenger[i].FFNumber.Length > 0))
                //{//3SSRFQTVUAHK/UA123456382-1
                //    //ssrList.Add("3SSRFQTV" + itinerary.Passenger[i].FFAirline.ToUpper() + "HK/" + itinerary.Passenger[i].FFAirline + itinerary.Passenger[i].FFNumber + "-" + (i + 1));
                //    SSR ssr = new SSR();
                //    ssr.PaxId = itinerary.Passenger[i].PaxId;
                //    ssr.SsrCode = "FQTV";
                //    ssr.Detail = itinerary.Passenger[i].FFAirline + itinerary.Passenger[i].FFNumber;
                //    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                //    ssrPaxList.Add(ssrPax);
                //}
                if ((itinerary.Passenger[i].Type == PassengerType.Child || itinerary.Passenger[i].Type == PassengerType.Infant) && (itinerary.Passenger[i].DateOfBirth != null && itinerary.Passenger[i].DateOfBirth > new DateTime()))
                {

                    SSR ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    if (itinerary.Passenger[i].Type == PassengerType.Child)
                    {
                        ssr.SsrCode = "CHLD";
                    }
                    else
                    {
                        ssr.SsrCode = "INFT";
                    }
                    ssr.Detail = itinerary.Passenger[i].LastName + "/" + itinerary.Passenger[i].FirstName + "" + itinerary.Passenger[i].Title + " " + itinerary.Passenger[i].DateOfBirth.ToString("dd/MM/yyyy");
                    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                }
                if ((itinerary.Passenger[i].PassportNo != null && itinerary.Passenger[i].PassportNo.Length > 0) && (itinerary.Passenger[i].Country != null && itinerary.Passenger[i].Country.CountryCode != null && itinerary.Passenger[i].Country.CountryCode.Length > 0) && (itinerary.Passenger[i].Nationality.CountryCode != null && itinerary.Passenger[i].Nationality.CountryCode.Length > 0))
                {

                    SSR ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    ssr.SsrCode = "DOCS";// passport info
                    //ssr.Detail = itinerary.Passenger[i].PassportNo + "-" + itinerary.Passenger[i].Country.CountryCode;
                    //string detail = string.Format("P/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}", itinerary.Passenger[i].Country.CountryCode, itinerary.Passenger[i].PassportNo, itinerary.Passenger[i].Country.CountryCode,
                    //itinerary.Passenger[i].DateOfBirth.ToString("ddMMMyy"),itinerary.Passenger[i].Gender==Gender.Male?"M":"F",itinerary.Passenger[i].PassportExpiry.ToString("ddMMMyy"),itinerary.Passenger[i].FirstName+" "+itinerary.Passenger[i].LastName,itinerary.Passenger[i].FirstName+" "+itinerary.Passenger[i].LastName);

                    string detail = string.Format("P/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}", itinerary.Passenger[i].Country.CountryCode,
                        GenericStatic.FilterAlphaNumeric(itinerary.Passenger[i].PassportNo), itinerary.Passenger[i].Nationality.CountryCode,
                        itinerary.Passenger[i].DateOfBirth.ToString("ddMMMyy"), itinerary.Passenger[i].Gender == Gender.Male ? "M" : "F",
                        itinerary.Passenger[i].PassportExpiry.ToString("ddMMMyy"), GenericStatic.FilterAlphaNumeric(itinerary.Passenger[i].FirstName),
                        GenericStatic.FilterAlphaNumeric(itinerary.Passenger[i].LastName));
                    ssr.Detail = detail;
                    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                }
                if (itinerary.Passenger[i].IsLeadPax)
                {
                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].CellPhone))
                    {
                        SSR ssrEntry = new SSR();
                        ssrEntry.PaxId = itinerary.Passenger[i].PaxId;
                        ssrEntry.SsrCode = "CTCM";
                        ssrEntry.Detail = GenericStatic.FilterAlphaNumeric(itinerary.Passenger[i].CellPhone);
                        KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssrEntry);
                        ssrPaxList.Add(ssrPax);
                    }
                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Email))
                    {
                        SSR ssrEntry = new SSR();
                        ssrEntry.PaxId = itinerary.Passenger[i].PaxId;
                        ssrEntry.SsrCode = "CTCE";
                        ssrEntry.Detail = (itinerary.Passenger[i].Email).Replace("@", "//").Replace("_", "..").Replace("-", "./");
                        KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssrEntry);
                        ssrPaxList.Add(ssrPax);
                    }
                }
            }
            //Trace.TraceInformation("UAPI.GenerateSSR exiting");
            return ssrPaxList;
        }
        # endregion


        /// <summary>
        /// This method will retrieve the Latest price and will be used for Repricing
        /// </summary>
        /// <param name="result"></param>
        /// <param name="credentials"></param>
        /// <param name="fares"></param>
        /// <returns></returns>
        //for B2C Temp purpose, once we implement new overloaded method in B2c, this method need to be scrapped
        public AirPriceRsp RePrice(SearchResult result, SearchType type, UAPICredentials credentials, ref Fare[] fares, ref string BookingValuesChanged)
        {
            AirPriceRsp priceResponse = new AirPriceRsp();
            BookingValuesChanged = string.Empty;
            try
            {
                Connection();
                AirPriceReq priceReq = new AirPriceReq();
                priceReq.AirItinerary = new AirItinerary();

                /* To avoid duplicate airsegments for VIA flights.*/
                priceReq.AirItinerary.AirSegment = result.UapiPricingSolution.AirSegment.Select(x => x).Distinct().ToArray();

                //Update missing Provider Code and remove AirAvailInfo and FlightDetailsRef
                for (int i = 0; i < priceReq.AirItinerary.AirSegment.Length; i++)
                {
                    typeBaseAirSegment segment = priceReq.AirItinerary.AirSegment[i];
                    segment.ProviderCode = result.UapiPricingSolution.AirPricingInfo[0].ProviderCode;
                    //segment.ClassOfService = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i].BookingCode;
                    segment.AirAvailInfo = new AirAvailInfo[0];
                    segment.FlightDetailsRef = new FlightDetailsRef[0];
                }

                /*********************************************************************************************************                 
                 *  If a Connection/SegmentIndex is returned in a Low Fare Shopping or Air Availability response,        *
                 *  it indicates a connection between two or more air segments. The Connection indicator must be used    * 
                 *  when Pricing and Booking to ensure the flights are sold correctly and a sell failure does not occur. *     
                 *  Remember, if in the Low Fare Shopping response example, the SegmentIndex indicators were 0, 1, 3, 4. * 
                 *  That means in the Air Pricing request, AirSegment 1, 2, 4, and 5 require the Connection indicator.   *                  
                 *********************************************************************************************************/
                if (result.UapiPricingSolution.Connection != null && result.UapiPricingSolution.Connection.Length > 0)
                {
                    foreach (UAPIdll.Air46.Connection con in result.UapiPricingSolution.Connection)
                    {
                        for (int i = 0; i < result.UapiPricingSolution.AirSegment.Length; i++)
                        {
                            if ((con.SegmentIndex) == i)
                            {
                                priceReq.AirItinerary.AirSegment[i].Connection = new Connection();
                            }
                        }
                    }
                }

                priceReq.BillingPointOfSaleInfo = new BillingPointOfSaleInfo();
                priceReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                priceReq.CheckFlightDetails = true;
                priceReq.FareRuleType = typeFareRuleType.@short;
                priceReq.CheckOBFees = "true";

                //Get the total pax count
                int paxCount = 0, adults = 0, childs = 0, infants = 0;
                for (int i = 0; i < result.FareBreakdown.Length; i++)
                {
                    paxCount += result.FareBreakdown[i].PassengerCount;
                    switch (result.FareBreakdown[i].PassengerType)
                    {
                        case PassengerType.Adult:
                            adults = result.FareBreakdown[i].PassengerCount;
                            break;
                        case PassengerType.Child:
                            childs = result.FareBreakdown[i].PassengerCount;
                            break;
                        case PassengerType.Infant:
                            infants = result.FareBreakdown[i].PassengerCount;
                            break;
                    }
                }

                //Initialize Search passengers
                priceReq.SearchPassenger = new SearchPassenger[paxCount];

                for (int p = 0; p < adults; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "ADT";
                    priceReq.SearchPassenger[p].Age = "40";// as per documentation hard-coding
                }

                for (int p = adults; p < adults + childs; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "CNN";
                    priceReq.SearchPassenger[p].Age = "5";
                }

                for (int p = (adults + childs); p < paxCount; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "INF";
                    priceReq.SearchPassenger[p].Age = "1";
                }


                priceReq.AirPricingCommand = new AirPricingCommand[1];
                priceReq.AirPricingCommand[0] = new AirPricingCommand();
                priceReq.AirPricingCommand[0].AirPricingModifiers = new AirPricingModifiers();
                //priceReq.AirPricingCommand[0].AirSegmentPricingModifiers = new AirSegmentPricingModifiers[0];
                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins = new UAPIdll.Air46.CabinClass[1];

                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins[0] = new UAPIdll.Air46.CabinClass();
                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins[0].Type = result.Flights[0][0].CabinClass;

                //priceReq.AirPricingCommand[0].CabinClass = result.Flights[0][0].CabinClass;
                //priceReq.AirPricingCommand[0].AirSegmentPricingModifiers = new AirSegmentPricingModifiers[result.UapiPricingSolution.AirSegment.Length];

                //for (int i = 0; i < result.UapiPricingSolution.AirSegment.Length; i++)
                //{
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i] = new AirSegmentPricingModifiers();
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].AirSegmentRef = result.UapiPricingSolution.AirSegment[i].Key;
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes = new BookingCode[1];
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes[0] = new BookingCode();
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes[0].Code = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i].BookingCode;

                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].CabinClass = result.Flights[0][0].CabinClass;
                //}

                priceReq.AirPricingModifiers = new AirPricingModifiers();
                priceReq.AirPricingModifiers.PermittedCabins = new UAPIdll.Air46.CabinClass[1];
                priceReq.AirPricingModifiers.PermittedCabins[0] = new UAPIdll.Air46.CabinClass();
                priceReq.AirPricingModifiers.PermittedCabins[0].Type = result.Flights[0][0].CabinClass;
                priceReq.AirPricingModifiers.InventoryRequestType = typeInventoryRequest.DirectAccess;// Always checks availability with Provider

                //Assign Airline AccountCodes for AirlinePrivateFare booking if AccountCode is returned from Search Response
                UAPIdll.Air46.AirPricingInfo pricingInfo = result.UapiPricingSolution.AirPricingInfo[0];//Take always first Adult PricingInfo
                if (pricingInfo != null && pricingInfo.PricingMethod == UAPIdll.Air46.typePricingMethod.GuaranteedUsingAirlinePrivateFare && pricingInfo.FareInfo != null && pricingInfo.FareInfo.Length > 0 && pricingInfo.FareInfo[0].AccountCode != null && pricingInfo.FareInfo[0].AccountCode.Length > 0)
                {

                    priceReq.AirPricingModifiers.AccountCodes = new UAPIdll.Air46.AccountCode[1];
                    priceReq.AirPricingModifiers.AccountCodes[0] = new UAPIdll.Air46.AccountCode();
                    priceReq.AirPricingModifiers.AccountCodes[0].Code = pricingInfo.FareInfo[0].AccountCode[0].Code;
                    priceReq.AirPricingModifiers.AccountCodes[0].ProviderCode = pricingInfo.ProviderCode;
                    if (pricingInfo.FareInfo[0].AccountCode[0].SupplierCode != null && pricingInfo.FareInfo[0].AccountCode[0].Type.Length > 0)
                    {
                        priceReq.AirPricingModifiers.AccountCodes[0].SupplierCode = pricingInfo.FareInfo[0].AccountCode[0].SupplierCode;
                    }
                    if (pricingInfo.FareInfo[0].AccountCode[0].Type != null && pricingInfo.FareInfo[0].AccountCode[0].Type.Length > 0)
                    {
                        priceReq.AirPricingModifiers.AccountCodes[0].Type = pricingInfo.FareInfo[0].AccountCode[0].Type;
                    }

                    //Assign Master Account codes
                    //string[] codes = ConfigurationManager.AppSettings["UAPIAccountCodes"].Split(',');
                    //priceReq.AirPricingModifiers.AccountCodeFaresOnly = false;
                    //priceReq.AirPricingModifiers.AccountCodeFaresOnlySpecified = false;
                    //priceReq.AirPricingModifiers.AccountCodes = new AccountCode[codes.Length];
                    //for (int i = 0; i < codes.Length; i++)
                    //{
                    //    priceReq.AirPricingModifiers.AccountCodes[i] = new AccountCode();
                    //    priceReq.AirPricingModifiers.AccountCodes[i].Code = codes[i];
                    //    priceReq.AirPricingModifiers.AccountCodes[i].ProviderCode = "1G";
                    //    //priceModifier.AccountCodes[i].SupplierCode = "";
                    //    //priceModifier.AccountCodes[i].Type = "";
                    //}
                    //priceReq.AirPricingModifiers.FaresIndicator = UAPIdll.Air46.typeFaresIndicator.PrivateFaresOnly;
                }

                priceReq.TargetBranch = credentials.TargetBranch;



                string sOrigin = result.Flights[0].ToList().Select(x => x.Origin.AirportCode).FirstOrDefault();
                string sDestination = result.Flights[0].ToList().Select(x => x.Destination.AirportCode).Last();
                try
                {
                    System.Xml.Serialization.XmlSerializer serReq = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Air46.AirPriceReq));
                    System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                    System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                    serReq.Serialize(writerRes, priceReq);  // Here Classes are converted to XML String. 
                                                            // This can be viewed in SB or writer.
                                                            // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument docres = new XmlDocument();
                    docres.LoadXml(sbRes.ToString());
                    string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightRePriceRequest_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    docres.Save(filePath);
                }
                catch { }


                AirPriceBinding binding = new AirPriceBinding();
                binding.Url = urlAir;
                binding.Credentials = new NetworkCredential(credentials.UserName, credentials.Password);
                binding.PreAuthenticate = true;//to implement GZIP

                priceResponse = binding.service(priceReq);

                //Load static xml
                //System.Xml.Serialization.XmlSerializer serLoc = new System.Xml.Serialization.XmlSerializer(typeof(AirPriceRsp));
                //string staticpath = ConfigurationManager.AppSettings["TestStaticXmlLoadPath"];
                //System.IO.StreamReader writerLoc = new StreamReader(@staticpath + "FlightRePriceResponse_6f1bdba7-abbc-413d-8eaf-233aee464c39_4008_20062018_070012.xml");
                //priceResponse = serLoc.Deserialize(writerLoc) as AirPriceRsp;
                //writerLoc.Close();

                try
                {
                    System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Air46.AirPriceRsp));
                    System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                    System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                    serRes.Serialize(writerRes, priceResponse);     // Here Classes are converted to XML String. 
                                                                    // This can be viewed in SB or writer.
                                                                    // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument docres = new XmlDocument();
                    docres.LoadXml(sbRes.ToString());
                    string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightRePriceResponse_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    docres.Save(filePath);
                }
                catch { }


                string errorMessage = string.Empty;

                if (priceResponse != null && priceResponse.AirPriceResult != null && priceResponse.AirPriceResult.Length > 0 && priceResponse.AirPriceResult[0].AirPricingSolution != null && priceResponse.AirPriceResult[0].AirPricingSolution.Length > 0)
                {
                    if (priceResponse.ResponseMessage != null)
                    {
                        foreach (ResponseMessage responseMsg in priceResponse.ResponseMessage)
                        {
                            if (responseMsg.Type == ResponseMessageType.Error)
                            {
                                errorMessage += responseMsg.Value;
                            }
                        }
                    }

                    if (errorMessage.Length > 0)
                    {
                        throw new Exception(errorMessage);
                    }
                    else
                    {
                        decimal baseDiscount = 20;
                        //Check whether any key is specified
                        if (ConfigurationManager.AppSettings["UAPIDiscount"] != null)
                        {
                            baseDiscount = Convert.ToDecimal(ConfigurationManager.AppSettings["UAPIDiscount"]);
                        }

                        decimal baseDiscountROE = 1;
                        //Retrieve Agent Exchange rate from the xml currency
                        string currency = getCurrency(priceResponse.AirPriceResult[0].AirPricingSolution[0].TotalPrice);
                        rateOfExchange = (double)ExchangeRates[currency];
                        baseDiscountROE = ExchangeRates["AED"];//Get the ROE for Discount

                        List<Fare> FareList = new List<Fare>(fares);
                        result.Price.TaxBreakups = new List<TaxBreakup>();
                        for (int i = 0; i < priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo.Length; i++)
                        {
                            AirPricingInfo priceInfo = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[i];

                            ////////////////////////////////////////////////////////////////////////////////
                            //  Updating the BaseFare, TotalFare & SupplierFare pax type wise
                            //  Updating the PublishedFare and Tax in the Price for B2C
                            ////////////////////////////////////////////////////////////////////////////////                            

                            Fare fare = FareList.Find(delegate (Fare f) { return f.PassengerType == PassengerType.Adult && priceInfo.PassengerType[0].Code == "ADT" || f.PassengerType == PassengerType.Child && priceInfo.PassengerType[0].Code == "CNN" || f.PassengerType == PassengerType.Infant && priceInfo.PassengerType[0].Code == "INF"; });

                            fare.BaseFare = ((Convert.ToDouble(priceInfo.ApproximateBasePrice.Remove(0, 3))) * rateOfExchange) * fare.PassengerCount;
                            fare.TotalFare = ((Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3))) * rateOfExchange) * fare.PassengerCount;
                            fare.SupplierFare = Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3)) * fare.PassengerCount;
                            result.Price.PublishedFare += ((Convert.ToDecimal(priceInfo.ApproximateBasePrice.Remove(0, 3))) * (decimal)rateOfExchange);
                            result.Price.Tax += ((Convert.ToDecimal(priceInfo.Taxes.Remove(0, 3))) * (decimal)rateOfExchange);

                            if (priceInfo.TaxInfo != null)
                            {
                                foreach (typeTaxInfo tax in priceInfo.TaxInfo)
                                {
                                    if (result.Price.TaxBreakups.Exists(t => t.TaxCode == tax.Category))
                                    {
                                        TaxBreakup breakup = result.Price.TaxBreakups.Find(t => t.TaxCode == tax.Category);
                                        breakup.TaxValue += (decimal)(getCurrAmount(tax.Amount) * priceInfo.PassengerType.Length * rateOfExchange);

                                    }
                                    else
                                    {
                                        TaxBreakup breakup = new TaxBreakup();
                                        breakup.TaxCode = tax.Category;
                                        breakup.TaxValue = (decimal)(getCurrAmount(tax.Amount) * priceInfo.PassengerType.Length * rateOfExchange);
                                        result.Price.TaxBreakups.Add(breakup);
                                    }
                                }
                            }

                        }

                        result.Price.K3Tax = result.Price.TaxBreakups.Where(x => x.TaxCode == "K3").Sum(y => y.TaxValue);

                        //Get the Price details from Price Response AirPricingSolution
                        decimal newTotal = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].TotalPrice.Remove(0, 3));
                        decimal newTax = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].Taxes.Remove(0, 3));
                        decimal newBasePrice = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].ApproximateBasePrice.Remove(0, 3));

                        //Get the price details from existing AirPricingSolution
                        decimal basePrice = Convert.ToDecimal(result.UapiPricingSolution.ApproximateBasePrice.Remove(0, 3));
                        decimal Taxes = Convert.ToDecimal(result.UapiPricingSolution.Taxes.Remove(0, 3));
                        decimal Total = Convert.ToDecimal(result.UapiPricingSolution.TotalPrice.Remove(0, 3));

                        Total = Convert.ToDecimal(result.UapiPricingSolution.AirPricingInfo[0].TotalPrice.Remove(0, 3)) * priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].PassengerType.Length;
                        if (result.UapiPricingSolution.AirPricingInfo.Length > 1)
                        {
                            Total += Convert.ToDecimal(result.UapiPricingSolution.AirPricingInfo[1].TotalPrice.Remove(0, 3)) * priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[1].PassengerType.Length;
                        }
                        if (result.UapiPricingSolution.AirPricingInfo.Length > 2)
                        {
                            Total += Convert.ToDecimal(result.UapiPricingSolution.AirPricingInfo[2].TotalPrice.Remove(0, 3)) * priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[2].PassengerType.Length;
                        }
                        /******************************************************************************************
                         *      Check whether any of segments times have changed or not
                         * ****************************************************************************************/

                        //Sort the segments based on Departure Time
                        List<typeBaseAirSegment> airSegments = new List<typeBaseAirSegment>();

                        //typeBaseAirSegment[] segments = priceResponse.AirItinerary.AirSegment;
                        //Array.Sort(segments, delegate (typeBaseAirSegment s1, typeBaseAirSegment s2) { return Convert.ToDateTime(s1.DepartureTime).CompareTo(Convert.ToDateTime(s2.DepartureTime)); });
                        //airSegments.AddRange(segments);

                        priceResponse.AirItinerary.AirSegment.ToList().ForEach(x =>
                        {

                            BookingInfo bkgInfo = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo.FirstOrDefault(k => k.SegmentRef == x.Key);
                            x.CabinClass = bkgInfo.CabinClass;
                            if (x.FlightDetails.Length > 1)
                            {
                                x.FlightDetails.ToList().ForEach(y =>
                                {
                                    typeBaseAirSegment tmp = new typeBaseAirSegment();
                                    tmp.ClassOfService = x.ClassOfService; tmp.CabinClass = x.CabinClass;
                                    tmp.DepartureTime = y.DepartureTime; tmp.ArrivalTime = y.ArrivalTime;
                                    tmp.Origin = y.Origin; tmp.Destination = y.Destination; airSegments.Add(tmp);
                                });
                            }
                            else
                                airSegments.Add(x);
                        });

                        try
                        {
                            //Check whether any time difference or Booking class difference is there
                            for (int i = 0; i < airSegments.Count; i++)
                            {
                                typeBaseAirSegment aSeg = airSegments[i];
                                //BookingInfo bkgInfo = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i];
                                //Calculate departure time and arrival time excluding the time zone
                                string depTime = string.Empty;
                                if (aSeg.DepartureTime.Contains("+"))
                                {
                                    depTime = aSeg.DepartureTime.Split('+')[0];
                                }
                                else if (aSeg.DepartureTime.Split('-').Length > 3)
                                {
                                    depTime = aSeg.DepartureTime.Remove(aSeg.DepartureTime.LastIndexOf("-"));
                                }
                                else
                                {
                                    depTime = aSeg.DepartureTime;
                                }

                                string arrTime = string.Empty;
                                if (aSeg.ArrivalTime.Contains("+"))
                                {
                                    arrTime = aSeg.ArrivalTime.Split('+')[0];
                                }
                                else if (aSeg.ArrivalTime.Split('-').Length > 3)
                                {
                                    arrTime = aSeg.ArrivalTime.Remove(aSeg.ArrivalTime.LastIndexOf("-"));
                                }
                                else
                                {
                                    arrTime = aSeg.ArrivalTime;
                                }

                                /***************************************************************************************************
                                 * Whenever there is a change in the Segment details, i.e. Departure Time, Arrival Time, Booking 
                                 * Class or Cabin Class, we are updating the changed values to BookingValuesChanged string in the 
                                 * following format. ex: if searched for DXB-BKK then DXB-BOM|D|A|B|C,BOM-BKK|A|B|C
                                 * Changed values will be added segment wise seperated by comma (,) and each change value will be
                                 * seperated by pipe '|' 
                                 * ORG-DEST - Indicates the segment origin and destination
                                 * D - Indicates Departure Time is changed
                                 * A - Indicates Arrival Time is changed
                                 * B - Indicates Booking class is changed
                                 * C - Indicates Cabin class is changed
                                 * *************************************************************************************************/
                                //Check time difference for onward segments by matching the origin & destination airport codes
                                if (i < result.Flights[0].Length && aSeg.Group == result.Flights[0][i].Group && result.Flights[0][i].Origin.AirportCode == aSeg.Origin && result.Flights[0][i].Destination.AirportCode == aSeg.Destination)
                                {
                                    FlightInfo fSeg = result.Flights[0][i];

                                    if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                    {
                                        result.IsTimesChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|D";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                        }
                                        fSeg.UapiDepartureTime = aSeg.DepartureTime;
                                        fSeg.UapiArrivalTime = aSeg.ArrivalTime;
                                    }
                                    if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                    {
                                        result.IsTimesChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|A";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                        }
                                        fSeg.UapiDepartureTime = aSeg.DepartureTime;
                                        fSeg.UapiArrivalTime = aSeg.ArrivalTime;
                                    }
                                    if (fSeg.BookingClass != aSeg.ClassOfService)
                                    {
                                        result.IsBookingClassChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|B";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                        }
                                    }
                                    if (fSeg.CabinClass != aSeg.CabinClass)
                                    {
                                        result.IsBookingClassChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|C";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                        }
                                    }
                                }
                                else if (type == SearchType.MultiWay)//Check difference for Multiway segments
                                {
                                    int ret = result.Flights[0].Length;
                                    if (result.Flights[1][i - ret].Origin.AirportCode == aSeg.Origin && result.Flights[1][i - ret].Destination.AirportCode == aSeg.Destination)
                                    {
                                        FlightInfo fSeg = result.Flights[1][i - ret];
                                        if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|D";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                        }
                                        if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|A";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                        }
                                        if (fSeg.BookingClass != aSeg.ClassOfService)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|B";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                        }
                                        if (fSeg.CabinClass != aSeg.CabinClass)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|C";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                        }
                                    }
                                }
                                else if (type == SearchType.Return)//Check difference for Return segments
                                {
                                    if (result.Flights[1][i - result.Flights[0].Length].Origin.AirportCode == aSeg.Origin && result.Flights[1][i - result.Flights[0].Length].Destination.AirportCode == aSeg.Destination)
                                    {
                                        FlightInfo fSeg = result.Flights[1][i - result.Flights[0].Length];
                                        if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|D";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                        }
                                        if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|A";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                        }
                                        if (fSeg.BookingClass != aSeg.ClassOfService)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|B";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                        }
                                        if (fSeg.CabinClass != aSeg.CabinClass)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|C";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to compare dates in UAPI Reprice. " + ex.ToString(), "");
                            result.RepriceErrorMessage = "There is a technical error while confirming your price. Please Search again";
                        }

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //
                        //  If any of the values are not same, i.e. Base Price, Taxes , Total, Departure Time, Arrival Time or Booking Class
                        //  update the segment details and AirPricingSolution
                        //
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        /********************************************************************************************************
                         *  Update the AirSegmentDetails returned from Price Response and update the result Flight Segments     *
                         *  along with UapiPricingSolution. These values are required to pass in the Reservation Request.       *
                         *  Values that are updated include                                                                     *
                         *  1.  Segment Key                                                                                      * 
                         *  2.  Departure Time
                         *  3.  Arrival Time
                         *  4.  Booking Class
                         *  5.  Cabin Class
                         *  5.  Duration
                         *  6.  Group
                         *  7.  Flight Number
                         *  8.  Link Availability
                         *  9.  Availability Source
                         *  10. Polled Availability Option
                         *  11. Provider Code
                         *  12. Segment Ref Key
                         *  13. Code Share Info                                                                                 *    
                        * *******************************************************************************************************/


                        //Update details always
                        //if (Total != newTotal || result.IsTimesChanged || result.IsBookingClassChanged)
                        {
                            //Assign the new UapiPricingSolution returned from the pricing response.
                            result.UapiPricingSolution = priceResponse.AirPriceResult[0].AirPricingSolution[0];
                            //Assign the segments returned from the price response bcoz segments will be empty in UapiPricingSolution
                            result.UapiPricingSolution.AirSegment = priceResponse.AirItinerary.AirSegment;
                            //Formation of Default baggage in Re-Price.
                            result = DefaultBaggageFormation(priceResponse, result);

                            foreach (UAPIdll.Air46.AirPricingInfo price in result.UapiPricingSolution.AirPricingInfo)
                            {
                                price.BaggageAllowances = null;//Remove the baggage info 

                                foreach (UAPIdll.Air46.FareInfo fare in price.FareInfo)
                                {
                                    fare.Endorsement = new Endorsement[0];//Remove any endorsements                                    
                                    fare.Brand = null;//Clear any Branding
                                }

                            }

                            //Add all Flight segments to update the segment info from the response
                            List<FlightInfo> FlightSegments = new List<FlightInfo>();
                            FlightSegments.AddRange(result.Flights[0]);
                            if (result.Flights.Length > 1)
                            {
                                FlightSegments.AddRange(result.Flights[1]);
                            }

                            //Update the result segments as per segment details
                            for (int i = 0; i < priceResponse.AirItinerary.AirSegment.Length; i++)
                            {
                                typeBaseAirSegment segment = priceResponse.AirItinerary.AirSegment[i];

                                for (int j = 0; j < segment.FlightDetails.Length; j++)
                                {
                                    FlightDetails flightDetail = segment.FlightDetails[j];
                                    //Calculate departure time and arrival time excluding the time zone
                                    string depTime = string.Empty;
                                    //if (segment.DepartureTime.Contains("+"))
                                    if (flightDetail.DepartureTime.Contains("+"))
                                    {
                                        depTime = flightDetail.DepartureTime.Split('+')[0];
                                    }
                                    //else if (segment.DepartureTime.Split('-').Length > 3)
                                    else if (flightDetail.DepartureTime.Split('-').Length > 3)
                                    {
                                        depTime = flightDetail.DepartureTime.Remove(flightDetail.DepartureTime.LastIndexOf("-"));
                                    }
                                    else
                                    {
                                        depTime = flightDetail.DepartureTime;
                                    }

                                    string arrTime = string.Empty;
                                    //if (segment.ArrivalTime.Contains("+"))
                                    if (flightDetail.ArrivalTime.Contains("+"))
                                    {
                                        arrTime = flightDetail.ArrivalTime.Split('+')[0];
                                    }
                                    //else if (segment.ArrivalTime.Split('-').Length > 3)
                                    else if (flightDetail.ArrivalTime.Split('-').Length > 3)
                                    {
                                        arrTime = flightDetail.ArrivalTime.Remove(flightDetail.ArrivalTime.LastIndexOf("-"));
                                    }
                                    else
                                    {
                                        arrTime = flightDetail.ArrivalTime;
                                    }

                                    //Assign onward segment details 
                                    //if (i < result.Flights[0].Length && result.Flights[0][j].Origin.AirportCode == segment.Origin && result.Flights[0][j].Destination.AirportCode == segment.Destination)

                                    //Retrieve Segment info
                                    FlightInfo fInfo = FlightSegments.Find(delegate (FlightInfo f) { return f.Origin.AirportCode == flightDetail.Origin && f.Destination.AirportCode == flightDetail.Destination; });
                                    if (fInfo != null)
                                    {
                                        fInfo.UapiSegmentRefKey = segment.Key;
                                        fInfo.BookingClass = segment.ClassOfService;
                                        fInfo.DepartureTime = Convert.ToDateTime(depTime);
                                        fInfo.ArrivalTime = Convert.ToDateTime(arrTime);
                                        fInfo.UapiArrivalTime = segment.ArrivalTime;//Always update AirSegment times in order to send in AirReservationReq
                                        fInfo.UapiDepartureTime = segment.DepartureTime;
                                        fInfo.Duration = new TimeSpan(0, Convert.ToInt32(flightDetail.FlightTime), 0);
                                        fInfo.Group = segment.Group;
                                        fInfo.FlightNumber = segment.FlightNumber;
                                        fInfo.CabinClass = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo[0].CabinClass;

                                        fInfo.UAPIReservationValues[UAPI.LINK_AVAILABILITY] = segment.LinkAvailability;
                                        fInfo.UAPIReservationValues[UAPI.POLLED_AVAILABILITY_OPTION] = segment.PolledAvailabilityOption;
                                        fInfo.UAPIReservationValues[UAPI.AVAILABILITY_SOURCE] = segment.AvailabilitySource;
                                        fInfo.UAPIReservationValues[UAPI.PROVIDER_CODE] = segment.ProviderCode;
                                        fInfo.UAPIReservationValues[UAPI.SEGMENT_REF] = segment.Key;
                                        if (segment.CodeshareInfo != null && segment.CodeshareInfo.OperatingCarrier != fInfo.Airline)
                                        {
                                            fInfo.OperatingCarrier = segment.CodeshareInfo.OperatingCarrier + "-" + segment.CodeshareInfo.OperatingFlightNumber + " " + segment.CodeshareInfo.Value;
                                        }
                                    }
                                    //else if (type != SearchType.OneWay)//Assign Multiway & Return segment details
                                    //{
                                    //    int ret = result.Flights[0].Length;
                                    //    //if (result.Flights[1][j - ret].Origin.AirportCode == segment.Origin && result.Flights[1][j - ret].Destination.AirportCode == segment.Destination)
                                    //    if (result.Flights[1][j - ret].Origin.AirportCode == flightDetail.Origin && result.Flights[1][j - ret].Destination.AirportCode == flightDetail.Destination)
                                    //    {
                                    //        result.Flights[1][j - ret].UapiSegmentRefKey = segment.Key;
                                    //        result.Flights[1][j - ret].BookingClass = segment.ClassOfService;
                                    //        result.Flights[1][j - ret].ArrivalTime = Convert.ToDateTime(arrTime);
                                    //        result.Flights[1][j - ret].DepartureTime = Convert.ToDateTime(depTime);
                                    //        result.Flights[1][j - ret].UapiArrivalTime = flightDetail.ArrivalTime;
                                    //        result.Flights[1][j - ret].UapiDepartureTime = flightDetail.DepartureTime;
                                    //        result.Flights[1][j - ret].Duration = new TimeSpan(0, Convert.ToInt32(segment.FlightTime), 0);
                                    //        result.Flights[1][j - ret].Group = segment.Group;
                                    //        result.Flights[1][j - ret].FlightNumber = segment.FlightNumber;
                                    //        result.Flights[1][j - ret].CabinClass = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo[0].CabinClass;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.LINK_AVAILABILITY] = segment.LinkAvailability;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.POLLED_AVAILABILITY_OPTION] = segment.PolledAvailabilityOption;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.AVAILABILITY_SOURCE] = segment.AvailabilitySource;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.PROVIDER_CODE] = segment.ProviderCode;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.SEGMENT_REF] = segment.Key;
                                    //        if (segment.CodeshareInfo != null && segment.CodeshareInfo.OperatingCarrier != result.Flights[1][j - ret].Airline)
                                    //        {
                                    //            result.Flights[1][j - ret].OperatingCarrier = segment.CodeshareInfo.OperatingCarrier + "-" + segment.CodeshareInfo.OperatingFlightNumber + " " + segment.CodeshareInfo.Value;
                                    //        }
                                    //    }
                                    //}

                                }
                            }
                        }
                    }
                }
            }
            catch (SoapException se)
            {
                result.RepriceErrorMessage = se.Message;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "(UAPI Reprice)" + ex.ToString(), "");
                result.RepriceErrorMessage = "There is a technical error while confirming your price. Please Search again";
            }

            return priceResponse;
        }

        /// <summary>
        /// Overloaded method for passing ProhitbitNonRefundableFares=true/false based on the SearchRequest.
        /// </summary>
        /// <param name="result">SearchResult object for updating the price and uapiPricingSolution</param>
        /// <param name="type">SearchType to evaluate flight info array</param>
        /// <param name="request">SearchRequest object for knowing whether to request Refundable or NonRefundable fares</param>
        /// <param name="credentials">UapiCredentials object for Authentication</param>
        /// <param name="fares">Fares array reference for returning updated fares</param>
        /// <param name="BookingValuesChanged">To know whether bookingClass or times have changed or not</param>
        /// <returns></returns>
        public AirPriceRsp RePrice(SearchResult result, SearchType type, SearchRequest request, UAPICredentials credentials, ref Fare[] fares, ref string BookingValuesChanged)
        {
            AirPriceRsp priceResponse = new AirPriceRsp();
            BookingValuesChanged = string.Empty;
            try
            {
                Connection();
                AirPriceReq priceReq = new AirPriceReq();
                priceReq.AirItinerary = new AirItinerary();

                /* To avoid duplicate airsegments for VIA flights.*/
                priceReq.AirItinerary.AirSegment = result.UapiPricingSolution.AirSegment.Select(x => x).Distinct().ToArray();

                //Update missing Provider Code and remove AirAvailInfo and FlightDetailsRef
                for (int i = 0; i < priceReq.AirItinerary.AirSegment.Length; i++)
                {
                    typeBaseAirSegment segment = priceReq.AirItinerary.AirSegment[i];
                    segment.ProviderCode = result.UapiPricingSolution.AirPricingInfo[0].ProviderCode;
                    //segment.ClassOfService = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i].BookingCode;
                    segment.AirAvailInfo = new AirAvailInfo[0];
                    segment.FlightDetailsRef = new FlightDetailsRef[0];
                }

                /*********************************************************************************************************                 
                 *  If a Connection/SegmentIndex is returned in a Low Fare Shopping or Air Availability response,        *
                 *  it indicates a connection between two or more air segments. The Connection indicator must be used    * 
                 *  when Pricing and Booking to ensure the flights are sold correctly and a sell failure does not occur. *     
                 *  Remember, if in the Low Fare Shopping response example, the SegmentIndex indicators were 0, 1, 3, 4. * 
                 *  That means in the Air Pricing request, AirSegment 1, 2, 4, and 5 require the Connection indicator.   *                  
                 *********************************************************************************************************/
                if (result.UapiPricingSolution.Connection != null && result.UapiPricingSolution.Connection.Length > 0)
                {
                    foreach (UAPIdll.Air46.Connection con in result.UapiPricingSolution.Connection)
                    {
                        for (int i = 0; i < result.UapiPricingSolution.AirSegment.Length; i++)
                        {
                            if ((con.SegmentIndex) == i)
                            {
                                priceReq.AirItinerary.AirSegment[i].Connection = new Connection();
                            }
                        }
                    }
                }

                priceReq.BillingPointOfSaleInfo = new BillingPointOfSaleInfo();
                priceReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                priceReq.CheckFlightDetails = true;
                priceReq.FareRuleType = typeFareRuleType.@short;
                priceReq.CheckOBFees = "true";

                //Get the total pax count
                int paxCount = 0, adults = 0, childs = 0, infants = 0;
                for (int i = 0; i < result.FareBreakdown.Length; i++)
                {
                    paxCount += result.FareBreakdown[i].PassengerCount;
                    switch (result.FareBreakdown[i].PassengerType)
                    {
                        case PassengerType.Adult:
                            adults = result.FareBreakdown[i].PassengerCount;
                            break;
                        case PassengerType.Child:
                            childs = result.FareBreakdown[i].PassengerCount;
                            break;
                        case PassengerType.Infant:
                            infants = result.FareBreakdown[i].PassengerCount;
                            break;
                    }
                }

                //Initialize Search passengers
                priceReq.SearchPassenger = new SearchPassenger[paxCount];

                for (int p = 0; p < adults; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "ADT";
                    priceReq.SearchPassenger[p].Age = "40";// as per documentation hard-coding
                }

                for (int p = adults; p < adults + childs; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "CNN";
                    priceReq.SearchPassenger[p].Age = "5";
                }

                for (int p = (adults + childs); p < paxCount; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "INF";
                    priceReq.SearchPassenger[p].Age = "1";
                }


                priceReq.AirPricingCommand = new AirPricingCommand[1];
                priceReq.AirPricingCommand[0] = new AirPricingCommand();
                priceReq.AirPricingCommand[0].AirPricingModifiers = new AirPricingModifiers();
                //priceReq.AirPricingCommand[0].AirSegmentPricingModifiers = new AirSegmentPricingModifiers[0];
                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins = new UAPIdll.Air46.CabinClass[1];

                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins[0] = new UAPIdll.Air46.CabinClass();
                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins[0].Type = result.Flights[0][0].CabinClass;

                //priceReq.AirPricingCommand[0].CabinClass = result.Flights[0][0].CabinClass;
                //priceReq.AirPricingCommand[0].AirSegmentPricingModifiers = new AirSegmentPricingModifiers[result.UapiPricingSolution.AirSegment.Length];

                //for (int i = 0; i < result.UapiPricingSolution.AirSegment.Length; i++)
                //{
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i] = new AirSegmentPricingModifiers();
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].AirSegmentRef = result.UapiPricingSolution.AirSegment[i].Key;
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes = new BookingCode[1];
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes[0] = new BookingCode();
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes[0].Code = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i].BookingCode;

                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].CabinClass = result.Flights[0][0].CabinClass;
                //}

                priceReq.AirPricingModifiers = new AirPricingModifiers();
                priceReq.AirPricingModifiers.PermittedCabins = new UAPIdll.Air46.CabinClass[1];
                priceReq.AirPricingModifiers.PermittedCabins[0] = new UAPIdll.Air46.CabinClass();
                priceReq.AirPricingModifiers.PermittedCabins[0].Type = result.Flights[0][0].CabinClass;
                priceReq.AirPricingModifiers.InventoryRequestType = typeInventoryRequest.DirectAccess;// Always checks availability with Provider
                priceReq.AirPricingModifiers.ProhibitNonRefundableFares = request.RefundableFares;
                //Assign Airline AccountCodes for AirlinePrivateFare booking if AccountCode is returned from Search Response
                UAPIdll.Air46.AirPricingInfo pricingInfo = result.UapiPricingSolution.AirPricingInfo[0];//Take always first Adult PricingInfo
                if (pricingInfo != null && pricingInfo.PricingMethod == UAPIdll.Air46.typePricingMethod.GuaranteedUsingAirlinePrivateFare && pricingInfo.FareInfo != null && pricingInfo.FareInfo.Length > 0 && pricingInfo.FareInfo[0].AccountCode != null && pricingInfo.FareInfo[0].AccountCode.Length > 0)
                {

                    priceReq.AirPricingModifiers.AccountCodes = new UAPIdll.Air46.AccountCode[1];
                    priceReq.AirPricingModifiers.AccountCodes[0] = new UAPIdll.Air46.AccountCode();
                    priceReq.AirPricingModifiers.AccountCodes[0].Code = pricingInfo.FareInfo[0].AccountCode[0].Code;
                    priceReq.AirPricingModifiers.AccountCodes[0].ProviderCode = pricingInfo.ProviderCode;
                    if (pricingInfo.FareInfo[0].AccountCode[0].SupplierCode != null && pricingInfo.FareInfo[0].AccountCode[0].Type.Length > 0)
                    {
                        priceReq.AirPricingModifiers.AccountCodes[0].SupplierCode = pricingInfo.FareInfo[0].AccountCode[0].SupplierCode;
                    }
                    if (pricingInfo.FareInfo[0].AccountCode[0].Type != null && pricingInfo.FareInfo[0].AccountCode[0].Type.Length > 0)
                    {
                        priceReq.AirPricingModifiers.AccountCodes[0].Type = pricingInfo.FareInfo[0].AccountCode[0].Type;
                    }

                    //Assign Master Account codes
                    //string[] codes = ConfigurationManager.AppSettings["UAPIAccountCodes"].Split(',');
                    //priceReq.AirPricingModifiers.AccountCodeFaresOnly = false;
                    //priceReq.AirPricingModifiers.AccountCodeFaresOnlySpecified = false;
                    //priceReq.AirPricingModifiers.AccountCodes = new AccountCode[codes.Length];
                    //for (int i = 0; i < codes.Length; i++)
                    //{
                    //    priceReq.AirPricingModifiers.AccountCodes[i] = new AccountCode();
                    //    priceReq.AirPricingModifiers.AccountCodes[i].Code = codes[i];
                    //    priceReq.AirPricingModifiers.AccountCodes[i].ProviderCode = "1G";
                    //    //priceModifier.AccountCodes[i].SupplierCode = "";
                    //    //priceModifier.AccountCodes[i].Type = "";
                    //}
                    //priceReq.AirPricingModifiers.FaresIndicator = UAPIdll.Air46.typeFaresIndicator.PrivateFaresOnly;
                }

                priceReq.TargetBranch = credentials.TargetBranch;



                string sOrigin = result.Flights[0].ToList().Select(x => x.Origin.AirportCode).FirstOrDefault();
                string sDestination = result.Flights[0].ToList().Select(x => x.Destination.AirportCode).Last();
                try
                {
                    System.Xml.Serialization.XmlSerializer serReq = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Air46.AirPriceReq));
                    System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                    System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                    serReq.Serialize(writerRes, priceReq);  // Here Classes are converted to XML String. 
                                                            // This can be viewed in SB or writer.
                                                            // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument docres = new XmlDocument();
                    docres.LoadXml(sbRes.ToString());
                    string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightRePriceRequest_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    docres.Save(filePath);
                }
                catch { }


                AirPriceBinding binding = new AirPriceBinding();
                binding.Url = urlAir;
                binding.Credentials = new NetworkCredential(credentials.UserName, credentials.Password);
                binding.PreAuthenticate = true;//to implement GZIP

                priceResponse = binding.service(priceReq);

                //Load static xml
                //System.Xml.Serialization.XmlSerializer serLoc = new System.Xml.Serialization.XmlSerializer(typeof(AirPriceRsp));
                //string staticpath = ConfigurationManager.AppSettings["TestStaticXmlLoadPath"];
                //System.IO.StreamReader writerLoc = new StreamReader(@staticpath + "FlightRePriceResponse_6f1bdba7-abbc-413d-8eaf-233aee464c39_4008_20062018_070012.xml");
                //priceResponse = serLoc.Deserialize(writerLoc) as AirPriceRsp;
                //writerLoc.Close();

                try
                {
                    System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.Air46.AirPriceRsp));
                    System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                    System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                    serRes.Serialize(writerRes, priceResponse);     // Here Classes are converted to XML String. 
                                                                    // This can be viewed in SB or writer.
                                                                    // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument docres = new XmlDocument();
                    docres.LoadXml(sbRes.ToString());
                    string filePath = xmlPath + SessionId + "_" + AppUserId + "_FlightRePriceResponse_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    docres.Save(filePath);
                }
                catch { }


                string errorMessage = string.Empty;

                if (priceResponse != null && priceResponse.AirPriceResult != null && priceResponse.AirPriceResult.Length > 0 && priceResponse.AirPriceResult[0].AirPricingSolution != null && priceResponse.AirPriceResult[0].AirPricingSolution.Length > 0)
                {
                    if (priceResponse.ResponseMessage != null)
                    {
                        foreach (ResponseMessage responseMsg in priceResponse.ResponseMessage)
                        {
                            if (responseMsg.Type == ResponseMessageType.Error)
                            {
                                errorMessage += responseMsg.Value;
                            }
                        }
                    }

                    if (errorMessage.Length > 0)
                    {
                        throw new Exception(errorMessage);
                    }
                    else
                    {
                        decimal baseDiscount = 20;
                        //Check whether any key is specified
                        if (ConfigurationManager.AppSettings["UAPIDiscount"] != null)
                        {
                            baseDiscount = Convert.ToDecimal(ConfigurationManager.AppSettings["UAPIDiscount"]);
                        }

                        decimal baseDiscountROE = 1;
                        //Retrieve Agent Exchange rate from the xml currency
                        string currency = getCurrency(priceResponse.AirPriceResult[0].AirPricingSolution[0].TotalPrice);
                        rateOfExchange = (double)ExchangeRates[currency];
                        baseDiscountROE = ExchangeRates["AED"];//Get the ROE for Discount

                        List<Fare> FareList = new List<Fare>(fares);
                        result.Price.TaxBreakups = new List<TaxBreakup>();
                        for (int i = 0; i < priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo.Length; i++)
                        {
                            AirPricingInfo priceInfo = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[i];

                            ////////////////////////////////////////////////////////////////////////////////
                            //  Updating the BaseFare, TotalFare & SupplierFare pax type wise
                            //  Updating the PublishedFare and Tax in the Price for B2C
                            ////////////////////////////////////////////////////////////////////////////////                            

                            Fare fare = FareList.Find(delegate (Fare f) { return f.PassengerType == PassengerType.Adult && priceInfo.PassengerType[0].Code == "ADT" || f.PassengerType == PassengerType.Child && priceInfo.PassengerType[0].Code == "CNN" || f.PassengerType == PassengerType.Infant && priceInfo.PassengerType[0].Code == "INF"; });

                            fare.BaseFare = ((Convert.ToDouble(priceInfo.ApproximateBasePrice.Remove(0, 3))) * rateOfExchange) * fare.PassengerCount;
                            fare.TotalFare = ((Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3))) * rateOfExchange) * fare.PassengerCount;
                            fare.SupplierFare = Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3)) * fare.PassengerCount;
                            result.Price.PublishedFare += ((Convert.ToDecimal(priceInfo.ApproximateBasePrice.Remove(0, 3))) * (decimal)rateOfExchange);
                            result.Price.Tax += ((Convert.ToDecimal(priceInfo.Taxes.Remove(0, 3))) * (decimal)rateOfExchange);

                            if (priceInfo.TaxInfo != null)
                            {
                                foreach (typeTaxInfo tax in priceInfo.TaxInfo)
                                {
                                    if (result.Price.TaxBreakups.Exists(t => t.TaxCode == tax.Category))
                                    {
                                        TaxBreakup breakup = result.Price.TaxBreakups.Find(t => t.TaxCode == tax.Category);
                                        breakup.TaxValue += (decimal)(getCurrAmount(tax.Amount) * priceInfo.PassengerType.Length * rateOfExchange);

                                    }
                                    else
                                    {
                                        TaxBreakup breakup = new TaxBreakup();
                                        breakup.TaxCode = tax.Category;
                                        breakup.TaxValue = (decimal)(getCurrAmount(tax.Amount) * priceInfo.PassengerType.Length * rateOfExchange);
                                        result.Price.TaxBreakups.Add(breakup);
                                    }
                                }
                            }

                        }

                        result.Price.K3Tax = result.Price.TaxBreakups.Where(x => x.TaxCode == "K3").Sum(y => y.TaxValue);

                        //Get the Price details from Price Response AirPricingSolution
                        decimal newTotal = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].TotalPrice.Remove(0, 3));
                        decimal newTax = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].Taxes.Remove(0, 3));
                        decimal newBasePrice = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].ApproximateBasePrice.Remove(0, 3));

                        //Get the price details from existing AirPricingSolution
                        decimal basePrice = Convert.ToDecimal(result.UapiPricingSolution.ApproximateBasePrice.Remove(0, 3));
                        decimal Taxes = Convert.ToDecimal(result.UapiPricingSolution.Taxes.Remove(0, 3));
                        decimal Total = Convert.ToDecimal(result.UapiPricingSolution.TotalPrice.Remove(0, 3));

                        Total = Convert.ToDecimal(result.UapiPricingSolution.AirPricingInfo[0].TotalPrice.Remove(0, 3)) * priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].PassengerType.Length;
                        if (result.UapiPricingSolution.AirPricingInfo.Length > 1)
                        {
                            Total += Convert.ToDecimal(result.UapiPricingSolution.AirPricingInfo[1].TotalPrice.Remove(0, 3)) * priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[1].PassengerType.Length;
                        }
                        if (result.UapiPricingSolution.AirPricingInfo.Length > 2)
                        {
                            Total += Convert.ToDecimal(result.UapiPricingSolution.AirPricingInfo[2].TotalPrice.Remove(0, 3)) * priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[2].PassengerType.Length;
                        }
                        /******************************************************************************************
                         *      Check whether any of segments times have changed or not
                         * ****************************************************************************************/

                        //Sort the segments based on Departure Time
                        List<typeBaseAirSegment> airSegments = new List<typeBaseAirSegment>();

                        //typeBaseAirSegment[] segments = priceResponse.AirItinerary.AirSegment;
                        //Array.Sort(segments, delegate (typeBaseAirSegment s1, typeBaseAirSegment s2) { return Convert.ToDateTime(s1.DepartureTime).CompareTo(Convert.ToDateTime(s2.DepartureTime)); });
                        //airSegments.AddRange(segments);

                        priceResponse.AirItinerary.AirSegment.ToList().ForEach(x =>
                        {

                            BookingInfo bkgInfo = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo.FirstOrDefault(k => k.SegmentRef == x.Key);
                            x.CabinClass = bkgInfo.CabinClass;
                            if (x.FlightDetails.Length > 1)
                            {
                                x.FlightDetails.ToList().ForEach(y =>
                                {
                                    typeBaseAirSegment tmp = new typeBaseAirSegment();
                                    tmp.ClassOfService = x.ClassOfService; tmp.CabinClass = x.CabinClass;
                                    tmp.DepartureTime = y.DepartureTime; tmp.ArrivalTime = y.ArrivalTime;
                                    tmp.Origin = y.Origin; tmp.Destination = y.Destination; airSegments.Add(tmp);
                                });
                            }
                            else
                                airSegments.Add(x);
                        });

                        try
                        {
                            //Check whether any time difference or Booking class difference is there
                            for (int i = 0; i < airSegments.Count; i++)
                            {
                                typeBaseAirSegment aSeg = airSegments[i];
                                //BookingInfo bkgInfo = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i];
                                //Calculate departure time and arrival time excluding the time zone
                                string depTime = string.Empty;
                                if (aSeg.DepartureTime.Contains("+"))
                                {
                                    depTime = aSeg.DepartureTime.Split('+')[0];
                                }
                                else if (aSeg.DepartureTime.Split('-').Length > 3)
                                {
                                    depTime = aSeg.DepartureTime.Remove(aSeg.DepartureTime.LastIndexOf("-"));
                                }
                                else
                                {
                                    depTime = aSeg.DepartureTime;
                                }

                                string arrTime = string.Empty;
                                if (aSeg.ArrivalTime.Contains("+"))
                                {
                                    arrTime = aSeg.ArrivalTime.Split('+')[0];
                                }
                                else if (aSeg.ArrivalTime.Split('-').Length > 3)
                                {
                                    arrTime = aSeg.ArrivalTime.Remove(aSeg.ArrivalTime.LastIndexOf("-"));
                                }
                                else
                                {
                                    arrTime = aSeg.ArrivalTime;
                                }

                                /***************************************************************************************************
                                 * Whenever there is a change in the Segment details, i.e. Departure Time, Arrival Time, Booking 
                                 * Class or Cabin Class, we are updating the changed values to BookingValuesChanged string in the 
                                 * following format. ex: if searched for DXB-BKK then DXB-BOM|D|A|B|C,BOM-BKK|A|B|C
                                 * Changed values will be added segment wise seperated by comma (,) and each change value will be
                                 * seperated by pipe '|' 
                                 * ORG-DEST - Indicates the segment origin and destination
                                 * D - Indicates Departure Time is changed
                                 * A - Indicates Arrival Time is changed
                                 * B - Indicates Booking class is changed
                                 * C - Indicates Cabin class is changed
                                 * *************************************************************************************************/
                                //Check time difference for onward segments by matching the origin & destination airport codes
                                if (i < result.Flights[0].Length && aSeg.Group == result.Flights[0][i].Group && result.Flights[0][i].Origin.AirportCode == aSeg.Origin && result.Flights[0][i].Destination.AirportCode == aSeg.Destination)
                                {
                                    FlightInfo fSeg = result.Flights[0][i];

                                    if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                    {
                                        result.IsTimesChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|D";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                        }
                                        fSeg.UapiDepartureTime = aSeg.DepartureTime;
                                        fSeg.UapiArrivalTime = aSeg.ArrivalTime;
                                    }
                                    if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                    {
                                        result.IsTimesChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|A";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                        }
                                        fSeg.UapiDepartureTime = aSeg.DepartureTime;
                                        fSeg.UapiArrivalTime = aSeg.ArrivalTime;
                                    }
                                    if (fSeg.BookingClass != aSeg.ClassOfService)
                                    {
                                        result.IsBookingClassChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|B";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                        }
                                    }
                                    if (fSeg.CabinClass != aSeg.CabinClass)
                                    {
                                        result.IsBookingClassChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|C";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                        }
                                    }
                                }
                                else if (type == SearchType.MultiWay)//Check difference for Multiway segments
                                {
                                    int ret = result.Flights[0].Length;
                                    if (result.Flights[1][i - ret].Origin.AirportCode == aSeg.Origin && result.Flights[1][i - ret].Destination.AirportCode == aSeg.Destination)
                                    {
                                        FlightInfo fSeg = result.Flights[1][i - ret];
                                        if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|D";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                        }
                                        if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|A";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                        }
                                        if (fSeg.BookingClass != aSeg.ClassOfService)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|B";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                        }
                                        if (fSeg.CabinClass != aSeg.CabinClass)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|C";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                        }
                                    }
                                }
                                else if (type == SearchType.Return)//Check difference for Return segments
                                {
                                    if (result.Flights[1][i - result.Flights[0].Length].Origin.AirportCode == aSeg.Origin && result.Flights[1][i - result.Flights[0].Length].Destination.AirportCode == aSeg.Destination)
                                    {
                                        FlightInfo fSeg = result.Flights[1][i - result.Flights[0].Length];
                                        if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|D";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                        }
                                        if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|A";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                        }
                                        if (fSeg.BookingClass != aSeg.ClassOfService)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|B";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                        }
                                        if (fSeg.CabinClass != aSeg.CabinClass)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|C";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to compare dates in UAPI Reprice. " + ex.ToString(), "");
                            result.RepriceErrorMessage = "There is a technical error while confirming your price. Please Search again";
                        }

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //
                        //  If any of the values are not same, i.e. Base Price, Taxes , Total, Departure Time, Arrival Time or Booking Class
                        //  update the segment details and AirPricingSolution
                        //
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        /********************************************************************************************************
                         *  Update the AirSegmentDetails returned from Price Response and update the result Flight Segments     *
                         *  along with UapiPricingSolution. These values are required to pass in the Reservation Request.       *
                         *  Values that are updated include                                                                     *
                         *  1.  Segment Key                                                                                      * 
                         *  2.  Departure Time
                         *  3.  Arrival Time
                         *  4.  Booking Class
                         *  5.  Cabin Class
                         *  5.  Duration
                         *  6.  Group
                         *  7.  Flight Number
                         *  8.  Link Availability
                         *  9.  Availability Source
                         *  10. Polled Availability Option
                         *  11. Provider Code
                         *  12. Segment Ref Key
                         *  13. Code Share Info                                                                                 *    
                        * *******************************************************************************************************/


                        //Update details always
                        //if (Total != newTotal || result.IsTimesChanged || result.IsBookingClassChanged)
                        {
                            //Assign the new UapiPricingSolution returned from the pricing response.
                            result.UapiPricingSolution = priceResponse.AirPriceResult[0].AirPricingSolution[0];
                            //Assign the segments returned from the price response bcoz segments will be empty in UapiPricingSolution
                            result.UapiPricingSolution.AirSegment = priceResponse.AirItinerary.AirSegment;
                            //Formation of Default baggage in Re-Price.
                            result = DefaultBaggageFormation(priceResponse, result);

                            foreach (UAPIdll.Air46.AirPricingInfo price in result.UapiPricingSolution.AirPricingInfo)
                            {
                                price.BaggageAllowances = null;//Remove the baggage info 

                                foreach (UAPIdll.Air46.FareInfo fare in price.FareInfo)
                                {
                                    fare.Endorsement = new Endorsement[0];//Remove any endorsements                                    
                                    fare.Brand = null;//Clear any Branding
                                }
                            }

                            //Add all Flight segments to update the segment info from the response
                            List<FlightInfo> FlightSegments = new List<FlightInfo>();
                            FlightSegments.AddRange(result.Flights[0]);
                            if (result.Flights.Length > 1)
                            {
                                FlightSegments.AddRange(result.Flights[1]);
                            }

                            //Update the result segments as per segment details
                            for (int i = 0; i < priceResponse.AirItinerary.AirSegment.Length; i++)
                            {
                                typeBaseAirSegment segment = priceResponse.AirItinerary.AirSegment[i];

                                for (int j = 0; j < segment.FlightDetails.Length; j++)
                                {
                                    FlightDetails flightDetail = segment.FlightDetails[j];
                                    //Calculate departure time and arrival time excluding the time zone
                                    string depTime = string.Empty;
                                    //if (segment.DepartureTime.Contains("+"))
                                    if (flightDetail.DepartureTime.Contains("+"))
                                    {
                                        depTime = flightDetail.DepartureTime.Split('+')[0];
                                    }
                                    //else if (segment.DepartureTime.Split('-').Length > 3)
                                    else if (flightDetail.DepartureTime.Split('-').Length > 3)
                                    {
                                        depTime = flightDetail.DepartureTime.Remove(flightDetail.DepartureTime.LastIndexOf("-"));
                                    }
                                    else
                                    {
                                        depTime = flightDetail.DepartureTime;
                                    }

                                    string arrTime = string.Empty;
                                    //if (segment.ArrivalTime.Contains("+"))
                                    if (flightDetail.ArrivalTime.Contains("+"))
                                    {
                                        arrTime = flightDetail.ArrivalTime.Split('+')[0];
                                    }
                                    //else if (segment.ArrivalTime.Split('-').Length > 3)
                                    else if (flightDetail.ArrivalTime.Split('-').Length > 3)
                                    {
                                        arrTime = flightDetail.ArrivalTime.Remove(flightDetail.ArrivalTime.LastIndexOf("-"));
                                    }
                                    else
                                    {
                                        arrTime = flightDetail.ArrivalTime;
                                    }

                                    //Assign onward segment details 
                                    //if (i < result.Flights[0].Length && result.Flights[0][j].Origin.AirportCode == segment.Origin && result.Flights[0][j].Destination.AirportCode == segment.Destination)

                                    //Retrieve Segment info
                                    FlightInfo fInfo = FlightSegments.Find(delegate (FlightInfo f) { return f.Origin.AirportCode == flightDetail.Origin && f.Destination.AirportCode == flightDetail.Destination; });
                                    if (fInfo != null)
                                    {
                                        fInfo.UapiSegmentRefKey = segment.Key;
                                        fInfo.BookingClass = segment.ClassOfService;
                                        fInfo.DepartureTime = Convert.ToDateTime(depTime);
                                        fInfo.ArrivalTime = Convert.ToDateTime(arrTime);
                                        fInfo.UapiArrivalTime = segment.ArrivalTime;//Always update AirSegment times in order to send in AirReservationReq
                                        fInfo.UapiDepartureTime = segment.DepartureTime;
                                        fInfo.Duration = new TimeSpan(0, Convert.ToInt32(flightDetail.FlightTime), 0);
                                        fInfo.Group = segment.Group;
                                        fInfo.FlightNumber = segment.FlightNumber;
                                        fInfo.CabinClass = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo[0].CabinClass;

                                        fInfo.UAPIReservationValues[UAPI.LINK_AVAILABILITY] = segment.LinkAvailability;
                                        fInfo.UAPIReservationValues[UAPI.POLLED_AVAILABILITY_OPTION] = segment.PolledAvailabilityOption;
                                        fInfo.UAPIReservationValues[UAPI.AVAILABILITY_SOURCE] = segment.AvailabilitySource;
                                        fInfo.UAPIReservationValues[UAPI.PROVIDER_CODE] = segment.ProviderCode;
                                        fInfo.UAPIReservationValues[UAPI.SEGMENT_REF] = segment.Key;
                                        if (segment.CodeshareInfo != null && segment.CodeshareInfo.OperatingCarrier != fInfo.Airline)
                                        {
                                            fInfo.OperatingCarrier = segment.CodeshareInfo.OperatingCarrier + "-" + segment.CodeshareInfo.OperatingFlightNumber + " " + segment.CodeshareInfo.Value;
                                        }
                                    }
                                    //else if (type != SearchType.OneWay)//Assign Multiway & Return segment details
                                    //{
                                    //    int ret = result.Flights[0].Length;
                                    //    //if (result.Flights[1][j - ret].Origin.AirportCode == segment.Origin && result.Flights[1][j - ret].Destination.AirportCode == segment.Destination)
                                    //    if (result.Flights[1][j - ret].Origin.AirportCode == flightDetail.Origin && result.Flights[1][j - ret].Destination.AirportCode == flightDetail.Destination)
                                    //    {
                                    //        result.Flights[1][j - ret].UapiSegmentRefKey = segment.Key;
                                    //        result.Flights[1][j - ret].BookingClass = segment.ClassOfService;
                                    //        result.Flights[1][j - ret].ArrivalTime = Convert.ToDateTime(arrTime);
                                    //        result.Flights[1][j - ret].DepartureTime = Convert.ToDateTime(depTime);
                                    //        result.Flights[1][j - ret].UapiArrivalTime = flightDetail.ArrivalTime;
                                    //        result.Flights[1][j - ret].UapiDepartureTime = flightDetail.DepartureTime;
                                    //        result.Flights[1][j - ret].Duration = new TimeSpan(0, Convert.ToInt32(segment.FlightTime), 0);
                                    //        result.Flights[1][j - ret].Group = segment.Group;
                                    //        result.Flights[1][j - ret].FlightNumber = segment.FlightNumber;
                                    //        result.Flights[1][j - ret].CabinClass = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo[0].CabinClass;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.LINK_AVAILABILITY] = segment.LinkAvailability;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.POLLED_AVAILABILITY_OPTION] = segment.PolledAvailabilityOption;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.AVAILABILITY_SOURCE] = segment.AvailabilitySource;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.PROVIDER_CODE] = segment.ProviderCode;
                                    //        result.Flights[1][j - ret].UAPIReservationValues[UAPI.SEGMENT_REF] = segment.Key;
                                    //        if (segment.CodeshareInfo != null && segment.CodeshareInfo.OperatingCarrier != result.Flights[1][j - ret].Airline)
                                    //        {
                                    //            result.Flights[1][j - ret].OperatingCarrier = segment.CodeshareInfo.OperatingCarrier + "-" + segment.CodeshareInfo.OperatingFlightNumber + " " + segment.CodeshareInfo.Value;
                                    //        }
                                    //    }
                                    //}

                                }
                            }
                        }
                    }
                }
            }
            catch (SoapException se)
            {
                result.RepriceErrorMessage = se.Message;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "(UAPI Reprice)" + ex.ToString(), "");
                result.RepriceErrorMessage = "There is a technical error while confirming your price. Please Search again";
            }

            return priceResponse;
        }

        /// <summary>
        /// To modify universal record info 
        /// </summary>
        /// <param name="clsURModReq"></param>
        /// <param name="clsURModRsp"></param>
        public void ModifyUR(UAPIdll.Universal46.UniversalRecordModifyReq clsURModReq, ref UAPIdll.Universal46.UniversalRecordModifyRsp clsURModRsp, UAPICredentials clsUAC)
        {
            try
            {
                Connection();

                clsURModReq.TargetBranch = clsUAC.TargetBranch;
                clsURModReq.RecordIdentifier.ProviderCode = ProviderCode;

                clsURModReq.BillingPointOfSaleInfo = new UAPIdll.Universal46.BillingPointOfSaleInfo();
                clsURModReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";

                UAPIdll.Universal46.UniversalRecordModifyServiceBinding binding = new UAPIdll.Universal46.UniversalRecordModifyServiceBinding();
                binding.Url = urlUR;
                binding.Credentials = new NetworkCredential(clsUAC.UserName, clsUAC.Password);
                binding.PreAuthenticate = true;

                GenericStatic.WriteLogFile(typeof(UAPIdll.Universal46.UniversalRecordModifyReq), clsURModReq,
                    xmlPath + SessionId + "_" + AppUserId + "_" + "UAPI_URModifyReq_");

                clsURModRsp = binding.service(clsURModReq);

                GenericStatic.WriteLogFile(typeof(UAPIdll.Universal46.UniversalRecordModifyRsp), clsURModRsp,
                    xmlPath + SessionId + "_" + AppUserId + "_" + "UAPI_URModifyRsp_");
            }
            catch (SoapException se)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "(UAPI UR Modify soap error : )" + se.ToString(), "");
                throw se;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "(UAPI UR Modify error : )" + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// To reprice and update the price and segment info based on dynamic PCC credentials
        /// </summary>
        /// <param name="clsFLI"></param>
        /// <param name="clsCredNew"></param>
        /// <param name="clsCredOld"></param>
        /// <param name="clsURImpRsp"></param>
        /// <param name="priceResponse"></param>
        /// <returns></returns>
        public FlightItinerary RepriceDPCCPNR(FlightItinerary clsFLI, UAPICredentials clsCredNew,
            UAPICredentials clsCredOld, ref UAPIdll.Universal46.UniversalRecordImportRsp clsURImpRsp, ref AirPriceRsp priceResponse)
        {
            FlightItinerary clsFLIUAPI = new FlightItinerary();

            try
            {
                //clsFLIUAPI.Segments = clsFLI.Segments; clsFLIUAPI.Passenger = clsFLI.Passenger; clsFLIUAPI.FlightBookingSource = clsFLI.FlightBookingSource;
                clsFLIUAPI = JsonConvert.DeserializeObject<FlightItinerary>(JsonConvert.SerializeObject(clsFLI));
                bool iIsPriceUpdate = clsURImpRsp.UniversalRecord != null;
                if (clsURImpRsp.UniversalRecord == null)
                    clsURImpRsp = ImportUR(clsFLI.PNR, clsCredOld);

                if (clsURImpRsp == null || clsURImpRsp.UniversalRecord == null || clsURImpRsp.UniversalRecord.Items == null)
                    throw new Exception("PNR Info is not available.");

                if (clsURImpRsp.UniversalRecord.Items[0].AirPricingInfo == null)
                    throw new Exception("Price info is not available for this PNR.");

                if (clsURImpRsp.UniversalRecord.Items[0].DocumentInfo != null && clsURImpRsp.UniversalRecord.Items[0].DocumentInfo.TicketInfo != null
                    && clsURImpRsp.UniversalRecord.Items[0].DocumentInfo.TicketInfo.Length > 0)
                {
                    clsFLIUAPI.Ticketed = true;
                    Ticket[] ticket = null;
                    clsFLIUAPI = MakeBookingObject(clsURImpRsp, out ticket);
                    return clsFLIUAPI;
                }

                if (priceResponse == null)
                {
                    AirPriceReq priceReq = new AirPriceReq();
                    priceReq.AirItinerary = new AirItinerary();

                    priceReq.AirItinerary.AirSegment = new typeBaseAirSegment[clsFLI.Segments.Length];
                    priceReq.AirPricingCommand = new AirPricingCommand[1];
                    priceReq.AirPricingCommand[0] = new AirPricingCommand();
                    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers = new AirSegmentPricingModifiers[clsFLI.Segments.Length];

                    for (int i = 0; i < clsFLI.Segments.Length; i++)
                    {
                        priceReq.AirItinerary.AirSegment[i] = new typeBaseAirSegment();
                        priceReq.AirItinerary.AirSegment[i].Key = i.ToString();
                        priceReq.AirItinerary.AirSegment[i].ProviderCode = ProviderCode;
                        priceReq.AirItinerary.AirSegment[i].Group = clsFLI.Segments[i].Group;
                        priceReq.AirItinerary.AirSegment[i].Carrier = clsFLI.Segments[i].Airline;
                        priceReq.AirItinerary.AirSegment[i].FlightNumber = clsFLI.Segments[i].FlightNumber;
                        priceReq.AirItinerary.AirSegment[i].Origin = clsFLI.Segments[i].Origin.AirportCode;
                        priceReq.AirItinerary.AirSegment[i].Destination = clsFLI.Segments[i].Destination.AirportCode;

                        string DepTime = !string.IsNullOrEmpty(clsFLI.Segments[i].UapiDepartureTime) ? clsFLI.Segments[i].UapiDepartureTime :
                            clsURImpRsp.UniversalRecord.Items[0].AirSegment.Where(x => x.Origin == clsFLI.Segments[i].Origin.AirportCode &&
                            x.Destination == clsFLI.Segments[i].Destination.AirportCode).Select(y => y.DepartureTime).FirstOrDefault();

                        priceReq.AirItinerary.AirSegment[i].DepartureTime = DepTime;

                        string ArrTime = !string.IsNullOrEmpty(clsFLI.Segments[i].UapiArrivalTime) ? clsFLI.Segments[i].UapiArrivalTime :
                            clsURImpRsp.UniversalRecord.Items[0].AirSegment.Where(x => x.Origin == clsFLI.Segments[i].Origin.AirportCode &&
                            x.Destination == clsFLI.Segments[i].Destination.AirportCode).Select(y => y.ArrivalTime).FirstOrDefault();

                        priceReq.AirItinerary.AirSegment[i].ArrivalTime = ArrTime;
                        priceReq.AirItinerary.AirSegment[i].ClassOfService = clsFLI.Segments[i].BookingClass;

                        priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i] = new AirSegmentPricingModifiers();
                        priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].AirSegmentRef = i.ToString();
                        priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes = new BookingCode[1];
                        priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes[0] = new BookingCode();
                        priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes[0].Code = clsFLI.Segments[i].BookingClass;
                    }

                    priceReq.BillingPointOfSaleInfo = new BillingPointOfSaleInfo();
                    priceReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                    priceReq.CheckFlightDetails = true;
                    priceReq.FareRuleType = typeFareRuleType.@short;
                    priceReq.CheckOBFees = "true";

                    priceReq.SearchPassenger = new SearchPassenger[clsFLI.Passenger.Length];
                    for (int p = 0; p < clsFLI.Passenger.Length; p++)
                    {
                        priceReq.SearchPassenger[p] = new SearchPassenger();
                        priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                        priceReq.SearchPassenger[p].Code = clsFLI.Passenger[p].Type == PassengerType.Adult ? "ADT" : clsFLI.Passenger[p].Type == PassengerType.Child ? "CNN" : "INF";
                        priceReq.SearchPassenger[p].Age = clsFLI.Passenger[p].Type == PassengerType.Adult ? "40" : clsFLI.Passenger[p].Type == PassengerType.Child ? "5" : "1";
                    }

                    priceReq.AirPricingModifiers = new AirPricingModifiers();
                    priceReq.AirPricingModifiers.InventoryRequestType = typeInventoryRequest.Basic;
                    priceReq.AirPricingModifiers.AccountCodeFaresOnly = true;

                    priceReq.AirPricingModifiers.PermittedCabins = new UAPIdll.Air46.CabinClass[1];
                    priceReq.AirPricingModifiers.PermittedCabins[0] = new UAPIdll.Air46.CabinClass();
                    priceReq.AirPricingModifiers.PermittedCabins[0].Type = clsFLI.Segments[0].CabinClass;

                    if (!string.IsNullOrEmpty(PrivateFareCodes))
                    {
                        var arrCodes = PrivateFareCodes.Split(',');
                        priceReq.AirPricingModifiers.AccountCodes = new AccountCode[arrCodes.Length];
                        for (int i = 0; i < arrCodes.Length; i++)
                        {
                            priceReq.AirPricingModifiers.AccountCodes[i] = new AccountCode();
                            priceReq.AirPricingModifiers.AccountCodes[i].Code = arrCodes[i];
                            priceReq.AirPricingModifiers.AccountCodes[i].ProviderCode = ProviderCode;
                        }
                    }

                    priceReq.TargetBranch = clsCredNew.TargetBranch;

                    string sOrigin = clsFLI.Segments.ToList().Select(x => x.Origin.AirportCode).FirstOrDefault();
                    string sDestination = clsFLI.Segments.ToList().Select(x => x.Destination.AirportCode).Last();
                    sDestination = sDestination != sOrigin ? sDestination : clsFLI.Segments.Where(s => s.Group == 0).ToList().Select(x => x.Destination.AirportCode).Last();

                    GenericStatic.WriteLogFile(typeof(AirPriceReq), priceReq,
                        xmlPath + SessionId + "_" + AppUserId + "_" + sOrigin + "_" + sDestination + "_" + "DynamicPCCRepriceReq_");

                    AirPriceBinding binding = new AirPriceBinding();
                    binding.Url = urlAir;
                    binding.Credentials = new NetworkCredential(clsCredNew.UserName, clsCredNew.Password);
                    binding.PreAuthenticate = true;

                    priceResponse = binding.service(priceReq);

                    GenericStatic.WriteLogFile(typeof(AirPriceRsp), priceResponse,
                        xmlPath + SessionId + "_" + AppUserId + "_" + sOrigin + "_" + sDestination + "_" + "DynamicPCCRepriceRsp_");
                }

                if (iIsPriceUpdate)
                {
                    GenerateModifyUR(clsFLI, clsCredNew, priceResponse, "DEL", clsURImpRsp);
                    clsURImpRsp.UniversalRecord = null; Ticket[] ticket = null;
                    clsFLIUAPI = RetrieveItinerary(clsFLI.PNR, out ticket, clsCredNew, ref clsURImpRsp);
                }
                else
                    ModifyItineraryPrice(clsFLIUAPI, priceResponse);
            }
            catch (SoapException se)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "(UAPI RepriceDPCCPNR)" + se.ToString(), "");
                throw se;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "(UAPI RepriceDPCCPNR)" + ex.ToString(), "");
                throw ex;
            }

            return clsFLIUAPI;
        }

        /// <summary>
        /// To Modify the UR record price and segment info
        /// </summary>
        /// <param name="clsFLI"></param>
        /// <param name="clsCredentials"></param>
        /// <param name="priceResponse"></param>
        /// <param name="sAction"></param>
        /// <param name="URAirResrv"></param>
        public void GenerateModifyUR(FlightItinerary clsFLI, UAPICredentials clsUAC, AirPriceRsp priceResponse, string sAction,
            UAPIdll.Universal46.UniversalRecordImportRsp URAirResrv)
        {
            try
            {
                UAPIdll.Universal46.UniversalRecordModifyReq clsURMReq = new UAPIdll.Universal46.UniversalRecordModifyReq();

                clsURMReq.RecordIdentifier = new UAPIdll.Universal46.RecordIdentifier();
                clsURMReq.RecordIdentifier.UniversalLocatorCode = clsFLI.UniversalRecord;
                clsURMReq.RecordIdentifier.ProviderLocatorCode = clsFLI.PNR;

                int iPriceCnt = URAirResrv.UniversalRecord.Items[0].AirPricingInfo.Length;
                int iSegCnt = URAirResrv.UniversalRecord.Items[0].AirSegment.Length;
                clsURMReq.UniversalModifyCmd = new UAPIdll.Universal46.UniversalModifyCmd[iSegCnt + iPriceCnt + 2];

                for (int i = 0; i < iSegCnt; i++)
                {
                    clsURMReq.UniversalModifyCmd[i] = new UAPIdll.Universal46.UniversalModifyCmd();
                    clsURMReq.UniversalModifyCmd[i] =
                        GetModifyURDel((i + 1).ToString(), clsFLI.AirLocatorCode, URAirResrv.UniversalRecord.Items[0].AirSegment[i].Key, "S");
                }

                for (int i = 0; i < iPriceCnt; i++)
                {
                    clsURMReq.UniversalModifyCmd[iSegCnt + i] = new UAPIdll.Universal46.UniversalModifyCmd();
                    clsURMReq.UniversalModifyCmd[iSegCnt + i] =
                        GetModifyURDel((iSegCnt + 1 + i).ToString(), clsFLI.AirLocatorCode, URAirResrv.UniversalRecord.Items[0].AirPricingInfo[i].Key, "P");
                }

                clsURMReq.UniversalModifyCmd[iSegCnt + iPriceCnt] = GetModifyURAdd((iSegCnt + iPriceCnt + 1).ToString(), priceResponse, clsFLI.AirLocatorCode, "S");
                clsURMReq.UniversalModifyCmd[iSegCnt + iPriceCnt + 1] = GetModifyURAdd((iSegCnt + iPriceCnt + 2).ToString(), priceResponse, clsFLI.AirLocatorCode, "P");

                clsURMReq.Version = URAirResrv.UniversalRecord.Version; clsURMReq.ReturnRecord = true;

                string sOrigin = clsFLI.Segments.ToList().Select(x => x.Origin.AirportCode).FirstOrDefault();
                string sDestination = clsFLI.Segments.ToList().Select(x => x.Destination.AirportCode).Last();
                sDestination = sDestination != sOrigin ? sDestination : clsFLI.Segments.Where(s => s.Group == 0).ToList().Select(x => x.Destination.AirportCode).Last();

                SessionId += "_" + sOrigin + "_" + sDestination;

                UAPIdll.Universal46.UniversalRecordModifyRsp clsURModRsp = new UAPIdll.Universal46.UniversalRecordModifyRsp();
                ModifyUR(clsURMReq, ref clsURModRsp, clsUAC);
            }
            catch (SoapException se)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "(UAPI ModifyUR)" + se.ToString(), "");
                throw se;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "(UAPI ModifyUR)" + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// To get UR modify command object for price and segments delete
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sAirURLCode"></param>
        /// <param name="PriceKey"></param>
        /// <param name="sEType"></param>
        /// <returns></returns>
        public UAPIdll.Universal46.UniversalModifyCmd GetModifyURDel(string sKey, string sAirURLCode, string PriceKey, string sEType)
        {
            try
            {
                UAPIdll.Universal46.UniversalModifyCmd clsURMDelCmd = new UAPIdll.Universal46.UniversalModifyCmd();

                UAPIdll.Universal46.AirDelete clsAirDel = new UAPIdll.Universal46.AirDelete();
                clsAirDel.ReservationLocatorCode = sAirURLCode;
                clsAirDel.Element = sEType == "P" ? UAPIdll.Universal46.typeElement.AirPricingInfo : UAPIdll.Universal46.typeElement.AirSegment;
                clsAirDel.Key = PriceKey;
                clsURMDelCmd.Key = sKey;
                clsURMDelCmd.Item = clsAirDel;

                return clsURMDelCmd;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get UR modify command object for price and segments add
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="priceResponse"></param>
        /// <param name="sAirURLCode"></param>
        /// <param name="sType"></param>
        /// <returns></returns>
        public UAPIdll.Universal46.UniversalModifyCmd GetModifyURAdd(string sKey, AirPriceRsp priceResponse, string sAirURLCode, string sType)
        {
            try
            {
                UAPIdll.Universal46.UniversalModifyCmd clsURMAddCmd = new UAPIdll.Universal46.UniversalModifyCmd();
                UAPIdll.Universal46.AirAdd clsAirAdd = new UAPIdll.Universal46.AirAdd();

                if (sType == "S")
                {
                    string sSegInfo = JsonConvert.SerializeObject(priceResponse.AirItinerary.AirSegment);
                    UAPIdll.Universal46.typeBaseAirSegment[] arrAirSeg = JsonConvert.DeserializeObject<UAPIdll.Universal46.typeBaseAirSegment[]>(sSegInfo);
                    clsAirAdd.Items = arrAirSeg;
                }
                else
                {
                    string sPriceInfo = JsonConvert.SerializeObject(priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo);
                    UAPIdll.Universal46.AirPricingInfo[] arrPriceInfo = JsonConvert.DeserializeObject<UAPIdll.Universal46.AirPricingInfo[]>(sPriceInfo);
                    arrPriceInfo.Where(f => f != null).ToList().ForEach(x => x.FareInfo.Where(b => b != null && b.Brand != null).ToList()
                        .ForEach(y => { y.Brand.OptionalServices = null; }));
                    clsAirAdd.Items = arrPriceInfo;
                }

                clsAirAdd.ReservationLocatorCode = sAirURLCode;
                clsURMAddCmd.Key = sKey;
                clsURMAddCmd.Item = clsAirAdd;

                return clsURMAddCmd;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To modify itinerary price based on air price response with dynamic PCC credentials
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="priceResponse"></param>
        /// <returns></returns>
        public FlightItinerary ModifyItineraryPrice(FlightItinerary itinerary, AirPriceRsp priceResponse)
        {
            if (priceResponse.AirPriceResult == null || priceResponse.AirPriceResult[0].AirPricingSolution == null)
                throw new Exception("Price info is not available for this PNR.");

            try
            {
                AirPricingSolution clsAPS = priceResponse.AirPriceResult[0].AirPricingSolution[0];

                foreach (AirPricingInfo pricingInfo in clsAPS.AirPricingInfo)
                {
                    decimal baseFare = 0m, totalFare = 0m;
                    string currency = getCurrency(pricingInfo.TotalPrice);
                    rateOfExchange = ExchangeRates.ContainsKey(currency) ? Convert.ToDouble(ExchangeRates[currency]) : 1;

                    baseFare = (decimal)getCurrAmount(pricingInfo.ApproximateBasePrice) * Convert.ToDecimal(rateOfExchange);
                    totalFare = (decimal)getCurrAmount(pricingInfo.TotalPrice) * Convert.ToDecimal(rateOfExchange);

                    itinerary.Passenger.Where(x => x.Type == (pricingInfo.PassengerType[0].Code == "ADT" ? PassengerType.Adult :
                    pricingInfo.PassengerType[0].Code == "CNN" ? PassengerType.Child : PassengerType.Infant)).ToList().ForEach
                    (y =>
                    {

                        y.Price = new PriceAccounts();
                        y.Price.PublishedFare = baseFare;
                        y.Price.Tax = totalFare - baseFare;
                        y.Price.RateOfExchange = (decimal)rateOfExchange;
                        y.Price.Currency = agentBaseCurrency;
                        y.Price.CurrencyCode = agentBaseCurrency;
                        y.Price.SupplierPrice = (decimal)getCurrAmount(pricingInfo.TotalPrice);
                        y.Price.SupplierCurrency = pricingInfo.BasePrice.Substring(0, 3);
                        y.Price.AccPriceType = PriceType.PublishedFare;
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return itinerary;
        }

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);

        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
                GC.Collect();
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

        //Default Baggage formation segment wise after Reprice in UAPI
        public SearchResult DefaultBaggageFormation(AirPriceRsp priceResponse, SearchResult result)
        {
            try
            {
                result.BaggageIncludedInFare = string.Empty;
                string baggage = string.Empty;
                //Reading BaggageAllowanceInfo for Default Baggage pax wise.
                priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo.ToList().ForEach(p =>
                {
                    result.BaggageIncludedInFare = !string.IsNullOrEmpty(result.BaggageIncludedInFare) ? result.BaggageIncludedInFare + "|" : result.BaggageIncludedInFare;
                    p.BaggageAllowances.BaggageAllowanceInfo.ToList().ForEach(b =>
                    {
                        if (b.TextInfo != null && b.TextInfo.Length > 0 && b.TextInfo[0] != null && b.TextInfo[0].Text != null && b.TextInfo[0].Text.Length > 0 && b.TextInfo[0].Text[0] != null)
                        {
                            baggage = b.TextInfo[0].Text[0].ToLower().Contains("p") ? b.TextInfo[0].Text[0].ToLower().Replace("p", "") + " piece" : b.TextInfo[0].Text[0].ToLower().Replace("k", "") + " kg";
                        }
                        result.BaggageIncludedInFare += string.IsNullOrEmpty(result.BaggageIncludedInFare) ? baggage : "," + baggage;
                    });
                });

                if (!string.IsNullOrEmpty(result.BaggageIncludedInFare))
                {
                    string baggageCode = string.Empty;
                    string[] defaultbagPaxWise = result.BaggageIncludedInFare.Split('|').ToArray();
                    string defaultBaggageCodes = string.Empty;
                    //conversion of  pax wise baggage to segment wise & appending "|" separating the default baggage and storing in defaultBaggageCodes.
                    for (int i = 0; i < result.Flights.Length; i++)
                    {
                        defaultBaggageCodes = !string.IsNullOrEmpty(defaultBaggageCodes) ? defaultBaggageCodes + "|" : defaultBaggageCodes;
                        for (int j = 0; j < defaultbagPaxWise.Length; j++)
                        {
                            defaultBaggageCodes += string.IsNullOrEmpty(defaultBaggageCodes) ? defaultbagPaxWise[j].TrimStart(',').Split(',')[i] : "," + defaultbagPaxWise[j].TrimStart(',').Split(',')[i];
                        }
                    }
                    //Assigning default baggage segment wise 
                    for (int i = 0; i < result.Flights.Length; i++)
                    {
                        for (int j = 0; j < result.Flights[i].Length; j++)
                        {
                            result.Flights[i][j].DefaultBaggage = defaultBaggageCodes.Split('|')[i].TrimStart(',');
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to get default Baggage in UAPI Reprice. " + ex.ToString(), "");
            }
            return result;
        }

        #region Terminal Command Methods
        
        /// <summary>
        /// Check whether user has terminal access is there or not. Returns True if HostToken value is returned otherwise return False.
        /// </summary>
        /// <returns></returns>
        public bool CheckForTerminalAccess()
        {
            bool allowed = false;
            
            try
            {
                if (IsTerminalAccessAllowed)
                {
                    Connection();
                    UAPIdll.Terminal46.CreateTerminalSessionServiceBinding binding = new UAPIdll.Terminal46.CreateTerminalSessionServiceBinding();
                    binding.Url = urlTerminal;
                    binding.Credentials = new NetworkCredential(UserName, Password);
                    binding.PreAuthenticate = true;

                    UAPIdll.Terminal46.CreateTerminalSessionReq terminalSessionReq = new UAPIdll.Terminal46.CreateTerminalSessionReq();
                    terminalSessionReq.BillingPointOfSaleInfo = new UAPIdll.Terminal46.BillingPointOfSaleInfo();
                    terminalSessionReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                    terminalSessionReq.TargetBranch = TargetBranch;
                    terminalSessionReq.Host = "1G";
                    terminalSessionReq.TraceId = "TerminalSession-TraceId";

                    GenericStatic.WriteLogFile(typeof(UAPIdll.Terminal46.CreateTerminalSessionReq), terminalSessionReq,
                                xmlPath + SessionId + "_" + AppUserId + "_" + "CreateTerminalSessionReq_");

                    UAPIdll.Terminal46.CreateTerminalSessionRsp terminalSessionRsp = binding.service(terminalSessionReq);

                    GenericStatic.WriteLogFile(typeof(UAPIdll.Terminal46.CreateTerminalSessionRsp), terminalSessionRsp,
                                xmlPath + SessionId + "_" + AppUserId + "_" + "CreateTerminalSessionRsp_");

                    if (terminalSessionRsp != null && terminalSessionRsp.HostToken != null && !string.IsNullOrEmpty(terminalSessionRsp.HostToken.Value))
                    {
                        TerminalHostToken = terminalSessionRsp.HostToken.Value;
                        allowed = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to check for the uapi terminal access. Reason: " + ex.ToString(), "");
            }

            return allowed;
        }
        
        /// <summary>
        /// Pass the command to get the response. Replaces special characters ";gt ;lt < > )" from the response.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string GetTerminalAPIResponse(string command)
        {
            string response = string.Empty;

            try
            {
                if (IsTerminalAccessAllowed && !string.IsNullOrEmpty(TerminalHostToken))
                {
                    Connection();
                    UAPIdll.Terminal46.TerminalServiceBinding binding = new UAPIdll.Terminal46.TerminalServiceBinding();
                    binding.Url = urlTerminal;
                    binding.Credentials = new NetworkCredential(UserName, Password);
                    binding.PreAuthenticate = true;


                    UAPIdll.Terminal46.TerminalReq terminalReq = new UAPIdll.Terminal46.TerminalReq();
                    terminalReq.TerminalCommand = command;
                    terminalReq.BillingPointOfSaleInfo = new UAPIdll.Terminal46.BillingPointOfSaleInfo();
                    terminalReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                    terminalReq.TargetBranch = TargetBranch;
                    terminalReq.HostToken = new UAPIdll.Terminal46.HostToken();
                    terminalReq.HostToken.Host = "1G";
                    terminalReq.HostToken.Value = TerminalHostToken;

                    GenericStatic.WriteLogFile(typeof(UAPIdll.Terminal46.TerminalReq), terminalReq,
                                xmlPath + SessionId + "_" + AppUserId + "_" + "TerminalReq_");

                    UAPIdll.Terminal46.TerminalRsp terminalRsp = binding.service(terminalReq);

                    GenericStatic.WriteLogFile(typeof(UAPIdll.Terminal46.TerminalRsp), terminalRsp,
                                xmlPath + SessionId + "_" + AppUserId + "_" + "TerminalRsp_");

                    if (terminalRsp != null && terminalRsp.TerminalCommandResponse != null)
                    {
                        List<string> commandResponse = new List<string>(terminalRsp.TerminalCommandResponse);

                        commandResponse.ForEach(x =>
                        {
                            x = x.Replace("&gt;", "");
                            x = x.Replace("&lt;", "");
                            x = x.Replace(">", "");
                            x = x.Replace("<", "");
                            //x = x.Replace(")", "");
                            x = Environment.NewLine + x;
                            response += x;
                        });
                    }

                }

            }
            catch (Exception ex)
            {
                response = Environment.NewLine + ex.Message;
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get the response for terminal command: " + command + ". Reason: " + ex.ToString(), "");
            }
            return response;
        }
       
        /// <summary>
        /// Stop the session and returns the message.
        /// </summary>
        /// <returns></returns>
        public string EndTerminalSession()
        {
            string response = string.Empty;
            try
            {
                if (IsTerminalAccessAllowed && !string.IsNullOrEmpty(TerminalHostToken))
                {
                    Connection();
                    UAPIdll.Terminal46.EndTerminalSessionServiceBinding binding = new UAPIdll.Terminal46.EndTerminalSessionServiceBinding();
                    binding.Url = urlTerminal;
                    binding.Credentials = new NetworkCredential(UserName, Password);
                    binding.PreAuthenticate = true;

                    UAPIdll.Terminal46.EndTerminalSessionReq terminalSessionReq = new UAPIdll.Terminal46.EndTerminalSessionReq();
                    terminalSessionReq.BillingPointOfSaleInfo = new UAPIdll.Terminal46.BillingPointOfSaleInfo();
                    terminalSessionReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                    terminalSessionReq.TargetBranch = TargetBranch;
                    terminalSessionReq.HostToken = new UAPIdll.Terminal46.HostToken();
                    terminalSessionReq.HostToken.Host = "1G";
                    terminalSessionReq.HostToken.Value = TerminalHostToken;


                    GenericStatic.WriteLogFile(typeof(UAPIdll.Terminal46.EndTerminalSessionReq), terminalSessionReq,
                                xmlPath + SessionId + "_" + AppUserId + "_" + "EndTerminalSessionReq_");

                    UAPIdll.Terminal46.EndTerminalSessionRsp terminalSessionRsp = binding.service(terminalSessionReq);

                    GenericStatic.WriteLogFile(typeof(UAPIdll.Terminal46.EndTerminalSessionRsp), terminalSessionRsp,
                                xmlPath + SessionId + "_" + AppUserId + "_" + "EndTerminalSessionRsp_");

                    if (terminalSessionRsp != null && terminalSessionRsp.ResponseMessage != null)
                    {
                        terminalSessionRsp.ResponseMessage.ToList().ForEach(x =>
                        {
                            switch (x.Type)
                            {
                                case UAPIdll.Terminal46.ResponseMessageType.Error:
                                    response += Environment.NewLine + "Error: " + x.Value;
                                    break;
                                case UAPIdll.Terminal46.ResponseMessageType.Info:
                                    response += Environment.NewLine + "Info: " + x.Value;
                                    break;
                                case UAPIdll.Terminal46.ResponseMessageType.Warning:
                                    response += Environment.NewLine + "Warning: " + x.Value;
                                    break;
                            }

                        });
                    }

                }
            }
            catch (Exception ex)
            {
                response = Environment.NewLine + ex.Message;
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to end the terminal session. Reason: " + ex.ToString(), "");
            }

            return response;
        }
        
        #endregion
    }
}
