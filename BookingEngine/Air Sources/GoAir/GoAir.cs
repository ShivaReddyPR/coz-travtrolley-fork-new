﻿#region NameSpace
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoAir.BookingManager;//Reference to  BookingManager  WebService.
using GoAir.SessionManager;//Reference to  SessionManager  WebService.
using System.Xml.Serialization;
using System.IO;
using CT.Configuration;
using CT.BookingEngine;
using System.Net;
using CT.Core;
using System.Data;
using System.Threading;
#endregion

namespace CT.BookingEngine.GDS
{
    [Serializable]
    public class GoAirAPI : IDisposable
    {
        /* ==================Start:Booking Flow of GoAir============================/ 
         * ==================Navitaire SOAP API Version 4.1.1======================/
         * ==================TLS :1.2==============================================/
         * * ==================Integration Start Date : 9Jan,2019 By Lokesh==========/
        
         * ===================Start:Below are the Pax Titles=============================/
         * DR ---Doctor, MISS ---Miss,MR ---Mr,MRS ---Mrs ,MS --- Ms ,MSTR--Master
         * ====================End:Below are the Pax Titles=============================/
         * 
         * ===================Passenger Type Codes(PTCs)================================/
         * ADT --Adult,CHD -- Child , SSR -->INF
         * =============================================================================/
         * ===================Start: Cozmo B2B Booking Flow:============================/
         * 1)Create a new booking.
         * 2)Add SSR during booking [Infant,Baggage,Meals].
         * 3)Get Fare Rules.
         * 4)Get Account Balance.
         * 5)Cancel A Booking.
         * ******************************************************************************
         *==================Start:API CALL SEQUENCE FOR NEW BOOKING======================/
         * 1.Logon
         * 2.GetAvailability
         * 3.UpdateContacts[Call Only if GST input available]
         * 4.Sell [Holds the itinerary]
         * 5.GetSSRAvailabilityForBooking [For Bagggage and meals]
         * 6.SellBySSR [For Bagggage and meals selection across segments]
         * 7.UpdatePassengers
         * 8.AddPaymentToBooking
         * 8.1UpdateContactsRequest.
         * 9.BookingCommit
         * 10.GetBooking
         * 11.Logout.
         * ================End:API CALL SEQUENCE FOR NEW BOOKING=======================/
         * ==================Start:API CALL SEQUENCE FOR CANCEL BOOKING================/
         * 1.Logon
         * 2.GetBooking
         * 3.CancelByAll
         * 4.BookingCommit
         * 5.Logout
         * ================Start:API CALL SEQUENCE FOR CANCEL BOOKING:==================/
         * ===============================Session Management:==========================/
         * Logon ---Is used to generate signature
         * Logout -- Abandons the session.
         * Session Timeout : 30 minutes.
         *======================Availability and Booking:=============================/
         * GetAvailabilty
         * GetLowFareAvailability [Deprecated as of 3.4.5 .Use GetLowFareTripAvailability instead]
         * GetLowFareTripAvailability
         * GetTripAvailabilityWithSSR
         * ===========================================================================/
         */

        /*********************************Booking Logs Flow*********************************
         * _LogonReq_
         * _LogonRes_
         * _SearchReq_
         * _SearchRes_
         * _ItineraryReq_
         * _ItineraryRes_
         * _ClearReq_
         * _ClearRes_
         * _GSTReq_
         * _GSTRes_
         * _SellReq_
         * _SellRes_
         * _AvailableSSR_Req_
         * _AvailableSSR_Res_
         * _Sell_SSR_Req_
         * _Sell_SSR_Res_
         * _UpdatePaxReq_
         * _UpdatePaxRes_
         * _AddPaymentToBookingReq_
         * _AddPaymentToBookingRes_
         * _UpdateContactsReq_
         * _UpdateContactsRes_
         * _BookingCommitReq_
         * _BookingCommitRes_
         * _GetBookingReq_
         * _GetBookingRes_
         * _LogoutReq_
         * ************************************************************************/

        /************************Cancellation Flow*****************************
         * _LogonReq_
         * _LogonRes_
         * _GetBookingReq_
         * _GetBookingRes_
         * _CancelReq_
         * _CancelRes_
         * _BookingCommitReq_
         * _BookingCommitRes_
         * _LogoutReq_
         * ********************************************************************/

        /**********************START:PROMOTION CODE***********************/
        //Getavailability ->  PromotionCode :- Kindly enter Promocode in tag namely “PromotionCode”.
        // GetItineraryPrice -> ItineraryPriceRequest -> TypeOfSale -> PromotionCode : Kindly enter Promocode in tag namely “PromotionCode”.
        //  Sell -> SellJourneyByKeyRequest -> TypeOfSale -> PromotionCode : Kindly enter Promocode in tag namely “PromotionCode”.
        /**********************END:PROMOTION CODE*************************/

        #region Fields
        string agentId;
        string agentDomain;
        string password;
        int contractVersion;
        string xmlLogPath;
        string sSeatAvailResponseXSLT;
        string sessionId;
        int appUserId;
        Dictionary<string, decimal> exchangeRates;
        string agentBaseCurrency;
        int agentDecimalValue;
        string promoCode;
        string currencyCode;
        const int sessionTimeOut = 30;//G8 session time out (30 minutes)
        const string carrierCode = "G8";
        LogonResponse logonResponse = new LogonResponse();
        SearchRequest request = new SearchRequest();
        decimal rateOfExchange;
        List<SearchResult> CombinedSearchResults = new List<SearchResult>();
        string bookingSourceFlag = string.Empty; // To identify the bookingSource G8 or G8CORP.
        string signatureForGST;
        //GST Paramters 
        string gstNumber;
        string gstCompanyName;
        string gstOfficialEmail;

        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
        bool searchByAvailability;

        //Start:===============ItineraryResponse Variables================================

        List<GoAir.BookingManager.Journey> vJourneys = new List<GoAir.BookingManager.Journey>();
        List<GoAir.BookingManager.Segment> vSegments = new List<GoAir.BookingManager.Segment>();
        List<GoAir.BookingManager.Fare> vFares = new List<GoAir.BookingManager.Fare>();
        List<GoAir.BookingManager.PaxFare> vTotalPaxFares = new List<GoAir.BookingManager.PaxFare>();
        List<GoAir.BookingManager.PaxFare> vADTPaxFares = new List<GoAir.BookingManager.PaxFare>();
        List<GoAir.BookingManager.PaxFare> vCHDPaxFares = new List<GoAir.BookingManager.PaxFare>();
        List<GoAir.BookingManager.BookingServiceCharge> vADTServiceCharges = new List<GoAir.BookingManager.BookingServiceCharge>();
        List<GoAir.BookingManager.BookingServiceCharge> vCHDServiceCharges = new List<GoAir.BookingManager.BookingServiceCharge>();

        //End:===============ItineraryResponse Variables====================================

        //==============Start:One-One Search Variables ====================/
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        List<SearchRequest> searchRequests = new List<SearchRequest>();

        //==============End:One-One Search Variables ======================/

        #endregion

        #region Properties
        //The below properties are defined with lambda syntax as per c#7.0
        public string LoginName
        {
            get => agentId;
            set => agentId = value;
        }
        public string Password
        {
            get => password;
            set => password = value;
        }
        public string AgentDomain
        {
            get => agentDomain;
            set => agentDomain = value;
        }
        public string SessionId
        {
            get => sessionId;
            set => sessionId = value;
        }
        public int AppUserId
        {
            get => appUserId;
            set => appUserId = value;
        }
        public Dictionary<string, decimal> ExchangeRates
        {
            get => exchangeRates;
            set => exchangeRates = value;
        }
        public string AgentBaseCurrency
        {
            get => agentBaseCurrency;
            set => agentBaseCurrency = value;
        }
        public int AgentDecimalValue
        {
            get => agentDecimalValue;
            set => agentDecimalValue = value;
        }
        public string PromoCode
        {
            get => promoCode;
            set => promoCode = value;
        }
        public string CurrencyCode
        {
            get => currencyCode;
            set => currencyCode = value;
        }
        public string GSTNumber
        {
            get => gstNumber;
            set => gstNumber = value;
        }
        public string GSTCompanyName
        {
            get => gstCompanyName;
            set => gstCompanyName = value;
        }
        public string GSTOfficialEmail
        {
            get => gstOfficialEmail;
            set => gstOfficialEmail = value;
        }
        public bool SearchByAvailability
        {
            get => searchByAvailability;
            set => searchByAvailability = value;
        }

        public string BookingSourceFlag  // Added for Booking source G8 or G8CORP
        {
            get => bookingSourceFlag;
            set => bookingSourceFlag = value;
        }

        public string SignatureForGST
        {
            get => signatureForGST;
            set => signatureForGST = value;
        }

        /// <summary>
        /// for sector list added by bangar
        /// </summary>
        public List<SectorList> Sectors { get; set; }

        //Web Service Calls.
        BookingManagerClient bookingAPI = new BookingManagerClient();
        SessionManagerClient sessionAPI = new SessionManagerClient();
        GoAir.ContentManager.ContentManagerClient contentAPI = new GoAir.ContentManager.ContentManagerClient();
        #endregion

        #region Default Constructor
        public GoAirAPI()
        {
            xmlLogPath = ConfigurationSystem.GoAirConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
            sSeatAvailResponseXSLT = ConfigurationSystem.XslconfigPath + "\\NavitaireSeatMap.xslt";
            if (!Directory.Exists(xmlLogPath))
            {
                Directory.CreateDirectory(xmlLogPath);
            }
            contractVersion = Convert.ToInt32(ConfigurationSystem.GoAirConfig["ContractVersion"]);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            bookingAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.GoAirConfig["BookingService"]);
            sessionAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.GoAirConfig["SessionService"]);
            contentAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.GoAirConfig["ContentService"]);
            //To retrive the fare rules used the below properties
            if (contentAPI.Endpoint.Binding is System.ServiceModel.BasicHttpBinding)
            {
                ((System.ServiceModel.BasicHttpBinding)contentAPI.Endpoint.Binding).MaxReceivedMessageSize = int.MaxValue;
                ((System.ServiceModel.BasicHttpBinding)contentAPI.Endpoint.Binding).MaxBufferSize = int.MaxValue;
            }
        }
        #endregion

        #region IDisposable Implementation
        private bool disposedValue = false; // To detect redundant calls
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }
                disposedValue = true;
            }
        }
        ~GoAirAPI()
        {
            Dispose(false);
        }
        void IDisposable.Dispose()
        {
            Dispose(true);
        }
        #endregion

        #region Session Methods

        /// <summary>
        /// Authenticates and creates a session for the specified user.
        /// </summary>
        /// <returns></returns>
        private LogonResponse Login()
        {
            try
            {
                GetLogonResponse(null);
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute Login Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return logonResponse;
        }

        /// <summary>
        /// Ends a user’s session and clears session from the server.
        /// </summary>
        /// <param name="signature"></param>
        /// <returns></returns>
        private void Logout()
        {
            try
            {
                LogoutRequest request = new LogoutRequest();
                request.Signature = logonResponse.Signature;
                request.ContractVersion = contractVersion;
                request.EnableExceptionStackTrace = false;
                WriteLogFile(typeof(LogoutRequest), request, "_LogoutReq_");
                sessionAPI.Logout(request);

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute Logout Method. Reason : " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Returns the signature for one-one search
        /// </summary>
        /// <param name="eventNumber"></param>
        /// <returns></returns>
        private string CreateSession(object eventNumber)//For One-One Search Signature Will be assigned from this basket.
        {
            string sessionSignature = string.Empty;
            try
            {
                GetLogonResponse(eventNumber);
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature))
                {
                    sessionSignature = logonResponse.Signature;
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute CreateSession Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return sessionSignature;
        }

        /// <summary>
        /// Authenticates and creates a session for the user.
        /// </summary>
        /// <returns></returns>
        private LogonResponse GetLogonResponse(object eventNumber)
        {

            try
            {
                LogonRequest request = new LogonRequest();
                request.logonRequestData = new LogonRequestData();
                request.logonRequestData.AgentName = agentId;
                request.logonRequestData.DomainCode = agentDomain;
                request.logonRequestData.Password = password;
                request.ContractVersion = contractVersion;
                request.EnableExceptionStackTrace = false;
                string reqLogFileName = (eventNumber == null) ? "_LogonReq_" : "_LogonReq_" + Convert.ToString(eventNumber);
                string resLogFileName = (eventNumber == null) ? "_LogonRes_" : "_LogonRes_" + Convert.ToString(eventNumber);
                WriteLogFile(typeof(LogonRequest), request, reqLogFileName);
                logonResponse = sessionAPI.Logon(request);
                WriteLogFile(typeof(LogonResponse), logonResponse, resLogFileName);
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetLogonResponse Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return logonResponse;
        }

        #endregion

        #region Search Methods

        /// <summary>
        ///This method returns a boolean which determine whether to send a  Search Request to get the flight inventory based on the sector list that is provided by G8.
        /// </summary>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        public bool AllowSearch(SearchRequest searchRequest)
        {
            bool allowSearch = false;
            try
            {
                //START:PERFORM SEARCH BASED ON THE NEAR BY AIRPORT

                List<SectorList> locSectorlist = new List<SectorList>();
                locSectorlist = Sectors.FindAll(x => x.Supplier == "GoAirCityMapping");

                //if (ConfigurationSystem.SectorListConfig.ContainsKey("GoAirCityMapping"))
                if (locSectorlist.Count() > 0)
                {
                    //Dictionary<string, string> sectorsNearBy = ConfigurationSystem.SectorListConfig["GoAirCityMapping"];
                    Dictionary<string, string> sectorsNearBy = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);
                    if (sectorsNearBy.ContainsKey(searchRequest.Segments[0].Destination))
                    {
                        searchRequest.Segments[0].Destination = sectorsNearBy[searchRequest.Segments[0].Destination];
                    }
                    if (sectorsNearBy.ContainsKey(searchRequest.Segments[0].Origin) && searchRequest.Segments[0].NearByOriginPort)
                    {
                        searchRequest.Segments[0].Origin = sectorsNearBy[searchRequest.Segments[0].Origin];
                    }
                }
                //END:PERFORM SEARCH BASED ON THE NEAR BY AIRPORT
                locSectorlist = Sectors.FindAll(x => x.Supplier == "G8").ToList();
                if (locSectorlist.Count() > 0)
                {
                    // Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["Indigo"];
                    Dictionary<string, string> sectors = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);
                    if (sectors.ContainsKey(searchRequest.Segments[0].Origin))
                    {
                        if (sectors[searchRequest.Segments[0].Origin].Contains(searchRequest.Segments[0].Destination))
                        {
                            allowSearch = true;
                        }
                    }
                   
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute AllowSearch Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return allowSearch;
        }

        /// <summary>
        /// Returns the flight results with respect to GoAir for one way and round trip
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult[] Search(SearchRequest request)
        {
            try
            {
                if (AllowSearch(request))//Allow search only if the requested origin and destination is avaialble in the sectorlist provided by G8.
                {
                    this.request = request;
                    //Note1:For normal B2B Search call the LoginMethod
                    //Note2:For One-One Search Call the Login Method twice and carry that signature till search to book for both the results.
                    if (!request.SearchBySegments)
                    {
                        Login();
                    }
                    if ((logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature)) || request.SearchBySegments)
                    {
                        if (request.Type == SearchType.Return)//Return
                        {
                            if (request.SearchBySegments) //Note2:Return One-One Search Flow.
                            {
                                //Step 1: spilt the request into two onward requests
                                //Step 2: Form Two thread Requests and call the api methods .
                                /*Dictionary<string, int> readySources = new Dictionary<string, int>();
                                Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();

                                listOfThreads.Add("IG1", new WaitCallback(SearchOneWay));
                                listOfThreads.Add("IG2", new WaitCallback(SearchOneWay));*/

                                SearchRequest onwardRequest = request.Copy();
                                onwardRequest.Type = SearchType.OneWay;
                                onwardRequest.SearchBySegments = true;
                                onwardRequest.Segments = new FlightSegment[1];
                                onwardRequest.Segments[0] = request.Segments[0];

                                SearchRequest returnRequest = request.Copy();
                                returnRequest.Type = SearchType.OneWay;
                                returnRequest.SearchBySegments = true;
                                returnRequest.Segments = new FlightSegment[1];
                                returnRequest.Segments[0] = request.Segments[1];

                                searchRequests.Add(onwardRequest);
                                searchRequests.Add(returnRequest);
                                SearchOneWay(0);
                                SearchOneWay(1);
                                /*eventFlag = new AutoResetEvent[2];
                                readySources.Add("IG1", 1200);
                                readySources.Add("IG2", 1200);

                                int j = 0;
                                //Start each fare search within a Thread which will automatically terminate after the results are received.
                                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                                {
                                    if (readySources.ContainsKey(deThread.Key))
                                    {
                                        ThreadPool.QueueUserWorkItem(deThread.Value, j);
                                        eventFlag[j] = new AutoResetEvent(false);
                                        j++;
                                    }
                                }
                                if (j != 0)
                                {
                                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 2, 0), true))
                                    {
                                        //TODO: audit which thread is timed out                
                                    }
                                }*/
                            }
                            else
                            {
                                SearchForAllFares(); //Note2:Return Normal B2B Flow
                            }
                        }
                        //=========START:ONE WAY COMBINATION SEARCH==================
                        else if (request.Type == SearchType.OneWay && request.SearchBySegments)
                        {
                            SearchRequest onwardRequest = request.Copy();
                            onwardRequest.Type = SearchType.OneWay;
                            onwardRequest.SearchBySegments = true;
                            onwardRequest.Segments = new FlightSegment[1];
                            onwardRequest.Segments[0] = request.Segments[0];
                            searchRequests.Add(onwardRequest);
                            SearchOneWay(0);
                        }
                        //=========END:ONE WAY COMBINATION SEARCH=====================
                        else//One Way
                        {
                            SearchOneWay(null);
                        }
                    }
                }
                else
                {
                    throw new Exception("GoAir Search failed.Reason:Requested origin and destination not available in the sector list");
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute Search Method. Reason : " + ex.ToString(), "");
                throw new Exception("GoAir Search failed. Reason : " + ex.ToString(), ex);
            }

            //Note:For One-One Search
            //Note:For Return Flights Binding Assign Group-1
            if (request.SearchBySegments && CombinedSearchResults != null && CombinedSearchResults.Count > 0)
            {

                CombinedSearchResults.Where(x => x.Flights != null && x.Flights.Length > 0
                    && x.Flights[0] != null && x.Flights[0].Count() > 0
                    && x.Flights[0][0].Origin.AirportCode == request.Segments[0].Destination).ToList().All(p =>
                    {
                        p.Flights[0].All(u => { u.Group = 1; return true; }); return true;
                    });
            }
            return CombinedSearchResults.ToArray();
        }

        /// <summary>
        /// Assign all Availability details to get flight details
        /// </summary>
        /// <param name="availRequest"></param>
        private void GetTripAvailabilityRequest(ref GetAvailabilityRequest availRequest)
        {
            try
            {
                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = carrierCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 20; //For connecting flights 
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;

                    //Available Fare Types
                    //1.Regular Fare(R)
                    //2.Corporate Fare(C)
                    //3.Promo Fare(P)
                    //4.Special Fare(S)
                    //5.GoMarineFare Fare(GM)
                    //6.GSS Fare (PV)
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[6];
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "C";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "P";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[3] = "S";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[4] = "GM";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[5] = "PV";

                    //To Get Only Economy Class you have to pass all product class except GB/GC.
                    //To Get Only Business Class you have to pass GB/GC only.
                    if (request.Segments[0].flightCabinClass == CabinClass.Economy)//Economy Class
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses = new string[3];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[0] = "BC";//GOSMARTCORP ECONOMY
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[1] = "GS";//GOSMART ECONOMY
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[2] = "RT";//GOROUNDTRIP ECONOMY
                    }
                    else if (request.Segments[0].flightCabinClass == CabinClass.Business)//Business Class
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses = new string[2];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[0] = "GB";//GB RETAIL BUSINESS
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[1] = "GC";//GB CORPORATE BUSINESS.
                    }
                    else if (request.Segments[0].flightCabinClass == CabinClass.All)//Economy + Business Class.
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses = new string[7];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[0] = "GB";//GB RETAIL BUSINESS.
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[1] = "GC";//GB CORPORATE BUSINESS.
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[2] = "BC";//GOSMARTCORP ECONOMY.
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[3] = "GS";//GOSMART ECONOMY.
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[4] = "RT";//GOROUNDTRIP ECONOMY.
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[5] = "GF";//GOFLEXI ECONOMY.
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[6] = "SP";//SPECIAL GSS.
                    }

                    int paxTypeCount = 1;
                    if (request.ChildCount > 0)
                    {
                        paxTypeCount++;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(request.AdultCount + request.ChildCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxTypeCount];


                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0] = new PaxPriceType();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0].PaxType = "ADT";//Adult Passenger

                    if (request.ChildCount > 0)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1].PaxType = "CHD"; //Child Passenger
                    }

                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetTripAvailabilityRequest Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
        }


        #region Onward

        /// <summary>
        /// Gets the Oneway results and for one-one search also the same is executed twice
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchOneWay(object eventNumber)
        {
            try
            {
                #region GetAvailabilityRequest Object
                //S-1:Create object for GetAvailabilityRequest
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                SearchRequest request = this.request;
                if (request.SearchBySegments && eventNumber != null)//One-One Search
                {
                    availRequest.Signature = CreateSession(eventNumber);
                    request = searchRequests[Convert.ToInt32(eventNumber)];
                    this.request = request;
                }
                else//Normal Search
                {
                    availRequest.Signature = logonResponse.Signature;
                }
                availRequest.ContractVersion = contractVersion;
                availRequest.EnableExceptionStackTrace = false;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                //Assign all Availability details to get flight details
                GetTripAvailabilityRequest(ref availRequest);
                #endregion

                #region GoAir One Way SearchRequest and Search Response Logs

                WriteLogFile(typeof(GetAvailabilityRequest), availRequest, "_SearchReq_" + Convert.ToString(eventNumber));

                //S-2:GetAvailability: -Gets flight availability and fare information.

                GetAvailabilityResponse availResponse_v_4_5 = bookingAPI.GetAvailability(availRequest);
                TripAvailabilityResponse tripAvailResponse = availResponse_v_4_5.GetTripAvailabilityResponse;

                //Note1:Again we will reset the search time for maintaining the idle time out
                //Note2:This should be done for normal flow
                //Note3:This should not be done for one-one search flow

                Type[] types = new Type[] { typeof(AvailableFare) };

                WriteLogFile(typeof(GetAvailabilityResponse), availResponse_v_4_5, "_SearchRes_" + Convert.ToString(eventNumber), types);
                #endregion

                //1.Check for Schedules in the response.
                //2.Check for Journeys in the schedules.

                if (tripAvailResponse != null && tripAvailResponse.Schedules != null && tripAvailResponse.Schedules.Length > 0)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                        }
                    }
                    if (OnwardJourneys.Count > 0)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            if (OnwardJourneys[i].Segments.Length > 1)//Connecting Flights
                            {
                                if (eventNumber == null)//For normal flow do not assign the signature
                                {
                                    OnwardConnectingFlights(OnwardJourneys[i], availRequest.Signature);
                                }
                                else//For one-one assign the signature.
                                {
                                    OnwardConnectingFlights(OnwardJourneys[i], availRequest.Signature);
                                }
                            }
                            else//Direct Flghts
                            {
                                for (int j = 0; j < OnwardJourneys[i].Segments[0].Fares.Length; j++)
                                {
                                    if (eventNumber == null)//For normal flow do not assign the signature
                                    {
                                        OnwardDirectFlights(OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[j], availRequest.Signature);//Direct Flights
                                    }
                                    else//For one-one assign the signature
                                    {
                                        OnwardDirectFlights(OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[j], availRequest.Signature);//Direct Flights
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute SearchOneWay Method. Reason : " + ex.ToString(), "");
                //throw ex;

            }
            finally
            {
                if (eventNumber != null)
                {
                    //eventFlag[(int)eventNumber].Set();
                }
            }
        }



        /// <summary>
        /// Gets the Onward Direct flights in case of One Way trip
        /// </summary>
        /// <param name="OnwardJourney"></param>
        /// <param name="OnwardFare"></param>
        /// <param name="tripAvailabilityResponseVer2"></param>
        private void OnwardDirectFlights(Journey OnwardJourney, GoAir.BookingManager.Fare OnwardFare, string signature)
        {
            try
            {
                if (OnwardJourney != null && OnwardFare != null)
                {
                    SearchResult result = new SearchResult();
                    AssignCommonInfo(ref result, OnwardFare);

                    //For One-One Search From the result object we  will get the signature and assign it to the itinerary object.
                    if (!string.IsNullOrEmpty(signature))
                    {
                        result.RepriceErrorMessage = signature;
                    }

                    result.JourneySellKey = OnwardJourney.JourneySellKey;
                    result.FareType = GetFareType(OnwardFare.ProductClass);
                    result.FareSellKey = OnwardFare.FareSellKey;
                    result.Flights = new FlightInfo[1][];
                    List<FlightInfo> directFlights = GetFlightInfo(OnwardJourney.Segments[0], OnwardJourney, OnwardFare.ProductClass, "ONWARD");
                    if (directFlights != null && directFlights.Count > 0)
                    {
                        result.Flights[0] = directFlights.ToArray();
                    }
                    if (result.Flights[0] != null && result.Flights[0].Length > 0 && !string.IsNullOrEmpty(result.JourneySellKey) && !string.IsNullOrEmpty(result.FareSellKey))
                    {
                        if (request.InfantCount > 0)
                        {
                            string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPrice(OnwardJourney.JourneySellKey, OnwardFare.FareSellKey, OnwardJourney, logFileName);
                            GetPaxFareBreakDown(itineraryResponse, ref result);
                        }
                        else
                        {
                            GetPaxFareBreakDown(OnwardFare, ref result);
                        }
                    }
                    CombinedSearchResults.Add(result);
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute OnwardDirectFlights Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// Gets the Connecting Flights in case of OneWay Trip
        /// </summary>
        /// <param name="journeyOnWard"></param>
        /// <param name="tripAvailabilityResponseVer2"></param>
        private void OnwardConnectingFlights(Journey journeyOnWard, string signature)
        {
            try
            {
                if (journeyOnWard != null && journeyOnWard.Segments != null && journeyOnWard.Segments.Length > 0 && journeyOnWard.Segments.Length == 2)
                {
                    /*Fare mapping on segments based on One to One relationship. 
                     *For ex. combine 1st Fare of the 1st Segment with the 1st Fare of the 2nd Segment, 
                     *The 2nd Fare of the 1st Segment with the 2nd Fare of the 2nd Segment and so on (if there are more fares under each Segment).             
                     */
                    Segment firstSegmentCon = null;
                    Segment secondSegmentCon = null;
                    if (journeyOnWard.Segments[0] != null && journeyOnWard.Segments[1] != null)
                    {
                        firstSegmentCon = journeyOnWard.Segments[0];
                        secondSegmentCon = journeyOnWard.Segments[1];
                    }
                    if (firstSegmentCon != null && secondSegmentCon != null && firstSegmentCon.Fares != null && secondSegmentCon.Fares != null &&
                        firstSegmentCon.Fares.Length > 0 && secondSegmentCon.Fares.Length > 0 && (firstSegmentCon.Fares.Length == secondSegmentCon.Fares.Length))
                    {
                        for (int f = 0; f < firstSegmentCon.Fares.Length; f++)
                        {
                            if (firstSegmentCon.Fares[f] != null && secondSegmentCon.Fares[f] != null)
                            {
                                GoAir.BookingManager.Fare fareFirstConSegOnWard = firstSegmentCon.Fares[f];
                                GoAir.BookingManager.Fare fareSecondConSegOnward = secondSegmentCon.Fares[f];

                                SearchResult result = new SearchResult();
                                AssignCommonInfo(ref result, fareFirstConSegOnWard);

                                //For One-One Search From the result object we  will get the signature and assign it to the itinerary object.
                                if (!string.IsNullOrEmpty(signature))
                                {
                                    result.RepriceErrorMessage = signature;
                                }

                                result.JourneySellKey = journeyOnWard.JourneySellKey;
                                result.FareType = GetFareType(fareFirstConSegOnWard.ProductClass);
                                result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                result.Flights = new FlightInfo[1][];
                                List<FlightInfo> firstSegmentConFlights = GetFlightInfo(firstSegmentCon, journeyOnWard, fareFirstConSegOnWard.ProductClass, "ONWARD");
                                List<FlightInfo> secondSegmentConFlights = GetFlightInfo(secondSegmentCon, journeyOnWard, fareSecondConSegOnward.ProductClass, "ONWARD");
                                if (firstSegmentConFlights != null && secondSegmentConFlights != null && firstSegmentConFlights.Count > 0 && secondSegmentConFlights.Count > 0)
                                {
                                    List<FlightInfo> listOfFlights = new List<FlightInfo>();
                                    listOfFlights.AddRange(firstSegmentConFlights);
                                    listOfFlights.AddRange(secondSegmentConFlights);
                                    result.Flights[0] = listOfFlights.ToArray();
                                }

                                if (result.Flights[0] != null && result.Flights[0].Length > 0) //Onward Journey Connecting or Via Flights
                                {
                                    string logFileName = string.Empty;
                                    for (int m = 0; m < result.Flights[0].Length; m++)
                                    {
                                        if (result.Flights[0][m].Status.ToUpper() != "VIA")
                                        {
                                            if (!string.IsNullOrEmpty(logFileName))
                                            {
                                                logFileName += "_" + result.Flights[0][1].FlightNumber + "_" + result.FareType + "_" + Convert.ToString(result.Flights[0][1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            }
                                            else
                                            {
                                                logFileName = result.Flights[0][m].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][m].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            }
                                        }
                                        else
                                        {
                                            logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                        }
                                    }
                                    if (request.InfantCount > 0)
                                    {
                                        PriceItineraryResponse itineraryResponse = GetItineraryPrice(journeyOnWard.JourneySellKey, fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey, journeyOnWard, logFileName);
                                        if (itineraryResponse != null)
                                        {
                                            GetPaxFareBreakDown(itineraryResponse, ref result);
                                        }
                                    }
                                    else
                                    {
                                        GetPaxFareBreakDown(fareFirstConSegOnWard, ref result);
                                        GetPaxFareBreakDown(fareSecondConSegOnward, ref result);
                                    }
                                }
                                CombinedSearchResults.Add(result);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute OnwardConnectingFlights Method. Reason : " + ex.ToString(), "");
                throw ex;
            }

        }

        #endregion

        #region RoundTrip

        /// <summary>
        /// Gets the round trip results for GoAir
        /// </summary>
        public void SearchForAllFares()
        {
            try
            {
                List<Journey> OnwardJourneys = new List<Journey>();
                List<Journey> ReturnJourneys = new List<Journey>();
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = logonResponse.Signature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];

                //Assign all Availability details to get flight details
                GetTripAvailabilityRequest(ref availRequest);

                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                availRequest.EnableExceptionStackTrace = false;
                WriteLogFile(typeof(GetAvailabilityRequest), availRequest, "_RoundTripSearchRequest_");
                GetAvailabilityResponse getAvailabilityVer2Response = bookingAPI.GetAvailability(availRequest);
                TripAvailabilityResponse tripAvailabilityResponseVer2 = getAvailabilityVer2Response.GetTripAvailabilityResponse;

                Type[] types = new Type[] { typeof(AvailableFare) };

                WriteLogFile(typeof(GetAvailabilityResponse), getAvailabilityVer2Response, "_RoundTripSearchResponse_", types);

                //Seperate onward and return journeys
                if (tripAvailabilityResponseVer2 != null && tripAvailabilityResponseVer2.Schedules != null && tripAvailabilityResponseVer2.Schedules.Length > 0)
                {
                    for (int i = 0; i < tripAvailabilityResponseVer2.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailabilityResponseVer2.Schedules[i].Length; j++)
                        {
                            if (tripAvailabilityResponseVer2.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                if (tripAvailabilityResponseVer2.Schedules[i][j].Journeys.Length > 0)
                                {
                                    OnwardJourneys.AddRange(tripAvailabilityResponseVer2.Schedules[i][j].Journeys);
                                }
                            }
                            if (tripAvailabilityResponseVer2.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                if (tripAvailabilityResponseVer2.Schedules[i][j].Journeys.Length > 0)
                                {
                                    ReturnJourneys.AddRange(tripAvailabilityResponseVer2.Schedules[i][j].Journeys);
                                }
                            }
                        }
                    }
                }
                if (OnwardJourneys != null && OnwardJourneys.Count > 0 && ReturnJourneys != null && ReturnJourneys.Count > 0)
                {
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int j = 0; j < ReturnJourneys.Count; j++)
                        {
                            //Case-1:Both are direct flights
                            if (OnwardJourneys[i] != null && ReturnJourneys[j] != null && OnwardJourneys[i].Segments != null && ReturnJourneys[j].Segments != null && OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments.Length == 1)
                            {
                                foreach (GoAir.BookingManager.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                {
                                    foreach (GoAir.BookingManager.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                    {
                                        if (onfare != null && retFare != null)
                                        {
                                            SearchResult result = new SearchResult();
                                            AssignCommonInfo(ref result, onfare);
                                            result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                            result.FareType = GetFareType(onfare.ProductClass);
                                            result.FareSellKey = onfare.FareSellKey;
                                            result.RepriceErrorMessage = logonResponse.Signature; //Till Search To Book Same Signature Should be carried

                                            result.Flights = new FlightInfo[2][];
                                            List<FlightInfo> onwardFlights = GetFlightInfo(OnwardJourneys[i].Segments[0], OnwardJourneys[i], onfare.ProductClass, "ONWARD");
                                            if (onwardFlights != null && onwardFlights.Count > 0)
                                            {
                                                result.Flights[0] = onwardFlights.ToArray();
                                            }
                                            result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                            result.FareType += "," + GetFareType(retFare.ProductClass);
                                            result.FareSellKey += "|" + retFare.FareSellKey;
                                            List<FlightInfo> returnFlights = GetFlightInfo(ReturnJourneys[j].Segments[0], ReturnJourneys[j], retFare.ProductClass, "RETURN");
                                            if (returnFlights != null && returnFlights.Count > 0)
                                            {
                                                result.Flights[1] = returnFlights.ToArray();
                                            }
                                            if (result.Flights[0] != null && result.Flights[0].Length > 0 && result.Flights[1] != null && result.Flights[1].Length > 0)
                                            {
                                                if (request.InfantCount > 0)
                                                {
                                                    FlightDesignator Designator = OnwardJourneys[i].Segments[0].FlightDesignator;
                                                    string logFileName = Designator.FlightNumber + "_" + GetFareType(onfare.ProductClass).Replace(" ", "_");// + Convert.ToString(OnwardJourneys[i].Segments[0].STD).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                    Designator = ReturnJourneys[j].Segments[0].FlightDesignator;
                                                    logFileName += Designator.FlightNumber + "_" + GetFareType(retFare.ProductClass).Replace(" ", "_");// + Convert.ToString(ReturnJourneys[j].Segments[0].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                    PriceItineraryResponse itineraryResponse = GetItineraryPrice(OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i], logFileName, retFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);
                                                    if (itineraryResponse != null)
                                                    {
                                                        GetPaxFareBreakDown(itineraryResponse, ref result);
                                                    }
                                                }
                                                else
                                                {
                                                    GetPaxFareBreakDown(onfare, ref result);
                                                    GetPaxFareBreakDown(retFare, ref result);
                                                }
                                            }
                                            CombinedSearchResults.Add(result);

                                        }

                                    }
                                }
                            }

                            //Case 2: Both Onward and Return Connecting Flights
                            else if (OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length > 1 && ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length > 1)
                            {
                                GetRoundTripConnectingResults(OnwardJourneys[i], ReturnJourneys[j], tripAvailabilityResponseVer2);
                            }
                            //Case-3: Onward Direct and Return Connecting
                            else if (OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length == 2)
                            {
                                if (OnwardJourneys[i].Segments[0].Fares != null && OnwardJourneys[i].Segments[0].Fares.Length > 0 && ReturnJourneys[j].Segments[0].Fares != null && ReturnJourneys[j].Segments[0].Fares.Length > 0 && ReturnJourneys[j].Segments[1].Fares != null && ReturnJourneys[j].Segments[1].Fares.Length > 0)
                                {
                                    Segment firstSegmentConReturn = null;
                                    Segment secondSegmentConReturn = null;
                                    if (ReturnJourneys[j].Segments[0] != null && ReturnJourneys[j].Segments[1] != null)
                                    {
                                        firstSegmentConReturn = ReturnJourneys[j].Segments[0];
                                        secondSegmentConReturn = ReturnJourneys[j].Segments[1];
                                    }
                                    if (firstSegmentConReturn != null && secondSegmentConReturn != null && firstSegmentConReturn.Fares.Length == secondSegmentConReturn.Fares.Length)
                                    {
                                        for (int f = 0; f < OnwardJourneys[i].Segments[0].Fares.Length; f++)
                                        {
                                            for (int g = 0; g < firstSegmentConReturn.Fares.Length; g++)
                                            {
                                                if (OnwardJourneys[i].Segments[0].Fares[f] != null && firstSegmentConReturn.Fares[g] != null && secondSegmentConReturn.Fares[g] != null)
                                                {
                                                    GoAir.BookingManager.Fare availableOnwardFare = OnwardJourneys[i].Segments[0].Fares[f];
                                                    GoAir.BookingManager.Fare fareFirstConSegReturn = firstSegmentConReturn.Fares[g];
                                                    GoAir.BookingManager.Fare fareSecondConSegReturn = secondSegmentConReturn.Fares[g];

                                                    SearchResult result = new SearchResult();
                                                    AssignCommonInfo(ref result, availableOnwardFare);

                                                    result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                    result.FareSellKey = availableOnwardFare.FareSellKey;
                                                    result.FareType = GetFareType(availableOnwardFare.ProductClass);
                                                    result.RepriceErrorMessage = logonResponse.Signature;//Till Search To Book Single signature should be carried;

                                                    result.Flights = new FlightInfo[2][];
                                                    List<FlightInfo> onwardFlights = GetFlightInfo(OnwardJourneys[i].Segments[0], OnwardJourneys[i], availableOnwardFare.ProductClass, "ONWARD");

                                                    if (onwardFlights != null && onwardFlights.Count > 0)
                                                    {
                                                        result.Flights[0] = onwardFlights.ToArray();
                                                    }

                                                    result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                    result.FareType += "," + GetFareType(fareFirstConSegReturn.ProductClass);
                                                    result.FareSellKey += "|" + fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey;

                                                    List<FlightInfo> firstSegmentConFlightsR = GetFlightInfo(firstSegmentConReturn, ReturnJourneys[j], fareFirstConSegReturn.ProductClass, "RETURN");
                                                    List<FlightInfo> secondSegmentConFlightsR = GetFlightInfo(secondSegmentConReturn, ReturnJourneys[j], fareSecondConSegReturn.ProductClass, "RETURN");
                                                    if (firstSegmentConFlightsR != null && secondSegmentConFlightsR != null && firstSegmentConFlightsR.Count > 0 && secondSegmentConFlightsR.Count > 0)
                                                    {
                                                        List<FlightInfo> listOfFlightsR = new List<FlightInfo>();
                                                        listOfFlightsR.AddRange(firstSegmentConFlightsR);
                                                        listOfFlightsR.AddRange(secondSegmentConFlightsR);
                                                        result.Flights[1] = listOfFlightsR.ToArray();
                                                    }
                                                    if (result.Flights[0] != null && result.Flights[0].Length > 0 && result.Flights[1] != null && result.Flights[1].Length > 0)
                                                    {
                                                        if (request.InfantCount > 0)
                                                        {
                                                            string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Split(',')[0].Replace(" ", "_");// + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            logFileName += "_" + result.Flights[1][0].FlightNumber + "_" + result.FareType.Split(',')[1].Replace(" ", "_");// + "_" + Convert.ToString(result.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            //logFileName += "_" + Convert.ToString(result.Flights[1][result.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            PriceItineraryResponse itineraryResponse = GetItineraryPrice(OnwardJourneys[i].JourneySellKey, availableOnwardFare.FareSellKey, OnwardJourneys[i], logFileName, fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);
                                                            if (itineraryResponse != null)
                                                            {
                                                                GetPaxFareBreakDown(itineraryResponse, ref result);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            GetPaxFareBreakDown(availableOnwardFare, ref result);
                                                            GetPaxFareBreakDown(fareFirstConSegReturn, ref result);
                                                            GetPaxFareBreakDown(fareSecondConSegReturn, ref result);
                                                        }
                                                    }
                                                    CombinedSearchResults.Add(result);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //Case-4: Onward Connecting and Return Direct
                            else if (OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length == 2 && ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length == 1)
                            {
                                if (ReturnJourneys[j].Segments[0].Fares != null && ReturnJourneys[j].Segments[0].Fares.Length > 0 && OnwardJourneys[i].Segments[0].Fares != null && OnwardJourneys[i].Segments[0].Fares.Length > 0 && OnwardJourneys[i].Segments[1].Fares != null && OnwardJourneys[i].Segments[1].Fares.Length > 0)
                                {
                                    Segment firstSegmentConOnward = null;
                                    Segment secondSegmentConOnward = null;

                                    if (OnwardJourneys[i].Segments[0] != null && OnwardJourneys[i].Segments[1] != null)
                                    {
                                        firstSegmentConOnward = OnwardJourneys[i].Segments[0];
                                        secondSegmentConOnward = OnwardJourneys[i].Segments[1];
                                    }
                                    if (firstSegmentConOnward != null && secondSegmentConOnward != null && firstSegmentConOnward.Fares.Length == secondSegmentConOnward.Fares.Length)
                                    {
                                        for (int f = 0; f < firstSegmentConOnward.Fares.Length; f++)
                                        {
                                            for (int g = 0; g < ReturnJourneys[j].Segments[0].Fares.Length; g++)
                                            {
                                                if (firstSegmentConOnward.Fares[f] != null && secondSegmentConOnward.Fares[f] != null && ReturnJourneys[j].Segments[0].Fares[g] != null)
                                                {
                                                    GoAir.BookingManager.Fare fareFirstConSegOnWard = firstSegmentConOnward.Fares[f];
                                                    GoAir.BookingManager.Fare fareSecondConSegOnward = secondSegmentConOnward.Fares[f];

                                                    GoAir.BookingManager.Fare availableReturnFare = ReturnJourneys[j].Segments[0].Fares[g];


                                                    SearchResult result = new SearchResult();
                                                    AssignCommonInfo(ref result, fareFirstConSegOnWard);
                                                    result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                    result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                                    result.FareType = GetFareType(fareFirstConSegOnWard.ProductClass);
                                                    result.RepriceErrorMessage = logonResponse.Signature;

                                                    result.Flights = new FlightInfo[2][];

                                                    List<FlightInfo> firstSegmentConFlightsO = GetFlightInfo(firstSegmentConOnward, OnwardJourneys[i], fareFirstConSegOnWard.ProductClass, "ONWARD");
                                                    List<FlightInfo> secondSegmentConFlightsO = GetFlightInfo(secondSegmentConOnward, OnwardJourneys[i], fareSecondConSegOnward.ProductClass, "ONWARD");
                                                    if (firstSegmentConFlightsO != null && secondSegmentConFlightsO != null && firstSegmentConFlightsO.Count > 0 && secondSegmentConFlightsO.Count > 0)
                                                    {
                                                        List<FlightInfo> listOfFlightsO = new List<FlightInfo>();
                                                        listOfFlightsO.AddRange(firstSegmentConFlightsO);
                                                        listOfFlightsO.AddRange(secondSegmentConFlightsO);
                                                        result.Flights[0] = listOfFlightsO.ToArray();
                                                    }
                                                    result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                    result.FareType += "," + GetFareType(availableReturnFare.ProductClass);
                                                    result.FareSellKey += "|" + availableReturnFare.FareSellKey;
                                                    List<FlightInfo> returnFlights = GetFlightInfo(ReturnJourneys[j].Segments[0], ReturnJourneys[j], availableReturnFare.ProductClass, "RETURN");
                                                    if (returnFlights != null && returnFlights.Count > 0)
                                                    {
                                                        result.Flights[1] = returnFlights.ToArray();
                                                    }
                                                    if (result.Flights[0] != null && result.Flights[0].Length > 0 && result.Flights[1] != null && result.Flights[1].Length > 0)
                                                    {
                                                        if (request.InfantCount > 0)
                                                        {
                                                            string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Split(',')[0].Replace(" ", "_");// + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            logFileName += "_" + result.Flights[1][0].FlightNumber + "_" + result.FareType.Split(',')[1].Replace(" ", "_");// + "_" + Convert.ToString(result.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            //logFileName += "_" + Convert.ToString(result.Flights[1][result.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            PriceItineraryResponse itineraryResponse = GetItineraryPrice(OnwardJourneys[i].JourneySellKey, fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey, OnwardJourneys[i], logFileName, availableReturnFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);
                                                            if (itineraryResponse != null)
                                                            {
                                                                GetPaxFareBreakDown(itineraryResponse, ref result);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            GetPaxFareBreakDown(fareFirstConSegOnWard, ref result);
                                                            GetPaxFareBreakDown(fareSecondConSegOnward, ref result);
                                                            GetPaxFareBreakDown(availableReturnFare, ref result);
                                                        }
                                                    }
                                                    CombinedSearchResults.Add(result);
                                                }
                                            }
                                        }
                                    }
                                }

                            }





                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute SearchForAllFares Method. Reason : " + ex.ToString(), "");
                throw ex;
            }

        }

        /// <summary>
        /// Gets the connecting flight results for round trip.
        /// </summary>
        /// <param name="OnwardJourneys"></param>
        /// <param name="ReturnJourneys"></param>
        /// <param name="tripAvailabilityResponseVer2"></param>
        public void GetRoundTripConnectingResults(Journey OnwardJourneys, Journey ReturnJourneys, TripAvailabilityResponse tripAvailabilityResponseVer2)
        {
            try
            {
                if (OnwardJourneys != null && ReturnJourneys != null && OnwardJourneys.Segments != null && OnwardJourneys.Segments.Length > 0 && ReturnJourneys.Segments != null && ReturnJourneys.Segments.Length > 0 && OnwardJourneys.Segments.Length == 2 && ReturnJourneys.Segments.Length == 2)
                {
                    Segment firstSegmentConOnward = null;//Onward Journey First Connecting Segment
                    Segment secondSegmentConOnward = null;//Onward Journey Second Connecting Segment

                    Segment firstSegmentConReturn = null;//Return Journey First Connecting Segment
                    Segment secondSegmentConReturn = null;//Return Journey First Connecting Segment

                    if (OnwardJourneys.Segments[0] != null && OnwardJourneys.Segments[1] != null)
                    {
                        firstSegmentConOnward = OnwardJourneys.Segments[0];
                        secondSegmentConOnward = OnwardJourneys.Segments[1];
                    }
                    if (ReturnJourneys.Segments[0] != null && ReturnJourneys.Segments[1] != null)
                    {
                        firstSegmentConReturn = ReturnJourneys.Segments[0];
                        secondSegmentConReturn = ReturnJourneys.Segments[1];
                    }

                    if (firstSegmentConOnward != null && secondSegmentConOnward != null && firstSegmentConReturn != null && secondSegmentConReturn != null)
                    {
                        if (firstSegmentConOnward.Fares != null && firstSegmentConOnward.Fares.Length > 0 &&
                            secondSegmentConOnward.Fares != null && secondSegmentConOnward.Fares.Length > 0 &&

                            firstSegmentConReturn.Fares != null && firstSegmentConReturn.Fares.Length > 0 &&
                            secondSegmentConReturn.Fares != null && secondSegmentConReturn.Fares.Length > 0 &&

                            firstSegmentConOnward.Fares.Length == secondSegmentConOnward.Fares.Length &&
                            firstSegmentConReturn.Fares.Length == secondSegmentConReturn.Fares.Length)
                        {
                            for (int f = 0; f < firstSegmentConOnward.Fares.Length; f++)
                            {
                                for (int g = 0; g < firstSegmentConReturn.Fares.Length; g++)
                                {
                                    if (firstSegmentConOnward.Fares[f] != null && secondSegmentConOnward.Fares[f] != null && firstSegmentConReturn.Fares[g] != null && secondSegmentConReturn.Fares[g] != null)
                                    {
                                        GoAir.BookingManager.Fare fareFirstConSegOnWard = firstSegmentConOnward.Fares[f];
                                        GoAir.BookingManager.Fare fareSecondConSegOnward = secondSegmentConOnward.Fares[f];

                                        GoAir.BookingManager.Fare fareFirstConSegReturn = firstSegmentConReturn.Fares[g];
                                        GoAir.BookingManager.Fare fareSecondConSegReturn = secondSegmentConReturn.Fares[g];

                                        SearchResult result = new SearchResult();
                                        AssignCommonInfo(ref result, fareFirstConSegOnWard);
                                        result.RepriceErrorMessage = logonResponse.Signature;//Till Search To Book Same signature should be carried;

                                        result.JourneySellKey = OnwardJourneys.JourneySellKey;
                                        result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                        result.FareType = GetFareType(fareFirstConSegOnWard.ProductClass);

                                        result.Flights = new FlightInfo[2][];

                                        List<FlightInfo> firstSegmentConFlightsO = GetFlightInfo(firstSegmentConOnward, OnwardJourneys, fareFirstConSegOnWard.ProductClass, "ONWARD");
                                        List<FlightInfo> secondSegmentConFlightsO = GetFlightInfo(secondSegmentConOnward, OnwardJourneys, fareSecondConSegOnward.ProductClass, "ONWARD");
                                        if (firstSegmentConFlightsO != null && secondSegmentConFlightsO != null && firstSegmentConFlightsO.Count > 0 && secondSegmentConFlightsO.Count > 0)
                                        {
                                            List<FlightInfo> listOfFlightsO = new List<FlightInfo>();
                                            listOfFlightsO.AddRange(firstSegmentConFlightsO);
                                            listOfFlightsO.AddRange(secondSegmentConFlightsO);
                                            result.Flights[0] = listOfFlightsO.ToArray();
                                        }

                                        result.JourneySellKey += "|" + ReturnJourneys.JourneySellKey;
                                        result.FareType += "," + GetFareType(fareFirstConSegReturn.ProductClass);
                                        result.FareSellKey += "|" + fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey;

                                        List<FlightInfo> firstSegmentConFlightsR = GetFlightInfo(firstSegmentConReturn, ReturnJourneys, fareFirstConSegReturn.ProductClass, "RETURN");
                                        List<FlightInfo> secondSegmentConFlightsR = GetFlightInfo(secondSegmentConReturn, ReturnJourneys, fareSecondConSegReturn.ProductClass, "RETURN");
                                        if (firstSegmentConFlightsR != null && secondSegmentConFlightsR != null && firstSegmentConFlightsR.Count > 0 && secondSegmentConFlightsR.Count > 0)
                                        {
                                            List<FlightInfo> listOfFlightsR = new List<FlightInfo>();
                                            listOfFlightsR.AddRange(firstSegmentConFlightsR);
                                            listOfFlightsR.AddRange(secondSegmentConFlightsR);
                                            result.Flights[1] = listOfFlightsR.ToArray();
                                        }
                                        if (result.Flights[0] != null && result.Flights[1] != null && result.Flights[0].Length > 0 && result.Flights[1].Length > 0)
                                        {
                                            if (request.InfantCount > 0)
                                            {
                                                string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Split(',')[0].Replace(" ", "_");// + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                //logFileName += "_" + Convert.ToString(result.Flights[0][result.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                logFileName += "_" + result.Flights[1][0].FlightNumber + "_" + result.FareType.Split(',')[1].Replace(" ", "_");// + "_" + Convert.ToString(result.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                //logFileName += "_" + Convert.ToString(result.Flights[1][result.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                PriceItineraryResponse itineraryResponse = GetItineraryPrice(OnwardJourneys.JourneySellKey, fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey, OnwardJourneys, logFileName, fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey, ReturnJourneys.JourneySellKey, ReturnJourneys);
                                                if (itineraryResponse != null)
                                                {
                                                    GetPaxFareBreakDown(itineraryResponse, ref result);
                                                }
                                            }
                                            else
                                            {
                                                GetPaxFareBreakDown(fareFirstConSegOnWard, ref result);
                                                GetPaxFareBreakDown(fareSecondConSegOnward, ref result);
                                                GetPaxFareBreakDown(fareFirstConSegReturn, ref result);
                                                GetPaxFareBreakDown(fareSecondConSegReturn, ref result);
                                            }
                                        }
                                        CombinedSearchResults.Add(result);
                                    }
                                }
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetRoundTripConnectingResults Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
        }

        #endregion


        #endregion

        #region Helper Methods

        /// <summary>
        /// This methods returns the flight objects info based on segments legs info
        /// </summary>
        /// <param name="segment"></param>
        /// <param name="availableJourney"></param>
        /// <param name="productClass"></param>
        /// <param name="journeyType"></param>
        /// <returns></returns>
        public List<FlightInfo> GetFlightInfo(Segment segment, Journey availableJourney, string productClass, string journeyType)
        {
            List<FlightInfo> flightInfo = new List<FlightInfo>();
            try
            {
                if (availableJourney != null && segment != null && segment.Legs != null && segment.Legs.Length > 0)
                {
                    for (int k = 0; k < segment.Legs.Length; k++)
                    {
                        if (segment.Legs[k] != null)
                        {
                            FlightInfo flightDetails = new FlightInfo();
                            flightDetails.Airline = carrierCode;
                            flightDetails.ArrivalTime = segment.Legs[k].STA;
                            flightDetails.ArrTerminal = segment.Legs[k].LegInfo.ArrivalTerminal;
                            flightDetails.BookingClass = (segment != null && segment.CabinOfService.Trim().Length > 0 ? segment.CabinOfService : "C");
                            if (!string.IsNullOrEmpty(segment.CabinOfService.Trim()))
                            {
                                flightDetails.CabinClass = segment.CabinOfService;
                            }
                            else
                            {
                                flightDetails.CabinClass = "Economy";
                            }
                            flightDetails.Craft = segment.Legs[k].LegInfo.EquipmentType + "-" + segment.Legs[k].LegInfo.EquipmentTypeSuffix;
                            flightDetails.DepartureTime = segment.Legs[k].STD;
                            flightDetails.DepTerminal = segment.Legs[k].LegInfo.DepartureTerminal;
                            flightDetails.Destination = new Airport(segment.Legs[k].ArrivalStation);
                            flightDetails.Duration = flightDetails.ArrivalTime.Subtract(flightDetails.DepartureTime);
                            flightDetails.ETicketEligible = segment.Legs[k].LegInfo.ETicket;
                            flightDetails.FlightNumber = segment.Legs[k].FlightDesignator.FlightNumber;
                            flightDetails.FlightStatus = FlightStatus.Confirmed;
                            if (journeyType.ToUpper() == "ONWARD")
                            {
                                flightDetails.Group = 0;
                            }
                            else if (journeyType.ToUpper() == "RETURN")
                            {
                                flightDetails.Group = 1;
                            }
                            flightDetails.OperatingCarrier = carrierCode;
                            flightDetails.Origin = new Airport(segment.Legs[k].DepartureStation);
                            flightDetails.Stops = availableJourney.Segments.Length - 1;

                            //Direct flight
                            if (segment.Legs.Length == 1 && availableJourney.Segments.Length == 1)
                            {
                                flightDetails.Status = string.Empty;
                            }
                            //Connecting flight
                            else if (availableJourney.Segments.Length > 1)
                            {
                                int legCount = 0;
                                for (int s = 0; s < availableJourney.Segments.Length; s++)
                                {
                                    legCount += availableJourney.Segments[s].Legs.Length;
                                }
                                if (legCount == 2)
                                {
                                    flightDetails.Status = string.Empty; //Connecting direct flight
                                }
                                else
                                {
                                    flightDetails.Status = "VIA"; //Connecting via flight
                                }
                            }
                            //VIA Flight
                            else if (segment.Legs.Length > 1 && availableJourney.Segments.Length == 1)
                            {
                                flightDetails.Status = "VIA";
                            }
                            flightDetails.UapiSegmentRefKey = segment.SegmentSellKey;
                            flightDetails.SegmentFareType = GetFareType(productClass);
                            flightInfo.Add(flightDetails);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetFlightInfo Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return flightInfo;
        }

        /// <summary>
        /// Returns the Fare Type for a Product Class 
        /// </summary>
        /// <param name="productClass"></param>
        /// <returns></returns>
        private string GetFareType(string productClass)
        {
            string fareType = "";
            try
            {

                switch (productClass)
                {
                    case "GS":
                        fareType = "GOSMART ECONOMY";
                        break;
                    case "GC":
                        fareType = "GB CORPORATE BUSINESS";
                        break;
                    case "GB":
                        fareType = "GB RETAIL BUSINESS";
                        break;
                    case "BC":
                        fareType = "GOSMARTCORP ECONOMY";
                        break;
                    case "RT":
                        fareType = "GOROUNDTRIP ECONOMY";
                        break;
                    case "GF":
                        fareType = "GOFLEXI ECONOMY";
                        break;
                    case "SP":
                        fareType = "SPECIAL GSS";
                        break;
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetFareType Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return fareType;
        }

        /// <summary>
        /// Assigns the fare break down from the fare object 
        /// </summary>
        /// <param name="priceItineraryResponse"></param>
        /// <param name="searchResult"></param>
        private void GetPaxFareBreakDown(GoAir.BookingManager.Fare priceItineraryResponse, ref SearchResult searchResult)
        {
            try
            {
                if (priceItineraryResponse != null && priceItineraryResponse.PaxFares != null && priceItineraryResponse.PaxFares.Length > 0)
                {
                    if (priceItineraryResponse.PaxFares[0] != null && priceItineraryResponse.PaxFares[0].ServiceCharges[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.PaxFares[0].ServiceCharges[0].CurrencyCode))
                    {
                        rateOfExchange = exchangeRates[priceItineraryResponse.PaxFares[0].ServiceCharges[0].CurrencyCode];
                    }
                    PaxFare adultPaxfare = priceItineraryResponse.PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault();
                    PaxFare childPaxfare = priceItineraryResponse.PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault();

                    //Calculates the Adult Price
                    if (adultPaxfare != null && adultPaxfare.ServiceCharges != null && adultPaxfare.ServiceCharges.Length > 0 && searchResult.FareBreakdown[0] != null)
                    {
                        foreach (BookingServiceCharge charge in adultPaxfare.ServiceCharges)
                        {
                            if (charge.ChargeType == GoAir.BookingManager.ChargeType.FarePrice)
                            {
                                double baseFareAdult = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                searchResult.FareBreakdown[0].BaseFare += baseFareAdult;
                                searchResult.FareBreakdown[0].TotalFare += baseFareAdult;
                                searchResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                searchResult.Price.PublishedFare += (decimal)baseFareAdult;
                                searchResult.Price.SupplierPrice += charge.Amount;
                            }
                            //DEDUCT THE PROMOTION DISCOUNT FROM THE BASE FARE
                            else if (charge.ChargeType == GoAir.BookingManager.ChargeType.PromotionDiscount)
                            {
                                double discountAdult = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                searchResult.FareBreakdown[0].BaseFare = searchResult.FareBreakdown[0].BaseFare - discountAdult;
                                searchResult.FareBreakdown[0].TotalFare = searchResult.FareBreakdown[0].TotalFare - discountAdult;
                                searchResult.FareBreakdown[0].SupplierFare = searchResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                            }
                            //For the below charge types  
                            //====PromotionDiscount
                            //======IncludedAddOnServiceFee
                            //=====IncludedTax
                            //====IncludedTravelFee
                            //Do not compute the Tax Values
                            else if (charge.ChargeType != GoAir.BookingManager.ChargeType.PromotionDiscount && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedAddOnServiceFee && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedTax && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedTravelFee)
                            {
                                double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                searchResult.FareBreakdown[0].TotalFare += tax;
                                searchResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                searchResult.Price.Tax += (decimal)tax;
                                searchResult.Price.SupplierPrice += charge.Amount;
                            }
                        }
                    }
                    //Calculates the Child Price
                    if (childPaxfare != null && childPaxfare.ServiceCharges != null && childPaxfare.ServiceCharges.Length > 0 && searchResult.FareBreakdown[1] != null)
                    {
                        foreach (BookingServiceCharge charge in childPaxfare.ServiceCharges)
                        {
                            if (charge.ChargeType == GoAir.BookingManager.ChargeType.FarePrice)
                            {
                                double baseFareChd = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                searchResult.FareBreakdown[1].BaseFare += baseFareChd;
                                searchResult.FareBreakdown[1].TotalFare += baseFareChd;
                                searchResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                searchResult.Price.PublishedFare += (decimal)baseFareChd;
                                searchResult.Price.SupplierPrice += charge.Amount;
                            }
                            //DEDUCT THE PROMOTION DISCOUNT
                            else if (charge.ChargeType == GoAir.BookingManager.ChargeType.PromotionDiscount)
                            {
                                double discountChild = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                searchResult.FareBreakdown[1].BaseFare = searchResult.FareBreakdown[1].BaseFare - discountChild;
                                searchResult.FareBreakdown[1].TotalFare = searchResult.FareBreakdown[1].TotalFare - discountChild;
                                searchResult.FareBreakdown[1].SupplierFare = searchResult.FareBreakdown[1].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                            }
                            //For the below charge types  
                            //====PromotionDiscount
                            //======IncludedAddOnServiceFee
                            //=====IncludedTax
                            //====IncludedTravelFee
                            //Do not compute the Tax Values
                            else if (charge.ChargeType != GoAir.BookingManager.ChargeType.PromotionDiscount && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedAddOnServiceFee && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedTax && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedTravelFee)
                            {
                                double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                searchResult.FareBreakdown[1].TotalFare += tax;
                                searchResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                searchResult.Price.Tax += (decimal)tax;
                                searchResult.Price.SupplierPrice += charge.Amount;
                            }
                        }
                    }
                    searchResult.Price.SupplierCurrency = currencyCode;
                    searchResult.BaseFare = (double)searchResult.Price.PublishedFare;
                    searchResult.Tax = (double)searchResult.Price.Tax;
                    searchResult.TotalFare = (double)(Math.Round((searchResult.BaseFare + searchResult.Tax), agentDecimalValue));
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetPaxFareBreakDown Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// Assigns the common information for the result object
        /// </summary>
        /// <param name="result"></param>
        /// <param name="fare"></param>
        private void AssignCommonInfo(ref SearchResult result, GoAir.BookingManager.Fare fare)
        {
            try
            {
                if (result != null && fare != null)
                {
                    result.IsLCC = true;
                    result.Airline = carrierCode;
                    result.Currency = agentBaseCurrency;
                    result.EticketEligible = true;
                    result.NonRefundable = false;
                    if (bookingSourceFlag == "G8")
                    {
                        result.ResultBookingSource = BookingSource.GoAir;
                    }
                    else
                    {
                        result.ResultBookingSource = BookingSource.GoAirCorp;
                    }

                    //FareRules
                    result.FareRules = new List<FareRule>();
                    FareRule fareRule = new FareRule();
                    fareRule.Airline = result.Airline;
                    fareRule.Destination = request.Segments[0].Destination;
                    fareRule.FareBasisCode = fare.FareBasisCode;
                    fareRule.FareInfoRef = fare.RuleNumber;
                    fareRule.Origin = request.Segments[0].Origin;
                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                    result.FareRules.Add(fareRule);

                    int fareBreakDownCount = 0;
                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    //FareBreakdown
                    result.FareBreakdown = new Fare[fareBreakDownCount];

                    //Default ADULT FareBreakDown.
                    result.FareBreakdown[0] = new Fare();
                    result.FareBreakdown[0].PassengerCount = request.AdultCount;
                    result.FareBreakdown[0].PassengerType = PassengerType.Adult;

                    if (request.ChildCount > 0)
                    {
                        result.FareBreakdown[1] = new Fare();
                        result.FareBreakdown[1].PassengerCount = request.ChildCount;
                        result.FareBreakdown[1].PassengerType = PassengerType.Child;
                    }
                    //Infant Count-- if both children and infants exists in the request.
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        result.FareBreakdown[2] = new Fare();
                        result.FareBreakdown[2].PassengerCount = request.InfantCount;
                        result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                    }
                    // If only infant count exists in the rquest.
                    else if (request.ChildCount == 0 && request.InfantCount > 0)
                    {
                        result.FareBreakdown[1] = new Fare();
                        result.FareBreakdown[1].PassengerCount = request.InfantCount;
                        result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                    }
                    result.Price = new PriceAccounts();
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute AssignCommonInfo Method. Reason : " + ex.ToString(), "");
                throw ex;
            }

        }

        /// <summary>
        /// Calculates the PaxFareBreakDown from the PriceItineraryResponse object
        /// </summary>
        /// <param name="priceItineraryResponse"></param>
        /// <param name="searchResult"></param>
        private void GetPaxFareBreakDown(PriceItineraryResponse priceItineraryResponse, ref SearchResult searchResult)
        {
            try
            {
                //Clear the values
                adtPaxTaxBreakUp.Clear();
                chdPaxTaxBreakUp.Clear();
                inftPaxTaxBreakUp.Clear();
                paxTypeTaxBreakUp.Clear();
                vJourneys.Clear();
                vSegments.Clear();
                vFares.Clear();
                vTotalPaxFares.Clear();
                vADTPaxFares.Clear();
                vCHDPaxFares.Clear();
                vADTServiceCharges.Clear();
                vCHDServiceCharges.Clear();

                //S-1:Booking
                //S-2:Booking -->Journeys[]
                //S-3:Booking -->Journey -->Segments[]
                //S-4:Booking -->Journey -->Segment -->Fares[]
                //S-5:Booking -->Journey -->Segment -->Fare -->PaxFares[]
                //S-6:Booking -->Journey-->Segment-->Fare-->PaxFare --><PaxType>ADT</PaxType> -->BookingServiceCharge


                //S-1:Journeys
                if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
                {
                    vJourneys.AddRange(priceItineraryResponse.Booking.Journeys);
                }
                //S-2:Segments
                if (vJourneys != null && vJourneys.Count > 0)
                {
                    for (int a = 0; a < vJourneys.Count; a++)
                    {
                        vSegments.AddRange(vJourneys[a].Segments.ToList());
                    }
                }
                //S-3:Fares
                if (vSegments != null && vSegments.Count > 0)
                {
                    for (int a = 0; a < vSegments.Count; a++)
                    {
                        vFares.AddRange(vSegments[a].Fares.ToList());
                    }
                }
                //S-4:Total Pax Fares
                if (vFares != null && vFares.Count > 0)
                {
                    for (int a = 0; a < vFares.Count; a++)
                    {
                        vTotalPaxFares.AddRange(vFares[a].PaxFares.ToList());
                    }
                }
                //S-5: ADT PaxFare and CHD PaxFare
                if (vTotalPaxFares != null && vTotalPaxFares.Count > 0)
                {
                    vADTPaxFares = vTotalPaxFares.Where(a => a.PaxType == "ADT").Select(b => b).ToList();
                    if (request.ChildCount > 0)
                    {
                        vCHDPaxFares = vTotalPaxFares.Where(a => a.PaxType == "CHD").Select(b => b).ToList();
                    }
                }
                //S-6: Service Charges ADT
                if (vADTPaxFares != null && vADTPaxFares.Count > 0)
                {
                    for (int a = 0; a < vADTPaxFares.Count; a++)
                    {
                        vADTServiceCharges.AddRange(vADTPaxFares[a].ServiceCharges.ToList());
                    }
                }
                //S-6.1: Service Charges CHD
                if (vCHDPaxFares != null && vCHDPaxFares.Count > 0)
                {
                    for (int a = 0; a < vCHDPaxFares.Count; a++)
                    {
                        vCHDServiceCharges.AddRange(vCHDPaxFares[a].ServiceCharges.ToList());
                    }
                }

                if (priceItineraryResponse != null && searchResult != null)
                {
                    if (priceItineraryResponse.Booking != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.CurrencyCode))
                    {
                        rateOfExchange = exchangeRates[priceItineraryResponse.Booking.CurrencyCode];
                    }

                    //Calculates the Adult Price
                    if (vADTServiceCharges.Count > 0)
                    {
                        foreach (BookingServiceCharge charge in vADTServiceCharges)
                        {
                            AssignCharges(charge, 0, ref searchResult);
                        }
                    }
                    //Calculates the Child Price
                    if (vCHDServiceCharges.Count > 0)
                    {
                        foreach (BookingServiceCharge charge in vCHDServiceCharges)
                        {
                            AssignCharges(charge, 1, ref searchResult);
                        }
                    }
                    int infantIndex = 1;
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        infantIndex = 2;
                    }
                    //Infant and Baggage Price Calculation
                    if (priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
                    {
                        //Infant Price Calculation
                        for (int j = 0; j < priceItineraryResponse.Booking.Journeys.Length; j++)
                        {
                            if (request.InfantCount > 0 && priceItineraryResponse.Booking.Passengers != null && priceItineraryResponse.Booking.Passengers.Length > 0 && priceItineraryResponse.Booking.Passengers[0] != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees.Length > 0 && priceItineraryResponse.Booking.Passengers[0].PassengerFees[j] != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees[j].ServiceCharges != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees[j] != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees[j].ServiceCharges.Length > 0)
                            {
                                foreach (BookingServiceCharge charge in priceItineraryResponse.Booking.Passengers[0].PassengerFees[j].ServiceCharges)
                                {
                                    AssignInftCharges(charge, infantIndex, ref searchResult);
                                }
                            }

                            #region Baggage Calculation
                            try
                            {
                                if (priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                {
                                    searchResult.BaggageIncludedInFare = priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                }
                                for (int k = 1; k < searchResult.Flights[0].Length; k++)
                                {
                                    if (searchResult.Flights[0][k].Status.ToUpper() != "VIA")
                                    {
                                        if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[k] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                        {
                                            searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                        {
                                            searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                        }
                                    }
                                }
                                if (request.Type == SearchType.Return && !request.SearchBySegments)
                                {
                                    if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                    {
                                        searchResult.BaggageIncludedInFare += "|" + priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                    }
                                    for (int k = 1; k < searchResult.Flights[1].Length; k++)
                                    {
                                        if (searchResult.Flights[1][k].Status.ToUpper() != "VIA")
                                        {
                                            if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[k] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                            {
                                                searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                            }
                                        }
                                        else
                                        {
                                            if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                            {
                                                searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetPaxFareBreakDown Method. Reason : " + ex.ToString(), "");
                            }
                            #endregion
                        }
                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            searchResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }
                        searchResult.Price.SupplierCurrency = currencyCode;
                        searchResult.BaseFare = (double)searchResult.Price.PublishedFare;
                        searchResult.Tax = (double)searchResult.Price.Tax;
                        searchResult.TotalFare = (double)(Math.Round((searchResult.BaseFare + searchResult.Tax), agentDecimalValue));
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetPaxFareBreakDown Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// Assign the charges for Adult and Child Pax Types
        /// </summary>
        /// <param name="charge"></param>
        /// <param name="index"></param>
        /// <param name="searchResult"></param>
        public void AssignCharges(BookingServiceCharge charge, int index, ref SearchResult searchResult)
        {
            try
            {
                if (charge.ChargeType == GoAir.BookingManager.ChargeType.FarePrice)
                {
                    double baseFare = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.FareBreakdown[index].BaseFare += baseFare;
                    searchResult.FareBreakdown[index].TotalFare += baseFare;
                    searchResult.FareBreakdown[index].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.Price.PublishedFare += (decimal)baseFare;
                    searchResult.Price.SupplierPrice += charge.Amount;
                }

                //DEDUCT THE PROMOTION DISCOUNT
                else if (charge.ChargeType == GoAir.BookingManager.ChargeType.PromotionDiscount)
                {
                    double discountAdult = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.FareBreakdown[index].BaseFare = searchResult.FareBreakdown[index].BaseFare - discountAdult;
                    searchResult.FareBreakdown[index].TotalFare = searchResult.FareBreakdown[index].TotalFare - discountAdult;
                    searchResult.FareBreakdown[index].SupplierFare = searchResult.FareBreakdown[index].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                    searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                }

                //For the below charge types  
                //====PromotionDiscount
                //======IncludedAddOnServiceFee
                //=====IncludedTax
                //====IncludedTravelFee
                //Do not compute the Tax Values
                else if (charge.ChargeType != GoAir.BookingManager.ChargeType.PromotionDiscount && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedAddOnServiceFee && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedTax && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedTravelFee)
                {
                    double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.FareBreakdown[index].TotalFare += tax;
                    searchResult.FareBreakdown[index].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.Price.Tax += (decimal)tax;
                    searchResult.Price.SupplierPrice += charge.Amount;
                    if (charge.TicketCode.ToString().Contains("SST") || charge.TicketCode.ToString().Contains("CST") || charge.TicketCode.ToString().Contains("IST"))
                    {
                        searchResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[index].PassengerCount);
                    }
                    if (searchResult.FareBreakdown[index].PassengerType == PassengerType.Adult)
                    {
                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                    }
                    else if (searchResult.FareBreakdown[index].PassengerType == PassengerType.Child)
                    {
                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute AssignCharges Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// Assign the charges for infant pax Type
        /// </summary>
        /// </summary>
        /// <param name="charge"></param>
        /// <param name="index"></param>
        /// <param name="searchResult"></param>
        public void AssignInftCharges(BookingServiceCharge charge, int index, ref SearchResult searchResult)
        {
            try
            {
                if (charge.ChargeType == GoAir.BookingManager.ChargeType.ServiceCharge)
                {
                    double baseFare = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.FareBreakdown[index].TotalFare += baseFare;
                    searchResult.FareBreakdown[index].BaseFare += baseFare;
                    searchResult.FareBreakdown[index].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.Price.PublishedFare += (decimal)baseFare;
                    searchResult.Price.SupplierPrice += charge.Amount;
                }
                //For the below charge types  
                //====PromotionDiscount
                //======IncludedAddOnServiceFee
                //=====IncludedTax
                //====IncludedTravelFee
                //Do not compute the Tax Values
                else if (charge.ChargeType != GoAir.BookingManager.ChargeType.PromotionDiscount && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedAddOnServiceFee && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedTax && charge.ChargeType != GoAir.BookingManager.ChargeType.IncludedTravelFee)
                {
                    double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.FareBreakdown[index].TotalFare += tax;
                    searchResult.FareBreakdown[index].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[index].PassengerCount);
                    searchResult.Price.Tax += (decimal)tax;
                    searchResult.Price.SupplierPrice += charge.Amount;
                    if (charge.TicketCode.ToString().Contains("SST") || charge.TicketCode.ToString().Contains("CST") || charge.TicketCode.ToString().Contains("IST"))
                    {
                        searchResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[index].PassengerCount);
                    }
                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute AssignInftCharges Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// Serializes the request and response objects
        /// </summary>
        /// <param name="type"></param>
        /// <param name="response"></param>
        /// <param name="fileName"></param>
        void WriteLogFile(Type type, object response, string fileName)
        {
            try
            {
                XmlSerializer ser = new XmlSerializer(type);
                string filePath = xmlLogPath + bookingSourceFlag +"_"+ sessionId + "_" + appUserId + fileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, response);
                sw.Close();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute WriteLogFile Method. Reason : " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Serializes the request and response objects 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="response"></param>
        /// <param name="fileName"></param>
        void WriteLogFile(Type type, object response, string fileName, Type[] types)
        {
            try
            {
                XmlSerializer ser = new XmlSerializer(type, types);
                string filePath = xmlLogPath + bookingSourceFlag+"_"+sessionId + "_" + appUserId + fileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, response);
                sw.Close();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute WriteLogFile Method with nullable types. Reason : " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Assigns the Pax SSR Codes
        /// </summary>
        /// <param name="paxSSR"></param>
        /// <param name="SSRCode"></param>
        /// <param name="arrivalStn"></param>
        /// <param name="depStn"></param>
        /// <param name="paxNumber"></param>
        private void AssignPaxSSR(ref PaxSSR paxSSR, string SSRCode, string arrivalStn, String depStn, int paxNumber)
        {
            try
            {
                paxSSR.State = GoAir.BookingManager.MessageState.New;
                paxSSR.ActionStatusCode = "NN";
                paxSSR.ArrivalStation = arrivalStn;
                paxSSR.DepartureStation = depStn;
                paxSSR.PassengerNumber = (Int16)paxNumber;
                paxSSR.SSRCode = SSRCode;
                paxSSR.SSRNumber = (Int16)0;
                paxSSR.SSRValue = (Int16)0;
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute AssignPaxSSR Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// Method returns the total number of passengers opted for meal and baggage selection in the itinerary.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private int GetSSRCount(FlightItinerary itinerary)
        {
            int bag_meal_selection = 0;
            try
            {

                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        if (!string.IsNullOrEmpty(itinerary.Passenger[i].BaggageType) || (!string.IsNullOrEmpty(itinerary.Passenger[i].MealType)))
                        {
                            bag_meal_selection++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetSSRCount Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return bag_meal_selection;
        }

        /// <summary>
        /// Gets the Infant Count from the itinerary object
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private int GetInfantCount(FlightItinerary itinerary)
        {
            int infantCount = 0;
            try
            {
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type == PassengerType.Infant)
                    {
                        infantCount++;
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetInfantCount Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return infantCount;
        }

        #endregion

        #region Cancel Booking

        /// <summary>
        /// Gets the booking details for the supplied PNR
        /// </summary>
        /// <param name="pnr"></param>
        /// <returns></returns>
        public GetBookingResponse GetBooking(string pnr)
        {
            IBookingManager bookingAPI = this.bookingAPI;
            GetBookingResponse response = new GetBookingResponse();
            try
            {
                GetBookingRequestData requestData = new GetBookingRequestData();
                requestData.GetBookingBy = GetBookingBy.RecordLocator;
                requestData.GetByRecordLocator = new GetByRecordLocator();
                requestData.GetByRecordLocator.RecordLocator = pnr;

                //New object for signature inclusion.
                GetBookingRequest getBookingRequestObj = new GetBookingRequest();
                getBookingRequestObj.ContractVersion = contractVersion;
                getBookingRequestObj.Signature = logonResponse.Signature;
                getBookingRequestObj.GetBookingReqData = requestData;
                getBookingRequestObj.EnableExceptionStackTrace = false;

                WriteLogFile(typeof(GetBookingRequest), getBookingRequestObj, "_GetBookingReq_");
                response = bookingAPI.GetBooking(getBookingRequestObj);
                WriteLogFile(typeof(GetBookingResponse), response, "_GetBookingRes_");
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetBooking Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return response;
        }

        /// <summary>
        /// Cancels the itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public Dictionary<string, string> CancelBooking(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();
            Login();
            try
            {
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature))
                {
                    GetBookingResponse response = GetBooking(itinerary.PNR);
                    CancelRequestData requestData = new CancelRequestData();
                    requestData.CancelBy = CancelBy.All;
                    CancelRequest cancelRequestObj = new CancelRequest();
                    cancelRequestObj.CancelRequestData = requestData;
                    cancelRequestObj.ContractVersion = contractVersion;
                    cancelRequestObj.Signature = logonResponse.Signature;
                    cancelRequestObj.EnableExceptionStackTrace = false;

                    WriteLogFile(typeof(CancelRequest), cancelRequestObj, "_CancelReq_");

                    CancelResponse cancelResponse = bookingAPI.Cancel(cancelRequestObj);
                    BookingUpdateResponseData responseData = cancelResponse.BookingUpdateResponseData;

                    WriteLogFile(typeof(CancelResponse), cancelResponse, "_CancelRes_");

                    if (responseData != null && responseData.Error == null)
                    {

                        string charges = responseData.Success.PNRAmount.TotalCost.ToString();
                        if (responseData.Success.PNRAmount.AlternateCurrencyBalanceDue > 0)
                        {
                            charges = responseData.Success.PNRAmount.AlternateCurrencyBalanceDue.ToString().Replace("-", "");
                        }
                        rateOfExchange = (responseData.Success.PNRAmount.AlternateCurrencyCode != null && responseData.Success.PNRAmount.AlternateCurrencyCode.Trim().Length > 0 ? exchangeRates[responseData.Success.PNRAmount.AlternateCurrencyCode] : exchangeRates[currencyCode]);
                        cancellationData.Add("Cancelled", "True");
                        cancellationData.Add("Charges", (Convert.ToDecimal(charges) * rateOfExchange).ToString());

                        BookingCommitResponse bookingCommitResponse = new BookingCommitResponse();
                        BookingCommitRequestData bookingCommitRequestData = new BookingCommitRequestData();

                        bookingCommitRequestData.State = GoAir.BookingManager.MessageState.New;
                        bookingCommitRequestData.RecordLocator = response.Booking.RecordLocator;
                        bookingCommitRequestData.CurrencyCode = response.Booking.CurrencyCode;

                        //New object for signature inclusion
                        BookingCommitRequest bookingCommitRequestObj = new BookingCommitRequest();
                        bookingCommitRequestObj.BookingCommitRequestData = bookingCommitRequestData;
                        bookingCommitRequestObj.ContractVersion = contractVersion;
                        bookingCommitRequestObj.Signature = logonResponse.Signature;
                        bookingCommitRequestObj.EnableExceptionStackTrace = false;


                        WriteLogFile(typeof(BookingCommitRequest), bookingCommitRequestObj, "_BookingCommitReq_");
                        bookingCommitResponse = bookingAPI.BookingCommit(bookingCommitRequestObj);
                        WriteLogFile(typeof(BookingCommitResponse), bookingCommitResponse, "_BookingCommitRes_");
                    }
                    else
                    {
                        cancellationData.Add("Cancelled", "False");
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute CancelBooking Method. Reason : " + ex.ToString(), "");
            }
            finally
            {
                Logout();
            }
            return cancellationData;
        }
        #endregion

        #region FareRules

        /// <summary>
        /// Returns the fare rules
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public List<FareRule> GetFareRules(SearchResult result)
        {
            List<FareRule> fareRules = new List<FareRule>();
            //S-1:Login
            //S-2:Fare Rule Request
            try
            {
                //S-1:Login
                if (!string.IsNullOrEmpty(result.RepriceErrorMessage))//For One-One Search
                {
                    logonResponse.Signature = result.RepriceErrorMessage;
                }
                else//For normal B2B flow
                {
                    Login();
                }
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature))
                {
                    //S-2:Fare Rule Request
                    GoAir.ContentManager.GetFareRuleInfoRequest fareRuleInfoRequest = new GoAir.ContentManager.GetFareRuleInfoRequest();
                    fareRuleInfoRequest.ContractVersion = contractVersion;
                    fareRuleInfoRequest.Signature = logonResponse.Signature;
                    fareRuleInfoRequest.EnableExceptionStackTrace = false;

                    GoAir.ContentManager.FareRuleRequestData fareRuleRequestData = new GoAir.ContentManager.FareRuleRequestData();
                    fareRuleRequestData.CultureCode = "en-GB";
                    fareRuleRequestData.CarrierCode = carrierCode;
                    fareRuleRequestData.ClassOfService = result.Flights[0][0].BookingClass;//Y
                    fareRuleRequestData.FareBasisCode = result.FareRules[0].FareBasisCode;//FO9RBINX
                    fareRuleRequestData.RuleNumber = "0001";
                    fareRuleInfoRequest.fareRuleReqData = fareRuleRequestData;

                    WriteLogFile(typeof(GoAir.ContentManager.GetFareRuleInfoRequest), fareRuleInfoRequest, "_FareRuleReq_");

                    GoAir.ContentManager.GetFareRuleInfoResponse fareRuleInfoResponse = contentAPI.GetFareRuleInfo(fareRuleInfoRequest);

                    WriteLogFile(typeof(GoAir.ContentManager.GetFareRuleInfoResponse), fareRuleInfoResponse, "_FareRuleRes_");

                    if (fareRuleInfoResponse != null)
                    {
                        byte[] encodedBytes = fareRuleInfoResponse.FareRuleInfo.Data;
                        string decodedText = System.Text.Encoding.UTF8.GetString(encodedBytes);

                        //To Get the text in plain format.
                        System.Windows.Forms.RichTextBox rtBox = new System.Windows.Forms.RichTextBox();
                        rtBox.Rtf = decodedText;
                        string plainText = rtBox.Text;

                        FareRule DNfareRules = new FareRule();
                        DNfareRules.Origin = result.Flights[0][0].Origin.CityCode;
                        DNfareRules.Destination = result.Flights[0][0].Destination.CityCode;
                        DNfareRules.Airline = carrierCode;
                        DNfareRules.FareRuleDetail = plainText;
                        DNfareRules.FareBasisCode = result.FareRules[0].FareBasisCode;
                        fareRules.Add(DNfareRules);
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetFareRules Method. Reason : " + ex.ToString(), "");
            }
            return fareRules;
        }

        #endregion

        #region For Bagggage and meals selection across segments

        /// <summary>
        /// Holds the itinerary for One-One Search
        /// </summary>
        /// <param name="searchResult"></param>
        /// <returns></returns>
        public BookingUpdateResponseData ExecuteSellForSearchByAvailability(SearchResult searchResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {

                FlightItinerary itinerary = new FlightItinerary();
                itinerary.Segments = SearchResult.GetSegments(searchResult);
                itinerary.TicketAdvisory = searchResult.FareSellKey;

                string onwardJourneySellKey = string.Empty;
                List<string> sellKeysListOnward = new List<string>();

                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();


                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }


                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < searchResult.FareBreakdown.Length; i++)
                {
                    if (searchResult.FareBreakdown[i] != null && searchResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < searchResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (searchResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                //APPLY PROMO CODE
                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;

                    //Apply service bundle codes to get relavent fares 
                    TimeSpan timeDiff = searchResult.Flights[0][0].DepartureTime - DateTime.Now;
                    if (searchResult.FareType.Contains("GOSMARTCORP ECONOMY"))
                    {
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ServiceBundleList = new string[] { timeDiff.TotalHours > 10 ? "CRPX" : "CRLM" };
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ApplyServiceBundle = ApplyServiceBundle.Yes;
                    }
                    else if (searchResult.FareType.Contains("CORPORATE BUSINESS"))
                    {
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ServiceBundleList = new string[] { timeDiff.TotalHours > 10 ? "GBPX" : "GBLM" };
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ApplyServiceBundle = ApplyServiceBundle.Yes;
                    }
                }            

                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = logonResponse.Signature;
                sellRequestObj.EnableExceptionStackTrace = false;
                WriteLogFile(typeof(SellRequest), sellRequestObj, "_SellReq_");


                SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                WriteLogFile(typeof(SellResponse), sellResponse, "_SellRes_");
                if (sellResponse != null && sellResponse.BookingUpdateResponseData != null)
                {
                    responseData = sellResponse.BookingUpdateResponseData;
                }
                //Added by lokesh on 20/12/2018
                //To Avoid Duplicate Bookings
                //Checking the segment count with the SSR response segment count
                //If that doesn't match we will call the LogOut method.
                //This Validation Does not allow the additional bookings that are already there in the signature.

                if (responseData != null && responseData.Success != null && responseData.Success.PNRAmount != null)
                {
                    int segCount = distinctSellKeysOnward.Count();
                    if (segCount != Convert.ToInt32(responseData.Success.PNRAmount.SegmentCount))
                    {
                        Logout();
                        throw new Exception("(GoAir)Segment Count Mismatch ! Please Search Again..");
                    }
                }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute ExecuteSellForSearchByAvailability Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        /// <summary>
        /// This method returns the Available SSR's based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetAvailableSSR(SearchResult result)
        {
            DataTable dtSourceBaggage = null;
            try
            {
                //SSR : AdditionalBaggage and Meals [For Both Onward and Return Journeys]
                //Note:Infant is also an SSR.
                //But we will get the infant price from the price itinerary response.

                //Steps to get the available SSR Without GST input.
                //1.Login
                //2.Sell [Holds the complete itinerary]
                //3.GetSSRAvailabilityForBooking.

                //Steps to get the available SSR With GST input.
                //1.Login
                //2.UpdateContactRequest 
                //3.Sell[Holds the complete itinerary]
                //4.GetSSRAvailabilityForBooking
                //5.GetBookingFromState

                //Step1: Login
                if (result != null && !string.IsNullOrEmpty(result.RepriceErrorMessage))//For One-One Search
                {
                    //Here the signature is different for 2 different results
                    //So from the result object we need to assign here
                    //till search to book unique signature should be carried.
                    logonResponse.Signature = result.RepriceErrorMessage;
                }
                else//For Normal Search 
                {
                    Login();
                }
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature))
                {

                    //This method will releases the itinerary.
                    //Clears the previous objects from the session if any objects are there.
                    //This method will avoid duplicate bookings.
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = logonResponse.Signature;

                    WriteLogFile(typeof(ClearRequest), clearRequest, "_ClearReq_");

                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);

                    WriteLogFile(typeof(ClearResponse), clearResponse, "_ClearRes_");


                    //Note:Verify any GST input available then execute UpdateContactsRequest first.
                    //Step-1.1: If any GST input then UpdateContactGSTDetails method
                    UpdateContactsResponse updateGSTResponse = null;
                    if (!string.IsNullOrEmpty(gstCompanyName) && (gstCompanyName.Length > 0) &&
                       !string.IsNullOrEmpty(gstNumber) && (gstNumber.Length > 0) &&
                        !string.IsNullOrEmpty(gstOfficialEmail) && (gstOfficialEmail.Length > 0)
                       && !string.IsNullOrEmpty(logonResponse.Signature))
                    {

                        updateGSTResponse = UpdateContactGSTDetails();

                    }
                    //Step2:Sell
                    BookingUpdateResponseData responseData = null;
                    if (searchByAvailability)
                    {
                        responseData = ExecuteSellForSearchByAvailability(result);
                    }
                    else
                    {
                        responseData = ExecuteSell(result);
                    }
                    if (responseData != null)
                    {
                        //Step-3: GetAvailableSSR

                        GetSSRAvailabilityForBookingResponse ssrAvaialbleResponse = new GetSSRAvailabilityForBookingResponse();
                        GetSSRAvailabilityForBookingRequest ssrRequest = new GetSSRAvailabilityForBookingRequest();
                        ssrRequest.ContractVersion = contractVersion;
                        ssrRequest.Signature = logonResponse.Signature;

                        SSRAvailabilityForBookingRequest availRequest = new SSRAvailabilityForBookingRequest();
                        availRequest.InventoryControlled = true;
                        availRequest.NonInventoryControlled = true;
                        availRequest.SeatDependent = true;
                        availRequest.NonSeatDependent = true;
                        availRequest.CurrencyCode = currencyCode;

                        List<LegKey> legKeysList = new List<LegKey>();
                        for (int i = 0; i < result.Flights.Length; i++)
                        {
                            for (int j = 0; j < result.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = carrierCode;
                                legKey.FlightNumber = result.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = result.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = result.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }
                        if (legKeysList != null && legKeysList.Count > 0)
                        {
                            availRequest.SegmentKeyList = legKeysList.ToArray();
                        }
                        ssrRequest.SSRAvailabilityForBookingRequest = availRequest;

                        WriteLogFile(typeof(GetSSRAvailabilityForBookingRequest), ssrRequest, "_AvailableSSR_Req_");

                        ssrAvaialbleResponse = bookingAPI.GetSSRAvailabilityForBooking(ssrRequest);
                        WriteLogFile(typeof(GetSSRAvailabilityForBookingResponse), ssrAvaialbleResponse, "_AvailableSSR_Res_");

                        SSRAvailabilityForBookingResponse ssrResponse = null;
                        if (ssrAvaialbleResponse != null && ssrAvaialbleResponse.SSRAvailabilityForBookingResponse != null)
                        {
                            ssrResponse = ssrAvaialbleResponse.SSRAvailabilityForBookingResponse;
                        }
                        if (ssrResponse != null && ssrResponse.SSRSegmentList != null && ssrResponse.SSRSegmentList.Length > 0)
                        {
                            dtSourceBaggage = new DataTable();
                            if (dtSourceBaggage.Columns.Count == 0)
                            {
                                dtSourceBaggage.Columns.Add("Code", typeof(string));
                                dtSourceBaggage.Columns.Add("Price", typeof(decimal));
                                dtSourceBaggage.Columns.Add("Group", typeof(int));
                                dtSourceBaggage.Columns.Add("Currency", typeof(string));
                                dtSourceBaggage.Columns.Add("Description", typeof(string));
                                dtSourceBaggage.Columns.Add("QtyAvailable", typeof(int));
                            }
                            List<AvailablePaxSSR> availablePaxSSRs_OnwardBag = new List<AvailablePaxSSR>();//OnwardBaggage
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnBag = new List<AvailablePaxSSR>();//ReturnBaggage

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardMeal = new List<AvailablePaxSSR>();//OnwardMeal
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnMeal = new List<AvailablePaxSSR>();//ReturnMeal

                            List<string> SSR_MealCodes = new List<string>();
                            SSR_MealCodes.Add("SAVO");//Samosa Savoury
                            SSR_MealCodes.Add("JNML");//Jain Sandwich
                            SSR_MealCodes.Add("SUNV");//Non Veg Sub Roll
                            SSR_MealCodes.Add("SUVG");//Veg Sub Roll
                            SSR_MealCodes.Add("SWNV");//Non Veg Sandwich
                            SSR_MealCodes.Add("SWVG");//Veg Sandwich
                            SSR_MealCodes.Add("OLSC");//Samosa and Chai
                            SSR_MealCodes.Add("OLNB");//Non Veg Sub and Coke
                            SSR_MealCodes.Add("OLNS");//Non Veg Sandwich and coke
                            SSR_MealCodes.Add("OLVB");//Veg Sub and Coke
                            SSR_MealCodes.Add("OLVS");//Veg Sandwich and Coke
                            SSR_MealCodes.Add("FEST");//Festive Meal
                            SSR_MealCodes.Add("PMVG");//Premium Meal Tortilla Wrap Veg and Coke
                            SSR_MealCodes.Add("PMNV");//Premium Meal Tortilla Wrap non-Veg and Coke
                            SSR_MealCodes.Add("SSNV");//Smart Snack nv.
                            SSR_MealCodes.Add("SSVG");//Smart Snack Veg.
                            SSR_MealCodes.Add("KDML");//Smart Kid Bag.
                            SSR_MealCodes.Add("DCPP");//Maa ke haath jaisa Daal Chawal + Paperboat.
                            SSR_MealCodes.Add("KCPP");//Maa ke haath jaisa Kadi Chawal + Paperboat.
                            SSR_MealCodes.Add("SRPP");//Maa ke haath jaisa Sambar rice + Paperboat.
                            SSR_MealCodes.Add("CRPX");//Corporate free meal.

                            for (int i = 0; i < result.Flights.Length; i++)
                            {
                                for (int j = 0; j < result.Flights[i].Select(x => x.FlightNumber).Distinct().ToArray().Count(); j++)
                                {
                                    string DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = result.Flights[i][j].Destination.AirportCode;

                                    for (int p = 0; p < ssrResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            if (i == 0)
                                            {
                                                availablePaxSSRs_OnwardBag.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.StartsWith("XC"))).ToList());
                                                if (sSRSegment.AvailablePaxSSRList.Any(s => (SSR_MealCodes.Contains(s.SSRCode))))
                                                {
                                                    availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                                }
                                            }
                                            else
                                            {
                                                availablePaxSSRs_ReturnBag.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.StartsWith("XC"))).ToList());
                                                if (sSRSegment.AvailablePaxSSRList.Any(s => (SSR_MealCodes.Contains(s.SSRCode))))
                                                {
                                                    availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            List<string> SSR_BaggageCodes = new List<string>();
                            SSR_BaggageCodes.Add("XC05");//Pre purchase excess baggage 5  Kgs
                            SSR_BaggageCodes.Add("XC10");//Pre purchase excess baggage 10 Kgs
                            SSR_BaggageCodes.Add("XC15");//Pre purchase excess baggage 15 Kgs
                            SSR_BaggageCodes.Add("XC30");//Pre purchase excess baggage 30 Kgs

                            if (availablePaxSSRs_OnwardBag.Count > 0)
                            {
                                foreach (string baggageCode in SSR_BaggageCodes)
                                {
                                    AvailablePaxSSR bagggeSSR = availablePaxSSRs_OnwardBag.Where(m => m.SSRCode.Contains(baggageCode)).FirstOrDefault();
                                    if (bagggeSSR != null)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        if (bagggeSSR.PaxSSRPriceList != null)
                                        {
                                            paxSSRPrices.AddRange(bagggeSSR.PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal baggagePrice = charges.Where(o => o.ChargeType == GoAir.BookingManager.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];
                                       
                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = baggageCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(baggagePrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "0";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (baggageCode)
                                        {
                                            case "XC05":
                                                dr["Description"] = "Prepaid Excess Baggage –5Kg";
                                                break;
                                            case "XC10":
                                                dr["Description"] = "Prepaid Excess Baggage –10Kg";
                                                break;
                                            case "XC15":
                                                dr["Description"] = "Prepaid Excess Baggage –15Kg";
                                                break;
                                            case "XC30":
                                                dr["Description"] = "Prepaid Excess Baggage –30Kg";
                                                break;
                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            } //OnwardBaggage

                            if (availablePaxSSRs_ReturnBag.Count > 0)
                            {
                                foreach (string baggageCode in SSR_BaggageCodes)
                                {
                                    AvailablePaxSSR bagggeSSR = availablePaxSSRs_ReturnBag.Where(m => m.SSRCode.Contains(baggageCode)).FirstOrDefault();
                                    if (bagggeSSR != null)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        if (bagggeSSR.PaxSSRPriceList != null)
                                        {
                                            paxSSRPrices.AddRange(bagggeSSR.PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal baggagePrice = charges.Where(o => o.ChargeType == GoAir.BookingManager.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = baggageCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(baggagePrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "1";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (baggageCode)
                                        {
                                            case "XC05":
                                                dr["Description"] = "Prepaid Excess Baggage –5Kg";
                                                break;
                                            case "XC10":
                                                dr["Description"] = "Prepaid Excess Baggage –10Kg";
                                                break;
                                            case "XC15":
                                                dr["Description"] = "Prepaid Excess Baggage –15Kg";
                                                break;
                                            case "XC30":
                                                dr["Description"] = "Prepaid Excess Baggage –30Kg";
                                                break;
                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            } //ReturnBaggage

                            if (availablePaxSSRs_OnwardMeal.Count > 0)
                            {
                                foreach (string mealCode in SSR_MealCodes)
                                {
                                    List<AvailablePaxSSR> mealSSR = availablePaxSSRs_OnwardMeal.Where(m => m.SSRCode.Contains(mealCode)).ToList();
                                    if (mealSSR != null && mealSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < mealSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(mealSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal mealPrice = charges.Where(o => o.ChargeType == GoAir.BookingManager.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = (charges.Count > 0 ? exchangeRates[charges[0].CurrencyCode] : 1);
                                        //For Corporate Fare Add Corporate Default Meal.
                                        if (mealCode == "CRPX" && (result.FareType.Contains("GB CORPORATE BUSINESS") || result.FareType.Contains("GOSMARTCORP ECONOMY")))
                                        {
                                            DataRow drCorpMeal = dtSourceBaggage.NewRow();
                                            drCorpMeal["Code"] = "CRPX";
                                            drCorpMeal["Price"] = Math.Round(Convert.ToDecimal(0) * rateOfExchange, agentDecimalValue);
                                            drCorpMeal["Group"] = "0";
                                            drCorpMeal["Currency"] = agentBaseCurrency;
                                            drCorpMeal["QtyAvailable"] = 5000;
                                            drCorpMeal["Description"] = "Corporate Default Meal";
                                            dtSourceBaggage.Rows.Add(drCorpMeal);
                                        }
                                        else
                                        {
                                            DataRow dr = dtSourceBaggage.NewRow();
                                            dr["Code"] = mealCode;
                                            dr["Price"] = Math.Round(Convert.ToDecimal(mealPrice) * rateOfExchange, agentDecimalValue);
                                            dr["Group"] = "0";
                                            dr["Currency"] = agentBaseCurrency;

                                            switch (mealCode)
                                            {
                                                case "SAVO":
                                                    dr["Description"] = "Samosa Savoury";
                                                    break;
                                                case "JNML":
                                                    dr["Description"] = "Jain Sandwich";
                                                    break;
                                                case "SUNV":
                                                    dr["Description"] = "Non Veg Sub Roll";
                                                    break;
                                                case "SUVG":
                                                    dr["Description"] = "Veg Sub Roll";
                                                    break;
                                                case "SWNV":
                                                    dr["Description"] = "Non Veg Sandwich";
                                                    break;
                                                case "SWVG":
                                                    dr["Description"] = "Veg Sandwich";
                                                    break;
                                                case "OLSC":
                                                    dr["Description"] = "Samosa and Chai";
                                                    break;
                                                case "OLNB":
                                                    dr["Description"] = "Non Veg Sub and Coke";
                                                    break;
                                                case "OLNS":
                                                    dr["Description"] = "Non Veg Sandwich and coke";
                                                    break;
                                                case "OLVB":
                                                    dr["Description"] = "Veg Sub and Coke";
                                                    break;
                                                case "OLVS":
                                                    dr["Description"] = "Veg Sandwich and Coke";
                                                    break;
                                                case "FEST":
                                                    dr["Description"] = "Festive Meal";
                                                    break;
                                                case "PMVG":
                                                    dr["Description"] = "Premium Meal Tortilla Wrap Veg + Coke";
                                                    break;
                                                case "PMNV":
                                                    dr["Description"] = "Premium Meal Tortilla Wrap non-Veg + Coke";
                                                    break;
                                                case "SSNV":
                                                    dr["Description"] = "Smart Snack NV.";
                                                    break;
                                                case "SSVG":
                                                    dr["Description"] = "Smart Snack Veg.";
                                                    break;
                                                case "KDML":
                                                    dr["Description"] = "Smart Kid Bag.";
                                                    break;
                                                case "DCPP":
                                                    dr["Description"] = "Maa ke haath jaisa Daal Chawal + Paperboat.";
                                                    break;
                                                case "KCPP":
                                                    dr["Description"] = "Maa ke haath jaisa Kadi Chawal + Paperboat";
                                                    break;
                                                case "SRPP":
                                                    dr["Description"] = "Maa ke haath jaisa Sambar rice + Paperboat.";
                                                    break;
                                            }
                                            dr["QtyAvailable"] = 5000;
                                            dtSourceBaggage.Rows.Add(dr);
                                        }

                                        

                                    }

                                }
                            } //OnwardMeal

                            if (availablePaxSSRs_ReturnMeal.Count > 0) //ReturnMeal
                            {
                                foreach (string mealCode in SSR_MealCodes)
                                {
                                    List<AvailablePaxSSR> mealSSR = availablePaxSSRs_ReturnMeal.Where(m => m.SSRCode.Contains(mealCode)).ToList();
                                    if (mealSSR != null && mealSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < mealSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(mealSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal mealPrice = charges.Where(o => o.ChargeType == GoAir.BookingManager.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = (charges.Count > 0 ? exchangeRates[charges[0].CurrencyCode] : 1);
                                        if (mealCode == "CRPX" && (result.FareType.Contains("GB CORPORATE BUSINESS") || result.FareType.Contains("GOSMARTCORP ECONOMY")))
                                        {
                                            DataRow drCorpMeal = dtSourceBaggage.NewRow();
                                            drCorpMeal["Code"] = "CRPX";
                                            drCorpMeal["Price"] = Math.Round(Convert.ToDecimal(0) * rateOfExchange, agentDecimalValue);
                                            drCorpMeal["Group"] = "1";
                                            drCorpMeal["Currency"] = agentBaseCurrency;
                                            drCorpMeal["QtyAvailable"] = 5000;
                                            drCorpMeal["Description"] = "Corporate Default Meal";
                                            dtSourceBaggage.Rows.Add(drCorpMeal);
                                        }
                                        else
                                        {
                                            DataRow dr = dtSourceBaggage.NewRow();
                                            dr["Code"] = mealCode;
                                            dr["Price"] = Math.Round(Convert.ToDecimal(mealPrice) * rateOfExchange, agentDecimalValue);
                                            dr["Group"] = "1";
                                            dr["Currency"] = agentBaseCurrency;
                                            switch (mealCode)
                                            {
                                                case "SAVO":
                                                    dr["Description"] = "Samosa Savoury";
                                                    break;
                                                case "JNML":
                                                    dr["Description"] = "Jain Sandwich";
                                                    break;
                                                case "SUNV":
                                                    dr["Description"] = "Non Veg Sub Roll";
                                                    break;
                                                case "SUVG":
                                                    dr["Description"] = "Veg Sub Roll";
                                                    break;
                                                case "SWNV":
                                                    dr["Description"] = "Non Veg Sandwich";
                                                    break;
                                                case "SWVG":
                                                    dr["Description"] = "Veg Sandwich";
                                                    break;
                                                case "OLSC":
                                                    dr["Description"] = "Samosa and Chai";
                                                    break;
                                                case "OLNB":
                                                    dr["Description"] = "Non Veg Sub and Coke";
                                                    break;
                                                case "OLNS":
                                                    dr["Description"] = "Non Veg Sandwich and coke";
                                                    break;
                                                case "OLVB":
                                                    dr["Description"] = "Veg Sub and Coke";
                                                    break;
                                                case "OLVS":
                                                    dr["Description"] = "Veg Sandwich and Coke";
                                                    break;
                                                case "FEST":
                                                    dr["Description"] = "Festive Meal";
                                                    break;
                                                case "PMVG":
                                                    dr["Description"] = "Premium Meal Tortilla Wrap Veg + Coke";
                                                    break;
                                                case "PMNV":
                                                    dr["Description"] = "Premium Meal Tortilla Wrap non-Veg + Coke";
                                                    break;
                                                case "SSNV":
                                                    dr["Description"] = "Smart Snack NV.";
                                                    break;
                                                case "SSVG":
                                                    dr["Description"] = "Smart Snack Veg.";
                                                    break;
                                                case "KDML":
                                                    dr["Description"] = "Smart Kid Bag.";
                                                    break;
                                                case "DCPP":
                                                    dr["Description"] = "Maa ke haath jaisa Daal Chawal + Paperboat.";
                                                    break;
                                                case "KCPP":
                                                    dr["Description"] = "Maa ke haath jaisa Kadi Chawal + Paperboat";
                                                    break;
                                                case "SRPP":
                                                    dr["Description"] = "Maa ke haath jaisa Sambar rice + Paperboat.";
                                                    break;
                                            }
                                            dr["QtyAvailable"] = 5000;
                                            dtSourceBaggage.Rows.Add(dr);
                                        }
                                    }
                                }
                            } //ReturnMeal

                        }
                    }
                }
            }
            catch (Exception ex)
            {


                //In case of exception in any of the above requests call the clear method.
                //This method will release the itinerary.
                //Clears the session.
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature))
                {
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = logonResponse.Signature;
                    WriteLogFile(typeof(ClearRequest), clearRequest, "_ClearReq_");
                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                    WriteLogFile(typeof(ClearResponse), clearResponse, "_ClearRes_");
                }
                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetAvailableSSR Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return dtSourceBaggage;
        }

        /// <summary>
        /// Holds the itinerary for normal search
        /// </summary>
        /// <param name="searchResult"></param>
        /// <returns></returns>
        public BookingUpdateResponseData ExecuteSell(SearchResult searchResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {

                FlightItinerary itinerary = new FlightItinerary();
                itinerary.Segments = SearchResult.GetSegments(searchResult);
                itinerary.TicketAdvisory = searchResult.FareSellKey;

                string onwardJourneySellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    if (itinerary.Segments[p].Group == 0)//Onward
                    {
                        sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                    if (itinerary.Segments[p].Group == 1)//Return
                    {
                        sellKeysListReturn.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }
                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                if (itinerary.TicketAdvisory.Split('|').Length > 1)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                if (itinerary.TicketAdvisory.Split('|').Length > 1)//Round trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < searchResult.FareBreakdown.Length; i++)
                {
                    if (searchResult.FareBreakdown[i] != null && searchResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < searchResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (searchResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                //APPLY PROMO CODE
                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;

                    //Apply service bundle codes to get relavent fares 
                    TimeSpan timeDiff = searchResult.Flights[0][0].DepartureTime - DateTime.Now;
                    if (searchResult.FareType.Contains("GOSMARTCORP ECONOMY"))
                    {
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ServiceBundleList = new string[] { timeDiff.TotalHours > 10 ? "CRPX" : "CRLM" };
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ApplyServiceBundle = ApplyServiceBundle.Yes;
                    }
                    else if (searchResult.FareType.Contains("CORPORATE BUSINESS"))
                    {
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ServiceBundleList = new string[] { timeDiff.TotalHours > 10 ? "GBPX" : "GBLM" };
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ApplyServiceBundle = ApplyServiceBundle.Yes;
                    }
                }

                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = logonResponse.Signature;
                sellRequestObj.EnableExceptionStackTrace = false;
                WriteLogFile(typeof(SellRequest), sellRequestObj, "_SellReq_");

                SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                if (sellResponse != null && sellResponse.BookingUpdateResponseData != null)
                {
                    responseData = sellResponse.BookingUpdateResponseData;
                }
                WriteLogFile(typeof(SellResponse), sellResponse, "_SellRes_");

                //Added by lokesh on 20/12/2018
                //To Avoid Duplicate Bookings
                //Checking the segment count with the SSR response segment count
                //If that doesn't match we will call the LogOut method.
                //This Validation Does not allow the additional bookings that are already there in the signature.
                if (responseData != null && responseData.Success != null && responseData.Success.PNRAmount != null)
                {
                    int segCount = distinctSellKeysOnward.Count();
                    if (distinctSellKeysReturn.Count() > 0)
                    {
                        segCount = segCount + distinctSellKeysReturn.Count();
                    }
                    if (segCount != Convert.ToInt32(responseData.Success.PNRAmount.SegmentCount))
                    {
                        Logout();
                        throw new Exception("(GoAir)Segment Count Mismatch ! Please Search Again..");
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to execute (ExecuteSell) Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        /// <summary>
        /// This method returns the Seat Availability response based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public SeatAvailabilityResp GetSeatAvailableSSR(SearchResult result, string sOrigin, string sDestination,
            SeatAvailabilityResp clsSeatAvailabilityResp, string session)
        {
            //Note1:for normal search signature will be assigned from the basket.
            //Note2:for one -one search signature will be assigned from the itinerary object.
            SegmentSeats clsSegmentSeats = new SegmentSeats();
            LogonResponse loginResponse = new LogonResponse();
            try
            {
                //Step1: Assign API credentails and Login
                if (result != null && !string.IsNullOrEmpty(result.RepriceErrorMessage) && result.RepriceErrorMessage.Length > 0)//For One-One Search
                {
                    //Here the signature is different for 2 different results
                    //So from the result object we need to assign here
                    //till seaech to book unique signature should be carried.
                    loginResponse.Signature = result.RepriceErrorMessage;
                }
                else//For Normal Search 
                {
                    loginResponse = Login();
                }
                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
                {
                    SegmentSeats seg = clsSeatAvailabilityResp.SegmentSeat.Where(x => x.Origin == sOrigin && x.Destination == sDestination).FirstOrDefault();

                    GetSeatAvailabilityRequest clsGetSeatAvailabilityRequest = new GetSeatAvailabilityRequest();

                    clsGetSeatAvailabilityRequest.ContractVersion = contractVersion;
                    clsGetSeatAvailabilityRequest.Signature = loginResponse.Signature;

                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest = new SeatAvailabilityRequest();
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.STD = seg.STD;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.DepartureStation = seg.Origin;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.ArrivalStation = seg.Destination;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.IncludeSeatFees = true;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.SeatAssignmentMode = SeatAssignmentMode.PreSeatAssignment;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.FlightNumber = seg.FlightNo;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.CarrierCode = "G8";
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.EnforceSeatGroupRestrictions = true;

                    GenericStatic.WriteLogFile(typeof(GetSeatAvailabilityRequest), clsGetSeatAvailabilityRequest,
                        xmlLogPath + sessionId + "_" + appUserId + "_" + seg.Origin + "_" + seg.Destination + "_" + "G8_GetSeatAvailabilityRequest_");

                    GetSeatAvailabilityResponse clsGetSeatAvailabilityResponse = bookingAPI.GetSeatAvailability(clsGetSeatAvailabilityRequest);

                    string filePath = GenericStatic.WriteLogFile(typeof(GetSeatAvailabilityResponse), clsGetSeatAvailabilityResponse,
                        xmlLogPath + sessionId + "_" + appUserId + "_" + seg.Origin + "_" + seg.Destination + "_" + "G8_GetSeatAvailabilityResponse_");

                    clsSegmentSeats = GenericStatic.TransformXMLAndDeserialize(clsSegmentSeats, File.ReadAllText(filePath), sSeatAvailResponseXSLT);
                    clsSegmentSeats.SeatInfoDetails = clsSegmentSeats.SeatInfoDetails.GroupBy(x => x.SeatNo).Select(y => y.First()).ToList();

                    var itemIndex = clsSeatAvailabilityResp.SegmentSeat.FindIndex(x => x.Origin == sOrigin && x.Destination == sDestination);
                    clsSeatAvailabilityResp.SegmentSeat.RemoveAt(itemIndex);
                    clsSeatAvailabilityResp.SegmentSeat.Insert(itemIndex, clsSegmentSeats);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(GoAir)Failed to get seat availability response. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return clsSeatAvailabilityResp;
        }

        #endregion

        #region Re -Pricing Methods

        /// <summary>
        /// Gets the Itinerary Price for Onward Trip
        /// </summary>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="journey"></param>
        /// <param name="logFileName"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPrice(string journeySellKey, string fareSellKey, Journey journey, string logFileName)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            try
            {
                IBookingManager bookingAPI = this.bookingAPI;
                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = logonResponse.Signature;
                priceItinRequest.ContractVersion = contractVersion;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(request.AdultCount + request.ChildCount);


                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[1];
                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[2];
                }

                //Adult Pax Price Types
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxCount = (Int16)(request.AdultCount);

                //Child Pax Price Types
                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxCount = (Int16)(request.ChildCount);
                }
                //Infant Pax Price Types
                if (request.InfantCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = currencyCode;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[journey.Segments.Length];

                    for (int m = 0; m < journey.Segments.Length; m++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m] = new SegmentSSRRequest();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].ArrivalStation = journey.Segments[m].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].DepartureStation = journey.Segments[m].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].FlightDesignator = journey.Segments[m].FlightDesignator;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].STD = journey.Segments[m].STD;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs = new PaxSSR[request.InfantCount];
                        for (int i = 0; i < request.InfantCount; i++)
                        {
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i] = new PaxSSR();
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ActionStatusCode = "NN";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ArrivalStation = journey.Segments[m].ArrivalStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].DepartureStation = journey.Segments[m].DepartureStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].PassengerNumber = (Int16)i;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRCode = "INFT";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRNumber = 0;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRValue = 0;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].IsInServiceBundle = false;
                        }
                    }
                }
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                //FOR APPLYING PROMO CODE
                if (!string.IsNullOrEmpty(promoCode))
                {
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
                }
                priceItinRequest.EnableExceptionStackTrace = false;

                WriteLogFile(typeof(PriceItineraryRequest), priceItinRequest, "_ItineraryReq_" + logFileName);
                piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                WriteLogFile(typeof(PriceItineraryResponse), piResponse, "_ItineraryRes_" + logFileName);

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetItineraryPrice for one way Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return piResponse;
        }

        /// <summary>
        /// Gets the Itinerary Price for Round Trip
        /// </summary>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="journey"></param>
        /// <param name="logFileName"></param>
        /// <param name="fareSellKeyRet"></param>
        /// <param name="journeySellKeyRet"></param>
        /// <param name="ReturnJouney"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPrice(string journeySellKey, string fareSellKey, Journey journey, string logFileName, string fareSellKeyRet, string journeySellKeyRet, Journey ReturnJouney)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            try
            {
                IBookingManager bookingAPI = this.bookingAPI;
                int paxCount = request.AdultCount + request.ChildCount;

                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = logonResponse.Signature;
                priceItinRequest.ContractVersion = contractVersion;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(request.AdultCount + request.ChildCount + request.InfantCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = fareSellKeyRet;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = journeySellKeyRet;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[1];
                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[2];
                }

                //Adult Pax Price Types
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxCount = (Int16)(request.AdultCount);

                //Child Pax Price Types
                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxCount = (Int16)(request.ChildCount);
                }
                if (request.InfantCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = currencyCode;
                    int segcount = journey.Segments.Length + ReturnJouney.Segments.Length;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[segcount];
                    int f = -1;
                    for (int m = 0; m < journey.Segments.Length; m++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m] = new SegmentSSRRequest();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].ArrivalStation = journey.Segments[m].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].DepartureStation = journey.Segments[m].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].FlightDesignator = journey.Segments[m].FlightDesignator;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].STD = journey.Segments[m].STD;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs = new PaxSSR[request.InfantCount];
                        for (int i = 0; i < request.InfantCount; i++)
                        {

                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i] = new PaxSSR();
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ActionStatusCode = "NN";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ArrivalStation = journey.Segments[m].ArrivalStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].DepartureStation = journey.Segments[m].DepartureStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].PassengerNumber = (Int16)i;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRCode = "INFT";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRNumber = 0;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRValue = 0;
                        }
                        f++;
                    }

                    for (int d = 0; d < ReturnJouney.Segments.Length; d++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1] = new SegmentSSRRequest();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].ArrivalStation = ReturnJouney.Segments[d].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].DepartureStation = ReturnJouney.Segments[d].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].FlightDesignator = ReturnJouney.Segments[d].FlightDesignator;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].STD = ReturnJouney.Segments[d].STD;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs = new PaxSSR[request.InfantCount];
                        for (int i = 0; i < request.InfantCount; i++)
                        {

                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i] = new PaxSSR();
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].ActionStatusCode = "NN";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].ArrivalStation = ReturnJouney.Segments[d].ArrivalStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].DepartureStation = ReturnJouney.Segments[d].DepartureStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].PassengerNumber = (Int16)i;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRCode = "INFT";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRNumber = 0;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRValue = 0;
                        }
                        f++;
                    }
                }
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                //FOR APPLYING PROMO CODE
                if (!string.IsNullOrEmpty(promoCode))
                {
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
                }
                priceItinRequest.EnableExceptionStackTrace = false;
                WriteLogFile(typeof(PriceItineraryRequest), priceItinRequest, "_ItineraryPriceReq_" + logFileName);
                piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                WriteLogFile(typeof(Booking), piResponse.Booking, "_ItineraryPriceRes_" + logFileName);
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetItineraryPrice for Round Trip Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return piResponse;
        }

        /// <summary>
        /// Gets the Re-Priced Fare . 
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="request"></param>
        public void GetRePricedFare(ref SearchResult resultObj, SearchRequest request)
        {
            try
            {

                this.request = request;
                //List which holds the Pax types  : "ADT","CHD" ,"INFT";
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }
                if (request.InfantCount > 0)
                {
                    FarePaxtypes.Add("INFT");
                }
                string logFileName = string.Empty;
                PriceItineraryResponse itineraryResponse = null;
                if (!string.IsNullOrEmpty(resultObj.RepriceErrorMessage))//For One-One Search
                {
                    logonResponse.Signature = resultObj.RepriceErrorMessage;
                }
                else//For Normal Search
                {
                    Login();
                }
                if (request.Type == SearchType.OneWay || request.SearchBySegments)
                {
                    if (resultObj.Flights[0].Length == 1)//Onward Journey Direct Flights
                    {
                        logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    }
                    if (resultObj.Flights[0].Length > 1) //Onward Journey Connecting or Via Flights
                    {
                        for (int m = 0; m < resultObj.Flights[0].Length; m++)
                        {
                            if (resultObj.Flights[0][m].Status.ToUpper() != "VIA")
                            {
                                if (!string.IsNullOrEmpty(logFileName))
                                {
                                    logFileName += "_" + resultObj.Flights[0][1].FlightNumber + "_" + resultObj.FareType;// + "_" + Convert.ToString(resultObj.Flights[0][1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                }
                                else
                                {
                                    logFileName = resultObj.Flights[0][m].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_");// + "_" + Convert.ToString(resultObj.Flights[0][m].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                }
                            }
                            else
                            {
                                logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_");// + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            }
                        }
                    }
                    itineraryResponse = GetItineraryPrice(resultObj.JourneySellKey, resultObj.FareSellKey, null, logFileName);
                }
                else if (request.Type == SearchType.Return)
                {
                    //Both onward and return journeys are direct flights.
                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length == 1)
                    {
                        logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_");// + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        logFileName += "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_");// + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    }
                    //Both onward and return journeys may be connecting or via flights.
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length > 1)
                    {
                        logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_");// + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        //logFileName += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        logFileName += "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_");// + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        //logFileName += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    }
                    //Onward may be connecting or via and return direct flights
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length == 1)
                    {
                        logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_");// + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        //logFileName += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        logFileName += "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_");// + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    }
                    //Onward direct flight and return may be via or connecting flights
                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length > 1)
                    {
                        logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_");// + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        logFileName += "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_");// + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        //logFileName += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    }
                    itineraryResponse = GetItineraryPrice(resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], null, logFileName, resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], null);
                }
                if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                {

                    resultObj.Price = new PriceAccounts();
                    if (request.AdultCount > 0)
                    {
                        resultObj.FareBreakdown[0] = new Fare();
                        resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                        resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                    }
                    if (request.ChildCount > 0)
                    {
                        resultObj.FareBreakdown[1] = new Fare();
                        resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                        resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                    }
                    rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                    GetPaxFareBreakDown(itineraryResponse, ref resultObj);
                }
            }
            catch (Exception ex)
            {

                Logout();
                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute GetRePricedFare Method. Reason : " + ex.ToString(), "");
                throw new Exception("(GoAir) No fare available", ex);
            }
        }

        #endregion

        #region GST Methods

        /// <summary>
        /// Gets the GetUpdated GST Fare Break Up.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="request"></param>
        public void GetUpdatedGSTFareBreakUp(ref SearchResult result, SearchRequest request) // Added by Ziyad 22-nov-2018
        {
            //Note1:For Normal Search Signature will be assigned from the basket from the user id.
            //Note2:For One-One Search Signature Will be assigned from the itinerary object [endorsement property]
            try
            {
                //If the lead pax supplies the GST input.
                //Then Get the updated GST farebreak up.

                if (!string.IsNullOrEmpty(result.RepriceErrorMessage) && result.RepriceErrorMessage.Length > 0)
                {
                    logonResponse.Signature = result.RepriceErrorMessage;
                }
                else
                {
                    Login();
                }

                //Get the bifurcation of taxes after GST input.
                if (logonResponse != null && logonResponse.Signature.Length > 0)
                {
                    GetBookingFromStateResponse updatedGSTBreakup = GetBookingFromState();
                    if (updatedGSTBreakup != null && updatedGSTBreakup.BookingData != null && updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[updatedGSTBreakup.BookingData.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();


                        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();

                        for (int journey = 0; journey < updatedGSTBreakup.BookingData.Journeys.Length; journey++)
                        {
                            if (updatedGSTBreakup.BookingData.Journeys[journey] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length > 0)
                            {
                                for (int segment = 0; segment < updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length; segment++)
                                {
                                    if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length > 0)
                                    {
                                        for (int fare = 0; fare < updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                        {
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                            {
                                                adultPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                            }
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                            {
                                                childPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // For infant updating from intial result fare
                        Fare infantFare = result.FareBreakdown.FirstOrDefault(P => (P.PassengerType == PassengerType.Infant));
                        result.Price = new PriceAccounts();
                        result.BaseFare = 0;
                        result.Tax = 0;
                        result.TotalFare = 0;
                        result.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        result.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            result.FareBreakdown[0] = new Fare();
                            result.FareBreakdown[0].PassengerCount = request.AdultCount;
                            result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            result.FareBreakdown[1] = new Fare();
                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }

                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareAdult.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (result.FareBreakdown[0] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case GoAir.BookingManager.ChargeType.FarePrice:
                                                    double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                    result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                    result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.PublishedFare += (decimal)baseFareAdult;
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case GoAir.BookingManager.ChargeType.PromotionDiscount:
                                                    double discountAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].BaseFare = result.FareBreakdown[0].BaseFare - discountAdult;
                                                    result.FareBreakdown[0].TotalFare = result.FareBreakdown[0].TotalFare - discountAdult;
                                                    result.FareBreakdown[0].SupplierFare = result.FareBreakdown[0].SupplierFare - (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.PublishedFare = (decimal)(result.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                    result.Price.SupplierPrice = result.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                //For the below charge types  
                                                //====PromotionDiscount
                                                //======IncludedAddOnServiceFee
                                                //=====IncludedTax
                                                //====IncludedTravelFee
                                                //Do not compute the Tax Values
                                                case GoAir.BookingManager.ChargeType.AddOnServiceCancelFee:
                                                case GoAir.BookingManager.ChargeType.AddOnServiceFee:
                                                case GoAir.BookingManager.ChargeType.AddOnServiceMarkup:
                                                case GoAir.BookingManager.ChargeType.AddOnServicePrice:
                                                case GoAir.BookingManager.ChargeType.Calculated:
                                                case GoAir.BookingManager.ChargeType.ConnectionAdjustmentAmount:
                                                case GoAir.BookingManager.ChargeType.Discount:
                                                case GoAir.BookingManager.ChargeType.DiscountPoints:
                                                case GoAir.BookingManager.ChargeType.FarePoints:
                                                case GoAir.BookingManager.ChargeType.FareSurcharge:
                                                //case GoAir.BookingManager.ChargeType.IncludedAddOnServiceFee:
                                                //case GoAir.BookingManager.ChargeType.IncludedTravelFee:
                                                case GoAir.BookingManager.ChargeType.Loyalty:
                                                case GoAir.BookingManager.ChargeType.Note:
                                                case GoAir.BookingManager.ChargeType.ServiceCharge:
                                                case GoAir.BookingManager.ChargeType.Tax:
                                                case GoAir.BookingManager.ChargeType.TravelFee:
                                                case GoAir.BookingManager.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].TotalFare += tax;
                                                    result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.Tax += (decimal)tax;
                                                    if (charge.TicketCode.ToString().Contains("SST") || charge.TicketCode.ToString().Contains("CST") || charge.TicketCode.ToString().Contains("IST"))
                                                    {
                                                        result.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                       
                                                    }
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareChild.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (result.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case GoAir.BookingManager.ChargeType.FarePrice:
                                                    double baseFareChild = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].BaseFare += baseFareChild;
                                                    result.FareBreakdown[1].TotalFare += baseFareChild;
                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                    result.Price.PublishedFare += (decimal)baseFareChild;
                                                    result.Price.SupplierPrice += charge.Amount;                                                    
                                                    break;

                                                case GoAir.BookingManager.ChargeType.PromotionDiscount:
                                                    double discountChild = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].BaseFare = result.FareBreakdown[1].BaseFare - discountChild;
                                                    result.FareBreakdown[1].TotalFare = result.FareBreakdown[1].TotalFare - discountChild;
                                                    result.FareBreakdown[1].SupplierFare = result.FareBreakdown[1].SupplierFare - (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                    result.Price.PublishedFare = (decimal)(result.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                    result.Price.SupplierPrice = result.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                //For the below charge types  
                                                //====PromotionDiscount
                                                //======IncludedAddOnServiceFee
                                                //=====IncludedTax
                                                //====IncludedTravelFee
                                                //Do not compute the Tax Values
                                                case GoAir.BookingManager.ChargeType.AddOnServiceCancelFee:
                                                case GoAir.BookingManager.ChargeType.AddOnServiceFee:
                                                case GoAir.BookingManager.ChargeType.AddOnServiceMarkup:
                                                case GoAir.BookingManager.ChargeType.AddOnServicePrice:
                                                case GoAir.BookingManager.ChargeType.Calculated:
                                                case GoAir.BookingManager.ChargeType.ConnectionAdjustmentAmount:
                                                case GoAir.BookingManager.ChargeType.Discount:
                                                case GoAir.BookingManager.ChargeType.DiscountPoints:
                                                case GoAir.BookingManager.ChargeType.FarePoints:
                                                case GoAir.BookingManager.ChargeType.FareSurcharge:
                                                //case GoAir.BookingManager.ChargeType.IncludedAddOnServiceFee:
                                                //case GoAir.BookingManager.ChargeType.IncludedTravelFee:
                                                case GoAir.BookingManager.ChargeType.Loyalty:
                                                case GoAir.BookingManager.ChargeType.Note:
                                                case GoAir.BookingManager.ChargeType.ServiceCharge:
                                                case GoAir.BookingManager.ChargeType.Tax:
                                                case GoAir.BookingManager.ChargeType.TravelFee:
                                                case GoAir.BookingManager.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].TotalFare += tax;
                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                    result.Price.Tax += (decimal)tax;
                                                    if (charge.TicketCode.ToString().Contains("SST") || charge.TicketCode.ToString().Contains("CST") || charge.TicketCode.ToString().Contains("IST"))
                                                    {
                                                        result.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                       
                                                    }
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (request.InfantCount > 0)
                        {

                            if (infantFare != null)
                            {

                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    result.FareBreakdown[2] = infantFare;
                                }
                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                {

                                    result.FareBreakdown[1] = infantFare;

                                }

                                result.Price.PublishedFare += (decimal)infantFare.BaseFare;
                                result.Price.Tax += (decimal)infantFare.Tax;
                                result.Price.SupplierPrice += (decimal)infantFare.SupplierFare;

                            }

                        }

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }
                        result.Price.SupplierCurrency = currencyCode;
                        result.BaseFare = (double)result.Price.PublishedFare;
                        result.Tax = (double)result.Price.Tax;
                        result.TotalFare = (double)Math.Round((result.BaseFare + result.Tax), agentDecimalValue);
                    }
                }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "(GoAir)Failed to execute GetUpdatedGSTFareBreakUp. Error : " + ex.ToString(), "");
                throw ex;
            }

        }


        /// <summary>
        /// To Get the bifurcation of Taxes after GSTN update.
        /// </summary>
        /// <returns></returns>
        public GetBookingFromStateResponse GetBookingFromState()
        {
            GetBookingFromStateResponse getBookingFromStateResponse = new GetBookingFromStateResponse();
            try
            {
                GetBookingFromStateRequest getBookingFromStateRequest = new GetBookingFromStateRequest();
                getBookingFromStateRequest.ContractVersion = contractVersion;
                getBookingFromStateRequest.Signature = logonResponse.Signature;
                WriteLogFile(typeof(GetBookingFromStateRequest), getBookingFromStateRequest, "_GST_TaxBreakUpReq_");
                getBookingFromStateResponse = bookingAPI.GetBookingFromState(getBookingFromStateRequest);
                WriteLogFile(typeof(GetBookingFromStateResponse), getBookingFromStateResponse, "_GST_TaxBreakUpRes_");
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "(GoAir)Failed to execute GetBookingFromState method. Error : " + ex.ToString(), "");
                throw ex;
            }
            return getBookingFromStateResponse;
        }

        /// <summary>
        /// Sends the Lead Pax GST Details
        /// </summary>
        /// <returns></returns>
        public UpdateContactsResponse UpdateContactGSTDetails()
        {
            UpdateContactsResponse updateContactsResponse = new UpdateContactsResponse();
            try
            {
                UpdateContactsRequest updateContactsRequest = new UpdateContactsRequest();
                updateContactsRequest.ContractVersion = contractVersion;
                updateContactsRequest.Signature = string.IsNullOrEmpty(logonResponse.Signature) ? signatureForGST : logonResponse.Signature;
                UpdateContactsRequestData updateContactsRequestData = new UpdateContactsRequestData();
                updateContactsRequestData.BookingContactList = new BookingContact[1];
                updateContactsRequestData.BookingContactList[0] = new BookingContact();
                updateContactsRequestData.BookingContactList[0].State = GoAir.BookingManager.MessageState.New;
                updateContactsRequestData.BookingContactList[0].TypeCode = "G";
                updateContactsRequestData.BookingContactList[0].EmailAddress = gstOfficialEmail;
                updateContactsRequestData.BookingContactList[0].CustomerNumber = gstNumber;
                updateContactsRequestData.BookingContactList[0].CompanyName = gstCompanyName;
                updateContactsRequestData.BookingContactList[0].DistributionOption = DistributionOption.None;
                updateContactsRequestData.BookingContactList[0].NotificationPreference = NotificationPreference.None;
                updateContactsRequest.updateContactsRequestData = updateContactsRequestData;
                WriteLogFile(typeof(UpdateContactsRequest), updateContactsRequest, "_LeadPaxGSTReq_");
                updateContactsResponse = bookingAPI.UpdateContacts(updateContactsRequest);
                WriteLogFile(typeof(UpdateContactsResponse), updateContactsResponse, "_LeadPaxGSTRes_");
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "(GoAir)Failed to execute UpdateContactGSTDetails method . Error : " + ex.ToString(), "");
                throw ex;
            }
            return updateContactsResponse;
        }
        #endregion

        #region Booking Methods

        /// <summary>
        /// Books the GoAir Itinerary.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse BookItinerary(ref FlightItinerary itinerary)
        {
            //Below are the steps for booking flow.
            //1.SellBySSR
            //2.UpdatePassengers
            //3.AddPaymentToBooking
            //4.BookingCommit
            //5.GetBooking
            //6. Logout
            BookingResponse response = new BookingResponse();
            IBookingManager bookingAPI = this.bookingAPI;
            try
            {

                //Note:for normal search signature will be assigned from the basket based on the user Id.
                //Note:for one-one search signature will be assigned from the itinerary object[Endorsement Property] .
                if (!string.IsNullOrEmpty(itinerary.Endorsement) && itinerary.Endorsement.Length > 0)
                {
                    logonResponse.Signature = itinerary.Endorsement;
                }
                else
                {
                    Login();
                }
                if (!string.IsNullOrEmpty(logonResponse.Signature))
                {
                    if (itinerary.BookRequestType == BookingFlowStatus.UpdatePax || itinerary.BookRequestType == BookingFlowStatus.AssignSeat)
                    {
                        BookingUpdateResponseData clsBookingUpdateResponseData = new BookingUpdateResponseData();
                        int sBagMealcnt = itinerary.Passenger.Where(y => y.Type != PassengerType.Infant && (!string.IsNullOrEmpty(y.BaggageType)
                        || !string.IsNullOrEmpty(y.MealType))).Count();

                        int sInfntcnt = itinerary.Passenger.Where(y => y.Type == PassengerType.Infant).Count();

                        if (itinerary.BookRequestType == BookingFlowStatus.UpdatePax && (sBagMealcnt > 0 || sInfntcnt > 0))
                            clsBookingUpdateResponseData = BookSSR(itinerary);
                        else
                        {
                            response.Status = BookingResponseStatus.Successful;
                            clsBookingUpdateResponseData.Success = new Success();
                        }

                        if (clsBookingUpdateResponseData != null && clsBookingUpdateResponseData.Success != null)
                        {
                            clsBookingUpdateResponseData = UpdatePassengers(itinerary);
                            if (clsBookingUpdateResponseData != null && clsBookingUpdateResponseData.Success != null && clsBookingUpdateResponseData.Success.PNRAmount.TotalCost > 0)
                            {
                                itinerary.BookRequestType = BookingFlowStatus.AssignSeat;
                                if (itinerary.Passenger.Where(y => !string.IsNullOrEmpty(y.SeatInfo)).Count() > 0)
                                {
                                    response = AssignSeats(ref itinerary, logonResponse.Signature);
                                }
                                else
                                {
                                    itinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
                                    response.Status = BookingResponseStatus.Successful; response.Error = string.Empty;
                                }

                                itinerary.ItineraryAmountDue = itinerary.ItineraryAmountDue == 0 ? clsBookingUpdateResponseData.Success.PNRAmount.TotalCost :
                                    itinerary.ItineraryAmountDue;
                            }
                            else
                            {
                                response.Status = BookingResponseStatus.UpdatePaxFailed;
                                response.Error = "Update Passenger failed, please check the logs";
                            }
                        }
                        else
                        {
                            response.Status = BookingResponseStatus.Bookbaggagefailed;
                            response.Error = "SSR booking failed, please check the logs";
                        }
                    }
                    else if (itinerary.BookRequestType == BookingFlowStatus.PaxUpdated)
                    {
                        //Step-3:AddPaymentToBooking
                        bool bBookingStatus = false;
                        itinerary.BookRequestType = BookingFlowStatus.AddPayment;
                        AddPaymentToBookingResponseData paymentResponse = AddPaymentForBooking(itinerary.ItineraryAmountDue);
                        //If there are any Payment validations failed do not allow to commit booking

                        bBookingStatus = (paymentResponse != null && paymentResponse.ValidationPayment != null && paymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.ValidationPayment.PaymentValidationErrors.Length == 0);

                        //Step-3.1 Update Contacts Request.
                        if (bBookingStatus)
                        {
                            bBookingStatus = false;
                            UpdateContactsResponse updateContactsResponse = UpdateContacts(itinerary);
                            bBookingStatus = (updateContactsResponse != null && updateContactsResponse.BookingUpdateResponseData != null && updateContactsResponse.BookingUpdateResponseData.Error == null);
                            if (bBookingStatus)
                            {
                                bBookingStatus = false;
                                BookingUpdateResponseData bookingCommit = BookingCommit(itinerary);
                                if (bookingCommit != null && bookingCommit.Error == null)
                                {
                                    response.PNR = bookingCommit.Success.RecordLocator;
                                    response.ProdType = ProductType.Flight;
                                    response.Error = string.Empty;
                                    response.Status = BookingResponseStatus.Successful;
                                    itinerary.PNR = response.PNR;
                                    itinerary.FareType = "Pub";

                                    //Step-5 Get Booking 
                                    GetBooking(response.PNR);
                                    response.Status = BookingResponseStatus.Successful;
                                    itinerary.BookRequestType = BookingFlowStatus.BookingSuccess;
                                }
                                else
                                {
                                    response.Status = BookingResponseStatus.Failed;
                                    response.Error = "Booking Commit Failed !";
                                }
                            }
                            else
                            {
                                response.Status = BookingResponseStatus.Failed;
                                response.Error = updateContactsResponse.BookingUpdateResponseData.Error.ErrorText;
                            }
                        }
                        else
                        {
                            response.Status = BookingResponseStatus.Failed;
                            response.Error = paymentResponse.ValidationPayment.PaymentValidationErrors[0].ErrorDescription;
                        }
                    }
                    else
                    {
                        //Step-1:SellBySSR
                        int baggageMealCount = GetSSRCount(itinerary);
                        if (GetSSRCount(itinerary) > 0 || GetInfantCount(itinerary) > 0)
                        {
                            BookingUpdateResponseData bagResponse = BookSSR(itinerary);
                        }

                        //Step-2.UpdatePassengers.
                        BookingUpdateResponseData upResponse = UpdatePassengers(itinerary);
                        bool updatePaxStatus = false;
                        if (upResponse != null && upResponse.Success != null && upResponse.Success.PNRAmount.TotalCost > 0)
                        {
                            updatePaxStatus = true;
                        }
                        else
                        {
                            response.Status = BookingResponseStatus.Failed;
                            response.Error = "Update Passengers Failed";
                        }

                        //Step-3:AddPaymentToBooking
                        bool paymentStatus = false;
                        if (updatePaxStatus)
                        {
                            AddPaymentToBookingResponseData paymentResponse = AddPaymentForBooking(upResponse.Success.PNRAmount.TotalCost);
                            //If there are any Payment validations failed do not allow to commit booking
                            if (paymentResponse != null && paymentResponse.ValidationPayment != null && paymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.ValidationPayment.PaymentValidationErrors.Length == 0)
                            {
                                paymentStatus = true;
                            }
                            else
                            {
                                response.Status = BookingResponseStatus.Failed;
                                response.Error = paymentResponse.ValidationPayment.PaymentValidationErrors[0].ErrorDescription;
                            }
                        }

                        //Step-3.1 Update Contacts Request.
                        bool updateContacts = false;
                        if (paymentStatus)
                        {
                            UpdateContactsResponse updateContactsResponse = UpdateContacts(itinerary);
                            if (updateContactsResponse != null && updateContactsResponse.BookingUpdateResponseData != null && updateContactsResponse.BookingUpdateResponseData.Error == null)
                            {
                                updateContacts = true;
                            }
                            else
                            {
                                response.Status = BookingResponseStatus.Failed;
                                response.Error = updateContactsResponse.BookingUpdateResponseData.Error.ErrorText;
                            }
                        }

                        //Step-4.BookingCommit
                        bool bookingCommitStatus = false;
                        if (updateContacts)
                        {
                            BookingUpdateResponseData bookingCommit = BookingCommit(itinerary);
                            if (bookingCommit != null && bookingCommit.Error == null)
                            {
                                bookingCommitStatus = true;
                                response.PNR = bookingCommit.Success.RecordLocator;
                                response.ProdType = ProductType.Flight;
                                response.Error = string.Empty;
                                response.Status = BookingResponseStatus.Successful;
                                itinerary.PNR = response.PNR;
                                itinerary.FareType = "Pub";

                            }
                            else
                            {
                                response.Status = BookingResponseStatus.Failed;
                                response.Error = "Booking Commit Failed !";
                            }
                        }
                        //Step-5 Get Booking 
                        if (bookingCommitStatus)
                        {
                            GetBooking(response.PNR);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(Go Air)Failed to execute BookItinerary method. Reason : " + ex.ToString(), "");
                response.Error = ex.Message;
                response.Status = BookingResponseStatus.Failed;
                Logout();
            }
            finally
            {
                if (itinerary.BookRequestType != BookingFlowStatus.AssignSeat && itinerary.BookRequestType != BookingFlowStatus.PaxUpdated)
                    Logout();
            }
            return response;
        }

        /// <summary>
        /// Reserves the Additional Baggage ,Meals and Infants for the itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private BookingUpdateResponseData BookSSR(FlightItinerary itinerary)
        {
            IBookingManager bookingAPI = this.bookingAPI;
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {
                SellRequest request = new SellRequest();
                request.ContractVersion = contractVersion;
                request.EnableExceptionStackTrace = false;
                request.Signature = logonResponse.Signature;

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

                //Segment Wise SSR requests.
                List<SegmentSSRRequest> paxsegmentSSRRequest = new List<SegmentSSRRequest>();

                var objFlights = itinerary.Segments.Select(p => p.FlightNumber).Distinct().ToArray();

                int assignInfant;
                //1.Step-1:Iterate Through Segments Loop.
                for (int s = 0; s < objFlights.Length; s++)
                {
                    assignInfant = 0;
                    SegmentSSRRequest objSSRSegReq = new SegmentSSRRequest();

                    var sOrigindet = itinerary.Segments.Where(y => y.FlightNumber == objFlights[s]).Select(z => z).ToList().FirstOrDefault();
                    var sDestdet = itinerary.Segments.Where(y => y.FlightNumber == objFlights[s]).Select(z => z).ToList().Last();

                    FlightDesignator fd = new FlightDesignator();
                    fd.CarrierCode = carrierCode;
                    fd.FlightNumber = objFlights[s];
                    objSSRSegReq.FlightDesignator = fd;

                    objSSRSegReq.ArrivalStation = sDestdet.Destination.AirportCode;
                    objSSRSegReq.DepartureStation = sOrigindet.Origin.AirportCode;
                    objSSRSegReq.STD = sOrigindet.DepartureTime;

                    List<PaxSSR> paxSSRs = new List<PaxSSR>();
                    for (int p = 0; p < itinerary.Passenger.Length; p++)
                    {
                        //Infant SSR
                        if (itinerary.Passenger[p].Type == PassengerType.Infant)
                        {
                            PaxSSR inftSSR = new PaxSSR();
                            AssignPaxSSR(ref inftSSR, "INFT", sDestdet.Destination.AirportCode, sOrigindet.Origin.AirportCode, assignInfant);
                            paxSSRs.Add(inftSSR);
                            assignInfant = assignInfant + 1;
                        }
                        //Baggage SSR
                        if (itinerary.Passenger[p].Type != PassengerType.Infant && !string.IsNullOrEmpty(itinerary.Passenger[p].BaggageType))
                        {
                            string baggageCode = itinerary.Passenger[p].BaggageType.Split(',')[0];//For Normal One Way + Combination One Way + Combination Round Trip.
                            if (itinerary.Passenger[p].BaggageType.Split(',').Length > 1 && sOrigindet.Group == 1)//For Normal Round Trip
                            {
                                baggageCode = itinerary.Passenger[p].BaggageType.Split(',')[1];
                            }
                            if (!string.IsNullOrEmpty(baggageCode))
                            {
                                PaxSSR bagSSR = new PaxSSR();
                                AssignPaxSSR(ref bagSSR, baggageCode, sDestdet.Destination.AirportCode, sOrigindet.Origin.AirportCode, p);
                                paxSSRs.Add(bagSSR);
                            }
                        }
                        //Meal SSR
                        if (itinerary.Passenger[p].Type != PassengerType.Infant && !string.IsNullOrEmpty(itinerary.Passenger[p].MealType))
                        {
                            string mealCode = itinerary.Passenger[p].MealType.Split(',')[0];//For Normal One Way + Combination One Way + Combination Round Trip.
                            if (itinerary.Passenger[p].MealType.Split(',').Length > 1 && sOrigindet.Group == 1)//For Normal Round Trip
                            {
                                mealCode = itinerary.Passenger[p].MealType.Split(',')[1];
                            }
                            if (!string.IsNullOrEmpty(mealCode))
                            {
                                PaxSSR mealSSR = new PaxSSR();
                                AssignPaxSSR(ref mealSSR, mealCode, sDestdet.Destination.AirportCode, sOrigindet.Origin.AirportCode, p);
                                paxSSRs.Add(mealSSR);
                            }
                        }
                    }
                    objSSRSegReq.PaxSSRs = paxSSRs.ToArray();
                    paxsegmentSSRRequest.Add(objSSRSegReq);
                    paxSSRs.Clear();
                }

                requestData.SellSSR.SSRRequest.SegmentSSRRequests = paxsegmentSSRRequest.ToArray();
                request.SellRequestData = requestData;

                WriteLogFile(typeof(SellRequest), request, "_Sell_SSR_Req_");
                SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                responseData = sellResponse_v_4_5.BookingUpdateResponseData;
                WriteLogFile(typeof(SellResponse), sellResponse_v_4_5, "_Sell_SSR_Res_");
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute BookSSR Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        /// <summary>
        /// <summary>
        /// Updates passenger details for the booking in booking state. 
        /// The UpdatePassengers method processes the list of passengers input in the UpdatePassengersRequest 
        /// using the PassengerNumber field to match the passengers to passengers in booking state. 
        /// The matching passenger details are replaced with the details from the request. 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private BookingUpdateResponseData UpdatePassengers(FlightItinerary itinerary)
        {
            IBookingManager bookingAPI = this.bookingAPI;
            BookingUpdateResponseData upResponse = new BookingUpdateResponseData();
            try
            {
                if (!string.IsNullOrEmpty(logonResponse.Signature))
                {

                    int paxCount = 0, infantCount = 0, infants = 0;
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            paxCount++;
                        }
                        else
                        {
                            infantCount++;
                        }
                    }
                    UpdatePassengersRequest updatePassengerRequest = new UpdatePassengersRequest();
                    updatePassengerRequest.ContractVersion = contractVersion;
                    updatePassengerRequest.Signature = logonResponse.Signature;
                    updatePassengerRequest.EnableExceptionStackTrace = false;
                    updatePassengerRequest.updatePassengersRequestData = new UpdatePassengersRequestData();
                    updatePassengerRequest.updatePassengersRequestData.Passengers = new Passenger[paxCount];

                    for (int i = 0; i < paxCount; i++)
                    {
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i] = new Passenger();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerNumber = (Int16)i;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].State = GoAir.BookingManager.MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names = new BookingName[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0] = new BookingName();
                        //Go Air allows the below pax titles only
                        //1.DR --Doctor
                        //2.MISS -- Miss.
                        //3.MR -- Mr.
                        //4.MRS --Mrs.
                        //4.MS --Ms.
                        //4.MSTR --Master.
                        if (itinerary.Passenger[i].Title.ToUpper() == "DR" || itinerary.Passenger[i].Title.ToUpper() == "MISS" || itinerary.Passenger[i].Title.ToUpper() == "MR" || itinerary.Passenger[i].Title.ToUpper() == "MRS" || itinerary.Passenger[i].Title.ToUpper() == "MS" || itinerary.Passenger[i].Title.ToUpper() == "MSTR")
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].Title = itinerary.Passenger[i].Title.ToUpper();
                        }
                        else//If none of the titles matches
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].Title = "MR";
                        }
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].FirstName = itinerary.Passenger[i].FirstName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].LastName = itinerary.Passenger[i].LastName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].State = GoAir.BookingManager.MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo = new PassengerInfo();
                        if (itinerary.Passenger[i].Gender == Gender.Male)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = GoAir.BookingManager.Gender.Male;
                        }
                        else
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = GoAir.BookingManager.Gender.Female;
                        }
                        if (itinerary.Passenger[i].Nationality != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                            }
                        }
                        if (itinerary.Passenger[i].Country != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                            }
                        }
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos = new PassengerTypeInfo[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0] = new PassengerTypeInfo();
                        if (itinerary.Passenger[i].Type == PassengerType.Adult)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "ADT";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-30);
                            }
                            if (infantCount - infants > 0)
                            {
                                FlightPassenger infantPax = itinerary.Passenger[paxCount + infants];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant = new PassengerInfant();
                                if (infantPax.DateOfBirth != DateTime.MinValue)
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = infantPax.DateOfBirth;
                                }
                                else
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = DateTime.Today.AddDays(-365);
                                }
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Gender = (infantPax.Gender == Gender.Male ? GoAir.BookingManager.Gender.Male : GoAir.BookingManager.Gender.Female);
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names = new BookingName[1];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0] = new BookingName();
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].FirstName = infantPax.FirstName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].LastName = infantPax.LastName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].Title = string.Empty;//As per latest 4.2 infant will have no title
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].State = GoAir.BookingManager.MessageState.New;
                                if (itinerary.Passenger[i].Nationality != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                                    }
                                }
                                if (itinerary.Passenger[i].Country != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                                    }
                                }

                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.State = GoAir.BookingManager.MessageState.New;
                                infants++;
                            }
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "CHD";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-10);
                            }
                        }
                    }
                    WriteLogFile(typeof(UpdatePassengersRequest), updatePassengerRequest, "_UpdatePaxReq_");
                    UpdatePassengersResponse upResponse_v_4_5 = bookingAPI.UpdatePassengers(updatePassengerRequest);
                    upResponse = upResponse_v_4_5.BookingUpdateResponseData;
                    WriteLogFile(typeof(UpdatePassengersResponse), upResponse_v_4_5, "_UpdatePaxRes_");
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute UpdatePassengers Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return upResponse;
        }

        /// <summary>
        /// Assign seats for the selected pax. 
        /// Assign seats will send seat assign request for each passenger and each segment based on user seat selection 
        /// using the PassengerNumber field to match the passengers to passengers in booking state. 
        /// The matching passenger seat details are replaced with the seat details from the request. 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingResponse AssignSeats(ref FlightItinerary clsFlightItinerary, string signature)
        {
            AssignSeatsResponse clsAssignSeatsResponse = new AssignSeatsResponse();
            DateTime dtSTD; string sOrigin = string.Empty, sDestination = string.Empty; bool bSegStatus = true;
            BookingResponse clsBookingResponse = new BookingResponse();

            var objFlights = clsFlightItinerary.Segments.Select(p => p.FlightNumber).Distinct();

            AssignSeatsRequest clsAssignSeatsRequest = new AssignSeatsRequest();

            clsAssignSeatsRequest.ContractVersion = contractVersion;
            clsAssignSeatsRequest.Signature = signature;
            clsAssignSeatsRequest.EnableExceptionStackTrace = false;
            clsAssignSeatsRequest.SellSeatRequest = new SeatSellRequest();

            clsAssignSeatsRequest.SellSeatRequest.SeatAssignmentMode = SeatAssignmentMode.PreSeatAssignment;
            clsAssignSeatsRequest.SellSeatRequest.IncludeSeatData = true;
            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests = new SegmentSeatRequest[clsFlightItinerary.Passenger.Where(m => m.Type != PassengerType.Infant).Count() * objFlights.Count()];
            int segcnt = 0;

            foreach (string flt in objFlights)
            {
                try
                {
                    sOrigin = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(z => z.Origin.AirportCode).FirstOrDefault();
                    sDestination = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(z => z.Destination.AirportCode).Last();
                    dtSTD = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(f => f.DepartureTime).FirstOrDefault();

                    clsFlightItinerary.Passenger.ToList().ForEach(p =>
                    {
                        var obj = p.liPaxSeatInfo.Where(y => y.SeatStatus == "A" && !string.IsNullOrEmpty(y.SeatNo) && y.Segment == sOrigin + "-" + sDestination).FirstOrDefault();
                        if (obj != null)
                        {
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt] = new SegmentSeatRequest();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].ArrivalStation = sDestination;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].DepartureStation = sOrigin;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].STD = dtSTD;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers = new short[1];
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers[0] = new short();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers[0] = (short)obj.PaxNo;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].UnitDesignator = obj.SeatNo;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator = new FlightDesignator();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator.CarrierCode = "G8";
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator.FlightNumber = flt.Length < 4 ? " " + flt : flt;
                            segcnt++;
                        }
                    });
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "(GoAir)Failed to prepare seat assign request. Error: " + ex.GetBaseException(), "");
                    clsFlightItinerary.Passenger.ToList().ForEach(y => y.liPaxSeatInfo.
                                ForEach(s => { y.SeatInfo = string.Empty; y.Price.SeatPrice = 0; s.SeatNo = "NoSeat"; s.Price = 0; s.SeatStatus = "F"; }));
                    clsBookingResponse.Error = "Failed to prepare seat assign request."; clsBookingResponse.Status = BookingResponseStatus.Assignseatfailed;
                    throw ex;
                }
            }

            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests = clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests.Where(j => j != null).ToArray();
            clsFlightItinerary.ItineraryAmountDue = 0;

            if (clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests.Length > 0)
            {
                GenericStatic.WriteLogFile(typeof(AssignSeatsRequest), clsAssignSeatsRequest,
                    xmlLogPath + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + "_G8_AssignSeatRequest_");

                try
                {
                    clsAssignSeatsResponse = bookingAPI.AssignSeats(clsAssignSeatsRequest);
                }
                catch (Exception ex)
                {
                    bSegStatus = false;
                    Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to assign seat from supplier. Error: " + ex.GetBaseException(), "");
                }

                GenericStatic.WriteLogFile(typeof(AssignSeatsResponse), clsAssignSeatsResponse,
                    xmlLogPath + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + "_G8_AssignSeatResponse_");

                if (bSegStatus && clsAssignSeatsResponse.BookingUpdateResponseData != null && clsAssignSeatsResponse.BookingUpdateResponseData.Success != null)
                {
                    clsFlightItinerary.ItineraryAmountDue = clsAssignSeatsResponse.BookingUpdateResponseData.Success.PNRAmount.BalanceDue;
                    clsBookingResponse.Status = BookingResponseStatus.Successful;
                    clsFlightItinerary.Passenger.ToList().ForEach(y => y.liPaxSeatInfo.ForEach(s => s.SeatStatus = "S"));
                    clsFlightItinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
                }
                else
                {
                    string err = string.Empty;
                    if (bSegStatus && clsAssignSeatsResponse.AssignedSeatInfo.JourneyList != null && clsAssignSeatsResponse.AssignedSeatInfo.JourneyList.Length > 0)
                    {
                        var Segments = new List<AssignSeatSegment>();
                        clsAssignSeatsResponse.AssignedSeatInfo.JourneyList.ToList().ForEach(x => { if (x.Segments != null) { Segments.AddRange(x.Segments); } });

                        var paxseats = new List<PaxSeat>();
                        Segments.ForEach(x => { if (x.PaxSeats != null) { paxseats.AddRange(x.PaxSeats); } });

                        clsFlightItinerary.Passenger.Where(b => !string.IsNullOrEmpty(b.SeatInfo)).ToList().
                            ForEach(y => y.liPaxSeatInfo.Where(a => a.SeatStatus == "A" && !string.IsNullOrEmpty(a.SeatNo)).ToList().ForEach(s =>
                            {
                                if (paxseats.Where(p => p.PassengerNumber == s.PaxNo && p.DepartureStation + "-" + p.ArrivalStation == s.Segment).Count() == 0)
                                {
                                    string msg = "Failed to assign seat for passenger " + y.FirstName + " " + y.LastName + " for segment (" + s.Segment + ")";
                                    err = string.IsNullOrEmpty(err) ? msg : err + "@" + msg;
                                    s.SeatNo = "NoSeat"; y.Price.SeatPrice -= s.Price; s.Price = 0; s.SeatStatus = "F";
                                }
                                else
                                    s.SeatStatus = "S";

                            }));
                    }
                    else
                    {
                        clsFlightItinerary.Passenger.Where(b => !string.IsNullOrEmpty(b.SeatInfo)).ToList().
                            ForEach(y => y.liPaxSeatInfo.Where(a => a.SeatStatus == "A" && !string.IsNullOrEmpty(a.SeatNo)).ToList().ForEach(s =>
                            {
                                string msg = "Failed to assign seat for passenger " + y.FirstName + " " + y.LastName + " for segment (" + s.Segment + ")";
                                err = string.IsNullOrEmpty(err) ? msg : err + "@" + msg;
                                s.SeatNo = "NoSeat"; y.Price.SeatPrice -= s.Price; s.Price = 0; s.SeatStatus = "F";
                            }));
                    }
                    clsBookingResponse.Status = BookingResponseStatus.Assignseatfailed;
                    clsBookingResponse.Error = err;
                }

            }
            else
            {
                clsFlightItinerary.ItineraryAmountDue = 0;
                clsBookingResponse.Status = BookingResponseStatus.Successful;
                clsFlightItinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
            }
            return clsBookingResponse;
        }
        /// <summary>
        /// Confirmation of Total Price
        /// </summary>
        /// <param name="bookingAmount"></param>
        /// <returns></returns>
        private AddPaymentToBookingResponseData AddPaymentForBooking(decimal bookingAmount)
        {
            IBookingManager bookingAPI = this.bookingAPI;
            AddPaymentToBookingResponseData response = new AddPaymentToBookingResponseData();
            try
            {
                if (!string.IsNullOrEmpty(logonResponse.Signature))
                {
                    AddPaymentToBookingRequest payRequest = new AddPaymentToBookingRequest();
                    payRequest.ContractVersion = contractVersion;
                    payRequest.Signature = logonResponse.Signature;
                    payRequest.EnableExceptionStackTrace = false;
                    AddPaymentToBookingRequestData request = new AddPaymentToBookingRequestData();

                    //1.<AccountNumber>otaapi</AccountNumber> in AddPaymentToBooking should always be in capital letters

                    request.AccountNumber = agentId.ToUpper();
                    request.AccountNumberID = 0;
                    request.AuthorizationCode = "";
                    request.Deposit = false;
                    request.Expiration = new DateTime();
                    request.Installments = 0;
                    request.MessageState = GoAir.BookingManager.MessageState.New;
                    request.ParentPaymentID = 0;
                    request.PaymentAddresses = new PaymentAddress[0];
                    request.PaymentFields = new PaymentField[0];
                    request.PaymentMethodCode = "AG";
                    request.PaymentMethodType = RequestPaymentMethodType.AgencyAccount;
                    request.PaymentText = "";
                    request.QuotedAmount = bookingAmount;
                    request.QuotedCurrencyCode = currencyCode;
                    request.ReferenceType = PaymentReferenceType.Default;
                    request.Status = BookingPaymentStatus.New;
                    request.ThreeDSecureRequest = new ThreeDSecureRequest();
                    request.WaiveFee = false;
                    payRequest.addPaymentToBookingReqData = request;

                    WriteLogFile(typeof(AddPaymentToBookingRequest), payRequest, "_AddPaymentToBookingReq_");
                    AddPaymentToBookingResponse response_v_4_5 = bookingAPI.AddPaymentToBooking(payRequest);
                    response = response_v_4_5.BookingPaymentResponse;
                    WriteLogFile(typeof(AddPaymentToBookingResponse), response_v_4_5, "_AddPaymentToBookingRes_");

                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute AddPaymentForBooking Method. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return response;
        }

        /// <summary>
        /// Flight Itinerary email is sent to the lead passenger
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public UpdateContactsResponse UpdateContacts(FlightItinerary itinerary)
        {
            IBookingManager bookingAPI = this.bookingAPI;
            UpdateContactsResponse updateContactsResponse = null;
            try
            {

                UpdateContactsRequest updateContactsRequest = new UpdateContactsRequest();
                updateContactsRequest.ContractVersion = contractVersion;
                updateContactsRequest.Signature = logonResponse.Signature;
                updateContactsRequest.EnableExceptionStackTrace = false;

                UpdateContactsRequestData updateContactsRequestData = new UpdateContactsRequestData();
                updateContactsRequestData.BookingContactList = new BookingContact[1];
                BookingContact bookingContact = new BookingContact();
                bookingContact.State = GoAir.BookingManager.MessageState.New;
                bookingContact.TypeCode = "P";
                bookingContact.Names = new BookingName[1];
                bookingContact.Names[0] = new BookingName();
                bookingContact.Names[0].FirstName = itinerary.Passenger[0].FirstName;
                bookingContact.Names[0].LastName = itinerary.Passenger[0].LastName;
                bookingContact.Names[0].Title = itinerary.Passenger[0].Title.ToUpper();
                if (!string.IsNullOrEmpty(itinerary.Passenger[0].Email))
                {
                    bookingContact.EmailAddress = itinerary.Passenger[0].Email;
                }
                bookingContact.NotificationPreference = NotificationPreference.None;
                bookingContact.DistributionOption = DistributionOption.None;
                bookingContact.HomePhone = itinerary.Passenger[0].CellPhone;

                updateContactsRequestData.BookingContactList[0] = bookingContact;
                updateContactsRequest.updateContactsRequestData = updateContactsRequestData;
                WriteLogFile(typeof(UpdateContactsRequest), updateContactsRequest, "_UpdateContactsReq_");
                updateContactsResponse = bookingAPI.UpdateContacts(updateContactsRequest);
                WriteLogFile(typeof(UpdateContactsResponse), updateContactsResponse, "_UpdateContactsRes_");
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute UpdateContacts Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return updateContactsResponse;

        }

        /// <summary>
        /// Books the itinerary and gives the RecordLocator(PNR)
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private BookingUpdateResponseData BookingCommit(FlightItinerary itinerary)
        {

            IBookingManager bookingAPI = this.bookingAPI;
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {

                BookingCommitRequest bookRequest = new BookingCommitRequest();
                bookRequest.ContractVersion = contractVersion;
                bookRequest.Signature = logonResponse.Signature;
                bookRequest.EnableExceptionStackTrace = false;

                BookingCommitRequestData request = new BookingCommitRequestData();
                request.BookingChangeCode = "";
                request.BookingComments = new BookingComment[0];
                request.SourcePOS = new PointOfSale();
                request.SourcePOS.AgentCode = "AG";
                request.SourcePOS.OrganizationCode = agentId;
                request.SourcePOS.DomainCode = agentDomain;

                request.BookingID = 0;
                request.BookingParentID = 0;
                request.ChangeHoldDateTime = false;
                request.CurrencyCode = currencyCode;
                request.DistributeToContacts = true;//Send booking confirmation email to the pax
                request.GroupName = "";
                request.NumericRecordLocator = "";
                request.ParentRecordLocator = "";
                request.RecordLocator = "";
                request.RecordLocators = new RecordLocator[0];
                request.RestrictionOverride = false;

                request.State = GoAir.BookingManager.MessageState.New;
                request.SystemCode = "";
                request.WaiveNameChangeFee = false;
                request.WaivePenaltyFee = false;
                request.WaiveSpoilageFee = false;
                request.Passengers = new Passenger[0];
                request.PaxCount = (Int16)(itinerary.Passenger.Length - GetInfantCount(itinerary));


                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    FlightPassenger pax = itinerary.Passenger[i];
                    if (pax.Type != PassengerType.Infant)
                    {
                        if (pax.IsLeadPax)
                        {
                            request.BookingContacts = new BookingContact[1];
                            request.BookingContacts[0] = new BookingContact();
                            request.BookingContacts[0].AddressLine1 = (string.IsNullOrEmpty(pax.AddressLine1) ? "Address Line1" : (pax.AddressLine1.Length <= 52 ? pax.AddressLine1 : pax.AddressLine1.Substring(0, 52)));//Address Line1 Must Be No more than 52 Characters in length.;
                            request.BookingContacts[0].AddressLine2 = pax.AddressLine2;
                            request.BookingContacts[0].AddressLine3 = "";
                            request.BookingContacts[0].City = "SHJ";
                            request.BookingContacts[0].CompanyName = "Cozmo Travel";
                            if (pax.Country != null && !string.IsNullOrEmpty(pax.Country.CountryCode))
                            {

                                request.BookingContacts[0].CountryCode = pax.Country.CountryCode;

                            }
                            else //Since we are hard coding city as "SHJ" so we are passing country code as "AE";
                            {
                                request.BookingContacts[0].CountryCode = "AE";
                            }
                            request.BookingContacts[0].CultureCode = "en-GB";
                            request.BookingContacts[0].SourceOrganization = agentId;
                            request.BookingContacts[0].CustomerNumber = "";
                            request.BookingContacts[0].DistributionOption = DistributionOption.Email;
                            //Modified by Lokesh on 27-Nov-2017. Pass Lead Pax email for contact info.
                            if (!string.IsNullOrEmpty(pax.Email))
                            {
                                request.BookingContacts[0].EmailAddress = pax.Email; //The Original Ticket details will be sent to this email ,not to the passenger email address.
                            }
                            else
                            {
                                request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com"; //The Original Ticket details will be sent to this email ,not to the passenger email address.
                            }
                            request.BookingContacts[0].Fax = "";
                            request.BookingContacts[0].HomePhone = pax.CellPhone;
                            request.BookingContacts[0].Names = new BookingName[1];
                            request.BookingContacts[0].Names[0] = new BookingName();
                            request.BookingContacts[0].Names[0].FirstName = pax.FirstName;
                            request.BookingContacts[0].Names[0].LastName = pax.LastName;
                            request.BookingContacts[0].Names[0].MiddleName = "";
                            request.BookingContacts[0].Names[0].State = GoAir.BookingManager.MessageState.New;
                            request.BookingContacts[0].Names[0].Suffix = "";
                            request.BookingContacts[0].Names[0].Title = pax.Title.ToUpper();//Send always UPPER CASE
                            request.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                            //Added by Lokesh on 4-Oct-2017 Regarding Lead Pax Destination Phone Number.
                            if (!string.IsNullOrEmpty(pax.DestinationPhone))
                            {
                                request.BookingContacts[0].OtherPhone = pax.DestinationPhone;
                            }
                            else
                            {
                                request.BookingContacts[0].OtherPhone = "";
                            }
                            request.BookingContacts[0].PostalCode = "3093";
                            request.BookingContacts[0].ProvinceState = "SHJ";
                            request.BookingContacts[0].State = GoAir.BookingManager.MessageState.New;
                            request.BookingContacts[0].TypeCode = "P";
                            request.BookingContacts[0].WorkPhone = pax.CellPhone;
                        }
                    }
                }
                bookRequest.BookingCommitRequestData = request;
                WriteLogFile(typeof(BookingCommitRequest), bookRequest, "_BookingCommitReq_");
                BookingCommitResponse bookingCommitResponse_v_4_5 = bookingAPI.BookingCommit(bookRequest);
                responseData = bookingCommitResponse_v_4_5.BookingUpdateResponseData;
                WriteLogFile(typeof(BookingCommitResponse), bookingCommitResponse_v_4_5, "_BookingCommitRes_");
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(GoAir)Failed to Execute BookingCommit Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        #endregion
    }

}


