using System;
using System.Collections.Generic;
using System.Text;
using SpiceJet.SpiceWebRef;
using SpiceJet.SpiceJetProdWebRef;
using CT.Configuration;

namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// Spice Jet wrapper is made for picking different spice Jet web reference objects for different mode - test Mode, Production Mode
    /// Here creating an object of System.Object and then assigning this parent object to web reference object,
    /// Derived class (web Reference Class) methods are access by explicitly overriding this Object to its web reference object
    /// 
    /// This class is useful where any web reference (either its Test or Production) method is called more than one time then no need to check again and again whether its test or Prod. It will pick itself in one go.
    /// </summary>
    public class SpiceJetWrapper
    {        
        bool testMode;        
        Object obj;

        public SpiceJetWrapper(bool isTest)
        {
            testMode = Convert.ToBoolean(ConfigurationSystem.SpiceJetConfig["TestMode"]);
            if (ConfigurationSystem.SpiceJetConfig["Mode"] == "SpiceJet")
            {
                if (testMode)
                {
                    SJ_API_TEST test = new SJ_API_TEST();
                    test.Url = ConfigurationSystem.SpiceJetConfig["WStestURL"];
                    obj = test;

                    //obj = new SJ_API_TEST();
                }
                else
                {
                    SJ_API_WS1 prod = new SJ_API_WS1();
                    prod.Url = ConfigurationSystem.SpiceJetConfig["WSprodURL"];
                    obj = prod;

                    //obj = new SJ_API_WS1();                
                }
            }
        }

        /// <summary>
        /// Get Availability search for Spice Jet
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="response"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public object GetAvailability(string msg, ref string response, ref string error)
        {     
            if (testMode)
            {
                return ((SJ_API_TEST)obj).GetAvailability(msg, ref response, ref error);
            }
            else
            {
                return ((SJ_API_WS1)obj).GetAvailability(msg, ref response, ref error);             
            }
        }

        /// <summary>
        /// Fare Quote for Spice Jet
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="response"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public object FareQuote(string msg, ref string response, ref string error)
        {            
            if (testMode)
            {
                return ((SJ_API_TEST)obj).FareQuote(msg, ref response, ref error);
            }
            else
            {
                return ((SJ_API_WS1)obj).FareQuote(msg, ref response, ref error);
            }
        }

        /// <summary>
        /// Book Reservation for Spice Jet
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="response"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public object BookReservation(string msg, ref string response, ref string error)
        {           
            if (testMode)
            {
                return ((SJ_API_TEST)obj).BookReservation(msg, ref response, ref error);
            }
            else
            {
                return ((SJ_API_WS1)obj).BookReservation(msg, ref response, ref error);
            }
        }

        /// <summary>
        /// Display reservation For A PNR 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="response"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public object DisplayReservation(string msg, ref string response, ref string error)
        {
            if (testMode)
            {
                return ((SJ_API_TEST)obj).DisplayReservation(msg, ref response, ref error);
            }
            else
            {
                return ((SJ_API_WS1)obj).DisplayReservation(msg, ref response, ref error);
            }
        }
        
    }
}
