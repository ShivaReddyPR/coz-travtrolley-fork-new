﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using SpiceJet.SGSession;
using SpiceJet.SGBooking;
using CT.Configuration;
using CT.Core;
using System.Data;
using System.Threading;
using System.Net;

namespace CT.BookingEngine.GDS
{
    [Serializable]
    public class SpiceJetAPIV1:IDisposable
    {
        string agentId;
        string agentDomain;
        string password;
        string organisationCode;
        int contractVersion;

        Dictionary<string, decimal> exchangeRates;
        decimal rateOfExchange;
        string agentBaseCurrency;
        int agentDecimalValue;
        string xmlLogPath;
        Hashtable pricedItineraries = new Hashtable();
        DataTable dtBaggage = new DataTable();
        /********************Search Variables******************/
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        //Search Request
        SearchRequest request = new SearchRequest();
        //Signature of the Login response
        string loginSignature = "";
        //Search results will be combined for return search and one way search
        List<SearchResult> CombinedSearchResults = new List<SearchResult>();
        //Web Service for all operations except the Login
        BookingManagerClient bookingAPI = new BookingManagerClient();
        //Web Service for Login and Logout
        SessionManagerClient sessionAPI = new SessionManagerClient();

        string currencyCode;//For India GST implementation the currency code should be in INR format.

        //Added for GST Flow 
        string gstNumber;
        string gstCompanyName;
        string gstOfficialEmail;
        //Added for Corporate Flow
        string promoCode;

        string bookingSourceFlag;
        string sessionId;

        /****************************End***********************/
        public SpiceJetAPIV1()
        {
            xmlLogPath = ConfigurationSystem.SpiceJetConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
            if (!Directory.Exists(xmlLogPath))
            {
                Directory.CreateDirectory(xmlLogPath);
            }
            
            contractVersion = Convert.ToInt32(ConfigurationSystem.SpiceJetConfig["ContractVersion"]);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;//Added by Lokesh on 27/02/2018.
            if (dtBaggage.Columns.Count == 0)
            {
                dtBaggage.Columns.Add("Code", typeof(string));
                dtBaggage.Columns.Add("Price", typeof(decimal));
                dtBaggage.Columns.Add("Group", typeof(int));
                dtBaggage.Columns.Add("Currency", typeof(string));
                dtBaggage.Columns.Add("Description", typeof(string));
                dtBaggage.Columns.Add("QtyAvailable", typeof(int));
            }
            //Assign the web service url from source xml config
            bookingAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.SpiceJetConfig["BookingService"]);
            sessionAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.SpiceJetConfig["SessionService"]);
        }

        public string LoginName
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string AgentDomain
        {
            get { return agentDomain; }
            set { agentDomain = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public int AgentDecimalValue
        {
            get { return agentDecimalValue; }
            set { agentDecimalValue = value; }
        }

        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        public string GSTNumber
        {
            get { return gstNumber; }
            set { gstNumber = value; }
        }
        public string GSTCompanyName
        {
            get { return gstCompanyName; }
            set { gstCompanyName = value; }
        }
        public string GSTOfficialEmail
        {
            get { return gstOfficialEmail; }
            set { gstOfficialEmail = value; }
        }
        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }

        public string BookingSourceFlag
        {
            get { return bookingSourceFlag; }
            set { bookingSourceFlag = value; }
        }
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        /// <summary>
        /// Authenticates and creates a session for the specified user
        /// </summary>
        /// <returns></returns>
        private LogonResponse Login()
        {
            LogonResponse response = new LogonResponse();

            try
            {
                LogonRequest request = new LogonRequest();
                request.logonRequestData = new LogonRequestData();
                request.logonRequestData.AgentName = agentId;
                request.logonRequestData.DomainCode = agentDomain;
                request.logonRequestData.LocationCode = "";
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.Password = password;
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.TerminalInfo = "";
                request.ContractVersion = contractVersion;

                XmlSerializer ser = new XmlSerializer(typeof(LogonRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGLogonRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, request);
                sw.Close();


                response.Signature = sessionAPI.Logon(contractVersion, request.logonRequestData);

                ser = new XmlSerializer(typeof(LogonResponse));
                filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGLogonResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                sw = new StreamWriter(filePath);
                ser.Serialize(sw, response);
                sw.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Login failed. Reason : " + ex.Message, ex);
            }

            return response;
        }




        /// <summary>
        /// Ends a user’s session and clears session from the server.
        /// </summary>
        /// <param name="signature"></param>
        /// <returns></returns>
        private LogoutResponse Logout(string signature)
        {
            LogoutResponse response = new LogoutResponse();

            try
            {
                LogoutRequest request = new LogoutRequest();

                request.Signature = signature;

                XmlSerializer ser = new XmlSerializer(typeof(LogoutRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGLogoutRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, request);
                sw.Close();


                sessionAPI.Logout(contractVersion, request.Signature);
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Login failed. Reason : " + ex.Message, ex);
            }

            return response;
        }


        public SearchResult[] Search(SearchRequest request)
        {
            SearchResult[] results = new SearchResult[0];
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                LogonResponse loginResponse = Login();

                if (loginResponse != null && loginResponse.Signature.Length > 0)
                {
                    TripAvailabilityResponse tripAvailResponse = null;
                    this.request = request;
                    loginSignature = loginResponse.Signature;

                    /************************************************************************************************
                     *  If Return Search then call individual fare search for Regular, Hand Baggage, Special Return *
                     *  and Family Fares. Family Fares will only be search if the Total Pax Count greater than or 
                     *  equal to 4 excluding Infant pax i.e AdultCount + ChildCount >= 4. Earlier we use to make 
                     *  combinations of all fares. But as per SpiceJet we should not merge Special Return and Family
                     *  Fares with other fares since they are special fares. So now we are making same fare results
                     *  i.e Onward -> Special Return 
                     *      Return -> Special Return 
                     *  All the searched results will be stored in CombinedSearchResults list and returned.
                    ** **********************************************************************************************/


                    if (request.Type == SearchType.Return)
                    {
                        //ReadySources will hold same source orderings i.e SG1, SG2 ....
                        Dictionary<string, int> readySources = new Dictionary<string, int>();

                        //This dictionary will hold what fare search we want to call
                        Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();

                        //listOfThreads.Add("SG1", new WaitCallback(SearchForRegularFares));
                        //listOfThreads.Add("SG2", new WaitCallback(SearchForFlexFares));
                        //listOfThreads.Add("SG1", new WaitCallback(SearchForSpecialReturnFares));
                        listOfThreads.Add("SG4", new WaitCallback(SearchForHandBaggageFares));//Hand baggage one-one
                        listOfThreads.Add("SG1", new WaitCallback(SearchForAllFares));//Regular, Flex & Special Return fares are combined now
                        listOfThreads.Add("SG2", new WaitCallback(SearchForPromoFares));//Advance Purchase Fares (Non refundable)

                        //Family Fares will only be returned if pax count >= 4 excluding infants
                        if ((request.AdultCount + request.ChildCount) >= 4)
                        {
                            eventFlag = new AutoResetEvent[4];//Total 5 fares need to be searched
                            listOfThreads.Add("SG3", new WaitCallback(SearchForFamilyFares));//Add FamilyFares for search                            

                            //Add the ordering with timeout of 300 seconds for each fare search
                            readySources.Add("SG1", 300);
                            readySources.Add("SG2", 300);
                            readySources.Add("SG3", 300);
                            readySources.Add("SG4", 300);
                        }
                        else
                        {
                            eventFlag = new AutoResetEvent[3];//Total 4 fares need to be searched bcoz FamilyFares is not qualified
                            readySources.Add("SG1", 300);
                            readySources.Add("SG2", 300);
                            readySources.Add("SG4", 300);
                        }

                        int j = 0;
                        //Start each fare search within a Thread which will automatically terminate after the results are received.
                        foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                        {
                            if (readySources.ContainsKey(deThread.Key))
                            {
                                ThreadPool.QueueUserWorkItem(deThread.Value, j);
                                eventFlag[j] = new AutoResetEvent(false);
                                j++;
                            }
                        }

                        if (j != 0)
                        {
                            if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 1200), true))
                            {
                                //TODO: audit which thread is timed out                
                            }
                        }
                    }




                    else if (request.Type == SearchType.OneWay) //If one way search then call all fares
                    {
                        try
                        {
                            //Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();
                            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                            List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                            GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                            GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                            availRequest.ContractVersion = contractVersion;
                            availRequest.Signature = loginResponse.Signature;
                            availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                            if (request.Type == SearchType.OneWay)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                            }

                            for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                                if(!string.IsNullOrEmpty(promoCode))
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                                }
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";
                                //In order to get connecting flights this value should be greater than 5
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                                //Currently SpiceJet is allowing only Lowest Fares out of all combinations available in the Connecting segments
                                //that's why we are passing LowestFareClass which will always return Single Fare i.e. Regular Fare (Saver)
                                //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.LowestFareClass;

                                if ((request.AdultCount + request.ChildCount) >= 4)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[6];
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R"; //Regular Fare
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F"; //Family Fares
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";//Hand Baggage
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[3] = "IO";//Advance Purchase (Non Refundable but changeable at Rs.2000/2350 domestic/ineternational)
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[4] = "MX";//SpiceFlex
                                    if (!string.IsNullOrEmpty(promoCode))
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[5] = "C";//CorporateFare
                                    }
                                }
                                else
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[7];
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";//Regular
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";//Hand Baggage
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "IO";//Advance Purchase
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[3] = "T";//Added by brahma 22.07.2016 For Coupon Fares
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[4] = "S";//Added by shiva 26.Sep.2016 For Fare difference
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[5] = "MX";//SpiceFlex
                                    if (!string.IsNullOrEmpty(promoCode))
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[6] = "C";//Corporate Fare
                                    }
                                }
                                int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                                for (int a = 0; a < request.AdultCount; a++)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                                }
                                if (request.ChildCount > 0)
                                {
                                    for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                                    }
                                }
                                if (request.InfantCount > 0)
                                {
                                    for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                                    }
                                }
                            }
                            availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                            try
                            {

                                XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                StreamWriter sw = new StreamWriter(filePath);
                                ser.Serialize(sw, availRequest);
                                sw.Close();
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                            }

                            tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginResponse.Signature, availRequest.TripAvailabilityRequest);


                            try
                            {
                                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                //XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                                //doc.Save(filePath);
                                Type[] types = new Type[] { typeof(AvailableFare) };


                                filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                                XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse),types);
                                StreamWriter sw = new StreamWriter(filePath);
                                ser.Serialize(sw, tripAvailResponse);
                                sw.Close();
                            }
                            catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                            


                            if (tripAvailResponse != null && request.Type == SearchType.OneWay)
                            {
                                List<Journey> OnwardJourneys = new List<Journey>();

                                List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();

                                for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                                {
                                    for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                                    {
                                        if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                                        {
                                            OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                                        }
                                    }
                                }





                                for (int i = 0; i < OnwardJourneys.Count; i++)
                                {
                                    for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                    {
                                        onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                                    }
                                }



                                if (OnwardJourneys != null && OnwardJourneys.Count > 0)
                                {

                                    rateOfExchange = exchangeRates[OnwardJourneys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];

                                    int fareBreakDownCount = 0;

                                    if (request.AdultCount > 0)
                                    {
                                        fareBreakDownCount = 1;
                                    }
                                    if (request.ChildCount > 0)
                                    {
                                        fareBreakDownCount++;
                                    }
                                    if (request.InfantCount > 0)
                                    {
                                        fareBreakDownCount++;
                                    }

                                    for (int i = 0; i < OnwardJourneys.Count; i++)
                                    {
                                        if (OnwardJourneys[i].Segments.Length > 1)//Condition which determines that the journey has connecting flights.
                                        {
                                            #region Connecting Flights
                                            List<SearchResult> conFlightsResults = GetConnectingFlightsResults(OnwardJourneys[i], fareBreakDownCount);//Gets the connecting flight results.
                                            if (conFlightsResults.Count > 0)
                                            {
                                                ResultList.AddRange(conFlightsResults);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Direct Flights
                                            foreach (SpiceJet.SGBooking.Fare onFare in OnwardJourneys[i].Segments[0].Fares)
                                            {
                                                //Code added by Lokesh on 21/09/2016.
                                                FlightDesignator Designator = OnwardJourneys[i].Segments[0].FlightDesignator;
                                                string onwardFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(onFare.ProductClass).Replace(" ", "_") + "_" + Convert.ToString(OnwardJourneys[i].Segments[0].STD).Replace('/' ,'_').Replace(" " , "_").Replace(':' ,'_');

                                                PriceItineraryResponse onResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onFare.FareSellKey, OnwardJourneys[i], onwardFare);
                                                //paxTaxBreakUp.Clear();

                                                paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                                                adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();


                                               

                                                SearchResult result = new SearchResult();
                                                result.IsLCC = true;
                                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                                }
                                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                }
                                                result.Airline = "SG";
                                                result.Currency = agentBaseCurrency;
                                                result.EticketEligible = true;
                                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                                result.FareRules = new List<FareRule>();
                                                result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                result.FareType = GetFareTypeForProductClass(onFare.ProductClass);
                                                result.FareSellKey = onFare.FareSellKey;
                                                result.NonRefundable = false;
                                                if (request.Type == SearchType.OneWay)
                                                {
                                                    result.Flights = new FlightInfo[1][];
                                                }

                                                result.Flights[0] = new FlightInfo[1];

                                                for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                                {
                                                    result.Flights[0][k] = new FlightInfo();
                                                    result.Flights[0][k].Airline = "SG";
                                                    result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                    result.Flights[0][k].ArrTerminal = "";
                                                    result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "C");
                                                    result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                    result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                    result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);

                                                    result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[0][k].Group = 0;
                                                    result.Flights[0][k].OperatingCarrier = "SG";
                                                    result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                    result.Flights[0][k].Stops = OnwardJourneys[i].Segments.Length-1;
                                                    result.Flights[0][k].Status = "";
                                                    result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                    result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onFare.ProductClass);

                                                    result.Price = new PriceAccounts();

                                                    List<string> FarePaxtypes = new List<string>();//List which holds the Pax types  : "ADT","CHD"

                                                    foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }

                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();//Gets the unique pax type for fare price calculation.

                                                    #region FareBreakdown of adult and child for one way journey -- direct flights
                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;

                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }


                                                        if (onResponse != null)
                                                        {

                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":

                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        //result.FareBreakdown[0].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        // result.FareBreakdown[0].TotalFare += (double)result.FareBreakdown[0].BaseFare;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        // result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;

                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        break;
                                                                                }

                                                                                break;

                                                                            case "CHD":

                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        //  result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        // result.FareBreakdown[1].TotalFare += (double)result.FareBreakdown[1].BaseFare;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        //  result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        //result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;

                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                                        break;
                                                                                }


                                                                                break;

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                    /*  foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        switch (fare.PaxType)
                                                        {
                                                            case "ADT":
                                                                result.FareBreakdown[0] = new Fare();
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD":
                                                                result.FareBreakdown[1] = new Fare();
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;

                                                        }
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            result.FareBreakdown[2] = new Fare();
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            result.FareBreakdown[1] = new Fare();
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }
                                                        if (onResponse != null)
                                                        {
                                                            foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                            {
                                                                if (fare.PaxType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (fare.PaxType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        result.FareBreakdown[0].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += (double)result.FareBreakdown[0].BaseFare;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                }
                                                                                break;
                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += (double)result.FareBreakdown[1].BaseFare;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                }
                                                                                break;

                                                                        }
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    } */
                                                }

                                                #region #region FareBreakdown of infant for one way journey -- direct flights
                                                if (request.InfantCount > 0 && onResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in onResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;

                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    // result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;

                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}
                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    //result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                                    //result.FareBreakdown[1].TotalFare += (double)result.FareBreakdown[1].BaseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    //result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    //result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;

                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}

                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }

                                                    }
                                                }
                                                #endregion

                                                //if(paxTaxBreakUp.Count >0)
                                                //{
                                                //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                                                //}
                                                if(adtPaxTaxBreakUp.Count >0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT",adtPaxTaxBreakUp));
                                                }
                                                if (chdPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                                                }
                                                if (inftPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                                                }
                                                if(paxTypeTaxBreakUp.Count >0)
                                                {
                                                    result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                                                }
                                                result.BaggageIncludedInFare = onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                                result.Price.SupplierCurrency = currencyCode;
                                                result.BaseFare = (double)result.Price.PublishedFare;
                                                result.Tax = (double)result.Price.Tax;
                                                result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax),agentDecimalValue));
                                                ResultList.Add(result);
                                            }
                                            #endregion
                                        }

                                    }
                                }

                                CombinedSearchResults.AddRange(ResultList);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get results for One-way search. Reason : " + ex.ToString(), "");
                        }
                    }

                    results = CombinedSearchResults.ToArray();//Combine all the results and add them to array.

                    Logout(loginResponse.Signature);//Ends a user’s session and clears session from the server.
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Search failed. Reason : " + ex.ToString(), ex);
            }

            return results;
        }

        /// <summary>
        /// Regular, Regular (Sale), Flex & Special Return Trip fares are combined in the method. Other than these fares will be shown one-one only.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchForRegularFares(object eventNumber)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            //Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();
            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "RS";

                    //In order to get connecting flights this value should be greater than 5
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                    //Currently SpiceJet is allowing only Lowest Fares out of all combinations available in the Connecting segments
                    //that's why we are passing LowestFareClass which will always return Single Fare i.e. Regular Fare (Saver)
                    //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.LowestFareClass;

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[2];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                    //}
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;


                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestRegular_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }
                


                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);


                try
                {
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseRegular_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                    doc.Save(filePath);

                    //filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                    //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                    //StreamWriter sw = new StreamWriter(filePath);
                    //ser.Serialize(sw, tripAvailResponse);
                    //sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                    // rateOfExchange = exchangeRates[currencyCode];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                if (OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments.Length == 1)
                                {
                                    foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                    {
                                        foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                        {
                                            //Code added by Lokesh on 21/09/2016.
                                            FlightDesignator Designator = OnwardJourneys[i].Segments[0].FlightDesignator;
                                            string onwardFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(onfare.ProductClass).Replace(" ", "_") + Convert.ToString(OnwardJourneys[i].Segments[0].STD).Replace('/' ,'_').Replace(" " , "_").Replace(':' ,'_');
                                            Designator = ReturnJourneys[j].Segments[0].FlightDesignator;
                                            string returnFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(retFare.ProductClass).Replace(" ", "_") + Convert.ToString(ReturnJourneys[j].Segments[0].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_') ;
                                            //PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, OnwardJourneys[i].Segments[0], ReturnJourneys[j].Segments[0], onwardFare, returnFare);
                                            PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i], onwardFare + returnFare, retFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);

                                            if (priceResponse != null)
                                            {
                                                //paxTaxBreakUp.Clear();

                                                paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                                                adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();

                                           

                                                rateOfExchange = exchangeRates[priceResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                                                SearchResult result = new SearchResult();
                                                result.IsLCC = true;
                                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                                }
                                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                }
                                                result.Airline = "SG";
                                                result.Currency = agentBaseCurrency;
                                                result.EticketEligible = true;
                                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                                result.FareRules = new List<FareRule>();
                                                result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                if (request.Type == SearchType.OneWay)
                                                {
                                                    result.Flights = new FlightInfo[1][];
                                                }
                                                else
                                                {
                                                    result.Flights = new FlightInfo[2][];
                                                }
                                                result.Flights[0] = new FlightInfo[1];
                                                for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                                {
                                                    result.Flights[0][k] = new FlightInfo();
                                                    result.Flights[0][k].Airline = "SG";
                                                    result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                    result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                                    result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                    result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                    result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                                    result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                                    result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[0][k].Group = 0;
                                                    result.Flights[0][k].OperatingCarrier = "SG";
                                                    result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                    result.Flights[0][k].Stops = OnwardJourneys[i].Segments.Length-1;
                                                    result.Flights[0][k].Status = "";
                                                    result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                    result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey = onfare.FareSellKey;
                                                    result.Price = new PriceAccounts();
                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();


                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;

                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }


                                                        if (priceResponse != null)
                                                        {
                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;

                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                result.BaggageIncludedInFare = priceResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();

                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}
                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}
                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }

                                                    }
                                                }                                               

                                                //****************** Return Flight details ***************************//

                                                result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                                result.FareSellKey += "|" + retFare.FareSellKey;
                                                result.Flights[1] = new FlightInfo[1];
                                                for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                                {
                                                    result.Flights[1][k] = new FlightInfo();
                                                    result.Flights[1][k].Airline = "SG";
                                                    result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                    result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                    result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                    result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                    result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                    result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                    result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[1][k].Group = 1;
                                                    result.Flights[1][k].OperatingCarrier = "SG";
                                                    result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                    result.Flights[1][k].Stops = ReturnJourneys[j].Segments.Length-1;
                                                    result.Flights[1][k].Status = "";
                                                    result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                    result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey += "|" + retFare.FareSellKey;

                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in ReturnJourneys[j].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();
                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;
                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }

                                                        if (priceResponse != null)
                                                        {

                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }                                                    
                                                }

                                                result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[1].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}


                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}


                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                                //if (paxTaxBreakUp.Count > 0)
                                                //{
                                                //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                                                //}

                                                if (adtPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                                                }
                                                if (chdPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                                                }
                                                if (inftPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                                                }
                                                if (paxTypeTaxBreakUp.Count > 0)
                                                {
                                                    result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                                                }

                                                result.Price.SupplierCurrency = currencyCode;
                                                result.BaseFare = (double)(result.Price.PublishedFare);
                                                result.Tax = (double)(result.Price.Tax);
                                                result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax),agentDecimalValue));
                                                ResultList.Add(result);
                                            }
                                        }
                                    }
                                }
                                else //Connecting flight results
                                {
                                    if (OnwardJourneys[i].Segments.Length > 1 && ReturnJourneys[j].Segments.Length > 1)
                                    {
                                        List<SearchResult> conFlightsResults = GetConnectingFlightsCombinationsResults(OnwardJourneys[i], ReturnJourneys[j], fareBreakDownCount);
                                        if (conFlightsResults.Count > 0)
                                        {
                                            ResultList.AddRange(conFlightsResults);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get RegularFare results." + ex.ToString(), "");
                eventFlag[(int)eventNumber].Set();
            }
        }

        /// <summary>
        /// Advance Purchase Fares (These are seasonal fares which will only be active for several days).
        /// When cancelled only statuatory taxes will be refunded. 
        /// TODO: We need to have DB Table for these kind of fares to switch on/off in search.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchForPromoFares(object eventNumber)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            //Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();

            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";
                    //In order to get connecting flights this value should be greater than 5
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                    //Currently SpiceJet is allowing only Lowest Fares out of all combinations available in the Connecting segments
                    //that's why we are passing LowestFareClass which will always return Single Fare i.e. Regular Fare (Saver)
                    //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.LowestFareClass;


                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "IO";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "MX";//SpiceFlex
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "C";//CorporateFare
                    }

                        //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                        //}
                        int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestAdvPurchase_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }

                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                try
                {
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseAdvPurchase_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                    doc.Save(filePath);

                    //filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                    //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                    //StreamWriter sw = new StreamWriter(filePath);
                    //ser.Serialize(sw, tripAvailResponse);
                    //sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                   // rateOfExchange = exchangeRates[currencyCode];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                if (OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments.Length == 1)
                                {
                                    foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                    {
                                        foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                        {
                                            //Code added by Lokesh on 21/09/2016.
                                            FlightDesignator Designator = OnwardJourneys[i].Segments[0].FlightDesignator;
                                            string onwardFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(onfare.ProductClass).Replace(" ", "_") + Convert.ToString(OnwardJourneys[i].Segments[0].STD).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            Designator = ReturnJourneys[j].Segments[0].FlightDesignator;
                                            string returnFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(retFare.ProductClass).Replace(" ", "_") + Convert.ToString(ReturnJourneys[j].Segments[0].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            //PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, OnwardJourneys[i].Segments[0], ReturnJourneys[j].Segments[0], onwardFare, returnFare);
                                            PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i], onwardFare + returnFare, retFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);

                                            
                                            if (priceResponse != null)
                                            {
                                                //paxTaxBreakUp.Clear();
                                                paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                                                adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();


                                                
                                                rateOfExchange = exchangeRates[priceResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];

                                                SearchResult result = new SearchResult();
                                                result.IsLCC = true;
                                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                                }
                                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                }
                                                result.Airline = "SG";
                                                result.Currency = agentBaseCurrency;
                                                result.EticketEligible = true;
                                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                                result.FareRules = new List<FareRule>();
                                                result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                if (request.Type == SearchType.OneWay)
                                                {
                                                    result.Flights = new FlightInfo[1][];
                                                }
                                                else
                                                {
                                                    result.Flights = new FlightInfo[2][];
                                                }
                                                result.Flights[0] = new FlightInfo[1];
                                                for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                                {
                                                    result.Flights[0][k] = new FlightInfo();
                                                    result.Flights[0][k].Airline = "SG";
                                                    result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                    result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                                    result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                    result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                    result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                                    result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                                    result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[0][k].Group = 0;
                                                    result.Flights[0][k].OperatingCarrier = "SG";
                                                    result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                    result.Flights[0][k].Stops = OnwardJourneys[i].Segments.Length-1;
                                                    result.Flights[0][k].Status = "";
                                                    result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                    result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey = onfare.FareSellKey;
                                                    result.Price = new PriceAccounts();

                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();


                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;

                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }


                                                        if (priceResponse != null)
                                                        {

                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":

                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":

                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                                        break;
                                                                                }


                                                                                break;

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }


                                                    
                                                }
                                                result.BaggageIncludedInFare = priceResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();

                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}

                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}

                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }

                                                    }
                                                }

                                                //************************ Return flight details *******************************//

                                                result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                                result.FareSellKey += "|" + retFare.FareSellKey;
                                                result.Flights[1] = new FlightInfo[1];
                                                for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                                {
                                                    //if (retFare.ProductClass == ReturnJourneys[j].Segments[k].Fares[0].ProductClass)

                                                    result.Flights[1][k] = new FlightInfo();
                                                    result.Flights[1][k].Airline = "SG";
                                                    result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                    result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                    result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                    result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                    result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                    result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                    result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[1][k].Group = 1;
                                                    result.Flights[1][k].OperatingCarrier = "SG";
                                                    result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                    result.Flights[1][k].Stops = ReturnJourneys[j].Segments.Length-1;
                                                    result.Flights[1][k].Status = "";
                                                    result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                    result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey += "|" + retFare.FareSellKey;

                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in ReturnJourneys[j].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();
                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;
                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }

                                                        if (priceResponse != null)
                                                        {
                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":

                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}


                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":

                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}



                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[1].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}


                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}


                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }

                                                //if (paxTaxBreakUp.Count > 0)
                                                //{
                                                //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                                                //}


                                                if (adtPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                                                }
                                                if (chdPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                                                }
                                                if (inftPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                                                }
                                                if (paxTypeTaxBreakUp.Count > 0)
                                                {
                                                    result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                                                }


                                                result.Price.SupplierCurrency = currencyCode;
                                                result.BaseFare = (double)result.Price.PublishedFare;
                                                result.Tax = (double)result.Price.Tax;
                                                result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax), agentDecimalValue));

                                                ResultList.Add(result);
                                            }                                            
                                        }
                                    }
                                }
                                else //Connecting flights
                                {
                                    if (OnwardJourneys[i].Segments.Length > 1 && ReturnJourneys[j].Segments.Length > 1)
                                    {
                                        List<SearchResult> conFlightsResults = GetConnectingFlightsCombinationsResults(OnwardJourneys[i], ReturnJourneys[j], fareBreakDownCount);
                                        if (conFlightsResults.Count > 0)
                                        {
                                            ResultList.AddRange(conFlightsResults);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get FlexFare results." + ex.ToString(), "");
                eventFlag[(int)eventNumber].Set();
            }
        }
        /// <summary>
        /// Family Fares ( Only when pax count is greater than or equal to 4 excluding infants )
        /// These fares cannot be combined with fares. When cancelled charges will be applicable.         
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchForFamilyFares(object eventNumber)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            //Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();

            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
             List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();


            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";
                    //In order to get connecting flights this value should be greater than 5
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                    //Currently SpiceJet is allowing only Lowest Fares out of all combinations available in the Connecting segments
                    //that's why we are passing LowestFareClass which will always return Single Fare i.e. Regular Fare (Saver)
                    //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.LowestFareClass;

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "F";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "MX";//Spice Flex
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "C";//CorporateFare;
                    }
                        //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                        //}
                        int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestFamily_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }

                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);


                try
                {
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseFamily_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                    doc.Save(filePath);

                    //filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                    //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                    //StreamWriter sw = new StreamWriter(filePath);
                    //ser.Serialize(sw, tripAvailResponse);
                    //sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                   // rateOfExchange = exchangeRates[currencyCode];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                if (OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments.Length == 1)
                                {
                                    foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                    {
                                        foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                        {
                                            //Code added by Lokesh on 21/09/2016.
                                            FlightDesignator Designator = OnwardJourneys[i].Segments[0].FlightDesignator;
                                            string onwardFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(onfare.ProductClass).Replace(" ", "_") + Convert.ToString(OnwardJourneys[i].Segments[0].STD).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            Designator = ReturnJourneys[j].Segments[0].FlightDesignator;
                                            string returnFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(retFare.ProductClass).Replace(" ", "_") + Convert.ToString(ReturnJourneys[j].Segments[0].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            //PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, OnwardJourneys[i].Segments[0], ReturnJourneys[j].Segments[0], onwardFare, returnFare);
                                            PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i], onwardFare + returnFare, retFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);

                                            if (priceResponse != null)
                                            {
                                                rateOfExchange = exchangeRates[priceResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                                                //paxTaxBreakUp.Clear();

                                                paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                                                adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();

                                                


                                                SearchResult result = new SearchResult();
                                                result.IsLCC = true;
                                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                                }
                                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                }
                                                result.Airline = "SG";
                                                result.Currency = agentBaseCurrency;
                                                result.EticketEligible = true;
                                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                                result.FareRules = new List<FareRule>();
                                                result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                if (request.Type == SearchType.OneWay)
                                                {
                                                    result.Flights = new FlightInfo[1][];
                                                }
                                                else
                                                {
                                                    result.Flights = new FlightInfo[2][];
                                                }
                                                result.Flights[0] = new FlightInfo[1];
                                                for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                                {
                                                    result.Flights[0][k] = new FlightInfo();
                                                    result.Flights[0][k].Airline = "SG";
                                                    result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                    result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                                    result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                    result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                    result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                                    result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                                    result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[0][k].Group = 0;
                                                    result.Flights[0][k].OperatingCarrier = "SG";
                                                    result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                    result.Flights[0][k].Stops = OnwardJourneys[i].Segments.Length-1;
                                                    result.Flights[0][k].Status = "";
                                                    result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                    result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey = onfare.FareSellKey;
                                                    result.Price = new PriceAccounts();

                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();


                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;

                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }


                                                        if (priceResponse != null)
                                                        {
                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}


                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}


                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }                                                   
                                                }

                                                result.BaggageIncludedInFare = priceResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}


                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}

                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }

                                                //******************* Return flight details ****************************//

                                                result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                                result.FareSellKey += "|" + retFare.FareSellKey;
                                                result.Flights[1] = new FlightInfo[1];
                                                for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                                {
                                                    //if (retFare.ProductClass == ReturnJourneys[j].Segments[k].Fares[0].ProductClass)

                                                    result.Flights[1][k] = new FlightInfo();
                                                    result.Flights[1][k].Airline = "SG";
                                                    result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                    result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                    result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                    result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                    result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                    result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                    result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[1][k].Group = 1;
                                                    result.Flights[1][k].OperatingCarrier = "SG";
                                                    result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                    result.Flights[1][k].Stops = ReturnJourneys[j].Segments.Length-1;
                                                    result.Flights[1][k].Status = "";
                                                    result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                    result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey += "|" + retFare.FareSellKey;

                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in ReturnJourneys[j].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();
                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;
                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }

                                                        if (priceResponse != null)
                                                        {
                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}



                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}


                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }                                                   
                                                }
                                                result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[1].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}

                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}

                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                                //if (paxTaxBreakUp.Count > 0)
                                                //{
                                                //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                                                //}
                                                if (adtPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                                                }
                                                if (chdPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                                                }
                                                if (inftPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                                                }
                                                if (paxTypeTaxBreakUp.Count > 0)
                                                {
                                                    result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                                                }


                                                result.Price.SupplierCurrency = currencyCode;
                                                result.BaseFare = (double)result.Price.PublishedFare;
                                                result.Tax = (double)result.Price.Tax;
                                                result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax), agentDecimalValue));

                                                ResultList.Add(result);
                                            }
                                        }
                                    }
                                }
                                else //Connecting flights
                                {
                                    if (OnwardJourneys[i].Segments.Length > 1 && ReturnJourneys[j].Segments.Length > 1)
                                    {
                                        List<SearchResult> conFlightsResults = GetConnectingFlightsCombinationsResults(OnwardJourneys[i], ReturnJourneys[j], fareBreakDownCount);
                                        if (conFlightsResults.Count > 0)
                                        {
                                            ResultList.AddRange(conFlightsResults);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get FamilyFares results." + ex.ToString(), "");
                eventFlag[(int)eventNumber].Set();
            }
        }

        /// <summary>
        /// Not In use now
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchForSpecialReturnFares(object eventNumber)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
             List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            //Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();
            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "XA";
                    //In order to get connecting flights this value should be greater than 5
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                    //Currently SpiceJet is allowing only Lowest Fares out of all combinations available in the Connecting segments
                    //that's why we are passing LowestFareClass which will always return Single Fare i.e. Regular Fare (Saver)
                    //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.LowestFareClass;

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[2];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                    //}
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;


                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestSpecialRT_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }
               


                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);


                try
                {
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseSpecialRT_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                    doc.Save(filePath);

                    //filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                    //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                    //StreamWriter sw = new StreamWriter(filePath);
                    //ser.Serialize(sw, tripAvailResponse);
                    //sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
            

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                  //  rateOfExchange = exchangeRates[currencyCode];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                if (OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments.Length == 1)
                                {
                                    foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                    {
                                        foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                        {
                                            //Code added by Lokesh on 21/09/2016.
                                            FlightDesignator Designator = OnwardJourneys[i].Segments[0].FlightDesignator;
                                            string onwardFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(onfare.ProductClass).Replace(" ", "_") + Convert.ToString(OnwardJourneys[i].Segments[0].STD).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            Designator = ReturnJourneys[j].Segments[0].FlightDesignator;
                                            string returnFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(retFare.ProductClass).Replace(" ", "_") + Convert.ToString(ReturnJourneys[j].Segments[0].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            //PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, OnwardJourneys[i].Segments[0], ReturnJourneys[j].Segments[0], onwardFare, returnFare);
                                            PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i], onwardFare + returnFare, retFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);

                                            if (priceResponse != null)
                                            {
                                                rateOfExchange = exchangeRates[priceResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                                                //paxTaxBreakUp.Clear();

                                                paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                                                adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();

                                            

                                                SearchResult result = new SearchResult();
                                                result.IsLCC = true;
                                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                                }
                                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                }
                                                result.Airline = "SG";
                                                result.Currency = agentBaseCurrency;
                                                result.EticketEligible = true;
                                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                                result.FareRules = new List<FareRule>();
                                                result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                if (request.Type == SearchType.OneWay)
                                                {
                                                    result.Flights = new FlightInfo[1][];
                                                }
                                                else
                                                {
                                                    result.Flights = new FlightInfo[2][];
                                                }
                                                result.Flights[0] = new FlightInfo[1];
                                                for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                                {
                                                    result.Flights[0][k] = new FlightInfo();
                                                    result.Flights[0][k].Airline = "SG";
                                                    result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                    result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                                    result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                    result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                    result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                                    result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                                    result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[0][k].Group = 0;
                                                    result.Flights[0][k].OperatingCarrier = "SG";
                                                    result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                    result.Flights[0][k].Stops = OnwardJourneys[i].Segments.Length-1;
                                                    result.Flights[0][k].Status = "";
                                                    result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                    result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey = onfare.FareSellKey;
                                                    result.Price = new PriceAccounts();
                                                    //int count = 0;

                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();


                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;

                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }


                                                        if (priceResponse != null)
                                                        {
                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;

                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));





                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }                                                    
                                                }
                                                result.BaggageIncludedInFare = priceResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();

                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}



                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}



                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                    }
                                                } 

                                                //*************************** return flight details ****************************//

                                                result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                                result.FareSellKey += "|" + retFare.FareSellKey;
                                                result.Flights[1] = new FlightInfo[1];
                                                for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                                {
                                                    //if (retFare.ProductClass == ReturnJourneys[j].Segments[k].Fares[0].ProductClass)

                                                    result.Flights[1][k] = new FlightInfo();
                                                    result.Flights[1][k].Airline = "SG";
                                                    result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                    result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                    result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                    result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                    result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                    result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                    result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[1][k].Group = 1;
                                                    result.Flights[1][k].OperatingCarrier = "SG";
                                                    result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                    result.Flights[1][k].Stops = ReturnJourneys[j].Segments.Length-1;
                                                    result.Flights[1][k].Status = "";
                                                    result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                    result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey += "|" + retFare.FareSellKey;
                                                    //int count = 0;
                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in ReturnJourneys[j].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();
                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;
                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }

                                                        if (priceResponse != null)
                                                        {
                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[1].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}


                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));





                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}




                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                                //if (paxTaxBreakUp.Count > 0)
                                                //{
                                                //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                                                //}

                                                if (adtPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                                                }
                                                if (chdPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                                                }
                                                if (inftPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                                                }
                                                if (paxTypeTaxBreakUp.Count > 0)
                                                {
                                                    result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                                                }

                                                result.Price.SupplierCurrency = currencyCode;
                                                result.BaseFare = (double)result.Price.PublishedFare;
                                                result.Tax = (double)result.Price.Tax;
                                                result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax), agentDecimalValue));
                                                ResultList.Add(result);
                                            }
                                        }
                                    }
                                }
                                else //Connecting flight results
                                {
                                    if (OnwardJourneys[i].Segments.Length > 1 && ReturnJourneys[j].Segments.Length > 1)
                                    {
                                        List<SearchResult> conFlightsResults = GetConnectingFlightsCombinationsResults(OnwardJourneys[i], ReturnJourneys[j], fareBreakDownCount);
                                        if (conFlightsResults.Count > 0)
                                        {
                                            ResultList.AddRange(conFlightsResults);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get SpecialReturn results." + ex.ToString(), "");
                eventFlag[(int)eventNumber].Set();
            }
        }


        /// <summary>
        /// Hand Baggage Only Fares ( Available only for Domestic sectors and generally returns low fares. No
        /// Excess baggage can be allowed with this fare. 
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchForHandBaggageFares(object eventNumber)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            //Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();
            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
             List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";
                    //In order to get connecting flights this value should be greater than 5
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                    //Currently SpiceJet is allowing only Lowest Fares out of all combinations available in the Connecting segments
                    //that's why we are passing LowestFareClass which will always return Single Fare i.e. Regular Fare (Saver)
                    //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.LowestFareClass;

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "HB";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "MX";//Spice Flex
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "C";//CorporateFare;
                    }
                    //}
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestHandBaggage_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }

                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);


                try
                {
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseHandBaggage_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                    doc.Save(filePath);

                    //filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                    //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                    //StreamWriter sw = new StreamWriter(filePath);
                    //ser.Serialize(sw, tripAvailResponse);
                    //sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                    //rateOfExchange = exchangeRates[currencyCode];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                if (OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments.Length == 1)
                                {
                                    foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                    {
                                        foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                        {
                                            //Code added by Lokesh on 21/09/2016.
                                            FlightDesignator Designator = OnwardJourneys[i].Segments[0].FlightDesignator;
                                            string onwardFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(onfare.ProductClass).Replace(" ", "_") + Convert.ToString(OnwardJourneys[i].Segments[0].STD).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            Designator = ReturnJourneys[j].Segments[0].FlightDesignator;
                                            string returnFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(retFare.ProductClass).Replace(" ", "_") + Convert.ToString(ReturnJourneys[j].Segments[0].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            //PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, OnwardJourneys[i].Segments[0], ReturnJourneys[j].Segments[0], onwardFare, returnFare);
                                            PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i], onwardFare + returnFare, retFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);
                                            if (priceResponse != null)
                                            {
                                               // paxTaxBreakUp.Clear();

                                                paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                                                adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();

                                             
                                                SearchResult result = new SearchResult();
                                                rateOfExchange = exchangeRates[priceResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];

                                                result.IsLCC = true;
                                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                                }
                                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                }
                                                result.Airline = "SG";
                                                result.Currency = agentBaseCurrency;
                                                result.EticketEligible = true;
                                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                                result.FareRules = new List<FareRule>();
                                                result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                if (request.Type == SearchType.OneWay)
                                                {
                                                    result.Flights = new FlightInfo[1][];
                                                }
                                                else
                                                {
                                                    result.Flights = new FlightInfo[2][];
                                                }
                                                result.Flights[0] = new FlightInfo[1];
                                                for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                                {
                                                    result.Flights[0][k] = new FlightInfo();
                                                    result.Flights[0][k].Airline = "SG";
                                                    result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                    result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                                    result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                    result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                    result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                                    result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                                    result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[0][k].Group = 0;
                                                    result.Flights[0][k].OperatingCarrier = "SG";
                                                    result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                    result.Flights[0][k].Stops = OnwardJourneys[i].Segments.Length-1;
                                                    result.Flights[0][k].Status = "";
                                                    result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                    result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey = onfare.FareSellKey;
                                                    result.Price = new PriceAccounts();

                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();


                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;

                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }


                                                        if (priceResponse != null)
                                                        {
                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}

                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}


                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }                                                    
                                                }

                                                result.BaggageIncludedInFare = priceResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                                
                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}


                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}

                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                               
                                                //********************* return flight details ************************//

                                                result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                                result.FareSellKey += "|" + retFare.FareSellKey;
                                                result.Flights[1] = new FlightInfo[1];
                                                for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                                {
                                                    result.Flights[1][k] = new FlightInfo();
                                                    result.Flights[1][k].Airline = "SG";
                                                    result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                    result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                    result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                    result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                    result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                    result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                    result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[1][k].Group = 1;
                                                    result.Flights[1][k].OperatingCarrier = "SG";
                                                    result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                    result.Flights[1][k].Stops = ReturnJourneys[j].Segments.Length-1;
                                                    result.Flights[1][k].Status = "";
                                                    result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                    result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey += "|" + retFare.FareSellKey;


                                                    List<string> FarePaxtypes = new List<string>();

                                                    foreach (PaxFare fare in ReturnJourneys[j].Segments[k].Fares[0].PaxFares)
                                                    {
                                                        FarePaxtypes.Add(fare.PaxType);
                                                    }
                                                    FarePaxtypes = FarePaxtypes.Distinct().ToList();
                                                    foreach (String passengerType in FarePaxtypes)
                                                    {
                                                        switch (passengerType)
                                                        {
                                                            case "ADT": //Adult
                                                                if (result.FareBreakdown[0] == null)
                                                                {
                                                                    result.FareBreakdown[0] = new Fare();
                                                                }
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                break;
                                                            case "CHD": //Child
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                break;
                                                        }

                                                        //Infant Count
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }

                                                        //Child Count
                                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                        }

                                                        if (priceResponse != null)
                                                        {
                                                            //We need to combine fares of all the segments from the price itenary response

                                                            foreach (PaxFare pfare in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares)
                                                            {
                                                                if (passengerType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (passengerType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}


                                                                                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                                        break;
                                                                                }
                                                                                break;

                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        double baseFareChd = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                        result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                        result.Price.Tax += (decimal)tax;
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                        //}
                                                                                        //else
                                                                                        //{
                                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                        //}


                                                                                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }                                                   
                                                }
                                                result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                if (request.InfantCount > 0 && priceResponse != null)
                                                {
                                                    foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[1].ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                    double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += baseFare;
                                                                    result.FareBreakdown[2].BaseFare += baseFare;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                    result.FareBreakdown[2].TotalFare += tax;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}




                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].BaseFare += baseFare;
                                                                    result.FareBreakdown[1].TotalFare += baseFare;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.PublishedFare += (decimal)baseFare;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                    result.FareBreakdown[1].TotalFare += tax;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                    result.Price.Tax += (decimal)tax;
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                    //}



                                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                                //if (paxTaxBreakUp.Count > 0)
                                                //{
                                                //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                                                //}

                                                if (adtPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                                                }
                                                if (chdPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                                                }
                                                if (inftPaxTaxBreakUp.Count > 0)
                                                {
                                                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                                                }
                                                if (paxTypeTaxBreakUp.Count > 0)
                                                {
                                                    result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                                                }

                                                result.Price.SupplierCurrency = currencyCode;
                                                result.BaseFare = (double)result.Price.PublishedFare;
                                                result.Tax = (double)result.Price.Tax;
                                                result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax), agentDecimalValue));
                                                ResultList.Add(result);
                                            }
                                        }
                                    }
                                }
                                else //Connecting flight results
                                {
                                    if (OnwardJourneys[i].Segments.Length > 1 && ReturnJourneys[j].Segments.Length > 1)
                                    {
                                        List<SearchResult> conFlightsResults = GetConnectingFlightsCombinationsResults(OnwardJourneys[i], ReturnJourneys[j], fareBreakDownCount);
                                        if (conFlightsResults.Count > 0)
                                        {
                                            ResultList.AddRange(conFlightsResults);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get HandBaggage results." + ex.ToString(), "");
                eventFlag[(int)eventNumber].Set();
            }
        }

        /// <summary>
        /// In this method we are making combinations of Regular (Saver), Regular (Saver Sale(Promo)), Flex & Special Return Trip fares.
        /// All these fares will be made combination of each other.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchForAllFares(object eventNumber)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            List<Journey> OnwardJourneys = new List<Journey>();
            List<Journey> ReturnJourneys = new List<Journey>();
            //Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();
            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
             List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();


            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";//Regular Fare
                    //In order to get connecting flights this value should be greater than 5
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                    //Currently SpiceJet is allowing only Lowest Fares out of all combinations available in the Connecting segments
                    //that's why we are passing LowestFareClass which will always return Single Fare i.e. Regular Fare (Saver)
                    //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.LowestFareClass;

                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[5];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "S";//Added by Shiva 22-Sep-2016 for Price difference
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "T";//Added by brahmam 22.07.2016 For Coupon Fares
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[3] = "MX";//Spice Flex
                        if (!string.IsNullOrEmpty(promoCode))
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[4] = "C";//CorporateFare
                        }
                    }

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestRegularFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }

                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);


                try
                {
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseRegularFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                    doc.Save(filePath);
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                

                if (tripAvailResponse != null)//Seperate onward and return journeys
                {
                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                                {
                                    OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                                }
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                                {
                                    ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                                }
                            }
                        }
                    }
                }
                //------------------------------------------------End Search Regular Fares-----------------------------------------------------

                #region Old Commented code
                ////------------------------------------------------Search for Flex Fares now----------------------------------------------------

                //for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                //{
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "HF";//Flex Fare
                //}

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                //{
                //    try
                //    {
                //        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                //        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestFlexFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //        StreamWriter sw = new StreamWriter(filePath);
                //        ser.Serialize(sw, availRequest);
                //        sw.Close();
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                //    }
                //}

                //tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                //{
                //    try
                //    {
                //        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseFlexFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                //        doc.Save(filePath);
                //    }
                //    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                //}

                //if (tripAvailResponse != null) //Seperate onward and return journeys
                //{
                //    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                //    {
                //        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                //        {
                //            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                //            {
                //                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                //                {
                //                    OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                //                }
                //            }
                //            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                //            {
                //                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                //                {
                //                    ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                //                }
                //            }
                //        }
                //    }
                //}

                ////----------------------------------------------------End Search Flex Fares-----------------------------------------------------

                ////------------------------------------------------Search for HandBaggage Fares now----------------------------------------------------

                //for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                //{
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "HO";//HandBaggage Fare
                //}

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                //{
                //    try
                //    {
                //        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                //        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestHandBaggageFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //        StreamWriter sw = new StreamWriter(filePath);
                //        ser.Serialize(sw, availRequest);
                //        sw.Close();
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                //    }
                //}

                //tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                //{
                //    try
                //    {
                //        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseHandBaggageFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                //        doc.Save(filePath);
                //    }
                //    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                //}

                //if (tripAvailResponse != null)//Seperate onward and return journeys
                //{

                //    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                //    {
                //        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                //        {
                //            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                //            {
                //                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                //                {
                //                    OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                //                }
                //            }
                //            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                //            {
                //                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                //                {
                //                    ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                //                }
                //            }
                //        }
                //    }
                //}

                ////----------------------------------------------------End Search Hand Baggage Fares-----------------------------------------------------

                ////------------------------------------------------Search for Regular & Promo Fares now----------------------------------------------------

                ////for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                //{
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0] = new AvailabilityRequest();
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].ArrivalStation = request.Segments[1].Destination;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].BeginDate = request.Segments[1].PreferredDepartureTime;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].FlightType = FlightType.All;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].CarrierCode = "";
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].CurrencyCode = currencyCode;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].DepartureStation = request.Segments[1].Origin;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].DiscountCode = "";
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].DisplayCurrencyCode = currencyCode;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].Dow = DOW.Daily;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].EndDate = request.Segments[1].PreferredArrivalTime;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].FareClassControl = FareClassControl.Default;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].IncludeTaxesAndFees = false;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].FareRuleFilter = FareRuleFilter.Default;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].SSRCollectionsMode = SSRCollectionsMode.None;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].InboundOutbound = InboundOutbound.None;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].IncludeAllotments = false;


                //    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxCount = Convert.ToInt16(paxCount);
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxPriceTypes = new PaxPriceType[paxCount];

                //    for (int a = 0; a < request.AdultCount; a++)
                //    {
                //        availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxPriceTypes[a] = new PaxPriceType();
                //        availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxPriceTypes[a].PaxType = "ADT";
                //        availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxPriceTypes[a].PaxDiscountCode = "";
                //    }
                //    if (request.ChildCount > 0)
                //    {
                //        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                //        {
                //            availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxPriceTypes[c] = new PaxPriceType();
                //            availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxPriceTypes[c].PaxType = "CHD";
                //        }
                //    }
                //    if (request.InfantCount > 0)
                //    {
                //        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                //        {
                //            availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxPriceTypes[k] = new PaxPriceType();
                //            availRequest.TripAvailabilityRequest.AvailabilityRequests[0].PaxPriceTypes[k].PaxType = "INFT";
                //        }
                //    }

                //    availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].ProductClassCode = "SS";//Promo Fare
                //    //availRequest.TripAvailabilityRequest.AvailabilityRequests[1].ProductClassCode = "SS";//Promo Fare
                //}

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                //{
                //    try
                //    {
                //        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                //        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestRegularPromoFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //        StreamWriter sw = new StreamWriter(filePath);
                //        ser.Serialize(sw, availRequest);
                //        sw.Close();
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                //    }
                //}

                //tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                //{
                //    try
                //    {
                //        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseRegularPromoFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                //        doc.Save(filePath);
                //    }
                //    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                //}

                //if (tripAvailResponse != null)//Seperate onward and return journeys
                //{

                //    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                //    {
                //        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                //        {
                //            //if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                //            //{
                //            //    if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                //            //    {
                //            //        OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                //            //    }
                //            //}
                //            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[1].Origin)
                //            {
                //                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                //                {
                //                    ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                //                }
                //            }
                //        }
                //    }
                //}

                ////----------------------------------------------------End Search Regular & Promo Fares-----------------------------------------------------


                ////------------------------------------------------Search for Flex & Promo Fares now----------------------------------------------------

                ////for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                //{
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[0].ProductClassCode = "HF";//Promo Fare
                //    availRequest.TripAvailabilityRequest.AvailabilityRequests[1].ProductClassCode = "SS";//Promo Fare
                //}

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                //{
                //    try
                //    {
                //        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                //        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchRequestFlexPromoFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //        StreamWriter sw = new StreamWriter(filePath);
                //        ser.Serialize(sw, availRequest);
                //        sw.Close();
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                //    }
                //}

                //tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                //{
                //    try
                //    {
                //        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSearchResponseFlexPromoFares_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                //        doc.Save(filePath);
                //    }
                //    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                //}

                //if (tripAvailResponse != null)//Seperate onward and return journeys
                //{

                //    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                //    {
                //        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                //        {
                //            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                //            {
                //                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                //                {
                //                    OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                //                }
                //            }
                //            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                //            {
                //                if (tripAvailResponse.Schedules[i][j].Journeys.Length > 0)
                //                {
                //                    ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                //                }
                //            }
                //        }
                //    }
                //}

                ////----------------------------------------------------End Search Flex & Promo Fares-----------------------------------------------------
                

                #endregion


                int fareBreakDownCount = 0;

                if (request.AdultCount > 0)
                {
                    fareBreakDownCount = 1;
                }
                if (request.ChildCount > 0)
                {
                    fareBreakDownCount++;
                }
                if (request.InfantCount > 0)
                {
                    fareBreakDownCount++;
                }
                //Retrieve Currency Code from the search response
                //rateOfExchange = exchangeRates[OnwardJourneys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];

                if (request.Type == SearchType.Return)
                {
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int j = 0; j < ReturnJourneys.Count; j++)
                        {
                            if (OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments.Length == 1)
                            {
                                foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                {
                                    //if (onfare.ProductClass != "XA")
                                    {
                                        foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                        {
                                            //if (retFare.ProductClass != "XA")
                                            {
                                                //Code added by Lokesh on 21/09/2016.
                                                FlightDesignator Designator = OnwardJourneys[i].Segments[0].FlightDesignator;
                                                string onwardFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(onfare.ProductClass).Replace(" ", "_") + Convert.ToString(OnwardJourneys[i].Segments[0].STD).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                Designator = ReturnJourneys[j].Segments[0].FlightDesignator;
                                                string returnFare = Designator.FlightNumber + "_" + GetFareTypeForProductClass(retFare.ProductClass).Replace(" ", "_") + Convert.ToString(ReturnJourneys[j].Segments[0].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                //PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, OnwardJourneys[i].Segments[0], ReturnJourneys[j].Segments[0], onwardFare, returnFare,);
                                                PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i], onwardFare + returnFare, retFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);

                                                if (priceResponse != null)
                                                {
                                                    rateOfExchange = exchangeRates[priceResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                                                    //paxTaxBreakUp.Clear();

                                                    paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                                                    adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                    chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                    inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();

                                                    SearchResult result = new SearchResult();
                                                    result.IsLCC = true;
                                                    if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                    {
                                                        result.ResultBookingSource = BookingSource.SpiceJet;
                                                    }
                                                    else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                    {
                                                        result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                    }
                                                    result.Airline = "SG";
                                                    result.Currency = agentBaseCurrency;
                                                    result.EticketEligible = true;
                                                    result.FareBreakdown = new Fare[fareBreakDownCount];
                                                    result.FareRules = new List<FareRule>();
                                                    result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                    result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                    if (request.Type == SearchType.OneWay)
                                                    {
                                                        result.Flights = new FlightInfo[1][];
                                                    }
                                                    else
                                                    {
                                                        result.Flights = new FlightInfo[2][];
                                                    }
                                                    result.Flights[0] = new FlightInfo[1];
                                                    for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                                    {
                                                        result.Flights[0][k] = new FlightInfo();
                                                        result.Flights[0][k].Airline = "SG";
                                                        result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                        result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                        result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                                        result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                        result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                        result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                        result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                        result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                                        result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                                        result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                        result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                        result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                        result.Flights[0][k].Group = 0;
                                                        result.Flights[0][k].OperatingCarrier = "SG";
                                                        result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                        result.Flights[0][k].Stops = OnwardJourneys[i].Segments.Length-1;
                                                        result.Flights[0][k].Status = "";
                                                        result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                        result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                        result.NonRefundable = false;
                                                        result.FareSellKey = onfare.FareSellKey;
                                                        result.Price = new PriceAccounts();

                                                        //int count = 0;

                                                        List<string> FarePaxtypes = new List<string>();

                                                        foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                                        {
                                                            FarePaxtypes.Add(fare.PaxType);
                                                        }
                                                        FarePaxtypes = FarePaxtypes.Distinct().ToList();


                                                        foreach (String passengerType in FarePaxtypes)
                                                        {
                                                            switch (passengerType)
                                                            {
                                                                case "ADT": //Adult
                                                                    if (result.FareBreakdown[0] == null)
                                                                    {
                                                                        result.FareBreakdown[0] = new Fare();
                                                                    }
                                                                    result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                    result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                    break;
                                                                case "CHD": //Child
                                                                    if (result.FareBreakdown[1] == null)
                                                                    {
                                                                        result.FareBreakdown[1] = new Fare();
                                                                    }
                                                                    result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                    result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                    break;

                                                            }

                                                            //Infant Count
                                                            if (request.ChildCount > 0 && request.InfantCount > 0)
                                                            {
                                                                if (result.FareBreakdown[2] == null)
                                                                {
                                                                    result.FareBreakdown[2] = new Fare();
                                                                }
                                                                result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                                result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                            }

                                                            //Child Count
                                                            else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                            {
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                            }


                                                            if (priceResponse != null)
                                                            {
                                                                //We need to combine fares of all the segments from the price itenary response

                                                                foreach (PaxFare pfare in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                                                {
                                                                    if (passengerType == pfare.PaxType)
                                                                    {
                                                                        foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                        {
                                                                            switch (passengerType)
                                                                            {
                                                                                case "ADT":
                                                                                    switch (charge.ChargeType)
                                                                                    {
                                                                                        case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                            double baseFareAdult = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[0].PassengerCount);
                                                                                            result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                            result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                            result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                            result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                                            break;
                                                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                        case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                        case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                        case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                            double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[0].PassengerCount);
                                                                                            result.FareBreakdown[0].TotalFare += tax;
                                                                                            result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                            result.Price.Tax += (decimal)tax;
                                                                                            result.Price.SupplierPrice += charge.Amount;

                                                                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                            //{
                                                                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                            //}
                                                                                            //else
                                                                                            //{
                                                                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                            //}

                                                                                            adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                                            break;
                                                                                    }
                                                                                    break;

                                                                                case "CHD":
                                                                                    switch (charge.ChargeType)
                                                                                    {
                                                                                        case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                            double baseFareChd = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                                            result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                            result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                            result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                                            break;
                                                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                        case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                        case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                        case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                            double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                            result.FareBreakdown[1].TotalFare += tax;
                                                                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                            result.Price.Tax += (decimal)tax;
                                                                                            result.Price.SupplierPrice += charge.Amount;

                                                                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                            //{
                                                                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                            //}
                                                                                            //else
                                                                                            //{
                                                                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                            //}


                                                                                            chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                                            break;
                                                                                    }
                                                                                    break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    result.BaggageIncludedInFare = priceResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();

                                                    if (request.InfantCount > 0 && priceResponse != null)
                                                    {
                                                        foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                                                        {
                                                            if (request.ChildCount > 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                        double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                                                        result.FareBreakdown[2].TotalFare += baseFare;
                                                                        result.FareBreakdown[2].BaseFare += baseFare;
                                                                        result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                        result.Price.PublishedFare += (decimal)baseFare;
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                        result.FareBreakdown[2].TotalFare += tax;
                                                                        result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                        result.Price.Tax += (decimal)tax;
                                                                        result.Price.SupplierPrice += charge.Amount;

                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                        //{
                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                        //}
                                                                        //else
                                                                        //{
                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                        //}

                                                                        inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                        break;
                                                                }
                                                            }
                                                            else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                        double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                        result.FareBreakdown[1].BaseFare += baseFare;
                                                                        result.FareBreakdown[1].TotalFare += baseFare;
                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                        result.Price.PublishedFare += (decimal)baseFare;
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                        result.Price.Tax += (decimal)tax;
                                                                        result.Price.SupplierPrice += charge.Amount;

                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                        //{
                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                        //}
                                                                        //else
                                                                        //{
                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                        //}

                                                                        inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                        break;
                                                                }
                                                            }

                                                        }
                                                    }                                                   

                                                    //*************************** return flight details **************************//

                                                    result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                    result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.FareSellKey += "|" + retFare.FareSellKey;
                                                    result.Flights[1] = new FlightInfo[1];
                                                    for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                                    {
                                                        result.Flights[1][k] = new FlightInfo();
                                                        result.Flights[1][k].Airline = "SG";
                                                        result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                        result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                        result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                        result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                        result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                        result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                        result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                        result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                        result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                        result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                        result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                        result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                        result.Flights[1][k].Group = 1;
                                                        result.Flights[1][k].OperatingCarrier = "SG";
                                                        result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                        result.Flights[1][k].Stops = ReturnJourneys[j].Segments.Length-1;
                                                        result.Flights[1][k].Status = "";
                                                        result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                        result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                        result.NonRefundable = false;
                                                        result.FareSellKey += "|" + retFare.FareSellKey;
                                                        //int count = 0;

                                                        List<string> FarePaxtypes = new List<string>();

                                                        foreach (PaxFare fare in ReturnJourneys[j].Segments[k].Fares[0].PaxFares)
                                                        {
                                                            FarePaxtypes.Add(fare.PaxType);
                                                        }
                                                        FarePaxtypes = FarePaxtypes.Distinct().ToList();
                                                        foreach (String passengerType in FarePaxtypes)
                                                        {
                                                            switch (passengerType)
                                                            {
                                                                case "ADT": //Adult
                                                                    if (result.FareBreakdown[0] == null)
                                                                    {
                                                                        result.FareBreakdown[0] = new Fare();
                                                                    }
                                                                    result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                    result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                    break;
                                                                case "CHD": //Child
                                                                    if (result.FareBreakdown[1] == null)
                                                                    {
                                                                        result.FareBreakdown[1] = new Fare();
                                                                    }
                                                                    result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                                    result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                                    break;
                                                            }

                                                            //Infant Count
                                                            if (request.ChildCount > 0 && request.InfantCount > 0)
                                                            {
                                                                if (result.FareBreakdown[2] == null)
                                                                {
                                                                    result.FareBreakdown[2] = new Fare();
                                                                }
                                                                result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                                result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                            }

                                                            //Child Count
                                                            else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                            {
                                                                if (result.FareBreakdown[1] == null)
                                                                {
                                                                    result.FareBreakdown[1] = new Fare();
                                                                }
                                                                result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                                result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                            }

                                                            if (priceResponse != null)
                                                            {
                                                                //We need to combine fares of all the segments from the price itenary response

                                                                foreach (PaxFare pfare in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares)
                                                                {
                                                                    if (passengerType == pfare.PaxType)
                                                                    {
                                                                        foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                                                        {
                                                                            switch (passengerType)
                                                                            {
                                                                                case "ADT":
                                                                                    switch (charge.ChargeType)
                                                                                    {
                                                                                        case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                            double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                                            result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                            result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                            result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                            result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                                            break;
                                                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                        case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                        case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                        case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                            double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[0].PassengerCount);
                                                                                            result.FareBreakdown[0].TotalFare += tax;
                                                                                            result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                                            result.Price.Tax += (decimal)tax;
                                                                                            result.Price.SupplierPrice += charge.Amount;

                                                                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                            //{
                                                                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                            //}
                                                                                            //else
                                                                                            //{
                                                                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                            //}

                                                                                            adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                                                            break;
                                                                                    }
                                                                                    break;

                                                                                case "CHD":
                                                                                    switch (charge.ChargeType)
                                                                                    {
                                                                                        case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                            double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                            result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                            result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                            result.Price.PublishedFare += (decimal)baseFareChd;
                                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                                            break;
                                                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                        case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                        case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                        case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                            double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                                            result.FareBreakdown[1].TotalFare += tax;
                                                                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                                            result.Price.Tax += (decimal)tax;
                                                                                            result.Price.SupplierPrice += charge.Amount;

                                                                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                                            //{
                                                                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                                            //}
                                                                                            //else
                                                                                            //{
                                                                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                                            //}


                                                                                            chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                                            break;
                                                                                    }
                                                                                    break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }                                                       
                                                    }
                                                    if (request.InfantCount > 0 && priceResponse != null)
                                                    {
                                                        foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[1].ServiceCharges)
                                                        {
                                                            if (request.ChildCount > 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                                                        double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                        result.FareBreakdown[2].TotalFare += baseFare;
                                                                        result.FareBreakdown[2].BaseFare += baseFare;
                                                                        result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                        result.Price.PublishedFare += (decimal)baseFare;
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                                                        result.FareBreakdown[2].TotalFare += tax;
                                                                        result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                                                        result.Price.Tax += (decimal)tax;
                                                                        result.Price.SupplierPrice += charge.Amount;

                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                        //{
                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                        //}
                                                                        //else
                                                                        //{
                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                        //}


                                                                        inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                        break;
                                                                }
                                                            }
                                                            else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                        double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                        result.FareBreakdown[1].BaseFare += baseFare;
                                                                        result.FareBreakdown[1].TotalFare += baseFare;
                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                        result.Price.PublishedFare += (decimal)baseFare;
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                        result.Price.Tax += (decimal)tax;
                                                                        result.Price.SupplierPrice += charge.Amount;

                                                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                        //{
                                                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                        //}
                                                                        //else
                                                                        //{
                                                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                        //}


                                                                        inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }

                                                    //if (paxTaxBreakUp.Count > 0)
                                                    //{
                                                    //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                                                    //}

                                                    if (adtPaxTaxBreakUp.Count > 0)
                                                    {
                                                        paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                                                    }
                                                    if (chdPaxTaxBreakUp.Count > 0)
                                                    {
                                                        paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                                                    }
                                                    if (inftPaxTaxBreakUp.Count > 0)
                                                    {
                                                        paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                                                    }
                                                    if (paxTypeTaxBreakUp.Count > 0)
                                                    {
                                                        result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                                                    }


                                                    result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                   
                                                    result.Price.SupplierCurrency = currencyCode;
                                                    result.BaseFare = (double)result.Price.PublishedFare;
                                                    result.Tax = (double)result.Price.Tax;
                                                    result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax), agentDecimalValue));
                                                    ResultList.Add(result);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else //Connecting flight results
                            {
                                if (OnwardJourneys[i].Segments.Length > 1 && ReturnJourneys[j].Segments.Length > 1)
                                {
                                    List<SearchResult> conFlightsResults = GetConnectingFlightsCombinationsResults(OnwardJourneys[i], ReturnJourneys[j], fareBreakDownCount);
                                    if (conFlightsResults.Count > 0)
                                    {
                                        ResultList.AddRange(conFlightsResults);
                                    }
                                }
                            }
                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get results." + ex.ToString(), "");
                eventFlag[(int)eventNumber].Set();
            }
        }

        /// <summary>
        /// Returns the Fare Type for a Product Class 
        /// </summary>
        /// <param name="productClass"></param>
        /// <returns></returns>
        private string GetFareTypeForProductClass(string productClass)
        {
            string fareType = "";
            switch (productClass)
            {
                case "RS":
                    fareType = "Regular Fare";
                    break;
                case "XA":
                    fareType = "Special Return Trip";
                    break;
                case "HF":
                    fareType = "SpiceFlex Fare";
                    break;
                case "XB":
                    fareType = "Family & Friends";
                    break;
                case "HO":
                    fareType = "HandBaggage Only";
                    break;
                case "SS":
                    fareType = "Regular Fare (Sale)";
                    break;
                case "NF":
                    fareType = "Advance Purchase";
                    break;
                case "RM":  //Added by brahmam 22.07.2016
                    fareType = "Coupon Fare";
                    break;
                case "CN":
                    fareType = "Promo Coupon";//Added by Shiva 22-Sep-2016 for Price difference
                    break;
                case "CP":
                    fareType = "Corporate Fare";
                    break;
            }
            return fareType;
        }

        /// <summary>
        /// Connecting Flights Get price itenary type
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="journey"></param>
        /// <param name="onwardFare"></param>
        /// <returns></returns>

        protected PriceItineraryResponse GetItineraryPrice(SearchRequest request, string journeySellKey, string fareSellKey, Journey journey, string onwardFare, string fareSellKeyRet, string journeySellKeyRet, Journey ReturnJouney)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            //string jkey = journeySellKey;

            //string journeykey = jkey.Replace("~", "_").Replace(" ", "_");
            //journeykey = journeykey.Replace("/", "_");
            //journeykey = journeykey.Replace(":", "_");
            //if (pricedItineraries.ContainsKey(journeykey + "|" + fareSellKey))
            //{
            //    piResponse = pricedItineraries[journeykey + "|" + fareSellKey] as PriceItineraryResponse;
            //}
            //else
            {
                IBookingManager bookingAPI = this.bookingAPI;

                int paxCount = request.AdultCount + request.ChildCount;

                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = loginSignature;
                priceItinRequest.ContractVersion = 340;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(paxCount + request.InfantCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = fareSellKeyRet;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = journeySellKeyRet;
                for (int i = 0; i < request.AdultCount; i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                }
                for (int i = request.AdultCount; i < (paxCount); i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                }
                if (request.InfantCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = currencyCode;
                    int segcount = journey.Segments.Length + ReturnJouney.Segments.Length;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[segcount];
                    int f = -1;
                    for (int m = 0; m < journey.Segments.Length; m++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m] = new SegmentSSRRequest();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].ArrivalStation = journey.Segments[m].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].DepartureStation = journey.Segments[m].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].FlightDesignator = journey.Segments[m].FlightDesignator;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].STD = journey.Segments[m].STD;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs = new PaxSSR[request.InfantCount];
                        for (int i = 0; i < request.InfantCount; i++)
                        {

                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i] = new PaxSSR();
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ActionStatusCode = "NN";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ArrivalStation = journey.Segments[m].ArrivalStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].DepartureStation = journey.Segments[m].DepartureStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].PassengerNumber = (Int16)i;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRCode = "INFT";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRNumber = 0;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRValue = 0;
                        }
                        f++;
                    }

                    for (int d = 0; d < ReturnJouney.Segments.Length; d++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1] = new SegmentSSRRequest();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].ArrivalStation = ReturnJouney.Segments[d].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].DepartureStation = ReturnJouney.Segments[d].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].FlightDesignator = ReturnJouney.Segments[d].FlightDesignator;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].STD = ReturnJouney.Segments[d].STD;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs = new PaxSSR[request.InfantCount];
                        for (int i = 0; i < request.InfantCount; i++)
                        {

                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i] = new PaxSSR();
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].ActionStatusCode = "NN";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].ArrivalStation = ReturnJouney.Segments[d].ArrivalStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].DepartureStation = ReturnJouney.Segments[d].DepartureStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].PassengerNumber = (Int16)i;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRCode = "INFT";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRNumber = 0;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRValue = 0;
                        }
                        f++;
                    }
                }
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceRequest_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";

                    //string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceRequest_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, priceItinRequest);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }


                try
                {
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                    //pricedItineraries.Add(journeykey + "|" + fareSellKey, piResponse);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceResponse_"+onwardFare+ "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        //string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceResponse_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "__" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                }
            }

            return piResponse;
        }



        /// <summary>
        /// Returns the fare breakup for the segment for One way only
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        /// 

        protected PriceItineraryResponse GetItineraryPrice(SearchRequest request, string journeySellKey, string fareSellKey, Journey journey, string onwardFare)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            //string jkey = journeySellKey;

            //string journeykey = jkey.Replace("~", "_").Replace(" ", "_");
            //journeykey = journeykey.Replace("/", "_");
            //journeykey = journeykey.Replace(":", "_");
            //if (pricedItineraries.ContainsKey(journeykey + "|" + fareSellKey))
            //{
            //    piResponse = pricedItineraries[journeykey + "|" + fareSellKey] as PriceItineraryResponse;
            //}
            //else
            {
                IBookingManager bookingAPI = this.bookingAPI;

                int paxCount = request.AdultCount + request.ChildCount;

                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = loginSignature;
                priceItinRequest.ContractVersion = 340;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(paxCount + request.InfantCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;


                for (int i = 0; i < request.AdultCount; i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                }
                for (int i = request.AdultCount; i < (paxCount); i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                }
                if (request.InfantCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = currencyCode;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[journey.Segments.Length];

                    for (int m = 0; m < journey.Segments.Length; m++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m] = new SegmentSSRRequest();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].ArrivalStation = journey.Segments[m].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].DepartureStation = journey.Segments[m].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].FlightDesignator = journey.Segments[m].FlightDesignator;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].STD = journey.Segments[m].STD;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs = new PaxSSR[request.InfantCount];
                        for (int i = 0; i < request.InfantCount; i++)
                        {
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i] = new PaxSSR();
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ActionStatusCode = "NN";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ArrivalStation = journey.Segments[m].ArrivalStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].DepartureStation = journey.Segments[m].DepartureStation;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].PassengerNumber = (Int16)i;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRCode = "INFT";
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRNumber = 0;
                            priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRValue = 0;
                        }
                    }
                }
                

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceRequest_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";

                    //string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceRequest_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, priceItinRequest);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }


                try
                {
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                    //pricedItineraries.Add(journeykey + "|" + fareSellKey, piResponse);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceResponse_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        //string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceResponse_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "__" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                }
            }

            return piResponse;
        }

        /// <summary>
        /// Returns the combined fare breakup for Round Trip segments. Some sectors may attract GST Tax and in order
        /// to retrieve those taxes we need to combine fare sell keys.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="journeySellKeyRet"></param>
        /// <param name="fareSellKeyRet"></param>
        /// <param name="onSegment"></param>
        /// <param name="retSegment"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPrice(SearchRequest request, string journeySellKey, string fareSellKey, string journeySellKeyRet, string fareSellKeyRet, Segment onSegment, Segment retSegment, string onwardFare, string returnFare)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            try
            {
                //string jkey = journeySellKey;

                //string journeykey = jkey.Replace("SG", "").Replace(DateTime.Now.Year.ToString(), "").Replace("~", "").Replace(" ", "_");
                //journeykey = journeykey.Replace("/", "_");
                //journeykey = journeykey.Replace(":", "_");
                //journeykey += "_#_" + journeySellKeyRet.Replace("SG", "").Replace(DateTime.Now.Year.ToString(), "").Replace("~", "").Replace(" ", "_").Replace("/", "_").Replace(":", "_");
                //if (pricedItineraries.ContainsKey(journeykey + "|" + fareSellKey + "|" + fareSellKeyRet))
                //{
                //    piResponse = pricedItineraries[journeykey + "|" + fareSellKey + "|" + fareSellKeyRet] as PriceItineraryResponse;
                //}
                //else
                {
                    IBookingManager bookingAPI = this.bookingAPI;

                    int paxCount = request.AdultCount + request.ChildCount;

                    PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                    priceItinRequest.Signature = loginSignature;
                    priceItinRequest.ContractVersion = 340;
                    priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                    priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(paxCount + request.InfantCount);
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = fareSellKeyRet;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = journeySellKeyRet;


                    for (int i = 0; i < request.AdultCount; i++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                    }
                    for (int i = request.AdultCount; i < (paxCount); i++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                    }
                    priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = currencyCode;
                    List<SegmentSSRRequest> infantSSrRequests = new List<SegmentSSRRequest>();

                    SegmentSSRRequest segmentSSRRequest = new SegmentSSRRequest();
                    segmentSSRRequest.ArrivalStation = onSegment.ArrivalStation;
                    segmentSSRRequest.DepartureStation = onSegment.DepartureStation;
                    segmentSSRRequest.FlightDesignator = onSegment.FlightDesignator;
                    segmentSSRRequest.STD = onSegment.STD;
                    segmentSSRRequest.PaxSSRs = new PaxSSR[request.InfantCount];
                    for (int i = 0; i < request.InfantCount; i++)
                    {
                        segmentSSRRequest.PaxSSRs[i] = new PaxSSR();
                        segmentSSRRequest.PaxSSRs[i].ActionStatusCode = "NN";
                        segmentSSRRequest.PaxSSRs[i].ArrivalStation = onSegment.ArrivalStation;
                        segmentSSRRequest.PaxSSRs[i].DepartureStation = onSegment.DepartureStation;
                        segmentSSRRequest.PaxSSRs[i].PassengerNumber = (Int16)i;
                        segmentSSRRequest.PaxSSRs[i].SSRCode = "INFT";
                        segmentSSRRequest.PaxSSRs[i].SSRNumber = 0;
                        segmentSSRRequest.PaxSSRs[i].SSRValue = 0;
                        infantSSrRequests.Add(segmentSSRRequest);
                    }
                    segmentSSRRequest = new SegmentSSRRequest();
                    segmentSSRRequest.ArrivalStation = retSegment.ArrivalStation;
                    segmentSSRRequest.DepartureStation = retSegment.DepartureStation;
                    segmentSSRRequest.FlightDesignator = retSegment.FlightDesignator;
                    segmentSSRRequest.STD = retSegment.STD;
                    segmentSSRRequest.PaxSSRs = new PaxSSR[request.InfantCount];
                    for (int i = 0; i < request.InfantCount; i++)
                    {
                        segmentSSRRequest.PaxSSRs[i] = new PaxSSR();
                        segmentSSRRequest.PaxSSRs[i].ActionStatusCode = "NN";
                        segmentSSRRequest.PaxSSRs[i].ArrivalStation = retSegment.ArrivalStation;
                        segmentSSRRequest.PaxSSRs[i].DepartureStation = retSegment.DepartureStation;
                        segmentSSRRequest.PaxSSRs[i].PassengerNumber = (Int16)i;
                        segmentSSRRequest.PaxSSRs[i].SSRCode = "INFT";
                        segmentSSRRequest.PaxSSRs[i].SSRNumber = 0;
                        segmentSSRRequest.PaxSSRs[i].SSRValue = 0;
                        infantSSrRequests.Add(segmentSSRRequest);
                    }
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = infantSSrRequests.ToArray();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceRequest_" + onwardFare + "_" + returnFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";

                        // string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceRequest_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "#" + fareSellKeyRet.Replace("~", "_") + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, priceItinRequest);
                        sw.Close();
                    }
                    catch { }



                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                    //if (!pricedItineraries.ContainsKey(journeykey + "|" + fareSellKey + "|" + fareSellKeyRet))
                    //{
                    //    pricedItineraries.Add(journeykey + "|" + fareSellKey + "|" + fareSellKeyRet, piResponse);
                    //}
                    System.Threading.Thread.Sleep(200);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));

                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceResponse_" + onwardFare + "_" + returnFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";

                        //string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetItineraryPriceResponse_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "#" + fareSellKeyRet.Replace("~", "_") + "__" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                piResponse = null;
            }
            return piResponse;
        }
        /// <summary>
        /// Returns the baggage details from the DB Table. BKE_Source_Baggage_Info
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetBaggage(SearchResult result)
        {
            DataTable dtSourceBaggage = SourceBaggage.GetSourceBaggages(BookingSource.SpiceJet);

            rateOfExchange = exchangeRates[dtSourceBaggage.Rows[0]["BaggageCurrency"].ToString()];

            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    
                        DataRow[] baggages = new DataRow[0];

                        if (result.Flights[0][0].Origin.CountryCode == "IN" && result.Flights[0][result.Flights[0].Length - 1].Destination.CountryCode == "IN")
                        {
                            baggages = dtSourceBaggage.Select("IsDomestic=1");
                        }
                        else
                        {
                            baggages = dtSourceBaggage.Select("IsDomestic=0");
                        }
                        if (!result.Flights[i][j].SegmentFareType.Contains("HandBaggage"))
                        {
                            foreach (DataRow row in baggages)
                            {
                                DataRow dr = dtBaggage.NewRow();
                                dr["Code"] = row["BaggageCode"];
                                dr["Price"] = Math.Round(Convert.ToDecimal(row["BaggagePrice"]) * rateOfExchange, agentDecimalValue);
                                dr["Group"] = result.Flights[i][j].Group;
                                dr["Currency"] = agentBaseCurrency;
                                switch (row["BaggageCode"].ToString())
                                {
                                    case "EB05":
                                        dr["Description"] = "5Kg Excess Baggage";
                                        break;
                                    case "EB10":
                                        dr["Description"] = "10Kg Excess Baggage";
                                        break;
                                    case "EB15":
                                        dr["Description"] = "15Kg Excess Baggage";
                                        break;
                                    case "EB20":
                                        dr["Description"] = "20Kg Excess Baggage";
                                        break;
                                    case "EB30":
                                        dr["Description"] = "30Kg Excess Baggage";
                                        break;
                                }
                                dr["QtyAvailable"] = 5000;
                                dtBaggage.Rows.Add(dr);
                            }
                        }
                    
                  
                }
            }

            dtBaggage.DefaultView.Sort = "Code ASC,Group ASC";

            return dtBaggage;

        }


        /// <summary>
        /// Depricated (Not in use Now)
        /// </summary>
        /// <param name="result"></param>
        /// <param name="request"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData GetInfantFares(SearchRequest request, Journey journey, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequestData requestData = new SellRequestData();

                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[journey.Segments.Length];

                for (int i = 0; i < journey.Segments.Length; i++)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = journey.Segments[i].ArrivalStation;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = journey.Segments[i].DepartureStation;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = journey.Segments[i].FlightDesignator.CarrierCode.Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = journey.Segments[i].FlightDesignator.FlightNumber.Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.OpSuffix = journey.Segments[i].FlightDesignator.OpSuffix;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = journey.Segments[i].STD;

                    for (int j = 0; j < request.InfantCount; j++)
                    {
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[request.InfantCount];
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j] = new PaxSSR();
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].State = MessageState.Modified;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].ActionStatusCode = "NN";
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].ArrivalStation = journey.Segments[i].ArrivalStation;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].DepartureStation = journey.Segments[i].DepartureStation;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].PassengerNumber = (Int16)j;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].SSRCode = "INFT";
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].SSRValue = 0;
                    }
                }

                //New object for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSSRInfantRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }

              
                responseData = bookingAPI.Sell(contractVersion, signature, requestData);

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSSRInfantResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }

        /// <summary>
        /// Hold booking before ticket
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        public BookingUpdateResponseData Book(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                //LogonResponse loginResponse = Login();
                SellRequestData requestData = new SellRequestData();

                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;

                int onwardSegCount = 0, returnSegCount = 0;

                foreach (FlightInfo seg in itinerary.Segments)
                {
                    if (seg.Group == 0)
                    {
                        onwardSegCount++;
                    }
                    if (seg.Group == 1)
                    {
                        returnSegCount++;
                    }
                }

                //Onward journey Connecting Flights 
                if (returnSegCount == 0 && onwardSegCount > 1)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = itinerary.Segments[0].UapiSegmentRefKey;
                    //Combine the journey sell keys for the connecting flights
                    for (int i = 1; i < itinerary.Segments.Length; i++)
                    {
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey += "^" + itinerary.Segments[i].UapiSegmentRefKey;
                    }
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";
                }               
                else if (onwardSegCount > 1 && returnSegCount > 1)//Return connection booking
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                    string returnJourneySellKey = string.Empty;
                    string onwardjourneySellKey = string.Empty;

                    //Combine the journey sell keys for the connecting flights
                    for (int i = 0; i < itinerary.Segments.Length; i++)
                    {
                        if (itinerary.Segments[i].Group == 0)
                        {
                            if (onwardjourneySellKey.Length > 0)
                            {
                                onwardjourneySellKey += "^" + itinerary.Segments[i].UapiSegmentRefKey;
                            }
                            else
                            {
                                onwardjourneySellKey = itinerary.Segments[i].UapiSegmentRefKey;
                            }
                        }
                        else
                        {
                            if (returnJourneySellKey.Length > 0)
                            {
                                returnJourneySellKey += "^" + itinerary.Segments[i].UapiSegmentRefKey;
                            }
                            else
                            {
                                returnJourneySellKey = itinerary.Segments[i].UapiSegmentRefKey;
                            }
                        }
                    }

                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardjourneySellKey;

                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;

                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];

                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                else//one way or return direct flight booking
                {
                    //This condition works when Onward Journey is direct flight.
                    //                        (or)
                    //When both onward and return journeys are direct flights.

                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[itinerary.Segments.Length];

                    for (int i = 0; i < itinerary.Segments.Length; i++)
                    {
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[i] = new SellKeyList();
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[i].JourneySellKey = itinerary.Segments[i].UapiSegmentRefKey;
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[i].FareSellKey = itinerary.TicketAdvisory.Split('|')[i];
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[i].StandbyPriorityCode = "";
                    }
                }

                //int paxCount = itinerary.Passenger.Length;
                //requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = new PaxPriceType[paxCount];
                //requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxCount;
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        switch (itinerary.Passenger[i].Type)
                        {
                            case PassengerType.Adult:
                                priceType.PaxType = "ADT";
                                break;
                            case PassengerType.Child:
                                priceType.PaxType = "CHD";
                                break;
                            //case PassengerType.Infant:
                            //    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType[i].PaxType = "INFT";
                            //    break;
                        }
                        paxPriceTypes.Add(priceType);
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;


                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;


                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }

                
                responseData = bookingAPI.Sell(contractVersion, signature, requestData);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }

        /// <summary>
        /// Confirmation of Additional Baggage
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingUpdateResponseData BookBaggage(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];
                int counter = 0, baggageCount = 0;
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "SG";
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber.Replace("SG", "").Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                    List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                    for (int j = 0; j < itinerary.Passenger.Length; j++)
                    {
                        if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                        {
                            if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.State = MessageState.New;
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                //paxSSR.FeeCode = "";
                                //paxSSR.Note = "";
                                paxSSR.PassengerNumber = (Int16)j;
                                if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1)//Return
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[itinerary.Segments[i].Group];
                                }
                                else
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType;//Oneway
                                }
                                //paxSSR.SSRDetail = "";
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                if (paxSSR.SSRCode.Length > 0)
                                {
                                    PaxSSRs.Add(paxSSR);
                                }
                                baggageCount++;
                            }

                        }
                    }


                    for (int j = 0; j < itinerary.Passenger.Length; j++)
                    {
                        if (itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0 && itinerary.Passenger[j].Type != PassengerType.Infant)
                        {
                            //if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType.Length > 0)
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.State = MessageState.New;
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                paxSSR.PassengerNumber = (Int16)j;
                                if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && itinerary.Segments[i].Group ==1)//Return
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[itinerary.Segments[i].Group];
                                }
                                else if(itinerary.Segments[i].Group == 0)
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[0];//Oneway
                                }
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                {
                                    PaxSSRs.Add(paxSSR);
                                }
                                baggageCount++;
                            }

                        }
                    }

                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                    counter++;
                }

                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;


                //If Excess baggage is selected then book it otherwise skip this operation
                if (baggageCount > 0)
                {
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGBookBaggageRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, sellRequestObj);
                        sw.Close();
                    }
                    catch { }

                    responseData = bookingAPI.Sell(contractVersion, signature, requestData);

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGBookBaggageResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, responseData);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    responseData = null;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to book Baggage. Reason : " + ex.ToString(), "");
            }

            //Logout(loginResponse.Signature);

            return responseData;
        }

        /// <summary>
        /// Combining Infant fare for the Booking.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        public decimal SellSSRForInfant(FlightItinerary itinerary, string signature)
        {
            decimal bookingAmount = 0;
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];

                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "SG";
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber.Replace("SG", "").Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                    //requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[itinerary.Passenger.Length];
                    //if (i == 0)
                    {
                        int counter = 0;
                        List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                        for (int j = 0; j < itinerary.Passenger.Length; j++)
                        {
                            if (itinerary.Passenger[j].Type == PassengerType.Infant)
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                paxSSR.FeeCode = "";
                                paxSSR.Note = "";
                                paxSSR.PassengerNumber = (Int16)counter;
                                paxSSR.SSRCode = "INFT";
                                paxSSR.SSRDetail = "";
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                counter++;
                                PaxSSRs.Add(paxSSR);
                            }
                        }
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                    }
                }
                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;


                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGBookInfantRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }

                responseData = bookingAPI.Sell(contractVersion, signature, requestData);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGBookInfantResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }
                bookingAmount += responseData.Success.PNRAmount.TotalCost;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to book Baggage. Reason : " + ex.ToString(), "");
            }

            //Logout(loginResponse.Signature);

            return bookingAmount;
        }

        /// <summary>
        /// Confirmation of Total Price
        /// </summary>
        /// <param name="bookingAmount"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private AddPaymentToBookingResponseData AddPaymentForBooking(decimal bookingAmount, string signature)
        {
            AddPaymentToBookingResponseData response = new AddPaymentToBookingResponseData();

            try
            {
                AddPaymentToBookingRequestData request = new AddPaymentToBookingRequestData();
                request.AccountNumber = agentId;
                request.AccountNumberID = 0;
                request.AuthorizationCode = "";
                request.Deposit = false;
                request.Expiration = new DateTime();
                request.Installments = 0;
                request.MessageState = MessageState.New;
                request.ParentPaymentID = 0;
                request.PaymentAddresses = new PaymentAddress[0];
                request.PaymentFields = new PaymentField[0];
                request.PaymentMethodCode = "AG";
                request.PaymentMethodType = RequestPaymentMethodType.AgencyAccount;
                request.PaymentText = "";
                request.QuotedAmount = bookingAmount;
                request.QuotedCurrencyCode = currencyCode;
                request.ReferenceType = PaymentReferenceType.Default;
                request.Status = BookingPaymentStatus.New;
                request.ThreeDSecureRequest = new ThreeDSecureRequest();
                request.WaiveFee = false;

                //New object creation for signature inclusion.
                AddPaymentToBookingRequest addPaymentToBookingObj = new AddPaymentToBookingRequest();
                addPaymentToBookingObj.addPaymentToBookingReqData = request;
                addPaymentToBookingObj.ContractVersion = contractVersion;
                addPaymentToBookingObj.Signature = signature;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGAddPaymentToBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, addPaymentToBookingObj);
                    sw.Close();
                }
                catch { }

                response = bookingAPI.AddPaymentToBooking(contractVersion, signature, request);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGAddPaymentToBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Add Payment for booking. Reason : " + ex.ToString(), "");
            }

            //Logout(loginResponse.Signature);

            return response;
        }

        /// <summary>
        /// Commit the booking and generate PNR.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse BookItinerary(ref FlightItinerary itinerary)
        {
            BookingResponse response = new BookingResponse();


            LogonResponse loginResponse = Login();

            BookingUpdateResponseData bookResponse = Book(itinerary, loginResponse.Signature);

            int infantCount = 0;
            foreach (FlightPassenger pax in itinerary.Passenger)
            {
                if (pax.Type == PassengerType.Infant)
                {
                    infantCount += 1;
                }
            }
            BookingUpdateResponseData bagResponse = BookBaggage(itinerary, loginResponse.Signature);

            if (bagResponse != null)
            {
                bookResponse = bagResponse;
            }

            decimal bookingAmount = 0;
            if (infantCount > 0)
            {
                //SellSSR by Infant
                bookingAmount = SellSSRForInfant(itinerary, loginResponse.Signature);
            }
            else
            {
                bookingAmount = bookResponse.Success.PNRAmount.TotalCost;
            }

            BookingUpdateResponseData updatePaxResponse = UpdatePassengers(itinerary, loginResponse.Signature);//Added by Lokesh on 27-Feb-2018 .
            if (updatePaxResponse != null && updatePaxResponse.Success != null && updatePaxResponse.Success.PNRAmount.TotalCost > 0)
            {

                AddPaymentForBooking(bookingAmount, loginResponse.Signature);
             
                try
                {
                    BookingUpdateResponseData responseData = new BookingUpdateResponseData();

                    BookingCommitRequestData request = new BookingCommitRequestData();
                    request.BookingChangeCode = "";
                    request.BookingComments = new BookingComment[0];

                    request.BookingID = 0;
                    request.BookingParentID = 0;
                    request.ChangeHoldDateTime = false;
                    request.CurrencyCode = currencyCode;
                    request.DistributeToContacts = false;
                    request.GroupName = "";
                    request.NumericRecordLocator = "";
                    request.ParentRecordLocator = "";
                    request.RecordLocator = "";
                    request.RecordLocators = new RecordLocator[0];
                    request.RestrictionOverride = false;

                    request.State = MessageState.New;
                    request.SystemCode = "";
                    request.WaiveNameChangeFee = false;
                    request.WaivePenaltyFee = false;
                    request.WaiveSpoilageFee = false;
                   // request.Passengers = new Passenger[itinerary.Passenger.Length - infantCount];
                    request.PaxCount = (Int16)(itinerary.Passenger.Length - infantCount);

                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        FlightPassenger pax = itinerary.Passenger[i];
                        if (pax.Type != PassengerType.Infant)
                        {
                          //  request.Passengers[i] = new Passenger();
                            if (pax.IsLeadPax)
                            {
                                request.BookingContacts = new BookingContact[1];
                                request.BookingContacts[0] = new BookingContact();
                                request.BookingContacts[0].AddressLine1 = (string.IsNullOrEmpty(pax.AddressLine1) ? "Address Line1" : pax.AddressLine1);
                                request.BookingContacts[0].AddressLine2 = pax.AddressLine2;
                                request.BookingContacts[0].AddressLine3 = "";
                                request.BookingContacts[0].City = "SHJ";
                                request.BookingContacts[0].CompanyName = "Cozmo Travel";
                                if (pax.Country != null && !string.IsNullOrEmpty(pax.Country.CountryCode))
                                {
                                    request.BookingContacts[0].CountryCode = pax.Country.CountryCode;
                                }
                                else
                                {
                                   request.BookingContacts[0].CountryCode = "AE";
                                }
                                request.BookingContacts[0].CultureCode = "";
                               request.BookingContacts[0].CustomerNumber = "";
                               request.BookingContacts[0].DistributionOption = DistributionOption.Email;
                               request.BookingContacts[0].EmailAddress = pax.Email;
                                request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com";

                                if (!string.IsNullOrEmpty(pax.Email))
                                {
                                    request.BookingContacts[0].EmailAddress = pax.Email; //to get ticket voucher emails from SpiceJet
                                }
                                else
                                {
                                   request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com";
                                }
                               request.BookingContacts[0].Fax = "";
                                request.BookingContacts[0].HomePhone = pax.CellPhone;
                                request.BookingContacts[0].WorkPhone = pax.CellPhone;
                                request.BookingContacts[0].Names = new BookingName[1];
                               request.BookingContacts[0].Names[0] = new BookingName();
                               request.BookingContacts[0].Names[0].FirstName = pax.FirstName;
                               request.BookingContacts[0].Names[0].LastName = pax.LastName;
                                request.BookingContacts[0].Names[0].MiddleName = "";
                              // request.BookingContacts[0].Names[0].State = MessageState.Modified;
                               request.BookingContacts[0].Names[0].Suffix = "";
                                request.BookingContacts[0].Names[0].Title = pax.Title;
                               request.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                                //Added by Lokesh on 4-Oct-2017 Regarding Lead Pax Destination Phone Number.
                               if (!string.IsNullOrEmpty(pax.DestinationPhone))
                                {
                                    request.BookingContacts[0].OtherPhone = pax.DestinationPhone;
                                }
                                else
                                {
                                    request.BookingContacts[0].OtherPhone = "";
                                }
                                request.BookingContacts[0].PostalCode = "3093";
                                request.BookingContacts[0].ProvinceState = "SHJ";
                                request.BookingContacts[0].SourceOrganization = "";
                               request.BookingContacts[0].State = MessageState.Modified;
                               request.BookingContacts[0].TypeCode = "P";
                                request.BookingContacts[0].WorkPhone = pax.CellPhone;
                            }

                           // request.Passengers[i].CustomerNumber = "";
                          //  request.Passengers[i].FamilyNumber = (Int16)i;

                          //  request.Passengers[i].Names = new BookingName[1];
                          //  request.Passengers[i].Names[0] = new BookingName();
                         //   request.Passengers[i].Names[0].FirstName = pax.FirstName;
                          //  request.Passengers[i].Names[0].LastName = pax.LastName;
                         //   request.Passengers[i].Names[0].MiddleName = "";
                         //   request.Passengers[i].Names[0].State = MessageState.New;
                         //   request.Passengers[i].Names[0].Suffix = "";
                         //   request.Passengers[i].Names[0].Title = pax.Title;
                         //   request.Passengers[i].PassengerAddresses = new PassengerAddress[0];
                            //request.Passengers[i].PassengerBags = new PassengerBag[0];
                            //request.Passengers[i].PassengerFees = new PassengerFee[0];
                          //  request.Passengers[i].PassengerID = 0;
                            //request.Passengers[i].PassengerInfants = new PassengerInfant[0];
                            //request.Passengers[i].PassengerInfo = new PassengerInfo();
                            //request.Passengers[i].PassengerInfos = new PassengerInfo[0];
                         //   request.Passengers[i].PassengerNumber = (Int16)i;
                            //request.Passengers[i].PassengerProgram = new PassengerProgram();
                            //request.Passengers[i].PassengerPrograms = new PassengerProgram[0];
                            //request.Passengers[i].PassengerTravelDocuments = new PassengerTravelDocument[0];
                            //request.Passengers[i].PassengerTypeInfos = new PassengerTypeInfo[0];
                          //  request.Passengers[i].PaxDiscountCode = "";
                          //  request.Passengers[i].PseudoPassenger = false;
                           // request.Passengers[i].State = MessageState.Modified;

                            //Assign each adult to each infant.

                            //request.Passengers[i].PassengerInfants = new PassengerInfant[0];
                            //request.Passengers[i].PassengerInfants[i] = new PassengerInfant();
                            //request.Passengers[i].PassengerInfants[i].DOB = pax.DateOfBirth;
                            //if (pax.Gender == Gender.Male)
                            //{
                            //    request.Passengers[i].PassengerInfants[i].Gender = SpiceJet.SGBooking.Gender.Male;
                            //}
                            //else if (pax.Gender == Gender.Female)
                            //{
                            //    request.Passengers[i].PassengerInfants[i].Gender = SpiceJet.SGBooking.Gender.Female;
                            //}
                            //request.Passengers[i].PassengerInfants[i].Names = new BookingName[1];
                            //request.Passengers[i].PassengerInfants[i].Names[0] = new BookingName();
                            //request.Passengers[i].PassengerInfants[i].Names[0].FirstName = pax.FirstName;
                            //request.Passengers[i].PassengerInfants[i].Names[0].LastName = pax.LastName;
                            //request.Passengers[i].PassengerInfants[i].Names[0].MiddleName = "";
                            //request.Passengers[i].PassengerInfants[i].Names[0].State = MessageState.New;
                            //request.Passengers[i].PassengerInfants[i].Names[0].Suffix = "";
                            //request.Passengers[i].PassengerInfants[i].Names[0].Title = pax.Title;
                            //request.Passengers[i].PassengerInfants[i].Nationality = pax.Nationality.CountryCode;
                            //request.Passengers[i].PassengerInfants[i].ResidentCountry = pax.Country.CountryCode;
                            //request.Passengers[i].PassengerInfants[i].State = MessageState.New;



                        }
                    }

                    //List<PassengerInfant> paxInfants = new List<PassengerInfant>();
                    //for (int j = itinerary.Passenger.Length - infantCount; j < itinerary.Passenger.Length; j++)
                    //{
                    //    int counter = 0;
                    //    FlightPassenger pax = itinerary.Passenger[j];
                    //    request.Passengers[j].PassengerInfants[counter].Gender = SpiceJet.SGBooking.Gender.Male;

                    //}

                    if (infantCount > 0)
                    {
                        int j = 0;
                        int infants = 0;
                        for (int i = 0; i < itinerary.Passenger.Length; i++)
                        {
                            FlightPassenger pax1 = itinerary.Passenger[i];
                            if (pax1.Type == PassengerType.Adult)
                            {
                                //int counter = 0;
                                if (infantCount - infants > 0)
                                {
                                  //  request.Passengers[i].PassengerInfants = new PassengerInfant[1];

                                    if (i == 0)
                                    {
                                        j = itinerary.Passenger.Length - infantCount;
                                    }
                                    //for ( j < itinerary.Passenger.Length;)
                                    //{
                                    FlightPassenger pax = itinerary.Passenger[j];
                                  //  request.Passengers[i].PassengerInfants[counter] = new PassengerInfant();
                                    if (pax.DateOfBirth != DateTime.MinValue)
                                    {
                                       // request.Passengers[i].PassengerInfants[counter].DOB = pax.DateOfBirth;
                                    }
                                    else
                                    {
                                       // request.Passengers[i].PassengerInfants[counter].DOB = DateTime.Today.AddDays(-365);
                                    }
                                    if (pax.Gender == Gender.Male)
                                    {
                                       // request.Passengers[i].PassengerInfants[counter].Gender = SpiceJet.SGBooking.Gender.Male;
                                    }
                                    else if (pax.Gender == Gender.Female)
                                    {
                                       // request.Passengers[i].PassengerInfants[counter].Gender = SpiceJet.SGBooking.Gender.Female;
                                    }
                                 //   request.Passengers[i].PassengerInfants[counter].Names = new BookingName[1];
                                  //  request.Passengers[i].PassengerInfants[counter].Names[0] = new BookingName();
                                 //   request.Passengers[i].PassengerInfants[counter].Names[0].FirstName = pax.FirstName;
                                 //   request.Passengers[i].PassengerInfants[counter].Names[0].LastName = pax.LastName;
                                 //   request.Passengers[i].PassengerInfants[counter].Names[0].MiddleName = "";
                                 //   request.Passengers[i].PassengerInfants[counter].Names[0].State = MessageState.New;
                                  //  request.Passengers[i].PassengerInfants[counter].Names[0].Suffix = "";
                                  //  request.Passengers[i].PassengerInfants[counter].Names[0].Title = pax.Title;
                                    if (pax.Nationality != null)
                                    {
                                        if (!string.IsNullOrEmpty(pax.Nationality.CountryCode))
                                        {
                                            //request.Passengers[i].PassengerInfants[counter].Nationality = pax.Nationality.CountryCode;
                                        }
                                    }
                                    if (pax.Country != null)
                                    {
                                        if (!string.IsNullOrEmpty(pax.Country.CountryCode))
                                        {
                                            //request.Passengers[i].PassengerInfants[counter].ResidentCountry = pax.Country.CountryCode;
                                        }
                                    }
                                   // request.Passengers[i].PassengerInfants[counter].State = MessageState.New;
                                    //counter++;
                                    if ((j + 1) < itinerary.Passenger.Length)
                                    {
                                        j++;
                                    }
                                }
                                infants++;
                                //}
                            }
                        }
                    }

                    //New object for signature inclusion
                    BookingCommitRequest bookingCommitRequestObj = new BookingCommitRequest();
                    bookingCommitRequestObj.BookingCommitRequestData = request;
                    bookingCommitRequestObj.ContractVersion = contractVersion;
                    bookingCommitRequestObj.Signature = loginResponse.Signature;

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(BookingCommitRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGBookingCommitRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, bookingCommitRequestObj);
                        sw.Close();
                    }
                    catch { }

                    responseData = bookingAPI.BookingCommit(contractVersion, loginResponse.Signature, request);

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGBookingCommitResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, responseData);
                        sw.Close();
                    }
                    catch { }

                    if (responseData != null)
                    {
                        if (responseData.Error == null)
                        {
                            response.PNR = responseData.Success.RecordLocator;
                            response.ProdType = ProductType.Flight;
                            response.Error = "";
                            response.Status = BookingResponseStatus.Successful;
                            itinerary.PNR = response.PNR;
                            itinerary.FareType = "Pub";
                        }
                        else
                        {
                            response.Status = BookingResponseStatus.Failed;
                        }
                    }
                }
               
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Commit booking. Reason : " + ex.ToString(), "");
                    response.Error = ex.Message;
                    response.Status = BookingResponseStatus.Failed;
                }
            }
            else//UpdatePassenger failed
            {
                response.Status = BookingResponseStatus.Failed;
            }

            Logout(loginResponse.Signature);

            return response;
        }


        public GetBookingResponse GetBooking(string pnr, string signature)
        {
            GetBookingResponse response = new GetBookingResponse();

            try
            {
                GetBookingRequestData requestData = new GetBookingRequestData();
                requestData.GetBookingBy = GetBookingBy.RecordLocator;
                requestData.GetByRecordLocator = new GetByRecordLocator();
                requestData.GetByRecordLocator.RecordLocator = pnr;


                //New object for signature inclusion
                GetBookingRequest getBookingRequestObj = new GetBookingRequest();
                getBookingRequestObj.ContractVersion = contractVersion;
                getBookingRequestObj.Signature = signature;
                getBookingRequestObj.GetBookingReqData = requestData;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetBookingRequest_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingRequestObj);
                    sw.Close();
                }
                catch { }

                response.Booking = bookingAPI.GetBooking(contractVersion, signature, requestData);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGGetBookingResponse_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to get Booking for PNR : " + pnr + ". Reason : " + ex.ToString(), "");
            }

            return response;
        }

        public Dictionary<string, string> CancelBooking(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();

            LogonResponse loginResponse = Login();

            try
            {
                GetBookingResponse response = GetBooking(itinerary.PNR, loginResponse.Signature);

           

                CancelRequestData requestData = new CancelRequestData();
                requestData.CancelBy = CancelBy.Journey;
                requestData.CancelJourney = new CancelJourney();
                requestData.CancelJourney.CancelJourneyRequest = new CancelJourneyRequest();
                requestData.CancelJourney.CancelJourneyRequest.Journeys = new Journey[response.Booking.Journeys.Length];
                for (int i = 0; i < response.Booking.Journeys.Length; i++)
                {
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i] = new Journey();
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments = new Segment[response.Booking.Journeys[i].Segments.Length];
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].State = MessageState.New;
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].JourneySellKey = response.Booking.Journeys[i].JourneySellKey;
                    for (int j = 0; j < response.Booking.Journeys[i].Segments.Length; j++)
                    {
                        Segment segment = response.Booking.Journeys[i].Segments[j];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j] = new Segment();
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ActionStatusCode = "NN";
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ArrivalStation = segment.ArrivalStation;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = SpiceJet.SGBooking.ChannelType.API;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].DepartureStation = segment.DepartureStation;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares = new SpiceJet.SGBooking.Fare[segment.Fares.Length];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].FlightDesignator = segment.FlightDesignator;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].International = segment.International;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs = new Leg[segment.Legs.Length];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].SegmentSellKey = segment.SegmentSellKey;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STA = segment.STA;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].State = MessageState.New;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STD = segment.STD;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = segment.ChannelType;

                        for (int f = 0; f < response.Booking.Journeys[i].Segments[j].Fares.Length; f++)
                        {
                            SpiceJet.SGBooking.Fare fare = response.Booking.Journeys[i].Segments[j].Fares[f];
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f] = new SpiceJet.SGBooking.Fare();
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].CarrierCode = fare.CarrierCode;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassOfService = fare.ClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassType = fare.ClassType;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareApplicationType = fare.FareApplicationType;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareBasisCode = fare.FareBasisCode;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareClassOfService = fare.FareClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSellKey = fare.FareSellKey;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSequence = fare.FareSequence;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareStatus = FareStatus.Default;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].InboundOutbound = fare.InboundOutbound;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].IsAllotmentMarketFare = false;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].RuleNumber = fare.RuleNumber;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].OriginalClassOfService = fare.OriginalClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].TravelClassCode = fare.TravelClassCode;
                        }
                        for (int l = 0; l < response.Booking.Journeys[i].Segments[j].Legs.Length; l++)
                        {
                            Leg leg = response.Booking.Journeys[i].Segments[j].Legs[l];
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l] = new Leg();
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].ArrivalStation = leg.ArrivalStation;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].DepartureStation = leg.DepartureStation;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].FlightDesignator = leg.FlightDesignator;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].InventoryLegID = leg.InventoryLegID;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].LegInfo = leg.LegInfo;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].OperationsInfo = leg.OperationsInfo;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STA = leg.STA;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].State = MessageState.New;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STD = leg.STD;
                        }
                    }
                    requestData.CancelJourney.CancelJourneyRequest.WaivePenaltyFee = false;
                    requestData.CancelJourney.CancelJourneyRequest.WaiveSpoilageFee = false;
                    requestData.CancelJourney.CancelJourneyRequest.PreventReprice = false;

                }

                //New object creation for signature inclusion.
                CancelRequest cancelRequestObj = new CancelRequest();
                cancelRequestObj.CancelRequestData = requestData;
                cancelRequestObj.ContractVersion = contractVersion;
                cancelRequestObj.Signature = loginResponse.Signature;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(CancelRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGCancelRequest_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, cancelRequestObj);
                    sw.Close();
                }
                catch { }

                BookingUpdateResponseData responseData = bookingAPI.Cancel(contractVersion, loginResponse.Signature, requestData);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGCancelResponse_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }

                if (responseData != null)
                {
                    if (responseData.Error == null)
                    {
                        string charges = responseData.Success.PNRAmount.TotalCost.ToString();
                        if (responseData.Success.PNRAmount.AlternateCurrencyBalanceDue > 0)
                        {
                            charges = responseData.Success.PNRAmount.AlternateCurrencyBalanceDue.ToString().Replace("-", "");
                        }
                        rateOfExchange = (responseData.Success.PNRAmount.AlternateCurrencyCode != null && responseData.Success.PNRAmount.AlternateCurrencyCode.Trim().Length > 0 ? exchangeRates[responseData.Success.PNRAmount.AlternateCurrencyCode] : exchangeRates[currencyCode]);
                        cancellationData.Add("Cancelled", "True");
                        cancellationData.Add("Charges", (Convert.ToDecimal(charges) * rateOfExchange).ToString());
                    }
                    else
                    {
                        cancellationData.Add("Cancelled", "False");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetCancellation, Severity.High, 1, "(SpiceJet)Failed to Cancel booking. Reason : " + ex.ToString(), "");
            }

            Logout(loginResponse.Signature);

            return cancellationData;
        }

        public List<FareRule> GetFareRule(string origin, string dest, string fareBasisCode)
        {
            List<FareRule> fareRules = new List<FareRule>();

            try
            {
                FareRule DNfareRules = new FareRule();
                DNfareRules.Origin = origin;
                DNfareRules.Destination = dest;
                DNfareRules.Airline = "SG";
                DNfareRules.FareRuleDetail = Util.GetLCCFareRules("SG");
                DNfareRules.FareBasisCode = fareBasisCode;

                fareRules.Add(DNfareRules);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJet, Severity.High, 1, "(SpiceJet)Failed to get Fare Rules.Reason" + ex.ToString(), "");
            }

            return fareRules;
        }

        /// <summary>
        /// Gets the connecting flight results for One Way
        /// </summary>
        /// <param name="journeyOnRet"></param>
        /// <param name="fareBreakDownCount"></param>
        /// <returns></returns>
        public List<SearchResult> GetConnectingFlightsResults(Journey journeyOnRet, int fareBreakDownCount)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
           // Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();
            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
             List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            try
            {
                if (journeyOnRet.Segments.Length > 1)
                {
                    string jSellKey = journeyOnRet.Segments[0].Fares[0].FareSellKey;//FirstConnectingSegment
                    //Journey Fare Type used for log naming
                    string connectingFareType = journeyOnRet.Segments[0].FlightDesignator.FlightNumber + "_" + GetFareTypeForProductClass(journeyOnRet.Segments[0].Fares[0].ProductClass).Replace(" ", "_") + Convert.ToString(journeyOnRet.Segments[0].STD).Replace('/' ,'_').Replace(" " , "_").Replace(':' ,'_');
                    string ProductClass = journeyOnRet.Segments[0].Fares[0].ProductClass;
                    //Clubbed Journey Sell Key Formation 
                    for (int i = 1; i < journeyOnRet.Segments.Length; i++)
                    {
                        connectingFareType += "_" + journeyOnRet.Segments[i].FlightDesignator.FlightNumber + "_" + GetFareTypeForProductClass(journeyOnRet.Segments[i].Fares[0].ProductClass).Replace(" ", "_");

                        if (journeyOnRet.Segments[i] != null)
                        {
                            jSellKey += "^" + journeyOnRet.Segments[i].Fares[0].FareSellKey;
                        }
                    }
                    connectingFareType += "_" + Convert.ToString(journeyOnRet.Segments[journeyOnRet.Segments.Length - 1].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');

                    //Gets the PriceItinerary for the entire journey
                    PriceItineraryResponse onResponse = GetItineraryPrice(request, journeyOnRet.JourneySellKey, jSellKey, journeyOnRet, connectingFareType);
                    if (onResponse != null)
                    {
                        rateOfExchange = exchangeRates[onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                       // paxTaxBreakUp.Clear();

                        paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                        adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                        chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                        inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();

                      

                        SearchResult result = new SearchResult();
                        result.IsLCC = true;
                        if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                        {
                            result.ResultBookingSource = BookingSource.SpiceJet;
                        }
                        else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                        {
                            result.ResultBookingSource = BookingSource.SpiceJetCorp;
                        }
                        result.Airline = "SG";
                        result.Currency = agentBaseCurrency;
                        result.EticketEligible = true;
                        result.FareBreakdown = new Fare[fareBreakDownCount];
                        result.FareRules = new List<FareRule>();
                        result.JourneySellKey = journeyOnRet.JourneySellKey;
                        result.FareType = GetFareTypeForProductClass(ProductClass);
                        result.FareSellKey = jSellKey;//Clubbed Journey Sell Key.
                        result.NonRefundable = false;
                        if (request.Type == SearchType.OneWay)
                        {
                            result.Flights = new FlightInfo[1][];
                        }
                        else
                        {
                            result.Flights = new FlightInfo[1][];
                        }
                        result.Flights[0] = new FlightInfo[journeyOnRet.Segments.Length];
                        result.Price = new PriceAccounts();

                        for (int k = 0; k < journeyOnRet.Segments.Length; k++)
                        {
                            result.Flights[0][k] = new FlightInfo();
                            result.Flights[0][k].Airline = "SG";
                            result.Flights[0][k].ArrivalTime = journeyOnRet.Segments[k].Legs[0].STA;
                            result.Flights[0][k].ArrTerminal = journeyOnRet.Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                            result.Flights[0][k].BookingClass = (journeyOnRet.Segments[k].CabinOfService != null && journeyOnRet.Segments[k].CabinOfService.Length > 0 ? journeyOnRet.Segments[k].CabinOfService : "C");
                            result.Flights[0][k].CabinClass = journeyOnRet.Segments[k].CabinOfService;
                            result.Flights[0][k].Craft = journeyOnRet.Segments[k].Legs[0].LegInfo.EquipmentType + "-" + journeyOnRet.Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                            result.Flights[0][k].DepartureTime = journeyOnRet.Segments[k].STD;
                            result.Flights[0][k].DepTerminal = journeyOnRet.Segments[k].Legs[0].LegInfo.DepartureTerminal;
                            result.Flights[0][k].Destination = new Airport(journeyOnRet.Segments[k].ArrivalStation);
                            result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                            result.Flights[0][k].ETicketEligible = journeyOnRet.Segments[k].Legs[0].LegInfo.ETicket;
                            result.Flights[0][k].FlightNumber = journeyOnRet.Segments[k].FlightDesignator.FlightNumber;
                            result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                            result.Flights[0][k].Group = 0;
                            result.Flights[0][k].OperatingCarrier = "SG";
                            result.Flights[0][k].Origin = new Airport(journeyOnRet.Segments[k].DepartureStation);
                            result.Flights[0][k].Stops = journeyOnRet.Segments.Length-1;
                            result.Flights[0][k].Status = "";
                            result.Flights[0][k].UapiSegmentRefKey = journeyOnRet.Segments[k].SegmentSellKey;
                            result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(ProductClass);

                            List<string> FarePaxtypes = new List<string>();//List which holds the Pax types  : "ADT","CHD"

                            foreach (PaxFare fare in journeyOnRet.Segments[k].Fares[0].PaxFares)
                            {
                                FarePaxtypes.Add(fare.PaxType);
                            }
                            FarePaxtypes = FarePaxtypes.Distinct().ToList();//Gets the unique pax type for fare price calculation.

                            #region FareBreakdown one way journey
                            foreach (String passengerType in FarePaxtypes)
                            {
                                switch (passengerType)
                                {
                                    case "ADT": //Adult
                                        if (result.FareBreakdown[0] == null)
                                        {
                                            result.FareBreakdown[0] = new Fare();
                                        }
                                        result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                        result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                        break;
                                    case "CHD": //Child
                                        if (result.FareBreakdown[1] == null)
                                        {
                                            result.FareBreakdown[1] = new Fare();
                                        }
                                        result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                        result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                        break;

                                }

                                //Infant Count
                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    if (result.FareBreakdown[2] == null)
                                    {
                                        result.FareBreakdown[2] = new Fare();
                                    }
                                    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                }

                                //Child Count
                                else if (request.ChildCount == 0 && request.InfantCount > 0)
                                {
                                    if (result.FareBreakdown[1] == null)
                                    {
                                        result.FareBreakdown[1] = new Fare();
                                    }
                                    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                }


                                if (onResponse != null)
                                {

                                    //We need to combine fares of all the segments from the price itenary response

                                    foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                    {
                                        if (passengerType == pfare.PaxType)
                                        {
                                            foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                            {
                                                switch (passengerType)
                                                {
                                                    case "ADT":

                                                        switch (charge.ChargeType)
                                                        {
                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                                result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                //result.FareBreakdown[0].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[0].PassengerCount);
                                                                result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                // result.FareBreakdown[0].TotalFare += (double)result.FareBreakdown[0].BaseFare;
                                                                result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                // result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                result.Price.PublishedFare += (decimal)baseFareAdult;
                                                                result.Price.SupplierPrice += charge.Amount;
                                                                break;
                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[0].PassengerCount);
                                                                result.FareBreakdown[0].TotalFare += tax;
                                                                result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                // result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                result.Price.Tax += (decimal)tax;
                                                                result.Price.SupplierPrice += charge.Amount;
                                                                //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                //{
                                                                //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                //}
                                                                //else
                                                                //{
                                                                //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                //}


                                                                adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                                break;
                                                        }

                                                        break;

                                                    case "CHD":

                                                        switch (charge.ChargeType)
                                                        {
                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                double baseFareChd = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                result.FareBreakdown[1].BaseFare += baseFareChd;
                                                                //  result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                                result.FareBreakdown[1].TotalFare += baseFareChd;
                                                                // result.FareBreakdown[1].TotalFare += (double)result.FareBreakdown[1].BaseFare;
                                                                result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                //  result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                result.Price.PublishedFare += (decimal)baseFareChd;
                                                                result.Price.SupplierPrice += charge.Amount;
                                                                break;
                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                                result.FareBreakdown[1].TotalFare += tax;
                                                                result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                //result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                result.Price.Tax += (decimal)tax;
                                                                result.Price.SupplierPrice += charge.Amount;
                                                                //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                                //{
                                                                //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                                //}
                                                                //else
                                                                //{
                                                                //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                                //}

                                                                chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                                break;
                                                        }


                                                        break;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }



                        if (request.InfantCount > 0 && onResponse != null)
                        {
                            foreach (BookingServiceCharge charge in onResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                            {
                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    switch (charge.ChargeType)
                                    {
                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                            double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                            //result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[2].PassengerCount);
                                            //result.FareBreakdown[2].TotalFare += (double)result.FareBreakdown[2].BaseFare;
                                            result.FareBreakdown[2].TotalFare += baseFare;
                                            result.FareBreakdown[2].BaseFare += baseFare;

                                            result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                            // result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                            result.Price.PublishedFare += (decimal)baseFare;
                                            result.Price.SupplierPrice += charge.Amount;
                                            break;
                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                            double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                            result.FareBreakdown[2].TotalFare += tax;
                                            result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                            // result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                            result.Price.Tax += (decimal)tax;
                                            result.Price.SupplierPrice += charge.Amount;
                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                            //{
                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                            //}
                                            //else
                                            //{
                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                            //}

                                            inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                            break;
                                    }
                                }
                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                {
                                    switch (charge.ChargeType)
                                    {
                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                            double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                            result.FareBreakdown[1].BaseFare += baseFare;
                                            //result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                            //result.FareBreakdown[1].TotalFare += (double)result.FareBreakdown[1].BaseFare;
                                            result.FareBreakdown[1].TotalFare += baseFare;
                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                            //result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                            result.Price.PublishedFare += (decimal)baseFare;
                                            result.Price.SupplierPrice += charge.Amount;
                                            break;
                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                            double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                            result.FareBreakdown[1].TotalFare += tax;
                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                            //result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                            result.Price.Tax += (decimal)tax;
                                            result.Price.SupplierPrice += charge.Amount;
                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                            //{
                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                            //}
                                            //else
                                            //{
                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                            //}

                                            inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                            break;
                                    }
                                }

                            }
                        }

                        //if (paxTaxBreakUp.Count > 0)
                        //{
                        //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                        //}

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }

                        result.BaggageIncludedInFare = onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        for (int k = 1; k < journeyOnRet.Segments.Length; k++)
                        {

                            result.BaggageIncludedInFare += "," + onResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                        result.Price.SupplierCurrency = currencyCode;
                        result.BaseFare = (double)result.Price.PublishedFare;
                        result.Tax = (double)result.Price.Tax;
                        result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax), agentDecimalValue));
                        ResultList.Add(result);

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet failed to get Connecting Flights Results.Reason : " + ex.Message, ex);
            }

            return ResultList;
        }

        /// <summary>
        /// Returns the combination of results for conecting flights for round trip journey only
        /// </summary>
        /// <param name="OnwardJourneys"></param>
        /// <param name="ReturnJourneys"></param>
        /// <param name="fareBreakDownCount"></param>
        /// <returns></returns>
        public List<SearchResult> GetConnectingFlightsCombinationsResults(Journey OnwardJourneys, Journey ReturnJourneys, int fareBreakDownCount)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            //Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();
            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
             List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

            try
            {
                //Onward Journeys Clubbed Sell Key Information
                string jSellKey = OnwardJourneys.Segments[0].Fares[0].FareSellKey;
                string connectingFareType = OnwardJourneys.Segments[0].FlightDesignator.FlightNumber + "_" + GetFareTypeForProductClass(OnwardJourneys.Segments[0].Fares[0].ProductClass).Replace(" ", "_") + "_" + Convert.ToString(OnwardJourneys.Segments[0].STD).Replace('/' ,'_').Replace(" " , "_").Replace(':' ,'_');
                string ProductClass = OnwardJourneys.Segments[0].Fares[0].ProductClass;
                for (int i = 1; i < OnwardJourneys.Segments.Length; i++)
                {
                    connectingFareType += "_" + OnwardJourneys.Segments[i].FlightDesignator.FlightNumber;

                    if (OnwardJourneys.Segments[i] != null)
                    {
                        jSellKey += "^" + OnwardJourneys.Segments[i].Fares[0].FareSellKey;
                    }
                }

                connectingFareType += "_" + Convert.ToString(OnwardJourneys.Segments[OnwardJourneys.Segments.Length - 1].STA).Replace('/' ,'_').Replace(" " , "_").Replace(':' ,'_');

                //Return Journeys Clubbed Sell Key Information
                string jSellKey1 = ReturnJourneys.Segments[0].Fares[0].FareSellKey;
                string connectingFareType1 = "__" + ReturnJourneys.Segments[0].FlightDesignator.FlightNumber + "_" + GetFareTypeForProductClass(ReturnJourneys.Segments[0].Fares[0].ProductClass).Replace(" ", "_") + "_" + Convert.ToString(ReturnJourneys.Segments[0].STD).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                string ProductClass1 = ReturnJourneys.Segments[0].Fares[0].ProductClass;
                for (int i = 1; i < ReturnJourneys.Segments.Length; i++)
                {
                    connectingFareType1 += "_" + ReturnJourneys.Segments[i].FlightDesignator.FlightNumber;
                    if (ReturnJourneys.Segments[i] != null)
                    {
                        jSellKey1 += "^" + ReturnJourneys.Segments[i].Fares[0].FareSellKey;
                    }
                }

                connectingFareType1 += "_" + Convert.ToString(ReturnJourneys.Segments[ReturnJourneys.Segments.Length - 1].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');

                PriceItineraryResponse priceResponse = GetItineraryPrice(request, OnwardJourneys.JourneySellKey, jSellKey, OnwardJourneys, connectingFareType.Trim() + connectingFareType1.Trim(), jSellKey1, ReturnJourneys.JourneySellKey, ReturnJourneys);
                if (priceResponse != null)
                {
                    rateOfExchange = exchangeRates[priceResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                    //paxTaxBreakUp.Clear();

                    paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<string, decimal>>>>();
                    adtPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                    chdPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();
                    inftPaxTaxBreakUp = new List<KeyValuePair<string, decimal>>();

                    


                    SearchResult result = new SearchResult();
                    result.IsLCC = true;
                    if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                    {
                        result.ResultBookingSource = BookingSource.SpiceJet;
                    }
                    else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                    {
                        result.ResultBookingSource = BookingSource.SpiceJetCorp;
                    }
                    result.Airline = "SG";
                    result.Currency = agentBaseCurrency;
                    result.EticketEligible = true;
                    result.FareBreakdown = new Fare[fareBreakDownCount];
                    result.FareRules = new List<FareRule>();
                    result.JourneySellKey = OnwardJourneys.JourneySellKey;
                    result.FareSellKey = jSellKey;
                    result.FareType = GetFareTypeForProductClass(OnwardJourneys.Segments[0].Fares[0].ProductClass);
                    if (request.Type == SearchType.OneWay)
                    {
                        result.Flights = new FlightInfo[1][];
                    }
                    else
                    {
                        result.Flights = new FlightInfo[2][];
                    }
                    result.Flights[0] = new FlightInfo[OnwardJourneys.Segments.Length];
                    result.Price = new PriceAccounts();
                    for (int k = 0; k < OnwardJourneys.Segments.Length; k++)
                    {
                        result.Flights[0][k] = new FlightInfo();
                        result.Flights[0][k].Airline = "SG";
                        result.Flights[0][k].ArrivalTime = OnwardJourneys.Segments[k].Legs[0].STA;
                        result.Flights[0][k].ArrTerminal = OnwardJourneys.Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                        result.Flights[0][k].BookingClass = (OnwardJourneys.Segments[k].CabinOfService != null && OnwardJourneys.Segments[k].CabinOfService.Length > 0 ? OnwardJourneys.Segments[k].CabinOfService : "C");
                        result.Flights[0][k].CabinClass = OnwardJourneys.Segments[k].CabinOfService;
                        result.Flights[0][k].Craft = OnwardJourneys.Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys.Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                        result.Flights[0][k].DepartureTime = OnwardJourneys.Segments[k].STD;
                        result.Flights[0][k].DepTerminal = OnwardJourneys.Segments[k].Legs[0].LegInfo.DepartureTerminal;
                        result.Flights[0][k].Destination = new Airport(OnwardJourneys.Segments[k].ArrivalStation);
                        result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                        result.Flights[0][k].ETicketEligible = OnwardJourneys.Segments[k].Legs[0].LegInfo.ETicket;
                        result.Flights[0][k].FlightNumber = OnwardJourneys.Segments[k].FlightDesignator.FlightNumber;
                        result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                        result.Flights[0][k].Group = 0;
                        result.Flights[0][k].OperatingCarrier = "SG";
                        result.Flights[0][k].Origin = new Airport(OnwardJourneys.Segments[k].DepartureStation);
                        result.Flights[0][k].Stops = OnwardJourneys.Segments.Length-1;
                        result.Flights[0][k].Status = "";
                        result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys.Segments[k].SegmentSellKey;
                        result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(ProductClass);

                        List<string> FarePaxtypes = new List<string>();

                        foreach (PaxFare fare in OnwardJourneys.Segments[k].Fares[0].PaxFares)
                        {
                            FarePaxtypes.Add(fare.PaxType);
                        }
                        FarePaxtypes = FarePaxtypes.Distinct().ToList();


                        foreach (String passengerType in FarePaxtypes)
                        {
                            switch (passengerType)
                            {
                                case "ADT": //Adult
                                    if (result.FareBreakdown[0] == null)
                                    {
                                        result.FareBreakdown[0] = new Fare();
                                    }
                                    result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                    result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                    break;
                                case "CHD": //Child
                                    if (result.FareBreakdown[1] == null)
                                    {
                                        result.FareBreakdown[1] = new Fare();
                                    }
                                    result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                    result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                    break;

                            }

                            //Infant Count
                            if (request.ChildCount > 0 && request.InfantCount > 0)
                            {
                                if (result.FareBreakdown[2] == null)
                                {
                                    result.FareBreakdown[2] = new Fare();
                                }
                                result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                            }

                            //Child Count
                            else if (request.ChildCount == 0 && request.InfantCount > 0)
                            {
                                if (result.FareBreakdown[1] == null)
                                {
                                    result.FareBreakdown[1] = new Fare();
                                }
                                result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                            }


                            if (priceResponse != null)
                            {

                                //We need to combine fares of all the segments from the price itenary response

                                foreach (PaxFare pfare in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares)
                                {
                                    if (passengerType == pfare.PaxType)
                                    {
                                        foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[0].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                        {
                                            switch (passengerType)
                                            {
                                                case "ADT":

                                                    switch (charge.ChargeType)
                                                    {
                                                        case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                            double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                            result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                            result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                            result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                            result.Price.PublishedFare += (decimal)baseFareAdult;
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                        case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                        case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                        case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                            double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[0].PassengerCount);
                                                            result.FareBreakdown[0].TotalFare += tax;
                                                            result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                            result.Price.Tax += (decimal)tax;
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                            //{
                                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                            //}
                                                            //else
                                                            //{
                                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                            //}
                                                            adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                                            break;
                                                    }

                                                    break;

                                                case "CHD":

                                                    switch (charge.ChargeType)
                                                    {
                                                        case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                            double baseFareChd = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                            result.FareBreakdown[1].BaseFare += baseFareChd;
                                                            result.FareBreakdown[1].TotalFare += baseFareChd;
                                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                            result.Price.PublishedFare += (decimal)baseFareChd;
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                        case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                        case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                        case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                            double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                            result.FareBreakdown[1].TotalFare += tax;
                                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                            result.Price.Tax += (decimal)tax;
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                            //{
                                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                            //}
                                                            //else
                                                            //{
                                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                            //}


                                                            chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                            break;
                                                    }


                                                    break;

                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }

                    if (request.InfantCount > 0 && priceResponse != null)
                    {
                        foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[0].ServiceCharges)
                        {
                            if (request.ChildCount > 0 && request.InfantCount > 0)
                            {
                                switch (charge.ChargeType)
                                {
                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                        double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                        result.FareBreakdown[2].TotalFare += baseFare;
                                        result.FareBreakdown[2].BaseFare += baseFare;
                                        result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                        result.Price.PublishedFare += (decimal)baseFare;
                                        result.Price.SupplierPrice += charge.Amount;
                                        break;
                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                        result.FareBreakdown[2].TotalFare += tax;
                                        result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                        result.Price.Tax += (decimal)tax;
                                        result.Price.SupplierPrice += charge.Amount;
                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                        //{
                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                        //}
                                        //else
                                        //{
                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                        //}
                                        inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));



                                        break;
                                }
                            }
                            else if (request.ChildCount <= 0 && request.InfantCount > 0)
                            {
                                switch (charge.ChargeType)
                                {
                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                        double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                        result.FareBreakdown[1].BaseFare += baseFare;
                                        result.FareBreakdown[1].TotalFare += baseFare;
                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                        result.Price.PublishedFare += (decimal)baseFare;
                                        result.Price.SupplierPrice += charge.Amount;
                                        break;
                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                        result.FareBreakdown[1].TotalFare += tax;
                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                        result.Price.Tax += (decimal)tax;
                                        result.Price.SupplierPrice += charge.Amount;
                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                        //{
                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                        //}
                                        //else
                                        //{
                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                        //}


                                        inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                        break;
                                }
                            }

                        }
                    }
                    result.BaggageIncludedInFare = priceResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    //combine the onward segment baggage allowance
                    for (int k = 1; k < OnwardJourneys.Segments.Length; k++)
                    {
                        result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    }

                    //********************* Return flight details **************************//

                    result.JourneySellKey += "|" + ReturnJourneys.JourneySellKey;
                    result.FareType += "," + GetFareTypeForProductClass(ProductClass1);
                    result.Flights[1] = new FlightInfo[ReturnJourneys.Segments.Length];
                    for (int k = 0; k < ReturnJourneys.Segments.Length; k++)
                    {

                        result.Flights[1][k] = new FlightInfo();
                        result.Flights[1][k].Airline = "SG";
                        result.Flights[1][k].ArrivalTime = ReturnJourneys.Segments[k].Legs[0].STA;
                        result.Flights[1][k].ArrTerminal = ReturnJourneys.Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                        result.Flights[1][k].BookingClass = (ReturnJourneys.Segments[k].CabinOfService != null && ReturnJourneys.Segments[k].CabinOfService.Length > 0 ? ReturnJourneys.Segments[k].CabinOfService : "");
                        result.Flights[1][k].CabinClass = ReturnJourneys.Segments[k].CabinOfService;
                        result.Flights[1][k].Craft = ReturnJourneys.Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys.Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                        result.Flights[1][k].DepartureTime = ReturnJourneys.Segments[k].STD;
                        result.Flights[1][k].DepTerminal = ReturnJourneys.Segments[k].Legs[0].LegInfo.DepartureTerminal;
                        result.Flights[1][k].Destination = new Airport(ReturnJourneys.Segments[k].ArrivalStation);
                        result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                        result.Flights[1][k].ETicketEligible = ReturnJourneys.Segments[k].Legs[0].LegInfo.ETicket;
                        result.Flights[1][k].FlightNumber = ReturnJourneys.Segments[k].FlightDesignator.FlightNumber;
                        result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                        result.Flights[1][k].Group = 1;
                        result.Flights[1][k].OperatingCarrier = "SG";
                        result.Flights[1][k].Origin = new Airport(ReturnJourneys.Segments[k].DepartureStation);
                        result.Flights[1][k].Stops = ReturnJourneys.Segments.Length-1;
                        result.Flights[1][k].Status = "";
                        result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys.Segments[k].SegmentSellKey;
                        result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(ProductClass1);
                        result.NonRefundable = false;
                        result.FareSellKey += "|" + jSellKey1;

                        List<string> FarePaxtypes = new List<string>();

                        foreach (PaxFare fare in ReturnJourneys.Segments[k].Fares[0].PaxFares)
                        {
                            FarePaxtypes.Add(fare.PaxType);
                        }
                        FarePaxtypes = FarePaxtypes.Distinct().ToList();
                        foreach (String passengerType in FarePaxtypes)
                        {
                            switch (passengerType)
                            {
                                case "ADT": //Adult
                                    if (result.FareBreakdown[0] == null)
                                    {
                                        result.FareBreakdown[0] = new Fare();
                                    }
                                    result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                    result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                    break;
                                case "CHD": //Child
                                    if (result.FareBreakdown[1] == null)
                                    {
                                        result.FareBreakdown[1] = new Fare();
                                    }
                                    result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                    result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                    break;
                            }

                            //Infant Count
                            if (request.ChildCount > 0 && request.InfantCount > 0)
                            {
                                if (result.FareBreakdown[2] == null)
                                {
                                    result.FareBreakdown[2] = new Fare();
                                }
                                result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                            }

                            //Child Count
                            else if (request.ChildCount == 0 && request.InfantCount > 0)
                            {
                                if (result.FareBreakdown[1] == null)
                                {
                                    result.FareBreakdown[1] = new Fare();
                                }
                                result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                            }

                            if (priceResponse != null)
                            {

                                //We need to combine fares of all the segments from the price itenary response

                                foreach (PaxFare pfare in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares)
                                {
                                    if (passengerType == pfare.PaxType)
                                    {
                                        foreach (BookingServiceCharge charge in priceResponse.Booking.Journeys[1].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                        {
                                            switch (passengerType)
                                            {
                                                case "ADT":

                                                    switch (charge.ChargeType)
                                                    {
                                                        case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                            double baseFareAdult = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[0].PassengerCount);
                                                            result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                            result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                            result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                            result.Price.PublishedFare += (decimal)baseFareAdult;
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                        case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                        case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                        case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                            double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                            result.FareBreakdown[0].TotalFare += tax;
                                                            result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                            result.Price.Tax += (decimal)tax;
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                            //{
                                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                            //}
                                                            //else
                                                            //{
                                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                            //}

                                                            adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                                            break;
                                                    }

                                                    break;

                                                case "CHD":

                                                    switch (charge.ChargeType)
                                                    {
                                                        case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                            double baseFareChd = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                            result.FareBreakdown[1].BaseFare += baseFareChd;
                                                            result.FareBreakdown[1].TotalFare += baseFareChd;
                                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                            result.Price.PublishedFare += (decimal)baseFareChd;
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                        case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                        case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                        case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                            double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[1].PassengerCount);
                                                            result.FareBreakdown[1].TotalFare += tax;
                                                            result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                            result.Price.Tax += (decimal)tax;
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                                            //{
                                                            //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                                            //}
                                                            //else
                                                            //{
                                                            //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                                            //}

                                                            chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));


                                                            break;
                                                    }


                                                    break;

                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (request.InfantCount > 0 && priceResponse != null)
                    {
                        foreach (BookingServiceCharge charge in priceResponse.Booking.Passengers[0].PassengerFees[1].ServiceCharges)
                        {
                            if (request.ChildCount > 0 && request.InfantCount > 0)
                            {
                                switch (charge.ChargeType)
                                {
                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:

                                        double baseFare = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                        result.FareBreakdown[2].TotalFare += baseFare;
                                        result.FareBreakdown[2].BaseFare += baseFare;
                                        result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                        result.Price.PublishedFare += (decimal)baseFare;
                                        result.Price.SupplierPrice += charge.Amount;
                                        break;
                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                        double tax = (double)((charge.Amount * rateOfExchange)* result.FareBreakdown[2].PassengerCount);
                                        result.FareBreakdown[2].TotalFare += tax;
                                        result.FareBreakdown[2].SupplierFare += (double)(charge.Amount * result.FareBreakdown[2].PassengerCount);
                                        result.Price.Tax += (decimal)tax;
                                        result.Price.SupplierPrice += charge.Amount;
                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                        //{
                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                        //}
                                        //else
                                        //{
                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                        //}



                                        inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                        break;
                                }
                            }
                            else if (request.ChildCount <= 0 && request.InfantCount > 0)
                            {
                                switch (charge.ChargeType)
                                {
                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                        double baseFare = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                        result.FareBreakdown[1].BaseFare += baseFare;
                                        result.FareBreakdown[1].TotalFare += baseFare;
                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                        result.Price.PublishedFare += (decimal)baseFare;
                                        result.Price.SupplierPrice += charge.Amount;
                                        break;
                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                        double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                        result.FareBreakdown[1].TotalFare += tax;
                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                        result.Price.Tax += (decimal)tax;
                                        result.Price.SupplierPrice += charge.Amount;
                                        //if (paxTaxBreakUp.ContainsKey(charge.ChargeCode))
                                        //{
                                        //    paxTaxBreakUp[charge.ChargeCode] = paxTaxBreakUp[charge.ChargeCode] + Convert.ToDecimal(tax);
                                        //}
                                        //else
                                        //{
                                        //    paxTaxBreakUp[charge.ChargeCode] = Convert.ToDecimal(tax);
                                        //}



                                        inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                                        break;
                                }
                            }

                        }
                    }
                    //if (paxTaxBreakUp.Count > 0)
                    //{
                    //    result.Price.PaxTaxBreakUp = paxTaxBreakUp;
                    //}
                    if (adtPaxTaxBreakUp.Count > 0)
                    {
                        paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                    }
                    if (chdPaxTaxBreakUp.Count > 0)
                    {
                        paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                    }
                    if (inftPaxTaxBreakUp.Count > 0)
                    {
                        paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                    }
                    if (paxTypeTaxBreakUp.Count > 0)
                    {
                        result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                    }
                    result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    //combine return segments baggage allowance
                    for (int k = 1; k < ReturnJourneys.Segments.Length; k++)
                    {
                        result.BaggageIncludedInFare += "," + priceResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    }
                    result.Price.SupplierCurrency = currencyCode;
                    result.BaseFare = (double)(result.Price.PublishedFare);
                    result.Tax = (double)(result.Price.Tax);
                    result.TotalFare = (double)(Math.Round((result.BaseFare + result.Tax), agentDecimalValue));
                    ResultList.Add(result);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("(SpiceJet) failed to get Connecting Flights Combinations Results.Reason : " + ex.Message, ex);
            }
            return ResultList;
        }
        

        //Added by Lokesh on 27-Feb-2018 .

        /// <summary>
        /// Updates passenger details for the booking in booking state. 
        /// The UpdatePassengers method processes the list of passengers input in the UpdatePassengersRequest 
        /// using the PassengerNumber field to match the passengers to passengers in booking state. 
        /// The matching passenger details are replaced with the details from the request. 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData UpdatePassengers(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData upResponse = new BookingUpdateResponseData();

            try
            {

                if (itinerary != null && !string.IsNullOrEmpty(signature) && signature.Length > 0)
                {

                    int paxCount = 0, infantCount = 0, infants = 0;
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            paxCount++;
                        }
                        else
                        {
                            infantCount++;
                        }
                    }

                    UpdatePassengersRequest updatePassengerRequest = new UpdatePassengersRequest();
                    updatePassengerRequest.ContractVersion = contractVersion;
                    updatePassengerRequest.Signature = signature;
                    updatePassengerRequest.updatePassengersRequestData = new UpdatePassengersRequestData();
                    updatePassengerRequest.updatePassengersRequestData.Passengers = new Passenger[paxCount];

                    for (int i = 0; i < paxCount; i++)
                    {
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i] = new Passenger();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerNumber = (Int16)i;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].State = MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names = new BookingName[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0] = new BookingName();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].Title = itinerary.Passenger[i].Title.ToUpper();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].FirstName = itinerary.Passenger[i].FirstName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].LastName = itinerary.Passenger[i].LastName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].State = MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo = new PassengerInfo();
                        if (itinerary.Passenger[i].Gender == Gender.Male)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = SpiceJet.SGBooking.Gender.Male;
                        }
                        else
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = SpiceJet.SGBooking.Gender.Female;
                        }
                        if (itinerary.Passenger[i].Nationality != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                            }
                        }
                        if (itinerary.Passenger[i].Country != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                            }
                        }
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos = new PassengerTypeInfo[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0] = new PassengerTypeInfo();
                        if (itinerary.Passenger[i].Type == PassengerType.Adult)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "ADT";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-30);
                            }
                            if (infantCount - infants > 0)
                            {
                                FlightPassenger infantPax = itinerary.Passenger[paxCount + infants];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant = new PassengerInfant();
                                if (infantPax.DateOfBirth != DateTime.MinValue)
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = infantPax.DateOfBirth;
                                }
                                else
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = DateTime.Today.AddDays(-365);
                                }
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Gender = (infantPax.Gender == Gender.Male ? SpiceJet.SGBooking.Gender.Male : SpiceJet.SGBooking.Gender.Female);
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names = new BookingName[1];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0] = new BookingName();
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].FirstName = infantPax.FirstName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].LastName = infantPax.LastName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].Title = infantPax.Title.ToUpper();
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].State = MessageState.New;
                                if (itinerary.Passenger[i].Nationality != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                                    }
                                }
                                if (itinerary.Passenger[i].Country != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                                    }
                                }

                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.State = MessageState.New;
                                infants++;
                            }
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "CHD";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-10);
                            }
                        }
                    }

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(UpdatePassengersRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGUpdatePassengersRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, updatePassengerRequest);
                        sw.Close();
                    }
                    catch { }


                    upResponse = bookingAPI.UpdatePassengers(contractVersion, signature, updatePassengerRequest.updatePassengersRequestData);

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGUpdatePassengersResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, upResponse);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    throw new Exception("(SpiceJet)Failed to update passengers as itinerary or signature passed may be null");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to update passengers. Error : " + ex.ToString(), "");
                throw ex;
            }
            return upResponse;
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~SpiceJetAPIV1()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion



        /****Below are the important points with respect to GST implementation****/
        //1.GST Implementation.
        //2.All carriers travelling to/ from /within india be able to apply the correct GST charges.
        //3.GST Registration Number ,Name of GST holder, Email of GST holder.
        //4.If PNR has multiple passengers only 1 GST Registration Number is allowed per PNR
        //5.In case of B2B transactions,2 new method calls will be used in the booking flow.
        //6.Methods: UpdateContactRequest and GetBookingFromState.

        /// <summary>
        /// Updates the GST details for the lead passenger.
        /// </summary>
        /// <returns></returns>
        public UpdateContactsResponse UpdateContactGSTDetails(string signature)
        {
            UpdateContactsResponse updateContactsResponse = new UpdateContactsResponse();
            BookingUpdateResponseData bookingUpdateResponseData = new BookingUpdateResponseData();
            try
            {
                UpdateContactsRequest updateContactsRequest = new UpdateContactsRequest();

                updateContactsRequest.ContractVersion = contractVersion;
                updateContactsRequest.Signature = signature;

                UpdateContactsRequestData updateContactsRequestData = new UpdateContactsRequestData();
                updateContactsRequestData.BookingContactList = new BookingContact[1];
                updateContactsRequestData.BookingContactList[0] = new BookingContact();
                updateContactsRequestData.BookingContactList[0].State = SpiceJet.SGBooking.MessageState.New;
                updateContactsRequestData.BookingContactList[0].TypeCode = "G";
                updateContactsRequestData.BookingContactList[0].EmailAddress = gstOfficialEmail;
                updateContactsRequestData.BookingContactList[0].CustomerNumber = gstNumber;
                updateContactsRequestData.BookingContactList[0].CompanyName = gstCompanyName;
                updateContactsRequestData.BookingContactList[0].DistributionOption = DistributionOption.None;
                updateContactsRequestData.BookingContactList[0].NotificationPreference = NotificationPreference.None;
                updateContactsRequest.updateContactsRequestData = updateContactsRequestData;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(UpdateContactsRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGLeadPaxGSTRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, updateContactsRequest);
                    sw.Close();
                }
                catch { }
                bookingUpdateResponseData = bookingAPI.UpdateContacts(contractVersion, signature, updateContactsRequestData);
                if (bookingUpdateResponseData != null)
                {
                    updateContactsResponse.BookingUpdateResponseData = bookingUpdateResponseData;
                }
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(UpdateContactsResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGLeadPaxGSTResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, updateContactsResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to update GST Details for the lead passenger. Error : " + ex.ToString(), "");
                throw ex;
            }
            return updateContactsResponse;
        }

        /// <summary>
        /// To Get the bifurcation of Taxes after GSTN update.
        /// </summary>
        /// <returns></returns>
        public GetBookingFromStateResponse GetBookingFromState(string signature)
        {
            GetBookingFromStateResponse getBookingFromStateResponse = new GetBookingFromStateResponse();
            Booking booking = new Booking();
            try
            {
                GetBookingFromStateRequest getBookingFromStateRequest = new GetBookingFromStateRequest();
                getBookingFromStateRequest.ContractVersion = contractVersion;
                getBookingFromStateRequest.Signature = signature;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingFromStateRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SG_GST_Tax_Breakup_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingFromStateRequest);
                    sw.Close();
                }
                catch { }
                booking = bookingAPI.GetBookingFromState(contractVersion, signature);
                if (booking != null)
                {
                    getBookingFromStateResponse.BookingData = booking;
                }
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingFromStateResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SG_GST_Tax_Breakup_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingFromStateResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to get updated GST break up for the lead passenger. Error : " + ex.ToString(), "");
                throw ex;
            }
            return getBookingFromStateResponse;
        }


        ///<summary>
        /// Holds the selected itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public void GetUpdatedGSTFareBreakUp(ref SearchResult result,SearchRequest request)
        {
            try
            {
                //If the lead pax supplies the GST input.
                //Then Get the updated GST farebreak up.
                LogonResponse loginResponse = Login();
                if (result != null && loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature))
                {
                   GetBookingFromStateResponse updatedGSTBreakup = GetBookingFromState(loginResponse.Signature);
                    
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to GetUpdatedGSTFareBreakUp . Error : " + ex.ToString(), "");
                throw ex;
            }
        }
        /// <summary>
        /// This method returns the Available SSR's based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetAvailableSSR(SearchResult result)
        {
            DataTable dtSourceBaggage = null;
            try
            {
                //SSR : AdditionalBaggage and Meals [For Both Onward and Return Journeys]
                //Note:Infant is also an SSR.
                //But we will get the infant price from the price itinerary response.

                //Steps to get the available SSR Without GST input.
                //1.Login
                //2.Sell [Holds the complete itinerary]
                //3.GetSSRAvailabilityForBooking.

                //Steps to get the available SSR With GST input.
                //1.Login
                //2.UpdateContactRequest 
                //3.Sell[Holds the complete itinerary]
                //4.GetSSRAvailabilityForBooking
                //5.GetBookingFromState
               
                //Step1: Login
                LogonResponse loginResponse = Login();
                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
                {
                    //Note:Verify any GST input available then execute UpdateContactsRequest first.
                    //Step-1.1: If any GST input then UpdateContactGSTDetails method
                    UpdateContactsResponse updateGSTResponse = null;
                    if (!string.IsNullOrEmpty(gstCompanyName) && (gstCompanyName.Length > 0) &&
                       !string.IsNullOrEmpty(gstNumber) && (gstNumber.Length > 0) &&
                        !string.IsNullOrEmpty(gstOfficialEmail) && (gstOfficialEmail.Length > 0)
                       && !string.IsNullOrEmpty(loginResponse.Signature))
                    {

                        updateGSTResponse = UpdateContactGSTDetails(loginResponse.Signature);

                    }
                    //Step2:Sell
                    BookingUpdateResponseData responseData = ExecuteSell(loginResponse.Signature, result);
                    if (responseData != null)
                    {
                        //Step-3: GetAvailableSSR

                        SSRAvailabilityForBookingResponse ssrResponse = new SSRAvailabilityForBookingResponse();
                        GetSSRAvailabilityForBookingRequest ssrRequest = new GetSSRAvailabilityForBookingRequest();
                        ssrRequest.ContractVersion = contractVersion;
                        ssrRequest.Signature = loginResponse.Signature;

                        SSRAvailabilityForBookingRequest availRequest = new SSRAvailabilityForBookingRequest();
                        availRequest.InventoryControlled = true;
                        availRequest.NonInventoryControlled = true;
                        availRequest.SeatDependent = true;
                        availRequest.NonSeatDependent = true;
                        availRequest.CurrencyCode = currencyCode;

                        List<LegKey> legKeysList = new List<LegKey>();
                        for (int i = 0; i < result.Flights.Length; i++)
                        {
                            for (int j = 0; j < result.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = "SG";
                                legKey.FlightNumber = result.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = result.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = result.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }
                        if (legKeysList != null && legKeysList.Count > 0)
                        {
                            availRequest.SegmentKeyList = legKeysList.ToArray();
                        }
                        ssrRequest.SSRAvailabilityForBookingRequest = availRequest;
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SG_AvailableSSR_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrRequest);
                            sw.Close();
                        }
                        catch { }
                        ssrResponse = bookingAPI.GetSSRAvailabilityForBooking(contractVersion, loginResponse.Signature, availRequest);
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(SSRAvailabilityForBookingResponse));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SG_AvailableSSR_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrResponse);
                            sw.Close();
                        }
                        catch { }
                        if (ssrResponse != null && ssrResponse.SSRSegmentList != null && ssrResponse.SSRSegmentList.Length > 0)
                        {
                            dtSourceBaggage = new DataTable();
                            if (dtSourceBaggage.Columns.Count == 0)
                            {
                                dtSourceBaggage.Columns.Add("Code", typeof(string));
                                dtSourceBaggage.Columns.Add("Price", typeof(decimal));
                                dtSourceBaggage.Columns.Add("Group", typeof(int));
                                dtSourceBaggage.Columns.Add("Currency", typeof(string));
                                dtSourceBaggage.Columns.Add("Description", typeof(string));
                                dtSourceBaggage.Columns.Add("QtyAvailable", typeof(int));
                            }
                            List<AvailablePaxSSR> availablePaxSSRs_OnwardBag = new List<AvailablePaxSSR>();//OnwardBaggage
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnBag = new List<AvailablePaxSSR>();//ReturnBaggage

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardMeal = new List<AvailablePaxSSR>();//OnwardMeal
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnMeal = new List<AvailablePaxSSR>();//ReturnMeal


                            for (int i = 0; i < result.Flights.Length; i++)
                            {
                                for (int j = 0; j < result.Flights[i].Length; j++)
                                {
                                    string DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = result.Flights[i][j].Destination.AirportCode;

                                    for (int p = 0; p < ssrResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            if (i == 0)
                                            {
                                                availablePaxSSRs_OnwardBag.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("XBAG"))).ToList());
                                                availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("VGML"))).ToList());
                                                availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("NVML"))).ToList());

                                            }
                                            else
                                            {
                                                availablePaxSSRs_ReturnBag.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("XBAG"))).ToList());
                                                availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("VGML"))).ToList());
                                                availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("NVML"))).ToList());

                                            }
                                        }
                                    }

                                }
                            }

                            List<string> SSR_BaggageCodes = new List<string>();
                            SSR_BaggageCodes.Add("EB05");
                            SSR_BaggageCodes.Add("EB10");
                            SSR_BaggageCodes.Add("EB15");
                            SSR_BaggageCodes.Add("EB20");
                            SSR_BaggageCodes.Add("EB30");

                            List<string> SSR_MealCodes = new List<string>();
                            SSR_MealCodes.Add("VGML");
                            SSR_MealCodes.Add("NVML");

                            if (availablePaxSSRs_OnwardBag.Count > 0)
                            {
                                foreach (string baggageCode in SSR_BaggageCodes)
                                {
                                    List<AvailablePaxSSR> bagggeSSR = availablePaxSSRs_OnwardBag.Where(m => m.SSRCode.Contains(baggageCode)).ToList();
                                    if (bagggeSSR != null && bagggeSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < bagggeSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(bagggeSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal baggagePrice = charges.Where(o => o.ChargeType == SpiceJet.SGBooking.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = baggageCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(baggagePrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "0";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (baggageCode)
                                        {
                                            case "EB05":
                                                dr["Description"] = "5Kg Excess Baggage";
                                                break;
                                            case "EB10":
                                                dr["Description"] = "10Kg Excess Baggage";
                                                break;
                                            case "EB15":
                                                dr["Description"] = "15Kg Excess Baggage";
                                                break;
                                            case "EB20":
                                                dr["Description"] = "20Kg Excess Baggage";
                                                break;
                                            case "EB30":
                                                dr["Description"] = "30Kg Excess Baggage";
                                                break;
                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            } //OnwardBaggage

                            if (availablePaxSSRs_ReturnBag.Count > 0)
                            {
                                foreach (string baggageCode in SSR_BaggageCodes)
                                {
                                    List<AvailablePaxSSR> bagggeSSR = availablePaxSSRs_ReturnBag.Where(m => m.SSRCode.Contains(baggageCode)).ToList();
                                    if (bagggeSSR != null && bagggeSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < bagggeSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(bagggeSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal baggagePrice = charges.Where(o => o.ChargeType == SpiceJet.SGBooking.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = baggageCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(baggagePrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "1";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (baggageCode)
                                        {
                                            case "EB05":
                                                dr["Description"] = "5Kg Excess Baggage";
                                                break;
                                            case "EB10":
                                                dr["Description"] = "10Kg Excess Baggage";
                                                break;
                                            case "EB15":
                                                dr["Description"] = "15Kg Excess Baggage";
                                                break;
                                            case "EB20":
                                                dr["Description"] = "20Kg Excess Baggage";
                                                break;
                                            case "EB30":
                                                dr["Description"] = "30Kg Excess Baggage";
                                                break;
                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            } //ReturnBaggage

                            if (availablePaxSSRs_OnwardMeal.Count > 0)
                            {
                                foreach (string mealCode in SSR_MealCodes)
                                {
                                    List<AvailablePaxSSR> mealSSR = availablePaxSSRs_OnwardMeal.Where(m => m.SSRCode.Contains(mealCode)).ToList();
                                    if (mealSSR != null && mealSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < mealSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(mealSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal mealPrice = charges.Where(o => o.ChargeType == SpiceJet.SGBooking.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = mealCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(mealPrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "0";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (mealCode)
                                        {
                                            case "VGML":
                                                dr["Description"] = "Veg Meal";
                                                break;
                                            case "NVML":
                                                dr["Description"] = "Non Veg Meal";
                                                break;
                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            } //OnwardMeal

                            if (availablePaxSSRs_ReturnMeal.Count > 0)
                            {
                                foreach (string mealCode in SSR_MealCodes)
                                {
                                    List<AvailablePaxSSR> mealSSR = availablePaxSSRs_ReturnMeal.Where(m => m.SSRCode.Contains(mealCode)).ToList();
                                    if (mealSSR != null && mealSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < mealSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(mealSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal mealPrice = charges.Where(o => o.ChargeType == SpiceJet.SGBooking.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = mealCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(mealPrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "1";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (mealCode)
                                        {
                                            case "VGML":
                                                dr["Description"] = "Veg Meal";
                                                break;
                                            case "NVML":
                                                dr["Description"] = "Non Veg Meal";
                                                break;
                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            } //ReturnMeal

                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Execute Sell Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return dtSourceBaggage;
        }

        /// <summary>
        /// This method holds the itinerary.This method is a part of the booking process.
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="searchResult"></param>
        /// <returns></returns>
        public BookingUpdateResponseData ExecuteSell(string signature ,SearchResult searchResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {

                FlightItinerary itinerary = new FlightItinerary();
                itinerary.Segments = SearchResult.GetSegments(searchResult);
                itinerary.TicketAdvisory = searchResult.FareSellKey;

                string onwardJourneySellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    if (itinerary.Segments[p].Group == 0)//Onward
                    {
                        sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                    if (itinerary.Segments[p].Group == 1)//Return
                    {
                        sellKeysListReturn.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }
                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                if (itinerary.TicketAdvisory.Split('|').Length>1)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                if (itinerary.TicketAdvisory.Split('|').Length>1)//Round trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < searchResult.FareBreakdown.Length; i++)
                {
                    if (searchResult.FareBreakdown[i] != null && searchResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < searchResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (searchResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
               
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSellRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }
                responseData = bookingAPI.Sell(contractVersion, signature, requestData);
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_"  +"SGSellResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Execute Sell Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

    }
}
