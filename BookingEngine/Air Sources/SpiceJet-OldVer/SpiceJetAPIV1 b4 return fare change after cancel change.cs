﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using SpiceJet.SGSession;
using SpiceJet.SGBooking;
using CT.BookingEngine;
using CT.Configuration;
using CT.Core;
using System.Data;
using System.Threading;

namespace CT.BookingEngine.GDS
{
    [Serializable]
    public class SpiceJetAPIV1
    {
        string agentId;
        string agentDomain;
        string password;
        string organisationCode;
        int contractVersion;

        Dictionary<string, decimal> exchangeRates;
        decimal rateOfExchange;
        string agentBaseCurrency;
        int agentDecimalValue;
        string xmlLogPath;
        Hashtable pricedItineraries = new Hashtable();
        DataTable dtBaggage = new DataTable();

        /********************Search Variables******************/
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        //Search Request
        SearchRequest request = new SearchRequest();
        //Signature of the Login response
        string loginSignature="";
        //Search results will be combined for return search and one way search
        List<SearchResult> CombinedSearchResults = new List<SearchResult>();        
        //Web Service for all operations except the Login
        BookingManagerClient bookingAPI = new BookingManagerClient();
        //Web Service for Login and Logout
        SessionManagerClient sessionAPI = new SessionManagerClient();
        /****************************End***********************/                
        public SpiceJetAPIV1()
        {
            xmlLogPath = ConfigurationSystem.SpiceJetConfig["XmlLogPath"];
            if (!Directory.Exists(xmlLogPath))
            {
                Directory.CreateDirectory(xmlLogPath);
            }

            agentId = ConfigurationSystem.SpiceJetConfig["AgentId"];
            agentDomain = ConfigurationSystem.SpiceJetConfig["AgentDomain"];
            password = ConfigurationSystem.SpiceJetConfig["Password"];
            organisationCode = ConfigurationSystem.SpiceJetConfig["OrganisationCode"];
            contractVersion = Convert.ToInt32(ConfigurationSystem.SpiceJetConfig["ContractVersion"]);

            if (dtBaggage.Columns.Count == 0)
            {
                dtBaggage.Columns.Add("Code", typeof(string));
                dtBaggage.Columns.Add("Price", typeof(decimal));
                dtBaggage.Columns.Add("Group", typeof(int));
                dtBaggage.Columns.Add("Currency", typeof(string));
                dtBaggage.Columns.Add("Description", typeof(string));
                dtBaggage.Columns.Add("QtyAvailable", typeof(int));
            }
            //Assign the web service url from source xml config
            bookingAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.SpiceJetConfig["BookingService"]);
            sessionAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.SpiceJetConfig["SessionService"]);
        }

        public string LoginName
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string AgentDomain
        {
            get { return agentDomain; }
            set { agentDomain = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public int AgentDecimalValue
        {
            get { return agentDecimalValue; }
            set { agentDecimalValue = value; }
        }

        private LogonResponse Login()
        {
            LogonResponse response = new LogonResponse();

            try
            {
                LogonRequest request = new LogonRequest();                
                request.logonRequestData = new LogonRequestData();
                request.logonRequestData.AgentName = agentId;
                request.logonRequestData.DomainCode = agentDomain;
                request.logonRequestData.LocationCode = "";
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.Password = password;
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.TerminalInfo = "";

                XmlSerializer ser = new XmlSerializer(typeof(LogonRequest));
                string filePath = xmlLogPath + "SGLogonRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, request);
                sw.Close();

                
                response.Signature = sessionAPI.Logon(contractVersion, request.logonRequestData);

                ser = new XmlSerializer(typeof(LogonResponse));
                filePath = xmlLogPath + "SGLogonResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                sw = new StreamWriter(filePath);
                ser.Serialize(sw, response);
                sw.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Login failed. Reason : " + ex.Message, ex);
            }

            return response;
        }

        private LogoutResponse Logout(string signature)
        {
            LogoutResponse response = new LogoutResponse();

            try
            {
                LogoutRequest request = new LogoutRequest();
                
                request.Signature = signature;

                XmlSerializer ser = new XmlSerializer(typeof(LogoutRequest));
                string filePath = xmlLogPath + "SGLogoutRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, request);
                sw.Close();

                
                sessionAPI.Logout(contractVersion, request.Signature);                
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Login failed. Reason : " + ex.Message, ex);
            }

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult[] Search(SearchRequest request)
        {
            SearchResult[] results = new SearchResult[0];
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                LogonResponse loginResponse = Login();

                if (loginResponse != null && loginResponse.Signature.Length > 0)
                {
                    TripAvailabilityResponse tripAvailResponse = null;
                    this.request = request;
                    loginSignature = loginResponse.Signature;

                    /************************************************************************************************
                     *  If Return Search then call individual fare search for Regular, Hand Baggage, Special Return *
                     *  and Family Fares. Family Fares will only be search if the Total Pax Count greater than or 
                     *  equal to 4 excluding Infant pax i.e AdultCount + ChildCount >= 4. Earlier we use to make 
                     *  combinations of all fares. But as per SpiceJet we should not merge Special Return and Family
                     *  Fares with other fares since they are special fares. So now we are making same fare results
                     *  i.e Onward -> Special Return 
                     *      Return -> Special Return 
                     *  All the searched results will be stored in CombinedSearchResults list and returned.
                     * **********************************************************************************************/
                    if (request.Type == SearchType.Return)
                    {                       
                        //ReadySources will hold same source orderings i.e SG1, SG2 ....
                        Dictionary<string, int> readySources = new Dictionary<string, int>();
                                                
                        //This dictionary will hold what fare search we want to call
                        Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();

                        listOfThreads.Add("SG1", new WaitCallback(SearchForRegularFares));
                        listOfThreads.Add("SG2", new WaitCallback(SearchForFlexFares));                        
                        listOfThreads.Add("SG3", new WaitCallback(SearchForSpecialReturnFares));
                        listOfThreads.Add("SG4", new WaitCallback(SearchForHandBaggageFares));

                        //Family Fares will only be returned if pax count >= 4 excluding infants
                        if ((request.AdultCount + request.ChildCount) >= 4)
                        {
                            eventFlag = new AutoResetEvent[5];//Total 5 fares need to be searched
                            listOfThreads.Add("SG5", new WaitCallback(SearchForFamilyFares));//Add FamilyFares for search                            

                            //Add the ordering with timeout of 300 seconds for each fare search
                            readySources.Add("SG1", 300);
                            readySources.Add("SG2", 300);
                            readySources.Add("SG3", 300);
                            readySources.Add("SG4", 300);
                            readySources.Add("SG5", 300);
                        }
                        else
                        {
                            eventFlag = new AutoResetEvent[4];//Total 4 fares need to be searched bcoz FamilyFares is not qualified
                            readySources.Add("SG1", 300);
                            readySources.Add("SG2", 300);
                            readySources.Add("SG3", 300);
                            readySources.Add("SG4", 300);
                        }                       
                        
                        int j = 0;
                        //Start each fare search within a Thread which will automatically terminate after the results are received.
                        foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                        {
                            if (readySources.ContainsKey(deThread.Key))
                            {
                                ThreadPool.QueueUserWorkItem(deThread.Value, j);
                                eventFlag[j] = new AutoResetEvent(false);
                                j++;
                            }
                        }

                        if (j != 0)
                        {
                            if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 1500), true))
                            {
                                //TODO: audit which thread is timed out                
                            }
                        }
                    }
                    else if (request.Type == SearchType.OneWay) //If one way search then call all fares
                    {
                        #region One Way Search Results
                        GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                        GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                        availRequest.ContractVersion = contractVersion;
                        availRequest.Signature = loginResponse.Signature;
                        availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                        if (request.Type == SearchType.OneWay)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                        }

                        for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = "INR";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = "INR";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.Default;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = false;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";

                            if ((request.AdultCount + request.ChildCount) >= 4)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                            }
                            else
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[2];
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                            }
                            int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                            for (int a = 0; a < request.AdultCount; a++)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                            }
                            if (request.ChildCount > 0)
                            {
                                for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                                }
                            }
                            if (request.InfantCount > 0)
                            {
                                for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                                }
                            }
                        }
                        availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                        if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            try
                            {
                                XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                                string filePath = xmlLogPath + "SGSearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                StreamWriter sw = new StreamWriter(filePath);
                                ser.Serialize(sw, availRequest);
                                sw.Close();
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                            }
                        }

                        
                        tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginResponse.Signature, availRequest.TripAvailabilityRequest);

                        if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            try
                            {
                                string filePath = xmlLogPath + "SGSearchResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                                doc.Save(filePath);

                                //filePath = xmlLogPath + "SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                                //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                                //StreamWriter sw = new StreamWriter(filePath);
                                //ser.Serialize(sw, tripAvailResponse);
                                //sw.Close();
                            }
                            catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                        }
                    }

                    if (tripAvailResponse != null && request.Type == SearchType.OneWay)
                    {
                        List<Journey> OnwardJourneys = new List<Journey>();


                        List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();


                        rateOfExchange = exchangeRates["INR"];

                        for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                        {
                            for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                            {
                                if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                                {
                                    OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                                }

                            }
                        }
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                            {
                                onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                            }
                        }

                        int fareBreakDownCount = 0;

                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }


                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {                            
                            foreach (SpiceJet.SGBooking.Fare onFare in OnwardJourneys[i].Segments[0].Fares)
                            {
                                
                                PriceItineraryResponse onResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onFare.FareSellKey, OnwardJourneys[i]);

                                SearchResult result = new SearchResult();

                                result.ResultBookingSource = BookingSource.SpiceJet;
                                result.Airline = "SG";
                                result.Currency = agentBaseCurrency;
                                result.EticketEligible = true;
                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                result.FareRules = new List<FareRule>();
                                result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                result.FareType = GetFareTypeForProductClass(onFare.ProductClass);
                                result.FareSellKey = onFare.FareSellKey;
                                result.NonRefundable = false;
                                if (request.Type == SearchType.OneWay)
                                {
                                    result.Flights = new FlightInfo[1][];
                                }

                                result.Flights[0] = new FlightInfo[1];
                                
                                for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                {   
                                    result.Flights[0][k] = new FlightInfo();
                                    result.Flights[0][k].Airline = "SG";
                                    result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                    result.Flights[0][k].ArrTerminal = "";
                                    result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "C");
                                    result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                    result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                    result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                    result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                    result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                    result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                    result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                    result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                    result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                    result.Flights[0][k].Group = 0;
                                    result.Flights[0][k].OperatingCarrier = "SG";
                                    result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                    result.Flights[0][k].Stops = 1;
                                    result.Flights[0][k].Status = "";
                                    result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                    result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onFare.ProductClass);

                                    result.Price = new PriceAccounts();

                                    foreach (PaxFare fare in OnwardJourneys[i].Segments[k].Fares[0].PaxFares)
                                    {

                                        switch (fare.PaxType)
                                        {
                                            case "ADT":
                                                result.FareBreakdown[0] = new Fare();
                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                break;
                                            case "CHD":
                                                result.FareBreakdown[1] = new Fare();
                                                result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                break;

                                        }
                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                        {
                                            result.FareBreakdown[2] = new Fare();
                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                        }
                                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                                        {
                                            result.FareBreakdown[1] = new Fare();
                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                        }
                                        if (onResponse != null)
                                        {
                                            foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                            {
                                                if (fare.PaxType == pfare.PaxType)
                                                {
                                                    foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                    {
                                                        switch (fare.PaxType)
                                                        {
                                                            case "ADT":
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                        result.FareBreakdown[0].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[0].PassengerCount);
                                                                        result.FareBreakdown[0].TotalFare += (double)result.FareBreakdown[0].BaseFare;
                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                        double tax = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[0].PassengerCount);
                                                                        result.FareBreakdown[0].TotalFare += tax;
                                                                        result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                }
                                                                break;
                                                            case "CHD":
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                        result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                                        result.FareBreakdown[1].TotalFare += (double)result.FareBreakdown[1].BaseFare;
                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                        double tax = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                                        result.FareBreakdown[1].TotalFare += tax;
                                                                        result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                }
                                                                break;

                                                        }
                                                    }
                                                }
                                            }

                                            result.BaggageIncludedInFare = onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                        }
                                    }

                                    foreach (Passenger pax in onResponse.Booking.Passengers)
                                    {
                                        foreach (PassengerFee fee in pax.PassengerFees)
                                        {
                                            foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                            {
                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                {
                                                    switch (charge.ChargeType)
                                                    {
                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                            result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[2].PassengerCount);
                                                            result.FareBreakdown[2].TotalFare += (double)result.FareBreakdown[2].BaseFare;
                                                            result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                            result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                            double tax = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[2].PassengerCount);
                                                            result.FareBreakdown[2].TotalFare += tax;
                                                            result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                            result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                    }
                                                }
                                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                {
                                                    switch (charge.ChargeType)
                                                    {
                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                            result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                            result.FareBreakdown[1].TotalFare += (double)result.FareBreakdown[1].BaseFare;
                                                            result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                            result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                            double tax = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * result.FareBreakdown[1].PassengerCount);
                                                            result.FareBreakdown[1].TotalFare += tax;
                                                            result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                            result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                            result.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }                                   
                                }
                                result.Price.SupplierCurrency = "INR";

                                result.BaseFare = (double)Math.Round(result.Price.PublishedFare, agentDecimalValue);
                                result.Tax = (double)Math.Round(result.Price.Tax, agentDecimalValue);
                                result.TotalFare = result.BaseFare + result.Tax;
                                ResultList.Add(result);                               
                            }
                        } 
                        #endregion
                        CombinedSearchResults.AddRange(ResultList);
                    }

                    results = CombinedSearchResults.ToArray();

                    Logout(loginResponse.Signature);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Search failed. Reason : " + ex.ToString(), ex);
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchForRegularFares(object eventNumber)
        {            
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "RS";

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[2];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                    //}
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                        string filePath = xmlLogPath + "SGSearchRequestRegular_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, availRequest);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                    }
                }

                
                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        string filePath = xmlLogPath + "SGSearchResponseRegular_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                        doc.Save(filePath);

                        //filePath = xmlLogPath + "SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                        //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                        //StreamWriter sw = new StreamWriter(filePath);
                        //ser.Serialize(sw, tripAvailResponse);
                        //sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                }

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                    rateOfExchange = exchangeRates["INR"];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                {
                                    PriceItineraryResponse onResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i]);

                                    foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                    {
                                        PriceItineraryResponse retResponse = GetItineraryPrice(request, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, ReturnJourneys[j]);

                                        SearchResult result = new SearchResult();

                                        result.ResultBookingSource = BookingSource.SpiceJet;
                                        result.Airline = "SG";
                                        result.Currency = agentBaseCurrency;
                                        result.EticketEligible = true;
                                        result.FareBreakdown = new Fare[fareBreakDownCount];
                                        result.FareRules = new List<FareRule>();
                                        result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                        result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                        if (request.Type == SearchType.OneWay)
                                        {
                                            result.Flights = new FlightInfo[1][];
                                        }
                                        else
                                        {
                                            result.Flights = new FlightInfo[2][];
                                        }
                                        result.Flights[0] = new FlightInfo[1];
                                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                        {
                                            result.Flights[0][k] = new FlightInfo();
                                            result.Flights[0][k].Airline = "SG";
                                            result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                            result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                            result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                            result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                            result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                            result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                            result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                            result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                            result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                            result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                            result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                            result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                            result.Flights[0][k].Group = 0;
                                            result.Flights[0][k].OperatingCarrier = "SG";
                                            result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                            result.Flights[0][k].Stops = 1;
                                            result.Flights[0][k].Status = "";
                                            result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                            result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                            result.NonRefundable = false;
                                            result.FareSellKey = onfare.FareSellKey;
                                            result.Price = new PriceAccounts();
                                            int count = 0;
                                            foreach (PaxFare fare in onfare.PaxFares)
                                            {
                                                switch (fare.PaxType)
                                                {
                                                    case "ADT":
                                                        if (result.FareBreakdown[0] == null)
                                                        {
                                                            result.FareBreakdown[0] = new Fare();
                                                        }
                                                        result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                        result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                        break;
                                                    case "CHD":
                                                        if (result.FareBreakdown[1] == null)
                                                        {
                                                            result.FareBreakdown[1] = new Fare();
                                                        }
                                                        result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                        result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                        break;
                                                }
                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                {
                                                    result.FareBreakdown[2] = new Fare();
                                                    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                }
                                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                {
                                                    result.FareBreakdown[1] = new Fare();
                                                    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                }
                                                if (onResponse != null)
                                                {
                                                    foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                    {
                                                        if (fare.PaxType == pfare.PaxType)
                                                        {
                                                            foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                            {
                                                                switch (fare.PaxType)
                                                                {
                                                                    case "ADT":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;
                                                                    case "CHD":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;

                                                                }
                                                            }
                                                        }
                                                    }


                                                    result.BaggageIncludedInFare = onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                                }
                                                count++;
                                            }

                                            foreach (Passenger pax in onResponse.Booking.Passengers)
                                            {
                                                foreach (PassengerFee fee in pax.PassengerFees)
                                                {
                                                    foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                        result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                        result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                        result.FareSellKey += "|" + retFare.FareSellKey;
                                        result.Flights[1] = new FlightInfo[1];
                                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                        {
                                            result.Flights[1][k] = new FlightInfo();
                                            result.Flights[1][k].Airline = "SG";
                                            result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                            result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                            result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                            result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                            result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                            result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                            result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                            result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                            result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                            result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                            result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                            result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                            result.Flights[1][k].Group = 1;
                                            result.Flights[1][k].OperatingCarrier = "SG";
                                            result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                            result.Flights[1][k].Stops = 1;
                                            result.Flights[1][k].Status = "";
                                            result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                            result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                            result.NonRefundable = false;
                                            result.FareSellKey += "|" + retFare.FareSellKey;
                                            int count = 0;
                                            foreach (PaxFare fare in retFare.PaxFares)
                                            {

                                                if (retResponse != null)
                                                {
                                                    foreach (PaxFare pfare in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                    {
                                                        if (fare.PaxType == pfare.PaxType)
                                                        {
                                                            foreach (BookingServiceCharge charge in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                            {
                                                                switch (fare.PaxType)
                                                                {
                                                                    case "ADT":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;
                                                                    case "CHD":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }



                                                    result.BaggageIncludedInFare += "," + onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                }
                                                count++;
                                            }

                                            foreach (Passenger pax in retResponse.Booking.Passengers)
                                            {
                                                foreach (PassengerFee fee in pax.PassengerFees)
                                                {
                                                    foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:

                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        result.Price.SupplierCurrency = "INR";
                                        result.BaseFare = (double)Math.Round(result.Price.PublishedFare, agentDecimalValue);
                                        result.Tax = (double)Math.Round(result.Price.Tax, agentDecimalValue);
                                        result.TotalFare = result.BaseFare + result.Tax;
                                        ResultList.Add(result);
                                    }
                                }
                            }
                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Search failed. Reason : " + ex.ToString(), ex);
            }         
        }

        public void SearchForFlexFares(object eventNumber)
        {            
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "HF";

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[2];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                    //}
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                        string filePath = xmlLogPath + "SGSearchRequestFlex_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, availRequest);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                    }
                }

                
                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        string filePath = xmlLogPath + "SGSearchResponseFlex_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                        doc.Save(filePath);

                        //filePath = xmlLogPath + "SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                        //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                        //StreamWriter sw = new StreamWriter(filePath);
                        //ser.Serialize(sw, tripAvailResponse);
                        //sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                }

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                    rateOfExchange = exchangeRates["INR"];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                {
                                    PriceItineraryResponse onResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i]);

                                    foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                    {
                                        PriceItineraryResponse retResponse = GetItineraryPrice(request, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, ReturnJourneys[j]);

                                        SearchResult result = new SearchResult();

                                        result.ResultBookingSource = BookingSource.SpiceJet;
                                        result.Airline = "SG";
                                        result.Currency = agentBaseCurrency;
                                        result.EticketEligible = true;
                                        result.FareBreakdown = new Fare[fareBreakDownCount];
                                        result.FareRules = new List<FareRule>();
                                        result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                        result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                        if (request.Type == SearchType.OneWay)
                                        {
                                            result.Flights = new FlightInfo[1][];
                                        }
                                        else
                                        {
                                            result.Flights = new FlightInfo[2][];
                                        }
                                        result.Flights[0] = new FlightInfo[1];
                                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                        {

                                            result.Flights[0][k] = new FlightInfo();
                                            result.Flights[0][k].Airline = "SG";
                                            result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                            result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                            result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                            result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                            result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                            result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                            result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                            result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                            result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                            result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                            result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                            result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                            result.Flights[0][k].Group = 0;
                                            result.Flights[0][k].OperatingCarrier = "SG";
                                            result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                            result.Flights[0][k].Stops = 1;
                                            result.Flights[0][k].Status = "";
                                            result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                            result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                            result.NonRefundable = false;
                                            result.FareSellKey = onfare.FareSellKey;
                                            result.Price = new PriceAccounts();
                                            int count = 0;
                                            foreach (PaxFare fare in onfare.PaxFares)
                                            {
                                                switch (fare.PaxType)
                                                {
                                                    case "ADT":
                                                        if (result.FareBreakdown[0] == null)
                                                        {
                                                            result.FareBreakdown[0] = new Fare();
                                                        }
                                                        result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                        result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                        break;
                                                    case "CHD":
                                                        if (result.FareBreakdown[1] == null)
                                                        {
                                                            result.FareBreakdown[1] = new Fare();
                                                        }
                                                        result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                        result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                        break;
                                                }
                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                {
                                                    result.FareBreakdown[2] = new Fare();
                                                    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                }
                                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                {
                                                    result.FareBreakdown[1] = new Fare();
                                                    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                }
                                                if (onResponse != null)
                                                {
                                                    foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                    {
                                                        if (fare.PaxType == pfare.PaxType)
                                                        {
                                                            foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                            {
                                                                switch (fare.PaxType)
                                                                {
                                                                    case "ADT":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;
                                                                    case "CHD":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;

                                                                }
                                                            }
                                                        }
                                                    }


                                                    result.BaggageIncludedInFare = onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                                }
                                                count++;
                                            }

                                            foreach (Passenger pax in onResponse.Booking.Passengers)
                                            {
                                                foreach (PassengerFee fee in pax.PassengerFees)
                                                {
                                                    foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                        result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                        result.FareSellKey += "|" + retFare.FareSellKey;
                                        result.Flights[1] = new FlightInfo[1];
                                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                        {
                                            //if (retFare.ProductClass == ReturnJourneys[j].Segments[k].Fares[0].ProductClass)
                                            {
                                                result.Flights[1][k] = new FlightInfo();
                                                result.Flights[1][k].Airline = "SG";
                                                result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                result.Flights[1][k].Group = 1;
                                                result.Flights[1][k].OperatingCarrier = "SG";
                                                result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                result.Flights[1][k].Stops = 1;
                                                result.Flights[1][k].Status = "";
                                                result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                result.NonRefundable = false;
                                                result.FareSellKey += "|" + retFare.FareSellKey;
                                                int count = 0;
                                                foreach (PaxFare fare in retFare.PaxFares)
                                                {

                                                    if (retResponse != null)
                                                    {
                                                        foreach (PaxFare pfare in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                        {
                                                            if (fare.PaxType == pfare.PaxType)
                                                            {
                                                                foreach (BookingServiceCharge charge in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                                {
                                                                    switch (fare.PaxType)
                                                                    {
                                                                        case "ADT":
                                                                            switch (charge.ChargeType)
                                                                            {
                                                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                    result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                    result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        case "CHD":
                                                                            switch (charge.ChargeType)
                                                                            {
                                                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                    result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                    result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                }
                                                            }
                                                        }



                                                        result.BaggageIncludedInFare += "," + onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                    }
                                                    count++;
                                                }

                                                foreach (Passenger pax in retResponse.Booking.Passengers)
                                                {
                                                    foreach (PassengerFee fee in pax.PassengerFees)
                                                    {
                                                        foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                        {
                                                            if (request.ChildCount > 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                        result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:

                                                                        result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                }
                                                            }
                                                            else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                        result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                        result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                        result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        result.Price.SupplierCurrency = "INR";
                                        result.BaseFare = (double)Math.Round(result.Price.PublishedFare, agentDecimalValue);
                                        result.Tax = (double)Math.Round(result.Price.Tax, agentDecimalValue);
                                        result.TotalFare = result.BaseFare + result.Tax;

                                        ResultList.Add(result);
                                    }
                                }
                            }
                        }
                    }
                }
                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Search failed. Reason : " + ex.ToString(), ex);
            }            
        }

        public void SearchForFamilyFares(object eventNumber)
        {            
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "XB";

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[2];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                    //}
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                        string filePath = xmlLogPath + "SGSearchRequestFamily_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, availRequest);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                    }
                }

                
                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        string filePath = xmlLogPath + "SGSearchResponseFamily_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                        doc.Save(filePath);

                        //filePath = xmlLogPath + "SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                        //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                        //StreamWriter sw = new StreamWriter(filePath);
                        //ser.Serialize(sw, tripAvailResponse);
                        //sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                }

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                    rateOfExchange = exchangeRates["INR"];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                {
                                    PriceItineraryResponse onResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i]);

                                    foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                    {
                                        PriceItineraryResponse retResponse = GetItineraryPrice(request, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, ReturnJourneys[j]);

                                        //if ((onfare.ProductClass == "XA" && retFare.ProductClass == "XA"))
                                        {
                                            SearchResult result = new SearchResult();

                                            result.ResultBookingSource = BookingSource.SpiceJet;
                                            result.Airline = "SG";
                                            result.Currency = agentBaseCurrency;
                                            result.EticketEligible = true;
                                            result.FareBreakdown = new Fare[fareBreakDownCount];
                                            result.FareRules = new List<FareRule>();
                                            result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                            result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                            if (request.Type == SearchType.OneWay)
                                            {
                                                result.Flights = new FlightInfo[1][];
                                            }
                                            else
                                            {
                                                result.Flights = new FlightInfo[2][];
                                            }
                                            result.Flights[0] = new FlightInfo[1];
                                            for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                            {

                                                result.Flights[0][k] = new FlightInfo();
                                                result.Flights[0][k].Airline = "SG";
                                                result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                                result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                                result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                                result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                result.Flights[0][k].Group = 0;
                                                result.Flights[0][k].OperatingCarrier = "SG";
                                                result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                result.Flights[0][k].Stops = 1;
                                                result.Flights[0][k].Status = "";
                                                result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                result.NonRefundable = false;
                                                result.FareSellKey = onfare.FareSellKey;
                                                result.Price = new PriceAccounts();
                                                int count = 0;
                                                foreach (PaxFare fare in onfare.PaxFares)
                                                {
                                                    switch (fare.PaxType)
                                                    {
                                                        case "ADT":
                                                            if (result.FareBreakdown[0] == null)
                                                            {
                                                                result.FareBreakdown[0] = new Fare();
                                                            }
                                                            result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                            result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                            break;
                                                        case "CHD":
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                            break;
                                                    }
                                                    if (request.ChildCount > 0 && request.InfantCount > 0)
                                                    {
                                                        result.FareBreakdown[2] = new Fare();
                                                        result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                        result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                    }
                                                    else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                    {
                                                        result.FareBreakdown[1] = new Fare();
                                                        result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                        result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                    }
                                                    if (onResponse != null)
                                                    {
                                                        foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                        {
                                                            if (fare.PaxType == pfare.PaxType)
                                                            {
                                                                foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                                {
                                                                    switch (fare.PaxType)
                                                                    {
                                                                        case "ADT":
                                                                            switch (charge.ChargeType)
                                                                            {
                                                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                    result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                    result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        case "CHD":
                                                                            switch (charge.ChargeType)
                                                                            {
                                                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                    result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                    result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                            }
                                                                            break;

                                                                    }
                                                                }
                                                            }
                                                        }


                                                        result.BaggageIncludedInFare = onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                                    }
                                                    count++;
                                                }

                                                foreach (Passenger pax in onResponse.Booking.Passengers)
                                                {
                                                    foreach (PassengerFee fee in pax.PassengerFees)
                                                    {
                                                        foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                        {
                                                            if (request.ChildCount > 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                        result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                }
                                                            }
                                                            else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                        result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                            result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                            result.FareSellKey += "|" + retFare.FareSellKey;
                                            result.Flights[1] = new FlightInfo[1];
                                            for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                            {
                                                //if (retFare.ProductClass == ReturnJourneys[j].Segments[k].Fares[0].ProductClass)
                                                {
                                                    result.Flights[1][k] = new FlightInfo();
                                                    result.Flights[1][k].Airline = "SG";
                                                    result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                    result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                    result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                    result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                    result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                    result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                    result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[1][k].Group = 1;
                                                    result.Flights[1][k].OperatingCarrier = "SG";
                                                    result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                    result.Flights[1][k].Stops = 1;
                                                    result.Flights[1][k].Status = "";
                                                    result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                    result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey += "|" + retFare.FareSellKey;
                                                    int count = 0;
                                                    foreach (PaxFare fare in retFare.PaxFares)
                                                    {

                                                        if (retResponse != null)
                                                        {
                                                            foreach (PaxFare pfare in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                            {
                                                                if (fare.PaxType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (fare.PaxType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                        result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                        result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                }
                                                                                break;
                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                        result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                        result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }



                                                            result.BaggageIncludedInFare += "," + onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                        }
                                                        count++;
                                                    }

                                                    foreach (Passenger pax in retResponse.Booking.Passengers)
                                                    {
                                                        foreach (PassengerFee fee in pax.PassengerFees)
                                                        {
                                                            foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                            {
                                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                                {
                                                                    switch (charge.ChargeType)
                                                                    {
                                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                            result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                            break;
                                                                        case SpiceJet.SGBooking.ChargeType.Tax:

                                                                            result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                            result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                            break;
                                                                    }
                                                                }
                                                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                                {
                                                                    switch (charge.ChargeType)
                                                                    {
                                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                            result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                            result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                            break;
                                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                            result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                            result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                            break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            result.Price.SupplierCurrency = "INR";
                                            result.BaseFare = (double)Math.Round(result.Price.PublishedFare, agentDecimalValue);
                                            result.Tax = (double)Math.Round(result.Price.Tax, agentDecimalValue);
                                            result.TotalFare = result.BaseFare + result.Tax;

                                            ResultList.Add(result);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Search failed. Reason : " + ex.ToString(), ex);
            }            
        }

        public void SearchForSpecialReturnFares(object eventNumber)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "XA";

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[2];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                    //}
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                        string filePath = xmlLogPath + "SGSearchRequestSpecialRT_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, availRequest);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                    }
                }

                
                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        string filePath = xmlLogPath + "SGSearchResponseSpecialRT_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                        doc.Save(filePath);

                        //filePath = xmlLogPath + "SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                        //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                        //StreamWriter sw = new StreamWriter(filePath);
                        //ser.Serialize(sw, tripAvailResponse);
                        //sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                }

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                    rateOfExchange = exchangeRates["INR"];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                {
                                    PriceItineraryResponse onResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i]);

                                    foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                    {
                                        PriceItineraryResponse retResponse = GetItineraryPrice(request, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, ReturnJourneys[j]);
                                        //if ((onfare.ProductClass == "XA" && retFare.ProductClass == "XA"))
                                        {
                                            SearchResult result = new SearchResult();

                                            result.ResultBookingSource = BookingSource.SpiceJet;
                                            result.Airline = "SG";
                                            result.Currency = agentBaseCurrency;
                                            result.EticketEligible = true;
                                            result.FareBreakdown = new Fare[fareBreakDownCount];
                                            result.FareRules = new List<FareRule>();
                                            result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                            result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                            if (request.Type == SearchType.OneWay)
                                            {
                                                result.Flights = new FlightInfo[1][];
                                            }
                                            else
                                            {
                                                result.Flights = new FlightInfo[2][];
                                            }
                                            result.Flights[0] = new FlightInfo[1];
                                            for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                            {

                                                result.Flights[0][k] = new FlightInfo();
                                                result.Flights[0][k].Airline = "SG";
                                                result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                                result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                                result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                                result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                                result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                                result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                                result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                                result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                                result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                                result.Flights[0][k].Group = 0;
                                                result.Flights[0][k].OperatingCarrier = "SG";
                                                result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                                result.Flights[0][k].Stops = 1;
                                                result.Flights[0][k].Status = "";
                                                result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                                result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                result.NonRefundable = false;
                                                result.FareSellKey = onfare.FareSellKey;
                                                result.Price = new PriceAccounts();
                                                int count = 0;
                                                foreach (PaxFare fare in onfare.PaxFares)
                                                {
                                                    switch (fare.PaxType)
                                                    {
                                                        case "ADT":
                                                            if (result.FareBreakdown[0] == null)
                                                            {
                                                                result.FareBreakdown[0] = new Fare();
                                                            }
                                                            result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                            result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                            break;
                                                        case "CHD":
                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                            break;
                                                    }
                                                    if (request.ChildCount > 0 && request.InfantCount > 0)
                                                    {
                                                        result.FareBreakdown[2] = new Fare();
                                                        result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                        result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                    }
                                                    else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                    {
                                                        result.FareBreakdown[1] = new Fare();
                                                        result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                        result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                    }
                                                    if (onResponse != null)
                                                    {
                                                        foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                        {
                                                            if (fare.PaxType == pfare.PaxType)
                                                            {
                                                                foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                                {
                                                                    switch (fare.PaxType)
                                                                    {
                                                                        case "ADT":
                                                                            switch (charge.ChargeType)
                                                                            {
                                                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                    result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                    result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        case "CHD":
                                                                            switch (charge.ChargeType)
                                                                            {
                                                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                    result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                    result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                    result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                                    break;
                                                                            }
                                                                            break;

                                                                    }
                                                                }
                                                            }
                                                        }


                                                        result.BaggageIncludedInFare = onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                                    }
                                                    count++;
                                                }

                                                foreach (Passenger pax in onResponse.Booking.Passengers)
                                                {
                                                    foreach (PassengerFee fee in pax.PassengerFees)
                                                    {
                                                        foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                        {
                                                            if (request.ChildCount > 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                        result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                }
                                                            }
                                                            else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                            {
                                                                switch (charge.ChargeType)
                                                                {
                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                        result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                        result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                        result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                        result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                            result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                            result.FareSellKey += "|" + retFare.FareSellKey;
                                            result.Flights[1] = new FlightInfo[1];
                                            for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                            {
                                                //if (retFare.ProductClass == ReturnJourneys[j].Segments[k].Fares[0].ProductClass)
                                                {
                                                    result.Flights[1][k] = new FlightInfo();
                                                    result.Flights[1][k].Airline = "SG";
                                                    result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                                    result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                                    result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                                    result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                                    result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                                    result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                                    result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                                    result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                                    result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                                    result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                                    result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                                    result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                                    result.Flights[1][k].Group = 1;
                                                    result.Flights[1][k].OperatingCarrier = "SG";
                                                    result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                                    result.Flights[1][k].Stops = 1;
                                                    result.Flights[1][k].Status = "";
                                                    result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                                    result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.NonRefundable = false;
                                                    result.FareSellKey += "|" + retFare.FareSellKey;
                                                    int count = 0;
                                                    foreach (PaxFare fare in retFare.PaxFares)
                                                    {

                                                        if (retResponse != null)
                                                        {
                                                            foreach (PaxFare pfare in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                            {
                                                                if (fare.PaxType == pfare.PaxType)
                                                                {
                                                                    foreach (BookingServiceCharge charge in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                                    {
                                                                        switch (fare.PaxType)
                                                                        {
                                                                            case "ADT":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                        result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                        result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                }
                                                                                break;
                                                                            case "CHD":
                                                                                switch (charge.ChargeType)
                                                                                {
                                                                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                        result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                        result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                    case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                    case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                    case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                        result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                        result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                        result.Price.SupplierPrice += charge.Amount;
                                                                                        break;
                                                                                }
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }



                                                            result.BaggageIncludedInFare += "," + onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                        }
                                                        count++;
                                                    }

                                                    foreach (Passenger pax in retResponse.Booking.Passengers)
                                                    {
                                                        foreach (PassengerFee fee in pax.PassengerFees)
                                                        {
                                                            foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                            {
                                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                                {
                                                                    switch (charge.ChargeType)
                                                                    {
                                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                            result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                            break;
                                                                        case SpiceJet.SGBooking.ChargeType.Tax:

                                                                            result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                            result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                            break;
                                                                    }
                                                                }
                                                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                                {
                                                                    switch (charge.ChargeType)
                                                                    {
                                                                        case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                            result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                            result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                            break;
                                                                        case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                            result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                            result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                            result.Price.SupplierPrice += charge.Amount;
                                                                            break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            result.Price.SupplierCurrency = "INR";
                                            result.BaseFare = (double)Math.Round(result.Price.PublishedFare, agentDecimalValue);
                                            result.Tax = (double)Math.Round(result.Price.Tax, agentDecimalValue);
                                            result.TotalFare = result.BaseFare + result.Tax;

                                            ResultList.Add(result);
                                        }
                                    }
                                }
                            }
                        }
                    }                   
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Search failed. Reason : " + ex.ToString(), ex);
            }
        }

        public void SearchForHandBaggageFares(object eventNumber)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }

                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = "INR";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "HO";

                    //if ((request.AdultCount + request.ChildCount) >= 4)
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";
                    //}
                    //else
                    //{
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[2];
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                    //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";
                    //}
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                        string filePath = xmlLogPath + "SGSearchRequestHandBaggage_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, availRequest);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                    }
                }

                
                TripAvailabilityResponse tripAvailResponse = bookingAPI.GetAvailability(contractVersion, loginSignature, availRequest.TripAvailabilityRequest);

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        string filePath = xmlLogPath + "SGSearchResponseHandBaggage_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                        doc.Save(filePath);

                        //filePath = xmlLogPath + "SGSearchResponse_" + DateTime.Now.AddMilliseconds(1000).ToString("yyyyMMdd_hhmmss") + ".xml";
                        //XmlSerializer ser = new XmlSerializer(typeof(TripAvailabilityResponse));                            
                        //StreamWriter sw = new StreamWriter(filePath);
                        //ser.Serialize(sw, tripAvailResponse);
                        //sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                }

                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<SpiceJet.SGBooking.Fare> onwardFares = new List<SpiceJet.SGBooking.Fare>();
                    List<SpiceJet.SGBooking.Fare> returnFares = new List<SpiceJet.SGBooking.Fare>();

                    rateOfExchange = exchangeRates["INR"];

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }

                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                foreach (SpiceJet.SGBooking.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                {
                                    PriceItineraryResponse onResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, onfare.FareSellKey, OnwardJourneys[i]);

                                    foreach (SpiceJet.SGBooking.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                    {
                                        PriceItineraryResponse retResponse = GetItineraryPrice(request, ReturnJourneys[j].JourneySellKey, retFare.FareSellKey, ReturnJourneys[j]);

                                        SearchResult result = new SearchResult();

                                        result.ResultBookingSource = BookingSource.SpiceJet;
                                        result.Airline = "SG";
                                        result.Currency = agentBaseCurrency;
                                        result.EticketEligible = true;
                                        result.FareBreakdown = new Fare[fareBreakDownCount];
                                        result.FareRules = new List<FareRule>();
                                        result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                        result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                        if (request.Type == SearchType.OneWay)
                                        {
                                            result.Flights = new FlightInfo[1][];
                                        }
                                        else
                                        {
                                            result.Flights = new FlightInfo[2][];
                                        }
                                        result.Flights[0] = new FlightInfo[1];
                                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                                        {
                                            result.Flights[0][k] = new FlightInfo();
                                            result.Flights[0][k].Airline = "SG";
                                            result.Flights[0][k].ArrivalTime = OnwardJourneys[i].Segments[k].Legs[0].STA;
                                            result.Flights[0][k].ArrTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                            result.Flights[0][k].BookingClass = (OnwardJourneys[i].Segments[k].CabinOfService != null && OnwardJourneys[i].Segments[k].CabinOfService.Length > 0 ? OnwardJourneys[i].Segments[k].CabinOfService : "");
                                            result.Flights[0][k].CabinClass = OnwardJourneys[i].Segments[k].CabinOfService;
                                            result.Flights[0][k].Craft = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + OnwardJourneys[i].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                            result.Flights[0][k].DepartureTime = OnwardJourneys[i].Segments[k].STD;
                                            result.Flights[0][k].DepTerminal = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                            result.Flights[0][k].Destination = new Airport(OnwardJourneys[i].Segments[k].ArrivalStation);
                                            result.Flights[0][k].Duration = result.Flights[0][k].ArrivalTime.Subtract(result.Flights[0][k].DepartureTime);
                                            result.Flights[0][k].ETicketEligible = OnwardJourneys[i].Segments[k].Legs[0].LegInfo.ETicket;
                                            result.Flights[0][k].FlightNumber = OnwardJourneys[i].Segments[k].FlightDesignator.FlightNumber;
                                            result.Flights[0][k].FlightStatus = FlightStatus.Confirmed;
                                            result.Flights[0][k].Group = 0;
                                            result.Flights[0][k].OperatingCarrier = "SG";
                                            result.Flights[0][k].Origin = new Airport(OnwardJourneys[i].Segments[k].DepartureStation);
                                            result.Flights[0][k].Stops = 1;
                                            result.Flights[0][k].Status = "";
                                            result.Flights[0][k].UapiSegmentRefKey = OnwardJourneys[i].Segments[k].SegmentSellKey;
                                            result.Flights[0][k].SegmentFareType = GetFareTypeForProductClass(onfare.ProductClass);
                                            result.NonRefundable = false;
                                            result.FareSellKey = onfare.FareSellKey;
                                            result.Price = new PriceAccounts();
                                            int count = 0;
                                            foreach (PaxFare fare in onfare.PaxFares)
                                            {
                                                switch (fare.PaxType)
                                                {
                                                    case "ADT":
                                                        if (result.FareBreakdown[0] == null)
                                                        {
                                                            result.FareBreakdown[0] = new Fare();
                                                        }
                                                        result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                        result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                        break;
                                                    case "CHD":
                                                        if (result.FareBreakdown[1] == null)
                                                        {
                                                            result.FareBreakdown[1] = new Fare();
                                                        }
                                                        result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                        result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                        break;
                                                }
                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                {
                                                    result.FareBreakdown[2] = new Fare();
                                                    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                }
                                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                {
                                                    result.FareBreakdown[1] = new Fare();
                                                    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                }
                                                if (onResponse != null)
                                                {
                                                    foreach (PaxFare pfare in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                    {
                                                        if (fare.PaxType == pfare.PaxType)
                                                        {
                                                            foreach (BookingServiceCharge charge in onResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                            {
                                                                switch (fare.PaxType)
                                                                {
                                                                    case "ADT":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;
                                                                    case "CHD":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;

                                                                }
                                                            }
                                                        }
                                                    }


                                                    result.BaggageIncludedInFare = onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                                }
                                                count++;
                                            }

                                            foreach (Passenger pax in onResponse.Booking.Passengers)
                                            {
                                                foreach (PassengerFee fee in pax.PassengerFees)
                                                {
                                                    foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                        result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                        result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                        result.FareSellKey += "|" + retFare.FareSellKey;
                                        result.Flights[1] = new FlightInfo[1];
                                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                                        {
                                            result.Flights[1][k] = new FlightInfo();
                                            result.Flights[1][k].Airline = "SG";
                                            result.Flights[1][k].ArrivalTime = ReturnJourneys[j].Segments[k].Legs[0].STA;
                                            result.Flights[1][k].ArrTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ArrivalTerminal;
                                            result.Flights[1][k].BookingClass = (ReturnJourneys[j].Segments[k].CabinOfService != null && ReturnJourneys[j].Segments[k].CabinOfService.Length > 0 ? ReturnJourneys[j].Segments[k].CabinOfService : "");
                                            result.Flights[1][k].CabinClass = ReturnJourneys[j].Segments[k].CabinOfService;
                                            result.Flights[1][k].Craft = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentType + "-" + ReturnJourneys[j].Segments[k].Legs[0].LegInfo.EquipmentTypeSuffix;
                                            result.Flights[1][k].DepartureTime = ReturnJourneys[j].Segments[k].STD;
                                            result.Flights[1][k].DepTerminal = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.DepartureTerminal;
                                            result.Flights[1][k].Destination = new Airport(ReturnJourneys[j].Segments[k].ArrivalStation);
                                            result.Flights[1][k].Duration = result.Flights[1][k].ArrivalTime.Subtract(result.Flights[1][k].DepartureTime);
                                            result.Flights[1][k].ETicketEligible = ReturnJourneys[j].Segments[k].Legs[0].LegInfo.ETicket;
                                            result.Flights[1][k].FlightNumber = ReturnJourneys[j].Segments[k].FlightDesignator.FlightNumber;
                                            result.Flights[1][k].FlightStatus = FlightStatus.Confirmed;
                                            result.Flights[1][k].Group = 1;
                                            result.Flights[1][k].OperatingCarrier = "SG";
                                            result.Flights[1][k].Origin = new Airport(ReturnJourneys[j].Segments[k].DepartureStation);
                                            result.Flights[1][k].Stops = 1;
                                            result.Flights[1][k].Status = "";
                                            result.Flights[1][k].UapiSegmentRefKey = ReturnJourneys[j].Segments[k].SegmentSellKey;
                                            result.Flights[1][k].SegmentFareType = GetFareTypeForProductClass(retFare.ProductClass);
                                            result.NonRefundable = false;
                                            result.FareSellKey += "|" + retFare.FareSellKey;
                                            int count = 0;
                                            foreach (PaxFare fare in retFare.PaxFares)
                                            {

                                                if (retResponse != null)
                                                {
                                                    foreach (PaxFare pfare in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares)
                                                    {
                                                        if (fare.PaxType == pfare.PaxType)
                                                        {
                                                            foreach (BookingServiceCharge charge in retResponse.Booking.Journeys[0].Segments[0].Fares[0].PaxFares[0].ServiceCharges)
                                                            {
                                                                switch (fare.PaxType)
                                                                {
                                                                    case "ADT":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[0].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[0].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[0].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;
                                                                    case "CHD":
                                                                        switch (charge.ChargeType)
                                                                        {
                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                result.FareBreakdown[1].BaseFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.PublishedFare += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                result.FareBreakdown[1].TotalFare += (double)Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                                result.Price.Tax += Math.Round((charge.Amount * rateOfExchange), agentDecimalValue);
                                                                                result.Price.SupplierPrice += charge.Amount;
                                                                                break;
                                                                        }
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }



                                                    result.BaggageIncludedInFare += "," + onResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight;
                                                }
                                                count++;
                                            }

                                            foreach (Passenger pax in retResponse.Booking.Passengers)
                                            {
                                                foreach (PassengerFee fee in pax.PassengerFees)
                                                {
                                                    foreach (BookingServiceCharge charge in fee.ServiceCharges)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[2].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:

                                                                    result.FareBreakdown[2].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[2].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {
                                                            switch (charge.ChargeType)
                                                            {
                                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                    result.FareBreakdown[1].BaseFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.PublishedFare += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                                    result.FareBreakdown[1].TotalFare += (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue)); ;
                                                                    result.FareBreakdown[1].SupplierFare += (double)charge.Amount;
                                                                    result.Price.Tax += (Math.Round((charge.Amount * rateOfExchange), agentDecimalValue));
                                                                    result.Price.SupplierPrice += charge.Amount;
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        result.Price.SupplierCurrency = "INR";
                                        result.BaseFare = (double)Math.Round(result.Price.PublishedFare, agentDecimalValue);
                                        result.Tax = (double)Math.Round(result.Price.Tax, agentDecimalValue);
                                        result.TotalFare = result.BaseFare + result.Tax;
                                        ResultList.Add(result);
                                    }
                                }
                            }
                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Search failed. Reason : " + ex.ToString(), ex);
            }
        }

        private string GetFareTypeForProductClass(string productClass)
        {
            string fareType ="";
            switch (productClass)
            {
                case "RS":
                    fareType = "Regular Fare";
                    break;
                case "XA":
                    fareType = "Special Return Trip";
                    break;
                case "HF":
                    fareType = "SpiceFlex Fare";
                    break;
                case "XB":
                    fareType = "Family & Friends";
                    break;
                case "HO":
                    fareType = "HandBaggage Only";
                    break;
                case "SS":
                    fareType = "Promo Fare";
                    break;
            }
            return fareType;
        }

        /// <summary>
        /// Returns the fare breakup for the segment
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPrice(SearchRequest request, string journeySellKey, string fareSellKey, Journey journey)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            string jkey = journeySellKey;

            string journeykey = jkey.Replace("~", "_").Replace(" ", "_");
            journeykey = journeykey.Replace("/", "_");
            journeykey = journeykey.Replace(":", "_");
            if (pricedItineraries.ContainsKey(journeykey + "|" + fareSellKey))
            {
                piResponse = pricedItineraries[journeykey + "|" + fareSellKey] as PriceItineraryResponse;
            }
            else
            {
                IBookingManager bookingAPI = this.bookingAPI;
                
                int paxCount = request.AdultCount + request.ChildCount;

                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = loginSignature;
                priceItinRequest.ContractVersion = 340;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;
                
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = "INR";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(paxCount + request.InfantCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;


                for (int i = 0; i < request.AdultCount; i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                }
                for (int i = request.AdultCount; i < (paxCount); i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                }
                for (int i = 0; i < request.InfantCount; i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = "INR";
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[1];
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0] = new SegmentSSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].ArrivalStation = journey.Segments[0].ArrivalStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].DepartureStation = journey.Segments[0].DepartureStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].FlightDesignator = journey.Segments[0].FlightDesignator;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].STD = journey.Segments[0].STD;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs = new PaxSSR[request.InfantCount];
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs[i] = new PaxSSR();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs[i].ActionStatusCode = "NN";
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs[i].ArrivalStation = journey.Segments[0].ArrivalStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs[i].DepartureStation = journey.Segments[0].DepartureStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs[i].PassengerNumber = (Int16)i;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs[i].SSRCode = "INFT";
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs[i].SSRNumber = 0;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[0].PaxSSRs[i].SSRValue = 0;
                }

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                    string filePath = xmlLogPath + "SGGetItineraryPriceRequest_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, priceItinRequest);
                    sw.Close();
                }
                catch { }


                try
                {
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                    pricedItineraries.Add(journeykey + "|" + fareSellKey, piResponse);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + "SGGetItineraryPriceResponse_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "__" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                }
            }

            return piResponse;
        }

        /// <summary>
        /// Returns the baggage details
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetBaggage(SearchResult result)
        {
            DataTable dtSourceBaggage = SourceBaggage.GetSourceBaggages(BookingSource.SpiceJet);

            rateOfExchange = exchangeRates[dtSourceBaggage.Rows[0]["BaggageCurrency"].ToString()];

            

            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    DataRow[] baggages = new DataRow[0];
                    if (result.Flights[i][j].Origin.CountryCode == "IN" && result.Flights[i][j].Destination.CountryCode == "IN")
                    {
                        baggages = dtSourceBaggage.Select("IsDomestic=1");
                    }
                    else
                    {
                        baggages = dtSourceBaggage.Select("IsDomestic=0");
                    }

                    if(!result.Flights[i][j].SegmentFareType.Contains("HandBaggage"))
                    {
                        foreach (DataRow row in baggages)
                        {
                            DataRow dr = dtBaggage.NewRow();
                            dr["Code"] = row["BaggageCode"];
                            dr["Price"] = Math.Round(Convert.ToDecimal(row["BaggagePrice"]) * rateOfExchange, agentDecimalValue);
                            dr["Group"] = result.Flights[i][j].Group;
                            dr["Currency"] = agentBaseCurrency;
                            switch (row["BaggageCode"].ToString())
                            {
                                case "EB05":
                                    dr["Description"] = "5Kg Excess Baggage";
                                    break;
                                case "EB10":
                                    dr["Description"] = "10Kg Excess Baggage";
                                    break;
                                case "EB15":
                                    dr["Description"] = "15Kg Excess Baggage";
                                    break;
                                case "EB20":
                                    dr["Description"] = "20Kg Excess Baggage";
                                    break;
                                case "EB30":
                                    dr["Description"] = "30Kg Excess Baggage";
                                    break;
                            }
                            dr["QtyAvailable"] = 5000;
                            dtBaggage.Rows.Add(dr);
                        }
                    }
                }
            }

            dtBaggage.DefaultView.Sort = "Code ASC,Group ASC";

            return dtBaggage;

        }

        
        /// <summary>
        /// Depricated
        /// </summary>
        /// <param name="result"></param>
        /// <param name="request"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData GetInfantFares(SearchRequest request, Journey journey, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {                
                SellRequestData requestData = new SellRequestData();

                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CurrencyCode = "INR";
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[journey.Segments.Length];

                for (int i = 0; i < journey.Segments.Length; i++)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = journey.Segments[i].ArrivalStation;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = journey.Segments[i].DepartureStation;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = journey.Segments[i].FlightDesignator.CarrierCode.Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = journey.Segments[i].FlightDesignator.FlightNumber.Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.OpSuffix = journey.Segments[i].FlightDesignator.OpSuffix;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = journey.Segments[i].STD;

                    for (int j = 0; j < request.InfantCount; j++)
                    {                       
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[request.InfantCount];
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j] = new PaxSSR();
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].State = MessageState.Modified;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].ActionStatusCode = "NN";
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].ArrivalStation = journey.Segments[i].ArrivalStation;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].DepartureStation = journey.Segments[i].DepartureStation;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].PassengerNumber = (Int16)j;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].SSRCode = "INFT";
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].SSRValue = 0;
                    }
                }
                

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    XmlSerializer ser = new XmlSerializer(typeof(SellRequestData));
                    string filePath = xmlLogPath + "SGSSRInfantRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, requestData);
                    sw.Close();
                }

                BookingManagerClient service = new BookingManagerClient();
                responseData = service.Sell(contractVersion, signature, requestData);

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + "SGSSRInfantResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }

        /// <summary>
        /// Hold booking before ticket
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        public BookingUpdateResponseData Book(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                //LogonResponse loginResponse = Login();
                SellRequestData requestData = new SellRequestData();

                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = "INR";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[itinerary.Segments.Length];

                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[i] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[i].JourneySellKey = itinerary.Segments[i].UapiSegmentRefKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[i].FareSellKey = itinerary.TicketAdvisory.Split('|')[i];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[i].StandbyPriorityCode = "";
                }
                //int paxCount = itinerary.Passenger.Length;
                //requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = new PaxPriceType[paxCount];
                //requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxCount;
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        switch (itinerary.Passenger[i].Type)
                        {
                            case PassengerType.Adult:
                                priceType.PaxType = "ADT";
                                break;
                            case PassengerType.Child:
                                priceType.PaxType = "CHD";
                                break;
                            //case PassengerType.Infant:
                            //    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType[i].PaxType = "INFT";
                            //    break;
                        }
                        paxPriceTypes.Add(priceType);
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(SellRequestData));
                        string filePath = xmlLogPath + "SGBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, requestData);
                        sw.Close();
                    }
                }
                catch { }

                BookingManagerClient service = new BookingManagerClient();
                responseData = service.Sell(contractVersion, signature, requestData);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + "SGBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, responseData);
                        sw.Close();
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }

        /// <summary>
        /// Confirmation of Additional Baggage
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingUpdateResponseData BookBaggage(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            

            //LogonResponse loginResponse = Login();

            try
            {
                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = "INR";
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];
                int counter = 0, baggageCount=0;
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {                    
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();                    
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "SG";
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber.Replace("SG", "").Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                    List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                    for (int j = 0; j < itinerary.Passenger.Length; j++)
                    {
                        if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                        {
                            if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.State = MessageState.New;
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                //paxSSR.FeeCode = "";
                                //paxSSR.Note = "";
                                paxSSR.PassengerNumber = (Int16)j;
                                if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1)//Return
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[itinerary.Segments[i].Group];
                                }
                                else
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType;//Oneway
                                }
                                //paxSSR.SSRDetail = "";
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                PaxSSRs.Add(paxSSR);
                                baggageCount++;
                            }

                        }
                    }
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                    counter++;
                }
                

                //If Excess baggage is selected then book it otherwise skip this operation
                if (baggageCount > 0)
                {
                    try
                    {
                        if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(SellRequestData));
                            string filePath = xmlLogPath + "SGBookBaggageRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, requestData);
                            sw.Close();
                        }
                    }
                    catch { }

                    responseData = bookingAPI.Sell(contractVersion, signature, requestData);

                    try
                    {
                        if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                            string filePath = xmlLogPath + "SGBookBaggageResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, responseData);
                            sw.Close();
                        }
                    }
                    catch { }
                }
                else
                {
                    responseData = null;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to book Baggage. Reason : " + ex.ToString(), "");
            }
            
            //Logout(loginResponse.Signature);

            return responseData;
        }

        public decimal SellSSRForInfant(FlightItinerary itinerary, string signature)
        {
            decimal bookingAmount = 0;
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            

            //LogonResponse loginResponse = Login();

            try
            {
                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = "INR";
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];
                
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "SG";
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber.Replace("SG", "").Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                    //requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[itinerary.Passenger.Length];
                    //if (i == 0)
                    {
                        int counter = 0;
                        List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                        for (int j = 0; j < itinerary.Passenger.Length; j++)
                        {
                            if (itinerary.Passenger[j].Type == PassengerType.Infant)
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                paxSSR.FeeCode = "";
                                paxSSR.Note = "";
                                paxSSR.PassengerNumber = (Int16)counter;
                                paxSSR.SSRCode = "INFT";
                                paxSSR.SSRDetail = "";
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                counter++;
                                PaxSSRs.Add(paxSSR);
                            }
                        }
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                    }
                }                

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(SellRequestData));
                        string filePath = xmlLogPath + "SGBookInfantRequest_" +  DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, requestData);
                        sw.Close();
                    }
                }
                catch { }

                responseData = bookingAPI.Sell(contractVersion, signature, requestData);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + "SGBookInfantResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, responseData);
                        sw.Close();
                    }
                }
                catch { }
                bookingAmount += responseData.Success.PNRAmount.TotalCost;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to book Baggage. Reason : " + ex.ToString(), "");
            }

            //Logout(loginResponse.Signature);

            return bookingAmount;
        }

        /// <summary>
        /// Confirmation of Price
        /// </summary>
        /// <param name="bookingAmount"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private AddPaymentToBookingResponseData AddPaymentForBooking(decimal bookingAmount, string signature)
        {
            
            AddPaymentToBookingResponseData response = new AddPaymentToBookingResponseData();

            //LogonResponse loginResponse = Login();
            try
            {                
                AddPaymentToBookingRequestData request = new AddPaymentToBookingRequestData();
                request.AccountNumber = organisationCode;
                request.AccountNumberID = 0;               
                request.AuthorizationCode = "";
                request.Deposit = false;
                request.Expiration = new DateTime();
                request.Installments = 0;                
                request.MessageState = MessageState.New;
                request.ParentPaymentID = 0;
                request.PaymentAddresses = new PaymentAddress[0];
                request.PaymentFields = new PaymentField[0];
                request.PaymentMethodCode = "AG";
                request.PaymentMethodType = RequestPaymentMethodType.AgencyAccount;
                request.PaymentText = "";                
                request.QuotedAmount = bookingAmount;
                request.QuotedCurrencyCode = "INR";
                request.ReferenceType = PaymentReferenceType.Default;
                request.Status = BookingPaymentStatus.New;
                request.ThreeDSecureRequest = new ThreeDSecureRequest();
                request.WaiveFee = false;

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingRequestData));
                        string filePath = xmlLogPath + "SGAddPaymentToBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                }
                catch { }

                response = bookingAPI.AddPaymentToBooking(contractVersion, signature, request);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingResponseData));
                        string filePath = xmlLogPath + "SGAddPaymentToBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Add Payment for booking. Reason : " + ex.ToString(), "");
            }

            //Logout(loginResponse.Signature);

            return response;
        }

        /// <summary>
        /// Commit the booking and generate PNR.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse BookItinerary(ref FlightItinerary itinerary)
        {
            BookingResponse response = new BookingResponse();
            

            LogonResponse loginResponse = Login();

            BookingUpdateResponseData bookResponse = Book(itinerary, loginResponse.Signature);

            int infantCount = 0;
            foreach (FlightPassenger pax in itinerary.Passenger)
            {
                if (pax.Type == PassengerType.Infant)
                {
                    infantCount += 1;
                }
            }
            BookingUpdateResponseData bagResponse = BookBaggage(itinerary, loginResponse.Signature);

            if (bagResponse != null)
            {
                bookResponse = bagResponse;
            }

            decimal bookingAmount = 0;
            if (infantCount > 0)
            {
                //SellSSR by Infant
                bookingAmount = SellSSRForInfant(itinerary, loginResponse.Signature);
            }
            else
            {
                bookingAmount = bookResponse.Success.PNRAmount.TotalCost;
            }

            AddPaymentForBooking(bookingAmount, loginResponse.Signature);

            try
            {
                BookingUpdateResponseData responseData = new BookingUpdateResponseData();

                BookingCommitRequestData request = new BookingCommitRequestData();
                request.BookingChangeCode = "";
                request.BookingComments = new BookingComment[0];
                
                request.BookingID = 0;
                request.BookingParentID = 0;
                request.ChangeHoldDateTime = false;
                request.CurrencyCode = "INR";
                request.DistributeToContacts = false;
                request.GroupName = "";
                request.NumericRecordLocator = "";
                request.ParentRecordLocator = "";
                request.RecordLocator = "";
                request.RecordLocators = new RecordLocator[0];
                request.RestrictionOverride = false;
                
                request.State = MessageState.New;
                request.SystemCode = "";
                request.WaiveNameChangeFee = false;
                request.WaivePenaltyFee = false;
                request.WaiveSpoilageFee = false;
                request.Passengers = new Passenger[itinerary.Passenger.Length - infantCount];
                request.PaxCount = (Int16)(itinerary.Passenger.Length - infantCount);

                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    FlightPassenger pax = itinerary.Passenger[i];
                    if (pax.Type != PassengerType.Infant)
                    {
                        request.Passengers[i] = new Passenger();
                        if (pax.IsLeadPax)
                        {
                            request.BookingContacts = new BookingContact[1];
                            request.BookingContacts[0] = new BookingContact();
                            request.BookingContacts[0].AddressLine1 = pax.AddressLine1;
                            request.BookingContacts[0].AddressLine2 = pax.AddressLine2;
                            request.BookingContacts[0].AddressLine3 = "";
                            request.BookingContacts[0].City = "SHJ";
                            request.BookingContacts[0].CompanyName = "Cozmo Travel";
                            request.BookingContacts[0].CountryCode = pax.Country.CountryCode;
                            request.BookingContacts[0].CultureCode = "";
                            request.BookingContacts[0].CustomerNumber = "";
                            request.BookingContacts[0].DistributionOption = DistributionOption.Email;
                            request.BookingContacts[0].EmailAddress = pax.Email;
                            request.BookingContacts[0].Fax = "";
                            request.BookingContacts[0].HomePhone = pax.CellPhone;
                            request.BookingContacts[0].Names = new BookingName[1];
                            request.BookingContacts[0].Names[0] = new BookingName();
                            request.BookingContacts[0].Names[0].FirstName = pax.FirstName;
                            request.BookingContacts[0].Names[0].LastName = pax.LastName;
                            request.BookingContacts[0].Names[0].MiddleName = "";
                            request.BookingContacts[0].Names[0].State = MessageState.Modified;
                            request.BookingContacts[0].Names[0].Suffix = "";
                            request.BookingContacts[0].Names[0].Title = pax.Title;
                            request.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                            request.BookingContacts[0].OtherPhone = "";
                            request.BookingContacts[0].PostalCode = "3093";
                            request.BookingContacts[0].ProvinceState = "SHJ";
                            request.BookingContacts[0].SourceOrganization = "";
                            request.BookingContacts[0].State = MessageState.Modified;
                            request.BookingContacts[0].TypeCode = "P";
                            request.BookingContacts[0].WorkPhone = pax.CellPhone;
                        }

                        request.Passengers[i].CustomerNumber = "";
                        request.Passengers[i].FamilyNumber = (Int16)i;

                        request.Passengers[i].Names = new BookingName[1];
                        request.Passengers[i].Names[0] = new BookingName();
                        request.Passengers[i].Names[0].FirstName = pax.FirstName;
                        request.Passengers[i].Names[0].LastName = pax.LastName;
                        request.Passengers[i].Names[0].MiddleName = "";
                        request.Passengers[i].Names[0].State = MessageState.New;
                        request.Passengers[i].Names[0].Suffix = "";
                        request.Passengers[i].Names[0].Title = pax.Title;
                        request.Passengers[i].PassengerAddresses = new PassengerAddress[0];
                        //request.Passengers[i].PassengerBags = new PassengerBag[0];
                        //request.Passengers[i].PassengerFees = new PassengerFee[0];
                        request.Passengers[i].PassengerID = 0;
                        //request.Passengers[i].PassengerInfants = new PassengerInfant[0];
                        //request.Passengers[i].PassengerInfo = new PassengerInfo();
                        //request.Passengers[i].PassengerInfos = new PassengerInfo[0];
                        request.Passengers[i].PassengerNumber = (Int16)i;
                        //request.Passengers[i].PassengerProgram = new PassengerProgram();
                        //request.Passengers[i].PassengerPrograms = new PassengerProgram[0];
                        //request.Passengers[i].PassengerTravelDocuments = new PassengerTravelDocument[0];
                        //request.Passengers[i].PassengerTypeInfos = new PassengerTypeInfo[0];
                        request.Passengers[i].PaxDiscountCode = "";
                        request.Passengers[i].PseudoPassenger = false;
                        request.Passengers[i].State = MessageState.Modified;
                    }
                }
                int counter = 0;
                request.Passengers[0].PassengerInfants = new PassengerInfant[infantCount];
                for (int j = itinerary.Passenger.Length - infantCount; j < itinerary.Passenger.Length; j++)
                {
                    FlightPassenger pax = itinerary.Passenger[j];
                    request.Passengers[0].PassengerInfants[counter] = new PassengerInfant();
                    request.Passengers[0].PassengerInfants[counter].DOB = pax.DateOfBirth;
                    if (pax.Gender == Gender.Male)
                    {
                        request.Passengers[0].PassengerInfants[counter].Gender = SpiceJet.SGBooking.Gender.Male;
                    }
                    else if (pax.Gender == Gender.Female)
                    {
                        request.Passengers[0].PassengerInfants[counter].Gender = SpiceJet.SGBooking.Gender.Female;
                    }
                    request.Passengers[0].PassengerInfants[counter].Names = new BookingName[1];
                    request.Passengers[0].PassengerInfants[counter].Names[0] = new BookingName();
                    request.Passengers[0].PassengerInfants[counter].Names[0].FirstName = pax.FirstName;
                    request.Passengers[0].PassengerInfants[counter].Names[0].LastName = pax.LastName;
                    request.Passengers[0].PassengerInfants[counter].Names[0].MiddleName = "";
                    request.Passengers[0].PassengerInfants[counter].Names[0].State = MessageState.New;
                    request.Passengers[0].PassengerInfants[counter].Names[0].Suffix = "";
                    request.Passengers[0].PassengerInfants[counter].Names[0].Title = pax.Title;
                    request.Passengers[0].PassengerInfants[counter].Nationality = pax.Nationality.CountryCode;
                    request.Passengers[0].PassengerInfants[counter].ResidentCountry = pax.Country.CountryCode;
                    request.Passengers[0].PassengerInfants[counter].State = MessageState.New;
                    counter++;
                }

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(BookingCommitRequestData));
                        string filePath = xmlLogPath + "SGBookingCommitRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                }
                catch { }

                responseData = bookingAPI.BookingCommit(contractVersion, loginResponse.Signature, request);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + "SGBookingCommitResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, responseData);
                        sw.Close();
                    }
                }
                catch { }

                if (responseData != null)
                {
                    if (responseData.Error == null)
                    {
                        response.PNR = responseData.Success.RecordLocator;
                        response.ProdType = ProductType.Flight;
                        response.Error = "";
                        response.Status = BookingResponseStatus.Successful;
                        itinerary.PNR = response.PNR;
                        itinerary.FareType = "Pub";
                        
                        
                    }
                    else
                    {
                        response.Status = BookingResponseStatus.Failed;
                    }
                }               

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Commit booking. Reason : " + ex.ToString(), "");
                response.Error = ex.Message;
                response.Status = BookingResponseStatus.Failed;
            }

            Logout(loginResponse.Signature);

            return response;
        }


        public GetBookingResponse GetBooking(string pnr, string signature)
        {
            GetBookingResponse response = new GetBookingResponse();

            try
            {
                

                
                GetBookingRequestData requestData = new GetBookingRequestData();
                requestData.GetBookingBy = GetBookingBy.RecordLocator;
                requestData.GetByRecordLocator = new GetByRecordLocator();
                requestData.GetByRecordLocator.RecordLocator = pnr;

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(GetBookingRequestData));
                        string filePath = xmlLogPath + "SGGetBookingRequest_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, requestData);
                        sw.Close();
                    }
                }
                catch { }

                response.Booking = bookingAPI.GetBooking(contractVersion, signature, requestData);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(GetBookingResponse));
                        string filePath = xmlLogPath + "SGGetBookingResponse_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                }
                catch { }

                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to get Booking for PNR : " + pnr + ". Reason : " + ex.ToString(), "");
            }

            return response;
        }

        public Dictionary<string, string> CancelBooking(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();

            LogonResponse loginResponse = Login();

            try
            {   
                GetBookingResponse response = GetBooking(itinerary.PNR, loginResponse.Signature);                

                BookingManagerClient bookinAPI = new BookingManagerClient();                

                CancelRequestData requestData = new CancelRequestData();
                requestData.CancelBy = CancelBy.Journey;
                requestData.CancelJourney = new CancelJourney();
                requestData.CancelJourney.CancelJourneyRequest = new CancelJourneyRequest();
                requestData.CancelJourney.CancelJourneyRequest.Journeys = new Journey[response.Booking.Journeys.Length];
                for (int i = 0; i < response.Booking.Journeys.Length; i++)
                {
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i] = new Journey();
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments = new Segment[1];
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].State = MessageState.New;
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].JourneySellKey = response.Booking.Journeys[i].JourneySellKey;
                    for (int j=0;j< response.Booking.Journeys[i].Segments.Length;j++)
                    {
                        Segment segment = response.Booking.Journeys[i].Segments[j];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j] = new Segment();
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ActionStatusCode = "NN";
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ArrivalStation = segment.ArrivalStation;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = SpiceJet.SGBooking.ChannelType.API;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].DepartureStation = segment.DepartureStation;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares = new SpiceJet.SGBooking.Fare[segment.Fares.Length];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].FlightDesignator = segment.FlightDesignator;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].International = segment.International;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs = new Leg[segment.Legs.Length];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].SegmentSellKey = segment.SegmentSellKey;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STA = segment.STA;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].State = MessageState.New;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STD = segment.STD;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = segment.ChannelType;

                        for (int f = 0; f < response.Booking.Journeys[i].Segments[j].Fares.Length; f++)
                        {
                            SpiceJet.SGBooking.Fare fare = response.Booking.Journeys[i].Segments[j].Fares[f];
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f] = new SpiceJet.SGBooking.Fare();
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].CarrierCode = fare.CarrierCode;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassOfService = fare.ClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassType = fare.ClassType;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareApplicationType = fare.FareApplicationType;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareBasisCode = fare.FareBasisCode;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareClassOfService = fare.FareClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSellKey = fare.FareSellKey;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSequence = fare.FareSequence;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareStatus = FareStatus.Default;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].InboundOutbound = fare.InboundOutbound;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].IsAllotmentMarketFare = false;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].RuleNumber = fare.RuleNumber;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].OriginalClassOfService = fare.OriginalClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].TravelClassCode = fare.TravelClassCode;
                        }
                        for (int l = 0; l < response.Booking.Journeys[i].Segments[j].Legs.Length; l++)
                        {
                            Leg leg = response.Booking.Journeys[i].Segments[j].Legs[l];
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l] = new Leg();
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].ArrivalStation = leg.ArrivalStation;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].DepartureStation = leg.DepartureStation;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].FlightDesignator = leg.FlightDesignator;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].InventoryLegID = leg.InventoryLegID;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].LegInfo = leg.LegInfo;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].OperationsInfo = leg.OperationsInfo;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STA = leg.STA;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].State = MessageState.New;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STD = leg.STD;
                        }
                    }
                    requestData.CancelJourney.CancelJourneyRequest.WaivePenaltyFee = false;
                    requestData.CancelJourney.CancelJourneyRequest.WaiveSpoilageFee = false;
                    requestData.CancelJourney.CancelJourneyRequest.PreventReprice = false;
                    
                }

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(CancelRequestData));
                        string filePath = xmlLogPath + "SGCancelRequest_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, requestData);
                        sw.Close();
                    }
                }
                catch { }

                BookingUpdateResponseData responseData = bookinAPI.Cancel(contractVersion, loginResponse.Signature, requestData);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + "SGCancelResponse_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, responseData);
                        sw.Close();
                    }
                }
                catch { }

                if (responseData != null)
                {
                    if (responseData.Error == null)
                    {
                        //Instead of BalanceDue, TotalCost must be read for cancellation charges.
                        //Balance due is the refund amount.
                        string charges = responseData.Success.PNRAmount.TotalCost.ToString();
                        if (responseData.Success.PNRAmount.AlternateCurrencyBalanceDue > 0)
                        {
                            charges = responseData.Success.PNRAmount.AlternateCurrencyBalanceDue.ToString().Replace("-", "");
                        }
                        rateOfExchange = (responseData.Success.PNRAmount.AlternateCurrencyCode != null && responseData.Success.PNRAmount.AlternateCurrencyCode.Trim().Length > 0 ? exchangeRates[responseData.Success.PNRAmount.AlternateCurrencyCode] : exchangeRates["INR"]);
                        cancellationData.Add("Cancelled", "True");
                        cancellationData.Add("Charges", (Convert.ToDecimal(charges) * rateOfExchange).ToString());
                    }
                    else
                    {
                        cancellationData.Add("Cancelled", "False");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetCancellation, Severity.High, 1, "(SpiceJet)Failed to Cancel booking. Reason : " + ex.ToString(), "");
            }

            Logout(loginResponse.Signature);


            return cancellationData;
        }
    }
}
