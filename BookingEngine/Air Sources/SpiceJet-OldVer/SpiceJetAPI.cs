using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using CT.BookingEngine;
using SpiceJet.SpiceWebRef;
using System.Net;
using CT.Core;
using CT.Configuration;
using SpiceJet.SpiceJetProdWebRef;
using CT.TicketReceipt.BusinessLayer;



namespace CT.BookingEngine.GDS
{
    public class SpiceJetAPI 
    {

        #region Constants for building Flight/Fare String.
        string opSuffix = string.Empty;
        string serviceType = "J";
        string flightStatus = string.Empty;   // used for whole flight(not individual segments).
        string offSetTime = "+0530";
        string irregularOperationIndicator = "n";
        string remarksFlightString = string.Empty;
        int lid = 180;
        string jointOpDesignator = string.Empty;
        #endregion

        string origin = string.Empty;
        string destination = string.Empty;

        //TODO: make testMode false for PRODUCTION
        static bool testMode = Convert.ToBoolean(ConfigurationSystem.SpiceJetConfig["TestMode"]);

        // TODO: keep loging name , password in a config file
        string ClientId = string.Empty;
        string LoginName = string.Empty;
        string Password = string.Empty;

        SpiceJetWrapper api = new SpiceJetWrapper(testMode);

        public SpiceJetAPI()
        {
            // getting data from configuration file
            ClientId = ConfigurationSystem.SpiceJetConfig["ClientId"];
            LoginName = ConfigurationSystem.SpiceJetConfig["LoginName"];
            Password = ConfigurationSystem.SpiceJetConfig["Password"];
        }

        /// <summary>
        ///  Search for spice jet flights
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        public SpiceJetSearchResult[] Search(SearchRequest request, string sessionId)
        {            
            Trace.TraceInformation("SpiceJet.Search entered");
            string msg = GetAvailabilibiltyRequest(request);
            Basket.FlightBookingSession[sessionId].XmlMessage.Add(msg);
            string resp = string.Empty;
            string err = string.Empty;

            Audit.Add(EventType.SpiceJetSearch, Severity.Normal, 0, "Spice Jet Seach Request message Generated | " + DateTime.Now + "| Request XML" + msg, "");

            // SOAP REQUEST
            try
            {
                api.GetAvailability(msg, ref resp, ref err);
            }
            catch (WebException excep)
            {
                Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation((Exception)excep, "Web Exception returned from SpiceJet."));
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Web Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Error: " + excep.Message);
                throw new WebException("Error: " + excep.Message);
            }
            catch (System.Web.Services.Protocols.SoapException excep)
            {
                Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation((Exception)excep, "SOAP Exception returned from SpiceJet."));
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "SOAP Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Error: " + excep.Message);
                throw new BookingEngineException("Error: " + excep.Message);
            }
            // TO REMOVE -- This code is added to run this api in case spice Jet not working
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load("C:\\test_xml\\searchresult.xml");
            //resp = xmlDoc.InnerXml;                      

            if (resp.Length == 0)
            {
                Basket.FlightBookingSession[sessionId].Log.Add("Zero length from Spicejet.");
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Spice Jet Search failure: ORIGIN" + request.Segments[0].Origin + "| DEST: " + request.Segments[0].Destination + "|" + DateTime.Now + "| error message: " + err, "");

                Trace.TraceError("Unable to establish connection with Spice Jet");
                throw new BookingEngineException("<br>Error:Unable to establish connection with Spice Jet");
            }

            Basket.FlightBookingSession[sessionId].XmlMessage.Add(resp);
            Audit.Add(EventType.SpiceJetSearch, Severity.Normal, 0, "Spice Jet Search Successful: ORIGIN" + request.Segments[0].Origin + "| DEST: " + request.Segments[0].Destination + "|" + DateTime.Now + "| Response XML" + resp, "");

            string NumberPassengers = Convert.ToString((request.AdultCount + request.ChildCount + request.SeniorCount));
            SpiceJetSearchResult[] searchRes = CreateSearchResults(resp, NumberPassengers, origin, destination, request.AdultCount, request.ChildCount, request.SeniorCount);

            Trace.TraceInformation("SpiceJet.Search exit");
            return searchRes;
        }

        /// <summary>
        /// Parsing Fare quote response
        /// </summary>
        /// <param name="resp"></param>
        /// <returns> Hashtable FareQuotesList</returns>
        private Hashtable GenerateFareQuoteResult(string response)
        {
            Trace.TraceInformation("SpiceJet.GenerateFareQuoteResult entered: response = " + response);

            XmlNode tempNode;
            TextReader textReader = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(textReader);

            Hashtable FareQuotesList = new Hashtable();

            // namespace creating
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/"); //SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "urn:os:farequote");
            nsmgr.AddNamespace("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
            nsmgr.AddNamespace("type", "ns1:FareQuoteResponse");
            nsmgr.AddNamespace("ns2", "http://xml.apache.org/xml-soap");
            nsmgr.AddNamespace("type", "ns2:Vector");
            nsmgr.AddNamespace("type", "ns1:AirComponent");

            // checking for any error found from spice jet            
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:getFareQuoteResponse/return/Exceptions", nsmgr);
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.SpiceJetFareQuote, Severity.High, 0, "Error returned from SpiceJet. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + response, "");

                Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);

                // For local file -- remove it -- STARt
                // This code is added to run this api in case spice Jet not working

                //XmlDocument xmlD = new XmlDocument();
                //xmlD.Load("C:\\test_xml\\XMLFile.xml");
                //string resp = xmlD.InnerXml;

                //textReader = new StringReader(resp);               
                //xmlDoc.Load(textReader);
                // For local file -- remove it -- End
            }

            XmlNodeList TaxInfo = xmlDoc.SelectNodes("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:getFareQuoteResponse/return/AirComponents/item", nsmgr);

            if (TaxInfo.Count == 0)
            {
                Audit.Add(EventType.SpiceJetFareQuote, Severity.High, 0, "No results found in response by Spice Jet. | " + DateTime.Now + "| Response XML" + response, "");
                Trace.TraceError("No results found in response by Spice Jet.");
                throw new BookingEngineException("<br> No results found in response by Spice Jet.");
            }

            // if only one way search then way = 1;
            int way = 1;
            foreach (XmlNode itemStart in TaxInfo)
            {
                // fare info - Total ammount, If total ammount does not come, throw an exception
                tempNode = itemStart.SelectSingleNode("Fares/item/TotalAmount/Amount");
                if (tempNode != null && tempNode.InnerText.Length > 0)
                {
                    if (way == 1)
                    {
                        FareQuotesList.Add("TotalOutBound", tempNode.InnerText);
                    }
                    if (way == 2)
                    {
                        FareQuotesList.Add("TotalInBound", tempNode.InnerText);
                    }
                }
                else
                {
                    Audit.Add(EventType.SpiceJetFareQuote, Severity.High, 0, "Error returned from SpiceJet. Error Message: Total Ammount in spice jet fare quote response not coming | " + DateTime.Now + "| Response XML" + response, "");
                    Trace.TraceError("Total Ammount in spice jet fare quote response not coming. Response =" + response);
                    throw new BookingEngineException("<br> Total Ammount in spice jet fare quote response not coming.");
                }

                // fare info - Base ammount, If base ammount does not come, throw an exception
                tempNode = itemStart.SelectSingleNode("Fares/item/BaseAmount/Amount");
                if (tempNode != null && tempNode.InnerText.Length > 0)
                {
                    if (way == 1)
                    {
                        FareQuotesList.Add("BaseFareOutBound", tempNode.InnerText);
                    }
                    if (way == 2)
                    {
                        FareQuotesList.Add("BaseFareInBound", tempNode.InnerText);
                    }
                }
                else
                {
                    Audit.Add(EventType.SpiceJetFareQuote, Severity.High, 0, "Error returned from SpiceJet. Error Message: Base Ammount in spice jet fare quote response not coming | " + DateTime.Now + "| Response XML" + response, "");
                    Trace.TraceError("Base Ammount in spice jet fare quote response not coming. Response =" + response);
                    throw new BookingEngineException("<br>Error: Base Ammount in spice jet fare quote response not coming.");
                }

                // Reading FuelSurcharge.
                XmlNodeList taxNodeList = itemStart.SelectNodes("Fares/item/Taxes/item");
                foreach (XmlNode taxNode in taxNodeList)
                {
                    tempNode = taxNode.SelectSingleNode("Level");
                    if (tempNode != null && tempNode.InnerText == "2")
                    {
                        tempNode = taxNode.SelectSingleNode("Amount/Amount");
                        if (tempNode != null)
                        {
                            if (way == 1)
                            {
                                FareQuotesList.Add("FuelSurchargeOutBound", tempNode.InnerText);
                            }
                            else if (way == 2)
                            {
                                FareQuotesList.Add("FuelSurchargeInBound", tempNode.InnerText);
                            }
                        }
                    }
                }

                decimal tax = 0;
                if (way == 1)
                {
                    tax = Convert.ToDecimal(FareQuotesList["TotalOutBound"]) - Convert.ToDecimal(FareQuotesList["BaseFareOutBound"]);
                    FareQuotesList.Add("TaxOutBound", tax);
                }
                if (way == 2)
                {
                    tax = Convert.ToDecimal(FareQuotesList["TotalInBound"]) - Convert.ToDecimal(FareQuotesList["BaseFareInBound"]);
                    FareQuotesList.Add("TaxInBound", tax);
                }

                way++;
            }

            Trace.TraceInformation("SpiceJet.GenerateFareQuoteResult exit");
            return FareQuotesList;
        }

        /// <summary>
        /// Genrerating get availability request 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// 
        protected string GetAvailabilibiltyRequest(SearchRequest request)
        {
            Trace.TraceInformation("SpiceJet.GetAvailabilibiltyRequest entered");

            string traceid = TraceIdGeneration();

            string depdate = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'").ToUpper();   //"2006-11-02T00:00:00Z";
            origin = request.Segments[0].Origin;
            destination = request.Segments[0].Destination;
            string numberFares = ""; // Fares for an flight should be configurable
            string numberPassengers = Convert.ToString((request.AdultCount + request.ChildCount + request.SeniorCount));

            string retdate = string.Empty;

            StringBuilder message = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(message);
            writer.WriteStartElement("Request");
            writer.WriteStartElement("Authorization"); //Authorization start

            writer.WriteElementString("ClientID", ClientId); //Client Id
            writer.WriteElementString("LoginName", LoginName);
            writer.WriteElementString("Password", Password);
            writer.WriteEndElement(); //Authorization finish

            writer.WriteElementString("TraceID", traceid);

            writer.WriteStartElement("Query"); // Start Query
            writer.WriteStartElement("item"); // start item 

            writer.WriteElementString("DepartureDate", depdate);

            Trace.TraceInformation("SpiceJet.GetAvailabilityMessage Origin = " + request.Segments[0].Origin);

            writer.WriteElementString("Origin", origin);
            writer.WriteElementString("Destination", destination);
            writer.WriteElementString("NumberFares", numberFares);
            writer.WriteElementString("NumberPassengers", numberPassengers);
            writer.WriteElementString("DaysOut", "0");

            writer.WriteStartElement("Options"); // Options start

            if (request.Type == SearchType.Return)
            {
                retdate = request.Segments[1].PreferredDepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'").ToUpper();
            }
            else
            {
                retdate = "";
            }
            writer.WriteElementString("ReturnDate", retdate); //Option for regular return

            writer.WriteEndElement(); // Options End

            writer.WriteEndElement(); // end item
            writer.WriteEndElement(); // end Query
            writer.WriteEndElement(); // end Request

            writer.Close();
            Trace.TraceInformation("SpiceJet.GetAvailabilibiltyRequest exit");
            return message.ToString();

        }

        /// <summary>
        /// Commong Trace Id generation method
        /// </summary>
        /// <returns></returns>
        private string TraceIdGeneration()
        {
            string traceid = DateTime.Now.ToString("ddMMyyyyHHmmsss");
            traceid = "TT" + traceid;
            return traceid;
        }

        /// <summary>
        /// Parsing search result SOAP response
        /// </summary>
        /// <param name="response"></param>
        /// <param name="numPassenger"></param>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        private static SpiceJetSearchResult[] CreateSearchResults(string response, string numPassenger, string origin, string destination, int adultCount, int childCount, int seniorCount)
        {
            Trace.TraceInformation("SpiceJet.CreateSearchResults entered");

            XmlNode tempNode;

            TextReader textReader = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(textReader);

            int len = 0;        // counter for not null result pair
            SpiceJetSearchResult[] searchResults = new SpiceJetSearchResult[0];
            SpiceJetSearchResult[] searchResultsOB = new SpiceJetSearchResult[0];
            SpiceJetSearchResult[] searchResultsIB = new SpiceJetSearchResult[0];

            // namespace creating
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/"); //SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "urn:os:avl");
            nsmgr.AddNamespace("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
            nsmgr.AddNamespace("type", "ns1:GetAvailabilityResponse");
            nsmgr.AddNamespace("ns2", "http://xml.apache.org/xml-soap");
            nsmgr.AddNamespace("type", "ns1:AvailabilityGroup");
            nsmgr.AddNamespace("type", "ns2:Vector");
            nsmgr.AddNamespace("type", "ns1:AirComponent");

            // checking for any error found from spice jet           
            XmlNode errorInfo = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring", nsmgr);

            if (errorInfo != null && errorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error returned from SpiceJet. Error Message: " + errorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + response, "");
                Trace.TraceError("Error: " + errorInfo.InnerText);
                throw new BookingEngineException("<br> " + errorInfo.InnerText);

                // For local file -- remove it -- STARt
                // This code is added to run this api in case spice Jet not working

                //XmlDocument xmlD = new XmlDocument();
                //xmlD.Load("C:\\test_xml\\XMLFile.xml");
                //string resp = xmlD.InnerXml;

                //textReader = new StringReader(resp);               
                //xmlDoc.Load(textReader);
                // For local file -- remove it -- End
            }

            // checking if response xml is having any data for given source and destination -- START

            XmlNode ExceptionInfo = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:getAvailabilityResponse/return/Exceptions/item/MessageText", nsmgr);

            if (ExceptionInfo == null && errorInfo == null)
            {
                // checking if response xml is having any data for given source and destination -- END

                XmlNodeList AvailabilityInfo = xmlDoc.SelectNodes("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:getAvailabilityResponse/return/AvailabilityGroup/item", nsmgr);

                if (AvailabilityInfo.Count == 0)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error returned from SpiceJet. Error Message:No results found in response by Spice Jet | " + DateTime.Now + "| Response XML" + response, "");
                    Trace.TraceError("No results found in response by Spice Jet");
                    throw new BookingEngineException("<br> No results found in response by Spice Jet");
                }

                // if only one way search then way = 1;
                int way = 1;
                foreach (XmlNode itemStart in AvailabilityInfo)
                {
                    XmlNodeList OutInInfo = itemStart.SelectNodes("Components/item");
                    int i = 0;
                    int CountNotFare = 0; // This variable is used to count how many items do not contain fare information for spice jet response. As in Test version of spice jet few items dont contain fare information.
                    SpiceJetSearchResult[] searchResultsOI = new SpiceJetSearchResult[OutInInfo.Count];

                    foreach (XmlNode xmlNode in OutInInfo)
                    {
                        SpiceJetSearchResult result = new SpiceJetSearchResult();
                        bool FareNotContained = false; // Used because In test version of spice jet, few items dont contain fare information
                        // Fare Related

                        result.ResultBookingSource = BookingSource.SpiceJet;
                        tempNode = xmlNode.SelectSingleNode("Fares");
                        if (tempNode != null && tempNode.InnerText != "")
                        {
                            XmlNodeList FaresItem = tempNode.SelectNodes("item");
                            int FareBreakDownCount = 0;
                            if (adultCount > 0)
                            {
                                FareBreakDownCount++;
                            }
                            if (childCount > 0)
                            {
                                FareBreakDownCount++;
                            }
                            if (seniorCount > 0)
                            {
                                FareBreakDownCount++;
                            }
                            result.FareBreakdown = new Fare[FareBreakDownCount];

                            //foreach (XmlNode xmlNodeSecond in FaresItem)
                            // Here we are picking lowest fare element
                            int lowestFare = GetLowestFareIndex(FaresItem);
                            tempNode = FaresItem[lowestFare].SelectSingleNode("BaseAmount/Amount");
                            if (tempNode != null)
                            {
                                double basefare = Convert.ToDouble(tempNode.InnerText);

                                // For spice Jet availability - base code is same as total ammount

                                basefare = Convert.ToDouble(tempNode.InnerText);

                                // making Fare string
                                result.FareString[0] = FaresItem[0].InnerXml;

                                result.BaseFare = basefare * Convert.ToInt32(numPassenger);
                                result.TotalFare = basefare * Convert.ToInt32(numPassenger);

                                // currency - If currency code in not INR then throw exception
                                tempNode = FaresItem[0].SelectSingleNode("BaseAmount/CurrencyCode");
                                if (tempNode != null)
                                {
                                    if (tempNode.InnerText == "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                                    {
                                        result.Currency = tempNode.InnerText;
                                    }
                                    else
                                    {
                                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error in Spice Jet: Currency Code is not" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "| " + DateTime.Now + "| Response XML" + response, "");
                                        Trace.TraceError("Currency Code is not" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "in Spice Jet");
                                        throw new BookingEngineException("<br> Currency Code is not" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "in Spice Jet");
                                    }
                                }

                                // FareBasisCode                                    
                                tempNode = FaresItem[0].SelectSingleNode("FareBasisCode");
                                if (tempNode != null && tempNode.InnerText.Length > 0)
                                {
                                    result.SelectedFareBasisCode[0] = tempNode.InnerText;
                                }
                                result.FareRules = new List<FareRule>();
                                FareRule tempFareRule = new FareRule();
                                tempFareRule.FareBasisCode = result.SelectedFareBasisCode[0];
                                result.FareRules.Add(tempFareRule);


                                int k = 0;
                                if (adultCount > 0)
                                {
                                    Fare fare = GetFareByPassenger(adultCount, "Adult", Convert.ToInt32(numPassenger), result.BaseFare);
                                    result.FareBreakdown[k] = fare;
                                    k++;
                                }
                                if (childCount > 0)
                                {
                                    Fare fare = GetFareByPassenger(childCount, "Child", Convert.ToInt32(numPassenger), result.BaseFare);
                                    result.FareBreakdown[k] = fare;
                                    k++;
                                }
                                if (seniorCount > 0)
                                {
                                    Fare fare = GetFareByPassenger(seniorCount, "Senior", Convert.ToInt32(numPassenger), result.BaseFare);
                                    result.FareBreakdown[k] = fare;
                                }

                                // Fare type
                                result.FareType = "PUB";

                                //TODO: Fare Quotes -- need to keep one more 'FareBasisCode' field for calculating Fare quotes for spice JET
                                // -- Need to keep 'FareGroup' 'ToSell', 'Cabin' 'Overnight' 'Status' 'ServiceType' 'ArrivalTimeOffset' in search result for calculating Fare quotes for spice JET                                 

                            }
                            else if (tempNode == null || tempNode.InnerText == "")
                            {
                                FareNotContained = true;
                            }
                        }
                        else if (tempNode == null || tempNode.InnerText == "")
                        {
                            FareNotContained = true;
                        }

                        // Flights Related

                        if (FareNotContained == false)
                        {
                            int g = 0;  // FOR FLI_GRP                    
                            result.Flights = new FlightInfo[1][];

                            // Making temporary flight list
                            ArrayList flightList = new ArrayList();
                            TimeSpan AccDuration = new TimeSpan();

                            // Making Flight String 
                            // Flight string is used for getting fare quote request - as it is taking filght legs as it is coming in avaialibility response
                            result.FlightString[0] = xmlNode.SelectSingleNode("Flights/item").InnerXml;

                            XmlNodeList FlightItem = xmlNode.SelectNodes("Flights/item/FlightLegs/item");
                            int flightCounter = 0;

                            if (FlightItem.Count > 0)
                            {
                                foreach (XmlNode FlightNode in FlightItem)
                                {
                                    FlightInfo info = new FlightInfo();

                                    // Airline
                                    tempNode = xmlNode.SelectSingleNode("Flights/item/CarrierCode");
                                    if (tempNode != null)
                                    {
                                        if (tempNode.InnerText == "0S")// Checking for the flight code returned from spicejet as it either returns '0S' or 'SG' 
                                        {
                                            info.Airline = "SG";
                                        }
                                        else
                                        {
                                            info.Airline = tempNode.InnerText;
                                        }
                                    }
                                    else
                                    {
                                        info.Airline = string.Empty;
                                    }

                                    // Flight Number
                                    tempNode = xmlNode.SelectSingleNode("Flights/item/FlightNumber");
                                    if (tempNode != null)
                                    {
                                        info.FlightNumber = tempNode.InnerText;
                                    }
                                    else
                                    {
                                        // Flight number cannot be empty
                                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error in Spice Jet: Flight number is empty | " + DateTime.Now + "| Response XML" + response, "");
                                        Trace.TraceError("Spice Jet Error: Flight number is empty");
                                        throw new BookingEngineException("<br> Flight number is empty");
                                    }

                                    // Booking Class -- Spice Jet show fares for only single Booking Class - Economy Class (For Economy class code is 'Y')

                                    info.BookingClass = "Y";



                                    tempNode = FlightNode.SelectSingleNode("ScheduledDepartureTime");
                                    if (tempNode != null)
                                    {
                                        info.DepartureTime = ParseDate(tempNode.InnerText);
                                    }
                                    else
                                    {
                                        // Schedule departure time cannot be empty
                                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error in Spice Jet: Scheduled Departure Time is empty | " + DateTime.Now + "| Response XML" + response, "");
                                        Trace.TraceError("Spice Jet Error: Scheduled Departure Time is empty");
                                        throw new BookingEngineException("<br> Scheduled Departure Time is empty");
                                    }

                                    tempNode = FlightNode.SelectSingleNode("ScheduledArrivalTime");
                                    if (tempNode != null)
                                    {
                                        info.ArrivalTime = ParseDate(tempNode.InnerText);
                                    }
                                    else
                                    {
                                        // Schedule Arrival time cannot be empty
                                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error in Spice Jet: Scheduled Arrival Time is empty | " + DateTime.Now + "| Response XML" + response, "");
                                        Trace.TraceError("Spice Jet Error: Scheduled Arrival Time is empty");
                                        throw new BookingEngineException("<br> Scheduled Arrival Time is empty");
                                    }

                                    tempNode = FlightNode.SelectSingleNode("AircraftType");
                                    if (tempNode != null)
                                    {
                                        info.Craft = tempNode.InnerText;
                                    }
                                    else
                                    {
                                        info.Craft = string.Empty;
                                    }

                                    // Meal type
                                    tempNode = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:getAvailabilityResponse/return/AvailabilityGroup/item/Components/item/Fares/Meal", nsmgr);

                                    if (tempNode != null)
                                    {
                                        info.MealType = tempNode.InnerText;
                                    }
                                    else
                                    {
                                        info.MealType = string.Empty;
                                    }

                                    //tempNode = xmlNode.SelectSingleNode("Flights/item/NumberOfStops");
                                    //if (tempNode != null)
                                    //{
                                    //    info.Stops = Convert.ToInt32(tempNode.InnerText);
                                    //}

                                    // "Flights/item/NumberOfStops" comes with a flight group.
                                    // Stops does not come with individual segments. 
                                    // Since this is LCC and has domestic flights only we take the assumption that the legs don't have stops
                                    info.Stops = 0;

                                    //TODO:
                                    //tempNode = FlightNode.SelectSingleNode("E_TIC_ELI");
                                    //if (tempNode != null && tempNode.InnerText.ToUpper() == "E")
                                    //{
                                    //    info.ETicketEligible = true;
                                    //}
                                    //else
                                    //{
                                    //    info.ETicketEligible = false;
                                    //}

                                    tempNode = FlightNode.SelectSingleNode("DepartureAirport");
                                    if (tempNode != null || (tempNode != null && tempNode.InnerText != ""))
                                    {
                                        info.Origin = new Airport(tempNode.InnerText);
                                    }
                                    else
                                    {
                                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error in Spice Jet: DepartureAirport are not coming | " + DateTime.Now + "| Response XML" + response, "");
                                        Trace.TraceError("DepartureAirport are not coming ");
                                        throw new BookingEngineException("<br> DepartureAirport are not coming ");
                                    }

                                    tempNode = FlightNode.SelectSingleNode("ArrivalAirport");
                                    if (tempNode != null || (tempNode != null && tempNode.InnerText != ""))
                                    {
                                        info.Destination = new Airport(tempNode.InnerText);
                                    }
                                    else
                                    {
                                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error in Spice Jet: ArrivalAirport are not coming | " + DateTime.Now + "| Response XML" + response, "");
                                        Trace.TraceError("ArrivalAirport are not coming ");
                                        throw new BookingEngineException("<br> ArrivalAirport are not coming ");
                                    }

                                    // other flight information
                                    tempNode = FlightNode.SelectSingleNode("DepartureTerminal");
                                    if (tempNode != null || (tempNode != null && tempNode.InnerText != ""))
                                    {
                                        info.DepTerminal = tempNode.InnerText;
                                    }
                                    else
                                    {
                                        info.DepTerminal = string.Empty;
                                    }

                                    tempNode = FlightNode.SelectSingleNode("ArrivalTerminal");
                                    if (tempNode != null || (tempNode != null && tempNode.InnerText != ""))
                                    {
                                        info.ArrTerminal = tempNode.InnerText;
                                    }
                                    else
                                    {
                                        info.ArrTerminal = string.Empty;
                                    }


                                    info.GroundTime = new TimeSpan();

                                    info.Duration = (info.ArrivalTime - info.DepartureTime);

                                    AccDuration = AccDuration + info.Duration;

                                    // PTC details
                                    //string PaxType = string.Empty;
                                    //if (result.FareBreakdown[0] != null)
                                    //{
                                    //    if (result.FareBreakdown[0].PassengerType == PassengerType.Adult)
                                    //    {
                                    //        PaxType = "ADT";
                                    //    }
                                    //    if (result.FareBreakdown[0].PassengerType == PassengerType.Child)
                                    //    {
                                    //        PaxType = "CNN";
                                    //    }
                                    //    if (result.FareBreakdown[0].PassengerType == PassengerType.Infant)
                                    //    {
                                    //        PaxType = "INF";
                                    //    }
                                    //    if (result.FareBreakdown[0].PassengerType == PassengerType.Senior)
                                    //    {
                                    //        PaxType = "SNN";
                                    //    }
                                    //}

                                    // all flights added
                                    info.ETicketEligible = true;
                                    flightList.Add(info);
                                    flightCounter++;
                                }

                                result.Flights[g] = new FlightInfo[flightList.Count];
                                for (int f = 0; f < flightList.Count; f++)
                                {
                                    result.Flights[g][f] = (FlightInfo)flightList[f];
                                }
                                flightList.Clear();
                            }
                            else
                            {
                                Audit.Add(EventType.SpiceJetSearch, Severity.High, 0, "Error in Spice Jet: FlightLegs are not coming | " + DateTime.Now + "| Response XML" + response, "");
                                Trace.TraceError("FlightLegs are not coming ");
                                throw new BookingEngineException("<br> FlightLegs are not coming ");
                            }

                            searchResultsOI[i] = result;
                            i++;
                        }
                        else if (FareNotContained == true)
                        {
                            CountNotFare++;
                        }
                    }

                    // remving null element of array
                    // This check is added to sort out the null results as in test server of spice jet few items does not contain fare information so skipping those items
                    SpiceJetSearchResult[] searchResultsTemp = new SpiceJetSearchResult[1];
                    if (CountNotFare > 0)
                    {
                        searchResultsTemp = new SpiceJetSearchResult[searchResultsOI.Length - CountNotFare];
                        int h = 0;
                        for (int n = 0; n < searchResultsOI.Length - 1; n++)
                        {
                            if ((searchResultsOI[n] != null) && (searchResultsOI[n].BaseFare != 0))
                            {
                                searchResultsTemp[h] = searchResultsOI[n];
                                h++;
                            }
                        }
                    }
                    else
                    {
                        searchResultsTemp = searchResultsOI;
                    }

                    // moving result in a single array 
                    if (way == 1)
                    {
                        searchResultsOB = searchResultsTemp;
                    }
                    else if (way == 2)
                    {
                        searchResultsIB = searchResultsTemp;
                    }
                    way++;
                }

                // Merging two outbound and Inboud array in a single search result object 

                // Combination logic
                // LOGIC: here we are making combination of each outbound with each inbound result with checking that inbound departure time is not less than a mininmum amount of time of outbound arrival time (as it could make conflict for same day inbound and outbound flight)
                // After then removing null element of search result array
                // After that sorting array by price
                //TODO: minimum amount of time gap should be configurable

                if ((searchResultsIB != null && searchResultsIB.Length > 0) && (searchResultsOB != null && searchResultsOB.Length > 0))
                {
                    searchResults = new SpiceJetSearchResult[searchResultsOB.Length * searchResultsIB.Length];
                    // merging inbound and outbound array                   

                    for (int n = 0; n < (searchResultsOB.Length); n++)
                    {
                        for (int m = 0; m < (searchResultsIB.Length); m++)
                        {
                            DateTime arrivalTime = searchResultsOB[n].Flights[0][searchResultsOB[n].Flights[0].Length - 1].ArrivalTime;
                            DateTime compareTime = arrivalTime.AddHours(1);
                            if (compareTime <= searchResultsIB[m].Flights[0][0].DepartureTime)
                            {
                                searchResults[len] = new SpiceJetSearchResult();
                                searchResults[len].BaseFare = searchResultsOB[n].BaseFare + searchResultsIB[m].BaseFare;
                                searchResults[len].Currency = searchResultsOB[n].Currency;

                                searchResults[len].FareBreakdown = new Fare[searchResultsOB[n].FareBreakdown.Length];
                                for (int k = 0; k < searchResultsOB[n].FareBreakdown.Length; k++)
                                {
                                    searchResults[len].FareBreakdown[k] = new Fare();
                                    searchResults[len].FareBreakdown[k].BaseFare = searchResultsOB[n].FareBreakdown[k].BaseFare + searchResultsIB[m].FareBreakdown[k].BaseFare;
                                    searchResults[len].FareBreakdown[k].PassengerCount = searchResultsOB[n].FareBreakdown[k].PassengerCount;
                                    searchResults[len].FareBreakdown[k].PassengerType = searchResultsOB[n].FareBreakdown[k].PassengerType;
                                    searchResults[len].FareBreakdown[k].TotalFare = searchResultsOB[n].FareBreakdown[k].TotalFare + searchResultsIB[m].FareBreakdown[k].TotalFare;

                                }
                                searchResults[len].FareType = searchResultsOB[n].FareType;

                                searchResults[len].Flights = new FlightInfo[2][];

                                //searchResults[len].Flights[1] = new FlightInfo[2];
                                searchResults[len].Flights[0] = searchResultsOB[n].Flights[0];
                                searchResults[len].Flights[1] = searchResultsIB[m].Flights[0];
                                searchResults[len].TotalFare = searchResultsOB[n].TotalFare + searchResultsIB[m].TotalFare;

                                searchResults[len].Tax = searchResultsOB[n].Tax + searchResultsIB[m].Tax;

                                searchResults[len].FareString[0] = searchResultsOB[n].FareString[0];
                                searchResults[len].FareString[1] = searchResultsIB[m].FareString[0];

                                searchResults[len].FlightString[0] = searchResultsOB[n].FlightString[0];
                                searchResults[len].FlightString[1] = searchResultsIB[m].FlightString[0];

                                searchResults[len].SelectedFareBasisCode[0] = searchResultsOB[n].SelectedFareBasisCode[0];
                                searchResults[len].SelectedFareBasisCode[1] = searchResultsIB[m].SelectedFareBasisCode[0];

                                searchResults[len].FareRules = new List<FareRule>();
                                FareRule fRule1 = new FareRule();
                                FareRule fRule2 = new FareRule();
                                fRule1.FareBasisCode = searchResultsOB[n].SelectedFareBasisCode[0];
                                fRule2.FareBasisCode = searchResultsIB[m].SelectedFareBasisCode[0];
                                searchResults[len].FareRules.Add(fRule1);
                                searchResults[len].FareRules.Add(fRule2);

                                searchResults[len].ResultBookingSource = BookingSource.SpiceJet;
                                len++;
                            }
                        }
                    }
                }
                else
                {
                    searchResults = searchResultsOB;
                }
            }
            else if ((ExceptionInfo != null) && (ExceptionInfo.InnerText.Length != 0))
            {
                searchResults = new SpiceJetSearchResult[0];
            }

            SpiceJetSearchResult[] finalResult = new SpiceJetSearchResult[len];

            // if search result for one way only - In this case no pairing made
            if (searchResultsIB != null && searchResultsIB.Length > 0)
            {
                for (int i = 0; i < len; i++)
                {
                    finalResult[i] = searchResults[i];
                }
            }
            else
            {
                finalResult = searchResults;
            }

            // sorting array by fare
            for (int n = 0; n < finalResult.Length - 1; n++)
            {
                for (int m = 0; m < (finalResult.Length - n - 1); m++)
                {
                    if (finalResult[m].TotalFare > finalResult[m + 1].TotalFare)
                    {
                        SpiceJetSearchResult temp = finalResult[m];
                        finalResult[m] = finalResult[m + 1];
                        finalResult[m + 1] = temp;
                    }
                }
            }

            Trace.TraceInformation("SpiceJet.CreateSearchResults Exit: result =" + finalResult);
            return finalResult;
        }

        /// <summary>
        /// Gets Farequotes for the given search result.
        /// </summary>
        /// <param name="result">Search result for which the fare quote is needed.</param>
        /// <returns>Hashtable with farequote information.</returns>
        public Hashtable GetFareQuotes(SpiceJetSearchResult result)
        {
            return GetFareQuotes((SearchResult)result);
        }

        /// <summary>
        /// Gets Farequotes for the given search result.
        /// </summary>
        /// <param name="result">Search result for which the fare quote is needed.</param>
        /// <returns>Hashtable with farequote information.</returns>
        public Hashtable GetFareQuotes(SearchResult result)
        {
            int length = 1;
            if (result.Flights.Length > 1)
            {
                length = 2; // if the flight is roundtrip then it will have two fareBasis Code.
            }
            string[] fareBasisCode = new string[length];
            string[] fareString = new string[length];
            string[] flightString = new string[length];
            fareString[0] = BuildFareString(result);
            flightString[0] = BuildFlightString(result, true);
            if (result.FareRules[0] != null)
            {
                fareBasisCode[0] = result.FareRules[0].FareBasisCode;
            }
            else
            {
                throw new ArgumentException("Result must contain FareRules", "result.FareRules");
            }
            if (result.Flights.Length > 1)
            {
                // for return trip.
                fareString[1] = fareString[0];
                flightString[1] = BuildFlightString(result, false);
                if (result.FareRules[1] != null)
                {
                    fareBasisCode[1] = result.FareRules[1].FareBasisCode;
                }
                else
                {
                    throw new ArgumentException("Fare rule for inbound flight not found.", "result.FareRules");
                }
            }
            // else its oneway and areBasesCode[1] is not needed.
            return GetFareQuotes(fareBasisCode, fareString, flightString);
        }

        /// <summary>
        /// Gets Farequotes for the given search itinerary.
        /// </summary>
        /// <param name="result">Itinerary for which the fare quote is needed.</param>
        /// <returns>Hashtable with farequote information.</returns>
        public Hashtable GetFareQuotes(FlightItinerary itinerary)
        {
            int returnSegment = 0;  // after this index the segments are for return trip if available.
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (itinerary.Destination == itinerary.Segments[i].Destination.CityCode || itinerary.Destination == itinerary.Segments[i].Destination.AirportCode)
                {
                    returnSegment = i;
                    break;
                }
            }
            // determining if roundtrip.
            bool roundTrip = returnSegment < (itinerary.Segments.Length - 1);
            int length = 1;
            if (roundTrip)
            {
                length = 2; // for round trip there are two fare basis code.
            }
            string[] fareBasisCode = new string[length];
            string[] fareString = new string[length];
            string[] flightString = new string[length];
            fareString[0] = BuildFareString(itinerary);
            flightString[0] = BuildFlightString(itinerary, true);
            if (itinerary.FareRules[0] != null)
            {
                fareBasisCode[0] = itinerary.FareRules[0].FareBasisCode;
            }
            else
            {
                throw new ArgumentException("Itinerary must contain FareRules", "itinerary.FareRules");
            }
            if (roundTrip)
            {
                // fro roundtrip
                fareString[1] = fareString[0];
                flightString[1] = BuildFlightString(itinerary, false);
                if (itinerary.FareRules[1] != null)
                {
                    fareBasisCode[1] = itinerary.FareRules[1].FareBasisCode;
                }
                else
                {
                    throw new ArgumentException("Fare rule for inbound flight not found.", "itinerary.FareRules");
                }
            }
            // else its oneway and areBasesCode[1] is not needed.
            return GetFareQuotes(fareBasisCode, fareString, flightString);
        }

        /// <summary>
        /// Getting Fare quote for spicific choosen group
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public Hashtable GetFareQuotes(string[] fareBasisCode, string[] fareString, string[] flightString)
        {
            Trace.TraceInformation("SpiceJet.GetFareQuotes entered");
            string msg = GetFareQuoteRequest(fareBasisCode, fareString, flightString);
            string resp = string.Empty;
            string err = string.Empty;

            Audit.Add(EventType.SpiceJetFareQuote, Severity.Normal, 0, "Spice Jet FareQuote Request message Generated | " + DateTime.Now + "| Request XML" + msg, "");

            // SOAP REQUEST
            try
            {
                api.FareQuote(msg, ref resp, ref err);
            }
            catch (WebException excep)
            {
                Audit.Add(EventType.SpiceJetFareQuote, Severity.High, 0, "Web Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Error: " + excep.Message);
                throw new WebException(excep.Message);
            }

            // TO REMOVE -- This code is added to run this api in case spice Jet not working
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load("C:\\test_xml\\FareQuote.xml"); //fareRes1.xml
            //resp = xmlDoc.InnerXml; 

            if (err.Length != 0)
            {
                Audit.Add(EventType.SpiceJetFareQuote, Severity.High, 0, "Spice Jet error. Error Message: " + err + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Spice Jet Error: " + err);
                throw new BookingEngineException("<br>No response coming from spice jet for fare quote request");
            }

            if (resp.Length == 0)
            {
                Audit.Add(EventType.SpiceJetFareQuote, Severity.High, 0, "Spice Jet error. Error Message: No response coming from spice jet for fare quote request | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("No response coming from spice jet for fare quote request");
                throw new BookingEngineException("<br>No response coming from spice jet for fare quote request");
            }

            Audit.Add(EventType.SpiceJetFareQuote, Severity.Normal, 0, "Spice Jet FareQuote Successful: " + "|" + DateTime.Now + "| Response XML" + resp, "");

            Hashtable FareQuotesList = GenerateFareQuoteResult(resp);

            Trace.TraceInformation("SpiceJet.GetFareQuotes Exit");
            return FareQuotesList;
        }

        ///
        /// Fare Quote Request
        ///
        private string GetFareQuoteRequest(string[] fareBasisCode, string[] fareString, string[] flightString)
        {
            Trace.TraceInformation("SpiceJet.GetFareQuoteRequest entered");

            string traceid = TraceIdGeneration();

            StringBuilder message = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(message);
            writer.WriteStartElement("Request"); // --
            writer.WriteStartElement("Authorization"); //Authorization start --

            writer.WriteElementString("ClientID", ClientId); //Client Id
            writer.WriteElementString("LoginName", LoginName);
            writer.WriteElementString("Password", Password);
            writer.WriteEndElement(); //Authorization finish -- 

            writer.WriteElementString("TraceID", traceid);

            writer.WriteStartElement("Query"); // Start Query -- 

            writer.WriteStartElement("PassengerList"); //PassengerList start --
            writer.WriteStartElement("item"); //item start --
            writer.WriteElementString("TypeCode", "ADT");
            writer.WriteEndElement(); //item finish -- 
            writer.WriteEndElement(); //PassengerList finish -- 

            writer.WriteStartElement("AirComponents"); //AirComponents start --
            writer.WriteStartElement("item"); //item start --          
            //writer.WriteElementString("SelectedFareBasisCode", result.SelectedFareBasisCode[0]);
            writer.WriteElementString("SelectedFareBasisCode", fareBasisCode[0]);
            writer.WriteStartElement("Fares"); //Fares start --

            //string fareString1 = "<item>" + result.FareString[0] + "</item>";  // fare string
            string fareString1 = "<item>" + fareString[0] + "</item>";  // fare string
            TextReader tr1 = new StringReader(fareString1);
            XmlReader xr1 = XmlReader.Create(tr1);
            writer.WriteNode(xr1, false);

            writer.WriteEndElement(); //Fares finish -- 

            writer.WriteStartElement("Flights"); //Flights start --
            string fareString2 = "<item>" + flightString[0] + "</item>";   // Flight string
            TextReader tr2 = new StringReader(fareString2);
            XmlReader xr2 = XmlReader.Create(tr2);
            writer.WriteNode(xr2, false);

            writer.WriteEndElement(); //Flights finish -- 

            writer.WriteEndElement(); //item finish -- 

            if (flightString.Length > 1)
            {
                writer.WriteStartElement("item"); //item start --
                writer.WriteElementString("SelectedFareBasisCode", fareBasisCode[1]);
                writer.WriteStartElement("Fares"); //Fares start --
                string fareString3 = "<item>" + fareString[1] + "</item>";  // fare string
                TextReader tr3 = new StringReader(fareString3);
                XmlReader xr3 = XmlReader.Create(tr3);
                writer.WriteNode(xr3, false);
                writer.WriteEndElement(); //Fares finish -- 

                writer.WriteStartElement("Flights"); //Flights start --
                string fareString4 = "<item>" + flightString[1] + "</item>";   // Flight string
                TextReader tr4 = new StringReader(fareString4);
                XmlReader xr4 = XmlReader.Create(tr4);
                writer.WriteNode(xr4, false);
                writer.WriteEndElement(); //Flights finish -- 

                writer.WriteEndElement(); //item finish -- 
            }

            writer.WriteEndElement(); //AirComponents finish -- 

            writer.WriteEndElement(); // end Query --
            writer.WriteEndElement(); // end Request -- 

            writer.Close();
            Trace.TraceInformation("SpiceJet.GetFareQuoteRequest exit");
            return message.ToString();
        }

        /// <summary>
        /// Making Booking (As hold itinerary for WORLD SPAN)
        /// TODO: remove fareBassCode another variable - If class extended for individual itinerary
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BookingResponse SpiceJetBooking(FlightItinerary itinerary, int bookingAgencyId, string createdBy, string sessionId)
        {
            Trace.TraceInformation("SpiceJet.SpiceJetBooking entered");

            string msg = GetBookingRequest(itinerary, bookingAgencyId, createdBy);
            Basket.FlightBookingSession[sessionId].XmlMessage.Add(msg);
            string resp = string.Empty;
            string err = string.Empty;

            Audit.Add(EventType.SpiceJetBooking, Severity.Normal, 0, "Spice Jet Booking Request message Generated | " + DateTime.Now + "| Request XML" + msg, "");

            // SOAP REQUEST
            try
            {
                api.BookReservation(msg, ref resp, ref err);
            }
            catch (WebException excep)
            {
                Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation((Exception)excep, "Web Exception returned from SpiceJet."));
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "Web Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Error: " + excep.Message);
                throw new WebException(excep.Message);
            }
            catch (System.Web.Services.Protocols.SoapException excep)
            {
                Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation((Exception)excep, "Soap Exception returned from SpiceJet."));
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "Soap Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Error: " + excep.Message);
                throw new WebException(excep.Message);
            }
            catch (Exception excep)
            {
                Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation(excep, "Exception returned from SpiceJet."));
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Error: " + excep.Message);
                throw new BookingEngineException(excep.Message);
            }
            // START - TODO: Remove -- For testing only
            // This code is added to run this api in case spice Jet not working
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load("C:\\test_xml\\booking_prod.xml"); //SJ_BookRes1.xml
            //StringWriter sw = new StringWriter();
            //xmlDoc.Save(sw);
            //resp = sw.ToString();
            // END - TODO: Remove

            if (resp.Length == 0)
            {
                Basket.FlightBookingSession[sessionId].Log.Add("Zero length response for booking SG.");
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "No Response from Spice jet " + "|" + DateTime.Now + "| Error Message" + err, "");
                Trace.TraceError("No Response from Spice jet");
                throw new BookingEngineException("<br>Error: No Response from spice jet");
            }
            else
            {
                Basket.FlightBookingSession[sessionId].XmlMessage.Add(resp);
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "Response from Spice jet =" + resp + "|" + DateTime.Now + "| request = " + msg, "");
            }

            Trace.TraceInformation("SpiceJet.SpiceJetBooking Response received");

            // read PNR   
            BookingResponse response = ReadPNR(resp);
            if (response.Status == BookingResponseStatus.Successful || response.Status == BookingResponseStatus.BookedOther)
            {
                itinerary.PNR = response.PNR;
            }

            Trace.TraceInformation("SpiceJet.SpiceJetBooking exit");
            return response;
        }


        /// <summary>
        /// Generating Booking request
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private string GetBookingRequest(FlightItinerary itinerary, int bookingAgencyId, string createdBy)
        {
            Trace.TraceInformation("SpiceJet.GetBookingRequest entered");
            string traceid = TraceIdGeneration();
            double totalAmount = 0;
            double displayAmmount = 0;

            AgentMaster trvlBTQInfo = new AgentMaster(bookingAgencyId);
            //trvlBTQInfo.Load(bookingAgencyId); // Here we are accessing contact information of Travel Boutique

            SpiceJetSearchResult result = new SpiceJetSearchResult();

            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                if (itinerary.Passenger[i].Type != PassengerType.Infant)
                {
                    totalAmount = totalAmount + Convert.ToDouble(itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax);
                    displayAmmount = displayAmmount + Convert.ToDouble(itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax);
                }
            }

            StringBuilder message = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(message);
            writer.WriteStartElement("Request"); // --
            writer.WriteStartElement("Authorization"); //Authorization start --

            writer.WriteElementString("ClientID", ClientId); //Client Id
            writer.WriteElementString("LoginName", LoginName);
            writer.WriteElementString("Password", Password);
            writer.WriteEndElement(); //Authorization finish -- 

            writer.WriteElementString("TraceID", traceid);

            writer.WriteStartElement("Reservation"); // Start Reservation -- 
            writer.WriteElementString("CallerName", createdBy);

            writer.WriteStartElement("ContactInfo"); //ContactInfo start --
            writer.WriteElementString("FirstName", "AgentMaster"); // Here we are keeping 'AgentMaster' as First Name
            writer.WriteElementString("MiddleName", "");
            writer.WriteElementString("LastName", trvlBTQInfo.Name.Replace("&", " ")); // In the AgentMaster name may contain "&" (like Neel Tours & Travel) which causes problem in pasring. Hence replaced with &amp; trvlBTQInfo.AgencyName.Replace("&", "&amp;")
            writer.WriteElementString("EmailAddress", itinerary.Passenger[0].Email);
            writer.WriteElementString("HomePhone", itinerary.Passenger[0].CellPhone);
            writer.WriteElementString("FaxPhone", trvlBTQInfo.Fax);
            writer.WriteStartElement("Address"); //Address start --
            writer.WriteElementString("AddressName", trvlBTQInfo.Name.Replace("&", " "));
            writer.WriteElementString("Line1", trvlBTQInfo.Address.Replace("&", " "));
            writer.WriteElementString("Line2", trvlBTQInfo.Address.Replace("&", " "));
            writer.WriteElementString("Line3", "");
            writer.WriteElementString("City", trvlBTQInfo.City);
            writer.WriteElementString("State", ""); //TODO: State Name does not exist in AgentMaster information, make this loaded in AgentMaster.Load()

            writer.WriteElementString("Country", "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryName"] + ""); //TODO: Country should not be hard coded, make this loaded in AgentMaster.Load()
            writer.WriteElementString("Zip", "");
            writer.WriteEndElement(); //Address finish -- 
            writer.WriteEndElement(); //ContactInfo finish -- 

            writer.WriteStartElement("DisplayTotalAmount"); //DisplayTotalAmount start --
            writer.WriteElementString("Amount", Convert.ToString(displayAmmount));
            writer.WriteElementString("CurrencyCode", "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "");
            writer.WriteEndElement(); //DisplayTotalAmount finish -- 

            writer.WriteStartElement("TotalAmount"); //TotalAmount start --
            writer.WriteElementString("Amount", Convert.ToString(totalAmount));
            writer.WriteElementString("CurrencyCode", "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "");
            writer.WriteEndElement(); //TotalAmount finish -- 
            if (itinerary.TourCode != null && itinerary.TourCode.Length > 0)
            {
                writer.WriteElementString("PromotionCode", itinerary.TourCode);
            }
            writer.WriteStartElement("Remarks"); //Remarks start --            
            writer.WriteEndElement(); //Remarks finish -- 

            writer.WriteStartElement("Payments"); //item start --
            writer.WriteStartElement("item"); //item start --

            writer.WriteStartElement("BaseAmount"); //BaseAmount start --
            writer.WriteElementString("Amount", Convert.ToString(totalAmount));
            writer.WriteElementString("CurrencyCode", "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + ""); // TO DO: change hard coding
            writer.WriteEndElement(); //BaseAmount finish -- 

            writer.WriteStartElement("DisplayAmount"); //DisplayAmount start --
            writer.WriteElementString("Amount", Convert.ToString(displayAmmount));
            writer.WriteElementString("CurrencyCode", "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "");
            writer.WriteEndElement(); //DisplayAmount finish -- 

            //writer.WriteElementString("PaymentType", "CC"); // according to payment type - (CC, AG, or PP)
            //writer.WriteStartElement("CreditCard"); //CreditCard start -- TO DO if credit card payemment
            //writer.WriteElementString("BillingAddrLine1", "ADT");
            //writer.WriteElementString("BillingAddrPostalCode", "ADT");
            //writer.WriteElementString("BillingAddrFirstName", "ADT");
            //writer.WriteElementString("BillingAddrLastName", "ADT");
            //writer.WriteElementString("CreditCardVendorCode", "ADT");
            //writer.WriteElementString("BillingAddrCountryCode", "ADT");
            //writer.WriteElementString("BillingAddrCity", "ADT");
            //writer.WriteElementString("CreditCardExpirationDate", "ADT");
            //writer.WriteElementString("CreditCardNumber", "ADT");
            //writer.WriteElementString("BillingAddrStateProvCode", "ADT");
            //writer.WriteElementString("CVV", "ADT");
            //writer.WriteEndElement(); //CreditCard finish -- 

            writer.WriteElementString("PaymentType", "AG"); // for AgentMaster Payment
            writer.WriteStartElement("AgencyPayment"); // AgencyPayment Start --
            writer.WriteElementString("Comment", "");
            writer.WriteElementString("AuthorizationCode", ""); // TO Do: fill data
            writer.WriteElementString("AgencyNubmer", "TBSHP57770"); // To do fill data
            writer.WriteEndElement(); //AgencyPayment finish -- 

            writer.WriteEndElement(); //item finish -- 
            writer.WriteEndElement(); //Payments finish -- 

            writer.WriteElementString("NumberPassengers", Convert.ToString(itinerary.Passenger.Length));
            writer.WriteStartElement("PassengerList"); //PassengerList start --

            for (int j = 0; j < itinerary.Passenger.Length; j++)
            {
                string passenger = string.Empty;
                if (itinerary.Passenger[j].Type == PassengerType.Adult)
                {
                    passenger = "ADT";
                }
                if (itinerary.Passenger[j].Type == PassengerType.Child)
                {
                    passenger = "CHD";
                }
                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                {
                    passenger = "INF"; //TODo: Spice Jet does not support Infant booking
                }
                if (itinerary.Passenger[j].Type == PassengerType.Senior)
                {
                    passenger = "ADT"; //TODO: Check whether spice jet supports senior
                }
                writer.WriteStartElement("item"); //item start --
                writer.WriteElementString("Fees", "");
                writer.WriteElementString("TypeCode", passenger);  // TO DO: SpiceJet by takes only ADT
                writer.WriteElementString("Title", itinerary.Passenger[j].Title);
                writer.WriteElementString("FirstName", itinerary.Passenger[j].FirstName);
                writer.WriteElementString("LastName", itinerary.Passenger[j].LastName);
                writer.WriteEndElement(); //item finish -- 
            }

            writer.WriteEndElement(); //PassengerList finish -- 

            //IN case of one way if flight is DEL-BOM-BLR where DEL is origin and BLR is destination

            writer.WriteStartElement("AirComponents"); //AirComponents start --

            writer.WriteStartElement("item"); //item start --
            writer.WriteElementString("SelectedFareBasisCode", itinerary.FareRules[0].FareBasisCode);
            writer.WriteStartElement("Flights"); //Flights start --
            writer.WriteStartElement("item"); //item start --

            writer.WriteElementString("DepartureDateTime", itinerary.Segments[0].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'").ToUpper());
            writer.WriteElementString("CarrierCode", itinerary.Segments[0].Airline);
            writer.WriteElementString("FlightNumber", itinerary.Segments[0].FlightNumber);
            writer.WriteElementString("Origin", itinerary.Segments[0].Origin.CityCode);
            writer.WriteElementString("Destination", itinerary.Destination);

            writer.WriteEndElement(); //item finish -- 
            writer.WriteEndElement(); //Flights finish -- 
            writer.WriteEndElement(); //item finish -- 

            // Second flight -- return flight
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (itinerary.Destination == itinerary.Segments[i].Origin.AirportCode)
                {
                    writer.WriteStartElement("item"); //item start --
                    writer.WriteElementString("SelectedFareBasisCode", itinerary.FareRules[1].FareBasisCode);
                    writer.WriteStartElement("Flights"); //Flights start --
                    writer.WriteStartElement("item"); //item start --
                    writer.WriteElementString("DepartureDateTime", itinerary.Segments[i].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'").ToUpper());
                    writer.WriteElementString("CarrierCode", itinerary.Segments[i].Airline);
                    writer.WriteElementString("FlightNumber", itinerary.Segments[i].FlightNumber);
                    writer.WriteElementString("Origin", itinerary.Segments[i].Origin.CityCode);
                    writer.WriteElementString("Destination", itinerary.Origin);
                    writer.WriteEndElement(); //item finish -- 
                    writer.WriteEndElement(); //Flights finish -- 
                    writer.WriteEndElement(); //item finish -- 
                }
            }

            writer.WriteEndElement(); //AirComponents finish -- 


            writer.WriteEndElement(); // end Reservation --
            writer.WriteEndElement(); // end Request -- 

            writer.Close();

            Trace.TraceInformation("SpiceJet.GetBookingRequest exit");
            return message.ToString();
        }

        /// <summary>
        /// Reading PNR from Booking response
        /// </summary>
        /// <param name="resp"></param>
        /// <returns></returns>
        private BookingResponse ReadPNR(string response)
        {
            Trace.TraceInformation("SpiceJet.ReadPNR entered");
            BookingResponse pnrData = new BookingResponse();
            XmlNode tempNode;

            TextReader textReader = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(textReader);

            // CHECKING FOR ANY error in Book response
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "urn:os:bookreservation");
            nsmgr.AddNamespace("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
            nsmgr.AddNamespace("type", "ns1:BookReservationResponse");
            nsmgr.AddNamespace("ns2", "http://xml.apache.org/xml-soap");
            nsmgr.AddNamespace("type", "ns2:Vector");
            nsmgr.AddNamespace("type", "ns1:PassengerNameRecord");

            //THIS IS A FIX: Creating another name space because - Spice Jet PROD is not updated according to TEST Version
            // So checking Display reservation type

            // namespace creating - FIX
            XmlNamespaceManager nsmgrProd = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgrProd.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgrProd.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgrProd.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgrProd.AddNamespace("ns1", "urn:os:displayreservation");
            nsmgrProd.AddNamespace("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
            nsmgrProd.AddNamespace("type", "ns1:DisplayReservationResponse");
            nsmgrProd.AddNamespace("ns2", "http://xml.apache.org/xml-soap");
            nsmgrProd.AddNamespace("type", "ns2:Vector");
            nsmgrProd.AddNamespace("type", "ns1:Reservation");

            // prod version error information - FIX
            XmlNode ErrorInfoProd = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Exceptions/item/MessageText", nsmgrProd);

            // checking for any error found from spice jet  
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:bookreservationResponse/return/Exceptions/item/MessageText", nsmgr);

            // checking for any error found in "fault string" from spice jet           
            XmlNode errorInfProd = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring", nsmgrProd);

            XmlNode errorInf = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring", nsmgr);

            if ((ErrorInfo != null) || ((ErrorInfo != null) && (ErrorInfo.InnerText != "")) || (ErrorInfoProd != null) || ((ErrorInfoProd != null) && (ErrorInfoProd.InnerText != "")))
            {
                if ((ErrorInfo != null) && (ErrorInfo.InnerText != null))
                {
                    Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "Exception returned from SpiceJet. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| request XML" + response, "");
                    Trace.TraceError("Spice Jet Booking Error: " + ErrorInfo.InnerText);
                    pnrData = new BookingResponse(BookingResponseStatus.Failed, ErrorInfo.InnerText, string.Empty);
                }
                else if ((ErrorInfoProd != null) && (ErrorInfoProd.InnerText != null))
                {
                    Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "Exception returned from SpiceJet. Error Message:" + ErrorInfoProd.InnerText + " | " + DateTime.Now + "| request XML" + response, "");
                    Trace.TraceError("Spice Jet Booking Error: " + ErrorInfoProd.InnerText);
                    pnrData = new BookingResponse(BookingResponseStatus.Failed, ErrorInfoProd.InnerText, string.Empty);
                }
                // Exception is not thrown because - If response is not successful it will not return PNR no and in this case we are showing message - Unable to book itinerary.
                //throw new BookingEngineException("<br>Error: " + ErrorInfo.InnerText);
            }
            else if ((errorInf != null) || ((errorInf != null) && (errorInf.InnerText != "")) || (errorInfProd != null) || ((errorInfProd != null) && (errorInfProd.InnerText != "")))
            {
                if ((errorInf != null) && (errorInf.InnerText != null))
                {
                    Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "FaultString returned from SpiceJet. Error Message:" + errorInf.InnerText + " | " + DateTime.Now + "| request XML" + response, "");
                    Trace.TraceError("Spice Jet Booking Error: " + errorInf.InnerText);
                    pnrData = new BookingResponse(BookingResponseStatus.Failed, errorInf.InnerText, string.Empty);
                }
                else if ((errorInfProd != null) && (errorInfProd.InnerText != null))
                {
                    Audit.Add(EventType.SpiceJetBooking, Severity.High, 0, "FaultString returned from SpiceJet. Error Message:" + errorInfProd.InnerText + " | " + DateTime.Now + "| request XML" + response, "");
                    Trace.TraceError("Spice Jet Booking Error: " + errorInfProd.InnerText);
                    pnrData = new BookingResponse(BookingResponseStatus.Failed, errorInfProd.InnerText, string.Empty);
                }
            }
            else
            {
                // Auditing that Booking successfully done
                Audit.Add(EventType.SpiceJetBooking, Severity.Normal, 0, "Spice Jet Booking Successful: " + "|" + DateTime.Now + "| Response XML" + response, "");

                XmlNode PNRstart;

                // PNR info
                if (xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:bookreservationResponse/return/Pnr", nsmgr) != null)
                {
                    PNRstart = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:bookreservationResponse/return/Pnr", nsmgr);
                }
                else
                {
                    PNRstart = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Reservation/PnrReference", nsmgrProd);
                }

                // PNR ID
                if (PNRstart != null)
                {
                    tempNode = PNRstart.SelectSingleNode("PNRId");
                    if (tempNode != null)
                    {
                        pnrData = new BookingResponse(BookingResponseStatus.Successful, string.Empty, tempNode.InnerText);
                    }

                    if (pnrData.Status == BookingResponseStatus.Successful)
                    {
                        Trace.TraceInformation("SpiceJet.ReadPNR exiting : Booking successful.");
                    }
                    else if (pnrData.Status == BookingResponseStatus.Failed)
                    {
                        Trace.TraceInformation("SpiceJet.ReadPNR exiting : Booking failed.");
                    }
                }
                else
                {
                    pnrData = new BookingResponse(BookingResponseStatus.Failed, "No response from Spice Jet", string.Empty);
                }
            }
            return pnrData;
        }

        /// <summary>
        /// Display reservation
        /// </summary>
        /// <param name="resp"></param>
        /// <returns></returns>
        public ArrayList DisplayReservation(string pnr)
        {
            Trace.TraceInformation("SpiceJet.DisplayReservation entered");

            string msg = GetDisplayReservationRequest(pnr);
            string resp = string.Empty;
            string err = string.Empty;

            Audit.Add(EventType.SpiceJetDisplayPNR, Severity.Normal, 0, "Spice Jet Display Reservation message Generated | " + DateTime.Now + "| Request XML" + msg, "");

            // SOAP REQUEST
            try
            {
                api.DisplayReservation(msg, ref resp, ref err);
            }
            catch (WebException excep)
            {
                Audit.Add(EventType.DisplayPNR, Severity.High, 0, "Web Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Error: " + excep.Message);
                throw new WebException(excep.Message);
            }

            // START - TODO: Remove -- For testing only
            // This code is added to run this api in case spice Jet not working
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load("C:\\test_xml\\SJ_DisplayRes1.xml");
            //StringWriter sw = new StringWriter();
            //xmlDoc.Save(sw);
            //resp = sw.ToString();
            // END - TODO: Remove

            if (resp.Length == 0)
            {
                Trace.TraceError("Unable to establish connection with Spice Jet");
                throw new BookingEngineException("<br>Error:Unable to establish connection with Spice Jet");
            }

            Audit.Add(EventType.SpiceJetDisplayPNR, Severity.Normal, 0, "Spice Jet Booking Reservation message Generated | " + DateTime.Now + "| Request XML" + msg, "");

            // read PNR
            //Hashtable pnr = new Hashtable();
            //pnr = ReadPNR(resp);

            //PNRid
            // Display reservation
            ArrayList info = ReadDisplayResResponse(resp);
            Trace.TraceInformation("SpiceJet.DisplayReservation exit");

            return info;
        }

        /// <summary>
        /// Generating Display Reservation Request
        /// </summary>
        /// <param name="pnr"></param>
        /// <returns></returns>
        private string GetDisplayReservationRequest(string pnr)
        {
            Trace.TraceInformation("SpiceJet.GetDisplayReservationRequest entered");
            string traceid = TraceIdGeneration();

            StringBuilder message = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(message);
            writer.WriteStartElement("Request"); // start Request --
            writer.WriteStartElement("Authorization"); //Authorization start --

            writer.WriteElementString("ClientID", ClientId); //Client Id
            writer.WriteElementString("LoginName", LoginName);
            writer.WriteElementString("Password", Password);
            writer.WriteEndElement(); //Authorization finish -- 

            writer.WriteElementString("TraceID", traceid);

            writer.WriteStartElement("Query"); // Start Query -- 

            writer.WriteElementString("pnrid", pnr);

            writer.WriteEndElement(); // end Query --
            writer.WriteEndElement(); // end Request -- 

            writer.Close();
            Trace.TraceInformation("SpiceJet.GetDisplayReservationRequest exit");
            return message.ToString();
        }

        /// <summary>
        /// Private method for calling fare for each paeenger type
        /// </summary>
        /// <param name="passengerCount">count of passenger of this type</param>
        /// <param name="passengerType">Passenger Type i.e. Adult, Child, Senior etc.</param>
        /// <param name="numPassenger">total number of passenger</param>
        /// <returns></returns>
        private static Fare GetFareByPassenger(int passengerCount, string passType, int numPassenger, double baseFare)
        {
            Trace.TraceInformation("SpiceJet.GetFareByPassenger exit");
            Fare fare = new Fare();

            // passenger type
            //tempNode = xmlNodeSecond.SelectSingleNode("PassengerTypeCode");
            fare.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), passType);

            fare.BaseFare = (baseFare * (passengerCount)) / Convert.ToInt32(numPassenger);
            fare.TotalFare = (baseFare * (passengerCount)) / Convert.ToInt32(numPassenger);
            // No of passenger
            fare.PassengerCount = passengerCount;
            Trace.TraceInformation("SpiceJet.GetFareByPassenger exit");
            return fare;
        }

        /// <summary>
        /// Date parsing for - date Format: yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        private static DateTime ParseDate(string dateString)
        {
            string dateStr = dateString.Substring(0, 10) + " " + dateString.Substring(11, 8);
            DateTime dt = DateTime.ParseExact(dateStr, "yyyy-MM-dd HH:mm:ss", null);
            return dt;
        }

        /// <summary>
        /// This will give TRUE of False for sending request for search even if for corresponding origin destination this GDS's data does not exist in database
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <param name="isDomestic"></param>
        /// <returns></returns>
        public static bool SearchForSpiceJet(string origin, string destination, bool isDomestic)
        {
            Trace.TraceInformation("SpiceJet.SearchForSpiceJet exit");
            bool passForSearch = false;

            // check if origin destination is in the given pair
            if (isDomestic == true)
            {
                passForSearch = true;
            }

            Trace.TraceInformation("SpiceJet.SearchForSpiceJet exit");
            return passForSearch;
        }


        /// <summary>
        /// Read Display Reservation response for Booking Purpose
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private ArrayList ReadDisplayResResponse(string response)
        {
            Trace.TraceInformation("SpiceJet.ReadDisplayResResponse Enter.");

            XmlNode tempNode;
            ArrayList flightLegs = new ArrayList();

            TextReader stringRead = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);

            //Hashtable pnrData = new Hashtable();

            // namespace creating
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/"); //SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "urn:os:displayreservation");
            nsmgr.AddNamespace("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
            nsmgr.AddNamespace("type", "ns1:DisplayReservationResponse");
            nsmgr.AddNamespace("ns2", "http://xml.apache.org/xml-soap");
            nsmgr.AddNamespace("type", "ns2:Vector");
            nsmgr.AddNamespace("type", "ns1:Reservation");

            // checking for any error found from spice jet            
            // earlier it was 'displayReservationResponse' in place of 'bookreservationResponse'

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Exceptions/item/MessageText", nsmgr);

            // checking for any error found from spice jet 
            if ((ErrorInfo != null) || ((ErrorInfo != null) && (ErrorInfo.InnerText != "")))
            {
                if ((ErrorInfo != null) && (ErrorInfo.InnerText != null))
                {
                    Audit.Add(EventType.Book, Severity.High, 0, "Exception returned from SpiceJet. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| request XML" + response, "");
                    Trace.TraceError("Error: " + ErrorInfo.InnerText);
                    throw new BookingEngineException("Spice Jet Error: " + ErrorInfo.InnerText);
                }
            }
            else
            {
                // PNR info
                XmlNodeList flightInfo = xmlDoc.SelectNodes("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Reservation/AirComponents/item/Flights/item/FlightLegs/item", nsmgr);

                foreach (XmlNode info in flightInfo)
                {
                    tempNode = info.SelectSingleNode("DepartureTerminal");

                    if (tempNode != null)
                    {
                        flightLegs.Add(tempNode.InnerText);
                    }
                }
            }

            Trace.TraceInformation("SpiceJet.ReadDisplayResResponse Exit ");

            return flightLegs;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="dest"></param>
        /// <param name="fareBasisCode"></param>
        /// <returns></returns>

        public static List<FareRule> GetFareRule(string origin, string dest, string fareBasisCode)
        {
            Trace.TraceInformation("SpiceJet.GetFareRule Enter");
            List<FareRule> fareRuleList = new List<FareRule>();

            FareRule SGfareRules = new FareRule();
            SGfareRules.Origin = origin;
            SGfareRules.Destination = dest;
            SGfareRules.Airline = "SG";
            SGfareRules.FareRuleDetail = Util.GetLCCFareRules("SG");
            SGfareRules.FareBasisCode = fareBasisCode;

            fareRuleList.Add(SGfareRules);

            Trace.TraceInformation("SpiceJet.GetFareRule exit");
            return fareRuleList;
        }

        /// <summary>
        /// Display PNR - For Import PNR
        /// </summary>
        /// <param name="resp"></param>
        /// <returns></returns>
        public FlightItinerary RetrieveItinerary(string pnr, ref string fareBasisCodeOB, ref string fareBasisCodeIB)
        {
            Trace.TraceInformation("SpiceJet.RetrieveItinerary entered");

            string msg = GetDisplayReservationRequest(pnr);
            string resp = string.Empty;
            string err = string.Empty;

            Audit.Add(EventType.SpiceJetDisplayPNR, Severity.Normal, 0, "Spice Jet Display Reservation message Generated | " + DateTime.Now + "| Request XML" + msg, "");

            // SOAP REQUEST
            try
            {
                api.DisplayReservation(msg, ref resp, ref err);
            }
            catch (WebException excep)
            {
                Audit.Add(EventType.DisplayPNR, Severity.High, 0, "Web Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + msg, "");
                Trace.TraceError("Error: " + excep.Message);
                throw new WebException(excep.Message);
            }

            // START - TODO: Remove -- For testing only
            // This code is added to run this api in case spice Jet not working
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load("C:\\test_xml\\SJ_DisplayRes1.xml");
            //StringWriter sw = new StringWriter();
            //xmlDoc.Save(sw);
            //resp = sw.ToString();
            // END - TODO: Remove

            if (resp.Length == 0)
            {
                Trace.TraceError("Unable to establish connection with Spice Jet");
                throw new BookingEngineException("<br>Error:Unable to establish connection with Spice Jet");
            }

            Audit.Add(EventType.SpiceJetDisplayPNR, Severity.Normal, 0, "Spice Jet Display Reservation message Generated | " + DateTime.Now + "| Request XML" + msg, "");

            // read PNR
            //Hashtable pnr = new Hashtable();
            //pnr = ReadPNR(resp);

            //PNRid
            // Display reservation
            FlightItinerary itinerary = ReadFullDisplayResResponse(resp, ref fareBasisCodeOB, ref fareBasisCodeIB);
            Trace.TraceInformation("SpiceJet.RetrieveItinerary exit");

            return itinerary;
        }

        private FlightItinerary ReadFullDisplayResResponse(string response, ref string fareBasisCodeOB, ref string fareBasisCodeIB)
        {
            Trace.TraceInformation("SpiceJet.ReadDisplayResResponse Enter.");

            XmlNode tempNode;
            FlightItinerary itinerary = new FlightItinerary();

            TextReader stringRead = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);

            //Hashtable pnrData = new Hashtable();

            // namespace creating
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/"); //SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "urn:os:displayreservation");
            nsmgr.AddNamespace("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
            nsmgr.AddNamespace("type", "ns1:DisplayReservationResponse");
            nsmgr.AddNamespace("ns2", "http://xml.apache.org/xml-soap");
            nsmgr.AddNamespace("type", "ns2:Vector");
            nsmgr.AddNamespace("type", "ns1:Reservation");

            // checking for any error found from spice jet            
            // earlier it was 'displayReservationResponse' in place of 'bookreservationResponse'

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Exceptions/item/MessageText", nsmgr);

            // checking for any error found from spice jet 
            if ((ErrorInfo != null) || ((ErrorInfo != null) && (ErrorInfo.InnerText != "")))
            {
                if ((ErrorInfo != null) && (ErrorInfo.InnerText != null))
                {
                    Audit.Add(EventType.SpiceJetDisplayPNR, Severity.High, 0, "Exception returned from SpiceJet. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| request XML" + response, "");
                    Trace.TraceError("Error: " + ErrorInfo.InnerText);
                    throw new BookingEngineException("Spice Jet Error: " + ErrorInfo.InnerText);
                }
            }
            else
            {
                // PNR info
                XmlNodeList flightInfo = xmlDoc.SelectNodes("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Reservation/AirComponents/item", nsmgr);
                List<FlightInfo> flightSegments = new List<FlightInfo>();
                Dictionary<string, Fare> PaxPrice = new Dictionary<string, Fare>();
                string currency = string.Empty;
                int round = 0;
                foreach (XmlNode info in flightInfo)
                {
                    //FARE INFORMATION 
                    XmlNodeList fares = info.SelectNodes("Fares/item");
                    foreach (XmlNode pInfo in fares)
                    {
                        if (!PaxPrice.ContainsKey(pInfo.SelectSingleNode("PassengerTypeCode").InnerText))
                        {
                            Fare price = new Fare();
                            tempNode = pInfo.SelectSingleNode("TotalAmount/CurrencyCode");
                            if (tempNode != null)
                            {
                                currency = tempNode.InnerText;
                            }

                            tempNode = pInfo.SelectSingleNode("BaseAmount/Amount");
                            if (tempNode != null)
                            {
                                price.BaseFare = Convert.ToDouble(tempNode.InnerText);
                            }

                            tempNode = pInfo.SelectSingleNode("TotalAmount/Amount");
                            if (tempNode != null)
                            {
                                price.TotalFare = Convert.ToDouble(tempNode.InnerText);
                            }

                            tempNode = pInfo.SelectSingleNode("PassengerTypeCode");
                            if (tempNode != null)
                            {
                                if (tempNode.InnerText == "ADT")
                                {
                                    price.PassengerType = PassengerType.Adult;
                                }
                                else if (tempNode.InnerText == "CHD")
                                {
                                    price.PassengerType = PassengerType.Child;
                                }
                                else if (tempNode.InnerText == "INF")
                                {
                                    price.PassengerType = PassengerType.Infant;
                                }
                            }
                            tempNode = pInfo.SelectSingleNode("NumPaxTypes");
                            if (tempNode != null)
                            {
                                price.PassengerCount = Convert.ToInt32(tempNode.InnerText);
                            }
                            PaxPrice.Add(pInfo.SelectSingleNode("PassengerTypeCode").InnerText, price);
                        }
                        else
                        {
                            tempNode = pInfo.SelectSingleNode("BaseAmount/Amount");
                            if (tempNode != null)
                            {
                                PaxPrice[pInfo.SelectSingleNode("PassengerTypeCode").InnerText].BaseFare += Convert.ToDouble(tempNode.InnerText);
                            }

                            tempNode = pInfo.SelectSingleNode("TotalAmount/Amount");
                            if (tempNode != null)
                            {
                                PaxPrice[pInfo.SelectSingleNode("PassengerTypeCode").InnerText].TotalFare += Convert.ToDouble(tempNode.InnerText);
                            }
                        }
                        if (round == 0)
                        {
                            fareBasisCodeOB = pInfo.SelectSingleNode("FareBasisCode").InnerText;
                        }
                        if (round == 1)
                        {
                            fareBasisCodeIB = pInfo.SelectSingleNode("FareBasisCode").InnerText;
                        }
                    }

                    // FLIGHT INFORMATION
                    itinerary.Origin = info.SelectSingleNode("Flights/item/Origin").InnerText;
                    itinerary.Destination = info.SelectSingleNode("Flights/item/Destination").InnerText;
                    itinerary.FlightBookingSource = BookingSource.SpiceJet;
                    itinerary.Ticketed = true;
                    itinerary.PNR = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Reservation/PnrReference/PNRId", nsmgr).InnerText;

                    XmlNodeList flights = info.SelectNodes("Flights/item/FlightLegs/item");
                    //itinerary.Segments = new FlightInfo[flights.Count];                   

                    foreach (XmlNode fInfo in flights)
                    {
                        FlightInfo flInfo = new FlightInfo();
                        flInfo.Airline = "SG";
                        flInfo.CreatedOn = DateTime.Now.ToUniversalTime();
                        flInfo.LastModifiedOn = DateTime.Now.ToUniversalTime();

                        tempNode = info.SelectSingleNode("Flights/item/FlightNumber");
                        if (tempNode != null)
                        {
                            flInfo.FlightNumber = tempNode.InnerText;
                        }
                        else
                        {
                            // Flight number cannot be empty
                            Audit.Add(EventType.SpiceJetDisplayPNR, Severity.High, 0, "Error in Spice Jet: Flight number is empty | " + DateTime.Now + "| Response XML" + response, "");
                            Trace.TraceError("Spice Jet Error: Flight number is empty");
                            throw new BookingEngineException("<br> Flight number is empty");
                        }

                        // Meal type
                        tempNode = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Reservation/AirComponents/item/Fares/Meal", nsmgr);

                        if (tempNode != null)
                        {
                            flInfo.MealType = tempNode.InnerText;
                        }
                        else
                        {
                            flInfo.MealType = string.Empty;
                        }

                        // Booking Class -- Spice Jet show fares for only single Booking Class - Economy Class (For Economy class code is 'Y')
                        flInfo.BookingClass = "Y";
                        // As for spice Jet no ground time coming from spice Jet
                        flInfo.GroundTime = new TimeSpan();
                        flInfo.Duration = (flInfo.ArrivalTime - flInfo.DepartureTime);

                        flInfo.AirlinePNR = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Reservation/PnrReference/PNRId", nsmgr).InnerText;
                        flInfo.FlightStatus = FlightStatus.Confirmed;
                        //flInfo.FlightNumber
                        //flInfo.Status
                        //flInfo.ETicketEligible   
                        //flInfo.Stops

                        tempNode = fInfo.SelectSingleNode("ScheduledDepartureTime");
                        if (tempNode != null)
                        {
                            flInfo.DepartureTime = ParseDate(tempNode.InnerText);
                        }
                        else
                        {
                            // Schedule departure time cannot be empty
                            Audit.Add(EventType.SpiceJetDisplayPNR, Severity.High, 0, "Error in Spice Jet Display PNR: Departure Time is empty | " + DateTime.Now + "| Response XML" + response, "");
                            Trace.TraceError("Spice Jet Error: Departure Time is empty");
                            throw new BookingEngineException("Departure Time is empty");
                        }

                        tempNode = fInfo.SelectSingleNode("ScheduledArrivalTime");
                        if (tempNode != null)
                        {
                            flInfo.ArrivalTime = ParseDate(tempNode.InnerText);
                        }
                        else
                        {
                            // Schedule departure time cannot be empty
                            Audit.Add(EventType.SpiceJetDisplayPNR, Severity.High, 0, "Error in Spice Jet Display PNR: Arrival Time is empty | " + DateTime.Now + "| Response XML" + response, "");
                            Trace.TraceError("Spice Jet Error: Arrival Time is empty");
                            throw new BookingEngineException("Arrival Time is empty");
                        }

                        tempNode = fInfo.SelectSingleNode("ArrivalAirport");
                        if (tempNode != null)
                        {
                            flInfo.Destination = new Airport(tempNode.InnerText);
                        }

                        tempNode = fInfo.SelectSingleNode("ArrivalTerminal");
                        if (tempNode != null)
                        {
                            flInfo.ArrTerminal = tempNode.InnerText;
                        }

                        tempNode = fInfo.SelectSingleNode("DepartureTerminal");
                        if (tempNode != null)
                        {
                            flInfo.DepTerminal = tempNode.InnerText;
                        }

                        tempNode = fInfo.SelectSingleNode("DepartureAirport");
                        if (tempNode != null)
                        {
                            flInfo.Origin = new Airport(tempNode.InnerText);
                        }
                        flightSegments.Add(flInfo);
                    }
                    round++;
                }
                flightSegments.TrimExcess();
                itinerary.Segments = flightSegments.ToArray();

                // Passenger Information               
                XmlNodeList paxInfo = xmlDoc.SelectNodes("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:displayReservationResponse/return/Reservation/PassengerList/item", nsmgr);
                itinerary.Passenger = new FlightPassenger[paxInfo.Count];
                int j = 0;
                foreach (XmlNode info in paxInfo)
                {
                    itinerary.Passenger[j] = new FlightPassenger();
                    tempNode = info.SelectSingleNode("FirstName");
                    if (tempNode != null)
                    {
                        itinerary.Passenger[j].FirstName = tempNode.InnerText;
                    }

                    tempNode = info.SelectSingleNode("LastName");
                    if (tempNode != null)
                    {
                        itinerary.Passenger[j].LastName = tempNode.InnerText;
                    }

                    tempNode = info.SelectSingleNode("Title");
                    if (tempNode != null)
                    {
                        itinerary.Passenger[j].Title = tempNode.InnerText;
                    }

                    tempNode = info.SelectSingleNode("DateOfBirth");
                    if (tempNode != null && tempNode.InnerText.Length > 0)
                    {
                        itinerary.Passenger[j].DateOfBirth = Convert.ToDateTime(tempNode.InnerText);
                    }

                    tempNode = info.SelectSingleNode("MealPreference");
                    if (tempNode != null && tempNode.InnerText.Length > 0)
                    {
                        itinerary.Passenger[j].Meal = Meal.GetMeal(tempNode.InnerText);
                    }
                    else
                    {
                        itinerary.Passenger[j].Meal = new Meal();
                    }

                    for (int k = 0; k < PaxPrice.Count; k++)
                    {
                        itinerary.Passenger[j].Price = new PriceAccounts();
                        string passenger = string.Empty;

                        // Base fare got from response is for a single passenger
                        itinerary.Passenger[j].Price.PublishedFare = Convert.ToDecimal((PaxPrice[info.SelectSingleNode("TypeCode").InnerText].BaseFare)); //  / PaxPrice[info.SelectSingleNode("TypeCode").InnerText].PassengerCount 
                        // Total fare got from response is total of all passenger base fare + tax
                        itinerary.Passenger[j].Price.Tax = Convert.ToDecimal((PaxPrice[info.SelectSingleNode("TypeCode").InnerText].TotalFare / PaxPrice[info.SelectSingleNode("TypeCode").InnerText].PassengerCount) - PaxPrice[info.SelectSingleNode("TypeCode").InnerText].BaseFare);
                        itinerary.Passenger[j].Price.Currency = currency;
                    }

                    if (info.SelectSingleNode("TypeCode").InnerText == "ADT")
                    {
                        itinerary.Passenger[j].Type = PassengerType.Adult;
                    }
                    if (info.SelectSingleNode("TypeCode").InnerText == "CHD")
                    {
                        itinerary.Passenger[j].Type = PassengerType.Child;
                    }
                    if (info.SelectSingleNode("TypeCode").InnerText == "INF")
                    {
                        itinerary.Passenger[j].Type = PassengerType.Infant;
                    }

                    itinerary.FareType = "PUB";
                    j++;
                }

                //TODO: FareRule       
            }
            Trace.TraceInformation("SpiceJet.ReadDisplayResResponse Exit ");

            return itinerary;
        }

        // For sorting result on fare and chossing lowest fare index
        private static int GetLowestFareIndex(XmlNodeList fareArray)
        {
            int min = 0;
            List<double> fares = new List<double>();
            // Creating list of fare
            for (int k = 0; k < fareArray.Count; k++)
            {
                fares.Add(Convert.ToDouble(fareArray[k].SelectSingleNode("BaseAmount/Amount").InnerText));
            }

            for (int pass = 0; pass < fares.Count - 1; pass++)
            {
                for (int j = pass + 1; j < fares.Count; j++)
                {
                    if (fares[j] < fares[min])
                    {
                        min = j;
                    }
                }
            }

            return min;
        }

        private string BuildFareString(FlightItinerary itinerary)
        {
            decimal baseFare = 0;
            string currency = string.Empty;
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                if (itinerary.Passenger[i].Type == PassengerType.Adult)
                {
                    baseFare = itinerary.Passenger[i].Price.PublishedFare;
                    currency = itinerary.Passenger[i].Price.Currency;
                    break;
                }
            }
            return BuildFareString(baseFare, currency, itinerary.FareRules[0].FareBasisCode);
        }
        private string BuildFareString(SpiceJetSearchResult result)
        {
            return BuildFareString((SearchResult)result);
        }
        private string BuildFareString(SearchResult result)
        {
            // TODO: Remove hardcoded index.
            return BuildFareString(Convert.ToDecimal(result.FareBreakdown[0].BaseFare), result.Currency, result.FareRules[0].FareBasisCode);
        }
        private string BuildFareString(decimal fare, string currency, string fareBasisCode)
        {
            StringBuilder message = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(message);
            writer.WriteStartElement("Fare");
            writer.WriteAttributeString("xsi", "type", null, "ns1:Currency");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteStartElement("BaseAmount");
            writer.WriteAttributeString("xsi", "type", null, "ns1:Currency");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            // TODO: FareBreakdown[0], remove the hardcoded index.
            writer.WriteStartElement("Amount");
            writer.WriteAttributeString("xsi", "type", null, "xsd:double");
            writer.WriteValue(fare.ToString("0.0"));
            writer.WriteEndElement();
            writer.WriteStartElement("CurrencyCode");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteValue(currency);
            writer.WriteEndElement();
            writer.WriteEndElement();   // BaseAmount
            writer.WriteStartElement("PassengerTypeCode");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue("ADT");
            writer.WriteEndElement();
            writer.WriteStartElement("Meal");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(string.Empty);
            writer.WriteEndElement();
            writer.WriteStartElement("FareGroup");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue("R");
            writer.WriteEndElement();
            writer.WriteStartElement("ToSell");
            writer.WriteAttributeString("xsi", "type", null, "xsd:int");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue("100");
            writer.WriteEndElement();
            writer.WriteStartElement("Cabin");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue("0");
            writer.WriteEndElement();
            writer.WriteStartElement("FareBasisCode");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(fareBasisCode);
            writer.WriteEndElement();
            writer.WriteEndElement();   // Fare
            writer.Close();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(message.ToString());
            XmlNode node = doc.SelectSingleNode("/Fare");
            return node.InnerXml;
        }

        private string BuildFlightString(FlightItinerary itinerary, bool outBound)
        {
            int returnSegment = 0;
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (itinerary.Destination == itinerary.Segments[i].Destination.CityCode || itinerary.Destination == itinerary.Segments[i].Destination.AirportCode)
                {
                    returnSegment = i;
                    break;
                }
            }
            if (outBound)
            {
                FlightInfo[] segment = new FlightInfo[returnSegment + 1];
                for (int i = 0; i <= returnSegment; i++)
                {
                    segment[i] = itinerary.Segments[i];
                }
                return BuildFlightString(segment, itinerary.Origin, itinerary.Destination, itinerary.Passenger.Length);
            }
            else
            {
                int len = itinerary.Segments.Length - returnSegment - 1;
                FlightInfo[] segment = new FlightInfo[len];
                for (int i = 0; i < len; i++)
                {
                    segment[i] = itinerary.Segments[i + returnSegment + 1];
                }
                return BuildFlightString(segment, itinerary.Destination, itinerary.Origin, itinerary.Passenger.Length);
            }
        }
        private string BuildFlightString(SpiceJetSearchResult result, bool outBound)
        {
            return BuildFlightString((SearchResult)result, outBound);
        }
        private string BuildFlightString(SearchResult result, bool outBound)
        {
            int flightIndex = Convert.ToInt32(!outBound);
            int len = result.Flights[flightIndex].Length - 1;
            // sold count being used 0 since we don't have the passenger count at this time.
            return BuildFlightString(result.Flights[flightIndex], result.Flights[flightIndex][0].Origin.CityCode, result.Flights[flightIndex][len].Destination.CityCode, 0);
        }
        private string BuildFlightString(FlightInfo[] segment, string origin, string destination, int sold)
        {
            int len = segment.Length - 1;
            StringBuilder message = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(message);
            writer.WriteStartElement("Flight");
            writer.WriteAttributeString("xsi", "type", null, "xsd:Flight");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");

            writer.WriteStartElement("ArrivalDateTime");
            writer.WriteAttributeString("xsi", "type", null, "xsd:dateTime");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(segment[len].ArrivalTime.ToString("s") + ".000Z");
            writer.WriteEndElement();

            writer.WriteStartElement("OpSuffix");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(opSuffix);
            writer.WriteEndElement();

            writer.WriteStartElement("NumberOfStops");
            writer.WriteAttributeString("xsi", "type", null, "xsd:int");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(len.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("DepartureDateTime");
            writer.WriteAttributeString("xsi", "type", null, "xsd:dateTime");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(segment[0].DepartureTime.ToString("s") + ".000Z");
            writer.WriteEndElement();

            writer.WriteStartElement("AircraftType");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(segment[0].Craft);
            writer.WriteEndElement();

            writer.WriteStartElement("DepartureTerminal");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(segment[0].DepTerminal);
            writer.WriteEndElement();

            bool overNight = segment[0].DepartureTime.Day != segment[len].ArrivalTime.Day;
            writer.WriteStartElement("Overnight");
            writer.WriteAttributeString("xsi", "type", null, "xsd:boolean");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(overNight.ToString().ToLower());
            writer.WriteEndElement();

            writer.WriteStartElement("Status");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(string.Empty);
            writer.WriteEndElement();

            writer.WriteStartElement("Origin");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(origin);
            writer.WriteEndElement();

            writer.WriteStartElement("ArrivalTerminal");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(segment[len].ArrTerminal);
            writer.WriteEndElement();

            writer.WriteStartElement("ServiceType");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(serviceType);
            writer.WriteEndElement();

            writer.WriteStartElement("ArrivalTimeOffset");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(offSetTime);
            writer.WriteEndElement();

            writer.WriteStartElement("Destination");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(destination);
            writer.WriteEndElement();

            #region Writing FlightLegs
            writer.WriteStartElement("FlightLegs");
            writer.WriteAttributeString("xsi", "type", null, "ns2:Vector");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            for (int i = 0; i < segment.Length; i++)
            {
                writer.WriteStartElement("item");
                writer.WriteAttributeString("xsi", "type", null, "ns1:FlightLeg");

                writer.WriteStartElement("IrregularOperationIndicator");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(irregularOperationIndicator);
                writer.WriteEndElement();

                writer.WriteStartElement("Remarks");
                writer.WriteAttributeString("xsi", "type", null, "ns2:Vector");
                writer.WriteValue(remarksFlightString);
                writer.WriteEndElement();

                writer.WriteStartElement("FlightDesignator");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(segment[i].Airline + "  " + segment[i].FlightNumber);
                writer.WriteEndElement();

                writer.WriteStartElement("ScheduledDepartureTime");
                writer.WriteAttributeString("xsi", "type", null, "xsd:dateTime");
                writer.WriteValue(segment[i].DepartureTime.ToString("s") + ".000Z");
                writer.WriteEndElement();

                writer.WriteStartElement("ScheduledArrivalTime");
                writer.WriteAttributeString("xsi", "type", null, "xsd:dateTime");
                writer.WriteValue(segment[i].ArrivalTime.ToString("s") + ".000Z");
                writer.WriteEndElement();

                writer.WriteStartElement("Lid");
                writer.WriteAttributeString("xsi", "type", null, "xsd:int");
                writer.WriteValue(lid.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("DepartureDate");
                writer.WriteAttributeString("xsi", "type", null, "xsd:dateTime");
                writer.WriteValue(segment[i].DepartureTime.ToString("s") + ".000Z");
                writer.WriteEndElement();

                writer.WriteStartElement("Sold");
                writer.WriteAttributeString("xsi", "type", null, "xsd:int");
                writer.WriteValue(sold);
                writer.WriteEndElement();

                writer.WriteStartElement("DepartureAirport");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(segment[i].Origin.AirportCode);
                writer.WriteEndElement();

                writer.WriteStartElement("OpSuffix");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(opSuffix);
                writer.WriteEndElement();

                writer.WriteStartElement("FlightDetail");
                writer.WriteAttributeString("xsi", "type", null, "ns1:FlightDetail");
                writer.WriteAttributeString("xsi", "nil", null, "true");
                writer.WriteValue(string.Empty);
                writer.WriteEndElement();

                writer.WriteStartElement("AircraftType");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(segment[i].Craft);
                writer.WriteEndElement();

                writer.WriteStartElement("DepartureTerminal");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(segment[i].DepTerminal);
                writer.WriteEndElement();

                overNight = segment[i].DepartureTime.Day != segment[i].ArrivalTime.Day;
                writer.WriteStartElement("Overnight");
                writer.WriteAttributeString("xsi", "type", null, "xsd:boolean");
                writer.WriteValue(overNight.ToString().ToLower());
                writer.WriteEndElement();

                if (segment[i].Status == null)
                {
                    segment[i].Status = string.Empty;
                }
                writer.WriteStartElement("Status");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(segment[i].Status);
                writer.WriteEndElement();

                writer.WriteStartElement("ArrivalTerminal");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(segment[i].ArrTerminal);
                writer.WriteEndElement();

                writer.WriteStartElement("AircraftCode");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(segment[i].Craft);
                writer.WriteEndElement();

                writer.WriteStartElement("ServiceType");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(serviceType);
                writer.WriteEndElement();

                writer.WriteStartElement("ArrivalTimeOffset");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(offSetTime);
                writer.WriteEndElement();

                writer.WriteStartElement("JointOpDesig");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(jointOpDesignator);
                writer.WriteEndElement();

                writer.WriteStartElement("Capacity");
                writer.WriteAttributeString("xsi", "type", null, "xsd:int");
                writer.WriteValue(lid);
                writer.WriteEndElement();

                writer.WriteStartElement("DepartureTimeOffset");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(offSetTime);
                writer.WriteEndElement();

                writer.WriteStartElement("ArrivalAirport");
                writer.WriteAttributeString("xsi", "type", null, "xsd:string");
                writer.WriteValue(segment[i].Destination.AirportCode);
                writer.WriteEndElement();

                writer.WriteEndElement();   // item
            }

            writer.WriteEndElement();   // FlightLegs
            #endregion

            writer.WriteStartElement("DepartureTimeOffset");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(offSetTime);
            writer.WriteEndElement();

            writer.WriteStartElement("FlightNumber");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(segment[0].FlightNumber);
            writer.WriteEndElement();

            writer.WriteStartElement("CarrierCode");
            writer.WriteAttributeString("xsi", "type", null, "xsd:string");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteValue(segment[0].Airline);
            writer.WriteEndElement();

            writer.WriteEndElement();   // Flight
            writer.Close();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(message.ToString());
            XmlNode node = doc.SelectSingleNode("/Flight");
            return node.InnerXml;
        }

        /// <summary>
        /// To cancel the pnr from the spice jet site by screen scraping
        /// </summary>
        /// <param name="pnr">pnr to be cancelled</param>
        /// <param name="memberId">logged in member's id</param>
        /// <returns></returns>
        public bool CancelPnr(string pnr,int memberId)
        {
            CookieContainer CookieContainerObject = new CookieContainer();
            string response;
            bool isSuccess = false;
            string sid = string.Empty;
            string loginId = Convert.ToString(ConfigurationSystem.SpiceJetConfig["websiteUserName"]);
            string password = Convert.ToString(ConfigurationSystem.SpiceJetConfig["websitePassword"]);
            bool isAutoCancellationOn = Convert.ToBoolean(ConfigurationSystem.SpiceJetConfig["isAutoCancellationOns"]);
            try
            {
                sid = GetSid(memberId);
                string strPost = "event=login&module=AG&page=LOGIN_ENTRY&language=EN&mode=&sid=" + sid + "&loggedIn=&ref=&travel=2&hdnLTC=false&hdnCorporate=false&hdnSPCP=false&hdnTBF=false&iata=" + loginId + "&iata_pw=" + password + "&login=1";
                string PostUrl = "http://book.spicejet.com/skylights/cgi-bin/skylights.cgi";
                /* Login on spicejet website*/
                string loginMatch = "Welcome " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + "!";
                response = GetResponse(PostUrl, strPost, ref CookieContainerObject, memberId);
                if (response.IndexOf(loginMatch) > 0)
                {
                    /*go to pnr look up page- skipped as it is not necessary*/
                    //strPost = "event=nav&module=C3&page=PNR_LIST&language=EN&mode=&sid=" + sid + "&loggedIn=&ref=";
                    //response = GetResponse(PostUrl, strPost, sid, ref CookieContainerObject,memberId);
                    strPost = "event=lookup&module=C3&page=PNR_LIST&language=EN&mode=&sid=" + sid + "&loggedIn=1&ref=&pnr2=" + pnr + "&pnr1=" + pnr + "&sortOrderActive=ASCENDING&sortOrderPrevious=DESCENDING&sortActive=date&sortPrevious=date";
                    /*pnr look up */
                    string getItinerary = "Modify This Booking";
                    response = GetResponse(PostUrl, strPost, ref CookieContainerObject, memberId);
                    if (response.IndexOf(getItinerary) > 0 && isAutoCancellationOn)
                    {
                        strPost = "event=cancel_booking&module=C3&page=ITINERARY&language=EN&mode=&sid=" + sid + "&loggedIn=1&ref=&block_First_Leg=0=&block_Second_Leg=0&Corpmeal=off&destpage=&destmodule=&curr_seg_index=1";
                        string succPost = "<div class=\"note\"> Once you are done making changes to the booking, click Finish. </div>";
                        string succPost2 = "id=\"continueButton\" class=\"button\" value=\"finish\" onClick=\"javascript:submitFinish()";
                        /*cancellation conformation*/
                        response = GetResponse(PostUrl, strPost, ref CookieContainerObject, memberId);
                        if (response.Contains(succPost) || response.Contains(succPost2))
                        {
                            strPost = "event=finish&module=C3&page=ITINERARY&language=EN&mode=&sid=" + sid + "&loggedIn=1&ref=&block_First_Leg=0=&block_Second_Leg=0&Corpmeal=off&destpage=&destmodule=&curr_seg_index=1";
                            /*Final step - cancellation*/
                            response = GetResponse(PostUrl, strPost, ref CookieContainerObject, memberId);
                            succPost = "<p>Reservation Status:  <span class=\"pnrStatus\">CANCELED";
                            if (response.Contains(succPost))
                            {
                                isSuccess = true;
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Audit.Add(EventType.SpiceJetCancellation, Severity.High, memberId, "exception while cancellation pnr: " + exp.Message, "");
            }
            return isSuccess;
        }
        
        /// <summary>
        /// method to get sessionId form spicejet site
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        private string GetSid(int memberId)
        {
            string postUrl = "http://book.spicejet.com/skylights/cgi-bin/skylights.cgi?module=AG";
            string strResult;
            WebClient webclient = new WebClient();
            HttpWebResponse objResponse;
            HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(postUrl);
            objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                strResult = sr.ReadToEnd();
                sr.Close();
            }
            objResponse.Close();
            string sidString = "<input type='hidden' name='sid' value=";
            int sidIndex = strResult.IndexOf(sidString);
            sidString = strResult.Substring(sidIndex + sidString.Length + 1);
            int endIndex = sidString.IndexOf("' />");
            string sid = sidString.Substring(0, endIndex);
            Audit.Add(EventType.SpiceJetCancellation, Severity.Normal, memberId, "SpiceJet cancellation with sid: "+sid+"and Response: " + strResult, "");
            return sid;
        }

        /// <summary>
        /// To get the response from spicejet site by posting request  
        /// </summary>
        /// <param name="PostUrl">url to hit</param>
        /// <param name="strPost">string containing post data</param>
        /// <param name="CookieContainerObject">cookie conatiner</param>
        /// <param name="memberId">logged in member's id</param>
        /// <returns></returns>
        private string GetResponse(string PostUrl, string strPost, ref CookieContainer CookieContainerObject,int memberId)
        {
            string strResult;
            StreamWriter writer = null;
            HttpWebResponse objResponse;
            HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(PostUrl);
            objRequest.Method = "POST";
            objRequest.CookieContainer = CookieContainerObject;
            objRequest.ContentLength = strPost.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            try
            {
                writer = new StreamWriter(objRequest.GetRequestStream());
                writer.Write(strPost);
            }
            catch (Exception)
            {
            }
            finally
            {
                writer.Close();
            }
            objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                strResult = sr.ReadToEnd();
                sr.Close();
            }
            objResponse.Close();
            Audit.Add(EventType.SpiceJetCancellation, Severity.Normal, memberId, "SpiceJet cancellation Response: " + strResult, "");
            return strResult;
        }
    }
}