﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;
using System.Xml.Serialization;
using System.IO;
using CT.Configuration;
using System.Xml;

namespace CT.BookingEngine.GDS
{
    public class FlightInventory:IDisposable
    {
        private string agentBaseCurrency;
        private Dictionary<string, decimal> exchangeRates;
        private int decimalValue;
        private int appUserId;
        private string sessionId;
        private decimal ROE = 1;
        private string fileSavingPath = null;

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        public int DecimalValue
        {
            get { return decimalValue; }
            set { decimalValue = value; }
        }
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        public FlightInventory()
        {
            fileSavingPath = ConfigurationSystem.FlightInvConfig["xmlPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";            
            if (!System.IO.Directory.Exists(fileSavingPath))
            {
                System.IO.Directory.CreateDirectory(fileSavingPath);
            }
        }


        public SearchResult[] FlightSearch(SearchRequest request)
        {
            try
            {
                string filepath = fileSavingPath + "FlightInventorySearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "_"+sessionId+ ".xml";
                XmlDocument doc = CustomXmlSerializer.Serialize(request, 1, "FlightSearchRequest");
                doc.Save(filepath);

                //XmlSerializer ser = new XmlSerializer(typeof(SearchRequest));
                //string filePath = fileSavingPath + "FlightInventorySearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                //StreamWriter sw = new StreamWriter(filePath);
                //ser.Serialize(sw, request);
                //sw.Close();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "(FlightInventory)Failed to Save Search Request. Reason : " + ex.ToString(), "");
            }

            SearchResult[] result = new SearchResult[0];
            try
            {
                string Origin = null;
                string Destination = null;
                string fromDate = null;
                string toDate = null;
                string cabinClass = null;
                string preferredAirlines = null;
                if (request.Segments.Length > 0)
                {
                    Origin = request.Segments[0].Origin;
                    Destination = request.Segments[0].Destination;
                    fromDate = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");
                    if (request.Type == SearchType.OneWay)
                    {
                        toDate = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        toDate = request.Segments[1].PreferredDepartureTime.ToString("yyyy-MM-dd");
                    }
                    if (Convert.ToInt32(request.Segments[0].flightCabinClass) > 1)
                    {
                        cabinClass = request.Segments[0].flightCabinClass.ToString();
                    }
                    if (request.Segments[0].PreferredAirlines.Length > 0)
                    { 
                        for(int airlines=0;airlines<request.Segments[0].PreferredAirlines.Length;airlines++)
                        {
                            if (!string.IsNullOrEmpty(preferredAirlines))
                            {
                                preferredAirlines += "," + request.Segments[0].PreferredAirlines[airlines];
                            }
                            else {
                                preferredAirlines = request.Segments[0].PreferredAirlines[airlines];
                            }
                        }
                    }
                }
                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@Origin", Origin);
                paramList[1] = new SqlParameter("@Destination", Destination);
                paramList[2] = new SqlParameter("@FromDate", fromDate);
                paramList[3] = new SqlParameter("@ToDate", toDate);
                paramList[4] = new SqlParameter("@Adults", request.AdultCount);
                paramList[5] = new SqlParameter("@Childs", request.ChildCount);
                paramList[6] = new SqlParameter("@Infants", request.InfantCount);
                paramList[7] = new SqlParameter("@SearchType", request.Type.ToString());
                paramList[8] = new SqlParameter("@Stops", request.MaxStops);
                if (!string.IsNullOrEmpty(cabinClass))
                {
                    paramList[9] = new SqlParameter("@CabinClass", cabinClass);
                }
                else {
                    paramList[9] = new SqlParameter("@CabinClass", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(preferredAirlines))
                {
                    paramList[10] = new SqlParameter("@PrefferedAirlines", preferredAirlines);
                }
                else
                {
                    paramList[10] = new SqlParameter("@PrefferedAirlines", DBNull.Value);
                }

                DataSet ds = DBGateway.ExecuteQuery("usp_SearchByFlightInventory", paramList);
                if (ds != null && ds.Tables.Count > 0)
                {
                    List<SearchResult> lstSearchResult = new List<SearchResult>();
                    DataTable resultDet = ds.Tables[0];

                    //Getting the row count from the table based on the distinct "InvId".
                    int resultCount = resultDet.DefaultView.ToTable(true, new string[] { "InvId" }).Rows.Count;

                    for (int count = 0; count < resultCount; count++)
                    {
                        SearchResult res = new SearchResult();
                        //here getting the distinct "InvId" from the table.
                        int invId = Convert.ToInt32(resultDet.DefaultView.ToTable(true, new string[] { "InvId" }).Rows[count][0]);


                        //Reading Flight details
                        DataTable flightDet = ds.Tables[0];

                        DataRow[] onwardFlights = flightDet.Select("Origin='" + Origin + "' and Destination='" + Destination + "' and InvId=" + invId);
                        DataRow[] returnFlights = null;
                        if (request.Type.ToString() == "Return")
                        {
                            returnFlights = flightDet.Select("Origin='" + Destination + "' and Destination='" + Origin + "' and InvId=" + invId);
                        }

                        if ((request.Type == SearchType.OneWay && onwardFlights.Length > 0) || (request.Type == SearchType.Return && onwardFlights.Length == returnFlights.Length))
                        {
                            res.JourneySellKey = Convert.ToString(invId);
                            res.ResultBookingSource = BookingSource.FlightInventory;
                            if (request.Sources.Contains("CB"))
                                res.ResultBookingSource = BookingSource.CozmoBus;
                            //res.FareRules = new List<FareRule>();
                            //Added by Anji , to assing farerules to object.
                            res.FareRules = GetFareRule(Origin, Destination);
                            res.FareType = string.Empty;

                            if (request.Type == SearchType.OneWay)
                            {
                                res.Flights = new FlightInfo[1][];
                            }
                            else
                            {
                                res.Flights = new FlightInfo[2][];
                            }
                            res.FareType = "PUB";
                            res.Flights[0] = new FlightInfo[1];

                            res.Airline = onwardFlights[0]["AirlineCode"].ToString();
                            res.ValidatingAirline = res.Airline;

                            for (int i = 0; i < onwardFlights.Length; i++)
                            {
                                DataRow row = onwardFlights[i];
                                if (row["IsLCC"] != DBNull.Value)
                                {
                                    res.IsLCC = Convert.ToBoolean(row["IsLCC"]);
                                }
                                res.Flights[0][i] = new FlightInfo();
                                if (row["GroundTime"] != DBNull.Value)
                                {
                                    res.Flights[0][i].GroundTime = TimeSpan.Parse(row["GroundTime"].ToString());
                                }
                                else {
                                    res.Flights[0][i].GroundTime = new TimeSpan();
                                }
                                if (row["Origin"] != DBNull.Value)
                                {
                                    res.Flights[0][i].Origin = new Airport(row["Origin"].ToString());
                                }
                                if (row["Destination"] != DBNull.Value)
                                {
                                    res.Flights[0][i].Destination = new Airport(row["Destination"].ToString());
                                }
                                if (row["Stops"] != DBNull.Value)
                                {
                                    res.Flights[0][i].Stops = Convert.ToInt32(row["Stops"]);
                                }
                                if (row["AirlineCode"] != DBNull.Value)
                                {
                                    res.Flights[0][i].Airline = Convert.ToString(row["AirlineCode"]);
                                }
                                if (row["ArrivalTime"] != DBNull.Value)
                                {
                                    res.Flights[0][i].ArrivalTime = Convert.ToDateTime(row["ArrivalTime"]);
                                }
                                if (row["CabinClass"] != DBNull.Value)
                                {
                                    res.Flights[0][i].CabinClass = Convert.ToString(row["CabinClass"]);
                                }
                                if (row["Craft"] != DBNull.Value)
                                {
                                    res.Flights[0][i].Craft = Convert.ToString(row["Craft"]);
                                }
                                else {
                                    res.Flights[0][i].Craft = string.Empty;
                                }
                                if (row["DepartureTime"] != DBNull.Value)
                                {
                                    res.Flights[0][i].DepartureTime = Convert.ToDateTime(row["DepartureTime"]);
                                }
                                if (row["Duration"] != DBNull.Value)
                                {
                                    res.Flights[0][i].Duration = TimeSpan.Parse(row["Duration"].ToString());
                                }
                                if (row["FlightNo"] != DBNull.Value)
                                {
                                    res.Flights[0][i].FlightNumber = Convert.ToString(row["FlightNo"]);
                                }
                                if (row["Terminal"] != DBNull.Value)
                                {
                                    res.Flights[0][i].DepTerminal = Convert.ToString(row["Terminal"]);
                                }
                                else {
                                    res.Flights[0][i].DepTerminal = string.Empty;
                                }
                                res.Flights[0][i].Group = 0;
                                res.Flights[0][i].AirlinePNR = string.Empty;
                                res.Flights[0][i].ArrTerminal = string.Empty;
                                res.Flights[0][i].ETicketEligible = true;
                                res.Flights[0][i].FlightStatus = FlightStatus.Confirmed;
                                if (row["OperatingCarrier"] != DBNull.Value)
                                {
                                    res.Flights[0][i].OperatingCarrier = Convert.ToString(row["OperatingCarrier"]);
                                }
                                else {
                                    res.Flights[0][i].OperatingCarrier = string.Empty;
                                }
                                res.Flights[0][i].Status = string.Empty;
                                //res.Flights[0][i].Group = Convert.ToInt32(row["Group"]);
                                if (row["BookingClass"] != DBNull.Value)
                                {
                                    res.Flights[0][i].BookingClass = Convert.ToString(row["BookingClass"]);
                                }
                                else {
                                    res.Flights[0][i].BookingClass = string.Empty;
                                }
                                if (row["StopOver"] != DBNull.Value)
                                {
                                    res.Flights[0][i].StopOver = Convert.ToBoolean(row["StopOver"]);
                                }
                                else
                                {
                                    res.Flights[0][i].StopOver = false;
                                }

                                if (returnFlights != null && returnFlights.Length > 0)
                                {
                                    res.Flights[1] = new FlightInfo[1];
                                    for (int j = 0; j < returnFlights.Length; j++)
                                    {
                                        DataRow dr = returnFlights[i];
                                        if (dr["IsLcc"] != DBNull.Value)
                                        {
                                            res.IsLCC = Convert.ToBoolean(dr["IsLcc"]);
                                        }
                                        res.Flights[1][j] = new FlightInfo();
                                        if (dr["GroundTime"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].GroundTime = TimeSpan.Parse(dr["GroundTime"].ToString());
                                        }
                                        else {
                                            res.Flights[1][j].GroundTime = new TimeSpan();
                                        }
                                        if (dr["Origin"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].Origin = new Airport(dr["Origin"].ToString());
                                        }
                                        if (dr["Destination"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].Destination = new Airport(dr["Destination"].ToString());
                                        }
                                        if (dr["Stops"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].Stops = Convert.ToInt32(dr["Stops"]);
                                        }
                                        if (dr["AirlineCode"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].Airline = Convert.ToString(dr["AirlineCode"]);
                                        }
                                        if (dr["ArrivalTime"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].ArrivalTime = Convert.ToDateTime(dr["ArrivalTime"]);
                                        }
                                        if (dr["CabinClass"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].CabinClass = Convert.ToString(dr["CabinClass"]);
                                        }
                                        if (dr["Craft"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].Craft = Convert.ToString(dr["Craft"]);
                                        }
                                        else {
                                            res.Flights[1][j].Craft = string.Empty;
                                        }
                                        if (dr["DepartureTime"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].DepartureTime = Convert.ToDateTime(dr["DepartureTime"]);
                                        }
                                        if (dr["Duration"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].Duration = TimeSpan.Parse(dr["Duration"].ToString());
                                        }
                                        if (dr["FlightNo"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].FlightNumber = Convert.ToString(dr["FlightNo"]);
                                        }
                                        if (dr["Terminal"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].DepTerminal = Convert.ToString(dr["Terminal"]);
                                        }
                                        else {
                                            res.Flights[1][j].DepTerminal = string.Empty;
                                        }
                                        res.Flights[1][j].Group = 1;
                                        res.Flights[1][j].AirlinePNR = string.Empty;
                                        res.Flights[1][j].ArrTerminal = string.Empty;
                                        res.Flights[1][j].ETicketEligible = true;
                                        res.Flights[1][j].FlightStatus = FlightStatus.Confirmed;
                                        if (row["OperatingCarrier"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].OperatingCarrier = Convert.ToString(row["OperatingCarrier"]);
                                        }
                                        else {
                                            res.Flights[1][j].OperatingCarrier = string.Empty;
                                        }

                                        res.Flights[1][j].Status = string.Empty;
                                        //res.Flights[1][j].Group = Convert.ToInt32(row["Group"]);
                                        if (row["BookingClass"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].BookingClass = Convert.ToString(row["BookingClass"]);
                                        }
                                        else {
                                            res.Flights[1][j].BookingClass = string.Empty;
                                        }
                                        if (row["StopOver"] != DBNull.Value)
                                        {
                                            res.Flights[1][j].StopOver = Convert.ToBoolean(row["StopOver"]);
                                        }
                                        else
                                        {
                                            res.Flights[1][j].StopOver = false;
                                        }
                                    }
                                }
                            }

                            //Reading Fares
                            if (ds.Tables.Count > 1)
                            {
                                DataRow[] price = ds.Tables[1].Select("InvId=" + invId);
                                int paxcount = 1;
                                if (request.ChildCount > 0)
                                {
                                    paxcount++;
                                }
                                if (request.InfantCount > 0)
                                {
                                    paxcount++;
                                }
                                res.FareBreakdown = new Fare[paxcount];
                                string supplierCurrency = null;
                                if (price[0]["Currency"] != DBNull.Value)
                                {
                                    supplierCurrency = Convert.ToString(price[0]["Currency"]);
                                    ROE = exchangeRates[supplierCurrency];
                                }

                                res.Currency = agentBaseCurrency;
                                res.Price = new PriceAccounts();
                                int index = 0;
                                for (int k = 0; k < price.Length; k++)
                                {
                                    DataRow paxrow = price[k];
                                    if (paxrow["PaxType"] != DBNull.Value)
                                    {
                                        if (k == 0)
                                        {
                                            paxrow = price[0];
                                            res.FareBreakdown[index] = new Fare();
                                            if (paxrow["BaseFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].BaseFare = (Convert.ToDouble(paxrow["BaseFare"]) * request.AdultCount) * (double)ROE;
                                            }
                                            if (paxrow["PublishedFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].TotalFare = (Convert.ToDouble(paxrow["PublishedFare"]) * request.AdultCount) * (double)ROE;
                                            }
                                            if (paxrow["PublishedFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].SupplierFare = Convert.ToDouble(paxrow["PublishedFare"]) * request.AdultCount;
                                            }
                                            res.FareBreakdown[index].PassengerCount = request.AdultCount;
                                            res.FareBreakdown[index].PassengerType = PassengerType.Adult;

                                            res.Price.PublishedFare += (decimal)res.FareBreakdown[index].BaseFare;
                                            res.Price.Tax += res.FareBreakdown[index].Tax;
                                            res.Price.SupplierPrice += (decimal)res.FareBreakdown[index].SupplierFare;
                                            res.Price.SupplierCurrency = supplierCurrency;
                                            res.BaggageIncludedInFare = "0";

                                            index++;
                                        }
                                        else if (paxrow["PaxType"].ToString() == "CNN" && request.ChildCount > 0)
                                        {
                                            paxrow = price[1];
                                            res.FareBreakdown[index] = new Fare();
                                            if (paxrow["BaseFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].BaseFare = (Convert.ToDouble(paxrow["BaseFare"]) * request.ChildCount) * (double)ROE;

                                            } if (paxrow["PublishedFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].TotalFare = (Convert.ToDouble(paxrow["PublishedFare"]) * request.ChildCount) * (double)ROE;
                                            }
                                            if (paxrow["PublishedFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].SupplierFare = Convert.ToDouble(paxrow["PublishedFare"]) * request.ChildCount;
                                            }
                                            res.FareBreakdown[index].PassengerCount = request.ChildCount;
                                            res.FareBreakdown[index].PassengerType = PassengerType.Child;

                                            res.Price.PublishedFare += (decimal)res.FareBreakdown[index].BaseFare;
                                            res.Price.Tax += res.FareBreakdown[index].Tax;
                                            res.Price.SupplierPrice += (decimal)res.FareBreakdown[index].SupplierFare;
                                            res.Price.SupplierCurrency = supplierCurrency;
                                            res.BaggageIncludedInFare = "0";
                                            index++;
                                        }
                                        else if (paxrow["PaxType"].ToString() == "INF" && request.InfantCount > 0)
                                        {
                                            paxrow = price[2];
                                            res.FareBreakdown[index] = new Fare();
                                            if (paxrow["BaseFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].BaseFare = (Convert.ToDouble(paxrow["BaseFare"]) * request.InfantCount) * (double)ROE;
                                            }
                                            if (paxrow["PublishedFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].TotalFare = (Convert.ToDouble(paxrow["PublishedFare"]) * request.InfantCount) * (double)ROE;
                                            }
                                            if (paxrow["PublishedFare"] != DBNull.Value)
                                            {
                                                res.FareBreakdown[index].SupplierFare = Convert.ToDouble(paxrow["PublishedFare"]) * request.InfantCount;
                                            }
                                            res.FareBreakdown[index].PassengerCount = request.InfantCount;

                                            res.Price.PublishedFare += (decimal)res.FareBreakdown[index].BaseFare;
                                            res.Price.Tax += res.FareBreakdown[index].Tax;
                                            res.Price.SupplierPrice += (decimal)res.FareBreakdown[index].SupplierFare;
                                            res.Price.SupplierCurrency = supplierCurrency;
                                            res.FareBreakdown[index].PassengerType = PassengerType.Infant;
                                            res.BaggageIncludedInFare = "0";
                                            index++;
                                        }
                                    }
                                }
                            }

                            //Reading Seat Quota and PNR
                            if (ds.Tables.Count > 2)
                            {
                                DataRow[] Quota = ds.Tables[2].Select("InvId=" + invId);

                                if (Quota != null)
                                {
                                    string pnrs = string.Empty, seats = string.Empty;
                                    foreach (DataRow row in Quota)
                                    {
                                        if (row["Seats"] != DBNull.Value && row["PNR"] != DBNull.Value)
                                        {
                                            if (Convert.ToInt32(row["Seats"]) > 0)//Checking the seats count from database is greater than 0
                                            {
                                                //checking the No.of seats added for this particular InvId is reaching the request count
                                                if (GetSeatCount(seats) < request.AdultCount + request.ChildCount)
                                                {
                                                    if (Convert.ToInt32(row["Seats"]) == request.AdultCount + request.ChildCount)
                                                    {
                                                        seats = (request.AdultCount + request.ChildCount).ToString();
                                                        pnrs = row["PNR"].ToString();
                                                        break;
                                                    }
                                                    if (Convert.ToInt32(row["Seats"]) < request.AdultCount + request.ChildCount)
                                                    {
                                                        if (!string.IsNullOrEmpty(seats))
                                                        {
                                                            int seatcount = ((request.AdultCount + request.ChildCount) - Convert.ToInt32(GetSeatCount(seats)));
                                                            if (Convert.ToInt32(row["Seats"]) >= seatcount)
                                                            {
                                                                seats += "," + seatcount;
                                                            }
                                                            else if (Convert.ToInt32(row["Seats"]) < seatcount)
                                                            {
                                                                seats += "," + Convert.ToInt32(row["Seats"]);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            seats = (Convert.ToInt32(row["Seats"])).ToString();
                                                        }
                                                        if (!string.IsNullOrEmpty(pnrs))
                                                        {
                                                            //pnrs += "," + row["PNR"].ToString();
                                                            pnrs += "/" + row["PNR"].ToString();
                                                        }
                                                        else
                                                        {
                                                            pnrs = row["PNR"].ToString();
                                                        }
                                                    }
                                                    if (Convert.ToInt32(row["Seats"]) > request.AdultCount + request.ChildCount)
                                                    {
                                                        if (!string.IsNullOrEmpty(seats))
                                                        {
                                                            int seatcount = ((request.AdultCount + request.ChildCount) - Convert.ToInt32(GetSeatCount(seats)));
                                                            if (Convert.ToInt32(row["Seats"]) >= seatcount)
                                                            {
                                                                seats += "," + seatcount;
                                                            }
                                                            else if (Convert.ToInt32(row["Seats"]) < seatcount)
                                                            {
                                                                seats += "," + Convert.ToInt32(row["Seats"]);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            seats = Convert.ToString(request.AdultCount + request.ChildCount);
                                                        }
                                                        if (!string.IsNullOrEmpty(pnrs))
                                                        {
                                                            pnrs += "," + row["PNR"].ToString();
                                                        }
                                                        else
                                                        {
                                                            pnrs = row["PNR"].ToString();
                                                        }
                                                        break;
                                                    }
                                                }
                                                else //if the request paxcount is reached for the particular InvId then we are breaking the loop.
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    res.GUID = seats;
                                    res.FareSellKey = pnrs;
                                }
                            }

                            res.BaseFare = (double)res.Price.PublishedFare;
                            res.Tax = (double)res.Price.Tax;
                            res.TotalFare = res.BaseFare + res.Tax;

                            lstSearchResult.Add(res);
                        }
                        result = lstSearchResult.ToArray();
                    }

                }
                try
                {
                    string filePath = fileSavingPath + "FlightInventorySearchResults_DataSet_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "_" + sessionId + ".xml";
                    XmlDocument doc = CustomXmlSerializer.Serialize(result, 1, "DataSet");
                    doc.Save(filePath);

                    string filePath1 = fileSavingPath + "FlightInventorySearchResults_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "_" + sessionId + ".xml";
                    XmlDocument doc1 = CustomXmlSerializer.Serialize(result, 1, "SearchResult");
                    doc1.Save(filePath1);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.High, 1, "(FlightInventory)Failed to Save Search Results. Reason : " + ex.ToString(), "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "FlightInventory Search Error:" + ex.ToString(), string.Empty);
                throw ex;
            }
            return result;
        }

        private int GetSeatCount(string Seats)
        {
            int count = 0;
            if (!string.IsNullOrEmpty(Seats))
            {
                string[] seatsCount = Seats.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < seatsCount.Length; i++)
                {
                    count += Convert.ToInt32(seatsCount[i]);
                }
            }
            return count;
        }

        public BookingResponse Book(ref FlightItinerary itinerary)
        {
            try
            {
                string filepath = fileSavingPath + "FlightInventoryBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "_" + sessionId + ".xml";
                XmlDocument doc = CustomXmlSerializer.Serialize(itinerary, 1, "FlightItitnerary");
                doc.Save(filepath);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(FlightInventory)Failed to serialize Booking Request. Reason : " + ex.ToString(), "");
            }


            BookingResponse response = new BookingResponse();
            try
            {
                Audit.Add(EventType.Book, Severity.High, 1, "(values): itinerary.PNR=" + itinerary.PNR + ",itinerary.Quota=" + itinerary.GUID.ToString() + ",itinerary.InventoryId=" + itinerary.SpecialRequest.ToString(), "");
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@PNR", itinerary.PNR);
                paramList[1] = new SqlParameter("@SeatQuota", itinerary.GUID);
                paramList[2] = new SqlParameter("@InventoryId", itinerary.SpecialRequest);
                paramList[3] = new SqlParameter("@supplierPNR", SqlDbType.NVarChar, 10);
                paramList[3].Direction = ParameterDirection.Output;
                paramList[4] = new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 200);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@ErrorCode", SqlDbType.NVarChar, 2);
                paramList[5].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("usp_FlightInventoryBooking", paramList);
                if (paramList[3].Value != DBNull.Value && Convert.ToString(paramList[5].Value) != "E")
                {
                    itinerary.SupplierLocatorCode = paramList[3].Value.ToString();
                    response.PNR = itinerary.PNR;
                    response.Status = BookingResponseStatus.Successful;
                    response.ProdType = ProductType.Flight;
                }
                else {
                    response.Status = BookingResponseStatus.Failed;
                    response.Error = paramList[4].Value.ToString();
                    response.ProdType = ProductType.Flight;
                    Audit.Add(EventType.Book, Severity.High, 1, "(response.Error)Reason : " + response.Error, "");
                }
                
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(BookingResponse));
                    string filePath = fileSavingPath + "FlightInventoryBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "_" + sessionId + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Book, Severity.High, 1, "(FlightInventory)Failed to Save Booking Response. Reason : " + ex.ToString(), "");
                }

            }
            catch (Exception ex)
            {
                response.Status = BookingResponseStatus.Failed;
                response.Error = ex.Message.ToString();

                Audit.Add(EventType.Search, Severity.High, appUserId, "FlightInventory Booking Error:" + ex.ToString(), string.Empty);
                throw;
            }


            return response;
        }


        public TicketingResponse Ticket(FlightItinerary itinerary, ref Ticket[] tickets)
        {
            try
            {
                string filepath = fileSavingPath + "FlightInventoryTicketingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "_" + sessionId + ".xml";
                XmlDocument doc = CustomXmlSerializer.Serialize(itinerary, 1, "FlightItitnerary");
                doc.Save(filepath);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Ticketing, Severity.High, 1, "(FlightInventory)Failed to Save Ticketing Request. Reason : " + ex.ToString(), "");
            }


            TicketingResponse response = new TicketingResponse();
            try
            {
                List<Ticket> lstTicket = new List<Ticket>();
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    Ticket ticket = new Ticket();

                    ticket.ConjunctionNumber = string.Empty;
                    ticket.CorporateCode = string.Empty;
                    ticket.CreatedBy = itinerary.CreatedBy;
                    ticket.CreatedOn = DateTime.Now;
                    ticket.Endorsement = string.Empty;
                    ticket.ETicket = true;
                    ticket.FareCalculation = string.Empty;
                    ticket.FareRule = string.Empty;
                    ticket.FlightId = itinerary.FlightId;
                    ticket.IssueDate = DateTime.Now;
                    ticket.LastModifiedBy = itinerary.CreatedBy;
                    ticket.PaxFirstName = itinerary.Passenger[i].FirstName;
                    ticket.PaxId = itinerary.Passenger[i].PaxId;
                    ticket.PaxLastName = itinerary.Passenger[i].LastName;
                    ticket.PaxType = itinerary.Passenger[i].Type;
                    ticket.Price = itinerary.Passenger[i].Price;
                    ticket.Status = "Ticketed";
                    ticket.TicketAdvisory = string.Empty;
                    ticket.TicketDesignator = string.Empty;
                    ticket.TicketNumber = itinerary.PNR;
                    ticket.TicketType = string.Empty;
                    ticket.Title = itinerary.Passenger[i].Title;
                    ticket.ValidatingAriline = itinerary.ValidatingAirlineCode;
                    ticket.PtcDetail = new List<SegmentPTCDetail>();

                    lstTicket.Add(ticket);
                }
                tickets = lstTicket.ToArray();
                response.PNR = itinerary.PNR;
                response.Status = TicketingResponseStatus.Successful;

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(TicketingResponse));
                    string filePath = fileSavingPath + "FlightInventoryTicketingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "_" + sessionId + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Book, Severity.High, 1, "(FlightInventory)Failed to Save Ticketing Response. Reason : " + ex.ToString(), "");
                }
            }
            catch (Exception ex)
            {
                response.Status = TicketingResponseStatus.NotCreated;
                throw ex;
            }
            return response;
        }

        /// <summary>
        /// Fare Rule for FlightInventory
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="dest"></param>
        /// <param name="fareBasisCode"></param>
        /// <returns></returns>
        public static List<FareRule> GetFareRule(string origin, string dest)
        {
            List<FareRule> fareRuleList = new List<FareRule>();

            FareRule DNfareRules = new FareRule();
            DNfareRules.Origin = origin;
            DNfareRules.Destination = dest;
            DNfareRules.Airline = "FI";
            DNfareRules.FareRuleDetail = Util.GetLCCFareRules("FI");
            DNfareRules.FareBasisCode = string.Empty;

            fareRuleList.Add(DNfareRules);

            return fareRuleList;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~FlightInventory()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
