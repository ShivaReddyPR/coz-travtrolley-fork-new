﻿using System;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;
using CT.Core;

namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// Acts as a parent class for FlightInventorySource.
    /// </summary>
    public class FlightInventoryHeader
    {
        #region Variables

        private const long NEW_RECORD = -1;
        private string _origin;
        private string _destination;
        private string _searchType;
        private DateTime _departureDate;
        private DateTime _returnDate;
        private int _stops;
        private string _referenceNumber;
        private DateTime _bookingPeriodFrom;
        private DateTime _bookingPeriodTo;
        private bool _isLCC;
        private DataTable _dtInventoryDetails;
        private DataTable _dtFareDetails;
        private DataTable _dtInventoryQuota;
        private int _invID;
        private int _createdBy;
        private string _invStatus;


        #endregion

        #region Properties

        public int InvID
        {
            get { return _invID; }
            set { _invID = value; }
        }

        public DataTable DtInventoryDetails
        {
            get { return _dtInventoryDetails; }
            set { _dtInventoryDetails = value; }
        }

        public DataTable DtFareDetails
        {
            get { return _dtFareDetails; }
            set { _dtFareDetails = value; }
        }

        public DataTable DtInventoryQuota
        {
            get { return _dtInventoryQuota; }
            set { _dtInventoryQuota = value; }
        }

        public string Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }
        public string Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }
        public string SearchType
        {
            get { return _searchType; }
            set { _searchType = value; }
        }

        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set { _departureDate = value; }
        }

        public DateTime ReturnDate
        {
            get { return _returnDate; }
            set { _returnDate = value; }
        }

        public int Stops
        {
            get { return _stops; }
            set { _stops = value; }
        }
        public string ReferenceNumber
        {
            get { return _referenceNumber; }
            set { _referenceNumber = value; }
        }

        public DateTime BookingPeriodFrom
        {
            get { return _bookingPeriodFrom; }
            set { _bookingPeriodFrom = value; }
        }
        public DateTime BookingPeriodTo
        {
            get { return _bookingPeriodTo; }
            set { _bookingPeriodTo = value; }
        }

        public bool IsLCC
        {
            get { return _isLCC; }
            set { _isLCC = value; }
        }

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string InvStatus
        {
            get { return _invStatus; }
            set { _invStatus = value; }
        }

        #endregion

        #region Constructor Logic

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FlightInventoryHeader()
        {
            try
            {
                _invID = Convert.ToInt32(NEW_RECORD);
                GetDetails(NEW_RECORD); //Gets the Tables schema intially.
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public FlightInventoryHeader(long id)
        {
            try
            {
                GetDetails(id); //Gets the Tables schema intially.
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the details corresponding to CZ_Flight_Inventory  
        /// </summary>
        /// <param name="id"></param>
        public void GetDetails(long id)
        {
            try
            {
                DataSet ds = GetData(id);
                UpdateBusinessData(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Get the following tables schema CZ_Flight_Inventory_Details,CZ_Flight_Inventory_FareDetails,CZ_Flight_Inventory_Quota
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataSet GetData(long id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_Inv_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_FlightInventory_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Update the Properties in FlightInventoryHeader class.
        /// </summary>
        /// <param name="ds"></param>
        private void UpdateBusinessData(DataSet ds)
        {
            try
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) //CZ_Flight_Inventory Header Table
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (dr["SearchType"] != DBNull.Value)
                            {
                                _searchType = Convert.ToString(dr["SearchType"]);
                            }
                            if (dr["InvId"] != DBNull.Value)
                            {
                                _invID = Convert.ToInt32(dr["InvId"]);
                            }
                            if (dr["Origin"] != DBNull.Value)
                            {
                                _origin = Convert.ToString(dr["Origin"]);
                            }
                            if (dr["Destination"] != DBNull.Value)
                            {
                                _destination = Convert.ToString(dr["Destination"]);
                            }
                            if (dr["DepartureDate"] != DBNull.Value)
                            {
                                _departureDate = Convert.ToDateTime(dr["DepartureDate"]);
                            }
                            if (dr["ReturnDate"] != DBNull.Value)
                            {
                                _returnDate = Convert.ToDateTime(dr["ReturnDate"]);
                            }
                            if (dr["Stops"] != DBNull.Value)
                            {
                                _stops = Convert.ToInt32(dr["Stops"]);
                            }
                            if (dr["ReferenceNo"] != DBNull.Value)
                            {
                                _referenceNumber = Convert.ToString(dr["ReferenceNo"]);
                            }
                            if (dr["BookingPeriodFrom"] != DBNull.Value)
                            {
                                _bookingPeriodFrom = Convert.ToDateTime(dr["BookingPeriodFrom"]);
                            }
                            if (dr["BookingPeriodTo"] != DBNull.Value)
                            {
                                _bookingPeriodTo = Convert.ToDateTime(dr["BookingPeriodTo"]);
                            }
                            if (dr["isLCC"] != DBNull.Value)
                            {
                                _isLCC = Convert.ToBoolean(dr["isLCC"]);
                            }
                            if (dr["InvStatus"] != DBNull.Value)
                            {
                                _invStatus = Convert.ToString(dr["InvStatus"]);
                            }

                        }
                    }
                    _dtInventoryDetails = ds.Tables[1];//CZ_Flight_Inventory_Details
                    _dtFareDetails = ds.Tables[2];//CZ_Flight_Inventory_FareDetails
                    _dtInventoryQuota = ds.Tables[3];//CZ_Flight_Inventory_Quota

                  
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region SaveMethods

        /// <summary>
        /// Saves the record into CZ_Flight_Inventory,CZ_Flight_Inventory_Details,CZ_Flight_Inventory_FareDetails,CZ_Flight_Inventory_Quota
        /// </summary>
        public void Save()
        {
            SqlCommand cmd = null;
            SqlTransaction trans = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                //CZ_Flight_Inventory -- Header Table New Record insertion
                SqlParameter[] paramList = new SqlParameter[14];
                paramList[0] = new SqlParameter("@P_origin", _origin);
                paramList[1] = new SqlParameter("@P_destination", _destination);
                paramList[2] = new SqlParameter("@P_searchType", _searchType);
                paramList[3] = new SqlParameter("@P_departureDate", _departureDate);
                if (_returnDate == DateTime.MinValue)
                {
                    paramList[4] = new SqlParameter("@P_returnDate", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_returnDate", _returnDate);
                }
                paramList[5] = new SqlParameter("@P_stops", _stops);
                paramList[6] = new SqlParameter("@P_bookingPeriodFrom", _bookingPeriodFrom);
                paramList[7] = new SqlParameter("@P_bokkingPeriodTo", _bookingPeriodTo);
                paramList[8] = new SqlParameter("@P_isLCC", _isLCC);
                if (string.IsNullOrEmpty(_referenceNumber))
                {
                    paramList[9] = new SqlParameter("@P_referenceNumber", DBNull.Value);
                }
                else
                {
                    paramList[9] = new SqlParameter("@P_referenceNumber", _referenceNumber);
                }
                paramList[10] = new SqlParameter("@P_INV_ID_RET", SqlDbType.Int, 200);
                paramList[10].Direction = ParameterDirection.Output;
                paramList[11] = new SqlParameter("@P_invID", _invID);
                paramList[12] = new SqlParameter("@P_CreatedBy", _createdBy);
                paramList[13] = new SqlParameter("@P_InvStatus", _invStatus);

                DBGateway.ExecuteNonQueryDetails(cmd, "usp_SaveFlightInventory", paramList);

                if (paramList[10].Value != DBNull.Value) //PK -- InventoryId.
                {
                    //Inventory Details -- CZ_Flight_Inventory_Details
                    if (_dtInventoryDetails != null && _dtInventoryDetails.Rows.Count > 0)
                    {
                        foreach (DataRow dr in _dtInventoryDetails.Rows)
                        {
                            SaveFlightInventoryDetails(cmd, dr, Convert.ToInt32(paramList[10].Value));
                        }
                    }
                    //Inventory Fare Details -- CZ_Flight_Inventory_FareDetails
                    if (_dtFareDetails != null && _dtFareDetails.Rows.Count > 0)
                    {
                        foreach (DataRow dr in _dtFareDetails.Rows)
                        {
                            SaveFlightInventoryFareDetails(cmd, dr, Convert.ToInt32(paramList[10].Value));
                        }
                    }
                    //Inventory Quota -- CZ_Flight_Inventory_Quota
                    if (_dtInventoryQuota != null && _dtInventoryQuota.Rows.Count > 0)
                    {
                        foreach (DataRow dr in _dtInventoryQuota.Rows)
                        {
                            SaveFlightInventoryQuota(cmd, dr, Convert.ToInt32(paramList[10].Value));
                        }
                    }
                }
               
                trans.Commit();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(FlightInvHeader)Failed to insert record into tbl CZ_Flight_Inventory. Error:" + ex.ToString(), "");

            }
        }
        /// <summary>
        /// Save Flight Inventory Details into CZ_Flight_Inventory_Details for particular inventory id
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dr"></param>
        /// <param name="inventoryId"></param>
        public void SaveFlightInventoryDetails(SqlCommand cmd, DataRow dr, int inventoryId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[22];
                paramList[0] = new SqlParameter("@P_InvId", inventoryId);
                paramList[1] = new SqlParameter("@P_Origin", dr["Origin"]);
                paramList[2] = new SqlParameter("@P_Destination", dr["Destination"]);
                paramList[3] = new SqlParameter("@P_DepartureTime", dr["DepartureTime"]);
                paramList[4] = new SqlParameter("@P_ArrivalTime", dr["ArrivalTime"]);
                paramList[5] = new SqlParameter("@P_FlightNo", dr["FlightNo"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["TimeZone"])))
                {
                    paramList[6] = new SqlParameter("@P_TimeZone", dr["TimeZone"]);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_TimeZone", DBNull.Value);
                } 
              
                paramList[7] = new SqlParameter("@P_CabinClass", dr["CabinClass"]);

                if (!string.IsNullOrEmpty(Convert.ToString(dr["FlightStatus"])))
                {
                    paramList[8] = new SqlParameter("@P_FlightStatus", dr["FlightStatus"]);
                }
                else
                {
                    paramList[8] = new SqlParameter("@P_FlightStatus",DBNull.Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dr["Terminal"])))
                {
                    paramList[9] = new SqlParameter("@P_Terminal", dr["Terminal"]);
                }
                else
                {
                    paramList[9] = new SqlParameter("@P_Terminal", DBNull.Value);
                }  
            
                paramList[10] = new SqlParameter("@P_Duration", dr["Duration"]);

                if (!string.IsNullOrEmpty(Convert.ToString(dr["GroundTime"])))
                {
                    paramList[11] = new SqlParameter("@P_GroundTime", dr["GroundTime"]);
                }
                else
                {
                    paramList[11] = new SqlParameter("@P_GroundTime", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dr["StopOver"])))
                {
                    paramList[12] = new SqlParameter("@P_StopOver", dr["StopOver"]);
                }
                else
                {
                    paramList[12] = new SqlParameter("@P_StopOver",DBNull.Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dr["Craft"])))
                {
                    paramList[13] = new SqlParameter("@P_Craft", dr["Craft"]);
                }
                else
                {
                    paramList[13] = new SqlParameter("@P_Craft", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dr["AirlinePNR"])))
                {
                    paramList[14] = new SqlParameter("@P_AirlinePNR", dr["AirlinePNR"]);
                }
                else
                {
                    paramList[14] = new SqlParameter("@P_AirlinePNR",DBNull.Value);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(dr["OperatingCarrier"])))
                {
                    paramList[15] = new SqlParameter("@P_OperatingCarrier", dr["OperatingCarrier"]);
                }
                else
                {
                    paramList[15] = new SqlParameter("@P_OperatingCarrier",DBNull.Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dr["LastTicketedDate"])))
                {
                    paramList[16] = new SqlParameter("@P_LastTicketedDate", dr["LastTicketedDate"]);
                }
                else
                {
                    paramList[16] = new SqlParameter("@P_LastTicketedDate", DBNull.Value);
                }

                paramList[17] = new SqlParameter("@P_AirlineCode", dr["AirlineCode"]);

                if (!string.IsNullOrEmpty(Convert.ToString(dr["Group"])))
                {
                    paramList[18] = new SqlParameter("@P_Group", dr["Group"]);
                }
                else
                {
                    paramList[18] = new SqlParameter("@P_Group",DBNull.Value);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(dr["BookingClass"])))
                {
                    paramList[19] = new SqlParameter("@P_BookingClass", dr["BookingClass"]);
                }
                else
                {
                    paramList[19] = new SqlParameter("@P_BookingClass",DBNull.Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dr["DetailId"])))
                {
                    paramList[20] = new SqlParameter("@P_DetailID", Convert.ToInt32 (dr["DetailId"]));
                }
                else
                {
                    paramList[20] = new SqlParameter("@P_DetailID", -1);
                }
                paramList[21] = new SqlParameter("@P_CreatedBy", CreatedBy);
                
                DBGateway.ExecuteNonQueryDetails(cmd, "usp_Save_FlightInventoryDetails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(FlightInvHeader)Failed to insert record into tbl CZ_Flight_Inventory_Details. Error:" + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Save Flight Inventory Fare Details into CZ_Flight_Inventory_FareDetails for particular inventory id
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dr"></param>
        /// <param name="inventoryId"></param>
        public void SaveFlightInventoryFareDetails(SqlCommand cmd, DataRow dr, int inventoryId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[13];
                paramList[0] = new SqlParameter("@P_InvId", inventoryId);
                paramList[1] = new SqlParameter("@P_PaxType", dr["PaxType"]);
                paramList[2] = new SqlParameter("@P_BaseFare", dr["BaseFare"]);
                paramList[3] = new SqlParameter("@P_Tax", dr["Tax"]);
                paramList[4] = new SqlParameter("@P_PublishedFare", dr["PublishedFare"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["OtherCharges"])))
                {
                    paramList[5] = new SqlParameter("@P_OtherCharges", dr["OtherCharges"]);
                }
                else
                {
                    paramList[5] = new SqlParameter("@P_OtherCharges",DBNull.Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dr["ServiceTax"])))
                {
                    paramList[6] = new SqlParameter("@P_ServiceTax", dr["ServiceTax"]);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_ServiceTax",DBNull.Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dr["NetFare"])))
                {
                    paramList[7] = new SqlParameter("@P_NetFare", dr["NetFare"]);
                }
                else
                {
                    paramList[7] = new SqlParameter("@P_NetFare",DBNull.Value);
                }
               
                paramList[8] = new SqlParameter("@P_Currency", dr["Currency"]);
                paramList[9] = new SqlParameter("@P_PriceType", dr["PriceType"]);
                paramList[10] = new SqlParameter("@P_BaggageIncluded",false);//Need to discuss this.
                if (!string.IsNullOrEmpty(Convert.ToString(dr["FareId"])))
                {
                    paramList[11] = new SqlParameter("@P_FareId", Convert.ToInt32(dr["FareId"]));
                }
                else
                {
                    paramList[11] = new SqlParameter("@P_FareId", -1);
                }
                paramList[12] = new SqlParameter("@P_CreatedBy", CreatedBy);

                DBGateway.ExecuteNonQueryDetails(cmd, "usp_Save_FlightInventory_FareDetails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(FlightInvMaster)Failed to insert record into tbl CZ_Flight_Inventory_FareDetails. Error:" + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Save Flight Inventory Quota into CZ_Flight_Inventory_Quota for particular inventory id
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dr"></param>
        /// <param name="inventoryId"></param>

        public void SaveFlightInventoryQuota(SqlCommand cmd, DataRow dr, int inventoryId)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_InvId", inventoryId);
                paramList[1] = new SqlParameter("@P_PNR", dr["PNR"]);
                paramList[2] = new SqlParameter("@P_AvailQuota", dr["AvailQuota"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["UsedQuota"])))
                {
                    paramList[3] = new SqlParameter("@P_UsedQuota", dr["UsedQuota"]);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_UsedQuota",DBNull.Value);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(dr["QuotaId"])))
                {
                    paramList[4] = new SqlParameter("@P_QuotaId",Convert.ToInt32(dr["QuotaId"]));
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_QuotaId" ,-1);
                }
                paramList[5] = new SqlParameter("@P_CreatedBy", CreatedBy);

                DBGateway.ExecuteNonQueryDetails(cmd, "usp_Save_FlightInventory_FareQuota", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(FlightInvHeader)Failed to insert record into tbl CZ_Flight_Inventory_Quota. Error:" + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Get the Flight Inventory Details from CZ_Flight_Inventory,CZ_Flight_Inventory_Details,CZ_Flight_Inventory_FareDetails,CZ_Flight_Inventory_Quota
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static DataTable GetFlightInventoryDetails(string status)
        {
            DataTable dtMandateSources = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];            
                paramList[0] = new SqlParameter("@P_Status", status);
                dtMandateSources = DBGateway.FillDataTableSP("usp_GetFlightInventoryDetails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(FlightInvHeader)Failed to read Flight Inventory. Error:" + ex.ToString(), "");
            }
            return dtMandateSources;
        }

        /// <summary>
        /// Upadte the row status in the tbl CZ_Flight_Inventory against the InvId
        /// </summary>
        /// <param name="invId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="invStatus"></param>
        public static void UpdateFlightInventoryRowStatus(int invId, int modifiedBy, string invStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_INV_ID", invId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_INV_STATUS", invStatus);
                DBGateway.ExecuteNonQuery("usp_Update_Flight_Inventory_Row_Status", paramList);
            }
            catch { throw; }
        }
        #endregion
    }
}
