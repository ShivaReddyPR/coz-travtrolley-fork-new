using System;
using System.Collections.Generic;
using System.Text;

namespace CT.MemCache
{
    public static class MemCacheKeys
    {
        public const string PagingConfig = "mck_PagingConfig";
        public const string BookingEngineConfig = "mck_BookingEngineConfig";
        public const string WorldspanConfig = "mck_WorldspanConfig";
        public const string AmadeusConfig = "mck_AmadeusConfig";        
        public const string CoreConfig = "mck_CoreConfig";
        public const string MobileConfig = "mck_MobileConfig";
        public const string MailingTimeConfig = "mck_MailingTimeConfig";
        public const string ResultCountBySourcesConfig = "mck_ResultCountBySourcesConfig";
        public const string ActiveSourcesConfig = "mck_ActiveSourcesConfig";        
        public const string CMSImageConfig = "mck_CMSImageConfig";
        public const string SpiceJetConfig = "mck_SpiceJetConfig";
        public const string AuditConfig = "mck_AuditConfig";
        public const string NavitaireConfig = "mck_NavitaireConfig";
        public const string TestModeConfig = "mck_TestModeConfig";
        public const string RobotIdConfig = "mck_RobotIdConfig";
        public const string EmailConfig = "mck_EmailConfig";
        public const string ApiSourcesConfig = "mck_ApiSourcesConfig";
        public const string IndigoConfig = "mck_IndigoConfig";
        public const string ParamountConfig = "mck_ParamountConfig";        
        public const string AirDeccanConfig = "mck_AirDeccanConfig";
        public const string MdlrConfig = "mck_MdlrConfigConfig";
        public const string WLAdConfig = "mck_WLAdConfig";
        public const string DesiyaConfig = "mck_DesiyaConfig";
        public const string DesiyaCreditConfig = "mck_DesiyaCreditConfig";
        public const string GTAConfig = "mck_GTAConfig";
        public const string MikiConfig = "mck_MikiConfig";
        public const string TravcoConfig = "mck_TravcoConfig";
        public const string AllowedTitlesConfig = "mck_AllowedTitlesConfig";
        public const string GoAirConfig = "mck_GoAirConfig";
        public const string FaqConfig = "mck_FaqConfig";
        public const string HotelFaqConfig = "mck_HotelFaqConfig";
        public const string TBOHotelConfig = "mck_TBOHotelConfig";
        public const string PromotionConfig = "mck_PromotionConfig";
        public const string HotelAutoCancelConfig = "mck_HotelAutoCancelConfig";
        public const string ICICIConfig = "mck_ICICIConfig";
        public const string HotelBedsConfig = "mck_HotelBedsConfig";
        public const string GalileoConfig = "mck_GalileoConfig";
        public const string UAPIConfig = "mck_UAPIConfig";
        public const string TouricoConfig = "mck_TouricoConfig";
        public const string SamaConfig = "mck_SamaConfig";
        public const string SamaCityConfig = "mck_SamaCityConfig";
        public const string SamaTimeZoneConfig = "mck_SamaTimeZoneConfig";
        public const string IRCTCConfig = "mck_IRCTCConfig";
        public const string IANConfig = "mck_IANConfig";
        //public const string IndiaTimesConfig = "mck_IndiaTimesConfig";
        public const string AirlineCashBackConfig = "mck_AirlineCashBackConfig";
        public const string NonCommisionableTaxConfig = "mck_NonCommisionableTaxConfig";
        public const string StatusListConfig = "mck_StatusListConfig";
        public const string HermesConfig = "mck_HermesConfig";
        public const string SectorListConfig = "mck_SectorListConfig";
        public const string DefaultCategoryConfig = "mck_DefaultCategoryConfig";
        public const string ReportingServicesConfig = "mck_ReportingServicesConfig";
        public const string SmsConfig = "mck_SmsConfig";
        public const string statusListConfig = "mck_statusListConfig";
        public const string HotelConnectConfig = "mck_HotelConnectConfig";
        public const string CreditCardConfig = "mck_CreditCardConfig";
        public const string ITRedConfig = "mck_ITRedConfig";
        public const string FlyDubaiConfig = "mck_FlyDubaiConfig";
        public const string RoleTaskList = "mck_RoleTaskList";
        public const string MailReaderIdConfig = "mck_MailReaderIdConfig";
        
        public const string HolidayFaqConfig = "mck_HolidayFaqConfig";
        public const string HolidayDealConfig = "mck_HolidayDealConfig";
        public const string VisaConfig = "mck_VisaConfig";
        public const string IffcoInsuranceConfig = "mck_IffcoInsuranceConfig";
        public const string AirlineProfile = "mck_AirlineProfile";
        public const string AirArabia = "mck_AirArabia";
        public const string DOTWConfig = "mck_DOTWConfig";
        public const string WSTConfig = "mck_WSTConfig";
        public const string PayWorldConfig = "mck_PayWorldConfig";
        public const string LocaleConfig = "mck_LocaleConfig";
        public const string SwitchConfig = "mck_SwitchConfig";
        public const string Zeus = "mck_Zeus";
        public const string RezLiveConfig = "mck_RezLiveConfig";
        public const string LOHConfig = "mck_LOHConfig";
        //public const string TBOHotelConfig = "mck_TBOHotelConfig";
        public const string B2CSettingsConfig = "mck_B2CSettingsConfig";
        public const string TBOAirConfig = "mck_TBOAirConfig";
        public const string JACConfig = "mck_JACConfig";
        public const string EETConfig = "mck_EETConfig";
        public const string SayaraConfig = "mck_SAYARAConfig";
        public const string AirIndiaExpressConfig = "mck_AirIndiaExpressConfig";
        public const string FlightInvConfig = "mck_FlightInvConfig";
        //Add by somasekhar on 24/08/2018 for Yatra
        public const string YatraConfig = "mck_YatraConfig";
        public const string SalamAirConfig = "mck_SalamAirConfig";
        //Added by lokesh for Jazeera
        public const string JazeeraConfig = "mck_JazeeraConfig";
//Add by somasekhar on 04/12/2018 for OYO 
        public const string OYOConfig = "mck_OYOConfig";
        //Add by Harish on 01/04/2019 for Gimmonix 
        public const string GIMMONIXConfig = "mck_GIMMONIXConfig";
        //Add by Praveen on 07/09/2019 for UAPI Hotel
        public const string UAPIHotelConfig = "mck_UAPIHotelConfig";
        //Add by somasekhar on 09/2019 for CZA Activity 
        public const string ActivityServiceConfig = "mck_ActivityServiceConfig";


        public const string BabylonConfig = "mck_BabylonConfig";//FOR BABYLON
        public const string TalixoConfig = "mck_TalixoConfig";//FOR Talixo
                                                              //Add by Krishna Phani on 14/04/2020 for Illusion 
        public const string IllusionsHotelConfig = "mck_ILLUSIONSConfig";
        public const string HotelExtranetConfig = "mck_HotelExtranetConfig";//For HotelExtranet

    }
}
