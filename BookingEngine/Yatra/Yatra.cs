﻿using CT.Configuration;
using CT.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

namespace CT.BookingEngine.GDS
{
    public class Yatra : IDisposable
    {
        #region Members
        /// <summary>
        /// To get Login userId
        /// </summary>
        private long appUserId;
        private string sessionId;
        /// <summary>
        /// To Store logs file path of Request and Response of whole Booking Process(Search to Cancellation)
        /// </summary>
        private string XmlPath = string.Empty;
        /// <summary>
        /// This Id is given by Supplier(Yatra) ,configured in Yatra.Config.xml and Loaded from ConfigurationSystem.cs YatraConfig 
        /// </summary>
        private string propertyID = string.Empty;
        /// <summary>
        /// This is given by Supplier(Yatra) ,configured in Yatra.Config.xml and Loaded from ConfigurationSystem.cs YatraConfig 
        /// </summary>
        private string userName = string.Empty;
        /// This is given by Supplier(Yatra) ,configured in Yatra.Config.xml and Loaded from ConfigurationSystem.cs YatraConfig 
        /// </summary>
        private string password = string.Empty;
        /// <summary>
        /// language which is accepted by Supplier(Yatra) , by default(en). Configured from Yatra.Config.xml and Loaded from ConfigurationSystem.cs YatraConfig 
        /// </summary>
        private string language = string.Empty;
        /// <summary>
        /// currency which is accepted by Supplier(Yatra - INR) ,configured in Yatra.Config.xml and Loaded from ConfigurationSystem.cs YatraConfig 
        /// </summary>
        private string currency = string.Empty;
        decimal rateOfExchange = 1;
        string agentCurrency = "";
        Dictionary<string, decimal> exchangeRates;
        int decimalPoint;
        string sourceCountryCode;
        /// <summary>
        /// City/Hotel Details Search end point URL given by Supplier(Yatra) ,configured in Yatra.Config.xml and Loaded from ConfigurationSystem.cs YatraConfig 
        /// </summary>
        private string searchAvailReqURL = string.Empty;
        /// <summary>
        /// Provisional/Final Booking end point URL given by Supplier(Yatra) ,configured in Yatra.Config.xml and Loaded from ConfigurationSystem.cs YatraConfig 
        /// </summary>
        private string bookingReqURL = string.Empty;
        /// <summary>
        /// Initiate/Confirm cancellation end point URL given by Supplier(Yatra) ,configured in Yatra.Config.xml and Loaded from ConfigurationSystem.cs YatraConfig 
        /// </summary>
        private string cancellationReqURL = string.Empty;
        #endregion

        #region Constructors
        /// <summary>
        ///  Constructor ,here loading App config details.
        /// </summary>
        /// <param name="sessionId"></param>
        public Yatra()
        {
            LoadCredentials();
        }
        /// <summary>
        /// Parmeterized Constructor here only loading first time sessionId
        /// </summary>
        /// <param name="sessionId"></param>
        public Yatra(string sessionId) : this()
        {
            this.sessionId = sessionId;
        }
        private void LoadCredentials()
        {
            //Create Hotel Xml Log path folder per day wise
            XmlPath = ConfigurationSystem.YatraConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            propertyID = ConfigurationSystem.YatraConfig["PropertyID"];
            userName = ConfigurationSystem.YatraConfig["Username"];
            password = ConfigurationSystem.YatraConfig["Password"];
            language = ConfigurationSystem.YatraConfig["lang"];
            currency = ConfigurationSystem.YatraConfig["currency"];
            searchAvailReqURL = ConfigurationSystem.YatraConfig["SearchAvailReqURL"];
            bookingReqURL = ConfigurationSystem.YatraConfig["BookingReqURL"];
            cancellationReqURL = ConfigurationSystem.YatraConfig["CancellationReqURL"];
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }
        #endregion

        #region Properties
        public long AppUserId
        {
            get
            {
                return appUserId;
            }

            set
            {
                appUserId = value;
            }
        }
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string SessionId
        {
            get
            {
                return sessionId;
            }

            set
            {
                sessionId = value;
            }
        }
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        #endregion

        #region Common Methods 
        /// <summary>
        /// This is common method ,ErrorCode corresponding message provided by Yatra
        /// </summary>
        /// <param name="code">Error Code Provided by Supplier(Yatra)</param>
        /// <returns>error messge string</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private string ErrorMessage(string Code)
        {
            string errorMessage = "No Hotels Found";
            switch (Code)
            {
                case "001":
                    errorMessage = "SearchHotelId_Is_Null";
                    break;
                case "002":
                    errorMessage = "Search_Hotel_City_Name_Is_Not_Available";
                    break;
                case "010":
                    errorMessage = "No_Hotels_Found";
                    break;
                case "011":
                    errorMessage = "Search_City_Or_Country_Is_Not_Available";
                    break;
                case "012":
                    errorMessage = "Invalid_Requested_Currency_Code";
                    break;
                case "013":
                    errorMessage = "HotelSeoId_Is_Not_Available";
                    break;
                case "014":
                    errorMessage = "Pagination_From_Is_Not_Natural_Number";
                    break;
                case "015":
                    errorMessage = "Pagination_To_Is_Not_Natural_Number";
                    break;
                case "016":
                    errorMessage = "Pagination_To_Is_Not_Greater_Than_Or_Equal_To_From";
                    break;
                case "017":
                    errorMessage = "Pagination_To_Is_More_Than_200_Greater_Than_From";
                    break;
                case "018":
                    errorMessage = "NO_OF_ROOMS_SELECTED_ARE_NOT_VALID";
                    break;
                case "019":
                    errorMessage = "No_Of_Adults_Entered_Is_Not_Valid";
                    break;
                case "020":
                    errorMessage = "No_Of_Children_Entered_Is_Not_Valid";
                    break;
                case "021":
                    errorMessage = "NUMBER_OF_CHILDREN_AGES_ENTERED_IS_NOT_VALID_OR_DOES_NOT_MATCH_WITH_CHILDREN_COUNT_PROVIDED";
                    break;
                case "022":
                    errorMessage = "ATLEAST_ONE_OF_THE_CHILD_AGES_IS_OUT_OF_VALID_RANGE";
                    break;
                case "023":
                    errorMessage = "INVALID_DATE_FORMAT";
                    break;
                case "024":
                    errorMessage = "ONE_OF_THE_DATES_IS_NOT_IN_FUTURE";
                    break;
                case "025":
                    errorMessage = "CHECK_IN_DATE_NOT_WITHIN_MAX";
                    break;
                case "026":
                    errorMessage = "CHECK_IN_DATE_IS_NOT_BEFORE_CHECK_OUT_DATE";
                    break;
                case "027":
                    errorMessage = "STAY_DURATION_IS_ABOVE_PERMITTED_MAX";
                    break;
                case "029":
                    errorMessage = "UNABLE_TO_PARSE_THE_RESPONSE_NOW";
                    break;
                case "030":
                    errorMessage = "UNEXPECTED_EXCEPTION";
                    break;
                case "031":
                    errorMessage = "WebService_Authentication_requirements_Are_Not_Allowed_To_Empty";
                    break;
                case "032":
                    errorMessage = "ATLEAST_ONE_OF_ROOM_INFO_IS_NOT_VALID";
                    break;
                case "033":
                    errorMessage = "EMAIL_INVALID";
                    break;
                case "034":
                    errorMessage = "PHONE_CODE_OR_NUMBER_INVALID";
                    break;
                case "035":
                    errorMessage = "Pay_At_Hotel_Not_Supported";
                    break;
                case "036":
                    errorMessage = "Hotel_Do_Not_Support_Pay_At_Hotel_Model";
                    break;
                case "037":
                    errorMessage = "Insufficient_Booking_Window_Period";
                    break;
                case "038":
                    errorMessage = "Could_Not_Process_The_Booking";
                    break;
                case "039":
                    errorMessage = "Invalid_Provisional_Booking_Id";
                    break;
                case "040":
                    errorMessage = "BOOKING_FAILURE_EXCEPTION";
                    break;
                case "041":
                    errorMessage = "CANCELLATION_FAILURE_EXCEPTION";
                    break;
                case "042":
                    errorMessage = "No_BookingId_for_Cancellation";
                    break;
                case "043":
                    errorMessage = "No_Email_for_Cancellation";
                    break;
                case "044":
                    errorMessage = "No_LastName_for_Cancellation";
                    break;
                case "045":
                    errorMessage = "Invalid_BookingId_for_Cancellation";
                    break;
                case "046":
                    errorMessage = "Invalid_Email_for_Cancellation";
                    break;
                case "047":
                    errorMessage = "Invalid_LastName_for_Cancellation";
                    break;
                case "048":
                    errorMessage = "DATA_ACCESS_EXCEPTION_FOR_CANCELLATION_VALIDATION";
                    break;
                case "049":
                    errorMessage = "No_Dates_for_Cancellation";
                    break;
                case "050":
                    errorMessage = "Invalid_Dates_for_Cancellation";
                    break;
                case "051":
                    errorMessage = "No_Dates_for_provided_BookingId";
                    break;
                case "052":
                    errorMessage = "No_CancellationPolicy_Found";
                    break;
                case "053":
                    errorMessage = "Cancellation_Dates_Doesnt_Have_CheckinDate_or_CheckoutDate";
                    break;
                case "054":
                    errorMessage = "Cancellation_Dates_Not_In_Sequence";
                    break;
                case "055":
                    errorMessage = "Search_Destination_Is_Not_Available";
                    break;
                case "056":
                    errorMessage = "CHECK_IN_DATE_IS_BEFORE_CURRENT_DA";
                    break;
                case "057":
                    errorMessage = "Search_POI_Id_Not_Available";
                    break;
                case "058":
                    errorMessage = "Search_POI_LatLong_OR_City_Not_Available";
                    break;
                case "059":
                    errorMessage = "Search_Multi_Vendor_Id_Not_Available";
                    break;
                case "060":
                    errorMessage = "No_Of_Vendor_Id_Is_Above_Permitted_Max";
                    break;
                case "061":
                    errorMessage = "Country_Code_Is_Not_As_Per_ISOCountry_Code";
                    break;
                case "062":
                    errorMessage = "Search_Chain_Id_Not_Available";
                    break;
                case "063":
                    errorMessage = "Search_City_Theme_Code_Not_Available";
                    break;
                case "064":
                    errorMessage = "Search_City_Category_Code_Not_Available";
                    break;
                case "065":
                    errorMessage = "state_seo_id_is_null";
                    break;
                case "066":
                    errorMessage = "country_seo_id_is_null";
                    break;
                case "067":
                    errorMessage = "Search_City_Theme_Category_Code_Not_Available";
                    break;
                case "068":
                    errorMessage = "Credit_Card_Validation_Failed";
                    break;
                case "069":
                    errorMessage = "ITS_NOT_A_HTTPS_REQUEST";
                    break;
                case "070":
                    errorMessage = "CREDITCARD_NUMBER_EMPTY";
                    break;
                case "071":
                    errorMessage = "CREDITCARD_NAME_EMPTY";
                    break;
                case "072":
                    errorMessage = "CREDITCARD_EXP_EMPTY";
                    break;
                case "073":
                    errorMessage = "CREDITCARD_COUNTRY_EMPTY";
                    break;
                case "074":
                    errorMessage = "INVALID_DISCOUNT_COUPON";
                    break;
                case "075":
                    errorMessage = "DISCOUNT_COUPON_NOT_APPLICABLE";
                    break;
                case "077":
                    errorMessage = "Dates_Not_Falling_Between_Block_Out_Dates_In_Pay_At_Hotel_Model";
                    break;
                case "078":
                    errorMessage = "Search_Amenity_OR_Destination_Not_Available";
                    break;
                case "079":
                    errorMessage = "Search_Amenity_Not_Available";
                    break;
                case "081":
                    errorMessage = "ROOM_NOT_AVAILABLE";
                    break;
                case "083":
                    errorMessage = "ROOM_RATE_CHANGED";
                    break;
                case "084":
                    errorMessage = "HOTEL_NOT_AVAILABLE";
                    break;
                case "085":
                    errorMessage = "PROVISIONAL_BOOKING_EXPIRED";
                    break;
                case "086":
                    errorMessage = "BOOKING_ALREADY_CONFIRMED";
                    break;
                case "087":
                    errorMessage = "INVALID_PAYATHOTEL_VALUE";
                    break;
                case "088":
                    errorMessage = "BOOKING_MAY_BE_CONFIRMED";
                    break;
                case "097":
                    errorMessage = "RATE_LIMITING_EXCEPTION";
                    break;

            }

            return errorMessage;
        }
        /// <summary>
        /// Send the XML format request to the server and pulls the XML format response .
        /// </summary>
        /// <param name="requeststring">Xml formatted string</param>
        /// <param name="url">CitySearch/HotelDetail/ProvisionalBooking/FinalBooking/InitiateCancellation/ConfirmCancellation end point Url</param>
        /// <param name="reqType">Type of request(City Search/Hotel Details/Provisional Booking/Final Booking/Initiate Cancellation/Confirm Cancellation)</param>
        /// <returns>XmlDocument</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private XmlDocument SendRequest(string requeststring, string url, string reqType)
        {
            XmlDocument xmlDoc = new XmlDocument();
            HttpWebResponse objResponse = null;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "POST";
                req.ContentLength = requeststring.Length;
                req.ContentType = "text/xml;charset=\"utf-8\"";
                req.Accept = "text/xml";
                req.KeepAlive = true;
                try
                {
                    Stream str = req.GetRequestStream();
                    StreamWriter writer = new StreamWriter(str);
                    writer.Write(requeststring);
                    writer.Flush();
                    Stream resp = Stream.Null;
                    objResponse = (HttpWebResponse)req.GetResponse();
                }
                catch (WebException webEx)
                {
                    throw new Exception("Yatra-Failed to get response from " + reqType + " request : " + webEx.Message, webEx);
                }
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    xmlDoc.Load(sr);

                }

            }
            catch (Exception ex)
            {
                throw new Exception("Yatra-Failed to get response from " + reqType + " request : " + ex.Message, ex);
            }
            return xmlDoc;
        }
        #endregion

        #region Get Hotel Results
        /// <summary>
        /// Returns Hotel Results for the Search Criteria.
        /// </summary>
        /// <param name="req">Hotel Request object</param>
        /// <param name="markup">B2B Markup Type comes from table  against agent - B2B markup</param>
        /// <param name="markupType">B2B Markup Type comes from table against agent - F: Fixed, P: Percentage</param>
        /// <returns>HotelSearchResult[]</returns>
        public HotelSearchResult[] GetHotelAvailability(HotelRequest req, decimal markup, string markupType)
        {
            HotelSearchResult[] searchRes = new HotelSearchResult[0];
            HotelSearchResult[] result;
            #region Request string
            // Prapare XML format string request  
            string request = GenerateHotelAvailabilityRequest(req, string.Empty);
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(request);
                string filePath = XmlPath + appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CitySearchRequest.xml";
                XmlDoc.Save(filePath);
            }
            catch { }
            #endregion
            #region Send Reqest for Get Hotels
            XmlDocument xmlResp = new XmlDocument();
            try
            {   //xmlResp.Load(@"D:\sekhar\YATRA\Document\city search response1.xml");
		// xmlResp.Load(@"C:\Users\Admin\Desktop\yatra\5992_3c54b561-a516-4b90-aae1-3db5e7aafb50_23102019_112220_CitySearchResponse.xml");
		//Sending request to Yatra end point url for City Search
                xmlResp = SendRequest(request, searchAvailReqURL, "City Search");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                        "Exception returned from Yatra.GetHotelAvailability Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                }), "");

                throw new BookingEngineException("Error: " + ex.Message);
            }
            #endregion
            #region Generate Serch Result
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = GenerateSearchResult(xmlResp, req, markup, markupType);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                        "Exception returned from Yatra.GetHotelAvailability Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                }), "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
            {
                    " Response from Yatra for Search Request:| ",
                    DateTime.Now,
                    "| request XML:",
                    request,
                    "|response XML:",
                    xmlResp.OuterXml
            }), "");
            result = searchRes;
            #endregion
            return result;
        }
        /// <summary>
        /// Generates Hotel Search Request XML for City Search and Hotel Search.
        /// </summary>
        /// <param name="request">Hotel request</param>
        /// <param name="hotelCode">Hotel Code</param>
        /// <returns>Xml string</returns>
        private string GenerateHotelAvailabilityRequest(HotelRequest request, string hotelCode)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_HotelAvailRQ", "http://www.opentravel.org/OTA/2003/05"); //OTA_HotelAvailRQ start
                writer.WriteAttributeString("RequestedCurrency", "INR");
                writer.WriteAttributeString("SortOrder", "PRICE");
                writer.WriteAttributeString("Version", "0.0");
                writer.WriteAttributeString("PrimaryLangID", "en");
                writer.WriteAttributeString("SearchCacheLevel", "Live");
                writer.WriteStartElement("AvailRequestSegments"); //AvailRequestSegments start
                writer.WriteStartElement("AvailRequestSegment"); //AvailRequestSegment start
                writer.WriteStartElement("HotelSearchCriteria"); //HotelSearchCriteria start
                writer.WriteStartElement("Criterion"); //Criterion start 

                if (string.IsNullOrEmpty(hotelCode)) // This node not required , If search is based on HotelCode 
                {
                    writer.WriteStartElement("Address"); //Address start                                                         
                    writer.WriteElementString("CityName",request.CityName);//CityName start   //CityName                
                    writer.WriteStartElement("CountryName"); //CountryName start   
                    writer.WriteAttributeString("Code", request.CountryName);
                    writer.WriteEndElement();//CountryName
                    writer.WriteEndElement();//Address
                }
                writer.WriteStartElement("HotelRef"); //HotelRef start
                if (!string.IsNullOrEmpty(hotelCode))// This attribute not required , If search is based on HotelCode 
                {
                    writer.WriteAttributeString("HotelCode", hotelCode);
                }
                writer.WriteEndElement();//HotelRef

                writer.WriteStartElement("StayDateRange"); //StayDateRange start   
                writer.WriteAttributeString("End", request.EndDate.ToString("yyyy-MM-dd"));
                writer.WriteAttributeString("Start", request.StartDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();//StayDateRange

                writer.WriteStartElement("RoomStayCandidates"); //RoomStayCandidates start   
                //For Multiple Rooms
                for (int i = 0; i < request.NoOfRooms; i++)
                {
                    writer.WriteStartElement("RoomStayCandidate"); //RoomStayCandidate start   
                    writer.WriteStartElement("GuestCounts"); //GuestCounts start  
                    //-- For Adults
                    writer.WriteStartElement("GuestCount"); //GuestCount start 
                    if (!string.IsNullOrEmpty(hotelCode))// This attribute not required , If search is based on HotelCode 
                    {
                        writer.WriteAttributeString("ResGuestRPH", i.ToString());
                    }
                    writer.WriteAttributeString("AgeQualifyingCode", "10");
                    writer.WriteAttributeString("Count", request.RoomGuest[i].noOfAdults.ToString());
                    writer.WriteEndElement();//GuestCount
                    for (int j = 0; j < request.RoomGuest[i].noOfChild; j++)
                    {
                        //-- For Childs
                        int childAge = request.RoomGuest[i].childAge[j];
                        if (childAge > 2)
                        {

                            writer.WriteStartElement("GuestCount"); //GuestCount start   
                            if (!string.IsNullOrEmpty(hotelCode))// This attribute not required , If search is based on HotelCode 
                            {
                                writer.WriteAttributeString("ResGuestRPH", i.ToString());
                            }
                            writer.WriteAttributeString("Age", childAge.ToString());
                            writer.WriteAttributeString("AgeQualifyingCode", "8");
                            writer.WriteEndElement();//GuestCount
                        }
                    }
                    writer.WriteEndElement();//GuestCounts
                    writer.WriteEndElement();//RoomStayCandidate
                }

                writer.WriteEndElement();//RoomStayCandidates

                if (string.IsNullOrEmpty(hotelCode)) // This node not required , If search is based on HotelCode 
                {
                    writer.WriteStartElement("Award"); //Award start   
                    writer.WriteAttributeString("Rating", request.Rating.ToString());
                    writer.WriteEndElement();//Award
                }
                writer.WriteStartElement("TPA_Extensions"); //TPA_Extensions start  
                writer.WriteStartElement("Pagination"); //Pagination start  
                if (string.IsNullOrEmpty(hotelCode)) // This attribute not required , If search is based on HotelCode 
                {
                    writer.WriteAttributeString("enabled", "true");
                }
                if (!string.IsNullOrEmpty(hotelCode)) // This attribute not required , If search is based on HotelCode 
                {
                    writer.WriteAttributeString("hotelsFrom", "0");
                    writer.WriteAttributeString("hotelsTo", "0");
                }
                writer.WriteEndElement();//Pagination
                if (string.IsNullOrEmpty(hotelCode)) // This node not required , If search is based on HotelCode 
                {
                    writer.WriteStartElement("HotelBasicInformation"); //HotelBasicInformation start   
                    writer.WriteStartElement("Reviews"); //Reviews  start   
                    writer.WriteEndElement();//Reviews 
                    writer.WriteEndElement();//HotelBasicInformation
                }
                writer.WriteStartElement("UserAuthentication"); //UserAuthentication start   
                writer.WriteAttributeString("password", password);
                writer.WriteAttributeString("propertyId", propertyID);
                writer.WriteAttributeString("username", userName);
                writer.WriteEndElement();//UserAuthentication
                if (string.IsNullOrEmpty(hotelCode)) // This node not required , If search is based on HotelCode 
                {
                    writer.WriteStartElement("Promotion"); //Promotion start   
                    writer.WriteAttributeString("Type", "BOTH");
                    writer.WriteAttributeString("Name", "ALL Promotions");
                    writer.WriteEndElement();//Promotion
                }
                writer.WriteEndElement();//TPA_Extensions

                writer.WriteEndElement();//Criterion
                writer.WriteEndElement();//HotelSearchCriteria
                writer.WriteEndElement();// AvailRequestSegment
                writer.WriteEndElement();// AvailRequestSegments
                writer.WriteEndElement();//OTA_HotelAvailRQ 
                writer.WriteEndElement();//Soap:Body
                writer.WriteEndElement();//Soap:Envelope

                writer.Flush();
                writer.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Yatra-Failed to generate Hotel Request message : " + ex.Message, ex);
            }

            return requestMsg.ToString();
        }
        /// <summary>
        /// Reading XML response, assigning hotel data to the HotelSearchResult and Displaly Result in SearchResult Page.
        /// </summary>
        /// <param name="xmlDoc">xml Response Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here  we are calucating B2B Markup and InputVat and also Genarting Hotel Results object</remarks>
        private HotelSearchResult[] GenerateSearchResult(XmlDocument xmlDoc, HotelRequest request, decimal markup, string markupType)
        {

            //Loading All Hotels Static data and images city wise
            //optimized Code
            DataTable dtHotels = HotelStaticData.GetStaticHotelIds(request.CityCode, HotelBookingSource.Yatra);
            DataTable dtImages = HotelImages.GetImagesByCityCode(request.CityCode, HotelBookingSource.Yatra);

            //  HotelSearchResult[] searchResult = new HotelSearchResult[0];
            try
            {
                string filePath = XmlPath + appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CitySearchResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            XmlNode tempNode =null;
            // XmlNode AvailRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS", nsmgr);

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:Errors/ns1:Error", nsmgr);
            //xmlDoc.SelectSingleNode("/OTA_HotelAvailRS/Errors/Error");
            if (ErrorInfo != null)
            {
                XmlAttribute ErrorCode = ErrorInfo.Attributes["Code"];
                if (!string.IsNullOrEmpty(ErrorCode.Value))
                {
                    Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                    {
                    " Yatra:GenerateSearchResult,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                    }), "");
                    throw new BookingEngineException("<br>" + ErrorMessage(ErrorCode.Value));
                }

            }
            PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());

            #region
            XmlNodeList roomStays = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay", nsmgr);
            //  XmlNodeList roomStays = root.SelectSingleNode("soap:Body", nsmgr).FirstChild.ChildNodes[1].ChildNodes;

            List<HotelSearchResult> hotelResults = new List<HotelSearchResult>();

            if (roomStays.Count > 0)
            {

                foreach (XmlNode roomStay in roomStays)
                {
                    
                    HotelSearchResult hotelResult = new HotelSearchResult();

                    #region Hotel Basic Details
                    //request hotelResult.PropertyType = searchId; //Multiple Search Time they Given Multiple Search Id's...This Id We Need To Send Booking tIme
                    hotelResult.BookingSource = HotelBookingSource.Yatra;
                    hotelResult.CityCode = request.CityCode;
                    hotelResult.Currency = currency;
                    hotelResult.StartDate = request.StartDate;
                    hotelResult.EndDate = request.EndDate;
                    foreach (XmlNode htlBasicInfo in roomStay)
                    {
                        if (htlBasicInfo != null && htlBasicInfo.Name== "BasicPropertyInfo")
                        {
                            tempNode = htlBasicInfo;
                        }                       
                    }
                   // tempNode = roomStay.ChildNodes[4];//xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:BasicPropertyInfo", nsmgr);
                    //roomStay.SelectSingleNode("/OTA_HotelAvailRS/RoomStays/RoomStay/BasicPropertyInfo");
                    if (tempNode != null)
                    {
                        XmlAttribute hotelcode = tempNode.Attributes["HotelCode"];
                        if (hotelcode != null)
                        {
                            hotelResult.HotelCode = hotelcode.Value;
                        }
                    }
                    #endregion
                    #region search time avoid Staticinfo downloading                    

                    DataRow[] hotelStaticData = new DataRow[0];
                    try
                    {
                        hotelStaticData = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");
                        if (hotelStaticData != null && hotelStaticData.Length > 0)
                        {

                            hotelResult.HotelName = hotelStaticData[0]["hotelName"].ToString();
                            hotelResult.HotelDescription = hotelStaticData[0]["description"].ToString();
                            hotelResult.HotelAddress = hotelStaticData[0]["address"].ToString();
                            hotelResult.HotelMap = hotelStaticData[0]["hotelMaps"].ToString();
                            hotelResult.HotelLocation = hotelStaticData[0]["location"].ToString();
                            if (!string.IsNullOrEmpty(hotelStaticData[0]["hotelRating"].ToString()))
                            {
                                hotelResult.Rating = (HotelRating)Convert.ToInt32(Math.Floor(Convert.ToDecimal(hotelStaticData[0]["hotelRating"].ToString())));
                            }
                            Audit.Add(EventType.YatraBooking, Severity.High, (int)appUserId, " Hotel static OK" , "");
                        }
                    }
                    //catch { continue; }
                    catch (Exception exp)
                    {
                        Audit.Add(EventType.YatraBooking, Severity.High, (int)appUserId, " Hotel static"+exp.ToString(), "");
                    }
                    DataRow[] hotelImages = new DataRow[0];
                    try
                    {
                        hotelImages = dtImages.Select("hotelCode='" + hotelResult.HotelCode + "'");
                        string hImages = (hotelImages != null && hotelImages.Length > 0 ? hotelImages[0]["images"].ToString() : string.Empty);
                        hotelResult.HotelPicture = string.Empty;
                        if (!string.IsNullOrEmpty(hImages))
                        {
                            hotelResult.HotelPicture = hImages.Split('|')[0];
                        }
                        Audit.Add(EventType.YatraBooking, Severity.High, (int)appUserId, " Hotel picture OK", "");
                    }
                    //catch { continue; }
                    catch (Exception exp1)
                    {
                        Audit.Add(EventType.YatraBooking, Severity.High, (int)appUserId, " Hotel image" + exp1.ToString(), "");
                    }

                    #endregion
                    #region To get only those satisfy the search conditions
                    if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                    {
                        if (!(hotelResult.HotelName != null && hotelResult.HotelName.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(hotelResult.HotelName.ToUpper()) : true))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    #endregion
                    #region Rooms Binding
                    /////
                    Audit.Add(EventType.YatraBooking, Severity.High, (int)appUserId, " ziyad Hotel Name " + hotelResult.HotelName, "");

                    XmlNodeList RoomTypesNodeList = null;// roomStay.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomTypes/ns1:RoomType", nsmgr);
                    //roomStay.FirstChild.ChildNodes;
                    XmlNodeList RatePlansNodeList = null;//xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan", nsmgr);
                    // roomStay.ChildNodes[1].ChildNodes;
                    XmlNodeList RoomRatesNodeList = null;//xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate", nsmgr);
                    //roomStay.ChildNodes[2].ChildNodes;

                    foreach (XmlNode RoomResponse in roomStay)
                    {
                        if (RoomResponse != null && RoomResponse.Name == "RoomTypes")
                        {
                            RoomTypesNodeList = RoomResponse.ChildNodes;
                        }
                        if (RoomResponse != null && RoomResponse.Name == "RatePlans")
                        {
                            RatePlansNodeList = RoomResponse.ChildNodes;
                        }
                        if (RoomResponse != null && RoomResponse.Name == "RoomRates")
                        {
                            RoomRatesNodeList = RoomResponse.ChildNodes;
                        }                        
                    }

                    hotelResult.RoomGuest = request.RoomGuest;
                    rateOfExchange = (exchangeRates.ContainsKey(hotelResult.Currency) ? exchangeRates[hotelResult.Currency] : 1);
                    int noOfDays = request.EndDate.Subtract(request.StartDate).Days;
                    if (RatePlansNodeList != null)
                    {
                        int rrCount = 0; // RoomRates count
                        int index = 0;
                        HotelRoomsDetails[] rooms = new HotelRoomsDetails[RatePlansNodeList.Count * request.NoOfRooms];
                        foreach (XmlNode ratePlanNode in RatePlansNodeList)
                        {

                            for (int i = 0; i < Convert.ToInt32(request.NoOfRooms); i++)
                            {
                                HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                                roomDetail.SequenceNo = (i + 1).ToString();
                                //This node will used in  Price Calculation  Region
                                XmlNode tempNodeRates = RoomRatesNodeList.Item(rrCount);

                                #region Room Details ( Room Type , Rate Plan , Room Rates, etc... )

                                XmlAttribute RoomTypeName = ratePlanNode.Attributes["RatePlanName"];
                                if (RoomTypeName != null)
                                {
                                    roomDetail.RoomTypeName = RoomTypeName.Value;
                                }
                                XmlAttribute RoomTypeCode = tempNodeRates.Attributes["RoomID"];
                                if (RoomTypeCode != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.SequenceNo + "|" + RoomTypeCode.Value;
                                }
                                XmlAttribute RatePlanCode = ratePlanNode.Attributes["RatePlanCode"];
                                if (RatePlanCode != null)
                                {
                                    roomDetail.RatePlanCode = RatePlanCode.Value;
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + RatePlanCode.Value;
                                }
                                XmlAttribute currencyCode = tempNode.Attributes["CurrencyCode"]; ;
                                if (currencyCode != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + currencyCode.Value;//CurrencyCode
                                }

                                #endregion//Room Details ( Room Type , Rate Plan , Room Rates, etc... ) - end
                                #region Promotion  TODO
                                if (ratePlanNode != null)
                                {
                                    string rpCode = ratePlanNode.Attributes["RatePlanCode"].InnerText;
                                    if (!string.IsNullOrEmpty(rpCode) && rpCode == roomDetail.RoomTypeCode.Split('|')[2])
                                    {
                                        foreach (XmlNode Promo in ratePlanNode)
                                        {
                                            XmlNode PromoNode;
                                            switch (Promo.Name)
                                            {
                                                case "RatePlanDescription":
                                                    foreach (XmlNode textNode in Promo)
                                                    {
                                                        roomDetail.PromoMessage = textNode.InnerText + ",";
                                                        hotelResult.PromoMessage = textNode.InnerText + ",";
                                                    }
                                                    break;
                                                case "TPA_Extensions":
                                                    PromoNode = Promo.ChildNodes[0];
                                                    if (PromoNode != null)
                                                    {
                                                        XmlAttribute promotionid = PromoNode.Attributes["Id"];

                                                        if (promotionid != null)
                                                        {
                                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + promotionid.Value;
                                                        }
                                                    }
                                                    break;
                                                case "RatePlanInclusions":
                                                    string roomAmenity = string.Empty;
                                                    roomDetail.Amenities = new List<string>(); 
                                                    foreach (XmlNode lstRatePlanInc in RatePlansNodeList)
                                                    {
                                                        //XmlNode tempRPCode = lstRatePlanInc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan", nsmgr);
                                                        string ratePlanInc = lstRatePlanInc.Attributes["RatePlanCode"].InnerText;
                                                        if (!string.IsNullOrEmpty(ratePlanInc) && ratePlanInc == roomDetail.RoomTypeCode.Split('|')[2])
                                                        {
                                                            XmlNode tempPlanInc = lstRatePlanInc.ChildNodes[3];
                                                            //.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan/ns1:RatePlanInclusions/ns1:RatePlanInclusionDesciption", nsmgr);
                                                            foreach (XmlNode textNode in tempPlanInc)
                                                            {
                                                                roomAmenity += textNode.InnerText + ",";
                                                            }
                                                        }
                                                    }
                                                    if (!string.IsNullOrEmpty(roomAmenity))
                                                    {
                                                        roomDetail.mealPlanDesc = roomAmenity.TrimEnd(',');
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                #endregion
                                #region Price Calculation 

                                decimal totPrice = 0m;
                                decimal inclusiveAmount = 0;
                                decimal taxesAmount = 0;

                                foreach (XmlNode tempRoomRatesNode in RoomRatesNodeList)
                                {
                                    XmlAttribute roomID = tempRoomRatesNode.Attributes["RoomID"];
                                    XmlAttribute ratePlanCode = tempRoomRatesNode.Attributes["RatePlanCode"];

                                    if (tempRoomRatesNode != null && roomID.Value == RoomTypeCode.Value && ratePlanCode.Value == RatePlanCode.Value)
                                    {
                                        XmlNode tempNodeRate = tempRoomRatesNode.FirstChild.ChildNodes[0];
                                        //xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate", nsmgr);// tempRoomRatesNode.FirstChild.ChildNodes;

                                        foreach (XmlNode RateNodeTags in tempNodeRate)
                                        {
                                            switch (RateNodeTags.Name)
                                            {
                                                case "Base":
                                                    //xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:Base/ns1:Taxes", nsmgr);
                                                    //XmlNode baseAmount = tempRoomRatesNode.FirstChild.ChildNodes[0].FirstChild;
                                                    if (RateNodeTags != null)
                                                    {
                                                        XmlAttribute AmountBeforeTax = RateNodeTags.Attributes["AmountBeforeTax"];
                                                        if (AmountBeforeTax != null)
                                                        {
                                                            inclusiveAmount += Convert.ToDecimal(AmountBeforeTax.Value);
                                                        }
                                                    }
                                                    //xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:Base/ns1:Taxes", nsmgr);
                                                    XmlNode taxes = //xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:Base/ns1:Taxes", nsmgr);
                                                                RateNodeTags.FirstChild;
                                                    if (taxes != null)
                                                    {
                                                        XmlAttribute tax = taxes.Attributes["Amount"];
                                                        if (tax != null)
                                                        {
                                                            taxesAmount += Convert.ToDecimal(tax.Value);
                                                        }
                                                    }
                                                    break;
                                                case "AdditionalGuestAmounts":
                                                    foreach (XmlNode AdditionalGuestAmounts in RateNodeTags)
                                                    {
                                                        XmlNode GuestAmount = AdditionalGuestAmounts.FirstChild; 
                                                            //xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:AdditionalGuestAmounts/ns1:AdditionalGuestAmount/ns1:Amount", nsmgr);// tempRoomRatesNode.FirstChild.ChildNodes;
                                                        if (GuestAmount != null)
                                                        {
                                                            XmlAttribute Amount = GuestAmount.Attributes["AmountBeforeTax"];
                                                            if (Amount != null)
                                                            {
                                                                inclusiveAmount += Convert.ToDecimal(Amount.Value);
                                                            }
                                                        }
                                                    }
                                                    break;
                                                case "Discount":
                                                    XmlAttribute Discount = RateNodeTags.Attributes["AmountBeforeTax"];
                                                    if (Discount != null)
                                                    {
                                                        inclusiveAmount -= Convert.ToDecimal(Discount.Value);
                                                    }
                                                    break;
                                            }
                                        }
                                    }

                                }

                                totPrice = (inclusiveAmount + taxesAmount);
                                //Booking Time Required
                                roomDetail.TBOPrice = new PriceAccounts();
                                roomDetail.TBOPrice.NetFare = inclusiveAmount;
                                roomDetail.TBOPrice.Tax = taxesAmount;
                                /*  Supplier(Yatra) is not segregating room wise price. so, we are doing segregation room wise price based on no of rooms. 
                                    Ex: TotAmount = 2000 INR(2 rooms, 4 nights) given by supplier
                                    Then we are showing , choose room time                                  
                                    Select room 1 amount = 1000 (4 nights)(day wise breakup = 1 - 500, 2 - 500, 3 - 500, 4 - 500 )
                                    Select room 2 amount = 1000 (4 nights)(day wise breakup = 1 - 500, 2 - 500, 3 - 500, 4 - 500 )
                                    Total Amout Summary= 2000
                                 */
                                totPrice = totPrice / request.NoOfRooms;

                                decimal hotelTotalPrice = 0m;
                                decimal vatAmount = 0m;
                                hotelTotalPrice = Math.Round((totPrice) * rateOfExchange, decimalPoint);
                                if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                {
                                    hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                }
                                hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                                roomDetail.TotalPrice = hotelTotalPrice;
                                roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                                roomDetail.MarkupType = markupType;
                                roomDetail.MarkupValue = markup;
                                roomDetail.SellingFare = roomDetail.TotalPrice;
                                roomDetail.supplierPrice = Math.Round((totPrice));
                                // roomDetail.TotalTax = taxesAmount;//supplier taxes
                                roomDetail.TaxDetail = new PriceTaxDetails();
                                roomDetail.TaxDetail = priceTaxDet;
                                roomDetail.InputVATAmount = vatAmount;
                                hotelResult.Price = new PriceAccounts();
                                hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                hotelResult.Price.SupplierPrice = Math.Round(inclusiveAmount); // Math.Round((totPrice * noOfDays));
                                //hotelResult.Price.Tax = taxesAmount * noOfDays;
                                hotelResult.Price.RateOfExchange = rateOfExchange;

                                //day wise price calucation
                                #region day wise price calucation
                                System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                decimal totalprice = roomDetail.TotalPrice;

                                for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                {
                                    decimal price = roomDetail.TotalPrice / diffResult.Days;
                                    if (fareIndex == diffResult.Days - 1)
                                    {
                                        price = totalprice;
                                    }
                                    totalprice -= price;
                                    hRoomRates[fareIndex].Amount = price;
                                    hRoomRates[fareIndex].BaseFare = price;
                                    hRoomRates[fareIndex].SellingFare = price;
                                    hRoomRates[fareIndex].Totalfare = price;
                                    hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                    hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                                }
                                roomDetail.Rates = hRoomRates;
                                #endregion //day wise price calucation - END

                                #endregion //Price Calculation  - END
                                #region Cancellation Policy
                                //XmlNodeList cancellationPolicyList =  ratePlanNode.ChildNodes[1].FirstChild.ChildNodes;
                                XmlNode cancellationPolicyList = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan/ns1:CancelPenalties/ns1:CancelPenalty", nsmgr);
                                //XmlNode cancellationPolicyList = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan/ns1:CancelPenalties/ns1:CancelPenalty", nsmgr);///ns1:CancelPenalty/ns1:PenaltyDescription
                                int buffer = Convert.ToInt32(ConfigurationSystem.YatraConfig["buffer"]);
                                DateTime bufferedCancelDate = DateTime.MinValue;
                                string message = string.Empty;
                                string cancelDate = string.Empty;
                                //string bufferedCancelDate = string.Empty;
                                foreach (XmlNode cancelPolicy in cancellationPolicyList)
                                {
                                    if (cancelPolicy.Name == "PenaltyDescription")
                                    {
                                        XmlAttribute cancelPolicyNode = cancelPolicy.Attributes["Name"];
                                        if (cancelPolicyNode != null && cancelPolicyNode.Value.Trim().ToUpper() == "CANCELLATION_POLICY")
                                        {
                                            if (string.IsNullOrEmpty(cancelDate))
                                            {
                                                try
                                                {
                                                    //for finding Date (dd-MMM-yy format -- 09-Dec-18)
                                                    Regex rgx = new Regex(@"\d{2}-[A-Z]{1}[a-z]{2}-\d{2}");
                                                    Match mat = rgx.Match(cancelPolicy.InnerText);
                                                    cancelDate = mat.ToString();//cancelPolicy.InnerText;
                                                    ////cancelDate = cancelDate.Substring(cancelDate.Substring(0, cancelDate.LastIndexOf("y") + 1).Length, cancelDate.Length - (cancelDate.Substring(0, cancelDate.LastIndexOf("y") + 1).Length));
                                                    ////cancelDate = cancelDate.Substring(0, 10);
                                                    if (!string.IsNullOrEmpty(cancelDate))
                                                    {
                                                        bufferedCancelDate = Convert.ToDateTime(cancelDate).AddDays(-buffer);
                                                    }
                                                }
                                                catch { }
                                            }
                                            message += cancelPolicy.InnerText + "|";
                                        }
                                    }

                                }
                                message = message.TrimEnd('|');
                                roomDetail.CancellationPolicy = bufferedCancelDate != DateTime.MinValue ? message.Replace(cancelDate, " " + bufferedCancelDate.ToString("dd-MMM-yy")) : "Cancellation details are not available";

                                #endregion

                                rooms[index] = roomDetail;
                                index++;
                            }
                            hotelResult.RoomDetails = rooms.OrderBy(o => o.supplierPrice).ToArray();
                            rrCount += noOfDays;//hotelResult.EndDate.Subtract(hotelResult.StartDate).Days; // RataPlans count increment

                        }

                    }
                    hotelResult.Currency = agentCurrency;
                    for (int i = 0; i < request.NoOfRooms; i++)
                    {
                        for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                        {
                            if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                            {
                                hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                hotelResult.Price.AccPriceType = PriceType.NetFare;
                                hotelResult.Price.RateOfExchange = rateOfExchange;
                                hotelResult.Price.Currency = agentCurrency;
                                hotelResult.Price.CurrencyCode = agentCurrency;
                                break;
                            }
                        }
                    }
                    #endregion
                    hotelResults.Add(hotelResult);                
                }
            }
            return hotelResults.Where(h=> !string.IsNullOrEmpty(h.HotelName)).ToArray();

            #endregion

        }
        /// <summary>
        /// GetRooms
        /// </summary>
        /// <param name="hotelResult">HotelResults object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This B2B Markup</param>
        /// <param name="markupType">this B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <remarks>Here Only we are getting real live inventory and price</remarks>
        public void GetRooms(ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType)
        {
            #region Request string
            // Prapare XML format string request  
            string reqestrooms = GenerateHotelAvailabilityRequest(request, hotelResult.HotelCode);
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(reqestrooms);
                string filePath = XmlPath + appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelDetailRequest.xml";
                XmlDoc.Save(filePath);
            }
            catch { }
            #endregion

            #region Send Reqest for Get Hotels
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                //Sending request to Yatra end point url for Hotel Detail Search
                xmlResp = SendRequest(reqestrooms, searchAvailReqURL, "Hotel Details");
                //xmlResp.Load(@"C:\Users\Admin\Desktop\yatra\5992_3c54b561-a516-4b90-aae1-3db5e7aafb50_23102019_112256_HotelDetailResponse.xml");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                        "Exception returned from Yatra.GetRooms Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                }), "");

                throw new BookingEngineException("Error: " + ex.Message);
            }
            #endregion
            #region Generate Serch Result
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    hotelResult = GenerateRoomsSearchResult(xmlResp, ref hotelResult, request, markup, markupType); //TODO
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                        "Exception returned from Yatra.GetRooms Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                }), "");
                ////Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
            {
                    " Response from Yatra for Search Request:| ",
                    DateTime.Now,
                    "| request XML:",
                    request,
                    "|response XML:",
                    xmlResp.OuterXml
            }), "");
            // result = searchRes;
            #endregion
        }
        /// <summary>
        /// Reading XML response, assigning hotel data to the HotelSearchResult and Displaly Result in SearchResult Page.
        /// </summary>
        /// <param name="xmlDoc">xml Response Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here Only we are calucating B2B Markup and InputVat and also Genarting Hotel Results object</remarks>
        private HotelSearchResult GenerateRoomsSearchResult(XmlDocument xmlDoc, ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType)
        {
            try
            {
                string filePath = XmlPath + appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelDetailResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            XmlNode root = xmlDoc.DocumentElement;
            #region Error from API
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:Errors/ns1:Error", nsmgr);

            if (ErrorInfo != null)
            {
                XmlAttribute ErrorCode = ErrorInfo.Attributes["Code"];
                if (!string.IsNullOrEmpty(ErrorCode.Value))
                {
                    Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                    {
                    " Yatra:GenerateRoomsSearchResult,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                    }), "");
                    throw new BookingEngineException("<br>" + ErrorMessage(ErrorCode.Value));
                }

            }
            #endregion

            PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());

            #region Room Stay(Hotels) 
            XmlNodeList roomStays = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay", nsmgr);
            //root.SelectSingleNode("soap:Body", nsmgr).FirstChild.ChildNodes[1].ChildNodes;

            if (roomStays.Count > 0)
            {
                foreach (XmlNode roomStay in roomStays)
                {
                    ///Rooms Binding
                    XmlNode supplierCurrency = null;//xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:BasicPropertyInfo", nsmgr);
                    //roomStay.ChildNodes[4]; 
                    XmlNodeList RoomTypesNodeList = null;// xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomTypes/ns1:RoomType", nsmgr);
                    //roomStay.FirstChild.ChildNodes; 
                    XmlNodeList RatePlansNodeList = null;// xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan", nsmgr);
                    //roomStay.ChildNodes[1].ChildNodes;
                    XmlNodeList RoomRatesNodeList = null;// xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate", nsmgr);
                    //roomStay.ChildNodes[2].ChildNodes;
                    XmlNode RoomAmenities = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:TPA_Extensions/ns1:HotelBasicInformation/ns1:Amenities", nsmgr);
                    foreach (XmlNode htlDetails in roomStay)
                    {                        
                        if (htlDetails != null && htlDetails.Name == "RoomTypes")
                        {
                            RoomTypesNodeList = htlDetails.ChildNodes;
                        }
                        if (htlDetails != null && htlDetails.Name == "RatePlans")
                        {
                            RatePlansNodeList = htlDetails.ChildNodes;
                        }
                        if (htlDetails != null && htlDetails.Name == "RoomRates")
                        {
                            RoomRatesNodeList = htlDetails.ChildNodes;
                        }
                        if (htlDetails != null && htlDetails.Name == "BasicPropertyInfo")
                        {
                            supplierCurrency = htlDetails;
                        }
                    }
                    hotelResult.RoomGuest = request.RoomGuest;
                    XmlAttribute supplierCurrencyAttibute = null;
                    if (supplierCurrency != null)
                    {
                        supplierCurrencyAttibute = supplierCurrency.Attributes["CurrencyCode"];
                    }
                    rateOfExchange = (exchangeRates.ContainsKey(supplierCurrencyAttibute.Value) ? exchangeRates[supplierCurrencyAttibute.Value] : 1);
                    int noOfDays = request.EndDate.Subtract(request.StartDate).Days;
                    if (RatePlansNodeList != null)
                    {
                        //string roomAmenity = string.Empty;
                        int rrCount = 0; // RoomRates count
                        int index = 0;
                        HotelRoomsDetails[] rooms = new HotelRoomsDetails[RatePlansNodeList.Count * request.NoOfRooms];
                        foreach (XmlNode ratePlanNode in RatePlansNodeList)
                        {
                            for (int i = 0; i < Convert.ToInt32(request.NoOfRooms); i++)
                            {
                                HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                                roomDetail.SequenceNo = (i + 1).ToString();
                                //This node will used in  Price Calculation  Region
                                XmlNode tempNodeRates = RoomRatesNodeList.Item(rrCount);

                                #region Room Details ( Room Type , Rate Plan , Room Rates, etc... )

                                //XmlAttribute NonSmoking = RoomTypeNode.Attributes["NonSmoking"];
                                //if (NonSmoking != null)
                                //{
                                //    roomDetail.SmokingPreference = NonSmoking.Value == "true" ? "Non Smoking" : "Smoking Allowed";
                                //}

                                XmlAttribute RoomTypeName = ratePlanNode.Attributes["RatePlanName"];
                                if (RoomTypeName != null)
                                {
                                    roomDetail.RoomTypeName = RoomTypeName.Value;
                                }
                                XmlAttribute RoomTypeCode = tempNodeRates.Attributes["RoomID"];
                                if (RoomTypeCode != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.SequenceNo + "|" + RoomTypeCode.Value;
                                }
                                XmlAttribute RatePlanCode = ratePlanNode.Attributes["RatePlanCode"];
                                if (RatePlanCode != null)
                                {
                                    roomDetail.RatePlanCode = RatePlanCode.Value;
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + RatePlanCode.Value;
                                }
                                roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + "INR";//CurrencyCode

                                #endregion
                                #region Promotion  
                                if (ratePlanNode != null)
                                {
                                    string rpCode = ratePlanNode.Attributes["RatePlanCode"].InnerText;
                                    if (!string.IsNullOrEmpty(rpCode) && rpCode == roomDetail.RoomTypeCode.Split('|')[2])
                                    {
                                        foreach (XmlNode Promo in ratePlanNode)
                                        {
                                            XmlNode PromoNode;
                                            switch (Promo.Name)
                                            {
                                                case "RatePlanDescription":
                                                    foreach (XmlNode textNode in Promo)
                                                    {
                                                        roomDetail.PromoMessage = textNode.InnerText;
                                                        hotelResult.PromoMessage = textNode.InnerText + ",";
                                                    }
                                                    break;
                                                case "TPA_Extensions":
                                                    PromoNode = Promo.ChildNodes[0];
                                                    if (PromoNode != null)
                                                    {
                                                        XmlAttribute promotionid = PromoNode.Attributes["Id"];

                                                        if (promotionid != null)
                                                        {
                                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + promotionid.Value;
                                                        }
                                                    }
                                                    break;
                                                case "RatePlanInclusions":

                                                    string mealPlan = string.Empty;

                                                    foreach (XmlNode lstRatePlanInc in RatePlansNodeList)
                                                    {
                                                        //XmlNode tempRPCode = lstRatePlanInc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan", nsmgr);
                                                        string ratePlanInc = lstRatePlanInc.Attributes["RatePlanCode"].InnerText;
                                                        if (!string.IsNullOrEmpty(ratePlanInc) && ratePlanInc == roomDetail.RoomTypeCode.Split('|')[2])
                                                        {
                                                            XmlNode tempPlanInc = lstRatePlanInc.ChildNodes[3];
                                                            //.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan/ns1:RatePlanInclusions/ns1:RatePlanInclusionDesciption", nsmgr);
                                                            foreach (XmlNode textNode in tempPlanInc)
                                                            {
                                                                mealPlan += textNode.InnerText + ",";
                                                            }
                                                        }

                                                    }
                                                    if (!string.IsNullOrEmpty(mealPlan))
                                                    {
                                                        roomDetail.mealPlanDesc = mealPlan;
                                                    }
                                                    break;

                                            }
                                        }
                                    }
                                }
                                #endregion
                                #region Room Amenities
                                if (RoomAmenities != null)
                                {
                                    roomDetail.Amenities = new List<string>();
                                    foreach (XmlNode RoomAmenitie in RoomAmenities)
                                    {
                                        if (RoomAmenitie.Name == "RoomAmenities")
                                        {
                                            XmlAttribute description = RoomAmenitie.Attributes["description"];
                                            if (description != null)
                                            {
                                                roomDetail.Amenities.Add(description.Value);
                                                //roomAmenity += description.Value + ",";
                                            }
                                        }
                                    }

                                }
                                #endregion
                                #region Price Calculation 

                                if (tempNodeRates != null)
                                {
                                    decimal totPrice = 0m;
                                    decimal inclusiveAmount = 0;
                                    decimal taxesAmount = 0;

                                    foreach (XmlNode tempRoomRatesNode in RoomRatesNodeList)
                                    {
                                        XmlAttribute roomID = tempRoomRatesNode.Attributes["RoomID"];
                                        XmlAttribute ratePlanCode = tempRoomRatesNode.Attributes["RatePlanCode"];
                                        
                                            if (tempRoomRatesNode != null && roomID.Value == RoomTypeCode.Value && ratePlanCode.Value == RatePlanCode.Value)
                                            {
                                                XmlNode tempNodeRate = tempRoomRatesNode.FirstChild.ChildNodes[0];
                                                // xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate", nsmgr);// tempRoomRatesNode.FirstChild.ChildNodes;

                                                foreach (XmlNode RateNodeTags in tempNodeRate)
                                                {
                                                    switch (RateNodeTags.Name)
                                                    {
                                                        case "Base":///soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:Base 
                                                            //xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:Base/ns1:Taxes", nsmgr);
                                                            //XmlNode baseAmount = tempRoomRatesNode.FirstChild.ChildNodes[0].FirstChild;
                                                            if (RateNodeTags != null)
                                                            {
                                                                XmlAttribute AmountBeforeTax = RateNodeTags.Attributes["AmountBeforeTax"];
                                                                if (AmountBeforeTax != null)
                                                                {
                                                                    inclusiveAmount += Convert.ToDecimal(AmountBeforeTax.Value);
                                                                }
                                                            }

                                                            XmlNode taxes =// xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:Base/ns1:Taxes", nsmgr);
                                                                        RateNodeTags.FirstChild;///soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:Base/ns1:Taxes
                                                            if (taxes != null)
                                                            {
                                                                XmlAttribute tax = taxes.Attributes["Amount"];
                                                                if (tax != null)
                                                                {
                                                                    taxesAmount += Convert.ToDecimal(tax.Value);
                                                                }
                                                            }
                                                            break;
                                                        case "AdditionalGuestAmounts":
                                                            foreach (XmlNode AdditionalGuestAmounts in RateNodeTags)
                                                            {
                                                                XmlNode GuestAmount = AdditionalGuestAmounts.FirstChild;
                                                                //xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RoomRates/ns1:RoomRate/ns1:Rates/ns1:Rate/ns1:AdditionalGuestAmounts/ns1:AdditionalGuestAmount/ns1:Amount", nsmgr);// tempRoomRatesNode.FirstChild;
                                                                if (GuestAmount != null)
                                                                {
                                                                    XmlAttribute Amount = GuestAmount.Attributes["AmountBeforeTax"];
                                                                    if (Amount != null)
                                                                    {
                                                                        inclusiveAmount += Convert.ToDecimal(Amount.Value);
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case "Discount":
                                                            XmlAttribute Discount = RateNodeTags.Attributes["AmountBeforeTax"];
                                                            if (Discount != null)
                                                            {
                                                                inclusiveAmount -= Convert.ToDecimal(Discount.Value);
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                       
                                    }

                                    totPrice = inclusiveAmount + taxesAmount;

                                    //Booking Time Required
                                    roomDetail.TBOPrice = new PriceAccounts();
                                    roomDetail.TBOPrice.NetFare = inclusiveAmount;
                                    roomDetail.TBOPrice.Tax = taxesAmount;
                                    /*  Supplier(Yatra) is not segregating room wise price. so, we are doing segregation room wise price based on no of rooms. 
                                    Ex: TotAmount = 2000 INR(2 rooms, 4 nights) given by supplier
                                    Then we are showing , choose room time                                  
                                    Select room 1 amount = 1000 (4 nights)(day wise breakup = 1 - 500, 2 - 500, 3 - 500, 4 - 500 )
                                    Select room 2 amount = 1000 (4 nights)(day wise breakup = 1 - 500, 2 - 500, 3 - 500, 4 - 500 )
                                    Total Amout Summary= 2000
                                 */
                                    totPrice = totPrice / request.NoOfRooms;

                                    decimal hotelTotalPrice = 0m;
                                    decimal vatAmount = 0m;
                                    hotelTotalPrice = Math.Round((totPrice) * rateOfExchange, decimalPoint);
                                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                    {
                                        hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                    }
                                    hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                                    roomDetail.TotalPrice = hotelTotalPrice;
                                    roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                                    roomDetail.MarkupType = markupType;
                                    roomDetail.MarkupValue = markup;
                                    roomDetail.SellingFare = roomDetail.TotalPrice;
                                    roomDetail.supplierPrice = Math.Round((totPrice));
                                    // roomDetail.TotalTax = taxesAmount ;//supplier taxes 
                                    roomDetail.TaxDetail = new PriceTaxDetails();
                                    roomDetail.TaxDetail = priceTaxDet;
                                    roomDetail.InputVATAmount = vatAmount;
                                    hotelResult.Price = new PriceAccounts();
                                    hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                    hotelResult.Price.SupplierPrice = Math.Round(inclusiveAmount); // Math.Round((totPrice * noOfDays));
                                    //hotelResult.Price.Tax = taxesAmount * noOfDays;
                                    hotelResult.Price.RateOfExchange = rateOfExchange;
                                }
                                //day wise price calucation
                                System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                decimal totalprice = roomDetail.TotalPrice;

                                for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                {
                                    decimal price = roomDetail.TotalPrice / diffResult.Days;
                                    if (fareIndex == diffResult.Days - 1)
                                    {
                                        price = totalprice;
                                    }
                                    totalprice -= price;
                                    hRoomRates[fareIndex].Amount = price;
                                    hRoomRates[fareIndex].BaseFare = price;
                                    hRoomRates[fareIndex].SellingFare = price;
                                    hRoomRates[fareIndex].Totalfare = price;
                                    hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                    hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                                }
                                roomDetail.Rates = hRoomRates;

                                #endregion
                                #region Cancellation Policy
                                //XmlNodeList cancellationPolicyList =  ratePlanNode.ChildNodes[1].FirstChild.ChildNodes;
                                XmlNode cancellationPolicyList = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan/ns1:CancelPenalties/ns1:CancelPenalty", nsmgr);
                                //XmlNode cancellationPolicyList = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:RatePlans/ns1:RatePlan/ns1:CancelPenalties/ns1:CancelPenalty", nsmgr);///ns1:CancelPenalty/ns1:PenaltyDescription
                                int buffer = Convert.ToInt32(ConfigurationSystem.YatraConfig["buffer"]);
                                DateTime bufferedCancelDate = DateTime.MinValue;
                                string message = string.Empty;
                                string cancelDate = string.Empty;
                                //string bufferedCancelDate = string.Empty;
                                foreach (XmlNode cancelPolicy in cancellationPolicyList)
                                {
                                    if (cancelPolicy.Name == "PenaltyDescription")
                                    {
                                        XmlAttribute cancelPolicyNode = cancelPolicy.Attributes["Name"];
                                        if (cancelPolicyNode != null && cancelPolicyNode.Value.Trim().ToUpper() == "CANCELLATION_POLICY")
                                        {
                                            if (string.IsNullOrEmpty(cancelDate))
                                            {
                                                try
                                                {
                                                    //for finding Date (dd-MMM-yy format -- 09-Dec-18)
                                                    Regex rgx = new Regex(@"\d{2}-[A-Z]{1}[a-z]{2}-\d{2}");
                                                    Match mat = rgx.Match(cancelPolicy.InnerText);
                                                    cancelDate = mat.ToString();//cancelPolicy.InnerText;
                                                    ////cancelDate = cancelDate.Substring(cancelDate.Substring(0, cancelDate.LastIndexOf("y") + 1).Length, cancelDate.Length - (cancelDate.Substring(0, cancelDate.LastIndexOf("y") + 1).Length));
                                                    ////cancelDate = cancelDate.Substring(0, 10);
                                                    if (!string.IsNullOrEmpty(cancelDate))
                                                    {
                                                        bufferedCancelDate = Convert.ToDateTime(cancelDate).AddDays(-buffer);
                                                    }
                                                }
                                                catch { }
                                            }
                                            message += cancelPolicy.InnerText + "|";
                                        }
                                    }

                                }
                                message = message.TrimEnd('|');
                                roomDetail.CancellationPolicy = bufferedCancelDate != DateTime.MinValue ? message.Replace(cancelDate, " " + bufferedCancelDate.ToString("dd-MMM-yy")) : message;

                                #endregion

                                rooms[index] = roomDetail;
                                index++;
                            }
                            hotelResult.RoomDetails = rooms.OrderBy(o => o.supplierPrice).ToArray();
                            //hotelResult.PromoMessage = roomAmenity;
                            rrCount += hotelResult.EndDate.Subtract(hotelResult.StartDate).Days;
                        }

                    }
                    hotelResult.Currency = agentCurrency;
                    for (int i = 0; i < request.NoOfRooms; i++)
                    {
                        for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                        {
                            if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                            {
                                hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                hotelResult.Price.AccPriceType = PriceType.NetFare;
                                hotelResult.Price.RateOfExchange = rateOfExchange;
                                hotelResult.Price.Currency = agentCurrency;
                                hotelResult.Price.CurrencyCode = agentCurrency;
                                break;
                            }
                        }
                    }

                }

            }
            return hotelResult;
            #endregion
        }
        #endregion

        #region Booking
        /// <summary>
        /// Booking Method
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(booking correspondent room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        /// <remarks>Here we are preparing Booking request object and sending book request to api </remarks>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            //Provide any unique identifiers which can be used for checking theroot cause if booking fails and also for auditing purpose. 
            // Recomended by supplier 

            // Limited by the timer resolution which is platform dependent. But you will be able to generate many per second without risk of collisson.
            string TransactionIdentifier = String.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);

            //Provisional Booking Req generation
            string request = GenerateBookingRequest(itinerary, "Provisional", TransactionIdentifier, string.Empty, string.Empty) ?? string.Empty; ;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(request);
            try
            {
                string filePath = XmlPath + appUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ProvisionalBookingRequest.xml";
                doc.Save(filePath);
            }
            catch { }

            XmlDocument xmlResp = new XmlDocument();
            //string resp = string.Empty;
            BookingResponse searchRes = new BookingResponse();
            try
            {
                //Provisional Booking req send
                xmlResp = SendRequest(request, bookingReqURL, "Provisional Booking");

                #region Final Booking 
                try
                {
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        string Type = string.Empty;
                        string ID = string.Empty;
                        //Provisional booking response( Here we get Provisional booking ID, Type)
                        ReadResponseProvisionalBooking(xmlResp, ref Type, ref ID);

                        if (!string.IsNullOrEmpty(Type) && !string.IsNullOrEmpty(ID))
                        {
                            //Final Booking Req generation
                            request = GenerateBookingRequest(itinerary, "Final", TransactionIdentifier, Type, ID); // Type - Provisional Booking Res Type, ID - Provisional Booking Res ID,
                            doc = new XmlDocument();
                            doc.LoadXml(request);
                            try
                            {
                                string filePath = XmlPath + appUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_FinalBookingRequest.xml";
                                doc.Save(filePath);
                            }
                            catch { }

                            //Final Booking req send
                            xmlResp = SendRequest(request, bookingReqURL, "Final Booking");
                            try
                            {
                                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                                {
                                    //Reading Final booking response (Here we get Confirmation No)
                                    itinerary.BookingRefNo = TransactionIdentifier;// for  Supplier Uniq identifier , Provided by Us.
                                    searchRes = ReadResponseBooking(xmlResp, itinerary);
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new BookingEngineException("Error: " + ex.Message);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.YatraBooking, Severity.High, (int)appUserId, "Exception returned from Yatra.GetBooking Error Message:" + ex.ToString() + " | " + DateTime.Now + "| provisional booking response XML" + request + "|provisional booking response XML" + xmlResp.OuterXml, "");
                    throw new BookingEngineException("Error: " + ex.ToString());
                }

                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraBooking, Severity.High, (int)appUserId, "Exception returned from Yatra.GetBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| provisional booking request XML" + request + "|provisional booking response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }

            return searchRes;
        }
        /// <summary>
        /// GenerateBookingRequest(Provisional/Final)
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <param name="booking">Booing Type , Provisional/Final</param>
        /// <param name="TransactionIdentifier">Provide any unique identifiers which can be used for checking theroot cause if booking fails and also for auditing purpose.</param>
        /// <param name="Type">Provisional/Final booking type Provided by supplier , for Provisional booking is always empty </param>
        /// <param name="ID">Provisional/Final booking ID Provided by supplier , for Provisional booking is always empty </param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenerateBookingRequest(HotelItinerary itinerary, string booking, string TransactionIdentifier, string Type, string ID)
        {
            StringBuilder requestMsg = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_HotelResRQ", "http://www.opentravel.org/OTA/2003/05"); //OTA_HotelResRQ start
                if (booking == "Provisional")
                {
                    writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                    writer.WriteAttributeString("xsi", "schemaLocation", null, "http://www.opentravel.org/OTA/2003/05 OTA_HotelResRQ.xsd");
                    writer.WriteAttributeString("Version", "1.003");
                }
                writer.WriteAttributeString("CorrelationID", sessionId);// Search based User SessionId
                writer.WriteAttributeString("TransactionIdentifier", TransactionIdentifier);
                //POS Section
                writer.WriteStartElement("POS"); //POS start
                writer.WriteStartElement("Source"); //Source start
                writer.WriteAttributeString("ISOCurrency", currency);
                writer.WriteStartElement("RequestorID"); //RequestorID start 
                writer.WriteAttributeString("ID", propertyID);
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteStartElement("CompanyName");//CompanyName
                writer.WriteAttributeString("Code", userName);
                writer.WriteEndElement();//CompanyName
                writer.WriteEndElement();//RequestorID  
                writer.WriteEndElement();// Source
                writer.WriteEndElement();// POS
                //POS Section - end

                //UniqueID Section
                writer.WriteStartElement("UniqueID");//UniqueID
                writer.WriteAttributeString("Type", Type);
                writer.WriteAttributeString("ID", ID);
                writer.WriteEndElement();//UniqueID
                //UniqueID Section - end

                //HotelReservations Section
                writer.WriteStartElement("HotelReservations"); //HotelReservations  start 
                writer.WriteStartElement("HotelReservation"); //HotelReservation  start 

                List<HotelRoom> roomsList = new List<HotelRoom>();
                if (booking == "Provisional")
                {
                    roomsList.AddRange(itinerary.Roomtype);
                    decimal amountBeforeTaxes = 0;
                    decimal taxes = 0;
                    if (itinerary.Roomtype[0].TBOPrice != null)
                    {
                        amountBeforeTaxes = itinerary.Roomtype[0].TBOPrice.NetFare;
                        taxes = itinerary.Roomtype[0].TBOPrice.Tax;
                    }
                    string roomTypeCode = string.Empty;
                    string ratePlanCode = string.Empty;
                    string promotionCode = string.Empty;
                    string supplierCurrency = string.Empty;
                    if (roomsList.Count > 0)
                    {
                        //RoomTypeCode  =  roomsList[0].RoomTypeCode=SequenceNo|RoomTypeCode|RatePlanCode|CurrencyCode|promotionid
                        string[] RoomTypeCode = roomsList[0].RoomTypeCode.Split('|');
                        if (RoomTypeCode.Length > 3)
                        {
                            roomTypeCode = RoomTypeCode[1];
                            ratePlanCode = RoomTypeCode[2];
                            supplierCurrency = RoomTypeCode[3];
                           
                        }
                        if (RoomTypeCode.Length > 4)
                        {
                            promotionCode = RoomTypeCode[4];
                        }
                    }

                    writer.WriteStartElement("RoomStays"); //RoomStays  start 
                    writer.WriteStartElement("RoomStay"); //RoomStay  start 
                    writer.WriteAttributeString("PromotionCode", "");//promotionCode 

                    writer.WriteStartElement("RoomTypes"); //RoomTypes  start 
                    writer.WriteStartElement("RoomType"); //RoomType  start 
                    writer.WriteAttributeString("NumberOfUnits", itinerary.NoOfRooms.ToString());
                    writer.WriteAttributeString("RoomTypeCode", roomTypeCode);
                    writer.WriteEndElement();//RoomType
                    writer.WriteEndElement();//RoomTypes

                    writer.WriteStartElement("RatePlans"); //RatePlans  start 
                    writer.WriteStartElement("RatePlan"); //RatePlan  start  
                    writer.WriteAttributeString("RatePlanCode", ratePlanCode);
                    writer.WriteEndElement();//RatePlans
                    writer.WriteEndElement();//RatePlans

                    writer.WriteStartElement("GuestCounts"); //GuestCounts  start 
                    writer.WriteAttributeString("IsPerRoom", "false");

                    //For Multiple Rooms
                    for (int i = 0; i < itinerary.NoOfRooms; i++)
                    {
                        //For Adults
                        writer.WriteStartElement("GuestCount"); //GuestCount  start 
                        writer.WriteAttributeString("ResGuestRPH", i.ToString());
                        writer.WriteAttributeString("AgeQualifyingCode", "10");
                        writer.WriteAttributeString("Count", itinerary.Roomtype[i].AdultCount.ToString());
                        writer.WriteEndElement();//GuestCount
                        for (int j = 0; j < itinerary.Roomtype[i].ChildCount; j++)
                        {
                            //-- For Childs
                            int childAge = itinerary.Roomtype[i].ChildAge[j];
                            if (childAge > 2)
                            {
                                writer.WriteStartElement("GuestCount"); //GuestCount  start 
                                writer.WriteAttributeString("ResGuestRPH", i.ToString());
                                writer.WriteAttributeString("Age", childAge.ToString());
                                writer.WriteAttributeString("AgeQualifyingCode", "8");
                                writer.WriteAttributeString("Count", "1");
                                writer.WriteEndElement();//GuestCount
                            }
                        }
                    }
                    writer.WriteEndElement();//GuestCounts

                    writer.WriteStartElement("TimeSpan"); //TimeSpan  start 
                    writer.WriteAttributeString("End", itinerary.EndDate.ToString("yyyy-MM-dd"));
                    writer.WriteAttributeString("Start", itinerary.StartDate.ToString("yyyy-MM-dd"));
                    writer.WriteEndElement();//TimeSpan

                    writer.WriteStartElement("Total"); //Total  start 
                    writer.WriteAttributeString("AmountBeforeTax", amountBeforeTaxes.ToString());
                    writer.WriteAttributeString("CurrencyCode", "INR");
                    writer.WriteStartElement("Taxes"); //Taxes  start 
                    writer.WriteAttributeString("Amount", taxes.ToString());
                    writer.WriteEndElement();//Taxes
                    writer.WriteEndElement();//Total

                    writer.WriteStartElement("BasicPropertyInfo"); //BasicPropertyInfo  start 
                    writer.WriteAttributeString("HotelCode", itinerary.HotelCode);
                    writer.WriteEndElement();//BasicPropertyInfo
                    if (roomsList[0].PassenegerInfo[0].IsGST)
                    {
                        writer.WriteStartElement("TPA_Extensions"); //TPA_Extensions  start 
                        writer.WriteStartElement("CustomerGST"); //CustomerGST  start 
                        writer.WriteElementString("GSTNumber", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTNumber);//GSTNumber start   //GSTNumber 
                        writer.WriteElementString("GSTCompanyName", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyName);//GSTCompanyName start   //GSTCompanyName 
                        writer.WriteElementString("GSTCompanyEmailId", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyEmailId);//GSTCompanyEmailId start   //GSTCompanyEmailId 
                        writer.WriteElementString("GSTCompanyAddress", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyAddress);//GSTCompanyAddress start   //GSTCompanyAddress 
                        writer.WriteElementString("GSTCity", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCity);//GSTCity start   //GSTCity 
                        writer.WriteElementString("GSTPinCode", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTPinCode);//GSTPinCode start   //GSTPinCode 
                        writer.WriteElementString("GSTState", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTState);//GSTState start   //GSTState 
                        writer.WriteElementString("GSTPhoneISD", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTPhoneISD);//GSTPhoneISD start   //GSTPhoneISD 
                        writer.WriteElementString("GSTPhoneNumber", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTPhoneNumber);//GSTPhoneNumber start   //GSTPhoneNumber 
                        writer.WriteElementString("CustomerName", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCustomerName);//CustomerName start   //CustomerName 
                        writer.WriteElementString("CustomerAddress", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCustomerAddress);//CustomerAddress start   //CustomerAddress 
                        writer.WriteElementString("CustomerState", roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCustomerState);//CustomerState start   //CustomerState 
                        writer.WriteEndElement();//CustomerGST
                        writer.WriteEndElement();//TPA_Extensions
                    }
                   

                    writer.WriteStartElement("Comments"); //Comments  start 
                    writer.WriteStartElement("Comment"); //Comment  start 
                    writer.WriteElementString("Text", "");//Text start   //Text 
                    writer.WriteEndElement();//Comment
                    writer.WriteEndElement();//Comments

                    writer.WriteEndElement();//RoomStay
                    writer.WriteEndElement();//GuestCounts 

                    writer.WriteStartElement("ResGuests"); //ResGuests  start 
                    writer.WriteStartElement("ResGuest"); //ResGuest  start 
                    writer.WriteStartElement("Profiles"); //Profiles  start 
                    writer.WriteStartElement("ProfileInfo"); //ProfileInfo  start 
                    writer.WriteStartElement("Profile"); //Profile  start 
                    writer.WriteAttributeString("ProfileType", "1");
                    writer.WriteStartElement("Customer"); //Customer  start 

                    writer.WriteStartElement("PersonName"); //PersonName  start 
                    writer.WriteElementString("GivenName", roomsList[0].PassenegerInfo[0].Lastname);//Mandetory
                    writer.WriteElementString("MiddleName", "");
                    writer.WriteElementString("Surname", roomsList[0].PassenegerInfo[0].Firstname);//Mandetory
                    writer.WriteEndElement();//PersonName
                    writer.WriteStartElement("Telephone"); //Telephone  start 
                    writer.WriteAttributeString("CountryAccessCode", "0091");// roomsList[0].PassenegerInfo[0].Countrycode);
                    writer.WriteAttributeString("Extension", "0");
                    writer.WriteAttributeString("AreaCityCode", "91");// roomsList[0].PassenegerInfo[0].Areacode);
                    writer.WriteAttributeString("PhoneNumber", "9136056109");//  roomsList[0].PassenegerInfo[0].Phoneno);
                    writer.WriteAttributeString("PhoneTechType", "1");
                    writer.WriteEndElement();//Telephone

                    writer.WriteElementString("Email", roomsList[0].PassenegerInfo[0].Email); //Email  start  //Email
                    writer.WriteStartElement("Address"); //Address  start 
                    writer.WriteElementString("AddressLine", "Delhi");// roomsList[0].PassenegerInfo[0].Addressline1);
                    //writer.WriteAttributeString("AddressLine", "");
                    writer.WriteElementString("CityName", "Delhi");// roomsList[0].PassenegerInfo[0].City);
                    writer.WriteElementString("PostalCode", "110041");// roomsList[0].PassenegerInfo[0].Zipcode);
                    writer.WriteElementString("StateProv", "Delhi");// roomsList[0].PassenegerInfo[0].State);
                    writer.WriteElementString("CountryName", "India");// roomsList[0].PassenegerInfo[0].Country);
                    writer.WriteEndElement();//Address

                    writer.WriteEndElement();//Customer
                    writer.WriteEndElement();//Profile
                    writer.WriteEndElement();//ProfileInfo
                    writer.WriteEndElement();//Profiles
                    writer.WriteEndElement();//ResGuest 
                    writer.WriteEndElement();//ResGuests 
                }

                writer.WriteStartElement("ResGlobalInfo"); //ResGlobalInfo  start 
                writer.WriteStartElement("Guarantee"); //Guarantee  start 
                writer.WriteAttributeString("GuaranteeType", "PrePay");
                writer.WriteEndElement();//Guarantee 
                writer.WriteEndElement();//ResGlobalInfo 
                writer.WriteEndElement();//HotelReservation 
                writer.WriteEndElement();//HotelReservations 
                //HotelReservations Section - end               

                writer.WriteEndElement();//OTA_HotelResRQ 
                writer.WriteEndElement();//Soap:Body
                writer.WriteEndElement();//Soap:Envelope

                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Yatra: Failed generate " + booking + " request for Hotel : " + itinerary.HotelName, ex);
            }

            return requestMsg.ToString();
        }
        /// <summary>
        ///  ReadResponseProvisionalBooking
        /// </summary>
        /// <param name="xmlDoc">ProvisionalBooking xml Response Object </param>
        /// <param name="Type">Provisional booking type Provided by supplier </param>
        /// <param name="ID">Provisional booking ID Provided by supplier</param>
        /// <returns>string</returns>
        private void ReadResponseProvisionalBooking(XmlDocument xmlDoc, ref string Type, ref string ID)
        {
            try
            {
                try
                {
                    string filePath = XmlPath + appUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ProvisionalBookingResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

                #region Error from API
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelResRS/ns1:Errors/ns1:Error", nsmgr);

                if (ErrorInfo != null)
                {
                    XmlAttribute ErrorCode = ErrorInfo.Attributes["Code"];
                    if (!string.IsNullOrEmpty(ErrorCode.Value))
                    {
                        Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                        {
                    " Yatra:GenerateProvisionalBookingResult,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                        }), "");
                        throw new BookingEngineException("<br>" + ErrorMessage(ErrorCode.Value));
                    }
                }
                #endregion

                XmlNode provisionalRes = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelResRS/ns1:HotelReservations/ns1:HotelReservation/ns1:UniqueID", nsmgr);

                if (provisionalRes != null)
                {
                    XmlAttribute xmlAttrbuteType = provisionalRes.Attributes["Type"];
                    if (xmlAttrbuteType != null)
                    {
                        Type = xmlAttrbuteType.Value;
                    }
                    XmlAttribute xmlAttrbuteID = provisionalRes.Attributes["ID"];
                    if (xmlAttrbuteID != null)
                    {
                        ID = xmlAttrbuteID.Value;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Yatra: Failed to Read provisional booking response ", ex);
            }
        }
        /// <summary>
        /// ReadBookingResponse
        /// </summary>
        /// <param name="xmlDoc">Booking response xml</param>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>Here Reading booking response object
        /// here only we need to get confirmationNo and BookingId
        /// </remarks>
        private BookingResponse ReadResponseBooking(XmlDocument xmlDoc, HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                try
                {
                    string filePath = XmlPath + appUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_FinalBookingResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

                #region Error from API
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:Errors/ns1:Error", nsmgr);

                if (ErrorInfo != null)
                {
                    XmlAttribute ErrorCode = ErrorInfo.Attributes["Code"];
                    if (!string.IsNullOrEmpty(ErrorCode.Value))
                    {
                        Audit.Add(EventType.YatraAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                        {
                    " Yatra:ReadResponseBookingResult,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                        }), "");
                        throw new BookingEngineException("<br>" + ErrorMessage(ErrorCode.Value));
                    }

                }
                #endregion
                XmlNode finalRes = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelResRS/ns1:HotelReservations/ns1:HotelReservation/ns1:UniqueID", nsmgr);

                if (finalRes != null)
                {

                    XmlAttribute id = finalRes.Attributes["ID"];
                    if (id != null)
                    {
                        itinerary.ConfirmationNo = id.Value;
                    }

                    bookResponse.ConfirmationNo = itinerary.ConfirmationNo;
                    bookResponse.Status = BookingResponseStatus.Successful;
                    itinerary.Status = HotelBookingStatus.Confirmed;
                    itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: Yatra as per final booking form Confirmation No: " + itinerary.ConfirmationNo;
                }
                else
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Yatra: Failed to Read booking response "+ex, ex);
            }
            return bookResponse;
        }
        #endregion

        #region cancel Booking
        /// <summary>
        /// CancelHotelBooking
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Preparing CancelHotelBooking request object and sending CancelHotelBooking to api</remarks>
        public Dictionary<string, string> CancelHotelBooking(HotelItinerary itinerary)
        {
            Dictionary<string, string> cancelRes = new Dictionary<string, string>();
            //Initiate Cancel Req Generation
            string request = GenerateCancelRequest(itinerary, "Initiate");
            //request = request.Replace("<CompanyName />", "<CompanyName Code=" + "\"" + userName + "\" >" + "</CompanyName>");
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = XmlPath + appUserId + "_" + itinerary.ConfirmationNo + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_InitiateCancellationRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlDocument xmlRes = new XmlDocument();
            try
            {
                //Get Initiate Cancel Response
                xmlRes = SendRequest(request, cancellationReqURL, "Initiate Cancellation");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraCancel, Severity.High, (int)appUserId, "Exception returned from Yatra.CancelHotelBooking Initiate Cancellation Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlRes != null && xmlRes.ChildNodes != null && xmlRes.ChildNodes.Count > 0)
                {
                    cancelRes = ReadPreCancelResponse(itinerary, xmlRes);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraCancel, Severity.High, (int)appUserId, "Exception returned from Yatra.ReadPreCancelResponse Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancelRes;
        }
        /// <summary>
        /// GenerateCanceRequest
        /// </summary>
        /// <param name="itinerary">HotelItinerary object</param> 
        /// <param name="cancelType">Cancellation Type (Initiate/Cancel)</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenerateCancelRequest(HotelItinerary itinerary, string cancelType)
        {
            StringBuilder requestMsg = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                writer.WriteStartElement("soapenv", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");////soapenv:Envelope  start
                writer.WriteAttributeString("xmlns", "ns", null, "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("soapenv", "Header", null);  //soapenv:Header start
                writer.WriteEndElement();//soapenv:Header end
                writer.WriteStartElement("soapenv", "Body", null); //soapenv:Body  start
                writer.WriteStartElement("ns", "OTA_CancelRQ", null); //OTA_CancelRQ start  
                writer.WriteAttributeString("CancelType", cancelType);
                writer.WriteAttributeString("Version", "1.0");

                //POS Section
                writer.WriteStartElement("ns", "POS", null); //POS start
                writer.WriteStartElement("ns", "Source", null); //Source start 
                writer.WriteStartElement("ns", "RequestorID", null); //RequestorID start 
                writer.WriteAttributeString("ID", propertyID);
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteStartElement("ns", "CompanyName", null);//CompanyName
                writer.WriteAttributeString("Code", userName);
                writer.WriteEndElement();//CompanyName
                writer.WriteEndElement();//RequestorID  -- end
                writer.WriteEndElement();// Source
                writer.WriteEndElement();// POS
                //POS Section - end

                //Unique Id Section  -- start
                writer.WriteStartElement("ns", "UniqueID", null); //UniqueID start 
                writer.WriteAttributeString("ID", itinerary.ConfirmationNo);
                writer.WriteEndElement();//UniqueID  -- end
                // Unique Id section  --  end

                //Verification Section  -- start  
                writer.WriteStartElement("ns", "Verification", null); //Verification start
                writer.WriteStartElement("ns", "PersonName", null); //PersonName start  
                writer.WriteElementString("ns", "Surname", null, itinerary.HotelPassenger.Firstname);//todo
                writer.WriteEndElement();// PersonName
                writer.WriteElementString("ns", "Email", null, itinerary.HotelPassenger.Email);//"techteam@Yatra.com"
                writer.WriteEndElement();// Verification                
                // Verification section  --  end 

                //TPA_Extensions Section  -- start  
                writer.WriteStartElement("ns", "TPA_Extensions", null); //TPA_Extensions start
                //writer.WriteStartElement("ns", "CancelDates", null); //CancelDates start
                //For Cancellation dates
                for (int i = 0; i < itinerary.EndDate.Subtract(itinerary.StartDate).Days; i++)
                {
                    writer.WriteElementString("ns", "CancelDates", null, itinerary.StartDate.AddDays(i).ToString("yyyy/dd/MM"));
                }
                // writer.WriteEndElement();// CancelDates    
                writer.WriteEndElement();// TPA_Extensions                
                // TPA_Extensions section  --  end

                writer.WriteEndElement();//OTA_CancelRQ  -- end
                writer.WriteEndElement();//soapenv:Body  -- end
                writer.WriteEndElement();//soapenv:Envelope  -- end

                writer.Flush();
                writer.Close();

            }
            catch (Exception ex)
            {
                cancelType = cancelType == "Initiace" ? "Initiate Cancellation" : "Confirm Cancellation";
                throw new Exception("Yatra: Failed generate " + cancelType + " Request info request for ConfirmationNo : " + itinerary.ConfirmationNo, ex);
            }
            return requestMsg.ToString();
        }
        /// <summary>
        /// ReadPreCanceResponse
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <param name="xmlDoc">PreCanceResponse xml</param>
        /// <returns>Dictionary</returns>
        /// <remarks> call cancel status method</remarks>
        private Dictionary<string, string> ReadPreCancelResponse(HotelItinerary itinerary, XmlDocument xmlDoc)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            XmlNode tempNode;
            try
            {
                string filePath = XmlPath + appUserId + "_" + itinerary.ConfirmationNo + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_InitiateCancellationResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            #region Error from API
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_CancelRS/ns1:Errors/ns1:Error", nsmgr);

            if (ErrorInfo != null)
            {
                XmlAttribute ErrorCode = ErrorInfo.Attributes["Code"];
                if (!string.IsNullOrEmpty(ErrorCode.Value))
                {
                    Audit.Add(EventType.YatraCancel, Severity.High, (int)appUserId, string.Concat(new object[]
                    {
                    "Error Returned from Yatra. Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                    }), "");
                    throw new BookingEngineException("<br>" + ErrorMessage(ErrorCode.Value));
                }

            }
            #endregion
            tempNode = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_CancelRS", nsmgr).Attributes["Status"];
            if (tempNode != null && tempNode.InnerText == "PendingCancellation")// Status PendingCancellation Means Response Success for Initiate Cancellation
            {
                string bookingRefNo = string.Empty;

                string cancelToken = string.Empty;
                decimal cancelCost = 0;
                string currencyCode = string.Empty;
                //CancelRules Node
                XmlNode CancellationAmountSummary = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_CancelRS/ns1:CancelInfoRS/ns1:CancelRules", nsmgr);
                if (CancellationAmountSummary != null)
                {
                    bookingRefNo = itinerary.ConfirmationNo;
                    foreach (XmlNode RefundRateInclusive in CancellationAmountSummary)
                    {
                        //RefundRateInclusive  -- CancelRule Node  Day wise and Amount Attribute
                        if (RefundRateInclusive != null)
                        {
                            XmlAttribute amount = RefundRateInclusive.Attributes["Amount"];
                            if (amount != null)
                            {
                                cancelCost += Convert.ToDecimal(amount.Value);
                            }
                            XmlAttribute CurrencyCode = RefundRateInclusive.Attributes["CurrencyCode"];
                            if (CurrencyCode != null)
                            {
                                currencyCode = CurrencyCode.Value;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(bookingRefNo) && cancelCost >= 0)
                    {
                        //Here Calling CancelStatus and get cancellation number
                        cancelToken = GetCancellationNumber(itinerary);
                        if (!string.IsNullOrEmpty(cancelToken))
                        {
                            cancellationCharges.Add("Currency", currencyCode);
                            decimal refundAmount = (cancelCost);
                            cancellationCharges.Add("Amount", Convert.ToString(refundAmount));
                            cancellationCharges.Add("Status", "Cancelled");
                            cancellationCharges.Add("ID", cancelToken);
                        }
                        else
                        {
                            cancellationCharges.Add("Status", "Failed");
                        }
                    }
                    else
                    {
                        cancellationCharges.Add("Status", "Failed");
                    }
                }
            }
            return cancellationCharges;

        }
        /// <summary>
        /// GetCancellationNumber
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param> 
        /// <returns>string (cancellation number)</returns>
        /// <remarks>Here we are preparing CancelStatus request object and sending CancelStatus to api</remarks>
        private string GetCancellationNumber(HotelItinerary itinerary)
        {
            string cancelToken = string.Empty;
            //Confirm Cancel Req Generation
            string request = GenerateCancelRequest(itinerary, "Cancel");
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = XmlPath + appUserId + "_" + itinerary.ConfirmationNo + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_ConfirmCancellationRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlDocument xmlRes = new XmlDocument();
            try
            {
                //Get Confirm Cancel Response
                xmlRes = SendRequest(request, cancellationReqURL, "Confirm Cancellation");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraCancel, Severity.High, (int)appUserId, "Exception returned from Yatra.CancelHotelBooking Confirm Cancellation Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlRes != null && xmlRes.ChildNodes != null && xmlRes.ChildNodes.Count > 0)
                {
                    cancelToken = ReadCancelResponse(itinerary, xmlRes);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraCancel, Severity.High, 0, "Exception returned from Yatra.GetCancelStatus Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancelToken;
        }
        /// <summary>
        /// ReadCancelResponse
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <param name="xmlDoc">cancelReponse xml</param>
        /// <returns>string (Cancellation Number)</returns>
        /// <remarks>Here checking status if status is true booking cancelled else booking not cancelled</remarks>
        private string ReadCancelResponse(HotelItinerary itinerary, XmlDocument xmlDoc)
        {
            string cancellationNumber = string.Empty;
            try
            {
                string filePath = XmlPath + appUserId + "_" + itinerary.ConfirmationNo + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_ConfirmCancellationResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            XmlNode tempNode = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_CancelRS", nsmgr).Attributes["Status"];

            if (tempNode != null && tempNode.InnerText == "Cancelled") //Here Status 200 Means Response Success
            {
                XmlAttribute UniqueID = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_CancelRS/ns1:CancelInfoRS/ns1:UniqueID", nsmgr).Attributes["ID"];
                if (UniqueID != null)
                {
                    cancellationNumber = UniqueID.Value;
                }
            }
            else
            {
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_CancelRS/ns1:Errors/ns1:Error", nsmgr);

                if (ErrorInfo != null)
                {
                    XmlAttribute ErrorCode = ErrorInfo.Attributes["Code"];
                    if (!string.IsNullOrEmpty(ErrorCode.Value))
                    {
                        Audit.Add(EventType.YatraCancel, Severity.High, (int)appUserId, string.Concat(new object[]
                        {
                    "Error Returned from Yatra. Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                        }), "");
                        throw new BookingEngineException("<br>" + ErrorMessage(ErrorCode.Value));
                    }

                }
            }
            return cancellationNumber;
        }
        #endregion

        #region Get Hotel Static Data
        /// <summary>
        /// GetHotelDetailsInformation
        /// </summary>
        /// <param name="cityCode">HotelCityCode</param>
        /// <param name="hotelName">HotelName</param>
        /// <param name="hotelCode">HotelCode</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Every 15days (configuraed in App.config -- Yatra.config.xml) Need to download StaticData
        /// here we are checking if static data is not there our DB we are sending static data request and get the response save our database
        /// </remarks>
        public HotelDetails GetHotelDetailsInformation(string cityCode, string hotelName, string hotelCode)
        {
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData staticInfo = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();

            //get the info whether to update the static data
            int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.YatraConfig["TimeStamp"]);
            bool imageUpdate = true;
            bool isUpdateReq = false;
            bool isReqSend = false;

            string request = string.Empty;
            string resp = string.Empty;

            try
            {
                staticInfo.Load(hotelCode, cityCode, HotelBookingSource.Yatra);
                imageInfo.Load(hotelCode, cityCode, HotelBookingSource.Yatra);
                if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                {
                    imageUpdate = false;
                }
                if (staticInfo.HotelName != null && staticInfo.HotelName.Length > 0 && !string.IsNullOrEmpty(staticInfo.HotelDescription) && !string.IsNullOrEmpty(staticInfo.HotelAddress))
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(staticInfo.TimeStamp);
                    //if it is less than REquired time stamp days then load from DB
                    //TO DO 
                    //if (diffRes.Days > timeStampDays)
                    //{
                    //    // Set the variable as true
                    //    isUpdateReq = true;
                    //    imageUpdate = true;
                    //}
                    //else
                    {
                        hotelInfo.HotelCode = staticInfo.HotelCode;
                        hotelInfo.HotelName = staticInfo.HotelName;
                        hotelInfo.Map = staticInfo.HotelMap;
                        hotelInfo.Description = staticInfo.HotelDescription;
                        hotelInfo.HotelPolicy = staticInfo.HotelPolicy;

                        string[] image = new string[0];
                        List<string> dldImgList = new List<string>();
                        {
                            string[] dldImages = imageInfo.Images.Split('|');
                            foreach (string img in dldImages)
                            {
                                if (!string.IsNullOrEmpty(img)) dldImgList.Add(img);
                            }
                        }
                        hotelInfo.Images = dldImgList;
                        //hotelInfo.Image = dldImgList.Count > 0 ? dldImgList[0] : "";
                        hotelInfo.Address = staticInfo.HotelAddress;
                        hotelInfo.PhoneNumber = staticInfo.PhoneNumber;
                        hotelInfo.PinCode = staticInfo.PinCode;
                        hotelInfo.Email = staticInfo.EMail;
                        hotelInfo.URL = staticInfo.URL;
                        hotelInfo.FaxNumber = staticInfo.FaxNumber;
                        Dictionary<string, string> attractions = new Dictionary<string, string>();
                        hotelInfo.Attractions = attractions;
                        List<string> facilities = new List<string>();
                        List<string> roomFacilities = new List<string>();
                        if (!string.IsNullOrEmpty(staticInfo.HotelFacilities))
                        {
                            string[] hotelFacility = staticInfo.HotelFacilities.Split('$');

                            string[] facilList = hotelFacility[0].Split('|');
                            foreach (string facl in facilList)
                            {
                                facilities.Add(facl);
                            }
                            if (hotelFacility.Length > 1)
                            {
                                string[] roomFacilList = hotelFacility[1].Split('|');
                                foreach (string roomFacl in roomFacilList)
                                {
                                    roomFacilities.Add(roomFacl);
                                }
                            }
                        }
                        hotelInfo.HotelFacilities = facilities;
                        hotelInfo.RoomFacilities = roomFacilities;
                        List<string> locations = new List<string>();
                        string[] locationList = staticInfo.HotelLocation.Split('|');
                        foreach (string loc in locationList)
                        {
                            locations.Add(loc);
                        }
                        hotelInfo.Location = locations;
                    }
                }
                else
                {
                    isReqSend = true;
                }
                if (isReqSend || isUpdateReq)
                {
                    // Saving in Cache
                    if (hotelInfo.HotelName == null)
                    {
                        return hotelInfo;
                    }
                    else
                    {
                        staticInfo.HotelCode = hotelCode;
                        staticInfo.HotelName = hotelInfo.HotelName;
                        staticInfo.HotelAddress = hotelInfo.Address;
                        staticInfo.CityCode = cityCode;
                        List<string> facilities = hotelInfo.HotelFacilities;
                        string facilty = string.Empty;
                        foreach (string facl in facilities)
                        {
                            facilty += facl + "|";
                        }
                        List<string> roomFacilities = hotelInfo.RoomFacilities;
                        string roomFacilty = string.Empty;
                        foreach (string roomFacl in roomFacilities)
                        {
                            roomFacilty += roomFacl + "|";
                        }
                        staticInfo.HotelFacilities = facilty + "$" + roomFacilty;
                        staticInfo.SpecialAttraction = string.Empty;

                        List<string> locations2 = hotelInfo.Location;
                        string location = string.Empty;
                        foreach (string loc in locations2)
                        {
                            location = location + loc + "|";
                        }
                        staticInfo.HotelLocation = location;
                        staticInfo.HotelMap = hotelInfo.Map;
                        staticInfo.HotelDescription = hotelInfo.Description;
                        staticInfo.FaxNumber = hotelInfo.FaxNumber;
                        staticInfo.EMail = hotelInfo.Email;
                        staticInfo.PinCode = hotelInfo.PinCode.ToString();
                        staticInfo.PhoneNumber = hotelInfo.PhoneNumber;
                        staticInfo.URL = hotelInfo.URL;
                        staticInfo.Source = HotelBookingSource.Agoda;
                        staticInfo.Rating = hotelInfo.hotelRating;
                        staticInfo.HotelPolicy = hotelInfo.HotelPolicy;
                        staticInfo.Status = true;
                        staticInfo.Save();
                        //images
                        List<string> imgList = hotelInfo.Images;
                        string images = string.Empty;
                        foreach (string img in imgList)
                        {
                            images += img + "|";
                        }
                        imageInfo.Images = images;
                        imageInfo.CityCode = cityCode;
                        imageInfo.HotelCode = hotelCode;
                        imageInfo.Source = HotelBookingSource.Yatra;
                        imageInfo.DownloadedImgs = images;

                        if (isUpdateReq)
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Update();
                            }
                        }
                        else
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Save();
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.YatraStatic, Severity.High, (int)appUserId, "Exception returned from Agoda.HotelStatic Error Message:" + ex.Message + " | " + DateTime.Now, "");
            }
            return hotelInfo;
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }
        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~Yatra()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }
        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}

