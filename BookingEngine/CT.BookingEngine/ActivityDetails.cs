﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Collections.Generic;

/// <summary>
/// Summary description for ActivityDetails
/// </summary>
namespace CT.BookingEngine
{
    public class ActivityDetails
    {
        //static string masterDB = System.Configuration.ConfigurationSettings.AppSettings["MasterDB"].ToString();
        //static string masterDBagentId = System.Configuration.ConfigurationSettings.AppSettings["MasterDBAgentId"].ToString();
        long _activityId;
        int _agencyId;
        string _activityName;
        decimal _amount;
        string _image1;
        string _overview;
        string _inclusions;
        string _exclusions;
        string _itinerary1;
        string _itinerary2;
        string _cancellPolicy;
        string _details;
        public static Dictionary<string, decimal> ExchangeRates;
        public static string AgentCurrency;
        public long ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }
        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public string Image1
        {
            get { return _image1; }
            set { _image1 = value; }
        }
        public string Overview
        {
            get { return _overview; }
            set { _overview = value; }
        }
        public string Inclusions
        {
            get { return _inclusions; }
            set { _inclusions = value; }
        }

        public string Exclusions
        {
            get { return _exclusions; }
            set { _exclusions = value; }
        }
        public string Itinerary1
        {
            get { return _itinerary1; }
            set { _itinerary1 = value; }
        }
        public string Itinerary2
        {
            get { return _itinerary2; }
            set { _itinerary2 = value; }
        }
        public string CancellPolicy
        {
            get { return _cancellPolicy; }
            set { _cancellPolicy = value; }
        }
        public string Details
        {
            get { return _details; }
            set { _details = value; }
        }
        public ActivityDetails()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public static DataTable GetActivities(long agencyId, string isFixedDeparture)
        {
            string supplierCurrency = "AED";
            decimal rateOfExchange = (ExchangeRates.ContainsKey(supplierCurrency) ? ExchangeRates[supplierCurrency] : 1);
            DataTable dtMarkup;
            if (isFixedDeparture != "Y")
            {
                dtMarkup = CT.BookingEngine.UpdateMarkup.Load((int)agencyId, string.Empty, (int)ProductType.Activity);
            }
            else
            {
                dtMarkup = CT.BookingEngine.UpdateMarkup.Load((int)agencyId, string.Empty, (int)ProductType.FixedDeparture);
            }
            DataView dv = dtMarkup.DefaultView;
            dv.RowFilter = "TransType IN('B2B')";
            dtMarkup = dv.ToTable();
            decimal markup = 0; string markupType = string.Empty;
            if (dtMarkup != null && dtMarkup.Rows.Count > 0)
            {
                markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
            }
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            decimal sourceamount;
            decimal markupValue;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[2];
                   // paramList[0] = new SqlParameter("@ActivityStatus", status);
                    paramList[0] = new SqlParameter("@agencyId", agencyId);
                    paramList[1] = new SqlParameter("@ActivityStatus", isFixedDeparture);
                    data = DBGateway.ExecuteReaderSP("usp_GetActivities", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                }
                catch
                {
                    throw;
                }
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                //foreach (System.Data.DataColumn col in dt.Columns) col.ReadOnly = false; 
                foreach (DataRow dr in dt.Rows)
                {
                    dt.Columns["Amount"].ReadOnly = false;
                    sourceamount = Convert.ToDecimal(dr["Amount"]) * rateOfExchange;
                    markupValue = ((markupType == "F") ? markup : (Convert.ToDecimal(sourceamount) * (markup / 100)));
                    dr["Amount"] = sourceamount + markupValue;
                }
            }
            return dt;
        }

        public DataTable LoadCity(string isFixedDeparture,int agencyId)
        {
            DataTable dtCities = new DataTable();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@agencyid", agencyId);
                    paramList[1] = new SqlParameter("@IsFixedDeparture", isFixedDeparture);
                    data = DBGateway.ExecuteReaderSP("usp_GetActivityCityInfo", paramList, connection);
                    dtCities.Load(data);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return dtCities;
        }


        public DataTable LoadTheme(string themeType)
        {
            DataTable themeList = new DataTable();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@themeType", themeType);
                    data = DBGateway.ExecuteReaderSP("usp_GetActivityThemeInfo", paramList, connection);
                    themeList.Load(data);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return themeList;
        }

        public List<string> LoadDuration(string isFixedDeparture, int agencyId)
        {
            List<string> duration = new List<string>();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@agencyid", agencyId);
                    paramList[1] = new SqlParameter("@IsFixedDeparture", isFixedDeparture);
                    data = DBGateway.ExecuteReaderSP("usp_GetActivityDurationInfo", paramList, connection);
                    while (data.Read())
                    {
                        duration.Add((string)data["durationHours"]);

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return duration;
        }

        public DataTable LoadCountry(string isFixedDeparture,int agencyId)
        {
            DataTable dtCountries = new DataTable();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@agencyid", agencyId);
                    paramList[1] = new SqlParameter("@IsFixedDeparture", isFixedDeparture);
                    data = DBGateway.ExecuteReaderSP("usp_GetActivityCountryInfo", paramList, connection);
                    dtCountries.Load(data);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return dtCountries;
        }
        public  DataTable GetCity()
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[0];
                    data = DBGateway.ExecuteReaderSP("usp_GetCity", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public  DataTable GetCountry()
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[0];
                    data = DBGateway.ExecuteReaderSP( "usp_GetCountries", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public  DataSet LoadCountryCity()
        {
            DataSet ds = new DataSet();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[0];
                    //data = DBGateway.ExecuteReaderSP(SPNames.GetActivityCountryCityInfo, paramList, connection);
                    //ds = DBGateway.FillSP(SPNames.GetActivityCountryCityInfo, paramList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
               
            }
            return ds;
        }

        public static ActivityDetails GetActivityDetailById(int id)
        {
            ActivityDetails objActivity = new ActivityDetails();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@ActivityId", id);

                    data = DBGateway.ExecuteReaderSP("usp_GetActivityDetailById", paramList, connection);
                    if (data.Read())
                    {
                       
                        objActivity._activityId = Convert.ToInt32(data["activityId"]);
                        objActivity._agencyId = Convert.ToInt32(data["agencyId"]);
                        objActivity._activityName = Convert.ToString(data["activityName"]);
                        objActivity._amount = Convert.ToDecimal(data["Amount"]);
                        objActivity._image1 = Convert.ToString(data["imagePath1"]);
                        objActivity._overview = Convert.ToString(data["overview"]);
                        objActivity._inclusions = Convert.ToString(data["inclusions"]);
                        objActivity._exclusions = Convert.ToString(data["exclusions"]);
                        objActivity._itinerary1 = Convert.ToString(data["itinerary1"]);
                        objActivity._itinerary2 = Convert.ToString(data["itinerary2"]);
                        objActivity._cancellPolicy = Convert.ToString(data["cancellPolicy"]);
                        objActivity._details = Convert.ToString(data["details"]);
                        //templist.Add(objActivity);
                     }
                }
                catch
                {
                    throw;
                }
            }
            return objActivity;
        }

        public static DataTable GetActivityTransactionDetail(long activityId)
        {
            DataTable themeList = new DataTable();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@ATHId", activityId);
                    data = DBGateway.ExecuteReaderSP("usp_GetActivityTransactionPrice", paramList, connection);
                    themeList.Load(data);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return themeList;
        }

        public static DataTable GetPriceActivityIds(DateTime fromDate, DateTime toDate)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@FROMDATE", fromDate);
                    paramList[1] = new SqlParameter("@TODATE", toDate);
                    data = DBGateway.ExecuteReaderSP("usp_GetTourPriceActivity", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        public DataTable LoadRegion(string isFixedDeparture,int agencyId)
        {
            DataTable dtRegions = new DataTable();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@agencyid", agencyId);
                    paramList[1] = new SqlParameter("@IsFixedDeparture", isFixedDeparture);
                    data = DBGateway.ExecuteReaderSP("usp_GetActivityRegionInfo", paramList, connection);
                    dtRegions.Load(data);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return dtRegions;
        }


    }
}
