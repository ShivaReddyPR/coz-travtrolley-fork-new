using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    [Serializable]
    public class HotelRoom
    {
        int roomId;
        int hotelId;
        string roomTypeCode;
        string ratePlanCode;
        string ratePlanId;// new property to store rateBases /rateBasis id for blocking rooms cross checking --added by ziyad 08-feb-2013
        string noOfUnits;
        int adultCount;
        int childCount; // - Optional
        List<int> childAge; // It will contain Child Age 
        string roomName;
        List<string> ameneties;
        int priceId;
        HotelRoomFareBreakDown[] roomFareBreakDown;
        decimal extraGuestCharge;
        decimal childCharge;
        bool extraBed;
        int noOfExtraBed;
        int noOfCots;
        bool sharingBed;
        string roomwiseConfirmNo;
        string roomwiseConfirmStatus;
        string roomCancelNo;
        string roomwiseCancelStatus;
        

        string mealPlanDesc = string.Empty;
        List<HotelPassenger> paasengerInfo;
        List<HotelTaxBreakup> taxInfo;
        string gxsupplier;
        /*************************************
         * VERSION 2 inclusions of DOTW      *
         * ***********************************/
        int roomCapacityInfo;
        string essentialInformation;//Specifically used for GTA API
        /// <summary>
        /// Encapsulates details about the maximum capacity of the room. 
        /// </summary>
        /// 
        #region Specifically used for TBOHotel API
        string smokingPreference;
        PriceAccounts tboPrice;
        int roomIndex;
        string supplements; // Using For storing Bookiing Identifier Details in Miki
        string bedTypeCode;
        //Agoda purpose only we are using
        List<Surcharges> surChargeList;
        List<RoomTaxBreakup> taxBreakUps;
        string allocType;//used for HIS
        string sellType;//used for HIS
        int rqId;//used for HIS
        #endregion
        public int RoomCapacityInfo
        {
            get { return roomCapacityInfo; }
            set { roomCapacityInfo = value; }
        }
        int roomPaxCapacity;

        /// <summary>
        ///  Maximum number of guests for this room (adults children) 
        /// </summary>
        public int RoomPaxCapacity
        {
            get { return roomPaxCapacity; }
            set { roomPaxCapacity = value; }
        }
        int allowedAdultsWithoutChildren;

        /// <summary>
        /// Maximum number of adults that can book this room without any children. 
        /// </summary>
        public int AllowedAdultsWithoutChildren
        {
            get { return allowedAdultsWithoutChildren; }
            set { allowedAdultsWithoutChildren = value; }
        }
        int allowedAdultsWithChildren;

        /// <summary>
        /// Maximum number of adults that can book this room with children. 
        /// The maximum number of children allowed to book the room would be 
        /// the difference between roomPaxCapacity and allowedAdultsWithChildren. 
        /// </summary>
        public int AllowedAdultsWithChildren
        {
            get { return allowedAdultsWithChildren; }
            set { allowedAdultsWithChildren = value; }
        }
        int maxExtraBed;

        /// <summary>
        /// Specifies the maximum number of extra-beds that can be booked with this room type. 
        /// </summary>
        public int MaxExtraBed
        {
            get { return maxExtraBed; }
            set { maxExtraBed = value; }
        }

        RoomRateType rateType;

        /// <summary>
        /// Specifies the rate type. Possible values:
        /// 1 - DOTW rate type
        /// 2 - DYNAMIC DIRECT rate type
        /// 3 - DYNAMIC 3rd PARTY rate type 
        /// Please note that the blocking process guarantees the allotment 
        /// (holds inventory) only for DOTW rate types. 
        /// For the DYNAMIC rates, only the price is checked and guaranteed. 
        /// Sending the blocking request is however mandatory for both rate types. 
        /// </summary>
        public RoomRateType RateType
        {
            get { return rateType; }
            set { rateType = value; }
        }

        int currencyId;

        /// <summary>
        /// Internal code of the currency in which the prices for this rate are provided.
        /// ONLY For Non Refundable Advance Purchase Rates, 
        /// this can be a different currency than the one requested. 
        /// To see the prices in the requested currency you should look for the price elements 
        /// which clearly specify InRequestedCurrency 
        /// (totalInRequestedCurrency, 
        /// totalMinimumSellingInRequestedCurrency, 
        /// priceInRequestedCurrency, 
        /// priceMinimumSellingInRequestedCurrency) 
        /// </summary>
        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }
        string currencyCode;

        /// <summary>
        /// Attribute in rateType element.  
        /// The 3 letter code which identifies the 
        /// currency in which the rates are provided. 
        /// </summary>
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }
        string description;

        /// <summary>
        /// Attribute in rateType element.  
        /// A short text description of the rate, 
        /// indicating the type of rate 
        /// (whether it is DOTW, Dynamic or 3rd party). 
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        bool nonRefundable;

        /// <summary>
        /// Attribute in ratetype element. 
        /// If present, indicates that the rate is 
        /// Non Refundable Advance Purchase.
        /// Possible values:  yes
        /// To be able to book this rate it is mandatory 
        /// for your application to use 
        /// the savebooking and bookitinerary booking flow. 
        /// </summary>
        public bool NonRefundable
        {
            get { return nonRefundable; }
            set { nonRefundable = value; }
        }
        string notes;

        /// <summary>
        ///  Attribute in rateType element. 
        ///  If present, contains a free flow text with 
        ///  additional notes about the rate, mainly
        /// </summary>
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        string paymentMode;

        /// <summary>
        ///  When present, indicates that for this rate, a special mode of payment is required. 
        ///  If the value is CC this means the rate requires a credit card prepayment. 
        ///  Possible values:    CC - Credit Card
        ///  To be able to book this type of rate it is mandatory for your application
        ///  to use the savebooking and bookitinerary booking flow. 
        /// </summary>
        public string PaymentMode
        {
            get { return paymentMode; }
            set { paymentMode = value; }
        }
        bool allowExtraMeals;

        /// <summary>
        ///  If present, this element specifies if the parent rate basis can be booked with extra meals.
        /// </summary>
        public bool AllowExtraMeals
        {
            get { return allowExtraMeals; }
            set { allowExtraMeals = value; }
        }
        bool allowSpecialRequests;

        /// <summary>
        ///  If present, this element specifies if for this rate, special requests can be sent to the supplier. 
        ///  All of the special requests are subject to availability at check in.
        /// </summary>
        public bool AllowSpecialRequests
        {
            get { return allowSpecialRequests; }
            set { allowSpecialRequests = value; }
        }
        bool allowSpecials;

        /// <summary>
        /// Present only when the room type has a special offer. 
        /// Specifies if the room type special offer is also valid with this rate basis.
        /// </summary>
        public bool AllowSpecials
        {
            get { return allowSpecials; }
            set { allowSpecials = value; }
        }
        int passengerNamesRequiredForBooking;

        /// <summary>
        /// If present, specifies how many passenger names are required for completing the booking. 
        /// If missing, one leading passenger name is sufficient for the booking, the other passengers names being optional.
        /// </summary>
        public int PassengerNamesRequiredForBooking
        {
            get { return passengerNamesRequiredForBooking; }
            set { passengerNamesRequiredForBooking = value; }
        }

        bool ammendRestricted;

        /// <summary>
        /// If present, this element indicates that a future amendment 
        /// (using the updatebooking method) done in the time period 
        /// defined by this rule, will not be possible. Fixed Value=true.
        /// </summary>
        public bool AmmendRestricted
        {
            get { return ammendRestricted; }
            set { ammendRestricted = value; }
        }
        bool cancelRestricted;

        /// <summary>
        /// If present, this element indicates that a future cancel 
        /// (using the cancelbooking or deleteitinerary methods) 
        /// done in the time period defined by this rule, will not be possible.
        /// Fixed Value=true.
        /// </summary>
        public bool CancelRestricted
        {
            get { return cancelRestricted; }
            set { cancelRestricted = value; }
        }
        bool noShowPolicy;

        /// <summary>
        /// If present, this element indicates that the presented charge is a no show charge. 
        /// In this case the elements fromDate, fromDateDetails, toDate, toDateDetails, 
        /// amendingPossible will be absent. Fixed Value = true.
        /// </summary>
        public bool NoShowPolicy
        {
            get { return noShowPolicy; }
            set { noShowPolicy = value; }
        }

        bool includedAdditionalService;

        public bool IncludedAdditionalService
        {
            get { return includedAdditionalService; }
            set { includedAdditionalService = value; }
        }
        /************************************
         *      End VERSION 2 Changes       *
         * **********************************/

        /// <summary>
        /// Price detail of one hotel booking
        /// </summary>
        PriceAccounts price;
        decimal previousFare;

        public int RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }

        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }

        public string RoomTypeCode
        {
            get { return roomTypeCode; }
            set { roomTypeCode = value; }
        }

        public string RatePlanCode
        {
            get { return ratePlanCode; }
            set { ratePlanCode = value; }
        }
        public string RatePlanId
        {
            get { return ratePlanId; }
            set { ratePlanId = value; }
        }

        public string NoOfUnits
        {
            get { return noOfUnits; }
            set { noOfUnits = value; }
        }

        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }

        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }

        public List<int> ChildAge
        {
            get { return childAge; }
            set { childAge = value; }
        }

        public string RoomName
        {
            get { return roomName; }
            set { roomName = value; }
        }

        public List<string> Ameneties
        {
            get { return ameneties; }
            set { ameneties = value; }
        }

        public int PriceId
        {
            get { return priceId; }
            set { priceId = value; }
        }

        public HotelRoomFareBreakDown[] RoomFareBreakDown
        {
            get { return roomFareBreakDown; }
            set { roomFareBreakDown = value; }
        }

        public decimal ExtraGuestCharge
        {
            get { return extraGuestCharge; }
            set { extraGuestCharge = value; }
        }
        public decimal ChildCharge
        {
            get { return childCharge; }
            set { childCharge = value; }
        }
        public PriceAccounts Price
        {
            get { return price; }
            set { price = value; }
        }

        public decimal PreviousFare
        {
            get { return previousFare; }
            set { previousFare = value; }
        }
        public bool ExtraBed
        {
            get { return extraBed; }
            set { extraBed = value; }
        }
        public int NoOfExtraBed
        {
            get { return noOfExtraBed; }
            set { noOfExtraBed = value; }
        }
        public int NoOfCots
        {
            get { return noOfCots; }
            set { noOfCots = value; }
        }
        public bool SharingBed
        {
            get { return sharingBed; }
            set { sharingBed = value; }
        }

        public List<HotelPassenger> PassenegerInfo
        {
            get { return paasengerInfo; }
            set { paasengerInfo = value; }
        }
        public List<HotelTaxBreakup> TaxInfo
        {
            get { return taxInfo; }
            set { taxInfo = value; }
        }


        public string MealPlanDesc
        {
            get { return mealPlanDesc; }
            set { mealPlanDesc = value; }
        }

        public string EssentialInformation
        {
            get{return essentialInformation;}
            set { essentialInformation = value; }
        }
        public string SmokingPreference
        {
            get { return smokingPreference; }
            set { smokingPreference = value; }
        }
        public PriceAccounts TBOPrice
        {
            get { return tboPrice; }
            set { tboPrice = value; }
        }
        public int RoomIndex
        {
            get { return roomIndex; }
            set { roomIndex = value; }
        }
        public string BedTypeCode
        {
            get { return bedTypeCode; }
            set { bedTypeCode = value; }
        }
        public string Supplements
        {
            get { return supplements; }
            set { supplements = value; }
        }

        public List<Surcharges> SurChargeList
        {
            get
            {
                return surChargeList;
            }

            set
            {
                surChargeList = value;
            }
        }

        public string Gxsupplier { get => gxsupplier; set => gxsupplier = value; }
        public string GuaranteeTypeCode { get; set; }
        public List<RoomTaxBreakup> TaxBreakUps { get => taxBreakUps; set => taxBreakUps = value; }
//Cancel Policy Updates
        public List<HotelCancelPolicy> CancelPolicy { get; set; }
        public string RoomwiseConfirmNo
        {
            get { return roomwiseConfirmNo; }
            set { roomwiseConfirmNo = value; }
        }
        public string RoomwiseConfirmStatus
        {
            get { return roomwiseConfirmStatus; }
            set { roomwiseConfirmStatus = value; }
        }
        public string RoomCancelNo
        {
            get { return roomCancelNo; }
            set { roomCancelNo = value; }
        }
        public string RoomwiseCancelStatus
        {
            get { return roomwiseCancelStatus; }
            set { roomwiseCancelStatus = value; }
        }
           public string AllocType
        {
            get { return allocType; }
            set { allocType = value; }
        }
        public string SellType
        {
            get { return sellType; }
            set { sellType = value; }
        }

        public int RqId { get => rqId; set => rqId = value; }
        #region Method

        public int Save()
        {
            //TODO: validate that the fields are properly set before calling the SP
            //Trace.TraceInformation("HotelRoom.Save entered.");

            SqlParameter[] paramList = new SqlParameter[19];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            paramList[1] = new SqlParameter("@adultCount", adultCount);
            paramList[2] = new SqlParameter("@childCount", childCount);
            string childAgeString = string.Empty;
            if (childAge != null)
            {
                for (int i = 0; i < childAge.Count; i++)
                {
                    childAgeString = childAgeString + "|" + childAge[i];
                }
            }
            paramList[3] = new SqlParameter("@childAge", childAgeString);
            paramList[4] = new SqlParameter("@roomName", roomName);
            paramList[5] = new SqlParameter("@roomTypeCode", roomTypeCode);
            paramList[6] = new SqlParameter("@ratePlanCode", ratePlanCode);
            string amenetiesString = string.Empty;
            if (ameneties != null)
            {
                for (int i = 0; i < ameneties.Count; i++)
                {
                    amenetiesString = amenetiesString + "|" + ameneties[i];
                }
            }
            paramList[7] = new SqlParameter("@amenities", amenetiesString);
            paramList[8] = new SqlParameter("@noOfUnit", noOfUnits);
            paramList[9] = new SqlParameter("@extraGuestCharge", extraGuestCharge);
            paramList[10] = new SqlParameter("@priceId", price.PriceId);
            paramList[11] = new SqlParameter("@roomId", SqlDbType.Int);
            paramList[12] = new SqlParameter("@extraBed", extraBed);
            paramList[13] = new SqlParameter("@noOfCots", noOfCots);
            paramList[14] = new SqlParameter("@sharingBed", sharingBed);
            if (noOfExtraBed == 0)
            {
                if (extraBed)//if true in the previous booking default 1 
                {
                    noOfExtraBed = 1;
                }
                else
                {
                    noOfExtraBed = 0;
                }
            }
            paramList[15] = new SqlParameter("@noOfExtraBed", noOfExtraBed);
            paramList[16] = new SqlParameter("@childCharge", childCharge);
            paramList[11].Direction = ParameterDirection.Output;
            paramList[17] = new SqlParameter("@MealPlanDesc", mealPlanDesc);
        if(!string.IsNullOrEmpty(gxsupplier))    paramList[18] = new SqlParameter("@Gxsupplier", gxsupplier);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHotelRoom, paramList);
            roomId = (int)paramList[11].Value;

            // saving room fare break
            if(RoomFareBreakDown!=null)
            {
                for (int i = 0; i < roomFareBreakDown.Length; i++)
                {
                    roomFareBreakDown[i].RoomId = roomId;
                    roomFareBreakDown[i].Save();
                }
            }
            
            foreach (HotelPassenger passInfo in paasengerInfo)
            {
                passInfo.RoomId = roomId;
                passInfo.HotelId = hotelId;
                passInfo.Save();
            }
            #region roomTax breakUp
            if (TaxBreakUps != null)
            {
                foreach (RoomTaxBreakup breakups in TaxBreakUps)
                {
                    HotelTaxBreakup taxBreakUps = new HotelTaxBreakup();
                    taxBreakUps.RoomId = roomId;
                    taxBreakUps.HotelId = hotelId;
                    taxBreakUps.TaxName = breakups.TaxTitle;
                    taxBreakUps.TaxValue = breakups.Value;
                    taxBreakUps.IsIncluded = breakups.IsIncluded ?? false;
                    taxBreakUps.UnitType = breakups.UnitType;
                    taxBreakUps.IsMandatory = breakups.IsMandatory ?? false;
                    taxBreakUps.FrequencyType = breakups.FrequencyType;
                    taxBreakUps.Save();
                }
            }
            #endregion
            return roomId;
        }

        public HotelRoom[] Load(int hotelId)
        {
            ////Trace.TraceInformation("HotelRoom.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId Id should be positive integer", "hotelId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);

            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelRoomByHotelId, paramList,connection);
            List<HotelRoom> hotelRooms = new List<HotelRoom>();
            using (DataTable dtHotelRooms = DBGateway.FillDataTableSP(SPNames.GetHotelRoomByHotelId, paramList))
            {
                if (dtHotelRooms != null && dtHotelRooms.Rows.Count > 0)
                {
                    //while (data.Read())
                    foreach (DataRow data in dtHotelRooms.Rows)
                    {
                        HotelRoom hroom = new HotelRoom();
                        hroom.roomId = Convert.ToInt32(data["roomId"]);
                        hroom.hotelId = Convert.ToInt32(data["hotelId"]);
                        hroom.adultCount = Convert.ToInt16(data["adultCount"]);
                        string ameneties = Convert.ToString(data["amenities"]);
                        string[] amenetiesArray = ameneties.Split('|');
                        hroom.ameneties = new List<string>();
                        for (int i = 0; i < amenetiesArray.Length; i++)
                        {
                            if (amenetiesArray[i] != "")
                            {
                                hroom.ameneties.Add(amenetiesArray[i]);
                            }
                        }

                        //hroom.childAge
                        string childAge = Convert.ToString(data["childAge"]);
                        string[] childAgeList = childAge.Split('|');
                        hroom.childAge = new List<int>();
                        for (int i = 0; i < childAgeList.Length; i++)
                        {
                            if (childAgeList[i] != "" && childAgeList[i] != null)
                            {
                                hroom.childAge.Add(Convert.ToInt16(childAgeList[i]));
                            }
                        }

                        hroom.childCount = Convert.ToInt32(data["childCount"]);
                        hroom.noOfUnits = Convert.ToString(data["noOfUnit"]);
                        hroom.priceId = Convert.ToInt32(data["priceId"]);
                        hroom.ratePlanCode = Convert.ToString(data["ratePlanCode"]);
                        hroom.extraGuestCharge = Convert.ToDecimal(data["extraGuestCharge"]);
                        hroom.childCharge = Convert.ToDecimal(data["childCharge"]);
                        hroom.roomFareBreakDown = HotelRoomFareBreakDown.Load(hroom.roomId);

                        hroom.roomName = Convert.ToString(data["roomName"]);
                        hroom.roomTypeCode = Convert.ToString(data["roomTypeCode"]);
                        if (!Convert.IsDBNull(data["extraBed"]))
                        {
                            hroom.extraBed = Convert.ToBoolean(data["extraBed"]);
                        }
                        if (!Convert.IsDBNull(data["noOfExtraBed"]))
                        {
                            hroom.noOfExtraBed = Convert.ToInt32(data["noOfExtraBed"]);
                        }
                        if (!Convert.IsDBNull(data["noOfCots"]))
                        {
                            hroom.noOfCots = Convert.ToInt32(data["noOfCots"]);
                        }
                        if (!Convert.IsDBNull(data["sharingBed"]))
                        {
                            hroom.sharingBed = Convert.ToBoolean(data["sharingBed"]);
                        }
                        if (!Convert.IsDBNull(data["MealPlanDesc"]))
                        {
                            hroom.mealPlanDesc = Convert.ToString(data["MealPlanDesc"]);
                        }
                        if (!Convert.IsDBNull(data["roomwiseconfirmationNo"]))
                        {
                            hroom.roomwiseConfirmNo = Convert.ToString(data["roomwiseconfirmationNo"]);
                        }
                        if (!Convert.IsDBNull(data["roomwiseconfirmStatus"]))
                        {
                            hroom.roomwiseConfirmStatus = Convert.ToString(data["roomwiseconfirmStatus"]);
                        }
                        if (!Convert.IsDBNull(data["roomwisecancelStatus"]))
                        {
                            hroom.roomwiseCancelStatus = Convert.ToString(data["roomwisecancelStatus"]);
                        }
                        hroom.gxsupplier = Convert.ToString(data["gxsupplierName"]);
                        //price information
                        PriceAccounts pAcc = new PriceAccounts();
                        pAcc.Load(hroom.priceId);
                        hroom.price = pAcc;
                        HotelPassenger passInfo = new HotelPassenger();
                        hroom.paasengerInfo = passInfo.Load(hroom.hotelId, hroom.roomId);
                        HotelTaxBreakup taxInfo = new HotelTaxBreakup();
                        hroom.taxInfo    = taxInfo.Load(hroom.hotelId, hroom.roomId);
                        hotelRooms.Add(hroom);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            HotelRoom[] hotelRoomArray = hotelRooms.ToArray();
            ////Trace.TraceInformation("HotelRoom.Load exiting.");
            return hotelRoomArray;
        }

        public HotelRoom LoadByRoomId(int roomId)
        {
            //Trace.TraceInformation("HotelRoom.LoadByRoomId entered : roomId = " + roomId);
            if (roomId <= 0)
            {
                throw new ArgumentException("RoomId Id should be positive integer", "roomId");
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@roomId", roomId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelRoomByRoomId, paramList,connection);
            HotelRoom hroom = new HotelRoom();
            if (data.Read())
            {
                hroom.roomId = Convert.ToInt32(data["roomId"]);
                hroom.hotelId = Convert.ToInt32(data["hotelId"]);
                hroom.adultCount = Convert.ToInt16(data["adultCount"]);
                string ameneties = Convert.ToString(data["amenities"]);
                string[] amenetiesArray = ameneties.Split('|');
                hroom.ameneties = new List<string>();
                for (int i = 0; i < amenetiesArray.Length; i++)
                {
                    if (amenetiesArray[i] != "")
                    {
                        hroom.ameneties.Add(amenetiesArray[i]);
                    }
                }

                //hroom.childAge
                string childAge = Convert.ToString(data["childAge"]);
                string[] childAgeList = childAge.Split('|');
                hroom.childAge = new List<int>();
                for (int i = 0; i < childAgeList.Length; i++)
                {
                    if (childAgeList[i] != "" && childAgeList[i] != null)
                    {
                        hroom.childAge.Add(Convert.ToInt16(childAgeList[i]));
                    }
                }

                hroom.childCount = Convert.ToInt32(data["childCount"]);
                hroom.noOfUnits = Convert.ToString(data["noOfUnit"]);
                hroom.priceId = Convert.ToInt32(data["priceId"]);
                hroom.ratePlanCode = data["ratePlanCode"].ToString();
                hroom.extraGuestCharge = Convert.ToDecimal(data["extraGuestCharge"]);

                hroom.roomFareBreakDown = HotelRoomFareBreakDown.Load(hroom.roomId);

                hroom.roomName = data["roomName"].ToString();
                hroom.roomTypeCode = data["roomTypeCode"].ToString();
                if (data["extraBed"] != DBNull.Value)
                {
                    hroom.extraBed = Convert.ToBoolean(data["extraBed"]);
                }
                if (!Convert.IsDBNull(data["noOfExtraBed"]))
                {
                    hroom.noOfExtraBed = Convert.ToInt32(data["noOfExtraBed"]);
                }
                if (data["noOfCots"] != DBNull.Value)
                {
                    hroom.noOfCots = Convert.ToInt32(data["noOfCots"]);
                }
                if (data["sharingBed"] != DBNull.Value)
                {
                    hroom.sharingBed = Convert.ToBoolean(data["sharingBed"]);
                }
                //price information
                PriceAccounts pAcc = new PriceAccounts();
                pAcc.Load(hroom.priceId);
                hroom.price = pAcc;
            }
            data.Close();
            connection.Close();

            //Trace.TraceInformation("HotelRoom.LoadByRoomId exiting.");

            return hroom;
        }
        /// <summary>
        /// This Method is used to update the room information.
        /// </summary>
        /// <param name="roomId"></param>
        public void UpdateRoom(int roomId)
        {
            //Trace.TraceInformation("HotelRoom.UpdateRoom entered : roomId = " + roomId);
            //first delete the fare breakup information.
            HotelRoomFareBreakDown roomFareBreakUp = new HotelRoomFareBreakDown();
            roomFareBreakUp.DeleteFareBreakDown(roomId);
            // Add the latest farebreak up Information.
            for (int i = 0; i < roomFareBreakDown.Length; i++)
            {
                roomFareBreakDown[i].RoomId = roomId;
                roomFareBreakDown[i].Save();
            }
            ///Trace.TraceInformation("HotelRoom.UpdateRoom Exited : roomId = " + roomId);
        }
        /// <summary>
        /// This method is used to Update the Only Pax Info
        /// </summary>
        /// <param name="prevroomId"></param>
        /// <param name="prevHotelId"></param>
        public void UpdatePax(int prevroomId, int prevHotelId)
        {
            //Trace.TraceInformation("HotelRoom.UpdatePax entered : roomId = " + prevroomId);
            List<HotelPassenger> prevPaxInfo = new List<HotelPassenger>();
            HotelPassenger paxInfo = new HotelPassenger();
            prevPaxInfo = paxInfo.Load(prevHotelId, roomId);
            int i = 0;
            //update the Passenger information.
            foreach (HotelPassenger passInfo in paasengerInfo)
            {
                passInfo.PaxId = prevPaxInfo[i++].PaxId;
                passInfo.Update();
            }
            //Trace.TraceInformation("HotelRoom.UpdatePax exit : roomId = " + prevroomId);
        }


        /// <summary>
        /// This method is used to get the RoomTypes anfd room meal plans
        /// </summary>

        public  static DataSet getRoomData()
        {
            DataSet dsRoomData = null;
            List<SqlParameter> parameters = new List<SqlParameter>();
            try
            {
                dsRoomData= DBGateway.FillSP("usp_getRoomData", parameters.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception atgetRoomData(),reason is :" + ex.ToString(), "");
            }
            return dsRoomData;
        }

        # endregion

        /// <summary>
        /// This method is Get HotelItineraryRoomInfo
        /// </summary>
        /// <param name="HotelId">HotelId</param>
        public static DataTable GetHotelItineraryRoomInfo(int HotelId)
        {
            try
            {
                DataTable dtR = new DataTable();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@hotelId", HotelId);
                //dtR = DBGateway.FillDataTableSP("usp_GetHotelRoomByHotelId", paramList);
                dtR = DBGateway.FillDataTableSP("usp_GetHotelSaleSummary", paramList);
                return dtR;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateitineraryRoomStatus(int HotelId, int roomId,string SubResNo, string RoomStatus,string flag)
        {
            //Trace.TraceInformation("HotelItinerary.Update entered  ");

            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@flag", flag);
            paramList[1] = new SqlParameter("@hotelId", HotelId);
            paramList[2] = new SqlParameter("@roomId", roomId);
            paramList[3] = new SqlParameter("@roomsubResNo", SubResNo);
            paramList[4] = new SqlParameter("@roomStatus", RoomStatus);
            

            int retVal = DBGateway.ExecuteNonQuerySP("usp_UpdateHotelRoomStatus", paramList);
            //Trace.TraceInformation("HotelItinerary.Update exiting count" + retVal);
        }
    }
}
