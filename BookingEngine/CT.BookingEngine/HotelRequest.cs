using System;
using System.Collections.Generic;

namespace CT.BookingEngine
{

    public enum HotelRequestMode
    {
        B2B = 1,
        BookingAPI = 2,
        WhiteLabel = 3
    }
    [Serializable]
    public struct RoomGuestData
    {
        public int noOfAdults;
        public int noOfChild;
        public List<int> childAge; // ChildAge list size should be equal to noOfChild
    }
    [Serializable]
    public struct RoomInfo
    {
        public string roomTypeCode;
        public string roomTypeName;
        public int noOfRooms;
        public int noOfCots;
        public List<int> childAge;
    }
    [Serializable]
    public class HotelRequest
    {
        # region private variables
        private int cityId;
        private DateTime startDate;
        private DateTime endDate;
        private string cityName;
        private string cityCode;
        private int noOfRooms; // total no of rooms
        private RoomGuestData[] roomGuest;
        private string hotelName; // - Optional
        private string area; // - Optional
        private string attraction; // - Optional
        private HotelRating rating; // by Default - All
        private int extraCots; // - Optional
        private string currency; // Always 'INR' - Optional
        private string priceRange; //ie. 1000 & gretaed than, 2000 & greated than - Optional
        private AvailabilityType availabilityType;
        private List<string> sources;
        private List<string> hotelStaticId; //get list of static ids
        private bool isDomestic;
        private bool searchByArea;
        private string countryName;
        private bool isMultiRoom;
        private int minRating;
        private int maxRating;
        private HotelRequestMode requestMode;
        private string _corptravelreason;
        private string _corptraveler;
        //private List<RoomInfo> multiRoom;

        /// <summary>
        /// Changes in Version 2 of DOTW
        /// updated by shiva dt: 30 April 2013
        /// </summary>
        private string passengerNationality;
        private string passengerCountryOfResidence;
        private RoomRateType roomRateType;

        /// <summary>
        /// End Changes of Version 2
        /// </summary>
        //VAT Changes
        string loginCountryCode;
        string countryCode;
        private double longtitude;
        private double latitude;
        private int radius;
        private int[] supplierIds;
        private bool _IsGetHotelDtls;// For Trip Service detail Call
        private string _URLocator;
        public int _BookedBy;
        #endregion

        # region Public properties
        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public string CityName
        {
            get
            {
                return cityName;
            }
            set
            {
                cityName = value;
            }
        }

        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }

        public int NoOfRooms
        {
            get
            {
                return noOfRooms;
            }
            set
            {
                noOfRooms = value;
            }
        }

        public RoomGuestData[] RoomGuest
        {
            get
            {
                return roomGuest;
            }
            set
            {
                roomGuest = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        public string Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }

        public string Attraction
        {
            get
            {
                return attraction;
            }
            set
            {
                attraction = value;
            }
        }

        public HotelRating Rating
        {
            get
            {
                return rating;
            }
            set
            {
                rating = value;
            }
        }

        public int ExtraCots
        {
            get
            {
                return extraCots;
            }
            set
            {
                extraCots = value;
            }
        }

        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }

        public string PriceRange
        {
            get
            {
                return priceRange;
            }
            set
            {
                priceRange = value;
            }
        }

        public AvailabilityType AvailabilityType
        {
            get { return availabilityType; }
            set { availabilityType = value; }
        }

        public List<string> Sources
        {
            get { return sources; }
            set { sources = value; }
        }
        public List<string> HotelStaticId
        {
            get { return hotelStaticId; }
            set { hotelStaticId = value; }
        }
        public bool IsDomestic
        {
            get { return isDomestic; }
            set { isDomestic = value; }
        }
        public bool SearchByArea
        {
            get { return searchByArea; }
            set { searchByArea = value; }
        }
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public bool IsMultiRoom
        {
            get { return isMultiRoom; }
            set { isMultiRoom = value; }
        }
        //public List<RoomInfo> MultiRoom
        //{
        //    get { return multiRoom; }
        //    set { multiRoom = value; }
        //}
        public int MinRating
        {
            get { return minRating; }
            set { minRating = value; }
        }
        public int MaxRating
        {
            get { return maxRating; }
            set { maxRating = value; }
        }
        public HotelRequestMode RequestMode
        {
            get { return requestMode; }
            set { requestMode = value; }
        }
        
        
        /// <summary>
        /// Specifies passenger nationality and it is mandatory to be sent in the request. 
        /// This is DOTW country internal code that can be obtained using the getallcountries request. 
        /// </summary>
        public string PassengerNationality
        {
            get { return passengerNationality; }
            set { passengerNationality = value; }
        }

        /// <summary>
        /// Specifies which type of rates (or suppliers - 
        /// if the request is sent with noPrice set to true) 
        /// are taken into consideration by the system. Possible values:
        /// 1 - DOTW rate type
        /// 2 - DYNAMIC DIRECT rate type
        /// 3 - DYNAMIC 3rd PARTY rate type
        /// </summary>
        public RoomRateType RoomRateType
        {
            get { return roomRateType; }
            set { roomRateType = value; }
        }
       
        /// <summary>
        /// Specifies passenger country of residence and it is mandatory to be sent in the request. 
        /// This is DOTW country internal code that can be obtained using the getallcountries request.
        /// </summary>
        public string PassengerCountryOfResidence
        {
            get { return passengerCountryOfResidence; }
            set { passengerCountryOfResidence = value; }
        }

        public string LoginCountryCode
        {
            get
            {
                return loginCountryCode;
            }

            set
            {
                loginCountryCode = value;
            }
        }

        public string CountryCode
        {
            get
            {
                return countryCode;
            }

            set
            {
                countryCode = value;
            }
        }

        public double Longtitude { get => longtitude; set => longtitude = value; }
        public double Latitude { get => latitude; set => latitude = value; }
        public int Radius { get => radius; set => radius = value; }
        public int[] SupplierIds { get => supplierIds; set => supplierIds = value; }
        public string Corptravelreason { get => _corptravelreason; set => _corptravelreason = value; }
        public string Corptraveler { get => _corptraveler; set => _corptraveler = value; }
        public bool IsGetHotelDtls { get => _IsGetHotelDtls; set => _IsGetHotelDtls = value; }
        public string URLocator { get => _URLocator; set => _URLocator = value; }
        /// <summary>
        /// To hold transaction type B2C/B2B
        /// </summary>
        public string Transtype { get; set; }
        /// <summary>
        /// To save booked for user id
        /// </summary>
        public int BookedBy { get => _BookedBy; set => _BookedBy = value; }
        #endregion

        #region PUBLIC METHODS
        public HotelRequest CopyByValue()
        {
            HotelRequest hReq = new HotelRequest();
            hReq.cityId = cityId;
            hReq.area = area;
            hReq.attraction = attraction;
            hReq.availabilityType = availabilityType;
            if (longtitude > 0 && latitude > 0)
            {
                hReq.longtitude = longtitude;
                hReq.latitude = latitude;
                hReq.radius = radius;
            }
            else
            {
                hReq.cityCode = cityCode;
                hReq.cityName = cityName;

                hReq.countryName = countryName;
            }
            hReq.SupplierIds = SupplierIds;
            hReq.currency = currency;
            hReq.endDate = endDate;
            hReq.extraCots = extraCots;
            hReq.hotelName = hotelName;
            hReq.isDomestic = isDomestic;
            hReq.isMultiRoom = isMultiRoom;
            //hReq.multiRoom = multiRoom;
            hReq.noOfRooms = noOfRooms;
            hReq.priceRange = priceRange;
            hReq.rating = rating;
            hReq.roomGuest = roomGuest;
            hReq.searchByArea = searchByArea;
            hReq.sources = sources;
            hReq.hotelStaticId = hotelStaticId;
            hReq.startDate = startDate;
            hReq.minRating = minRating;
            hReq.maxRating = maxRating;
            hReq.requestMode = requestMode;
            DOTWCountry dotw = new DOTWCountry();
            Dictionary<string, string> Countries = null;
            int i = 0;
            bool result = int.TryParse(PassengerNationality, out i); //i now = 108
            if (result)
            {
                Countries = dotw.GetAllCountries();
                hReq.PassengerNationality = CT.Core.Country.GetCountryCodeFromCountryName(Countries[PassengerNationality]);
            }
            else
                hReq.PassengerNationality = PassengerNationality;
            result = int.TryParse(passengerCountryOfResidence, out i); //i now = 108
            if (result)
            {
                if (Countries == null)
                    Countries = dotw.GetAllCountries();
                hReq.passengerCountryOfResidence = CT.Core.Country.GetCountryCodeFromCountryName(Countries[PassengerCountryOfResidence]);
            }
            else
            {
                hReq.passengerCountryOfResidence = passengerCountryOfResidence;
            }
           // hReq.PassengerNationality = PassengerNationality;
            hReq.loginCountryCode = loginCountryCode;
            hReq.countryCode = countryCode;
            hReq.Transtype = Transtype;
            hReq.BookedBy = BookedBy;
            return hReq;
        }

        #endregion
    }
}
