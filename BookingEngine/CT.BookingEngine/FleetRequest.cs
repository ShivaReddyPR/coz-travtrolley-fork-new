﻿using System;
//using System.Linq;

namespace CT.BookingEngine
{
    public class FleetRequest
    {
        string fromCity;       //pickup city 
        string fromCityCode;       
        string fromLocation;   //pickup location 
        string fromLocationCode;  
        DateTime fromDateTime; //pickupTime
        string carType;
        string returnStatus;
        string toCity;
        string toCityCode;
        string toLocation;
        string toLocationCode;
        DateTime toDateTime;

        #region Properties
        public string FromCity
        {
            get { return fromCity; }
            set { fromCity = value; }
        }
        public string FromCityCode
        {
            get { return fromCityCode; }
            set { fromCityCode = value; }
        }
        public string FromLocation
        {
            get { return fromLocation; }
            set { fromLocation = value; }
        }
        public string FromLocationCode
        {
            get { return fromLocationCode; }
            set { fromLocationCode = value; }
        }
        public DateTime FromDateTime
        {
            get { return fromDateTime; }
            set { fromDateTime = value; }
        }
        public string CarType
        {
            get { return carType; }
            set { carType = value; }
        }
        public string ReturnStatus
        {
            get { return returnStatus; }
            set { returnStatus = value; }
        }
        public string ToCity
        {
            get { return toCity; }
            set { toCity = value; }
        }
        public string ToCityCode
        {
            get { return toCityCode; }
            set { toCityCode = value; }
        }
        public string ToLocation
        {
            get { return toLocation; }
            set { toLocation = value; }
        }
        public string ToLocationCode
        {
            get { return toLocationCode; }
            set { toLocationCode = value; }
        }
        public DateTime ToDateTime
        {
            get { return toDateTime; }
            set { toDateTime = value; }
        }
        #endregion
        #region PUBLIC METHODS
        public FleetRequest CopyByValue()
        {
            FleetRequest fleetRquest = new FleetRequest();
            fleetRquest.fromCity = fromCity;
            fleetRquest.fromDateTime = fromDateTime;
            fleetRquest.fromLocation = fromLocation;
            fleetRquest.toCity = toCity;
            fleetRquest.toDateTime = toDateTime;
            fleetRquest.ToLocation = ToLocation;
            fleetRquest.returnStatus = returnStatus;
            fleetRquest.carType = carType;
            fleetRquest.fromCityCode = fromCityCode;
            fleetRquest.toCityCode = toCityCode;
            fleetRquest.fromLocationCode = fromLocationCode;
            fleetRquest.toCityCode = toCityCode;
            return fleetRquest;
        }
        #endregion
    }
}
