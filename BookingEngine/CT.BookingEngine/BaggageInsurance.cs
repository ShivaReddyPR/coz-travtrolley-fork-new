﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Linq;
using System.Text;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using System.Threading.Tasks;
using System.Transactions;
using CT.TicketReceipt.Common;

namespace CT.BookingEngine
{
    public class BaggageInsuranceMaster
    {
        int planID;
        string plancode;
        string plantitle;
        string plandescription;
        string planmarkingpoints;
        decimal planadultprice;
        string sourceCurrency;
        decimal rateOfExchange;
        Dictionary<string, decimal> agentExchageRates;
        decimal sourceAmount;
        decimal markup;
        decimal markupValue;
        string markupType;
        decimal discount;
        decimal discountValue;
        string discountType;
        decimal gstAmount;
        string paxCurrency;
        List<GSTTaxDetail> gstDetails;
        decimal inputVAT;
        decimal outputVAT;
        decimal serviceFee;

        #region BaggageInsurance class properties
        public int PlanID
        {
            get { return planID; }
            set { planID = value; }
        }

        /// <summary>
        /// Get or set Plan Code
        /// </summary>
        public string PlanCode
        {
            get
            {
                return plancode;
            }
            set
            {
                plancode = value;
            }
        }
        /// <summary>
        /// Get or set Plan Title
        /// </summary>
        public string PlanTitle
        {
            get
            {
                return plantitle;
            }
            set
            {
                plantitle = value;
            }
        }
        /// <summary>
        /// Get or set Plan Description
        /// </summary>
        public string PlanDescription
        {
            get
            {
                return plandescription;
            }
            set
            {
                plandescription = value;
            }
        }
        /// <summary>
        /// Get or set PlanMarketingpoints
        /// </summary>
        public string PlanMarketingPoints
        {
            get
            {
                return planmarkingpoints;
            }
            set
            {
                planmarkingpoints = value;
            }
        }
        /// <summary>
        /// Get or set PlanMarketingpoints
        /// </summary>
        public decimal PlanAdultPrice
        {
            get
            {
                return planadultprice;
            }
            set
            {
                planadultprice = value;
            }
        }

        public decimal GSTAmount
        {
            get { return gstAmount; }
            set { gstAmount = value; }
        }

        public decimal InputVAT
        {
            get { return inputVAT; }
            set { inputVAT = value; }
        }
        public decimal OutputVAT
        {
            get { return outputVAT; }
            set { outputVAT = value; }
        }

        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return agentExchageRates; }
            set { agentExchageRates = value; }
        }
        public decimal SourceAmount
        {
            get { return sourceAmount; }
            set { sourceAmount = value; }
        }
        public decimal Markup
        {
            get { return markup; }
            set { markup = value; }
        }
        public decimal MarkupValue
        {
            get { return markupValue; }
            set { markupValue = value; }
        }
        public string MarkupType
        {
            get { return markupType; }
            set { markupType = value; }
        }
        public string PaxCurrency
        {
            get { return paxCurrency; }
            set { paxCurrency = value; }
        }
        public decimal RateOfExchange
        {
            get { return rateOfExchange; }
            set { rateOfExchange = value; }
        }
        public string SourceCurrency
        {
            get { return sourceCurrency; }
            set { sourceCurrency = value; }
        }

        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }

        public string DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }

        public List<GSTTaxDetail> GSTDetails
        {
            get { return gstDetails; }
            set { gstDetails = value; }
        }
        public decimal ServiceFee
        {
            get { return serviceFee; }
            set { serviceFee = value; }
        }
        #endregion

        #region BaggageInsurance Members

        private int planid;
        private int _id;
        private const int NEW_RECORD = -1;
        #endregion
        

        #region Save the BaggageInsurance in database
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@planid", _id);
                paramList[1] = new SqlParameter("@plancode", plancode);
                paramList[2] = new SqlParameter("@plantitle", plantitle);
                paramList[3] = new SqlParameter("@plandescription", plandescription);
                paramList[4] = new SqlParameter("@planmarketingpoints", planmarkingpoints);
                paramList[5] = new SqlParameter("@planadultprice", planadultprice);
                paramList[6] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[6].Direction = ParameterDirection.Output;
                paramList[7] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[7].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("usp_BaggageInsurance_add_update", paramList);
                string messageType = Utility.ToString(paramList[6].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[7].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
            // return planid;
        }
        #endregion
        public static DataTable GetAdditionalServiceMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("usp_GetBaggageInsuranceMasterDetails").Tables[0];
            }
            catch
            {
                throw;
            }
        }

       
        #region Constructors
        public BaggageInsuranceMaster()
        {
            _id = -1;
        }
        public BaggageInsuranceMaster(int id)
        {


            _id = id;
            GetDetails(id);
        }
        #endregion
        private void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@Plan_Id", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_BaggageInsuranceMaster_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["PLAN_ID"]);
                plancode = Utility.ToString(dr["PLAN_CODE"]);
                plantitle = Utility.ToString(dr["PLAN_TITLE"]);
                plandescription = Utility.ToString(dr["PLAN_DESCRIPTION"]);
                planmarkingpoints = Utility.ToString(dr["PLAN_MARKETING_POINTS"]);
                planadultprice = Utility.ToDecimal(dr["PLAN_ADULT_PRICE"]);

            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["PLAN_ID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch
            { throw; }

        }


        public List<BaggageInsuranceMaster> GetPlanDetails(int agentID, Dictionary<string, decimal> agentExchangeRates, string currency, int noOfPax, int locationId, int decimalPoint  )
        {
            List<BaggageInsuranceMaster> objBaggageInsuranceList = new List<BaggageInsuranceMaster>();
            try
            {
                LocationMaster location = new LocationMaster(locationId);
                string destinationType = "International";

                SqlParameter[] parm = new SqlParameter[0];
                DataTable dt = DBGateway.FillDataTableSP(SPNames.GetBaggageInsPlanDetails, parm);
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        BaggageInsuranceMaster objBaggageInsurance = new BaggageInsuranceMaster();
                        objBaggageInsurance.planID = Convert.ToInt32(dr["PLAN_ID"].ToString());
                        //objBaggageInsurance.PlanCode = dr["PLAN_CODE"].ToString();
                        objBaggageInsurance.PlanTitle = dr["PLAN_TITLE"].ToString();
                        objBaggageInsurance.PlanDescription = dr["PLAN_DESCRIPTION"].ToString();
                        objBaggageInsurance.PlanMarketingPoints = dr["PLAN_MARKETING_POINTS"].ToString();
                        objBaggageInsurance.PlanAdultPrice = Convert.ToDecimal(dr["PLAN_ADULT_PRICE"].ToString());
                        objBaggageInsurance.SourceAmount = Convert.ToDecimal(dr["PLAN_ADULT_PRICE"].ToString());
                        objBaggageInsurance.SourceCurrency = dr["PLAN_CURRENCY"].ToString();
                        objBaggageInsurance.PaxCurrency = currency;
                        objBaggageInsurance.ServiceFee = Convert.ToDecimal(dr["PLAN_SERVICE_FEE"].ToString());
                        objBaggageInsurance.PlanAdultPrice += objBaggageInsurance.ServiceFee;
                        //Calculated price
                        objBaggageInsurance.agentExchageRates = agentExchangeRates;
                        string Currency = dr["PLAN_CURRENCY"].ToString();

                        if (agentExchangeRates != null && agentExchangeRates.ContainsKey(Currency))  //location.Currency
                        {
                            objBaggageInsurance.RateOfExchange = agentExchangeRates[Currency];
                        }
                        else
                        {
                            objBaggageInsurance.RateOfExchange = 1;
                        }

                        objBaggageInsurance.PlanAdultPrice = (objBaggageInsurance.PlanAdultPrice * objBaggageInsurance.RateOfExchange * noOfPax);//+ objBaggageInsurance.MarkUP - objBaggageInsurance.discount;


                        DataTable dtMarkup = UpdateMarkup.Load(agentID, BookingSource.Baggage.ToString(), (int)ProductType.BaggageInsurance);

                        // mark up & discount  calculation

                        if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                        {
                            objBaggageInsurance.MarkupValue = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"].ToString());
                            objBaggageInsurance.DiscountValue = Convert.ToDecimal(dtMarkup.Rows[0]["Discount"].ToString());
                            objBaggageInsurance.MarkupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                            objBaggageInsurance.DiscountType = dtMarkup.Rows[0]["DiscountType"].ToString();

                            if (objBaggageInsurance.MarkupType == "P")
                            {
                                objBaggageInsurance.Markup = objBaggageInsurance.PlanAdultPrice * (objBaggageInsurance.MarkupValue / 100);
                            }
                            if (objBaggageInsurance.DiscountType == "P")
                            {
                                objBaggageInsurance.Discount = objBaggageInsurance.PlanAdultPrice * (objBaggageInsurance.DiscountValue / 100);
                            }
                            if (objBaggageInsurance.MarkupType == "F")
                            {
                                
                                objBaggageInsurance.Markup = objBaggageInsurance.MarkupValue * noOfPax;

                            }
                            if (objBaggageInsurance.DiscountType == "F")
                            {
                                objBaggageInsurance.Discount = objBaggageInsurance.discountValue * noOfPax;
                            }

                        }
                        else
                        {
                            objBaggageInsurance.Markup = 0;
                            objBaggageInsurance.discount = 0;
                            objBaggageInsurance.MarkupValue = 0;
                            objBaggageInsurance.discountValue = 0;
                            objBaggageInsurance.MarkupType = "P";
                            objBaggageInsurance.discountType = "P";

                        }
                        //Calculating GST on Mark up 
                        decimal gstAmount = 0;
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        //calculating GST always on Mumbai location(location configured in web.config).
                        gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, objBaggageInsurance.Markup, locationId);
                        objBaggageInsurance.GSTAmount = gstAmount;
                        objBaggageInsurance.GSTDetails = gstTaxList;

                        //calculating VAT on Total amount (amount+SF+mark up-discount+GST)
                        decimal outPutVAT = 0m;
                        decimal inputVAT = 0m;
                        decimal totalAmount = 0m;
                        PriceTaxDetails taxDetails = PriceTaxDetails.GetVATCharges(location.CountryCode, location.CountryCode, location.CountryCode, (int)ProductType.BaggageInsurance, Module.Insurance.ToString(), destinationType);
                        if (taxDetails != null && taxDetails.InputVAT != null)
                        {
                            taxDetails.InputVAT.CalculateVatAmount(objBaggageInsurance.planadultprice, ref inputVAT, decimalPoint);
                        }
                        if (taxDetails != null && taxDetails.OutputVAT != null && taxDetails.OutputVAT.Applied)
                        {
                            totalAmount = objBaggageInsurance.PlanAdultPrice + objBaggageInsurance.Markup + objBaggageInsurance.GSTAmount - objBaggageInsurance.Discount;
                            //calculating output vat if VAT applied on SF then VAT on (markup + GST amount)
                            outPutVAT = taxDetails.OutputVAT.CalculateVatAmount(totalAmount, objBaggageInsurance.Markup + objBaggageInsurance.GSTAmount, decimalPoint);
                        }
                        objBaggageInsurance.InputVAT = inputVAT;
                        objBaggageInsurance.OutputVAT = outPutVAT;

                        objBaggageInsuranceList.Add(objBaggageInsurance);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objBaggageInsuranceList;
        }

    }
    public class BaggageInsuranceHeader
    {
        int bid;
        int planId;
        int noOfPassengers;
        int noOfSectors;
        int agentId;
        decimal totalAmount;
        string email;
        string phoneNumber;
        string pnr;
        int location;
        long createdBy;
        string policy;
        string firstName;
        string lastName;
        string currency;
        string planTitle;
        string transType;

        #region properties
        public int Bid
        {
            get { return bid; }
            set { bid = value; }
        }
        public int PlanId
        {
            get { return planId; }
            set { planId = value; }
        }
        public string PlanTitle
        {
            get { return planTitle; }
            set { planTitle = value; }
        }

        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }

        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public int NoOfPassengers
        {
            get { return noOfPassengers; }
            set { noOfPassengers = value; }
        }

        public int NoOfSectors
        {
            get { return noOfSectors; }
            set { noOfSectors = value; }
        }

        public int AgentID
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public decimal TotalAmount
        {
            get { return totalAmount; }
            set
            {
                totalAmount = value;
            }
        }

        public string EmailID
        {
            get { return email; }
            set { email = value; }
        }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set
            {
                phoneNumber = value;
            }
        }

        public string PNR
        {
            get { return pnr; }
            set { pnr = value; }
        }

        public int Location
        {
            get { return location; }
            set { location = value; }
        }

        public long CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public string Policy
        {
            get { return policy; }
            set { policy = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        #endregion

        public DataSet LoadBaggageHeader(int bId)
        {
            DataSet dsHeader = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@Bid", bId);
                dsHeader = DBGateway.FillSP(SPNames.GetBaggageHeader, param);
                for (int i = 0; i < dsHeader.Tables[0].Rows.Count; i++)
                {
                    Bid = Convert.ToInt32(dsHeader.Tables[0].Rows[i]["BI_ID"].ToString());
                    PlanId = Convert.ToInt32(dsHeader.Tables[0].Rows[i]["BI_PLAN_ID"].ToString());
                    NoOfPassengers = Convert.ToInt32(dsHeader.Tables[0].Rows[i]["BI_NOOF_PAX"].ToString());
                    NoOfSectors = Convert.ToInt32(dsHeader.Tables[0].Rows[i]["BI_NOOF_SECTORS"].ToString());
                    AgentID = Convert.ToInt32(dsHeader.Tables[0].Rows[i]["BI_AGENT_ID"].ToString());
                    TotalAmount = Convert.ToDecimal(dsHeader.Tables[0].Rows[i]["BI_TOTAL_PRICE"].ToString());
                    EmailID = dsHeader.Tables[0].Rows[i]["BI_EMAIL"].ToString();
                    PhoneNumber = dsHeader.Tables[0].Rows[i]["BI_PHONE_NO"].ToString();
                    PNR = dsHeader.Tables[0].Rows[i]["BI_PNR"].ToString();
                    Location = Convert.ToInt32(dsHeader.Tables[0].Rows[i]["BI_LOCATION_ID"].ToString());
                    Policy = dsHeader.Tables[0].Rows[i]["BI_POLICY_NO"].ToString();
                    PlanTitle = dsHeader.Tables[0].Rows[i]["Title"].ToString();

                }
                if (dsHeader.Tables[1] != null && dsHeader.Tables[1].Rows.Count > 0)
                {
                    FirstName = dsHeader.Tables[1].Rows[0]["PAX_FIRST_NAME"].ToString();
                    LastName = dsHeader.Tables[1].Rows[0]["PAX_LAST_NAME"].ToString();
                   
                }


            }
            catch (Exception ex)
            {

            }
            return dsHeader;

        }
        public int SaveBaggageHeaderDetails()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[12];
                paramList[0] = new SqlParameter("@Bid", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[1] = new SqlParameter("@PlanID", PlanId);
                paramList[2] = new SqlParameter("@NoOfSectors", NoOfSectors);
                paramList[3] = new SqlParameter("@NoOfPax", noOfPassengers);
                paramList[4] = new SqlParameter("@PNR", PNR);
                paramList[5] = new SqlParameter("@AgentID", agentId);
                paramList[6] = new SqlParameter("@LocationID", Location);
                paramList[7] = new SqlParameter("@TotalPrice", TotalAmount);
                paramList[8] = new SqlParameter("@CreatedBY", CreatedBy);
                paramList[9] = new SqlParameter("@EmailID", EmailID);
                paramList[10] = new SqlParameter("@PhoneNumber", PhoneNumber);
                paramList[11] = new SqlParameter("@TransType", "B2B");

                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddBaggageHeader, paramList);
                return Convert.ToInt32(paramList[0].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable LoadPaxDetails(int headerId)
        {
            DataTable dt = new DataTable();
            SqlParameter[] parm = new SqlParameter[1];
            parm[0] = new SqlParameter("@Id", headerId);
            return DBGateway.FillDataTableSP(SPNames.GetPaxDetails, parm);
        }
        public static void Update(int planId, int status, int userID)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@Bid", planId);
                paramList[1] = new SqlParameter("@BookingStatus", status);
                paramList[2] = new SqlParameter("@UserID", userID);
                int count = DBGateway.ExecuteNonQuerySP(SPNames.UpdateBaggageBookingStatus, paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public class BaggagePassengerDetails
    {
        int id;
        int noOfPassengers;
        int noOfSectors;
        int agentId;
        string title;
        string firstName;
        string lastName;
        string pnrNumber;
        string ticketNumber;
        decimal paxAmount;
        decimal totalAmount;
        string email;
        string phoneNumber;
        string ticketNo;
        string policyNo;
        long createdBy;
        string sourceCurrency;
        string paxCurrency;
        decimal rateOfExchange;
        Dictionary<string, decimal> agentExchageRates;
        decimal sourceAmount;
        decimal markup;
        decimal markupValue;
        string markupType;
        decimal discount;
        string discountType;
        List<GSTTaxDetail> gstDetails;
        decimal gstAmount;
        decimal inputVAT;
        decimal outputVAT;
        //added by HAri
        int unDelivaredBags;
        string passengerName;
        decimal discountValue;


        #region Properties
        public int Bid
        {
            get { return id; }
            set { id = value; }
        }
        public int NoOfPassengers
        {
            get { return noOfPassengers; }
            set { noOfPassengers = value; }
        }

        public int NoOfSectors
        {
            get { return noOfSectors; }
            set { noOfSectors = value; }
        }

        public int AgentID
        {
            get { return agentId; }
            set { agentId = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public decimal RateOfExchange
        {
            get { return rateOfExchange; }
            set { rateOfExchange = value; }
        }
        public decimal InputVAT
        {
            get { return inputVAT; }
            set { inputVAT = value; }
        }
        public decimal OutputVAT
        {
            get { return outputVAT; }
            set { outputVAT = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string PNRNumber
        {
            get { return pnrNumber; }
            set { pnrNumber = value; }
        }

        public string TicketNumber
        {
            get { return ticketNumber; }
            set { ticketNumber = value; }
        }

        public decimal PaxAmount
        {
            get { return paxAmount; }
            set { paxAmount = value; }
        }

        public decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public string EmailID
        {
            get { return email; }
            set { email = value; }
        }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set
            {
                phoneNumber = value;
            }
        }

        public string TicketNO
        {
            get { return ticketNo; }
            set { ticketNo = value; }
        }
        public string PolicyNo { get { return policyNo; } set { policyNo = value; } }

        public long Createdby { get { return createdBy; } set { createdBy = value; } }

        public decimal SourceAmount
        {
            get { return sourceAmount; }
            set { sourceAmount = value; }
        }
        public decimal Markup
        {
            get { return markup; }
            set { markup = value; }
        }
        public decimal MarkupValue
        {
            get { return markupValue; }
            set { markupValue = value; }
        }
        public string MarkupType
        {
            get { return markupType; }
            set { markupType = value; }
        }

        public string SourceCurrency
        {
            get { return sourceCurrency; }
            set { sourceCurrency = value; }
        }

        public string Paxcurrency
        {
            get { return paxCurrency; }
            set { paxCurrency = value; }
        }
        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }

        public string DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }

        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return agentExchageRates; }
            set { agentExchageRates = value; }
        }
        public List<GSTTaxDetail> GSTDetails
        {
            get { return gstDetails; }
            set { gstDetails = value; }
        }
        public decimal GstAmount
        {
            get { return gstAmount; }
            set { gstAmount = value; }
        }

        public int UnDelivaredBags
        {
            get { return unDelivaredBags; }
            set { unDelivaredBags = value; }
        }

        public string PassengerName
        {
            get { return passengerName; }
            set { passengerName = value; }
        }
        #endregion

        public void SavePassengersDetails(List<BaggagePassengerDetails> baggagePassengersList)
        {
            try
            {
                int paxId = 0;
                for (int i = 0; i < baggagePassengersList.Count; i++)
                {
                    SqlParameter[] param = new SqlParameter[25];
                    param[0] = new SqlParameter("@paxId", paxId);
                    param[0].Direction = ParameterDirection.Output;
                    param[1] = new SqlParameter("@Bid", baggagePassengersList[i].Bid);
                    param[2] = new SqlParameter("@Title", baggagePassengersList[i].Title);
                    param[3] = new SqlParameter("@FirstName", baggagePassengersList[i].FirstName);
                    param[4] = new SqlParameter("@LastName", baggagePassengersList[i].LastName);
                    param[5] = new SqlParameter("@EmailID", baggagePassengersList[i].EmailID);
                    param[6] = new SqlParameter("@PhoneNumber", baggagePassengersList[i].PhoneNumber);
                    param[7] = new SqlParameter("@PNR", baggagePassengersList[i].PNRNumber);
                    param[8] = new SqlParameter("@TicketNo", baggagePassengersList[i].TicketNumber);
                    param[9] = new SqlParameter("@PolicyNo", baggagePassengersList[i].PolicyNo);
                    param[10] = new SqlParameter("@PaxAmount", baggagePassengersList[i].PaxAmount);
                    param[11] = new SqlParameter("@CreatedBY", baggagePassengersList[i].Createdby);

                    param[12] = new SqlParameter("@RateOfExchange", baggagePassengersList[i].rateOfExchange);
                    param[13] = new SqlParameter("@PaxCurrency", baggagePassengersList[i].Paxcurrency);
                    param[14] = new SqlParameter("@SourceAmount", baggagePassengersList[i].SourceAmount);
                    param[15] = new SqlParameter("@SourceCurrency", baggagePassengersList[i].SourceCurrency);
                    param[16] = new SqlParameter("@Markup", baggagePassengersList[i].Markup);
                    param[17] = new SqlParameter("@MarkupValue", baggagePassengersList[i].MarkupValue);
                    param[18] = new SqlParameter("@MarkupType", baggagePassengersList[i].MarkupType);
                    param[19] = new SqlParameter("@Discount", baggagePassengersList[i].Discount);
                    param[20] = new SqlParameter("@DiscountType", baggagePassengersList[i].DiscountType);
                    param[21] = new SqlParameter("@DiscountValue", baggagePassengersList[i].DiscountValue);
                    param[22] = new SqlParameter("@PAX_GST", baggagePassengersList[i].GstAmount);
                    param[23] = new SqlParameter("@PAX_INPUTVAT", baggagePassengersList[i].InputVAT);
                    param[24] = new SqlParameter("@PAX_OUTPUTVAT", baggagePassengersList[i].OutputVAT);

                    //param[11]=new SqlParameter()

                    int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddBaggagePaxDetails, param);
                    paxId = Convert.ToInt32(param[0].Value);

                    if (baggagePassengersList[i].GSTDetails != null)
                    {
                        foreach (var item in baggagePassengersList[i].GSTDetails)
                        {
                            item.PriceId = paxId;
                            item.ProductID = 15;
                            item.Save();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public DataTable GetpassengerDetails()
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@PAX_PNR", PNRNumber);
            param[1] = new SqlParameter("@PAX_PASSSENGER_NAME", PassengerName.Trim());
            param[2] = new SqlParameter("@PAX_EMAIL", EmailID);
            param[3] = new SqlParameter("@PAX_PHONE_NO", PhoneNumber);
            DataTable dt = DBGateway.FillDataTableSP(SPNames.GetBaggageTrackingPaxDetails, param);
            return dt;
        }

        public int UpdatBaggageTrackingStatus(int paxID, string remarks)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@PaxID", paxID);
            param[1] = new SqlParameter("@Remarks", remarks);
            return DBGateway.ExecuteNonQuerySP(SPNames.UpdateBaggageTrackingStatus, param);     

        }

        //Added by somasekhar on 12/10/2018
        // for Update passanger support document in table CZ_BAGGAGE_INSURANCE_PAX_DETAILS
        public string  UpdatePassengerSupportDocByPaxId(long paxId, bool pirDoc, bool boardpassDoc, int noOfUndeliverBags)
        {
            string trackNumber = "";
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@pax_track_number", SqlDbType.NVarChar, 50, trackNumber);
            param[0].Direction = ParameterDirection.Output;
            param[1] = new SqlParameter("@paxId", paxId);
            param[2] = new SqlParameter("@noOfUndeliveredBags", noOfUndeliverBags);
            param[3] = new SqlParameter("@pirDoc", pirDoc);
            param[4] = new SqlParameter("@boardpassDoc", boardpassDoc);
          
            int row= DBGateway.ExecuteNonQuerySP(SPNames.UpdatePassengerSupportDocByPaxId, param);
            return trackNumber = param[0].Value.ToString();
        }


    }
    public class BaggageInsuranceVoucherCls
    {
        int planID;
        string planName;
        decimal planPrice;
        string paxName;
        string ticketNo;
        string pnr;
        decimal serviceFee;
        string taxCode;
        decimal cgstTaxValue;
        decimal sgstTaxValue;
        decimal igstTaxValue;
        decimal taxAmount;
        decimal totalFees;
        string policyNo;
        decimal inputVAT;
        decimal outputVAT;
        decimal discount;
        decimal markup;
        string plandescription;

        #region properties
        public int PlanID
        {
            get { return planID; }
            set { planID = value; }
        }

        public string PlanName
        {
            get { return planName; }
            set { planName = value; }
        }
        public decimal PlanPrice
        {
            get { return planPrice; }
            set { planPrice = value; }
        }

        public string PaxName
        {
            get { return paxName; }
            set { paxName = value; }
        }
        public string TicketNo
        {
            get { return ticketNo; }
            set { ticketNo = value; }
        }
        public string PNR
        {
            get { return pnr; }
            set { pnr = value; }
        }

        public decimal ServiceFee
        {
            get { return serviceFee; }
            set { serviceFee = value; }
        }

        public string TaxCode
        {
            get { return taxCode; }
            set { taxCode = value; }
        }

        public decimal  CGSTTaxValue
        {
            get { return cgstTaxValue; }
            set { cgstTaxValue = value; }
        }
        public decimal SGSTTaxValue
        {
            get { return sgstTaxValue; }
            set { sgstTaxValue = value; }
        }
        public decimal IGSTTaxValue
        {
            get { return igstTaxValue; }
            set { igstTaxValue = value; }
        }

        public decimal TaxAmount
        {
            get {return taxAmount; }
            set { taxAmount = value; }
        }
        public decimal TotalFees
        {
            get { return totalFees; }
            set { totalFees = value; }
        }
        public string PolicyNo
        {
            get { return policyNo; }
            set { policyNo = value; }
        }
        public decimal InputVAT
        {
            get { return inputVAT; }
            set { inputVAT = value; }
        }
        public decimal OutputVAT
        {
            get { return outputVAT; }
            set { outputVAT = value; }
        }
        public decimal Markup
        {
            get { return markup; }
            set { markup = value; }
        }
        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        
        public string PlanDescription
        {
            get { return plandescription; }
            set { plandescription = value; }
        }
        #endregion

        public DataSet GetVoucherDetails(int bid)
        {
            try
            {
                DataSet ds = new DataSet();

                SqlParameter[] parm = new SqlParameter[1];
                parm[0] = new SqlParameter("@BID", bid);
                return ds = DBGateway.FillSP(SPNames.VoucherDetails, parm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public enum BaggageInsuranceBookingStatus
    {
        Confirmed = 1,
        Cancelled = 2,
        CancelRequest = 7
    }
}

