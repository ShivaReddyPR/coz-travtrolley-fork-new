﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
namespace CT.BookingEngine
{
    /// <summary>
    /// Class used to get Tax details for the Origin & Destination
    /// </summary>
    public class AirlineJourneyPriceDetails
    {
        

        private int _priceId;
        private string _airlineCode;
        private string _origin;
        private string _destination;
        private string _currency;
        private decimal _tax;
        private int _decimalPoint;
        private SearchType _type;


        public SearchType Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public int PriceId
        {
            get { return _priceId; }
            set { _priceId = value; }
        }
        public string AirlineCode
        {
            get { return _airlineCode; }
            set { _airlineCode = value; }
        }
        public string Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }
        public string Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }

        public decimal Tax
        {
            get { return _tax; }
            set { _tax = value; }
        }

        public int DecimalPoint
        {
            get { return _decimalPoint; }
            set { _decimalPoint = value; }
        }
       

       
        public AirlineJourneyPriceDetails()
        {
            _tax = 0; //Intially assigning the tax value to zero.
        }
        

        #region Methods
        /// <summary>
        /// Saves the tax details info with respect to airline code,origin and destination.
        /// </summary>
        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_AirlineCode", _airlineCode);
                paramList[1] = new SqlParameter("@P_origin", _origin);
                paramList[2] = new SqlParameter("@P_Destination", _destination);
                paramList[3] = new SqlParameter("@P_Tax", Math.Round(_tax, _decimalPoint));
                paramList[4] = new SqlParameter("@P_Currency", _currency);
                paramList[5] = new SqlParameter("@P_SearchType", _type);

                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddUpdateAirlineJourneyPriceDetails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "(" + _airlineCode + ")Failed to save the tax details info.Error: " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Gets the tax price with respect to airline code,origin and destination.
        /// </summary>
        /// <param name="airlineCode"></param>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public decimal LoadTaxDetails(string airlineCode, string origin, string destination, SearchType type,string currency)
        {
           
            try
            {

                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_AirlineCode", airlineCode);
                paramList[1] = new SqlParameter("@P_origin", origin);
                paramList[2] = new SqlParameter("@P_Destination", destination);
                paramList[3] = new SqlParameter("@P_Tax", SqlDbType.Decimal);
                paramList[3].Direction = ParameterDirection.Output;
                paramList[4] = new SqlParameter("@P_SearchType", type);
                paramList[5] = new SqlParameter("@P_Currency", currency);
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_GetAirlineJourneyTaxDetails", paramList);
                if (paramList[3].Value != DBNull.Value)
                {
                    _tax = Convert.ToDecimal(paramList[3].Value);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "("+airlineCode + ")Failed to Load the tax details info.Error: " + ex.ToString(), "");
            }
            return _tax;
        }
        #endregion
    }
}
