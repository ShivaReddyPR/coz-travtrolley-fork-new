using System;

namespace CT.BookingEngine
{
    public enum CabinClass
    {
        NoCabin=0,
        All = 1,
        Economy = 2,
        PremiumEconomy = 3,
        Business = 4,
        PremiumBusiness = 5,
        First = 6
    }
    [Serializable]
    public class FlightSegment
    {
        //TODO: change this to use citycodes
        string origin;
        string destination;
        DateTime preferredDepartureTime;
        DateTime preferredArrivalTime;
        CabinClass cabinClass;
        //TODO: change this
        string[] preferredAirlines;
        bool _NearByOriginPort;

        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }

        public DateTime PreferredDepartureTime
        {
            get
            {
                return preferredDepartureTime;
            }
            set
            {
                preferredDepartureTime = value;
            }
        }

        public DateTime PreferredArrivalTime
        {
            get
            {
                return preferredArrivalTime;
            }
            set
            {
                preferredArrivalTime = value;
            }
        }

        public CabinClass flightCabinClass
        {
            get
            {
                return cabinClass;
            }
            set
            {
                cabinClass = value;
            }
        }

        //TODO: not a good idea to expose an array as a property
        public string[] PreferredAirlines
        {
            get
            {
                return preferredAirlines;
            }
            set
            {
                preferredAirlines = value;
            }
        }
        public bool NearByOriginPort
        {
            get
            {
                return _NearByOriginPort;
            }
            set
            {
                _NearByOriginPort = value;
            }
        }

        public FlightSegment Copy()
        {
            FlightSegment segment = new FlightSegment();
            segment.cabinClass = cabinClass;
            segment.destination = destination;
            segment.origin = origin;
            if (preferredAirlines != null)
            {
                segment.preferredAirlines = (string[])preferredAirlines.Clone();
            }   // else nothing to assign. the value will remain null.
            segment.preferredArrivalTime = preferredArrivalTime;
            segment.preferredDepartureTime = preferredDepartureTime;
            segment.NearByOriginPort = NearByOriginPort;
            return segment;
        }
    }
}
