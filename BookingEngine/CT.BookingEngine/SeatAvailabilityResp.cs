﻿using System;
using System.Collections.Generic;

/// <summary>
/// Summary description for SeatAvailabilityResp
/// </summary>
[Serializable]
public class SeatAvailabilityResp
{
    public string StatusCode { get; set; }
    public List<SegmentSeats> SegmentSeat { get; set; }
}

[Serializable]
public class SegmentSeats
{
    public string SegmntID { get; set; }
    public string Origin { get; set; }
    public string Destination { get; set; }
    public string JourneyType { get; set; }
    public DateTime STD { get; set; }
    public DateTime STA { get; set; }
    public string FlightNo { get; set; }
    public string AircraftName { get; set; }
    public string NoOfSeatsAvailable { get; set; }
    public string TotalSeats { get; set; }
    public List<SeatInfoDetails> SeatInfoDetails { get; set; }
}

[Serializable]
public class SeatInfoDetails
{
    public int RowNo { get; set; }
    public string AvailablityType { get; set; }
    public string Description { get; set; }
    public string Code { get; set; }
    public string SeatType { get; set; }
    public string SeatNo { get; set; }
    public string SeatWayType { get; set; }
    public string Compartment { get; set; }
    public string Deck { get; set; }
    public decimal Price { get; set; }
    public decimal TAX { get; set; }
    public decimal Discount { get; set; }
    public string CurrencyCode { get; set; }
    public decimal BaseCurrencyPrice { get; set; }
    public decimal BaseCurrencyTAX { get; set; }
    public string BaseCurrencyCode { get; set; }
    public int SeatNoForCount { get; set; }
    public int SeatPriceGroup { get; set; }
    public List<SeatProperty> SeatProperty { get; set; }
}

[Serializable]
public class SeatProperty
{
    public string PropName { get; set; }
    public string PropValue { get; set; }
}