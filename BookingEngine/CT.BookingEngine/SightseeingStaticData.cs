using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using System.Net;
namespace CT.BookingEngine
{
    public class SightseeingStaticData
    {
        private string itemCode;
        private string cityCode;
        private string description;
        private string tourInfo;
        private string includes;
        private string departurePoint;
        private string flashLink;
        private string notes;
        private List<TourOperations> tourOperation;
        private SightseeingBookingSource source;
        private DateTime timeStamp;
        private string itemName;
        private string imageInfo;
        private string closedDates;
        private string excludes;
        private string cancellationPolicy;
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public SightseeingBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public string ImageInfo
        {
            get { return imageInfo; }
            set { imageInfo = value; }
        }
        public string TourInfo
        {
            get { return tourInfo; }
            set { tourInfo = value; }
        }
        public string Includes
        {
            get { return includes; }
            set { includes = value; }
        }
        public string DeparturePoint
        {
            get { return departurePoint; }
            set { departurePoint = value; }
        }
        public string FlashLink
        {
            get { return flashLink; }
            set { flashLink = value; }
        }
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        public DateTime TimeStamp
        {
            get { return timeStamp; }
            set { timeStamp = value; }
        }
        public List<TourOperations> TourOperationInfo
        {
            get { return tourOperation; }
            set { tourOperation = value; }
        }
        public string ClosedDates
        {
            get { return closedDates; }
            set { closedDates = value; }
        }
        public string Excludes
        {
            get { return excludes; }
            set { excludes = value; }
        }
        public string CancellationPolicy
        {
            get { return cancellationPolicy; }
            set { cancellationPolicy = value; }
        }

        /// <summary>
        /// This Method is used to save the Sightseeing Static Data
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("SightseeingStaticData.Save entered.");
            try
            {
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {
                    SqlParameter[] paramList = new SqlParameter[12];
                    paramList[0] = new SqlParameter("@itemCode", itemCode);
                    paramList[1] = new SqlParameter("@cityCode", cityCode);
                    paramList[2] = new SqlParameter("@description", description);
                    paramList[3] = new SqlParameter("@tour", tourInfo);
                    paramList[4] = new SqlParameter("@includes", includes);
                    paramList[5] = new SqlParameter("@departurePoint", departurePoint);
                    paramList[6] = new SqlParameter("@flashLink", flashLink);
                    paramList[7] = new SqlParameter("@notes", notes);
                    paramList[8] = new SqlParameter("@source", source);
                    paramList[9] = new SqlParameter("@itemName", itemName);
                    paramList[10] = new SqlParameter("@imageInfo", imageInfo);
                    paramList[11] = new SqlParameter("@closedDates", closedDates);
                    int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddSightseeingStaticData, paramList);
                    foreach (TourOperations tour in tourOperation)
                    {
                        tour.CityCode = cityCode;
                        tour.ItemCode = itemCode;
                        tour.Save();
                    }
                    updateTransaction.Complete();
                }
            }
            catch (Exception exp)
            {
                Audit.Add(EventType.SightseeingSearch, Severity.High, 1, "Exception in Saving Static Data. Message:" + exp.Message, "");
            }
            //Trace.TraceInformation("SightseeingStaticData.Save exiting");
        }

        public void Load(string itmCode, string ctyCode, SightseeingBookingSource src)
        {
            //Trace.TraceInformation("SightseeingStaticData.Load entered : hotelCode = " + itmCode + "|Source = " + src.ToString());
            if (itmCode != null && itmCode.Length == 0)
            {
                throw new ArgumentException("ItemCode Should be enterd !", "");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@itemCode", itmCode);
            paramList[1] = new SqlParameter("@cityCode", ctyCode);
            paramList[2] = new SqlParameter("@source", (int)src);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetSightseeingStaticDataByCode, paramList, connection);
            using (DataTable dataSight = DBGateway.FillDataTableSP(SPNames.GetSightseeingStaticDataByCode, paramList))
            {
                if (dataSight != null && dataSight.Rows.Count > 0)
                {
                    foreach(DataRow data in dataSight.Rows)
                    {
                        itemCode = itmCode;
                        cityCode = ctyCode;
                        source = (SightseeingBookingSource)Convert.ToInt16(data["source"]);
                        timeStamp = Convert.ToDateTime(data["timeStamp"]);
                        description =Convert.ToString(data["description"]);
                        tourInfo = Convert.ToString(data["tour"]);
                        includes = Convert.ToString(data["includes"]);
                        departurePoint = Convert.ToString(data["departurePoint"]);
                        flashLink = Convert.ToString(data["flashLink"]);
                        notes = Convert.ToString(data["notes"]);
                        itemName = Convert.ToString(data["itemName"]);
                        imageInfo = Convert.ToString(data["imageInfo"]);
                        closedDates = Convert.ToString(data["closedDates"]);
                        excludes = Convert.ToString(data["exclusions"]);
                        
                    }
                }
            }
            //data.Close();
            //connection.Close();
            TourOperations tour = new TourOperations();
            tourOperation = tour.Load(ctyCode, itmCode);
            //Trace.TraceInformation("SightseeingStaticData.Load exit :");
        }
        public void Update()
        {
            //Trace.TraceInformation("SightseeingStaticData.Update entered.");
            try
            {
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {
                    SqlParameter[] paramList = new SqlParameter[12];
                    paramList[0] = new SqlParameter("@itemCode", itemCode);
                    paramList[1] = new SqlParameter("@cityCode", cityCode);
                    paramList[2] = new SqlParameter("@description", description);
                    paramList[3] = new SqlParameter("@tour", tourInfo);
                    paramList[4] = new SqlParameter("@includes", includes);
                    paramList[5] = new SqlParameter("@departurePoint", departurePoint);
                    paramList[6] = new SqlParameter("@flashLink", flashLink);
                    paramList[7] = new SqlParameter("@notes", notes);
                    paramList[8] = new SqlParameter("@source", source);
                    paramList[9] = new SqlParameter("@itemName", itemName);
                    paramList[10] = new SqlParameter("@imageInfo", imageInfo);
                    paramList[11] = new SqlParameter("@closedDates", closedDates);
                    int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateSightseeingStaticData, paramList);
                    TourOperations tourDetails = new TourOperations();
                    tourDetails.CityCode = cityCode;
                    tourDetails.ItemCode = itemCode;
                    tourDetails.Delete();
                    foreach (TourOperations tour in tourOperation)
                    {
                        tour.CityCode = cityCode;
                        tour.ItemCode = itemCode;
                        tour.Save();
                    }
                    updateTransaction.Complete();
                }
            }
            catch (Exception exp)
            {
                Audit.Add(EventType.SightseeingSearch, Severity.High, 1, "Exception in Updating Static Data. Message:" + exp.Message, "");
            }
            //Trace.TraceInformation("SightseeingStaticData.Update exiting");

        }
        public string DownloadImage(string images, SightseeingBookingSource Source)
        {
            //Trace.TraceInformation("SightseeingStaticData.DownloadImage entered.");
            // Download image from given URL and save image name only in database.
            string imagesURL = string.Empty;
            string[] imgURL = images.Replace("\\", "/").Split('|');
            string imgPath = string.Empty;
            if (Source == SightseeingBookingSource.GTA)
            {
                imgPath = Configuration.ConfigurationSystem.GTAConfig["downloadImagesPath"].ToString() + "\\";
            }
           
            for (int k = 0; k < imgURL.Length - 1; k++)
            {
                WebClient Client = new WebClient();
                string guidValue = Guid.NewGuid().ToString();
                string[] fileName = imgURL[k].Split('/');
                string imageFileName = fileName[fileName.Length - 1];
                try
                {
                    Client.DownloadFile(imgURL[k], imgPath + guidValue + imageFileName);
                    imagesURL += guidValue + imageFileName + "|";
                }
                catch (Exception excep)
                {
                    Audit.Add(EventType.Exception, Severity.Low, 0, "HotelImage.DownloadImage Downloading Image Failed Message : " + excep.Message, "");
                }
            }
            //Trace.TraceInformation("SightseeingStaticData.DownloadImage Exit.");
            return imagesURL;
        }
    }


}
