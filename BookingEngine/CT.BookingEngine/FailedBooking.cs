using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;

namespace CT.BookingEngine
{
    /// <summary>
    /// Status
    /// </summary>
    public enum Status : int
    {
        /// <summary>
        /// NotSaved
        /// </summary>
        NotSaved = 0,
        /// <summary>
        /// Saved
        /// </summary>
        Saved = 1,
        /// <summary>
        /// Removed
        /// </summary>
        Removed = 2
    }
    /// <summary>
    /// This class is using to save the FailedBookings in DB and retrive the FailedBookings from DB
    /// </summary>
    public class FailedBooking
    {
        #region variables
        /// <summary>
        /// table unique id
        /// </summary>
        private int failedBookingId;
        /// <summary>
        /// Login userId
        /// </summary>
        private int memberId;
        /// <summary>
        /// ConfirmationNo
        /// </summary>
        private string pnr;
        /// Assignig UAPI UR
        string universalRecord=string.Empty;// For 
        /// Assignig UAPI Airline PNR (UAPI)
        string airLocatorCode = string.Empty;
        /// <summary>
        /// Login AgencyId
        /// </summary>
        private int agencyId;
        /// <summary>
        /// Created Date
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// currentStatus
        /// </summary>
        private Status currentStatus;
        /// <summary>
        /// lastModifiedOn
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// BookingSource
        /// </summary>
        private BookingSource source;
        /// <summary>
        /// Login AgencyName
        /// </summary>
        private string agencyName;
        /// <summary>
        /// 
        /// </summary>
        private string itineraryXML;
        private ProductType product;
        private string remarks=string.Empty;
        private HotelBookingSource hotelSource;
        private string userName;
        private string ticketxml;
        #endregion

        #region properities
        /// <summary>
        /// FailedBookingId
        /// </summary>
        public int FailedBookingId
        {
            get { return failedBookingId; }
            set { failedBookingId=value; }
        }
        /// <summary>
        /// MemberId
        /// </summary>
        public int MemberId
        {
            get { return memberId; }
            set { memberId = value; }
        }
        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public string UniversalRecord
        {
            get
            {
                return universalRecord;
            }
            set
            {
                universalRecord = value;
            }
        }

        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public string AirLocatorCode
        {
            get
            {
                return airLocatorCode;
            }
            set
            {
                airLocatorCode = value;
            }
        }
        /// <summary>
        /// PNR
        /// </summary>
        public string PNR
        {
            get { return pnr; }
            set { pnr = value; }
        }
        /// <summary>
        /// AgencyId
        /// </summary>
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }
        /// <summary>
        /// CreatedOn
        /// </summary>
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        /// <summary>
        /// CurretnStatus
        /// </summary>
        public Status CurretnStatus
        {
            get { return currentStatus; }
            set { currentStatus = value; }
        }
        /// <summary>
        /// Source
        /// </summary>
        public BookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        /// <summary>
        /// AgencyName
        /// </summary>
        public string AgencyName
        {
            get { return agencyName; }
            set { agencyName = value; }
        }
        /// <summary>
        /// ItineraryXML
        /// </summary>
        public string ItineraryXML
        {
            get { return itineraryXML; }
            set { itineraryXML = value; }
        }
        /// <summary>
        /// Product
        /// </summary>
        public ProductType Product
        {
            get { return product; }
            set { product = value; }
        }
        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        /// <summary>
        /// HotelSources
        /// </summary>
        public HotelBookingSource HotelSources
        {
            get { return hotelSource; }
            set { hotelSource = value; }
        }
        /// <summary>
        /// UserName
        /// </summary>
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        /// <summary>
        /// Ticketxml
        /// </summary>
        public string Ticketxml
        {
            get
            {
                return ticketxml;
            }

            set
            {
                ticketxml = value;
            }
        }
        #endregion
        /// <summary>
        /// This Method  is sued to Save the FailedBooking
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("FailedBooking.Save entered:");
            SqlParameter[] paramList = new SqlParameter[12];
            paramList[0] = new SqlParameter("@memberId", memberId);
            paramList[1] = new SqlParameter("@universalRecord", universalRecord);
            paramList[2] = new SqlParameter("@airLocatorCode", airLocatorCode);
            paramList[3] = new SqlParameter("@pnr", pnr);
            paramList[4] = new SqlParameter("@agencyId", agencyId);
            paramList[5] = new SqlParameter("@currentStatus", (int)currentStatus);
            paramList[6] = new SqlParameter("@source", (int)source);
            paramList[7] = new SqlParameter("@agencyName", agencyName);
            paramList[8] = new SqlParameter("@itineraryXML",itineraryXML);
            paramList[9] = new SqlParameter("@productTypeId", (int)product);
            paramList[10] = new SqlParameter("@ticketxml", ticketxml);
            if (!string.IsNullOrEmpty(remarks))paramList[10] = new SqlParameter("@remarks", remarks);
            DBGateway.ExecuteNonQuerySP(SPNames.AddFailedBooking, paramList);
            //Trace.TraceInformation("FailedBooking.Save exited");
        }
        /// <summary>
        /// This Method  is sued to Load the FailedBookings by pnrNo
        /// </summary>
        /// <param name="pnrNo">pnrNo</param>
        /// <returns>FailedBooking</returns>
        public static FailedBooking Load(string pnrNo)
        {
            //Trace.TraceInformation("FailedBooking.Load entered:");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@pnr", pnrNo);
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.GetFailedBooking, paramList, con);
            FailedBooking fb = new FailedBooking();
            if (dr.Read())
            {
                fb.failedBookingId = Convert.ToInt32(dr["failedBookingId"]);
                fb.universalRecord = Convert.ToString(dr["universalRecord"]);
                fb.airLocatorCode = Convert.ToString(dr["airLocatorCode"]);
                fb.pnr = Convert.ToString(dr["pnr"]);
                fb.pnr = Convert.ToString(dr["pnr"]);
                fb.failedBookingId = Convert.ToInt32(dr["failedBookingId"]);
                fb.agencyId = Convert.ToInt32(dr["agencyId"]);
                fb.memberId = Convert.ToInt32(dr["memberId"]);
                fb.createdOn = Convert.ToDateTime(dr["createdOn"]);
                fb.currentStatus = (Status)Enum.Parse(typeof(Status), dr["currentStatus"].ToString());
                fb.lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
                fb.agencyName = Convert.ToString(dr["agencyName"]);
                if (Convert.ToInt32(dr["ProductTypeId"]) == (int)ProductType.Flight)
                {
                    fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), dr["source"].ToString());
                }
                else
                {
                    fb.hotelSource = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), dr["source"].ToString());
                }
                fb.itineraryXML = Convert.ToString(dr["itineraryXML"]);
                fb.ticketxml= Convert.ToString(dr["ticketxml"]);

            }
            dr.Close();
            con.Close();
            //Trace.TraceInformation("FailedBooking.Load exited:");
            return fb;
        }
        /// <summary>
        /// This Method  is sued to Load the FailedBookings by pnrNo
        /// </summary>
        /// <param name="agencyIdNo">agencyIdNo</param>
        /// <param name="pnrNo">pnrNo</param>
        /// <returns>FailedBooking</returns>
        public static FailedBooking Load(int agencyIdNo, string pnrNo)
        {
            //Trace.TraceInformation("FailedBooking.LoadForAgency entered:");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyIdNo);
            paramList[1] = new SqlParameter("@pnr", pnrNo);
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.GetPNRFailedBookingForAgency, paramList, con);
            FailedBooking fb = new FailedBooking();
            if (dr.Read())
            {
                fb.failedBookingId = Convert.ToInt32(dr["failedBookingId"]);
                fb.universalRecord = Convert.ToString(dr["universalRecord"]);
                fb.airLocatorCode = Convert.ToString(dr["airLocatorCode"]);
                fb.pnr = Convert.ToString(dr["pnr"]);
                fb.failedBookingId = Convert.ToInt32(dr["failedBookingId"]);
                fb.agencyId = Convert.ToInt32(dr["agencyId"]);
                fb.memberId = Convert.ToInt32(dr["memberId"]);
                fb.createdOn = Convert.ToDateTime(dr["createdOn"]);
                fb.currentStatus = (Status)Enum.Parse(typeof(Status), dr["currentStatus"].ToString());
                fb.agencyName = Convert.ToString(dr["agencyName"]);
                fb.lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
                if (Convert.ToInt32(dr["ProductTypeId"]) == (int)ProductType.Flight)
                {
                    fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), dr["source"].ToString());
                }
                else
                {
                    fb.hotelSource = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), dr["source"].ToString());
                }
            }
            dr.Close();
            con.Close();
            //Trace.TraceInformation("FailedBooking.LoadForAgency exited:");
            return fb;
        }
        /// <summary>
        /// This Method  is sued to Load the FlightItinerary by pendingId
        /// </summary>
        /// <param name="pendingId">pendingId</param>
        /// <returns>FlightItinerary</returns>
        public static FlightItinerary LoadItinerary(int pendingId)
        {
            //Trace.TraceInformation("FailedBooking.LoadItinerary entered:");
            string strItinerary = string.Empty; ;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@failedBookingId", pendingId);
            // SqlConnection con = DBGateway.GetConnection();
            //SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.GetFailedBookingItinerary, paramList, con);
            using (DataTable dtFailed = DBGateway.FillDataTableSP(SPNames.GetFailedBookingItinerary, paramList))
            {
                if (dtFailed != null && dtFailed.Rows.Count > 0 && dtFailed.Rows[0]["itineraryXML"] != DBNull.Value)
                {
                    strItinerary = Convert.ToString(dtFailed.Rows[0]["itineraryXML"]);
                }
            }
            //dr.Close();
            //con.Close();
            //Trace.TraceInformation("FailedBooking.LoadItinerary exited:");
            return GetObject(strItinerary);
        }

        /// <summary>
        /// CheckPNR
        /// </summary>
        /// <param name="pnrNo">pnrNo</param>
        /// <returns>bool</returns>
        public static bool CheckPNR(string pnrNo)
        {
            //Trace.TraceInformation("FailedBooking.CheckPNR entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@pnr", pnrNo);
            paramList[1] = new SqlParameter("@isPresent", SqlDbType.Bit);
            paramList[1].Direction = ParameterDirection.Output;
            DBGateway.ExecuteNonQuerySP(SPNames.CheckFailedBookingPNR, paramList);
            //Trace.TraceInformation("FailedBooking.CheckPNR exited");
            return Convert.ToBoolean(paramList[1].Value);
        }
        /// <summary>
        /// This Method is used to Update Status
        /// </summary>
        /// <param name="pnrNo"></param>
        /// <param name="stat"></param>
        /// <param name="remarks"></param>
        public static void UpdateStatus(string pnrNo, Status stat, string remarks)
        {
            //Trace.TraceInformation("FailedBooking.UpdateStatus entered");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@pnr", pnrNo);
            paramList[1] = new SqlParameter("@currentStatus", (int)stat);
            paramList[2] = new SqlParameter("@remarks", remarks);
            DBGateway.ExecuteNonQuerySP(SPNames.UpdateFailedBookingStatus, paramList);
            //Trace.TraceInformation("FailedBooking.UpdateStatus exited");
        }
        /// <summary>
        /// GetAllFailedBooking
        /// </summary>
        /// <param name="lower">lower</param>
        /// <param name="upper">upper</param>
        /// <param name="productTypeId">productTypeId</param>
        /// <returns></returns>
        public static List<FailedBooking> GetAllFailedBooking(int lower, int upper,int productTypeId)
        {
            //Trace.TraceInformation("FailedBooking.GetAllFailedBooking entered");
            List<FailedBooking> lstFailedBooking = new List<FailedBooking>();

            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@lower", lower);
            paramList[1] = new SqlParameter("@upper", upper);
            paramList[2] = new SqlParameter("@ProductTypeId", productTypeId);
            //SqlConnection con = DBGateway.GetConnection();
            //SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.GetAllFailedBooking, paramList, con);
            using (DataTable dtFailed = DBGateway.FillDataTableSP(SPNames.GetAllFailedBooking, paramList))
            {
                if (dtFailed != null && dtFailed.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtFailed.Rows)
                    {
                        FailedBooking fb = new FailedBooking();
                        fb.pnr = Convert.ToString(dr["pnr"]);
                        if (Convert.ToInt32(dr["ProductTypeId"]) == (int)ProductType.Flight)
                        {
                            fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), Convert.ToString(dr["source"]));
                        }
                        else
                        {
                            fb.hotelSource = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), Convert.ToString(dr["source"]));
                        }
                        //fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), dr["source"].ToString());
                        fb.agencyId = Convert.ToInt32(dr["agencyId"]);
                        fb.memberId = Convert.ToInt32(dr["memberId"]);
                        fb.createdOn = Convert.ToDateTime(dr["createdOn"]);
                        fb.currentStatus = (Status)Enum.Parse(typeof(Status), Convert.ToString(dr["currentStatus"]));
                        fb.lastModifiedOn = (DateTime)dr["lastModifiedOn"];
                        fb.agencyName = Convert.ToString(dr["agencyName"]);
                        fb.universalRecord = Convert.ToString(dr["universalRecord"]);
                        fb.userName = Convert.ToString(dr["userName"]);
                        lstFailedBooking.Add(fb);
                    }
                }
            }
            //dr.Close();
            //con.Close();
            //Trace.TraceInformation("FailedBooking.GetAllFailedBooking exited");
            return lstFailedBooking;
        }
        /// <summary>
        /// GetCount
        /// </summary>
        /// <param name="productTypeId">productTypeId</param>
        /// <returns>int</returns>
        public static int GetCount(int productTypeId)
        {
            //Trace.TraceInformation("FailedBooking.GetCount entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@count", SqlDbType.Int);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@ProductTypeId", productTypeId);
            DBGateway.ExecuteNonQuerySP(SPNames.CountFailedBooking, paramList);
            ////Trace.TraceInformation("FailedBooking.GetCount exited");
            return (int)paramList[0].Value;

        }
        /// <summary>
        /// GetCount
        /// </summary>
        /// <param name="agencyIdNo">agencyIdNo</param>
        /// <param name="productTypeId">productTypeId</param>
        /// <returns>int</returns>
        public static int GetCount(int agencyIdNo,int productTypeId)
        {
            //Trace.TraceInformation("FailedBooking.GetCount entered");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@agencyId", agencyIdNo);
            paramList[1] = new SqlParameter("@count", SqlDbType.Int);
            paramList[1].Direction = ParameterDirection.Output;
            paramList[2] = new SqlParameter("@ProductTypeId", productTypeId);
            DBGateway.ExecuteNonQuerySP(SPNames.CountFailedBookingForAgency, paramList);
            //Trace.TraceInformation("FailedBooking.GetCount exited");
            return (int)paramList[1].Value;

        }
        /// <summary>
        /// Get AllFailedBooking
        /// </summary>
        /// <param name="agencyIdNo">agencyIdNo</param>
        /// <param name="lower">lower</param>
        /// <param name="upper">upper</param>
        /// <param name="productTypeId">productTypeId</param>
        /// <returns> List FailedBooking object</returns>
        public static List<FailedBooking> GetAllFailedBooking(int agencyIdNo, int lower, int upper,int productTypeId)
        {
            //Trace.TraceInformation("FailedBooking.GetFailedBookingForAgency entered");
            List<FailedBooking> lstFailedBooking = new List<FailedBooking>();
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@agencyId", agencyIdNo);
            paramList[1] = new SqlParameter("@lower", lower);
            paramList[2] = new SqlParameter("@upper", upper);
            paramList[3] = new SqlParameter("@ProductTypeId", productTypeId);
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.GetFailedBookingForAgency, paramList, con);
            while (dr.Read())
            {
                FailedBooking fb = new FailedBooking();
                fb.failedBookingId=Convert.ToInt32(dr["failedBookingId"]);
                fb.pnr = Convert.ToString(dr["pnr"]);
                if (Convert.ToInt32(dr["ProductTypeId"]) == (int)ProductType.Flight)
                {
                    fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), dr["source"].ToString());
                }
                else
                {
                    fb.hotelSource = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), dr["source"].ToString());
                }
                fb.agencyId = Convert.ToInt32(dr["agencyId"]);
                fb.memberId = Convert.ToInt32(dr["memberId"]);
                fb.createdOn = Convert.ToDateTime(dr["createdOn"]);
                fb.currentStatus = (Status)Enum.Parse(typeof(Status), dr["currentStatus"].ToString());
                fb.lastModifiedOn = (DateTime)dr["lastModifiedOn"];
                fb.agencyName = Convert.ToString(dr["agencyName"]);
                fb.itineraryXML=Convert.ToString(dr["itineraryXML"]);
                fb.universalRecord = dr["universalRecord"].ToString();
                fb.userName=dr["userName"].ToString();
                lstFailedBooking.Add(fb);
            }
            dr.Close();
            con.Close();
            //Trace.TraceInformation("FailedBooking.GetFailedBookingForAgency exited");
            return lstFailedBooking;
        }
        /// <summary>
        /// GetXML
        /// </summary>
        /// <param name="itinerary">itinerary</param>
        /// <returns>string</returns>
        public static string GetXML(FlightItinerary itinerary)
        {
            itinerary.ProductType = ProductType.Flight;//Assign ProductType                        
            //Trace.TraceInformation("FailedBooking.GetXML entered:");
            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(typeof(FlightItinerary));
            MemoryStream stream = new MemoryStream();
            try
            {
                serializer.Serialize(stream, itinerary);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml.Trim();
            }
            catch(Exception ex)
            {
                try
                {
                    string messageText = "PNR = " + itinerary.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n. PNR created successfully but itinerary failed to serialize.";
                    string message = Util.GetExceptionInformation(ex, "Itinerary Serialization failed.\r\n" + messageText);
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, message, string.Empty);
                    CT.Core.Email.Send("Itinerary Serialization Failed", message);
                }
                catch
                {
                }
                return "Object can't be serailized";
            }
            finally
            {
                stream.Close();
                //Trace.TraceInformation("FailedBooking.GetXML exited");
            }
        }
        /// <summary>
        /// GetXML
        /// </summary>
        /// <param name="ticket">ticket</param>
        /// <param name="PNR">PNR</param>
        /// <param name="FlightBookingSource">FlightBookingSource</param>
        /// <returns>string</returns>
        public static string GetXML(Ticket[] ticket,string PNR,BookingSource FlightBookingSource)
        {          
            //Trace.TraceInformation("FailedBooking.GetXML entered:");
            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(typeof(Ticket[]));
            MemoryStream stream = new MemoryStream();
            try
            {
                serializer.Serialize(stream, ticket);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml.Trim();
            }
            catch (Exception ex)
            {
                try
                {
                    string messageText = "PNR = " + PNR + "\r\n Booking Source is " + FlightBookingSource.ToString() + "\r\n. PNR created successfully but itinerary failed to serialize.";
                    string message = Util.GetExceptionInformation(ex, "Itinerary Serialization failed.\r\n" + messageText);
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, message, string.Empty);
                    CT.Core.Email.Send("Itinerary Serialization Failed", message);
                }
                catch
                {
                }
                return "Object can't be serailized";
            }
            finally
            {
                stream.Close();
                //Trace.TraceInformation("FailedBooking.GetXML exited");
            }
        }
        /// <summary>
        /// GetXML
        /// </summary>
        /// <param name="itinerary">HotelItinerary</param>
        /// <returns>string</returns>
        public static string GetXML(HotelItinerary itinerary)
        {
            //Trace.TraceInformation("FailedBooking.GetXML entered:");
            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(typeof(HotelItinerary));
            MemoryStream stream = new MemoryStream();
            try
            {
                serializer.Serialize(stream, itinerary);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml.Trim();
            }
            catch (Exception ex)
            {
                try
                {
                    string messageText = "Confirmation No = " + itinerary.ConfirmationNo+ "\r\n Booking Source is " + itinerary.Source.ToString() + "\r\n. PNR created successfully but itinerary failed to serialize.";
                    string message = Util.GetExceptionInformation(ex, "Itinerary Serialization failed.\r\n" + messageText);
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, message, string.Empty);
                    CT.Core.Email.Send("Hotel Itinerary Serialization Failed", message);
                }
                catch
                {
                }
                return "Object can't be serailized";
            }
            finally
            {
                stream.Close();
                //Trace.TraceInformation("FailedBooking.GetXML exited");
            }
        }
        /// <summary>
        /// GetXML
        /// </summary>
        /// <param name="itinerary">itinerary</param>
        /// <returns>string</returns>
        public static string GetXML(SightseeingItinerary itinerary)
        {
            //Trace.TraceInformation("FailedBooking.GetXML entered:");
            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(typeof(SightseeingItinerary));
            MemoryStream stream = new MemoryStream();
            try
            {
                serializer.Serialize(stream, itinerary);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml.Trim();
            }
            catch (Exception ex)
            {
                try
                {
                    string messageText = "Confirmation No = " + itinerary.ConfirmationNo + "\r\n Booking Source is " + itinerary.Source.ToString() + "\r\n. PNR created successfully but itinerary failed to serialize.";
                    string message = Util.GetExceptionInformation(ex, "Itinerary Serialization failed.\r\n" + messageText);
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, message, string.Empty);
                    CT.Core.Email.Send("SightSeeing Itinerary Serialization Failed", message);
                }
                catch
                {
                }
                return "Object can't be serailized";
            }
            finally
            {
                stream.Close();
                //Trace.TraceInformation("FailedBooking.GetXML exited");
            }
        }
        //Added by brahmamm FaildBooking Xml Genarate
        /// <summary>
        /// GetXML
        /// </summary>
        /// <param name="itinerary">itinerary</param>
        /// <returns>string</returns>
        public static string GetXML(TransferItinerary itinerary)
        {
            ////Trace.TraceInformation("FailedBooking.GetXML entered:");
            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(typeof(TransferItinerary));
            MemoryStream stream = new MemoryStream();
            try
            {
                serializer.Serialize(stream, itinerary);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml.Trim();
            }
            catch (Exception ex)
            {
                try
                {
                    string messageText = "Confirmation No = " + itinerary.ConfirmationNo + "\r\n Booking Source is " + itinerary.Source.ToString() + "\r\n. PNR created successfully but itinerary failed to serialize.";
                    string message = Util.GetExceptionInformation(ex, "Itinerary Serialization failed.\r\n" + messageText);
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, message, string.Empty);
                    CT.Core.Email.Send("Transfers Itinerary Serialization Failed", message);
                }
                catch
                {
                }
                return "Object can't be serailized";
            }
            finally
            {
                stream.Close();
                ////Trace.TraceInformation("FailedBooking.GetXML exited");
            }
        }
        //Added by brahma genarating xml
        /// <summary>
        /// GetXML
        /// </summary>
        /// <param name="itinerary">itinerary</param>
        /// <returns>string</returns>
        public static string GetXML(FleetItinerary itinerary)
        {
            //Trace.TraceInformation("FailedBooking.GetXML entered:");
            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(typeof(FleetItinerary));
            MemoryStream stream = new MemoryStream();
            try
            {
                serializer.Serialize(stream, itinerary);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml.Trim();
            }
            catch (Exception ex)
            {
                try
                {
                    string messageText = "Confirmation No = " + itinerary.BookingRefNo + "\r\n Booking Source is " + itinerary.Source.ToString() + "\r\n. PNR created successfully but itinerary failed to serialize.";
                    string message = Util.GetExceptionInformation(ex, "Itinerary Serialization failed.\r\n" + messageText);
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, message, string.Empty);
                    CT.Core.Email.Send("Fleet Itinerary Serialization Failed", message);
                }
                catch
                {
                }
                return "Object can't be serailized";
            }
            finally
            {
                stream.Close();
                //Trace.TraceInformation("FailedBooking.GetXML exited");
            }
        }
        /// <summary>
        /// GetObject
        /// </summary>
        /// <param name="itinerary">itinerary</param>
        /// <returns>FlightItinerary</returns>
        public static FlightItinerary GetObject(string itinerary)
        {
            StringReader strReader = null;
            strReader = new StringReader(itinerary);
            XmlSerializer serializer = new XmlSerializer(typeof(FlightItinerary));
            XmlReader xmlRead = new XmlTextReader(strReader);
            try
            {
                FlightItinerary fltItinerary = (FlightItinerary)serializer.Deserialize(xmlRead);
                return fltItinerary;
            }
            catch
            {
                return null;
            }
            finally
            {
                strReader.Close();
                xmlRead.Close();
            }
        }
        /// <summary>
        /// GenerateItinerary
        /// </summary>
        /// <param name="fbItinerary">fbItinerary</param>
        /// <returns>HotelItinerary</returns>
        public static HotelItinerary GenerateItinerary(string fbItinerary)
        {
            HotelItinerary itinerery = new HotelItinerary();
            XmlNode tempNode;
            TextReader stringRead = new StringReader(fbItinerary);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);

            XmlNodeList hotelInfo = xmlDoc.SelectNodes("HotelItinerary");
            if (hotelInfo.Count == 0)
            {
                //Trace.TraceError("No Hotel Details found. Response not coming! . Response =" + fbItinerary);
                throw new BookingEngineException("<br> No Hotel Details found!. Response not coming ");
            }
            else
            {
                foreach (XmlNode hInfo in hotelInfo)
                {
                    tempNode = hInfo.SelectSingleNode("ProductTypeId");
                    if (tempNode != null)
                    {
                        itinerery.ProductTypeId = Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("ProductId");
                    if (tempNode != null)
                    {
                        itinerery.ProductId = Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("BookingMode");
                    if (tempNode != null)
                    {
                        string temp = tempNode.InnerText;
                        switch (temp)
                        {
                            case "Auto": itinerery.BookingMode = BookingMode.Auto;
                                break;
                            case "Manual": itinerery.BookingMode = BookingMode.Manual;
                                break;
                            case "Import": itinerery.BookingMode = BookingMode.Import;
                                break;
                            case "WhiteLabel": itinerery.BookingMode = BookingMode.WhiteLabel;
                                break;
                            case "BookingAPI": itinerery.BookingMode = BookingMode.BookingAPI;
                                break;
                            case "Itimes": itinerery.BookingMode = BookingMode.Itimes;
                                break;
                            case "ManualImport": itinerery.BookingMode = BookingMode.ManualImport;
                                break;
                        }
                    }
                    tempNode = hInfo.SelectSingleNode("ProductType");
                    if (tempNode != null)
                    {
                        string temp = tempNode.InnerText;
                        switch (temp)
                        {
                            case "Hotel": itinerery.ProductType = ProductType.Hotel;
                                break;
                            case "Flight": itinerery.ProductType = ProductType.Flight;
                                break;
                                //for time being i worte for hotel and flight.
                        }
                    }
                    tempNode = hInfo.SelectSingleNode("ConfirmationNo");
                    if (tempNode != null)
                    {
                        itinerery.ConfirmationNo = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("HotelId");
                    if (tempNode != null)
                    {
                        itinerery.HotelId = Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("HotelCode");
                    if (tempNode != null)
                    {
                        itinerery.HotelCode = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("HotelName");
                    if (tempNode != null)
                    {
                        itinerery.HotelName = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("HotelCategory");
                    if (tempNode != null)
                    {
                        itinerery.HotelCategory = tempNode.InnerText;
                    }

                    tempNode = hInfo.SelectSingleNode("Rating");
                    if (tempNode != null)
                    {
                        string temp = tempNode.InnerText;
                        switch (temp)
                        {
                            case "OneStar": itinerery.Rating = HotelRating.OneStar; itinerery.HotelCategory = "Economy";
                                break;
                            case "TwoStar": itinerery.Rating = HotelRating.TwoStar; itinerery.HotelCategory = "Budget";
                                break;
                            case "ThreeStar": itinerery.Rating = HotelRating.ThreeStar; itinerery.HotelCategory = "Standard";
                                break;
                            case "FourStar": itinerery.Rating = HotelRating.FourStar; itinerery.HotelCategory = "Superior";
                                break;
                            case "FiveStar": itinerery.Rating = HotelRating.FiveStar; itinerery.HotelCategory = "Luxury";
                                break;
                        }
                    }
                    tempNode = hInfo.SelectSingleNode("StartDate");
                    if (tempNode != null)
                    {
                        itinerery.StartDate = Convert.ToDateTime(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("EndDate");
                    if (tempNode != null)
                    {
                        itinerery.EndDate = Convert.ToDateTime(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("HotelAddress1");
                    if (tempNode != null)
                    {
                        itinerery.HotelAddress1 = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("CityCode");
                    if (tempNode != null)
                    {
                        itinerery.CityCode = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("CityRef");
                    if (tempNode != null)
                    {
                        itinerery.CityRef = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("Status");
                    if (tempNode != null)
                    {
                        string temp = tempNode.InnerText;
                        switch (temp)
                        {
                            case "Failed": itinerery.Status = HotelBookingStatus.Failed;
                                break;
                            case "Confirmed": itinerery.Status = HotelBookingStatus.Confirmed;
                                break;
                            case "Cancelled": itinerery.Status = HotelBookingStatus.Cancelled;
                                break;
                            case "Pending": itinerery.Status = HotelBookingStatus.Pending;
                                break;
                            case "Error": itinerery.Status = HotelBookingStatus.Error;
                                break;
                        }
                    }
                    tempNode = hInfo.SelectSingleNode("SpecialRequest");
                    if (tempNode != null)
                    {
                        itinerery.SpecialRequest = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("HotelPolicyDetails");
                    if (tempNode != null)
                    {
                        itinerery.HotelPolicyDetails = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("HotelCancelPolicy");
                    if (tempNode != null)
                    {
                        itinerery.HotelCancelPolicy = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("NoOfRooms");
                    if (tempNode != null)
                    {
                        itinerery.NoOfRooms = Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("CreatedBy");
                    if (tempNode != null)
                    {
                        itinerery.CreatedBy = Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("CreatedOn");
                    if (tempNode != null)
                    {
                        itinerery.CreatedOn = Convert.ToDateTime(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("LastModifiedBy");
                    if (tempNode != null)
                    {
                        itinerery.LastModifiedBy = Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("LastModifiedOn");
                    if (tempNode != null)
                    {
                        itinerery.LastModifiedOn = Convert.ToDateTime(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("Source");
                    if (tempNode != null)
                    {
                        string temp = tempNode.InnerText;
                        switch (temp)
                        {
                            case "DOTW": itinerery.Source = HotelBookingSource.DOTW;
                                break;
                            case "HotelConnect": itinerery.Source = HotelBookingSource.HotelConnect;
                                break;
                            case "RezLive": itinerery.Source = HotelBookingSource.RezLive;
                                break;
                            case "LOH": itinerery.Source = HotelBookingSource.LOH;
                                break;
                            case "Desiya": itinerery.Source = HotelBookingSource.Desiya;
                                break;
                            case "GTA": itinerery.Source = HotelBookingSource.GTA;
                                break;
                            case "HotelBeds": itinerery.Source = HotelBookingSource.HotelBeds;
                                break;
                            case "JAC": itinerery.Source = HotelBookingSource.JAC;
                                break;
                            case "Miki": itinerery.Source = HotelBookingSource.Miki;
                                break;
                            case "TBOHotel": itinerery.Source = HotelBookingSource.TBOHotel;
                                break;
                            case "WST": itinerery.Source = HotelBookingSource.WST;
                                break;
                            //case "EET": itinerery.Source = HotelBookingSource.EET;
                            //    break;
                            //for time being i wrote for some sources only.

                            //Added By somasekhar on 18/09/2018 for 
                            case "Yatra": itinerery.Source = HotelBookingSource.Yatra;
                                    break;
                            //Added By Somasekhar on 11/12/2018 for OYO
                            case "OYO":
                                itinerery.Source = HotelBookingSource.OYO;
                                break; 
                        }
                    }
                    tempNode = hInfo.SelectSingleNode("IsDomestic");
                    if (tempNode != null)
                    {
                        itinerery.IsDomestic = Convert.ToBoolean(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("CancelId");
                    if (tempNode != null)
                    {
                        itinerery.CancelId = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("FlightInfo");
                    if (tempNode != null)
                    {
                        itinerery.FlightInfo = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("BookingRefNo");
                    if (tempNode != null)
                    {
                        itinerery.BookingRefNo = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("Map");
                    if (tempNode != null)
                    {
                        itinerery.Map = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("LastCancellationDate");
                    if (tempNode != null)
                    {
                        // To get Last Cancellation Date of Bookoing  -- for OYO
                        if (hInfo.SelectSingleNode("Source").InnerText == "OYO")
                        {                            
                            //=======   for finding Date (dd-MMM-yyyy format -- 13 Dec 2018 / 13 DEC 2018  )  ==============
                            Regex rgx = new Regex(@"\d{2}\s*[A-Za-z]{3}\s*\d{4}");
                            Match mat = rgx.Match(itinerery.HotelCancelPolicy);
                            if (!string.IsNullOrEmpty(mat.ToString()))
                            {
                                itinerery.LastCancellationDate = Convert.ToDateTime( mat.ToString());
                            }
                            else {
                                itinerery.LastCancellationDate = itinerery.StartDate;
                            }
                        }
                        //====================================
                        if (hInfo.SelectSingleNode("Source").InnerText == "Yatra")
                        {
                            //=======   for finding Date (dd-MMM-yy format -- 09-Dec-18 / 09-Dec-18  )  ==============
                            Regex rgx = new Regex(@"\d{2}-[A-Za-z]{3}-\d{2}");
                            Match mat = rgx.Match(itinerery.HotelCancelPolicy);
                            if (!string.IsNullOrEmpty(mat.ToString()))
                            {
                                itinerery.LastCancellationDate = Convert.ToDateTime(mat.ToString());
                            }
                            else
                            {
                                itinerery.LastCancellationDate = itinerery.StartDate;
                            }
                        }
                        //====================================
                        else
                        {
                            //itinerery.LastCancellationDate = Convert.ToDateTime(tempNode.InnerText);
                            itinerery.LastCancellationDate = itinerery.StartDate;
                        }
                    }
                    tempNode = hInfo.SelectSingleNode("VoucherStatus");
                    if (tempNode != null)
                    {
                        itinerery.VoucherStatus = Convert.ToBoolean(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("AgencyReference");
                    if (tempNode != null)
                    {
                        itinerery.AgencyReference = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("VatDescription");
                    if (tempNode != null)
                    {
                        itinerery.VatDescription = tempNode.InnerText;
                    }
                    //tempNode = hInfo.SelectSingleNode("CCInfo");
                    //if (tempNode != null)
                    //{
                    //    itinerery.CCInfo = "abc";   //pending
                    //}
                    tempNode = hInfo.SelectSingleNode("PassengerNationality");
                    if (tempNode != null)
                    {
                        itinerery.PassengerNationality = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("PassengerCountryOfResidence");
                    if (tempNode != null)
                    {
                        itinerery.PassengerCountryOfResidence = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("PaymentGuaranteedBy");
                    if (tempNode != null)
                    {
                        itinerery.PaymentGuaranteedBy = tempNode.InnerText;
                    }
                    tempNode = hInfo.SelectSingleNode("LocationId");
                    if (tempNode != null)
                    {
                        itinerery.LocationId = Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("AgencyId");
                    if (tempNode != null)
                    {
                        itinerery.AgencyId = Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hInfo.SelectSingleNode("TransType");
                    if (tempNode != null)
                    {
                        itinerery.TransType = tempNode.InnerText;
                    }

                    XmlNodeList roomTypes = hInfo.SelectNodes("Roomtype");
                    foreach (XmlNode rmtype in roomTypes)
                    {
                        XmlNodeList roomsInfo = rmtype.SelectNodes("HotelRoom");
                        HotelRoom[] hotelRooms = new HotelRoom[roomsInfo.Count];
                        int i = 0;
                        foreach (XmlNode hotelRoom in roomsInfo)
                        {
                            HotelRoom room = new HotelRoom();
                            tempNode = hotelRoom.SelectSingleNode("RoomCapacityInfo");
                            if (tempNode != null)
                            {
                                room.RoomCapacityInfo = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("RoomPaxCapacity");
                            if (tempNode != null)
                            {
                                room.RoomPaxCapacity = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("AllowedAdultsWithoutChildren");
                            if (tempNode != null)
                            {
                                room.AllowedAdultsWithoutChildren = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("AllowedAdultsWithChildren");
                            if (tempNode != null)
                            {
                                room.AllowedAdultsWithChildren = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("MaxExtraBed");
                            if (tempNode != null)
                            {
                                room.MaxExtraBed = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("RateType");
                            if (tempNode != null)
                            {
                                string temp = tempNode.InnerText;
                                switch (temp)
                                {
                                    case "DOTW": room.RateType = RoomRateType.DOTW;
                                        break;
                                    case "DYNAMIC_3rd_PARTY": room.RateType = RoomRateType.DYNAMIC_3rd_PARTY;
                                        break;
                                    case "DYNAMIC_DIRECT": room.RateType = RoomRateType.DYNAMIC_DIRECT;
                                        break;
                                }
                            }
                            tempNode = hotelRoom.SelectSingleNode("CurrencyId");
                            if (tempNode != null)
                            {
                                room.CurrencyId = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("NonRefundable");
                            if (tempNode != null)
                            {
                                room.NonRefundable = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("AllowExtraMeals");
                            if (tempNode != null)
                            {
                                room.AllowExtraMeals = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("AllowSpecialRequests");
                            if (tempNode != null)
                            {
                                room.AllowSpecialRequests = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("AllowSpecials");
                            if (tempNode != null)
                            {
                                room.AllowSpecials = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("PassengerNamesRequiredForBooking");
                            if (tempNode != null)
                            {
                                room.PassengerNamesRequiredForBooking = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("AmmendRestricted");
                            if (tempNode != null)
                            {
                                room.AmmendRestricted = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("CancelRestricted");
                            if (tempNode != null)
                            {
                                room.CancelRestricted = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("NoShowPolicy");
                            if (tempNode != null)
                            {
                                room.NoShowPolicy = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("IncludedAdditionalService");
                            if (tempNode != null)
                            {
                                room.IncludedAdditionalService = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("RoomId");
                            if (tempNode != null)
                            {
                                room.RoomId = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("HotelId");
                            if (tempNode != null)
                            {
                                room.HotelId = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("RoomTypeCode");
                            if (tempNode != null)
                            {
                                room.RoomTypeCode = tempNode.InnerText;
                            }
                            tempNode = hotelRoom.SelectSingleNode("RatePlanCode");
                            if (tempNode != null)
                            {
                                room.RatePlanCode = tempNode.InnerText;
                            }
                            tempNode = hotelRoom.SelectSingleNode("NoOfUnits");
                            if (tempNode != null)
                            {
                                room.NoOfUnits = tempNode.InnerText;
                            }
                            tempNode = hotelRoom.SelectSingleNode("AdultCount");
                            if (tempNode != null)
                            {
                                room.AdultCount = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("ChildCount");
                            if (tempNode != null)
                            {
                                room.ChildCount = Convert.ToInt32(tempNode.InnerText);
                            }

                            //ChildAges Loading........   Added by brahmam  12.Nov.2015
                            XmlNode childAge = hotelRoom.SelectSingleNode("ChildAge");
                            List<int> childAges = new List<int>();
                            if(childAges.Count>0)
                            { 
                            foreach (XmlNode age in childAge.SelectNodes("int"))
                            {
                                if (age != null)
                                {
                                    childAges.Add(Convert.ToInt32(age.InnerText));
                                }   
                                room.ChildAge = childAges;
                            }
                            }
                            tempNode = hotelRoom.SelectSingleNode("RoomName");
                            if (tempNode != null)
                            {
                                room.RoomName = tempNode.InnerText;
                            }

                            XmlNodeList amenity = hotelRoom.SelectNodes("Ameneties");
                            List<string> amenities = new List<string>();
                            foreach (XmlNode ameniti in amenity)
                            {
                                tempNode = ameniti.SelectSingleNode("string");
                                if (tempNode != null)
                                {
                                    amenities.Add(tempNode.InnerText);
                                }
                                room.Ameneties = amenities;
                            }
                            tempNode = hotelRoom.SelectSingleNode("PriceId");
                            if (tempNode != null)
                            {
                                room.PriceId = Convert.ToInt32(tempNode.InnerText);
                            }

                            
                            XmlNodeList RoomFareBreakDowns = hotelRoom.SelectNodes("RoomFareBreakDown");

                            foreach (XmlNode RoomFareBreakDown in RoomFareBreakDowns)
                            {
                                int j = 0;
                                XmlNodeList HotelRoomFareBreakDowns = RoomFareBreakDown.SelectNodes("HotelRoomFareBreakDown");
                                HotelRoomFareBreakDown[] HRoomFareBD = new HotelRoomFareBreakDown[HotelRoomFareBreakDowns.Count];
                                foreach (XmlNode HotelRoomFareBreakDown in HotelRoomFareBreakDowns)
                                {
                                    HotelRoomFareBreakDown obj = new HotelRoomFareBreakDown();
                                    tempNode = HotelRoomFareBreakDown.SelectSingleNode("RoomId");
                                    if (tempNode != null)
                                    {
                                        obj.RoomId = Convert.ToInt32(tempNode.InnerText);
                                    }
                                    tempNode = HotelRoomFareBreakDown.SelectSingleNode("Date");
                                    if (tempNode != null)
                                    {
                                        obj.Date = Convert.ToDateTime(tempNode.InnerText);
                                    }
                                    tempNode = HotelRoomFareBreakDown.SelectSingleNode("RoomPrice");
                                    if (tempNode != null)
                                    {
                                        obj.RoomPrice = Convert.ToDecimal(tempNode.InnerText);
                                    }
                                    HRoomFareBD[j] = obj;
                                    j++;
                                }
                                room.RoomFareBreakDown = HRoomFareBD;
                            }
                            

                            tempNode = hotelRoom.SelectSingleNode("ExtraGuestCharge");
                            if (tempNode != null)
                            {
                                room.ExtraGuestCharge = Convert.ToDecimal(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("ChildCharge");
                            if (tempNode != null)
                            {
                                room.ChildCharge = Convert.ToDecimal(tempNode.InnerText);
                            }

                            
                            XmlNodeList priceNodes = hotelRoom.SelectNodes("Price");
                            foreach (XmlNode price in priceNodes)
                            {
                                PriceAccounts priceObj = new PriceAccounts();
                                tempNode = price.SelectSingleNode("CommissionType");
                                if (tempNode!= null)
                                {
                                    priceObj.CommissionType = tempNode.InnerText;
                                }
                                tempNode = price.SelectSingleNode("AirlineTransFee");
                                if (tempNode != null)
                                {
                                    priceObj.AirlineTransFee = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("TdsCommission");
                                if (tempNode != null)
                                {
                                    priceObj.TdsCommission = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("TDSPLB");
                                if (tempNode != null)
                                {
                                    priceObj.TDSPLB = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("PriceId");
                                if (tempNode != null)
                                {
                                    priceObj.PriceId = Convert.ToInt32(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("PublishedFare");
                                if (tempNode != null)
                                {
                                    priceObj.PublishedFare = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("NetFare");
                                if (tempNode != null)
                                {
                                    priceObj.NetFare = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("Markup");
                                if (tempNode != null)
                                {
                                    priceObj.Markup = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("SeviceTax");
                                if (tempNode != null)
                                {
                                    priceObj.SeviceTax = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("CessTax");
                                if (tempNode != null)
                                {
                                    priceObj.CessTax = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("OurCommission");
                                if (tempNode != null)
                                {
                                    priceObj.OurCommission = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("OurPLB");
                                if (tempNode != null)
                                {
                                    priceObj.OurPLB = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("AgentCommission");
                                if (tempNode != null)
                                {
                                    priceObj.AgentCommission = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("AgentPLB");
                                if (tempNode != null)
                                {
                                    priceObj.AgentPLB = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("OtherCharges");
                                if (tempNode != null)
                                {
                                    priceObj.OtherCharges = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("Tax");
                                if (tempNode != null)
                                {
                                    priceObj.Tax = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("WhiteLabelDiscount");
                                if (tempNode != null)
                                {
                                    priceObj.WhiteLabelDiscount = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("TransactionFee");
                                if (tempNode != null)
                                {
                                    priceObj.TransactionFee = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("Currency");
                                if (tempNode != null)
                                {
                                    priceObj.Currency = tempNode.InnerText;
                                }
                                tempNode = price.SelectSingleNode("AccPriceType");
                                if (tempNode != null)
                                {
                                    string temp = tempNode.InnerText;
                                    switch(temp)
                                    {
                                        case "NetFare": priceObj.AccPriceType = PriceType.NetFare;
                                            break;
                                        case "PublishedFare": priceObj.AccPriceType = PriceType.PublishedFare;
                                            break;
                                    }
                                }
                                tempNode = price.SelectSingleNode("RateOfExchange");
                                if (tempNode != null)
                                {
                                    priceObj.RateOfExchange = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("CurrencyCode");
                                if (tempNode != null)
                                {
                                    priceObj.CurrencyCode = tempNode.InnerText;
                                }
                                tempNode = price.SelectSingleNode("AdditionalTxnFee");
                                if (tempNode != null)
                                {
                                    priceObj.AdditionalTxnFee = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("WLCharge");
                                if (tempNode != null)
                                {
                                    priceObj.WLCharge = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("Discount");
                                if (tempNode != null)
                                {
                                    priceObj.Discount = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("ReverseHandlingCharge");
                                if (tempNode != null)
                                {
                                    priceObj.ReverseHandlingCharge = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("YQTax");
                                if (tempNode != null)
                                {
                                    priceObj.YQTax = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("IsServiceTaxOnBaseFarePlusYQ");
                                if (tempNode != null)
                                {
                                    priceObj.IsServiceTaxOnBaseFarePlusYQ = Convert.ToBoolean(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("TdsRate");
                                if (tempNode != null)
                                {
                                    priceObj.TdsRate = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("BaggageCharge");
                                if (tempNode != null)
                                {
                                    priceObj.BaggageCharge = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("AsvAmount");
                                if (tempNode != null)
                                {
                                    priceObj.AsvAmount = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("AsvElement");
                                if (tempNode != null)
                                {
                                    priceObj.AsvType = tempNode.InnerText;
                                }
                                tempNode = price.SelectSingleNode("SupplierPrice");
                                if (tempNode != null)
                                {
                                    priceObj.SupplierPrice = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("SupplierCurrency");
                                if (tempNode != null)
                                {
                                    priceObj.SupplierCurrency = tempNode.InnerText;
                                }
                                tempNode = price.SelectSingleNode("MarkupValue");
                                if (tempNode != null)
                                {
                                    priceObj.MarkupValue = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("MarkupType");
                                if (tempNode != null)
                                {
                                    priceObj.MarkupType = tempNode.InnerText;
                                }
                                tempNode = price.SelectSingleNode("DiscountValue");
                                if (tempNode != null)
                                {
                                    priceObj.DiscountValue =Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("DecimalPoint");
                                if (tempNode != null)
                                {
                                    priceObj.DecimalPoint = Convert.ToInt32(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("InsuranceAmount");
                                if (tempNode != null)
                                {
                                    priceObj.InsuranceAmount = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("B2CMarkup");
                                if (tempNode != null)
                                {
                                    priceObj.B2CMarkup = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("B2CMarkupType");
                                if (tempNode != null)
                                {
                                    priceObj.B2CMarkupType = tempNode.InnerText;
                                }
                                tempNode = price.SelectSingleNode("B2CMarkupValue");
                                if (tempNode != null)
                                {
                                    priceObj.B2CMarkupValue = Convert.ToDecimal(tempNode.InnerText);
                                }
                                tempNode = price.SelectSingleNode("B2CCreditCardCharges");
                                if (tempNode != null)
                                {
                                    priceObj.B2CCreditCardCharges = Convert.ToDecimal(tempNode.InnerText);
                                }
                                room.Price = priceObj;
                            }
                            

                            tempNode = hotelRoom.SelectSingleNode("PreviousFare");
                            if (tempNode != null)
                            {
                                room.PreviousFare = Convert.ToDecimal(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("ExtraBed");
                            if (tempNode != null)
                            {
                                room.ExtraBed = Convert.ToBoolean(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("NoOfExtraBed");
                            if (tempNode != null)
                            {
                                room.NoOfExtraBed = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("NoOfCots");
                            if (tempNode != null)
                            {
                                room.NoOfCots = Convert.ToInt32(tempNode.InnerText);
                            }
                            tempNode = hotelRoom.SelectSingleNode("SharingBed");
                            if (tempNode != null)
                            {
                                room.SharingBed = Convert.ToBoolean(tempNode.InnerText);
                            }


                            XmlNodeList Passengers = hotelRoom.SelectNodes("PassenegerInfo");
                           List<HotelPassenger>  PassengerInfo = new List<HotelPassenger>();
                            
                            foreach(XmlNode passenger in Passengers)
                            {
                                XmlNodeList hotelPassengers = passenger.SelectNodes("HotelPassenger");
                                foreach(XmlNode hotelPassenger in  hotelPassengers)
                                {
                                    HotelPassenger passengerObj = new HotelPassenger();
                                    tempNode = hotelPassenger.SelectSingleNode("PaxId");
                                    if (tempNode != null)
                                    {
                                        passengerObj.PaxId = Convert.ToInt32(tempNode.InnerText);
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("HotelId");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.HotelId = Convert.ToInt32(tempNode.InnerText);
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Title");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.Title = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Firstname");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.Firstname = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Lastname");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.Lastname = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("NationalityCode");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.NationalityCode = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Email");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.Email = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Nationality");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.Nationality = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("PaxType");
                                    if(tempNode!= null)
                                    {
                                        string temp = tempNode.InnerText;

                                        switch(temp)
                                        {
                                            case "Adult": passengerObj.PaxType = HotelPaxType.Adult;
                                                break;
                                            case "Child": passengerObj.PaxType = HotelPaxType.Child;
                                                break;
                                        }
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("RoomId");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.RoomId = Convert.ToInt32(tempNode.InnerText);
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("LeadPassenger");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.LeadPassenger = Convert.ToBoolean(tempNode.InnerText);
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Age");
                                    if(tempNode!= null)
                                    {
                                        passengerObj.Age = Convert.ToInt32(tempNode.InnerText);
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Employee");
                                    if (tempNode != null)
                                    {
                                        passengerObj.Employee = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Division");
                                    if (tempNode != null)
                                    {
                                        passengerObj.Division = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Department");
                                    if (tempNode != null)
                                    {
                                        passengerObj.Department = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("Purpose");
                                    if (tempNode != null)
                                    {
                                        passengerObj.Purpose = tempNode.InnerText;
                                    }
                                    tempNode = hotelPassenger.SelectSingleNode("EmployeeID");
                                    if (tempNode != null)
                                    {
                                        passengerObj.EmployeeID = tempNode.InnerText;
                                    }
                                    PassengerInfo.Add(passengerObj);
                                }
                                room.PassenegerInfo = PassengerInfo;
                                //hotelRooms[i].PassenegerInfo = room.PassenegerInfo;
                            }
                            
                            
                            tempNode = hotelRoom.SelectSingleNode("MealPlanDesc");
                            if (tempNode != null)
                            {
                                room.MealPlanDesc = tempNode.InnerText;
                            }
                            hotelRooms[i] = room;
                            i++;
                            
                        }
                        
                        itinerery.Roomtype = hotelRooms;
                    }
                }
            }
            return itinerery;
        }
    }
}
