﻿using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.BookingEngine
{
    public class AgodaStatic
    {
        #region variables
        string hotelCode;
        string cityCode;
        int infantAge;
        int childAgeFrom;
        int childAgeTo;
        bool childStayFee;
        int minGuestAge;
        int source;
        #endregion
        #region properities
        public string HotelCode
        {
            get
            {
                return hotelCode;
            }

            set
            {
                hotelCode = value;
            }
        }

        public string CityCode
        {
            get
            {
                return cityCode;
            }

            set
            {
                cityCode = value;
            }
        }

        public int InfantAge
        {
            get
            {
                return infantAge;
            }

            set
            {
                infantAge = value;
            }
        }

        public int ChildAgeFrom
        {
            get
            {
                return childAgeFrom;
            }

            set
            {
                childAgeFrom = value;
            }
        }

        public int ChildAgeTo
        {
            get
            {
                return childAgeTo;
            }

            set
            {
                childAgeTo = value;
            }
        }

        public bool ChildStayFee
        {
            get
            {
                return childStayFee;
            }

            set
            {
                childStayFee = value;
            }
        }

        public int MinGuestAge
        {
            get
            {
                return minGuestAge;
            }

            set
            {
                minGuestAge = value;
            }
        }

        public int Source
        {
            get
            {
                return source;
            }

            set
            {
                source = value;
            }
        }
        #endregion

        public void Save()
        {
            SqlParameter[] paramList = new SqlParameter[8];
            paramList[0] = new SqlParameter("@P_HotelCode", hotelCode);
            paramList[1] = new SqlParameter("@P_CityCode", cityCode);
            paramList[2] = new SqlParameter("@P_Infant_age", infantAge);
            paramList[3] = new SqlParameter("@P_Children_age_from", childAgeFrom);
            paramList[4] = new SqlParameter("@P_Children_age_to",childAgeTo);
            paramList[5] = new SqlParameter("@P_Children_stay_free", childStayFee);
            paramList[6] = new SqlParameter("@P_Min_guest_age", minGuestAge);
            paramList[7] = new SqlParameter("@P_Source", source);
            int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AgodaChildPolicy_addupdate", paramList);
        }
    }
}
