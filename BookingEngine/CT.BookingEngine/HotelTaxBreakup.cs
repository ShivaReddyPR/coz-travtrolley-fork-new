﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine
{
    public class HotelTaxBreakup
    {

        #region Variables
        /// <summary>
        /// This variable is used,to get hotelId
        /// </summary>
        int hotelId;
        /// <summary>
        /// This variable is used,to get roomId
        /// </summary>
        int roomId;
        /// <summary>
        /// This variable is used,to get taxName
        /// </summary>
        string taxName;
        /// <summary>
        /// This variable is used,to get taxValue
        /// </summary>
        decimal taxValue;
        /// <summary>
        /// This variable is used,to get isIncluded
        /// </summary>
        bool isIncluded;
        /// <summary>
        /// This variable is used,to get unitType
        /// </summary>
        string unitType;
        /// <summary>
        /// This variable is used,to get ismandatory
        /// </summary>
        bool isMandatory;
        /// <summary>
        /// This variable is used,to get FrequencyType
        /// </summary>
        string frequencyType;
        #endregion
        #region Properties
        /// <summary>
        /// This variable is used,to get hotelId
        /// </summary>
        public int HotelId { get => hotelId; set => hotelId = value; }
        /// <summary>
        /// This variable is used,to get roomId
        /// </summary>
        public int RoomId { get => roomId; set => roomId = value; }
        public string TaxName { get => taxName; set => taxName = value; }
        public decimal TaxValue { get => taxValue; set => taxValue = value; }
        public bool IsIncluded { get => isIncluded; set => isIncluded = value; }
        public string UnitType { get => unitType; set => unitType = value; }
        public bool IsMandatory { get => isMandatory; set => isMandatory = value; }
        public string FrequencyType { get => frequencyType; set => frequencyType = value; }



        #endregion
        #region public Methods
        public void Save()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@P_HotelId", hotelId));
            parameters.Add(new SqlParameter("@P_RoomId", roomId));
            parameters.Add(new SqlParameter("@P_TaxName", taxName));
            parameters.Add(new SqlParameter("@P_TaxValue", taxValue));
            parameters.Add(new SqlParameter("@P_IsIncluded", isIncluded));
            parameters.Add(new SqlParameter("@P_UnitType", unitType));
            parameters.Add(new SqlParameter("@P_IsMandatory", isMandatory));
            parameters.Add(new SqlParameter("@P_FrequencyType", frequencyType));
            DBGateway.ExecuteQuery("usp_addhoteltaxbreakup", parameters.ToArray());
        }
        /// <summary>
        /// This Method is used to load all Passenger details of a particular Room
        /// </summary>
        /// <param name="hotelId">hotelId</param>
        /// <param name="roomId">roomId</param>
        /// <returns>List HotelPassenger</returns>
        public List<HotelTaxBreakup> Load(int hotelId, int roomId)
        {
            
            //Trace.TraceInformation("HotelPassenger.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId should be positive integer", "hotelId");
            }
            List<HotelTaxBreakup> hotelTaxList = new List<HotelTaxBreakup>();
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            paramList[1] = new SqlParameter("@roomId", roomId);

            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelPassengerByHotelIdRoomId, paramList,connection);
            using (DataTable dtHotelTax = DBGateway.FillDataTableSP(SPNames.GetHotelTaxBreakup, paramList))
            {
                if (dtHotelTax !=null && dtHotelTax.Rows.Count > 0)
                {
                    //while (data.Read())
                    foreach (DataRow data in dtHotelTax.Rows)
                    {
                        HotelTaxBreakup taxInfo = new HotelTaxBreakup();
                        if (data["HotelID"] != DBNull.Value) taxInfo.HotelId = Convert.ToInt32(data["HotelID"]);
                        if (data["RoomID"] != DBNull.Value) taxInfo.RoomId = Convert.ToInt32(data["RoomID"]);
                        if (data["TaxName"] != DBNull.Value) taxInfo.TaxName = Convert.ToString(data["TaxName"]);
                        if (data["UnitType"] != DBNull.Value) taxInfo.UnitType = Convert.ToString(data["UnitType"]);
                        if (data["TaxValue"] != DBNull.Value) taxInfo.TaxValue = Convert.ToDecimal(data["TaxValue"]);
                        if (data["IsIncluded"] != DBNull.Value) taxInfo.IsIncluded = Convert.ToBoolean(data["IsIncluded"]);
                        if (data["IsMandatory"] != DBNull.Value) taxInfo.IsMandatory = Convert.ToBoolean(data["IsMandatory"]);
                        if (data["FrequencyType"] != DBNull.Value) taxInfo.FrequencyType = Convert.ToString(data["FrequencyType"]);




                        hotelTaxList.Add(taxInfo);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("HotelPassenger.Load exiting.");
            return hotelTaxList;
        }
        #endregion
    }
}
