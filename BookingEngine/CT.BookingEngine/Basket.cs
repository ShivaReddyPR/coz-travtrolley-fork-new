using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using CT.Configuration;

namespace CT.BookingEngine
{
    //Basket to handle session data
    [Serializable]
    public class FlightSessionData
    {
        #region Member Variables
        /// <summary>
        /// The final result obtained as result of flight searach.
        /// </summary>
        private SearchResult[] result; //ziya-todo
        /// <summary>
        /// The final result obtained as result of flight searach.
        /// </summary>
        //[XmlIgnore] ziya-todo
        public SearchResult[] Result
        {
            get { return result; }
            set { result = value; }
        }

        /// <summary>
        /// Timestamp when search was made.
        /// </summary>
        private DateTime searchTime;
        /// <summary>
        /// Timestamp when search was made.
        /// </summary>
        public DateTime SearchTime
        {
            get { return searchTime; }
            set { searchTime = value; }
        }

        /// <summary>
        /// FlightItinerary being booked.
        /// </summary>
        private FlightItinerary itinerary; //ziya-todo
        /// <summary>
        /// FlightItinerary being booked.
        /// </summary>
        public FlightItinerary Itinerary
        {
            get { return itinerary; }
            set { itinerary = value; }
        }

        /// <summary>
        /// Event and activity log.
        /// </summary>
        private List<string> log;
        /// <summary>
        /// Event and activity log.
        /// </summary>
        public List<string> Log
        {
            get { return log; }
            set { log = value; }
        }

        /// <summary>
        /// List of xml messages sent and received from sources.
        /// </summary>
        private List<string> xmlMessage;
        /// <summary>
        /// List of xml messages sent and received from sources.
        /// Sample Key - 1A:Search, SG:Book, 6E:FareQuote, 1P:Ticket etc.
        /// </summary>
        public List<string> XmlMessage
        {
            get { return xmlMessage; }
            set { xmlMessage = value; }
        }

        /// <summary>
        /// Exceptions that came accross
        /// </summary>
        private List<string> excep;
        /// <summary>
        /// Exceptions that came accross
        /// </summary>
        public List<string> Excep
        {
            get { return excep; }
            set { excep = value; }
        }

        /// <summary>
        /// This flag is set true if session dump is to be placed in a file.
        /// </summary>
        private bool dumpOnClean;
        /// <summary>
        /// This flag is set true if session dump is to be placed in a file.
        /// </summary>
        public bool DumpOnClean
        {
            get { return dumpOnClean; }
            set { dumpOnClean = value; }
        }
        #endregion

        public FlightSessionData()
        {
            this.log = new List<string>();
            this.excep = new List<string>();
            this.xmlMessage = new List<string>();
        }
    }
    [Serializable]
    public class Basket
    {

        public static BookingSessionThreadSafeDic BookingSession = new BookingSessionThreadSafeDic();        
        /// <summary>
        /// Dictionary holding FlightResultClass object, with key as session id.
        /// </summary>
        public static FlightBookingSessionThreadSafeDic FlightBookingSession = new FlightBookingSessionThreadSafeDic();
       
        public static InsuranceBookingSessionThreadSafeDic InsuranceBookingSession = new InsuranceBookingSessionThreadSafeDic();
        private static DateTime lastCleaned = DateTime.Now;
        public static DateTime LastCleaned
        {
            get { return lastCleaned; }
            set { lastCleaned = value; }
        }

        /// <summary>
        /// To clean Basket having expired sessions
        /// </summary>
        public static void Clean()
        {
            lastCleaned = DateTime.Now;
            bool dumpError = false;
            bool dumpAll = false;
            double sessionExpiryMinutes = 15;
            if (ConfigurationSystem.BookingEngineConfig.ContainsKey("sessionExpiryTime"))
            {
                sessionExpiryMinutes = Convert.ToDouble(ConfigurationSystem.BookingEngineConfig["sessionExpiryTime"]);
            }
            if (ConfigurationSystem.BookingEngineConfig.ContainsKey("dumpError"))
            {
                dumpError = Convert.ToBoolean(ConfigurationSystem.BookingEngineConfig["dumpError"]);
            }
            List<string> expiryInsuranceKeyList = InsuranceBookingSession.GetExpiredKeyList();
            if (ConfigurationSystem.BookingEngineConfig.ContainsKey("dumpAll"))
            {
                dumpAll = Convert.ToBoolean(ConfigurationSystem.BookingEngineConfig["dumpAll"]);
            }
            TimeSpan sessionExpiryTime = TimeSpan.FromMinutes(sessionExpiryMinutes);
            List<string> expiredKeyList = FlightBookingSession.GetExpiredKeyList();
            foreach (string expiredKey in expiredKeyList)
            {
                /*if (FlightBookingSession.ContainsKey(expiredKey) && (((FlightBookingSession[expiredKey].DumpOnClean || FlightBookingSession[expiredKey].Itinerary != null) && dumpError) || dumpAll)) ziya-todo
                {
                    FlightBookingSession[expiredKey].Log.Add("Dumping expired key.");
                    DumpSession(expiredKey);
                }
                if (FlightBookingSession.ContainsKey(expiredKey) && FlightBookingSession[expiredKey].SearchTime + sessionExpiryTime < DateTime.Now)
                {
                    BookingSession.Remove(expiredKey);
                    FlightBookingSession.Remove(expiredKey);
                }*/
            }
            foreach (string expiredKey in expiryInsuranceKeyList)
            {
                if (InsuranceBookingSession.ContainsKey(expiredKey) && InsuranceBookingSession[expiredKey].Time + sessionExpiryTime < DateTime.Now)
                {
                    InsuranceBookingSession.Remove(expiredKey);
                }
            }
        }
        public static void DumpSession(string sessionId)
        {
            try
            {
                if (Basket.FlightBookingSession.ContainsKey(sessionId))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(FlightSessionData));
                    TextWriter tw = new StreamWriter(ConfigurationSystem.BookingEngineConfig["dumpFolder"] + @"\" + sessionId + ".xml");
                    xs.Serialize(tw, (object)FlightBookingSession[sessionId]);
                }
            }

            catch (Exception) { }   // just the thread should not crash if serialization fails.
        }
    }
    [Serializable]
    public class BookingSessionThreadSafeDic 
    {
        private Dictionary<string, Dictionary<string, object>> BookingSession = new Dictionary<string, Dictionary<string, object>>();
         
        /// <summary>
        /// This is an indexer for dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Dictionary<string, object> this[string key]
        {
            get
            {
                lock (BookingSession)
                {
                    return BookingSession[key];
                }
            }
            set
            {
                lock (BookingSession)
                {
                    BookingSession[key] = value;
                }
            }
        }
        /// <summary>
        /// This method will return the count of the dictinary
        /// </summary>
        public int Count
        {
            get
            {
                int count = 0;
                lock (BookingSession)
                {
                    count = BookingSession.Count;
                }
                return count;
            }
        }
        /// <summary>
        /// This method will add value in dictionary 
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        public void Add(string Key, Dictionary<string, object> value)
        {
            lock (BookingSession)
            {
                BookingSession.Add(Key, value);
            }
        }
        /// <summary>
        /// This method will remove the key from the dictionary
        /// </summary>
        /// <param name="Key"></param>
        public void Remove(string key)
        {
            lock (BookingSession)
            {
                if (BookingSession.ContainsKey(key))
                {
                    BookingSession.Remove(key);
                }
            }
        }
        /// <summary>
        /// This method will check whether the required key is present in dictionary or not
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool ContainsKey(string key)
        {
            lock (BookingSession)
            {
                return BookingSession.ContainsKey(key);
            }
        }

    }
    [Serializable]
    public class FlightBookingSessionThreadSafeDic 
    {
        private Dictionary<string, FlightSessionData> FlightBookingSession = new Dictionary<string, FlightSessionData>();
        /// <summary>
        /// This is an indexer for dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public FlightSessionData this[string key]
        {
            get
            {
                lock (FlightBookingSession)
                {
                    return FlightBookingSession[key];
                }
            }
            set
            {
                lock (FlightBookingSession)
                {
                    FlightBookingSession[key] = value;
                }
            }
        }      
        /// <summary>
        /// This method will return the count of the dictinary
        /// </summary>
        public int Count
        {
            get
            {
                int count = 0;
                lock (FlightBookingSession)
                {
                    count = FlightBookingSession.Count;
                }
                return count;
            }
        }
        /// <summary>
        /// This method will add value in dictionary 
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        public void Add(string key, FlightSessionData value)
        {
            lock (FlightBookingSession)
            {
                FlightBookingSession.Add(key, value);
            }
        }
        /// <summary>
        /// This method will remove the key from the dictionary
        /// </summary>
        /// <param name="Key"></param>
        public void Remove(string key)
        {
            lock (FlightBookingSession)
            {
                if (FlightBookingSession.ContainsKey(key))
                {
                    FlightBookingSession.Remove(key);
                }
            }
        }
        /// <summary>
        /// This method will check whether the required key is present in dictionary or not
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool ContainsKey(string key)
        {
            lock (FlightBookingSession)
            {
                return FlightBookingSession.ContainsKey(key);
            }
        }
        public List<string> GetExpiredKeyList()
        {
            List<string> expiredKeyList = new List<string>();            
            double sessionExpiryMinutes = 15;
            if (ConfigurationSystem.BookingEngineConfig.ContainsKey("sessionExpiryTime"))
            {
                sessionExpiryMinutes = Convert.ToDouble(ConfigurationSystem.BookingEngineConfig["sessionExpiryTime"]);
            }
            TimeSpan sessionExpiryTime = TimeSpan.FromMinutes(sessionExpiryMinutes);
            lock (FlightBookingSession)
            {
                foreach (KeyValuePair<string, FlightSessionData> basketItem in FlightBookingSession)
                {
                    if (basketItem.Value.SearchTime + sessionExpiryTime < DateTime.Now)
                    {
                        expiredKeyList.Add(basketItem.Key);
                    }
                }
            }
            return expiredKeyList;
        }
    }
    [Serializable]
    public class InsuranceSessionData
    {
        private object searchResult;
        private DateTime time;

        public object SearchResult
        {
            get
            {
                return searchResult;
            }
            set
            {
                searchResult = value;
            }
        }
        public DateTime Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }
    }
    [Serializable]
    public class InsuranceBookingSessionThreadSafeDic
    {
        private Dictionary<string, InsuranceSessionData> InsuranceBookingSession = new Dictionary<string, InsuranceSessionData>();
        /// <summary>
        /// This is an indexer for dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public InsuranceSessionData this[string key]
        {
            get
            {
                lock (InsuranceBookingSession)
                {
                    return InsuranceBookingSession[key];
                }
            }
            set
            {
                lock (InsuranceBookingSession)
                {
                    InsuranceBookingSession[key] = value;
                }
            }
        }
        /// <summary>
        /// This method will return the count of the dictinary
        /// </summary>
        public int Count
        {
            get
            {
                int count = 0;
                lock (InsuranceBookingSession)
                {
                    count = InsuranceBookingSession.Count;
                }
                return count;
            }
        }
        /// <summary>
        /// This method will add value in dictionary 
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        public void Add(string key, InsuranceSessionData value)
        {
            lock (InsuranceBookingSession)
            {
                InsuranceBookingSession.Add(key, value);
            }
        }
        /// <summary>
        /// This method will remove the key from the dictionary
        /// </summary>
        /// <param name="Key"></param>
        public void Remove(string key)
        {
            lock (InsuranceBookingSession)
            {
                if (InsuranceBookingSession.ContainsKey(key))
                {
                    InsuranceBookingSession.Remove(key);
                }
            }
        }
        /// <summary>
        /// This method will check whether the required key is present in dictionary or not
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool ContainsKey(string key)
        {
            lock (InsuranceBookingSession)
            {
                return InsuranceBookingSession.ContainsKey(key);
            }
        }
        public List<string> GetExpiredKeyList()
        {
            List<string> expiredKeyList = new List<string>();
            double sessionExpiryMinutes = 15;
            if (ConfigurationSystem.BookingEngineConfig.ContainsKey("sessionExpiryTime"))
            {
                sessionExpiryMinutes = Convert.ToDouble(ConfigurationSystem.BookingEngineConfig["sessionExpiryTime"]);
            }
            TimeSpan sessionExpiryTime = TimeSpan.FromMinutes(sessionExpiryMinutes);
            lock (InsuranceBookingSession)
            {
                foreach (KeyValuePair<string, InsuranceSessionData> basketItem in InsuranceBookingSession)
                {
                    if (basketItem.Value.Time + sessionExpiryTime < DateTime.Now)
                    {
                        expiredKeyList.Add(basketItem.Key);
                    }
                }
            }
            return expiredKeyList;
        }
    }
}


