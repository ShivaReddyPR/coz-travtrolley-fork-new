﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Data.SqlClient;
using System.Transactions;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public enum CarBookingSource
    {
        Sayara = 1
    }
    public enum CarBookingStatus
    {
        Confirmed = 1,
        Cancelled = 2,
        Failed = 0,
        Pending = 3,
        Error = 4
    }
    [Serializable]
    public class FleetItinerary : CT.BookingEngine.Product
    {
        int fleetId;
        string fromCityCode;
        string fromCity;
        string fromLocationCode;
        string fromLocation;
        DateTime fromDate;
        string toCityCode;
        string toCity;
        string toLocationCode;
        string toLocation;
        DateTime toDate;
        string fleetType;
        string fleetName;
        RentType rentType;
        decimal cdw=-1;
        decimal pai=-1;
        decimal scdw=-1;
        decimal gps=-1;
        decimal cseat=-1;
        decimal vmd=-1;
        decimal kmrest;
        decimal kmrate;
        decimal chauffer=-1;
        decimal emirates_Link_Charge=-1;
        string bookingRefNo;
        int vehTariffId;
        int vehId;
        CarBookingStatus bookingStatus;
        bool voucherStatus;
        int locationId;
        int agencyId;
        string transType;
        int createdBy;
        DateTime createdOn;
        int modifiedBy;
        DateTime modifiedOn;
        PriceAccounts price;
        string cancelId;
        CarBookingSource source;
        decimal airportCharge=-1;
        string currency;
        string returnStatus;
        List<FleetPassenger> passengerInfo;
        string status;
        SerializableDictionary<string, decimal> sourceServiceAmount;
        decimal ins_Excess;
        string remarks;
        #region properities
        public int FleetId
        {
            get { return fleetId; }
            set { fleetId = value; }
        }
        public string FromCityCode
        {
            get { return fromCityCode; }
            set { fromCityCode = value; }
        }
        public string FromCity
        {
            get { return fromCity; }
            set { fromCity = value; }
        }
        public string FromLocationCode
        {
            get { return fromLocationCode; }
            set { fromLocationCode = value; }
        }
        public string FromLocation
        {
            get { return fromLocation; }
            set { fromLocation = value; }
        }
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        public string ToCityCode
        {
            get { return toCityCode; }
            set { toCityCode = value; }
        }
        public string ToCity
        {
            get { return toCity; }
            set { toCity = value; }
        }
        public string ToLocationCode
        {
            get { return toLocationCode; }
            set { toLocationCode = value; }
        }
        public string ToLocation
        {
            get { return toLocation; }
            set { toLocation = value; }
        }
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        public string FleetName
        {
            get { return fleetName; }
            set { fleetName = value; }
        }
        public RentType RentType
        {
            get { return rentType; }
            set { rentType = value; }
        }
        public decimal CDW
        {
            get { return cdw; }
            set { cdw = value; }
        }
        public decimal PAI
        {
            get { return pai; }
            set { pai = value; }
        }
        public decimal SCDW
        {
            get { return scdw; }
            set { scdw = value; }
        }
        public decimal GPS
        {
            get { return gps; }
            set { gps = value; }
        }
        public decimal CSEAT
        {
            get { return cseat; }
            set { cseat = value; }
        }
        public decimal VMD
        {
            get { return vmd; }
            set { vmd = value; }
        }
        public decimal KMREST
        {
            get { return kmrest; }
            set { kmrest = value; }
        }
        public decimal KMRATE
        {
            get { return kmrate; }
            set { kmrate = value; }
        }
        public decimal CHAUFFER
        {
            get { return chauffer; }
            set { chauffer = value; }
        }
        public decimal Emirates_Link_Charge
        {
            get { return emirates_Link_Charge; }
            set { emirates_Link_Charge = value; }
        }
        public string BookingRefNo
        {
            get { return bookingRefNo; }
            set { bookingRefNo = value; }
        }
        public int VehTariffId
        {
            get { return vehTariffId; }
            set { vehTariffId = value; }
        }
        public int VehId
        {
            get { return vehId; }
            set { vehId = value; }
        }
        public CarBookingStatus BookingStatus
        {
            get { return bookingStatus; }
            set { bookingStatus = value; }
        }
        public bool VoucherStatus
        {
            get { return voucherStatus; }
            set { voucherStatus = value; }
        }
        public int LocationId
        {
            get { return locationId; }
            set { locationId = value; }
        }
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }
        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }
        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }
        public PriceAccounts Price
        {
            get { return price; }
            set { price = value; }
        }
        public string CancelId
        {
            get { return cancelId; }
            set { cancelId = value; }
        }
        public CarBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public decimal AirportCharge
        {
            get { return airportCharge; }
            set { airportCharge = value; }
        }
        public string FleetType
        {
            get { return fleetType; }
            set { fleetType = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public string ReturnStatus
        {
            get { return returnStatus; }
            set { returnStatus = value; }
        }
        public List<FleetPassenger> PassengerInfo
        {
            get { return passengerInfo; }
            set { passengerInfo = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public SerializableDictionary<string, decimal> SourceServiceAmount
        {
            get { return sourceServiceAmount; }
            set { sourceServiceAmount = value; }
        }
        public decimal Ins_Excess
        {
            get { return ins_Excess; }
            set { ins_Excess = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        #endregion

          /// <summary>
        /// This Method is used to Load the Fleet Details based on Fleet ID.
        /// </summary>
        /// <param name="transferId"></param>
        #region Methods

        public override void Save(Product prod)
        {
            try
            {
                FleetItinerary itinearary = (FleetItinerary)prod;
                SqlParameter[] paramList = new SqlParameter[41];

                using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    //itinerary before only saving price
                    itinearary.Price.Save();

                    paramList[0] = new SqlParameter("@fleetId", itinearary.FleetId);
                    paramList[1] = new SqlParameter("@fromCityCode", itinearary.FromCityCode);
                    paramList[2] = new SqlParameter("@fromCity", itinearary.FromCity);
                    paramList[3] = new SqlParameter("@fromLocationCode", itinearary.fromLocationCode);
                    paramList[4] = new SqlParameter("@fromLocation", itinearary.FromLocation);
                    paramList[5] = new SqlParameter("@fromDate", itinearary.FromDate);
                    if(!string.IsNullOrEmpty(itinearary.ToCityCode)) paramList[6] = new SqlParameter("@toCityCode", itinearary.ToCityCode);
                    if (!string.IsNullOrEmpty(itinearary.ToCity)) paramList[7] = new SqlParameter("@toCity", itinearary.ToCity);
                    if (!string.IsNullOrEmpty(itinearary.ToLocationCode)) paramList[8] = new SqlParameter("@toLocationCode", itinearary.ToLocationCode);
                    if (!string.IsNullOrEmpty(itinearary.ToLocation)) paramList[9] = new SqlParameter("@toLocation", itinearary.ToLocation);
                    paramList[10] = new SqlParameter("@toDate", itinearary.ToDate);
                    paramList[11] = new SqlParameter("@fleetName", itinearary.FleetName);
                    paramList[12] = new SqlParameter("@fleetType", itinearary.FleetType);
                    paramList[13] = new SqlParameter("@rentType", itinearary.RentType);
                    paramList[14] = new SqlParameter("@CDW", itinearary.CDW);
                    paramList[15] = new SqlParameter("@PAI", itinearary.PAI);
                    paramList[16] = new SqlParameter("@SCDW", itinearary.SCDW);
                    paramList[17] = new SqlParameter("@GPS", itinearary.GPS);
                    paramList[18] = new SqlParameter("@CSEAT", itinearary.CSEAT);
                    paramList[19] = new SqlParameter("@VMD", itinearary.VMD);
                    paramList[20] = new SqlParameter("@KMREST", itinearary.KMREST);
                    paramList[21] = new SqlParameter("@KMRATE", itinearary.KMRATE);
                    paramList[22] = new SqlParameter("@CHAUFFER", itinearary.CHAUFFER);
                    paramList[23] = new SqlParameter("@AirportCharge", itinearary.AirportCharge);
                    paramList[24] = new SqlParameter("@Emirates_Link_Charge", itinearary.Emirates_Link_Charge);
                    paramList[25] = new SqlParameter("@bookingRefNo", itinearary.bookingRefNo);
                    paramList[26] = new SqlParameter("@supVehTariffId", itinearary.vehTariffId);
                    paramList[27] = new SqlParameter("@supVehId", itinearary.VehId);
                    paramList[28] = new SqlParameter("@BookingStatus", itinearary.BookingStatus);
                    paramList[29] = new SqlParameter("@voucherStatus", itinearary.VoucherStatus);
                    paramList[30] = new SqlParameter("@locationId", itinearary.LocationId);
                    paramList[31] = new SqlParameter("@agencyid", itinearary.AgencyId);
                    paramList[32] = new SqlParameter("@TransType", itinearary.TransType);
                    paramList[33] = new SqlParameter("@cancelId", itinearary.CancelId);
                    paramList[34] = new SqlParameter("@source", itinearary.Source);
                    paramList[35] = new SqlParameter("@priceId", itinearary.Price.PriceId);
                    paramList[36] = new SqlParameter("@createdBy", itinearary.CreatedBy);
                    paramList[37] = new SqlParameter("@currency", itinearary.Currency);
                    paramList[38] = new SqlParameter("@returnStatus", itinearary.ReturnStatus);
                    paramList[39] = new SqlParameter("@status", "A");
                    paramList[40] = new SqlParameter("@InsExcess", itinearary.ins_Excess);
                    paramList[0].Direction = ParameterDirection.Output;

                    int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddFleetItinerary, paramList);
                    fleetId = (int)paramList[0].Value;
                    if (itinearary.passengerInfo != null)
                    {
                        foreach (FleetPassenger paxDet in itinearary.PassengerInfo)
                        {
                            paxDet.FleetId = fleetId;
                            paxDet.ClientId = itinearary.passengerInfo[0].ClientId;
                            paxDet.AgentId = itinearary.agencyId;
                            paxDet.Save();
                        }
                    }
                    updateTransaction.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Load(int fleetId)
        {
            if (fleetId <= 0)
            {
                throw new ArgumentException("Fleet Id should be positive integer");
            }
            this.fleetId = fleetId;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@fleetId", fleetId);
            try
            {
                PriceAccounts priceData = new PriceAccounts();
                //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetFleetItinerary, paramList, connection);
                using (DataTable dtItinerary = DBGateway.FillDataTableSP(SPNames.GetFleetItinerary, paramList))
                {
                    //if (data.Read())
                    if (dtItinerary != null)
                    {
                        foreach (DataRow data in dtItinerary.Rows)
                        {
                            fleetId = Convert.ToInt32(data["fleetId"]);
                            fromCityCode = Convert.ToString(data["fromCityCode"]);
                            fromCity = Convert.ToString(data["fromCity"]);
                            fromLocationCode = Convert.ToString(data["fromLocationCode"]);
                            fromLocation = Convert.ToString(data["fromLocation"]);
                            fromDate = Convert.ToDateTime(data["fromDate"]);
                            toCityCode = Convert.ToString(data["toCityCode"]);
                            toCity = Convert.ToString(data["toCity"]);
                            toLocationCode = Convert.ToString(data["toLocationCode"]);
                            toLocation = Convert.ToString(data["toLocation"]);
                            toDate = Convert.ToDateTime(data["toDate"]);
                            fleetName = Convert.ToString(data["fleetName"]);
                            fleetType = Convert.ToString(data["fleetType"]);
                            rentType = (RentType)Convert.ToInt32(data["rentType"]);
                            cdw = Convert.ToDecimal(data["CDW"]);
                            pai = Convert.ToDecimal(data["PAI"]);
                            scdw = Convert.ToDecimal(data["SCDW"]);
                            gps = Convert.ToDecimal(data["GPS"]);
                            cseat = Convert.ToDecimal(data["CSEAT"]);
                            vmd = Convert.ToDecimal(data["VMD"]);
                            kmrest = Convert.ToDecimal(data["KMREST"]);
                            kmrate = Convert.ToDecimal(data["KMRATE"]);
                            chauffer = Convert.ToDecimal(data["CHAUFFER"]);
                            airportCharge = Convert.ToDecimal(data["AirportCharge"]);
                            emirates_Link_Charge = Convert.ToDecimal(data["Emirates_Link_Charge"]);
                            bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                            vehTariffId = Convert.ToInt32(data["supVehTariffId"]);
                            vehId = Convert.ToInt32(data["supVehId"]);
                            bookingStatus = (CarBookingStatus)Convert.ToInt32(data["bookingStatus"]);
                            voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                            locationId = Convert.ToInt32(data["locationId"]);
                            agencyId = Convert.ToInt32(data["agencyid"]);
                            transType = Convert.ToString(data["TransType"]);
                            cancelId = Convert.ToString(data["cancelId"]);

                            source = (CarBookingSource)Convert.ToInt32(data["source"]);
                            priceData.Load(Convert.ToInt32(data["priceId"]));
                            price = priceData;
                            FleetPassenger pass = new FleetPassenger();
                            passengerInfo = pass.Load(fleetId);
                            createdBy = Convert.ToInt32(data["createdBy"]);
                            createdOn = Convert.ToDateTime(data["createdOn"]);
                            currency = Convert.ToString(data["currency"]);
                            returnStatus = Convert.ToString(data["returnStatus"]);
                            status = Convert.ToString(data["status"]);
                            ins_Excess = Convert.ToDecimal(data["InsExcess"]);
                        }
                    }
                }
                //else
                //{
                //    data.Close();
                //    connection.Close();
                //}
                //data.Close();
                //connection.Close();
            }
            catch (Exception exp)
            {
                //connection.Close();
                throw new Exception("Fleet id does not exist in database", exp);
            }
        }
        /// <summary>
        /// This Method is used to get SightseeingId using Confirmation Number
        /// </summary>
        /// <param name="confNo"></param>
        /// <returns></returns>
        public static int GetFleetId(string confNo)
        {
            int fleetId = 0;
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@confNo", confNo);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetFleetId, paramList, connection);
            if (data.Read())
            {
                fleetId = Convert.ToInt32(data["fleetId"]);
            }
            data.Close();
            connection.Close();
            return fleetId;
        }
        public static List<int> LoadSourceId()
        {
            List<int> idList = new List<int>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAllFleetSourceId, paramList, connection);
            while (data.Read())
            {
                idList.Add(Convert.ToInt32(data["fleetSourceId"]));
            }
            data.Close();
            connection.Close();
            return idList;
        }
        /// <summary>
        /// This MEthod is used to Update the Itinerary BookingStatus
        /// </summary>
        public void UpdateBookingStatus()
        {
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@status", bookingStatus);
            paramList[1] = new SqlParameter("@cancelId", cancelId);
            paramList[2] = new SqlParameter("@fleetId", fleetId);

            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateFleetItineary, paramList);
        }
        #endregion
    }
}
