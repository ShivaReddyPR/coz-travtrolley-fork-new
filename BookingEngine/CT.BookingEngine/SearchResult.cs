using System;
using System.Collections.Generic;


namespace CT.BookingEngine
{
    [Serializable]
    public class SearchResult : IComparer<SearchResult> //:Air20.AirPricingSolution 
    {
        #region Private Members
        double baseFare;
        double tax;
        double totalFare;
        string currency;
        string fareType; //TODO: this can be an enum
        Fare[] fareBreakdown;
        FlightInfo[][] flights;
        List<FareRule> fareRules;
        string lastTicketDate;
        string ticketAdvisory;
        PriceAccounts price;
        /// <summary>
        /// validating airline code of the flight.
        /// </summary>
        string validatingAirline;
        /// <summary>
        /// True if the fare is non refundable.
        /// </summary>
        bool nonRefundable=true; //Default non refundable for all sources
        /// <summary>
        /// True if E-ticket is allowed.
        /// </summary>
        bool eticketEligible;
        string airline;
        /// <summary>
        /// To mentain unique Id of results when shown on return search page after filtering and sorting.
        /// </summary>
        int resultId;
        /// <summary>
        /// stores the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        string aliasAirlineCode;

        private BookingSource resultBookingSource;
        private UAPIdll.Air46.AirPricingSolution uapiPricingSolution;
        
        /// <summary>
        /// For Fly Dubai
        /// </summary>
        private Dictionary<string, List<int>> fareInformationId;
        
        /// <summary>
        /// For Fly Dubai
        /// </summary>
        private string guid;
        //TODO: Fare Basis Code ?
        /// <summary>
        /// Set 'True' if included in Fare for FlyDubai.
        /// </summary>
        private bool isBaggageIncluded;

        private Dictionary<long, bool> baggageIncluded;// for fz

        /// <summary>
        /// Stores journey sell key for SpiceJet result
        /// </summary>
        private string journeySellKey;
        /// <summary>
        /// Stores fare sell key for SpiceJet result
        /// </summary>
        private string fareSellKey;

        /// <summary>
        /// Baggage included in the Total Fare for FlyDubai. 20Kg, 30kg or 40kg.
        /// </summary>
        private string baggageIncludedInFare;//=string.Empty;

        /// <summary>
        /// For TBO Air
        /// </summary>
        private bool isLcc;

        /// <summary>
        /// for TBO Air
        /// </summary>
        private PriceAccounts tboPrice;

        /// <summary>
        /// FareBreakDown in Original Price(INR)
        /// </summary>
        private Fare[] tboFareBreakdown;

        /// <summary>
        /// Used in UAPI Repricing. Will be true if any of the segment 
        /// Departure or Arrival times have changed from the original result.
        /// </summary>
        private bool isTimesChanged;

        /// <summary>
        /// Used in UAPI Repricing. Will be true if any of the segment
        /// ClassOfService node in xml or BookingClass is changed from the original result.
        /// </summary>
        private bool isBookingClassChanged;
        private TravelPolicyResult _travelPolicyResult;
        
        /// <summary>
        /// Used in UAPI Repricing. If there is any error while doing Repricing
        /// then error message will be returned to the page.
        /// </summary>

        /// <summary>
        /// Used in UAPI Repricing. If there is any error while doing Repricing
        /// then error message will be returned to the page.
        /// </summary>
        private string repriceErrorMessage;

        private bool isGSTMandatory;
        /// <summary>
        /// Login Country Code for calculating Handling Fee
        /// </summary>
        private string loginCountryCode;
        private DynamicMarkupThreshold markupThreshold;
        private string resultKey; //For Combination Fares clubbing the results based on similar flight information of different fare types
        private bool _isSpecialRoundTrip;//For Combination Search If both onward and return results are SpecialRoundTripFares;

        #endregion

        #region Public Properties
        // For UAPI to get price in Create Reservation Res
        public UAPIdll.Air46.AirPricingSolution UapiPricingSolution
        {
            get
            {
                return uapiPricingSolution;
            }
            set
            {
                uapiPricingSolution = value;
            }
        }
        public FlightInfo[][] Flights
        {
            get
            {
                return flights;
            }
            set
            {
                flights = value;
            }
        }

        [System.Xml.Serialization.XmlArray("FareRules")]
        public List<FareRule> FareRules
        {
            get
            {
                return fareRules;
            }
            set
            {
                fareRules = value;
            }
        }

        public Fare[] FareBreakdown
        {
            get
            {
                return fareBreakdown;
            }
            set
            {
                fareBreakdown = value;
            }
        }

        public double BaseFare
        {
            get
            {
                return baseFare;
            }
            set
            {
                baseFare = value;
            }
        }

        public double Tax
        {
            get
            {
                return tax;
            }
            set
            {
                tax = value;
            }
        }

        public double TotalFare
        {
            get
            {
                return totalFare;
            }
            set
            {
                totalFare = value;
            }
        }

        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }

        public string FareType
        {
            get
            {
                return fareType;
            }
            set
            {
                fareType = value;
            }
        }
        public string LastTicketDate
        {
            get
            {
                return lastTicketDate;
            }
            set
            {
                lastTicketDate = value;
            }
        }

        public string TicketAdvisory
        {
            get
            {
                return ticketAdvisory;
            }
            set
            {
                ticketAdvisory = value;
            }
        }

        public PriceAccounts Price
        {
            get 
            {
                return this.price;
            }
            set
            {
                this.price = value;
            }
        }

        /// <summary>
        /// get the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        public string AliasAirlineCode
        {
            get
            {
                if (aliasAirlineCode != null && aliasAirlineCode.Length > 0)
                {
                    return aliasAirlineCode;
                }
                else
                {
                    for (int g = 0; g < flights.Length; g++)
                    {
                        for (int f = 0; f < flights[g].Length; f++)
                        {
                            if (flights[g][f].Airline == "IT")
                            {

                                foreach (KeyValuePair<int, int> s in CT.Configuration.ConfigurationSystem.ITRedConfig)
                                {
                                    if (s.Key <= Convert.ToInt32(flights[g][f].FlightNumber) && s.Value >= Convert.ToInt32(flights[g][f].FlightNumber))
                                    {
                                        aliasAirlineCode = "ITR";
                                        break;
                                    }
                                }
                            }
                            if (aliasAirlineCode == "ITR")
                            {
                                break;
                            }
                        }
                        if (aliasAirlineCode == "ITR")
                        {
                            break;
                        }
                    }
                    if (aliasAirlineCode != "ITR")
                    {
                        aliasAirlineCode = ValidatingAirline;
                    }
                    return aliasAirlineCode;
                }
            }
            set
            {
                aliasAirlineCode = value;
            }
        }

        /// <summary>
        /// Gets the validating airline of the flight.
        /// </summary>
        public string ValidatingAirline
        {
            get
            {
                if (validatingAirline == null || validatingAirline.Length == 0)
                {
                    validatingAirline = flights[0][0].Airline;
                    string countryKey = flights[0][0].Origin.CountryCode;
                    bool done = false;
                    for (int g = 0; g < flights.Length; g++)
                    {
                        for (int f = 0; f < flights[g].Length; f++)
                        {
                            if (flights[g][f].Destination.CountryCode != countryKey)
                            {
                                validatingAirline = flights[g][f].Airline;
                                done = true;
                                break;
                            }
                        }
                        if (done)
                        {
                            break;
                        }
                    }
                }
                return validatingAirline;
            }
            set
            {
                validatingAirline = value;
            }
        }

        /// <summary>
        /// True if fare is non refundable.
        /// </summary>
        public bool NonRefundable
        {
            get
            {
                return nonRefundable;
            }
            set
            {
                nonRefundable = value;
            }
        }
        /// <summary>
        /// True if eticket is allowed.
        /// </summary>
        public bool EticketEligible
        {
            get
            {
                return eticketEligible;
            }
            set
            {
                eticketEligible = value;
            }
        }
        public BookingSource ResultBookingSource
        {
            get
            {
                return resultBookingSource;
            }
            set
            {
                resultBookingSource = value;
            }
        }
        public string Airline
        {
            get
            {
                return airline;
            }
            set
            {
                airline = value;
            }
        }
        /// <summary>
        /// To mentain unique Id of results when shown on return search page after filtering and sorting.
        /// </summary>
        public int ResultId
        {
            get
            {
                return resultId;
            }
            set
            {
                resultId = value;
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        /// <summary>
        /// Stores FareInfo ID from xml result for FlyDubai
        /// </summary>
        public Dictionary<string, List<int>> FareInformationId
        {
            get { return fareInformationId; }
            set { fareInformationId = value; }
        }

        /// <summary>
        /// Stores GUID generated for FlyDubai
        /// </summary>
        public string GUID
        {
            get { return guid; }
            set { guid = value; }
        }
        /// <summary>
        /// If Baggage Fare is included in Tax details then set 'True' otherwise set 'False'. Added for FlyDubai.
        /// </summary>
        public bool IsBaggageIncluded
        {
            get { return isBaggageIncluded; }
            set { isBaggageIncluded = value; }
        }
        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<long, bool> BaggageIncluded
        {
            get { return baggageIncluded; }
            set { baggageIncluded = value; }
        }

        /// <summary>
        /// Set Baggage Code included in Total Fare for FlyDubai. Example:- 20kg.
        /// </summary>
        public string BaggageIncludedInFare
        {
            get { return baggageIncludedInFare; }
            set { baggageIncludedInFare = value; }
        }
        /// <summary>
        /// Stores JourneySellKey for SpiceJet Result.
        /// </summary>
        public string JourneySellKey
        {
            get { return journeySellKey; }
            set { journeySellKey = value; }
        }
        /// <summary>
        /// Stores FareSellKey for SpiceJet Result
        /// </summary>
        public string FareSellKey
        {
            get { return fareSellKey; }
            set { fareSellKey = value; }
        }


        /// <summary>
        /// True or False for TBOAir (Added by Ravi)
        /// </summary>
        public bool IsLCC
        {
            get { return isLcc; }
            set { isLcc = value; }
        }


        /// <summary>
        /// Used to store TBO Specific Currency values (INR) to be used while booking (Added by Shiva).
        /// </summary>
        public PriceAccounts TBOPrice
        {
            get { return tboPrice; }
            set { tboPrice = value; }
        }
        /// <summary>
        /// Used to store TBO Specific Currency values (PaxType wise) (INR) to be used while booking(Added by Shiva).
        /// </summary>
        public Fare[] TBOFareBreakdown
        {
            get { return tboFareBreakdown; }
            set { tboFareBreakdown = value; }
        }

        /// <summary>
        /// Used in UAPI Repricing. Will be true if any of the segment 
        /// Departure or Arrival times have changed from the original result.
        /// </summary>
        public bool IsTimesChanged
        {
            get { return isTimesChanged; }
            set { isTimesChanged = value; }
        }

        /// <summary>
        /// Used in UAPI Repricing. Will be true if any of the segment
        /// ClassOfService node in xml or BookingClass is changed from the original result.
        /// </summary>
        public bool IsBookingClassChanged
        {
            get { return isBookingClassChanged; }
            set { isBookingClassChanged = value; }
        }

        public TravelPolicyResult TravelPolicyResult
        {
            get { return _travelPolicyResult; }
            set { _travelPolicyResult = value; }
        }

        /// <summary>
        /// Used in UAPI Repricing. If there is any error while doing Repricing
        /// then error message will be returned to the page.
        /// </summary>
        public string RepriceErrorMessage
        {
            get { return repriceErrorMessage; }
            set { repriceErrorMessage = value; }
        }

        /// <summary>
        /// Check whether GST fields need to pass for the supplier while booking
        /// </summary>
        public bool IsGSTMandatory
        {
            get
            {
                return isGSTMandatory;
            }

            set
            {
                isGSTMandatory = value;
            }
        }

        public string LoginCountryCode
        {
            get
            {
                return loginCountryCode;
            }

            set
            {
                loginCountryCode = value;
            }
        }

        /// <summary>
        /// For Dynamic Markup
        /// </summary>
        public DynamicMarkupThreshold MarkupThreshold { get => markupThreshold; set => markupThreshold = value; }

        /// <summary>
        /// For Combination Fares clubbing the results based on similar flight information of different fare types
        /// </summary>
        public string ResultKey
        {
            get
            {
                return resultKey;
            }

            set
            {
                resultKey = value;
            }
        }



        /// <summary>
        /// //For Combination Search If both onward and return results are SpecialRoundTripFares;
        /// </summary>

        public bool IsSpecialRoundTrip
        {
            get
            {
                return _isSpecialRoundTrip;
            }

            set
            {
                _isSpecialRoundTrip = value;
            }
        }


        #endregion

        /// <summary>
        /// Gets an array of flight which includes flights from all flight groups
        /// </summary>
        /// <param name="result">Search result object</param>
        /// <returns>Array of Flightinfo (segments)</returns>
        /// 
        public static FlightInfo[] GetSegments(SearchResult result)
        {
            int flightCount = 0;
            for (int g = 0; g < result.Flights.Length; g++)
            {
                flightCount += result.Flights[g].Length;
            }
            FlightInfo[] flightSegments = new FlightInfo[flightCount];
            int i = 0;
            for (int g = 0; g < result.Flights.Length; g++)
            {
                for (int f = 0; f < result.Flights[g].Length; f++)
                {
                    flightSegments[i] = result.Flights[g][f];
                    if (result.ResultBookingSource == BookingSource.FlyDubai)
                    {
                        if (result.Flights[g][f].FlightNumber.Contains("/"))
                        {
                            flightSegments[i].FlightNumber = result.Flights[g][f].FlightNumber.Split('/')[f];
                        }
                    }
                    i++;
                }
            }
            return flightSegments;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingSource"></param>
        /// <returns></returns>
        public static string GetFlightBookingSource(BookingSource bookingSource)
        {
            string source = "";
            switch (bookingSource)
            {
                case BookingSource.AirArabia:
                    source = "'G9'";
                    break;
                case BookingSource.FlyDubai:
                    source = "'FZ'";
                    break;
                case BookingSource.UAPI:
                    source = "'UA'";
                    break;
                case BookingSource.SpiceJet:
                    source = "'SG'";
                    break;
                case BookingSource.TBOAir:
                    source = "'TA'";
                    break;
                case BookingSource.Indigo:
                    source = "'6E'";
                    break;

                case BookingSource.AirIndiaExpressIntl:
                    source = "'IX'";
                    break;
                case BookingSource.GoAir:
                    source = "'G8'";
                    break;
                case BookingSource.FlightInventory:
                    source = "'FI'";
                    break;
                case BookingSource.PKFares:
                    source = "'PK'";
                    break;
                case BookingSource.Amadeus:
                    source = "'1A'";
                    break;
                case BookingSource.SpiceJetCorp:
                    source = "'SGCORP'";
                    break;
                case BookingSource.IndigoCorp:
                    source = "'6ECORP'";
                    break;
                case BookingSource.Jazeera:
                    source = "'J9'";
                    break;
                case BookingSource.GoAirCorp:
                    source = "'G8CORP'";
                    break;
                case BookingSource.CozmoBus:
                    source = "'CB'";
                    break;
                case BookingSource.Babylon:
                    source = "'BABYLON'";
                    break;
                case BookingSource.SalamAir:
                    source = "'OV'";
                    break;
		case BookingSource.Sabre:
                    source = "'SABRE'";
                    break;

            }

            return source;
        }

        #region IComparer<SearchResult> Members

        public int Compare(SearchResult x, SearchResult y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (y == null)
                {
                    return 1;
                }
                else
                {
                    int retVal = x.TotalFare.CompareTo(y.TotalFare);

                    if (retVal != 0)
                    {
                        return retVal;
                    }
                    else
                    {
                        return x.TotalFare.CompareTo(y.TotalFare);
                    }
                }
            }
        }

        #endregion
    }
}
