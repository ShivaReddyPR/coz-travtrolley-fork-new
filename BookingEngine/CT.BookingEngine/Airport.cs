using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Linq;

namespace CT.BookingEngine
{
    [Serializable]
    public class Airport
    {
        /// <summary>
        /// Airport Code
        /// </summary>
        private string airportCode;
        /// <summary>
        /// Name of Ariport
        /// </summary>
        private string airportName;
        /// <summary>
        /// Code of city where the airport is located
        /// </summary>
        private string cityCode;
        /// <summary>
        /// Name of city where the airport is located
        /// </summary>
        private string cityName;
        /// <summary>
        /// Code of country where the airport is located
        /// </summary>
        private string countryCode;
        /// <summary>
        /// Name of country where the airport is located
        /// </summary>
        private string countryName;

        //Added by Lokesh on 13-Mar-2018 regarding AirportMaster Page
        private int airportIndex;//Variable which holds the Airport Index 
        private string airportNameAR;//Variable which holds the Airport Name in Arabic Format
        private string cityNameAR;//Variable which holds the city Name in Arabic Format
        private string countryNameAR;//Variable which holds the country Name in Arabic Format
        private string status;
        private int createdBy;
        private int airportId;
        private decimal timeZone;

        /// <summary>
        /// Name of currency where the airport is located
        /// </summary>
        private string currencyCode;//Added for Jazeera//Airport currency code should be sent in the request.

        /// <summary>
        /// Default Constructor
        /// </summary>

        public Airport() { }

        /// <summary>
        /// Constructor that loads details according to airportCode
        /// </summary>
        public Airport(string arpCode)
        {
            airportCode = arpCode;
            try
            {
                Load();
            }
            catch (ArgumentException)
            {
                airportName = string.Empty;
                cityCode = arpCode;
                cityName = string.Empty;
                countryCode = string.Empty;
                countryName = string.Empty;
            }
        }

        /// <summary>
        /// Gets the airport code
        /// </summary>
        public string AirportCode
        {
            get { return airportCode; }
            set { airportCode = value; }
        }

        /// <summary>
        /// Gets the airport name
        /// </summary>
        public string AirportName
        {
            get { return airportName; }
            set { airportName = value; }
        }

        /// <summary>
        /// Gets the code of city where the airport is located
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }

        /// <summary>
        /// Gets the name of city where the airport is located
        /// </summary>
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }

        /// <summary>
        /// Gets the code of country where the airport is located
        /// </summary>
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }

        /// <summary>
        /// Gets the name of country where the airport is located
        /// </summary>
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }


        /// <summary>
        /// Variable which holds the Airport Index
        /// </summary>
        public int AirportIndex
        {
            get { return airportIndex; }
            set { airportIndex = value; }
        }

        /// <summary>
        /// Variable which holds the Airport Name in Arabic Format
        /// </summary>
        public string AirportNameAR
        {
            get { return airportNameAR; }
            set { airportNameAR = value; }
        }


        /// <summary>
        /// Variable which holds the city Name in Arabic Format
        /// </summary>
        public string CityNameAR
        {
            get { return cityNameAR; }
            set { cityNameAR = value; }
        }

        /// <summary>
        /// Variable which holds the country Name in Arabic Format
        /// </summary>
        public string CountryNameAR
        {
            get { return countryNameAR; }
            set { countryNameAR = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public int AirportId
        {
            get { return airportId; }
            set { airportId = value; }
        }

        public decimal TimeZone
        {
            get
            {
                return timeZone;
            }

            set
            {
                timeZone = value;
            }
        }
        /// <summary>
        /// Gets the Airport Currency Code
        /// </summary>
        public string CurrencyCode//For Jazeera Purpose
        {
            get
            {
                return currencyCode;
            }

            set
            {
                currencyCode = value;
            }
        }





        /// <summary>
        /// Loads the airport object detail corresponding to airport code.
        /// </summary>
        public void Load()
        {
            //Trace.TraceInformation("Airport.Load entered airportCode = " + airportCode);
            if (airportCode == null || airportCode.Length == 0)
            {
                throw new ArgumentException("Airport Code can not be null or empty", "airportCode");
            }
            // checks whether Cache contains req. details.if it contains  data retrive from the Cache.
            if (CacheData.CheckAirportLoad(airportCode))
            {
                List<string> airList = new List<string>();
                airList = CacheData.AirportList[airportCode];
                airportName = (string)airList[0];
                cityCode = (string)airList[1];
                countryCode = (string)airList[2];
                cityName = CacheData.CityList[cityCode];
                countryName = CacheData.CountryList[countryCode];                
                timeZone = Convert.ToDecimal(airList[3]);
                currencyCode = Convert.ToString(airList[4]);//For Jazeera Purpose
                return;
            }
            else
            {
                List<string> tempList = new List<string>();
                //SqlConnection connection = DBGateway.GetConnection();

                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@airportCode", airportCode);
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAirport, paramList, connection);
                // If it doesnt contain in Cache retrieve data from the Database.
                using (DataTable dtAirport = DBGateway.FillDataTableSP(SPNames.GetAirport, paramList))
                {
                    if (dtAirport !=null && dtAirport.Rows.Count > 0)
                    {
                        DataRow dataReader = dtAirport.Rows[0];
                        airportName =Convert.ToString(dataReader["airportName"]);

                        cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                        cityName = Convert.ToString(dataReader["cityName"]);
                        countryCode = Convert.ToString(dataReader["countryCode"]);
                        countryName = Convert.ToString(dataReader["countryName"]);
                        if (dataReader["UtcTime"] != DBNull.Value)
                        {
                            timeZone = Convert.ToDecimal(dataReader["UtcTime"]);
                        }
                        if (dataReader["currencycode"] != DBNull.Value)//For Jazeera Purpose
                        {
                            currencyCode = Convert.ToString(dataReader["currencycode"]);
                        }
                        //Update the Cache.
                        tempList.Add(airportName);
                        tempList.Add(cityCode);
                        tempList.Add(countryCode);
                        tempList.Add(timeZone.ToString());
                        tempList.Add(Convert.ToString(currencyCode));//For Jazeera Purpose

                        CacheData.AirportList.Add(airportCode, tempList);
                        if (!CacheData.CheckGetCityName(cityCode))
                        {
                            CacheData.CityList.Add(cityCode, cityName);
                        }
                        if (!CacheData.CheckCountryName(countryCode))
                        {
                            CacheData.CountryList.Add(countryCode, countryName);
                        }
                        //dataReader.Close();
                        //connection.Close();
                    }

                    else
                    {
                        //dataReader.Close();
                        //connection.Close();
                        //Trace.TraceInformation("Airport.Load exiting : Airport does not exist. airportCode = " + airportCode);
                        throw new ArgumentException("Airport code does not exist in database");
                    }
                }
            }
        }

        /// <summary>
        /// Get valid city code - as if city name entered then corresponding city code and if airport code is entered then city code corresponding airport code.
        /// </summary>
        public ArrayList GetAirportCodesForCity(string city)
        {
            if (city == null && city.Length == 0)
            {
                //Trace.TraceInformation("city parameter must have a value");
                throw new ArgumentException("city parameter must have a value");
            }

            //Trace.TraceInformation("Airport.GetAirportCodesForCity entered  city" + city);
            ArrayList cityCodesArray = new ArrayList();



            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@to", city);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetValidCityCode, paramList, connection);
            using (DataTable dtValid = DBGateway.FillDataTableSP(SPNames.GetValidCityCode, paramList))
            {
                if (dtValid != null && dtValid.Rows.Count > 0)
                {
                    foreach (DataRow dataReader in dtValid.Rows)
                    {
                        string cityName;
                        string countryName;
                        //TODO: why are we doing this?
                        //if (dataReader.FieldCount == 1)
                        if (dtValid.Columns.Count == 1)
                        {
                            cityName = "";
                            countryName = "";
                        }
                        else
                        {
                            cityName = Convert.ToString(dataReader["cityName"]);
                            countryName = Convert.ToString(dataReader["countryName"]);
                        }
                        string[] temp = { Convert.ToString(dataReader["cityCode"]).ToUpper(), cityName, countryName };
                        cityCodesArray.Add(temp);

                    }
                }
            }

            //dataReader.Close();
            //connection.Close();

            if (cityCodesArray.Count == 0)
            {
                //Trace.TraceError("City Code does not exist for city (" + city + ")");

                string subjectMessage = "Cozmo City/Airport not in Database(B2B)";
                string bodyMessage = "Dear Executive,\n\nA City/Airport searched by user does not exist in our Database.\nCity/Airport searched : " + city + "\n\nThis is automated mail alert generated by Cozmo system.\n\n---\nCozmo System";
                Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["TechSupportMailingId"], subjectMessage, bodyMessage);

                throw new ArgumentException("City Code does not exist for city (" + city + ")");
            }

            //Trace.TraceInformation("Airport.GetAirportCodesForCity exiting city" + city);

            return cityCodesArray;

        }
        /// <summary>
        /// Gets the complete information about any city or airport name searched for 
        /// </summary>
        /// <param name="city">ciy name or the airport name to be searched for</param>
        /// <returns>An object of type Airport </returns>
        public static List<Airport> GetInfoForCityOrAirport(string city)
        {
            if (city == null && city.Length == 0)
            {
                //Trace.TraceInformation("city parameter must have a value");
                throw new ArgumentException("city parameter must have a value");
            }

            //Trace.TraceInformation("Airport.GetAirportCodesForCity entered  city" + city);
            List<Airport> airportList = new List<Airport>();
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@to", city);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAirportInfo, paramList, connection);
            using (DataTable dtAirport = DBGateway.FillDataTableSP(SPNames.GetAirportInfo, paramList))
            {
                if (dtAirport != null && dtAirport.Rows.Count > 0)
                {
                    foreach (DataRow dataReader in dtAirport.Rows)
                    {
                        Airport airport = new Airport();
                        airport.cityName = Convert.ToString(dataReader["cityName"]);
                        airport.countryName = Convert.ToString(dataReader["countryName"]);
                        airport.cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                        airport.countryCode = Convert.ToString(dataReader["countryCode"]);
                        airport.airportCode = Convert.ToString(dataReader["airportCode"]).ToUpper();
                        airport.airportName = Convert.ToString(dataReader["airportName"]);
                        airportList.Add(airport);
                    }
                }
            }
            //dataReader.Close();
            //connection.Close();
            //Trace.TraceInformation("Airport.GetAirportCodesForCity exiting city" + city);
            return airportList;
        }
        /// <summary>
        /// Gets the list of airport using city code
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public static List<Airport> GetAirports(string cityCode)
        {
            //Trace.TraceInformation("Airport.GetAirports entered  city code" + cityCode);
            if (cityCode == null || cityCode.Length != 3)
            {
                //Trace.TraceInformation("City Code parameter must have a value");
                throw new ArgumentException("city Code sholud be of length 3");
            }
            List<Airport> airportList = new List<Airport>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityCode", cityCode);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAirports, paramList, connection);
            while (dataReader.Read())
            {
                Airport airport = new Airport();
                airport.cityName = dataReader["cityName"].ToString();
                airport.countryName = dataReader["countryName"].ToString();
                airport.cityCode = dataReader["cityCode"].ToString().ToUpper();
                airport.countryCode = dataReader["countryCode"].ToString();
                airport.airportCode = dataReader["airportCode"].ToString().ToUpper();
                airport.airportName = dataReader["airportName"].ToString();
                airportList.Add(airport);
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("Airport.GetAirports exiting city code" + cityCode);
            return airportList;
        }

        // for getting city name
        public static ArrayList GetCityName(string citycode)
        {
            //Trace.TraceInformation("Airport.GetCityName entered  citycode" + citycode);
            ArrayList cityArray = new ArrayList();
            if (citycode == null)
            {
                //Trace.TraceInformation("citycode parameter must have a value");
                throw new ArgumentException("citycode parameter must have a value");
            }
            if (citycode.Length != 3)
            {
                //Trace.TraceInformation("citycode can only be three characters");
                throw new ArgumentException("citycode can only be three characters");
            }
            if (CacheData.CheckGetCityName(citycode))
            {
                string cName = CacheData.CityList[citycode];
                string[] temp = { citycode, cName };
                cityArray.Add(temp);
                return cityArray;
            }
            else
            {

                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@citycode", citycode);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetCityName, paramList, connection);
                while (dataReader.Read())
                {
                    string cityName;
                    string cityCode;
                    cityName = (string)dataReader["cityName"];
                    cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                    if (!CacheData.CheckGetCityName(cityCode))
                    {
                        CacheData.CityList.Add(cityCode, cityName);
                    }
                    string[] temp = { cityCode, cityName };
                    cityArray.Add(temp);
                }
                dataReader.Close();
                connection.Close();
                if (cityArray.Count == 0)
                {
                    //Trace.TraceError("City does not exist for given city code (" + citycode + ")");
                    throw new ArgumentException("City does not exist for given city code(" + citycode + ")");
                }
                //Trace.TraceInformation("Airport.GetCityName exiting citycode" + citycode);
                return cityArray;
            }
        }

        /// <summary>
        /// getting arrival and departure citi's airportcodes
        /// </summary>
        /// <param name="citycode"></param>
        /// <returns></returns>
        public static ArrayList GetAirportCode(string citycode)
        {
            //Trace.TraceInformation("Airport.GetAirportCode entered  citycode" + citycode);
            ArrayList airportCodes = new ArrayList();
            List<string> airList = new List<string>();
            string airCode = string.Empty;

            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@citycode", citycode);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAirportCodesByCity, paramList, connection);
            using (DataTable dtAirport = DBGateway.FillDataTableSP(SPNames.GetAirportCodesByCity, paramList))
            {
                if (dtAirport != null && dtAirport.Rows.Count > 0)
                {
                    foreach (DataRow dataReader in dtAirport.Rows)
                    {
                        string airportCode;
                        string airportName;
                        string cityCode;

                        airportCode = Convert.ToString(dataReader["airportCode"]).ToUpper();
                        airportName = Convert.ToString(dataReader["airportName"]);
                        cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                        //airList.Add(airportCode);
                        string[] temp = { airportCode, airportName, cityCode };
                        airportCodes.Add(temp);
                    }
                }
            }
            //CacheData.cityCodeList.Add(citycode, airList);
            //dataReader.Close();
            //connection.Close();

            if (airportCodes.Count == 0)
            {
                //Trace.TraceInformation("Airports does not exist for given city code (" + citycode + ")");
                throw new ArgumentException("Airports does not exist for given city code(" + citycode + ")");
            }

            //Trace.TraceInformation("Airport.GetAirportCode exiting citycode" + citycode);

            return airportCodes;
        }

        /// <summary>
        /// Get country code for given city code or airport code
        /// </summary>
        /// <param name="cityCodeOrAirportCode"></param>
        public string GetCountryCode(string cityCodeOrAirportCode)
        {
            //Trace.TraceInformation("Airport.GetCountryCode Enter citycode" + cityCodeOrAirportCode);
            ArrayList cityCodeArray = new ArrayList();
            try
            {
                cityCodeArray = GetAirportCodesForCity(cityCodeOrAirportCode);
            }
            catch
            {
                ////Trace.TraceInformation("No City Code exist for given City Code or Airport Code =" + cityCodeOrAirportCode + " | error = " + excep.Message);
                //throw new ArgumentException("No City Code exist for given City Code or Airport Code =" + cityCodeOrAirportCode + " | error = " + excep.Message);
            }
            if (cityCodeArray.Count > 0)
            {
                // get country code for cityCode
                Array a = (Array)cityCodeArray[0];
                cityCode = (string)a.GetValue(0);

                countryCode = (string)a.GetValue(2);

                if ((countryCode == null) || (countryCode != null && countryCode.Length == 0))
                {
                    // get data from data base
                    ArrayList airportData = GetAirportCode(cityCode);
                    Array b = (Array)airportData[0];
                    airportCode = (string)b.GetValue(0);
                    Load();
                }
            }

            //Trace.TraceInformation("Airport.GetCountryCode exiting citycode" + cityCodeOrAirportCode);
            return countryCode;
        }
        public static List<string> GetInvalidAirports(string csvAirports)
        {
            //Trace.TraceInformation("Airport.GetInvalidAirports entered  csvAirports" + csvAirports);
            List<string> tempList = new List<string>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@airportString", csvAirports);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAirportPresent, paramList, connection);
            string[] splitAirports = csvAirports.Split(',');
            List<string> dbArray = new List<string>();
            while (dataReader.Read())
            {
                dbArray.Add(dataReader["airportCode"].ToString());
            }
            foreach (string s in splitAirports)
            {
                foreach (string dbString in dbArray)
                {
                    if (s.IndexOf(dbString) < 0)
                    {
                        tempList.Add(s);
                        break;
                    }
                }
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("Airport.GetInvalidAirports exiting count" + tempList.Count);
            return tempList;
        }
        /// <summary>
        /// Adds new airport using airport code,airport name and city code 
        /// </summary>
        public void SaveAirport()
        {
            //Trace.TraceInformation("Airport.SaveAirport entered  ");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@airportCode", airportCode.ToUpper());
            paramList[1] = new SqlParameter("@airportName", airportName);
            paramList[2] = new SqlParameter("@cityCode", cityCode.ToUpper());
            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.AddAirport, paramList);
            //Trace.TraceInformation("Airport.SaveAirport exiting count" + retVal);

        }
        /// <summary>
        /// Updates the airport name through airport code
        /// </summary>
        public void Update()
        {
            //Trace.TraceInformation("Airport.Update entered  ");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@airportCode", airportCode.ToUpper());
            paramList[1] = new SqlParameter("@airportName", airportName);
            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateAirport, paramList);
            if (CacheData.CheckAirportLoad(airportCode))
            {
                CacheData.AirportList.Remove(airportCode);
                List<string> updateList = new List<string>();
                updateList.Add(airportName);
                updateList.Add(cityCode);
                updateList.Add(countryCode);
                CacheData.AirportList.Add(airportCode, updateList);
            }
            else
            {
                //No need to update the cache
            }
            //Trace.TraceInformation("Airport.Update exiting count" + retVal);
        }



        public static List<Airport> GetAirportsListForCountry(string countryCode)// Not Using anywhere
        {
            List<Airport> airportList = new List<Airport>();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@countryCode", countryCode);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetAirportList", paramList, connection);

                while (dataReader.Read())
                {

                    Airport airport = new Airport();
                    airport.cityName = (string)dataReader["cityName"];
                    airport.cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                    airport.countryCode = (string)dataReader["countryCode"];
                    airport.airportCode = Convert.ToString(dataReader["airportCode"]).ToUpper();
                    airport.airportName = (string)dataReader["airportName"];
                    airportList.Add(airport);
                }
                dataReader.Close();
                connection.Close();
            }
            return airportList;
        }





        public static List<Airport> GetAirportsForCountry(string countryCode)// not using 
        {
            List<Airport> airportList = new List<Airport>();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@countryCode", countryCode);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAirportsForCountry, paramList, connection);

                while (dataReader.Read())
                {
                    //Todo : Initialize countryNAME.Not Done because eextra join on DB
                    Airport airport = new Airport();
                    airport.cityName = (string)dataReader["cityName"];
                    airport.cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                    airport.countryCode = (string)dataReader["countryCode"];
                    airport.airportCode = Convert.ToString(dataReader["airportCode"]).ToUpper();
                    airport.airportName = (string)dataReader["airportName"];
                    airportList.Add(airport);
                }
                dataReader.Close();
                connection.Close();
            }
            return airportList;
        }


        ///Added by Lokesh on 13-Mar-2018 Reagarding AirportMaster Page
        /// <summary>
        /// This method returns the complete airport list from the table BKE_AIRPORT_LIST
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAirportList()
        {
            try
            {
                return GetAirportList(RecordStatus.All);
                //SqlParameter[] paramList = new SqlParameter[0];
                //return DBGateway.ExecuteQuery("USP_GET_BKE_AIRPORT_LIST", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// This method updates the row status in the table BKE_AIRPORT_LIST
        /// </summary>
        /// <param name="airportId"></param>
        /// <param name="status"></param>
        public static void UpdateRowStatus(int airportId, string status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_ID", airportId);
                paramList[1] = new SqlParameter("@P_STATUS", status);
                DBGateway.ExecuteNonQuery("USP_UPDATE_AIRPORTLIST_STATUS", paramList);
            }
            catch { throw; }
        }
        public static Trie<Airport> GetAirportCodesList()
        {

            Trie<Airport> trie1 = new Trie<Airport>();
            try
            {
                DataTable dtAirports = GetAirportList(RecordStatus.Activated);
                List<Airport> lstAirport = new List<Airport>();
                lstAirport = dtAirports.AsEnumerable().Select(r => new Airport
                {
                    AirportIndex = r.Field<int>("AirportIndex"),
                    AirportCode = r.Field<string>("AirportCode"),
                    AirportName = r.Field<string>("AirportName"),
                    CityName = r.Field<string>("CityName"),
                    CityCode = r.Field<string>("CityCode"),
                    CountryName = r.Field<string>("CountryName"),
                }).ToList();
                foreach (Airport airport in lstAirport)
                {
                    string airportCode = getModifyString(airport.AirportCode);
                    if (trie1.Find(airportCode).Count() == 0)
                    {
                        trie1.Add(airportCode, airport);
                    }
                    if (!string.IsNullOrEmpty(airport.CityName))
                    {
                        string cityname = getModifyString(airport.CityName);
                        if (trie1.Find(cityname).Count() == 0 && cityname != airportCode)
                        {
                            trie1.Add(cityname, airport);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return trie1;
        }
        ///Added by Harish Reagarding getting airport codes based on record status
        /// <summary>
        /// This method returns the complete airport list from the table BKE_AIRPORT_LIST
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAirportList(RecordStatus recordStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                if (recordStatus != RecordStatus.All)
                    paramList[0] = new SqlParameter("@P_RECORD_STATUS", Convert.ToString((char)recordStatus));
                return DBGateway.ExecuteQuery("USP_GET_BKE_AIRPORT_LIST", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[14];
                paramList[0] = new SqlParameter("@P_Airport_Id", airportId);
                paramList[1] = new SqlParameter("@P_AirportIndex", airportIndex);
                paramList[2] = new SqlParameter("@P_AirportCode", airportCode);
                paramList[3] = new SqlParameter("@P_AirportName", airportName);
                paramList[4] = new SqlParameter("@P_AirportNameAR", airportNameAR);
                paramList[5] = new SqlParameter("@P_CityCode", cityCode);
                paramList[6] = new SqlParameter("@P_CityName", cityName);
                paramList[7] = new SqlParameter("@P_CityNameAR", cityNameAR);
                paramList[8] = new SqlParameter("@P_CountryName", countryName);
                paramList[9] = new SqlParameter("@P_CountryNameAR", countryNameAR);
                paramList[10] = new SqlParameter("@P_Status", status);
                paramList[11] = new SqlParameter("@P_CreatedBy", createdBy);
                paramList[12] = new SqlParameter("@P_Airport_Id_Ret", SqlDbType.Int);
                paramList[12].Direction = ParameterDirection.Output;
                paramList[13] = new SqlParameter("@P_CountryCode", countryCode);
                int count = DBGateway.ExecuteNonQuery("USP_BKE_AIRPORT_LIST_ADDUPDATE", paramList);
                if (paramList[12].Value != DBNull.Value)
                {
                    airportId = Convert.ToInt32(paramList[12].Value);
                }
            }
            catch
            {
                throw;
            }
            return airportId;
        }
        #region private method
        private static string getModifyString(string modifyString)
        {
            return modifyString.ToLower().Trim().Replace(" ", "").Replace(".", "").Replace("'", "").Replace("�", "").Replace("�", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace("/", "").Replace("�", "").Replace("�", "e");
        }
        #endregion

    }
    public class Trie<T>
    {
        //public static Trie<T> tree ;
        private struct Suffix
        {
            string text;
            int index;

            public Suffix(string theText)
            {
                text = theText;
                index = 0;
            }

            public bool IsEmpty()
            {
                return index >= text.Length;
            }

            public char GetFirstChar()
            {
                return text[index];
            }

            public Suffix GetNextSuffix()
            {
                Suffix nextSuffix;
                nextSuffix.text = text;
                nextSuffix.index = index + 1;

                return nextSuffix;
            }
        }
        private class TrieNode
        {
            private static int NumDigits = 10;
            private static int NumLetters = 26;
            public List<T> values = new List<T>();
            private TrieNode[] branches = new TrieNode[NumDigits + NumLetters + NumLetters];

            public void Add(Suffix key, T value)
            {
                try
                {
                    if (key.IsEmpty())
                    {
                        values.Add(value);
                    }
                    else
                    {
                        int branchIndex = GetBranchIndexFromChar(key.GetFirstChar());
                        TrieNode node = branches[branchIndex];

                        if (node == null)
                            node = branches[branchIndex] = new TrieNode();

                        node.Add(key.GetNextSuffix(), value);
                    }
                }
                catch (Exception ex)
                {
                    string ss = key.ToString();
                    string hj = value.ToString();
                    throw ex;
                }
            }

            public TrieNode FindNode(Suffix key)
            {
                if (key.IsEmpty())
                {
                    return this;
                }
                else
                {
                    int branchIndex = GetBranchIndexFromChar(key.GetFirstChar());
                    TrieNode node = null;
                    if (branchIndex >= 0) // To Avoid Special Charactr Error , Shekhar 
                    {
                        node = branches[branchIndex];
                    }
                    if (node != null)
                        return node.FindNode(key.GetNextSuffix());
                    else
                        return null;
                }
            }

            public IEnumerator<T> GetEnumerator()
            {
                foreach (T value in values)
                    yield return value;

                foreach (TrieNode node in branches)
                {
                    if (node != null)
                    {
                        foreach (T value in node)
                            yield return value;
                    }
                }
            }

            private static int GetBranchIndexFromChar(char ch)
            {
                int index = -1;

                if (char.IsNumber(ch))
                {
                    index = ch - '0';
                }
                else
                {
                    if (char.IsLower(ch))
                    {
                        index = NumDigits + (ch - 'a');
                    }
                    else if (char.IsUpper(ch))
                    {
                        index = NumDigits + NumLetters + (ch - 'A');
                    }
                }

                return index;
            }

            private static char GetCharFromBranchIndex(int index)
            {
                char ch = '\0';

                if (index >= 0)
                {
                    if (index < NumDigits)
                        ch = (char)('0' + index);
                    else if (index < NumDigits + NumLetters)
                        ch = (char)('a' + (index - NumDigits));
                    else if (index < NumDigits + NumLetters + NumLetters)
                        ch = (char)('A' + (index - NumDigits - NumLetters));
                }

                return ch;
            }

            public IEnumerable<string> Keys
            {
                get
                {
                    if (values.Count > 0)
                        yield return "";

                    for (int branchIndex = 0; branchIndex < branches.Length; branchIndex++)
                    {
                        TrieNode node = branches[branchIndex];
                        if (node != null)
                        {
                            char branchChar = GetCharFromBranchIndex(branchIndex);
                            string keyPrefix = branchChar.ToString();

                            foreach (string key in node.Keys)
                                yield return keyPrefix + key;
                        }
                    }
                }
            }


        }

        public void Add(string key, T value)
        {
            root.Add(new Suffix(key), value);
        }

        public IEnumerable<T> Find(string key)
        {
            TrieNode node = root.FindNode(new Suffix(key));
            if (node != null)
            {
                foreach (T value in node.values)
                    yield return value;
            }
        }

        public IEnumerable<T> PrefixFind(string keyPrefix)
        {
            TrieNode node = root.FindNode(new Suffix(keyPrefix));
            if (node != null)
            {
                foreach (T value in node)
                    yield return value;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return root.GetEnumerator();
        }

        public IEnumerable<string> Keys
        {
            get { return root.Keys; }
        }

        /// <summary>
        /// If you want to use this method You have to inherit the class T with IEquatable<T> interface
        /// and provide an implementation of Equals function accordingly.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Remove(string removingKey, T removingValue)
        {
            TrieNode node = root.FindNode(new Suffix(removingKey));
            if (node != null)
            {
                return node.values.Remove(removingValue);
            }
            else
            {
                return false;
            }
        }

        private TrieNode root = new TrieNode();
    }
}
