﻿using System;
//using System.Linq;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.BookingEngine
{
    [Serializable]
    public class SourceBaggage
    {
        #region Members
        BookingSource source;
        string baggageCode;
        decimal baggagePrice;
        string bagggageCurrency;
        bool isActive;
        int createdBy;
        DateTime createdOn;
        int modifiedBy;
        DateTime modifiedOn; 
        #endregion


        #region Properties
        public BookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public string BaggageCode
        {
            get { return baggageCode; }
            set { baggageCode = value; }
        }
        public decimal BaggagePrice
        {
            get { return baggagePrice; }
            set { baggagePrice = value; }
        }
        public string BagggageCurrency
        {
            get { return bagggageCurrency; }
            set { bagggageCurrency = value; }
        }
        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }
        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }
        #endregion

        /// <summary>
        /// If already baggage code is there for Source we will update otherwise save.
        /// </summary>
        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_BaggageSourceId", (int)source);
                paramList[1] = new SqlParameter("@P_BaggageCode", baggageCode);
                paramList[2] = new SqlParameter("@P_BaggagePrice", baggagePrice);
                paramList[3] = new SqlParameter("@P_BaggageCurrency", bagggageCurrency);
                paramList[4] = new SqlParameter("@P_IsActive", isActive);
                paramList[5] = new SqlParameter("@P_CreatedBy", createdBy);

                int count = DBGateway.ExecuteNonQuerySP("usp_SaveSourceBaggageInfo", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, createdBy, "(SourceBaggage)Failed to save Source Baggage for " + source + ". Reason : " + ex.ToString(), "");
            }
        }

        public static DataTable GetSourceBaggages(BookingSource sourceId)
        {
            DataTable dtBaggages = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_BaggageSourceId", (int)sourceId);

                dtBaggages = DBGateway.FillDataTableSP("usp_GetSourceBaggageInfo", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(SourceBaggage)Failed to get Source Baggage for " + sourceId + ". Reason : " + ex.ToString(), "");
            }

            return dtBaggages;
        }

        /// <summary>
        /// This method returns the LCC Baggage Details based on currency and sourceid
        /// </summary>
        /// <param name="sourceId"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public static DataTable GetSourceBaggages(BookingSource sourceId,string currency)
        {
            DataTable dtBaggages = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_BaggageSourceId", (int)sourceId);
                paramList[1] = new SqlParameter("@P_Currency", currency);
                dtBaggages = DBGateway.FillDataTableSP("usp_GetLCCBaggage_ByCurrency", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(SourceBaggage)Failed to get Source Baggage for " + sourceId + ". Reason : " + ex.ToString(), "");
            }

            return dtBaggages;
        }
    }
}
