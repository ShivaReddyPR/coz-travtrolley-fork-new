using System;
using System.Collections.Generic;

namespace CT.BookingEngine
{
    [Serializable ]
    public class TransferSearchResult
    {
        [Serializable]
        public struct TransferPickUpPointDetails
        {
            public string PickUpPointName;
            public string PickUpPointCode;
            public string PickUpPointCityCode;
        }
        [Serializable]
        public struct TransferPickUpDetails
        {
            public string PickUpCode;
            public string PickUpName;
            public string PickUpDetailCode;
            public string PickUpDetailName;
            public List<TransferPickUpPointDetails> PickUpPointDetails;
        }
        [Serializable]
        public struct TransferDropOffDetails
        {
            public string DropOffCode;
            public string DropOffName;
            public float DropOffAllowForCheckInTime;
            public string DropOffDetailCode;
            public string DropOffDetailName;
        }
        [Serializable]
        public struct TransferOutOfHoursSupplement
        {
            public float FromTime;
            public float ToTime;
            public string Supplement;
            public string Details;
        }
        [Serializable]
        public struct TransferVehicleDetails
        {
            public string Vehicle;
            public string VehicleCode;
            public int VehicleMaximumPassengers;
            public int VehicleMaximumLuggage;
            public string LanguageCode;
            public string Language;
            public decimal ItemPrice;
            public PriceAccounts PriceInfo;
            public string ItemPriceCurrency;
            public string Confirmation;
            public string ConfirmationCode;
            public string ImageUrl;
            public string Category;
            public decimal DiscountPrice;
            public string MeetAndGreat;
            public string BufferTime;
            public string Currency;
            public decimal SellingFare;
            public decimal TotalPrice;
            public bool VoucherStatus;
            public int Seats;
            public int Luggage;
            public bool Animal;
            public int SportsLuggage;
            public bool WheelChair;
            public string ChildrenSeat1;
            public string ChildrenSeat2;
            
        }
        [Serializable]
        public struct TransferLocationDetails
        {
            public string LocationName;
            public string LocationCode;
        }

        public bool HasExtraInfo;
        public bool HasIdeas;
        public string City;
        public string CityCode;
        public string ItemName;
        public string ItemCode;
        public TransferPickUpDetails PickUp;
        public TransferDropOffDetails DropOff;
        public TransferOutOfHoursSupplement[] OutOfHoursSupplements;
        public decimal ApproximateTransferTime;
        public string ApproximateTransferTime_Str;
        public TransferVehicleDetails[] Vehicles;
        public TransferBookingSource Source;
        public PriceAccounts price;
        public string Currency;
        public string Conditions;
        public string Distance;
        public string SessionId;
        public string StartTime;
        public string EndTime;
        public string StrStartTime;
        public string StrEndTime;
    }
}
