﻿using System;
//using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public class InsuranceQueue
    {
        public InsuranceQueue()
        {
        }

        public static DataTable GetInsuranceQueue(DateTime departureDate, DateTime returnDate, DateTime purchaseDateFrom, DateTime purchaseDateTo, string policyNo, int agencyId, string pnrNo, string origin, string destination, string memberType, decimal locationId, long loginUserId,string agentType, string transType)
        {
            DataTable dtInsQueue;
            try
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[14];
                if(departureDate!= Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[0] = new SqlParameter("@P_DepartureDate", departureDate);
                if(returnDate != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[1] = new SqlParameter("@P_ReturnDate", returnDate);
                if (purchaseDateFrom != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[2] = new SqlParameter("@P_PurchaseDate_From", purchaseDateFrom);
                if (purchaseDateTo != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[3] = new SqlParameter("@P_PurchaseDate_To", purchaseDateTo);
                paramList[4] = new SqlParameter("@P_PolicyNo", policyNo);
                if(agencyId >0) paramList[5] = new SqlParameter("@P_AgencyId", agencyId);
                if(!string.IsNullOrEmpty(pnrNo)) paramList[6] = new SqlParameter("@P_PNRno", pnrNo);
                if (!string.IsNullOrEmpty(origin)) paramList[7] = new SqlParameter("@P_DepartureStationCode", origin);
                if (!string.IsNullOrEmpty(destination)) paramList[8] = new SqlParameter("@P_ArrivalStationCode", destination);
                paramList[9] = new SqlParameter("@P_USER_TYPE", memberType);
                paramList[10] = new SqlParameter("@P_LOCATION_ID", locationId);
                paramList[11] = new SqlParameter("@P_USER_ID", loginUserId);
		        paramList[12] = new SqlParameter("@P_AGENT_TYPE", agentType);
                //Added Param 19/08/2015 by Chandan Sharma
                paramList[13] = new SqlParameter("@P_TransType", transType);
                dtInsQueue = DBGateway.FillDataTableSP(SPNames.GetInsQueueList, paramList);
                return dtInsQueue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       //in queue plans deisplaying
        public static DataTable GetInsurancePlans(int insId)
        {
            try
            {
                DataTable dt = null;
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_insId", insId);
                dt = DBGateway.FillSP(SPNames.GetInsurancePlanList, paramList).Tables[0];
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet GetInsuranceQueueDetails(int id)
        {
            try
            {
                DataSet ds = null;
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_INS_HDR_ID", id);
                ds = DBGateway.FillSP(SPNames.GetInsuranceQueueDetails, paramList);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        # region Insure Report
        public static DataTable InsuranceGetList(DateTime fromDate, DateTime toDate, int agentId, long locationId, string memberType, string accountedStatus, string transType, string agentType)
        {
           // SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[7];
                    paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                    paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                    if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                    if (locationId > 0) paramList[3] = new SqlParameter("@P_LOCATION_ID", locationId);
                    //if (userId > 0) paramList[4] = new SqlParameter("@P_USER_ID", userId);
                    //paramList[5] = new SqlParameter("@P_USER_TYPE", memberType);
                    if (!string.IsNullOrEmpty(accountedStatus)) paramList[4] = new SqlParameter("@P_IsAccounted", accountedStatus);
                    if (!string.IsNullOrEmpty(transType)) paramList[5] = new SqlParameter("@P_TransType", transType);
                    if (!string.IsNullOrEmpty(agentType)) paramList[6] = new SqlParameter("@P_AgentType", agentType);

                    dt = DBGateway.FillDataTableSP(SPNames.InsuranceAcctList, paramList);
                    //if (data != null)
                    //{
                    //    dt.Load(data);
                    //}
                    //connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        public static DataTable GetInsuranceChangeRequestQueue(DateTime departureDate, DateTime returnDate, DateTime purchaseDateFrom, DateTime purchaseDateTo, string policyNo, int agencyId, string pnrNo, string origin, string destination, string memberType, decimal locationId, long loginUserId)
        {
            DataTable dtInsQueue;
            try
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[12];
                if (departureDate != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[0] = new SqlParameter("@P_DepartureDate", departureDate);
                if (returnDate != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[1] = new SqlParameter("@P_ReturnDate", returnDate);
                if (purchaseDateFrom != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[2] = new SqlParameter("@P_PurchaseDate_From", purchaseDateFrom);
                if (purchaseDateTo != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[3] = new SqlParameter("@P_PurchaseDate_To", purchaseDateTo);
                if (!string.IsNullOrEmpty(policyNo)) paramList[4] = new SqlParameter("@P_PolicyNo", policyNo);
                if (agencyId > 0) paramList[5] = new SqlParameter("@P_AgencyId", agencyId);
                if (!string.IsNullOrEmpty(pnrNo)) paramList[6] = new SqlParameter("@P_PNRno", pnrNo);
                if (!string.IsNullOrEmpty(origin)) paramList[7] = new SqlParameter("@P_Origin", origin);
                if (!string.IsNullOrEmpty(destination)) paramList[8] = new SqlParameter("@P_Destination", destination);
                paramList[9] = new SqlParameter("@P_USER_TYPE", memberType);
                paramList[10] = new SqlParameter("@P_LOCATION_ID", locationId);
                paramList[11] = new SqlParameter("@P_USER_ID", loginUserId);
                //if (status != "Select") paramList[12] = new SqlParameter("@P_STATUS", status);
                dtInsQueue = DBGateway.FillDataTableSP(SPNames.GetInsuranceChangeRequestList, paramList);
                return dtInsQueue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateInsAcctStatus(long InsId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];

                paramList[0] = new SqlParameter("@P_INS_ID", InsId);
                //paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[1] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[1].Direction = ParameterDirection.Output;
                paramList[2] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[2].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("usp_UpdateInsAccountedStatus", paramList);
            }
            catch { throw; }
        }

        //Loading Insurance Payment Information
        public static DataTable Load(int planId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];

                paramList[0] = new SqlParameter("@P_PLAN_ID", planId);
                return DBGateway.FillDataTableSP("usp_GetInsurancePaymentInfoByPlanId", paramList);
            }
            catch(Exception ex) { throw ex; }
        }

        # endregion
    }
}
