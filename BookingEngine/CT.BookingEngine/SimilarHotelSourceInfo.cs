﻿using System;

namespace CT.BookingEngine
{
    [Serializable]
    public class SimilarHotelSourceInfo
    {
        #region variables
        /// <summary>
        /// source
        /// </summary>
        HotelBookingSource source;
        /// <summary>
        /// hotelCode
        /// </summary>
        string hotelCode;
        /// <summary>
        /// cityCode
        /// </summary>
        string cityCode;
        /// <summary>
        /// roomDetails
        /// </summary>
        HotelRoomsDetails[] roomDetails;       
        /// <summary>
        /// sourcesessionId
        /// </summary>
        string sourceSessionId;
        /// <summary>
        /// TotalPrice
        /// </summary>
        decimal totalPrice;
        /// <summary>
        /// Discount
        /// </summary>
        decimal discount;
        #endregion
        #region properties
        /// <summary>
        /// source
        /// </summary>
        public HotelBookingSource Source { get => source; set => source = value; }
        /// <summary>
        /// hotelCode
        /// </summary>
        public string HotelCode { get => hotelCode; set => hotelCode = value; }
        /// <summary>
        /// cityCode
        /// </summary>
        public string CityCode { get => cityCode; set => cityCode = value; }
        /// <summary>
        /// roomDetails
        /// </summary>
        public HotelRoomsDetails[] RoomDetails { get => roomDetails; set => roomDetails = value; }
        public string SourceSessionId { get => sourceSessionId; set => sourceSessionId = value; }
        public decimal TotalPrice { get => totalPrice; set => totalPrice = value; }
        public decimal Discount { get => discount; set => discount = value; }
        #endregion
    }
}

