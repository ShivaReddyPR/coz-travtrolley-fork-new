﻿using System;
using CT.TicketReceipt.BusinessLayer;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
//using CT.AccountingEngine;
/// <summary>
/// Summary description for IMPMessages
/// </summary>
namespace CT.BookingEngine
{

    public class IMPMessages
    {
        private const long NEW_RECORD = -1;

        #region Member Variables
        private long _msgId;
        private int _agencyId;
        private string _subject;
        private string _message;
        private string _readStatus;
        private string _status;
        private long _createdBy;
        #endregion

        #region Properties
        public long MsgId
        {
            get { return _msgId; }
            set { _msgId = value; }
        }
        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }
        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        public string ReadStatus
        {
            get { return _readStatus; }
            set { _readStatus = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion

        #region Constructors
        public IMPMessages()
        {
            _msgId = NEW_RECORD;
        }
        public IMPMessages(long id)
        {
            _msgId = id;
            getDetails(_msgId);
        }
        #endregion 

        #region Member Functions

        private void getDetails(long id)
        {
            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }
        }

        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@MsgId", id);
                DataSet dsResult = DBGateway.ExecuteQuery("IMP_MESSAGES_GETDATA", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _msgId = Convert.ToInt64(dr["msg_id"]);
                _agencyId = Convert.ToInt32(dr["to_agency_id"]);
                _subject = Convert.ToString(dr["subject"]);
                _message = Convert.ToString(dr["message"]);
                _readStatus = Convert.ToString(dr["read_status"]);
                _status = Convert.ToString(dr["status"]);

            }
            catch
            {
                throw;
            }
        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[9];
                paramArr[0] = new SqlParameter("@MsgId", _msgId);
                paramArr[1] = new SqlParameter("@AgencyId", _agencyId);
                paramArr[2] = new SqlParameter("@Subject", _subject);
                paramArr[3] = new SqlParameter("@Message", _message);
                paramArr[4] = new SqlParameter("@ReadStatus", _readStatus);
                paramArr[5] = new SqlParameter("@Status", _status);
                paramArr[6] = new SqlParameter("@CreatedBy", _createdBy);
                SqlParameter paramMsgType = new SqlParameter("@IMP_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[7] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@IMP_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[8] = paramMsgText;

                DBGateway.ExecuteNonQuery("IMP_MESSAGES_ADD_UPDATE", paramArr);
                string messageType = Convert.ToString(paramArr[7].Value);
                if (messageType == "E")
                {
                    string message = Convert.ToString(paramArr[8].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@LIST_STATUS", status);
                paramList[1] = new SqlParameter("@RECORD_STATUS", Convert.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("[IMP_MESSAGES_GETLIST]", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataSet GetMessages(int Agencyid)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@AgencyId", Agencyid);
                DataSet dsResult = DBGateway.ExecuteQuery("GET_IMP_MESSAGES", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
