using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;

namespace CT.BookingEngine
{
    [Serializable]
    public struct TourOperation
    {
        public List<string> TourLanguageList;         //Tour Available Language
        public List<string> LanguageCode;
        public decimal ItemPrice;                          //Item Price in decimal
        public string Currency;
        public Dictionary<string, string> ConfirmationCodeList; //
        public List<string> SpecialItemList;      //
        public string SpecialItemName;
        public List<string> LangName;
        public PriceAccounts PriceInfo;
        public string CancellationPolicy;
        public string duration;
        public DateTime TourDate;

       
    }
    [Serializable]
    public struct EssentialInformation
    {
        public string Text;
        public DateTime DateFrom;
        public DateTime DateTo;
    }
    [Serializable]
    public class SightseeingSearchResult
    {
        private bool depaturePointRequired;              //if set to true, depature point is required ,when booking
        private string cityCode;                         //City Code & Name
        private string cityName;
        private string itemCode;                         //Sightseeing Code & Name
        private string itemName;
        private string duration;                         //Sighseeing Duration
        private Dictionary<string, string> sightseeingTypeList;          //Sightseeing Type Code & Name
        private Dictionary<string, string> sightseeingCategoryList;      //Sightseeing Category Code & Name
        private bool hasExtraInfo;
        private bool hasFlash;
        private TourOperation[] tourOperationList;
        private string[] additionalInformationList;
        private EssentialInformation[] essentialInformationList;
        private string description;
        private string image;
        private SightseeingBookingSource source;
        private string flashLink;
        List<DayWiseTourOperation> dayWisetourOperationList;
        private string[] cancellationCharges;
        private string[] inclusions;
        #region Properity
        public bool DepaturePointRequired
        {
            get { return depaturePointRequired; }
            set { depaturePointRequired = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }

        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }
        public string Duration
        {
            get { return duration; }
            set { duration = value; }
        }
        public Dictionary<string, string> SightseeingTypeList
        {
            get { return sightseeingTypeList; }
            set { sightseeingTypeList = value; }
        }
        public Dictionary<string, string> SightseeingCategoryList
        {
            get { return sightseeingCategoryList; }
            set { sightseeingCategoryList = value; }
        }
        public bool HasExtraInfo
        {
            get { return hasExtraInfo; }
            set { hasExtraInfo = value; }
        }
        public bool HasFlash
        {
            get { return hasFlash; }
            set { hasFlash = value; }
        }
        public TourOperation[] TourOperationList
        {
            get { return tourOperationList; }
            set { tourOperationList = value; }
        }
        public string[] AdditionalInformationList
        {
            get { return additionalInformationList; }
            set { additionalInformationList = value; }
        }
        public EssentialInformation[] EssentialInformationList
        {
            get { return essentialInformationList; }
            set { essentialInformationList = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string Image
        {
            get { return image; }
            set { image = value; }
        }
        public SightseeingBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public string FlashLink
        {
            get { return flashLink; }
            set { flashLink = value; }
        }
        public List<DayWiseTourOperation> DayWisetourOperationList
        {
            get { return dayWisetourOperationList; }
            set { dayWisetourOperationList = value; }
        }
        public string[] CancellationCharges
        {
            get { return cancellationCharges; }
            set { cancellationCharges = value; }
        }
        public string[] Inclusions
        {
            get { return inclusions; }
            set { inclusions = value; }
        }
        #endregion

        #region METHODS

        /// <summary>
        /// Save the search result into DB for the given session
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="searchResult"></param>
        public void Save(string sessionId, SightseeingSearchResult[] searchResult)
        {
            //Trace.TraceInformation("SightseeingSearchResult[].Save entered : sessionId = " + sessionId);

            if (sessionId == null)
            {
                throw new ArgumentException("The current session expired");
            }
            if (searchResult != null && searchResult.Length > 0)
            {
                byte[] data = GetByteArrayWithObject(searchResult);

                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@sessionId", sessionId);
                paramList[1] = new SqlParameter("@resultData", data);

                DBGateway.ExecuteNonQuerySP(SPNames.AddSightseeingSearchResultCache, paramList);
            }  // ELSE no data to save.

            //Trace.TraceInformation("SightseeingSearchResult.Save exited : sessionId = " + sessionId);
        }

        /// <summary>
        /// Load the data from DB for the given session
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        private SightseeingSearchResult[] Load(string sessionId)
        {
            //Trace.TraceInformation("SightseeingSearchResult.Load entered : sessionId = " + sessionId);

            if (sessionId == null)
            {
                throw new ArgumentException("The current session expired");
            }

            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sessionId", sessionId);

            // SqlConnection connection = DBGateway.GetConnection();
            //SqlDataReader dReader = DBGateway.ExecuteReaderSP(SPNames.GetSightseeingSearchResultCache, paramList, connection);
            byte[] data = new byte[0];
            using (DataTable dtSight = DBGateway.FillDataTableSP(SPNames.GetSightseeingSearchResultCache, paramList))
            {
                if (dtSight != null && dtSight.Rows.Count > 0)
                {
                    DataRow dReader = dtSight.Rows[0];
                    if (dtSight != null && dReader["resultData"] != DBNull.Value)
                    {
                        data = (byte[])dReader["resultData"];
                    }
                }
            }
            //dReader.Close();
            //connection.Close();

            SightseeingSearchResult[] searchResultCache = new SightseeingSearchResult[0];
            if (data.Length > 0)
            {
                searchResultCache = (SightseeingSearchResult[])GetObjectWithByteArray(data);
            }
            return searchResultCache;
        }

        /// <summary>
        /// Return the SightseeingResult for the sessionId on selected filter criteria
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="filterCriteria"></param>
        /// <param name="noOfPages"></param>
        /// <returns></returns>
        public SightseeingSearchResult[] GetFilteredResult(string sessionId, Dictionary<string, string> filterCriteria, ref int noOfPages, ref int totResCount)
        {
            SightseeingSearchResult[] noResult = new SightseeingSearchResult[0];
            if (string.IsNullOrEmpty(sessionId))
            {
                return noResult;
            }
            int recordsPerPage;

            if (filterCriteria.ContainsKey("recordsPerPage"))
            {
                recordsPerPage = Convert.ToInt32(filterCriteria["recordsPerPage"]);
            }
            else
            {
                throw new ArgumentException("Incomplete filter criteria. Need value of recordsPerPage.");
            }
            SightseeingSearchResult[] searchResultCache = Load(sessionId);
            if (searchResultCache.Length > 0)
            {
                #region get the filter criteria

                string catList = filterCriteria.ContainsKey("catList") ? filterCriteria["catList"] : string.Empty;
                string typeList = filterCriteria.ContainsKey("typeList") ? filterCriteria["typeList"] : string.Empty;
                string langList = filterCriteria.ContainsKey("langList") ? filterCriteria["langList"] : string.Empty;
                int orderby = filterCriteria.ContainsKey("orderBy") ? Convert.ToInt32(filterCriteria["orderBy"]) : 0;
                int pageNo = filterCriteria.ContainsKey("pageNo") ? Convert.ToInt32(filterCriteria["pageNo"]) : 1;

                #endregion

                SightseeingSearchResult[] sRFiltered = new SightseeingSearchResult[searchResultCache.Length];
                if ((catList != null && catList.Length > 0) || (typeList != null && typeList.Length > 0) || (langList != null && langList.Length > 0))
                {
                    int count = 0;
                    string catInfo = catList;
                    string[] typeInfo = typeList.Split('|');
                    string langInfo = langList;
                    bool typefilter, catfilter;
                    for (int k = 0; k < searchResultCache.Length; k++)
                    {
                        typefilter = false;
                        catfilter = false;
                        SightseeingSearchResult sight = new SightseeingSearchResult();
                        sight = searchResultCache[k];
                        if (typeInfo != null && typeInfo.Length > 1)
                        {
                            foreach (string type in typeInfo)
                            {
                                if (sight.SightseeingTypeList != null && sight.SightseeingTypeList.Count > 0 && sight.SightseeingTypeList.ContainsKey(type))
                                {
                                    typefilter = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            typefilter = true;
                        }
                        if (catInfo.Length > 1)
                        {
                            if (typefilter && sight.SightseeingCategoryList != null && sight.SightseeingCategoryList.Count > 0)
                            {
                                if (sight.SightseeingCategoryList.ContainsKey(catInfo))
                                {
                                    catfilter = true;
                                }
                                else
                                {
                                    catfilter = false;
                                }

                            }
                        }
                        else
                        {
                            catfilter = true;
                        }

                        if (typefilter && catfilter)
                        {
                            if (langInfo.Length >0)
                            {                              
                                    if (sight.TourOperationList[0].LangName[0] == "Unescorted" || (sight.TourOperationList[0].LanguageCode != null && sight.TourOperationList[0].LanguageCode.Contains(langInfo)))
                                    {
                                        sRFiltered[count++] = sight;
                                    }
                           
                            }
                            else
                            {
                                sRFiltered[count++] = sight;
                            }
                        }
                    }
                    if (sRFiltered.Length > count)
                    {
                        Array.Resize(ref sRFiltered, count);
                    }
                }
                else
                {
                    sRFiltered = searchResultCache;
                }
                //update the no of pages
                int totalFilteredRes = sRFiltered.Length;
                totResCount = totalFilteredRes;
                noOfPages = totalFilteredRes / recordsPerPage + Convert.ToInt16(totalFilteredRes % recordsPerPage > 0);

                //sorting the data by the given parameter
                switch (orderby)
                {
                    case 0: sRFiltered = SortByPrice(sRFiltered, 0);
                        break;
                    case 1: sRFiltered = SortByPrice(sRFiltered, 1);
                        break;
                    case 2: sRFiltered = SortByName(sRFiltered, 0);
                        break;
                    case 3: sRFiltered = SortByName(sRFiltered, 1);
                        break;
                    default:
                        break;
                }
                int endIndex = 0;
                int startIndex = recordsPerPage * (pageNo - 1);
                if ((startIndex + recordsPerPage) - 1 < totalFilteredRes)
                {
                    endIndex = (startIndex + recordsPerPage) - 1;
                }
                else
                {
                    endIndex = totalFilteredRes - 1;
                }
                //copy the required result for the requested page
                SightseeingSearchResult[] returnSearchResult = new SightseeingSearchResult[endIndex - startIndex + 1];
                for (int iX = 0; iX <= endIndex - startIndex; iX++)
                {
                    returnSearchResult[iX] = sRFiltered[startIndex + iX];
                }
                return returnSearchResult;
            }
            else
            {
                return noResult;
            }
        }

        /// <summary>
        /// Deserialize the ByteArray to object
        /// </summary>
        /// <param name="theByteArray"></param>
        /// <returns></returns>
        private object GetObjectWithByteArray(byte[] theByteArray)
        {
            MemoryStream ms = new MemoryStream(theByteArray);
            BinaryFormatter bf1 = new BinaryFormatter();
            ms.Position = 0;
            return bf1.Deserialize(ms);
        }

        /// <summary>
        /// Serialize the object to ByteArray
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private byte[] GetByteArrayWithObject(Object o)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf1 = new BinaryFormatter();
            bf1.Serialize(ms, o);
            return ms.ToArray();
        }

        /// <summary>
        /// Sort the result set order by price
        /// </summary>
        /// <param name="result"></param>
        /// <param name="order">if 0 low to high,1 high to low</param>
        /// <returns></returns>
        private SightseeingSearchResult[] SortByPrice(SightseeingSearchResult[] result, int order)
        {
            //Assumption : data from actual source comes sorted on Price in Ascending order
            if (order == 1)
            {
                Array.Reverse(result);
            }
            return result;
        }

        /// <summary>
        /// sort the result order by name
        /// </summary>
        /// <param name="result"></param>
        /// <param name="order">if 0 A-Z,1 Z-A</param>
        /// <returns></returns>
        private SightseeingSearchResult[] SortByName(SightseeingSearchResult[] result, int order)
        {
            for (int n = 0; n < result.Length - 1; n++)
            {
                for (int m = n + 1; m < result.Length; m++)
                {
                    if (result[n].ItemName.CompareTo(result[m].ItemName) > 0)
                    {
                        SightseeingSearchResult temp = result[n];
                        result[n] = result[m];
                        result[m] = temp;
                    }
                }
            }
            if (order == 1)
            {
                Array.Reverse(result);
            }

            return result;
        }


        #endregion
    }
    //Added on 09/2019 by somasekhar for  Activity(SightSeeing)
    [Serializable]
    public class DayWiseTourOperation
    {
        public DateTime BookingDate;
        public List<TourOperation> tourOperationList;
        public List<string> departurePointList;             //Binding the departure point details
        public bool isHotelRequired;                       //specifies departure point is hotel or not 
        public string DepTime;
    }


}
