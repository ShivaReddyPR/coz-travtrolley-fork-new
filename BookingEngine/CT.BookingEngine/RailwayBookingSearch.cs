﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    [Serializable]
    public class RailwayBookingSearch
    {
        //Offline Train Booking
        #region public properties
        /// <summary>
        /// BoardingDate
        /// </summary>
        public DateTime? BoardingDate { get; set; }
        /// <summary>
        /// TrainNumber
        /// </summary>
        public string TrainNumber { get; set; }
        /// <summary>
        /// TrainName
        /// </summary>
        public string TrainName { get; set; }
        /// <summary>
        /// AgentId
        /// </summary>
        public long? AgentId { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// PNR
        /// </summary>
        public string PNR { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        public int? CreatedBy { get; set; }
        /// <summary>
        /// CreatedOn
        /// </summary>
        public string CreatedOn { get; set; }
        /// <summary>
        /// Created From Date
        /// </summary>
        public DateTime? CreatedFrom { get; set; }
        /// <summary>
        /// CreatedOn To Date
        /// </summary>
        public DateTime? CreatedTo { get; set; }
        /// <summary>
        /// ReferenceId
        /// </summary>
        public string ReferenceId { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// PaxName
        /// </summary>
        public string PaxName { get; set; }
        /// <summary>
        /// NumberOfPages
        /// </summary>
        public int NumberOfPages { get; set; }
        /// <summary>
        /// PageNumber
        /// </summary>
        public int PageNumber { get; set; }
        /// <summary>
        /// PageSet - Each block for pagination
        /// </summary>
        public int PageSet { get; set; }
        /// <summary>
        /// TotalPageSet - Total number of page blocks
        /// </summary>
        public int TotalPageSet { get; set; }
        /// <summary>
        /// ResultCount - Result count to calculate pages
        /// </summary>
        public int ResultCount { get; set; }

        #endregion

        #region Constructors
        public RailwayBookingSearch()
        {
            PageNumber = 1;
            PageSet = 1;
        }
        #endregion

        #region Static Methods
        public static DataSet Search(RailwayBookingSearch search)
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@PageNumber", search.PageNumber));
                if (search.BoardingDate != null)
                    paramList.Add(new SqlParameter("@BoardingDate", search.BoardingDate));
                else
                    paramList.Add(new SqlParameter("@BoardingDate", DBNull.Value));
                if (search.TrainNumber != null)
                    paramList.Add(new SqlParameter("@TrainNumber", search.TrainNumber));
                else
                    paramList.Add(new SqlParameter("@TrainNumber", DBNull.Value));
                if (search.TrainName != null)
                    paramList.Add(new SqlParameter("@TrainName", search.TrainName));
                else
                    paramList.Add(new SqlParameter("@TrainName", DBNull.Value));
                if (search.AgentId != null)
                    paramList.Add(new SqlParameter("@AgentId", search.AgentId));
                else
                    paramList.Add(new SqlParameter("@AgentId", DBNull.Value));
                if (search.Email != null)
                    paramList.Add(new SqlParameter("@Email", search.Email));
                else
                    paramList.Add(new SqlParameter("@Email", DBNull.Value));
                if (search.PNR != null)
                    paramList.Add(new SqlParameter("@PNR", search.PNR));
                else
                    paramList.Add(new SqlParameter("@PNR", DBNull.Value));
                if (search.CreatedBy != null)
                    paramList.Add(new SqlParameter("@CreatedBy", search.CreatedBy));
                else
                    paramList.Add(new SqlParameter("@CreatedBy", DBNull.Value));
                if (search.CreatedOn != null)
                    paramList.Add(new SqlParameter("@CreatedOn", search.CreatedOn));
                else
                    paramList.Add(new SqlParameter("@CreatedOn", DBNull.Value));
                if (search.CreatedFrom != null)
                    paramList.Add(new SqlParameter("@CreatedFrom", search.CreatedFrom));
                else
                    paramList.Add(new SqlParameter("@CreatedFrom", DBNull.Value));
                if (search.CreatedTo != null)
                    paramList.Add(new SqlParameter("@CreatedTo", search.CreatedTo));
                else
                    paramList.Add(new SqlParameter("@CreatedTo", DBNull.Value));
                if (search.ReferenceId != null)
                    paramList.Add(new SqlParameter("@ReferenceId", search.ReferenceId));
                else
                    paramList.Add(new SqlParameter("@ReferenceId", DBNull.Value));
                if (search.Status != null)
                    paramList.Add(new SqlParameter("@Status", search.Status));
                else
                    paramList.Add(new SqlParameter("@Status", DBNull.Value));
                if (search.PaxName != null)
                    paramList.Add(new SqlParameter("@PaxName", search.PaxName));
                else
                    paramList.Add(new SqlParameter("@PaxName", DBNull.Value));
                return DBGateway.ExecuteQuery("BKE_SearchRailwayOfflineBooking", paramList.ToArray());

            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
