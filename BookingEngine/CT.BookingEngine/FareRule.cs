using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    [Serializable]
    public class FareRule
    {
        /// <summary>
        /// flightId of the flight to which the fare rule is associated
        /// </summary>
        private int flightId;
        /// <summary>
        /// gets or sets the flightId to which the fare rule is associated
        /// </summary>
        public int FlightId
        {
            get { return flightId; }
            set { flightId = value; }
        }

        /// <summary>
        /// three character code of the origin city
        /// </summary>
        private string origin;
        /// <summary>
        /// gets or sets the origin city code
        /// </summary>
        public string Origin
        {
            get { return origin; }
            set { origin = value; }
        }

        /// <summary>
        /// three character code of the destination city
        /// </summary>
        private string destination;
        /// <summary>
        /// gets or sets the destination city code
        /// </summary>
        public string Destination
        {
            get { return destination; }
            set { destination = value; }
        }

        /// <summary>
        /// two character airline code
        /// </summary>
        private string airline;
        /// <summary>
        /// gets or sets the airline code
        /// </summary>
        public string Airline
        {
            get { return airline; }
            set { airline = value; }
        }
        
        /// <summary>
        /// fare restrictions
        /// </summary>
        private string fareRestriction;
        /// <summary>
        /// gets or sets the fare restrictions
        /// </summary>
        public string FareRestriction
        {
            get { return fareRestriction; }
            set { fareRestriction = value; }
        }

        /// <summary>
        /// farebasis code
        /// </summary>
        private string fareBasisCode;
        /// <summary>
        /// gets or sets the fare basis code
        /// </summary>
        public string FareBasisCode
        {
            get { return fareBasisCode; }
            set { fareBasisCode = value; }
        }
        
        /// <summary>
        /// detailed fare rule.
        /// </summary>
        private string fareRuleDetail;
        /// <summary>
        /// gets or sets the fare rule detail
        /// </summary>
        public string FareRuleDetail
        {
            get { return fareRuleDetail; }
            set { fareRuleDetail = value; }
        }

        /// <summary>
        /// Return date if type is Return
        /// </summary>
        private DateTime returnDate;
        /// <summary>
        /// gets or sets the Return date
        /// </summary>
        public DateTime ReturnDate
        {
            get { return returnDate; }
            set { returnDate = value; }
        }


        /// <summary>
        /// Departure Time 
        /// </summary>
        private DateTime departureTime;
        /// <summary>
        /// gets or sets the DepartureTime
        /// </summary>
        public DateTime DepartureTime
        {
            get { return departureTime; }
            set { departureTime = value; }
        }


        /// <summary>
        /// gets or sets the Fare Rule Key Value for UAPI
        /// </summary>
        private string fareRuleKeyValue; 
        /// <summary>
        /// gets or sets the Fare Rule Key Value for UAPI
        /// </summary>
        public string FareRuleKeyValue //For UAPI
        {
            get { return fareRuleKeyValue; }
            set { fareRuleKeyValue = value; }
        }

        /// <summary>
        /// three character code of the destination city
        /// </summary>
        private string fareInfoRef; 
        /// <summary>
        /// gets or sets the Fare Info Refernce Key for UAPI
        /// </summary>
        public string FareInfoRef // For UAPI
        {
            get { return fareInfoRef; }
            set { fareInfoRef = value; }
        }

        /// <summary>
        /// Saves a fare rule
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("FareRule.Save entered : fareBasisCode = " + fareBasisCode);
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@flightId", flightId);
            paramList[1] = new SqlParameter("@origin", origin);
            paramList[2] = new SqlParameter("@destination", destination);
            paramList[3] = new SqlParameter("@airline", airline);
            if (fareRestriction != null)
            {
                paramList[4] = new SqlParameter("@fareRestriction", fareRestriction);
            }
            else
            {
                paramList[4] = new SqlParameter("@fareRestriction", string.Empty);
            }
            paramList[5] = new SqlParameter("@fareBasisCode", fareBasisCode);
            if (fareRuleDetail != null)
            {
                paramList[6] = new SqlParameter("@fareRuleDetail", fareRuleDetail);
            }
            else
            {
                paramList[6] = new SqlParameter("@fareRuleDetail", string.Empty);
            }
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddFareRule, paramList);
            //Trace.TraceInformation("FareRule.Save exiting : rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// Gets a list of fare rules.
        /// </summary>
        /// <param name="flightId">flightId of the flight for which fare rules are needed</param>
        /// <returns>Generic list of FareRule</returns>
        public static List<FareRule> GetFareRuleList(int flightId)
        {
            ////Trace.TraceInformation("FareRule.GetFareRuleList entered : flightId = " + flightId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            List<FareRule> fareRules = new List<FareRule>();
            //SqlConnection connection = DBGateway.GetConnection();
            //SqlDataReader dataReader = null;
            try
            {
                //dataReader = DBGateway.ExecuteReaderSP(SPNames.GetFareRuleList, paramList,connection);
                DataTable dtFareRules = DBGateway.FillDataTableSP(SPNames.GetFareRuleList, paramList);
                //while (dataReader.Read())
                if(dtFareRules != null)
                {
                    foreach (DataRow data in dtFareRules.Rows)
                    {
                        FareRule fareRule = new FareRule();
                        fareRule.flightId = flightId;
                        fareRule.origin =Convert.ToString(data["origin"]);
                        fareRule.destination = Convert.ToString(data["destination"]);
                        fareRule.airline = Convert.ToString(data["airline"]);
                        fareRule.fareRestriction = Convert.ToString(data["fareRestriction"]);
                        fareRule.fareBasisCode = Convert.ToString(data["fareBasisCode"]);
                        fareRule.fareRuleDetail = Convert.ToString(data["fareRuleDetail"]);
                        fareRules.Add(fareRule);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                //if (dataReader != null)
                //{
                //    dataReader.Close();
                //}
                //connection.Close();
            }
            //Trace.TraceInformation("FareRule.GetFareRuleList exiting : FareRule count = " + fareRules.Count);
            return fareRules;
        }
    }
}
