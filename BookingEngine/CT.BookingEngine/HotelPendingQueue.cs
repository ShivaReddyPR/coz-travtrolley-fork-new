﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine
{
    /// <summary>
    /// Summary description for WSGuest
    /// </summary>
    public class HotelGuest
    {
        private string title;
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        private string middleName;
        public string MiddleName
        {
            get
            {
                return middleName;
            }
            set
            {
                middleName = value;
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        private bool leadGuest;
        public bool LeadGuest
        {
            get
            {
                return leadGuest;
            }
            set
            {
                leadGuest = value;
            }
        }

        private int age;
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                age = value;
            }
        }

        private string addressline1;
        public string Addressline1
        {
            get
            {
                return addressline1;
            }
            set
            {
                addressline1 = value;
            }
        }

        private string addressline2;
        public string Addressline2
        {
            get
            {
                return addressline2;
            }
            set
            {
                addressline2 = value;
            }
        }

        private string countrycode;
        public string Countrycode
        {
            get
            {
                return countrycode;
            }
            set
            {
                countrycode = value;
            }
        }

        private string areacode;
        public string Areacode
        {
            get
            {
                return areacode;
            }
            set
            {
                areacode = value;
            }
        }

        private string phoneno;
        public string Phoneno
        {
            get
            {
                return phoneno;
            }
            set
            {
                phoneno = value;
            }
        }

        private string email;
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        private string city;
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        private string state;
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        private string country;
        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        private string zipcode;
        public string Zipcode
        {
            get
            {
                return zipcode;
            }
            set
            {
                zipcode = value;
            }
        }

        private HotelPaxType guestType;
        public HotelPaxType GuestType
        {
            get
            {
                return guestType;
            }
            set
            {
                guestType = value;
            }
        }

        private int roomIndex;
        public int RoomIndex
        {
            get
            {
                return roomIndex;
            }
            set
            {
                roomIndex = value;
            }
        }
        private string nationality;
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        private string nationalityCode;
        public string NationalityCode
        {
            get { return nationalityCode; }
            set { nationalityCode = value; }
        }
        public HotelGuest()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }

    public enum HotelGuestType
    {
        Adult = 1,
        Child = 2
    }

    public enum PayStatus
    {
        Accepted = 1,
        InProcess = 2,
        Rejected = 3
    }

    public struct PendingQueueHotelGuestDetail
    {
        public string Title;
        public string FirstName;
        public string MiddleName;
        public string LastName;
        public bool LeadGuest;
        public string Age;
        public string AddressLine1;
        public string AddressLine2;
        public string Countrycode;
        public string Areacode;
        public string Phoneno;
        public string Email;
        public string City;
        public string State;
        public string Country;
        public string Zipcode;
        public string GuestType;
        public int RoomIndex;
    }

    public class HotelPendingQueue
    {
        #region privateFields
        int hotelQueueId;
        HotelRating starRating;
        PaymentGatewaySource paySource;
        string paymentId;
        string orderId;
        int bookingId;
        decimal paymentAmount;
        int agencyId;
        string passengerInfo;
        string cityRef;
        string hotelName;
        string hotelCode;
        string address1;
        string address2;
        DateTime checkInDate;
        DateTime checkOutDate;
        int numberOfRooms;
        string roomName;
        HotelBookingStatus bookingStatus;
        HotelBookingSource source;
        bool isDomestic;
        string cityCode;
        PayStatus paymentStatus;
        string remarks;
        string ipAddress;
        string email;
        string phone;
        int adultCount;
        int childCount;
        string country;
        DateTime createdOn;
        int createdBy;
        DateTime lastModifiedOn;
        int lastModifiedBy;
        List<PendingQueueHotelGuestDetail> guestList;
        int leadPaxId;
        #endregion

        #region public Properties
        public int HotelQueueId
        {
            get
            {
                return hotelQueueId;
            }
            set
            {
                hotelQueueId = value;
            }
        }

        public HotelRating StarRating
        {
            get
            {
                return starRating;
            }
            set
            {
                starRating = value;
            }
        }

        public PaymentGatewaySource PaySource
        {
            get
            {
                return paySource;
            }
            set
            {
                paySource = value;
            }
        }

        public string PaymentId
        {
            get
            {
                return paymentId;
            }
            set
            {
                paymentId = value;
            }
        }

        public string OrderId
        {
            get
            {
                return orderId;
            }
            set
            {
                orderId = value;
            }
        }

        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }

        public decimal PaymentAmount
        {
            get
            {
                return paymentAmount;
            }
            set
            {
                paymentAmount = value;
            }
        }

        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }

        public string PassengerInfo
        {
            get
            {
                return passengerInfo;
            }
            set
            {
                passengerInfo = value;
            }
        }

        public string CityRef
        {
            get
            {
                return cityRef;
            }
            set
            {
                cityRef = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        public string HotelCode
        {
            get
            {
                return hotelCode;
            }
            set
            {
                hotelCode = value;
            }
        }

        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
            }
        }

        public DateTime CheckInDate
        {
            get
            {
                return checkInDate;
            }
            set
            {
                checkInDate = value;
            }
        }

        public DateTime CheckOutDate
        {
            get
            {
                return checkOutDate;
            }
            set
            {
                checkOutDate = value;
            }
        }

        public int NumberOfRooms
        {
            get
            {
                return numberOfRooms;
            }
            set
            {
                numberOfRooms = value;
            }
        }

        public string RoomName
        {
            get
            {
                return roomName;
            }
            set
            {
                roomName = value;
            }
        }

        public HotelBookingStatus BookingStatus
        {
            get
            {
                return bookingStatus;
            }
            set
            {
                bookingStatus = value;
            }
        }

        public HotelBookingSource Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
            }
        }

        public bool IsDomestic
        {
            get
            {
                return isDomestic;
            }
            set
            {
                isDomestic = value;
            }
        }

        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }

        public PayStatus PaymentStatus
        {
            get
            {
                return paymentStatus;
            }
            set
            {
                paymentStatus = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public int AdultCount
        {
            get
            {
                return adultCount;
            }
            set
            {
                adultCount = value;
            }
        }

        public int ChildCount
        {
            get
            {
                return childCount;
            }
            set
            {
                childCount = value;
            }
        }

        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public int CreatedBy
        {
            set
            {
                createdBy = value;
            }
            get
            {
                return createdBy;
            }
        }

        public DateTime LastModifiedOn
        {
            set
            {
                lastModifiedOn = value;
            }
            get
            {
                return lastModifiedOn;
            }
        }

        public int LastModifiedBy
        {
            set
            {
                lastModifiedBy = value;
            }
            get
            {
                return lastModifiedBy;
            }
        }

        public List<PendingQueueHotelGuestDetail> GuestList
        {
            get
            {
                return guestList;
            }
            set
            {
                guestList = value;
            }
        }

        public int LeadPaxId
        {
            get
            {
                return leadPaxId;
            }
            set
            {
                leadPaxId = value;
            }
        }

        #endregion
        /// <summary>
        /// Loads pending queue details for hotels.
        /// </summary>
        /// <param name="pageNo">Page Number</param>
        /// <param name="whereString">Where String</param>
        /// <returns>ITimesHotelPendingQueue List</returns>
        public static List<HotelPendingQueue> Load(int pageNo, string whereString)
        {
            List<HotelPendingQueue> tempList = new List<HotelPendingQueue>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@rowNo", pageNo * 100);
            paramList[1] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetITimesHotelPendingQueue, paramList, connection);
            if (data.HasRows)
            {
                while (data.Read())
                {
                    HotelPendingQueue itbd = new HotelPendingQueue();
                    itbd.hotelQueueId = Convert.ToInt32(data["hotelQueueId"]);
                    itbd.starRating = (HotelRating)(Enum.Parse(typeof(HotelRating), data["starRating"].ToString()));
                    itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                    itbd.paymentId = Convert.ToString(data["paymentId"]);
                    itbd.orderId = Convert.ToString(data["orderId"]);
                    if (data["bookingId"] != DBNull.Value)
                    {
                        itbd.bookingId = Convert.ToInt32(data["bookingId"]);
                    }
                    else
                    {
                        itbd.bookingId = 0;
                    }
                    itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                    itbd.agencyId = Convert.ToInt32(data["agencyId"]);
                    itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                    itbd.cityRef = Convert.ToString(data["cityRef"]);
                    itbd.hotelName = Convert.ToString(data["hotelName"]);
                    itbd.hotelCode = Convert.ToString(data["hotelCode"]);
                    itbd.address1 = Convert.ToString(data["address1"]);
                    if (data["address2"] != DBNull.Value)
                    {
                        itbd.address2 = Convert.ToString(data["address2"]);
                    }
                    else
                    {
                        itbd.address2 = string.Empty;
                    }
                    itbd.checkInDate = Convert.ToDateTime(data["checkInDate"]);
                    itbd.checkOutDate = Convert.ToDateTime(data["checkOutDate"]);
                    itbd.numberOfRooms = Convert.ToInt32(data["noOfRooms"]);
                    itbd.roomName = Convert.ToString(data["roomName"]);
                    itbd.bookingStatus = (HotelBookingStatus)(Enum.Parse(typeof(HotelBookingStatus), data["bookingStatus"].ToString()));
                    itbd.source = (HotelBookingSource)(Enum.Parse(typeof(HotelBookingSource), data["source"].ToString()));
                    itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    if (data["cityCode"] != DBNull.Value)
                    {
                        itbd.cityCode = Convert.ToString(data["cityCode"]);
                    }
                    else
                    {
                        itbd.cityCode = string.Empty;
                    }
                    itbd.paymentStatus = (PayStatus)(Enum.Parse(typeof(PayStatus), data["paymentStatus"].ToString()));
                    if (data["remarks"] != DBNull.Value)
                    {
                        itbd.remarks = Convert.ToString(data["remarks"]);
                    }
                    else
                    {
                        itbd.remarks = string.Empty;
                    }
                    itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                    itbd.email = Convert.ToString(data["email"]);
                    itbd.phone = Convert.ToString(data["phone"]);
                    itbd.adultCount = Convert.ToInt32(data["adultCount"]);
                    itbd.childCount = Convert.ToInt32(data["childCount"]);
                    itbd.country = Convert.ToString(data["country"]);
                    itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                    itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                    itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    tempList.Add(itbd);
                }
            }
            data.Close();
            connection.Close();
            return tempList;
        }
        /// <summary>
        /// Gets number of rows present according to where string.
        /// </summary>
        /// <param name="whereString">Where String.</param>
        /// <returns>Number of Rows.</returns>
        public static int GetRowCount(string whereString)
        {
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);

            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetITimesHotelPendingQueueCount, paramList, connection);
            int numberOfRows = 0;
            if (data.Read())
            {
                numberOfRows = Convert.ToInt32(data["numberOfRows"]);
            }
            data.Close();
            connection.Close();
            return numberOfRows;
        }
        /// <summary>
        /// Loads pending queue details for hotels.
        /// </summary>
        /// <param name="hotelQueueId">Queue Id</param>
        /// <returns>ITimesHotelPendingQueue object</returns>
        public static HotelPendingQueue GetTrip(int hotelQueueId)
        {
            HotelPendingQueue itbd = new HotelPendingQueue();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelQueueId", hotelQueueId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetITimesHotelPendingQueueDetailByHotelQueueId, paramList, connection);
            if (data.Read())
            {
                //ITimesBookingDetail itbd = new ITimesBookingDetail();
                itbd.hotelQueueId = Convert.ToInt32(data["hotelQueueId"]);
                itbd.starRating = (HotelRating)(Enum.Parse(typeof(HotelRating), data["starRating"].ToString()));
                itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                itbd.paymentId = Convert.ToString(data["paymentId"]);
                itbd.orderId = Convert.ToString(data["orderId"]);
                if (data["bookingId"] != DBNull.Value)
                {
                    itbd.bookingId = Convert.ToInt32(data["bookingId"]);
                }
                else
                {
                    itbd.bookingId = 0;
                }
                itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                itbd.agencyId = Convert.ToInt32(data["agencyId"]);
                itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                itbd.cityRef = Convert.ToString(data["cityRef"]);
                itbd.hotelName = Convert.ToString(data["hotelName"]);
                itbd.hotelCode = Convert.ToString(data["hotelCode"]);
                itbd.address1 = Convert.ToString(data["address1"]);
                if (data["address2"] != DBNull.Value)
                {
                    itbd.address2 = Convert.ToString(data["address2"]);
                }
                else
                {
                    itbd.address2 = string.Empty;
                }
                itbd.checkInDate = Convert.ToDateTime(data["checkInDate"]);
                itbd.checkOutDate = Convert.ToDateTime(data["checkOutDate"]);
                itbd.numberOfRooms = Convert.ToInt32(data["noOfRooms"]);
                itbd.roomName = Convert.ToString(data["roomName"]);
                itbd.bookingStatus = (HotelBookingStatus)(Enum.Parse(typeof(HotelBookingStatus), data["bookingStatus"].ToString()));
                itbd.source = (HotelBookingSource)(Enum.Parse(typeof(HotelBookingSource), data["source"].ToString()));
                itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                if (data["cityCode"] != DBNull.Value)
                {
                    itbd.cityCode = Convert.ToString(data["cityCode"]);
                }
                else
                {
                    itbd.cityCode = string.Empty;
                }
                itbd.paymentStatus = (PayStatus)(Enum.Parse(typeof(PayStatus), data["paymentStatus"].ToString()));
                if (data["remarks"] != DBNull.Value)
                {
                    itbd.remarks = Convert.ToString(data["remarks"]);
                }
                else
                {
                    itbd.remarks = string.Empty;
                }
                itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                itbd.email = Convert.ToString(data["email"]);
                itbd.phone = Convert.ToString(data["phone"]);
                itbd.adultCount = Convert.ToInt32(data["adultCount"]);
                itbd.childCount = Convert.ToInt32(data["childCount"]);
                itbd.country = Convert.ToString(data["country"]);
                itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
            }
            else
            {
                
            }
            data.Close();
            connection.Close();
            return itbd;
        }
        /// <summary>
        /// Saves pending queue details for hotel.
        /// </summary>
        /// <returns>Queue Id/Number of rows affected.</returns>
        public int Save()
        {
            if (hotelQueueId == 0)
            {
                SqlParameter[] paramList = new SqlParameter[31];

                paramList[0] = new SqlParameter("@starRating", (int)starRating);
                paramList[1] = new SqlParameter("@paymentGatewaySourceId", (int)(paySource));
                paramList[2] = new SqlParameter("@paymentId", paymentId);
                paramList[3] = new SqlParameter("@orderId", orderId);
                paramList[4] = new SqlParameter("@bookingId", bookingId);
                paramList[5] = new SqlParameter("@paymentAmount", paymentAmount);
                paramList[6] = new SqlParameter("@agencyId", agencyId);
                paramList[7] = new SqlParameter("@passengerInfo", passengerInfo);
                paramList[8] = new SqlParameter("@cityRef", cityRef);
                paramList[9] = new SqlParameter("@hotelName", hotelName);
                paramList[10] = new SqlParameter("@hotelCode", hotelCode);
                paramList[11] = new SqlParameter("@address1", address1);
                if (address2 != null)
                {
                    paramList[12] = new SqlParameter("@address2", address2);
                }
                else
                {
                    paramList[12] = new SqlParameter("@address2", string.Empty);
                }
                paramList[13] = new SqlParameter("@checkInDate", checkInDate);
                paramList[14] = new SqlParameter("@checkOutDate", checkOutDate);
                paramList[15] = new SqlParameter("@noOfRooms", numberOfRooms);
                paramList[16] = new SqlParameter("@roomName", roomName);
                paramList[17] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                paramList[18] = new SqlParameter("@source", (int)source);
                paramList[19] = new SqlParameter("@isDomestic", isDomestic);
                if (cityCode != null)
                {
                    paramList[20] = new SqlParameter("@cityCode", cityCode);
                }
                else
                {
                    paramList[20] = new SqlParameter("@cityCode", DBNull.Value);
                }
                paramList[21] = new SqlParameter("@paymentStatus", (int)paymentStatus);
                if (remarks == null || remarks == "")
                {
                    paramList[22] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[22] = new SqlParameter("@remarks", remarks);
                }
                paramList[23] = new SqlParameter("@ipAddress", ipAddress);
                paramList[24] = new SqlParameter("@email", email);
                if (phone == null || phone == "")
                {
                    paramList[25] = new SqlParameter("@phone", DBNull.Value);
                }
                else
                {
                    paramList[25] = new SqlParameter("@phone", phone);
                }
                paramList[26] = new SqlParameter("@adultCount", adultCount);
                paramList[27] = new SqlParameter("@childCount", childCount);
                paramList[28] = new SqlParameter("@country", country);
                paramList[29] = new SqlParameter("@createdBy", createdBy);
                paramList[30] = new SqlParameter("@hotelQueueId", SqlDbType.Int);
                paramList[30].Direction = ParameterDirection.Output;
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddITimesHotelPendingQueueDetails, paramList);
                if (rowsAffected > 0)
                {
                    return (int)paramList[30].Value;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@hotelQueueId", hotelQueueId);
                if (paymentId == null || paymentId == "")
                {
                    paramList[1] = new SqlParameter("@paymentId", DBNull.Value);
                }
                else
                {
                    paramList[1] = new SqlParameter("@paymentId", paymentId);
                }

                if (orderId == null || orderId == "")
                {
                    paramList[2] = new SqlParameter("@orderId", DBNull.Value);
                }
                else
                {
                    paramList[2] = new SqlParameter("@orderId", orderId);
                }
                paramList[3] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                paramList[4] = new SqlParameter("@paymentStatus", (int)paymentStatus);
                if (remarks == null || remarks == "")
                {
                    paramList[5] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[5] = new SqlParameter("@remarks", remarks);
                }
                paramList[6] = new SqlParameter("@bookingId", bookingId);
                paramList[7] = new SqlParameter("@lastModifiedBy", lastModifiedBy);

                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateITimesHotelPendingQueueDetails, paramList);
                if (rowsAffected > 0)
                {
                    return rowsAffected;
                }
                else
                {
                    return 0;
                }
            }
        }
        /// <summary>
        /// Updates Booking Status
        /// </summary>
        /// <param name="hotelQueueId">hotelQueueId</param>
        /// <param name="bookingStatus">bookingStatus</param>
        /// <returns>number of rows affected</returns>
        public static int UpdateBookingStatus(int hotelQueueId, HotelBookingStatus bookingStatus)
        {
            if (hotelQueueId <= 0)
            {
                throw new ArgumentException("QueueId Cannot be less than 0.", "hotelQueueId");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@hotelQueueId", hotelQueueId);
            paramList[1] = new SqlParameter("@bookingStatus", (int)bookingStatus);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateITimesHotelPendingQueueStatus, paramList);
            return rowsAffected;
        }
    }
}
