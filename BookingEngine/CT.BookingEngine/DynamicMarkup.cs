﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    /// <summary>
    /// Class for Storing Dynamic Markup details
    /// </summary>
    [Serializable]
    public class DynamicMarkup
    {
        /// <summary>
        /// insert newrecord id
        /// </summary>
        private const int NEW_RECORD = -1;
        /// <summary>
        /// Primary ID for the markup
        /// </summary>
        int dynID;
        /// <summary>
        /// Comparer Result Source
        /// </summary>
        string fromSource;
        /// <summary>
        /// Compared Result Source
        /// </summary>
        string compareSource;
        /// <summary>
        /// If airlineCodes are empty then all airline codes 
        /// need to be checked in the result otherwise specified only
        /// </summary>
        bool airlineCodeCheck;
        /// <summary>
        /// If routings are empty then all segments 
        /// need to be checked in the result otherwise specified only
        /// </summary>
        bool routeCheck;
        /// <summary>
        /// If bookingClasses are empty then all booking classes 
        /// need to be checked in the result otherwise specified only
        /// </summary>
        bool bookingClassCheck;
        /// <summary>
        /// All flight numbers need to check in the result
        /// </summary>
        bool flightNoCheck;
        /// <summary>
        /// Specified airline codes comma 
        /// </summary>
        string airlineCodes;
        /// <summary>
        /// Segment routes for comparison
        /// </summary>
        string routes;
        /// <summary>
        /// Booking classes for comparison
        /// </summary>
        string bookingClasses;
        /// <summary>
        /// Product this markup belongs to
        /// </summary>
        int productID;
        /// <summary>
        /// Agent this markup belongs to
        /// </summary>
        int agentID;
         long created_by;
        string transactionType;
        /// <summary>
        /// 
        /// </summary>
        List<DynamicMarkupThreshold> markupThresholds;

        public int DynID { get => dynID; set => dynID = value; }
        public string FromSource { get => fromSource; set => fromSource = value; }
        public string CompareSource { get => compareSource; set => compareSource = value; }
        public bool AirlineCodeCheck { get => airlineCodeCheck; set => airlineCodeCheck = value; }
        public bool RouteCheck { get => routeCheck; set => routeCheck = value; }
        public bool BookingClassCheck { get => bookingClassCheck; set => bookingClassCheck = value; }
        public bool FlightNo { get => flightNoCheck; set => flightNoCheck = value; }
        public string AirlineCodes { get => airlineCodes; set => airlineCodes = value; }
        public string Routes { get => routes; set => routes = value; }
        public string BookingClasses { get => bookingClasses; set => bookingClasses = value; }
        public int ProductID { get => productID; set => productID = value; }
        /// <summary>
        /// Agent this markup belongs to
        /// </summary>
        public int AgentID { get => agentID; set => agentID = value; }
        public List<DynamicMarkupThreshold> MarkupThresholds { get => markupThresholds; set => markupThresholds = value; }
        public long Created_by { get => created_by; set => created_by = value; }
        public string TransactionType { get => transactionType; set => transactionType = value; }

        #region Constructors
        public DynamicMarkup()
        {
            dynID = NEW_RECORD;

        }
        public DynamicMarkup(int id)
        {
            dynID = id;
            Getdetails(id);
        }
        #endregion
        
        public static List<DynamicMarkup> GetDynamicMarkups(List<string> sources, int agentId)
        {
            List<DynamicMarkup> dynamicMarkups = new List<DynamicMarkup>();

            try
            {
                if (sources != null && sources.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter("@P_Sources", string.Join(",", sources.ToArray())));
                    if (agentId > 0)
                        parameters.Add(new SqlParameter("@P_AgentId", agentId));
                    else
                        parameters.Add(new SqlParameter("@P_AgentId", DBNull.Value));

                    DataSet dsMarkups = DBGateway.FillSP("usp_GetDynamicMarkups", parameters.ToArray());

                    if (dsMarkups != null)
                    {
                        if (dsMarkups.Tables.Count > 1)
                        {
                            foreach (DataRow row in dsMarkups.Tables[0].Rows)
                            {
                                DynamicMarkup markup = new DynamicMarkup();
                                markup.agentID = Convert.ToInt32(row["AgentId"]);
                                markup.airlineCodeCheck = Convert.ToBoolean(row["airlineCodeCheck"]);
                                markup.airlineCodes = Convert.ToString(row["AirlineCodes"]);
                                markup.bookingClassCheck = Convert.ToBoolean(row["BookingClassCheck"]);
                                markup.bookingClasses = Convert.ToString(row["BookingClasses"]);
                                markup.compareSource = Convert.ToString(row["CompareSource"]);
                                markup.flightNoCheck = Convert.ToBoolean(row["flightNoCheck"]);
                                markup.fromSource = Convert.ToString(row["fromSource"]);
                                markup.productID = Convert.ToInt32(row["ProductID"]);
                                markup.routeCheck = Convert.ToBoolean(row["RouteCheck"]);
                                markup.dynID = Convert.ToInt32(row["DynId"]);

                                if (dsMarkups.Tables.Count == 2)
                                {
                                    if (markup.markupThresholds == null)
                                    {
                                        markup.markupThresholds = new List<DynamicMarkupThreshold>();
                                    }
                                    foreach (DataRow dr in dsMarkups.Tables[1].Rows)
                                    {
                                        if (Convert.ToInt32(dr["DynId"]) == markup.dynID)
                                        {
                                            DynamicMarkupThreshold threshold = new DynamicMarkupThreshold();
                                            threshold.FromAmount = Convert.ToDecimal(dr["FromAmount"]);
                                            threshold.MarkupType = Convert.ToString(dr["MarkupType"]);
                                            threshold.MarkupValue = Convert.ToDecimal(dr["MarkupValue"]);
                                            threshold.ToAmount = Convert.ToDecimal(dr["ToAmount"]);
                                            threshold.ThresholdId = Convert.ToInt32(dr["ThresholdId"]);
                                            threshold.DynId = Convert.ToInt32(dr["DynId"]);

                                            markup.markupThresholds.Add(threshold);
                                        }
                                    }
                                }

                                dynamicMarkups.Add(markup);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Retrieve Dynamic Markups. Reason: " + ex.ToString(), "");
            }
            return dynamicMarkups;
        }

        public void CalculateDynamicMarkup(SearchResult fromResult, SearchResult comparerResult, List<DynamicMarkup> dynamicMarkups)
        {
            try
            {
                string firstSource = SearchResult.GetFlightBookingSource(fromResult.ResultBookingSource).Replace("'", "");
                string secondSource = SearchResult.GetFlightBookingSource(comparerResult.ResultBookingSource).Replace("'", "");
                List<FlightInfo> fromSegments = new List<FlightInfo>(fromResult.Flights[0]);
                if (fromResult.Flights.Length > 1)
                {
                    fromSegments.AddRange(fromResult.Flights[1]);
                }
                List<FlightInfo> compareSegments = new List<FlightInfo>(comparerResult.Flights[0]);
                if (comparerResult.Flights.Length > 1)
                {
                    compareSegments.AddRange(comparerResult.Flights[1]);
                }

                DynamicMarkup markup = dynamicMarkups.Find(d => d.fromSource == firstSource && d.compareSource == secondSource);
                bool airlineMatch = false, routeMatch = false, bookingClassMatch = false, flightNoMatch = false;

                if (markup != null)
                {
                    if (markup.AirlineCodeCheck)
                    {
                        //if (string.IsNullOrEmpty(markup.AirlineCodes))
                        {
                            if (fromSegments.TrueForAll(f => compareSegments.All(x => x.Airline.Trim() == f.Airline.Trim())))
                            {
                                airlineMatch = true;
                            }
                        }
                        //else
                        //{
                        //    if (fromSegments.TrueForAll(f => markup.AirlineCodes.Split(',').All(x => x == f.Airline)) && compareSegments.TrueForAll(f => markup.AirlineCodes.Split(',').All(x => x == f.Airline)))
                        //    {
                        //        airlineMatch = true;
                        //    }
                        //}
                    }
                    else
                    {
                        airlineMatch = true;
                    }
                    if (markup.RouteCheck)
                    {
                        //if (string.IsNullOrEmpty(markup.Routes))
                        {
                            if (fromSegments.TrueForAll(f => compareSegments.Any(x => x.Origin.AirportCode.Trim() == f.Origin.AirportCode.Trim() && x.Destination.AirportCode.Trim() == f.Destination.AirportCode.Trim())))
                            {
                                routeMatch = true;
                            }
                        }
                        //else
                        //{
                        //    if (fromSegments.TrueForAll(f => markup.Routes.Split(',').All(x => x == f.Origin.AirportCode && x == f.Destination.AirportCode)) && compareSegments.TrueForAll(f => markup.Routes.Split(',').All(x => x == f.Origin.AirportCode && x == f.Destination.AirportCode)))
                        //    {
                        //        routeMatch = true;
                        //    }
                        //}
                    }
                    else
                    {
                        routeMatch = true;
                    }
                    if (bookingClassCheck)
                    {
                        //if (string.IsNullOrEmpty(markup.bookingClasses))
                        {
                            if (fromSegments.TrueForAll(f => compareSegments.All(x => x.BookingClass.Trim() == f.BookingClass.Trim())))
                            {
                                bookingClassMatch = true;
                            }
                        }
                        //else
                        //{
                        //    if (fromSegments.TrueForAll(f => markup.bookingClasses.Split(',').All(b => b == f.BookingClass)) && compareSegments.TrueForAll(x => markup.bookingClasses.Split(',').All(b => b == x.BookingClass)))
                        //    {
                        //        bookingClassMatch = true;
                        //    }
                        //}
                    }
                    else
                    {
                        bookingClassMatch = true;
                    }
                    if (flightNoCheck)
                    {
                        if (fromSegments.TrueForAll(f => compareSegments.All(x => x.FlightNumber.Trim() == f.FlightNumber.Trim())))
                        {
                            flightNoMatch = true;
                        }
                    }
                    else
                    {
                        flightNoMatch = true;
                    }
                    if (airlineMatch && routeMatch && bookingClassMatch && flightNoMatch)
                    {
                        List<DynamicMarkupThreshold> thresholds = markup.MarkupThresholds.FindAll(m => comparerResult.TotalFare > fromResult.TotalFare ? (decimal)(comparerResult.TotalFare - fromResult.TotalFare) >= m.FromAmount && (decimal)(comparerResult.TotalFare - fromResult.TotalFare) <= m.ToAmount : (decimal)(fromResult.TotalFare - comparerResult.TotalFare) >= m.FromAmount && (decimal)(fromResult.TotalFare - comparerResult.TotalFare) <= m.ToAmount);

                        if (thresholds != null && thresholds.Count > 0)
                        {
                            if (fromResult.MarkupThreshold == null)
                            {
                                string matchedFlight = string.Empty;
                                fromSegments.ForEach(f => matchedFlight += string.IsNullOrEmpty(matchedFlight) ? f.Airline + " " + f.FlightNumber : "," + f.Airline + " " + f.FlightNumber);
                                Audit.Add(EventType.Search, Severity.Normal, 1, "Dynamic Markup Applied for " + fromResult.ResultBookingSource.ToString() + "Original Price: " + fromResult.Currency + " " + fromResult.TotalFare + ", " + matchedFlight, "");
                                CalculateMarkup(fromResult, thresholds[0]);
                            }
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "Failed to retrieve Dynamic Markups for " + fromResult.ResultBookingSource + " to " + comparerResult.ResultBookingSource + ". Reason : " + ex.ToString(), "");
            }
        }

        void CalculateMarkup(SearchResult result, DynamicMarkupThreshold threshold)
        {
            decimal otherTaxes = (result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee) / (result.FareBreakdown.AsQueryable().ToList().Sum(f => f.PassengerCount));
            result.MarkupThreshold = threshold;
            foreach (Fare fare in result.FareBreakdown)
            {                
                if (threshold.MarkupType == "P")
                {
                    decimal totalFare = (decimal)(fare.TotalFare + (double)(otherTaxes * fare.PassengerCount));
                    decimal markup = totalFare * threshold.MarkupValue / 100;
                    fare.DynamicMarkup = markup;
                    fare.AgentMarkup += markup;
                    result.TotalFare += (double)markup;
                }
                else
                {
                    fare.DynamicMarkup = (threshold.MarkupValue * fare.PassengerCount);
                    fare.AgentMarkup += (threshold.MarkupValue * fare.PassengerCount);
                    result.TotalFare += (double)(threshold.MarkupValue * fare.PassengerCount);
                }
            }
            if (!string.IsNullOrEmpty(result.FareType) && !result.FareType.Contains("Dynamic Markup"))
                result.FareType += " Dynamic Markup";//To Identify the result which has been applied with dynamic markup

            if (!string.IsNullOrEmpty(result.Flights[0][0].SegmentFareType) && !result.Flights[0][0].SegmentFareType.Contains("Dynamic Markup"))
                result.Flights[0][0].SegmentFareType += " Dynamic Markup";//To Identify the result which has been applied with dynamic markup

        }

        #region Public Methods
        public void Save()
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[13];
                parameters[0] = new SqlParameter("@DynID", dynID);
                parameters[1] = new SqlParameter("@FromSource", fromSource);
                parameters[2] = new SqlParameter("@CompareSource", compareSource);
                parameters[3] = new SqlParameter("@AirlineCodeCheck", airlineCodeCheck);
                parameters[4] = new SqlParameter("@RouteCheck", routeCheck);
                parameters[5] = new SqlParameter("@BookingClassCheck", bookingClassCheck);
                parameters[6] = new SqlParameter("@FlightNoCheck", flightNoCheck);
                //parameters[7] = new SqlParameter("@AirlineCodes", airlineCodes);
                //parameters[8] = new SqlParameter("@Routes", routes);
                //parameters[9] = new SqlParameter("@BookingClasses", bookingClasses);
                parameters[7] = new SqlParameter("@ProductId", productID);
                parameters[8] = new SqlParameter("@AgentId", agentID);
                parameters[9] = new SqlParameter("@created_by", created_by);
                parameters[10] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                parameters[10].Direction = ParameterDirection.Output;
                parameters[11] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                parameters[11].Direction = ParameterDirection.Output;
                parameters[12] = new SqlParameter("@TransactionType", transactionType);
                DBGateway.ExecuteNonQuerySP("usp_DynamicMarkup_add_update", parameters);
                string messageType = Utility.ToString(parameters[10].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameters[11].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Static Method
        public static DataTable GetList(int companyId, string agentType, int loginAgent, ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];

                if (companyId > 0) paramList[0] = new SqlParameter("@P_COMPANY_ID", companyId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                if (!string.IsNullOrEmpty(agentType)) paramList[3] = new SqlParameter("@P_AGENT_TYPE", agentType);
                if (loginAgent > 0) paramList[4] = new SqlParameter("@P_LOGIN_AGENT_ID", loginAgent);

                return DBGateway.ExecuteQuery("ct_p_agent_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetAllActiveSourcesForFlight(ProductType Flight)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ProductId", Flight);
                return DBGateway.ExecuteQuery("usp_Get_Active_Sources", paramList).Tables[0];

            }
            catch { throw; }
        }
        public static DataTable GetDynamicMarkupMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("Usp_GetDynamicMarkup_Details").Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Private Methods
        private void Getdetails(int id)
        {
            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
        }
        private DataSet GetData(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@DynID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_DynamicMarkupMaster_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["DynID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                dynID = Utility.ToInteger(dr["DynID"]);
                fromSource = Utility.ToString(dr["FromSource"]);
                compareSource = Utility.ToString(dr["CompareSource"]);
                airlineCodeCheck = Utility.ToBoolean(dr["AirlineCodeCheck"]);
                routeCheck = Utility.ToBoolean(dr["RouteCheck"]);
                bookingClassCheck = Utility.ToBoolean(dr["BookingClassCheck"]);
                flightNoCheck = Utility.ToBoolean(dr["FlightNoCheck"]);
                airlineCodes = Utility.ToString(dr["AirlineCodes"]);
                routes = Utility.ToString(dr["Routes"]);
                bookingClasses = Utility.ToString(dr["BookingClasses"]);
                productID = Utility.ToInteger(dr["ProductId"]);
                agentID = Utility.ToInteger(dr["AgentId"]);
                transactionType = Utility.ToString(dr["TransactionType"]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class DynamicMarkupThreshold
    {
        private const int NEW_RECORD = -1;
        int thresholdId;
        int dynId;
        /// <summary>
        /// Total amount difference to be compared against the comparer source
        /// </summary>
        decimal fromAmount;
        /// <summary>
        /// Whether greater than or less than operator
        /// </summary>
        decimal toAmount;
        /// <summary>
        /// Markup value to be calculated
        /// </summary>
        decimal markupValue;
        /// <summary>
        /// Markup type Percent or Fixed
        /// </summary>
        string markupType;
        long created_by;

        public int ThresholdId { get => thresholdId; set => thresholdId = value; }
        public int DynId { get => dynId; set => dynId = value; }        
        public decimal MarkupValue { get => markupValue; set => markupValue = value; }
        public string MarkupType { get => markupType; set => markupType = value; }
        public decimal FromAmount { get => fromAmount; set => fromAmount = value; }
        public decimal ToAmount { get => toAmount; set => toAmount = value; }
        public long Created_by { get => created_by; set => created_by = value; }

        #region Constructors
        public DynamicMarkupThreshold()
        {
            thresholdId = NEW_RECORD;

        }
        public DynamicMarkupThreshold(int id)
        {
            thresholdId = id;
            Getdetails(id);
        }
        #endregion

        #region Public Method
        public void Save(int dynid)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[9];
                parameters[0] = new SqlParameter("@thresholdId", thresholdId);
                parameters[1] = new SqlParameter("@dynId", dynid);
                parameters[2] = new SqlParameter("@fromAmount", fromAmount);
                parameters[3] = new SqlParameter("@toamount", toAmount);
                parameters[4] = new SqlParameter("@markupValue", markupValue);
                parameters[5] = new SqlParameter("@markupType", markupType);
                parameters[6] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                parameters[6].Direction = ParameterDirection.Output;
                parameters[7] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                parameters[7].Direction = ParameterDirection.Output;
                parameters[8] = new SqlParameter("@created_by", created_by);
                DBGateway.ExecuteNonQuerySP("usp_DynamicMarkupThreshold_add_update", parameters);
                string messageType = Utility.ToString(parameters[6].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameters[7].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

            }
            catch (Exception ex)
            { throw ex; }
        }
        public static DataTable GetDynamicMarkupThresholdMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("Usp_GetDynamicMarkupThreshold_Details").Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Getdetails(int id)
        {
            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
        }
        private DataSet GetData(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ThresholdId", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_DynamicMarkupMasterThreshold_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["ThresholdId"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                thresholdId = Utility.ToInteger(dr["ThresholdId"]);
                dynId = Utility.ToInteger(dr["DynID"]);
                fromAmount = Utility.ToDecimal(dr["FromAmount"]);
                toAmount = Utility.ToDecimal(dr["ToAmount"]);
                markupValue = Utility.ToDecimal(dr["MarkupValue"]);
                markupType = Utility.ToString(dr["MarkupType"]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
