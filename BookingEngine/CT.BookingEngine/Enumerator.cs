namespace CT.BookingEngine
{
    /// <summary>
    /// This Enum contains the different types of charges in other charges
    /// </summary>
    public enum ChargeType
    {
        NoCharge=0,
        /// <summary>
        /// OtherCharges
        /// </summary>
        OtherCharges = 1,
        /// <summary>
        /// ServiceCharge
        /// </summary>
        ServiceCharge = 2,
        /// <summary>
        /// CreditCardCharge
        /// </summary>
        CreditCardCharge = 3,
        TrainSuperFastCharge = 4,
        TrainReservationCharge = 5,
        TrainOtherCharge = 6,
        TrainBedRollCharge = 7,
        CurrencyConversionCharges = 8,
        SupplierMarkup = 9,
        TBOMarkup = 10,
        BankHandlingCharges = 11,
        RevHandlingCharge = 12,
        ConvenienceCharge = 13
    }

    public enum APIBookingStatus
    {
        Failed = 1,
        Ticketed = 2,
        Rejected = 3
    }

    public enum PaymentGatewaySource
    {
        NoCard=0,
        /// <summary>
        /// This is payment gateway for HDFC
        /// </summary>
        HDFC = 1,
        /// <summary>
        /// This is payment gateway for American Express
        /// </summary>
        AMEX = 2,
        /// <summary>
        /// This is payment gateway for ICICI
        /// </summary>
        ICICI = 3,
        /// <summary>
        /// This is payment gateway for Oxicash
        /// </summary>
        OXICASH = 4,
        /// <summary>
        /// This is payment gateway for APICustomer
        /// </summary>
        APICustomer = 5,
        /// <summary>
        /// This is payment gateway for SBI
        /// </summary>
        SBI = 6,
        /// <summary>
        /// This is payment gateway for CCAvenue
        /// </summary>
        CCAvenue = 7,
        /// <summary>
        /// This is payment gateway for CCAvenue
        /// </summary>
        IANSource = 8,
        /// <summary>
        /// This is payment gateway for Beam
        /// </summary>
        Beam = 9,
        /// <summary>
        /// This is payment gateway for TicketVala
        /// </summary>
        TicketVala = 10,
        /// <summary>
        /// This is payment gateway for Axis
        /// </summary>
        Axis = 11,
        /// <summary>
        /// This is payment gateway for BillDesk
        /// </summary>
        BillDesk = 12,
        /// <summary>
        /// This is payment gateway for CozmoPG
        /// </summary>
        CozmoPG = 13,
        /// <summary>
        /// This is payment gateway for NEOPG
        /// </summary>
        NEOPG = 14,
        /// <summary>
        /// This is payment gateway for KNPay 
        /// added by Somasekhar on 08/05/2018
        /// </summary>
        KNPay = 15,
        /// <summary>
        /// This is payment gateway for SafexPay 
        /// added by Somasekhar on 27/06/2018
        /// </summary>
        SafexPay = 16,
        /// <summary>
        /// This is payment gateway for PayFort 
        /// added by Somasekhar on 10/11/2018
        /// </summary>
        PayFort = 17,

        RazorPay =18
    }
    public enum Gender
    {
        Null = 0,
        Male = 1,
        Female = 2
    }
    public enum ServiceFeeType
    {
        Default=0,
        Fixed = 1,
        Percentage = 2
    }
    public enum ServiceFeeDisplay
    {
        ShowInTax = 1,
        ShowSeparately = 2
    }    

    public enum ModeOfPayment
    {
        /// <summary>
        /// Value not assigned.
        /// </summary>?
        Null = 0,
        /// <summary>
        /// Payment made from cash account.
        /// </summary>
        Cash = 1,
        /// <summary>
        /// Payment made from credit account.
        /// </summary>
        Credit = 2,
        /// <summary>
        /// Payment made by credit card.
        /// </summary>
        CreditCard = 3,
        /// <summary>
        /// Payment made from Credit Limit
        /// </summary>
        CreditLimit = 4
    }
    public enum TrainBookingstatus
    {
        Confirm = 1,
        NotConfirmed = 2,
        Inprogress = 3,
        Cancelled = 4,
        Refunded = 5
    }
    public enum TrainQuota
    {
        //General
        GN = 1,
        //Ladies
        LD = 2,
        //Tatkal
        CK = 3
    }
    public enum TrainMealPreference
    {
        Veg = 1,
        Non_Veg = 2,
        NoPreference = 3
    }
    public enum TrainSeatPreference
    {
        //Apply space
        NoChoice = 1,
        WindowSeat = 2,
        Lower = 3,
        Middle = 4,
        Upper = 5,
        Side_Upper = 6,
        Side_Lower = 7
    }
    public enum TrainPaxType
    {
        SeniorCitizenMen = 1,
        SeniorCitizenWomen = 2,
        Adult = 3,
        Child = 4,     
        Infant=5
    }
    public enum TrainConcessionCode
    {
        // Not applicable
        ZZZZZZ = 1,
        //Female
        SRCTNW = 2,
        //Male
        SRCTZN = 3,
        //Not opted
        NOCONC = 4
    }
    public enum TrainType
    { 
        Rajadhani = 1,
        Shathabdi = 2,
        Ordinary = 3
    }

    public enum TrainPassengerGender
    { 
        Male = 1,
        Female = 2    
    }
    public enum IRCTCPaymentGateway
    {
        DEBIT = 1,
        RDS = 2
    }
    public enum IRCTCOperatorCode
    {
        HUTCH = 1,
        Tatatele = 2,
        Reliance = 3,
        Idea = 4,
        MTNL = 5,
        AirTel = 6
    }
    public enum IRCTCCardType
    {
        Cashcard = 1,
        ITZcard = 2
    }
    public enum IRCTCFareClass
    {
        Second = 1,
        Others = 2
    }
    public enum TrainPassengerStatus
    {
        Ticketed = 1,
        Cancelled = 2,
        UnderCancellation = 3,
        CancelledButNotRefunded =4
    }

    public enum TatkalRequestStatus
    {
        Requested=1,
        Successful=2,
        Unsuccessful=3,
        Cancelled=4
    }

    public enum TrainReservationChoice
    {
        None=1,
        BookOnlyOneLower=2,
        BookOnlyTwoLower=3,
        BookInSameCoach=4
    }

    public enum HotelApiBookingStatus
    {
        Failed = 0,
        Confirmed = 1,
        Cancelled = 2,
        Rejected = 5,
        Vouchered = 6
    }

    public enum IPAddress_Status
    {
        Blocked = 1,
        Suspicious = 2,
        Normal = 3
    }

    public enum Customer_Status
    {
        Blocked = 1,
        Suspicious = 2,
        Genuine = 3
    }
}
