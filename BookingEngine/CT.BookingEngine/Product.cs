using System;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;

namespace CT.BookingEngine
{
    [Serializable]
    public class Product
    {
        /// <summary>
        /// Product Type Id
        /// </summary>
        public int ProductTypeId;
        /// <summary>
        /// Product Id
        /// </summary>
        public int ProductId;

        /// <summary>
        /// Booking Mode(Whether booking is made from Automatic,Manual or ImportPNR )
        /// </summary>
        public BookingMode BookingMode;

        public ProductType ProductType;

        public Product()
        {
            
        }
        /// <summary>
        /// Get Booking Mode of A Product
        /// </summary>
        /// <param name="productId">Unique Id of a Product</param>
        /// <param name="productType">Type of Product</param>
        /// <returns></returns>
        public static BookingMode GetBookingMode(int productId, ProductType productType)
        {
            //Trace.TraceInformation("FlightItinerary.GetBookingMode entered : productId = " + productId);
            BookingMode bookingMode = new BookingMode();
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@productId", productId);
            paramList[1] = new SqlParameter("@productTypeId", (int)productType);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBookingMode, paramList,connection);
            using (DataTable dtBook = DBGateway.FillDataTableSP(SPNames.GetBookingMode, paramList))
            {
                if (dtBook != null && dtBook.Rows.Count > 0)
                {
                    DataRow data = dtBook.Rows[0];
                    if (data !=null && data["bookingModeId"] !=DBNull.Value)
                    {
                        bookingMode = (BookingMode)Enum.Parse(typeof(BookingMode),Convert.ToString(data["bookingModeId"]));
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("FlightItinerary.GetBookingMode exiting :" + bookingMode.ToString());
            return bookingMode;
        }

        /// <summary>
        /// It will return productId 
        /// </summary>
        /// <param name="bookingId">BookingId</param>
        /// <param name="productType"> Type of product</param>
        /// <returns>return product Id</returns>
        public static int GetProductIdByBookingId(int bookingId, ProductType productType)
        {
            //Trace.TraceInformation("Product.GetProductIdByBookingId entered : bookingId = " + bookingId);
            int productId = 0;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            paramList[1] = new SqlParameter("@productTypeId", (int)productType);

            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetProductIdByBookingId, paramList,connection);
            using (DataTable dtProduct = DBGateway.FillDataTableSP(SPNames.GetProductIdByBookingId, paramList))
            {
                if (dtProduct != null && dtProduct.Rows.Count > 0)
                {
                    DataRow data = dtProduct.Rows[0];
                    if (data !=null && data["productId"] !=DBNull.Value)
                    {
                        productId = Convert.ToInt32(data["productId"]);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("Product.GetProductIdByBookingId exiting :" + bookingId.ToString());
            return productId;
        }


        // Virtual Class for supporting multiple products
        public virtual void Save(Product product)
        {
            //No Implementation
        }
    }
}
