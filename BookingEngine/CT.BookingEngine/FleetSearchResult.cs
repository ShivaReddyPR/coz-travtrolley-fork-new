﻿using System;
//using System.Linq;

namespace CT.BookingEngine
{
    public enum RentType
    {
        Daily = 0,
        Weekly = 1,
        Monthly = 2,
    }
    public enum FleetCarType
    {
        Car = 0,
        SUV = 1,
    }
    public struct FleetSpecifications
    {
        public string FleetName;
        public FleetCarType CarType;
        public int SpecId;
        public string SpecName;
        public string SpecDetails;
        public int SrNo;
    }
    public class FleetSearchResult
    {
        int id;
        int idVehtariff;
        int vehId;
        int tariffDoc;
        RentType rentType;
        decimal dailyTariff;
        decimal tariff;
        decimal cdw;
        decimal pai;
        decimal scdw;
        decimal gps;
        decimal cseat;
        decimal vmd;
        decimal kmrest;
        decimal kmrate;
        decimal chauffer;
        decimal ins_Excess;
        DateTime fromDate;
        DateTime toDate;
        int srNo;
        string fleetName;
        FleetCarType carType;
        string imgFileName;
        FleetSpecifications[] fleetSpecs;
        decimal linkCharges;
        CarBookingSource source;
        PriceAccounts priceInfo;
        decimal totalPrice;                          //Item Price in decimal
        string currency;
        SerializableDictionary<string, decimal> sourceServiceAmount;
        int days;

        #region properities
        public int ID
        {
            get { return id; }
            set { id = value; }

        }
        public int IdVehTariff
        {
            get { return idVehtariff; }
            set { idVehtariff = value; }
        }

        public int VehId
        {
            get { return vehId; }
            set { vehId = value; }
        }

        public int TariffDoc
        {
            get { return tariffDoc; }
            set { tariffDoc = value; }
        }


        public RentType RentType
        {
            get { return rentType; }
            set { rentType = value; }
        }


        public decimal DailyTariff
        {
            get { return dailyTariff; }
            set { dailyTariff = value; }
        }
        public decimal Tariff
        {
            get { return tariff; }
            set { tariff = value; }
        }
        public decimal CDW //collision damage waiver
        {
            get { return cdw; }
            set { cdw = value; }
        }
        public decimal PAI //personal accident insurance
        {
            get { return pai; }
            set { pai = value; }
        }
        public decimal SCDW //super collision damage waiver
        {
            get { return scdw; }
            set { scdw = value; }
        }
        public decimal GPS //global positioning system
        {
            get { return gps; }
            set { gps = value; }
        }
        public decimal CSEAT
        {
            get { return cseat; }
            set { cseat = value; }
        }
        public decimal VMD
        {
            get { return vmd; }
            set { vmd = value; }
        }
        public decimal KMREST
        {
            get { return kmrest; }
            set { kmrest = value; }
        }
        public decimal KMRATE
        {
            get { return kmrate; }
            set { kmrate = value; }
        }


        public decimal Chauffer
        {
            get { return chauffer; }
            set { chauffer = value; }
        }
        public decimal Ins_Excess //insurance excess
        {
            get { return ins_Excess; }
            set { ins_Excess = value; }
        }
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        public int SRNO
        {
            get { return srNo; }
            set { srNo = value; }
        }
        public string FleetName
        {
            get { return fleetName; }
            set { fleetName = value; }
        }
        public FleetCarType CarType
        {
            get { return carType; }
            set { carType = value; }
        }
        public string ImgFileName
        {
            get { return imgFileName; }
            set { imgFileName = value; }
        }
        public FleetSpecifications[] FleetSpecs
        {
            get { return fleetSpecs; }
            set { fleetSpecs = value; }
        }

        public decimal LinkCharges
        {
            get { return linkCharges; }
            set { linkCharges = value; }
        }
        public CarBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public PriceAccounts PriceInfo
        {
            get { return priceInfo; }
            set { priceInfo = value; }
        }
        public decimal TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public int Days
        {
            get { return days; }
            set { days = value; }
        }
        public SerializableDictionary<string, decimal> SourceServiceAmount
        {
            get { return sourceServiceAmount; }
            set { sourceServiceAmount = value; }
        }
        #endregion
    }
}