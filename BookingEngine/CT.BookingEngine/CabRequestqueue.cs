﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CT.TicketReceipt.BusinessLayer;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public class CabRequestqueue
    {
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public int AgentId { get; set; }
        public int LocationId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public string ReferenceNumber { get; set; }

        public int DetId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime PickUpTime { get; set; }
        public string RentalType { get; set; }
        public string CabPreference { get; set; }
        public string CabModel { get; set; }
        public string PickUpFrom { get; set; }
        public string DropAt { get; set; }
        public string SpecialRequest { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string FlightNumber { get; set; }


        public static DataSet GetCabRequestQueue(DateTime FromDate, DateTime ToDate, int AgentId, int LocationId)
        {
            SqlParameter[] parameter = new SqlParameter[4];
            parameter[0] = new SqlParameter("@FromDate", FromDate);
            parameter[1] = new SqlParameter("@ToDate", ToDate);
            parameter[2] = new SqlParameter("@AgentId", AgentId);
            parameter[3] = new SqlParameter("@LocationId", LocationId);

            return DBGateway.FillSP(SPNames.GetCabRequestQueue, parameter);

        }
    }
}
