﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine
{
    public enum ForexStatus
    {
        Pending = 0,
        InProgress = 1,
        Confirmed = 2,
        Cancelled=3
    }
    [Serializable]
    public class ForexRequestDetails
    {
        public long FRID { get; set; }
        public string FRRefNo { get; set; }
        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public string UserName { get; set; }
        public long LocationId { get; set; }
        public string LocationName { get; set; }
        public bool IsOnbehalfAgent { get; set; }
        public string ProductType { get; set; }
        public string Currency { get; set; }
        public string ForexRequired { get; set; }
        public string AmountInINR { get; set; }
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string TravellingTo { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public List<ForexRequestRemarks> forexRequestRemarks { get; set; }
        public int save()
        {
            try
            {
                long id = FRID;
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@FRID", FRID));
                paramList[0].Direction = ParameterDirection.Output;
                paramList.Add(new SqlParameter("@AgentId", AgentId));
                paramList.Add(new SqlParameter("@FRRefNo", FRRefNo));
                paramList.Add(new SqlParameter("@LocationId", LocationId));
                paramList.Add(new SqlParameter("@IsOnbehalfAgent", IsOnbehalfAgent));
                paramList.Add(new SqlParameter("@ProductType", ProductType));
                paramList.Add(new SqlParameter("@Currency", Currency));
                paramList.Add(new SqlParameter("@ForexRequired", ForexRequired));
                paramList.Add(new SqlParameter("@AmountInINR", AmountInINR));
                paramList.Add(new SqlParameter("@Name", Name));
                paramList.Add(new SqlParameter("@MobileNumber", MobileNumber));
                paramList.Add(new SqlParameter("@Email", Email));
                paramList.Add(new SqlParameter("@TravellingTo", TravellingTo));
                paramList.Add(new SqlParameter("@CreatedBy", GetUserID()));
                paramList.Add(new SqlParameter("@CreatedOn", DateTime.Now));
                int res = DBGateway.ExecuteNonQuery("Usp_Forex_Request_Save", paramList.ToArray());
                FRID = paramList[0].Value != DBNull.Value ? Convert.ToInt32(paramList[0].Value) : FRID;
                if (FRID > 0)
                {
                    if (id == 0)
                    {
                        updateRefNo();
                    }
                    foreach (ForexRequestRemarks remarks in forexRequestRemarks)
                    {
                        remarks.FRID = FRID;
                        remarks.save();
                    }
                }
                else
                {
                    throw new Exception("Failed to saving the Forex Request");
                }
                return res;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, GetUserID(), "Forex Request failed to . Error:" + ex.ToString(), "");
                throw ex;
            }
        }
        private void updateRefNo()
        {
            try
            { 
                FRRefNo +=FRID;
                List <SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@FRID", FRID));  
                paramList.Add(new SqlParameter("@FRRefNo", FRRefNo));
                paramList.Add(new SqlParameter("@CreatedBy", GetUserID()));
                paramList.Add(new SqlParameter("@CreatedOn", DateTime.Now));
                int res = DBGateway.ExecuteNonQuery("Usp_Forex_Request_UpdateRefNo", paramList.ToArray()); 
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, GetUserID(), "Forex Request failed to update ref no . Error:" + ex.ToString(), "");
                throw ex;
            }
        }
        public List<ForexRequestDetails> Load()
        {
            List<ForexRequestDetails> forexRequestDetails = new List<ForexRequestDetails>();
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@FRID", FRID));
                paramList.Add(new SqlParameter("@AgentId", AgentId));
                paramList.Add(new SqlParameter("@ProductType", ProductType));
                paramList.Add(new SqlParameter("@Currency", Currency));
                paramList.Add(new SqlParameter("@Name", Name));
                paramList.Add(new SqlParameter("@MobileNumber", MobileNumber));
                paramList.Add(new SqlParameter("@Email", Email));
                paramList.Add(new SqlParameter("@TravellingTo", TravellingTo));
                paramList.Add(new SqlParameter("@startDate",CreatedOn));
                paramList.Add(new SqlParameter("@endDate", ModifiedOn));
                paramList.Add(new SqlParameter("@FRRefNo", FRRefNo));
                DataTable dt = DBGateway.FillDataTableSP("USP_GetForexRequestDetails", paramList.ToArray());
                foreach (DataRow dr in dt.Rows)
                {
                    ForexRequestDetails forexRequest = new ForexRequestDetails();
                    List<ForexRequestRemarks> requestRemarks = new List<ForexRequestRemarks>();
                    if (forexRequestDetails != null && forexRequestDetails.Exists(x => x.FRID == Convert.ToInt64(dr["FRID"])))
                    {
                        forexRequest = forexRequestDetails.Where(x => x.FRID == Convert.ToInt64(dr["FRID"])).FirstOrDefault();
                    }
                    forexRequest = forexRequest == null ? new ForexRequestDetails() : forexRequest; 
                    bool isExist = forexRequest == null ? false : true;
                    forexRequest.FRID = Convert.ToInt64(dr["FRID"]);
                    forexRequest.FRRefNo = Convert.ToString(dr["FRRefNo"]);
                    forexRequest.AgentId = Convert.ToInt32(dr["AgentId"]);
                    forexRequest.AgentName = Convert.ToString(dr["AgentName"]);
                    forexRequest.LocationId = Convert.ToInt64(dr["LocationId"]);
                    forexRequest.LocationName = Convert.ToString(dr["LocationName"]);
                    forexRequest.IsOnbehalfAgent = Convert.ToBoolean(dr["IsOnbehalfAgent"]);
                    forexRequest.ProductType = Convert.ToString(dr["ProductType"]);
                    forexRequest.Currency = Convert.ToString(dr["Currency"]);
                    forexRequest.ForexRequired = Convert.ToString(dr["ForexRequired"]);
                    forexRequest.AmountInINR = Convert.ToString(dr["AmountInINR"]);
                    forexRequest.Name = Convert.ToString(dr["Name"]);
                    forexRequest.MobileNumber = Convert.ToString(dr["MobileNumber"]);
                    forexRequest.Email = Convert.ToString(dr["Email"]);
                    forexRequest.TravellingTo = Convert.ToString(dr["TravellingTo"]);
                    forexRequest.UserName = Convert.ToString(dr["UserName"]);
                    if (dr["FRRID"] != DBNull.Value)
                    {
                        ForexRequestRemarks requestRemark = new ForexRequestRemarks();
                        requestRemark.FRRID = Convert.ToInt64(dr["FRRID"]);
                        requestRemark.FRID = Convert.ToInt64(dr["FRID"]);
                        requestRemark.RemarksStatus = Convert.ToString(dr["RemarksStatus"]);
                        requestRemark.Remarks = Convert.ToString(dr["Remarks"]);
                        requestRemark.CreatedOn = Convert.ToDateTime(dr["logTime"]);
                        if (forexRequest.forexRequestRemarks == null)
                        {
                            forexRequest.forexRequestRemarks = new List<ForexRequestRemarks>();
                        }
                         
                        forexRequest.forexRequestRemarks.Add(requestRemark);
                    }
                    if (isExist)
                        forexRequestDetails.Add(forexRequest);
                }
                forexRequestDetails= forexRequestDetails.Distinct().ToList();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, GetUserID(), "Forex Request failed to . Error:" + ex.ToString(), "");
                throw ex;
            }
            return forexRequestDetails;
        }
        public static int GetUserID()
        {
            return (Settings.LoginInfo != null ? (int)Settings.LoginInfo.UserID : 1);
        }
    }
    [Serializable]
    public class ForexRequestRemarks
    {
        public long FRRID { get; set; }
        public long FRID { get; set; }
        public string RemarksStatus { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public int save()
        {
            int res = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@FRRID", FRRID)); 
                paramList.Add(new SqlParameter("@FRID", FRID));
                paramList.Add(new SqlParameter("@RemarksStatus", RemarksStatus));
                paramList.Add(new SqlParameter("@Remarks", Remarks));
                paramList.Add(new SqlParameter("@CreatedBy", GetUserID()));
                paramList.Add(new SqlParameter("@CreatedOn", DateTime.Now));
                res = DBGateway.ExecuteNonQuery("Usp_Forex_RequestRemarks_Save", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, GetUserID(), "Forex Request Remarks failed to . Error:" + ex.ToString(), "");
                throw ex;
            }
            return res;
        }

        public static string LoadRemarks(long FRID)
        {
            string status = string.Empty;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>(); 
                paramList.Add(new SqlParameter("@FRID", FRID)); 
                DataTable dt = DBGateway.FillDataTableSP("Usp_GetForexRequestRemarks", paramList.ToArray());
                status = (dt != null && dt.Rows.Count > 0) ? dt.Rows [0]["RemarksStatus"].ToString(): string.Empty;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, GetUserID(), "Forex Request Remarks failed to . Error:" + ex.ToString(), "");
                throw ex;
            }
            return status;
        }
        public static int GetUserID()
        {
            return (Settings.LoginInfo != null ? (int)Settings.LoginInfo.UserID : 1);
        }
    }
}
