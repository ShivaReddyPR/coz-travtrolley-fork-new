using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public class HotelStaticData
    {
        #region Members
        private string hotelCode;
        private string cityCode;
        private string hotelName;
        private string hotelPicture;
        private string hotelDescription;
        private string hotelPolicy;
        private string hotelMap;
        private string hotelLocation;
        private string hotelAddress;
        private HotelRating rating;
        private string hotelFacilities;
        private string specialAttraction;
        private string phoneNumber;
        private string faxNumber;
        private string eMail;
        private string url;
        private string pinCode;
        private HotelBookingSource source;
        private DateTime timeStamp;
        private decimal fromPrice;
        private bool status;
        private List<string> images;
        private string hotelStaticID;
        private string travolutionaryID;
        private string rezliveID;
        private string illusionsHotelCode;
        string czInventoryId;
        string czInventoryCityID;
        string countryCode;
        string cityName;
        string rezLiveCityID;
        string illusionscitycode;
        string hotelExtraNetId;
        #endregion

        #region Properties
        public string HotelCode
        {
            get
            {
                return hotelCode;
            }
            set
            {
                hotelCode = value;
            }
        }
        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        public string HotelPicture
        {
            get
            {
                return hotelPicture;
            }
            set
            {
                hotelPicture = value;
            }
        }

        public string HotelDescription
        {
            get
            {
                return hotelDescription;
            }
            set
            {
                hotelDescription = value;
            }
        }
        public string HotelPolicy
        {
            get { return hotelPolicy; }
            set { hotelPolicy = value; }
        }
        public string HotelMap
        {
            get
            {
                return hotelMap;
            }
            set
            {
                hotelMap = value;
            }
        }

        public string HotelLocation
        {
            get
            {
                return hotelLocation;
            }
            set
            {
                hotelLocation = value;
            }
        }

        public string HotelAddress
        {
            get
            {
                return hotelAddress;
            }
            set
            {
                hotelAddress = value;
            }
        }
        public HotelRating Rating
        {
            get
            {
                return rating;
            }
            set
            {
                rating = value;
            }
        }
        public string HotelFacilities
        {
            get
            {
                return hotelFacilities;
            }
            set
            {
                hotelFacilities = value;
            }
        }
        public string SpecialAttraction
        {
            get
            {
                return specialAttraction;
            }
            set
            {
                specialAttraction = value;
            }
        }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }
        public string FaxNumber
        {
            get { return faxNumber; }
            set { faxNumber = value; }
        }
        public string EMail
        {
            get { return eMail; }
            set { eMail = value; }
        }
        public string URL
        {
            get { return url; }
            set { url = value; }
        }
        public string PinCode
        {
            get { return pinCode; }
            set { pinCode = value; }
        }
        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public DateTime TimeStamp
        {
            get { return timeStamp; }
            set { timeStamp = value; }
        }

        public decimal FromPrice
        {
            get { return fromPrice; }
            set { fromPrice = value; }
        }

        public bool Status
        {
            get { return status; }
            set { status = value; }
        }
        public List<string> Images
        {
            get { return images; }
            set { images = value; }
        }

        public string HotelStaticID { get => hotelStaticID; set => hotelStaticID = value; }
        public string TravolutionaryID { get => travolutionaryID; set => travolutionaryID = value; }
        public string RezliveID { get => rezliveID; set => rezliveID = value; }
        public string IllusionsHotelCode { get => illusionsHotelCode; set => illusionsHotelCode = value; }
        public string CZInventoryId { get => czInventoryId; set => czInventoryId = value; }
        public string CZInventoryCityID { get => czInventoryCityID; set => czInventoryCityID = value; }
        public string CountryCode { get => countryCode; set => countryCode = value; }
        public string CityName { get => cityName; set => cityName = value; }
        public string RezLiveCityID { get => rezLiveCityID; set => rezLiveCityID = value; }
        public string Illusionscitycode { get => illusionscitycode; set => illusionscitycode = value; }
        public string HotelExtraNetId { get => hotelExtraNetId; set => hotelExtraNetId = value; }
        #endregion

        #region Methods
        /// <summary>
        /// This Method is used to load the Static data
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="cityCode"></param>
        /// <param name="hSource"></param>
        public void Load(string hotel, string cityCode, HotelBookingSource hSource)
        {
            //Trace.TraceInformation("HotelStaticData.Load entered : hotelCode = " + hotel + "HotelSource = " + hSource.ToString());
            if (hotelCode != null && hotelCode.Length == 0)
            {
                throw new ArgumentException("hotelCode Should be enterd !", "hotelCode");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@hotelCode", hotel);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@source", (int)hSource);

            List<HotelRoom> hotelRooms = new List<HotelRoom>();
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelDataByCode, paramList,connection);
            using (DataTable dtHotelData = DBGateway.FillDataTableSP(SPNames.GetHotelDataByCode, paramList))
            {
                //if (data.Read())
                if (dtHotelData != null && dtHotelData.Rows.Count > 0)
                {
                    DataRow data = dtHotelData.Rows[0];
                    hotelCode =Convert.ToString(data["hotelCode"]);
                    hotelName = Convert.ToString(data["hotelName"]);
                    cityCode = Convert.ToString(data["hotelCity"]);
                    rating = (HotelRating)data["hotelRating"];
                    hotelLocation = Convert.ToString(data["location"]);
                    hotelDescription = Convert.ToString(data["description"]);
                    hotelPolicy = Convert.ToString(data["hotelPolicy"]);
                    hotelAddress = Convert.ToString(data["address"]);
                    specialAttraction = Convert.ToString(data["specialAttraction"]);
                    hotelMap = Convert.ToString(data["hotelMaps"]);
                    hotelFacilities = Convert.ToString(data["hotelFacility"]);
                    phoneNumber = Convert.ToString(data["phoneNumber"]);
                    faxNumber = Convert.ToString(data["faxNumber"]);
                    eMail = Convert.ToString(data["eMail"]);
                    url = Convert.ToString(data["URL"]);
                    pinCode = Convert.ToString(data["pinCode"]);
                    source = (HotelBookingSource)Convert.ToInt16(data["source"]);
                    timeStamp = Convert.ToDateTime(data["lastUpdate"]);
                    if (data["fromPrice"] != DBNull.Value)
                    {
                        fromPrice = Convert.ToDecimal(data["fromPrice"]);
                    }
                    else
                    {
                        fromPrice = 0;
                    }
                    if (data["status"] != DBNull.Value)
                    {
                        status = Convert.ToBoolean(data["status"]);
                    }

                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("HotelStaticData.Load exit :");
        }
        /// <summary>
        /// This Method is used to Save the Static Data.
        /// </summary>

        public void Save()
        {
            try
            {
                //Trace.TraceInformation("HotelStaticData.Save entered.");

                SqlParameter[] paramList = new SqlParameter[19];
                paramList[0] = new SqlParameter("@hotelCode", hotelCode);
                paramList[1] = new SqlParameter("@hotelName", hotelName);
                paramList[2] = new SqlParameter("@hotelCity", cityCode);
                paramList[3] = new SqlParameter("@hotelMaps", hotelMap);
                paramList[4] = new SqlParameter("@location", hotelLocation);
                paramList[5] = new SqlParameter("@description", hotelDescription);
                paramList[6] = new SqlParameter("@address", hotelAddress);
                paramList[7] = new SqlParameter("@specialAttraction", specialAttraction);
                paramList[8] = new SqlParameter("@hotelFacility", hotelFacilities);
                paramList[9] = new SqlParameter("@hotelRating", Convert.ToInt16(rating));
                paramList[10] = new SqlParameter("@phoneNumber", phoneNumber);
                paramList[11] = new SqlParameter("@faxNumber", faxNumber);
                paramList[12] = new SqlParameter("@eMail", eMail);
                paramList[13] = new SqlParameter("@URL", url);
                paramList[14] = new SqlParameter("@pinCode", pinCode);
                paramList[15] = new SqlParameter("@source", (int)source);
                paramList[16] = new SqlParameter("@hotelPolicy", hotelPolicy);
                paramList[17] = new SqlParameter("@fromPrice", fromPrice);
                paramList[18] = new SqlParameter("@status", status);

                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHotelData, paramList);
                //Trace.TraceInformation("HotelstaticData.Save exiting");
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }



        public void IllusionsSave()
        {
            try
            {
                //Trace.TraceInformation("HotelStaticData.Save entered.");

                SqlParameter[] paramList = new SqlParameter[12];
                paramList[0] = new SqlParameter("@hotelCode", hotelCode);
                paramList[1] = new SqlParameter("@hotelName", hotelName);
                paramList[2] = new SqlParameter("@hotelCity", cityCode);
                paramList[3] = new SqlParameter("@hotellat", hotelMap.Split('|')[0]);
                paramList[4] = new SqlParameter("@hotellng", hotelMap.Split('|')[1]);
                paramList[5] = new SqlParameter("@location", hotelLocation);
                paramList[6] = new SqlParameter("@description", hotelDescription);
                paramList[7] = new SqlParameter("@address", hotelAddress);
                //paramList[7] = new SqlParameter("@specialAttraction", specialAttraction);
                paramList[8] = new SqlParameter("@hotelFacility", hotelFacilities);
                //paramList[9] = new SqlParameter("@hotelRating", Convert.ToInt16(rating));
                paramList[9] = new SqlParameter("@phoneNumber", phoneNumber);
                //paramList[11] = new SqlParameter("@faxNumber", faxNumber);
                //paramList[12] = new SqlParameter("@eMail", eMail);
                paramList[10] = new SqlParameter("@URL", url);
                //paramList[14] = new SqlParameter("@pinCode", pinCode);
                //paramList[15] = new SqlParameter("@source", (int)source);
                //paramList[16] = new SqlParameter("@hotelPolicy", hotelPolicy);
                //paramList[17] = new SqlParameter("@fromPrice", fromPrice);
                //paramList[18] = new SqlParameter("@status", status);
                paramList[11] = new SqlParameter("@hotelpicture", hotelPicture);
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddIllusionshoteldata", paramList);
                //Trace.TraceInformation("HotelstaticData.Save exiting");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        /// <summary>
        /// This Method is used to Update the entire static Data.
        /// </summary>
       /* public void Update()
        {
            //Trace.TraceInformation("HotelStaticData.Update entered.");
            SqlParameter[] paramList = new SqlParameter[19];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@hotelName", hotelName);
            paramList[2] = new SqlParameter("@hotelCity", cityCode);
            paramList[3] = new SqlParameter("@hotelMaps", hotelMap);
            paramList[4] = new SqlParameter("@location", hotelLocation);
            paramList[5] = new SqlParameter("@description", hotelDescription);
            paramList[6] = new SqlParameter("@address", hotelAddress);
            paramList[7] = new SqlParameter("@specialAttraction", specialAttraction);
            paramList[8] = new SqlParameter("@hotelFacility", hotelFacilities);
            paramList[9] = new SqlParameter("@hotelRating", Convert.ToInt16(rating));
            paramList[10] = new SqlParameter("@phoneNumber", phoneNumber);
            paramList[11] = new SqlParameter("@faxNumber", faxNumber);
            paramList[12] = new SqlParameter("@eMail", eMail);
            paramList[13] = new SqlParameter("@URL", url);
            paramList[14] = new SqlParameter("@pinCode", pinCode);
            paramList[15] = new SqlParameter("@source", (int)source);
            paramList[16] = new SqlParameter("@hotelPolicy", hotelPolicy);
            paramList[17] = new SqlParameter("@fromPrice", fromPrice);
            paramList[18] = new SqlParameter("@status", status);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelStaticData, paramList);
            //Trace.TraceInformation("HotelstaticData.Update exiting");
        }*/
        /// <summary>
        /// This Method is used to Update the location.
        /// </summary>
        public void UpdateLocation()
        {
            //Trace.TraceInformation("HotelStaticData.UpdateLocation entered.");
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@hotelCity", cityCode);
            paramList[2] = new SqlParameter("@location", hotelLocation);
            paramList[3] = new SqlParameter("@source", (int)source);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelStaticDataLocation, paramList);
            //Trace.TraceInformation("HotelstaticData.UpdateLocation exiting");
        }
        #endregion
        #region DOTW hotel Ids
        public static DataTable GetStaticHotelIds(string cityCode, HotelBookingSource source)
        {
            DataTable dtHotels = new DataTable();
            try
            {
                dtHotels = GetStaticHotelIds(cityCode, source, 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtHotels;
        }
        public static DataTable GetStaticHotelIds(string cityCode,HotelBookingSource source,int maxChildAge)
        {
            DataTable dtHotels = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_CityCode", cityCode);
                paramList[1] = new SqlParameter("@P_Source", (int)source);
                paramList[2] = new SqlParameter("@P_MaxChildAge", maxChildAge);
                dtHotels = CT.TicketReceipt.DataAccessLayer.DBGateway.FillDataTableSP("usp_GetHotelIds", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtHotels;
        }

        public static Dictionary<string, DateTime> GetStaticDataCityDates(HotelBookingSource source)
        {
            Dictionary<string, DateTime> staticDataDates = new Dictionary<string, DateTime>();

            try
            {
                SqlParameter[] paramArray = new SqlParameter[1];
                paramArray[0] = new SqlParameter("@source", (int)source);
                DataTable dtCities = DBGateway.FillDataTableSP("usp_GetHotelCityLastUpdatedDateTime", paramArray);

                if (dtCities != null)
                {
                    foreach (DataRow dr in dtCities.Rows)
                    {
                        staticDataDates.Add(dr["hotelCity"].ToString().Trim(), Convert.ToDateTime(dr["lastUpdate"]));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return staticDataDates;
        }

        #endregion

        #region Gimmonix staticdata
        /// <summary>
        /// This Method is used to load Gimmonix  Static data based on Source
        /// </summary>
        /// <param name="cityname"></param>
        /// <param name="countrycode"></param>
        /// <param name="HotelIds"></param>
        /// <param name="source"></param>
        public List<HotelStaticData> StaticDataLoad(string cityname,string countrycode,string HotelIds,int source)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter>();
            List<HotelStaticData> hotelStaticData = new List<HotelStaticData>();
            try
            {
                if (!string.IsNullOrEmpty(cityname)) sqlParameter.Add(new SqlParameter("@P_CityName", cityname));
                if (!string.IsNullOrEmpty(countrycode)) sqlParameter.Add(new SqlParameter("@P_CountryCode", countrycode));
                if (!string.IsNullOrEmpty(HotelIds)) sqlParameter.Add(new SqlParameter("@P_HotelIds", HotelIds));
                if (source > 0) sqlParameter.Add(new SqlParameter("@P_Source", Convert.ToString(source)));
                using (DataTable dtHotelData = DBGateway.FillDataTableSP("USP_GETHOTALSTATICDATA", sqlParameter.ToArray()))
                {
                    if (dtHotelData != null && dtHotelData.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtHotelData.Rows.Count; i++)
                        {
                            HotelStaticData staticData = new HotelStaticData();
                            DataRow data = dtHotelData.Rows[i];
                            if (data["hotelCode"] != DBNull.Value)
                            {
                                staticData.hotelCode = Convert.ToString(data["hotelCode"]);
                            }
                            else
                            {
                                continue;
                            }
                            if (data["DisplayName"] != DBNull.Value)
                            {
                                staticData.hotelName = Convert.ToString(data["DisplayName"]);
                            }
                            //cityCode = Convert.ToString(data["hotelCity"]);
                            if (data["StarRating"] != DBNull.Value)
                            {
                                staticData.rating = (HotelRating)data["StarRating"];
                            }
                            else
                            {
                                staticData.rating = HotelRating.All;
                            }
                            // staticData.hotelLocation = Convert.ToString(data["location"]);
                            if (data["description"] != DBNull.Value)
                            {
                                staticData.hotelDescription = Convert.ToString(data["description"]);
                            }
                            //staticData.hotelPolicy = Convert.ToString(data["hotelPolicy"]);
                            if (data["Address"] != DBNull.Value)
                            {
                                staticData.hotelAddress = Convert.ToString(data["Address"]);
                            }
                            //staticData.specialAttraction = Convert.ToString(data["specialAttraction"]);
                            if (data["Lat"] != DBNull.Value && data["Lng"] != DBNull.Value)
                            {
                                staticData.hotelMap = Convert.ToString(data["Lat"]) + "||" + Convert.ToString(data["Lng"]);
                            }
                            if (data["hotelfacility"] != DBNull.Value || Convert.ToString(data["hotelfacility"]) != null)
                                staticData.hotelFacilities = Convert.ToString(data["hotelfacility"]);
                            if (data["Phone"] != DBNull.Value)
                            {
                                staticData.phoneNumber = Convert.ToString(data["Phone"]);
                            }
                            if (data["Fax"] != DBNull.Value)
                            {
                                staticData.faxNumber = Convert.ToString(data["Fax"]);
                            }
                            if (data["Email"] != DBNull.Value)
                            {
                                staticData.eMail = Convert.ToString(data["Email"]);
                            }
                            if (data["WebSite"] != DBNull.Value)
                            {
                                staticData.url = Convert.ToString(data["WebSite"]);
                            }
                            //staticData.pinCode = Convert.ToString(data["pinCode"]);
                            //staticData.source = (HotelBookingSource)Convert.ToInt16(data["source"]);
                            if (data["UpdateTime"] != DBNull.Value)
                            {
                                staticData.timeStamp = Convert.ToDateTime(data["UpdateTime"]);
                            }
                            if (data["status"] != DBNull.Value)
                            {
                                staticData.status = Convert.ToBoolean(data["status"]);
                            }
                            if (data["ImageUrl"] != DBNull.Value)
                            {
                                staticData.images = (Convert.ToString(data["ImageUrl"])).Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).Select(Convert.ToString).ToList();
                            }
                            if(data["HotelstaticID"] !=DBNull.Value)
                            {
                                staticData.hotelStaticID = (Convert.ToString(data["HotelstaticID"]));
                            }
                            hotelStaticData.Add(staticData);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return hotelStaticData;
        }
       
        /// <summary>
        /// This method is REZCITYID
        /// </summary>
        public static DataTable LoadCity(int SourceId, string CityName, string CountryCode)
        {
            try
            {
                DataTable dtR = new DataTable();
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@SourceId", SourceId);
                paramList[1] = new SqlParameter("@CityName", CityName);
                paramList[2] = new SqlParameter("@CountryCode", CountryCode);
                dtR = DBGateway.FillDataTableSP("Usp_GetCityIdbySource", paramList);
                return dtR;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is to get static data
        /// </summary>
        //public List<HotelStaticData> LoadStaticinfo(string cityname, string countrycode, string HotelIds)
        public List<HotelStaticData> LoadStaticinfo(string cityname, string countrycode, List<string>HotelStaticId)
        {
            
            int number = 0;
            string HotelIds = string.Empty;

            List<SqlParameter> sqlParameter = new List<SqlParameter>();
            List<HotelStaticData> hotelStaticData = new List<HotelStaticData>();

            if (string.IsNullOrEmpty(cityname) && string.IsNullOrEmpty(countrycode) && HotelStaticId == null)
            {
                return hotelStaticData;
            }
            else
            {
                try
                {
                    if (HotelStaticId != null)
                    {
                        HotelStaticId = HotelStaticId.Where(x => int.TryParse(x, out number)).ToList();
                        HotelIds = string.Join(",", HotelStaticId);
                    }
                    
                    if (!string.IsNullOrEmpty(cityname)) sqlParameter.Add(new SqlParameter("@P_CityName", cityname));
                    if (!string.IsNullOrEmpty(countrycode)) sqlParameter.Add(new SqlParameter("@P_CountryCode", countrycode));
                    if (!string.IsNullOrEmpty(HotelIds)) sqlParameter.Add(new SqlParameter("@P_HotelIds", HotelIds));

                    using (DataTable dtHotelData = DBGateway.FillDataTableSP("USP_GETHOTELSTATICINFO", sqlParameter.ToArray()))
                    {
                        if (dtHotelData != null && dtHotelData.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtHotelData.Rows.Count; i++)
                            {
                                HotelStaticData staticData = new HotelStaticData();
                                DataRow data = dtHotelData.Rows[i];
                                if (data["TravolutionaryID"] != DBNull.Value)
                                {
                                    staticData.travolutionaryID = Convert.ToString(data["TravolutionaryID"]);
                                }
                                if (data["rezliveID"] != DBNull.Value)
                                {
                                    staticData.rezliveID = Convert.ToString(data["rezliveID"]);
                                }
                                if (data["illusionsHotelCode"] != DBNull.Value)
                                {
                                    staticData.illusionsHotelCode = Convert.ToString(data["illusionsHotelCode"]);
                                }
                                if (data["DisplayName"] != DBNull.Value)
                                {
                                    staticData.hotelName = Convert.ToString(data["DisplayName"]);
                                }
                                if (data["StarRating"] != DBNull.Value)
                                {
                                    staticData.rating = (HotelRating)data["StarRating"];
                                }
                                else
                                {
                                    staticData.rating = HotelRating.All;
                                }
                                if (data["description"] != DBNull.Value)
                                {
                                    staticData.hotelDescription = Convert.ToString(data["description"]);
                                }
                                if (data["Address"] != DBNull.Value)
                                {
                                    staticData.hotelAddress = Convert.ToString(data["Address"]);
                                }
                                if (data["Lat"] != DBNull.Value && data["Lng"] != DBNull.Value)
                                {
                                    staticData.hotelMap = Convert.ToString(data["Lat"]) + "||" + Convert.ToString(data["Lng"]);
                                }
                                if (data["hotelfacility"] != DBNull.Value || Convert.ToString(data["hotelfacility"]) != null)
                                    staticData.hotelFacilities = Convert.ToString(data["hotelfacility"]);
                                if (data["Phone"] != DBNull.Value)
                                {
                                    staticData.phoneNumber = Convert.ToString(data["Phone"]);
                                }
                                if (data["Fax"] != DBNull.Value)
                                {
                                    staticData.faxNumber = Convert.ToString(data["Fax"]);
                                }
                                if (data["Email"] != DBNull.Value)
                                {
                                    staticData.eMail = Convert.ToString(data["Email"]);
                                }
                                if (data["WebSite"] != DBNull.Value)
                                {
                                    staticData.url = Convert.ToString(data["WebSite"]);
                                }
                                if (data["UpdateTime"] != DBNull.Value)
                                {
                                    staticData.timeStamp = Convert.ToDateTime(data["UpdateTime"]);
                                }
                                if (data["status"] != DBNull.Value)
                                {
                                    staticData.status = Convert.ToBoolean(data["status"]);
                                }
                                if (data["ImageUrl"] != DBNull.Value)
                                {
                                    staticData.images = (Convert.ToString(data["ImageUrl"])).Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).Select(Convert.ToString).ToList();
                                }
                                if (data["HotelstaticID"] != DBNull.Value)
                                {
                                    staticData.hotelStaticID = (Convert.ToString(data["HotelstaticID"]));
                                }
                                if (data["CZInventoryCityID"] != DBNull.Value)
                                {
                                    staticData.CZInventoryCityID = (Convert.ToString(data["CZInventoryCityID"]));
                                }
                                if (data["CZInventoryId"] != DBNull.Value)
                                {
                                    staticData.CZInventoryId = (Convert.ToString(data["CZInventoryId"]));
                                }
                                if (data["Illusionscitycode"] != DBNull.Value)
                                {
                                    staticData.illusionscitycode = (Convert.ToString(data["Illusionscitycode"]));
                                }
                                if (data["RezLiveCityID"] != DBNull.Value)
                                {
                                    staticData.rezLiveCityID = (Convert.ToString(data["RezLiveCityID"]));
                                }
                                if (data["hotelextraNetId"] != DBNull.Value)
                                {
                                    staticData.HotelExtraNetId = (Convert.ToString(data["hotelextraNetId"]));
                                }
                                staticData.CountryCode = countrycode;
                                staticData.CityName = cityname;
                                hotelStaticData.Add(staticData);

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return hotelStaticData;
            }
        }


        #endregion
    }
}
