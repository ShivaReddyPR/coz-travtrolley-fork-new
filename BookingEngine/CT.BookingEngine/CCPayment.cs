using System;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    /// <summary>
    /// Contains information about payment made with credit card.
    /// </summary>
    /// 
    [Serializable]
    public class CCPayment
    {
        #region Member Variables
        /// <summary>
        /// Unique id assigned to this payment, when saved in DB.
        /// </summary>
        private int id;
        /// <summary>
        /// Unique id assigned to this payment.
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Flight id for which the payment is made.
        /// </summary>
        private int flightId;
        /// <summary>
        /// Flight id for which this payment is made.
        /// </summary>
        public int FlightId
        {
            get { return flightId; }
            set { flightId = value; }
        }

        /// <summary>
        /// Credit Card detail.
        /// </summary>
        private CreditCard card;
        /// <summary>
        /// Credit card detail, using which payment is made.
        /// </summary>
        public CreditCard Card
        {
            get { return card; }
            set { card = value; }
        }

        /// <summary>
        /// Amout paid or to be paid with credit card.
        /// </summary>
        private decimal amount;
        /// <summary>
        /// Amount paid or to be paid with the credit card.
        /// </summary>
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        /// <summary>
        /// Currency of amount.
        /// </summary>
        private string currency;
        /// <summary>
        /// Three character currency code of the amount.
        /// </summary>
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        /// <summary>
        /// Auth Code (received after payment)
        /// </summary>
        private string authCode;
        /// <summary>
        /// Auth Code for the payment made.
        /// </summary>
        public string AuthCode
        {
            get { return authCode; }
            set { authCode = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Saves the current instance of object.
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("CCPayment.Save entered: flightId = " + flightId + ", amount = " + amount);
            if (flightId <= 0)
            {
                throw new ArgumentException("FlightId must be a positive non zero integer.");
            }
            if (amount <= 0)
            {
                throw new ArgumentException("Amount must be positive non zero value.");
            }
            if (currency == null || currency.Length != 3)
            {
                throw new ArgumentException("Currency must be assigned a three character currency code.");
            }
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@flightId", flightId);
            paramList[1] = new SqlParameter("@amount", amount);
            paramList[2] = new SqlParameter("@currency", currency);
            if (authCode != null)
            {
                paramList[3] = new SqlParameter("@authCode", authCode);
            }
            else
            {
                paramList[3] = new SqlParameter("@authCode", DBNull.Value);
            }
            paramList[4] = new SqlParameter("@cardHoldersName", card.Name);
            paramList[5] = new SqlParameter("@cardNumber", card.Number.Substring(0, 2) + card.Number.Substring(12));
            paramList[6] = new SqlParameter("@id", SqlDbType.Int);
            paramList[6].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddCCPayment, paramList);
            if (rowsAffected > 0)
            {
                id = (int)paramList[6].Value;
                //CT.Core.Audit.Add(CT.Core.EventType.Book, CT.Core.Severity.Normal, 0, "Payment by credit card: saved successfully. flightId = " + flightId + ", amount = " + amount + ", id = " + id, "");
            }
            else
            {
                CT.Core.Audit.Add(CT.Core.EventType.Book, CT.Core.Severity.Normal, 0, "Payment by credit card: save failed. flightId = " + flightId + ", amount = " + amount, "");
            }
            //Trace.TraceInformation("CCPayment.Save exiting: id = " + id);
        }

        public static CCPayment Load(int flightId)
        {
            //Trace.TraceInformation("CCPayment.Load entered: flightid = " + flightId);
            CCPayment ccPayment = new CCPayment();
            ccPayment.flightId = flightId;
            ccPayment.Load();
            //Trace.TraceInformation("CCPayment.Load exiting: id = ");
            return ccPayment;
        }

        public void Load()
        {
            ////Trace.TraceInformation("CCPayment.Load entered: flightid = " + flightId);
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetCCPayment, paramList, connection);
            CCPayment ccPayment = new CCPayment();
            using (DataTable dtCCPayment = DBGateway.FillDataTableSP(SPNames.GetCCPayment, paramList))
            {
                if (dtCCPayment !=null && dtCCPayment.Rows.Count > 0)
                {
                    DataRow data = dtCCPayment.Rows[0];
                    amount =Convert.ToDecimal(data["amount"]);
                    if (data["authCode"] != DBNull.Value)
                    {
                        authCode =Convert.ToString(data["authCode"]);
                    }   // else the value will remain null.
                    currency = Convert.ToString(data["currency"]);
                    id = Convert.ToInt32(data["id"]);
                    flightId = Convert.ToInt32(data["flightId"]);
                    card = new CreditCard();
                    if (data["cardHoldersName"] != DBNull.Value)
                    {
                        card.Name = Convert.ToString(data["cardHoldersName"]);
                    }   // else the value will remain null.
                    if (data["cardNumber"] != DBNull.Value)
                    {
                        string cardNumber = Convert.ToString(data["cardNumber"]).Trim();
                        card.Number = cardNumber.Substring(0, 2) + "XXXXXXXXXX" + cardNumber.Substring(2);
                    }   // else the value will remain null.
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("CCPayment.Load exiting: id = " );
        }
        #endregion
    }

    /// <summary>
    /// Credit Card detail
    /// </summary>
    [Serializable]
    public class CreditCard
    {
        /// <summary>
        /// Card holder's name.
        /// </summary>
        private string name;
        /// <summary>
        /// Card holder's name.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        /// <summary>
        /// 16 digit credit card number.
        /// </summary>
        private string number;
        /// <summary>
        /// 16 digit credit card number.
        /// </summary>
        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        /// <summary>
        /// Expiry date in "MMyy" format.
        /// </summary>
        private string expDate;
        /// <summary>
        /// Expiry date in "MMyy" format.
        /// </summary>
        public string ExpDate
        {
            get { return expDate; }
            set { expDate = value; }
        }

        /// <summary>
        /// CVV number of the card.
        /// </summary>
        private string cvv;
        /// <summary>
        /// CVV number of the card.
        /// </summary>
        public string CVV
        {
            get { return cvv; }
            set { cvv = value; }
        }

        /// <summary>
        /// Two character code of the company.
        /// </summary>
        private string company;
        /// <summary>
        /// Two character code of the company.
        /// </summary>
        public string Company
        {
            get { return company; }
            set { company = value; }
        }
        /// <summary>
        /// Credit card type Visa, Master
        /// </summary>
        private string _CardType;
        public string CardType { get => _CardType; set => _CardType = value; }
    }
}
