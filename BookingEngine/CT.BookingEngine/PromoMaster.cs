﻿using System;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CT.Core;

namespace CT.BookingEngine
{
    [Serializable]
    public class PromoMaster
    {
        #region PromoVariables
        int promoId;
        string promoCode;
        string promoName;
        int productId;
        DateTime bookDateFrom;
        DateTime bookDateTo;
        DateTime travelDateFrom;
        DateTime travelDateTo;
        string propertyCode;
        decimal minTranxAmount;
        int minPaxCount;
        int minRoomCount;
        string promoCity;
        string promoSource;
        string discountType;
        decimal discountValue;
        int stockInHand;
        int stockInUse;
        string status;
        int promoMemberId;
        DateTime createdOn;
        int createdBy;
        DateTime modifiedOn;
        int modifiedBy;
        #endregion

        #region Promo Properties
        public int PromoId
        {
            get { return promoId; }
            set { promoId = value; }
        }
        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }
        public string PromoName
        {
            get { return promoName; }
            set { promoName = value; }
        }
        public int ProductID
        {
            get { return productId; }
            set { productId = value; }
        }
        public DateTime BookDateFrom
        {
            get { return bookDateFrom; }
            set { bookDateFrom = value; }
        }
        public DateTime BookDateTo
        {
            get { return bookDateTo; }
            set { bookDateTo = value; }
        }
        public DateTime TravelDateFrom
        {
            get { return travelDateFrom; }
            set { travelDateFrom = value; }
        }
        public DateTime TravelDateTo
        {
            get { return travelDateTo; }
            set { travelDateTo = value; }
        }
        public string PropertyCode
        {
            get { return propertyCode; }
            set { propertyCode = value; }
        }
        public decimal MinTranxAmount
        {
            get { return minTranxAmount; }
            set { minTranxAmount = value; }
        }
        public int MinPaxCount
        {
            get { return minPaxCount; }
            set { minPaxCount = value; }
        }
        public int MinRoomCount
        {
            get { return minRoomCount; }
            set { minRoomCount = value; }
        }
        public string PromoCity
        {
            get { return promoCity; }
            set { promoCity = value; }
        }
        public string PromoSource
        {
            get { return promoSource; }
            set { promoSource = value; }
        }
        public string DoscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }
        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }
        public int StockInHand
        {
            get { return stockInHand; }
            set { stockInHand = value; }
        }
        public int StockInUse
        {
            get { return stockInUse; }
            set { stockInUse = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public int PromoMemberId
        {
            get { return promoMemberId; }
            set { promoMemberId = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }
        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }
        #endregion

        #region Static Method

        /* Retrieving Promo details*/
        public static DataTable GetProductPromotions(int productId, DateTime bookDateFrom, DateTime travelDateFrom, string propertyCode, decimal minTranxAmount, int minPaxCount, int minRoomCount, string promoCity, string promoSource, int promoMemberId, int minNights)
        {
            DataTable dtPromotions = null;
            try
            {
                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@P_ProductId", productId);
                paramList[1] = new SqlParameter("@P_BookingDate", bookDateFrom);
                paramList[2] = new SqlParameter("@P_TravelDate", travelDateFrom);
                paramList[3] = new SqlParameter("@P_PropertyCode", propertyCode);
                paramList[4] = new SqlParameter("@P_MinTranxAmount", minTranxAmount);
                paramList[5] = new SqlParameter("@P_MinPaxCount", minPaxCount);
                paramList[6] = new SqlParameter("@P_MinRoomCount", minRoomCount);
                paramList[7] = new SqlParameter("@P_City", promoCity);
                paramList[8] = new SqlParameter("@P_Source", promoSource);
                paramList[9] = new SqlParameter("@P_MemberId", promoMemberId);
                paramList[10] = new SqlParameter("@P_MinNights", minNights);
                dtPromotions = DBGateway.ExecuteQuery("usp_GetProductPromotions", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
            return dtPromotions;
        }



        #endregion

    }

    public class PromoDetails
    {
        #region PromoDetailsVariables

        int promoTranxId;
        int promoId;
        string promoCode;
        int productId;
        string discountType;
        decimal discountAmount;
        decimal discountValue;
        int referenceId;
        int createdBy;
        #endregion

        #region PromoDetailsProperties
        public int PromoTranxId
        {
            get { return promoTranxId; }
            set { promoTranxId = value; }
        }
        public int PromoId
        {
            get { return promoId; }
            set { promoId = value; }
        }

        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }

        public int ProductId
        {
            get { return productId; }
            set { productId = value; }
        }
        public string DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }
        public decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }
        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }
        public int ReferenceId
        {
            get { return referenceId; }
            set { referenceId = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        #endregion

        public void Save()
        {
            try
            {
                SqlParameter[] param = new SqlParameter[8];

                param[0] = new SqlParameter("@promoId", promoId);
                param[1] = new SqlParameter("@promoCode", promoCode);
                param[2] = new SqlParameter("@promoProductId", productId);
                param[3] = new SqlParameter("@promoDiscountType", discountType);
                param[4] = new SqlParameter("@PromoDiscountAmount", discountAmount);
                param[5] = new SqlParameter("@PromoDiscountValue", discountValue);
                param[6] = new SqlParameter("@PromoReferenceId", referenceId);
                param[7] = new SqlParameter("@PromoCreatedBy", createdBy);
                DBGateway.ExecuteNonQuerySP("usp_AddPromoDetails", param);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
        }

        public static DataTable GetPromoDetails(int productReferenceId)
        {
            DataTable dtPromoDetails = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ItineraryId", productReferenceId);

                dtPromoDetails = DBGateway.FillDataTableSP("usp_GetPromoTranxDetails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(PromoDetails)Failed to load Promo Details for ItineraryId : " + productReferenceId + ", Error:" + ex.ToString(), "");
            }

            return dtPromoDetails;
        }

        public void Load(int ProdRefId)
        {
            try
            {
                DataTable dt = GetPromoDetails(ProdRefId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    PromoTranxId = Convert.ToInt32(dt.Rows[0]["Promo_Tranx_Id"]);
                    PromoId = Convert.ToInt32(dt.Rows[0]["Promo_Id"]);
                    PromoCode = Convert.ToString(dt.Rows[0]["Promo_Code"]);
                    ProductId = Convert.ToInt32(dt.Rows[0]["Promo_Product_Id"]);
                    DiscountType = Convert.ToString(dt.Rows[0]["Promo_Discount_Type"]);
                    DiscountAmount = Convert.ToDecimal(dt.Rows[0]["Promo_Discount_Amount"]);
                    DiscountValue = Convert.ToDecimal(dt.Rows[0]["Promo_Discount_Value"]);
                    CreatedBy = Convert.ToInt32(dt.Rows[0]["Promo_CreatedBy"]);
                    ReferenceId = ProdRefId;
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
