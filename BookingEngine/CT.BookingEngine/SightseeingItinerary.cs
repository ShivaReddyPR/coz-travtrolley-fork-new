using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Transactions;
using System.Net;

namespace CT.BookingEngine
{
    public enum SightseeingBookingSource
    {
        GTA = 1,
        CZA = 2,
        HOTELBEDS =3
    }
    public enum SightseeingBookingStatus
    {
        Confirmed = 1,
        Cancelled = 2,
        Failed = 0,
        Pending = 3,
        Error = 4
    }
    [Serializable]
    public class SightseeingItinerary : CT.BookingEngine.Product
    {
        private int sightseeingId;
        private string cityCode;     //City Code
        private string cityName;
        private string itemCode;     //Item Code
        private string itemName;
        private string currency;             //Price Information
        private PriceAccounts priceInfo;
        private SightseeingBookingStatus status;        //Item Status
        private string confirmationNo;    //Item Reference in Booking
        private DateTime tourDate;   //Tour Date
        private string tourLanguage;
        private List<string> paxNames; //Pax Id list according to the PaxType List
        private List<int> childAge;
        private int adultCount;
        private int childCount;
        private string bookingRef; //Random-generated Unique Identifier
        private DateTime createdOn;
        private int createdBy;
        private DateTime lastModifiedOn;
        private int lastModifiedBy;
        private bool isDomestic;
        private bool voucherStatus;
        private DateTime lastCancellationDate;
        private string cancelPolicy;
        private List<SightseeingPenalty> penalityInfo;
        private string cancelId;
        private SightseeingBookingSource source;
        private string duration;
        private string depTime;
        private string depPointInfo;
        private string specialCode;
        private string supplierInfo;
        private string email;
        private string phNo;
        private string transType;
        private string address;
        private int agentId;
        private int locationId;
        private string note;
        private string specialName;
        private string telePhno;
        private string countryName;
        //Added on 09/2019 by somasekhar --- for Activity
        //for sending in the Search Request
        private string nationality;     //Nationality Code
        private string pickuppoint;
        FlightDetail flightInfo;
        #region Properities
        public int SightseeingId
        {
            get { return sightseeingId; }
            set { sightseeingId = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public List<string> PaxNames
        {
            get { return paxNames; }
            set { paxNames = value; }
        }
        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }
        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }
        public string CancelId
        {
            get { return cancelId; }
            set { cancelId = value; }
        }
        public string ConfirmationNo
        {
            get { return confirmationNo; }
            set { confirmationNo = value; }
        }
        public PriceAccounts Price
        {
            get { return priceInfo; }
            set { priceInfo = value; }
        }
        public SightseeingBookingStatus BookingStatus
        {
            get { return status; }
            set { status = value; }
        }
        public DateTime TourDate
        {
            get { return tourDate; }
            set { tourDate = value; }
        }
        public string Language
        {
            get { return tourLanguage; }
            set { tourLanguage = value; }
        }
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public List<SightseeingPenalty> PenalityInfo
        {
            get { return penalityInfo; }
            set { penalityInfo = value; }
        }
        public string CancellationPolicy
        {
            get { return cancelPolicy; }
            set { cancelPolicy = value; }
        }
        public DateTime LastCancellationDate
        {
            get { return lastCancellationDate; }
            set { lastCancellationDate = value; }
        }
        public string BookingReference
        {
            get { return bookingRef; }
            set { bookingRef = value; }
        }
        public bool IsDomestic
        {
            get { return isDomestic; }
            set { isDomestic = value; }
        }
        public bool VoucherStatus
        {
            get { return voucherStatus; }
            set { voucherStatus = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }
        public List<int> ChildAge
        {
            get { return childAge; }
            set { childAge = value; }
        }
        public SightseeingBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public string Duration
        {
            get { return duration; }
            set { duration = value; }
        }
        public string DepTime
        {
            get { return depTime; }
            set { depTime = value; }
        }
        public string DepPointInfo
        {
            get { return depPointInfo; }
            set { depPointInfo = value; }
        }
        public string SpecialCode
        {
            get { return specialCode; }
            set { specialCode = value; }
        }
        public string SupplierInfo
        {
            get { return supplierInfo; }
            set { supplierInfo = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string PhNo
        {
            get { return phNo; }
            set { phNo = value; }
        }
        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }
        public int LocationId
        {
            get { return locationId; }
            set { locationId = value; }
        }
        public string Note
        {
            get { return note; }
            set { note = value; }
        }
        public string SpecialName
        {
            get { return specialName; }
            set { specialName = value; }
        }
        public string TelePhno
        {
            get { return telePhno; }
            set { telePhno = value; }
        }
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        public FlightDetail FlightInfo
        {
            get { return flightInfo; }
            set { flightInfo = value; }
        }

        public string PickupPoint
        {
            get { return pickuppoint; }
            set{ pickuppoint = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// This Method is used to Save the Sightseeing Details.
        /// </summary>
        public override void Save(Product prod)
        {
            //Trace.TraceInformation("SightseeingItinerary.Save entered.");
            SightseeingItinerary itinearary = (SightseeingItinerary)prod;
            SqlParameter[] paramList = new SqlParameter[39];
            using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
            {
                itinearary.Price.Save();
                string paxName = string.Empty;
                foreach (string pax in itinearary.PaxNames)
                {
                    paxName += pax + "|";
                }
                string childData = string.Empty;
                foreach (int age in itinearary.ChildAge)
                {
                    childData += age.ToString() + "|";
                }
                paramList[0] = new SqlParameter("@sightseeingId", itinearary.SightseeingId);
                paramList[1] = new SqlParameter("@cityCode", itinearary.CityCode);
                paramList[2] = new SqlParameter("@cityName", itinearary.CityName);
                paramList[3] = new SqlParameter("@itemCode", itinearary.ItemCode);
                paramList[4] = new SqlParameter("@itemName", itinearary.ItemName);
                paramList[5] = new SqlParameter("@currency", itinearary.Currency);
                paramList[6] = new SqlParameter("@status", itinearary.BookingStatus);
                paramList[7] = new SqlParameter("@priceId", itinearary.Price.PriceId);
                paramList[8] = new SqlParameter("@confirmationNo", itinearary.ConfirmationNo);
                paramList[9] = new SqlParameter("@tourDate", itinearary.TourDate);
                paramList[10] = new SqlParameter("@tourLanguage", itinearary.Language.Split('#')[0]);
                paramList[11] = new SqlParameter("@paxNames", paxName);
                paramList[12] = new SqlParameter("@childAge", childData);
                paramList[13] = new SqlParameter("@adultCount", itinearary.AdultCount);
                paramList[14] = new SqlParameter("@childCount", itinearary.ChildCount);
                paramList[15] = new SqlParameter("@bookingRef", itinearary.BookingReference);
                paramList[16] = new SqlParameter("@createdBy", itinearary.CreatedBy);
                paramList[17] = new SqlParameter("@isDomestic", itinearary.IsDomestic);
                paramList[18] = new SqlParameter("@voucherStatus", itinearary.VoucherStatus);
                paramList[19] = new SqlParameter("@lastCancellationDate", itinearary.LastCancellationDate);
                paramList[20] = new SqlParameter("@cancelPolicy", itinearary.CancellationPolicy);
                paramList[21] = new SqlParameter("@cancelId", itinearary.CancelId);
                paramList[22] = new SqlParameter("@source", itinearary.Source);
                paramList[23] = new SqlParameter("@duration", itinearary.Duration);
                paramList[24] = new SqlParameter("@depTime", itinearary.DepTime);
                paramList[25] = new SqlParameter("@depPointInfo", itinearary.DepPointInfo);
                paramList[26] = new SqlParameter("@specialCode", itinearary.SpecialCode);
                paramList[27] = new SqlParameter("@supplierInfo", itinearary.SupplierInfo);
                paramList[28] = new SqlParameter("@Email", itinearary.email);
                paramList[29] = new SqlParameter("@PhNo", itinearary.phNo);
                paramList[30] = new SqlParameter("@TransType", itinearary.transType);
                paramList[31] = new SqlParameter("@Address", itinearary.address);
                paramList[32] = new SqlParameter("@AgentId", itinearary.agentId);
                paramList[33] = new SqlParameter("@LocationId", itinearary.locationId);
                paramList[34] = new SqlParameter("@Note", itinearary.note);
                paramList[35] = new SqlParameter("SpecialName", itinearary.specialName);
                paramList[36] = new SqlParameter("@TelePhno", itinearary.telePhno);
                paramList[37] = new SqlParameter("@CountryName", itinearary.countryName);
                paramList[38] = new SqlParameter("@pickupPoint", pickuppoint);
                paramList[0].Direction = ParameterDirection.Output;

                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddSightseeingItinerary, paramList);
                sightseeingId = (int)paramList[0].Value;
                if (itinearary.PenalityInfo != null)
                {
                    foreach (SightseeingPenalty penality in itinearary.PenalityInfo)
                    {
                        penality.SightseeingId = sightseeingId;
                        penality.Save();
                    }
                }
                updateTransaction.Complete();
            }
            //Trace.TraceInformation("SightseeingItinerary.Save Exit.");
        }
        /// <summary>
        /// This Method is used to Load the Sightseeing Details based on Transfer ID.
        /// </summary>
        /// <param name="transferId"></param>
        public void Load(int ssId)
        {
            //Trace.TraceInformation("SightseeingItinerary.Load entered : Id = " + ssId);
            if (ssId <= 0)
            {
                throw new ArgumentException("Sightseeing Id Id should be positive integer");
            }
            this.sightseeingId = ssId;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sightseeingId", sightseeingId);
            try
            {
                PriceAccounts priceData = new PriceAccounts();
                //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetSightseeingItinerary, paramList, connection);
                using (DataTable dataSightSeeing = DBGateway.FillDataTableSP(SPNames.GetSightseeingItinerary, paramList))
                {
                    if (dataSightSeeing !=null && dataSightSeeing.Rows.Count > 0)
                    {
                        foreach (DataRow data in dataSightSeeing.Rows)
                        {
                            confirmationNo =Convert.ToString(data["confirmationNo"]);
                            itemCode = Convert.ToString(data["itemCode"]);
                            cityCode = Convert.ToString(data["cityCode"]);
                            cityName = Convert.ToString(data["cityName"]);
                            itemName = Convert.ToString(data["itemName"]);
                            currency = Convert.ToString(data["currency"]);
                            status = (SightseeingBookingStatus)Convert.ToInt16(data["status"]);
                            priceData.Load(Convert.ToInt32(data["priceId"]));
                            priceInfo = priceData;
                            confirmationNo = Convert.ToString(data["confirmationNo"]);
                            tourDate = Convert.ToDateTime(data["tourDate"]);
                            tourLanguage = Convert.ToString(data["tourLanguage"]);
                            string[] PaxList = Convert.ToString(data["paxNames"]).Split('|');
                            List<string> paxData = new List<string>();
                            for (int i = 0; i < PaxList.Length - 1; i++)
                            {
                                paxData.Add(PaxList[i]);
                            }
                            paxNames = paxData;
                            string[] ageLst = Convert.ToString(data["childAge"]).Split('|');
                            List<int> ageData = new List<int>();
                            for (int j = 0; j < ageLst.Length - 1; j++)
                            {
                                ageData.Add(Convert.ToInt32(ageLst[j]));
                            }
                            childAge = ageData;
                            adultCount = Convert.ToInt32(data["adultCount"]);
                            childCount = Convert.ToInt32(data["childCount"]);
                            bookingRef = Convert.ToString(data["bookingRef"]);
                            createdBy = Convert.ToInt32(data["createdBy"]);
                            isDomestic = Convert.ToBoolean(data["isDomestic"]);
                            voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                            lastCancellationDate = Convert.ToDateTime(data["lastCancellationDate"]);
                            cancelPolicy = WebUtility.HtmlDecode( Convert.ToString(data["cancelPolicy"]));
                            createdOn = Convert.ToDateTime(data["createdOn"]);
                            cancelId = Convert.ToString(data["cancelId"]);
                            source = (SightseeingBookingSource)Convert.ToInt32(data["source"]);
                            duration = Convert.ToString(data["duration"]);
                            depTime = Convert.ToString(data["depTime"]);
                            depPointInfo = Convert.ToString(data["pickupLocation"]);
                            specialCode = Convert.ToString(data["specialCode"]);
                            supplierInfo = Convert.ToString(data["supplierInfo"]);
                            phNo = Convert.ToString(data["PhNo"]);
                            email = Convert.ToString(data["Email"]);
                            transType = Convert.ToString(data["TransType"]);
                            agentId = Convert.ToInt32(data["AgentId"]);
                            locationId = Convert.ToInt32(data["LocationId"]);
                            address = Convert.ToString(data["Address"]);
                            note = WebUtility.HtmlDecode( Convert.ToString(data["Note"]));
                            telePhno = Convert.ToString(data["SupplierTelephNo"]);
                            countryName = Convert.ToString(data["CountryName"]);
                            specialName = Convert.ToString(data["SpecialName"]);
                            bookingRef= Convert.ToString(data["bookingRef"]);
                            pickuppoint = Convert.ToString(data["pickuppoint"]);
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Sightseeing id does not exist in database");
                        //data.Close();
                        //connection.Close();
                        //Trace.TraceInformation("SightseeingItinerary.Load exiting : sightseeingId does not exist.transferId = " + ssId.ToString());
                    }
                }
                //data.Close();
                //connection.Close();
                //SightseeingPenalty penality = new SightseeingPenalty();
                //penalityInfo = penality.GetSightseeingPenality(ssId);
            }
            catch (Exception exp)
            {
                //connection.Close();
                throw new Exception("Sightseeing id does not exist in database", exp);
            }
        }
        /// <summary>
        /// This Method is used to get SightseeingId using Confirmation Number
        /// </summary>
        /// <param name="confNo"></param>
        /// <returns></returns>
        public static int GetSightseeingId(string confNo)
        {
            //Trace.TraceInformation("SightseeingItinerary.GetSightseeingId entered : confNo = " + confNo);
            int sightseeingId = 0;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@confNo", confNo);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetSightseeingId, paramList, connection);
            using (DataTable dtSight = DBGateway.FillDataTableSP(SPNames.GetSightseeingId, paramList))
            {
                if (dtSight != null && dtSight.Rows.Count > 0)
                {
                    DataRow data = dtSight.Rows[0];
                    if (data != null && data["sightseeingId"] != DBNull.Value)
                    {
                        sightseeingId = Convert.ToInt32(data["sightseeingId"]);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("SightseeingItinerary.GetSightseeingId exiting :" + sightseeingId.ToString());
            return sightseeingId;
        }
        /// <summary>
        /// This Method is sued to Update the Voucher status
        /// </summary>
        public void UpdateVoucherStatus()
        {
            //Trace.TraceInformation("TransferItinerary.UpdateVoucherStatus entered  ");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@voucherStatus", voucherStatus);
            paramList[1] = new SqlParameter("@sightseeingId", sightseeingId);

            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateSightseeingVoucherStatus, paramList);
            //Trace.TraceInformation("SightseeingItinerary.UpdateSightseeingStatus exiting count" + retVal);
        }
        /// <summary>
        /// This MEthod is used to Update the Itinerary BookingStatus
        /// </summary>
        public void UpdateBookingStatus()
        {
            //Trace.TraceInformation("SightseeingItinerary.UpdateBookingStatus entered  ");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@status", status);
            paramList[1] = new SqlParameter("@cancelId", cancelId);
            paramList[2] = new SqlParameter("@sightseeingId", sightseeingId);

            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateSightseeingItineary, paramList);
            //Trace.TraceInformation("SightseeingItinerary.UpdateBookingStatus  exiting count" + retVal);
        }
        public static List<int> LoadSourceId()
        {
            //Trace.TraceInformation("SightseeingSource.Load entered : ");
            List<int> idList = new List<int>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAllSightseeingSourceId, paramList, connection);
            while (data.Read())
            {
                idList.Add(Convert.ToInt32(data["sightseeingSourceId"]));
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("SightseeingSource.Load exiting :");
            return idList;
        }

        public void UpdateStockintable(int itemcode, int noOfguests)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@itemcode", itemcode));
            paramList.Add(new SqlParameter("@noOfguests", noOfguests));
            int retVal = DBGateway.ExecuteNonQuery("usp_UpdateActivityStockintable", paramList.ToArray());
        }

        public static DataTable GetSupplierEmail(int SightseeingId)
        {
            DataTable dtSupplerDetails = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_SightseeingId", SightseeingId);
                dtSupplerDetails = DBGateway.FillDataTableSP("USP_Get_Activity_SupplerMailID", paramList);
                   
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                
            }
            return dtSupplerDetails;
        }

        public static int UpdateReferenceNumber(string confirmationNumber,string reference)
        {
            int retVal = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@confirmationNumber", confirmationNumber));
                paramList.Add(new SqlParameter("@reference", reference));
                 retVal=DBGateway.ExecuteNonQuery("usp_UpdateSightseeingReferenceNumber", paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return retVal;
        }

        #endregion

    }
    //Activity Booking Need to send Mail
    public class FlightDetail
    {
        #region variables
        string pnr;
        string fligtNo;
        string destination;
        DateTime arrivaltime;
        string hotelName;
        string origin;
        DateTime departureTime;
        string deptFlightNo;
        #endregion
        #region Properities
        public string PNR
        {
            get { return pnr; }
            set { pnr = value; }
        }
        public string FligtNo
        {
            get { return fligtNo; }
            set { fligtNo = value; }
        }
        public string Destination
        {
            get { return destination; }
            set { destination = value; }
        }
        public DateTime Arrivaltime
        {
            get { return arrivaltime; }
            set { arrivaltime = value; }
        }
        public string HotelName
        {
            get { return hotelName; }
            set { hotelName = value; }
        }
        public string Origin
        {
            get { return origin; }
            set { origin = value; }
        }
        public DateTime DepartureTime
        {
            get { return departureTime; }
            set { departureTime = value; }
        }
        public string DeptFlightNo
        {
            get { return deptFlightNo; }
            set { deptFlightNo = value; }
        }
        #endregion
    }
}
