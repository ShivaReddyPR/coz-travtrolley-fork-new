﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using CT.TicketReceipt.Common;

namespace CT.BookingEngine
{
    [Serializable]
    public class PriceTaxDetails
    {
        int id;
        int priceId;
        InputVATDetail inputVAT;
        OutputVATDetail outputVAT;

        #region Properties

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public int PriceId
        {
            get
            {
                return priceId;
            }

            set
            {
                priceId = value;
            }
        }

        public InputVATDetail InputVAT
        {
            get
            {
                return inputVAT;
            }

            set
            {
                inputVAT = value;
            }
        }

        public OutputVATDetail OutputVAT
        {
            get
            {
                return outputVAT;
            }

            set
            {
                outputVAT = value;
            }
        }

        #endregion


        public static PriceTaxDetails GetVATCharges(string countryCode, string loginCountryCode, string supplierCountryCode, int productId, string module, string destinationType)
        {
            PriceTaxDetails taxDetail = new BookingEngine.PriceTaxDetails();
            if (loginCountryCode != "IN") //If Login Country Code IN Is there No need to check Input Vat
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[6];
                    paramList[0] = new SqlParameter("@P_CountryCode", countryCode);
                    paramList[1] = new SqlParameter("@P_Login_CountryCode", loginCountryCode);
                    paramList[2] = new SqlParameter("@P_Supplier_CountryCode", supplierCountryCode);
                    paramList[3] = new SqlParameter("@P_ProductId", productId);
                    paramList[4] = new SqlParameter("@P_Module", module);
                    paramList[5] = new SqlParameter("@P_DestinationType", destinationType);
                    DataSet dsTaxDetails = DBGateway.FillSP("usp_GetVATChargesByProduct", paramList);

                    if (dsTaxDetails != null)
                    {
                        for (int i = 0; i < dsTaxDetails.Tables.Count; i++)
                        {
                            DataTable dt = dsTaxDetails.Tables[i];
                            switch (i)
                            {
                                case 0://Input VAT

                                    if (dt.Rows.Count > 0) 
                                    {
                                        InputVATDetail invat = new InputVATDetail();
                                        DataRow dr = dt.Rows[0];
                                        if (dr["invat_id"] != DBNull.Value)
                                        {
                                            invat.Id = Convert.ToInt32(dr["invat_id"]);
                                        }
                                        if (dr["invat_applied"] != DBNull.Value)
                                        {
                                            invat.Applied = Convert.ToBoolean(dr["invat_applied"]);
                                        }
                                        if (dr["invat_value"] != DBNull.Value)
                                        {
                                            invat.Charge = Convert.ToDecimal(dr["invat_value"]);
                                        }
                                        if (dr["invat_cost_included"] != DBNull.Value)
                                        {
                                            invat.CostIncluded = Convert.ToBoolean(dr["invat_cost_included"]);
                                        }

                                        invat.DestinationType = DestinationType.International;
                                        invat.Module = Module.Ticket;
                                        taxDetail.inputVAT = invat;
                                    }

                                    break;
                                case 1://Output VAT

                                    if (dt.Rows.Count > 0)
                                    {
                                        OutputVATDetail outvat = new OutputVATDetail();
                                        DataRow dr = dt.Rows[0];
                                        if (dr["outvat_id"] != DBNull.Value)
                                        {
                                            outvat.Id = Convert.ToInt32(dr["outvat_id"]);
                                        }
                                        if (dr["outvat_applied"] != DBNull.Value)
                                        {
                                            outvat.Applied = Convert.ToBoolean(dr["outvat_applied"]);
                                        }
                                        if (dr["outvat_value"] != DBNull.Value)
                                        {
                                            outvat.Charge = Convert.ToDecimal(dr["outvat_value"]);
                                        }
                                        if (dr["outvat_on"] != DBNull.Value)
                                        {
                                            switch (dr["outvat_on"].ToString())
                                            {
                                                case "TC":
                                                    outvat.AppliedOn = VATAppliedOn.TC;
                                                    break;
                                                case "SF":
                                                    outvat.AppliedOn = VATAppliedOn.SF;
                                                    break;
                                            }
                                        }
                                        outvat.DestinationType = DestinationType.International;
                                        outvat.Module = Module.Ticket;

                                        taxDetail.outputVAT = outvat;
                                    }

                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Price, Severity.High, 0, "Failed to Retrieve VAT Charges in PriceTaxDetail. Reason : " + ex.ToString(), "");
                }
            }
            return taxDetail;
        }
       
        public void Save()
        {
            try
            {
                if (outputVAT != null || inputVAT != null)
                {
                    SqlParameter[] paramList = new SqlParameter[9];
                    paramList[0] = new SqlParameter("@P_PRICE_ID", priceId);
                    if (outputVAT != null)
                    {
                        paramList[1] = new SqlParameter("@P_OUTVAT_ID", outputVAT.Id);
                        paramList[2] = new SqlParameter("@P_OUTVAT_APPLIED", outputVAT.Applied);
                        paramList[3] = new SqlParameter("@P_OUTVAT_VALUE", outputVAT.Charge);
                        paramList[4] = new SqlParameter("@P_OUTVAT_ON", Convert.ToString(outputVAT.AppliedOn));
                    }
                    else
                    {
                        paramList[1] = new SqlParameter("@P_OUTVAT_ID", DBNull.Value);
                        paramList[2] = new SqlParameter("@P_OUTVAT_APPLIED", DBNull.Value);
                        paramList[3] = new SqlParameter("@P_OUTVAT_VALUE", DBNull.Value);
                        paramList[4] = new SqlParameter("@P_OUTVAT_ON", DBNull.Value);
                    }
                    if (inputVAT != null)
                    {
                        paramList[5] = new SqlParameter("@P_INVAT_ID", inputVAT.Id);
                        paramList[6] = new SqlParameter("@P_INVAT_APPLIED", inputVAT.Applied);
                        paramList[7] = new SqlParameter("@P_INVAT_VALUE", inputVAT.Charge);
                        paramList[8] = new SqlParameter("@P_INVAT_COST_INCLUDED", inputVAT.CostIncluded);

                    }
                    else
                    {
                        paramList[5] = new SqlParameter("@P_INVAT_ID", DBNull.Value);
                        paramList[6] = new SqlParameter("@P_INVAT_APPLIED", DBNull.Value);
                        paramList[7] = new SqlParameter("@P_INVAT_VALUE", DBNull.Value);
                        paramList[8] = new SqlParameter("@P_INVAT_COST_INCLUDED", DBNull.Value);
                    }
                    DBGateway.ExecuteNonQuerySP("usp_AddPriceTaxDetail", paramList);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Price, Severity.High, 0, "Failed to Save PriceTax Detail. Reason : " + ex.ToString(), "");
            }
        }
    }

    public enum DestinationType
    {
        Default=0,
        Domestic = 1,
        International = 2
    }

    public enum Module
    {
        Ticket,
        Hotel,
        Visa,
        Package,
        Meal,
        Seat,
        HALA,
        SightSeeing,
        Insurance,
        BaggageInsurance,
        Transfer
    }

    public enum VATAppliedOn
    {
        TC,
        SF
    }
    [Serializable]
    public class InputVATDetail
    {
        #region Members
        int id;
        string countryCode;
        string supplierCountryCode;
        DestinationType destinationType;
        int productId;
        Module module;
        bool applied;
        decimal charge;
        bool costIncluded;
        bool calculate;
        string remarks;
        string status;
        int createdBy;
        int modifiedBy;
        #endregion

        #region Properties

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string CountryCode
        {
            get
            {
                return countryCode;
            }

            set
            {
                countryCode = value;
            }
        }

        public string SupplierCountryCode
        {
            get
            {
                return supplierCountryCode;
            }

            set
            {
                supplierCountryCode = value;
            }
        }

        public DestinationType DestinationType
        {
            get
            {
                return destinationType;
            }

            set
            {
                destinationType = value;
            }
        }

        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public Module Module
        {
            get
            {
                return module;
            }

            set
            {
                module = value;
            }
        }

        public bool Applied
        {
            get
            {
                return applied;
            }

            set
            {
                applied = value;
            }
        }

        public decimal Charge
        {
            get
            {
                return charge;
            }

            set
            {
                charge = value;
            }
        }

        public bool CostIncluded
        {
            get
            {
                return costIncluded;
            }

            set
            {
                costIncluded = value;
            }
        }

        public bool Calculate
        {
            get
            {
                return calculate;
            }

            set
            {
                calculate = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }

            set
            {
                remarks = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }

            set
            {
                createdBy = value;
            }
        }

        public int ModifiedBy
        {
            get
            {
                return modifiedBy;
            }

            set
            {
                modifiedBy = value;
            }
        }

        #endregion

        #region Constructors
        public InputVATDetail()
        {
            id = -1;
        }
        public InputVATDetail(int ID)
        {
            id = ID;
            GetDetails(id);
        }
        #endregion
        public decimal CalculateVatAmount(decimal totalAmount, ref decimal vatAmount, int decimalPoint)
        {
            if (applied)
            {
                if (costIncluded)
                {
                    vatAmount = Math.Round((totalAmount - (totalAmount * 100) / (100 + charge)), decimalPoint);
                }
                else
                {
                    vatAmount = Math.Round(((totalAmount) * charge / 100), decimalPoint);
                    totalAmount = totalAmount + vatAmount;
                }
            }
            return totalAmount;
        }
        public void Save()
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[15];
                paramlist[0] = new SqlParameter("@P_INVAT_ID", id);
                paramlist[1] = new SqlParameter("@P_INVAT_COUNTRY_CODE", countryCode);
                paramlist[2] = new SqlParameter("@P_INVAT_SUPPLIER_COUNTRY_CODE", supplierCountryCode);
                paramlist[3] = new SqlParameter("@P_INVAT_DESTINATION_TYPE", destinationType.ToString());
                paramlist[4] = new SqlParameter("@P_INVAT_PRODUCT", productId);
                paramlist[5] = new SqlParameter("@P_INVAT_MODULE", module.ToString());
                paramlist[6] = new SqlParameter("@P_INVAT_APPLIED", applied);
                paramlist[7] = new SqlParameter("@P_INVAT_VALUE", charge);
                paramlist[8] = new SqlParameter("@P_INVAT_COST_INCLUDED", costIncluded);
                paramlist[9] = new SqlParameter("@P_INVAT_CALCULATE", calculate);
                paramlist[10] = new SqlParameter("@P_INVAT_REMARKS", remarks);
                paramlist[11] = new SqlParameter("@P_INVAT_STATUS", status);
                paramlist[12] = new SqlParameter("@P_INVAT_CREATED_BY", createdBy);
                paramlist[13] = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar, 10);
                paramlist[13].Direction = ParameterDirection.Output;
                paramlist[14] = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar, 100);
                paramlist[14].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("USP_P_INPUT_VAT_CONFIG_ADD_UPDATE", paramlist);
                string messagetype = Utility.ToString(paramlist[13].Value);
                if (messagetype == "E")
                {
                    string message = Utility.ToString(paramlist[14].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Private Methods
        private void GetDetails(int id)
        {
            try
            {
                DataTable dt = GetData(id);
                UpdataBusinessData(dt);
            }
            catch
            {
                throw;
            }
        }
        private DataTable GetData(int id)
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@P_INVAT_ID", id);
                return DBGateway.FillDataTableSP("USP_P_INPUT_VAT_CONFIG_GET_DATA", paramlist);
            }
            catch
            {
                throw;
            }
        }
        private void UpdataBusinessData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["invat_id"] != null)
                    {
                        id = Utility.ToInteger(dr["invat_id"]);
                    }
                    if (dr["invat_country_code"] != null)
                    {
                        countryCode = Utility.ToString(dr["invat_country_code"]);
                    }
                    if (dr["invat_supplier_country_code"] != null)
                    {
                        supplierCountryCode = Utility.ToString(dr["invat_supplier_country_code"]);
                    }
                    if (dr["invat_destination_type"] != null)
                    {
                        //destinationType = (DestinationType)Utility.ToInteger(dr["invat_destination_type"]);
                        switch (Utility.ToString(dr["invat_destination_type"]))
                        {
                            case "International":
                                destinationType = DestinationType.International;
                                break;
                            case "Domestic":
                                destinationType = DestinationType.Domestic;
                                break;
                        }
                    }
                    if (dr["invat_product"] != null)
                    {
                        productId = Utility.ToInteger(dr["invat_product"]);
                    }
                    if (dr["invat_module"] != null)
                    {
                        switch (Utility.ToString(dr["invat_module"]))
                        {
                            case "Ticket":
                                module = Module.Ticket;
                                break;
                            case "Hotel":
                                module = Module.Hotel;
                                break;
                            case "Visa":
                                module = Module.Visa;
                                break;
                            case "Package":
                                module = Module.Package;
                                break;
                            case "Meal":
                                module = Module.Meal;
                                break;
                            case "Seat":
                                module = Module.Seat;
                                break;
                            case "HALA":
                                module = Module.HALA;
                                break;

                        }
                    }
                    if (dr["invat_applied"] != null)
                    {
                        applied = Utility.ToBoolean(dr["invat_applied"]);
                    }
                    if (dr["invat_value"] != null)
                    {
                        charge = Utility.ToDecimal(dr["invat_value"]);
                    }
                    if (dr["invat_cost_included"] != null)
                    {
                        costIncluded = Utility.ToBoolean(dr["invat_cost_included"]);
                    }
                    if (dr["invat_calculate"] != null)
                    {
                        calculate = Utility.ToBoolean(dr["invat_calculate"]);
                    }
                    if (dr["invat_remarks"] != null)
                    {
                        remarks = Utility.ToString(dr["invat_remarks"]);
                    }
                }
            }
        }
        #endregion
        #region Static Methods
        public static DataTable GetList()
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[0];
                
                return DBGateway.FillDataTableSP("USP_P_INPUT_VAT_CONFIG_GET_LIST", paramlist);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
    [Serializable]
    public class OutputVATDetail
    {
        #region Members
        int id;
        string countryCode;
        string supplierCountryCode;
        DestinationType destinationType;
        int productId;
        Module module;
        bool applied;
        decimal charge;
        VATAppliedOn appliedOn;
        bool ccChargeApplied;
        bool calculate;
        string remarks;
        string status;
        int createdBy;
        int modifiedBy;
        #endregion

        #region Properties

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string CountryCode
        {
            get
            {
                return countryCode;
            }

            set
            {
                countryCode = value;
            }
        }

        public string SupplierCountryCode
        {
            get
            {
                return supplierCountryCode;
            }

            set
            {
                supplierCountryCode = value;
            }
        }

        public DestinationType DestinationType
        {
            get
            {
                return destinationType;
            }

            set
            {
                destinationType = value;
            }
        }

        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public Module Module
        {
            get
            {
                return module;
            }

            set
            {
                module = value;
            }
        }

        public bool Applied
        {
            get
            {
                return applied;
            }

            set
            {
                applied = value;
            }
        }

        public decimal Charge
        {
            get
            {
                return charge;
            }

            set
            {
                charge = value;
            }
        }

        public VATAppliedOn AppliedOn
        {
            get
            {
                return appliedOn;
            }

            set
            {
                appliedOn = value;
            }
        }

        public bool Calculate
        {
            get
            {
                return calculate;
            }

            set
            {
                calculate = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }

            set
            {
                remarks = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }

            set
            {
                createdBy = value;
            }
        }

        public int ModifiedBy
        {
            get
            {
                return modifiedBy;
            }

            set
            {
                modifiedBy = value;
            }
        }
        public bool CCChargeApplied
        {
            get
            {
                return ccChargeApplied;
            }
            set
            {
                ccChargeApplied = value;
            }
        }

        #endregion
        #region Constructors
        public OutputVATDetail()
        {
            id = -1;
        }
        public OutputVATDetail(int ID)
        {
            id = ID;
            GetDetails(id);
        }
        #endregion

        public decimal CalculateVatAmount(decimal totalAmount, decimal totalMarkup, int decimalPoint)
        {
            decimal vatAmount = 0m;
            if (Applied)
            {
                if (AppliedOn == VATAppliedOn.SF)
                {
                    vatAmount = Math.Round((totalMarkup * Charge / 100), decimalPoint);
                }
                else if (AppliedOn == VATAppliedOn.TC)
                {
                    vatAmount = Math.Round((totalAmount) * Charge / 100, decimalPoint);
                }
            }
            return vatAmount;
        }
        public void Save()
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[17];
                paramlist[0] = new SqlParameter("@P_OUTVAT_ID", id);
                paramlist[1] = new SqlParameter("@P_OUTVAT_COUNTRY_CODE", countryCode);
                paramlist[2] = new SqlParameter("@P_OUTVAT_DESTINATION_TYPE", destinationType.ToString());
                paramlist[3] = new SqlParameter("@P_OUTVAT_PRODUCT", productId);
                paramlist[4] = new SqlParameter("@P_OUTVAT_MODULE", module.ToString());
                paramlist[5] = new SqlParameter("@P_OUTVAT_APPLIED", applied);
                paramlist[6] = new SqlParameter("@P_OUTVAT_VALUE", charge);
                paramlist[7] = new SqlParameter("@P_OUTVAT_ON", appliedOn.ToString());
                paramlist[8] = new SqlParameter("@P_OUTVAT_CC_CHARGE_APPLIED", ccChargeApplied);
                paramlist[9] = new SqlParameter("@P_OUTVAT_CALCULATE", calculate);
                paramlist[10] = new SqlParameter("@P_OUTVAT_REMARKS", remarks);
                paramlist[11] = new SqlParameter("@P_OUTVAT_STATUS", status);
                paramlist[12] = new SqlParameter("@P_OUTVAT_CREATED_BY", createdBy);
                paramlist[13] = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar, 10);
                paramlist[13].Direction = ParameterDirection.Output;
                paramlist[14] = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar, 100);
                paramlist[14].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("USP_P_OUTPUT_VAT_CONFIG_ADD_UPDATE", paramlist);
                string messagetype = Utility.ToString(paramlist[13].Value);
                if (messagetype == "E")
                {
                    string message = Utility.ToString(paramlist[14].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Private Methods
        private void GetDetails(int id)
        {
            DataTable dt = GetData(id);
            UpdataBusinessData(dt);
        }
        private DataTable GetData(int id)
        {
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@P_OUTVAT_ID", id);
            return DBGateway.FillDataTableSP("USP_P_OUTPUT_VAT_CONFIG_GET_DATA", paramlist);
        }
        private void UpdataBusinessData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["outvat_id"] != null)
                    {
                        id = Utility.ToInteger(dr["outvat_id"]);
                    }
                    if (dr["outvat_country_code"] != null)
                    {
                        countryCode = Utility.ToString(dr["outvat_country_code"]);
                    }

                    if (dr["outvat_destination_type"] != null)
                    {
                        switch (Utility.ToString(dr["outvat_destination_type"]))
                        {
                            case "International":
                                destinationType = DestinationType.International;
                                break;
                            case "Domestic":
                                destinationType = DestinationType.Domestic;
                                break;
                        }
                    }
                    if (dr["outvat_product"] != null)
                    {
                        productId = Utility.ToInteger(dr["outvat_product"]);
                    }
                    if (dr["outvat_module"] != null)
                    {
                        switch (Utility.ToString(dr["outvat_module"]))
                        {
                            case "Ticket":
                                module = Module.Ticket;
                                break;
                            case "Hotel":
                                module = Module.Hotel;
                                break;
                            case "Visa":
                                module = Module.Visa;
                                break;
                            case "Package":
                                module = Module.Package;
                                break;
                            case "Meal":
                                module = Module.Meal;
                                break;
                            case "Seat":
                                module = Module.Seat;
                                break;
                            case "HALA":
                                module = Module.HALA;
                                break;
                        }
                    }
                    if (dr["outvat_applied"] != null)
                    {
                        applied = Utility.ToBoolean(dr["outvat_applied"]);
                    }
                    if (dr["outvat_value"] != null)
                    {
                        charge = Utility.ToDecimal(dr["outvat_value"]);
                    }
                    if (dr["outvat_on"] != null)
                    {
                        switch (Utility.ToString(dr["outvat_on"]))
                        {
                            case "TC":
                                appliedOn = VATAppliedOn.TC;
                                break;
                            case "SF":
                                appliedOn = VATAppliedOn.SF;
                                break;
                        }
                    }
                    if (dr["outvat_cc_charge_applied"] != null)
                    {
                        ccChargeApplied = Utility.ToBoolean(dr["outvat_cc_charge_applied"]);
                    }
                    if (dr["outvat_calculate"] != null)
                    {
                        calculate = Utility.ToBoolean(dr["outvat_calculate"]);
                    }
                    if (dr["outvat_remarks"] != null)
                    {
                        remarks = Utility.ToString(dr["outvat_remarks"]);
                    }
                }
            }
        }
        #endregion
        #region Static Methods
        public static DataTable GetList()
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[0];
                
                return DBGateway.FillDataTableSP("USP_P_OUTPUT_VAT_CONFIG_GET_LIST", paramlist);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
