﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using System.IO;
using CT.Configuration;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.IO.Compression;

namespace CT.BookingEngine
{
    public class CatalogueData
    {
        string catId;
        string catCode;
        string catCategory;
        string catValue;
        string catType;
        string catTypeName;
        int catSource;
        string catStatus;
        DateTime lastUpdated;

        private string agentCode;
        private string password;
        /// <summary>
        /// Based on this parameter value (in days)  data will be updated.
        /// </summary>
        private int staticDataUpdateInterval;
        DataTable dtStaticData;
        HotelBookingSource hotelSource;
        public CatalogueData()
        {
          
        }
        public CatalogueData(HotelBookingSource source)
        {
            hotelSource = source;
            if (source == HotelBookingSource.LOH)
            {
                agentCode = ConfigurationSystem.LOHConfig["AgentCode"];
                password = ConfigurationSystem.LOHConfig["Password"];
                staticDataUpdateInterval = Convert.ToInt32(ConfigurationSystem.LOHConfig["UpdateInterval"]);
            }
            else if (source == HotelBookingSource.EET)
            {
                agentCode = ConfigurationSystem.EETConfig["AgentCode"];
                password = ConfigurationSystem.EETConfig["Password"];
                staticDataUpdateInterval = Convert.ToInt32(ConfigurationSystem.EETConfig["UpdateInterval"]);
            }
            try
            {
                dtStaticData = GetDataFromDB("MP", source);
            }
            catch { }
        }

        public string CatId
        {
            get { return catId; }
            set { catId = value; }
        }

        public string CatCode
        {
            get { return catCode; }
            set { catCode = value; }
        }

        public string CatCategory
        {
            get { return catCategory; }
            set { catCategory = value; }
        }

        public string CatValue
        {
            get { return catValue; }
            set { catValue = value; }
        }

        public string CatType
        {
            get { return catType; }
            set { catType = value; }
        }

        public string CatTypeName
        {
            get { return catTypeName; }
            set { catTypeName = value; }
        }

        public int CatSource
        {
            get { return catSource; }
            set { catSource = value; }
        }

        public string CatStatus
        {
            get { return catStatus; }
            set { catStatus = value; }
        }

        public DateTime LastUpdated
        {
            get { return lastUpdated; }
            set { lastUpdated = value; }
        }

        #region Common Methods
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        #endregion




        /// <summary>
        /// Returns the  Data values based on the type and code requested.        
        /// </summary>
        /// <param name="code"> Data Code. Ex: "3"</param>
        /// <param name="type">Type of data to retrieve. RC - Room category, MP - Meal Plan, AC - AccommodationCategory, OS - OfferSupplementType, AS - AccommodationService. Ex: "MP"</param>
        /// <returns></returns>
        /// 
        public string GetStaticDataOLD(string code, string type, HotelBookingSource source)
        {
            Dictionary<string, string> staticData = new Dictionary<string, string>();
            DateTime before = DateTime.Now;
            try
            {
                //DataTable dtStaticData = GetDataFromDB(code, type, source);
               /* DataTable dtStaticData = new DataTable();

                if (dtStaticData != null && dtStaticData.Rows.Count > 0)
                {
                    DateTime lastUpdateDate = Convert.ToDateTime(dtStaticData.Rows[0]["Cat_LastUpdated"].ToString());

                    if (DateTime.Now.Subtract(lastUpdateDate).Days >= staticDataUpdateInterval)
                    {
                        try
                        {
                            List<CatalogueData> CatData = GetDataFromXML(source, type);

                            List<CatalogueData> filteredData = CatData.FindAll(delegate(CatalogueData cd) { return cd.catCode == code && cd.catType == type; });

                            foreach (CatalogueData fd in filteredData)
                            {
                                staticData.Add(fd.catCode, fd.catType);
                            }

                            foreach (CatalogueData cd in CatData)
                            {
                                cd.SaveCatalogueData();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        DataRow[] filteredData = dtStaticData.Select("Cat_Code='" + code + "' AND Cat_Type='" + type + "'");

                        if (filteredData != null && filteredData.Length > 0)
                        {
                            foreach (DataRow dr in filteredData)
                            {
                                staticData.Add(dr["Cat_Code"].ToString(), dr["Cat_Value"].ToString());
                            }
                        }
                    }
                }

                else
                {
                    try
                    {
                        //List<CatalogueData> CatData = GetDataFromXML(source, type);

                        //List<CatalogueData> filteredData = CatData.FindAll(delegate(CatalogueData cd) { return cd.catCode == code && catType == type; });

                        //foreach (CatalogueData fd in filteredData)
                        //{
                        //    staticData.Add(fd.catCode, fd.catType);
                        //}

                        //foreach (CatalogueData cd in CatData)
                        //{
                        //    cd.SaveCatalogueData();
                        //}
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }*/

                DateTime after = DateTime.Now;
                TimeSpan ts = after - before;

                //CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.Normal, 1, "Catelogue induvidual rows returned in " + ts.Milliseconds + " Milliseconds", "1");

            }
            catch (Exception ex)
            {
                throw new Exception(source.ToString() + "-StaticData-Failed to get Catalogue Data : " + ex.Message, ex);
            }
            return "hi";
            //return staticData[code];
        }
        public string GetStaticData(string code, string type, HotelBookingSource source)
        {
            Dictionary<string, string> staticData = new Dictionary<string, string>();

            try
            {
                //DataTable dtStaticData = GetDataFromDB(code, type, source);

                if (dtStaticData != null && dtStaticData.Rows.Count > 0)
                {
                    DateTime lastUpdateDate = Convert.ToDateTime(dtStaticData.Rows[0]["Cat_LastUpdated"].ToString());

                    if (DateTime.Now.Subtract(lastUpdateDate).Days >= staticDataUpdateInterval)
                    {
                        try
                        {
                            List<CatalogueData> CatData = GetDataFromXML(source, type);

                            List<CatalogueData> filteredData = CatData.FindAll(delegate(CatalogueData cd) { return cd.catCode == code && cd.catType == type; });

                            foreach (CatalogueData fd in filteredData)
                            {
                                staticData.Add(fd.catCode, fd.catType);
                            }

                            foreach (CatalogueData cd in CatData)
                            {
                                cd.SaveCatalogueData();
                            }
                            dtStaticData = GetDataFromDB(type, source);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        DataRow[] filteredData = dtStaticData.Select("Cat_Code='" + code + "' AND Cat_Type='" + type + "'");

                        if (filteredData != null && filteredData.Length > 0)
                        {
                            foreach (DataRow dr in filteredData)
                            {
                                staticData.Add(dr["Cat_Code"].ToString(), dr["Cat_Value"].ToString());
                            }
                        }
                    }
                }

                else
                {
                    try
                    {
                        List<CatalogueData> CatData = GetDataFromXML(source, type);

                        List<CatalogueData> filteredData = CatData.FindAll(delegate(CatalogueData cd) { return cd.catCode == code && catType == type; });

                        foreach (CatalogueData fd in filteredData)
                        {
                            staticData.Add(fd.catCode, fd.catType);
                        }

                        foreach (CatalogueData cd in CatData)
                        {
                            cd.SaveCatalogueData();
                        }
                        if (dtStaticData != null && dtStaticData.Rows.Count > 0)
                        {
                            dtStaticData = GetDataFromDB(type, source);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(source.ToString() + "-StaticData-Failed to get Catalogue Data : " + ex.Message, ex);
            }

            return staticData[code];
        }

        private void SaveCatalogueData()
        {
            try
            {
                //SqlConnection con = DBGateway.CreateConnection();
                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@P_Cat_Id", catId);
                paramList[1] = new SqlParameter("@P_Cat_Code", catCode);
                paramList[2] = new SqlParameter("@P_Cat_Category", catCategory);
                paramList[3] = new SqlParameter("@P_Cat_Value", catValue);
                paramList[4] = new SqlParameter("@P_Cat_Type", catType);
                paramList[5] = new SqlParameter("@P_Cat_Type_Name", catTypeName);
                paramList[6] = new SqlParameter("@P_Cat_Source", catSource);
                paramList[7] = new SqlParameter("@P_Cat_Status", catStatus);
                int rec = DBGateway.ExecuteNonQuerySP("usp_SaveCatalogueData", paramList);
            }
            catch (Exception ex)
            {
                throw new Exception("StaticData-Failed to save Catalogue Data : " + ex.Message, ex);
            }
        }

        /// <summary>
        /// Returns the  Data values from Database based on the type and code requested.        
        /// </summary>
        /// <param name="code"> Data Code. Ex: "3"</param>
        /// <param name="type">Type of data to retrieve.Ex: "MP" RC - Room category, MP - Meal Plan, AC - AccommodationCategory, OS - OfferSupplementType, AS - AccommodationService</param>
        /// <returns></returns>
        private DataTable GetDataFromDB(string type, HotelBookingSource source)
        {
            DataTable dtStaticData = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];                
                paramList[0] = new SqlParameter("@P_type", type);
                paramList[1] = new SqlParameter("@P_source", (int)source);
                dtStaticData = DBGateway.FillDataTableSP("usp_GetCatalogueData", paramList);
            }
            catch (Exception ex)
            {
                throw new Exception(source.ToString() + "-StaticData-Failed to retrieve data from DB : " + ex.Message, ex);
            }

            return dtStaticData;
        }

        private List<CatalogueData> GetDataFromXML(HotelBookingSource source, string type)
        {
            List<CatalogueData> StaticData = new List<CatalogueData>();
            string requestXML, responseXML;
            try
            {
                switch (source)
                {
                    case HotelBookingSource.LOH:
                        string response = GenerateCatalogueDataResponse();
                        StaticData = ReadCatalogueDataResponse(response);
                        break;
                    case HotelBookingSource.EET:
                        string eetResponse = GenerateCatalogueDataResponse();
                        StaticData = ReadCatalogueDataResponse(eetResponse);
                        break;
                    case HotelBookingSource.DOTW:
                        switch (type)
                        {
                            case "Leisure":
                                requestXML = GenerateLeisureItemsRequest();
                                responseXML = GetRequest(requestXML);
                                StaticData = ReadLeisureItemsResponse(responseXML);
                                break;
                            case "Business":
                                requestXML = GenerateBusinessItemsRequest();
                                responseXML = GetRequest(requestXML);
                                StaticData = ReadBusinessItemsResponse(responseXML);
                                break;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(source.ToString() + "-StaticData-Failed to get data from Catalogue data Web Service : " + ex.Message, ex);
            }

            return StaticData;
        }

        private string GenerateCatalogueDataRequest()
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.juniper.es/webservice/2007/");


                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("", "GetCatalogueService", "http://www.juniper.es/webservice/2007/");
                writer.WriteStartElement("CatalogueDataRQ");
                writer.WriteAttributeString("Language", "EN");

                writer.WriteStartElement("Login");
                writer.WriteAttributeString("Email", agentCode);
                writer.WriteAttributeString("Password", password);
                writer.WriteEndElement();//Login

                writer.WriteStartElement("Data");
                writer.WriteStartElement("DataTypes");
                writer.WriteAttributeString("Type", "RoomCategory");
                writer.WriteEndElement();//DataType
                writer.WriteStartElement("DataTypes");
                writer.WriteAttributeString("Type", "Mealplan");
                writer.WriteEndElement();//DataType
                writer.WriteStartElement("DataTypes");
                writer.WriteAttributeString("Type", "AccommodationType");
                writer.WriteEndElement();//DataType
                writer.WriteStartElement("DataTypes");
                writer.WriteAttributeString("Type", "AccommodationCategory");
                writer.WriteEndElement();//DataType
                writer.WriteStartElement("DataTypes");
                writer.WriteAttributeString("Type", "OfferSupplementType");
                writer.WriteStartElement("DataTypes");
                writer.WriteAttributeString("Type", "AccommodationService");
                writer.WriteEndElement();//DataType

                writer.WriteEndElement();//DataType
                writer.WriteEndElement();//Data
                writer.WriteEndElement();//CatalogueDataRQ
                writer.WriteEndElement();//GetCatalogueService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();
                string XmlPath = string.Empty;
                if (hotelSource == HotelBookingSource.LOH)
                {
                    XmlPath = ConfigurationSystem.LOHConfig["XmlLogPath"];
                }
                else
                {
                    XmlPath = ConfigurationSystem.EETConfig["XmlLogPath"];
                }
                XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
                try
                {
                    if (!System.IO.Directory.Exists(XmlPath))
                    {
                        System.IO.Directory.CreateDirectory(XmlPath);
                    }
                }
                catch { }
                try
                {
                    string filePath = string.Empty;
                    filePath = XmlPath + "CatalogueDataRequest_" + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, 1, filePath, "127.0.0.1");
                }
                catch { }

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to generate Catalogue Data request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        private string GenerateCatalogueDataResponse()
        {
            string responseFromServer = "";
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.LOHConfig["CatalogueDataReq"]);
                if (hotelSource == HotelBookingSource.EET)
                {
                    request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.EETConfig["CatalogueDataReq"]);
                }
                request.Method = "POST"; //Using POST method       
                string postData = GenerateCatalogueDataRequest();// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return responseFromServer;
        }

        private List<CatalogueData> ReadCatalogueDataResponse(string response)
        {
            List<CatalogueData> StaticData = new List<CatalogueData>();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response);
                string XmlPath = string.Empty;
                if (hotelSource == HotelBookingSource.LOH)
                {
                    XmlPath = ConfigurationSystem.LOHConfig["XmlLogPath"];
                }
                else
                {
                    XmlPath = ConfigurationSystem.EETConfig["XmlLogPath"];
                }
                XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
                try
                {
                    if (!System.IO.Directory.Exists(XmlPath))
                    {
                        System.IO.Directory.CreateDirectory(XmlPath);
                    }
                }
                catch { }
                try
                {
                    string filePath = string.Empty;
                    filePath = XmlPath + "CatalogueDataResponse_" + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, 1, filePath, "127.0.0.1");
                }
                catch { }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");

                XmlNode root = doc.DocumentElement;
                XmlNodeList Results = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.FirstChild.ChildNodes;

                if (Results[0].Name == "Error")
                {

                }
                else
                {

                    foreach (XmlNode result in Results)
                    {
                        switch (result.Attributes["Type"].Value)
                        {
                            case "RoomCategory":
                                foreach (XmlNode element in result.FirstChild.ChildNodes)
                                {
                                    CatalogueData cd = new CatalogueData();
                                    cd.catId = element.Attributes["Id"].Value;
                                    cd.catCode = element.Attributes["Code"].Value;
                                    if (element.Attributes["Category"] != null)
                                    {
                                        cd.catCategory = element.Attributes["Category"].Value;
                                    }
                                    else
                                    {
                                        cd.catCategory = string.Empty;
                                    }
                                    cd.catSource = (int)hotelSource;
                                    cd.catStatus = "A";
                                    cd.catType = "RC";
                                    cd.catTypeName = "Room Category";
                                    cd.catValue = element.InnerText;
                                    StaticData.Add(cd);
                                }
                                break;
                            case "Mealplan":
                                foreach (XmlNode element in result.FirstChild.ChildNodes)
                                {
                                    CatalogueData cd = new CatalogueData();
                                    cd.catId = element.Attributes["Id"].Value;
                                    cd.catCode = element.Attributes["Code"].Value;
                                    if (element.Attributes["Category"] != null)
                                    {
                                        cd.catCategory = element.Attributes["Category"].Value;
                                    }
                                    else
                                    {
                                        cd.catCategory = string.Empty;
                                    }
                                    cd.catSource = (int)hotelSource;
                                    cd.catStatus = "A";
                                    cd.catType = "MP";
                                    cd.catTypeName = "Meal Plan";
                                    cd.catValue = element.InnerText;
                                    StaticData.Add(cd);
                                }
                                break;
                            case "AccommodationType":
                                foreach (XmlNode element in result.FirstChild.ChildNodes)
                                {
                                    CatalogueData cd = new CatalogueData();
                                    cd.catId = element.Attributes["Id"].Value;
                                    cd.catCode = element.Attributes["Code"].Value;
                                    if (element.Attributes["Category"] != null)
                                    {
                                        cd.catCategory = element.Attributes["Category"].Value;
                                    }
                                    else
                                    {
                                        cd.catCategory = string.Empty;
                                    }
                                    cd.catSource = (int)hotelSource;
                                    cd.catStatus = "A";
                                    cd.catType = "AT";
                                    cd.catTypeName = "Accommodation Type";
                                    cd.catValue = element.InnerText;
                                    StaticData.Add(cd);
                                }
                                break;
                            case "AccommodationCategory":
                                foreach (XmlNode element in result.FirstChild.ChildNodes)
                                {
                                    CatalogueData cd = new CatalogueData();
                                    cd.catId = element.Attributes["Id"].Value;
                                    cd.catCode = element.Attributes["Code"].Value;
                                    if (element.Attributes["Category"] != null)
                                    {
                                        cd.catCategory = element.Attributes["Category"].Value;
                                    }
                                    else
                                    {
                                        cd.catCategory = string.Empty;
                                    }
                                    cd.catSource = (int)hotelSource;
                                    cd.catStatus = "A";
                                    cd.catType = "AC";
                                    cd.catTypeName = "Accommodation Category";
                                    cd.catValue = element.InnerText;
                                    StaticData.Add(cd);
                                }
                                break;
                            case "OfferSupplementType":
                                foreach (XmlNode element in result.FirstChild.ChildNodes)
                                {
                                    CatalogueData cd = new CatalogueData();
                                    cd.catId = element.Attributes["Id"].Value;
                                    cd.catCode = element.Attributes["Code"].Value;
                                    if (element.Attributes["Category"] != null)
                                    {
                                        cd.catCategory = element.Attributes["Category"].Value;
                                    }
                                    else
                                    {
                                        cd.catCategory = string.Empty;
                                    }
                                    cd.catSource = (int)hotelSource;
                                    cd.catStatus = "A";
                                    cd.catType = "OS";
                                    cd.catTypeName = "Offer Supplement Type";
                                    cd.catValue = element.InnerText;
                                    StaticData.Add(cd);
                                }
                                break;
                            case "AccommodationService":
                                foreach (XmlNode element in result.FirstChild.ChildNodes)
                                {
                                    CatalogueData cd = new CatalogueData();
                                    cd.catId = element.Attributes["Id"].Value;
                                    cd.catCode = element.Attributes["Code"].Value;
                                    if (element.Attributes["Category"] != null)
                                    {
                                        cd.catCategory = element.Attributes["Category"].Value;
                                    }
                                    else
                                    {
                                        cd.catCategory = string.Empty;
                                    }
                                    cd.catSource = (int)hotelSource;
                                    cd.catStatus = "A";
                                    cd.catType = "AS";
                                    cd.catTypeName = "Accommodation Service";
                                    cd.catValue = element.InnerText;
                                    StaticData.Add(cd);
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-StaticData-Failed to read Catalogue Data Response : " + ex.Message, ex);
            }
            return StaticData;
        }

        private string GenerateLeisureItemsRequest()
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", ConfigurationSystem.DOTWConfig["UserName"]);

            xmlString.WriteElementString("password", ConfigurationSystem.DOTWConfig["Password"]);
            xmlString.WriteElementString("id", ConfigurationSystem.DOTWConfig["CompanyCode"]);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "getleisureids");
            xmlString.WriteEndElement();//request
            xmlString.WriteEndElement();//customer

            xmlString.Close();
            xmlString.Flush();

            if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + "HotelLeisureItemsREQ_" + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(strWriter.ToString());
                doc.Save(filePath);
                doc = null;
            }

            return strWriter.ToString();
        }


        private string GetRequest(string request)
        {
            string response = "";
            try
            {
                bool testMode = false; string reqURL = string.Empty;
                if (ConfigurationSystem.DOTWConfig["TestMode"].Equals("true"))
                {
                    testMode = true;
                }
                else
                {
                    testMode = false;
                }
                if (testMode)
                {
                    reqURL = ConfigurationSystem.DOTWConfig["RequestURL"];
                }
                else
                {
                    reqURL = ConfigurationSystem.DOTWConfig["RequestProdURL"];
                }

                if (request.Contains("<?xml version=\"1.0\" encoding=\"utf-16\"?>"))
                {
                    request = request.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                }
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                byte[] byte1 = encoding.GetBytes(request);
                HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(reqURL);

                HttpWReq.ContentType = "text/xml";
                HttpWReq.ContentLength = byte1.Length;
                HttpWReq.Method = "POST";
                HttpWReq.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");

                System.IO.Stream StreamData = HttpWReq.GetRequestStream();
                StreamData.Write(byte1, 0, byte1.Length);
                StreamData.Close();

                // get response
                HttpWebResponse HttpWRes = HttpWReq.GetResponse() as HttpWebResponse;

                //Reading response
                Stream dataStream = GetStreamForResponse(HttpWRes);
                StreamReader reader = new StreamReader(dataStream);

                response = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response.ToString();
        }

        private List<CatalogueData> ReadLeisureItemsResponse(string response)
        {
            List<CatalogueData> leisureItems = new List<CatalogueData>();

            try
            {
                //Trace.TraceInformation("DOTWApi.ReadHotelLeisureReponse entered");
                
                TextReader stringRead = new StringReader(response);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stringRead);

                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
                if (ErrorInfo != null)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + response, "");
                    //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                else
                {
                    XmlNodeList leisureInfo = xmlDoc.SelectNodes("result/leisures/option");
                    if (leisureInfo.Count == 0)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: No leisure item found!| " + DateTime.Now + "| Response XML" + response, "");
                        //Trace.TraceError("No Leisure items found. Response not coming! . Response =" + response);
                        throw new BookingEngineException("<br> No Leisure items found!. Response not coming ");
                    }
                    else
                    {
                        foreach (XmlNode option in leisureInfo)
                        {
                            CatalogueData cd = new CatalogueData();
                            cd.CatCategory = "";
                            cd.CatId = option.Attributes["value"].Value;
                            cd.CatSource = (int)HotelBookingSource.DOTW;
                            cd.CatCode = "";
                            cd.CatStatus = "A";
                            cd.CatType = "Leisure";
                            cd.CatTypeName = "Leisure";
                            cd.CatValue = option.InnerText;
                            cd.LastUpdated = DateTime.Now;

                            leisureItems.Add(cd);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return leisureItems;
        }

        private string GenerateBusinessItemsRequest()
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", ConfigurationSystem.DOTWConfig["UserName"]);

            xmlString.WriteElementString("password", ConfigurationSystem.DOTWConfig["Password"]);
            xmlString.WriteElementString("id", ConfigurationSystem.DOTWConfig["CompanyCode"]);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "getbusinessids");
            xmlString.WriteEndElement();//request
            xmlString.WriteEndElement();//customer

            xmlString.Close();
            xmlString.Flush();

            if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + "HotelBusinessItemsREQ_" + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(strWriter.ToString());
                doc.Save(filePath);
                doc = null;
            }
            return strWriter.ToString();
        }

        private List<CatalogueData> ReadBusinessItemsResponse(string response)
        {
            List<CatalogueData> businessItems = new List<CatalogueData>();

            try
            {
                //Trace.TraceInformation("DOTWApi.ReadHotelBusinessReponse entered");

                TextReader stringRead = new StringReader(response);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stringRead);

                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
                if (ErrorInfo != null)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + response, "");
                    //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                else
                {
                    XmlNodeList businessInfo = xmlDoc.SelectNodes("result/business/option");
                    if (businessInfo.Count == 0)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: No business item found!| " + DateTime.Now + "| Response XML" + response, "");
                        //Trace.TraceError("No business items found. Response not coming!. Response =" + response);
                        throw new BookingEngineException("<br> No business items found!. Response not coming ");
                    }
                    else
                    {
                        foreach (XmlNode option in businessInfo)
                        {
                            CatalogueData cd = new CatalogueData();
                            cd.CatCategory = "";
                            cd.CatId = option.Attributes["value"].Value;
                            cd.CatSource = (int)HotelBookingSource.DOTW;
                            cd.CatCode = "";
                            cd.CatStatus = "A";
                            cd.CatType = "Leisure";
                            cd.CatTypeName = "Leisure";
                            cd.CatValue = option.InnerText;
                            cd.LastUpdated = DateTime.Now;

                            businessItems.Add(cd);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return businessItems;
        }
    }
}
