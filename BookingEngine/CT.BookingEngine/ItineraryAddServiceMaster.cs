﻿using System;
//using System.Linq;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CT.Core;

namespace CT.BookingEngine
{
    public class ItineraryAddServiceMaster
    {
        #region Members
        int service_id;
        string service_code;
        string service_name;
        decimal service_amount;
        int service_product;
        int service_agent_id;
        string service_type;
        string service_nationality;
        string service_status;
        int created_by;
        #endregion

        #region Properties

        public int Service_id
        {
            get { return service_id; }
            set { service_id = value; }
        }

        public string Service_code
        {
            get { return service_code; }
            set { service_code = value; }
        }
        public string Service_name
        {
            get { return service_name; }
            set { service_name = value; }
        }
        public decimal Service_amount
        {
            get { return service_amount; }
            set { service_amount = value; }
        }
        public int Service_product
        {
            get { return service_product; }
            set { service_product = value; }
        }
        public int Service_agent_id
        {
            get { return service_agent_id; }
            set { service_agent_id = value; }
        }
        public string Service_type
        {
            get { return service_type; }
            set { service_type = value; }
        }
        public string Service_nationality
        {
            get { return service_nationality; }
            set { service_nationality = value; }
        }
        public string Service_status
        {
            get { return service_status; }
            set { service_status = value; }
        }
        public int Created_by
        {

            get { return created_by; }
            set { created_by = value; }
        }


        #endregion

        #region Constructor
        public ItineraryAddServiceMaster()
        {
            Service_id = -1;
        }
        public ItineraryAddServiceMaster(int id)
        {
            Service_id = id;
            GetDetails(Service_id);
        }

        #endregion

        #region Methods
        private void GetDetails(int id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
        }
        public DataSet GetData(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ServiceId", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_GetAdditionalServiceDetailsByServiceId", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (dr["service_id"] != DBNull.Value)
                            {
                                service_id = Convert.ToInt32(dr["service_id"]);
                            }
                            if (dr["service_code"] != DBNull.Value)
                            {
                                service_code = Convert.ToString(dr["service_code"]);
                            }
                            if (dr["service_name"] != DBNull.Value)
                            {
                                service_name = Convert.ToString(dr["service_name"]);
                            }
                            if (dr["service_amount"] != DBNull.Value)
                            {
                                service_amount = Convert.ToDecimal(dr["service_amount"]);
                            }
                            if (dr["service_product"] != DBNull.Value)
                            {
                                service_product = Convert.ToInt32(dr["service_product"]);
                            }
                            if (dr["service_agent_id"] != DBNull.Value)
                            {
                                service_agent_id = Convert.ToInt32(dr["service_agent_id"]);
                            }
                            if (dr["service_type"] != DBNull.Value)
                            {
                                service_type = Convert.ToString(dr["service_type"]);
                            }

                            if (dr["service_nationality"] != DBNull.Value)
                            {
                                service_nationality = Convert.ToString(dr["service_nationality"]);
                            }
                            if (dr["service_status"] != DBNull.Value)
                            {
                                service_status = Convert.ToString(dr["service_status"]);
                            }
                            if (dr["created_by"] != DBNull.Value)
                            {
                                created_by = Convert.ToInt32(dr["created_by"]);
                            }

                        }

                    }




                }

            }
            catch
            { throw; }

        }

        public int Save()
        {
            int serviceId = 0;
            try
            {

                SqlParameter[] param = new SqlParameter[11];
                param[0] = new SqlParameter("@P_Service_Id", service_id);
                param[1] = new SqlParameter("@P_Service_Code", service_code);
                param[2] = new SqlParameter("@P_Service_Name", service_name);
                param[3] = new SqlParameter("@P_Service_Amount", service_amount);
                param[4] = new SqlParameter("@P_Service_Product", service_product);
                param[5] = new SqlParameter("@P_Service_Agent_Id", service_agent_id);
                param[6] = new SqlParameter("@P_Service_Type", service_type);
                param[7] = new SqlParameter("@P_Service_Nationality", service_nationality);
                param[8] = new SqlParameter("@P_Service_Status", service_status);
                param[9] = new SqlParameter("@P_Created_by", created_by);
                param[10] = new SqlParameter("@P_SerId_Ret", SqlDbType.Int);
                param[10].Direction = ParameterDirection.Output;
                int count = DBGateway.ExecuteNonQuerySP("usp_AddUpdate_AdditionalService", param);
                if (count > 0 && param[10].Value != DBNull.Value)
                {
                    serviceId = Convert.ToInt32(param[10].Value);
                }

            }
            catch
            {
                throw;
            }
            return serviceId;

        }

        public static DataTable GetAdditionalServiceMasterDetails(string agenttype,int agentid)
        {
            DataTable dtDetails = null;
            try
            {
                SqlParameter[] paramslist = new SqlParameter[2];
                paramslist[0] = new SqlParameter("@P_AGENT_TYPE", agenttype);
                paramslist[1] = new SqlParameter("@P_AGENT_ID", agentid);
                dtDetails = DBGateway.ExecuteQuery("usp_GetAllAdditionalServiceList",paramslist).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
            return dtDetails;
        }

        public static DataTable GetActiveServiceList(int agentId)
        {
            DataTable dtDetails = null;
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AgentId", agentId);
                dtDetails = DBGateway.ExecuteQuery("usp_GetActiveServiceList", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
            return dtDetails;
        }

        public void GetActiveServiceListByServiceType(int agentId)
        {

            try
            {

                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AgentId", agentId);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetActiveServiceList_ByServiceType", paramList, connection);
                if (dataReader.Read())
                {
                    service_nationality = Convert.ToString(dataReader["service_nationality"]);
                }
                dataReader.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }

        }



        #endregion

    }

    public class ItineraryAddServiceDetails
    {

        #region Members
        private int add_id;
        private int service_id;
        private int pax_id;
        private string pax_name;
        private string pax_nationality_id;
        private string pax_passport_no;
        private string pax_mobile_no;
        private DateTime traveldate;
        private string flightno;
        private string routing;
        private string remarks;
        private string flex_1;
        private string flex_2;
        private string flex_3;
        private string flex_4;
        private string status;
        private int created_by;
        private decimal amount;
        private string docNo;
        private string pnr_no;
        #endregion

        #region Properties
        public int Add_id
        {
            get { return add_id; }
            set { add_id = value; }
        }
        public int Service_id
        {
            get { return service_id; }
            set { service_id = value; }
        }
        public int Pax_id
        {
            get { return pax_id; }
            set { pax_id = value; }
        }
        public string Pax_name
        {
            get { return pax_name; }
            set { pax_name = value; }
        }

        public string Pax_nationality_id
        {
            get { return pax_nationality_id; }
            set { pax_nationality_id = value; }
        }
        public string Pax_passport_no
        {
            get { return pax_passport_no; }
            set { pax_passport_no = value; }
        }
        public string Pax_mobile_no
        {
            get { return pax_mobile_no; }
            set { pax_mobile_no = value; }
        }
        public DateTime Traveldate
        {
            get { return traveldate; }
            set { traveldate = value; }
        }
        public string Flightno
        {
            get { return flightno; }
            set { flightno = value; }
        }
        public string Routing
        {
            get { return routing; }
            set { routing = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public string Flex_1
        {
            get { return flex_1; }
            set { flex_1 = value; }
        }
        public string Flex_2
        {
            get { return flex_2; }
            set { flex_2 = value; }
        }
        public string Flex_3
        {
            get { return flex_3; }
            set { flex_3 = value; }
        }
        public string Flex_4
        {
            get { return flex_4; }
            set { flex_4 = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public int Created_by
        {

            get { return created_by; }
            set { created_by = value; }

        }
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }

        }
        public string DocNo
        {
            get { return docNo; }
            set { docNo = value; }
        }

        public string Pnr_No
        {
            get { return pnr_no; }
            set { pnr_no = value; }
        }
        #endregion

        #region Constructor
        public ItineraryAddServiceDetails()
        {
            add_id = -1;
        }
        #endregion

        #region Methods
        public int Save()
        {
            int detailId = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[21];
                param[0] = new SqlParameter("@P_add_id", add_id);
                param[1] = new SqlParameter("@P_service_id", service_id);
                param[2] = new SqlParameter("@P_pax_id", pax_id);
                param[3] = new SqlParameter("@P_pax_name", pax_name);
                param[4] = new SqlParameter("@P_pax_nationality_id", pax_nationality_id);
                param[5] = new SqlParameter("@P_pax_passport_no", pax_passport_no);
                param[6] = new SqlParameter("@P_pax_mobile_no", pax_mobile_no);
                param[7] = new SqlParameter("@P_traveldate", traveldate);
                param[8] = new SqlParameter("@P_flightno", flightno);
                param[9] = new SqlParameter("@P_routing", routing);

                if (string.IsNullOrEmpty(flex_1))
                {
                    param[10] = new SqlParameter("@P_flex_1", DBNull.Value);
                }
                else
                {
                    param[10] = new SqlParameter("@P_flex_1", flex_1);
                }
                if (string.IsNullOrEmpty(flex_2))
                {
                    param[11] = new SqlParameter("@P_flex_2", DBNull.Value);
                }
                else
                {
                    param[11] = new SqlParameter("@P_flex_2", flex_2);
                }
                if (string.IsNullOrEmpty(flex_3))
                {
                    param[12] = new SqlParameter("@P_flex_3", DBNull.Value);
                }
                else
                {
                    param[12] = new SqlParameter("@P_flex_3", flex_3);
                }
                if (string.IsNullOrEmpty(flex_4))
                {
                    param[13] = new SqlParameter("@P_flex_4", DBNull.Value);
                }
                else
                {
                    param[13] = new SqlParameter("@P_flex_4", flex_4);
                }

                param[14] = new SqlParameter("@P_status", status);
                param[15] = new SqlParameter("@P_created_by", created_by);
                param[16] = new SqlParameter("@P_remarks", remarks);

                param[17] = new SqlParameter("@P_amount", amount);
                param[18] = new SqlParameter("@P_pnrno", pnr_no);

                param[19] = new SqlParameter("@P_AddId_Ret", SqlDbType.Int);
                param[19].Direction = ParameterDirection.Output;


                param[20] = new SqlParameter("@P_Doc_No", SqlDbType.NVarChar, 20);
                param[20].Direction = ParameterDirection.Output;

                int count = DBGateway.ExecuteNonQuerySP("usp_AddUpdate_AdditionalService_Details", param);
                if (count > 0 && param[19].Value != DBNull.Value)
                {
                    detailId = Convert.ToInt32(param[19].Value);
                    if (param[20].Value != DBNull.Value)
                    {
                        docNo = Convert.ToString(param[20].Value);
                    }
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return detailId;

        }
        public static DataTable GetServiceDetails(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_pax_id", id);
                DataTable dtResult = DBGateway.FillDataTableSP("usp_Get_AdditionalService_Details_By_Pax", paramList);
                return dtResult;
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetServiceDetailsByServiceId(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_Ser_id", id);
                DataTable dtResult = DBGateway.FillDataTableSP("usp_Get_AdditionalService_Details_By_Service_id", paramList);
                return dtResult;
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetPaxDetails(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_pax_id", id);
                DataTable dtResult = DBGateway.FillDataTableSP("usp_Get_ServicePaxDetails_By_PaxId", paramList);
                return dtResult;
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetFlightInfo(int paxId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_pax_id", paxId);
                DataTable dtResult = DBGateway.FillDataTableSP("usp_Get_FlightInfoBy_PaxId", paramList);
                return dtResult;
            }
            catch
            {
                throw;
            }
        }

       
        /// <summary>
        /// This method returns the Hala Service List Items
        /// </summary>
        /// <param name="paxId"></param>
        /// <returns></returns>
        public static DataTable GetHalaQueueItems(DateTime fromDate, DateTime toDate,int isused)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_From_Date", fromDate);
                paramList[1] = new SqlParameter("@P_To_Date", toDate);
                if (isused >= 0) paramList[2] = new SqlParameter("@P_Is_Used", isused);
                DataTable dtResult = DBGateway.FillDataTableSP("usp_GetHalaQueueItems", paramList);
                return dtResult;
            }
            catch
            {
                throw;
            }
        }
        public static void UpdateFlightAcctStatus(long addId, int modifiedBy)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_ADD_ID", addId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[3].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("USP_UPDATE_BKE_FLIGHT_ADD_SERVICES_STATUS", paramList);
            }
            catch { throw; }
        }

        #endregion

    }

}
