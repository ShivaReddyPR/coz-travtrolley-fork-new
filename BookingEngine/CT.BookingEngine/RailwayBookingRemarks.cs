using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    //Offline Train Booking
    /// <summary>
    /// This class is using to save the RailwayPassenger in DB and retrive the RailwayPassenger from DB
    /// </summary>
    [Serializable ]
    public class RailwayBookingRemarks
    {
        #region public properties
        /// <summary>
        /// RBRId - BookingRemarkID
        /// </summary>
        public long BookingRemarksID { get; set; }
        /// <summary>
        /// RBId - BooingID
        /// </summary>
        public long BookingID { get; set; }
        /// <summary>
        /// RemarksStatus
        /// </summary>
        public string RemarksStatus { get; set; }
        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        public int CreatedBy { get; set; }
        /// <summary>
        /// CreatedOn
        /// </summary>
        public DateTime? CreatedOn { get; set; }
        /// <summary>
        /// ModifiedBy
        /// </summary>
        public int? ModifiedBy { get; set; }
        /// <summary>
        /// ModifiedOn
        /// </summary>
        public DateTime? ModifiedOn { get; set; }
        /// <summary>
        /// PNR
        /// </summary>
        public string PNR { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// This Method is used to Save the Hotel Passenger details
        /// </summary>
        /// <returns>int</returns>
        public int SaveRemarks()
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@MessageStatus", SqlDbType.Int));
                paramList[0].Direction = ParameterDirection.Output;
                paramList.Add(new SqlParameter("@Message", SqlDbType.NVarChar,150));
                paramList[1].Direction = ParameterDirection.Output;
                paramList.Add(new SqlParameter("@RBRId", BookingRemarksID));
                paramList.Add(new SqlParameter("@RBId", BookingID));
                paramList.Add(new SqlParameter("@RemarksStatus", RemarksStatus));
                paramList.Add(new SqlParameter("@Remarks", Remarks));
                paramList.Add(new SqlParameter("@CreatedBy", CreatedBy));
                if (PNR != null)
                    paramList.Add(new SqlParameter("@PNR", PNR));
                else
                    paramList.Add(new SqlParameter("@PNR", DBNull.Value));
                
                DBGateway.ExecuteNonQuerySP("usp_AddRailwayRemarksAndUpdatePNR", paramList.ToArray());
                int messageStatus= Convert.ToInt32(paramList[0].Value);
                string message = paramList[1].Value.ToString();
                return messageStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAllRemarks(long bookingID)
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@RBId", bookingID));
                return DBGateway.ExecuteQuery("usp_GetRailwayBookingRemarks", paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
