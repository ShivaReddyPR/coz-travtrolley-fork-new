using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using CT.Configuration;


namespace CT.BookingEngine
{
    public enum PassengerType
    {
        NoPax=0,
        Adult = 1,
        Child = 2,
        Infant = 3,
        Senior = 4
    }
    [Serializable]
    public class FlightPassenger
    {
        #region Private Variables
        /// <summary>
        /// Unique Id for a passenger
        /// </summary>
        int paxId;
        /// <summary>
        /// flight id of the flight to which the pax is linked
        /// </summary>
        int flightId;
        /// <summary>
        /// First name of passenger
        /// </summary>
        string firstName;
        /// <summary>
        /// Last name of passenger
        /// </summary>
        string lastName;
        /// <summary>
        /// TODO
        /// </summary>
        string title;
        /// <summary>
        /// full Name of passenger. (Title + FName + LName).
        /// </summary>
        string fullName;
        /// <summary>
        /// Mobile phone number of passenger
        /// </summary>
        string cellPhone;
        /// <summary>
        /// Indicates if the passenger is primary (leading) passenger
        /// </summary>
        bool isLeadPax;
        /// <summary>
        /// Date of Birth of passenger
        /// </summary> 
        DateTime dateOfBirth;
        /// <summary>
        /// Type of passenger. Adult, Child, Infant or Senior
        /// </summary>
        PassengerType type;
        /// <summary>
        /// Passport no of passenger
        /// </summary>
        string passportNo;
        /// <summary>
        /// Nationality of the passenger
        /// </summary>
        Country nationality;
        /// <summary>
        /// Country issueing passport.
        /// </summary>
        Country country;
        /// <summary>
        /// City Name
        /// </summary>
        string city;
        /// <summary>
        /// Address Line1
        /// </summary>
        string addressLine1;
        /// <summary>
        /// Address Line2
        /// </summary>
        string addressLine2;
        /// <summary>
        /// gender of the passenger
        /// </summary>
        Gender gender;
        /// <summary>
        /// EmailId of passenger
        /// </summary>
        string email;
        /// <summary>
        /// Meal Preference of passenger
        /// </summary>
        Meal meal;
        /// <summary>
        /// Seat Preference of passenger
        /// </summary>
        Seat seat;
        /// <summary>
        /// Price detail of services to passenger
        /// </summary>
        PriceAccounts price;
        /// <summary>
        /// Frequent flier airline
        /// </summary>
        string ffAirline;
        /// <summary>
        /// Frequent flier number
        /// </summary>
        string ffNumber;
        /// <summary>
        /// LastName.FirstName.Title spaces replaced with '.'.
        /// Used to identify a passenger in a booking.
        /// </summary>
        string paxKey;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Expirt Date of passport
        /// </summary> 
        DateTime passportExpiry;
        /// <summary>
        /// BaggageCode for G9
        /// </summary>
        string baggageCode;

        /// <summary>
        /// BaggageCode Type for FlyDubai (BAGB,BAGL etc)
        /// </summary>
        string baggageType;
        /// <summary>
        /// Category/SSRCategory Id for FlyDubai (99 for Baggage)
        /// </summary>
        string categoryId;
        /// <summary>
        /// Baggage Charge for Flydubai.
        /// </summary>
        List<decimal> baggageCharge;

        PriceAccounts tboPrice;

        /// <summary>
        /// Flex Fields
        /// </summary>
        List<FlightFlexDetails> flexDetailsList;

        string destinationPhone;//Added by Lokesh on 25-sept-2017 regarding Pax Destination phone for booking tickets

        string gstStateCode; //Added by Lokesh on 27-Mar-2018 For G9 Source if the customer is travelling from India and selects the state Id.
        string gstTaxRegNo;//Added by Lokesh on 27-Mar-2018 For G9 Source if the customer is travelling from India and provides the TaxRegNo.
        /// <summary>
        /// Mandatory for FraudLabs. Saved for Audit in B2C
        /// </summary>
        string stateCode;

        /// <summary>
        /// string key stands for Tax Type.
        /// double value is the Tax Value.
        /// </summary>
        private List<KeyValuePair<string, decimal>> taxBreakup;

        string mealType; //For Spicejet And indigo Meal Selection
        string mealDesc; //For Spicejet And indigo Meal Selection in Eticket display purpose;
               
        /// <summary>
        /// To add selected seat per pax per segment
        /// class object Added by Praveen to save selected seat info
        /// </summary>
        private List<PaxSeatInfo> _liPaxSeatInfo;

        /// <summary>
        /// To display all segments seat info
        /// string Added by Praveen to show selected seat info of all segments in payment confirmation page
        /// </summary>
        private string sSeatInfo;

        /// <summary>
        /// To store corporate profile Id
        /// string Added by Praveen to store corporate profile Id and save the same in itinerary passenger table
        /// </summary>
        private string _CorpProfileId;

        #endregion

        #region Public Members
        /// <summary>
        /// Gets or Sets paxId
        /// </summary>
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }

        /// <summary>
        /// Gets of sets the Flight Id.
        /// </summary>
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }

        /// <summary>
        /// Gets or sets First name
        /// </summary>
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        /// <summary>
        /// Gets or sets Last name
        /// </summary>
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        /// <summary>
        /// Gets or sets the title of passenger
        /// </summary>
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        /// <summary>
        /// Gets full name of a passenger.
        /// </summary>
        public string FullName
        {
            get
            {
                if (fullName == null || fullName.Length == 0)
                {
                    StringBuilder fName = new StringBuilder();
                    fName.Append(title);
                    fName.Append(" ");
                    fName.Append(firstName);
                    fName.Append(" ");
                    fName.Append(lastName);
                    fullName = fName.ToString().Trim();
                }
                return fullName;
            }
        }

        /// <summary>
        /// Gets or sets the Cellphone of passenger
        /// </summary>
        public string CellPhone
        {
            get
            {
                return cellPhone;
            }
            set
            {
                cellPhone = value;
            }
        }

        /// <summary>
        /// Gets or sets the isLeadPax property.
        /// </summary>
        public bool IsLeadPax
        {
            get
            {
                return isLeadPax;
            }
            set
            {
                isLeadPax = value;
            }
        }

        /// <summary>
        /// Gets or sets Date of birth
        /// </summary>
        public DateTime DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                dateOfBirth = value;
            }
        }

        /// <summary>
        /// Gets or sets the passenger type
        /// </summary>
        public PassengerType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        /// <summary>
        /// Gets or sets the passport number
        /// </summary>
        public string PassportNo
        {
            get
            {
                return passportNo;
            }
            set
            {
                passportNo = value;
            }
        }
        /// <summary>
        /// get or set the Nationality
        /// </summary>
        public Country Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                nationality = value;
            }
        }
        public Country Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        /// <summary>
        ///  gets or sets city value
        /// </summary>
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        /// <summary>
        /// Gets or sets the address line 1
        /// </summary>
        public string AddressLine1
        {
            get
            {
                return addressLine1;
            }
            set
            {
                addressLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the address line 2
        /// </summary>
        public string AddressLine2
        {
            get
            {
                return addressLine2;
            }
            set
            {
                addressLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Gender
        /// </summary>
        public Gender Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }

        /// <summary>
        /// Gets or sets the email id of passenger
        /// </summary>
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        /// <summary>
        /// Gets or sets the Meal preference of passenger
        /// </summary>
        public Meal Meal
        {
            get
            {
                return meal;
            }
            set
            {
                meal = value;
            }
        }

        /// <summary>
        /// Gets or sets the Seat preference
        /// </summary>
        public Seat Seat
        {
            get
            {
                return seat;
            }
            set
            {
                seat = value;
            }
        }

        /// <summary>
        /// Gets or sets the price for the services to the passenger
        /// </summary>
        public PriceAccounts Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }

        /// <summary>
        /// Gets or sets the Frequent Flier airline of passenger
        /// </summary>
        public string FFAirline
        {
            get
            {
                return ffAirline;
            }
            set
            {
                ffAirline = value;
            }
        }

        /// <summary>
        /// Gets or sets the Frequent Flier Number of the passenger
        /// </summary>
        public string FFNumber
        {
            get
            {
                return ffNumber;
            }
            set
            {
                ffNumber = value;
            }
        }

        /// <summary>
        /// Gets PaxKey. LastName.FirstName.Title with spaces replaced with '.'.
        /// </summary>
        public string PaxKey
        {
            get
            {
                if (paxKey == null || paxKey.Length == 0)
                {
                    StringBuilder key = new StringBuilder();
                    key.Append(lastName);
                    key.Append(".");
                    key.Append(firstName);
                    if (title != null && title.Length > 0)
                    {
                        //key.Append(".");
                        key.Append(title);
                    }
                    paxKey = key.ToString().Replace(' ', '.').Trim().ToUpper();
                }
                paxKey = paxKey.Replace(".", "");
                return paxKey;
            }
        }

        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets passport expiry date
        /// </summary>
        public DateTime PassportExpiry
        {
            get
            {
                return passportExpiry;
            }
            set
            {
                passportExpiry = value;
            }
        }
        /// <summary>
        /// BaggageCode for G9 (Air Arabia)
        /// </summary>
        public string BaggageCode
        {
            get { return baggageCode; }
            set { baggageCode = value; }
        }

        /// <summary>
        /// BaggageCode Type for FlyDubai (BAGB,BAGL etc)
        /// </summary>
        public string BaggageType
        {
            get { return baggageType; }
            set { baggageType = value; }
        }
        /// <summary>
        /// Category/SSRCategory Id for FlyDubai (99 for Baggage)
        /// </summary>
        public string CategoryId
        {
            get { return categoryId; }
            set { categoryId = value; }
        }

        public List<decimal> FlyDubaiBaggageCharge
        {
            get { return baggageCharge; }
            set { baggageCharge = value; }
        }

        /// <summary>
        /// Used to store TBO Specific Currency values (INR) to be used while booking.
        /// </summary>
        public PriceAccounts TBOPrice
        {
            get { return tboPrice; }
            set { tboPrice = value; }
        }
        /// <summary>
        /// Flex Details
        /// </summary>
        public List<FlightFlexDetails> FlexDetailsList
        {
            get { return flexDetailsList; }
            set { flexDetailsList = value; }
        }

        //Added by Lokesh on 25-sept-2017 regarding pax destination phone for booking tickets     
        public string DestinationPhone
        {
            get { return destinationPhone; }
            set { destinationPhone = value; }
        }

        //Added by Lokesh on 27/03/2018 For G9 Source only for lead Pax 
        //if the customer is travelling from India and provides any GST info (StateCode and Tax RegNo)

        public string GSTStateCode
        {
            get { return gstStateCode; }
            set { gstStateCode = value; }
        }

        public string  GSTTaxRegNo
        {
            get { return gstTaxRegNo; }
            set { gstTaxRegNo = value; }
        }

        /// <summary>
        /// Mandatory for FraudLabs. Saved for audit checking for FraudLabs (B2C)
        /// </summary>
        public string StateCode
        {
            get { return stateCode; }
            set { stateCode = value; }
        }

        /// <summary>
        /// Saving TaxBreakup for LCC Airlines after booking. TaxBreakup will be read from Ticket response.
        /// </summary>
        public List<KeyValuePair<string, decimal>> TaxBreakup
        {
            get { return taxBreakup; }
            set { taxBreakup = value; }
        }

        public string MealType
        {
            get { return mealType; }
            set { mealType = value; }
        }
        public string MealDesc
        {
            get { return mealDesc; }
            set { mealDesc = value; }
        }
        public string SeatInfo
        {
            get { return sSeatInfo; }
            set { sSeatInfo = value; }
        }
        public string CorpProfileId
        {
            get { return _CorpProfileId; }
            set { _CorpProfileId = value; }
        }

        public List<PaxSeatInfo> liPaxSeatInfo { get { return _liPaxSeatInfo; } set { _liPaxSeatInfo = value; } }
        #endregion

        /// <summary>
        /// Saves the information contained in Passenger object to flight passenger database
        /// </summary>
        public void Save()
        {
            bool bUpdate = paxId > 0;
            //Trace.TraceInformation("FlightPassenger.Save entered firstName = " + firstName + ", flightId = " + flightId);
            if (lastName == null || lastName.Length < 1)
            {
                throw new ArgumentException("Last name should be atleast 2 character long", "lastName");
            }
            if (flightId == 0)
            {
                throw new ArgumentException("Flight Id must have a value", "flightId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("created by must have positive integer value", "createdBy");
            }
            //Trace.TraceInformation("FlightPassenger.Save : compulsory parameters verified");
            SqlParameter[] paramList = new SqlParameter[30];
            
            paramList[0] = new SqlParameter("@firstName", firstName);
            paramList[1] = new SqlParameter("@lastName", lastName);
            paramList[2] = new SqlParameter("@title", title);
            paramList[3] = new SqlParameter("@flightId", flightId);
            paramList[4] = new SqlParameter("@cellPhone", cellPhone);
            paramList[5] = new SqlParameter("@leadPax", isLeadPax);
            if (dateOfBirth != DateTime.MinValue)
            {
                paramList[6] = new SqlParameter("@dateOfBirth", dateOfBirth);
            }
            else
            {
                paramList[6] = new SqlParameter("@dateOfBirth", DBNull.Value);
            }
            paramList[7] = new SqlParameter("@paxType", GetPTC(type));
            paramList[8] = new SqlParameter("@passportNumber", passportNo);
            if (country != null)
            {
                paramList[9] = new SqlParameter("@countryCode", country.CountryCode);
            }
            else
            {
                paramList[9] = new SqlParameter("@countryCode", DBNull.Value);
            }
            paramList[10] = new SqlParameter("@line1", addressLine1);
            paramList[11] = new SqlParameter("@line2", addressLine2);
            paramList[12] = new SqlParameter("@email", email);
            paramList[13] = new SqlParameter("@mealCode", meal.Code);
            //paramList[14] = new SqlParameter("@seatCode", seat.Code);


            if (liPaxSeatInfo != null && liPaxSeatInfo.Count > 0)
            {
                string sSeatNo = string.Empty;
                liPaxSeatInfo.ForEach(x => {
                    if (!string.IsNullOrEmpty(x.SeatNo) && x.SeatNo != "NoSeat")
                        sSeatNo = string.IsNullOrEmpty(sSeatNo) ? x.SeatNo : sSeatNo + "|" + x.SeatNo;
                    else
                        sSeatNo = string.IsNullOrEmpty(sSeatNo) ? "NS" : sSeatNo + "|" + "NS";
                });
                paramList[14] = new SqlParameter("@seatCode", sSeatNo);
            }
            else
                paramList[14] = new SqlParameter("@seatCode", DBNull.Value);
            paramList[15] = new SqlParameter("@priceId", price.PriceId);
            paramList[16] = new SqlParameter("@ffAirline", ffAirline);
            paramList[17] = new SqlParameter("@ffNumber", ffNumber);
            if (gender != Gender.Null)
            {
                paramList[18] = new SqlParameter("@gender", (int)gender);
            }
            else
            {
                paramList[18] = new SqlParameter("@gender", DBNull.Value);
            }
            paramList[19] = new SqlParameter("@createdBy", createdBy);
            if (city != null)
            {
                paramList[20] = new SqlParameter("@city", city);
            }
            else
            {
                paramList[20] = new SqlParameter("@city", DBNull.Value);
            }
            if (nationality != null)
            {
                paramList[21] = new SqlParameter("@nationality", nationality.CountryCode);
            }
            else
            {
                paramList[21] = new SqlParameter("@nationality", DBNull.Value);
            }
            if (baggageCode != null)
            {
                paramList[22] = new SqlParameter("@baggageCode", baggageCode);
            }
            else
            {
                paramList[22] = new SqlParameter("@baggageCode", DBNull.Value);
            }

            paramList[23] = new SqlParameter("@paxId", paxId);
            paramList[23].Direction = bUpdate ? paramList[23].Direction : ParameterDirection.Output;

            //Added by lokesh on 25-sept-2017 Regarding Pax Destination Phone.
            if (!string.IsNullOrEmpty(destinationPhone))
            {
                paramList[24] = new SqlParameter("@destinationPhone", destinationPhone);
            }
            else
            {
                paramList[24] = new SqlParameter("@destinationPhone", DBNull.Value);
            }

            //Added by Lokesh on 27/03/2018 For G9 Source only for lead Pax 
            //if the customer is travelling from India and provides any GST info (StateCode and Tax RegNo)

            if (!string.IsNullOrEmpty(gstTaxRegNo))
            {
                paramList[25] = new SqlParameter("@gsttaxregno", gstTaxRegNo);
            }
            else
            {
                paramList[25] = new SqlParameter("@gsttaxregno", DBNull.Value);
            }

            if (!string.IsNullOrEmpty(gstStateCode))
            {
                paramList[26] = new SqlParameter("@gststatecode", gstStateCode);
            }
            else
            {
                paramList[26] = new SqlParameter("@gststatecode", DBNull.Value);
            }
            if(!string.IsNullOrEmpty(stateCode))
            {
                paramList[27] = new SqlParameter("@stateCode", stateCode);
            }
            else
            {
                paramList[27] = new SqlParameter("@stateCode", DBNull.Value);
            }

            if (!string.IsNullOrEmpty(mealDesc) && mealDesc.Length >0)
            {
                paramList[28] = new SqlParameter("@mealDesc", mealDesc);
            }
            else
            {
                paramList[28] = new SqlParameter("@mealDesc", DBNull.Value);
            }

            if (!string.IsNullOrEmpty(_CorpProfileId))
            {
                paramList[29] = new SqlParameter("@CorpProfileId", _CorpProfileId);                
            }
            else
            {
                paramList[29] = new SqlParameter("@CorpProfileId", DBNull.Value);                
            }
            paramList[29].Direction = ParameterDirection.InputOutput;
            paramList[29].Size = 10;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddFlightPassenger, paramList);
            paxId = (int)paramList[23].Value;
            if (!string.IsNullOrEmpty(paramList[29].Value.ToString()) && paramList[29].Value != DBNull.Value)
                _CorpProfileId = Convert.ToString(paramList[29].Value);
            else
                Audit.Add(EventType.Book, Severity.High, 1, "(Corp)Failed to retrieve profile id for flight passenger email: " + email, "");
            
            //Trace.TraceInformation("FlightPassenger.Save exiting : paxId = " + paxId);

            //Flex details Saving only lead Pax
            if (!bUpdate && flexDetailsList != null && flexDetailsList.Count > 0)
            {
                foreach (FlightFlexDetails flexDet in flexDetailsList)
                {
                    flexDet.PaxId = paxId;
                    flexDet.CreatedBy = createdBy;
                    flexDet.Save();
                }
            }
        }
        /// <summary>
        /// Method to load the passenger details based in the paxId
        /// </summary>
        /// <param name="paxId"></param>
        public void Load(int paxId)
        {
            //Trace.TraceInformation("FlightPassenger.Load entered : paxId= " + paxId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            //SqlConnection connection = DBGateway.GetConnection();
            //SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.LoadPassenger, paramList,connection);
            DataTable dtPax = DBGateway.FillDataTableSP(SPNames.LoadPassenger, paramList);
            //while (dr.Read())
            if(dtPax != null)
            {
                foreach (DataRow dr in dtPax.Rows)
                {
                    addressLine1 = dr["line1"].ToString();
                    if (dr["line2"] != DBNull.Value)
                    {
                        addressLine2 = dr["line2"].ToString();
                    }

                    if (dr["city"] != DBNull.Value)
                    {
                        city = dr["city"].ToString();
                    }
                    nationality = new Country();
                    if (dr["nationality"] != DBNull.Value)
                    {
                        nationality.CountryCode = dr["nationality"].ToString();
                    }

                    if (dr["cellPhone"] != DBNull.Value)
                    {
                        cellPhone = dr["cellPhone"].ToString();
                    }
                    if (dr["dateOfBirth"] != DBNull.Value)
                    {
                        dateOfBirth = Convert.ToDateTime(dr["dateOfBirth"]);
                    }
                    if (dr["gender"] != DBNull.Value)
                    {
                        gender = (Gender)Enum.Parse(typeof(Gender), Convert.ToString(dr["gender"]));
                    }
                    email = dr["email"].ToString();
                    if (dr["FFAirlineCode"] != DBNull.Value)
                    {
                        ffAirline = dr["FFAirlineCode"].ToString();
                    }
                    if (dr["FFNumber"] != DBNull.Value)
                    {
                        ffNumber = dr["FFNumber"].ToString();
                    }
                    firstName = dr["firstName"].ToString();
                    flightId = Convert.ToInt32(dr["flightId"]);
                    isLeadPax = Convert.ToBoolean(dr["leadPax"]);
                    lastName = dr["lastName"].ToString();
                    type = GetPassengerType(dr["paxType"].ToString());
                    if (dr["title"] != DBNull.Value)
                    {
                        title = dr["title"].ToString();
                    }
                    if (dr["seatCode"].ToString().Length != 0)
                    {
                        seat = Seat.GetSeat(dr["seatCode"].ToString());
                    }
                    if (dr["mealCode"].ToString().Length != 0)
                    {
                        meal = Meal.GetMeal(dr["mealCode"].ToString());
                    }
                    price = new PriceAccounts();
                    price.Load(Convert.ToInt32(dr["priceId"]));
                    paxId = Convert.ToInt32(dr["PaxId"]);
                    if (dr["passportNumber"] != DBNull.Value)
                    {
                        passportNo = dr["passportNumber"].ToString();
                    }
                    country = new Country();
                    if (dr["countryCode"] != DBNull.Value)
                    {
                        country.CountryCode = dr["countryCode"].ToString();
                    }
                    createdBy = Convert.ToInt32(dr["createdBy"]);
                    createdOn = Convert.ToDateTime(dr["createdOn"]);
                    lastModifiedBy = Convert.ToInt32(dr["lastModifiedBy"]);
                    lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
                    if (dr["baggageCode"] != DBNull.Value)
                    {
                        baggageCode = dr["baggageCode"].ToString();
                    }
                    else
                    {
                        baggageCode = "";
                    }

                    //Added by Lokesh on 27/03/2018 For G9 Source only for lead Pax 
                    //if the customer is travelling from India and provides any GST info (StateCode and Tax RegNo)

                    if (dr["GSTStateCode"] != DBNull.Value)
                    {
                        gstStateCode = Convert.ToString(dr["GSTStateCode"]);
                    }
                    if (dr["GSTTaxRegNo"] != DBNull.Value)
                    {
                        gstTaxRegNo = Convert.ToString(dr["GSTTaxRegNo"]);
                    }
                    else
                    {
                        gstTaxRegNo = string.Empty;
                    }
                    if(dr["stateCode"] != DBNull.Value)
                    {
                        stateCode = Convert.ToString(dr["stateCode"]);
                    }
                    else
                    {
                        stateCode = string.Empty;
                    }
                    if (dr["CorpProfileId"] != DBNull.Value)
                    {
                        _CorpProfileId = Convert.ToString(dr["CorpProfileId"]);
                    }
                    else
                    {
                        _CorpProfileId = string.Empty;
                    }
                    flexDetailsList = FlightFlexDetails.GetFlightPaxDetails(paxId,1);
                }   
            }
            //dr.Close();
            //connection.Close();
        }

        /// <summary>
        /// Gets the passenger type code for a given PassengerType
        /// </summary>
        /// <param name="type">PassengerType for which PTC is needed</param>
        /// <returns>Passenger type code as string</returns>
        public static string GetPTC(PassengerType type)
        {
            switch (type)
            {
                case PassengerType.Adult:
                    return "ADT";
                case PassengerType.Child:
                    return "CNN";
                case PassengerType.Infant:
                    return "INF";
                case PassengerType.Senior:
                    return "SNN";
                default:
                    return string.Empty;
            }
        }
        /// <summary>
        /// Gets the passenger type code for a given PassengerType string
        /// </summary>
        /// <param name="type">PassengerType string for which PTC is needed</param>
        /// <returns>Passenger type code as PassengerType</returns>
        public static PassengerType GetPassengerType(string passengerType)
        {
            switch (passengerType)
            {
                case "ADT":
                    return PassengerType.Adult;
                case "CNN":
                    return PassengerType.Child;
                case "INF":
                    return PassengerType.Infant;
                case "SNN":
                    return PassengerType.Senior;
                default:
                    return PassengerType.Adult;
            }
        }

        /// <summary>
        /// Gets the list of all meal preferences with its mealCode
        /// </summary>
        /// <returns>SortedList with mealDescription as key and mealCode as value</returns>
        //public static List<KeyValuePair<string, string>> GetMealPreferenceList(BookingSource source)
        //{
        //    Trace.TraceInformation("FlightPassenger.GetMealPreferenceList entered");
        //    List<KeyValuePair<string, string>> mealPreferenceList = new List<KeyValuePair<string, string>>();
        //    // checks whether Cache contains req. details.if it contains  data retrive from the Cache.
        //    if (CacheData.CheckGetMealPreferenceList())
        //    {
        //        foreach (int gdsSource in CacheData.MealList.Keys)
        //        {
        //            if (gdsSource == (int)source)
        //            {
        //                //mealPreferenceList = CacheData.MealList[gdsSource];
        //                foreach (KeyValuePair<string, string> mealtype in CacheData.MealList[gdsSource])
        //                {
        //                    mealPreferenceList.Add(mealtype);
        //                }
        //            }
        //        }
        //    }
        //    // If it doesnt contain in Cache retrieve data from the Database.
        //    else
        //    {
        //        SqlParameter[] paramList = new SqlParameter[0];
        //        SqlConnection connection = DBGateway.GetConnection();
        //        SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetMealPreferenceList, paramList);
        //        while (dataReader.Read())
        //        {
        //            if (Convert.ToInt16(dataReader["source"]) == (int)source)
        //            {
        //                mealPreferenceList.Add(new KeyValuePair<string, string>((string)dataReader["mealCode"], (string)dataReader["mealDescription"]));

        //            }//Update the Cache.
        //            if (!CacheData.MealList.ContainsKey(Convert.ToInt16(dataReader["source"])))
        //            {
        //                Dictionary<string, string> meal = new Dictionary<string, string>();
        //                meal.Add((string)dataReader["mealCode"], (string)dataReader["mealDescription"]);
        //                CacheData.MealList.Add(Convert.ToInt16(dataReader["source"]), meal);
        //            }
        //            else
        //            {
        //                CacheData.MealList[Convert.ToInt16(dataReader["source"])].Add((string)dataReader["mealCode"], (string)dataReader["mealDescription"]);
        //            }
        //        }
        //        dataReader.Close();
        //        connection.Close();
        //    }
        //    Trace.TraceInformation("FlightPassenger.GetMealPreferenceList exiting : mealPreferenceList.Count = " + mealPreferenceList.Count);
        //    return mealPreferenceList;
        //}

        public static SortedList GetSeatPreferenceList()
        {
            //TODO: make static properties which get loaded only once
            //Trace.TraceInformation("FlightPasenger.GetSeatPreferenceList entered");
            SortedList seatPreferenceList = new SortedList();
            // checks whether Cache contains req. details.if it contains  data retrive from the Cache.
            if (CacheData.CheckGetSeatPreferenceList())
            {
                foreach (string seatName in CacheData.SeatList.Keys)
                {

                    seatPreferenceList.Add(seatName, CacheData.SeatList[seatName]);
                }
            }
            // If it doesnt contain in Cache retrieve data from the Database.
            else
            {
                SqlParameter[] paramList = new SqlParameter[0];
                SqlConnection connection = DBGateway.GetConnection();
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetSeatPreferenceList, paramList,connection);
                while (dataReader.Read())
                {
                    seatPreferenceList.Add((string)dataReader["seatDescription"], (string)dataReader["seatCode"]);
                    //Update the Cache.
                    CacheData.SeatList.Add((string)dataReader["seatDescription"], (string)dataReader["seatCode"]);
                }
                dataReader.Close();
                connection.Close();
            }
            //Trace.TraceInformation("FlightPasenger.GetSeatPreferenceList exiting : seatPreferenceList.Count = " + seatPreferenceList.Count);
            return seatPreferenceList;
        }
        /// <summary>
        /// Method gets the all info about passenger corresponding to a flightId
        /// </summary>
        /// <param name="flightId">flightId</param>
        /// <returns>FlightPassenger[] for a particular flightId</returns>
        public static FlightPassenger[] GetPassengers(int flightId)
        {
            //Trace.TraceInformation("FlightPassenger.GetPassengers entered : flightId = " + flightId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            DataSet dataSet = DBGateway.FillSP(SPNames.GetPassengersInfo, paramList);
            FlightPassenger[] passengers = new FlightPassenger[dataSet.Tables[0].Rows.Count];
            int count = 0;
            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                passengers[count] = new FlightPassenger();
                if (dr["city"] != DBNull.Value)
                {
                    passengers[count].city = dr["city"].ToString();
                }
                passengers[count].nationality = new Country();
                if (dr["nationality"] != DBNull.Value)
                {
                    passengers[count].nationality.CountryCode  = dr["nationality"].ToString();
                }
                passengers[count].addressLine1 = dr["line1"].ToString();
                if (dr["line2"] != DBNull.Value)
                {
                    passengers[count].addressLine2 = dr["line2"].ToString();
                }
                if (dr["cellPhone"] != DBNull.Value)
                {
                    passengers[count].cellPhone = dr["cellPhone"].ToString();
                }
                if (dr["dateOfBirth"] != DBNull.Value)
                {
                    passengers[count].dateOfBirth = Convert.ToDateTime(dr["dateOfBirth"]);
                }
                if (dr["gender"] != DBNull.Value)
                {
                    passengers[count].gender = (Gender)Enum.Parse(typeof(Gender), Convert.ToString(dr["gender"]));
                }
                passengers[count].email = dr["email"].ToString();
                if (dr["FFAirlineCode"] != DBNull.Value)
                {
                    passengers[count].ffAirline = dr["FFAirlineCode"].ToString();
                }
                if (dr["FFNumber"] != DBNull.Value)
                {
                    passengers[count].ffNumber = dr["FFNumber"].ToString();
                }
                passengers[count].firstName = dr["firstName"].ToString();
                passengers[count].flightId = flightId;
                passengers[count].isLeadPax = Convert.ToBoolean(dr["leadPax"]);
                passengers[count].lastName = dr["lastName"].ToString();
                passengers[count].type = GetPassengerType(dr["paxType"].ToString());
                if (dr["title"] != DBNull.Value)
                {
                    passengers[count].Title = dr["title"].ToString();
                }
                if (dr["seatCode"].ToString().Length != 0)
                {
                    passengers[count].seat = Seat.GetSeat(dr["seatCode"].ToString());
                }
                if (dr["mealCode"].ToString().Length != 0)
                {
                    passengers[count].meal = Meal.GetMeal(dr["mealCode"].ToString());
                }
                passengers[count].Price = new PriceAccounts();
                passengers[count].Price.Load(Convert.ToInt32(dr["priceId"]));
                passengers[count].paxId = Convert.ToInt32(dr["PaxId"]);
                if (dr["passportNumber"] != DBNull.Value)
                {
                    passengers[count].passportNo = dr["passportNumber"].ToString();
                }
                passengers[count].country = new Country();
                if (dr["countryCode"] != DBNull.Value)
                {
                    passengers[count].country.CountryCode = dr["countryCode"].ToString();
                }
                passengers[count].createdBy = Convert.ToInt32(dr["createdBy"]);
                passengers[count].createdOn = Convert.ToDateTime(dr["createdOn"]);
                passengers[count].lastModifiedBy = Convert.ToInt32(dr["lastModifiedBy"]);
                passengers[count].lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
                if (dr["baggageCode"] != DBNull.Value)
                {
                    passengers[count].baggageCode = dr["baggageCode"].ToString();
                }
                else
                {
                    passengers[count].baggageCode = "";
                }
                if (dr["mealDesc"] != DBNull.Value)
                {
                    passengers[count].MealDesc = dr["mealDesc"].ToString();
                }
                else
                {
                    passengers[count].MealDesc = string.Empty;
                }
                passengers[count].FlexDetailsList = FlightFlexDetails.GetFlightPaxDetails(passengers[count].paxId,1);
                count++;
            }
            //Trace.TraceInformation("Passenger.GetPassengers exiting : Rows=" + dataSet.Tables[0].Rows.Count);
            return passengers;
        }
        public static string GetMealCode(int paxId)
        {
            string mealCode = string.Empty;
            //Trace.TraceInformation("Passenger.GetMealCode entered : paxId = " + paxId);
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetMealCode, paramList,connection);
            using (DataTable dtMeal = DBGateway.FillDataTableSP(SPNames.GetMealCode, paramList))
            {
                if (dtMeal != null && dtMeal.Rows.Count > 0)
                {
                    DataRow dataReader = dtMeal.Rows[0];
                    if (dataReader !=null)
                    {
                        if (dataReader["mealCode"] != DBNull.Value)
                        {
                            mealCode =Convert.ToString(dataReader["mealCode"]);
                        }
                    }
                }
            }
            //dataReader.Close();
            //connection.Close();
            return mealCode;
        }

        public static FlightPassenger[] GetDummyPaxList(int adult, int child, int infant, int senior)
        {
            //Trace.TraceInformation("Passenger.GettDummyPaxList entered.");
            int[] paxCount = { adult, child, infant, senior };
            FlightPassenger[] paxList = new FlightPassenger[adult + child + infant + senior];
            for (int i = 0, k = 0; i < 4; i++)
            {
                for (int j = 0; j < paxCount[i]; j++, k++)
                {
                    paxList[k] = new FlightPassenger();
                    PassengerType type = (PassengerType)(i + 1);
                    paxList[k].type = type;
                    paxList[k].title = string.Empty;
                    paxList[k].firstName = GetPTC(type) + Convert.ToChar(j + 65);
                    paxList[k].lastName = "TEST";
                    if (type == PassengerType.Infant)
                    {
                        paxList[k].dateOfBirth = DateTime.Now.AddYears(-1);
                    }
                }
            }
            return paxList;
        }

        public static string GetPaxFullName(int paxId)
        {
            //Trace.TraceInformation("FlightItinerary.GetPaxFullName entered : paxId = " + paxId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            StringBuilder fullName = new StringBuilder();
            //SqlConnection connection = DBGateway.GetConnection();
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetPaxFullName, paramList,connection);
            using (DataTable dtPax = DBGateway.FillDataTableSP(SPNames.GetPaxFullName, paramList))
            {
                if (dtPax != null)
                {
                    DataRow data = dtPax.Rows[0];
                    if (data != null)
                    {
                        if (data["title"] != DBNull.Value)
                        {
                            fullName.Append(Convert.ToString(data["title"]));
                            fullName.Append(" ");
                        }
                        if (data["firstName"] != DBNull.Value)
                        {
                            fullName.Append(Convert.ToString(data["firstName"]));
                            fullName.Append(" ");
                        }
                        if (data["lastName"] != DBNull.Value)
                        {
                            fullName.Append(Convert.ToString(data["lastName"]));
                        }
                    }
                }
            }
            return fullName.ToString().Trim();
        }

        public override string ToString()
        {
            StringBuilder paxString = new StringBuilder(100);
            paxString.AppendFormat("{0} ", paxId);
            paxString.Append(title);
            paxString.AppendFormat(" {0} {1}", firstName, lastName);
            paxString.AppendFormat(" {0}", type.ToString());
            if (gender != Gender.Null)
            {
                paxString.AppendFormat(" {0}", gender.ToString());
            }
            if (!string.IsNullOrEmpty(email))
            {
                paxString.AppendFormat(" {0}", email);
            }
            if (!string.IsNullOrEmpty(addressLine1))
            {
                paxString.AppendFormat(" {0}", addressLine1);
            }
            if (!string.IsNullOrEmpty(addressLine2))
            {
                paxString.AppendFormat(" {0}", addressLine2);
            }
            return paxString.ToString();
        }
        public static DataSet GetAgentProfilePassengers(string whereString, int agentId)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@Filters", whereString);
                paramList[1] = new SqlParameter("@agentId", agentId);
                 dataSet = DBGateway.FillSP("usp_GetAgentProfilePassengerDetails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, 0, "Not able to Get SearchPassengerDetails by whereString" + " .Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
            }
            return dataSet;
        }
    }

    [Serializable]
    public class FlightFlexDetails
    {
        #region variables
        int _detailId;
        int _flexId;
        int _paxId;
        string _flexLabel;
        string _flexData;
        int _createdBy;
        int productId;
        string _flexGDSprefix;
        string _sIsOffline;
        #endregion
        #region properities
        public int DetailsId
        {
            get { return _detailId; }
            set { _detailId = value; }
        }
        public int FlexId
        {
            get { return _flexId; }
            set { _flexId = value; }
        }
        public int PaxId
        {
            get { return _paxId; }
            set { _paxId = value; }
        }
        public string FlexLabel
        {
            get { return _flexLabel; }
            set { _flexLabel = value; }
        }
        public string FlexData
        {
            get { return _flexData; }
            set { _flexData = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ProductID
        {
            get { return productId; }
            set { productId = value; }
        }
        public string FlexGDSprefix
        {
            get { return _flexGDSprefix; }
            set { _flexGDSprefix = value; }
        }
        public string sIsOffline { get => _sIsOffline; set => _sIsOffline = value; }
        #endregion
        /// <summary>
        /// Saving Flex Details
        /// </summary>
        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_FlexId", _flexId);
                paramList[1] = new SqlParameter("@P_PaxId", _paxId);
                paramList[2] = new SqlParameter("@P_FlexLabel", string.IsNullOrEmpty(_flexLabel) ? string.Empty : _flexLabel);
                paramList[3] = new SqlParameter("@P_FlexData", string.IsNullOrEmpty(_flexData) ? string.Empty : _flexData);
                paramList[4] = new SqlParameter("@P_CreatedBy", _createdBy);
                paramList[5] = new SqlParameter("@P_ProductId", productId);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(sIsOffline == "OE" ? SPNames.AddOfflineEntryFlexDetails : sIsOffline == "Y" ? SPNames.AddfflineFlexDetails : SPNames.AddFlightFlexDetails, paramList);
                //Audit.Add(EventType.FleetBooking, Severity.Low, _createdBy, "Saving Flex Details", "0");
            }
            catch(Exception ex)
            {
                //Audit.Add(EventType.FleetBooking, Severity.Low, _createdBy, "Error in Saving FlexPAXDetails Save method"+ex.ToString(), "0");
            }
        }

        public static List<FlightFlexDetails> GetFlightPaxDetails(int paxId, int productId)
        {
            List<FlightFlexDetails> flexDetails = new List<FlightFlexDetails>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@PaxId", paxId);
                paramList[1] = new SqlParameter("@ProductId", productId);
                DataTable dtFlex = DBGateway.FillDataTableSP("usp_GetFlightFlexDetails", paramList);

                foreach(DataRow row in dtFlex.Rows)
                {
                    FlightFlexDetails flightFlex = new FlightFlexDetails();
                    flightFlex.DetailsId =Convert.ToInt32 (row["DetailId"]);
                    flightFlex.FlexId= Convert.ToInt32(row["FlexId"]);
                    flightFlex.PaxId= Convert.ToInt32(row["PaxId"]);
                    flightFlex.FlexLabel = Convert.ToString(row["FlexLabel"]);
                    flightFlex.FlexData = Convert.ToString(row["FlexData"]);
                    flightFlex.CreatedBy = Convert.ToInt32(row["FlexCreatedBy"]);
                    flexDetails.Add(flightFlex);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, 0, "Not able to Get GetFlightPaxDetails" + " .Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
            }
            return flexDetails;
        }

        /// <summary>
        /// To get default flex details based on agent and product id
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static List<FlightFlexDetails> GetDefaultFlex(int agentId, int productId)
        {
            List<FlightFlexDetails> flexDetails = new List<FlightFlexDetails>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@AgentId", agentId);
                paramList[1] = new SqlParameter("@ProductId", productId);
                DataTable dtFlex = DBGateway.FillDataTableSP("usp_GetDefaultFlex", paramList);

                foreach (DataRow row in dtFlex.Rows)
                {
                    FlightFlexDetails flightFlex = new FlightFlexDetails();
                    flightFlex.FlexId = Convert.ToInt32(row["flexId"]);
                    flightFlex.FlexLabel = Convert.ToString(row["flexLabel"]);
                    flightFlex.FlexGDSprefix = Convert.ToString(row["flexGDSprefix"]);
                    flightFlex.ProductID = Convert.ToInt32(row["FlexProductId"]);
                    flexDetails.Add(flightFlex);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, 0, "Not able to Get GetFlightPaxDetails" + " .Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
            }
            return flexDetails;
        }
    }

    [Serializable]
    public class PaxSeatInfo
    {
        #region variables
        int _sPaxNo { get; set; }
        string _sSegment { get; set; }
        string _sSeatNo { get; set; }
        decimal _dcPrice { get; set; }
        string _sSeatStatus { get; set; }
        #endregion

        #region properities
        public int PaxNo { get { return _sPaxNo; } set { _sPaxNo = value; } }
        public string Segment { get { return _sSegment; } set { _sSegment = value; } }
        public string SeatNo { get { return _sSeatNo; } set { _sSeatNo = value; } }
        public decimal Price { get { return _dcPrice; } set { _dcPrice = value; } }
        public string SeatStatus { get { return _sSeatStatus; } set { _sSeatStatus = value; } }
        #endregion
    }
}
