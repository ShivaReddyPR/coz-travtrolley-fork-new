using System;
using System.Collections.Generic;

namespace CT.BookingEngine
{
    public enum SearchType
    {
        NoType=0,
        OneWay = 1,
        Return = 2,
        MultiWay = 3
    }
    [Serializable]
    public class SearchRequest
    {
        SearchType type;
        bool? isDomestic = null;
        bool restrictAirline;
        FlightSegment[] segments;
        int adultCount;
        int childCount;
        int seniorCount;
        int infantCount;
        private List<string> sources;

        bool refundableFares = false;
        string maxStops = string.Empty;
        // For Unrestricted carrier
        string unrestrictedcarrier;
        string corporateTravelReason;
        int corporateTravelReasonId;
        int corporateTravelProfileId;
        /// <summary>
        /// If Travel Policy ActionStatus="P then its True otherwise false
        /// </summary>
        bool appliedPolicy;
        bool searchBySegments = false;
        bool timeIntervalSpecified;

        public string Unrestrictedcarrier
        {
            get
            {
                return unrestrictedcarrier;
            }
            set
            {
                unrestrictedcarrier = value;
            }
        }

        public SearchType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
        public bool RestrictAirline
        {
            get
            {
                return restrictAirline;
            }
            set
            {
                restrictAirline = value;
            }
        }
        public bool IsDomestic
        {
            get
            {
                if (!isDomestic.HasValue)
                {
                    Airport Origin = new Airport(Segments[0].Origin);
                    Airport Destination = new Airport(Segments[0].Destination);
                    if (Origin.CountryCode != Destination.CountryCode || Origin.CountryCode != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "")
                    {
                        isDomestic = false;
                    }
                    else
                    {
                        isDomestic = true;
                    }
                }
                return isDomestic.Value;
            }
        }

        public FlightSegment[] Segments
        {
            get
            {
                return segments;
            }
            set
            {
                segments = value;
            }
        }

        public int AdultCount
        {
            get
            {
                return adultCount;
            }
            set
            {
                adultCount = value;
            }
        }

        public int ChildCount
        {
            get
            {
                return childCount;
            }
            set
            {
                childCount = value;
            }
        }

        public int SeniorCount
        {
            get
            {
                return seniorCount;
            }
            set
            {
                seniorCount = value;
            }
        }

        public int InfantCount
        {
            get
            {
                return infantCount;
            }
            set
            {
                infantCount = value;
            }
        }

        /// <summary>
        /// List of GDS Sources
        /// </summary>
        public List<string> Sources
        {
            get
            {
                return sources;
            }
            set
            {
                sources = value;
            }
        }
        public bool RefundableFares
        {
            get
            {
                return refundableFares;
            }
            set
            {
                refundableFares = value;
            }
        }

        public string MaxStops
        {
            get
            {
                return maxStops;
            }
            set
            {
                maxStops = value;
            }
        }

        public int CorporateTravelProfileId
        {
            get
            {
                return corporateTravelProfileId;
            }
            set
            {
                corporateTravelProfileId = value;
            }
        }

        public int CorporateTravelReasonId
        {
            get
            {
                return corporateTravelReasonId;
            }
            set
            {
                corporateTravelReasonId= value;
            }
        }

        public string CorporateTravelReason
        {
            get
            {
                return corporateTravelReason;
            }
            set
            {
                corporateTravelReason = value;
            }
        }

        /// <summary>
        /// If Travel Policy ActionStatus="P then its True otherwise false
        /// </summary>
        public bool AppliedPolicy
        {
            get { return appliedPolicy; }
            set { appliedPolicy = value; }
        }

        public bool SearchBySegments
        {
            get { return searchBySegments; }
            set { searchBySegments = value; }
        }


        public bool TimeIntervalSpecified { get => timeIntervalSpecified; set => timeIntervalSpecified = value; }

        /// <summary>
        /// Get the dummy SearchRequest Object
        /// </summary>
        /// <param name="airline">airline source</param>
        /// <param name="origin">origin city</param>
        /// <param name="destination">destination city</param>
        /// <returns></returns>
        public static SearchRequest GetDummySearchRequest(string airline, string origin, string destination, DateTime date)
        {
            SearchRequest req = new SearchRequest();
            req.Segments = new FlightSegment[1];
            FlightSegment fs = new FlightSegment();
            fs.Origin = origin;
            fs.Destination = destination;
            fs.PreferredAirlines = new string[0];
            fs.PreferredDepartureTime = date;
            req.AdultCount = 1;
            req.ChildCount = 0;
            req.InfantCount = 0;
            req.SeniorCount = 0;
            req.Type = SearchType.OneWay;
            req.RestrictAirline = false;
            List<string> sources = new List<string>();
            sources.Add(airline);
            fs.flightCabinClass = CabinClass.Economy;
            req.Sources = sources;
            req.Segments[0] = fs;
            return req;
        }

        public SearchRequest Copy()
        {
            SearchRequest request = new SearchRequest();
            request.adultCount = adultCount;
            request.childCount = childCount;
            request.infantCount = infantCount;
            request.seniorCount = seniorCount;
            request.isDomestic = isDomestic;
            request.restrictAirline = restrictAirline;
            request.segments = new FlightSegment[segments.Length];
            for (int i = 0; i < segments.Length; i++)
            {
                request.segments[i] = segments[i].Copy();
            }
            if (sources != null)
            {
                request.sources = new List<string>(sources);
            }   // else nothing to assign. the value will remain null.
            request.type = type;
            request.unrestrictedcarrier = unrestrictedcarrier;
            request.refundableFares = refundableFares;
            request.maxStops = maxStops;
            request.searchBySegments = searchBySegments;
            return request;
        }
    }
}
