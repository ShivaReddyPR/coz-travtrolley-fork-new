﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Data;
using System.Data.SqlClient;

namespace CT.BookingEngine
{
    public class FraudLabPaymentsList
    {
        #region members
        #endregion
        #region properties
        #endregion
        #region Constructors
        #endregion
        #region static Methods
        public static DataTable GetFraudLabList(string agentd,string productid,string status,DateTime fromdate,DateTime todate,string pgsource)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                if(!string.IsNullOrEmpty(agentd))
                {
                    paramList[0] = new SqlParameter("@P_AGENTID", agentd);
                }
                if (!string.IsNullOrEmpty(productid))
                {
                    paramList[1] = new SqlParameter("@P_PRODUCTID", productid);
                }
                if (!string.IsNullOrEmpty(status))
                {
                    paramList[2] = new SqlParameter("@P_STATUS", status);
                }
                    paramList[3] = new SqlParameter("@P_FROMDATE", fromdate);              
                    paramList[4] = new SqlParameter("@P_TODATE", todate);
                if (!string.IsNullOrEmpty(pgsource))
                {
                    paramList[5] = new SqlParameter("@P_PGSOURCE", pgsource);
                }
                DataTable dtResult = DBGateway.FillDataTableSP("USP_GETFRAUDLABLIST", paramList);
                return dtResult;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public static void UpdateFraudLabStatus(long responseid,string status,string remarks,int modifiedBy)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_RESPONSE_ID", responseid);
                paramList[1] = new SqlParameter("@P_STATUS", status); 
                paramList[2] = new SqlParameter("@P_REMARKS", remarks);
                paramList[3] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[4] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[5].Direction = ParameterDirection.Output;
                
                DBGateway.ExecuteNonQuery("usp_UpdateFraudLabsStatus", paramList);
            }
            catch(Exception ex)
               {
                throw ex;
            }
        }

        public static DataTable GetFraudLabListHistory(long reponseid)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_responseid", reponseid);
                DataTable dtResult = DBGateway.FillDataTableSP("usp_GetFraudLabListHistory", paramList);
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
