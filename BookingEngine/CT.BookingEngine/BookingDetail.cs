using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public enum ProductType
    {
        //Null = 0,
        Flight = 1,
        Hotel = 2,
        Packages = 3,
        Activity = 4,
        Insurance = 5,
        SightSeeing = 6,
        Train = 7,
        Transfers = 9,
        SMSPack = 8,
        MobileRecharge = 10,
        FixedDeparture = 11,
        Visa=12,
        Car=13,
        ItineraryAddService = 14,
        BaggageInsurance=15,
        Forex=16

    }
    public enum BookingStatus
    {
        NotDone=0,
        Hold = 1,
        Ready = 2,
        Cancelled = 3,
        InProgress = 4,
        Ticketed = 5,
        VoidInProgress = 6,
        Void = 7,
        Delivered = 8,
        Inactive = 9,
        AmendmentInProgress = 10,
        AmendmentDone = 11,
        Released = 12,
        OutsidePurchase = 13,
        Booked = 14,
        CancellationInProgress = 15,
        Refunded = 16,
        ModificationInProgress=17,
        Modified = 18,
        RefundInProgress = 19,
        //TODO: In case we add one more status there can be an affect in GetBooking method of BookingAPI(ref: Please see TODO list there)
        
        CancellationDone = 20      //Newly added for Flight change request
    }

   

    public class BookingDetail
    {
        #region Private Members
        /// <summary>
        /// Unique identity number for a booking
        /// </summary>
        private int bookingId;
        /// <summary>
        /// Booking Status
        /// </summary>
        private BookingStatus status;
        /// <summary>
        /// Agency id of the agency who made the booking
        /// </summary>
        private int agencyId;
        /// <summary>
        /// memberId of the member who has locked the booking for  processing.
        /// </summary>
        private int lockedBy;
        /// <summary>
        /// Previous booking status.
        /// </summary>
        private BookingStatus prevStatus;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        ///BookingId of the parent booking.This would be used for linking the insurance booking ids with the booking ids of the flights.       
        /// </summary>
        int parentBookingId;
        /// <summary>
        /// New method for - Chnages for New product type
        /// </summary>
        Product[] productsList;
        #endregion

        #region Public Properties

        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }
        /// <summary>
        /// Gets or sets Booking status
        /// </summary>
        public BookingStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        /// <summary>
        /// Gets or sets the agency Id of the agency who made the booking
        /// </summary>
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }

        /// <summary>
        /// Gets or sets the memberId who has locked the booking
        /// </summary>
        public int LockedBy
        {
            get
            {
                return lockedBy;
            }
            set
            {
                lockedBy = value;
            }
        }

        /// <summary>
        /// Gets or set the previous booking status.
        /// </summary>
        public BookingStatus PrevStatus
        {
            get
            {
                return prevStatus;
            }
            set
            {
                prevStatus = value;
            }
        }

        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        /// <summary>
        ///BookingId of the parent booking.This would be used for linking the insurance booking ids with the booking ids of the flights.       
        /// </summary>
        public int ParentBookingId
        {
            get
            {
                return parentBookingId;
            }
            set
            {
                parentBookingId = value;
            }
        }
        /// <summary>
        ///  New method for - Chnages for New product type
        /// </summary>
        public Product[] ProductsList
        {
            get
            {
                return productsList;
            }
            set
            {
                productsList = value;
            }
        }
        #endregion

        #region Class Constructors
        public BookingDetail()
        { 
        }
        public BookingDetail(int bookingId)
        {
            Load(bookingId);
        }
        #endregion

        #region Methods
        public void Save()
        {
            //Trace.TraceInformation("BookingDetail.Save entered : AgencyId = " + agencyId + ", TotalSellingPrice = " );

            SqlParameter[] paramList = new SqlParameter[5];
            if ((int)status <= 0)
            {
                throw new ArgumentException("Status should have a value", "status");
            }            
            if (agencyId <= 0)
            {
                throw new ArgumentException("AgencyId must have a value", "agencyId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a value", "createdBy");
            }
            //Trace.TraceInformation(" Verification of compulsory values done");
            paramList[0] = new SqlParameter("@status", (int)status);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@createdBy", createdBy);
            if (parentBookingId > 0)
            {
                paramList[3] = new SqlParameter("@parentBookingId", parentBookingId);
            }
            else
            {
                paramList[3] = new SqlParameter("@parentBookingId", DBNull.Value);
            }
            paramList[4] = new SqlParameter("@bookingId", SqlDbType.Int);
            paramList[4].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddBookingDetail, paramList);
            bookingId = (int)paramList[4].Value;
            //Trace.TraceInformation("BookingDetail.Save exiting : bookingId = " + bookingId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="SaveType"> for checking if save is calling from review booking - first time (FALSE for that )or retrieve itinerary (TRUE for that )</param>
        public void Save(FlightItinerary itinerary, bool SaveTypeItinerary)
        {
            //TODO: add checks to verify that itinerary has valid information
            //TODO: add transaction here
            //Trace.TraceInformation("BookingDetail.Save(FlightItinerary) entered : PNR = " + itinerary.PNR);

            // check if already that PNR does not exist
            if (SaveTypeItinerary == true)
            {
                int flightId = CheckPNR(itinerary.PNR);
                if (flightId != 0)
                {
                    throw new ArgumentException("This PNR No (" + itinerary.PNR + ") already exist in database.");
                }
            }
            Save();
            itinerary.BookingId = bookingId;
            itinerary.AgencyId = agencyId;
            itinerary.save();
            SaveProductLine(bookingId, ProductType.Flight, itinerary.FlightId, itinerary.BookingMode);
            //Trace.TraceInformation("BookingDetail.Save(FlightItinerary) exiting : bookingId = " + bookingId);
        }

        // New method for - Chnages for New product type
        public void SaveBooking(bool SaveTypeItinerary)
        {
            //TODO: add checks to verify that itinerary has valid information
            //TODO: add transaction here
            //Trace.TraceInformation("BookingDetail.Save(FlightItinerary) entered.");

            // check if already that PNR does not exist
            foreach (Product product in ProductsList)
            {
                if (product.ProductType == ProductType.Flight)
                {
                    if (SaveTypeItinerary == true)
                    {
                        //FlightItinerary itinerary = (FlightItinerary)product; ziya-todo
                        //int flightId = CheckPNR(itinerary.PNR);
                        //if (flightId != 0)
                        //{
                        //    throw new ArgumentException("This PNR No (" + itinerary.PNR + ") already exist in database.");
                        //}
                    }
                }

                Save();
                //if (product.ProductType == ProductType.Flight) ziya-todo
                //{
                //    FlightItinerary itinerary = (FlightItinerary)product;
                //    itinerary.AgencyId = agencyId ;
                //    itinerary.BookingId = bookingId;
                //}
                product.Save(product); // itinerary.save();
                if (product.ProductType == ProductType.Flight)
                {
                    //FlightItinerary itinerary = (FlightItinerary)product; ziya-todo
                    //SaveProductLine(bookingId, ProductType.Flight, itinerary.FlightId, itinerary.BookingMode);
                }
                else if (product.ProductType == ProductType.Hotel)
                {
                    HotelItinerary itinerary = (HotelItinerary)product;
                    SaveProductLine(bookingId, ProductType.Hotel, itinerary.HotelId, itinerary.BookingMode);
                }
                else if (product.ProductType == ProductType.Transfers)
                {
                    TransferItinerary  itinerary = (TransferItinerary)product;
                    SaveProductLine(bookingId, ProductType.Transfers, itinerary.TransferId, itinerary.BookingMode);
                }
                else if (product.ProductType == ProductType.SightSeeing)
                {
                    SightseeingItinerary itinerary = (SightseeingItinerary)product;
                    SaveProductLine(bookingId, ProductType.SightSeeing, itinerary.SightseeingId, itinerary.BookingMode);
                }
                else if (product.ProductType == ProductType.Car)
                {
                    FleetItinerary itinerary = (FleetItinerary)product;
                    SaveProductLine(bookingId, ProductType.Car, itinerary.FleetId, itinerary.BookingMode);
                }
                else if (product.ProductType == ProductType.MobileRecharge)
                {
                    //MobileRechargeItinerary itinerary = (MobileRechargeItinerary)product; ziya-todo
                    //SaveProductLine(bookingId, ProductType.MobileRecharge, itinerary.RechargeId, BookingMode.Manual);
                }
            }

            //Trace.TraceInformation("BookingDetail.Save(FlightItinerary) exiting.");
        }

        public void SaveProductLine(int bookingId, ProductType productType, int productId,BookingMode mode)
        {
            //Trace.TraceInformation("BookingDetail.SaveProductLine entered : bookingId = " + bookingId + ", productType = " + productType + ", productId = " + productId);
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            paramList[1] = new SqlParameter("@productTypeId", (int)productType);
            paramList[2] = new SqlParameter("@productId", productId);
            paramList[3] = new SqlParameter("@bookingModeId", (int)mode);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddProductLine, paramList);
            //Trace.TraceInformation("BookingDetail.SaveProductLine exiting rowsAffected = " + rowsAffected);
        }
        /// <summary>
        /// It gets an array of Product which contains productTypeId,ProductId
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public static Product[] GetProductsLine(int bookingId)
        {
            
            //Trace.TraceInformation("BookingDetail.GetProductsDetail entered : bookingId = " + bookingId);
            if (bookingId <= 0)
            {
                throw new ArgumentException("Booking Id should be positive integer", "bookingId");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            DataSet dataSet = DBGateway.FillSP(SPNames.GetProductLine, paramList);
            Product[] products = new Product[dataSet.Tables[0].Rows.Count];
            int count=0;
            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                products[count] = new Product();
                products[count].ProductId = Convert.ToInt32(dr["productId"]);
                products[count].ProductTypeId = Convert.ToInt32(dr["productTypeId"]);
                products[count].ProductType = (ProductType)Enum.Parse(typeof(ProductType), dr["productTypeId"].ToString());
                products[count].BookingMode = (BookingMode)Enum.Parse(typeof(BookingMode),dr["BookingModeId"].ToString());
                count++;
            }
            //Trace.TraceInformation("BookingDetail.GetProductsDetail exiting : Rows=" + dataSet.Tables[0].Rows.Count);
            return products;
        }
        /// <summary>
        /// Loads detail for a corresponding Booking
        /// </summary>
        /// <param name="bookingId">bookingId</param>
        private void Load(int bookingId)
        {
            ////Trace.TraceInformation("BookingDetail.Load entered : bookingId = " + bookingId);
            if (bookingId <= 0)
            {
                throw new ArgumentException("Booking Id should be positive integer", "bookingId");
            }
            this.bookingId = bookingId;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBookingDetail, paramList,connection);
            using (DataTable dtBooking = DBGateway.FillDataTableSP(SPNames.GetBookingDetail, paramList))
            {
                //if (data.Read())
                if (dtBooking != null && dtBooking.Rows.Count > 0)
                {
                    DataRow data = dtBooking.Rows[0];
                    status = (BookingStatus)Enum.Parse(typeof(BookingStatus), data["statusId"].ToString()); ;
                    if (data["agencyId"] != DBNull.Value)
                    {
                        agencyId = Convert.ToInt32(data["agencyId"]);
                    }
                    else
                    {
                        agencyId = 0;
                    }
                    if (data["lockedBy"] != DBNull.Value)
                    {
                        lockedBy =Convert.ToInt32(data["lockedBy"]);
                    }
                    else
                    {
                        lockedBy = 0;
                    }
                    if (data["prevStatusId"] != DBNull.Value)
                    {
                        prevStatus = (BookingStatus)Convert.ToInt32(data["prevStatusId"]);
                    }
                    productsList = GetProductsLine(bookingId);
                    if (data["parentBookingId"] != DBNull.Value)
                    {
                        parentBookingId = Convert.ToInt32(data["parentBookingId"]);
                    }
                    createdBy = Convert.ToInt32(data["createdBy"]);
                    createdOn = Convert.ToDateTime(data["createdOn"]);
                    lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    //data.Close();
                    //connection.Close();
                    //Trace.TraceInformation("BookingDetail.Load exiting :" + bookingId.ToString());
                }
                else
                {
                    //data.Close();
                    //connection.Close();
                    //Trace.TraceInformation("BookingDetail.Load exiting : BookingId does not exist.bookingId = " + bookingId.ToString());
                    throw new ArgumentException("Booking id does not exist in database");
                }
            }
        }

        /// <summary>
        /// Sets new status to a booking
        /// </summary>
        /// <param name="bStatus">New Status of the booking</param>
        /// <param name="modifiedBy">memberId of the member who is making modification</param>
        public void SetBookingStatus(BookingStatus status, int modifiedBy)
        {
            //Trace.TraceInformation("BookingDetail.SetStatus entered : new status = " + status.ToString());
            this.status = status;
            lastModifiedBy = modifiedBy;
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            paramList[1] = new SqlParameter("@statusId", (int)status);
            paramList[2] = new SqlParameter("@lastModifiedBy", modifiedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SetBookingStatus, paramList);
            //Trace.TraceInformation("BookingDetail.SetStatus exiting : rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// Sets new status to a booking
        /// </summary>
        /// <param name="bookingId">bookingId of the booking whose status is to be set</param>
        /// <param name="bStatus">New Status of the booking</param>
        /// <param name="modifiedBy">memberId of the member who is making modification</param>
        public static bool SetBookingStatus(int bookingId, BookingStatus status, int modifiedBy)
        {
            //Trace.TraceInformation("BookingDetail.SetStatus entered : new status = " + status.ToString());
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            paramList[1] = new SqlParameter("@statusId", (int)status);
            paramList[2] = new SqlParameter("@lastModifiedBy", modifiedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SetBookingStatus, paramList);
            //Trace.TraceInformation("BookingDetail.SetStatus exiting : rowsAffected = " + rowsAffected);
            return (rowsAffected > 0);
        }

        /// <summary>
        /// Sets new status to a booking
        /// </summary>
        /// <param name="bStatus">New Status of the booking</param>
        /// <param name="modifiedBy">memberId of the member who is making modification</param>
        public void Lock(BookingStatus status, int lockedBy)
        {
            //Trace.TraceInformation("BookingDetail.Lock entered : new status = " + status.ToString());
            this.status = status;
            lastModifiedBy = lockedBy;
            this.lockedBy = lockedBy;
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            paramList[1] = new SqlParameter("@statusId", (int)status);
            paramList[2] = new SqlParameter("@lockedBy", lockedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.LockBooking, paramList);
            //Trace.TraceInformation("BookingDetail.Lock exiting : rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// Unlocks a booking identified by bookingId.
        /// </summary>
        /// <param name="status">status to be set after unlock</param>
        /// <param name="releasedBy">memberId of member releasing the lock</param>
        public int Unlock(int releasedBy)
        {
            //Trace.TraceInformation("BookingDetail.ReleaseLock entered : new status = " + bookingId);
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            paramList[1] = new SqlParameter("@releasedBy", releasedBy);
            paramList[2] = new SqlParameter("@statusId", SqlDbType.Int);
            paramList[2].Direction = ParameterDirection.Output;
            paramList[3] = new SqlParameter("@prevStatusId", SqlDbType.Int);
            paramList[3].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UnlockBooking, paramList);
            if (rowsAffected > 0)
            {
                status = (BookingStatus)paramList[2].Value;
                prevStatus = (BookingStatus)paramList[3].Value;
            }
            lockedBy = 0;
            lastModifiedBy = releasedBy;
            //Trace.TraceInformation("BookingDetail.ReleaseLock exiting : rowsAffected = " + rowsAffected);
            return rowsAffected;
        }

        /// <summary>
        /// Unlocks a given booking
        /// </summary>
        /// <param name="status">status of the booking to be set after unlock.</param>
        /// <param name="releasedBy">memberId of the memeber releasing the lock</param>
        public int Unlock(BookingStatus newStatus, int releasedBy)
        {
            //Trace.TraceInformation("BookingDetail.ReleaseLock entered : new status = " + status.ToString());
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            paramList[1] = new SqlParameter("@releasedBy", releasedBy);
            paramList[2] = new SqlParameter("@statusId", (int)newStatus);
            paramList[3] = new SqlParameter("@prevStatusId", SqlDbType.Int);
            paramList[3].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UnlockBookingWithStatus, paramList);
            if (rowsAffected > 0)
            {
                prevStatus = (BookingStatus)paramList[3].Value;
            }
            status = newStatus;
            lastModifiedBy = releasedBy;
            lockedBy = 0;
            //Trace.TraceInformation("BookingDetail.ReleaseLock exiting : rowsAffected = " + rowsAffected);
            return rowsAffected;
        }

        //TODO: check if it is used somewhere. if not then delete. no need of this method now.
        /// <summary>
        /// Checks if a booking is locked by someone for processing
        /// </summary>
        /// <param name="bookingId">bookingId for which lock is to be checked</param>
        /// <param name="memberId">memberId for which the lock is to be checked</param>
        /// <returns>0 if not locked and memberId of member if locked by someone</returns>
        public static int CheckLock(int bookingId, int memberId)
        {
            //Trace.TraceInformation("BookingDetail.CheckLock entered : bookingId = " + bookingId + ", memberId = " + memberId);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            //paramList[1] = new SqlParameter("@lockedBy", memberId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.CheckLock, paramList,connection);
            int lockedBy = 0;
            if (dataReader["lockedBy"] != null)
            {
                lockedBy = (int)dataReader["lockedBy"];
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("BookingDetail.CheckLock exiting : locked = " + lockedBy.ToString());
            return lockedBy;
        }

        /// <summary>
        /// Checking If PNR already exist in Database
        /// </summary>
        /// <param name="PNR"></param>
        /// <returns></returns>
        private static int CheckPNR(string PNR)
        {
            int flightId = 0;
            ////Trace.TraceInformation("BookingDetail.CheckPNR entered : PNR = " + PNR);
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@pnr", PNR);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.CheckPNR, paramList,connection);
            using (DataTable dtPNR = DBGateway.FillDataTableSP(SPNames.CheckPNR, paramList))
            {                
                //if (dataReader != null && dataReader.Read())
                if(dtPNR != null && dtPNR.Rows.Count > 0)
                {
                    DataRow data = dtPNR.Rows[0];
                    if (data["flightId"] != DBNull.Value)
                    {
                        flightId =Convert.ToInt32(data["flightId"]);
                    }
                }
            }
            //dataReader.Close();
            //connection.Close();
            //Trace.TraceInformation("BookingDetail.CheckPNR exit : PNR = " + PNR);
            return flightId;
        }
        /// <summary>
        /// Method Gets the total price of all ready bookings by a given agency
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns>totalprice</returns>
        public static decimal GetReadyBookingPrice(int agencyId)
        {
            decimal totalPrice = 0;
            List<int> priceIdList = new List<int>();
            //Trace.TraceInformation("BookingDetail.GetReadyBookingPrice entered : agencyId = " + agencyId);
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer", "agencyId");
            }
            //int agencyTypeId = Agency.GetAgencyTypeId(agencyId); ziya-todo
            int agencyTypeId = 1;
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetReadyBookingPrice, paramList,connection);
            while (data != null && data.Read())
            {
                //priceIdList.Add(Convert.ToInt32(data["priceId"]));
                PriceAccounts price = new PriceAccounts();
                price.Load(Convert.ToInt32(data["priceId"]));
                //if (agencyTypeId == (int)Agencytype.Service) ZIYA-TODO
                if (agencyTypeId == 1)
                {
                    totalPrice = totalPrice + price.GetServiceAgentPrice();
                }
                else
                {
                    totalPrice = totalPrice + price.GetAgentPrice();
                }
                
            }
            data.Close();
            connection.Close();            
            //Trace.TraceInformation("BookingDetail.GetReadyBookingPrice exiting :");
            return totalPrice;
        }
        /// <summary>
        /// To Get Price of a booking
        /// </summary>
        /// <param name="bookingId"> Unique Id of a booking</param>
        /// <returns></returns>
        public static decimal GetBookingPrice(int bookingId)
        {
            decimal totalPrice = 0;            
            if (bookingId <= 0)
            {
                throw new ArgumentException("BookingId should be positive integer", "BookingId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBookingPrice, paramList,connection);
            using (DataTable dtPrice = DBGateway.FillDataTableSP(SPNames.GetBookingPrice, paramList))
            {
                //while (data != null && data.Read())
                if(dtPrice != null)
                {
                    foreach (DataRow data in dtPrice.Rows)
                    {
                        PriceAccounts price = new PriceAccounts();
                        price.Load(Convert.ToInt32(data["priceId"]));
                        totalPrice = totalPrice + price.GetAgentPrice();
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("BookingDetail.GetBookingPrice exiting :");
            return totalPrice;
        }

        public static DateTime GetTicketIssueDate(int flightId)
        {
            //Trace.TraceInformation("BookingDetail.GetTicketIssueDate entered : flightId = " + flightId);
            DateTime issueDate = new DateTime();
            if (flightId <= 0)
            {
                throw new ArgumentException("Flight Id should be positive integer", "flightId");
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);                     
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetTicketIssueDateByFlightId, paramList,connection);
            //We are taking Last Ticket Issue Date of a particular flightId
            while (data != null && data.Read())
            {
                issueDate = Convert.ToDateTime(data["issueDate"]);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("BookingDetail.GetTicketIssueDate exiting .");
            return issueDate;

        }

        public static int GetBookingIdByFlightId(int flightId)
        {
           // Trace.TraceInformation("BookingDetail.GetBookingIdByFlightId entered : flightId = " + flightId);
            int bookingId = 0;
            if (flightId <= 0)
            {
                throw new ArgumentException("Flight Id should be positive integer", "flightId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBookingIdByFlightId, paramList,connection);
            ////We are taking Last Ticket Issue Date of a particular flightId
            //while (data != null && data.Read())
            //{
            //    bookingId = Convert.ToInt32(data["bookingId"]);
            //}
            //data.Close();
            //connection.Close();
            DataTable dtBooking = DBGateway.FillDataTableSP(SPNames.GetBookingIdByFlightId, paramList);

            if (dtBooking != null && dtBooking.Rows.Count > 0 && dtBooking.Rows[0]["bookingId"] !=DBNull.Value)
            {
                bookingId = Convert.ToInt32(dtBooking.Rows[0]["bookingId"]);
            }
            //Trace.TraceInformation("BookingDetail.GetBookingIdByFlightId exiting .");
            return bookingId;

        }

        public static int GetBookingIdByProductId(int productId, ProductType prodType)
        {
            //Trace.TraceInformation("BookingDetail.GetBookingIdByProductId entered : productId = " + productId);
            int bookingId = 0;
            if (productId <= 0)
            {
                throw new ArgumentException("Product Id should be positive integer", "productId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@productId", productId);
            paramList[1] = new SqlParameter("@productType", (int)prodType);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBookingIdByProductId, paramList,connection);
            using (DataTable dtBooking = DBGateway.FillDataTableSP(SPNames.GetBookingIdByProductId, paramList))
            {
                if(dtBooking != null && dtBooking.Rows.Count > 0)
                {
                    DataRow data = dtBooking.Rows[0];
                    if (data["bookingId"] != DBNull.Value)
                    {
                        bookingId = Convert.ToInt32(data["bookingId"]);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("BookingDetail.GetBookingIdByProductId exiting .");
            return bookingId;
        }

        /// <summary>
        /// Method to load all the child bookings for the given parentBookingId.
        /// </summary>
        /// <param name="parentBookingId"></param>
        /// <returns></returns>
        public static List<BookingDetail> GetChildBookings(int parentBookingId)
        {
            //Trace.TraceInformation("BookingDetail.GetChildBookings() entered : parentBookingId = " + parentBookingId);
            List<BookingDetail> childBookings = new List<BookingDetail>();
            if (parentBookingId <= 0)
            {
                throw new ArgumentException("Parent BookingId must have a value", "parentBookingId");
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@parentBookingId", parentBookingId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetChildBookings, paramList,connection);
            while (dataReader.Read())
            {
                BookingDetail booking = new BookingDetail();
                booking.bookingId = Convert.ToInt32(dataReader["bookingId"]);
                if (dataReader["parentBookingId"] != DBNull.Value)
                {
                    booking.parentBookingId = Convert.ToInt32(dataReader["parentBookingId"]);
                }
                else
                {
                    booking.parentBookingId = 0;
                }
                booking.status = (BookingStatus)Enum.Parse(typeof(BookingStatus), dataReader["statusId"].ToString()); ;
                if (dataReader["agencyId"] != DBNull.Value)
                {
                    booking.agencyId = Convert.ToInt32(dataReader["agencyId"]);
                }
                else
                {
                    booking.agencyId = 0;
                }
                if (dataReader["lockedBy"] != DBNull.Value)
                {
                    booking.lockedBy = (int)dataReader["lockedBy"];
                }
                else
                {
                    booking.lockedBy = 0;
                }
                if (dataReader["prevStatusId"] != DBNull.Value)
                {
                    booking.prevStatus = (BookingStatus)Convert.ToInt32(dataReader["prevStatusId"]);
                }
                booking.productsList = GetProductsLine(booking.bookingId);
                booking.createdBy = Convert.ToInt32(dataReader["createdBy"]);
                booking.createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                booking.lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                booking.lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                childBookings.Add(booking);
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("BookingDetail.GetChildBookings() exiting : parentBookingId = " + parentBookingId);
            return childBookings;

        }


        /// <summary>
        /// To update Booking details 
        /// In Case of PNR refresh - status id will be saved as Inactive for given Booking Id
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="lastModifiedBy"></param>
        public static void Update(int bookingId, int lastModifiedBy)
        {
            //Trace.TraceInformation("BookingDetail.Update entered : BookingId = " + bookingId);
            SqlParameter[] paramList = new SqlParameter[3];
            if (bookingId <= 0)
            {
                throw new ArgumentException("bookingId must have a value", "bookingId");
            }
            if (lastModifiedBy <= 0)
            {
                throw new ArgumentException("lastModifiedBy must have a value", "lastModifiedBy");
            }
            //Trace.TraceInformation(" Verification of compulsory values done");
            paramList[0] = new SqlParameter("@status", BookingStatus.Inactive);            
            paramList[1] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[2] = new SqlParameter("@bookingId", bookingId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateBookingDetail, paramList);
            //Trace.TraceInformation("BookingDetail.Update exiting : bookingId = " + bookingId);
        }
        /// <summary>
        /// Method to update agency Id if agency changes for a booking.
        /// </summary>
        /// <param name="bookingid">bookingid</param>
        /// <param name="lastModifiedBy">lastModifiedBy</param>
        /// <param name="agencyId">agencyId</param>
        public static void Update(int bookingid, int lastModifiedBy, int agencyId)
        {
            //Trace.TraceInformation("BookingEngine.Update entered: BookingId=" + bookingid);
            SqlParameter[] paramList= new SqlParameter[3];
            if(bookingid<=0)
            {
                throw new ArgumentException("BookingId must have a value","bookingId");
            }
            if (lastModifiedBy <= 0)
            {
                throw new ArgumentException("lstModifiedBy must have a value", "lstModifiedBy");
            }
            paramList[0] = new SqlParameter("@bookingId", bookingid);
            paramList[1] = new SqlParameter("@lastModifiedBy",lastModifiedBy);
            paramList[2] = new SqlParameter("@agencyId",agencyId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateBookingDetailAgencyId, paramList);
            //Trace.TraceInformation("BookingDetail.Update exiting : bookingId="+bookingid);
        }

        public static DataSet GetBookingCount(int Agencyid,long userId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@AgencyId", Agencyid);
                if (userId > 0) paramList[1] = new SqlParameter("@UserId", userId);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_getBookingCount", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
       
        #endregion
    }
}
