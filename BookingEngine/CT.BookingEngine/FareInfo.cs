using System;
using System.Collections.Generic;
using System.Text;

namespace CT.BookingEngine
{
    public class FareInfo
    {
        /// <summary>
        /// Two character airline code.
        /// </summary>
        private string airline;
        /// <summary>
        /// Gets of sets the airline code.
        /// </summary>
        public string Airline
        {
            get { return airline; }
            set { airline = value; }
        }

        /// <summary>
        /// Origin airport.
        /// </summary>
        private Airport origin;
        /// <summary>
        /// Gets or sets the origin airport.
        /// </summary>
        public Airport Origin
        {
            get { return origin; }
            set { origin = value; }
        }

        /// <summary>
        /// Destination airport.
        /// </summary>
        private Airport destination;
        /// <summary>
        /// Gets or sets the destination airport.
        /// </summary>
        public Airport Destination
        {
            get { return destination; }
            set { destination = value; }
        }

        /// <summary>
        /// Restriction indicator.
        /// </summary>
        private string restrictionIndicator;
        /// <summary>
        /// Gets or sets the restriction indicator.
        /// </summary>
        public string RestrictionIndicator
        {
            get { return restrictionIndicator; }
            set { restrictionIndicator = value; }
        }

        /// <summary>
        /// Fare basis code for the fare.
        /// </summary>
        private string fareBasisCode;
        /// <summary>
        /// Gets or sets the fare basis code.
        /// </summary>
        public string FareBasisCode
        {
            get { return fareBasisCode; }
            set { fareBasisCode = value; }
        }

        /// <summary>
        /// Indicates if the fare is for round trip. True if the fare is round trip.
        /// </summary>
        private bool roundTrip;
        /// <summary>
        /// Gets or sets the round trip indicator. True if the fare is round trip.
        /// </summary>
        public bool RoundTrip
        {
            get { return roundTrip; }
            set { roundTrip = value; }
        }

        /// <summary>
        /// Currency code of the fare.
        /// </summary>
        private string currencyCode;
        /// <summary>
        /// Gets or sets the currency code.
        /// </summary>
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        /// <summary>
        /// Fare amount in currency as indicated in currency code.
        /// </summary>
        private decimal fare;
        /// <summary>
        /// Gets or sets the fare amount. Currency is as in currency code field.
        /// </summary>
        public decimal Fare
        {
            get { return fare; }
            set { fare = value; }
        }

        /// <summary>
        /// The date onwards which travel is allowed on this fare.
        /// </summary>
        private DateTime firstTravelDate;
        /// <summary>
        /// Gets or sets the first travel date.
        /// </summary>
        public DateTime FirstTravelDate
        {
            get { return firstTravelDate; }
            set { firstTravelDate = value; }
        }

        /// <summary>
        /// The date upto which the fare is valid for travel.
        /// </summary>
        private DateTime lastTravelDate;
        /// <summary>
        /// Gets or sets the last travel date.
        /// </summary>
        public DateTime LastTravelDate
        {
            get { return lastTravelDate; }
            set { lastTravelDate = value; }
        }

        /// <summary>
        /// The date onwards which ticketing is allowed.
        /// </summary>
        private DateTime firstTicketDate;
        /// <summary>
        /// Gets or sets the first ticket date.
        /// </summary>
        public DateTime FirstTicketDate
        {
            get { return firstTicketDate; }
            set { firstTicketDate = value; }
        }

        /// <summary>
        /// The date upto which ticketing is allowed.
        /// </summary>
        private DateTime lastTicketDate;
        /// <summary>
        /// Gets or sets the last ticketing date.
        /// </summary>
        public DateTime LastTicketDate
        {
            get { return lastTicketDate; }
            set { lastTicketDate = value; }
        }

        /// <summary>
        /// Class for travel.
        /// </summary>
        private string bookingClass;
        /// <summary>
        /// Gets or sets the class of travel.
        /// </summary>
        public string BookingClass
        {
            get { return bookingClass; }
            set { bookingClass = value; }
        }
    }
}
