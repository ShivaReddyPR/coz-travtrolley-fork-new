using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using System.Configuration;

namespace CT.BookingEngine
{
    //Offline Train Booking
    /// <summary>
    /// HotelBookingStatus
    /// </summary>
    public enum TrainBookingStatus
    {
        /// <summary>
        /// If booking is failed
        /// </summary>
        Failed = 0,
        /// <summary>
        /// if Booking is Confirmed
        /// </summary>
        Confirmed = 1,
        /// <summary>
        /// if booking is canceled
        /// </summary>
        Cancelled = 2,
        /// <summary>
        /// if booking is Pending
        /// </summary>
        Pending = 3,
        /// <summary>
        /// if booking is error
        /// </summary>
        Error = 4
    }

    public class RailwayItineraryResponse
    {
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// ReferenceID
        /// </summary>
        public string ReferenceID { get; set; }
    }

    [Serializable]
    public class RailwayItinerary
    {
        #region public properties
        /// <summary>
        /// RBId
        /// </summary>
        public long BookingID { get; set; }
        /// <summary>
        /// FromStation
        /// </summary>
        public string FromStation { get; set; }
        /// <summary>
        /// ToStation
        /// </summary>
        public string ToStation { get; set; }
        /// <summary>
        /// BoardingDate
        /// </summary>
        public DateTime BoardingDate { get; set; }
        /// <summary>
        /// BoardingFrom
        /// </summary>
        public string BoardingFrom { get; set; }
        /// <summary>
        /// ReservationUpto
        /// </summary>
        public string ReservationUpto { get; set; }
        /// <summary>
        /// Class
        /// </summary>
        public string Class { get; set; }
        /// <summary>
        /// TrainNumber
        /// </summary>
        public string TrainNumber { get; set; }
        /// <summary>
        /// TrainName
        /// </summary>
        public string TrainName { get; set; }
        /// <summary>
        /// IDProofType
        /// </summary>
        public string IDProofType { get; set; }
        /// <summary>
        /// IDProofNumber
        /// </summary>
        public string IDProofNumber { get; set; }
        /// <summary>
        /// Adults
        /// </summary>
        public int Adults { get; set; }
        /// <summary>
        /// Children
        /// </summary>
        public int Children { get; set; }
        /// <summary>
        /// Quota
        /// </summary>
        public string Quota { get; set; }
        /// <summary>
        /// AgentId
        /// </summary>
        public long AgentId { get; set; }
        /// <summary>
        /// LocationId
        /// </summary>
        public long LocationId { get; set; }
        /// <summary>
        /// IsOnbehalf
        /// </summary>
        public bool IsOnbehalf { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// PNR
        /// </summary>
        public string PNR { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        public int CreatedBy { get; set; }
        /// <summary>
        /// CreatedOn
        /// </summary>
        public DateTime CreatedOn { get; set; }
        /// <summary>
        /// ModifiedBy
        /// </summary>
        public int? ModifiedBy { get; set; }
        /// <summary>
        /// ModifiedOn
        /// </summary>
        public DateTime? ModifiedOn { get; set; }
        /// <summary>
        /// ReferenceID
        /// </summary>
        public string ReferenceID { get; set; }
        /// <summary>
        /// PassengerDetails
        /// </summary>
        public List<RailwayPassenger> PassengerDetails { get; set; }
        /// <summary>
        /// BookingRemarks
        /// </summary>
        public RailwayBookingRemarks BookingRemarks { get; set; }
        #endregion

        #region Methods
        /// <summary>
        ///  This method is to save the Record into HotelItinerary DB
        /// </summary>
        /// <param name="prod">Product</param>
        public RailwayItineraryResponse SaveItinerary()
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@RBId", SqlDbType.BigInt));
                paramList[0].Direction = ParameterDirection.Output;
                paramList.Add(new SqlParameter("@ReferenceId", SqlDbType.NVarChar,50));
                paramList[1].Direction = ParameterDirection.Output;
                paramList.Add(new SqlParameter("@FromStation", FromStation));
                paramList.Add(new SqlParameter("@ToStation", ToStation));
                paramList.Add(new SqlParameter("@BoardingDate", BoardingDate));
                paramList.Add(new SqlParameter("@BoardingFrom", BoardingFrom));
                paramList.Add(new SqlParameter("@ReservationUpto", ReservationUpto));
                paramList.Add(new SqlParameter("@Class", Class));
                paramList.Add(new SqlParameter("@TrainNumber", TrainNumber));
                paramList.Add(new SqlParameter("@TrainName", TrainName));
                if (IDProofType != null)
                    paramList.Add(new SqlParameter("@IDProofType", IDProofType));
                else
                    paramList.Add(new SqlParameter("@IDProofType", DBNull.Value));
                if (IDProofNumber != null)
                    paramList.Add(new SqlParameter("@IDProofNumber", IDProofNumber));
                else
                    paramList.Add(new SqlParameter("@IDProofNumber", DBNull.Value));
                paramList.Add(new SqlParameter("@Adults", Adults));
                paramList.Add(new SqlParameter("@Children", Children));
                paramList.Add(new SqlParameter("@Quota", Quota));
                paramList.Add(new SqlParameter("@AgentId", AgentId));
                paramList.Add(new SqlParameter("@LocationId",LocationId));
                paramList.Add(new SqlParameter("@IsOnbehalf", IsOnbehalf));
                paramList.Add(new SqlParameter("@Email", Email));
                paramList.Add(new SqlParameter("@Status", Status));
                paramList.Add(new SqlParameter("@PNR", DBNull.Value));
                paramList.Add(new SqlParameter("@CreatedBy", CreatedBy));
                DBGateway.ExecuteNonQuerySP("usp_AddRailwayItinerary", paramList.ToArray());
                BookingID = Convert.ToInt64(paramList[0].Value);
                ReferenceID = Convert.ToString(paramList[1].Value);
                List<RailwayPassenger> passengerDetails = PassengerDetails;
                // Saving passenger details
                if (PassengerDetails != null)
                {
                    foreach (RailwayPassenger passenger in PassengerDetails)
                    {
                        passenger.BookingID = BookingID;
                        passenger.SavePassengers();
                    }
                }
                if (BookingRemarks != null)
                {
                    RailwayBookingRemarks remarks = BookingRemarks;
                    remarks.BookingID = BookingID;
                    remarks.SaveRemarks();
                }
                string subject = "";
                string body = "";
                CreateItineraryMail(ref subject, ref body);
                SendMail(AgentId, Email, subject, body);

                RailwayItineraryResponse response = new RailwayItineraryResponse();
                response.ReferenceID = ReferenceID;
                response.Status = "success";

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static void SendMail(long agentID,string toEmail, string subject, string body)
        {
            string from = ConfigurationManager.AppSettings["fromEmail"] != null ? ConfigurationManager.AppSettings["fromEmail"] : "";
            string replyTo = from;
            List<string> toMailList = new List<string>();
            toMailList.Add(toEmail);
            string bcc = "";
            string appkey = "BCCTrainBooking";
            AgentAppConfig agentAppConfig = new AgentAppConfig();
            agentAppConfig.AgentID = Convert.ToInt32(agentID);
            List<AgentAppConfig> agentAppConfigs = agentAppConfig.GetConfigData();
            if (agentAppConfigs.Exists(x => x.AppKey == appkey))
            {
                bcc = !string.IsNullOrEmpty(agentAppConfigs.Where(x => x.AppKey == appkey).FirstOrDefault().AppValue)
                    ? agentAppConfigs.Where(x => x.AppKey == appkey).FirstOrDefault().AppValue : "";
            }
            try
            {
                CT.Core.Email.Send(from, replyTo, toMailList, subject, body, new Hashtable(), bcc);
            }
            catch (System.Net.Mail.SmtpException)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
            }
        }
        /// <summary>
        /// Create the body for booking request mail
        /// </summary>
        /// <param name="subject">email subject (ref)</param>
        /// <param name="body">email body (ref)</param>
        public void CreateItineraryMail(ref string subject, ref string body)
        {
            string paxName = "";
            if (PassengerDetails != null)
            {
                foreach (RailwayPassenger passenger in PassengerDetails)
                {
                    if (passenger.IsLeadPassenger)
                    {
                        paxName = passenger.PaxName;
                        break;
                    }
                }
            }
            string adultString = Convert.ToInt32(Adults) > 1? "Adults" : "Adult";
            string childtString = Convert.ToInt32(Children) > 1 ? "Children" : "Child";
            subject = "Train booking request has been submitted";
            StringBuilder sb = new StringBuilder();
            sb.Append("<body style=\"font-family:arial;font-size: 12px;margin: 0px;padding: 0px;background-color: White;color:#4c4c4b;\">");
            sb.Append("<div style=\"width:778px;margin: auto;\">");
            sb.Append("<div style=\"width:778px;float:left;\">");
            sb.Append("<div style=\"float:left;width:100%;border: solid 1px #c0c0c0;\">");
            sb.Append("<div style=\"float:left;width:97%;margin:0px;padding-top:20px;padding-left:0px;\">");
            sb.Append("<div style=\"float:left;width:100%;padding: 10px;\">");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Reference ID:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + ReferenceID + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Passenger Name:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + paxName + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Email:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + Email + "</span></p>");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Boarding Date:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + BoardingDate.ToString("dd MMM yy") + "</span></p>");
            sb.Append(" <p style=\"float:left; width:100%; margin:0; padding:0;\">"
                +"<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\"> No.of Guest(s):</span>"
                +"<span style=\"float:left;width:150px;font-weight:bold;font-size:15px;\">" + (Adults + Children).ToString() + "  </span>");
            sb.Append("<span style=\"float:left;width:120px;font-weight:bold;font-size:15px;\">" + Adults.ToString() +" "+ adultString + "</span>");
            if(Children>0)
                sb.Append("<span style=\"float:left;width:120px;font-weight:bold;font-size:15px;\">" + Children.ToString() + " " + childtString + "</span>");
            sb.Append("</p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Train Number:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + TrainNumber + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Train Name:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + TrainName + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Trip:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + FromStation + " - " + ToStation + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Boarding From:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + BoardingFrom + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Reservation Upto:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + ReservationUpto + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Requested on:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + DateTime.Now.ToString("dd MMM yy") + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Class:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + Class + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Quota:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + Quota + "</span></p>");
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Status:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + Status + "</span></p>");
            if(PNR!=null)
            {
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
                sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">PNR:</span> ");
                sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + PNR + "</span></p>");
            }
            sb.Append("</div></div>");
            sb.Append("</div></div></div></body>");
            body = sb.ToString();
        }
        /// <summary>
        /// Remove the duplicate rows in a datatable to bind the dropdown list
        /// </summary>
        /// <param name="table">Data Table</param>
        /// <param name="distinctColumnName">Distinct Column Name</param>
        /// <returns>DataTable</returns>
        public DataTable RemoveDuplicateRows(DataTable table, string distinctColumnName)
        {
            try
            {
                ArrayList UniqueRecords = new ArrayList();
                ArrayList DuplicateRecords = new ArrayList();
                // Check if records is already added to UniqueRecords otherwise add the records to DuplicateRecords
                foreach (DataRow dRow in table.Rows)
                {
                    if (UniqueRecords.Contains(dRow[distinctColumnName]))
                        DuplicateRecords.Add(dRow);
                    else
                        UniqueRecords.Add(dRow[distinctColumnName]);
                }
                // Remove duplicate rows from DataTable added to DuplicateRecords
                foreach (DataRow dRow in DuplicateRecords)
                {
                    table.Rows.Remove(dRow);
                }
                // Return the clean DataTable which contains unique records.
                return table;
            }
            catch (Exception ex)
            {
                //Retun the original table if any exception occurs.
                return table;
            }
        }
        #endregion
    }
}
