﻿//using System.Linq;

namespace CT.BookingEngine
{
    public class FleetCity
    {
        string cityCode;
        string cityName;
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
    }

    public class FleetLocation
    {
        string cityCode;
        string locationCode;
        string locationName;
        decimal airportCharges;

        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }

        public string LocationCode
        {
            get { return locationCode; }
            set { locationCode = value; }
        }

        public string LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public decimal AirportCharges
        {
            get { return airportCharges; }
            set { airportCharges = value; }
        }
    }

}
