using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;


namespace CT.BookingEngine
{
    public enum TypeOfDocument
    {
        PassportFront1 = 1,
        PassportFront2 = 2,
        PassportAdressPage1 = 3,
        PassportBack2 = 4,
        BTQForm = 5,
        TravellerCheque = 6,
        PassportRenew = 7
    }
    public class IntlTransferBookingDetails
    {
        private int transferId;
        TypeOfDocument documentName;
        private string fileName;

        public int TransferId
        {
            get { return transferId; }
            set { transferId = value; }
        }

        public TypeOfDocument DocumentName
        {
            get { return documentName; }
            set { documentName = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public List<IntlTransferBookingDetails> Load(int transferId)
        {
            //Trace.TraceInformation("IntlTransferBookingDetails.Load entered :transferID = " + transferId);
            if (transferId <= 0)
            {
                throw new ArgumentException("transferId value is null", "transferId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transferId", transferId);
            List<IntlTransferBookingDetails> listDetails = new List<IntlTransferBookingDetails>();

            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetIntelTransferById, paramList, connection);
            using (DataTable dtIntlTransfer = DBGateway.FillDataTableSP(SPNames.GetIntelTransferById, paramList))
            {
                if (dtIntlTransfer != null && dtIntlTransfer.Rows.Count > 0)
                {
                    //while (data.Read())
                    foreach(DataRow data in dtIntlTransfer.Rows)
                    {
                        IntlTransferBookingDetails internationalInfo = new IntlTransferBookingDetails();
                        internationalInfo.transferId = Convert.ToInt32(data["transferId"]);
                        internationalInfo.DocumentName = (TypeOfDocument)data["typeOfDocument"];
                        internationalInfo.FileName =Convert.ToString(data["fileName"]);
                        listDetails.Add(internationalInfo);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("IntlTransferBookingDetail.Load exiting.");
            return listDetails;
        }
        public void Save()
        {
            //Trace.TraceInformation("IntlTransferBookingDetails.Save entered.");
            SqlParameter[] paramList = new SqlParameter[3];
            try
            {
                paramList[0] = new SqlParameter("@transferId", transferId);
                paramList[1] = new SqlParameter("@typeOfDocument", (TypeOfDocument)documentName);
                paramList[2] = new SqlParameter("@fileName", fileName);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddIntlTransferBookingDetails, paramList);
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message);
            }

            //Trace.TraceInformation("IntlTransferBookingDetails.Save exiting");
        }
    }
}
