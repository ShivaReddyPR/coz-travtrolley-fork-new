﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CT.BookingEngine
{

    [Serializable]
    public class OfflinePaxInfo
    {
        #region Private Variables
        /// <summary>
        /// Unique Id for a passenger
        /// </summary>
        int paxId;
        /// <summary>
        /// flight id of the flight to which the pax is linked
        /// </summary>
        int flightId;
        /// <summary>
        /// First name of passenger
        /// </summary>
        string firstName;
        /// <summary>
        /// Last name of passenger
        /// </summary>
        string lastName;
        /// <summary>
        /// TODO
        /// </summary>
        string title;
        /// <summary>
        /// full Name of passenger. (Title + FName + LName).
        /// </summary>
        string fullName;
        /// <summary>
        /// Mobile phone number of passenger
        /// </summary>
        string cellPhone;
        /// <summary>
        /// Indicates if the passenger is primary (leading) passenger
        /// </summary>
        bool isLeadPax;
        /// <summary>
        /// Date of Birth of passenger
        /// </summary> 
        DateTime dateOfBirth;
        /// <summary>
        /// Type of passenger. Adult, Child, Infant or Senior
        /// </summary>
        PassengerType type;
        /// <summary>
        /// Passport no of passenger
        /// </summary>
        string passportNo;
        /// <summary>
        /// Nationality of the passenger
        /// </summary>
        string nationality;
        /// <summary>
        /// Country issueing passport.
        /// </summary>
        string country;
        /// <summary>
        /// City Name
        /// </summary>
        string city;
        /// <summary>
        /// Address Line1
        /// </summary>
        string addressLine1;
        /// <summary>
        /// Address Line2
        /// </summary>
        string addressLine2;
        /// <summary>
        /// gender of the passenger
        /// </summary>
        Gender gender;
        /// <summary>
        /// EmailId of passenger
        /// </summary>
        string email;
        /// <summary>
        /// Meal Preference of passenger
        /// </summary>
        Meal meal;
        /// <summary>
        /// Frequent flier airline
        /// </summary>
        string ffAirline;
        /// <summary>
        /// Frequent flier number
        /// </summary>
        string ffNumber;
        /// <summary>
        /// LastName.FirstName.Title spaces replaced with '.'.
        /// Used to identify a passenger in a booking.
        /// </summary>
        string paxKey;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Expirt Date of passport
        /// </summary> 
        DateTime passportExpiry;
        /// <summary>
        /// BaggageCode for G9
        /// </summary>
        string baggageCode;

        /// <summary>
        /// BaggageCode Type for FlyDubai (BAGB,BAGL etc)
        /// </summary>
        string baggageType;
        /// <summary>
        /// Category/SSRCategory Id for FlyDubai (99 for Baggage)
        /// </summary>
        string categoryId;
        /// <summary>
        /// Flex Fields
        /// </summary>
        List<FlightFlexDetails> flexDetailsList;


        string destinationPhone;//Added by Lokesh on 25-sept-2017 regarding Pax Destination phone for booking tickets

        string gstStateCode; //Added by Lokesh on 27-Mar-2018 For G9 Source if the customer is travelling from India and selects the state Id.
        string gstTaxRegNo;//Added by Lokesh on 27-Mar-2018 For G9 Source if the customer is travelling from India and provides the TaxRegNo.
        /// <summary>
        /// Mandatory for FraudLabs. Saved for Audit in B2C
        /// </summary>
        string stateCode;

        /// <summary>
        /// string key stands for Tax Type.
        /// double value is the Tax Value.
        /// </summary>
        private List<KeyValuePair<string, decimal>> taxBreakup;

        string mealType; //For Spicejet And indigo Meal Selection
        string mealDesc; //For Spicejet And indigo Meal Selection in Eticket display purpose;

        /// <summary>
        /// To display all segments seat info
        /// string Added by Praveen to show selected seat info of all segments in payment confirmation page
        /// </summary>
        private string sSeatInfo;

        /// <summary>
        /// To store corporate profile Id
        /// string Added by Praveen to store corporate profile Id and save the same in itinerary passenger table
        /// </summary>
        private string _CorpProfileId;
        /// <summary>
        /// hotelId
        /// </summary>
        int hotelId;
        /// <summary>
        /// roomId
        /// </summary>
        int roomId;

        #endregion

        #region Public Members
        /// <summary>
        /// Gets or Sets paxId
        /// </summary>
        public int HotelId
        {
            get
            {
                return hotelId;
            }
            set
            {
                hotelId = value;
            }
        }
        /// <summary>
        /// Gets or Sets paxId
        /// </summary>
        public int RoomId
        {
            get
            {
                return roomId;
            }
            set
            {
                roomId = value;
            }
        }
        /// <summary>
        /// Gets or Sets paxId
        /// </summary>
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }

        /// <summary>
        /// Gets of sets the Flight Id.
        /// </summary>
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }

        /// <summary>
        /// Gets or sets First name
        /// </summary>
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        /// <summary>
        /// Gets or sets Last name
        /// </summary>
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        /// <summary>
        /// Gets or sets the title of passenger
        /// </summary>
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        /// <summary>
        /// Gets full name of a passenger.
        /// </summary>
        public string FullName
        {
            get
            {
                if (fullName == null || fullName.Length == 0)
                {
                    StringBuilder fName = new StringBuilder();
                    fName.Append(title);
                    fName.Append(" ");
                    fName.Append(firstName);
                    fName.Append(" ");
                    fName.Append(lastName);
                    fullName = fName.ToString().Trim();
                }
                return fullName;
            }
        }

        /// <summary>
        /// Gets or sets the Cellphone of passenger
        /// </summary>
        public string CellPhone
        {
            get
            {
                return cellPhone;
            }
            set
            {
                cellPhone = value;
            }
        }

        /// <summary>
        /// Gets or sets the isLeadPax property.
        /// </summary>
        public bool IsLeadPax
        {
            get
            {
                return isLeadPax;
            }
            set
            {
                isLeadPax = value;
            }
        }

        /// <summary>
        /// Gets or sets Date of birth
        /// </summary>
        public DateTime DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                dateOfBirth = value;
            }
        }

        /// <summary>
        /// Gets or sets the passenger type
        /// </summary>
        public PassengerType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        /// <summary>
        /// Gets or sets the passport number
        /// </summary>
        public string PassportNo
        {
            get
            {
                return passportNo;
            }
            set
            {
                passportNo = value;
            }
        }
        /// <summary>
        /// get or set the Nationality
        /// </summary>
        public string Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                nationality = value;
            }
        }
        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        /// <summary>
        ///  gets or sets city value
        /// </summary>
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        /// <summary>
        /// Gets or sets the address line 1
        /// </summary>
        public string AddressLine1
        {
            get
            {
                return addressLine1;
            }
            set
            {
                addressLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the address line 2
        /// </summary>
        public string AddressLine2
        {
            get
            {
                return addressLine2;
            }
            set
            {
                addressLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Gender
        /// </summary>
        public Gender Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }

        /// <summary>
        /// Gets or sets the email id of passenger
        /// </summary>
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        /// <summary>
        /// Gets or sets the Meal preference of passenger
        /// </summary>
        public Meal Meal
        {
            get
            {
                return meal;
            }
            set
            {
                meal = value;
            }
        }

        /// <summary>
        /// Gets or sets the Frequent Flier airline of passenger
        /// </summary>
        public string FFAirline
        {
            get
            {
                return ffAirline;
            }
            set
            {
                ffAirline = value;
            }
        }

        /// <summary>
        /// Gets or sets the Frequent Flier Number of the passenger
        /// </summary>
        public string FFNumber
        {
            get
            {
                return ffNumber;
            }
            set
            {
                ffNumber = value;
            }
        }

        /// <summary>
        /// Gets PaxKey. LastName.FirstName.Title with spaces replaced with '.'.
        /// </summary>
        public string PaxKey
        {
            get
            {
                if (paxKey == null || paxKey.Length == 0)
                {
                    StringBuilder key = new StringBuilder();
                    key.Append(lastName);
                    key.Append(".");
                    key.Append(firstName);
                    if (title != null && title.Length > 0)
                    {
                        //key.Append(".");
                        key.Append(title);
                    }
                    paxKey = key.ToString().Replace(' ', '.').Trim().ToUpper();
                }
                paxKey = paxKey.Replace(".", "");
                return paxKey;
            }
        }

        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets passport expiry date
        /// </summary>
        public DateTime PassportExpiry
        {
            get
            {
                return passportExpiry;
            }
            set
            {
                passportExpiry = value;
            }
        }
        /// <summary>
        /// BaggageCode for G9 (Air Arabia)
        /// </summary>
        public string BaggageCode
        {
            get { return baggageCode; }
            set { baggageCode = value; }
        }

        /// <summary>
        /// BaggageCode Type for FlyDubai (BAGB,BAGL etc)
        /// </summary>
        public string BaggageType
        {
            get { return baggageType; }
            set { baggageType = value; }
        }
        /// <summary>
        /// Category/SSRCategory Id for FlyDubai (99 for Baggage)
        /// </summary>
        public string CategoryId
        {
            get { return categoryId; }
            set { categoryId = value; }
        }
        /// <summary>
        /// Flex Details
        /// </summary>
        public List<FlightFlexDetails> FlexDetailsList
        {
            get { return flexDetailsList; }
            set { flexDetailsList = value; }
        }


        //Added by Lokesh on 25-sept-2017 regarding pax destination phone for booking tickets     
        public string DestinationPhone
        {
            get { return destinationPhone; }
            set { destinationPhone = value; }
        }

        //Added by Lokesh on 27/03/2018 For G9 Source only for lead Pax 
        //if the customer is travelling from India and provides any GST info (StateCode and Tax RegNo)

        public string GSTStateCode
        {
            get { return gstStateCode; }
            set { gstStateCode = value; }
        }

        public string GSTTaxRegNo
        {
            get { return gstTaxRegNo; }
            set { gstTaxRegNo = value; }
        }

        /// <summary>
        /// Mandatory for FraudLabs. Saved for audit checking for FraudLabs (B2C)
        /// </summary>
        public string StateCode
        {
            get { return stateCode; }
            set { stateCode = value; }
        }

        /// <summary>
        /// Saving TaxBreakup for LCC Airlines after booking. TaxBreakup will be read from Ticket response.
        /// </summary>
        public List<KeyValuePair<string, decimal>> TaxBreakup
        {
            get { return taxBreakup; }
            set { taxBreakup = value; }
        }

        public string MealType
        {
            get { return mealType; }
            set { mealType = value; }
        }
        public string MealDesc
        {
            get { return mealDesc; }
            set { mealDesc = value; }
        }
        public string SeatInfo
        {
            get { return sSeatInfo; }
            set { sSeatInfo = value; }
        }
        public string CorpProfileId
        {
            get { return _CorpProfileId; }
            set { _CorpProfileId = value; }
        }
        #endregion

        /// <summary>
        /// Saves the information contained in Passenger object to flight passenger database
        /// </summary>
        public void Save()
        {
            if (lastName == null || lastName.Length < 1)
                throw new ArgumentException("Last name should be atleast 2 character long", "lastName");

            if (createdBy <= 0)
                throw new ArgumentException("created by must have positive integer value", "createdBy");

            SqlParameter[] paramList = new SqlParameter[33];

            paramList[0] = new SqlParameter("@OFLPAXId", SqlDbType.Int);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@FLTOFLId", flightId);
            paramList[2] = new SqlParameter("@HTLOFLId", hotelId);
            paramList[3] = new SqlParameter("@paxType", GetPTC(type));
            paramList[4] = new SqlParameter("@firstName", firstName);
            paramList[5] = new SqlParameter("@lastName", lastName);
            paramList[6] = new SqlParameter("@title", title);
            if (dateOfBirth != DateTime.MinValue)
                paramList[7] = new SqlParameter("@dateOfBirth", dateOfBirth);
            else
                paramList[7] = new SqlParameter("@dateOfBirth", DBNull.Value);
            paramList[8] = new SqlParameter("@passportNumber", passportNo);            
            paramList[9] = new SqlParameter("@countryCode", country);
            paramList[10] = new SqlParameter("@line1", addressLine1);
            paramList[11] = new SqlParameter("@line2", addressLine2);
            paramList[12] = new SqlParameter("@cellPhone", cellPhone);
            paramList[13] = new SqlParameter("@email", email);
            paramList[14] = new SqlParameter("@leadPax", isLeadPax);
            paramList[15] = new SqlParameter("@mealCode", meal.Code);
            paramList[16] = new SqlParameter("@seatCode", DBNull.Value);
            paramList[17] = new SqlParameter("@ffAirline", ffAirline);
            paramList[18] = new SqlParameter("@ffNumber", ffNumber);
            paramList[19] = new SqlParameter("@priceId", DBNull.Value);
            paramList[20] = new SqlParameter("@createdBy", createdBy);
            if (gender != Gender.Null)
                paramList[21] = new SqlParameter("@gender", (int)gender);
            else
                paramList[21] = new SqlParameter("@gender", DBNull.Value);
            paramList[22] = new SqlParameter("@nationality", nationality);
            paramList[23] = new SqlParameter("@city", city);
            paramList[24] = new SqlParameter("@baggageCode", baggageCode);
            paramList[25] = new SqlParameter("@destinationPhone", destinationPhone);
            paramList[26] = new SqlParameter("@gsttaxregno", gstTaxRegNo);
            paramList[27] = new SqlParameter("@gststatecode", gstStateCode);
            paramList[28] = new SqlParameter("@stateCode", stateCode);
            paramList[29] = new SqlParameter("@mealDesc", mealDesc);
            paramList[30] = new SqlParameter("@CorpProfileId", CorpProfileId);
            paramList[31] = new SqlParameter("@HTLRoomId", RoomId);
            if (PassportExpiry != DateTime.MinValue)
                paramList[32] = new SqlParameter("@PassportExpiry", PassportExpiry);
            else
                paramList[32] = new SqlParameter("@PassportExpiry", DBNull.Value);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddOfflinePassenger, paramList);
            paxId = (int)paramList[0].Value;

            if (flexDetailsList != null && flexDetailsList.Count > 0)
            {
                flexDetailsList = flexDetailsList.Where(x => !string.IsNullOrEmpty(x.FlexLabel)).ToList();
                foreach (FlightFlexDetails flexDet in flexDetailsList)
                {
                    flexDet.FlexData = string.IsNullOrEmpty(flexDet.FlexData) ? string.Empty : flexDet.FlexData;
                    flexDet.PaxId = paxId;
                    flexDet.sIsOffline = "Y"; flexDet.CreatedBy = createdBy;
                    flexDet.Save();
                }
            }
        }
        /// <summary>
        /// Gets the passenger type code for a given PassengerType
        /// </summary>
        /// <param name="type">PassengerType for which PTC is needed</param>
        /// <returns>Passenger type code as string</returns>
        public static string GetPTC(PassengerType type)
        {
            switch (type)
            {
                case PassengerType.Adult:
                    return "ADT";
                case PassengerType.Child:
                    return "CNN";
                case PassengerType.Infant:
                    return "INF";
                case PassengerType.Senior:
                    return "SNN";
                default:
                    return string.Empty;
            }
        }
        /// <summary>
        /// Gets the passenger type code for a given PassengerType string
        /// </summary>
        /// <param name="type">PassengerType string for which PTC is needed</param>
        /// <returns>Passenger type code as PassengerType</returns>
        public static PassengerType GetPassengerType(string passengerType)
        {
            switch (passengerType)
            {
                case "ADT":
                    return PassengerType.Adult;
                case "CNN":
                    return PassengerType.Child;
                case "INF":
                    return PassengerType.Infant;
                case "SNN":
                    return PassengerType.Senior;
                default:
                    return PassengerType.Adult;
            }
        }

        public static OfflinePaxInfo[] GetDummyPaxList(int adult, int child, int infant, int senior)
        {
            //Trace.TraceInformation("Passenger.GettDummyPaxList entered.");
            int[] paxCount = { adult, child, infant, senior };
            OfflinePaxInfo[] paxList = new OfflinePaxInfo[adult + child + infant + senior];
            for (int i = 0, k = 0; i < 4; i++)
            {
                for (int j = 0; j < paxCount[i]; j++, k++)
                {
                    paxList[k] = new OfflinePaxInfo();
                    PassengerType type = (PassengerType)(i + 1);
                    paxList[k].type = type;
                    paxList[k].title = string.Empty;
                    paxList[k].firstName = GetPTC(type) + Convert.ToChar(j + 65);
                    paxList[k].lastName = "TEST";
                    if (type == PassengerType.Infant)
                    {
                        paxList[k].dateOfBirth = DateTime.Now.AddYears(-1);
                    }
                }
            }
            return paxList;
        }

        public static string GetPaxFullName(int paxId)
        {
            //Trace.TraceInformation("FlightItinerary.GetPaxFullName entered : paxId = " + paxId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            StringBuilder fullName = new StringBuilder();
            //SqlConnection connection = DBGateway.GetConnection();
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetPaxFullName, paramList,connection);
            using (DataTable dtPax = DBGateway.FillDataTableSP(SPNames.GetPaxFullName, paramList))
            {
                if (dtPax != null)
                {
                    DataRow data = dtPax.Rows[0];
                    if (data != null)
                    {
                        if (data["title"] != DBNull.Value)
                        {
                            fullName.Append(Convert.ToString(data["title"]));
                            fullName.Append(" ");
                        }
                        if (data["firstName"] != DBNull.Value)
                        {
                            fullName.Append(Convert.ToString(data["firstName"]));
                            fullName.Append(" ");
                        }
                        if (data["lastName"] != DBNull.Value)
                        {
                            fullName.Append(Convert.ToString(data["lastName"]));
                        }
                    }
                }
            }
            return fullName.ToString().Trim();
        }
        /// <summary>
        /// Method to load the passenger details based on hotel id and room id
        /// </summary>
        /// <param name="paxId"></param>
        public List<OfflinePaxInfo> LoadOfflinePax(int OFLPAXId, int FLTOFLId, int HTLOFLId, int HTLRoomId, string sMode)
        {
            List<OfflinePaxInfo> liOfflinePaxInfo = new List<OfflinePaxInfo>();
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@OFLPAXId", OFLPAXId);
            paramList[1] = new SqlParameter("@FLTOFLId", FLTOFLId);
            paramList[2] = new SqlParameter("@HTLOFLId", HTLOFLId);
            paramList[3] = new SqlParameter("@HTLRoomId", HTLRoomId);
            paramList[4] = new SqlParameter("@Mode", sMode);
            DataTable dtPax = DBGateway.FillDataTableSP(SPNames.LoadOfflinePassenger, paramList);
            
            if (dtPax != null)
            {                
                foreach (DataRow dr in dtPax.Rows)
                {
                    liOfflinePaxInfo.Add(GetOfflinePaxInfo(dr));
                }
            }
            return liOfflinePaxInfo;
        }

        public OfflinePaxInfo GetOfflinePaxInfo(DataRow dr)
        {
            OfflinePaxInfo clsOfflinePaxInfo = new OfflinePaxInfo();
            clsOfflinePaxInfo.addressLine1 = Convert.ToString(dr["line1"]);
            clsOfflinePaxInfo.addressLine2 = Convert.ToString(dr["line2"]);
            clsOfflinePaxInfo.city = Convert.ToString(dr["city"]);
            clsOfflinePaxInfo.nationality = Convert.ToString(dr["nationality"]);
            clsOfflinePaxInfo.cellPhone = Convert.ToString(dr["cellPhone"]);
            clsOfflinePaxInfo.dateOfBirth = Convert.ToDateTime(dr["dateOfBirth"]);
            if (dr["gender"] != DBNull.Value)
                clsOfflinePaxInfo.gender = (Gender)Enum.Parse(typeof(Gender), Convert.ToString(dr["gender"]));
            clsOfflinePaxInfo.email = Convert.ToString(dr["email"]);
            clsOfflinePaxInfo.ffAirline = Convert.ToString(dr["FFAirlineCode"]);
            clsOfflinePaxInfo.ffNumber = Convert.ToString(dr["FFNumber"]);
            clsOfflinePaxInfo.flightId = Convert.ToInt32(dr["FLTOFLId"]);
            clsOfflinePaxInfo.firstName = Convert.ToString(dr["firstName"]);
            clsOfflinePaxInfo.isLeadPax = Convert.ToBoolean(dr["leadPax"]);
            clsOfflinePaxInfo.lastName = Convert.ToString(dr["lastName"]);
            clsOfflinePaxInfo.type = GetPassengerType(dr["paxType"].ToString());
            clsOfflinePaxInfo.title = Convert.ToString(dr["title"]);
            clsOfflinePaxInfo.paxId = Convert.ToInt32(dr["PaxId"]);
            clsOfflinePaxInfo.passportNo = Convert.ToString(dr["passportNumber"]);
            clsOfflinePaxInfo.country = Convert.ToString(dr["countryCode"]);
            clsOfflinePaxInfo.createdBy = Convert.ToInt32(dr["createdBy"]);
            clsOfflinePaxInfo.createdOn = Convert.ToDateTime(dr["createdOn"]);
            clsOfflinePaxInfo.lastModifiedBy = Convert.ToInt32(dr["lastModifiedBy"]);
            clsOfflinePaxInfo.lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
            clsOfflinePaxInfo.baggageCode = Convert.ToString(dr["baggageCode"]);
            clsOfflinePaxInfo.gstStateCode = Convert.ToString(dr["GSTStateCode"]);
            clsOfflinePaxInfo.gstTaxRegNo = Convert.ToString(dr["GSTTaxRegNo"]);
            clsOfflinePaxInfo.stateCode = Convert.ToString(dr["stateCode"]);
            return clsOfflinePaxInfo;
        }
    }

    [Serializable]
    public class OfflineSegInfo
    {
        #region Members
        /// <summary>
        /// Unique identity number for a leg of a booking
        /// </summary>
        int segmentId;
        /// <summary>
        /// Flight id to which the leg belongs
        /// </summary>
        int flightId;
        /// <summary>
        /// Name of the airline
        /// </summary>
        string airline;
        /// <summary>
        /// Origin Airport
        /// </summary>
        string origin;
        /// <summary>
        /// Destination Airport
        /// </summary>
        string destination;
        /// <summary>
        /// Flight number
        /// </summary>
        string flightNumber;
        /// <summary>
        /// Departure time at orgin airport
        /// </summary>
        DateTime departureTime;
        /// <summary>
        /// Arrival time at the destination
        /// </summary>
        DateTime arrivalTime;
        /// <summary>
        /// Booking Class
        /// </summary>
        string bookingClass;
        /// <summary>
        /// Booking Cabin Class
        /// </summary>
        CabinClass cabinClass;
        /// <summary>
        /// Arrival terminal of destination airport
        /// </summary>
        string arrTerminal;
        /// <summary>
        /// Departure terminal at origin airport
        /// </summary>
        string depTerminal;
        /// <summary>
        /// Status of flight;
        /// </summary>
        FlightStatus flightStatus;
        /// <summary>
        /// status of flight booking
        /// </summary>
        string status;
        /// <summary>
        /// Meal Type to be served in flight
        /// </summary>
        string mealType;
        /// <summary>
        /// Indicates if e-ticket can be issued
        /// </summary>
        bool eTicketEligible;
        /// <summary>
        /// Duration of the flight
        /// </summary>
        TimeSpan duration;
        /// <summary>
        /// Ground time at origin 
        /// </summary>
        TimeSpan groundTime;
        /// <summary>
        /// Total accumulated duration. Including ground time.
        /// </summary>
        TimeSpan accumulatedDuration;
        /// <summary>
        /// Indicates the origin is stop over, if true.
        /// </summary>
        bool stopOver;
        /// <summary>
        /// Stops in the flight
        /// </summary>
        int stops;
        /// <summary>
        /// Aircraft type code
        /// </summary>
        string craft;
        /// <summary>
        /// Distance in miles
        /// </summary>
        int mile;
        /// <summary>
        /// Airline Direct Resource Locator.
        /// </summary>
        string airlinePNR;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// conjunction number in case of cunjunction ticket.
        /// </summary>
        string conjunctionNo;


        /// <summary>
        /// Fare Info Key for a segment (UAPI)
        /// </summary>
        string fareInfoKey;

        /// <summary>
        /// to store UAPI Departure Date Format
        /// </summary>
        string uapiDepartureTime;

        /// <summary>
        /// to store UAPI Arrival Date Format
        /// </summary>
        string uapiArrivalTime;


        /// <summary>
        /// Group - 0 outbount,1 inbound(UAPI)
        /// </summary>
        int group;

        string operatingCarrier;
        /// <summary>
        /// Stores fare types for FlyDubai like Pay To Change, Free To Change or Basic
        /// </summary>
        string segmentFareType;

        /// <summary>
        /// TourCode returned for Negotiated Fare from UAPI
        /// </summary>
        string tourCode;

        /// <summary>
        /// Preferred time span Morning, evening etc..
        /// </summary>
        string _Preftimespan;
        #endregion

        #region Properties
        public int Group
        {
            get { return group; }
            set { group = value; }
        }


        public string UapiDepartureTime
        {
            get { return uapiDepartureTime; }
            set { uapiDepartureTime = value; }
        }

        public string UapiArrivalTime
        {
            get { return uapiArrivalTime; }
            set { uapiArrivalTime = value; }
        }

        public string ConjunctionNo
        {
            get { return conjunctionNo; }
            set { conjunctionNo = value; }
        }

        public string OperatingCarrier
        {
            get
            {
                return operatingCarrier;
            }
            set
            {
                operatingCarrier = value;
            }
        }

        public int SegmentId
        {
            get
            {
                return segmentId;
            }
            set
            {
                segmentId = value;
            }
        }



        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }

        public string Airline
        {
            get
            {
                return airline;
            }
            set
            {
                airline = value;
            }
        }

        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }

        public string FlightNumber
        {
            get
            {
                return flightNumber;
            }
            set
            {
                flightNumber = value;
            }
        }

        public DateTime DepartureTime
        {
            get
            {
                return departureTime;
            }
            set
            {
                departureTime = value;
            }
        }

        public DateTime ArrivalTime
        {
            get
            {
                return arrivalTime;
            }
            set
            {
                arrivalTime = value;
            }
        }

        public string BookingClass
        {
            get
            {
                return bookingClass;
            }
            set
            {
                bookingClass = value;
            }
        }
        public CabinClass CabinClass
        {
            get
            {
                return cabinClass;
            }
            set
            {
                cabinClass = value;
            }
        }

        public string FareInfoKey
        {
            get
            {
                return fareInfoKey;
            }
            set
            {
                fareInfoKey = value;
            }
        }

        public string ArrTerminal
        {
            get
            {
                return arrTerminal;
            }
            set
            {
                arrTerminal = value;
            }
        }

        public string DepTerminal
        {
            get
            {
                return depTerminal;
            }
            set
            {
                depTerminal = value;
            }
        }

        public FlightStatus FlightStatus
        {
            get
            {
                return flightStatus;
            }
            set
            {
                flightStatus = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string MealType
        {
            get
            {
                return mealType;
            }
            set
            {
                mealType = value;
            }
        }

        public bool ETicketEligible
        {
            get
            {
                return eTicketEligible;
            }
            set
            {
                eTicketEligible = value;
            }
        }

        public string AirlinePNR
        {
            get
            {
                return airlinePNR;
            }
            set
            {
                airlinePNR = value;
            }
        }

        public string Craft
        {
            get
            {
                return craft;
            }
            set
            {
                craft = value;
            }
        }

        public bool StopOver
        {
            get
            {
                return stopOver;
            }
            set
            {
                stopOver = value;
            }
        }

        public int Stops
        {
            get
            {
                return stops;
            }
            set
            {
                stops = value;
            }
        }

        public int Mile
        {
            get
            {
                return mile;
            }
            set
            {
                mile = value;
            }
        }

        [XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan Duration
        {
            get
            {
                return duration;
            }
            set
            {
                duration = value;
            }
        }
        [XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan GroundTime
        {
            get
            {
                return groundTime;
            }
            set
            {
                groundTime = value;
            }
        }
        [XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan AccumulatedDuration
        {
            get
            {
                return accumulatedDuration;
            }
            set
            {
                accumulatedDuration = value;
            }
        }

        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Stores fare types for Fly Dubai like Pay To Change or Free To Change or Basic.
        /// </summary>
        public string SegmentFareType
        {
            get { return segmentFareType; }
            set { segmentFareType = value; }
        }

        /// <summary>
        /// Tour Code returned for Negotiated Fare
        /// </summary>
        public string TourCode
        {
            get { return tourCode; }
            set { tourCode = value; }
        }

        /// <summary>
        /// Preferred time span, morning, evening etc...
        /// </summary>
        public string Preftimespan { get => _Preftimespan; set => _Preftimespan = value; }
        #endregion

        public void Save()
        {
            if (createdBy <= 0)
                throw new ArgumentException("createdBy must have a positive integer value", "createdBy");
            if (flightId == 0)
                throw new ArgumentException("FlightId should have a value", "flightId");
            if (origin == null || origin.Length == 0)
                throw new ArgumentException("Origin ariport code should have a value", "origin");
            if (destination == null || destination.Length == 0)
                throw new ArgumentException("Destination ariport code should have a value", "destination");
            if (departureTime == DateTime.MinValue)
                throw new ArgumentException("departure time should have a value", "departureTime");

            SqlParameter[] paramList = new SqlParameter[12];
            paramList[0] = new SqlParameter("@segmentId", SqlDbType.Int);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@OFLFLTId", FlightId);
            paramList[2] = new SqlParameter("@flightNum", FlightNumber);
            paramList[3] = new SqlParameter("@airlineCode", Airline);
            paramList[4] = new SqlParameter("@depAirport", Origin);
            paramList[5] = new SqlParameter("@arrAirport", Destination);
            paramList[6] = new SqlParameter("@depDateTime", DepartureTime);
            paramList[7] = arrivalTime == DateTime.MinValue ? new SqlParameter("@arrDateTime", DBNull.Value) : new SqlParameter("@arrDateTime", ArrivalTime);
            paramList[8] = new SqlParameter("@cabinClass", CabinClass);
            paramList[9] = new SqlParameter("@status", Status);
            paramList[10] = new SqlParameter("@createdBy", CreatedBy);
            paramList[11] = new SqlParameter("@PrefTimeSpan", Preftimespan);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddOfflineFlightInfo, paramList);
            segmentId = (int)paramList[0].Value;
            
        }

        public DataTable GetDefSegments()
        {
            DataTable dtList = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                dtList = DBGateway.FillDataTableSP("usp_GET_OFFLINE_DEFSEGMENTS", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CCAvenue, Severity.High, 1, ex.ToString(), "");
            }
            return dtList;
        }
    }

    [Serializable]
    public class OfflineItinerary
    {
        #region PrivateMembers
        /// <summary>
        /// Id of flight booked
        /// </summary>
        int flightId;
        /// <summary>
        /// Flight segments
        /// </summary>
        OfflineSegInfo[] segments;
        /// <summary>
        /// Details of other passengers
        /// </summary>
        OfflinePaxInfo[] passenger;
        /// <summary>
        /// PNR of the booking.
        /// </summary>
        string pnr;
        /// <summary>
        /// Booking source/GDS
        /// </summary>
        BookingSource flightBookingSource;
        /// <summary>
        /// Origin airport of the OND
        /// </summary>
        private string origin;
        /// <summary>
        /// Destination airport of the OND
        /// </summary>
        private string destination;
        /// <summary>
        /// Last ticketing date
        /// </summary>
        private DateTime lastTicketDate;
        /// <summary>
        /// Ticket advisory showing additional information or instruction about ticketing.
        /// </summary>
        private string ticketAdvisory;
        /// <summary>
        /// Fare type ( Published, Securate, or ....)
        /// </summary>
        private string fareType;
        /// <summary>
        /// Indicates the number of hits made for generating e-Ticket for this itinerary.
        /// </summary>
        private int eTicketHit;
        /// <summary>
        /// validating airline code
        /// </summary>
        private string airlineCode;
        /// <summary>
        /// first segment departure date is travel date
        /// </summary>
        private DateTime travelDate;
        /// <summary>
        /// Mode how booking is made(Import or Automatic or Manual)
        /// </summary>
        private BookingMode bookingMode;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// pricing type infomation ie "AUTO PRICED"
        /// </summary>
        private string pricingType;
        /// <summary>
        /// Ticket Information as given by worldspan.
        /// </summary>
        private bool ticketed;
        /// <summary>
        /// Flag indicating if the flight is domestic.
        /// Public proper
        /// </summary>
        private bool isDomestic;
        /// <summary>
        /// Mode of payment.
        /// </summary>
        private ModeOfPayment paymentMode;
        /// <summary>
        /// True if the fare is non refundable.
        /// </summary>
        private bool nonRefundable;
        /// <summary>
        /// agencyid of the agency to which the booking belongs
        /// </summary>
        private int agencyId;
        /// <summary>
        /// booking id of this itinerary
        /// </summary>
        private int bookingId;
        /// <summary>
        /// stores the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        string aliasAirlineCode;
        /// <summary>
        /// Tell wheter booking is done by other  
        /// </summary>
        private bool isOurBooking = true;

        /// <summary>
        /// Assignig UAPI UR
        /// </summary>
        string universalRecord;// For 

        /// <summary>
        /// Assignig UAPI Provider PNR (Gelileo)
        /// </summary>
        string airLocatorCode;// For storing Air Reservation Locator code to issue the Ticket 
        string supplierLocatorCode;// For storing Airline Pnr for future info
        int locationId;
        string locationName;
        string specialRequest;

        string guid;
        /// <summary>
        /// Set 'True' if included in Fare for FlyDubai.
        /// </summary>
        private bool isBaggageIncluded;

        /// <summary>
        /// Whether Insurance is added or not 
        /// </summary>
        private bool isInsured;
        /// <summary>
        /// Whether B2B or B2C transaction. Default B2B.
        /// </summary>
        private string transactionType = "B2B";

        /// <summary>
        /// For TBO Air
        /// </summary>
        private bool isLcc;
        /// <summary>
        /// TripId is used for Corporate profile bookings 
        /// which will be grouped based on TripId. Three
        /// bookings will be saved with this TripId in order
        /// to identify whether bookings to corporate or not.
        /// </summary>
        private string tripId;
        /// <summary>
        /// Check whether GST fields need to pass for the supplier while booking for TBOAir
        /// </summary>
        private bool isGSTMandatory;

        private string gstCompanyAddress;

        private string gstCompanyContactNumber;

        private string gstCompanyName;

        private string gstNumber;

        private string gstCompanyEmail;
        //end GST fields for TBOAir

        private string routingTripId;

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        private BookingFlowStatus _sBookRequestType;

        /// <summary>
        /// To hold pending amount for itinerary booking
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        private decimal _dcItineraryAmountDue;

        private bool _isSpecialRoundTrip;//For Combination Search If both onward and return results are SpecialRoundTripFares;

        private string _Bookref;
        private string _RequestType;
        private int _CorpTravelReasonId;
        string _Remarks;

        #endregion

        #region Public Properties

        /// <summary>
        /// Assign booking reference
        /// </summary>
        public string Bookref
        {
            get
            {
                return _Bookref;
            }
            set
            {
                _Bookref = value;
            }
        }

        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public string UniversalRecord
        {
            get
            {
                return universalRecord;
            }
            set
            {
                universalRecord = value;
            }
        }

        public string AirLocatorCode
        {
            get
            {
                return airLocatorCode;
            }
            set
            {
                airLocatorCode = value;
            }
        }
        public string SupplierLocatorCode
        {
            get
            {
                return supplierLocatorCode;
            }
            set
            {
                supplierLocatorCode = value;
            }
        }
        /// <summary>
        /// Gets or sets the flightId
        /// </summary>
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }
        /// <summary>
        /// Gets or sets the flight segments
        /// </summary>
        public OfflineSegInfo[] Segments
        {
            get
            {
                return segments;
            }
            set
            {
                segments = value;
            }
        }
        /// <summary>
        /// Gets or sets other passengers' detail
        /// </summary>
        public OfflinePaxInfo[] Passenger
        {
            get
            {
                return passenger;
            }
            set
            {
                passenger = value;
            }
        }
        /// <summary>
        /// Gets or sets the PNR of booking
        /// </summary>
        public string PNR
        {
            get
            {
                return pnr;
            }
            set
            {
                pnr = value;
            }
        }
        /// <summary>
        /// Gets or sets the flight booking source
        /// </summary>
        public BookingSource FlightBookingSource
        {
            get
            {
                return flightBookingSource;
            }
            set
            {
                flightBookingSource = value;
            }
        }
        /// <summary>
        /// Gets or sets the destination airport of the OND
        /// </summary>
        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }
        /// <summary>
        /// Gets or sets the fare type
        /// </summary>
        public string FareType
        {
            get
            {
                return fareType;
            }
            set
            {
                fareType = value;
            }
        }
        /// <summary>
        /// Gets or sets the last ticketing date
        /// </summary>
        public DateTime LastTicketDate
        {
            get
            {
                return lastTicketDate;
            }
            set
            {
                lastTicketDate = value;
            }
        }
        /// <summary>
        /// Gets or sets ticketAdvisory.
        /// </summary>
        public string TicketAdvisory
        {
            get
            {
                return ticketAdvisory;
            }
            set
            {
                ticketAdvisory = value;
            }
        }
        /// <summary>
        /// Gets or sets the origin airport of the OND
        /// </summary>
        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        /// <summary>
        /// Gets the number of eticket hits made for this itinerary.
        /// </summary>
        public int ETicketHit
        {
            get
            {
                return eTicketHit;
            }
            set
            {
                eTicketHit = value;
            }
        }
        /// <summary>
        /// Gets or sets the origin airport of the OND
        /// </summary>
        public string AirlineCode
        {
            get
            {
                return airlineCode;
            }
            set
            {
                airlineCode = value;
            }
        }
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or Sets PricingType string
        /// </summary>
        public string PricingType
        {
            get
            {
                return pricingType;
            }
            set
            {
                pricingType = value;
            }
        }

        /// <summary>
        /// Gets or Sets LocationId integer
        /// </summary>
        public int LocationId
        {
            get
            {
                return locationId;
            }
            set
            {
                locationId = value;
            }
        }

        public string LocationName
        {
            get
            {
                return locationName;
            }
            set
            {
                locationName = value;
            }
        }

        public bool Ticketed
        {
            get
            {
                return ticketed;
            }
            set
            {
                ticketed = value;
            }
        }

        /// <summary>
        /// Gets or sets the isDomestic field
        /// </summary>
        public bool IsDomestic
        {
            get
            {
                return this.isDomestic;
            }
            set
            {
                this.isDomestic = value;
            }
        }

        public bool IsOurBooking
        {
            get
            {
                return isOurBooking;
            }
            set
            {
                isOurBooking = value;
            }
        }

        /// <summary>
        /// Gets or Sets travelDate field
        /// </summary>
        public DateTime TravelDate
        {
            get
            {
                return this.travelDate;
            }
            set
            {
                this.travelDate = value;
            }
        }
        /// <summary>
        /// Gets or Sets BookingMode field
        /// </summary>
        public BookingMode BookingMode
        {
            get
            {
                if ((int)this.bookingMode == 0)
                {
                    this.bookingMode = BookingMode.Auto;
                }
                return this.bookingMode;
            }
            set
            {
                this.bookingMode = value;
            }
        }

        /// <summary>
        /// Mode of Payment.
        /// </summary>
        public ModeOfPayment PaymentMode
        {
            get
            {
                return this.paymentMode;
            }
            set
            {
                this.paymentMode = value;
            }
        }
        /// <summary>
        /// True if the fare associated is non refundable.
        /// </summary>
        public bool NonRefundable
        {
            get
            {
                return this.nonRefundable;
            }
            set
            {
                this.nonRefundable = value;
            }
        }
        /// <summary>
        /// agency Id to which this booking belongs
        /// </summary>
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        /// <summary>
        /// booking id of this itinerary
        /// </summary>
        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }

        /// <summary>
        /// gets and sets the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        public string AliasAirlineCode
        {
            get
            {
                return aliasAirlineCode;
            }
            set
            {
                aliasAirlineCode = value;
            }
        }
        /// <summary>
        /// Storing Remarks at the time of requesting cancellation
        /// </summary>
        public string SpecialRequest
        {
            get { return specialRequest; }
            set { specialRequest = value; }
        }



        public string GUID
        {
            get { return guid; }
            set { guid = value; }
        }

        /// <summary>
        /// If Baggage Fare is included in Tax details then set 'True' otherwise set 'False'. Added for FlyDubai.
        /// </summary>
        public bool IsBaggageIncluded
        {
            get { return isBaggageIncluded; }
            set { isBaggageIncluded = value; }
        }

        public bool IsInsured
        {
            get
            {
                return isInsured;
            }
            set
            {
                isInsured = value;
            }
        }

        /// <summary>
        /// Whether B2B or B2C transaction. Default B2B.
        /// </summary>
        public string TransactionType
        {
            get { return transactionType; }
            set { transactionType = value; }
        }

        /// <summary>
        /// True or False for TBOAir
        /// </summary>
        public bool IsLCC
        {
            get { return isLcc; }
            set { isLcc = value; }
        }

        public string TripId
        {
            get { return tripId; }
            set { tripId = value; }
        }

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        public BookingFlowStatus BookRequestType
        {
            get
            {
                return _sBookRequestType;
            }
            set
            {
                _sBookRequestType = value;
            }
        }

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        public decimal ItineraryAmountDue
        {
            get
            {
                return _dcItineraryAmountDue;
            }
            set
            {
                _dcItineraryAmountDue = value;
            }
        }
        public string Remarks { get => _Remarks; set => _Remarks = value; }

        #endregion

        #region Some additional Properties

        public bool IsGSTMandatory
        {
            get
            {
                return isGSTMandatory;
            }

            set
            {
                isGSTMandatory = value;
            }
        }

        public string GstCompanyAddress
        {
            get
            {
                return gstCompanyAddress;
            }

            set
            {
                gstCompanyAddress = value;
            }
        }

        public string GstCompanyContactNumber
        {
            get
            {
                return gstCompanyContactNumber;
            }

            set
            {
                gstCompanyContactNumber = value;
            }
        }

        public string GstCompanyName
        {
            get
            {
                return gstCompanyName;
            }

            set
            {
                gstCompanyName = value;
            }
        }

        public string GstNumber
        {
            get
            {
                return gstNumber;
            }

            set
            {
                gstNumber = value;
            }
        }

        public string GstCompanyEmail
        {
            get
            {
                return gstCompanyEmail;
            }

            set
            {
                gstCompanyEmail = value;
            }
        }

        /// <summary>
        /// Routing Identification Id for Onward and Return Itineraries
        /// </summary>
        public string RoutingTripId { get => routingTripId; set => routingTripId = value; }

        /// <summary>
        /// Book request type, if new request or change request
        /// </summary>
        public string RequestType { get => _RequestType; set => _RequestType = value; }

        /// <summary>
        /// Corporate travel reason Id
        /// </summary>
        public int CorpTravelReasonId { get => _CorpTravelReasonId; set => _CorpTravelReasonId = value; }

        /// <summary>
        /// //For Combination Search If both onward and return results are SpecialRoundTripFares;
        /// </summary>
        public bool IsSpecialRoundTrip
        {
            get
            {
                return _isSpecialRoundTrip;
            }

            set
            {
                _isSpecialRoundTrip = value;
            }
        }

        #endregion
        
        #region Constructors
        public OfflineItinerary()
        {
            
        }
        #endregion


        #region Public Methods
        
        public void Save()
        {            
            SqlParameter[] paramList = new SqlParameter[21];

            paramList[0] = new SqlParameter("@OFLFLTId", SqlDbType.Int);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@BookingRef", Bookref);
            //paramList[1].Direction = ParameterDirection.Output;
            paramList[2] = new SqlParameter("@pnr", PNR);
            paramList[3] = new SqlParameter("@universalRecord", universalRecord);
            paramList[4] = new SqlParameter("@airLocatorCode", airLocatorCode);
            paramList[5] = new SqlParameter("@supplierLocatorCode", universalRecord);
            paramList[6] = new SqlParameter("@origin", Origin);
            paramList[7] = new SqlParameter("@destination", Destination);
            paramList[8] = new SqlParameter("@source", (int)FlightBookingSource);
            paramList[9] = new SqlParameter("@isDomestic", IsDomestic);
            paramList[10] = new SqlParameter("@airlineCode", AirlineCode);
            paramList[11] = new SqlParameter("@travelDate", TravelDate);
            paramList[12] = new SqlParameter("@createdBy", CreatedBy);
            paramList[13] = new SqlParameter("@agencyId", AgencyId);
            paramList[14] = new SqlParameter("@aliasAirlineCode", AliasAirlineCode);
            paramList[15] = new SqlParameter("@locationId", locationId);
            paramList[16] = !string.IsNullOrEmpty(tripId) ? new SqlParameter("@RoutingTripId", tripId) : new SqlParameter("@RoutingTripId", DBNull.Value);
            paramList[17] = !string.IsNullOrEmpty(RoutingTripId) ? new SqlParameter("@tripId", RoutingTripId) : new SqlParameter("@tripId", DBNull.Value);
            paramList[18] = new SqlParameter("@RequestType", RequestType);
            paramList[19] = new SqlParameter("@CorpTravelReasonId", CorpTravelReasonId);
            paramList[20] = new SqlParameter("@Remarks", Remarks);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddOfflineItinerary, paramList);
            flightId = (int)paramList[0].Value;
            //Bookref = (string)paramList[1].Value;

            if (segments != null && segments.Length > 0)
            {
                segments = segments.Where(x => !string.IsNullOrEmpty(x.Origin)).ToArray();
                for (int i = 0; i < segments.Length; i++)
                {
                    segments[i].FlightId = flightId;
                    segments[i].Save();
                }
            }
        }

        public DataTable GetOfflineBookingEmail(string BookRefNo)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@BookRefNo", BookRefNo);
                    data = DBGateway.ExecuteReaderSP("usp_GetOffBookEmail", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
            return dt;
        }

        public DataSet GetBookDetails(string sRefKey, string sKeyType)
        {
            if (sRefKey.Length == 0)
                throw new ArgumentException("Offline flight ref key should not be empty", "FlightKey");

            DataSet dsDetails = new DataSet();

            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@RefKey", sRefKey);
            paramList[1] = new SqlParameter("@KeyType", sKeyType);
            try
            {
                dsDetails = DBGateway.ExecuteQuery(SPNames.GetOffFlightDetails, paramList);
            }
            catch (Exception exp)
            {
                throw new ArgumentException("Failed to get Offline flight details", exp);
            }
            return dsDetails;
        }

        public static DataSet GetOfflineFLQueueList(DateTime StartDate, DateTime EndDate, 
            int LocationId, int AgentId, string AgentType, int loginProfile)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];

                paramList[0] = new SqlParameter("@StartDate", StartDate);
                paramList[1] = new SqlParameter("@EndDate", EndDate);
                paramList[2] = new SqlParameter("@LocationId", LocationId);
                paramList[3] = new SqlParameter("@AgentId", AgentId);
                paramList[4] = new SqlParameter("@AgentType", AgentType);
                paramList[5] = new SqlParameter("@LoginProfile", loginProfile);

                dt = DBGateway.ExecuteQuery("usp_Get_OfflineFLQueue_list", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        
        public static int UpdateOffLineStatus(int flightId, string status, int userId, int productId) //To Update the Offline booking Queue status
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@flightId", flightId);
                paramList[1] = new SqlParameter("@Status", status);
                paramList[2] = new SqlParameter("@UserId", userId);
                paramList[3] = new SqlParameter("@ProductId", productId);
                return  DBGateway.ExecuteNonQuery("usp_UpdateOfflineStatus", paramList); 
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public static int SaveOfflineStatusHistory(int offlineId, string status,string remarks,int userId, int productId) //To Save the Offline Booking Queue status History
        {
            try
            {
                SqlParameter[] parametersList = new SqlParameter[5];
                parametersList[0] = new SqlParameter("@Offline_id", offlineId);
                parametersList[1] = new SqlParameter("@Status", status);
                parametersList[2] = new SqlParameter("@Remarks", remarks);
                parametersList[3] = new SqlParameter("@CreatedBy", userId);
                parametersList[4] = new SqlParameter("@ProductId", productId);

                return DBGateway.ExecuteNonQuery("usp_SaveOfflineStatusHisory", parametersList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To save offline approval status and send get emails to send
        /// </summary>
        /// <param name="TransId"></param>
        /// <param name="Approvalstatus"></param>
        /// <param name="Approver"></param>
        /// <param name="Traveller"></param>
        /// <param name="Remarks"></param>
        /// <param name="AgentId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static DataSet SaveOfflineApprovalStatus(int TransId, string Approvalstatus, int Approver, 
            string Remarks, int AgentId, int UserId, string locationEmail, string HostName, int ProductId)
        {
            DataSet ds = new DataSet();
            try
            {
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@TransId", TransId));
                liParams.Add(new SqlParameter("@APPROVAL_STATUS", Approvalstatus));
                liParams.Add(new SqlParameter("@APPROVER", Approver));
                liParams.Add(new SqlParameter("@REMARKS", Remarks));
                liParams.Add(new SqlParameter("@AGENTID", AgentId));
                liParams.Add(new SqlParameter("@USER", UserId));
                liParams.Add(new SqlParameter("@PRODUCT", ProductId));
                ds = DBGateway.ExecuteQuery("usp_Corp_SaveOfflineApproval", liParams.ToArray());
                                
                if (ds != null && ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        DataTable dt = ds.Tables[i];

                        if (dt == null || dt.Rows.Count == 0)
                            continue;

                        string UserName = string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["UserName"])) ? string.Empty :
                            GenericStatic.EncryptData(Convert.ToString(dt.Rows[0]["UserName"]));

                        EmailParams clsParams = new EmailParams();
                        clsParams.FromEmail = CT.Configuration.ConfigurationSystem.Email["fromEmail"];
                        clsParams.ToEmail = Convert.ToString(dt.Rows[0]["EmailTo"]) + (string.IsNullOrEmpty(locationEmail) ? string.Empty : "," + locationEmail);
                        clsParams.CCEmail = Convert.ToString(dt.Rows[0]["CCEmail"]);
                        clsParams.Subject = Convert.ToString(dt.Rows[0]["EmailSub"]);
                        clsParams.EmailBody = Convert.ToString(dt.Rows[0]["EmailBody"]).Replace("@opiadditionalpax", Convert.ToString(dt.Rows[0]["PaxInfo"]))
                            .Replace("@obFlexdata", Convert.ToString(dt.Rows[0]["FlexInfo"])).Replace("@USERNAME", UserName).Replace("@Host", HostName);

                        try { Email.Send(clsParams); } catch (Exception ex) { }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        /// <summary>
        /// To get offline approval data
        /// </summary>
        /// <param name="TransId"></param>
        /// <param name="TransType"></param>
        /// <param name="Approver"></param>
        /// <returns></returns>
        public static DataTable GetOfflineApproval(int TransId, string TransType, int Approver)
        {
            DataTable dt = new DataTable();
            
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@TRANSID", TransId));
                paramList.Add(new SqlParameter("@TRANSTYPE", TransType));
                paramList.Add(new SqlParameter("@APPROVER", Approver));
                dt = DBGateway.FillDataTableSP("usp_Corp_GetApprovalData", paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion
    }
}
