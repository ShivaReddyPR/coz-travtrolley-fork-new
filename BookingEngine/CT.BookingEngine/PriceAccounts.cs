using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
namespace CT.BookingEngine
{
    public enum PriceType
    {
        Default=0,
        PublishedFare = 1,
        NetFare = 2
    }

    [Serializable]
    public class PriceAccounts
    {
        public PriceAccounts()
        {
            publishedFare = 0;
            netFare = 0;
            markup = 0;
            agentCommission = 0;
            agentPLB = 0;
            ourCommission = 0;
            ourPLB = 0;
            tax = 0;
            otherCharges = 0;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = 0;
            tdsPLB = 0;
            serviceTax = 0;
            whiteLabelDiscount = 0;
            transactionFee = 0;
            currency = "";
            airlineTransFee = 0;
            accPriceType = PriceType.PublishedFare;
            cessTax = 0;
            wlCharge = 0;
            discount = 0;
            reverseHandlingCharge = 0;
            commissionType = "RB";
            yqTax = 0;
            k3Tax = 0;
            isServiceTaxOnBaseFarePlusYQ = false;
            tdsRate = 0;
            supplierCurrency = "";
            supplierPrice = 0;
            markupType = "";
            discountType = "";
            decimalPoint = 2;
            b2cMarkup = 0;
            b2cMarkupType = "";
            b2cMarkupValue = 0;
            agentCancelAmount = 0;
            supplierCancelAmount = 0;
            cancelInVat = 0;
            cancelOutVat = 0;
        }
        public PriceAccounts(decimal pFare, decimal nFare, decimal markUP, decimal agentComm, decimal agentPlb, decimal ourComm, decimal ourPlb, decimal TAX, decimal otherCharge, decimal tdsReq, decimal serviceTaxReq, decimal whiteLabelDiscountReq, decimal transactionFeeReq, string curr)
        {
            publishedFare = pFare;
            netFare = nFare;
            markup = markUP;
            agentCommission = agentComm;
            agentPLB = agentPlb;
            ourCommission = ourComm;
            ourPLB = ourPlb;
            tax = TAX;
            otherCharges = otherCharge;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = tdsReq;
            tdsPLB = tdsReq;
            serviceTax = serviceTaxReq;
            whiteLabelDiscount = whiteLabelDiscountReq;
            transactionFee = transactionFeeReq;
            currency = curr;
            commissionType = "RB";
            yqTax = 0;
            k3Tax = 0;
            isServiceTaxOnBaseFarePlusYQ = false;
            markupType = "";
            discountType = "";
            agentCancelAmount = 0;
            supplierCancelAmount = 0;
            cancelInVat = 0;
            cancelOutVat = 0;
        }

        public PriceAccounts(decimal pFare, decimal nFare, decimal markUP, decimal agentComm, decimal agentPlb, decimal ourComm, decimal ourPlb, decimal TAX, decimal otherCharge, decimal tdsReq, decimal serviceTaxReq, decimal whiteLabelDiscountReq, decimal transactionFeeReq, string curr, PriceType pType)
        {
            publishedFare = pFare;
            netFare = nFare;
            markup = markUP;
            agentCommission = agentComm;
            agentPLB = agentPlb;
            ourCommission = ourComm;
            ourPLB = ourPlb;
            tax = TAX;
            otherCharges = otherCharge;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = tdsReq;
            tdsPLB = tdsReq;
            serviceTax = serviceTaxReq;
            whiteLabelDiscount = whiteLabelDiscountReq;
            transactionFee = transactionFeeReq;
            currency = curr;
            accPriceType = pType;
            commissionType = "RB";
            yqTax = 0;
            isServiceTaxOnBaseFarePlusYQ = false;
            markupType = "";
            discountType = "";
            agentCancelAmount = 0;
            supplierCancelAmount = 0;
            cancelInVat = 0;
            cancelOutVat = 0;
        }
        #region Members
        /// <summary>
        /// Unique field for price
        /// </summary>
        private int priceId;
        /// <summary>
        /// Field contained Published Fare of booking
        /// </summary>
        private decimal publishedFare;
        /// <summary>
        /// Field contained netPrice of booking
        /// </summary>
        private decimal netFare;
        /// <summary>
        /// Field contained markup(incase of net fare)
        /// </summary>
        private decimal markup;
        /// <summary>
        /// Field contained tax at that price(in currency)
        /// </summary>
        private decimal tax;
        /// <summary>
        /// Field contains other charges(in currency)
        /// </summary>
        private decimal otherCharges;
        /// <summary>
        /// Field contains our commission
        /// </summary>
        private decimal ourCommission;
        /// <summary>
        /// Field contains our PLB(in currency)
        /// </summary>
        private decimal ourPLB;
        /// <summary>
        /// Commission field(for agent in currency)
        /// </summary>
        private decimal agentCommission;
        /// <summary>
        /// PLB field (for agent in rupees)
        /// </summary>
        private decimal agentPLB;
        ///// <summary>
        ///// Field contained tds of booking(e.g.in currency)
        ///// </summary>
        //private decimal tds;
        /// <summary>
        /// Field contained serviceTax of booking
        /// </summary>
        private decimal serviceTax;
        /// <summary>
        /// Currency string
        /// </summary>
        private string currency;
        private decimal tdsCommission;
        private decimal tdsPLB;
        /// <summary>
        /// Discount given to white label customer
        /// </summary>
        private decimal whiteLabelDiscount;
        /// <summary>
        /// Transaction Fees specially for LCC (based on basefare+fuelSurcharge)
        /// </summary>
        private decimal transactionFee;
        /// <summary>
        /// This contains airline transaction fee - AS Airdeccan has a seperate transaction fee term
        /// </summary>
        private decimal airlineTransFee;
        /// <summary>
        /// This list contains the charge breakup for other charges
        /// </summary>
        private List<ChargeBreakUp> chargeBU;
        /// <summary>
        /// Account Price Type - NetFare or Published fare
        /// </summary>
        private PriceType accPriceType;
        /// <summary>
        /// it will contain Rate of Exchange
        /// </summary>
        private decimal rateOfExchange;
        /// <summary>
        /// This will contain currency code - ie USD
        /// </summary>
        private string currencyCode;
        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare
        /// </summary>
        private decimal additionalTxnFee;
        /// <summary>
        /// Charge taken by agent from WL customer
        /// </summary>
        private decimal wlCharge;
        /// <summary>
        /// This is for train concession
        /// </summary>
        private decimal discount;

        /// <summary>
        /// About to be deducted from Agent Commision for IMport PNR
        /// </summary>
        private decimal reverseHandlingCharge;
        /// <summary>
        /// CommissionType method(RB/RT/V)
        /// </summary>
        private string commissionType;

        /// <summary>
        /// Field contains tdsRate
        /// </summary>
        private decimal tdsRate;
        private bool isServiceTaxOnBaseFarePlusYQ;
        /// <summary>
        /// baggage charge for G9 (Air Arabia)
        /// </summary>
        private decimal baggageCharge;
        private decimal asvAmount;
        private string asvType;
        private string asvElement;
        /// <summary>
        /// Price which is sent by supplier in the XML with currency other than AED.
        /// </summary>
        private decimal supplierPrice;
        /// <summary>
        /// Currency code sent by supplier in the XML other than AED.
        /// </summary>
        private string supplierCurrency;
        /// <summary>
        /// Agent Markup value set for this Hotel Room.
        /// </summary>
        private decimal markupValue;
        /// <summary>
        /// Agent Markup type set for this Hotel Room.
        /// </summary>
        private string markupType;
        /// <summary>
        /// Agent Discount value set for this Hotel Room.
        /// </summary>
        private decimal discountValue;
        /// <summary>
        /// Agent Discount Type set for this Hotel Room.
        /// </summary>
        private string discountType;
        /// <summary>
        /// Agent Decimal Value
        /// </summary>
        private int decimalPoint;
        
        /// <summary>
        /// FareID selected from result XML for FlyDubai.
        /// </summary>
        private List<int> fareInformationId;

        //insurance amount(paxwise) 
        private decimal insuranceAmount;
        /// <summary>
        /// B2C calculated Markup 
        /// </summary>
        private decimal b2cMarkup;
        /// <summary>
        /// B2C Markup Type. Fixed/Percentage.
        /// </summary>
        private string b2cMarkupType;
        /// <summary>
        /// B2C Markup value defined in BKE_MarkupRuleDetails
        /// </summary>
        private decimal b2cMarkupValue;

        private decimal b2cCreditCardCharges;
        /// <summary>
        /// TBO Air
        /// </summary>
        private decimal incentiveEarned;
        /// <summary>
        /// TBO Air
        /// </summary>
        private decimal tdsIncentive;
        /// <summary>
        /// TBO Air
        /// </summary>
        private decimal baseFare;
        /// <summary>
        /// TBO Air
        /// </summary>
        private decimal sserviceFee;

        private PriceTaxDetails taxDetail;

        private decimal inputVATAmount;

        private decimal outputVATAmount;

        private List<GSTTaxDetail> gstDetailsList;
        /// <summary>
        /// P-Percent, F-Fixed
        /// </summary>
        private string handlingFeeType;
        /// <summary>
        /// Value for calculation
        /// </summary>
        private decimal handlingFeeValue;

        /// <summary>
        /// Amount calculated using Type & Value
        /// </summary>
        private decimal handlingFeeAmount;
        private decimal mealCharge;//holds the  meal selection price for both sg and 6e 

        //This holds the tax break up along with code of all the passengers 
        //private Dictionary<String, decimal> paxTaxBreakUp = new Dictionary<String, decimal>();
        private List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
decimal commissionValue;

        /// <summary>
        /// To hold seat price of passenger
        /// </summary>
        private decimal dcSeatPrice;

        private decimal dynamicMarkup;
        private string dynamicMarkupType;
        private decimal dynamicMarkupValue;
        private int thresholdId;
        private List<TaxBreakup> taxBreakups;
        private decimal _FareDiff;
        private decimal agentCancelAmount;
        private decimal supplierCancelAmount;
        private decimal cancelInVat;
        private decimal cancelOutVat;
        /// <summary>
        /// Marketing fee from supplier
        /// </summary>
        private decimal marketingFee;
        /// <summary>
        /// Marketing fee discount percentage
        /// </summary>
        private decimal mfDiscountPercent;
        /// <summary>
        /// Marketing fee discount amount
        /// </summary>
        private decimal mfDiscountAmount;
        #endregion

        #region Properties
        public decimal CommissionValue
        {
            get { return commissionValue; }
            set { commissionValue = value; }
        }
        public string CommissionType
        {
            get { return commissionType; }
            set { commissionType = value; }
        }
        public decimal AirlineTransFee
        {
            get
            {
                return this.airlineTransFee;
            }
            set
            {
                this.airlineTransFee = value;
            }
        }

        public decimal TdsCommission
        {
            get
            {
                return this.tdsCommission;
            }
            set
            {
                this.tdsCommission = value;
            }
        }
        public decimal TDSPLB
        {
            get
            {
                return this.tdsPLB;
            }
            set
            {
                this.tdsPLB = value;
            }
        }
        /// <summary>
        /// Unique property for price
        /// </summary>
        public int PriceId
        {
            get
            {
                return this.priceId;
            }
            set
            {
                this.priceId = value;
            }
        }
        /// <summary>
        /// Property contained Published fare of the booking
        /// </summary>
        public decimal PublishedFare
        {
            get
            {
                return this.publishedFare;
            }
            set
            {
                this.publishedFare = value;
            }
        }
        /// <summary>
        /// Property contained Net Fare on Price
        /// </summary>
        public decimal NetFare
        {
            get
            {
                return this.netFare;
            }
            set
            {
                this.netFare = value;
            }
        }
        /// <summary>
        /// Property contained markup(incase of net fare)
        /// </summary>
        public decimal Markup
        {
            get
            {
                return this.markup;
            }
            set
            {
                this.markup = value;
            }
        }
        /// <summary>
        /// Property contains the service tax of booking
        /// </summary>
        public decimal SeviceTax
        {
            get
            {
                return this.serviceTax;
            }
            set
            {
                this.serviceTax = value;
            }
        }

        ///<summary>
        ///Field to cotain Cess Tax
        ///</summary>

        private decimal cessTax;
        /// <summary>
        /// Property to contain cessTax
        /// </summary>
        public decimal CessTax
        {
            get { return cessTax; }
            set { cessTax = value; }
        }
        ///// <summary>
        ///// property contained TDS of that booking
        ///// </summary>
        //public decimal TDS
        //{
        //    get
        //    {
        //        return this.tds;
        //    }
        //    set
        //    {
        //        this.tds = value;
        //    }
        //}
        /// <summary>
        /// Commission property for us(in currency)
        /// </summary>
        public decimal OurCommission
        {
            get
            {
                return this.ourCommission;
            }
            set
            {
                this.ourCommission = value;
            }
        }
        /// <summary>
        /// PLB property for us(in rupees)
        /// </summary>
        public decimal OurPLB
        {
            get
            {
                return this.ourPLB;
            }
            set
            {
                this.ourPLB = value;
            }
        }
        /// <summary>
        /// Commission property for agent(in rupees)
        /// </summary>
        public decimal AgentCommission
        {
            get
            {
                return this.agentCommission;
            }
            set
            {
                this.agentCommission = value;
            }
        }
        /// <summary>
        /// PLB property for agent(in rupees)
        /// </summary>
        public decimal AgentPLB
        {
            get
            {
                return this.agentPLB;
            }
            set
            {
                this.agentPLB = value;
            }
        }
        /// <summary>
        /// Other charges property for agent(in rupees)
        /// </summary>
        public decimal OtherCharges
        {
            get
            {
                return this.otherCharges;
            }
            set
            {
                this.otherCharges = value;
            }
        }
        /// <summary>
        /// Tax property at that price
        /// </summary>
        public decimal Tax
        {
            get
            {
                return this.tax;
            }
            set
            {
                this.tax = value;
            }
        }
        private decimal k3Tax;
        public decimal K3Tax
        {
            get { return this.k3Tax; }
            set { this.k3Tax = value; }
        }
        /// <summary>
        /// Gets or Sets the white label discount field
        /// </summary>
        public decimal WhiteLabelDiscount
        {
            get
            {
                return this.whiteLabelDiscount;
            }
            set
            {
                this.whiteLabelDiscount = value;
            }
        }
        /// <summary>
        /// Gets or sets TransactionFee field
        /// </summary>
        public decimal TransactionFee
        {
            get
            {
                return this.transactionFee;
            }
            set
            {
                this.transactionFee = value;
            }
        }
        /// <summary>
        /// Currency string for that price
        /// </summary>
        public string Currency
        {
            get
            {
                return this.currency;
            }
            set
            {
                this.currency = value;
            }
        }
        [System.Xml.Serialization.XmlArray("ChargeBU")]
        /// <summary>
        /// Gets or sets the list of charge break up for other charges
        /// </summary>
        public List<ChargeBreakUp> ChargeBU
        {
            get
            {
                return chargeBU;
            }
            set
            {
                chargeBU = value;
            }
        }

        /// <summary>
        /// Gets or sets the priceType
        /// </summary>
        public PriceType AccPriceType
        {
            get
            {
                return accPriceType;
            }
            set
            {
                accPriceType = value;
            }

        }
        /// <summary>
        /// Gets or sets the Rate of Exchange
        /// </summary>
        public decimal RateOfExchange
        {
            get { return rateOfExchange; }
            set { rateOfExchange = value; }
        }
        /// <summary>
        /// Gets or sets the Currency Code
        /// </summary>
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare.
        /// </summary>
        public decimal AdditionalTxnFee
        {
            get
            {
                return additionalTxnFee;
            }
            set
            {
                additionalTxnFee = value;
            }
        }
        public decimal WLCharge
        {
            get
            {
                return wlCharge;
            }
            set
            {
                wlCharge = value;
            }
        }
        public decimal Discount
        {
            get
            {
                return discount;
            }
            set
            {
                discount = value;
            }
        }
        public decimal ReverseHandlingCharge
        {
            get
            {
                return reverseHandlingCharge;
            }
            set
            {
                reverseHandlingCharge = value;
            }
        }

        /// <summary>
        ///Field to contain YQTax. 
        /// </summary>
        private decimal yqTax;
        public decimal YQTax
        {
            get { return yqTax; }
            set { yqTax = value; }
        }

        ///<summary>
        /// Is the service tax calculation use YQ
        ///</summary>
        public bool IsServiceTaxOnBaseFarePlusYQ
        {
            get { return isServiceTaxOnBaseFarePlusYQ; }
            set { isServiceTaxOnBaseFarePlusYQ = value; }
        }

        /// <summary>
        /// Field contains tdsRate
        /// </summary>

        public decimal TdsRate
        {
            get
            {
                return tdsRate;
            }
            set
            {
                tdsRate = value;
            }
        }

        public decimal BaggageCharge
        {
            get { return baggageCharge; }
            set { baggageCharge = value; }
        }
        public decimal AsvAmount
        {
            get { return asvAmount; }
            set { asvAmount = value; }
        }
        public string AsvType
        {
            get { return asvType; }
            set { asvType = value; }
        }
        public string AsvElement
        {
            get { return asvElement; }
            set { asvElement = value; }
        }

        /// <summary>
        /// Price which is sent by supplier in the XML with currency other than AED.
        /// </summary>
        public decimal SupplierPrice
        {
            get { return supplierPrice; }
            set { supplierPrice = value; }
        }

        /// <summary>
        /// Currency code sent by supplier in the XML other than AED.
        /// </summary>
        public string SupplierCurrency
        {
            get { return supplierCurrency; }
            set { supplierCurrency = value; }
        }
        /// <summary>
        /// Agent Markup value set for this Hotel Room.
        /// </summary>
        public decimal MarkupValue
        {
            get { return markupValue; }
            set { markupValue = value; }
        }
        /// <summary>
        /// Agent Markup type set for this Hotel Room.
        /// </summary>
        public string MarkupType
        {
            get { return markupType; }
            set { markupType = value; }
        }
        /// <summary>
        /// Agent Discount value set for this Hotel Room.
        /// </summary>
        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }
        /// <summary>
        /// Agent Discount type set for this Hotel Room.
        /// </summary>
        public string DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }

        /// <summary>
        /// Agent Decimal Point
        /// </summary>
        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }

        
        /// <summary>
        /// FareID selected from XML for FlyDubai.
        /// </summary>
        public List<int> FareInformationID
        {
            get { return fareInformationId; }
            set { fareInformationId = value; }
        }

        /// <summary>
        /// Insurance Amount
        /// </summary>
        public decimal InsuranceAmount
        {
            get { return insuranceAmount; }
            set { insuranceAmount = value; }
        }

        /// <summary>
        /// B2C calculated Markup 
        /// </summary>
        public decimal B2CMarkup
        {
            get { return b2cMarkup; }
            set { b2cMarkup = value; }
        }
        /// <summary>
        /// B2C Markup Type. Fixed/Percentage.
        /// </summary>
        public string B2CMarkupType
        {
            get { return b2cMarkupType; }
            set { b2cMarkupType = value; }
        }
        /// <summary>
        /// B2C Markup value defined in BKE_MarkupRuleDetails
        /// </summary>
        public decimal B2CMarkupValue
        {
            get { return b2cMarkupValue; }
            set { b2cMarkupValue = value; }
        }

        public decimal B2CCreditCardCharges
        {
            get { return b2cCreditCardCharges; }
            set { b2cCreditCardCharges = value; }
        }

        /// <summary>
        /// Base Fare of TBO Air
        /// </summary>
        public decimal BaseFare
        {
            get { return baseFare; }
            set { baseFare = value; }
        }
        /// <summary>
        /// Supplier service fee for TBO Air
        /// </summary>
        public decimal SServiceFee
        {
            get { return sserviceFee; }
            set { sserviceFee = value; }
        }

        /// <summary>
        /// Incentive earned in TBO Air
        /// </summary>
        public decimal IncentiveEarned
        {
            get { return incentiveEarned; }
            set { incentiveEarned = value; }
        }
        /// <summary>
        ///TDS Charged on Incentive for TBO Air
        /// </summary>
        public decimal TDSIncentive
        {
            get { return tdsIncentive; }
            set { tdsIncentive = value; }
        }

        public PriceTaxDetails TaxDetails
        {
            get { return taxDetail; }
            set { taxDetail = value; }
        }

        public decimal InputVATAmount
        {
            get
            {
                return inputVATAmount;
            }

            set
            {
                inputVATAmount = value;
            }
        }

        public decimal OutputVATAmount
        {
            get
            {
                return outputVATAmount;
            }

            set
            {
                outputVATAmount = value;
            }
        }

        public List<GSTTaxDetail> GSTDetailList
        {
            get
            {
                return gstDetailsList;
            }

            set
            {
                gstDetailsList = value;
            }
        }

        public string HandlingFeeType
        {
            get
            {
                return handlingFeeType;
            }

            set
            {
                handlingFeeType = value;
            }
        }

        public decimal HandlingFeeValue
        {
            get
            {
                return handlingFeeValue;
            }

            set
            {
                handlingFeeValue = value;
            }
        }

        public decimal HandlingFeeAmount
        {
            get
            {
                return handlingFeeAmount;
            }

            set
            {
                handlingFeeAmount = value;
            }
        }

        //holds the  meal selection price for both sg and 6e 
        public decimal MealCharge
        {
            get
            {
                return mealCharge;
            }

            set
            {
                mealCharge = value;
            }
        }

        //public Dictionary<String, decimal> PaxTaxBreakUp
        //{
        //    get
        //    {
        //        return paxTaxBreakUp;
        //    }

        //    set
        //    {
        //        paxTaxBreakUp = value;
        //    }
        //}

        public List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> PaxTypeTaxBreakUp
        {
            get
            {
                return paxTypeTaxBreakUp;
            }

            set
            {
                paxTypeTaxBreakUp = value;
            }
        }

        public decimal SeatPrice
        {
            get { return dcSeatPrice; }
            set { dcSeatPrice = value; }
        }

        public decimal DynamicMarkup { get => dynamicMarkup; set => dynamicMarkup = value; }
        public string DynamicMarkupType { get => dynamicMarkupType; set => dynamicMarkupType = value; }
        public decimal DynamicMarkupValue { get => dynamicMarkupValue; set => dynamicMarkupValue = value; }
        public int ThresholdId { get => thresholdId; set => thresholdId = value; }
        /// <summary>
        /// Contains tax breakup values from supplier
        /// </summary>
        public List<TaxBreakup> TaxBreakups { get => taxBreakups; set => taxBreakups = value; }
        /// <summary>
        /// To save fare difference if dynamic HAP credentials applied
        /// </summary>
        public decimal FareDiff { get => _FareDiff; set => _FareDiff = value; }

        /// <summary>
        ///Contains Agent cancellation amount
        /// </summary>
        public decimal AgentCancellationCharge {
            get => agentCancelAmount;
            set => agentCancelAmount= value;
        }
        /// <summary>
        ///Supplier cancellation amount
        /// </summary>
        public decimal SupplierCancellationCharge {
            get => supplierCancelAmount;
            set => supplierCancelAmount = value;
        }
        /// <summary>
        ///cancellation input vat amount
        /// </summary>
        public decimal CancellationInVat
        {
            get => cancelInVat;
            set => cancelInVat = value;
        }
        /// <summary>
        ///cancellation output vat amount
        /// </summary>
        public decimal CancellationOutVat
        {
            get => cancelOutVat;
            set => cancelOutVat = value;
        }
        /// <summary>
        /// Marketing fee from supplier
        /// </summary>
        public decimal MarketingFee { get => marketingFee; set => marketingFee = value; }
        /// <summary>
        /// Marketing fee discount percentage
        /// </summary>
        public decimal MFDiscountPercent { get => mfDiscountPercent; set => mfDiscountPercent = value; }
        /// <summary>
        /// Marketing fee discount amount
        /// </summary>
        public decimal MFDiscountAmount { get => mfDiscountAmount; set => mfDiscountAmount = value; }
        #endregion

        public void Save()
        {
            //Trace.TraceInformation("PriceAccounts.Save entered : ");

            // For few airlines it contains transation fee - adding this in to published fare
            publishedFare = publishedFare + airlineTransFee;
            if (priceId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[63];
                paramList[0] = new SqlParameter("@priceId", priceId);
                paramList[1] = new SqlParameter("@publishedFare", publishedFare);
                paramList[2] = new SqlParameter("@netFare", netFare);
                paramList[3] = new SqlParameter("@markup", markup);
                paramList[4] = new SqlParameter("@tax", tax);
                paramList[5] = new SqlParameter("@ourCommission", ourCommission);
                paramList[6] = new SqlParameter("@ourPLB", ourPLB);
                paramList[7] = new SqlParameter("@agentCommission", (agentCommission));
                paramList[8] = new SqlParameter("@agentPLB", (agentPLB));
                paramList[9] = new SqlParameter("@serviceTax", (serviceTax));
                paramList[10] = new SqlParameter("@tdsCommission", (tdsCommission));
                paramList[11] = new SqlParameter("@tdsPLB", (tdsPLB));
                paramList[12] = new SqlParameter("@otherCharges", (otherCharges));
                paramList[13] = new SqlParameter("@whiteLabelDiscount", (whiteLabelDiscount));
                paramList[14] = new SqlParameter("@transactionFee", transactionFee);
                paramList[15] = new SqlParameter("@currency", currency);
                paramList[16] = new SqlParameter("@priceType", accPriceType);
                paramList[17] = new SqlParameter("@rateOfExchange", rateOfExchange);
                paramList[18] = new SqlParameter("@currencyCode", currencyCode);
                paramList[19] = new SqlParameter("@additionalTxnFee", additionalTxnFee);
                paramList[20] = new SqlParameter("@wlCharge", wlCharge);
                paramList[21] = new SqlParameter("@discount", discount);
                paramList[22] = new SqlParameter("@commissionType", commissionType);
                paramList[23] = new SqlParameter("@reverseHandlingCharge", (reverseHandlingCharge));
                paramList[24] = new SqlParameter("@yqTax", (yqTax));
                paramList[25] = new SqlParameter("@isServiceTaxOnBaseFarePlusYQ", isServiceTaxOnBaseFarePlusYQ);
                paramList[26] = new SqlParameter("@tdsRate", tdsRate);
                paramList[27] = new SqlParameter("@baggageCharge", baggageCharge);
                paramList[28] = new SqlParameter("@asvAmount", asvAmount);
                paramList[29] = new SqlParameter("@asvType", asvType);
                paramList[30] = new SqlParameter("@asvElement", asvElement);
                paramList[31] = new SqlParameter("@markupValue", markupValue);
                paramList[32] = new SqlParameter("@markupType", markupType);
                paramList[33] = new SqlParameter("@discountValue", discountValue);
                paramList[34] = new SqlParameter("@discountType", discountType);
                paramList[35] = new SqlParameter("@sourceCurrency", supplierCurrency);
                paramList[36] = new SqlParameter("@sourceAmount", supplierPrice);
                paramList[37] = new SqlParameter("@B2CMarkup", b2cMarkup);
                paramList[38] = new SqlParameter("@B2CMarkupType", b2cMarkupType);
                paramList[39] = new SqlParameter("@B2CMarkupValue", b2cMarkupValue);
                paramList[40] = new SqlParameter("@IncentiveEarned", incentiveEarned);
                paramList[41] = new SqlParameter("@TdsOnIncentive", tdsIncentive);
                paramList[42] = new SqlParameter("@SServiceFee", sserviceFee);
                paramList[43] = new SqlParameter("@InputVATAmount", inputVATAmount);
                paramList[44] = new SqlParameter("@OutputVATAmount", outputVATAmount);
                paramList[45] = new SqlParameter("@HandlingFeeType", handlingFeeType);
                paramList[46] = new SqlParameter("@HandlingFeeValue", handlingFeeValue);
                paramList[47] = new SqlParameter("@HandlingFeeAmount", handlingFeeAmount);
                paramList[48] = new SqlParameter("@mealCharge", mealCharge);
                paramList[49] = new SqlParameter("@CommissionValue", commissionValue);
                paramList[50] = new SqlParameter("@DynamicMarkup", dynamicMarkup);
                paramList[51] = new SqlParameter("@DynamicMarkupType", dynamicMarkupType);
                paramList[52] = new SqlParameter("@DynamicMarkupValue", dynamicMarkupValue);
                paramList[53] = new SqlParameter("@ThresholdId", thresholdId);
                paramList[54] = new SqlParameter("@SeatPrice", SeatPrice);
                paramList[55] = new SqlParameter("@DHAP_FareDiff", FareDiff);
                paramList[56] = new SqlParameter("@AgentCancelCharge", agentCancelAmount);
                paramList[57] = new SqlParameter("@SupplierCancelCharge", supplierCancelAmount);
                paramList[58] = new SqlParameter("@CancelInVat", cancelInVat);
                paramList[59] = new SqlParameter("@CancelOutVat", cancelOutVat);
                paramList[60] = new SqlParameter("@marketingFee", marketingFee);
                paramList[61] = new SqlParameter("@mfDiscountPercent", mfDiscountPercent);
                paramList[62] = new SqlParameter("@mfDiscountAmount", mfDiscountAmount);

                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdatePrice, paramList);
                //Trace.TraceInformation("PriceAccounts.Save exiting : rowsAffected = " + rowsAffected);
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[62];
                paramList[0] = new SqlParameter("@priceId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
                //paramList[1] = new SqlParameter("@publishedFare", Math.Round(publishedFare, decimalPoint));
                //paramList[2] = new SqlParameter("@netFare", Math.Round(netFare, decimalPoint));
                //paramList[3] = new SqlParameter("@markup", Math.Round(markup, decimalPoint));
                //paramList[4] = new SqlParameter("@tax", Math.Round(tax, decimalPoint));
                //paramList[5] = new SqlParameter("@ourCommission", Math.Round(ourCommission, decimalPoint));
                //paramList[6] = new SqlParameter("@ourPLB", Math.Round(ourPLB, decimalPoint));
                //paramList[7] = new SqlParameter("@agentCommission", Math.Round(agentCommission, decimalPoint));
                //paramList[8] = new SqlParameter("@agentPLB", Math.Round(agentPLB, decimalPoint));
                //paramList[9] = new SqlParameter("@serviceTax", Math.Round(serviceTax, decimalPoint));
                //paramList[10] = new SqlParameter("@tdsCommission", Math.Round(tdsCommission, decimalPoint));
                //paramList[11] = new SqlParameter("@tdsPLB", Math.Round(tdsPLB, decimalPoint));
                //paramList[12] = new SqlParameter("@otherCharges", Math.Round(otherCharges, decimalPoint));
                //paramList[13] = new SqlParameter("@whiteLabelDiscount", Math.Round(whiteLabelDiscount, decimalPoint));
                //paramList[14] = new SqlParameter("@transactionFee", Math.Round(transactionFee, decimalPoint));
                //paramList[15] = new SqlParameter("@currency", currency);
                //paramList[16] = new SqlParameter("@priceType", accPriceType);
                //paramList[17] = new SqlParameter("@rateOfExchange", rateOfExchange);
                //paramList[18] = new SqlParameter("@currencyCode", currencyCode);
                //paramList[19] = new SqlParameter("@additionalTxnFee", Math.Round(additionalTxnFee, decimalPoint));
                //paramList[20] = new SqlParameter("@wlCharge", wlCharge);
                //paramList[21] = new SqlParameter("@discount", discount);
                //paramList[22] = new SqlParameter("@commissionType", commissionType);
                //paramList[23] = new SqlParameter("@reverseHandlingCharge", Math.Round(reverseHandlingCharge, decimalPoint));
                //paramList[24] = new SqlParameter("@yqTax", Math.Round(yqTax, decimalPoint));

                //Removed decimal rounding off while saving for Testing TODO:Ziyad
                paramList[1] = new SqlParameter("@publishedFare", publishedFare);
                paramList[2] = new SqlParameter("@netFare", netFare);
                paramList[3] = new SqlParameter("@markup", markup);
                paramList[4] = new SqlParameter("@tax", tax);
                paramList[5] = new SqlParameter("@ourCommission", ourCommission);
                paramList[6] = new SqlParameter("@ourPLB", ourPLB);
                paramList[7] = new SqlParameter("@agentCommission", (agentCommission));
                paramList[8] = new SqlParameter("@agentPLB", (agentPLB));
                paramList[9] = new SqlParameter("@serviceTax", (serviceTax));
                paramList[10] = new SqlParameter("@tdsCommission", (tdsCommission));
                paramList[11] = new SqlParameter("@tdsPLB", (tdsPLB));
                paramList[12] = new SqlParameter("@otherCharges", (otherCharges));
                paramList[13] = new SqlParameter("@whiteLabelDiscount", (whiteLabelDiscount));
                paramList[14] = new SqlParameter("@transactionFee", transactionFee);
                paramList[15] = new SqlParameter("@currency", currency);
                paramList[16] = new SqlParameter("@priceType", accPriceType);
                paramList[17] = new SqlParameter("@rateOfExchange", rateOfExchange);
                paramList[18] = new SqlParameter("@currencyCode", currencyCode);
                paramList[19] = new SqlParameter("@additionalTxnFee", additionalTxnFee);
                paramList[20] = new SqlParameter("@wlCharge", wlCharge);
                paramList[21] = new SqlParameter("@discount", discount);
                paramList[22] = new SqlParameter("@commissionType", commissionType);
                paramList[23] = new SqlParameter("@reverseHandlingCharge", (reverseHandlingCharge));
                paramList[24] = new SqlParameter("@yqTax", (yqTax));
                paramList[25] = new SqlParameter("@isServiceTaxOnBaseFarePlusYQ", isServiceTaxOnBaseFarePlusYQ);
                paramList[26] = new SqlParameter("@tdsRate", tdsRate);
                paramList[27] = new SqlParameter("@baggageCharge", baggageCharge);
                paramList[28] = new SqlParameter("@asvAmount", asvAmount);
                paramList[29] = new SqlParameter("@asvType", asvType);
                paramList[30] = new SqlParameter("@asvElement", asvElement);
                paramList[31] = new SqlParameter("@markupValue", markupValue);
                paramList[32] = new SqlParameter("@markupType", markupType);
                paramList[33] = new SqlParameter("@discountValue", discountValue);
                paramList[34] = new SqlParameter("@discountType", discountType);
                paramList[35] = new SqlParameter("@sourceCurrency", supplierCurrency);
                paramList[36] = new SqlParameter("@sourceAmount", supplierPrice);
                paramList[37] = new SqlParameter("@B2CMarkup", b2cMarkup);
                paramList[38] = new SqlParameter("@B2CMarkupType", b2cMarkupType);
                paramList[39] = new SqlParameter("@B2CMarkupValue", b2cMarkupValue);
                paramList[40] = new SqlParameter("@IncentiveEarned", incentiveEarned);
                paramList[41] = new SqlParameter("@TdsOnIncentive", tdsIncentive);
                paramList[42] = new SqlParameter("@SServiceFee", sserviceFee);
                paramList[43] = new SqlParameter("@InputVATAmount", inputVATAmount);//TODO: Testing with multiple pax
                paramList[44] = new SqlParameter("@OutputVATAmount", outputVATAmount);//TODO: Testing with multiple pax
                paramList[45] = new SqlParameter("@HandlingFeeType", handlingFeeType);
                paramList[46] = new SqlParameter("@HandlingFeeValue", handlingFeeValue);
                paramList[47] = new SqlParameter("@HandlingFeeAmount", handlingFeeAmount);
                paramList[48] = new SqlParameter("@mealCharge", mealCharge);
                paramList[49] = new SqlParameter("@CommissionValue", commissionValue);
                paramList[50] = new SqlParameter("@DynamicMarkup", dynamicMarkup);
                paramList[51] = new SqlParameter("@DynamicMarkupType", dynamicMarkupType);
                paramList[52] = new SqlParameter("@DynamicMarkupValue", dynamicMarkupValue);
                paramList[53] = new SqlParameter("@ThresholdId", thresholdId);
                paramList[54] = new SqlParameter("@SeatPrice", SeatPrice);
                paramList[55] = new SqlParameter("@AgentCancelCharge", agentCancelAmount);
                paramList[56] = new SqlParameter("@SupplierCancelCharge", supplierCancelAmount);
                paramList[57] = new SqlParameter("@CancelInVat", cancelInVat);
                paramList[58] = new SqlParameter("@CancelOutVat", cancelOutVat);
                paramList[59] = new SqlParameter("@marketingFee", marketingFee);
                paramList[60] = new SqlParameter("@mfDiscountPercent", mfDiscountPercent);
                paramList[61] = new SqlParameter("@mfDiscountAmount", mfDiscountAmount);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddPrice, paramList);
                priceId = (int)paramList[0].Value;
                for (int i = 0; i < chargeBU.Count; i++)
                {
                    chargeBU[i].PriceId =  this.priceId;
                    chargeBU[i].Save();
                }
                //Save VAT Details
                if (taxDetail != null && priceId > 0)
                {
                    taxDetail.PriceId = priceId;
                    taxDetail.Save();
                }
                //Save GST Details
                if (gstDetailsList != null)
                {
                    for (int i = 0; i < gstDetailsList.Count; i++)
                    {
                        gstDetailsList[i].PriceId = this.priceId;
                        gstDetailsList[i].Save();
                    }
                }
                //Trace.TraceInformation("PriceAccounts.Save exiting : rowsAffected = " + rowsAffected);
            }
        }

        public void Load(int priceId)
        {
            //Trace.TraceInformation("PriceAccounts.Load entered : ");
            this.priceId = priceId;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@priceId", priceId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPriceDetail, paramList,connection);
            using (DataTable dtPrice = DBGateway.FillDataTableSP(SPNames.GetPriceDetail, paramList))
            {
                if (dtPrice != null && dtPrice.Rows.Count > 0)
                {
                    foreach (DataRow data in dtPrice.Rows)
                    {
                        publishedFare = Convert.ToDecimal(data["publishedFare"]);
                        netFare = Convert.ToDecimal(data["netFare"]);
                        tax = Convert.ToDecimal(data["tax"]);
                        markup = Convert.ToDecimal(data["markup"]);
                        ourCommission = Convert.ToDecimal(data["ourCommission"]);
                        ourPLB = Convert.ToDecimal(data["ourPLB"]);
                        agentCommission = Convert.ToDecimal(data["agentCommission"]);
                        agentPLB = Convert.ToDecimal(data["agentPLB"]);
                        serviceTax = Convert.ToDecimal(data["serviceTax"]);
                        tdsRate = Convert.ToDecimal(data["tdsRate"]);
                        asvAmount = Convert.ToDecimal(data["asvamount"]);
                        asvElement = Convert.ToString(data["asvElement"]);
                        //tds = Convert.ToDecimal(data["tds"]);
                        if (data["tdsCommission"] != DBNull.Value)
                        {
                            tdsCommission = Convert.ToDecimal(data["tdsCommission"]);
                        }
                        if (data["tdsPLB"] != DBNull.Value)
                        {
                            tdsPLB = Convert.ToDecimal(data["tdsPLB"]);
                        }
                        otherCharges = Convert.ToDecimal(data["otherCharges"]);
                        whiteLabelDiscount = Convert.ToDecimal(data["whiteLabelDiscount"]);
                        transactionFee = Convert.ToDecimal(data["transactionFee"]);
                        accPriceType = (PriceType)data["priceType"];
                        if(data["IncentiveEarned"] != DBNull.Value)
                        {
                            incentiveEarned = Convert.ToDecimal(data["IncentiveEarned"]);
                        }
                        if(data["TDSOnIncentive"] != DBNull.Value)
                        {
                            tdsIncentive = Convert.ToDecimal(data["TDSOnIncentive"]);
                        }
                        currency = Convert.ToString(data["currency"]);
                        if (data["rateofExchange"] != DBNull.Value)
                        {
                            rateOfExchange = Convert.ToDecimal(data["rateofExchange"]);
                        }
                        if (data["currencyCode"] != DBNull.Value)
                        {
                            currencyCode = data["currencyCode"].ToString();
                        }
                        additionalTxnFee = Convert.ToDecimal(data["additionalTxnFee"]);
                        wlCharge = Convert.ToDecimal(data["wlCharge"]);
                        discount = Convert.ToDecimal(data["discount"]);
                        if (data["commissionType"] != DBNull.Value)
                        {
                            commissionType = Convert.ToString(data["commissionType"]);
                        }
                        if (data["reverseHandlingCharge"] != DBNull.Value)
                        {
                            reverseHandlingCharge = Convert.ToDecimal(data["reverseHandlingCharge"]);
                        }
                        if (data["yqTax"] != DBNull.Value)
                        {
                            yqTax = Convert.ToDecimal(data["yqTax"]);
                        }
                        isServiceTaxOnBaseFarePlusYQ = Convert.ToBoolean(data["isServiceTaxOnBaseFarePlusYQ"]);

                        if (data["baggageCharge"] != DBNull.Value)
                        {
                            baggageCharge = Convert.ToDecimal(data["baggageCharge"]);
                        }
                        ChargeBreakUp cbu = new ChargeBreakUp();
                        chargeBU = cbu.Load(priceId);
                        if (data["B2CMarkup"] != DBNull.Value)
                        {
                            b2cMarkup = Convert.ToDecimal(data["B2CMarkup"]);
                            b2cMarkupType = Convert.ToString(data["B2CMarkupType"]);
                            b2cMarkupValue = Convert.ToDecimal(data["B2CMarkupValue"]);
                        }
                        if (data["invat_amount"] != DBNull.Value)
                        {
                            inputVATAmount = Convert.ToDecimal(data["invat_amount"]);
                        }
                        if (data["outvat_amount"] != DBNull.Value)
                        {
                            outputVATAmount = Convert.ToDecimal(data["outvat_amount"]);
                        }
                        if (data["handlingFeeType"] != DBNull.Value)
                        {
                            handlingFeeType = Convert.ToString(data["handlingFeeType"]);
                        }
                        if (data["handlingFeeValue"] != DBNull.Value)
                        {
                            handlingFeeValue = Convert.ToDecimal(data["handlingFeeValue"]);
                        }
                        if (data["handlingFeeAmount"] != DBNull.Value)
                        {
                            handlingFeeAmount = Convert.ToDecimal(data["handlingFeeAmount"]);
                        }
                        if (data["mealCharge"] != DBNull.Value)
                        {
                            mealCharge = Convert.ToDecimal(data["mealCharge"]);
                        }
                        if (data["SeatPrice"] != DBNull.Value)
                        {
                            SeatPrice = Convert.ToDecimal(data["SeatPrice"]);
                        }
                        if (data["asvType"] != DBNull.Value)
                        {
                            AsvType = Convert.ToString(data["asvType"]);
                        }
                        if (data["SourceAmount"] != DBNull.Value)
                        {
                            supplierPrice = Convert.ToDecimal(data["SourceAmount"]);
                        }
                        if (data["SourceCurrency"] != DBNull.Value)
                        {
                            supplierCurrency = Convert.ToString(data["SourceCurrency"]);
                        }
                        if(data["markupValue"]!=DBNull.Value)
                        {
                            MarkupValue = Convert.ToDecimal(data["markupValue"]);
                        }
                        if(data["discountValue"] != DBNull.Value)
                        {
                            DiscountValue = Convert.ToDecimal(data["discountValue"]);
                        }
                        FareDiff = data["DHAP_FareDiff"] != DBNull.Value ? FareDiff = Convert.ToDecimal(data["DHAP_FareDiff"]) : 0m;
                        if (data["commissionValue"] != DBNull.Value)
                        {
                            CommissionValue = Convert.ToDecimal(data["commissionValue"]);
                        }
                        MarketingFee = data["MarketingFee"] != DBNull.Value ? Convert.ToDecimal(data["MarketingFee"]) : 0m;
                        MFDiscountPercent = data["MFDiscountPercent"] != DBNull.Value ? Convert.ToDecimal(data["MFDiscountPercent"]) : 0m;
                        MFDiscountAmount = data["MFDiscountAmount"] != DBNull.Value ? Convert.ToDecimal(data["MFDiscountAmount"]) : 0m;
                    }

                }
                else
                {
                    new ArgumentException("This price id is not present in database");
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("PriceAccounts.Load exiting :");
        }

        public void LoadForCreditNote(int itemId, int itemTypeId)
        {
            //Trace.TraceInformation("PriceAccounts.LoadForCreditNote entered : ");
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@itemId", itemId);
            paramList[1] = new SqlParameter("@itemTypeId", itemTypeId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPriceDetailForCreditNote, paramList,connection);
            using (DataTable dataPrice = DBGateway.FillDataTableSP(SPNames.GetPriceDetailForCreditNote, paramList))
            {
                if (dataPrice != null && dataPrice.Rows.Count > 0)
                {
                    foreach (DataRow data in dataPrice.Rows)
                    {
                        this.priceId = Convert.ToInt32(data["priceId"]);
                        publishedFare = Convert.ToDecimal(data["publishedFare"]);
                        netFare = Convert.ToDecimal(data["netFare"]);
                        tax = Convert.ToDecimal(data["tax"]);
                        markup = Convert.ToDecimal(data["markup"]);
                        ourCommission = Convert.ToDecimal(data["ourCommission"]);
                        ourPLB = Convert.ToDecimal(data["ourPLB"]);
                        agentCommission = Convert.ToDecimal(data["agentCommission"]);
                        agentPLB = Convert.ToDecimal(data["agentPLB"]);
                        serviceTax = Convert.ToDecimal(data["serviceTax"]);
                        //tds = Convert.ToDecimal(data["tds"]);
                        if (data["tdsCommission"] != DBNull.Value)
                        {
                            tdsCommission = Convert.ToDecimal(data["tdsCommission"]);
                        }
                        if (data["tdsPLB"] != DBNull.Value)
                        {
                            tdsPLB = Convert.ToDecimal(data["tdsPLB"]);
                        }
                        otherCharges = Convert.ToDecimal(data["otherCharges"]);
                        whiteLabelDiscount = Convert.ToDecimal(data["whiteLabelDiscount"]);
                        transactionFee = Convert.ToDecimal(data["transactionFee"]);
                        accPriceType = (PriceType)data["priceType"];

                        currency = data["currency"].ToString();
                        if (data["rateofExchange"] != DBNull.Value)
                        {
                            rateOfExchange = Convert.ToDecimal(data["rateofExchange"]);
                        }
                        if (data["currencyCode"] != DBNull.Value)
                        {
                            currencyCode = Convert.ToString(data["currencyCode"]);
                        }
                        additionalTxnFee = Convert.ToDecimal(data["additionalTxnFee"]);
                        wlCharge = Convert.ToDecimal(data["wlCharge"]);
                        discount = Convert.ToDecimal(data["discount"]);
                        ChargeBreakUp cbu = new ChargeBreakUp();
                        chargeBU = cbu.Load(priceId);
                        if (data["commissionType"] != DBNull.Value)
                        {
                            commissionType = Convert.ToString(data["commissionType"]);
                        }
                        if (data["reverseHandlingCharge"] != DBNull.Value)
                        {
                            reverseHandlingCharge = Convert.ToDecimal(data["reverseHandlingCharge"]);
                        }
                        if (data["yqTax"] != DBNull.Value)
                        {
                            yqTax = Convert.ToDecimal(data["yqTax"]);
                        }
                        isServiceTaxOnBaseFarePlusYQ = Convert.ToBoolean(data["isServiceTaxOnBaseFarePlusYQ"]);
                        if (data["baggageCharge"] != DBNull.Value)
                        {
                            baggageCharge = Convert.ToDecimal(data["baggageCharge"]);
                        }
                    }
                }
                else
                {
                    new ArgumentException("This price id is not present in database");
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("PriceAccounts.Load exiting :");
        }

        public Decimal GetAgentPrice()
        {
            decimal agentPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                //agentPrice = (publishedFare + airlineTransFee + serviceTax + tax + tdsCommission + tdsPLB + otherCharges - agentPLB - agentCommission + transactionFee + reverseHandlingCharge);
                agentPrice = (publishedFare + tax  );//removed transactionFee for FlyDubai.
            }
            else if (accPriceType == PriceType.NetFare)
            {
                agentPrice = (netFare + markup + otherCharges - discount); // + serviceTax + tax  Commented by brahmam
            }
            return agentPrice;
        }

        public Decimal GetServiceAgentPrice()
        {
            decimal agentPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                agentPrice = (publishedFare + airlineTransFee + serviceTax + tax + otherCharges + transactionFee);
            }
            else if (accPriceType == PriceType.NetFare)
            {
                agentPrice = (netFare + markup + serviceTax + tax + otherCharges);
            }
            return Math.Round(agentPrice, Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
        }

        public decimal GetOfferdPrice()
        {
            decimal offPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                offPrice = (publishedFare + airlineTransFee + tax - agentPLB - agentCommission + transactionFee);
            }
            else if (accPriceType == PriceType.NetFare)
            {
                offPrice = (netFare + markup + tax);
            }
            return offPrice;
        }

        public Decimal GetOfferedPriceWL()
        {
            decimal offPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                offPrice = (publishedFare + airlineTransFee + tax - whiteLabelDiscount);
            }
            else if (accPriceType == PriceType.NetFare)
            {
                offPrice = (netFare + markup + tax); //TODO: WhiteLabelDiscount
            }
            return offPrice;
        }
        /// <summary>
        /// Gets the list of price id's for all  the  unpaid invoices in the current fortnight
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static List<int> GetPriceIdListOfUnpaidInvoicesInCurrentFortnight(int agencyId)
        {
            //Trace.TraceInformation("Price.GetPriceIdListOfUnpaidInvoicesInCurrentFortnight entered agencyId=" + agencyId);
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            List<int> priceList = new List<int>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            //SqlConnection connection = DBGateway.GetConnection();
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPriceIdOfUnpaidInvoicesInCurrentFortnight, paramList,connection);
            using (DataTable dataPrice = DBGateway.FillDataTableSP(SPNames.GetPriceIdOfUnpaidInvoicesInCurrentFortnight, paramList))
            {
                if (dataPrice != null && dataPrice.Rows.Count > 0)
                {
                    foreach(DataRow data in dataPrice.Rows)
                    {
                        if (data["priceId"] != DBNull.Value)
                        {
                            priceList.Add(Convert.ToInt32(data["priceId"]));
                        }
                    }
                }
            }
           // data.Close();
            //connection.Close();
            //Trace.TraceInformation("Price.GetPriceIdListOfUnpaidInvoicesInCurrentFortnight exiting");
            return priceList;
        }
        /// <summary>
        /// Gets the total unpaid amount against raised and partially paid invoices of the current fortnight
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static decimal GetTotalAmountOfUnpaidInvoicesInCurrentFortnight(int agencyId)
        {
            //Trace.TraceInformation("Price.GetTotalAmountOfUnpaidInvoicesInCurrentFortnight entered agencyId=" + agencyId);
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            decimal totalAmount = 0;
            List<int> priceList = GetPriceIdListOfUnpaidInvoicesInCurrentFortnight(agencyId);
            string commaSeperatedPriceIdList = string.Empty;
            foreach (int priceId in priceList)
            {
                commaSeperatedPriceIdList = commaSeperatedPriceIdList + ", " + priceId;
            }
            if (commaSeperatedPriceIdList == string.Empty)
            {
                return 0;
            }
            commaSeperatedPriceIdList = commaSeperatedPriceIdList.TrimStart(',');
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@priceIdList", commaSeperatedPriceIdList);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPriceForPriceIdList, paramList,connection);
            while (data.Read())
            {
                PriceAccounts price = new PriceAccounts();
                price.publishedFare = Convert.ToDecimal(data["publishedFare"]);
                price.netFare = Convert.ToDecimal(data["netFare"]);
                price.tax = Convert.ToDecimal(data["tax"]);
                price.markup = Convert.ToDecimal(data["markup"]);
                price.ourCommission = Convert.ToDecimal(data["ourCommission"]);
                price.ourPLB = Convert.ToDecimal(data["ourPLB"]);
                price.agentCommission = Convert.ToDecimal(data["agentCommission"]);
                price.agentPLB = Convert.ToDecimal(data["agentPLB"]);
                price.serviceTax = Convert.ToDecimal(data["serviceTax"]);
                if (data["tdsCommission"] != DBNull.Value)
                {
                    price.tdsCommission = Convert.ToDecimal(data["tdsCommission"]);
                }
                if (data["tdsPLB"] != DBNull.Value)
                {
                    price.tdsPLB = Convert.ToDecimal(data["tdsPLB"]);
                }
                price.otherCharges = Convert.ToDecimal(data["otherCharges"]);
                price.whiteLabelDiscount = Convert.ToDecimal(data["whiteLabelDiscount"]);
                price.transactionFee = Convert.ToDecimal(data["transactionFee"]);
                price.accPriceType = (PriceType)data["priceType"];
                price.currency = data["currency"].ToString();
                if (data["rateofExchange"] != DBNull.Value)
                {
                    price.rateOfExchange = Convert.ToDecimal(data["rateofExchange"]);
                }
                if (data["currencyCode"] != DBNull.Value)
                {
                    price.currencyCode = data["currencyCode"].ToString();
                }
                price.additionalTxnFee = Convert.ToDecimal(data["additionalTxnFee"]);
                price.wlCharge = Convert.ToDecimal(data["wlCharge"]);
                totalAmount = totalAmount + price.GetAgentPrice();
            }
            totalAmount = totalAmount - Invoice.GetAmountAgainstPartialInvoicesOfCurrentFortnight(agencyId);
            data.Close();
            connection.Close();
            //Trace.TraceInformation("Price.GetTotalAmountOfUnpaidInvoicesInCurrentFortnight exiting");
            return totalAmount;
        }
        public decimal GetTrainTicketPrice()
        {
            decimal totalPrice = publishedFare - discount + markup + transactionFee;
            foreach (ChargeBreakUp breakUp in ChargeBU)
            {
                totalPrice += breakUp.Amount;
            }
            return totalPrice;
        }
        /// <summary>
        /// Gets the array of IRCTC fare 
        /// 0 -> ourPrice
        /// 1 -> irctcPrice
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static decimal[] GetIRCTCFare(IRCTCFareClass fareClass, int passengerCount, int agencyId)
        {
            //Trace.TraceInformation("Price.GetIRCTCFare entered ");
            if (passengerCount <= 0 || passengerCount > 6)
            {
                throw new ArgumentException("Passenger count must be between 1 and 6");
            }
            decimal[] irctcFare = new decimal[2];
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@AgencyId", agencyId);
            paramList[1] = new SqlParameter("@class", (int)fareClass);
            paramList[2] = new SqlParameter("@passengerCount", passengerCount);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetIRCTCPriceForAgent, paramList,connection);
            if (data.Read())
            {
                irctcFare[0] = Convert.ToInt32(data["ourPrice"]);
                irctcFare[1] = Convert.ToInt32(data["irctcPrice"]);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("Price.GetIRCTCFare entered");
            return irctcFare;
        }

        /// <summary>
        /// To get insurance price for agent
        /// </summary>
        /// <returns></returns>
        public decimal GetAgentInsurancePrice()
        {
            decimal agentPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                agentPrice = (publishedFare + serviceTax - agentCommission);
            }
            return Math.Round(agentPrice, Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
        }

        public decimal GetAgentFleetPrice()
        {
            decimal agentPrice = 0;
            if (accPriceType == PriceType.NetFare)
            {
                agentPrice = (netFare + markup - discount + tax);
            }
            return agentPrice;
        }


        /// <summary>
        /// To get list of price of More than one passenger 
        /// </summary>
        /// <returns></returns>
        public static List<PriceAccounts> GetPriceList(string priceIdString)
        {
            //Trace.TraceInformation("Price.GetPriceList entered priceIdString=" + priceIdString);
            if (priceIdString == "")
            {
                throw new ArgumentException("PriceIdString must have atleast one priceid", "priceIdString");
            }
            List<PriceAccounts> priceList = new List<PriceAccounts>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@priceId", priceIdString);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPriceList, paramList,connection);
            while (data.Read())
            {
                PriceAccounts price = new PriceAccounts();
                price = ReadDataReader(price, data);
                priceList.Add(price);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("Price.GetPriceList  exited");
            return priceList;
        }

        public static PriceAccounts ReadDataReader(PriceAccounts price, SqlDataReader data)
        {
                price.PriceId = Convert.ToInt32(data["priceId"]);
                price.publishedFare = Convert.ToDecimal(data["publishedFare"]);
                price.netFare = Convert.ToDecimal(data["netFare"]);
                price.tax = Convert.ToDecimal(data["tax"]);
                price.markup = Convert.ToDecimal(data["markup"]);
                price.ourCommission = Convert.ToDecimal(data["ourCommission"]);
                price.ourPLB = Convert.ToDecimal(data["ourPLB"]);
                price.agentCommission = Convert.ToDecimal(data["agentCommission"]);
                price.agentPLB = Convert.ToDecimal(data["agentPLB"]);
                price.serviceTax = Convert.ToDecimal(data["serviceTax"]);
                price.tdsRate = Convert.ToDecimal(data["tdsRate"]);
                //tds = Convert.ToDecimal(data["tds"]);
                if (data["tdsCommission"] != DBNull.Value)
                {
                    price.tdsCommission = Convert.ToDecimal(data["tdsCommission"]);
                }
                if (data["tdsPLB"] != DBNull.Value)
                {
                    price.tdsPLB = Convert.ToDecimal(data["tdsPLB"]);
                }
                price.otherCharges = Convert.ToDecimal(data["otherCharges"]);
                price.whiteLabelDiscount = Convert.ToDecimal(data["whiteLabelDiscount"]);
                price.transactionFee = Convert.ToDecimal(data["transactionFee"]);
                price.accPriceType = (PriceType)data["priceType"];

                price.currency = data["currency"].ToString();
                if (data["rateofExchange"] != DBNull.Value)
                {
                    price.rateOfExchange = Convert.ToDecimal(data["rateofExchange"]);
                }
                if (data["currencyCode"] != DBNull.Value)
                {
                    price.currencyCode = data["currencyCode"].ToString();
                }
                price.additionalTxnFee = Convert.ToDecimal(data["additionalTxnFee"]);
                price.wlCharge = Convert.ToDecimal(data["wlCharge"]);
                price.discount = Convert.ToDecimal(data["discount"]);
                if (data["commissionType"] != DBNull.Value)
                {
                    price.commissionType = data["commissionType"].ToString();
                }
                if (data["reverseHandlingCharge"] != DBNull.Value)
                {
                    price.reverseHandlingCharge = Convert.ToDecimal(data["reverseHandlingCharge"]);
                }
                if (data["yqTax"] != DBNull.Value)
                {
                    price.yqTax = Convert.ToDecimal(data["yqTax"]);
                }
                price.isServiceTaxOnBaseFarePlusYQ = Convert.ToBoolean(data["isServiceTaxOnBaseFarePlusYQ"]);
                ChargeBreakUp cbu = new ChargeBreakUp();
                price.chargeBU = cbu.Load(price.priceId);
                if (data["B2CMarkup"] != DBNull.Value)
                {
                    price.b2cMarkup = Convert.ToDecimal(data["B2CMarkup"]);
                    price.b2cMarkupType = data["B2CMarkupType"].ToString();
                    price.b2cMarkupValue = Convert.ToDecimal(data["B2CMarkupValue"]);
                }
            if (data["invat_amount"] != DBNull.Value)
            {
                price.inputVATAmount = Convert.ToDecimal(data["invat_amount"]);
            }
            if (data["outvat_amount"] != DBNull.Value)
            {
                price.outputVATAmount = Convert.ToDecimal(data["outvat_amount"]);
            }
            return price;
        }

        /// <summary>
        /// To update the fare difference if the booked fare is higher than ticketing fare
        /// </summary>
        /// <param name="iFlightId"></param>
        /// <param name="sDHAP"></param>
        /// <returns></returns>
        public int UpdatePriceDiff()
        {
            int count = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@PriceId", PriceId);
                paramList[1] = new SqlParameter("@PriceDiff", FareDiff);
                count = DBGateway.ExecuteNonQuerySP("usp_UpdatePriceDiff", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return count;
        }
    }

    [Serializable]
    public class TaxBreakup
    {
        private string taxCode;
        private decimal taxValue;

        public string TaxCode { get => taxCode; set => taxCode = value; }
        public decimal TaxValue { get => taxValue; set => taxValue = value; }

        public TaxBreakup()
        {
            this.taxCode = string.Empty;
        }

        public override string ToString()
        {
            return taxCode + ":" + taxValue;
        }
    }
}
