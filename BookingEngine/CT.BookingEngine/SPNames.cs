namespace CT.BookingEngine
{
    internal static class SPNames
    {
        public const string GetAirport = "usp_GetAirport";
        public const string GetValidCityCode = "usp_GetValidCityCode";
        public const string GetAirportInfo = "usp_GetAirportInfo";
        public const string GetAirportsForCountry = "usp_GetAirportsForCountry";
        public const string GetCityName = "usp_GetCityName";
        public const string GetCityNameFromCityCode = "usp_GetCityNameFromCityCode";
        public const string GetAirportCodesByCity = "usp_GetAirportCodesByCity";
        public const string AddBookingDetail = "usp_AddBookingDetail";
        public const string UpdateBookingDetailAgencyId = "usp_UpdateBookingDetailAgencyId";
        public const string AddFlightPassenger = "usp_AddFlightPassenger";
        public const string AddSegmentPTCDetail = "usp_AddSegmentPTCDetail";
        public const string AddFlightInfo = "usp_AddFlightInfo";
        public const string AddFlightItinerary = "usp_AddFlightItinerary";
        public const string UpdateFlightItineraryAgencyId= "usp_UpdateFlightItineraryAgencyId";
        public const string AddProductLine = "usp_AddProductLine";
        public const string LoadPassenger = "usp_LoadPassenger";
        public const string GetSelectedColumnsFromItineraryAgainstFlightId = "usp_GetSelectedColumnsFromItineraryAgainstFlightId";
        public const string GetSelectedColumnsFromFlightInfoAgainstFlightId = "usp_GetSelectedColumnsFromFlightInfoAgainstFlightId";
        public const string GetSelectedColumnsFromTicketAgainstFlightId = "usp_GetSelectedColumnsFromTicketAgainstFlightId";

        public const string GetMealPreferenceList = "usp_GetMealPreferenceList";
        public const string GetSeatPreferenceList = "usp_GetSeatPreferenceList";
        public const string GetMeal = "usp_GetMeal";
        public const string GetSeat = "usp_GetSeat";
        public const string GetPaxFullName = "usp_GetPaxFullName";

        
        public const string GetTravelDateByFlightId = "usp_GetTravelDateByFlightId";
        //For Airport
        public const string GetAirportNameByCode = "usp_GetAirportNameByCode";
        public const string AddAirport = "usp_SaveAirport";
        public const string UpdateAirport = "usp_UpdateAirport";

        public const string GetBookingDetail = "usp_GetBookingDetail";
        public const string GetProductLine = "usp_GetProductLine";
        public const string GetFlightDetail = "usp_GetFlightDetail";
        public const string GetSegmentsInfo = "usp_GetSegmentsInfo";
        public const string GetPassengersInfo = "usp_GetPassengersInfo";
        public const string IncrementETicketHit = "usp_IncrementETicketHit";
        public const string CompleteAirServiceRequest = "usp_CompleteAirServiceRequest";
        public const string UpdateServiceRequestCancellationCharge = "usp_UpdateServiceRequestCancellationCharge";
        
        public const string AddServiceRequest = "usp_AddServiceRequest";
        public const string GetServiceRequest = "usp_GetServiceRequest";
        public const string GetSegmentPTCDetail = "usp_GetSegmentPTCDetail";
        public const string GetRequestTypeList = "usp_GetRequestTypeList";

        public const string AddTicket = "usp_AddTicket";
        public const string AddTicketTaxBreakup = "usp_AddTicketTaxBreakup";
        public const string AddInvoice = "usp_AddInvoice";
        public const string AddCreditNote = "usp_AddCreditNote";
        public const string UpdateInvoice = "usp_UpdateInvoice";
        public const string UpdateInvoiceLineItem = "usp_UpdateInvoiceLineItem";
        public const string UpdateInvoiceLineItemByReferenceNumber="usp_UpdateInvoiceLineItemByReferenceNumber";
        public const string AddOfflineInvoice = "usp_AddOfflineInvoice";
        public const string AddInvoiceLineItem = "usp_AddInvoiceLineItem";
        public const string AddCreditNoteLineItem = "usp_AddCreditNoteLineItem";
        public const string GetTDSTillNow = "usp_GetTDSTillNow";
        public const string GetTicketListByFlightId = "usp_GetTicketListByFlightId";
        public const string SetBookingStatus = "usp_SetBookingStatus";
        public const string SetTicketStatus = "usp_SetTicketStatus";
        public const string LockBooking = "usp_LockBooking";
        public const string UnlockBooking = "usp_UnlockBooking";
        public const string UnlockBookingWithStatus = "usp_UnlockBookingWithStatus";
        public const string CheckLock = "usp_CheckLock";
        public const string UpdateSegmentPTCDetail = "usp_UpdateSegmentPTCDetail";
        public const string UpdateFlightInfo = "usp_UpdateFlightInfo";
        public const string InvoiceRaisedForFlight = "usp_InvoiceRaisedForFlight";
        public const string CreditNoteRaisedForItem = "usp_CreditNoteRaisedForItem";
        public const string CreditNoteRaisedForTicketId = "usp_CreditNoteRaisedForTicketId";
        public const string LoadTicket = "usp_LoadTicket";
        public const string UpdateTicket = "usp_UpdateTicket";
        public const string DeleteTaxBreakupForTicket = "usp_DeleteTaxBreakupForTicket";
        public const string LoadTaxBreakup = "usp_LoadTaxBreakup";
        public const string GetInvoiceNumberByFlightId = "usp_GetInvoiceNumberByFlightId";
        public const string GetCreditNoteNumberByFlightId = "usp_GetCreditNoteNumberByFlightId";
        public const string GetCreditNoteNumberByTicketId = "usp_GetCreditNoteNumberByTicketId";
        public const string GetCreditNoteNumberStringByOfflineBookingId = "usp_GetCreditNoteNumberStringByOfflineBookingId";
        public const string GetCreditNoteNumberByOfflineBookingId = "usp_GetCreditNoteNumberByOfflineBookingId";
        public const string GetInvoiceNumberByTicketId = "usp_GetInvoiceNumberByTicketId";
        public const string GetInvoiceNumberByBookingId = "usp_GetInvoiceNumberByBookingId";
        public const string GetAcceptedPaymentsAgainstInvoice = "usp_GetAcceptedPaymentsAgainstInvoice";
        public const string GetInvoiceNumberByItemId = "usp_GetInvoiceNumberByItemId";
        public const string UpdateSupplierAgainstTicket = "usp_UpdateSupplierAgainstTicket";

        //Misc invoice booking
        public const string AddMiscBooking = "usp_AddMiscInvoiceBooking";
        public const string UpdateMiscBooking = "usp_UpdateMiscInvoiceBooking";
        public const string GetMiscBookingNyId = "usp_GetMiscBookingNyId";


        public const string AddPrice = "usp_AddPrice";
        public const string GetPriceDetail = "usp_GetPriceDetail";
        public const string GetPriceDetailForCreditNote = "usp_GetPriceDetailForCreditNote";
        public const string AddBillingAddress = "usp_AddBillingAddress";
        public const string GetInvoice = "usp_GetInvoice";
        public const string GetInvoicesTotalByDateRange = "usp_GetInvoicesTotalByDateRange";
        
        public const string GetInvoicePrice = "usp_GetInvoicePrice";
        public const string GetLineItems = "usp_GetLineItems";
        public const string GetSupplierByInvoiceNumber = "usp_GetSupplierByInvoiceNumber";
        public const string GetSupplierRemmitance = "usp_GetSupplierRemmitance";
        public const string UpdatePrice = "usp_UpdatePrice";
        public const string GetPriceForPriceIdList = "usp_GetPriceForPriceIdList";
        public const string GetPriceIdOfUnpaidInvoicesInCurrentFortnight = "usp_GetPriceIdOfUnpaidInvoicesInCurrentFortnight";
        public const string GetIRCTCFare = "usp_GetIRCTCFare";
        public const string GetIRCTCPriceForAgent = "usp_GetIRCTCPriceForAgent";
        public const string GetMealCode = "usp_GetMealCode";
        public const string GetAgencyIdForTicket = "usp_GetAgencyIdForTicket";

        public const string AddFareRule = "usp_AddFareRule";
        public const string GetFareRuleList = "usp_GetFareRuleList";

        public const string GetFlightId = "usp_GetFlightId";
        public const string GetTicketIdForPax = "usp_GetTicketIdForPax";
        public const string IsInvoiceGenerated = "usp_IsInvoiceGenerated";
        public const string IsCreditNoteGenerated = "usp_IsCreditNoteGenerated";
        public const string IsCreditNoteGeneratedForProduct = "usp_IsCreditNoteGeneratedForProduct";
        public const string GetFlightIdByInvoiceNumber = "usp_GetFlightIdByInvoiceNumber";
        public const string GetPNRByFlightId = "usp_GetPNRByFlightId";        

        public const string CheckPNR = "usp_CheckPNR";
        
        public const string GetNonPaidInvoices = "usp_GetNonPaidInvoices";
        public const string GetCountForNonPaidInvoicesForAgent = "usp_GetCountForNonPaidInvoicesForAgent";
        public const string GetCountForNonPaidInvoicesForPaymentAcceptance = "usp_GetCountForNonPaidInvoicesForPaymentAccept";
        public const string GetNonPaidInvoicesForAgent = "usp_GetNonPaidInvoicesForAgent";
        public const string GetNonPaidInvoicesForAgentPageWise = "usp_GetNonPaidInvoicesForAgentPageWise";
        public const string GetNonPaidInvoicesForPaymentProcessingPageWise = "usp_GetNonPaidInvoicesForPaymentAcceptPageWise";
        public const string UpdateInvoiceStatus = "usp_UpdateInvoiceStatus";
        public const string GetReadyBookingPrice = "usp_GetReadyBookingPrice";
        public const string GetBookingPrice = "usp_GetBookingPrice";
        public const string GetPaymentDoneAgainstInvoice = "usp_GetPaymentDoneAgainstInvoice";
        public const string GetAllPaymentAgainstInvoice = "usp_GetAllPaymentAgainstInvoice";
        public const string IsPaymentDoneAgainstInvoice = "usp_IsPaymentDoneAgainstInvoice";

       // public const string 
        public const string GetInvoices = "usp_GetInvoices";
        public const string GetTicketIssueDateByFlightId = "usp_GetTicketIssueDateByFlightId";
        public const string GetTicketFromTicketNo = "usp_GetTicketFromTicketNo";
        public const string GetInvoicesForAgencies = "usp_GetInvoicesForAgencies";
        public const string GetInvoiceNumber = "usp_GetInvoiceNumber";
        public const string UpdateCalendar = "usp_UpdateCalendar";
        public const string GetLowestFareDetails = "usp_GetLowestFareDetails";
        public const string UpdateInvoiceRemark = "usp_UpdateInvoiceRemark";
        public const string GetUnpaidInvoicesWhoseDueDateIsOver = "usp_GetUnpaidInvoicesWhoseDueDateIsOver";
        public const string GetAmountAgainstPartialInvoicesOfCurrentFortnight = "usp_GetAmountAgainstPartialInvoicesOfCurrentFortnight";

        // for Auto Ticketing
        public const string GetBookingIdByFlightId = "usp_GetBookingIdByFlightId";
        public const string GetInvoiceOfTxn = "usp_GetInvoiceOfTxn";

        // For PNR Refresh 
        public const string UpdateBookingDetail = "usp_UpdateBookingDetail";
        public const string GetPriceIdByFlightId = "usp_GetPriceIdByFlightId";
        public const string GetServiceFeeFromPriceId = "usp_GetServiceFeeFromPriceId";

        public const string GetAirportPresent = "usp_GetAirportPresent";
        public const string IsDuplicatePNR = "usp_IsDuplicatePNR";
        
        
        // for LCC Tax        
        public const string GetTaxForSourceByOriginDest = "usp_GetTaxForSourceByOriginDest";

        // for LCC Fuel Surcharge
        public const string GetFuelSurchargeForSource = "usp_GetFuelSurchargeForSource";

        // For LCC FareRules
        public const string GetFareRulesForLcc = "usp_GetFareRulesForLcc";

        // For Assignment of Change Request
        public const string AddServiceRequestAssignmentLog = "usp_AddServiceRequestAssignmentLog";
        public const string GetServiceRequestStatus = "usp_GetServiceRequestStatus";
        
        public const string GetStatusOfCurrentRequest = "usp_GetStatusOfCurrentRequest";        
        public const string UpdateServiceRequestAssignment = "usp_UpdateServiceRequestAssignment";
        public const string UpdateFileNameInServiceRequest = "usp_UpdateFileNameInServiceRequest";
        public const string GetAssignedToServiceRequest = "usp_GetAssignedToServiceRequest";
        public const string GetServiceRequestIdByTicketId = "usp_GetServiceRequestIdByTicketId";  
        

        public const string AddCancellationCharge = "usp_AddCancellationCharge";
        public const string UpdateCancellationCharge = "usp_UpdateCancellationCharge";
        public const string GetCancellationCharge = "usp_GetCancellationCharge";
        public const string GetCancellationChargeOnItemTypeBasis = "usp_GetCancellationChargeOnItemTypeBasis";

        //For TekTravelRobot
        public const string UpdateFlightSchedule = "usp_UpdateFlightSchedule";
        public const string AddToUpdatedPNR = "usp_AddToUpdatedPNR";

        public const string GetSalesReport = "usp_GetSalesReport";
        public const string GetMiscSalesReport = "usp_GetMiscSalesReport";

        public const string GetHotelSalesReport = "usp_GetHotelSalesReport";
        public const string GetRefundReport = "usp_GetRefundReport";
        public const string GetRefundReportToday = "usp_GetRefundReportToday";

        public const string IsDomestic = "usp_IsDomestic";
        
        public const string UpdateAdvisory = "usp_UpdateAdvisory";
        public const string GetAdvisory = "usp_GetAdvisory";
        public const string GetAllAdvisory = "usp_GetAllAdvisory";
        public const string IsDomesticFlight = "usp_IsDomesticFlight";
        // for Calendar Fare;
        public const string GetDomesticCity = "usp_GetDomesticCity";
        public const string GetBookingMode = "usp_GetBookingMode";
        public const string GetProductIdByBookingId = "usp_GetProductIdByBookingId";  
      

        // For Hotel
        public const string AddHotelItinerary = "usp_AddHotelItinerary";
        public const string AddHotelPassenger = "usp_AddHotelPassenger";
        public const string AddHotelRoom = "usp_AddHotelRoom";
        public const string AddHotelFareBreakDown = "usp_AddHotelFareBreakDown";

        public const string GetHotelItinerary = "usp_GetHotelItinerary";
        public const string GetHotelRoomByHotelId = "usp_GetHotelRoomByHotelId";
        public const string GetHotelFareBreakDownByRoomId = "usp_GetHotelFareBreakDownByRoomId";
        public const string GetHotelPassengerByHotelId = "usp_GetHotelPassengerByHotelId";
        public const string GetHotelId = "usp_GetHotelId";
        public const string GetBookingIdByProductId = "usp_GetBookingIdByProductId";
        
        public const string InvoiceRaisedForHotel = "usp_InvoiceRaisedForHotel";
        public const string IsInvoiceGeneratedForHotel = "usp_IsInvoiceGeneratedForHotel";
        public const string GetHotelRoomByRoomId = "usp_GetHotelRoomByRoomId";
        public const string UpdateHotelItineary = "usp_UpdateHotelItineary";
        public const string IsBookingAlreadyExist = "usp_IsBookingAlreadyExist";
        public const string GetIntlHotelSalesReport = "usp_GetIntlHotelSalesReport";
        public const string UpdateHotelStaticData = "usp_UpdateHotelStaticData";
        public const string UpdateHotelStaticDataLocation = "usp_UpdateHotelStaticDataLocation";
        public const string UptStatusbyConfirmationNo = "usp_UpdateHTLPendingStatus";

        // For Offline booking
        public const string AddOfflineHotelItinerary = "usp_AddOfflineHotelItinerary";
        public const string AddOfflineHotelRoom = "usp_AddOfflineHotelRoom";
        public const string GetOfflineHotelItinerary = "usp_GetOfflineHotelItinerary";
        public const string LoadOfflinePassenger = "usp_LoadOfflinePassenger";
        public const string GetOfflineHotelRoom = "usp_GetOfflineHotelRoom";
        public const string AddOfflinePassenger = "usp_AddOfflinePassenger";
        public const string AddfflineFlexDetails = "usp_AddOfflineFlexDetails";
        public const string AddOfflineFlightInfo = "usp_AddOfflineFlightInfo";
        public const string AddOfflineItinerary = "usp_AddOfflineItinerary";
        public const string GetOffFlightDetails = "usp_GetOffFlightDetails";
        public const string GetOffHotelDetails = "usp_GetOffHotelDetails";

        // Booking History 
        public const string AddBookingHistory = "usp_AddBookingHistory";
        public const string GetListBookingHistory = "usp_GetListBookingHistory";
        public const string GetBookingHistory = "usp_GetBookingHistory";
        public const string GetBookingHistoryForProductId= "usp_GetBookingHistoryForProductId";
        public const string GetAirports = "usp_GetAirports";

        //ChargeBreakUp.cs
        public const string AddChargeBreakUpEntry = "usp_AddChargeBreakUpEntry";
        public const string GetChargeBreakUpEntry = "usp_GetChargeBreakUpEntry";
        // For IntlHotelBookingDetails
        public const string GetIntelHotelById = "usp_GetIntelHotelById";
        public const string AddIntlHotelBookingDetails = "usp_AddIntlHotelBookingDetails";
        public const string AddHotelData = "usp_AddHotelData";
        public const string GetHotelDataByCode = "usp_GetHotelDataByCode";
        public const string AddHotelImages = "usp_AddHotelImages";
        public const string GetHotelImagesByCode = "usp_GetHotelImagesByCode";
        // GTA City
        public const string AddGTACity = "usp_AddGTACity";
        public const string GetGTACities = "usp_GetGTACities";
        public const string GetGTACountries = "usp_GetGTACountries";
        public const string GetGTACityCode = "usp_GetGTACityCode";
        public const string GetGTACityDataByCode = "usp_GetGTACityDataByCode";
        public const string UpdateVoucherStatus = "usp_UpdateVoucherStatus";
        public const string UpdateHotelImages = "usp_UpdateHotelImages";
        public const string GetMainAreaCodeForCity = "usp_GetMainAreaCodeForCity";
        public const string GetGTAMainAreaCityCode = "usp_GetGTAMainAreaCityCode";
        public const string GetGTACountryCodes = "usp_GetGTACountryCodes";
        public const string GetGTAAOTNumber = "usp_GetGTAAOTNumber";
        public const string GetAllGTACity = "usp_GetAllGTACity";
        public const string GetCountryNameforCityCode = "usp_GetCountryNameforCityCode";
        //City Codes now for HotelBeds but it will include all later
        public const string GetHotelCityCode = "usp_GetHotelCityCode";
        // DOTW country List
        public const string GetDOTWCountries = "usp_GetDOTWCountries";
        
        //VoidationCharges per passenger
        public const string GetVoidationCharge = "usp_GetVoidationCharge";
        public const string GetBApiDetail = "usp_GetBApiDetail";
        public const string GetBApiDetailCount = "usp_GetBApiDetailCount";    
        public const string RemoveBApiBookingDetail = "usp_RemoveBApiBookingDetail";
        public const string GetBApiBookingDetailByRefId = "usp_GetBApiBookingDetailByRefId";
        
        public const string GetTicketNoAndSource = "usp_GetTicketNoAndSource";
        public const string AddBApiBookingDetail = "usp_AddBApiBookingDetail";
        public const string UpdateBApiBookingDetail = "usp_UpdateBApiBookingDetail";

        //For SSR Detail
        public const string AddSSR = "usp_AddSSR";
        public const string DeleteSSR = "usp_DeleteSSR";
        public const string GetSSRDetail = "usp_GetSSRDetail";

        // GTA Area Code
        public const string AddGTAAreaCode = "usp_AddGTAAreaCode";
        public const string UpdateAreaCountry = "usp_UpdateAreaCountry";
        public const string GetGTAAreaCodeByCountry = "usp_GetGTAAreaCodeByCountry";
        public const string GetCountryNameforAreaCode = "usp_GetCountryNameforAreaCode";
        public const string GetCurrencyForCountry = "usp_GetCurrencyForCountry";
        public const string GetGTATopDestination = "usp_GetGTATopDestination";

        //Hotel Penality
        public const string SaveHotelPenality = "usp_SaveHotelPenality";
        public const string GetHotelPenality = "usp_GetHotelPenality";

        // GTA Multiple Pax
        public const string GetHotelPassengerByHotelIdRoomId = "usp_GetHotelPassengerByHotelIdRoomId";
        public const string GetHotelTaxBreakup = "usp_GetHotelTaxBreakUp";

        //For LCC taxes updation
        public const string UpdateLccTaxes = "usp_UpdateLccTaxes";
        public const string GetListOfLcctaxes = "usp_GetListOfLcctaxes";
        public const string GetLccTax = "usp_GetLccTax";

        //Hotel Amendments
        public const string AmendHotelItineary = "usp_AmendHotelItineary";
        public const string UpdateHotelPassenger = "usp_UpdateHotelPassenger";
        public const string DeleteFareBreakDown = "usp_DeleteFareBreakDown";
        public const string DeleteHotelPenality = "usp_DeleteHotelPenality";
        public const string UpdateRoom = "usp_UpdateRoom";

        //Insurance
        public const string AddInsurance = "usp_AddInsurance";
        public const string UpdateInsurance = "usp_UpdateInsurance";
        public const string AddPolicyDetail = "usp_AddPolicyDetail";
        public const string GetPolicyDetail = "usp_GetPolicyDetail";
        public const string GetInsuranceDetail = "usp_GetInsuranceDetail";
        public const string GetChildBookings = "usp_GetChildBookings";
        public const string UpdatePolicyDetail = "usp_UpdatePolicyDetail";
        public const string InvoiceRaisedForInsurance = "usp_InvoiceRaisedForInsurance";
        public const string IsInvoiceGeneratedForInsurance = "usp_IsInvoiceGeneratedForInsurance";
        public const string GetPolicyDetailByPaxId = "usp_GetPolicyDetailByPaxId";
        public const string LoadPolicyDetail = "usp_LoadPolicyDetail";


        // for supplier

        public const string AddSupplierRemmittance = "usp_AddSupplierRemmittance";
        public const string UpdateSupplierRemmittance = "usp_UpdateSupplierRemmittance";

        //Credit Card Payment
        public const string AddCCPayment = "usp_AddCCPayment";
        public const string GetCCPayment = "usp_GetCCPayment";

        //Results in cache for filtering & paging
        public const string AddHotelSearchResultCache = "usp_AddHotelSearchResultCache";
        public const string GetHotelSearchResultCache = "usp_GetHotelSearchResultCache";
        public const string AddTBOConnectSearchResultCache = "usp_AddTBOConnectSearchResultCache";
        public const string GetTBOConnectSearchResultCache = "usp_GetTBOConenctSearchResultCache";

        //Transfers
        public const string GetTransferLocations = "usp_GetTransferLocations";
        public const string AddTransferStaticData = "usp_AddTransferStaticData";
        public const string GetTransferStaticDataByCode = "usp_GetTransferStaticDataByCode";
        public const string UpdateTransferStaticData = "usp_UpdateTransferStaticData";
        public const string SaveTransferPenality = "usp_SaveTransferPenality";
        public const string GetTransferPenality = "usp_GetTransferPenality";
        public const string DeleteTransferPenality = "usp_DeleteTransferPenality";
        public const string AddTransferItinerary = "usp_AddTransferItinerary";
        public const string AddTransferVehicle = "usp_AddTransferVehicle";
        public const string GetTransferItinerary = "usp_GetTransferItinerary";
        public const string GetVehicleByTransferId = "usp_GetVehicleByTransferId";
        public const string GetTransferId = "usp_GetTransferId";
        public const string UpdateTransferVoucherStatus = "usp_UpdateTransferVoucherStatus";
        public const string UpdateTransferItineary = "usp_UpdateTransferItineary";
        public const string GetIntelTransferById="usp_GetIntelTransferById";
        public const string AddIntlTransferBookingDetails = "usp_AddIntlTransferBookingDetails";
        public const string GetTrainStationsByCityCode = "usp_GetTrainStationsByCityCode";
        public const string IsInvoiceGeneratedForTransfers="usp_IsInvoiceGeneratedForTransfers";

        //tourico airportcode
        public const string GetTouricoAirportCode = "usp_GetTouricoAirportCode";
        //IAN city code
        public const string GetIANCityCode = "usp_GetIANCityCode";
        public const string GetIANDestinationID = "usp_GetIANDestinationCode";
        public const string GetIANStateList = "usp_GetIANStateList";
        public const string GetHotelCountries = "usp_GetHotelCountries";
        public const string GetHotelCities = "usp_GetHotelCities";
        public const string LoadHotelCity = "usp_LoadHotelCity";
        public const string GetCityIdFromCityName = "usp_GetCityIdFromCityName";
        public const string UpdateCityCode = "usp_UpdateCityCode";
        public const string GetHotelCityByCityName = "usp_GetHotelCityByCityName";
        public const string LoadHotelCityByName = "usp_LoadHotelCityByName";
        public const string GetGTACode = "usp_GetGTACode";
        public const string GetHotelCityByCountryName = "usp_GetHotelCityByCountryName";
        public const string GetHotelBedCode = "usp_GetHotelBedCode";
        public const string GetMikiCityCode = "usp_GetMikiCityCode";
        public const string GetTravcoCityCode = "usp_GetTravcoCityCode";
        public const string GetTouricoCode = "usp_GetTouricoCode";
                //SightSeeings
        public const string AddTourOperations = "usp_AddTourOperations";
        public const string GetTourOperations = "usp_GetTourOperations";
        public const string AddSightseeingStaticData = "usp_AddSightseeingStaticData";
        public const string GetSightseeingStaticDataByCode = "usp_GetSightseeingStaticDataByCode";
        public const string SaveSightseeingPenality = "usp_SaveSightseeingPenality";
        public const string GetSightseeingPenality = "usp_GetSightseeingPenality";
        public const string DeleteSightseeingPenality = "usp_DeleteSightseeingPenality";
        public const string GetIntelSightseeingById = "usp_GetIntelSightseeingById";
        public const string AddIntlSightseeingBookingDetails = "usp_AddIntlSightseeingBookingDetails";
        public const string AddSightseeingItinerary = "usp_AddSightseeingItinerary";
        public const string GetSightseeingItinerary = "usp_GetSightseeingItinerary";
        public const string GetSightseeingId = "usp_GetSightseeingId";
        public const string UpdateSightseeingVoucherStatus = "usp_UpdateSightseeingVoucherStatus";
        public const string UpdateSightseeingItineary = "usp_UpdateSightseeingItineary";
        public const string IsInvoiceGeneratedForSightseeing="usp_IsInvoiceGeneratedForSightseeing";
        public const string AddSightseeingSearchResultCache = "usp_AddSightseeingSearchResultCache";
        public const string GetSightseeingSearchResultCache = "usp_GetSightseeingSearchResultCache";
        public const string DeleteTourOperations = "usp_DeleteTourOperations";
        public const string UpdateSightseeingStaticData = "usp_UpdateSightseeingStaticData";

        //Train Itinerary
        public const string AddTrainItinerary = "usp_AddTrainItinerary";
        public const string GetTrainItinerary = "usp_GetTrainItinerary";
        public const string GetUniqueSequenceNumber = "usp_GetUniqueSequenceNumber";
        public const string CheckTrainSectorForInprogress = "usp_CheckTrainSectorForInprogress";
        public const string GetInProgressTrainBookingIDsForDate = "usp_GetInProgressTrainBookingIDsForDate";
        public const string GetTraversedTrainBookingIDsInWaiting = "usp_GetTraversedTrainBookingIDsInWaiting";


        //Fleet Itinerary
        public const string IsInvoiceGeneratedForFleet = "usp_IsInvoiceGeneratedForFleet";
        public const string InvoiceRaisedForFleet = "usp_InvoiceRaisedForFleet";
        public const string AddFleetItinerary = "usp_AddFleetItinerary";
        public const string GetFleetItinerary = "usp_GetFleetItinerary";
        public const string saveFleetPassengerDetails = "usp_saveFleetPassengerDetails";
        public const string GetFleetPassengerDetails = "usp_GetFleetPassengerDetails";
        public const string GetFleetId = "usp_GetFleetId";
        public const string GetPaxDetailsByPaxName = "usp_GetFleetPaxDetailsByPaxName";
        public const string GetPaxDetailsBypaxId = "usp_GetFleetPaxDetailsByPaxId";
        public const string GetAllFleetSourceId="usp_GetAllFleetSourceId";
        public const string UpdateFleetItineary = "usp_UpdateFleetItineary";
        //
        public const string UpdateTrainPassenger = "usp_UpdateTrainPassenger";
        public const string AddTrainPassenger = "usp_AddTrainPassenger";
        public const string GetTrainPassenger = "usp_GetTrainPassenger";
        public const string GetTrainPassengerByBookingId = "usp_GetTrainPassengerByBookingId";

        //IRCTC objects
        public const string GetTrainRoute = "usp_GetTrainRoute";
        public const string GetDistanceBetweenStations = "usp_GetDistanceBetweenStations";
        public const string GetTrainSourceAndDestinationStation = "usp_GetTrainSourceAndDestinationStation";
        public const string GetTrainTimeAtStation = "usp_GetTrainTimeAtStation";
        public const string GetTrainStation = "usp_GetTrainStation";
        public const string GetTrainStationByStationCode = "usp_GetTrainStationByStationCode";
        public const string GetIRCTCTrainRouteSchema = "usp_GetIRCTCTrainRouteSchema";
        public const string GetTrainsDayOfArrival = "usp_GetTrainsDayOfArrival";
        public const string GetTrainNumbers="usp_GetTrainNumbers";
        public const string GetTrainList="usp_GetTrainList";
        public const string DeleteAllTrainRoutes = "usp_DeleteAllTrainRoutes";
        public const string DeleteTrainRoutes = "usp_DeleteTrainRoutes";

        //Train Exception booking
        public const string AddTrainExceptionBooking = "usp_AddTrainExceptionBooking";
        public const string UpdateTrainExceptionBooking = "usp_UpdateTrainExceptionBooking";
        public const string GetTrainExceptionBooking = "usp_GetTrainExceptionBooking";
        public const string GetTrainExceptionBookingByPnr = "usp_GetTrainExceptionBookingByPnr";
        public const string GetAllTrainExceptionBooking = "usp_GetAllTrainExceptionBooking";
        public const string GetAllTrainExceptionBookingByAgencyId = "usp_GetAllTrainExceptionBookingByAgencyId";
        public const string GetUniqueTxnIDsForDate = "usp_GetUniqueTxnIDsForDate";

        //Failed Booking Objects
        public const string AddFailedBooking = "usp_AddFailedBooking";
        public const string GetFailedBooking = "usp_GetFailedBooking";
        public const string GetPNRFailedBookingForAgency = "usp_GetPNRFailedBookingForAgency";
        public const string GetAllFailedBooking = "usp_GetAllFailedBooking";
        public const string CountFailedBooking = "usp_CountFailedBooking";
        public const string CountFailedBookingForAgency = "usp_CountFailedBookingForAgency";
        public const string UpdateFailedBookingStatus = "usp_UpdateFailedBookingStatus";
        public const string GetFailedBookingForAgency = "usp_GetFailedBookingForAgency";
        public const string CheckFailedBookingPNR = "usp_CheckFailedBookingPNR";
        public const string GetFailedBookingItinerary = "usp_GetFailedBookingItinerary";

        //Tatkal Request Object
        public const string SaveTatkalBookingRequest="usp_SaveTatkalBookingRequest";
        public const string GetTatkalRequestByRequestId="usp_GetTatkalRequestByRequestId";
        public const string DeleteTatkalRequest="usp_DeleteTatkalRequest";
        public const string UpdateTatkalRequest="usp_UpdateTatkalRequest";
        public const string UpdateTatkalRequestStatus="usp_UpdateTatkalRequestStatus";
        public const string GetNoOfTatkalRequestsForAgency = "usp_GetNoOfTatkalRequestsForAgency";
        public const string GetNoOfTatkalRequestsForAdmin = "usp_GetNoOfTatkalRequestsForAdmin";
        public const string GetTatkalRequestsForAgency = "usp_GetTatkalRequestsForAgency";
        public const string GetTatkalRequestsForAdmin="usp_GetTatkalRequestsForAdmin";
        public const string GetTatkalRequestsIDForDateInterval ="usp_GetTatkalRequestsIDForDateInterval";
        
        //For CreditNoteQueue
        public const string GetTrainCreditNoteQueue = "usp_GetTrainCreditNoteQueue";
        public const string GetTrainCreditNote = "usp_GetTrainCreditNote";
        public const string AddTrainCreditNoteQueue = "usp_AddTrainCreditNoteQueue";
        public const string GetTrainCreditNoteCount = "usp_GetTrainCreditNoteCount";
        public const string GetTrainCreditNoteCountForAgent = "usp_GetTrainCreditNoteCountForAgent";
        public const string GetPriceList = "usp_GetPriceList";
        public const string GetTrainCreditNoteQueueForAgent = "usp_GetTrainCreditNoteQueueForAgent";

        //For TicketQueue
        public const string GetResultForAdminTicketQueue = "usp_GetResultForAdminTicketQueue";
        public const string GetResultForFlightSearch = "usp_GetResultForFlightSearch";
        public const string GetResultForInsuredTickets = "usp_GetResultForInsuredTickets";

        
        public const string GetResultForAdminTicketQueueByPnr = "usp_GetResultForAdminTicketQueueByPnr";
        public const string GetResultForAdminTicketQueueByTicketNumber = "usp_GetResultForAdminTicketQueueByTicketNumber";
        public const string GetResultForAdminTicketQueueByAgencyId = "usp_GetResultForAdminTicketQueueByAgencyId";
        public const string GetResultForAdminTicketQueueByPaxName = "usp_GetResultForAdminTicketQueueByPaxName";
        public const string GetResultForAdminTicketQueueByAirlineCodes = "usp_GetResultForAdminTicketQueueByAirlineCodes";
        public const string GetResultForAdminTicketQueueBySource = "usp_GetResultForAdminTicketQueueBySource";
        public const string GetResultForAdminTicketQueueByIsDomestic = "usp_GetResultForAdminTicketQueueByIsDomestic";
        public const string GetResultForAdminTicketQueueByAirlineCodesAndAgencyId = "usp_GetResultForAdminTicketQueueByAirlineCodesAndAgencyId";
        public const string GetResultForAdminTicketQueueByAgencyIdAndSourceCodes = "usp_GetResultForAdminTicketQueueByAgencyIdAndSourceCodes";
        public const string GetResultForAdminTicketQueueByAgencyIdAndIsDomestic = "usp_GetResultForAdminTicketQueueByAgencyIdAndIsDomestic";
        public const string GetResultForAdminTicketQueueByAirlineCodesAndSourceCodes = "usp_GetResultForAdminTicketQueueByAirlineCodesAndSourceCodes";
        public const string GetResultForAdminTicketQueueByAirlineCodesAndIsDomestic = "usp_GetResultForAdminTicketQueueByAirlineCodesAndIsDomestic";
        public const string GetResultForAdminTicketQueueBySourceCodeAndIsDomestic = "usp_GetResultForAdminTicketQueueBySourceCodeAndIsDomestic";
        public const string GetResultForAdminTicketQueueByAgencyIdAirlineCodeSourceCodeAndIsDomestic = "usp_GetResultForAdminTicketQueueByAgencyIdAirlineCodeSourceCodeAndIsDomestic";
        public const string GetResultForAdminTicketQueueByAgencyIdAirlineCodeAndSourceCode = "usp_GetResultForAdminTicketQueueByAgencyIdAirlineCodeAndSourceCode";
        public const string GetResultForAdminTicketQueueByAgencyIdAirlineCodeAndIsDomestic = "usp_GetResultForAdminTicketQueueByAgencyIdAirlineCodeAndIsDomestic";
        public const string GetResultForAdminTicketQueueByAgencyIdSourceCodeAndIsDomestic = "usp_GetResultForAdminTicketQueueByAgencyIdSourceCodeAndIsDomestic";
        public const string GetResultForAdminTicketQueueByAirlineCodeSourceCodeAndIsDomestic = "usp_GetResultForAdminTicketQueueByAirlineCodeSourceCodeAndIsDomestic";

        public const string GetResultForAgentQueueByPaxName = "usp_GetResultForAgentQueueByPaxName";
        public const string GetResultForAgentQueueByPnr = "usp_GetResultForAgentQueueByPnr";
        public const string GetResultForAgentByAirlinesCode = "usp_GetResultForAgentByAirlinesCode";
        public const string GetResultForAgentQueue = "usp_GetResultForAgentQueue";
        public const string GetResultForAgentTicketQueueByTicketNumber = "usp_GetResultForAgentTicketQueueByTicketNumber";
        public const string GetResultForAgentQueuePending = "usp_GetResultForAgentQueuePending";

        //For calendar View booking
        public const string GetBookingsInAMonthForAgency = "usp_GetBookingsInAMonthForAgency";

        //For BApiHotelBookingDetail
        public const string GetBApiHotelBookingDetailByRefId = "usp_GetBApiHotelBookingDetailByRefId";
        public const string GetBApiHotelBookingDetailByReferenceId = "usp_GetBApiHotelBookingDetailByReferenceId";
        public const string GetBApiHotelDetail = "usp_GetBApiHotelDetail";
        public const string GetBApiHotelDetailCount = "usp_GetBApiHotelDetailCount";
        public const string AddBApiHotelBookingDetail = "usp_AddBApiHotelBookingDetail";
        public const string UpdateBApiHotelBookingDetail = "usp_UpdateBApiHotelBookingDetail";
        public const string IsConfExistAndVouchered = "usp_IsConfExistAndVouchered";
        public const string IsConfirmationNoUnique = "usp_IsConfirmationNumberUnique";
        public const string GetHotelIdFromSource = "usp_GetHotelIdFromSource";
        public const string UpdateHotelVoucherStatus = "usp_UpdateVoucherStatusOfHotel";
        
        
        public const string GetRelevantQueuesForAgentBookingQueue = "usp_GetRelevantQueuesForAgentBookingQueue";

        //For Boarding Pass
        public const string AddBoardingPass = "usp_AddBoardingPass";
        public const string GetBoardingPass = "usp_GetBoardingPass";
        public const string GetBoardingPassByTicketid = "usp_GetBoardingPassByTicketid";
        //Mobile 
        public const string GetMobileOperators = "usp_GetMobileOperators";
        public const string GetCirclesForOperator = "usp_GetCirclesForOperator";
        public const string AddMobileRecharge = "usp_AddMobileRecharge";
        public const string LoadMobileRechargeItinerary = "usp_LoadMobileRechargeItinerary";
        public const string GetTotalRecordsforMobileRecharge = "usp_GetTotalRecordsforMobileRecharge";
        public const string IsInvoiceGeneratedForMobileRecharge = "usp_IsInvoiceGeneratedForMobileRecharge";
        public const string GetProcductTypes = "usp_GetMobileProcductTypes";
        public const string GetDenomination = "usp_GetMobileDenomination";
        public const string GetMobileFilteredResults = "usp_GetMobileFilteredResults";
        public const string GetMobileRecordsCount = "usp_GetMobileRecordsCount";

        // For Hotel Source.
        public const string AddHotelSource = "usp_AddHotelSource";
        public const string GetHotelSourceById = "usp_GetHotelSourceById";
        public const string UpdateHotelSource = "usp_UpdateHotelSource";
        public const string GetHotelSourceByName = "usp_GetHotelSourceByName";


        public const string GetExceptionHotelAgentByStateSearch = "usp_GetExceptionHotelAgentByStateSearch";
        public const string GetExceptionHotelAgentByAgencySearch = "usp_GetExceptionHotelAgentByAgencySearch";
        public const string AddInHotelAgentCommission = "usp_AddInHotelAgentCommission";
        public const string GetAgentHotelCommission = "usp_GetAgentHotelCommission";
        public const string DeleteFromHotelAgentCommission = "usp_DeleteFromHotelAgentCommission";
        public const string UpdateHotelAgentCommission = "usp_UpdateHotelAgentCommission";
        public const string GetAllHotelSourceList = "usp_GetAllHotelSourceList";

        public const string GetAllHotelSourceId = "usp_GetAllHotelSourceId";
        public const string GetHotelSourceOnGeoType = "usp_GetHotelSourceOnGeoType";
        public const string GetGimmonixHotelSources = "usp_GetGimmonixSuppliers";
        // INsurance
        public const string GetInsQueue = "usp_GetInsuranceQueue";
        public const string GetInsuranceQueueDetails = "usp_GetInsuranceQueueDetails";
        public const string InsuranceGetList = "USP_INSURANCEPLAN_GETLIST";
        public const string GetInsuranceChangeRequestQueue = "usp_GetInsuranceChangeRequestData";
        public const string GetInsurancePlanList = "usp_GetInsurancePlanList";

        public const string GetInsQueueList = "usp_GetInsuranceQueueList";
        public const string GetInsuranceChangeRequestList = "usp_GetInsuranceChangeRequestList";


        // Activity PROC
        public const string GetActivity = "usp_GetActivity";
        public const string GetActivityFlexMaster = "usp_GetActivityFlexMaster";
        public const string GetActivityPrice = "usp_GetActivityPrice";
        public const string GetActivityTransactionHeader = "usp_GetActivityTransactionHeader";
        public const string GetActivityTransactionDetail = "usp_GetActivityTransactionDetail";
        public const string GetActivityTransactionPrice = "usp_GetActivityTransactionPrice";
        public const string GetActivityFlexDetails = "usp_GetActivityFlexDetails";
        public const string GetFDDetails = "usp_GetFDPaymentDetails";
        public const string GetCountryList1 = "usp_GetCountryList";
        public const string AddActivityTransactionHeader = "usp_AddActivityTransactionHeader";
        public const string AddActivityTransactionDetail = "usp_AddActivityTransactionDetail";
        public const string AddActivityTransactionPrice = "usp_AddActivityTransactionPrice";
        public const string AddActivityFlexDetails = "usp_AddActivityFlexDetails";
        public const string UpdateActivityStock = "usp_UpdateActivityStock";
        public const string GetActivityQueue = "usp_Get_Activity_Queue";
        public const string GetActivityQueueDetials = "usp_Activiy_Queue_GetData";
        public const string GetTheme = "usp_GetTheme";
        public const string GetCityByCountryCode = "usp_GetCityByCountryCode";
        public const string AddActivity = "usp_AddActivityMaster";
        public const string addActivityFlexMaster = "usp_addActivityFlexMaster";
        public const string AddActivityPrice = "usp_AddActivityPrice";
        public const string UpdateActivityTransactionHeader = "usp_UpdateActivityTransactionHeader";
        public const string GetAllHotelCities = "usp_GetAllHotelCities";
        public const string IsInvoiceGeneratedForActivity = "usp_IsInvoiceGeneratedForActivity";
        public const string InvoiceRaisedForActivity = "usp_InvoiceRaisedForActivity";
        public const string IsInvoiceGeneratedForFixedDeparture = "usp_IsInvoiceGeneratedForFixedDeparture";
        public const string InvoiceRaisedForFixedDeparture = "usp_InvoiceRaisedForFixedDeparture";
        public const string InvoiceRaisedForTransfers = "usp_InvoiceRaisedForTransfers";
        public const string InvoiceRaisedForSightSeeing = "usp_InvoiceRaisedForSightSeeing";
        // FD
        public const string GetFixedDepartureChangeRequestData = "usp_GetFixderDepartureChangeRequestData";

// Packages
        public const string GetHolidayPackageSeasons = "usp_GetHolidayPackageSeasons";
        public const string GetHolidayPackageDeal = "usp_GetHolidayPackageDeal";
        public const string GetPackageCityList = "usp_GetPackageCityList";
        public const string GetHolidayPackages = "usp_GetHolidayPackagesForchild";
        public const string GetHolidayPackagesWithCurrency = "usp_GetHolidayPackagesForchildWithCurrency"; //added on 25/05/2016
        // Manage Hotel City Mapping Codes
        public const string CityCodeGetList = "usp_MapCityCodeGetList";
        public const string CityCodeUpdate = "usp_MapCityCodeUpdate";
        public const string SaveCityCode = "usp_SaveCityCode";
        public const string LoadCityCodes = "usp_LoadCityCodes";

        // Enquiry 
        public const string GetEnquiry = "usp_Enquiry_GetList";
        public const string Enquiry_Remarks_ADD = "usp_Enquiry_Remarks_ADD";
        public const string Enquiry_Remarks_GetList = "usp_Enquiry_Remarks_GetList";

        // Product Report
        public const string HotelAcctReportGetlist = "usp_hotel_acct_report_getlist";
        public const string FlightAcctReportGetlist = "usp_flight_acct_report_getlist";
        public const string FlightAccounttReportGetlist = "usp_flight_account_report_getlist";
        
        public const string GetV1FlightsReport = "usp_V1AirlineFlightReport";

        //added variable for get list of sightseeing Account Report <28/03/2016>
        public const string SightseeingAcctReportGetlist = "usp_sightseeing_acct_report_getlist";
        public const string UpdateSightseeingAccountedStatus = "usp_UpdateSightseeingAccountedStatus";
        
        //added variable for get list of Transfer Account Report <29/03/2016>
        public const string TransferAcctReportGetlist = "usp_transfer_acct_report_getlist";
        public const string UpdateTransferAccountedStatus = "usp_UpdateTransferAccountedStatus";
        
        // New Markup

        public const string ProductTypeGetList = "usp_PRODUCT_TYPE_GETLIST";
        public const string MarkupRulesAddUpdate = "USP_MarkupRules_Add_Update";
        public const string MarkupRuleDetailsAddUpdate = "USP_MarkupRuleDetails_Add_Update";
        public const string GetMarkupList = "usp_GetMarkupList";
        public const string GetAgentMarkup = "usp_GetAgentMarkup";
        public const string GetAgentB2CMarkup = "usp_GetAgentB2CMarkup";
        public const string GetAllAgentMarkup = "usp_GetAllAgentMarkup";

        // Flight/Hotel comm Report
        public const string HotelCommReportGetlist = "usp_hotel_comm_report_getlist";
        public const string FlightCommReportGetlist = "usp_flight_Comm_report_getlist";
        public const string GetB2CFixDepartureQueueList = "usp_GetB2C_FixDepQueueList";

        //For Transfers
        public const string AddTransferSource = "usp_AddTransferSource";
        public const string GetTransferSourceById = "usp_GetTransferSourceById";
        public const string UpdateTransferSource = "usp_UpdateTransferSource";
        public const string GetTransferSourceByName = "usp_GetTransferSourceByName";
        public const string GetAllTransferSourceId = "usp_GetAllTransferSourceId";
        //public const string GetTotalFilteredRecordsForTransfer = "usp_GetTotalFilteredRecordsForTransfer";
        //public const string GetTotalFilteredRecordsForTransferCount = "usp_GetTotalFilteredRecordsForTransferCount";
        public const string GetTransferSerivceRequestCount = "usp_GetTransferSerivceRequestCount";
        //public const string GetSerivceTransferRequests = "usp_GetSerivceTransferRequests";


        //For Sightseeings
        public const string AddSightseeingSource = "usp_AddSightseeingSource";
        public const string GetSightseeingSourceById = "usp_GetSightseeingSourceById";
        public const string UpdateSightseeingSource = "usp_UpdateSightseeingSource";
        public const string GetSightseeingSourceByName = "usp_GetSightseeingSourceByName";
        public const string GetAllSightseeingSourceId = "usp_GetAllSightseeingSourceId";
        //public const string GetTotalFilteredRecordsForSightseeingCount = "usp_GetTotalFilteredRecordsForSightseeingCount";
        //public const string GetTotalFilteredRecordsForSightseeing = "usp_GetTotalFilteredRecordsForSightseeing";
        //public const string GetSerivceSightseeingRequests = "usp_GetSerivceSightseeingRequests";
        //public const string GetSightseeingSerivceRequestCount = "usp_GetSightseeingSerivceRequestCount";
        //public const string GetSightseeingChangeRequestData = "usp_GetSightseeingChangeRequestData";

        public const string AddFlightFlexDetails = "usp_AddFlightFlexDetails";
        public const string InsuranceAcctList = "usp_insurance_acct_report_getlist";

        //Baggage Insurance
        public const string AddBaggageHeader = "usp_SaveBaggageHeaderDetails";
        public const string AddBaggagePaxDetails = "usp_SaveBaggagePaxDetails";
        public const string VoucherDetails = "usp_GetBaggageInsuranceVoucher";
        public const string GetBaggageHeader = "usp_GetBaggageHeader";
        public const string GetbaggageQueue = "usp_GetBaggageInsQueue";
        public const string GetPaxDetails = "usp_PaxDetailsByPlanId";
        public const string GetBaggageInsPlanDetails = "usp_GetBaggageInsPlanDetails";
        public const string UpdateBaggageBookingStatus = "usp_UpdateBaggageBookingStatus";
        public const string GetBaggageInsChangeReqQueue = "usp_GetBaggageInsChgReqQueue";
        public const string GetPlanDetailsByPlanID = "usp_GetPlanDetailsByPlanID";
        public const string GetBaggageTrackingPaxDetails = "usp_GetBaggageTrackingPaxDetails";
        public const string UpdatePassengerSupportDocByPaxId = "usp_UpdatePassengerSupportDocByPaxId";
        public const string IsInvoiceGeneratedForBaggageInsurance = "usp_IsInvoiceGeneratedForBaggageInsurance";
        public const string GetBaggageInsTrackingQueue = "usp_GetBaggageInsTrackingQueue";
        public const string UpdateBaggageTrackingStatus = "usp_UpdateBaggageTrackingStatus";
//VIJESH
        public const string GetAgentMarkupDetails = "usp_AgentMarkupGetDetails";
        public const string MarkupRulesDelete = "usp_MarkupRules_Delete";
        public const string GetAgentMarkupDetailsBtMRId = "usp_AgentMarkupGetDetailsByMRId";
        public const string AgentMarkupRuleDetailsAddUpdate = "USP_AgentMarkupRuleDetails_Add_Update"; 
        public const string AgentProductTypeGetList = "usp_AGENTWISE_PRODUCT_TYPE_GETLIST";

        //Offline Entry Flex details
        public const string AddOfflineEntryFlexDetails = "usp_AddOfflineEntryFlexDetails";
        
        

        //Cab Request 
        public const string SaveCabRequestDetails = "usp_SaveCabRequestDetails";
        public const string SaveCabRequestHeader = "usp_SaveCabRequestHeader";
        public const string SaveCabReqRateMaster = "usp_SaveCabReqRateMaster";
        public const string GetCabCharge = "usp_GetCabCharge";
        public const string GetRateDetailsById = "usp_GetRateDetailsById";
        public const string GetCabRequestQueue = "usp_GetCabRequestQueue";
        public const string GetCabPreference = "usp_GetCabPreferences";
        public const string GetCabReferenceNumber = "usp_GetReferenceNumber";
        // For ITimesHotelPendingQueue
        public const string GetITimesHotelPendingQueue = "usp_GetAPI_HotelPendingQueue";
        public const string GetITimesHotelPendingQueueCount = "usp_GetAPI_HotelPendingQueueCount";
        public const string GetITimesHotelPendingQueueDetailByHotelQueueId = "usp_GetAPI_HotelPendingQueueDetailByHotelQueueId";
        public const string AddITimesHotelPendingQueueDetails = "usp_AddAPI_HotelPendingQueueDetails";
        public const string UpdateITimesHotelPendingQueueDetails = "usp_UpdateAPI_HotelPendingQueueDetails";
        public const string UpdateITimesHotelPendingQueueStatus = "usp_UpdateAPI_HotelPendingQueueStatus";

        public const string AddITimesHotelBookingDetails = "usp_AddAPI_HotelBookingDetails";
        public const string UpdateITimesHotelBookingDetails = "usp_UpdateAPI_HotelBookingDetails";
        public const string GetITimesHotelBookingDetails = "usp_GetAPI_HotelBookingDetails";
        public const string GetHotelBookingDetailByTripId = "usp_GetHotelBookingDetailByTripId";
        public const string GetITimesHotelBookingCount = "usp_GetAPI_HotelBookingCount";
        public const string GetAllITimesHotelBookingDetails = "usp_GetAllAPI_HotelBookingDetails";
        public const string GetBookingFullStatus = "usp_GetBookingFullStatus";
        public const string UpdateCustomerStatus = "usp_UpdateCustomerStatus";
    }
}
