﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public class ManageCityMapCode
     {
         public ManageCityMapCode()
         {
         }
         public static DataTable GetList(string CityName,string source)
         {
             SqlDataReader data = null;
             DataTable dt = new DataTable();
             using (SqlConnection connection = DBGateway.GetConnection())
             {
                 SqlParameter[] paramList;
                 try
                 {
                     paramList = new SqlParameter[2];
                     if (!string.IsNullOrEmpty(CityName))
                     {
                         paramList[0] = new SqlParameter("@Destination", CityName);
                     }
                     if (!string.IsNullOrEmpty(source))
                     {
                         paramList[1] = new SqlParameter("@source", source);
                     }
                     data = DBGateway.ExecuteReaderSP(SPNames.CityCodeGetList, paramList, connection);
                     if (data != null)
                     {
                         dt.Load(data);
                     }
                     connection.Close();
                 }
                 catch
                 {
                     throw;
                 }
             }
             return dt;
         }

         public void UpdateCityCode(string Query)
         {
             try
             {
                 SqlParameter[] parameterList = new SqlParameter[1];
                 parameterList[0] = new SqlParameter("@Query", Query);
                 DBGateway.ExecuteNonQuerySP(SPNames.CityCodeUpdate, parameterList);
             }
             catch
             {
                 throw;
             }
         }
         public string SaveCityCode(string columns, string values, string destination, string destinationCode)
         {
             try
             {
                 SqlParameter[] parameterList = new SqlParameter[6];
                 parameterList[0] = new SqlParameter("@P_Columns", columns);
                 parameterList[1] = new SqlParameter("@P_Values", values);
                 parameterList[2] = new SqlParameter("@P_Destination", destination);
                 parameterList[3] = new SqlParameter("@P_CountryCode", destinationCode);
                 parameterList[4] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                 parameterList[4].Direction = ParameterDirection.Output;
                 parameterList[5] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                 parameterList[5].Direction = ParameterDirection.Output;
                 //parameterList[6] = new SqlParameter("@P_validateSource", validateSource);
                 DBGateway.ExecuteNonQuerySP(SPNames.SaveCityCode, parameterList);
                 string messageType = Convert.ToString(parameterList[4].Value);
                 return messageType;
             }
             catch
             {
                 throw;
             }
         }
         public static DataTable  LoadCityCodes()
         {
             SqlDataReader data = null;
             DataTable dt = new DataTable();
             using (SqlConnection connection = DBGateway.GetConnection())
             {
                 SqlParameter[] paramList;
                 try
                 {
                     paramList = new SqlParameter[0];
                     data = DBGateway.ExecuteReaderSP(SPNames.LoadCityCodes, paramList, connection);
                     if (data != null)
                     {
                         dt.Load(data);
                     }
                     connection.Close();
                 }
                 catch
                 {
                     throw;
                 }
             }
             return dt;
         }
     }
}
