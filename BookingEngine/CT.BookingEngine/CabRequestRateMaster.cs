﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

namespace CT.BookingEngine
{

    public class CabRequestRateMaster
    {

        public int Id { get; set; }
        public string RentalType { get; set; }
        public string CabModel { get; set; }
        public string CabPreference { get; set; }
        public decimal Amount { get; set; }
        public int? AgentId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public bool status { get; set; }

        public CabRequestRateMaster()
        {

        }

        public CabRequestRateMaster( long id)
        {
             Load(Convert.ToInt32(id)); 
        }

        public void Save()
        {
            try
            {
                SqlParameter[] parametersList = new SqlParameter[11];
                parametersList[0] = new SqlParameter("@Id", Id);
                parametersList[1] = new SqlParameter("@RentalType", RentalType);
                parametersList[2] = new SqlParameter("@CabPreference", CabPreference);
                parametersList[3] = new SqlParameter("@CabModel", CabModel);
                parametersList[4] = new SqlParameter("@Amount", Amount);
                parametersList[5] = new SqlParameter("@AgentId", AgentId);
                if(Id<=0)
                {
                    parametersList[6] = new SqlParameter("@CreatedBy", CreatedBy);
                    parametersList[7] = new SqlParameter("@ModifiedBy", DBNull.Value);
                }
                else
                {
                    parametersList[6] = new SqlParameter("@CreatedBy", DBNull.Value);
                    parametersList[7] = new SqlParameter("@ModifiedBy", ModifiedBy);
                }
               
                parametersList[8] = new SqlParameter("@Status", status);
                parametersList[9] = new SqlParameter("@MsgType", SqlDbType.VarChar, 10);
                parametersList[9].Direction = ParameterDirection.Output;
                parametersList[10] = new SqlParameter("@MsgText", SqlDbType.VarChar, 200);
                parametersList[10].Direction = ParameterDirection.Output;

                int rowCount = DBGateway.ExecuteNonQuerySP(SPNames.SaveCabReqRateMaster, parametersList);
                string messageType = Utility.ToString(parametersList[9].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parametersList[10].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }

        public DataTable GetCabPreferences(string listCode)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@listCode", listCode);
            return DBGateway.FillDataTableSP(SPNames.GetCabPreference, paramList);
        }

        public void Load(int id)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@Id", id);
                DataTable dt = new DataTable();
                dt = DBGateway.FillDataTableSP(SPNames.GetRateDetailsById, param);
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["RentalType"] != DBNull.Value)
                    {
                        RentalType = dr["RentalType"].ToString();
                    }
                    if (dr["CabPreference"] != DBNull.Value)
                    {
                        CabPreference = dr["CabPreference"].ToString();
                    }
                    if (dr["CabModel"] != DBNull.Value)
                    {
                        CabModel = dr["CabModel"].ToString();
                    }
                    if (dr["Amount"] != DBNull.Value)
                    {
                        Amount = Convert.ToDecimal(dr["Amount"].ToString());
                    }
                    if (dr["Status"] != DBNull.Value)
                    {
                        status = Convert.ToBoolean(dr["Status"]);
                    }
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.ChangeRequest, CT.Core.Severity.High, 0, "Error: while Load Cab Request Master Details.| " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            }


        }
        public DataTable GetRateDetails()
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@Id", DBNull.Value);
            return DBGateway.FillDataTableSP(SPNames.GetRateDetailsById, param);
        }

    }
}
