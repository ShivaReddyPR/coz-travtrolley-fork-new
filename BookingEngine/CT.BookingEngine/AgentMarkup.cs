﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;


namespace CT.BookingEngine
{
    public class AgentMarkup
    {
        #region Member Variables
        private const long NEW_RECORD = -1;
        private int _id;
        private int _mrDId;
        private int _productId;
        private string _sourceId;
        private int _createdBy;

        private int _agentId;
        private decimal _markup;
        private string _markupType;
        private decimal _discount;
        private string _discountType;
        private decimal _handlingFee;
        private string _handlingType;
        private int _retMRId;
        private decimal _agentMarkupVal;
        private decimal _ourCommission;

        private string _bookingType; // Added by somasekhar

        private int _userId;
        private string b2cMarkupType="B2B";
        private decimal _mFDiscount;

        /// <summary>
        /// INT-International, DOM-Domestic
        /// </summary>
        private string flightType;
        /// <summary>
        /// ONW-Onward, RET-Return
        /// </summary>
        private string journeyType;
        /// <summary>
        /// All-GDS & LCC, GDS, LCC
        /// </summary>
        private string carrierType;
        #endregion

        #region properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
         public int MrDId
        {
            get { return _mrDId; }
            set { _mrDId = value; }
         }
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public string SourceId
        {
            get { return _sourceId; }
            set { _sourceId = value; }
        }
         
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public decimal Markup
        {
            get { return _markup; }
            set { _markup = value; }
        }

        public string MarkupType
        {
            get { return _markupType; }
            set { _markupType = value; }
        }

        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        public string DiscountType
        {
            get { return _discountType; }
            set { _discountType = value; }
        }
        public decimal AgentMarkupVal
        {
            get { return _agentMarkupVal; }
            set { _agentMarkupVal = value; }
        }
        public decimal OurCommission
        {
            get { return _ourCommission; }
            set { _ourCommission = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public string B2CMarkupType
        {
            get { return b2cMarkupType; }
            set { b2cMarkupType = value; }
        }
        public string HandlingType
        {
            get { return _handlingType; }
            set { _handlingType = value; }
        }
        public decimal HandlingFee
        {
            get { return _handlingFee; }
            set { _handlingFee = value; }

        }
        public decimal MFDiscount
        {
            get { return _mFDiscount; }
            set { _mFDiscount = value; }
        }

 
             /// <summary>
             /// Possible values are All-DOM & INT, DOM-Domestic, INT-International
             /// </summary>
        public string FlightType
        {
            get
            {
                return flightType;
            }

            set
            {
                flightType = value;
            }
        }
        /// <summary>
        /// Possible values are All-ONW & RET, ONW-Onward, RET-Return
        /// </summary>

        public string JourneyType
        {
            get
            {
                return journeyType;
            }

            set
            {
                journeyType = value;
            }
        }

        /// <summary>
        /// Possible values are All - GDS & LCC, GDS, LCC. Markup 
        /// </summary>
        public string CarrierType
        {
            get
            {
                return carrierType;
            }

            set
            {
                carrierType = value;
            }
        }

        /// <summary>
        /// Added by somesekhar for Booking Type 
        /// Possible values are N-Normal, I-Import
        /// </summary>
        public string BookingType
        {
            get { return _bookingType; }
            set { _bookingType = value; }
        }
        #endregion
        public AgentMarkup()
        {
            _id = -1;
            _mrDId = -1;
        }


        
        public static DataTable AgentProductGetList(RecordStatus recordStatus,int agentId)
        {
            //SqlDataReader data = null;
            DataTable dt = new DataTable();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[3];                    
                    paramList[0] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString<char>((char)recordStatus));
                    if (agentId > 0) paramList[1] = new SqlParameter("@P_AgentId", agentId);
                    dt = DBGateway.FillDataTableSP(SPNames.AgentProductTypeGetList, paramList);
                    //if (data != null)
                    //{
                    //    dt.Load(data);
                    //}
                    //connection.Close();
                } 
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        public static DataTable GetMarkupList(int prodId, string source, int agentId, string agentType)
        {
            //SqlDataReader data = null;
            DataTable dt = new DataTable();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[4];
                    if (prodId > 0) paramList[0] = new SqlParameter("@ProductId", prodId);
                    if (!string.IsNullOrEmpty(source)) paramList[1] = new SqlParameter("@Source", source);
                    if (agentId > 0) paramList[2] = new SqlParameter("@AgentId", agentId);
                    if (!string.IsNullOrEmpty(agentType)) paramList[3] = new SqlParameter("@AgentType", agentType);
                    
                    dt = DBGateway.FillDataTableSP(SPNames.GetMarkupList, paramList);
                    //if (data != null)
                    //{
                    //    dt.Load(data);
                    //}
                    //connection.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
                }
            }
            return dt;
        }


        public static DataTable GetAgentMarkupDetails(int prodId, string source, int agentId,string agentType,string flightType, string journeyType,string carrierType)
        {
            //SqlDataReader data = null;
            DataTable dt = new DataTable();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[7];
                    if (prodId > 0) paramList[0] = new SqlParameter("@ProductId", prodId);
                    if (!string.IsNullOrEmpty(source)) paramList[1] = new SqlParameter("@Source", source);
                    if (agentId > 0) paramList[2] = new SqlParameter("@AgentId", agentId);
                    if (!string.IsNullOrEmpty(agentType)) paramList[3] = new SqlParameter("@AgentType", agentType);
                    if (!string.IsNullOrEmpty(flightType)) paramList[4] = new SqlParameter("@FlightType", flightType);
                    if (!string.IsNullOrEmpty(journeyType)) paramList[5] = new SqlParameter("@JourneyType", journeyType);
                    if (!string.IsNullOrEmpty(carrierType)) paramList[6] = new SqlParameter("@CarrierType", carrierType);
                    dt = DBGateway.FillDataTableSP(SPNames.GetAgentMarkupDetails, paramList);
                    //if (data != null)
                    //{
                    //    dt.Load(data);
                    //}
                    //connection.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
                }
            }
            return dt;
        }


        public static DataTable GetAgentMarkupDetailsbyMRId(int mrId)
        {
            //SqlDataReader data = null;
            DataTable dt = new DataTable();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[1];
                    if (mrId  > 0) paramList[0] = new SqlParameter("@MRId", mrId);                   
                    dt = DBGateway.FillDataTableSP(SPNames.GetAgentMarkupDetailsBtMRId, paramList);
                    //if (data != null)
                    //{
                    //    dt.Load(data);
                    //}
                    //connection.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
                }
            }
            return dt;
        }



        #region Public Methods
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;

            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;


                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@MRId", _id);
                paramList[1] = new SqlParameter("@ProductId", _productId);
                paramList[2] = new SqlParameter("@SourceId",_sourceId);
                paramList[3] = new SqlParameter("@CreatedBy", _createdBy);
                paramList[4] = new SqlParameter("@MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[5].Direction = ParameterDirection.Output;
                paramList[6] = new SqlParameter("@MRId_RETURN", SqlDbType.BigInt);
                paramList[6].Direction = ParameterDirection.Output;
                

                DBGateway.ExecuteNonQuerySP(SPNames.MarkupRulesAddUpdate, paramList);
                string messageType = Utility.ToString(paramList[4].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[5].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _retMRId = Utility.ToInteger(paramList[6].Value);

                SaveMarkupDetails(cmd,_retMRId);
                trans.Commit();
            }
            catch(Exception ex)
            {
                trans.Rollback();
                Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            
        }

        private void SaveMarkupDetails(SqlCommand cmd, int mrId)
        {
            SqlParameter[] paramArr = new SqlParameter[23];
            paramArr[0] = new SqlParameter("@MRDId", _mrDId);
            paramArr[1] = new SqlParameter("@MRId", mrId);
            if (_agentId > 0)
            {
                paramArr[2] = new SqlParameter("@AgentId", _agentId);
            }
            paramArr[3] = new SqlParameter("@Markup", _markup);
            paramArr[4] = new SqlParameter("@MarkupType", _markupType);
            paramArr[5] = new SqlParameter("@Discount", _discount);
            paramArr[6] = new SqlParameter("@DiscountType", _discountType);
            paramArr[7] = new SqlParameter("@CreatedBy", _createdBy);
            SqlParameter paramMsgType = new SqlParameter("@MSG_TYPE", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[8] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@MSG_TEXT", SqlDbType.NVarChar);
            paramMsgText.Size = 500;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[9] = paramMsgText;
            paramArr[10] = new SqlParameter("@AgentMarkup", _agentMarkupVal);
            paramArr[11] = new SqlParameter("@OurCommission", _ourCommission);
            if (_userId > 0)
            {
                paramArr[12] = new SqlParameter("@UserId", _userId);
            }
            else
            {
                paramArr[12] = new SqlParameter("@UserId", DBNull.Value);
            }
            paramArr[13] = new SqlParameter("@transType", b2cMarkupType);
            paramArr[14] = new SqlParameter("@FlightType", flightType == "-1" ? "" : flightType);
            paramArr[15] = new SqlParameter("@JourneyType", journeyType=="-1"? "" : journeyType);
            paramArr[16] = new SqlParameter("@CarrierType", carrierType == "-1" ? "" : carrierType);
            paramArr[17] = new SqlParameter("@HandlingFee",_handlingFee);
            paramArr[18] = new SqlParameter("@HandlingType", _handlingType);

            if(!string.IsNullOrEmpty(_sourceId))
                paramArr[19] = new SqlParameter("@SourceId", _sourceId);  //   03/07/2018 added by arun.
            else
                paramArr[19] = new SqlParameter("@SourceId", DBNull.Value );  //   03/07/2018 added by arun.

            paramArr[20] = new SqlParameter("@ProuctId",_productId );
            //Added by Somasekhar on 19/02/2019 for Flight BookingTpye -- possible Values are N-Normal, I- Import  
            if (!string.IsNullOrEmpty(_bookingType))
                paramArr[21] = new SqlParameter("@BookingType", _bookingType);
            else 
                paramArr[21]=new SqlParameter("@BookingType",DBNull.Value);
            paramArr[22] = new SqlParameter("@MFDiscount", _mFDiscount);
            DBGateway.ExecuteNonQuerySP(SPNames.AgentMarkupRuleDetailsAddUpdate, paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));
        }


        ///DELETE MKP RULES

        public static void DeleteMarkupRules(int mrId, int agentId, int modifiedBy)
        {
            try
            {

                SqlParameter[] paramArr = new SqlParameter[3];
                paramArr[0] = new SqlParameter("@P_MRID", mrId);
                paramArr[1] = new SqlParameter("@P_AGENT_ID", agentId);
                paramArr[2] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);

                DBGateway.ExecuteNonQuerySP(SPNames.MarkupRulesDelete, paramArr);
            }
            catch { throw; }
        }

        #endregion
        /// <summary>
        /// This method is used to return details as per Agent, Source and ProductId.
        /// </summary>
        /// <param name="agentId">AgentId</param>
        /// <param name="source">Source Name</param>
        /// <param name="prodId">ProductId</param>
        /// <returns>DataTable with markup details</returns>
        /// <exception cref="Exception">May throw an exception if any data is missing</exception>
        public static DataTable Load(int agentId, string source, int prodId)
        {
            //UpdateMarkup objUpdateMarkup = new UpdateMarkup();
            //SqlDataReader data = null;
            DataTable dt = new DataTable();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[3];
                    if (agentId > 0) //if (agentId > 1) Skipping Cozmo
                        {
                        paramList[0] = new SqlParameter("@AgentId", agentId);
                    }
                    paramList[1] = new SqlParameter("@Source", source);
                    paramList[2] = new SqlParameter("@ProductId", prodId);
                    dt = DBGateway.FillDataTableSP(SPNames.GetAgentMarkup, paramList);
                    //if (data !=null)
                    //{
                    //    dt.Load(data);
                    //}
                    //connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        /// <summary>
        /// This method is used to return B2C Markup details as User, Source and Product.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="source">Source Name</param>
        /// <param name="prodId">ProductId</param>
        /// <returns>DataTable of B2C Markup details</returns>
        /// <exception cref="Exception">May throw an Exception if any data is missing</exception>
        public static DataTable LoadB2C(int userId, string source, int prodId)
        {
            //UpdateMarkup objUpdateMarkup = new UpdateMarkup();
            //SqlDataReader data = null;
            DataTable dt = new DataTable();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[3];                    
                    paramList[0] = new SqlParameter("@UserId", userId);                    
                    paramList[1] = new SqlParameter("@Source", source);
                    paramList[2] = new SqlParameter("@ProductId", prodId);
                    dt = DBGateway.FillDataTableSP(SPNames.GetAgentB2CMarkup, paramList);
                    //if (data !=null)
                    //{
                    //    dt.Load(data);
                    //}
                    //connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        /// <summary>
        /// This method returns all Markups assigned to an Agent for a Product.
        /// </summary>
        /// <param name="agentId">AgentId</param>
        /// <param name="productId">ProductId</param>
        /// <returns>DateTable of Markup details</returns>
        /// <exception cref="System.Exception">May throw an exception if some data is missing</exception>
        public static DataTable GetAllMarkup(int agentId, int productId)
        {
            DataTable dtMarkup = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@AgentID", agentId);
                paramList[1] = new SqlParameter("@ProductID", productId);
                dtMarkup = DBGateway.FillDataTableSP(SPNames.GetAllAgentMarkup, paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtMarkup;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="productId"></param>
        /// <param name="flightType"></param>
        /// <param name="journeyType"></param>
        /// <param name="carrierType"></param>
        /// <returns></returns>
        public static DataTable GetAllMarkup(int agentId, int productId, string flightType, string journeyType, string carrierType)
        {
            DataTable dtMarkup = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@AgentID", agentId);
                paramList[1] = new SqlParameter("@ProductID", productId);
                paramList[2] = new SqlParameter("@FlightType", flightType);
                paramList[3] = new SqlParameter("@JourneyType", journeyType);
                paramList[4] = new SqlParameter("@CarrierType", carrierType);

                dtMarkup = DBGateway.FillDataTableSP(SPNames.GetAllAgentMarkup, paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtMarkup;
        }

    }


}
