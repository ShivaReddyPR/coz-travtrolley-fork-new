﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;


namespace CT.BookingEngine
{
    public class PromoDetails
    {
        #region PromoDetailsVariables

        int promoTranxId;
        int promoId;
        string promoCode;
        int productId;
        string discountType;
        decimal discountAmount;
        decimal discountValue;
        int referenceId;
        int createdBy;
        #endregion

        #region PromoDetailsProperties
        public int PromoTranxId
        {
            get { return promoTranxId; }
            set { promoTranxId = value; }
        }
        public int PromoId
        {
            get { return promoId; }
            set { promoId = value; }
        }
        
        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }

        public int ProductId
        {
            get { return productId; }
            set { productId = value; }
        }
        public string DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }
        public decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }
        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }
        public int ReferenceId
        {
            get { return referenceId; }
            set { referenceId = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        #endregion

        public void Save()
        {
            try
            {
                SqlParameter[] param = new SqlParameter[8];

                param[0] = new SqlParameter("@promoId", promoId);
                param[1] = new SqlParameter("@promoCode", promoCode);
                param[2] = new SqlParameter("@promoProductId", productId);
                param[3] = new SqlParameter("@promoDiscountType", discountType);
                param[4] = new SqlParameter("@PromoDiscountAmount", discountAmount);
                param[5] = new SqlParameter("@PromoDiscountValue", discountValue);
                param[6] = new SqlParameter("@PromoReferenceId", referenceId);
                param[7] = new SqlParameter("@PromoCreatedBy", createdBy);
                DBGateway.ExecuteNonQuerySP("usp_AddPromoDetails", param);
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
        }

        public static DataTable GetPromoDetails(int productReferenceId)
        {
            DataTable dtPromoDetails = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ItineraryId", productReferenceId);

                dtPromoDetails = DBGateway.FillDataTableSP("usp_GetPromoTranxDetails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(PromoDetails)Failed to load Promo Details for ItineraryId : " + productReferenceId + ", Error:" + ex.ToString(), "");
            }

            return dtPromoDetails;
        }
    }
}
