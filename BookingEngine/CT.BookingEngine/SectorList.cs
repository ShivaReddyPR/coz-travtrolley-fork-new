﻿using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CT.Configuration;

namespace CT.BookingEngine
{
    public class SectorList
    {
        public long SMID { get; set; }
        public string Supplier { get; set; }
        public string Origin { get; set; }
        public string Sectors { get; set; }
        public int Status { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        #region Constructors
        public SectorList()
        {
            SMID = -1;
        }
        public SectorList(long sectorMasterID)
        {
            SMID = sectorMasterID;
            GetDetails(sectorMasterID);
        }
        #endregion

        #region Methods
        public static DataTable GetSuppliers()
        {
            try
            {
                return DBGateway.ExecuteQuery("CT_P_Sector_GetSuppliers").Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetOrigins(string supplier, string origin)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_Sector_Supplier", supplier);
                paramList[1] = new SqlParameter("@P_Sector_Origin", origin);
                return DBGateway.ExecuteQuery("CT_P_Sector_GetOrigins", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        private DataTable GetSectors(long SMID)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_Sector_SMID", SMID);
                return DBGateway.ExecuteQuery("CT_P_Sector_GetSectors", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        private void GetDetails(long sectorMasterID)
        {
            DataTable dt = GetSectors(sectorMasterID);
            UpdateBusinessData(dt);
        }
        private void UpdateBusinessData(DataTable dt)
        {
            try
            {
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        if (Utility.ToLong(dr["SMID"]) > 0)
                        {
                            SMID = Utility.ToLong(dr["SMID"]);
                            Supplier = Utility.ToString(dr["Supplier"]);
                            Origin = Utility.ToString(dr["Origin"]);
                            Sectors = Utility.ToString(dr["Sectors"]);
                            Status = Utility.ToInteger(dr["Status"]);
                            CreatedBy = Utility.ToLong(dr["CreatedBy"]);
                            CreatedOn = Utility.ToDate(dr["CreatedOn"]);
                            if (dr["ModifiedBy"] != DBNull.Value)
                            {
                                ModifiedBy = Utility.ToLong(dr["ModifiedBy"]);
                            }
                            if (dr["ModifiedOn"] != DBNull.Value)
                            {
                                ModifiedOn = Utility.ToDate(dr["ModifiedOn"]);
                            }
                        }
                    }
                }
            }
            catch
            { throw; }
        }
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_Sector_SMID", SMID);
                paramList[1] = new SqlParameter("@P_Sector_Supplier", Supplier);
                paramList[2] = new SqlParameter("@P_Sector_Origin", Origin);
                paramList[3] = new SqlParameter("@P_Sector_Sectors", Sectors);
                paramList[4] = new SqlParameter("@P_Sector_Status", Status);
                paramList[5] = new SqlParameter("@P_Sector_CreatedBy", CreatedBy);
                paramList[6] = new SqlParameter("@P_Sector_ModifiedBy", ModifiedBy);
                if (SMID==-1)
                    paramList[7] = new SqlParameter("@P_Sector_ModifiedOn", DBNull.Value);
                else
                    paramList[7] = new SqlParameter("@P_Sector_ModifiedOn", ModifiedOn);
                paramList[8] = new SqlParameter("@P_MSG_text", SqlDbType.NVarChar, 200);
                paramList[8].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("CT_P_Sector_add_update", paramList);
                string message = Utility.ToString(paramList[8].Value);
                    if (message != string.Empty) throw new Exception(message);
                trans.Commit();
                
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
                GetSectorList(true);
            }
        }

        public void Save(List<SectorList> lstSector)
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                foreach (SectorList sl in lstSector)
                {
                    SqlParameter[] paramList = new SqlParameter[9];
                    paramList[0] = new SqlParameter("@P_Sector_SMID", sl.SMID);
                    paramList[1] = new SqlParameter("@P_Sector_Supplier", sl.Supplier);
                    paramList[2] = new SqlParameter("@P_Sector_Origin", sl.Origin);
                    paramList[3] = new SqlParameter("@P_Sector_Sectors", sl.Sectors);
                    paramList[4] = new SqlParameter("@P_Sector_Status", sl.Status);
                    paramList[5] = new SqlParameter("@P_Sector_CreatedBy", sl.CreatedBy);
                    paramList[6] = new SqlParameter("@P_Sector_ModifiedBy", sl.ModifiedBy);
                    if (sl.SMID == -1)
                        paramList[7] = new SqlParameter("@P_Sector_ModifiedOn", DBNull.Value);
                    else
                        paramList[7] = new SqlParameter("@P_Sector_ModifiedOn", sl.ModifiedOn);
                    paramList[8] = new SqlParameter("@P_MSG_text", SqlDbType.NVarChar, 200);
                    paramList[8].Direction = ParameterDirection.Output;
                    DBGateway.ExecuteNonQuery("CT_P_Sector_add_update", paramList);
                    string message = Utility.ToString(paramList[8].Value);
                    if (message != string.Empty) throw new Exception(message);
                    
                }

                trans.Commit();
                
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
                GetSectorList(true);
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_Sector_SMID", SMID);
                paramList[1] = new SqlParameter("@P_Sector_Status", Status);
                paramList[2] = new SqlParameter("@P_Sector_ModifiedBy", ModifiedBy);
                paramList[3] = new SqlParameter("@P_Sector_ModifiedOn", ModifiedOn);
                paramList[4] = new SqlParameter("@P_Sector_Operation_Type", "DELETE");
                paramList[5] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[5].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("CT_P_Sector_add_update", paramList);
                string message = Utility.ToString(paramList[5].Value);
                    if (message != string.Empty) throw new Exception(message);
                GetSectorList(true);
            }
            catch
            {
                throw;
            }
        }

        public static List<SectorList> GetSectorList(bool saveInDB)
        {
            List<SectorList> lstSectors = new List<SectorList>();

            try
            {
                if (CacheData.CheckSectorList() && !saveInDB)
                {
                    foreach (KeyValuePair<string,string> item in CacheData.SectorsList)
                    {
                        SectorList sectorList = new SectorList();
                        sectorList.Supplier = item.Key.Split('-')[0];
                        sectorList.Origin = item.Key.Split('-')[1];
                        sectorList.Sectors = item.Value;
                        lstSectors.Add(sectorList);
                    }
                }
                else
                {
                    DataSet ds = DBGateway.ExecuteQuery("CT_P_Sector_GetSectorList");
                    lstSectors = ds.Tables[0].AsEnumerable().Select(r => new SectorList
                    {
                        // SMID = r.Field<long>(1),
                        Supplier = r.Field<string>("Supplier"),
                        Origin = r.Field<string>("Origin"),
                        Sectors = r.Field<string>("Sectors"),
                        //  Status = r.Field<int>(1)
                    }).ToList();
                    CacheData.SectorsList.Clear();
                    foreach (SectorList sectorlist in lstSectors)
                    {
                        CacheData.SectorsList.Add(sectorlist.Supplier+"-"+sectorlist.Origin, sectorlist.Sectors);
                    }
                    
                }
                

                
            }
            catch(Exception ex)
            {
                throw;
            }

            return lstSectors;
        }
        #endregion
    }
}
