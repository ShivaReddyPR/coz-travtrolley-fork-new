using System;
using System.Collections.Generic;

namespace CT.BookingEngine
{
    [Serializable]
    public class TravelPolicyResult
    {

        bool _isUnderPolicy = true;
        bool _isPreferredAirline = false;
        bool _isNegotiatedFare = false;
        Dictionary<string, List<string>> _policyBreakingRules = new Dictionary<string, List<string>>();
        bool _isLLFApplied;
        double _lowestAmount;
        string _lowestFlight;
        string _cabinType;
        string cheapestFlight;
        double cheapestAmount;
        bool _isLLFResult=false;

        public bool IsUnderPolicy
        {
            get
            {
                return _isUnderPolicy;
            }
            set
            {
                _isUnderPolicy = value;
            
            }
        }
        public bool IsPreferredAirline
        {
            get
            {
                return _isPreferredAirline;
            }
            set
            {
                _isPreferredAirline = value;

            }
        }

        public bool IsNegotiatedFare
        {
            get { return _isNegotiatedFare; }
            set { _isNegotiatedFare = value; }
        }
        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<string, List<string>> PolicyBreakingRules
        {
            get
            {
                return _policyBreakingRules;
            }
            set
            {
                _policyBreakingRules = value;

            }
        }
        public bool IsLLFResult
        {
            get
            {
                return _isLLFResult;
            }
            set
            {
                _isLLFResult = value;

            }
        }

        /// <summary>
        /// Lowest Logical Fare applied based on the hours cap
        /// </summary>
        public bool IsLLFApplied { get => _isLLFApplied; set => _isLLFApplied = value; }
        /// <summary>
        /// Lowest amount applicable in the searched result list
        /// </summary>
        public double LowestAmount { get => _lowestAmount; set => _lowestAmount = value; }
        /// <summary>
        /// For comparing with lowest fare cabin type
        /// </summary>
        public string CabinType { get => _cabinType; set => _cabinType = value; }
        /// <summary>
        /// Store the Lowest fare flight details for DI entry
        /// </summary>
        /// <example>EK456 DXB-JED,EK457 JED-DXB</example>
        public string LowestFlight { get => _lowestFlight; set => _lowestFlight = value; }
        /// <summary>
        /// Assign lowest fare flight details based on price ascending sort for DI entry.
        /// </summary>
        /// <example>EK456 DXB-JED,EK457 JED-DXB</example>
        public string CheapestFlight { get => cheapestFlight; set => cheapestFlight = value; }
        /// <summary>
        /// Assign lowest fare flight amount based on price ascending sort for DI entry.
        /// </summary>
        public double CheapestAmount { get => cheapestAmount; set => cheapestAmount = value; }
    }
        

       
}
