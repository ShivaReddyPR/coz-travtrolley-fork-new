﻿using System;
using System.Collections.Generic;
//using System.Linq;

namespace CT.BookingEngine
{
    public class SightSeeingReguest
    {
        private string countryName;                      //Country Code
        private string destinationType;                  //Destination type, can be "city" or "area"
        private string destinationCode;                  //Destination code according to the DestinationType
        private string itemName;                         //filter by itemName
        private string itemCode;                         //filter by itemCode
        private DateTime tourDate;                       //Tour date
        private int noOfAdults;                          //number of adults
        private List<int> childrenList;                  //children age list
        private List<string> typeCodeList;               //filter by types
        private List<string> categoryCodeList;           //filter by category
        private int childCount;
        //Not Needed for Search
        private string specialCode;
        private string cityName;
        private string language; //Tour Language
        private int noDays;
        private string currency;
        //VAT Changes
        string loginCountryCode;
        string countryCode;
        // Added on 09/2019 by somasekhar for CZA activity
        //for sending in the Search Request
        private string nationality;     //Nationality Code
        private string transType;
        private List<string> sources;
        private long agencyId;
        private long locationId;
        #region Properties
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public string DestinationType
        {
            get { return destinationType; }
            set { destinationType = value; }
        }
        public string DestinationCode
        {
            get { return destinationCode; }
            set { destinationCode = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public DateTime TourDate
        {
            get { return tourDate; }
            set { tourDate = value; }
        }
        public int NoOfAdults
        {
            get { return noOfAdults; }
            set { noOfAdults = value; }
        }
        public List<int> ChildrenList
        {
            get { return childrenList; }
            set { childrenList = value; }
        }
        public List<string> TypeCodeList
        {
            get { return typeCodeList; }
            set { typeCodeList = value; }
        }
        public List<string> CategoryCodeList
        {
            get { return categoryCodeList; }
            set { categoryCodeList = value; }
        }
        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }
        public string SpecialCode
        {
            get { return specialCode; }
            set { specialCode = value; }
        }
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        public string Language
        {
            get { return language; }
            set { language = value; }
        }
        public int NoDays
        {
            get { return noDays; }
            set { noDays = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        public string LoginCountryCode
        {
            get
            {
                return loginCountryCode;
            }

            set
            {
                loginCountryCode = value;
            }
        }

        public string CountryCode
        {
            get
            {
                return countryCode;
            }

            set
            {
                countryCode = value;
            }
        }
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }
        public List<string> Sources
        {
            get { return sources; }
            set { sources = value; }
        }
        public long AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }
        public long LocationId
        {
            get { return locationId; }
            set { locationId = value; }
        }
        #endregion
        #region PUBLIC METHODS
        public SightSeeingReguest CopyByValue()
        {
            SightSeeingReguest sRquest = new SightSeeingReguest();
            sRquest.countryName = countryName;
            sRquest.destinationType = destinationType;
            sRquest.destinationCode = destinationCode;
            sRquest.itemName = itemName;
            sRquest.itemCode = itemCode;
            sRquest.tourDate = tourDate;
            sRquest.noOfAdults = noOfAdults;
            sRquest.childrenList = childrenList;
            sRquest.typeCodeList = typeCodeList;
            sRquest.categoryCodeList = categoryCodeList;
            sRquest.childCount = childCount;
            sRquest.specialCode = specialCode;
            sRquest.cityName = cityName;
            sRquest.language = language;
            sRquest.noDays = noDays;
            sRquest.currency = currency;
            sRquest.loginCountryCode = loginCountryCode;
            sRquest.countryCode = countryCode;
            return sRquest;
        }
        #endregion

    }
}
