﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Data.SqlClient;

namespace CT.BookingEngine
{
    //Cancel Policy Updates
    public class HotelCancelPolicy
    {
        /// <summary>
        /// HotelID
        /// </summary>
        public int HotelID { get; set; }
        /// <summary>
        /// RoomID
        /// </summary>
        public int RoomID { get; set; }
        /// <summary>
        /// FromDate
        /// </summary>
        public DateTime? FromDate { get; set; }
        /// <summary>
        /// Todate
        /// </summary>
        public DateTime? Todate { get; set; }
        /// <summary>
        /// ChargeType
        /// </summary>
        public string ChargeType { get; set; }
        /// <summary>
        /// BufferDays
        /// </summary>
        public int? BufferDays { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal? Amount { get; set; }
        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// Currency
        /// </summary>
        public string Currency { get; set; }

        # region Methods
        /// <summary>
        ///  This method is to save the Record into HotelCancelPolicy DB
        /// </summary>
        public void Save(HotelCancelPolicy hotelCancelPolicy)
        {
            SqlParameter[] paramList = new SqlParameter[9];
            paramList[0] = new SqlParameter("@HotelID", hotelCancelPolicy.HotelID);
            paramList[1] = new SqlParameter("@RoomID", hotelCancelPolicy.RoomID);
            paramList[2] = new SqlParameter("@FromDate", hotelCancelPolicy.FromDate);
            paramList[3] = new SqlParameter("@Todate", hotelCancelPolicy.Todate);
            paramList[4] = new SqlParameter("@ChargeType", hotelCancelPolicy.ChargeType);
            paramList[5] = new SqlParameter("@BufferDays", hotelCancelPolicy.BufferDays);
            paramList[6] = new SqlParameter("@Amount", hotelCancelPolicy.Amount);
            paramList[7] = new SqlParameter("@Remarks", hotelCancelPolicy.Remarks);
            paramList[8] = new SqlParameter("@Currency", hotelCancelPolicy.Currency);
            DBGateway.ExecuteNonQuerySP("usp_AddHotelCancelPolicy", paramList);
        }
        #endregion
    }
}
