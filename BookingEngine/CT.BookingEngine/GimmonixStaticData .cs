﻿using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine
{
   public class GimmonixStaticData
    {
        #region Fields and their Properties
        private const int NEW_RECORD = -1;
        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _hotelId;
        private string _hotelName;
        public string HotelName
        {
            get { return _hotelName; }
            set { _hotelName = value; }
        }
        public string HotleId
        {
            get { return _hotelId; }
            set { _hotelId = value; }
        }
        private string _countryName;
        public string CountryName
        {
            get { return _countryName; }
            set { _countryName = value; }
        }
        private string _cityName;
        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }
        private string _address;
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        private string _zipCode;
        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }
        private int _rating;
        public  int Rating
        {
            get { return _rating; }
            set { _rating = value; }
        }
        private string _lat;
        public string LAT
        {
            get { return _lat; }
            set { _lat = value; }
        }
        private string _lng;
        public string LNG
        {
            get { return _lng; }
            set { _lng = value; }
        }
        private int _roomCout;
        public int RoomCount
        {
            get { return _roomCout; }
            set { _roomCout = value; }
        }
        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _webSite;
        public string WebSite
        {
            get { return _webSite; }
            set { _webSite = value; }
        }
        private string _pCategory;
        public string PCategory
        {
            get { return _pCategory; }
            set { _pCategory = value; }
        }
        private string _chainCode;
        public string ChainCode
        {
            get { return _chainCode; }
            set { _chainCode = value; }
        }
        private string _hFacility;
        public string HFacility
        {
            get { return _hFacility; }
            set { _hFacility = value; }
        }
        private string _iUrl;
        public string ImageUrl
        {
            get { return _iUrl; }
            set { _iUrl = value; }
        }
        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        private int _modifiedBy;
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
		private int _rezliveId;
		public int RezliveID
		{
			get { return _rezliveId; }
			set { _rezliveId = value; }
		}
		private int _hisId;
		public int HisID
		{
			get { return _hisId; }
			set { _hisId = value; }
		}
		private string _illusionshotelcode;
		public string IllusionsHotelCode
		{
			get { return _illusionshotelcode; }
			set { _illusionshotelcode = value; }
		}
		private string _illusionscitycode;
		public string IllusionsCityCode
		{
			get { return _illusionscitycode; }
			set { _illusionscitycode = value; }
		}
		#endregion
		public GimmonixStaticData()
        {
            ID = NEW_RECORD;
        }
        public GimmonixStaticData(int _id)
        {
            ID = _id;
            getDetails(_id);
        }
        private void getDetails(int id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
        private DataSet GetData(int id)
        {

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add( new SqlParameter("@P_ID", id));
                DataSet dsResult = DBGateway.ExecuteQuery("Usp_HotelStatic_getData", parameters.ToArray());
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["HotelstaticID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["HotelstaticID"]);
                _hotelId = Utility.ToString(dr["TravolutionaryID"]);
                _hotelName = Utility.ToString(dr["DisplayName"]);
                _countryName = Utility.ToString(dr["CountryCode"]);
                _cityName = Utility.ToString(dr["CityName"]);
                _address = Utility.ToString(dr["Address"]);
                _zipCode = Utility.ToString(dr["ZipCode"]);
                _rating = Utility.ToInteger(dr["StarRating"]);
                _lat = Utility.ToString(dr["Lat"]);
                _lng = Utility.ToString(dr["Lng"]);
                _phone = Utility.ToString(dr["Phone"]);
                _roomCout = Utility.ToInteger(dr["RoomCount"]);
                _email = Utility.ToString(dr["Email"]);
                _webSite = Utility.ToString(dr["WebSite"]);
                _pCategory = Utility.ToString(dr["PropertyCategory"]);
                _chainCode = Utility.ToString(dr["ChainCode"]);
                _hFacility = Utility.ToString(dr["hotelfacility"]);
                _iUrl = Utility.ToString(dr["ImageUrl"]);
                _description = Utility.ToString(dr["description"]);
				_rezliveId = Utility.ToInteger(dr["rezliveID"]);
				_hisId = Utility.ToInteger(dr["HisID"]);
				_illusionshotelcode = Utility.ToString(dr["Illusionshotelcode"]);
				_illusionscitycode= Utility.ToString(dr["Illusionscitycode"]);
			}
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetCountryList()
        {
            //DataTable dt = new DataTable();
            try
            {
               return DBGateway.ExecuteQuery("usp_GetCountryList").Tables[0];
                //SortedList countryList = new SortedList();
                //SqlParameter[] paramList = new SqlParameter[0];
                //using (DataTable dtCountry = DBGateway.FillDataTableSP("usp_GetCountryList", paramList))
                //{
                //    if (dtCountry != null && dtCountry.Rows.Count > 0)
                //    {
                //        foreach (DataRow data in dtCountry.Rows)
                //        {
                //            if (data["countryName"] != DBNull.Value && data["countryCode"] != DBNull.Value)
                //            {
                //                countryList.Add(Convert.ToString(data["countryName"]), Convert.ToString(data["countryCode"]));
                //            }
                //        }
                //    }
                //}

                //countryList.TrimToSize();
                //return countryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return dt;
        }
        public void Save()
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@p_Id", _id));
                parameters.Add(new SqlParameter("@p_hotelid", _hotelId));
                parameters.Add(new SqlParameter("@p_hotelname", _hotelName));
                parameters.Add(new SqlParameter("@p_countryName", _countryName));
                parameters.Add(new SqlParameter("@p_cityName", _cityName));
                parameters.Add(new SqlParameter("@p_address", _address));
                parameters.Add(new SqlParameter("@p_zipcode", _zipCode));
                parameters.Add(new SqlParameter("@p_rating", _rating));
                parameters.Add(new SqlParameter("@p_lat", _lat));
                parameters.Add(new SqlParameter("@p_lng", _lng));
                parameters.Add(new SqlParameter("@p_rcount", _roomCout));
                parameters.Add(new SqlParameter("@p_phone", _phone));
                parameters.Add(new SqlParameter("@p_email", _email));
                parameters.Add(new SqlParameter("@p_webSite", _webSite));
                parameters.Add(new SqlParameter("@p_pcategory", _pCategory));
                parameters.Add(new SqlParameter("@p_chainCode", _chainCode));
                parameters.Add(new SqlParameter("@p_hFacility", _hFacility));
                parameters.Add(new SqlParameter("@p_imaeurl", _iUrl));
                parameters.Add(new SqlParameter("@p_description", _description));
                parameters.Add(new SqlParameter("@p_lastupdatedby",_modifiedBy));
                SqlParameter sqlParameter = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                sqlParameter.Direction = ParameterDirection.Output;
                parameters.Add(sqlParameter);
                SqlParameter parameter = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                parameter.Direction = ParameterDirection.Output;
                parameters.Add(parameter);
				parameters.Add(new SqlParameter("@p_rezliveid", _rezliveId));
				parameters.Add(new SqlParameter("@p_hisid", _hisId));
				parameters.Add(new SqlParameter("@p_Illusionshotelcode", _illusionshotelcode));
				parameters.Add(new SqlParameter("@p_Illusionscitycode", _illusionscitycode));
				DBGateway.ExecuteNonQuerySP("Usp_HotelStaticData_ADD_Update", parameters.ToArray());
                string messageType = Utility.ToString(sqlParameter.Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameter.Value);
                    if (message != string.Empty) throw new Exception(message);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetSearchDetails(string Country,string Hid,string Hname,string City,int?rating,string pnumber,string mail,string wSite)
        {
            DataTable dt = new DataTable();
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
               if(!string.IsNullOrEmpty(Country)) parameters.Add(new SqlParameter("@p_country", Country));
               if(!string.IsNullOrEmpty(Hid)) parameters.Add(new SqlParameter("@p_hid", Hid));
               if (!string.IsNullOrEmpty(Hname)) parameters.Add(new SqlParameter("@p_hname", Hname));
               if (!string.IsNullOrEmpty(City)) parameters.Add(new SqlParameter("@p_city", City));
               if(rating!=null) parameters.Add(new SqlParameter("@p_Rating", rating));
               if(!string.IsNullOrEmpty(pnumber)) parameters.Add(new SqlParameter("@p_Pnumber", pnumber));
               if (!string.IsNullOrEmpty(mail)) parameters.Add(new SqlParameter("@p_mail", mail));
               if (!string.IsNullOrEmpty(wSite)) parameters.Add(new SqlParameter("@p_wsite", wSite));
               dt = DBGateway.FillDataTableSP("usp_HotelStaticData_details", parameters.ToArray());

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public static string GetUpdateStatus(int id)
        {
            try
            {
                SqlParameter[] parameter = new SqlParameter[2];
                parameter[0] = new SqlParameter("@p_id", id);
                parameter[1] = new SqlParameter("@value", SqlDbType.VarChar, 100);
                parameter[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("usp_HotelStaticData_UpdateStatus", parameter);
                string value = Utility.ToString(parameter[1].Value);
                return value;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
