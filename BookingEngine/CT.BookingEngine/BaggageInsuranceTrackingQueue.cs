﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine
{
    public class BaggageInsuranceTrackingQueue
    {
        public DataTable GetBaggageInsuranceTrackingQueueList(int agentId, DateTime date, string Pnr, string agentType)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@AgentId", agentId);
            if (!string.IsNullOrEmpty(Pnr))
            {
                param[1] = new SqlParameter("@PNR", Pnr);
            }
            else
            {
                param[1] = new SqlParameter("@PNR", DBNull.Value);
            }

            param[2] = new SqlParameter("@CreatedDate", date);
           
            if (!string.IsNullOrEmpty(agentType))
            {
                param[3] = new SqlParameter("@AgentType", agentType);
            }
            else
            {
                param[3] = new SqlParameter("@AgentType", DBNull.Value);
            }
            
            dt = DBGateway.FillDataTableSP(SPNames.GetBaggageInsTrackingQueue, param);

            return dt;
        }
           
    }
}
