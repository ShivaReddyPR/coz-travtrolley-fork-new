﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine
{
    public class SightSeeingCancellationDetails
    {
        int resultIndex;
        string introduction;
        string[] itineraryDetails;
        string activityDetails;
        string[] cancellationPolicy;
        string activityName;
        string activityCityCode;
        SightSeeingFlexFields[] sightSeeingFlexFields;
        string[] notes;
        string image;
        string includes;
        string departurePoint;
        List<ActivityTourOperation> tourOperation;
        string excludes;

        public int ResultIndex
        {
            get { return resultIndex; }
            set { resultIndex = value; }
        }
        public string Introduction
        {
            get { return introduction; }
            set { introduction = value; }
        }
        public string[] ItineraryDetails
        {
            get { return itineraryDetails; }
            set { itineraryDetails = value; }
        }
        public string[] CancellationPolicies
        {
            get { return cancellationPolicy; }
            set { cancellationPolicy = value; }
        }
        public string ActivityDetails
        {
            get { return activityDetails; }
            set { activityDetails = value; }
        }
        public string ActivityCityCode
        {
            get { return activityCityCode; }
            set { activityCityCode = value; }
        }
        public string ActivityName
        {
            get { return activityName; }
            set { activityName = value; }
        }
        public SightSeeingFlexFields[] SightSeeingFlexFields
        {
            get { return sightSeeingFlexFields; }
            set { sightSeeingFlexFields = value; }
        }
        public string[] Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        public string Image
        {
            get { return image; }
            set { image = value; }
        }
        public string Includes
        {
            get { return includes; }
            set { includes = value; }
        }
        public string DeparturePoint
        {
            get { return departurePoint; }
            set { departurePoint = value; }
        }
        public List<ActivityTourOperation> TourOperation
        {
            get { return tourOperation; }
            set { tourOperation = value; }
        }
        public string Excludes
        {
            get { return excludes; }
            set { excludes = value; }
        }

    }

    public class ActivityTourOperation
    {
        string itemCode;
        string cityCode;
        string depName;
        string fromTime;
        string toTime;
        DateTime fromDate;
        DateTime toDate;

        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string DepName
        {
            get { return depName; }
            set { depName = value; }
        }
        public string FromTime
        {
            get { return fromTime; }
            set { fromTime = value; }
        }
        public string ToTime
        {
            get { return toTime; }
            set { toTime = value; }
        }
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
    }

    public class SightSeeingFlexFields
    {
        string flexFieldsName;
        string flexFieldDataType;
        bool flexFieldMandatory;

        public string FlexFieldsName
        {
            get { return flexFieldsName; }
            set { flexFieldsName = value; }
        }
        public string FlexFieldDataType
        {
            get { return flexFieldDataType; }
            set { flexFieldDataType = value; }
        }
        public bool FlexFieldsMandatory
        {
            get { return flexFieldMandatory; }
            set { flexFieldMandatory = value; }
        }
    }
}
