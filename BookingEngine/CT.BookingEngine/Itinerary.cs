using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Collections;
using System.IO;
using CT.Core;

namespace CT.BookingEngine
{
    public enum ItineraryStatus
    {
        NoStatus=0,
        OnHold = 1,
        Ticketed = 2
    }

    public enum BookingSource
    {
        Default=0,
        WorldSpan = 1,
        Abacus = 2,
        SpiceJet = 3,
        Amadeus = 4,
        Galileo = 5,
        Indigo = 6,
        Paramount = 7,
        AirDeccan = 8,
        Mdlr = 9,
        GoAir = 10,
        ThirdParty = 11,
        Sama = 12,
        AirArabia = 13,
        AirIndiaExpressIntl = 14,
        //AirIndiaExpressDom = 15,
        HermesAirLine = 16,
        FlyDubai = 17,
        AlJazeera = 18,
        UAPI = 20,
        TBOAir=21,
        FlightInventory=22,
        PKFares=23,
        Baggage=24,
        OffLine = 25,
        SpiceJetCorp = 26,
        IndigoCorp = 27,
        Jazeera = 28,
        GoAirCorp=29,
        CozmoBus=30,
        Babylon=31,
        SalamAir=32, //Added By suresh V
	Sabre=33

    }
    [Serializable]
    public class FlightItinerary : CT.BookingEngine.Product
    {
        #region PrivateMembers
        /// <summary>
        /// Id of flight booked
        /// </summary>
        int flightId;
        /// <summary>
        /// Flight segments
        /// </summary>
        FlightInfo[] segments;
        /// <summary>
        /// Details of other passengers
        /// </summary>
        FlightPassenger[] passenger;
        /// <summary>
        /// Fare Rules
        /// </summary>
        List<FareRule> fareRules;
        /// <summary>
        /// PNR of the booking.
        /// </summary>
        string pnr;
        /// <summary>
        /// Booking source/GDS
        /// </summary>
        BookingSource flightBookingSource;
        /// <summary>
        /// Origin airport of the OND
        /// </summary>
        private string origin;
        /// <summary>
        /// Destination airport of the OND
        /// </summary>
        private string destination;
        /// <summary>
        /// Last ticketing date
        /// </summary>
        private DateTime lastTicketDate;
        /// <summary>
        /// Ticket advisory showing additional information or instruction about ticketing.
        /// </summary>
        private string ticketAdvisory;
        /// <summary>
        /// Fare type ( Published, Securate, or ....)
        /// </summary>
        private string fareType;
        /// <summary>
        /// Indicates the number of hits made for generating e-Ticket for this itinerary.
        /// </summary>
        private int eTicketHit;
        /// <summary>
        /// validating airline code
        /// </summary>
        private string airlineCode;
        /// <summary>
        /// first segment departure date is travel date
        /// </summary>
        private DateTime travelDate;
        /// <summary>
        /// Mode how booking is made(Import or Automatic or Manual)
        /// </summary>
        private BookingMode bookingMode;
        /// <summary>
        /// List of SSRs associated with this flight.
        /// </summary>
        private List<SSR> ssr;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// pricing type infomation ie "AUTO PRICED"
        /// </summary>
        private string pricingType;
        /// <summary>
        /// 
        /// </summary>
        private string validatingAirline;
        /// <summary>
        /// Two character airline code of the validating airline.
        /// </summary>
        private string validatingAirlineCode;
        private string tourCode;

        private string endorsement;
        /// <summary>
        /// Ticket Information as given by worldspan.
        /// </summary>
        private bool ticketed;
        /// <summary>
        /// Flag indicating if the flight is domestic.
        /// Public proper
        /// </summary>
        private bool isDomestic;
        /// <summary>
        /// Mode of payment.
        /// </summary>
        private ModeOfPayment paymentMode;
        /// <summary>
        /// Credit Card Payment detail.
        /// </summary>
        private CCPayment ccPayment;
        /// <summary>
        /// True if the fare is non refundable.
        /// </summary>
        private bool nonRefundable;
        /// <summary>
        /// agencyid of the agency to which the booking belongs
        /// </summary>
        private int agencyId;
        /// <summary>
        /// booking id of this itinerary
        /// </summary>
        private int bookingId;
        /// <summary>
        /// stores the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        string aliasAirlineCode;
        /// <summary>
        /// Tell wheter booking is done by other  
        /// </summary>
        private bool isOurBooking = true;

        string supplierCode;


        /// <summary>
        /// Assignig UAPI Pricing Solution object
        /// </summary>
        UAPIdll.Air46.AirPricingSolution uapiPricingSolution;
        /// <summary>
        /// Assignig UAPI Pricing Solution Key
        /// </summary>
        int uapiPricingSolutionKey;

        /// <summary>
        /// Assignig UAPI UR
        /// </summary>
        string universalRecord;// For 

        /// <summary>
        /// Assignig UAPI Provider PNR (Gelileo)
        /// </summary>
        string airLocatorCode;// For storing Air Reservation Locator code to issue the Ticket 
        string supplierLocatorCode;// For storing Airline Pnr for future info
        int locationId;
        string locationName;
        string specialRequest;
        
        string guid;
        /// <summary>
        /// Set 'True' if included in Fare for FlyDubai.
        /// </summary>
        private bool isBaggageIncluded;

        /// <summary>
        /// Whether Insurance is added or not 
        /// </summary>
        private bool isInsured;
        /// <summary>
        /// Whether B2B or B2C transaction. Default B2B.
        /// </summary>
        private string transactionType = "B2B";
   
        /// <summary>
        /// For TBO Air
        /// </summary>
        private bool isLcc;
        /// <summary>
        /// 
        /// </summary>
        private Fare[] tboFareBreakdown;
        /// <summary>
        /// TripId is used for Corporate profile bookings 
        /// which will be grouped based on TripId. Three
        /// bookings will be saved with this TripId in order
        /// to identify whether bookings to corporate or not.
        /// </summary>
        private string tripId;

        /// <summary>
        /// Corporate Flight Policies saved against booking
        /// </summary>
        FlightPolicy flightPolicy;
        /// <summary>
        /// Check whether GST fields need to pass for the supplier while booking for TBOAir
        /// </summary>
        private bool isGSTMandatory;

        private string gstCompanyAddress;

        private string gstCompanyContactNumber;

        private string gstCompanyName;

        private string gstNumber;

        private string gstCompanyEmail;
        //end GST fields for TBOAir

        private string routingTripId;

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        private BookingFlowStatus _sBookRequestType;

        /// <summary>
        /// To hold pending amount for itinerary booking
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        private decimal _dcItineraryAmountDue;

        private bool _isSpecialRoundTrip;//For Combination Search If both onward and return results are SpecialRoundTripFares;
        private bool _sendmail;
        private bool _hotelReq;// Corp req Status for Hotel
        private string _sDynamicHAP;

        private SearchType resultType;//Added For Babylon purpose
        /// <summary>
        /// To store Booking user machine IP address
        /// </summary>
        string _BookUserIP;

        private int salesExecutiveId = 0;// For Offline Entry

        private int isEdit = 0;// For Offline Entry

        private string saleType = "N";

        private bool isOnBehalfAgent;
        #endregion

        #region Public Properties
        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public string UniversalRecord
        {
            get
            {
                return universalRecord;
            }
            set
            {
                universalRecord = value;
            }
        }

        
        public string AirLocatorCode
        {
            get
            {
                return airLocatorCode;
            }
            set
            {
                airLocatorCode = value;
            }
        }
        public string SupplierLocatorCode
        {
            get
            {
                return supplierLocatorCode;
            }
            set
            {
                supplierLocatorCode = value;
            }
        }

        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public int UapiPricingSolutionKey
        {
            get
            {
                return uapiPricingSolutionKey;
            }
            set
            {
                uapiPricingSolutionKey = value;
            }
        }

        /// <summary>
        /// Assing UAPI Pricing Solution Object
        /// </summary>        
        public UAPIdll.Air46.AirPricingSolution UapiPricingSolution
        {
            get
            {
                return uapiPricingSolution;
            }
            set
            {
                uapiPricingSolution= value;
            }
        }
        /// <summary>
        /// Gets or sets the flightId
        /// </summary>
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }
        /// <summary>
        /// Gets or sets the flight segments
        /// </summary>
        public FlightInfo[] Segments
        {
            get
            {
                return segments;
            }
            set
            {
                segments = value;
            }
        }
        /// <summary>
        /// Gets or sets other passengers' detail
        /// </summary>
        public FlightPassenger[] Passenger
        {
            get
            {
                return passenger;
            }
            set
            {
                passenger = value;
            }
        }
        /// <summary>
        /// Gets or sets the fare rules list.
        /// </summary>
        public List<FareRule> FareRules
        {
            get
            {
                return fareRules;
            }
            set
            {
                fareRules = value;
            }
        }
        /// <summary>
        /// Gets or sets the PNR of booking
        /// </summary>
        public string PNR
        {
            get
            {
                return pnr;
            }
            set
            {
                pnr = value;
            }
        }
        /// <summary>
        /// Gets or sets the flight booking source
        /// </summary>
        public BookingSource FlightBookingSource
        {
            get
            {
                return flightBookingSource;
            }
            set
            {
                flightBookingSource = value;
            }
        }
        /// <summary>
        /// Gets or sets the destination airport of the OND
        /// </summary>
        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }
        /// <summary>
        /// Gets or sets the fare type
        /// </summary>
        public string FareType
        {
            get
            {
                return fareType;
            }
            set
            {
                fareType = value;
            }
        }
        /// <summary>
        /// Gets or sets the last ticketing date
        /// </summary>
        public DateTime LastTicketDate
        {
            get
            {
                return lastTicketDate;
            }
            set
            {
                lastTicketDate = value;
            }
        }
        /// <summary>
        /// Gets or sets ticketAdvisory.
        /// </summary>
        public string TicketAdvisory
        {
            get
            {
                return ticketAdvisory;
            }
            set
            {
                ticketAdvisory = value;
            }
        }
        /// <summary>
        /// Gets or sets the origin airport of the OND
        /// </summary>
        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        /// <summary>
        /// Gets the number of eticket hits made for this itinerary.
        /// </summary>
        public int ETicketHit
        {
            get
            {
                return eTicketHit;
            }
            set
            {
                eTicketHit = value;
            }
        }
        public List<SSR> Ssr
        {
            get
            {
                if (ssr == null)
                {
                    if (flightId > 0)
                    {
                        ssr = SSR.GetList(flightId);
                    }
                    else
                    {
                        ssr = new List<SSR>();
                    }
                }
                return ssr;
            }
        }
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or Sets PricingType string
        /// </summary>
        public string PricingType
        {
            get
            {
                return pricingType;
            }
            set
            {
                pricingType = value;
            }
        }

        /// <summary>
        /// Gets or Sets LocationId integer
        /// </summary>
        public int LocationId
        {
            get
            {
                return locationId;
            }
            set
            {
                locationId = value;
            }
        }

        public string LocationName
        {
            get
            {
                return locationName;
            }
            set
            {
                locationName = value;
            }
        }

        public string SupplierCode
        {
            get { return supplierCode; }
            set { supplierCode = value; }
        }

        /// <summary>
        /// Gets the validating airline code of the flight. (Two character airline code)
        /// </summary>
        public string ValidatingAirline
        {
            get
            {
                if (flightBookingSource == BookingSource.WorldSpan || flightBookingSource == BookingSource.Amadeus || flightBookingSource == BookingSource.Galileo || flightBookingSource == BookingSource.UAPI || flightBookingSource == BookingSource.HermesAirLine)
                {
                    if (validatingAirline == null || validatingAirline.Length == 0)
                    {
                        validatingAirline = segments[0].Airline;
                        string countryKey = segments[0].Origin.CountryCode;
                        for (int i = 0; i < segments.Length; i++)
                        {
                            if (segments[i].Destination.CountryCode != countryKey)
                            {
                                validatingAirline = segments[i].Airline;
                                break;
                            }
                        }
                    }
                }
                else if (flightBookingSource == BookingSource.SpiceJet || flightBookingSource == BookingSource.SpiceJetCorp)
                {
                    Airline air = new Airline();
                    air.Load("SG");
                    validatingAirline = air.NumericCode;
                }
                else if (flightBookingSource == BookingSource.Indigo || flightBookingSource == BookingSource.IndigoCorp)
                {
                    Airline air = new Airline();
                    air.Load("6E");
                    validatingAirline = air.NumericCode;
                }
                else if (flightBookingSource == BookingSource.Paramount)
                {
                    Airline air = new Airline();
                    air.Load("I7");
                    validatingAirline = air.NumericCode;
                }
                else if (flightBookingSource == BookingSource.AirDeccan)
                {
                    Airline air = new Airline();
                    air.Load("DN");
                    validatingAirline = air.NumericCode;
                }
                else if (flightBookingSource == BookingSource.Mdlr)
                {
                    Airline air = new Airline();
                    air.Load("9H");
                    validatingAirline = air.NumericCode;
                }
                else if (flightBookingSource == BookingSource.GoAir || flightBookingSource == BookingSource.GoAirCorp)
                {
                    Airline air = new Airline();
                    air.Load("G8");
                    validatingAirline = air.NumericCode;
                }
                else if (flightBookingSource == BookingSource.Sama)
                {
                    Airline air = new Airline();
                    air.Load("ZS");
                    validatingAirline = air.NumericCode;
                }
                else if (flightBookingSource == BookingSource.AirArabia)
                {
                    Airline air = new Airline();
                    air.Load("G9");
                    validatingAirline = air.NumericCode;
                }
                else
                {
                    validatingAirline = string.Empty;
                }
                return validatingAirline;
            }
        }

        public string ValidatingAirlineCode
        {
            get
            {
                if (validatingAirlineCode == null || validatingAirlineCode.Length == 0)
                {
                    validatingAirlineCode = segments[0].Airline;
                    string countryKey = segments[0].Origin.CountryCode;
                    for (int i = 0; i < segments.Length; i++)
                    {
                        if (segments[i].Destination.CountryCode != countryKey)
                        {
                            validatingAirlineCode = segments[i].Airline;
                            break;
                        }
                    }
                }
                return validatingAirlineCode;
            }
            set
            {
                validatingAirlineCode = value;
            }
        }

        public string TourCode
        {
            get
            {
                if (tourCode == null || tourCode.Length == 0)
                {
                    string airline = segments[0].Airline;
                    // checking for multiple airlines.
                    for (int i = 1; i < segments.Length; i++)
                    {
                        if (airline != segments[i].Airline)
                        {
                            airline = string.Empty;
                            break;
                        }
                    }
                    // airline is empty string in case of multiple airline.
                    if (airline.Length > 0)
                    {
                        string bookingClass = segments[0].BookingClass;
                        // checking for multiple booking class.
                        for (int i = 1; i < segments.Length; i++)
                        {
                            if (segments[i].BookingClass != bookingClass)
                            {
                                bookingClass = string.Empty;
                                break;
                            }
                        }
                        // bookingClass is empty string in case of multiple class.
                        if (bookingClass.Length > 0)
                        {
                            string tempOrigin = segments[0].Origin.AirportCode;
                            string tempDestination = string.Empty;
                            for (int i = 0; i < segments.Length; i++)
                            {
                                if (segments[i].Destination.CityCode == destination || segments[i].Destination.AirportCode == destination)
                                {
                                    tempDestination = segments[i].Destination.AirportCode;
                                    break;
                                }
                            }
                            tourCode = PLBRule.GetTourCode(airline, bookingClass, tempOrigin, tempDestination, segments[0].DepartureTime);
                        }
                    }
                }
                // if tour code in not applicable it returns null. cases(mutiple airline or booking class.
                return tourCode;
            }
            set { tourCode = value; }

        }

        public string Endorsement// USED FOR lcc BOOKING IN SEGMENT WISE bOOKING PROCESSW
        {
            get
            {
                if (endorsement == null || endorsement.Length == 0)
                {
                    string airline = segments[0].Airline;
                    // checking for multiple airlines.
                    for (int i = 1; i < segments.Length; i++)
                    {
                        if (airline != segments[i].Airline)
                        {
                            airline = string.Empty;
                            break;
                        }
                    }
                    // airline is empty string in case of multiple airline.
                    if (airline.Length > 0)
                    {
                        string bookingClass = segments[0].BookingClass;
                        // checking for multiple booking class.
                        for (int i = 1; i < segments.Length; i++)
                        {
                            if (segments[i].BookingClass != bookingClass)
                            {
                                bookingClass = string.Empty;
                                break;
                            }
                        }
                        // bookingClass is empty string in case of multiple class.
                        if (bookingClass.Length > 0)
                        {
                            string tempOrigin = segments[0].Origin.AirportCode;
                            string tempDestination = string.Empty;
                            for (int i = 0; i < segments.Length; i++)
                            {
                                if (segments[i].Destination.CityCode == destination || segments[i].Destination.AirportCode == destination)
                                {
                                    tempDestination = segments[i].Destination.AirportCode;
                                    break;
                                }
                            }
                            endorsement = PLBRule.GetEndorsement(airline, bookingClass, tempOrigin, tempDestination, segments[0].DepartureTime);
                        }
                    }
                }
                // if endorsement in not applicable it returns null. cases(mutiple airline or booking class.
                return endorsement;
            }
            set { endorsement = value; }
        }


        public bool Ticketed
        {
            get
            {
                return ticketed;
            }
            set
            {
                ticketed = value;
            }
        }
        /// <summary>
        /// Gets or sets the isDomestic field
        /// </summary>
        public bool IsDomestic
        {
            get
            {
                return this.isDomestic;
            }
            set
            {
                this.isDomestic = value;
            }
        }
        public bool IsOurBooking
        {
            get 
            {
                return isOurBooking;
            }
            set
            {
                isOurBooking = value;
            }
        }
        /// <summary>
        /// Gets the airline code of itinerary. "YY" for multi airline.
        /// </summary>
        public string AirlineCode
        {
            get
            {
                if (string.IsNullOrEmpty(airlineCode))
                {
                    if (segments != null && segments.Length > 0)
                    {
                        airlineCode = segments[0].Airline;
                        for (int i = 1; i < segments.Length; i++)
                        {
                            if (airlineCode != segments[i].Airline)
                            {
                                airlineCode = "YY";
                                break;
                            }
                        }
                    }
                }
                return airlineCode;
            }
            set
            {
                airlineCode = value;
            }
        }

        /// <summary>
        /// Gets or Sets travelDate field
        /// </summary>
        public DateTime TravelDate
        {
            get
            {
                return this.travelDate;
            }
            set
            {
                this.travelDate = value;
            }
        }
        /// <summary>
        /// Gets or Sets BookingMode field
        /// </summary>
        public BookingMode BookingMode
        {
            get
            {
                if ((int)this.bookingMode == 0)
                {
                    this.bookingMode = BookingMode.Auto;
                }
                return this.bookingMode;
            }
            set
            {
                this.bookingMode = value;
            }
        }

        /// <summary>
        /// Mode of Payment.
        /// </summary>
        public ModeOfPayment PaymentMode
        {
            get
            {
                return this.paymentMode;
            }
            set
            {
                this.paymentMode = value;
            }
        }
        /// <summary>
        /// Credit Card payment detail.
        /// </summary>
        public CCPayment CcPayment
        {
            get
            {
                return this.ccPayment;
            }
            set
            {
                this.ccPayment = value;
            }
        }
        /// <summary>
        /// True if the fare associated is non refundable.
        /// </summary>
        public bool NonRefundable
        {
            get
            {
                return this.nonRefundable;
            }
            set
            {
                this.nonRefundable = value;
            }
        }
        /// <summary>
        /// agency Id to which this booking belongs
        /// </summary>
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        /// <summary>
        /// booking id of this itinerary
        /// </summary>
        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }

        /// <summary>
        /// gets and sets the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        public string AliasAirlineCode
        {
            get
            {
                if (aliasAirlineCode != null && aliasAirlineCode.Length > 0)
                {
                    return aliasAirlineCode;
                }
                else
                {
                    for (int g = 0; g < segments.Length; g++)
                    {
                        if (segments[g].Airline == "IT")
                        {
                            foreach (KeyValuePair<int, int> s in Configuration.ConfigurationSystem.ITRedConfig)
                            {
                                if (s.Key <= Convert.ToInt16(segments[g].FlightNumber) && s.Value >= Convert.ToInt16(segments[g].FlightNumber))
                                {
                                    aliasAirlineCode = "ITR";
                                    break;
                                }
                            }
                        }
                        if (aliasAirlineCode == "ITR")
                        {
                            break;
                        }
                    }
                    if (aliasAirlineCode != "ITR")
                    {
                        aliasAirlineCode = ValidatingAirlineCode;
                    }
                    return aliasAirlineCode;
                }
            }
            set
            {
                aliasAirlineCode = value;
            }
        }
        /// <summary>
        /// Storing Remarks at the time of requesting cancellation
        /// </summary>
        public string SpecialRequest
        {
            get { return specialRequest; }
            set { specialRequest = value; }
        }

       

        public string GUID
        {
            get { return guid; }
            set { guid = value; }
        }

        /// <summary>
        /// If Baggage Fare is included in Tax details then set 'True' otherwise set 'False'. Added for FlyDubai.
        /// </summary>
        public bool IsBaggageIncluded
        {
            get { return isBaggageIncluded; }
            set { isBaggageIncluded = value; }
        }

        public bool IsInsured
        {
            get
            {
                return isInsured;
            }
            set
            {
                isInsured = value;
            }
        }

        /// <summary>
        /// Whether B2B or B2C transaction. Default B2B.
        /// </summary>
        public string TransactionType
        {
            get { return transactionType; }
            set { transactionType = value; }
        }
        
        /// <summary>
        /// True or False for TBOAir
        /// </summary>
        public bool IsLCC
        {
            get { return isLcc; }
            set { isLcc = value; }
        }

       

        

        public Fare[] TBOFareBreakdown
        {
            get { return tboFareBreakdown; }
            set { tboFareBreakdown = value; }
        }

        public string TripId
        {
            get { return tripId; }
            set { tripId = value; }
        }

        public FlightPolicy FlightPolicy
        {
            get { return flightPolicy; }
            set { flightPolicy = value; }
        }

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        public BookingFlowStatus BookRequestType
        {
            get
            {
                return _sBookRequestType;
            }
            set
            {
                _sBookRequestType = value;
            }
        }

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        public decimal ItineraryAmountDue
        {
            get
            {
                return _dcItineraryAmountDue;
            }
            set
            {
                _dcItineraryAmountDue = value;
            }
        }

        public SearchType ResultType//Added For Babylon Purpose
        {
            get
            {
                return resultType;
            }
            set
            {
                resultType = value;
            }
        }

        public int SalesExecutiveId
        {
            get {
                return salesExecutiveId;
            }
            set
            {
                salesExecutiveId = value;
            }
        }
        
        #endregion

        #region Some additional Properties
        /// <summary>
        /// Total agent price. Calculated on demand.
        /// </summary>
        private decimal agentPrice = 0;
        /// <summary>
        /// Total AgentPrice for the itinerary.
        /// </summary>
        public decimal AgentPrice
        {
            get
            {
                if (agentPrice == 0)
                {
                    for (int i = 0; i < passenger.Length; i++)
                    {
                        agentPrice += passenger[i].Price.GetAgentPrice();
                    }
                }
                return agentPrice;
            }
        }


        public bool IsGSTMandatory
        {
            get
            {
                return isGSTMandatory;
            }

            set
            {
                isGSTMandatory = value;
            }
        }

        public string GstCompanyAddress
        {
            get
            {
                return gstCompanyAddress;
            }

            set
            {
                gstCompanyAddress = value;
            }
        }

        public string GstCompanyContactNumber
        {
            get
            {
                return gstCompanyContactNumber;
            }

            set
            {
                gstCompanyContactNumber = value;
            }
        }

        public string GstCompanyName
        {
            get
            {
                return gstCompanyName;
            }

            set
            {
                gstCompanyName = value;
            }
        }

        public string GstNumber
        {
            get
            {
                return gstNumber;
            }

            set
            {
                gstNumber = value;
            }
        }

        public string GstCompanyEmail
        {
            get
            {
                return gstCompanyEmail;
            }

            set
            {
                gstCompanyEmail = value;
            }
        }

        /// <summary>
        /// Routing Identification Id for Onward and Return Itineraries
        /// </summary>
        public string RoutingTripId { get => routingTripId; set => routingTripId = value; }

        /// <summary>
        /// //For Combination Search If both onward and return results are SpecialRoundTripFares;
        /// </summary>
        public bool IsSpecialRoundTrip
        {
            get
            {
                return _isSpecialRoundTrip;
            }

            set
            {
                _isSpecialRoundTrip = value;
            }
        }
        public bool Sendmail
        {
            get
            {
                return _sendmail;
            }

            set
            {
                _sendmail = value;
            }
        }
        public bool HotelReq { get => _hotelReq; set => _hotelReq = value; }

        /// <summary>
        /// To save dynamic pcc HAP
        /// </summary>
        public string DynamicHAP { get => _sDynamicHAP; set => _sDynamicHAP = value; }
        /// <summary>
        /// To store Booking Remarks
        /// </summary>
        public string BookUserIP { get => _BookUserIP; set => _BookUserIP = value; }

        public int IsEdit
        {
            get => isEdit;
            set => isEdit = value;
        }

        public string SaleType
        {
            get => saleType;
            set => saleType = value;
        }
        public bool IsOnBehalfAgent { get => isOnBehalfAgent; set => isOnBehalfAgent = value; }
        #endregion

        #region Class Constructors
        public FlightItinerary()
        {            
            paymentMode = ModeOfPayment.Credit;
        }
        public FlightItinerary(int flightId)
        {            
            Load(flightId);
        }
        #endregion

        #region Methods
        public void save()
        {
            //TODO: validate that the fields are properly set before calling the SP
            //Trace.TraceInformation("Itinerary.Save entered : PNR = " + pnr);
            bool bUpdate = flightId > 0;
            SqlParameter[] paramList = new SqlParameter[34];
            paramList[0] = new SqlParameter("@pnr", pnr);
            paramList[1] = new SqlParameter("@origin", origin);
            paramList[2] = new SqlParameter("@destination", destination);
            paramList[3] = new SqlParameter("@fareType", fareType);
            paramList[4] = new SqlParameter("@source", (int)flightBookingSource);
            //TODO: how to know if the last ticket date is not given. by default it goes 0001/01/01 12:00 AM
            if (lastTicketDate.Year >= DateTime.Now.Year)
            {
                paramList[5] = new SqlParameter("@lastTicketDate", lastTicketDate);
            }
            else
            {
                paramList[5] = new SqlParameter("@lastTicketDate", DBNull.Value);
            }
            if (ticketAdvisory != null)
            {
                paramList[6] = new SqlParameter("@ticketAdvisory", ticketAdvisory);
            }
            else
            {
                paramList[6] = new SqlParameter("@ticketAdvisory", string.Empty);
            }
            paramList[7] = new SqlParameter("@isDomestic", isDomestic);
            paramList[8] = new SqlParameter("@airlineCode", AirlineCode);
            paramList[9] = new SqlParameter("@travelDate", travelDate);
            paramList[10] = new SqlParameter("@paymentMode", (short)paymentMode);
            paramList[11] = new SqlParameter("@validatingAirlineCode", ValidatingAirlineCode);
            paramList[12] = new SqlParameter("@nonRefundable", nonRefundable);
            paramList[13] = new SqlParameter("@createdBy", createdBy);
            paramList[14] = new SqlParameter("@flightId", flightId);
            paramList[14].Direction = bUpdate ? paramList[14].Direction : ParameterDirection.Output;
            paramList[15] = new SqlParameter("@bookingId", bookingId);
            paramList[16] = new SqlParameter("@agencyId", agencyId);
            paramList[17] = new SqlParameter("@aliasAirlineCode", AliasAirlineCode);
            paramList[18] = new SqlParameter("@isOurBooking", isOurBooking);
            paramList[19] = new SqlParameter("@universalRecord", universalRecord);
            paramList[20] = new SqlParameter("@airLocatorCode", airLocatorCode);
            paramList[21] = new SqlParameter("@supplierLocatorCode", supplierLocatorCode);
            paramList[22] = new SqlParameter("@locationId", locationId);
            paramList[23] = new SqlParameter("@isInsured", isInsured);
            paramList[24] = new SqlParameter("@transType", transactionType);
            paramList[25] = new SqlParameter("@isLcc", isLcc);
            if (!string.IsNullOrEmpty(tripId))
            {
                paramList[26] = new SqlParameter("@tripId", tripId);
            }
            else
            {
                paramList[26] = new SqlParameter("@tripId", DBNull.Value);
            }
            if (!string.IsNullOrEmpty(supplierCode))
            {
                paramList[27] = new SqlParameter("@supp_code", supplierCode);
            }
            else
            {
                paramList[27] = new SqlParameter("@supp_code", DBNull.Value);
            }
            
            if(!string.IsNullOrEmpty(routingTripId))
            {
                paramList[28] = new SqlParameter("@routingTripId", routingTripId);
            }
            else
            {
                paramList[28] = new SqlParameter("@routingTripId", DBNull.Value);
            }
            paramList[29] = new SqlParameter("@IsHotReq", HotelReq);
            paramList[30] = new SqlParameter("@SalesExecutiveId", salesExecutiveId);
            paramList[31] = new SqlParameter("@isEdit", isEdit);
            paramList[32] = new SqlParameter("@saleType", saleType);
            paramList[33] = new SqlParameter("@isOnBehalfAgent", isOnBehalfAgent);
            //if(!string.IsNullOrEmpty(universalRecord)) paramList[19] = new SqlParameter("@universalRecord", universalRecord);
            //if (!string.IsNullOrEmpty(providerPNR)) paramList[20] = new SqlParameter("@providerPNR", providerPNR);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddFlightItinerary, paramList);
            flightId = (int)paramList[14].Value;
            if (paymentMode == ModeOfPayment.CreditCard)
            {
                //ccPayment.FlightId = flightId;
                //ccPayment.Save();
            }
            for (int i = 0; i < segments.Length; i++)
            {
                segments[i].FlightId = flightId;
                segments[i].Save();
            }
            for (int i = 0; i < passenger.Length; i++)
            {
                passenger[i].FlightId = flightId;
                passenger[i].Price.Save();
                passenger[i].Save();
            }
            if (!bUpdate)
            {
                for (int i = 0; i < fareRules.Count; i++)
                {
                    fareRules[i].FlightId = flightId;
                    fareRules[i].Save();
                }
                //Save Corporate profile flight policy
                if (flightPolicy != null)
                {
                    try
                    {                        
                        flightPolicy.Flightid = flightId;
                        flightPolicy.ProductId = 1;
                        flightPolicy.IsDomestic = this.isDomestic;
                        flightPolicy.TotalBookingAmount = GetBookingAmount();
                        if (flightPolicy.ProfileId <= 0 && !string.IsNullOrEmpty(passenger[0].CorpProfileId))
                            flightPolicy.ProfileId = Convert.ToInt32(passenger[0].CorpProfileId);
                        flightPolicy.Save();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Book, Severity.High, 1, "(Corp)Failed to save flight policy for flight passenger email: " + passenger[0].Email + ". Reason: " + ex.ToString(), "");
                    }
                }
            }
            //Trace.TraceInformation("Itinerary.Save exiting : flightId = " + flightId);
        }

        // New method for - Chnages for New product type
        public override void Save(Product prod)
        {
            FlightItinerary itinerary = (FlightItinerary)prod;
            //TODO: validate that the fields are properly set before calling the SP
            //Trace.TraceInformation("Itinerary.Save entered : PNR = " + itinerary.PNR);
            SqlParameter[] paramList = new SqlParameter[28];
            paramList[0] = new SqlParameter("@pnr", itinerary.PNR);
            paramList[1] = new SqlParameter("@origin", itinerary.Origin);
            paramList[2] = new SqlParameter("@destination", itinerary.Destination);
            paramList[3] = new SqlParameter("@fareType", itinerary.FareType);
            paramList[4] = new SqlParameter("@source", (int)itinerary.FlightBookingSource);
            //TODO: how to know if the last ticket date is not given. by default it goes 0001/01/01 12:00 AM
            if (itinerary.LastTicketDate.Year >= DateTime.Now.Year)
            {
                paramList[5] = new SqlParameter("@lastTicketDate", itinerary.LastTicketDate);
            }
            else
            {
                paramList[5] = new SqlParameter("@lastTicketDate", DBNull.Value);
            }
            if (ticketAdvisory != null)
            {
                paramList[6] = new SqlParameter("@ticketAdvisory", itinerary.TicketAdvisory);
            }
            else
            {
                paramList[6] = new SqlParameter("@ticketAdvisory", string.Empty);
            }
            paramList[7] = new SqlParameter("@isDomestic", itinerary.IsDomestic);
            paramList[8] = new SqlParameter("@airlineCode", itinerary.AirlineCode);
            paramList[9] = new SqlParameter("@travelDate", itinerary.TravelDate);
            paramList[10] = new SqlParameter("@paymentMode", (short)itinerary.PaymentMode);
            paramList[11] = new SqlParameter("@validatingAirlineCode", itinerary.ValidatingAirlineCode);
            paramList[12] = new SqlParameter("@nonRefundable", nonRefundable);
            paramList[13] = new SqlParameter("@createdBy", itinerary.CreatedBy);
            paramList[14] = new SqlParameter("@flightId", SqlDbType.Int);
            paramList[14].Direction = ParameterDirection.Output;
            paramList[15] = new SqlParameter("@bookingId", bookingId);
            paramList[16] = new SqlParameter("@agencyId", agencyId);
            paramList[17] = new SqlParameter("@aliasAirlineCode", itinerary.AliasAirlineCode);
            paramList[18] = new SqlParameter("@isOurBooking", isOurBooking);
            if (!string.IsNullOrEmpty(universalRecord)) paramList[19] = new SqlParameter("@universalRecord", universalRecord);
            if (!string.IsNullOrEmpty(airLocatorCode)) paramList[20] = new SqlParameter("@airLocatorCode", airLocatorCode);
            if (!string.IsNullOrEmpty(supplierLocatorCode)) paramList[21] = new SqlParameter("@supplierLocatorCode", universalRecord);
            paramList[22] = new SqlParameter("@locationId", locationId);
            paramList[23] = new SqlParameter("@transType", transactionType);
            paramList[24] = new SqlParameter("@isInsured", isInsured);
            paramList[25] = new SqlParameter("@isLcc", isLcc);
            if (!string.IsNullOrEmpty(tripId))
            {
                paramList[26] = new SqlParameter("@tripId", tripId);
            }
            else
            {
                paramList[26] = new SqlParameter("@tripId", DBNull.Value);
            }
            paramList[27] = new SqlParameter("@IsHotReq", HotelReq);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddFlightItinerary, paramList);
            flightId = (int)paramList[14].Value;
            ccPayment.FlightId = flightId;
            ccPayment.Save();
            for (int i = 0; i < segments.Length; i++)
            {
                segments[i].FlightId = flightId;
                segments[i].Save();
            }
            for (int i = 0; i < passenger.Length; i++)
            {
                passenger[i].FlightId = flightId;
                passenger[i].Price.Save();
                passenger[i].Save();
            }
            for (int i = 0; i < fareRules.Count; i++)
            {
                fareRules[i].FlightId = flightId;
                fareRules[i].Save();
            }
            //Save Corporate profile flight policy
            if (flightPolicy != null)
            {
                flightPolicy.Save();
            }
            //Trace.TraceInformation("Itinerary.Save exiting : flightId = " + flightId);
        }

        /// <summary>
        ///Used to change wait list as well PNR status
        /// </summary>
        /// <param name="itWs"></param>
        /// <returns></returns>
        public bool UpdateStatus(FlightItinerary itWs)
        {
            //Trace.TraceInformation("Itinerary.UpdateStatus entered : PNR = " + pnr);
            bool updated = false;
            for (int i = 0; i < segments.Length; i++)
            {
                if (segments[i].FlightKey == itWs.segments[i].FlightKey)
                {
                    segments[i].Status = itWs.segments[i].Status;
                    segments[i].LastModifiedOn = DateTime.UtcNow;
                    segments[i].LastModifiedBy = itWs.segments[i].LastModifiedBy;
                    segments[i].Save();
                    updated = true;
                }
                else
                {
                    for (int j = 0; j < itWs.segments.Length; j++)
                    {
                        if (segments[i].FlightKey == itWs.segments[j].FlightKey)
                        {
                            segments[i].Status = itWs.segments[j].Status;
                            segments[i].LastModifiedOn = DateTime.UtcNow;
                            segments[i].LastModifiedBy = itWs.segments[i].LastModifiedBy;
                            segments[i].Save();
                            updated = true;
                            break;
                        }
                    }
                }
            }
            //Trace.TraceInformation("Itinerary.UpdateStatus entered : PNR = " + pnr);
            return updated;
        }

        /// <summary>
        /// Loads all the data for a particular flightId
        /// </summary>
        /// <param name="flightId">flightId</param>
        private void Load(int flightId)
        {
            //Trace.TraceInformation("FlightItinerary.Load entered : flightId = " + flightId);
            if (flightId <= 0)
            {
                throw new ArgumentException("Flight Id should be positive integer", "FlightId");
            }
            this.flightId = flightId;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetFlightDetail, paramList, connection);
            DataTable dtItinerary = DBGateway.FillDataTableSP(SPNames.GetFlightDetail, paramList);
            //if (data.Read())
            if(dtItinerary != null && dtItinerary.Rows.Count > 0)
            {
                DataRow data = dtItinerary.Rows[0];
                destination = data["destination"].ToString();
                origin = data["origin"].ToString();
                fareType = data["fareType"].ToString();
                if (data["lastTicketingDate"] != DBNull.Value)
                {
                    lastTicketDate = Convert.ToDateTime(data["lastTicketingDate"]);
                }
                else
                {
                    lastTicketDate = DateTime.MinValue;
                }
                if (data["ticketAdvisory"] != DBNull.Value)
                {
                    ticketAdvisory = Convert.ToString(data["ticketAdvisory"]);
                }
                else
                {
                    ticketAdvisory = string.Empty;
                }
                pnr = data["pnr"].ToString();
                pnr = pnr.Trim();
                eTicketHit = Convert.ToInt32(data["eTicketHit"]);
                segments = FlightInfo.GetSegments(flightId);
                passenger = FlightPassenger.GetPassengers(flightId);
                fareRules = FareRule.GetFareRuleList(flightId);
                isDomestic = Convert.ToBoolean(data["isDomestic"]);
                airlineCode = data["airlineCode"].ToString();
                if (data["travelDate"] != DBNull.Value)
                {
                    travelDate = Convert.ToDateTime(data["travelDate"]);
                }
                else
                {
                    travelDate = segments[0].DepartureTime;
                }
                bookingMode = GetBookingMode(flightId, ProductType.Flight);
                if (data["paymentMode"] != DBNull.Value)
                {
                    paymentMode = (ModeOfPayment)(Convert.ToInt16(data["paymentMode"]));
                }// else paymentMode will have the default value Null = 0.
                if (paymentMode == ModeOfPayment.CreditCard)
                {
                    //ccPayment = CCPayment.Load(flightId);
                }
                if (data["validatingAirlineCode"] != DBNull.Value)
                {
                    validatingAirlineCode = Convert.ToString(data["validatingAirlineCode"]);
                }
                nonRefundable = Convert.ToBoolean(data["nonRefundable"]);
                createdBy = Convert.ToInt32(data["createdBy"]);
                createdOn = Convert.ToDateTime(data["createdOn"]);
                lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                flightBookingSource = (BookingSource)(Convert.ToInt32(data["source"]));
                bookingId = Convert.ToInt32(data["bookingId"]);
                agencyId = Convert.ToInt32(data["agencyId"]);
                aliasAirlineCode = Convert.ToString(data["aliasAirlineCode"]);
                isOurBooking = Convert.ToBoolean(data["isOurBooking"]);

                if (data["universalRecord"] != DBNull.Value)
                {
                    universalRecord = Convert.ToString(data["universalRecord"]);
                }
                if (data["airLocatorCode"] != DBNull.Value)
                {
                    airLocatorCode = Convert.ToString(data["airLocatorCode"]);
                }
                if (data["supplierLocatorCode"] != DBNull.Value)
                {
                    supplierLocatorCode = Convert.ToString(data["supplierLocatorCode"]);
                }
                locationId = Convert.ToInt32(data["locationId"]);
                locationName = Convert.ToString(data["locationName"]);
                isInsured = Convert.ToBoolean(data["IsInsured"]);
                transactionType = data["transType"].ToString();
                if (data["IsLcc"] != DBNull.Value)
                {
                    isLcc = Convert.ToBoolean(data["IsLcc"]);
                }
                if (data["tripId"] != DBNull.Value)
                {
                    tripId = data["tripId"].ToString();
                }
                if(data["routingTripId"] != DBNull.Value)
                {
                    routingTripId = data["routingTripId"].ToString();
                }
                if(data["saletype"]!=DBNull.Value)
                {
                    saleType = data["saletype"].ToString();
                }
                else
                {
                    saleType = "N";
                }
                //data.Close();
                //connection.Close();
                //Trace.TraceInformation("FlightItinerary.Load exiting :" + flightId.ToString());
            }
            else
            {
                //data.Close();
                //connection.Close();
                //Trace.TraceInformation("FlightItinerary.Load exiting : flightId does not exist.flightId = " + flightId.ToString());
                throw new ArgumentException("Flight id does not exist in database");
            }
        }
        public static int GetFlightId(string pnr)
        {
            int flightId = 0;
            ////Trace.TraceInformation("FlightItinerary.GetFlightId entered : pnr = " + pnr);
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@pnr", pnr);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetFlightId, paramList, connection);
            using (DataTable dtFlightIds = DBGateway.FillDataTableSP(SPNames.GetFlightId, paramList))
            {
                if (dtFlightIds != null && dtFlightIds.Rows.Count > 0)
                {
                    DataRow data = dtFlightIds.Rows[0];
                    if (data["flightId"] != DBNull.Value)
                    {
                        flightId = Convert.ToInt32(data["flightId"]);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            ////Trace.TraceInformation("FlightItinerary.GetFlightId exiting :" + flightId.ToString());
            return flightId;
        }
        public static void CreateMail(SearchRequest request, SearchResult[] result, Boolean isPublishPrice, string email, int agencyId, int memberId)
        {
            //if (result != null)
            //{
            //    int len = request.Segments.Length;

            //    Airport ori = new Airport(request.Segments[0].Origin);
            //    Airport dest = new Airport(request.Segments[0].Destination);
            //    string fromCity = string.Empty;
            //    string toCity = string.Empty;
            //    string wayType = string.Empty;
            //    string pax = string.Empty;
            //    string cabinClass = string.Empty;
            //    string paxString = "Passenger";
            //    UserMaster member = new UserMaster(memberId);
            //    AgentMaster agency = new AgentMaster(agencyId);
            //    Dictionary<string, string> prefPair = new Dictionary<string, string>();
            //    //UserPreference preference = new UserPreference();
            //    string currencyPref = string.Empty;
            //    Dictionary<string, decimal> rateList = new Dictionary<string, decimal>();
            //    decimal currencyFactor = 1;
            //    string origin = request.Segments[0].Origin;
            //    string destination = request.Segments[0].Destination;
            //    pax = Convert.ToString(request.AdultCount + request.ChildCount + request.SeniorCount + request.InfantCount);
            //    if (Convert.ToInt32(pax) > 1)
            //    {
            //        paxString = "Passengers";
            //    }
            //    cabinClass = Enum.Parse(typeof(CabinClass), request.Segments[0].flightCabinClass.ToString()).ToString();

            //    fromCity = request.Segments[0].Origin;
            //    toCity = request.Segments[0].Destination;
            //    if (request.Type.ToString() == "Return")
            //    {
            //        wayType = "- Return";
            //    }

            //    StringBuilder sb = new StringBuilder();
            //    sb.Append("<div style=\"width:600px;float:left;\">");
            //    sb.Append("<div style=\"width:585px;float:left;padding:5px 5px 10px 5px;border-bottom:solid 1px #7E7E7E;\">");
            //    if (member.LogoFile.Trim().Length != 0)
            //    {
            //        sb.Append("<span style=\"width:30px;height:30px;border:solid 1px black;float:left;\"><img alt=\"LOGO\" src=\"" + "http://www.travelboutiqueonline.com/UserImages/" + member.LogoFile + "\" /></span>");
            //    }
            //    //sb.Append("<span style=\"float:left;font-size:16px;font-weight:bold;font-family:verdana;\">" + agency.AgencyName + "</span>");
            //    sb.Append("<div style=\"width:100%;float:left;margin-top:5px;\"><span style=\"font-weight:bold\">" + fromCity + "</span> (" + origin + ") to <span style=\"font-weight:bold\">" + toCity + "</span> (" + destination + ") " + wayType + "</div>");
            //    sb.Append("<div style=\"width:100%;float:left;color:#9F9F9F;margin-top:3px;\">" + cabinClass + " Class | " + pax + " " + paxString + "</div>");
            //    sb.Append("</div>");
            //    //Airline service fee for Email Itinerary
            //    Dictionary<string, string> sFeeTypeList = new Dictionary<string, string>();
            //    Dictionary<string, string> sFeeValueList = new Dictionary<string, string>();
            //    if (Convert.ToBoolean(isPublishPrice))
            //    {
            //        GetMemberPrefServiceFee(agencyId, ref sFeeTypeList, ref sFeeValueList);
            //    }
            //    int i = 0;
            //    foreach (SearchResult sr in result)
            //    {
            //        sb.Append("<div style=\"width:100%;float:left;margin-top:5px;padding-bottom:10px;border-bottom:solid 1px black;\">");
            //        DictionaryEntry logo = GetLogo(result[i].Flights, ConfigurationSystem.Core["applicationPath"]);
            //        sb.Append("<div style=\"float:left;padding:5px\">");
            //        sb.Append("<span style=\"width:30px;height:30px;border:solid 1px black;float:left;\"><img alt=\"LOGO\" src=\"" + "http://www.travelboutiqueonline.com/" + logo.Value + "\" /></span>");
            //        sb.Append("<span style=\"float:left;font-size:14px;margin-left:10px;\">" + logo.Key + "</span>");

            //        if (Convert.ToBoolean(isPublishPrice))
            //        {
            //            int totalPax = 0;
            //            string feeType = string.Empty;
            //            double agentServiceFee = 0, feeValue = 0;
            //            decimal totalTxnFee = 0;
            //            for (int t = 0; t < result[i].FareBreakdown.Length; t++)
            //            {
            //                totalTxnFee += result[i].FareBreakdown[t].AirlineTransFee;
            //                totalPax += result[i].FareBreakdown[t].PassengerCount;
            //            }
            //            double totalPrice = agentServiceFee + result[i].TotalFare + Convert.ToDouble(result[i].Price.OtherCharges + result[i].Price.AdditionalTxnFee + result[i].Price.TransactionFee + totalTxnFee);
            //            if (!Util.IsDomestic(sr.Flights[0],CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"]))
            //            {
            //                if (sFeeTypeList.ContainsKey("INTL") && sFeeValueList.ContainsKey("INTL"))
            //                {
            //                    feeType = sFeeTypeList["INTL"];
            //                    feeValue = Convert.ToDouble(sFeeValueList["INTL"]);
            //                }
            //            }
            //            else if (sFeeTypeList.ContainsKey(sr.ValidatingAirline) && sFeeValueList.ContainsKey(sr.ValidatingAirline))
            //            {
            //                feeType = sFeeTypeList[sr.ValidatingAirline];
            //                feeValue = Convert.ToDouble(sFeeValueList[sr.ValidatingAirline]);
            //            }
            //            if (feeType == "PERCENTAGE")//Percentage
            //            {
            //                if (request.Type == SearchType.OneWay)
            //                {
            //                    agentServiceFee = totalPrice * feeValue / 100;
            //                }
            //                else if (request.Type == SearchType.Return)
            //                {
            //                    agentServiceFee = totalPrice * feeValue / 50;
            //                }
            //            }
            //            else if (feeType == "FIXED")//Fixed need to multiply by pax count
            //            {
            //                if (request.Type == SearchType.OneWay)
            //                {
            //                    agentServiceFee = feeValue * totalPax;
            //                }
            //                else if (request.Type == SearchType.Return)
            //                {
            //                    agentServiceFee = feeValue * totalPax * 2;
            //                }
            //            }
            //            decimal serviceTax = 0;
            //            if (Convert.ToBoolean(HttpContext.Current.Session["isB2B2BAgent"]))
            //            {
            //                serviceTax = result[i].Price.SeviceTax;
            //            }
            //            string currencyString = result[i].Currency;
            //            sb.Append("<div style=\"float:left\">");
            //            sb.Append("<div style=\"padding-right:5px;float:right;margin-right:39px;\">");
            //            if (result[i].Currency == "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
            //            {
            //            currencyString = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
            //            }

            //            //Load the preference for the current user
            //            //Dictionary<string, string> prefPair = new Dictionary<string, string>();
            //            try
            //            {
            //                //prefPair = UserPreference.GetPreferenceList(Member.GetPrimaryMemberId(agency.AgencyId), ItemType.Flight);
            //            }
            //            catch (ArgumentException)
            //            {
            //                //TODO: Error View - Unknown user.
            //            }

            //            //Currency Preference
            //            if (prefPair.ContainsKey(preference.CurrencyPref))
            //            {
            //                currencyPref = prefPair[preference.CurrencyPref];
            //                if (currencyPref.Equals("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "", StringComparison.OrdinalIgnoreCase) != true)
            //                {
            //                    StaticData staticInfo = new StaticData();
            //                    rateList = staticInfo.CurrencyROE;
            //                    currencyFactor = rateList[currencyPref];
            //                }

            //            }
            //            double inrFare = totalPrice + agentServiceFee + (double)serviceTax;
            //            double currencyPrefFair = inrFare / Convert.ToDouble(currencyFactor);

            //            if (currencyPref.Equals("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "") != true)
            //            {
            //                sb.Append("<div style=\"font-weight:bold\"> Fare " + currencyString + " " + (inrFare).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + " </div>");
            //                sb.Append("<div style=\"font-weight:bold;margin-left:15px\"> " + currencyPref + " " + (currencyPrefFair).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + " </div>");
            //            }
            //            else
            //            {
            //                sb.Append("<div style=\"font-weight:bold\"> Fare " + currencyString + " " + (inrFare).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + " </div>");
            //            }
                        
            //            sb.Append("</div>");
            //            sb.Append("</div>");
            //        }
            //        sb.Append("</div>");
            //        for (int count = 0; count < result[i].Flights.Length; count++)
            //        {
            //            TimeSpan GoingFlightTime = new TimeSpan(0, 0, 0);
            //            TimeSpan ReturnFlightTime = new TimeSpan(0, 0, 0);
            //            string stringForInOut = "Outbound Flight";
            //            string bkColor = "#CCEDFF";
            //            if (count == 0)
            //            {
            //                GoingFlightTime = new TimeSpan(0, 0, 0);
            //            }
            //            if (count == 1)
            //            {
            //                ReturnFlightTime = new TimeSpan(0, 0, 0);
            //                stringForInOut = "Return Flight";
            //                bkColor = "#99CDFF";
            //            }
            //            sb.Append("<div style=\"width:100%;background-color:" + bkColor + ";float:left;\">");
            //            sb.Append("<div style=\"margin-top:5px;padding:5px 10px 5px 5px;background-color:#B3E2FF;   float:left;font-size:17px;width:585px;\">");
            //            sb.Append("<div style=\"float:left;font-weight:bold\">" + stringForInOut + "</div>");
            //            sb.Append("</div>");
            //            sb.Append("<div style=\"width:590px;float:left;padding:5px;\">");
            //            sb.Append("<div style=\"float:left\">" + result[i].Flights[count][0].DepartureTime.DayOfWeek + " " + result[i].Flights[count][0].DepartureTime.ToString("dd MMM yyyy") + "</div>");
            //            for (int count1 = 0; count1 < result[i].Flights[count].Length; count1++)
            //            {
            //                if (count == 0)
            //                {
            //                    if (result[i].ResultBookingSource == BookingSource.Amadeus)
            //                    {
            //                        GoingFlightTime = GoingFlightTime + result[i].Flights[count][count1].AccumulatedDuration;
            //                    }
            //                    else
            //                    {
            //                        GoingFlightTime = GoingFlightTime + result[i].Flights[count][count1].Duration + result[i].Flights[count][count1].GroundTime;
            //                    }
            //                }
            //                if (count == 1)
            //                {
            //                    if (result[i].ResultBookingSource == BookingSource.Amadeus)
            //                    {
            //                        ReturnFlightTime = ReturnFlightTime + result[i].Flights[count][count1].AccumulatedDuration;
            //                    }
            //                    else
            //                    {
            //                        ReturnFlightTime = ReturnFlightTime + result[i].Flights[count][count1].Duration + result[i].Flights[count][count1].GroundTime;
            //                    }
            //                }
            //                string airlineName = string.Empty;
            //                if (logo.Key.ToString() == "Multiple Airlines")
            //                {
            //                    Airline airline = new Airline();
            //                    airline.Load(result[i].Flights[count][count1].Airline);
            //                    airlineName = airline.AirlineName;
            //                }
            //                sb.Append("<div style=\"width:95%;float:left;margin-top:15px;\">");
            //                sb.Append("<div style=\"float:left;\">");
            //                sb.Append("<div style=\"float:left;width:95%;\">" + airlineName + " Flight " + result[i].Flights[count][count1].Airline + " " + result[i].Flights[count][count1].FlightNumber + "  </div>");
            //                sb.Append("<div style=\"width:80px;float:left;\">Depart:</div>");
            //                sb.Append("<div style=\"width:316px;float:left;\">" + result[i].Flights[count][count1].Origin.CityName + " (" + result[i].Flights[count][count1].Origin.AirportName + ")</div>");
            //                sb.Append("<div style=\"float:left;font-weight:bold\"> " + result[i].Flights[count][count1].DepartureTime.ToShortTimeString() + "</div>");
            //                sb.Append("</div>");
            //                sb.Append("<div style=\"float:left;\">");
            //                sb.Append("<div style=\"width:80px;float:left;\">Arrive:</div>");
            //                sb.Append("<div style=\"width:316px;float:left;\">" + result[i].Flights[count][count1].Destination.CityName + " (" + result[i].Flights[count][count1].Destination.AirportName + ")</div>");
            //                sb.Append("<div style=\"float:left;font-weight:bold\">" + result[i].Flights[count][count1].ArrivalTime.ToShortTimeString() + "</div>");
            //                sb.Append("</div>");
            //                sb.Append("</div>");
            //            }
            //            if (stringForInOut == "Outbound Flight")
            //            {
            //                sb.Append("<div style=\"width:193px;float:left;margin-left:198px;margin-top:5px;\">Total Flight Time: " + GoingFlightTime.Hours + " hr " + GoingFlightTime.Minutes + " min</div>");
            //            }
            //            else
            //            {
            //                sb.Append("<div style=\"width:193px;float:left;margin-left:198px;margin-top:5px;\">Total Flight Time: " + ReturnFlightTime.Hours + " hr" + ReturnFlightTime.Minutes + "min</div>");
            //            }
            //            sb.Append("</div>");
            //            sb.Append("</div>");
            //        }
            //        sb.Append("</div>");
            //        i++;
            //    }
            //    sb.Append("</div>");
            //    List<string> toArray = new List<string>();
            //    string addressList = email;
            //    string[] addressArray = addressList.Split(',');
            //    for (int k = 0; k < addressArray.Length; k++)
            //    {
            //        toArray.Add(addressArray[k]);
            //    }
            //    string from = Agency.GetAgencyEmail(agency.AgencyId);
            //    string subjectLine = "Your itineraries for " + fromCity + "-" + toCity;
            //    try
            //    {
            //        Email.Send(from, from, toArray, subjectLine, sb.ToString(), new Hashtable());
            //    }
            //    catch (System.Net.Mail.SmtpException)
            //    {
            //        CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
            //    }

            //}
            //else
            //{
            //    throw new ArgumentException("There is no itinerary which is selected so please select the itinerary");
            //    // Response.Write("There is no itinerary which is selected so please select the itinerary");
            //}

        }


        public static DictionaryEntry GetLogo(FlightInfo[][] flight, string applPath)
        {
            DictionaryEntry logo = new DictionaryEntry();
            string tempAirline = flight[0][0].Airline;
            bool multiAirline = false;
            // checking for multiple airlines
            for (int g = 0; g < flight.Length; g++)
            {
                for (int f = 0; f < flight[g].Length; f++)
                {
                    if (tempAirline != flight[g][f].Airline)
                    {
                        multiAirline = true;
                        break;
                    }
                }
                if (multiAirline)
                {
                    break;
                }
            }
            //string logoFile = string.Empty;
            //string airlineName = string.Empty;
            // logoFile and airlineName for multiple carrier.
            if (multiAirline)
            {
                logo.Value = Airline.logoDirectory + "/" + Airline.multipleAirlineLogo;
                logo.Key = "Multiple Airlines";
            }
            else
            {
                // logofile and airline name for single carrier.
                try
                {
                    Airline departingAirline = new Airline();
                    departingAirline.Load(flight[0][0].Airline);
                    string logoPath = applPath + "\\" + Airline.logoDirectoryPath + "\\" + departingAirline.LogoFile;
                    logo.Key = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")";
                    // if logo file does not exist then assign default logo.
                    if (departingAirline.LogoFile.Length == 0 || !File.Exists(logoPath))
                    {
                        logo.Value = Airline.logoDirectory + "/" + Airline.defaultAirlineLogo;
                    }
                    else
                    {
                        logo.Value = Airline.logoDirectory + "/" + departingAirline.LogoFile;
                    }
                }
                catch (ArgumentException)
                {
                    logo.Value = logo.Value = Airline.logoDirectory + "/" + Airline.defaultAirlineLogo;
                    logo.Key = flight[0][0].Airline;
                }
            }
            return logo;
        }
        /// <summary>
        /// Gets the voidation charges based on the basis of whether the airline is domestic or international.
        /// </summary>
        /// <param name="isDomestic">isDomestic</param>
        public static decimal[] GetVoidationCharge(bool isDomestic)
        {
            //Trace.TraceInformation("FlightItinerary.GetVoidationCharge entered : isDomestic= " + isDomestic);
            decimal[] chargeInfo = new decimal[2];
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@isDomestic", isDomestic);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetVoidationCharge, paramList, connection);
            if (data.Read())
            {
                chargeInfo[0] = Convert.ToDecimal(data["ourCharge"]);
                chargeInfo[1] = Convert.ToDecimal(data["airlineCharge"]);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("FlightItinerary.GetVoidationCharge exiting :");
            return chargeInfo;

        }
        /// <summary>
        /// Checks if the itinerary corresponding to given flightId is ticketed or not.
        /// </summary>
        /// <param name="flightId">flightId of the itinerary</param>
        /// <returns>true if ticketing for all the passengers is created.</returns>
        public static bool IsTicketed(int flightId)
        {
            //Trace.TraceInformation("Itinerary.IsTicketed entered. flightId = " + flightId);
            FlightItinerary itinerary = new FlightItinerary(flightId);
            //Trace.TraceInformation("Itinerary.IsTicketed exiting");
            return IsTicketed(itinerary);
        }

        /// <summary>
        /// Checks if the given itinerary is ticketed or not.
        /// </summary>
        /// <param name="itinerary">flight itinerary for which ticketed status is to be checked.</param>
        /// <returns>true if ticketing for all the passengers is created.</returns>
        public static bool IsTicketed(FlightItinerary itinerary)
        {
            //Trace.TraceInformation("Itinerary.IsTicketed entered. flightId = " + itinerary.flightId);
            List<Ticket> ticket = Ticket.GetTicketList(itinerary.flightId);
            //Trace.TraceInformation("Itinerary.IsTicketed exiting");
            return IsTicketed(itinerary, ticket);
        }

        /// <summary>
        /// Checks if ticket for each passenger in itinerary exists in the given ticket list.
        /// </summary>
        /// <param name="itinerary">flight itinerary for which ticketed status is to be checked.</param>
        /// <param name="ticket">ticket list in which to search for tickets.</param>
        /// <returns>true if ticketing for all the passengers is created.</returns>
        public static bool IsTicketed(FlightItinerary itinerary, List<Ticket> ticket)
        {
            //Trace.TraceInformation("Itinerary.IsTicketed entered. flightId = " + itinerary.flightId);
            bool ticketCompleted = true;
            if (ticket.Count > 0 && ticket.Count == itinerary.Passenger.Length)
            {
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    bool paxTicketed = false;
                    if (ticket[i].PaxId == itinerary.Passenger[i].PaxId)
                    {
                        paxTicketed = true;
                        break;
                    }
                    else
                    {
                        for (int j = 0; j < ticket.Count; j++)
                        {
                            if (ticket[j].PaxId == itinerary.Passenger[i].PaxId)
                            {
                                paxTicketed = true;
                                break;
                            }
                        }
                    }
                    if (!paxTicketed)
                    {
                        ticketCompleted = false;
                        break;
                    }
                }
                //Trace.TraceInformation("Itinerary.IsTicketed() exiting.");
                return ticketCompleted;
            }
            else
            {
                //Trace.TraceInformation("Itinerary.IsTicketed() exiting.");
                return false;
            }
        }

        public void IncrementETicketHit()
        {
            //Trace.TraceInformation("Itinerary.IncrementETicketHit entered : PNR = " + pnr);
            if (flightId <= 0)
            {
                throw new ArgumentException("FlightId must have a positive non-zero value.");
            }
            if (lastModifiedBy <= 0)
            {
                throw new ArgumentException("LastModifiedBy must have a positive non-zero value.");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@flightId", flightId);
            paramList[1] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.IncrementETicketHit, paramList);
            //Trace.TraceInformation("Itinerary.IncrementETicketHit exiting. rowsAffected = " + rowsAffected);
        }
        public static void UpdateItinerary(int segmentId, DateTime depTime, DateTime arrTime, int index)
        {
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@segmentId", segmentId);
            paramList[1] = new SqlParameter("@depTime", depTime);
            paramList[2] = new SqlParameter("@arrTime", arrTime);
            paramList[3] = new SqlParameter("@index", index);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateFlightSchedule, paramList);
        }
        
        public static void AddToUpdatedPNR(int bookingId, string PNR, string taskName, DateTime lastUpdatedOn, bool readStatus)
        {
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@BookingId", bookingId);
            paramList[1] = new SqlParameter("@PNR", PNR);
            paramList[2] = new SqlParameter("@taskName", taskName);
            paramList[3] = new SqlParameter("@lastUpdatedOn", lastUpdatedOn);
            paramList[4] = new SqlParameter("@readStatus", readStatus);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddToUpdatedPNR, paramList);
        }

        public bool CheckDomestic(string country)
        {
            bool result = true;
            if (country != segments[0].Origin.CountryCode)
            {
                result = false;
            }
            else
            {
                for (int i = 0; i < segments.Length; i++)
                {
                    if (country != segments[i].Destination.CountryCode)
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Method Gets the flight is domestic or not
        /// </summary>
        /// <param name="flightId">flightId</param>
        /// <returns>isDomestic flag</returns>
        public static bool IsDomesticFlight(int flightId)
        {
            ///Trace.TraceInformation("Itinerary.IsDomesticFlight entered flightId=" + flightId);
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsDomesticFlight, paramList, connection);
            bool isDomestic = false;
            using (DataTable dtDomestic = DBGateway.FillDataTableSP(SPNames.IsDomesticFlight, paramList))
            {
                if (dtDomestic != null && dtDomestic.Rows.Count > 0)
                {
                    DataRow data = dtDomestic.Rows[0];
                    if (data != null)
                    {
                        if (data["isDomestic"] != DBNull.Value)
                        {
                            isDomestic = Convert.ToBoolean(data["isDomestic"]);
                        }
                    }
                }
            }
            //data.Close();
            ///connection.Close();
            //Trace.TraceInformation("Itinerary.IsDomesticFlight Exited isDomestic=" + isDomestic);
            return isDomestic;
        }
        /// <summary>
        /// Method checks whether this pnr is present for the given booking source or not
        /// (Currently date is not under consideration)
        /// </summary>
        /// <param name="pnr">pnr</param>
        /// <param name="source">booking source</param>
        /// <param name="flightDate">flight date</param>
        /// <returns></returns>
        public static bool IsDuplicatePNR(string pnr, BookingSource source, string ticketString)
        {
            //Trace.TraceInformation("Itinerary.IsDuplicatePNR entered : PNR = " + pnr);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@pnr", pnr);
            paramList[1] = new SqlParameter("@source", (int)source);
            paramList[2] = new SqlParameter("@ticketString", ticketString);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.IsDuplicatePNR, paramList, connection);
            if (dataReader != null && dataReader.HasRows)
            {
                dataReader.Close();
                connection.Close();
                return true;
            }
            else
            {
                dataReader.Close();
                connection.Close();
            }
            //Trace.TraceInformation("BookingDetail.CheckPNR exit : PNR = " + pnr);
            return false;
        }
        /// <summary>
        /// Get FlightId By InvoiceNumber
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        public static int GetFlightIdByInvoiceNumber(int invoiceNumber)
        {
            int flightId = 0;
            //Trace.TraceInformation("FlightItinerary.GetFlightIdByInvoiceNumber entered : invoiceNumber = " + invoiceNumber.ToString());
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetFlightIdByInvoiceNumber, paramList, connection);
            if (data.Read())
            {
                flightId = Convert.ToInt32(data["flightId"]);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("FlightItinerary.GetFlightIdByInvoiceNumber exiting :" + flightId.ToString());
            return flightId;
        }
        /// <summary>
        /// Method to update ageny Id againstFlightId.
        /// </summary>
        /// <param name="flightId">FlightID</param>
        /// <param name="lastModifiedBy">LastModifiedBy</param>
        /// <param name="agenyId">AgencyId</param>
        public static void Update(int flightId, int lastModifiedBy, int agenyId)
        {
            //Trace.TraceInformation("FlightItinerary.Update entered: flightId=" + flightId);
            SqlParameter[] paramList = new SqlParameter[3];
            if (flightId <= 0)
            {
                throw new ArgumentException("FlightId must have a value", "flightId");
            }
            if (lastModifiedBy <= 0)
            {
                throw new ArgumentException("lastModified must have a value", "lastModifiedBy");
            }
            paramList[0] = new SqlParameter("@flightId", flightId);
            paramList[1] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[2] = new SqlParameter("@agenyId", agenyId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateFlightItineraryAgencyId, paramList);
            //Trace.TraceInformation("FlightItinerary.Update exiting: flightId=" + flightId);
        }
        /// get pnr from flightid
        /// </summary>
        /// <param name="flightId">FlightId </param>
        /// <returns>pnr</returns>
        public static string GetPNRByFlightId(int flightId)
        {
            string pnr = string.Empty;
            ////Trace.TraceInformation("FlightItinerary.GetPNRByFlightId entered : flightId = " + flightId.ToString());
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPNRByFlightId, paramList, connection);
            using (DataTable dtPnr = DBGateway.FillDataTableSP(SPNames.GetPNRByFlightId, paramList))
            {
                if (dtPnr != null && dtPnr.Rows.Count > 0)
                {
                    DataRow data = dtPnr.Rows[0];
                    if (data != null)
                    {
                        if (data["pnr"] != DBNull.Value)
                        {
                            pnr =Convert.ToString(data["pnr"]).Trim();
                        }
                    }
                }
            }
            //data.Close();
            //connection.Close();
            ////Trace.TraceInformation("FlightItinerary.GetPNRByFlightId exiting :" + pnr);
            return pnr;
        }
        /// <summary>
        /// Gets travel date by flightId
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public static DateTime GetTravelDateByFlightId(int flightId)
        {
            DateTime trDate = new DateTime();
            //Trace.TraceInformation("FlightItinerary.GetTravelDateByFlightId entered : flightId = " + flightId.ToString());
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetTravelDateByFlightId, paramList, connection);
            if (data.Read())
            {
                trDate = Convert.ToDateTime(data["travelDate"]);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("FlightItinerary.GetTravelDateByFlightId exited:");
            return trDate;
        }

        public List<SSR> GetSsrList()
        {
            //Trace.TraceInformation("Itinerary.GetSsrList entered");
            List<SSR> ssrList = new List<SSR>();
            for (int i = 0; i < Passenger.Length; i++)
            {
                if (Passenger[i].Meal.Code != null || Passenger[i].Meal.Code == string.Empty)
                {
                    for (int j = 0; j < Segments.Length; j++)
                    {
                        SSR ssr = new SSR();
                        ssr.PaxId = Passenger[i].PaxId;
                        ssr.SsrCode = "MEAL";
                        ssr.Detail = Passenger[i].Meal.Code + " " + Segments[j].Origin.AirportCode + "-" + Segments[j].Destination.AirportCode;
                        ssrList.Add(ssr);
                        //ssrList.Add(Passenger[i].PaxKey + "-" + ssr.SsrCode, ssr);
                    }
                }                
                if ((Passenger[i].FFAirline != null && Passenger[i].FFAirline.Length > 0) && (Passenger[i].FFNumber != null && Passenger[i].FFNumber.Length > 0))
                {
                    SSR ssr = new SSR();
                    ssr.PaxId = Passenger[i].PaxId;
                    ssr.SsrCode = "FQTV";
                    ssr.Detail = Passenger[i].FFAirline + Passenger[i].FFNumber;
                    ssrList.Add(ssr);
                    //ssrList.Add(Passenger[i].PaxKey + "-" + ssr.SsrCode, ssr);
                }
                if ((Passenger[i].PassportNo != null && Passenger[i].PassportNo.Length > 0) && (Passenger[i].Country!=null &&Passenger[i].Country.CountryCode != null && Passenger[i].Country.CountryCode.Length > 0))
                {
                    SSR ssr = new SSR();
                    ssr.PaxId = Passenger[i].PaxId;
                    ssr.SsrCode = "PSPT";
                    ssr.Detail = Passenger[i].PassportNo + "-" + Passenger[i].Country.CountryCode;
                    ssrList.Add(ssr);
                    //ssrList.Add(Passenger[i].PaxKey + "-" + ssr.SsrCode, ssr);
                }
                if ((Passenger[i].Type == PassengerType.Child || Passenger[i].Type == PassengerType.Infant) && (Passenger[i].DateOfBirth != null && Passenger[i].DateOfBirth > new DateTime()))
                {
                    SSR ssr = new SSR();
                    ssr.PaxId = Passenger[i].PaxId;
                    if (Passenger[i].Type == PassengerType.Child)
                    {
                        ssr.SsrCode = "CHLD";
                    }
                    else
                    {
                        ssr.SsrCode = "INFT";
                    }
                    ssr.Detail = Passenger[i].DateOfBirth.ToString("dd/MM/yyyy");
                    ssrList.Add(ssr);
                    //ssrList.Add(Passenger[i].PaxKey + "-" + ssr.SsrCode, ssr);
                }
            }
            //Trace.TraceInformation("Itinerary.GetSsrList exiting");
            return ssrList;
        }

        public override string ToString()
        {
            //Trace.TraceInformation("Itinerary.ToString entered");
            StringBuilder itineraryString = new StringBuilder(2048);
            itineraryString.Append("Details of Itinerary :\r\n\r\n");
            itineraryString.AppendFormat("Product Id : {0}\r\n", ProductId);
            itineraryString.AppendFormat("Product Type : {0}\r\n", ProductType.ToString());
            itineraryString.AppendFormat("Booking Mode : {0}\r\n", bookingMode.ToString());
            itineraryString.AppendFormat("Booking Id : {0}\r\n", bookingId);
            itineraryString.AppendFormat("Flight Id : {0}\r\n", flightId);
            itineraryString.AppendFormat("Agency Id : {0}\r\n", agencyId);
            itineraryString.AppendFormat("PNR : {0}\r\n", pnr);
            itineraryString.AppendFormat("Routing Trip Id : {0}\r\n", routingTripId);
            itineraryString.AppendFormat("Is Domestic : {0}\r\n", IsDomestic);
            itineraryString.AppendFormat("Booking Source : {0}\r\n", flightBookingSource);
            itineraryString.AppendFormat("Travel Date : {0}\r\n", travelDate);
            itineraryString.AppendFormat("Origin : {0}\r\nDestination : {1}\r\n", origin, destination);
            for (int i = 0; i < segments.Length; i++)
            {
                itineraryString.AppendFormat("{0}\r\n", segments[i].ToString());
            }
            for (int i = 0; i < passenger.Length; i++)
            {
                itineraryString.AppendFormat("{0}\r\n", passenger[i].ToString());
            }
            itineraryString.AppendFormat("Payment Mode : {0}\r\n", paymentMode.ToString());
            if (paymentMode == ModeOfPayment.CreditCard && ccPayment != null && ccPayment.Card != null)
            {
                itineraryString.AppendFormat("\tCard Holder's Name : {0}\r\n", ccPayment.Card.Name);
                itineraryString.AppendFormat("\tCard Number : {0} XXXX XXXX XX{1}\r\n", ccPayment.Card.Number.Substring(0, 4), ccPayment.Card.Number.Substring(14));
                itineraryString.AppendFormat("\tCard Exp Dage : {0}\r\n", ccPayment.Card.ExpDate);
                itineraryString.AppendFormat("\tAmount : {0}\r\n", ccPayment.Amount);
                itineraryString.AppendFormat("\tCurrency : {0}\r\n", ccPayment.Currency);
            }
            itineraryString.AppendFormat("Fare Type : {0}\r\n", fareType);
            itineraryString.AppendFormat("Pricing Type : {0}\r\n", pricingType);
            itineraryString.AppendFormat("Non Refundable : {0}\r\n", nonRefundable);
            itineraryString.AppendFormat("Agent Price : {0}\r\n", AgentPrice);
            itineraryString.AppendFormat("aliasAirlineCode : {0}\r\n", aliasAirlineCode);
            itineraryString.AppendFormat("isOurBooking :  {0}\r\n", IsOurBooking);
            //Trace.TraceInformation("Itinerary.ToString exiting");
            return itineraryString.ToString();
        }
       

        public static void RefreshItinerarySelectedData(FlightItinerary itinerary )
        {
            //Trace.TraceInformation("Itinerary.RefreshItineraryData entered");
            SqlConnection conn = DBGateway.GetConnection();
            SqlDataAdapter adapter = null;
            DataTable table = new DataTable();
            
            try
            {
                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@flightId", itinerary.FlightId);

                SqlCommand cmd = new SqlCommand(SPNames.GetSelectedColumnsFromItineraryAgainstFlightId, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(paramlist);
                adapter=new SqlDataAdapter(cmd);

                adapter.Fill(table);
                table.Rows[0]["origin"] = itinerary.Origin;
                table.Rows[0]["destination"] = itinerary.Destination;
                table.Rows[0]["travelDate"] = itinerary.TravelDate;
                table.Rows[0]["lastModifiedOn"] = DateTime.UtcNow;
                table.Rows[0]["lastModifiedBy"] = itinerary.LastModifiedBy;

                SqlCommandBuilder command = new SqlCommandBuilder(adapter);
                adapter.Update(table);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.ChangeRequest, CT.Core.Severity.High, 0, "Error: while updating itinerary in case of reissuance.| " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            }
            finally
            {
                conn.Close();
                adapter.Dispose();
                table.Dispose(); 
            }
            //Trace.TraceInformation("Itinerary.RefreshItineraryData exited");
        }

        ///// <summary>
        /// get the pref service fee list from user pref
        /// </summary>
        /// <param name="agencyId">agency to load the primary member, so that on behalf booking will get the agency commission</param>
        /// <param name="sFeeTypeList"></param>
        /// <param name="sFeeValueList"></param>
        public static void GetMemberPrefServiceFee(int agencyId, ref Dictionary<string, string> sFeeTypeList, ref Dictionary<string, string> sFeeValueList)
        
        {
            //Load the preference for the current user
            //int memberId = Member.GetPrimaryMemberId(agencyId); to do ziya
            

            Dictionary<string, string> prefPair = new Dictionary<string, string>();
            try
            {
               // prefPair = UserPreference.GetPreferenceList(memberId, ItemType.Flight);
            }
            catch (ArgumentException)
            {
                //TODO: Error View - Unknown user.
            }
            //Airline List
            List<string> airlineCodes = new List<string>();
            airlineCodes.Add("G9");
            airlineCodes.Add("AI");
            airlineCodes.Add("IX");
            airlineCodes.Add("G8");
            airlineCodes.Add("IC");
            airlineCodes.Add("6E");
            airlineCodes.Add("9W");
            airlineCodes.Add("S2");
            airlineCodes.Add("IT");
            airlineCodes.Add("9H");
            airlineCodes.Add("I7");
            airlineCodes.Add("SG");
            airlineCodes.Add("INTL");

            foreach (string airline in airlineCodes)
            {
                //read the service fee for international airlines
                //if (airline == "INTL")
                //{
                //    if (prefPair.ContainsKey(UserPreference.InternationalServiceFeeType))
                //    {
                //        sFeeTypeList.Add(airline, prefPair[UserPreference.InternationalServiceFeeType]);
                //    }
                //    else
                //    {
                //        sFeeTypeList.Add(airline, "FIXED");
                //    }
                //    if (prefPair.ContainsKey(UserPreference.InternationalServiceFeeValue))
                //    {
                //        sFeeValueList.Add(airline, prefPair[UserPreference.InternationalServiceFeeValue]);
                //    }
                //    else
                //    {
                //        sFeeValueList.Add(airline, "0");
                //    }
                //    continue;
                //}// ziyad
                //read the service fee for domestic/lcc airlines
                if (prefPair.ContainsKey(airline + "-SF-TYPE"))
                {
                    sFeeTypeList.Add(airline, prefPair[airline + "-SF-TYPE"]);
                }
                else
                {
                    sFeeTypeList.Add(airline, "FIXED");
                }
                if (prefPair.ContainsKey(airline + "-SF-VALUE"))
                {
                    sFeeValueList.Add(airline, prefPair[airline + "-SF-VALUE"]);
                }
                else
                {
                    sFeeValueList.Add(airline, "0");
                }
            }
        }

        public bool CheckForDuplicateBooking()
        {
            bool duplicateBooking = false;
            if (!string.IsNullOrEmpty(tripId))// skippnig duplicate checkng for copr multiple booking criteria
                return false;

            try
            {
                string retFlightNum = string.Empty, PaxNames = string.Empty;

                foreach(FlightInfo seg in segments)
                {
                    if(seg.Group == 1)
                    {
                        retFlightNum = seg.FlightNumber;
                        break;
                    }
                }

                foreach(FlightPassenger pax in passenger)
                {
                    if(PaxNames.Length > 0)
                    {
                        PaxNames += "," + pax.FirstName.ToLower() + " " + pax.LastName.ToLower();
                    }
                    else
                    {
                        PaxNames =  pax.FirstName.ToLower() + " " + pax.LastName.ToLower() ;
                    }
                }
                
              

                SqlParameter[] paramList = new SqlParameter[10];
                paramList[0] = new SqlParameter("@P_Origin", origin);
                paramList[1] = new SqlParameter("@P_Destination", destination);
                paramList[2] = new SqlParameter("@P_TravelDate", travelDate);
                paramList[3] = new SqlParameter("@P_AirlineCode", segments[0].Airline);
                //If FlightInventory booking all agencies should be checked for the duplicate booking
                if (flightBookingSource == BookingSource.FlightInventory)
                    paramList[4] = new SqlParameter("@P_AgentId", DBNull.Value);
                else
                    paramList[4] = new SqlParameter("@P_AgentId", agencyId);                
                paramList[5] = new SqlParameter("@P_FlightNum", segments[0].FlightNumber);
                if (string.IsNullOrEmpty(retFlightNum))
                {
                    paramList[6] = new SqlParameter("@P_RetFlightNum", DBNull.Value);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_RetFlightNum", retFlightNum);
                }
                paramList[7] = new SqlParameter("@P_ProductId", 1);
                paramList[8] = new SqlParameter("@P_PaxNames", PaxNames);
                if (flightBookingSource == BookingSource.FlightInventory)
                {
                    paramList[9] = new SqlParameter("@P_Source", (int)BookingSource.FlightInventory);
                }
                else
                {
                    paramList[9] = new SqlParameter("@P_Source", DBNull.Value);
                }
                DataTable dtDuplicate = DBGateway.FillDataTableSP("usp_CheckForDuplicateFlightBookings", paramList);                

                if(dtDuplicate != null)
                {
                    if(dtDuplicate.Rows.Count > 0)//If atleast one row is returned then we have duplicate booking
                    {
                        duplicateBooking = (Convert.ToInt32(dtDuplicate.Rows[0]["Flights"]) > 0 ? true : false);
                    }
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, createdBy, "Failed to Check Duplicate Booking for Flight Itinerary. Reason : " + ex.ToString(), "");
            }

            return duplicateBooking;
        }

        /// <summary>
        /// Retrieves FlightId from Order Number stored in TicketAdvisory column for PKFares.
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public static int GetPNRforPKFareOrderNumber(string orderNumber)
        {
            int FlightId = 0;

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_OrderNumber", orderNumber);
                DataTable dtFlight = DBGateway.FillDataTableSP("usp_GetFlightIdForOrderNumber", paramList);

                if(dtFlight != null && dtFlight.Rows.Count > 0)
                {
                    FlightId = Convert.ToInt32(dtFlight.Rows[0]["flightId"]);
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to retrieve FlightId from PKFare Order Number : " + orderNumber + ". Reason : " + ex.ToString(), "");
            }

            return FlightId;
        }

        /// <summary>
        /// Update AirlinePNR pushed for PKFares from TicketingService.
        /// </summary>
        /// <param name="flightId"></param>
        /// <param name="airlinePNR"></param>
        /// <returns></returns>
        public static int UpdateAirlinePNR(int flightId, string airlinePNR)
        {
            int count = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_FlightId", flightId);
                paramList[1] = new SqlParameter("@P_AirlinePNR", airlinePNR);
                count = DBGateway.ExecuteNonQuerySP("usp_UpdateAirlinePNR", paramList);
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to update Airline PNR (" + airlinePNR + ") for FlightId=" + flightId + ". Reason : " + ex.ToString(), "");
            }
            return count;
        }
        public static DataTable GetInProgressPNR(string PNR)// Retrieving bookingId by passing PNR(added by Harish on 4-5-2018)
        {
            DataTable dtStatus = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_PNR", PNR);

                dtStatus = DBGateway.FillDataTableSP("USP_GetInProgressPNR", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to Get PNR (" + PNR + ") . Reason : " + ex.ToString(), "");
            }
            return dtStatus;
        }

        public static string GenerateRoutingTripPNR()
        {
            string PNR = string.Empty;

            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@P_dpRecordLocator", SqlDbType.VarChar);
                parameters[0].Direction = ParameterDirection.Output;
                parameters[0].Size = 10;
                DBGateway.ExecuteNonQuerySP("GenerateSuperPNR", parameters);

                if (parameters[0].Value != DBNull.Value)
                {
                    PNR = parameters[0].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to generate Routing Trip PNR. Reason : " + ex.ToString(), "");
            }

            return PNR;
        }


        public static int GetFlightIdbyRoutingTripId(string routingTripId, string pnr)
        {
            int flightId = 0;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_RoutingTripId", routingTripId));
                parameters.Add(new SqlParameter("@P_PNR", pnr));

                DataTable dtFlight = DBGateway.FillDataTableSP("usp_GetFlightIdbyRoutingTripId", parameters.ToArray());

                if (dtFlight != null && dtFlight.Rows.Count > 0)
                {
                    if (dtFlight.Rows[0]["FlightId"] != DBNull.Value)
                    {
                        flightId = Convert.ToInt32(dtFlight.Rows[0]["FlightId"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Retrieve FlightId by Routing Trip Id: " + routingTripId + ". Reason : " + ex.ToString(), "");
            }

            return flightId;
        }


        #endregion

        /// <summary>
        /// Updates the GDS PNR FOR PK FARES AS PER TicketingNumPush_V2
        /// </summary>
        /// <param name="flightId"></param>
        /// <param name="airlinePNR"></param>
        /// <returns></returns>
        public static int UpdatePKFaresGDSPNR(int flightId, string gdsPNR)
        {
            int count = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_FlightId", flightId);
                paramList[1] = new SqlParameter("@P_GDSPNR", gdsPNR);
                count = DBGateway.ExecuteNonQuerySP("usp_UpdatePKFaresGDSPNR", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to update gds PNR (" + gdsPNR + ") for FlightId=" + flightId + ". Reason : " + ex.ToString(), "");
            }
            return count;
        }

        /// <summary>
        /// To update dynamic PCC HAP
        /// </summary>
        /// <param name="iFlightId"></param>
        /// <param name="sDHAP"></param>
        /// <returns></returns>
        public static int UpdateDynamicHAP(int iFlightId, string sDHAP)
        {
            int count = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_FlightId", iFlightId);
                paramList[1] = new SqlParameter("@P_DHAP", sDHAP);
                count = DBGateway.ExecuteNonQuerySP("usp_UpdateDynamicHAP", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to update Dynamic HAP (" + sDHAP + ") for FlightId=" + iFlightId + ". Reason : " + ex.ToString(), "");
            }
            return count;
        }

        public static List<FlightItinerary> GetCorpItineraries(int flightId)
        {
            List<FlightItinerary> itineraries = new List<FlightItinerary>();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_FlightId", flightId));
                DataTable dtFlights = DBGateway.FillDataTableSP("usp_GetCorpFlightItineraries", parameters.ToArray());

                foreach (DataRow dr in dtFlights.Rows)
                {
                    FlightItinerary itinerary = new FlightItinerary(Convert.ToInt32(dr["flightId"]));
                    itineraries.Add(itinerary);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return itineraries;
        }

        /// <summary>
        /// This method will return the total booking amount including GST/VAT.
        /// </summary>
        /// <returns></returns>
        public decimal GetBookingAmount()
        {
            decimal totalAmount = 0;
            try
            {
                foreach (FlightPassenger pax in passenger)
                {
                    if (flightBookingSource != BookingSource.TBOAir)
                    {
                        totalAmount += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.BaggageCharge + pax.Price.MealCharge + pax.Price.SeatPrice + pax.Price.Markup + pax.Price.OutputVATAmount + pax.Price.HandlingFeeAmount - pax.Price.Discount;
                    }
                    else
                    {
                        totalAmount += Math.Ceiling(pax.Price.PublishedFare + pax.Price.Tax + pax.Price.BaggageCharge + pax.Price.MealCharge + pax.Price.SeatPrice + pax.Price.Markup + pax.Price.OutputVATAmount + pax.Price.HandlingFeeAmount - pax.Price.Discount + pax.Price.OtherCharges + pax.Price.AdditionalTxnFee + pax.Price.SServiceFee + pax.Price.TransactionFee);
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return totalAmount;
        }
    }
}
