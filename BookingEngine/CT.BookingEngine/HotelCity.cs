using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
//using CT.BookingEngine;

namespace CT.BookingEngine
{
    /// <summary>
    /// This class is using to save the HotelCity in DB and retrive the HotelCity from DB
    /// </summary>
    public class HotelCity
    {
        #region variables
        /// <summary>
        /// cityId
        /// </summary>
        private int cityId;
        /// <summary>
        /// cityCode
        /// </summary>
        private string cityCode;
        /// <summary>
        /// cityName
        /// </summary>
        private string cityName;
        /// <summary>
        /// stateCode
        /// </summary>
        private string stateCode;
        /// <summary>
        /// stateName
        /// </summary>
        private string stateName;
        /// <summary>
        /// countryName
        /// </summary>
        private string countryName;
        /// <summary>
        /// countryCode
        /// </summary>
        private string countryCode;
        /// <summary>
        /// currencyCode
        /// </summary>
        private string currencyCode;
        /// <summary>
        /// gtaCode
        /// </summary>
        private string gtaCode;
        /// <summary>
        /// hobCode
        /// </summary>
        private string hobCode;
        /// <summary>
        /// touCode
        /// </summary>
        private string touCode;
        /// <summary>
        /// tboCode
        /// </summary>
        private string tboCode;
        /// <summary>
        /// mikiCode
        /// </summary>
        private string mikiCode;
        /// <summary>
        /// travcoCode
        /// </summary>
        private string travcoCode;
        /// <summary>
        /// HotelBookingSource
        /// </summary>
        private HotelBookingSource source;
        /// <summary>
        /// index
        /// </summary>
        private int index;
        /// <summary>
        /// dotwCode
        /// </summary>
        private string dotwCode;
        /// <summary>
        /// wstCode
        /// </summary>
        private string wstCode;
        /// <summary>
        /// rezCode
        /// </summary>
        private string rezCode;
        /// <summary>
        /// lohCode
        /// </summary>
        private string lohCode;
        /// <summary>
        /// hisCode
        /// </summary>
        private string hisCode;
        /// <summary>
        /// jacCode
        /// </summary>
        private string jacCode;
        /// <summary>
        /// eetCode
        /// </summary>
        private string eetCode;
        /// <summary>
        /// agoda
        /// </summary>
        private string agoda;
        /// <summary>
        /// yatra   --Added by somasekhar on 24/08/2018
        /// </summary>
        private string yatra;

 /// <summary>
        /// grn
        /// </summary>
        private string grn;
        /// <summary>
        /// OYO   -- Added by somasekhar on 11/12/2018
        /// </summary>
        private string oyo;

        /// <summary>
        /// GIMMONIX   -- Added by Harish on 04/01/2019
        /// </summary>
        private string gimmonix;
        #endregion

        #region properities
        /// <summary>
        /// CityId
        /// </summary>
        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }
        /// <summary>
        /// CityCode
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        /// <summary>
        /// CityName
        /// </summary>
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        /// <summary>
        /// StateCode
        /// </summary>
        public string StateCode
        {
            get { return stateCode; }
            set { stateCode = value; }
        }
        /// <summary>
        /// StateName
        /// </summary>
        public string StateName
        {
            get { return stateName; }
            set { stateName = value; }
        }
        /// <summary>
        /// CountryCode
        /// </summary>
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
        /// <summary>
        /// CountryName
        /// </summary>
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        /// <summary>
        /// CurrencyCode
        /// </summary>
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }
        /// <summary>
        /// Source
        /// </summary>
        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        /// <summary>
        /// Index
        /// </summary>
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        /// <summary>
        /// GTACode
        /// </summary>
        public string GTACode
        {
            get { return gtaCode; }
            set { gtaCode = value; }
        }
        /// <summary>
        /// HOBCode
        /// </summary>
        public string HOBCode
        {
            get { return hobCode; }
            set { hobCode = value; }
        }
        /// <summary>
        /// TOUCode
        /// </summary>
        public string TOUCode
        {
            get { return touCode; }
            set { touCode = value; }
        }
        /// <summary>
        /// TBOCode
        /// </summary>
        public string TBOCode
        {
            get { return tboCode; }
            set { tboCode = value; }
        }
        /// <summary>
        /// MikiCode
        /// </summary>
        public string MikiCode
        {
            get { return mikiCode; }
            set { mikiCode = value; }
        }
        /// <summary>
        /// TravcoCode
        /// </summary>
        public string TravcoCode
        {
            get { return travcoCode; }
            set { travcoCode = value; }
        }
        /// <summary>
        /// DOTWCode
        /// </summary>
        public string DOTWCode
        {
            get { return dotwCode; }
            set { dotwCode = value; }
        }
        /// <summary>
        /// WSTCode
        /// </summary>
        public string WSTCode
        {
            get { return wstCode; }
            set { wstCode = value; }
        }
        /// <summary>
        /// RezCode
        /// </summary>
        public string RezCode
        {
            get { return rezCode; }
            set { rezCode = value; }
        }
        /// <summary>
        /// RezCode
        /// </summary>
        public string LOHCode
        {
            get { return lohCode; }
            set { lohCode = value; }
        }
        /// <summary>
        /// HISCode
        /// </summary>
        public string HISCode
        {
            get { return hisCode; }
            set { hisCode = value; }
        }
        /// <summary>
        /// JACCode
        /// </summary>
        public string JACCode
        {
            get { return jacCode; }
            set { jacCode = value; }
        }
        /// <summary>
        /// EETCode
        /// </summary>
        public string EETCode
        {
            get { return eetCode; }
            set { eetCode = value; }
        }
        /// <summary>
        /// AgodaCode
        /// </summary>
        public string AgodaCode
        {
            get { return agoda; }
            set { agoda = value; }
        }
        // Added by somasekhar on 24/08/2018
        /// <summary>
        /// YatraCode
        /// </summary>
        public string YatraCode
        {
            get { return yatra; }
            set { yatra = value; }
        }

         /// <summary>
        /// GRNCode
        /// </summary>
        public string GrnCode
        {
            get { return grn; }
            set { grn = value; }
        }
        // Added by somasekhar on 11/12/2018
        /// <summary>
        /// OYOCode
        /// </summary>
        public string OYOCode
        {
            get { return oyo; }
            set { oyo = value; }
        }

        // Added by Harish on Apr/1/2019
        /// <summary>
        /// Gimmonix Code
        /// </summary>
        public string Gimmonix
        {
            get { return gimmonix; }
            set { gimmonix = value; }
        }


        
        #endregion
        /// <summary>
        /// This method is to retrieve the result from ListGTACity corresponding to ListHotelCity
        /// </summary>
        /// <param name="gtaCityList">gtaCityList</param>
        /// <returns>List HotelCity</returns>
        public static List<HotelCity> GetHotelCity(List<GTACity> gtaCityList)
        {
            List<HotelCity> hotelCityList = new List<HotelCity>();
            foreach (GTACity gtaCity in gtaCityList)
            {
                HotelCity hCity = new HotelCity();
                hCity.cityCode = gtaCity.CityCode;
                hCity.cityName = gtaCity.CityName;
                hCity.countryCode = gtaCity.CountryCode;
                hCity.countryName = gtaCity.CountryName;
                hCity.currencyCode = gtaCity.CurrencyCode;
                hCity.index = gtaCity.Index;
                hCity.source = HotelBookingSource.GTA;
            }
            return hotelCityList;
        }
        ///<summary>
        /// This method is to retrieve the result from DB corresponding to cityname
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <param name="countryName">countryName</param>
        /// <returns>A list containing the result in a list</returns>
        public List<HotelCity> GetHotelCityByCityName(string cityName, string countryName)
        {
            //Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            SqlDataReader dr = null;
            SqlParameter[] paramList = new SqlParameter[0];
            List<HotelCity> cityList = new List<HotelCity>();
            SqlConnection con = DBGateway.GetConnection();
            if (countryName == null || countryName.Length == 0)
            {
                paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@cityName", cityName);
                dr = DBGateway.ExecuteReaderSP(SPNames.GetHotelCityByCityName, paramList,con);
            }
            else
            {
                paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@cityName", cityName);
                paramList[1] = new SqlParameter("@countryName", countryName);
                dr = DBGateway.ExecuteReaderSP(SPNames.GetHotelCityByCountryName, paramList,con);
            }
            while (dr.Read())
            {
                HotelCity city = new HotelCity();
                city.cityId = Convert.ToInt32(dr["cityId"]);
                city.cityName = Convert.ToString(dr["Destination"]);
                city.stateName = Convert.ToString(dr["stateprovince"]);
                city.countryName = Convert.ToString(dr["country"]);
                city.countryCode = Convert.ToString(dr["countryCode"]);
                city.gtaCode = Convert.ToString(dr["GTA"]);
                city.hobCode = Convert.ToString(dr["HotelBeds"]);
                city.touCode = Convert.ToString(dr["Tourico"]);
                city.tboCode = Convert.ToString(dr["TBOConnect"]);
                city.currencyCode = Convert.ToString(dr["currency"]);
                city.mikiCode = Convert.ToString(dr["Miki"]);
                city.travcoCode = Convert.ToString(dr["Travco"]);
                city.dotwCode = Convert.ToString(dr["DOTW"]);
                city.wstCode = Convert.ToString(dr["WST"]);
                city.rezCode = dr["RezLive"].ToString();
                city.lohCode = dr["LOH"].ToString();
                city.hisCode = dr["HIS"].ToString();
                cityList.Add(city);
            }
            dr.Close();
            con.Close();
            //Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            return cityList;
        }
        ///<summary>
        /// This method is to save the Record into HotelCityCode DB
        /// </summary>
        public void SaveHotelCity()
        {
            //Trace.TraceInformation("HotelCity.SaveHotelCity entered:");
            SqlParameter[] paramList = new SqlParameter[0];
            SqlConnection con = DBGateway.GetConnection();
            paramList = new SqlParameter[11];
            paramList[0] = new SqlParameter("@cityId", cityId);
            paramList[1] = new SqlParameter("@gta", gtaCode);
            paramList[2] = new SqlParameter("@hotelBeds", hobCode);
            paramList[3] = new SqlParameter("@tourico", touCode);
            paramList[4] = new SqlParameter("@miki", mikiCode);
            paramList[5] = new SqlParameter("@travco", travcoCode);
            paramList[6] = new SqlParameter("@currency", currencyCode);
            paramList[7] = new SqlParameter("@tboConnect", tboCode);
            paramList[8] = new SqlParameter("@dotw", dotwCode);
            paramList[9] = new SqlParameter("@wst", wstCode);
            paramList[10] = new SqlParameter("@his", hisCode);
            DBGateway.ExecuteNonQuerySP(SPNames.UpdateCityCode, paramList);
            con.Close();
            //Trace.TraceInformation("HotelCity.SaveHotelCity exited:");
        }
        ///<summary>To get city code with booking source</summary>
        /// <param name="cityName">cityName</param>
        /// <param name="countryName">countryName</param>
        /// <param name="hSource">hSource</param>
        /// <returns>string</returns>
        public string GetCityCode(string cityName, string countryName, HotelBookingSource hSource)
        {
            //Trace.TraceInformation("HotelCity.GetCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@source", (int)hSource);
            paramlist[1] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = DBGateway.GetConnection();
            string cityCode = string.Empty;
            countryName = countryName.Trim().ToUpper();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetHotelCityCode, paramlist,connection);
            while (datareader.Read())
            {
                if (countryName.Length > 0 && countryName == datareader["countryName"].ToString().Trim().ToUpper())
                {
                    cityCode = datareader["destCode"].ToString();
                    break;
                }
                else
                {
                    cityCode = datareader["destCode"].ToString();
                }
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetCityCode exiting");
            return cityCode;
        }
        ///<summary>
        /// To retrieve information about Tourico airportcode
        /// </summary>
        /// <param name="parentCityCode">parentCityCode</param>
        /// <param name="cityName">cityName</param>
        /// <returns>string</returns>

        public string GetTouricoAirportCode(string parentCityCode, string cityName)
        {
            //Trace.TraceInformation("HotelCity.GetTouricoAirportCode Entered");
            SqlParameter[] paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@parentCityCode", parentCityCode);
            paramlist[1] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = DBGateway.GetConnection();
            string airPortCode = string.Empty;
            cityName = cityName.Trim().ToUpper();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetTouricoAirportCode, paramlist,connection);
            while (datareader.Read())
            {
                if (cityName.Length > 0 && cityName == datareader["cityName"].ToString().Trim().ToUpper())
                {
                    airPortCode = datareader["airPortCode"].ToString();
                    break;
                }
                else
                {
                    airPortCode = datareader["airPortCode"].ToString();
                }
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetTouricoAirportCode exiting");
            return airPortCode;
        }
        /// <summary>
        /// To Retrieve GTA Code from GTACityCode DB
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <returns>List HotelCity</returns>
        public List<HotelCity> GetGTACode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            //Trace.TraceInformation("HotelCity.GetGTACode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName",cityName);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetGTACode, paramlist,connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["cityName"]);
                city.countryName = Convert.ToString(datareader["countryName"]);
                city.gtaCode = Convert.ToString(datareader["cityCode"]);
                city.CountryCode = Convert.ToString(datareader["countryCode"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            return cityList;
        }
        /// <summary>
        /// To Retrieve HotelBedCode from HotelBedsDestination DB
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <returns>List HotelCity</returns>
        public List<HotelCity> GetHotelBedCode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            //string HobCode=null;
            //Trace.TraceInformation("HotelCity.GetHotelBedCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetHotelBedCode, paramlist,connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["destName"]);
                city.countryName = Convert.ToString(datareader["countryName"]);
                city.hobCode = Convert.ToString(datareader["destCode"]);
                city.CountryCode = Convert.ToString(datareader["countryCode"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            return cityList;
        }
        /// <summary>
        /// To Retrieve Tourico code from 
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <returns>List HotelCity</returns>
        public List<HotelCity> GetTouricoCode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            //Trace.TraceInformation("HotelCity.GetTouricoCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetTouricoCode, paramlist,connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["cityName"]);
                city.countryName = Convert.ToString(datareader["country"]);
                city.touCode = Convert.ToString(datareader["airportCode"]);
                city.CountryCode = Convert.ToString(datareader["countryCode"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            return cityList;
        }
        ///<summary>To get citycode from IAN table</summary>
        /// <param name="parentCityCode">parentCityCode</param>
        /// <param name="cityName">cityName</param>
        public void GetIANCityCode(string parentCityCode, string cityName)
        {
            //Trace.TraceInformation("HotelCity.GetIANCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@parentCityCode", parentCityCode);
            paramlist[1] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetIANCityCode, paramlist,connection);
            if (datareader.Read())
            {
                this.cityCode = (string)datareader["airPortCode"];
                this.cityName = (string)datareader["cityName"];
                if (!datareader["countryCode"].Equals(DBNull.Value))
                {
                    this.countryCode = datareader["countryCode"].ToString();
                }
                if (!datareader["stateCode"].Equals(DBNull.Value))
                {
                    this.stateCode = datareader["stateCode"].ToString();
                }
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetIANCityCode exiting");
        }
        /// <summary>
        /// To get citycode from MikiCityCode table
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <returns>List HotelCity</returns>
        public List<HotelCity> GetMikiCityCode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            //Trace.TraceInformation("HotelCity.GetMikiCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetMikiCityCode, paramlist,connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["CityName"]).Trim();
                city.mikiCode = Convert.ToString(datareader["CityCode"]);
                city.CountryCode = Convert.ToString(datareader["countryCode"]);
                city.countryName = Convert.ToString(datareader["countryName"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetMikiCityCode exiting:" + cityName);
            return cityList;
        }
        /// <summary>
        /// To get citycode from TravcoCityCode table
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <returns>List HotelCity</returns>
        public List<HotelCity> GetTravcoCityCode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            //Trace.TraceInformation("HotelCity.GetTravcoCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetTravcoCityCode, paramlist,connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["CityName"]).Trim();
                city.travcoCode = Convert.ToString(datareader["CityCode"]);
                city.countryCode = Convert.ToString(datareader["StandardCountryCodes"]);
                city.CountryName = Convert.ToString(datareader["countryName"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetTravcoCityCode exiting:" + cityName);
            return cityList;
        }
        ///<summary>
        /// This method is used to get DestinationId corresponding to city and country 
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <param name="countryName">countryName</param>
        /// <returns>string</returns>
        public string GetIANDestinationId(string cityName, string countryName)
        {
            //Trace.TraceInformation("HotelCity.GetIANDestinationId Entered");
            SqlParameter[] paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            paramlist[1] = new SqlParameter("@countryName", countryName);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetIANDestinationID, paramlist,connection);
            string destId = string.Empty;
            if (datareader.Read())
            {
                destId = datareader["destinationID"].ToString();
            }
            datareader.Close();
            connection.Close();
            //Trace.TraceInformation("HotelCity.GetIANDestinationId exiting");
            return destId;
        }
        ///<summary>
        /// To retrive state list corresponding to country
        /// </summary>
        /// <param name="countryCode">countryCode</param>
        /// <returns>string</returns>
        public static string GetIANStateList(string countryCode)
        {
            //Trace.TraceInformation("HotelCity.GetIANStateList Entered");
            string stateListString = string.Empty;
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@countryCode", countryCode);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetIANStateList, paramlist,connection);
            StringBuilder stateList = new StringBuilder();
            while (datareader.Read())
            {
                if (!datareader["stateName"].Equals(DBNull.Value))
                {
                    stateList.Append(datareader["stateName"].ToString().Replace("|", "").Replace("#", ""));
                }
                if (!datareader["stateCode"].Equals(DBNull.Value))
                {
                    stateList.Append("|" + datareader["stateCode"].ToString().Replace("|", "").Replace("#", "") + "#");
                }
            }
            datareader.Close();
            connection.Close();
            stateListString = stateList.ToString();
            if (stateListString.Length > 0)
            {
                stateListString = stateListString.Remove(stateListString.Length - 1, 1);
            }
            //Trace.TraceInformation("HotelCity.GetIANStateList exiting");
            return stateListString;
        }
        /// <summary>
        /// This Method is used to get currency Code
        ///  </summary>
        /// <param name="currencyCode">currencyCode</param>
        /// <returns>string</returns>
        public static string GetCurrencySymbol(string currencyCode)
        {
            string symbol = string.Empty;
            if (currencyCode == "USD")
            {
                symbol = "$";
            }
            else if (currencyCode == "GBP")
            {
                symbol = "�";
            }
            else if (currencyCode == "EUR")
            {
                symbol = "�";
            }
            else if (currencyCode == "AUD")
            {
                symbol = "A$";
            }
            else if (currencyCode == "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
            {
                symbol = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
            }
            else
            {
                symbol = currencyCode;
            }
            return symbol;
        }
        /// <summary>
        /// This method gets the list of all distinct countries in HotelCityCode table
        /// </summary>
        /// <returns>List string</returns>
        public List<string> GetAllHotelCountries()
        {
            //Trace.TraceInformation("HotelCity.GetAllHotelCountries Entered");
            SqlParameter[] paramList = new SqlParameter[0];
            SqlConnection con = DBGateway.GetConnection();
            List<string> countryList = new List<string>();
            SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.GetHotelCountries, paramList,con);
            while (dr.Read())
            {
                countryList.Add(Convert.ToString(dr["country"]));
            }
            dr.Close();
            con.Close();
            //Trace.TraceInformation("HotelCity.GetAllHotelCountries Entered");
            return countryList;
        }

        /// <summary>
        /// This Method is used to get countryList
        /// </summary>
        /// <returns>List string</returns>
        public List<string> GetAllHotelCities()
        {
            List<string> countryList = new List<string>();
            //Trace.TraceInformation("HotelCity.GetAllHotelCities Entered");
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                DataTable dtCities = DBGateway.FillDataTableSP(SPNames.GetAllHotelCities, paramList);
                foreach (DataRow dr in dtCities.Rows)
                {
                    countryList.Add(Convert.ToString(dr["cityId"]) + "|" + dr["Destination"].ToString() + "|" + dr["stateprovince"].ToString() + "|" + dr["Country"].ToString());
                }
                //Trace.TraceInformation("HotelCity.GetAllHotelCities Exited");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return countryList;
        }

        /// <summary>
        /// This method gets the list of all cities in the given country
        /// </summary>
        /// <param name="country">country</param>
        /// <returns>List HotelCity</returns>
        public List<HotelCity> GetHotelCities(string country)
        {
            //Trace.TraceInformation("Hotelcity.GetHotelCities Entered:" + country);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@country", country);
            List<HotelCity> cityList = new List<HotelCity>();
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.GetHotelCities, paramList,con);
            while (dr.Read())
            {
                HotelCity city = new HotelCity();
                city.cityId = Convert.ToInt32(dr["cityId"]);
                city.cityName = (dr["stateprovince"] == DBNull.Value || Convert.ToString(dr["stateprovince"]) == "") ? Convert.ToString(dr["destination"]) : Convert.ToString(dr["destination"]) + ",   " + Convert.ToString(dr["stateprovince"]);
                cityList.Add(city);
            }
            dr.Close();
            con.Close();
            //Trace.TraceInformation("Hotelcity.GetHotelCities Exited:" + country);
            return cityList;
        }

        /// <summary>
        /// This Method is used to get countryList
        /// </summary>
        /// <returns>List string</returns>
        public List<string> GetAllHotelCitiesForGimmonix()
        {
            List<string> countryList = new List<string>();
            //Trace.TraceInformation("HotelCity.GetAllHotelCities Entered");
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                DataTable dtCities = DBGateway.FillDataTableSP("BKE_HTL_HOTEL_CITY_LIST", paramList);
                foreach (DataRow dr in dtCities.Rows)
                {
                    countryList.Add(Convert.ToString(dr["CityId"]) + "|" + dr["CityName"].ToString());
                }
                //Trace.TraceInformation("HotelCity.GetAllHotelCities Exited");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return countryList;
        }

        /// <summary>
        /// This method gets the a HotelCity based on cityId from HTL_HOTEL_CITY_CODE table
        /// </summary>
        /// <param name="cityIdNo">cityIdNo</param>
        /// <returns>HotelCity</returns>
        public static HotelCity GetCityInfoById(int cityIdNo)
        {
            HotelCity city = null;
            try
            {
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@cityId", cityIdNo));

                using (DataTable dtHotelCity = DBGateway.FillDataTableSP("usp_GetHotelCity", liParams.ToArray()))
                {
                    if (dtHotelCity != null && dtHotelCity.Rows.Count > 0)
                    {
                        DataRow dr = dtHotelCity.Rows[0];
                        city = new HotelCity();
                        city.cityName = Convert.ToString(dr["CityName"]);
                        city.cityId = Convert.ToInt32(dr["cityid"]);
                        city.countryCode = Convert.ToString(dr["CountryCode"]);
                        city.countryName = Convert.ToString(dr["countryName"]);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return city;
        }

        /// <summary>
        /// This method gets the a HotelCity based on cityId
        /// </summary>
        /// <param name="cityIdNo">cityIdNo</param>
        /// <returns>HotelCity</returns>
        public static HotelCity Load(int cityIdNo)
        {
            ////Trace.TraceInformation("HotelCity.Load entered:"+cityIdNo);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityId", cityIdNo);
            HotelCity city = null;
            //SqlConnection con = DBGateway.GetConnection();
            //SqlDataReader dr = DBGateway.ExecuteReaderSP(SPNames.LoadHotelCity, paramList,con);
            using (DataTable dtHotelCity = DBGateway.FillDataTableSP(SPNames.LoadHotelCity, paramList))
            {
                if (dtHotelCity != null && dtHotelCity.Rows.Count > 0)
                {
                    DataRow dr = dtHotelCity.Rows[0];
                    city = new HotelCity();
                    city.cityName = Convert.ToString(dr["destination"]);
                    city.cityId = Convert.ToInt32(dr["cityId"]);
                    city.countryCode = Convert.ToString(dr["countryCode"]);
                    city.countryName = Convert.ToString(dr["country"]);
                    city.currencyCode = Convert.ToString(dr["currency"]);
                    if (dr["destinationId"] != DBNull.Value)
                    {
                        city.cityCode = Convert.ToString(dr["destinationId"]);
                    }
                    else
                    {
                        city.cityCode = string.Empty;
                    }
                    if (dr["stateprovince"] != DBNull.Value)
                    {
                        city.stateCode = Convert.ToString(dr["stateprovince"]);
                    }
                    else
                    {
                        city.stateCode = string.Empty;
                    }
                    if (dr["GTA"] != DBNull.Value)
                    {
                        city.gtaCode = Convert.ToString(dr["GTA"]);
                    }
                    else
                    {
                        city.gtaCode = string.Empty;
                    }
                    if (dr["HotelBeds"] != DBNull.Value)
                    {
                        city.hobCode = Convert.ToString(dr["HotelBeds"]);
                    }
                    else
                    {
                        city.hobCode = string.Empty;
                    }
                    if (dr["Tourico"] != DBNull.Value)
                    {
                        city.touCode = Convert.ToString(dr["Tourico"]);
                    }
                    else
                    {
                        city.touCode = string.Empty;
                    }
                    if (dr["TBOHotel"] != DBNull.Value)
                    {
                        city.tboCode = Convert.ToString(dr["TBOHotel"]);
                    }
                    else
                    {
                        city.tboCode = string.Empty;
                    }
                    if (dr["Miki"] != DBNull.Value)
                    {
                        city.mikiCode = Convert.ToString(dr["Miki"]);
                    }
                    else
                    {
                        city.mikiCode = string.Empty;
                    }
                    if (dr["Travco"] != DBNull.Value)
                    {
                        city.travcoCode = Convert.ToString(dr["Travco"]);
                    }
                    else
                    {
                        city.travcoCode = string.Empty;
                    }
                    if (dr["DOTW"] != DBNull.Value)
                    {
                        city.dotwCode = Convert.ToString(dr["DOTW"]);
                    }
                    else
                    {
                        city.dotwCode = string.Empty;
                    }
                    if (dr["WST"] != DBNull.Value)
                    {
                        city.wstCode = Convert.ToString(dr["WST"]);
                    }
                    else
                    {
                        city.wstCode = string.Empty;
                    }

                    if (dr["RezLive"] != DBNull.Value)
                    {
                        city.rezCode = Convert.ToString(dr["RezLive"]);
                    }
                    else
                    {
                        city.rezCode = string.Empty;
                    }

                    if (dr["LOH"] != DBNull.Value)
                    {
                        city.lohCode = Convert.ToString(dr["LOH"]);
                    }
                    else
                    {
                        city.lohCode = string.Empty;
                    }

                    if (dr["HIS"] != DBNull.Value)
                    {
                        city.hisCode = Convert.ToString(dr["HIS"]);
                    }
                    else
                    {
                        city.hisCode = string.Empty;
                    }
                    if (dr["JAC"] != DBNull.Value)
                    {
                        city.jacCode = Convert.ToString(dr["JAC"]);
                    }
                    else
                    {
                        city.jacCode = string.Empty;
                    }
                    if (dr["EET"] != DBNull.Value)
                    {
                        city.eetCode = Convert.ToString(dr["EET"]);
                    }
                    else
                    {
                        city.eetCode = string.Empty;
                    }
                    if (dr["Agoda"] != DBNull.Value)
                    {
                        city.agoda = Convert.ToString(dr["Agoda"]);
                    }
                    else
                    {
                        city.agoda = string.Empty;
                    }
                    //Added by somasekhar on 21/08/2018 
                    if (dr["Yatra"] != DBNull.Value)
                    {
                        city.yatra = Convert.ToString(dr["Yatra"]);
                    }
                    else
                    {
                        city.yatra = string.Empty;
                    }

                    if (dr["GRN"] != DBNull.Value)
                    {
                        city.grn = Convert.ToString(dr["GRN"]);
                    }
                    else
                    {
                        city.grn = string.Empty;
                    }
                    //Added by somasekhar on 11/12/2018 
                    if (dr["OYO"] != DBNull.Value)
                    {
                        city.oyo = Convert.ToString(dr["OYO"]);
                    }
                    else
                    {
                        city.oyo = string.Empty;
                    }
                    //Added by Harish on Apr/01/2019 
                    if (dr["GIMMONIX"] != DBNull.Value)
                    {
                        city.gimmonix = Convert.ToString(dr["GIMMONIX"]);
                    }
                    else
                    {
                        city.gimmonix = string.Empty;
                    }
                }
            }
            //dr.Close();
            //con.Close();
            ////Trace.TraceInformation("HotelCity.Load exited");
            return city;
        }

        /// <summary>
        /// This method returns corresponding cityId of the given city.
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <returns>int</returns>
        public static int GetCityIdFromCityName(string cityName)
        {
            //Trace.TraceInformation("HotelCity.GetCityIdFromCityName entered:" + cityName);
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@cityName", cityName);
            paramList[1]=new SqlParameter("@cityId",SqlDbType.Int);
            paramList[1].Direction=ParameterDirection.Output;
            int rowsAffected=DBGateway.ExecuteNonQuerySP(SPNames.GetCityIdFromCityName, paramList);
            //Trace.TraceInformation("HotelCity.GetCityIdFromCityName exited:" + cityName);
            if (paramList[1].Value != DBNull.Value)
            {
                return Convert.ToInt32(paramList[1].Value);
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// This method returns corresponding cityName of the given count.
        /// </summary>
        /// <param name="cityName">cityName</param>
        /// <param name="province">province</param>
        /// <returns>int</returns>
        public static int GetCityIdFromCityName(string cityName, string province)
        {
            //Trace.TraceInformation("HotelCity.GetCityIdFromCityName entered:" + cityName);
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@cityName", cityName);
            paramList[1] = new SqlParameter("@province", province);
            paramList[2] = new SqlParameter("@cityId", SqlDbType.Int);
            paramList[2].Direction = ParameterDirection.Output;
            
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.GetCityIdFromCityName, paramList);
            //Trace.TraceInformation("HotelCity.GetCityIdFromCityName exited:" + cityName);
            if (paramList[2].Value != DBNull.Value)
            {
                return Convert.ToInt32(paramList[2].Value);
            }
            else
            {
                return 0;
            }
        }    
    }
}



    
