﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CT.BookingEngine
{
    [Serializable]
    public class OfflineHotelRoom
    {
        int roomId;
        int hotelId;
        string roomTypeCode;
        string ratePlanCode;
        string ratePlanId;
        string noOfUnits;
        int adultCount;
        int childCount; 
        List<int> childAge; 
        string roomName;
        List<string> ameneties;
        int priceId;
        decimal extraGuestCharge;
        decimal childCharge;
        bool extraBed;
        int noOfExtraBed;
        int noOfCots;
        bool sharingBed;

        string mealPlanDesc = string.Empty;
        List<OfflinePaxInfo> paasengerInfo;
        
        int roomCapacityInfo;
        string essentialInformation;

        #region Specifically used for TBOHotel API
        string smokingPreference;
        int roomIndex;
        string supplements; 
        string bedTypeCode;
        int roomPaxCapacity;
        int allowedAdultsWithoutChildren;
        int allowedAdultsWithChildren;
        int maxExtraBed;
        RoomRateType rateType;
        int currencyId;
        string currencyCode;
        string description;
        bool nonRefundable;
        string notes;
        string paymentMode;
        bool allowExtraMeals;
        bool allowSpecialRequests;
        bool allowSpecials;
        int passengerNamesRequiredForBooking;
        bool ammendRestricted;
        bool cancelRestricted;
        bool noShowPolicy;
        bool includedAdditionalService;
        #endregion

        public int RoomCapacityInfo
        {
            get { return roomCapacityInfo; }
            set { roomCapacityInfo = value; }
        }

        /// <summary>
        ///  Maximum number of guests for this room (adults children) 
        /// </summary>
        public int RoomPaxCapacity
        {
            get { return roomPaxCapacity; }
            set { roomPaxCapacity = value; }
        }

        /// <summary>
        /// Maximum number of adults that can book this room without any children. 
        /// </summary>
        public int AllowedAdultsWithoutChildren
        {
            get { return allowedAdultsWithoutChildren; }
            set { allowedAdultsWithoutChildren = value; }
        }

        /// <summary>
        /// Maximum number of adults that can book this room with children. 
        /// The maximum number of children allowed to book the room would be 
        /// the difference between roomPaxCapacity and allowedAdultsWithChildren. 
        /// </summary>
        public int AllowedAdultsWithChildren
        {
            get { return allowedAdultsWithChildren; }
            set { allowedAdultsWithChildren = value; }
        }

        /// <summary>
        /// Specifies the maximum number of extra-beds that can be booked with this room type. 
        /// </summary>
        public int MaxExtraBed
        {
            get { return maxExtraBed; }
            set { maxExtraBed = value; }
        }

        /// <summary>
        /// Specifies the rate type. Possible values:
        /// 1 - DOTW rate type
        /// 2 - DYNAMIC DIRECT rate type
        /// 3 - DYNAMIC 3rd PARTY rate type 
        /// Please note that the blocking process guarantees the allotment 
        /// (holds inventory) only for DOTW rate types. 
        /// For the DYNAMIC rates, only the price is checked and guaranteed. 
        /// Sending the blocking request is however mandatory for both rate types. 
        /// </summary>
        public RoomRateType RateType
        {
            get { return rateType; }
            set { rateType = value; }
        }

        /// <summary>
        /// Internal code of the currency in which the prices for this rate are provided.
        /// ONLY For Non Refundable Advance Purchase Rates, 
        /// this can be a different currency than the one requested. 
        /// To see the prices in the requested currency you should look for the price elements 
        /// which clearly specify InRequestedCurrency 
        /// (totalInRequestedCurrency, 
        /// totalMinimumSellingInRequestedCurrency, 
        /// priceInRequestedCurrency, 
        /// priceMinimumSellingInRequestedCurrency) 
        /// </summary>
        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }

        /// <summary>
        /// Attribute in rateType element.  
        /// The 3 letter code which identifies the 
        /// currency in which the rates are provided. 
        /// </summary>
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        /// <summary>
        /// Attribute in rateType element.  
        /// A short text description of the rate, 
        /// indicating the type of rate 
        /// (whether it is DOTW, Dynamic or 3rd party). 
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// Attribute in ratetype element. 
        /// If present, indicates that the rate is 
        /// Non Refundable Advance Purchase.
        /// Possible values:  yes
        /// To be able to book this rate it is mandatory 
        /// for your application to use 
        /// the savebooking and bookitinerary booking flow. 
        /// </summary>
        public bool NonRefundable
        {
            get { return nonRefundable; }
            set { nonRefundable = value; }
        }

        /// <summary>
        ///  Attribute in rateType element. 
        ///  If present, contains a free flow text with 
        ///  additional notes about the rate, mainly
        /// </summary>
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        /// <summary>
        ///  When present, indicates that for this rate, a special mode of payment is required. 
        ///  If the value is CC this means the rate requires a credit card prepayment. 
        ///  Possible values:    CC - Credit Card
        ///  To be able to book this type of rate it is mandatory for your application
        ///  to use the savebooking and bookitinerary booking flow. 
        /// </summary>
        public string PaymentMode
        {
            get { return paymentMode; }
            set { paymentMode = value; }
        }

        /// <summary>
        ///  If present, this element specifies if the parent rate basis can be booked with extra meals.
        /// </summary>
        public bool AllowExtraMeals
        {
            get { return allowExtraMeals; }
            set { allowExtraMeals = value; }
        }

        /// <summary>
        ///  If present, this element specifies if for this rate, special requests can be sent to the supplier. 
        ///  All of the special requests are subject to availability at check in.
        /// </summary>
        public bool AllowSpecialRequests
        {
            get { return allowSpecialRequests; }
            set { allowSpecialRequests = value; }
        }

        /// <summary>
        /// Present only when the room type has a special offer. 
        /// Specifies if the room type special offer is also valid with this rate basis.
        /// </summary>
        public bool AllowSpecials
        {
            get { return allowSpecials; }
            set { allowSpecials = value; }
        }

        /// <summary>
        /// If present, specifies how many passenger names are required for completing the booking. 
        /// If missing, one leading passenger name is sufficient for the booking, the other passengers names being optional.
        /// </summary>
        public int PassengerNamesRequiredForBooking
        {
            get { return passengerNamesRequiredForBooking; }
            set { passengerNamesRequiredForBooking = value; }
        }

        /// <summary>
        /// If present, this element indicates that a future amendment 
        /// (using the updatebooking method) done in the time period 
        /// defined by this rule, will not be possible. Fixed Value=true.
        /// </summary>
        public bool AmmendRestricted
        {
            get { return ammendRestricted; }
            set { ammendRestricted = value; }
        }

        /// <summary>
        /// If present, this element indicates that a future cancel 
        /// (using the cancelbooking or deleteitinerary methods) 
        /// done in the time period defined by this rule, will not be possible.
        /// Fixed Value=true.
        /// </summary>
        public bool CancelRestricted
        {
            get { return cancelRestricted; }
            set { cancelRestricted = value; }
        }

        /// <summary>
        /// If present, this element indicates that the presented charge is a no show charge. 
        /// In this case the elements fromDate, fromDateDetails, toDate, toDateDetails, 
        /// amendingPossible will be absent. Fixed Value = true.
        /// </summary>
        public bool NoShowPolicy
        {
            get { return noShowPolicy; }
            set { noShowPolicy = value; }
        }

        public bool IncludedAdditionalService
        {
            get { return includedAdditionalService; }
            set { includedAdditionalService = value; }
        }

        public int RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }

        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }

        public string RoomTypeCode
        {
            get { return roomTypeCode; }
            set { roomTypeCode = value; }
        }

        public string RatePlanCode
        {
            get { return ratePlanCode; }
            set { ratePlanCode = value; }
        }
        public string RatePlanId
        {
            get { return ratePlanId; }
            set { ratePlanId = value; }
        }

        public string NoOfUnits
        {
            get { return noOfUnits; }
            set { noOfUnits = value; }
        }

        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }

        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }

        public List<int> ChildAge
        {
            get { return childAge; }
            set { childAge = value; }
        }

        public string RoomName
        {
            get { return roomName; }
            set { roomName = value; }
        }

        public List<string> Ameneties
        {
            get { return ameneties; }
            set { ameneties = value; }
        }

        public int PriceId
        {
            get { return priceId; }
            set { priceId = value; }
        }

        public decimal ExtraGuestCharge
        {
            get { return extraGuestCharge; }
            set { extraGuestCharge = value; }
        }
        public decimal ChildCharge
        {
            get { return childCharge; }
            set { childCharge = value; }
        }
        public bool ExtraBed
        {
            get { return extraBed; }
            set { extraBed = value; }
        }
        public int NoOfExtraBed
        {
            get { return noOfExtraBed; }
            set { noOfExtraBed = value; }
        }
        public int NoOfCots
        {
            get { return noOfCots; }
            set { noOfCots = value; }
        }
        public bool SharingBed
        {
            get { return sharingBed; }
            set { sharingBed = value; }
        }

        public List<OfflinePaxInfo> PassenegerInfo
        {
            get { return paasengerInfo; }
            set { paasengerInfo = value; }
        }


        public string MealPlanDesc
        {
            get { return mealPlanDesc; }
            set { mealPlanDesc = value; }
        }

        public string EssentialInformation
        {
            get { return essentialInformation; }
            set { essentialInformation = value; }
        }
        public string SmokingPreference
        {
            get { return smokingPreference; }
            set { smokingPreference = value; }
        }
        public int RoomIndex
        {
            get { return roomIndex; }
            set { roomIndex = value; }
        }
        public string BedTypeCode
        {
            get { return bedTypeCode; }
            set { bedTypeCode = value; }
        }
        public string Supplements
        {
            get { return supplements; }
            set { supplements = value; }
        }
        #region Method

        public int Save()
        {
            SqlParameter[] paramList = new SqlParameter[18];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            paramList[1] = new SqlParameter("@adultCount", adultCount);
            paramList[2] = new SqlParameter("@childCount", childCount);

            string childAgeString = string.Empty;
            if (childAge != null && childAge.Count > 0)
                childAge.ForEach(x => childAgeString += Convert.ToString(x) + "|");

            paramList[3] = new SqlParameter("@childAge", childAgeString.TrimEnd('|'));
            paramList[4] = new SqlParameter("@roomName", roomName);
            paramList[5] = new SqlParameter("@roomTypeCode", roomTypeCode);
            paramList[6] = new SqlParameter("@ratePlanCode", ratePlanCode);

            string amenetiesString = string.Empty;
            if (ameneties != null && ameneties.Count > 0)
                ameneties.ForEach(x => amenetiesString += Convert.ToString(x) + "|");

            paramList[7] = new SqlParameter("@amenities", amenetiesString);
            paramList[8] = new SqlParameter("@noOfUnit", noOfUnits);
            paramList[9] = new SqlParameter("@extraGuestCharge", extraGuestCharge);
            paramList[10] = new SqlParameter("@priceId", SqlDbType.Int);
            paramList[11] = new SqlParameter("@extraBed", extraBed);
            paramList[12] = new SqlParameter("@noOfCots", noOfCots);
            paramList[13] = new SqlParameter("@sharingBed", sharingBed);
            paramList[14] = new SqlParameter("@noOfExtraBed", noOfExtraBed == 0 && extraBed ? 1 : noOfExtraBed);
            paramList[15] = new SqlParameter("@childCharge", childCharge);
            paramList[16] = new SqlParameter("@MealPlanDesc", mealPlanDesc);
            paramList[17] = new SqlParameter("@roomId", SqlDbType.Int);
            paramList[17].Direction = ParameterDirection.Output;

            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddOfflineHotelRoom, paramList);
            roomId = (int)paramList[17].Value;
            //foreach (OfflinePaxInfo passInfo in paasengerInfo)
            //{
            //    passInfo.RoomId = roomId;
            //    passInfo.HotelId = hotelId;
            //    passInfo.Save();
            //}

            return roomId;
        }

        public OfflineHotelRoom[] Load(int OFLROOMId, int HTLOFLId, string sMode)
        {
            if (hotelId <= 0)
                throw new ArgumentException("HotelId Id should be positive integer", "hotelId");
            
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@OFLROOMId", OFLROOMId);
            paramList[1] = new SqlParameter("@HTLOFLId", HTLOFLId);
            paramList[2] = new SqlParameter("@Mode", sMode);

            List<OfflineHotelRoom> hotelRooms = new List<OfflineHotelRoom>();
            using (DataTable dtHotelRooms = DBGateway.FillDataTableSP(SPNames.GetOfflineHotelRoom, paramList))
            {
                if (dtHotelRooms != null && dtHotelRooms.Rows.Count > 0)
                {
                    foreach (DataRow data in dtHotelRooms.Rows)
                    {   
                        hotelRooms.Add(GetOfflineHTLRoom(data));
                    }
                }
            }

            return hotelRooms.ToArray();
        }

        public OfflineHotelRoom GetOfflineHTLRoom(DataRow data)
        {
            OfflineHotelRoom hroom = new OfflineHotelRoom();
            
            hroom.roomId = Convert.ToInt32(data["roomId"]);
            hroom.hotelId = Convert.ToInt32(data["hotelId"]);
            hroom.adultCount = Convert.ToInt16(data["adultCount"]);            
            hroom.ameneties = !string.IsNullOrEmpty(Convert.ToString(data["amenities"])) ? Convert.ToString(data["amenities"]).Split('|').ToList() : hroom.ameneties;
            hroom.childAge = string.IsNullOrEmpty(Convert.ToString(data["childAge"])) ? hroom.childAge :
                Convert.ToString(data["childAge"]).Split('|').ToList().Select(int.Parse).ToList();
            hroom.childCount = Convert.ToInt32(data["childCount"]);
            hroom.noOfUnits = Convert.ToString(data["noOfUnit"]);
            hroom.priceId = Convert.ToInt32(data["priceId"]);
            hroom.ratePlanCode = Convert.ToString(data["ratePlanCode"]);
            hroom.extraGuestCharge = Convert.ToDecimal(data["extraGuestCharge"]);
            hroom.roomName = data["roomName"].ToString();
            hroom.roomTypeCode = data["roomTypeCode"].ToString();
            hroom.extraBed = data["extraBed"] != DBNull.Value ? Convert.ToBoolean(data["extraBed"]) : hroom.extraBed;
            hroom.noOfExtraBed = !Convert.IsDBNull(data["noOfExtraBed"]) ? Convert.ToInt32(data["noOfExtraBed"]) : hroom.noOfExtraBed;
            hroom.noOfCots = data["noOfCots"] != DBNull.Value ? Convert.ToInt32(data["noOfCots"]) : hroom.noOfCots;
            hroom.sharingBed = data["sharingBed"] != DBNull.Value ? Convert.ToBoolean(data["sharingBed"]) : hroom.sharingBed;
            OfflinePaxInfo passInfo = new OfflinePaxInfo();
            hroom.paasengerInfo = passInfo.LoadOfflinePax(0, 0, hroom.hotelId, hroom.roomId, "HT");
            return hroom;
        }
        # endregion
    }

    /// <summary>
    /// This class is using to hold offline Hotel Itinerary data
    /// </summary>
    [Serializable]
    public class OfflineHotelItinerary : Product
    {
        # region private variables
        /// <summary>
        /// BookingConfirmationNo
        /// </summary>
        string confirmationNo;
        /// <summary>
        /// HotelId(unique table Id)
        /// </summary>
        int hotelId;
        /// <summary>
        /// HotelCode
        /// </summary>
        string hotelCode;
        /// <summary>
        /// HotelName
        /// </summary>
        string hotelName;
        /// <summary>
        /// chainCode
        /// </summary>
        string chainCode;
        /// <summary>
        /// hotelCategory
        /// </summary>
        string hotelCategory;
        /// <summary>
        /// HotelRating
        /// </summary>
        HotelRating rating;
        /// <summary>
        ///  roomtype
        /// </summary>
        OfflineHotelRoom[] roomtype;
        /// <summary>
        /// CheckInDate
        /// </summary>
        DateTime startDate;
        /// <summary>
        /// CheckOutDate
        /// </summary>
        DateTime endDate;
        /// <summary>
        /// hotelAddress1
        /// </summary>
        string hotelAddress1;
        /// <summary>
        /// hotelAddress2
        /// </summary>
        string hotelAddress2;
        /// <summary>
        /// cityCode
        /// </summary>
        string cityCode;
        /// <summary>
        /// cityRef
        /// </summary>
        string cityRef;
        /// <summary>
        /// specialRequest
        /// </summary>
        string specialRequest;
        /// <summary>
        /// status
        /// </summary>
        HotelBookingStatus status;
        /// <summary>
        /// hotelPolicyDetails
        /// </summary>
        string hotelPolicyDetails;
        /// <summary>
        /// hotelCancelPolicy
        /// </summary>
        string hotelCancelPolicy;
        /// <summary>
        /// noOfRooms
        /// </summary>
        int noOfRooms;
        /// <summary>
        /// createdBy
        /// </summary>
        int createdBy;
        /// <summary>
        /// source
        /// </summary>
        HotelBookingSource source;
        /// <summary>
        /// isDomestic
        /// </summary>
        private bool isDomestic;
        /// <summary>
        /// cancelId
        /// </summary>
        string cancelId;
        /// <summary>
        /// flightInfo
        /// </summary>
        string flightInfo;
        /// <summary>
        /// bookingRefNo
        /// </summary>
        string bookingRefNo;
        /// <summary>
        /// map
        /// </summary>
        string appartmentName;
        /// <summary>
        /// lastCancellationDate(Deadline date)
        /// </summary>
        DateTime lastCancellationDate;
        /// <summary>
        /// voucherStatus(true/false)
        /// </summary>
        bool voucherStatus;
        /// <summary>
        /// penalityInfo
        /// </summary>
        List<HotelPenality> penalityInfo;
        /// <summary>
        /// agencyReference(our unique Ref)
        /// </summary>
        string agencyReference;
        /// <summary>
        /// vatDescription
        /// </summary>
        string additionalinfo;
        /// <summary>
        /// ccInfo
        /// </summary>
        CreditCardInfo ccInfo;
        /// <summary>
        /// propertyType
        /// </summary>
        string propertyType;
        /// <summary>
        /// supplierType
        /// </summary>
        string supplierType;
        /// <summary>
        /// passengerNationality
        /// </summary>
        string passengerNationality;// For DOTW req objects
        /// <summary>
        /// passengerCountryOfResidence
        /// </summary>
        string passengerCountryOfResidence;
        /// <summary>
        /// paymentGuaranteedBy
        /// </summary>
        string paymentGuaranteedBy = string.Empty;
        /// <summary>
        /// Booking Currency
        /// </summary>
        string currency;
        /// <summary>
        /// Login locationId
        /// </summary>
        int locationId;
        /// <summary>
        /// Booing AgencyId
        /// </summary>
        int agencyId;
        /// <summary>
        /// transType(B2B/B2C) 
        /// </summary>
        string transType = "B2B";
        /// <summary>
        /// Specific for LOH used to store USD price
        /// </summary>
        decimal totalPrice;
        /// <summary>
        /// Specific for LOH used for booking Hotel
        /// </summary>
        string sequenceNumber;

        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;

        /// <summary>
        /// rezlive purpose
        /// </summary>
        string destinationCountryCode;

        ModeOfPayment paymentMode = ModeOfPayment.Credit; //Default should 2  means credit Added by brahmam 
        string _RequestType;
        private int _CorpTravelReasonId;
        string _Remarks;
        #endregion

        #region public properties

        /// <summary>
        /// ConfirmationNo
        /// </summary>
        public string ConfirmationNo
        {
            get { return confirmationNo; }
            set { confirmationNo = value; }
        }
        /// <summary>
        /// HotelId
        /// </summary>
        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }
        /// <summary>
        /// HotelCode
        /// </summary>
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }
        /// <summary>
        /// HotelName
        /// </summary>
        public string HotelName
        {
            get { return hotelName; }
            set { hotelName = value; }
        }
        /// <summary>
        /// ChainCode
        /// </summary>
        public string ChainCode
        {
            get { return chainCode; }
            set { chainCode = value; }
        }
        /// <summary>
        /// HotelCategory
        /// </summary>
        public string HotelCategory
        {
            get { return hotelCategory; }
            set { hotelCategory = value; }
        }
        /// <summary>
        /// Rating
        /// </summary>
        public HotelRating Rating
        {
            get { return rating; }
            set { rating = value; }
        }

        /// <summary>
        /// Roomtype
        /// </summary>
        public OfflineHotelRoom[] Roomtype
        {
            get { return roomtype; }
            set { roomtype = value; }
        }
        /// <summary>
        /// StartDate
        /// </summary>
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        /// <summary>
        /// EndDate
        /// </summary>
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        /// <summary>
        /// HotelAddress1
        /// </summary>
        public string HotelAddress1
        {
            get { return hotelAddress1; }
            set { hotelAddress1 = value; }
        }
        /// <summary>
        /// HotelAddress2
        /// </summary>
        public string HotelAddress2
        {
            get { return hotelAddress2; }
            set { hotelAddress2 = value; }
        }
        /// <summary>
        /// CityCode
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        /// <summary>
        /// CityRef
        /// </summary>
        public string CityRef
        {
            get { return cityRef; }
            set { cityRef = value; }
        }
        /// <summary>
        /// Status
        /// </summary>
        public HotelBookingStatus Status
        {
            get { return status; }
            set { status = value; }
        }
        /// <summary>
        /// SpecialRequest
        /// </summary>
        public string SpecialRequest
        {
            get { return specialRequest; }
            set { specialRequest = value; }
        }

        /// <summary>
        /// Hotel Policy details - will be added with some delimeter or new line.
        /// </summary>
        public string HotelPolicyDetails
        {
            get { return hotelPolicyDetails; }
            set { hotelPolicyDetails = value; }
        }
        /// <summary>
        /// HotelCancelPolicy
        /// </summary>
        public string HotelCancelPolicy
        {
            get { return hotelCancelPolicy; }
            set { hotelCancelPolicy = value; }
        }

        /// <summary>
        /// Total number of rooms requested
        /// </summary>
        public int NoOfRooms
        {
            get
            {
                return noOfRooms;
            }
            set
            {
                noOfRooms = value;
            }
        }
        /// <summary>
        /// CreatedBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        /// <summary>
        /// Source
        /// </summary>
        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        /// <summary>
        /// IsDomestic
        /// </summary>
        public bool IsDomestic
        {
            get { return isDomestic; }
            set { isDomestic = value; }
        }
        /// <summary>
        /// CancelId(if booking cancelled)
        /// </summary>
        public string CancelId
        {
            get { return cancelId; }
            set { cancelId = value; }
        }
        /// <summary>
        /// FlightInfo
        /// </summary>
        public string FlightInfo
        {
            get { return flightInfo; }
            set { flightInfo = value; }
        }
        /// <summary>
        /// BookingRefNo
        /// </summary>
        public string BookingRefNo
        {
            get { return bookingRefNo; }
            set { bookingRefNo = value; }
        }
        /// <summary>
        /// Map
        /// </summary>
        public string Additionalinfo
        {
            get { return additionalinfo; }
            set { additionalinfo = value; }
        }
        /// <summary>
        /// LastCancellationDate
        /// </summary>
        public DateTime LastCancellationDate
        {
            get { return lastCancellationDate; }
            set { lastCancellationDate = value; }
        }
        /// <summary>
        /// VoucherStatus
        /// </summary>
        public bool VoucherStatus
        {
            get { return voucherStatus; }
            set { voucherStatus = value; }
        }
        /// <summary>
        /// PenalityInfo
        /// </summary>
        public List<HotelPenality> PenalityInfo
        {
            get { return penalityInfo; }
            set { penalityInfo = value; }
        }
        /// <summary>
        /// AgencyReference
        /// </summary>
        public string AgencyReference
        {
            get { return agencyReference; }
            set { agencyReference = value; }
        }
        /// <summary>
        /// VatDescription
        /// </summary>
        public string AppartmentName
        {
            get { return appartmentName; }
            set { appartmentName = value; }
        }
        /// <summary>
        /// CCInfo
        /// </summary>
        public CreditCardInfo CCInfo
        {
            get { return ccInfo; }
            set { ccInfo = value; }
        }
        /// <summary>
        /// PropertyType
        /// </summary>
        public string PropertyType
        {
            get { return propertyType; }
            set { propertyType = value; }
        }
        /// <summary>
        /// SupplierType
        /// </summary>
        public string SupplierType
        {
            get { return supplierType; }
            set { supplierType = value; }
        }
        /// <summary>
        /// PassengerNationality
        /// </summary>
        public string PassengerNationality
        {
            get { return passengerNationality; }
            set { passengerNationality = value; }
        }
        /// <summary>
        /// PassengerCountryOfResidence
        /// </summary>
        public string PassengerCountryOfResidence
        {
            get { return passengerCountryOfResidence; }
            set { passengerCountryOfResidence = value; }
        }
        /// <summary>
        /// PaymentGuaranteedBy
        /// </summary>
        public string PaymentGuaranteedBy
        {
            get { return paymentGuaranteedBy; }
            set { paymentGuaranteedBy = value; }
        }
        /// <summary>
        /// Currency
        /// </summary>
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        /// <summary>
        /// LocationId
        /// </summary>
        public int LocationId
        {
            get { return locationId; }
            set { locationId = value; }
        }
        /// <summary>
        /// AgencyId
        /// </summary>
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }

        /// <summary>
        /// Specifically used for LOH for storing USD price.
        /// </summary>
        public decimal TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }
        /// <summary>
        /// Specifically used for LOH to send while booking a hotel.
        /// </summary>
        public string SequenceNumber
        {
            get { return sequenceNumber; }
            set { sequenceNumber = value; }
        }
        /// <summary>
        /// TransType
        /// </summary>
        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }

        //RezLive purpose
        /// <summary>
        /// DestinationCountryCode
        /// </summary>
        public string CountryCode
        {
            get { return destinationCountryCode; }
            set { destinationCountryCode = value; }
        }
        //Checking card or credit added by brahmam 15.09.2016
        /// <summary>
        /// PaymentMode
        /// </summary>
        public ModeOfPayment PaymentMode
        {
            get { return paymentMode; }
            set { paymentMode = value; }
        }

        /// <summary>
        /// Book request type, if new request or change request
        /// </summary>
        public string RequestType { get => _RequestType; set => _RequestType = value; }

        /// <summary>
        /// Corporate travel reason Id
        /// </summary>
        public int CorpTravelReasonId { get => _CorpTravelReasonId; set => _CorpTravelReasonId = value; }
        public string Remarks { get => _Remarks; set => _Remarks = value; }
        # endregion
        /// <summary>
        /// Default Constructor
        /// </summary>
        public OfflineHotelItinerary()
        {
            paymentMode = ModeOfPayment.Credit;
        }

        # region Methods
        /// <summary>
        ///  This method is to save the Record into HTL_OFFLINE_ITINERARY table in DB
        /// </summary>
        /// <param name="prod">Product</param>
        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[35];
                paramList[0] = new SqlParameter("@HTLOFLId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[1] = new SqlParameter("@BookingRefNo", bookingRefNo);
                //paramList[1].Direction = ParameterDirection.Output;
                paramList[2] = new SqlParameter("@confirmationNo", ConfirmationNo);
                paramList[3] = new SqlParameter("@starRating", Rating);
                paramList[4] = new SqlParameter("@cityRef", cityRef);
                paramList[5] = new SqlParameter("@hotelName", HotelName);
                paramList[6] = new SqlParameter("@hotelCode", HotelCode);
                paramList[7] = new SqlParameter("@addressLine1", HotelAddress1);
                paramList[8] = new SqlParameter("@addressLine2", HotelAddress2);
                paramList[9] = new SqlParameter("@checkInDate", StartDate);
                paramList[10] = new SqlParameter("@checkOutDate", EndDate);
                paramList[11] = new SqlParameter("@noOfRooms", NoOfRooms);
                paramList[12] = new SqlParameter("@status", Status);
                paramList[13] = new SqlParameter("@specialRequest", SpecialRequest);
                paramList[14] = new SqlParameter("@createdBy", CreatedBy);
                paramList[15] = new SqlParameter("@hotelPolicyDetails", HotelPolicyDetails);
                paramList[16] = new SqlParameter("@source", Source);
                paramList[17] = new SqlParameter("@isDomestic", isDomestic);
                paramList[18] = new SqlParameter("@hotelCancelPolicy", hotelCancelPolicy);
                paramList[19] = new SqlParameter("@cityCode", cityCode);
                paramList[20] = new SqlParameter("@flightInfo", flightInfo);
                paramList[21] = new SqlParameter("@AppartmentName", AppartmentName);
                paramList[22] = lastCancellationDate == DateTime.MinValue ? new SqlParameter("@lastCancellationDate", DBNull.Value) : new SqlParameter("@lastCancellationDate", lastCancellationDate);
                paramList[23] = new SqlParameter("@voucherStatus", voucherStatus);
                paramList[24] = new SqlParameter("@hotelCategory", hotelCategory);
                paramList[25] = new SqlParameter("@agencyReference", agencyReference);
                paramList[26] = new SqlParameter("@Additionalinfo", Additionalinfo);
                paramList[27] = new SqlParameter("@paymentGuaranteedBy", paymentGuaranteedBy);
                paramList[28] = new SqlParameter("@locationId", LocationId);
                paramList[29] = new SqlParameter("@agencyId", AgencyId);
                paramList[30] = new SqlParameter("@TransType", transType);
                paramList[31] = new SqlParameter("@RequestType", RequestType);
                paramList[32] = new SqlParameter("@CorpTravelReasonId", CorpTravelReasonId);
                paramList[33] = new SqlParameter("@country", CountryCode);
                paramList[34] = new SqlParameter("@Remarks", Remarks);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddOfflineHotelItinerary, paramList);
                hotelId = (int)paramList[0].Value;
                //BookingRefNo = (string)paramList[1].Value;

                // Saving rooms details
                for (int i = 0; i < Roomtype.Length; i++)
                {
                    Roomtype = Roomtype.Where(p => p.AdultCount > 0).ToArray();
                    if (!string.IsNullOrEmpty(Roomtype[i].RoomTypeCode))
                    {
                        Roomtype[i].HotelId = hotelId;
                        Roomtype[i].Save();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is to retrieve the result from DB corresponding to offline hotelId
        /// </summary>
        /// <param name="hotelId">hotelId</param>
        public void Load(int hotelId)
        {
            if (hotelId <= 0)
                throw new ArgumentException("Offline HotelId Id should be positive integer", "hotelId");

            this.hotelId = hotelId;
            
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            try
            {
                using (DataTable dtHotelItinerary = DBGateway.FillDataTableSP(SPNames.GetOfflineHotelItinerary, paramList))
                {
                    if (dtHotelItinerary != null && dtHotelItinerary.Rows.Count > 0)
                    {
                        DataRow data = dtHotelItinerary.Rows[0];
                        confirmationNo = Convert.ToString(data["confirmationNo"]);
                        rating = (HotelRating)(Convert.ToInt32(data["starRating"]));
                        cityCode = Convert.ToString(data["cityCode"]);
                        hotelName = Convert.ToString(data["hotelName"]);
                        hotelCode = Convert.ToString(data["hotelCode"]);
                        hotelAddress1 = Convert.ToString(data["addressLine1"]);
                        hotelAddress2 = Convert.ToString(data["addressLine2"]);
                        startDate = Convert.ToDateTime(data["checkInDate"]);
                        endDate = Convert.ToDateTime(data["checkOutDate"]);
                        noOfRooms = Convert.ToInt16(data["noOfRooms"]);
                        hotelPolicyDetails = Convert.ToString(data["hotelPolicyDetails"]);
                        status = (HotelBookingStatus)(Convert.ToInt16(data["status"]));
                        if (data["specialRequest"] != null)
                        {
                            specialRequest = Convert.ToString(data["specialRequest"]);
                        }
                        isDomestic = Convert.ToBoolean(data["isDomestic"]);
                        createdBy = Convert.ToInt32(data["createdBy"]);
                        createdOn = Convert.ToDateTime(data["createdOn"]);
                        lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                        lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                        source = (HotelBookingSource)(Convert.ToInt32(data["source"]));
                        hotelCancelPolicy = Convert.ToString(data["hotelCancelPolicy"]);
                        cityRef = Convert.ToString(data["cityRef"]);
                        cancelId = Convert.ToString(data["cancellationId"]);
                        flightInfo = Convert.ToString(data["flightInfo"]);
                        bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                        AppartmentName = Convert.ToString(data["map"]);
                        voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                        if (data["paymentGuaranteedBy"] != DBNull.Value)
                        {
                            paymentGuaranteedBy = Convert.ToString(data["paymentGuaranteedBy"]);
                        }
                        else
                        {
                            paymentGuaranteedBy = "";
                        }
                        if (data["lastCancellationDate"] != DBNull.Value)
                        {
                            lastCancellationDate = Convert.ToDateTime(data["lastCancellationDate"]);
                        }
                        if (!data["hotelCategory"].Equals(DBNull.Value))
                        {
                            hotelCategory = Convert.ToString(data["hotelCategory"]);
                        }
                        if (!data["agencyReference"].Equals(DBNull.Value))
                        {
                            agencyReference = Convert.ToString(data["agencyReference"]);
                        }
                        if (!data["vatDescription"].Equals(DBNull.Value))
                        {
                            Additionalinfo = Convert.ToString(data["vatDescription"]);
                        }
                        locationId = Convert.ToInt32(data["locationId"]);
                        agencyId = Convert.ToInt32(data["agencyId"]);
                        transType = Convert.ToString(data["TransType"]);
                        if (!data["paymentMode"].Equals(DBNull.Value))
                        {
                            paymentMode = (ModeOfPayment)Convert.ToInt32(data["paymentMode"]);
                        }
                        OfflineHotelRoom clsOfflineHotelRoom = new OfflineHotelRoom();
                        roomtype = clsOfflineHotelRoom.Load(0, hotelId, "HT");
                    }
                }
            }
            catch (Exception exp)
            {
                throw new ArgumentException("Offline Hotel id does not exist in database", exp);
            }
        }

        public DataSet GetBookDetails(string sRefKey, string sKeyType)
        {
            if (sRefKey.Length == 0)
                throw new ArgumentException("Offline HotelId ref key should not be empty", "HotelKey");

            DataSet dsDetails = new DataSet();

            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@RefKey", sRefKey);
            paramList[1] = new SqlParameter("@KeyType", sKeyType);
            try
            {
                dsDetails = DBGateway.ExecuteQuery(SPNames.GetOffHotelDetails, paramList);
            }
            catch (Exception exp)
            {
                throw new ArgumentException("Failed to get Offline Hotel details", exp);
            }
            return dsDetails;
        }

        public static DataTable GetOfflineHTLQueueList(DateTime StartDate, DateTime EndDate, int LocationId, 
            int AgentId, string AgentType, int loginProfile)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];

                paramList[0] = new SqlParameter("@StartDate", StartDate);
                paramList[1] = new SqlParameter("@EndDate", EndDate);
                paramList[2] = new SqlParameter("@LocationId", LocationId);
                paramList[3] = new SqlParameter("@AgentId", AgentId);
                paramList[4] = new SqlParameter("@AgentType", AgentType);
                paramList[5] = new SqlParameter("@LoginProfile", loginProfile);
                dt = DBGateway.FillDataTableSP("usp_Get_OfflineHTLQueue_list", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        #endregion
    }
}
