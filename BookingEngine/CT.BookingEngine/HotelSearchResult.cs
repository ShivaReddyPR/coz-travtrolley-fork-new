using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;

namespace CT.BookingEngine
{
    /// <summary>
    /// To get hotel room policies
    /// </summary>
    [Serializable]
    public class HotelRoomPolicies
    {
        public string RoomRefKey { get; set; }

        public List<string> CancelPolicyMessages { get; set; }

        public List<RoomPolicyDetails> PolicyInfo { get; set; }

        public List<HotelCancelPolicy> CancelPolicyDetails { get; set; }
    }
    /// <summary>
    /// To get multiple policy details
    /// </summary>
    [Serializable]
    public class RoomPolicyDetails
    {
        public string Name { get; set; }

        public string Message { get; set; }
    }
    /// <summary>
    /// struct for Type of HotelDetails
    /// </summary>
    [Serializable]
    public struct HotelDetails
    {
        /// <summary>
        /// HotelCode
        /// </summary>
        public string HotelCode;
        /// <summary>
        /// HotelName
        /// </summary>
        public string HotelName;
        /// <summary>
        /// Description
        /// </summary>
        public string Description;
        //public Dictionary<string, string> Rooms; // Keys will be RoomName, RoomTypeId and Description
        /// <summary>
        /// RoomData
        /// </summary>
        public Room[] RoomData;
        /// <summary>
        /// HotelFacilities
        /// </summary>
        public List<string> HotelFacilities;
        /// <summary>
        /// HotelPolicy
        /// </summary>
        public string HotelPolicy; // CheckOut time, check-in time
        /// <summary>
        /// RoomFacilities
        /// </summary>
        public List<string> RoomFacilities;
        /// <summary>
        /// Services
        /// </summary>
        public string Services;
        /// <summary>
        /// Attractions
        /// </summary>
        public Dictionary<string, string> Attractions; // key will be Description and Distance + Unit
        /// <summary>
        /// Image
        /// </summary>
        public string Image;
        /// <summary>
        /// Images
        /// </summary>
        public List<string> Images;
        /// <summary>
        /// Address
        /// </summary>
        public string Address;
        /// <summary>
        /// PinCode
        /// </summary>
        public string PinCode;
        /// <summary>
        /// CountryName
        /// </summary>
        public string CountryName;
        /// <summary>
        /// PhoneNumber
        /// </summary>
        public string PhoneNumber;
        /// <summary>
        /// Email
        /// </summary>
        public string Email;
        /// <summary>
        /// URL
        /// </summary>
        public string URL;
        /// <summary>
        /// FaxNumber
        /// </summary>
        public string FaxNumber;
        /// <summary>
        /// Map
        /// </summary>
        public string Map;
        /// <summary>
        /// hotelRating
        /// </summary>
        public HotelRating hotelRating;
        /// <summary>
        /// Location
        /// </summary>
        public List<string> Location;
        
    }
    /// <summary>
    /// struct for Type of Room
    /// </summary>
    [Serializable]
    public struct Room
    {
        /// <summary>
        /// RoomName
        /// </summary>
        public string RoomName;
        /// <summary>
        /// RoomTypeId
        /// </summary>
        public string RoomTypeId;
        /// <summary>
        /// Description
        /// </summary>
        public string Description;
        /// <summary>
        /// Images
        /// </summary>
        public List<string> Images;
    }
    /// <summary>
    /// struct for Type of HotelRoomsDetails
    /// </summary>
    [Serializable]
    public struct HotelRoomsDetails
    {
        /// <summary>
        /// RoomTypeCode
        /// </summary>
        public string RoomTypeCode;
        /// <summary>
        /// RoomTypeName
        /// </summary>
        public string RoomTypeName;
        /// <summary>
        /// RatePlanCode
        /// </summary>
        public string RatePlanCode;
        /// <summary>
        /// RatePlanId
        /// </summary>
        public string RatePlanId;
        /// <summary>
        /// CancellationPolicy
        /// </summary>
        public string CancellationPolicy;
        //Cancel Policy Updates
        /// <summary>
        /// CancelPolicyList
        /// </summary>
        public List<HotelCancelPolicy> CancelPolicyList;
        /// <summary>
        /// Rates
        /// </summary>
        public RoomRates[] Rates;
        /// <summary>
        /// SellExtraGuestCharges
        /// </summary>
        public decimal SellExtraGuestCharges;
        /// <summary>
        /// PubExtraGuestCharges
        /// </summary>
        public decimal PubExtraGuestCharges;
        /// <summary>
        /// TotalPrice
        /// </summary>
        public decimal TotalPrice; // sum of roomrates + totaltax
        /// <summary>
        /// TotalTax
        /// </summary>
        public decimal TotalTax;
        /// <summary>
        /// SellingFare
        /// </summary>
        public decimal SellingFare;
        /// <summary>
        /// Amenities
        /// </summary>
        public List<string> Amenities;
        /// <summary>
        /// Occupancy
        /// </summary>
        public Dictionary<string, int> Occupancy; // Keys are - MaxAdult, MaxChild, MaxInfant, MaxGuest, BaseAdult, BaseChild
        /// <summary>
        /// SequenceNo
        /// </summary>
        public string SequenceNo;//for which room it is associated 1|2|3 means this room type is for 1st ,2nd & 3rd room.
        /// <summary>
        /// Discount
        /// </summary>
        public decimal Discount;
        /// <summary>
        /// DiscountType
        /// </summary>
        public string DiscountType;
        /// <summary>
        /// DiscountValue
        /// </summary>
        public decimal DiscountValue;
        /// <summary>
        /// Markup
        /// </summary>
        public decimal Markup;
        /// <summary>
        /// MarkupType
        /// </summary>
        public string MarkupType;
        /// <summary>
        /// MarkupValue
        /// </summary>
        public decimal MarkupValue;
        /// <summary>
        /// ChildCharges
        /// </summary>
        public decimal ChildCharges;
        /// <summary>
        /// mealPlanDesc
        /// </summary>
        public string mealPlanDesc;
        /// <summary>
        /// PromoMessage
        /// </summary>
        public string PromoMessage;
        /// <summary>
        /// supplierPrice
        /// </summary>
        public decimal supplierPrice;
        /// <summary>
        /// MaxExtraBeds
        /// </summary>
        public int MaxExtraBeds;
        /// <summary>
        /// EssentialInformation
        /// </summary>
        public string EssentialInformation; //Specifically used for GTA API
        /// <summary>
        /// SharingBedding
        /// </summary>
        public bool SharingBedding;//Specifically used for GTA API
        /// <summary>
        /// NumberOfCots
        /// </summary>
        public int NumberOfCots;   //Specifically used for GTA API
        /// <summary>
        /// IsExtraBed
        /// </summary>
        public bool IsExtraBed;
        //Specied used for B2C purpose only 
        /// <summary>
        /// B2CMarkup
        /// </summary>
        public decimal B2CMarkup;
        /// <summary>
        /// B2CMarkupType
        /// </summary>
        public string B2CMarkupType;
        /// <summary>
        /// B2CMarkupValue
        /// </summary>
        public decimal B2CMarkupValue;
        /// <summary>
        /// Especially for DOTW rooms, some of the rate basis may come
        /// with this node value as 'true'. Then we need to warn the 
        /// user that this room type cannot be cancelled from date mentioned
        /// in the cancellation policy.
        /// </summary>
        /// <example><CancelRestricted>true</CancelRestricted></example>
        public bool CancelRestricted;

        #region Specifically used for TBOHotel & MIKI API
        /// <summary>
        /// TBOPrice
        /// </summary>
        public PriceAccounts TBOPrice;
        /// <summary>
        /// RoomIndex
        /// </summary>
        public int RoomIndex;
        /// <summary>
        /// BedTypeCode
        /// </summary>
        public string BedTypeCode;
        /// <summary>
        /// SmokingPreference
        /// </summary>
        public string SmokingPreference;
        /// <summary>
        /// Supplements
        /// </summary>
        public string Supplements;// Miki and TBO
        #endregion

        //VAT Calucation
        /// <summary>
        /// InputVATAmount
        /// </summary>
        public decimal InputVATAmount;
        /// <summary>
        /// TaxDetail
        /// </summary>
        public PriceTaxDetails TaxDetail;
        /// <summary>
        /// SurChargeList
        /// </summary>
        public List<Surcharges> SurChargeList;
        #region Gimmonix Room Images
        /// <summary>
        /// Images
        /// </summary>
        public List<string> Images;
        /// <summary>
        /// HotelPolicy Results
        /// </summary>
        public TravelPolicyResult TravelPolicyResult;
        /// <summary>
        /// To get next API call URL for UAPI hotel
        /// </summary>
        public string NextURL;
        /// <summary>
        /// To hold gurantee indicator for UAPI hotel
        /// </summary>
        public string GuaranteeTypeCode;
        #endregion
        /// <summary>
        /// TaxBreakups;
        /// </summary>
        public List<RoomTaxBreakup> TaxBreakups;
         ///<summary>
        /// SupplierName;
        /// </summary>
        public string SupplierName;
        ///<summary>
        /// SupplierId;
        /// </summary>
        public string SupplierId;
        ///<summary>
        /// CommonRoomKey;
        /// </summary>
        public string CommonRoomKey;
        ///<summary>
        /// IsIndividualSelection;
        /// </summary>
        public bool IsIndividualSelection;
          /// <summary>
        /// RoomConfirmNo
        /// </summary>
        public string RoomConfirmNo;
        /// <summary>
        /// RoomConfirmStatus
        /// </summary>
        public string RoomConfirmStatus;
        /// <summary>
        /// RoomCancelNo
        /// </summary>
        public string RoomCancelNo;
        /// <summary>
        /// RoomCancelStatus
        /// </summary>
        public string RoomCancelStatus;
        /// <summary>
        /// RoomRefKey
        /// </summary>
        public string roomRefKey;
        /// <summary>
        /// RoomGroupRefKey
        /// </summary>
        public string roomGroupRefKey;
        /// <summary>
        /// gxRoomId
        /// </summary>
        public string gxRoomId;
        /// <summary>
        /// gxPkgName
        /// </summary>
        public string gxPkgName;
        /// <summary>
        /// Room Policy details
        /// </summary>
        public HotelRoomPolicies roomPolicies;
        /// <summary>
        /// Especially for Illusion rooms, need to check dynamic source r not
        /// </summary>
        /// <example><DynamicYN>N</DynamicYN></example>
        public string DynamicInventory;
 ///<summary>
        /// AllocationType;
        /// </summary>
        public string AllocationType;
        ///<summary>
        /// SellType;
        /// </summary>
        public string SellType;
        ///<summary>
        /// RqId;
        /// </summary>
        public int RqId;
        /// <summary>
        /// Gross Fare from supplier
        /// </summary>
        public decimal PublishedFare;
        /// <summary>
        /// Marketing fee from supplier
        /// </summary>
        public decimal MarketingFee;
        /// <summary>
        /// Marketing fee discount percentage
        /// </summary>
        public decimal MFDiscountPercent;
        /// <summary>
        /// Marketing fee discount amount
        /// </summary>
        public decimal MFDiscountAmount;
    }
    /// <summary>
    /// struct for Type of RoomRates
    /// </summary>
    [Serializable]
    public struct RoomRates
    {
        //public WeekDays Days;
        /// <summary>
        /// Days
        /// </summary>
        public DateTime Days; // for 0 it will be first day, for 1 it will be second day and so on
        /// <summary>
        /// RateType
        /// </summary>
        public RateType RateType;
        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount;
        /// <summary>
        /// BaseFare
        /// </summary>
        public decimal BaseFare;
        /// <summary>
        /// Totalfare
        /// </summary>
        public decimal Totalfare;
        /// <summary>
        /// SellingFare
        /// </summary>
        public decimal SellingFare;
    }
    /// <summary>
    /// enum for Type of HotelRating
    /// </summary>
    [Serializable]
    public enum HotelRating
    {
        /// <summary>
        /// All
        /// </summary>
        All = 0,
        /// <summary>
        /// OneStar
        /// </summary>
        OneStar = 1,
        /// <summary>
        /// TwoStar
        /// </summary>
        TwoStar = 2,
        /// <summary>
        /// ThreeStar
        /// </summary>
        ThreeStar = 3,
        /// <summary>
        /// FourStar
        /// </summary>
        FourStar = 4,
        /// <summary>
        /// FiveStar
        /// </summary>
        FiveStar = 5

    }
    /// <summary>
    /// enum for Type of RateType
    /// </summary>
    [Serializable]
    public enum RateType
    {
        /// <summary>
        /// Published
        /// </summary>
        Published = 1, //SellRate == published
        /// <summary>
        /// Negotiated
        /// </summary>
        Negotiated = 2  // Negotiated == NetRate
    }

    /// <summary>
    /// Specifies which type of rates 
    /// (or suppliers - if the request is sent with noPrice set to true) 
    /// are taken into consideration by the system
    /// </summary>
    [Serializable]
    public enum RoomRateType
    {
        /// <summary>
        /// DOTW
        /// </summary>
        DOTW = 1,
        /// <summary>
        /// DYNAMIC_DIRECT
        /// </summary>
        DYNAMIC_DIRECT = 2,
        /// <summary>
        /// DYNAMIC_3rd_PARTY
        /// </summary>
        DYNAMIC_3rd_PARTY = 3 
    }
    /// <summary>
    /// enum for Type of RoomType
    /// </summary>
    [Serializable]
    public enum RoomType
    {
        /// <summary>
        /// Single
        /// </summary>
        Single = 1,
        /// <summary>
        /// Double
        /// </summary>
        Double = 2,
        /// <summary>
        /// Triple
        /// </summary>
        Triple = 3,
        /// <summary>
        /// Quad
        /// </summary>
        Quad = 4
    }
    /// <summary>
    /// enum for Type of AvailabilityType
    /// </summary>
    [Serializable]
    public enum AvailabilityType
    {
        /// <summary>
        /// OnRequest
        /// </summary>
        OnRequest = 1,
        /// <summary>
        /// Confirmed
        /// </summary>
        Confirmed = 2
    }

    [Serializable]
    public enum ResultCacheType
    {
        
        CoreHotelResult=1,
        CoreRoomDetails = 2,
        FlightQueryParams=3,
        BookingDetails=4
    }
    /// <summary>
    /// struct for Type of Surcharges
    /// </summary>
    //Agoda purpose only we are using
    [Serializable]
    public struct Surcharges
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id;
        /// <summary>
        /// Method
        /// </summary>
        public string Method;
        /// <summary>
        /// Charge
        /// </summary>
        public string Charge;
        /// <summary>
        /// Margin
        /// </summary>
        public string Margin;
        /// <summary>
        /// Name
        /// </summary>
        public string Name;
        /// <summary>
        /// Exclusive
        /// </summary>
        public decimal Exclusive;
        /// <summary>
        /// tax
        /// </summary>
        public decimal tax;
        /// <summary>
        /// Fees
        /// </summary>
        public decimal Fees;
        /// <summary>
        /// Inclusive
        /// </summary>
        public decimal Inclusive;
    }
    /// <summary>
    /// struct for Type of RoomTaxBreakup
    /// </summary>
    [Serializable]
    public struct RoomTaxBreakup
    {
        /// <summary>
        /// Currency
        /// </summary>
        public string Currency;
        /// <summary>
        /// TaxTitle
        /// </summary>
        public string TaxTitle;
        /// <summary>
        /// IsIncluded
        /// </summary>
        public bool? IsIncluded;
        /// <summary>
        /// UnitType
        /// </summary>
        public string UnitType;
        /// <summary>
        /// UnitType
        /// </summary>
        public decimal Value;
        /// <summary>
        /// IsIncluded
        /// </summary>
        public bool? IsMandatory;
        /// <summary>
        /// FrequencyType
        /// </summary>
        public string FrequencyType;


    }
        /// <summary>
        /// This class is using to save the HotelSearchResult in DB and retrive the HotelSearchResult from DB
        /// </summary>
        [Serializable]
    public class HotelSearchResult : IComparer<HotelSearchResult>
    {
        # region private variables
        /// <summary>
        /// rPH
        /// </summary>
        private int rPH; // re4lated key to related hotel - used for identifying fare and hotel relationship
        /// <summary>
        /// hotelCode
        /// </summary>
        private string hotelCode;
        /// <summary>
        /// cityCode
        /// </summary>
        private string cityCode;
        /// <summary>
        /// hotelName
        /// </summary>
        private string hotelName;
        /// <summary>
        /// hotelCategory
        /// </summary>
        private string hotelCategory;
        /// <summary>
        /// hotelPicture
        /// </summary>
        private string hotelPicture;
        /// <summary>
        /// hotelDescription
        /// </summary>
        private string hotelDescription;
        /// <summary>
        /// hotelMap
        /// </summary>
        private string hotelMap;
        /// <summary>
        /// hotelLocation
        /// </summary>
        private string hotelLocation;
        /// <summary>
        /// hotelAddress
        /// </summary>
        private string hotelAddress;
        /// <summary>
        /// hotelContactNo
        /// </summary>
        private string hotelContactNo;
        /// <summary>
        /// hotelFacilities
        /// </summary>
        private List<string> hotelFacilities;
        /// <summary>
        /// rating
        /// </summary>
        private HotelRating rating;
        /// <summary>
        /// startDate
        /// </summary>
        private DateTime startDate;
        /// <summary>
        /// endDate
        /// </summary>
        private DateTime endDate;
        /// <summary>
        /// rateType
        /// </summary>
        private RateType rateType;
        /// <summary>
        /// roomGuest
        /// </summary>
        private RoomGuestData[] roomGuest;
        /// <summary>
        /// roomDetails
        /// </summary>
        private HotelRoomsDetails[] roomDetails;
        /// <summary>
        /// totalPrice
        /// </summary>
        private decimal totalPrice;
        /// <summary>
        /// totalTax
        /// </summary>
        private decimal totalTax;
        /// <summary>
        /// sellingFare
        /// </summary>
        private decimal sellingFare;
        /// <summary>
        /// availType
        /// </summary>
        private AvailabilityType availType;
        /// <summary>
        /// bookingSource
        /// </summary>
        private HotelBookingSource bookingSource;
        /// <summary>
        /// price
        /// </summary>
        private PriceAccounts price;
        /// <summary>
        /// currency
        /// </summary>
        private string currency;
        /// <summary>
        /// propertyType
        /// </summary>
        private string propertyType;
        /// <summary>
        /// supplierType
        /// </summary>
        private string supplierType;
        /// <summary>
        /// promoMessage
        /// </summary>
        private string promoMessage = string.Empty;
        /// <summary>
        /// isFeaturedHotel
        /// </summary>
        private bool isFeaturedHotel;
        /// <summary>
        /// Specifically used for LOH API
        /// </summary>
        private string sequenceNumber;
        /// <summary>
        /// lastInActiveTime
        /// </summary>
        private DateTime lastInActiveTime;//time validation Booking requesttime

        /// <summary>
        /// lastInActiveTime
        /// </summary>
        private List<string> hotelImages;
        /// <summary>
        /// To store UAPI hotel next call URL
        /// </summary>
        private string _NextURL = string.Empty;
        /// <summary>
        /// To store  hotel source name
        /// </summary>
        private string sourceName = string.Empty;
        /// <summary>
        /// To store StaticID for avoid duplication Hotel 
        /// </summary>
        private string hotelStaticID;
        /// To store  RoomSelectionType either same selection or individual selection
        /// </summary>
        private bool isIndividualSelection;
        /// <summary>
        /// <summary>
        /// To store list of Suppliers(Having same hotel) 
        /// </summary>
        private List<SimilarHotelSourceInfo> similarHotels;
        /// <summary>
        /// To store hotelpolicydetails for avoid duplication Hotel 
        /// </summary>
        private string hotelPolicyDetails;
        /// <summary>
        /// To store result reference key 
        /// </summary>
        private string hotelRefKey;
        #endregion

        # region public properties
        /// <summary>
        /// RPH
        /// </summary>
        public int RPH
        {
            get
            {
                return rPH;
            }
            set
            {
                rPH = value;
            }
        }
        /// <summary>
        /// HotelCode
        /// </summary>
        public string HotelCode
        {
            get
            {
                return hotelCode;
            }
            set
            {
                hotelCode = value;
            }
        }
        /// <summary>
        /// RoomGuest
        /// </summary>
        public RoomGuestData[] RoomGuest
        {
            get
            {
                return roomGuest;
            }
            set
            {
                roomGuest = value;
            }
        }
        /// <summary>
        /// CityCode
        /// </summary>
        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }
        /// <summary>
        /// HotelName
        /// </summary>
        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }
        /// <summary>
        /// HotelCategory
        /// </summary>
        public string HotelCategory
        {
            get
            {
                return hotelCategory;
            }
            set
            {
                hotelCategory = value;
            }
        }
        /// <summary>
        /// HotelFacilities
        /// </summary>
        public List<string> HotelFacilities
        {
            get
            {
                return hotelFacilities;
            }
            set
            {
                hotelFacilities = value;
            }
        }
        /// <summary>
        /// HotelPicture
        /// </summary>
        public string HotelPicture
        {
            get
            {
                return hotelPicture;
            }
            set
            {
                hotelPicture = value;
            }
        }
        /// <summary>
        /// HotelDescription
        /// </summary>
        public string HotelDescription
        {
            get
            {
                return hotelDescription;
            }
            set
            {
                hotelDescription = value;
            }
        }
        /// <summary>
        /// HotelMap
        /// </summary>
        public string HotelMap
        {
            get
            {
                return hotelMap;
            }
            set
            {
                hotelMap = value;
            }
        }
        /// <summary>
        /// HotelLocation
        /// </summary>
        public string HotelLocation
        {
            get
            {
                return hotelLocation;
            }
            set
            {
                hotelLocation = value;
            }
        }
        /// <summary>
        /// HotelAddress
        /// </summary>
        public string HotelAddress
        {
            get
            {
                return hotelAddress;
            }
            set
            {
                hotelAddress = value;
            }
        }
        /// <summary>
        /// HotelContactNo
        /// </summary>
        public string HotelContactNo
        {
            get
            {
                return hotelContactNo;
            }
            set
            {
                hotelContactNo = value;
            }
        }
        /// <summary>
        /// Rating
        /// </summary>
        public HotelRating Rating
        {
            get
            {
                return rating;
            }
            set
            {
                rating = value;
            }
        }
        /// <summary>
        /// StartDate
        /// </summary>
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        /// <summary>
        /// EndDate
        /// </summary>
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        /// <summary>
        /// RateType
        /// </summary>
        public RateType RateType
        {
            get { return rateType; }
            set { rateType = value; }
        }
        /// <summary>
        /// RoomDetails
        /// </summary>
        public HotelRoomsDetails[] RoomDetails
        {
            get { return roomDetails; }
            set { roomDetails = value; }
        }
        /// <summary>
        /// TotalPrice
        /// </summary>
        public decimal TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }
        /// <summary>
        /// TotalTax
        /// </summary>
        public decimal TotalTax
        {
            get { return totalTax; }
            set { totalTax = value; }
        }
        /// <summary>
        /// SellingFare
        /// </summary>
        public decimal SellingFare
        {
            get { return sellingFare; }
            set { sellingFare = value; }
        }
        /// <summary>
        /// AvailType
        /// </summary>
        public AvailabilityType AvailType
        {
            get { return availType; }
            set { availType = value; }
        }
        /// <summary>
        /// BookingSource
        /// </summary>
        public HotelBookingSource BookingSource
        {
            get { return bookingSource; }
            set { bookingSource = value; }
        }
        /// <summary>
        /// Price
        /// </summary>
        public PriceAccounts Price
        {
            get { return price; }
            set { price = value; }
        }
        /// <summary>
        /// Currency
        /// </summary>
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        /// <summary>
        /// PropertyType
        /// </summary>
        public string PropertyType
        {
            get { return propertyType; }
            set { propertyType = value; }
        }
        /// <summary>
        /// SupplierType
        /// </summary>
        public string SupplierType
        {
            get { return supplierType; }
            set { supplierType = value; }
        }
        /// <summary>
        /// PromoMessage
        /// </summary>
        public string PromoMessage
        {
            get { return promoMessage; }
            set { promoMessage = value; }
        }
        /// <summary>
        /// IsFeaturedHotel
        /// </summary>
        public bool IsFeaturedHotel
        {
            get { return isFeaturedHotel; }
            set { isFeaturedHotel = value; }
        }
        /// <summary>
        /// Used specifically for LOH API in Hotel Booking.
        /// </summary>
        public string SequenceNumber
        {
            get { return sequenceNumber; }
            set { sequenceNumber = value; }
        }
        /// <summary>
        /// LastInActivieTime
        /// </summary>
        public DateTime LastInActivieTime
        {
            get { return lastInActiveTime; }
            set { lastInActiveTime = value; }
        }
        /// <summary>
        /// HotelImages
        /// </summary>
        public List<string> HotelImages
        {
            get { return hotelImages; }
            set { hotelImages = value; }
        }
        /// <summary>
        /// To store UAPI hotel Token
        /// </summary>
        public string NextURL { get => _NextURL; set => _NextURL = value; }
        /// <summary>
        /// To store HotelSource Name
        /// </summary>
        public string SourceName { get => sourceName; set => sourceName = value; }
        /// <summary>
        /// To hold hotel booking seesion id 
        /// </summary>
        public string ApiSessionId { get; set; }
        /// <summary>
        /// To hold transaction type B2C/B2B
        /// </summary>
        public string Transtype { get; set; }
        /// <summary>
        /// To hold hotel region 
        /// </summary>
        public string HotelRegion { get; set; }
        /// <summary>
        /// To store HotelStaticId for Avoid Duplicate Hotels
        /// </summary>
        public string HotelStaticID { get => hotelStaticID; set => hotelStaticID = value; }
        /// <summary>
        /// To store HotelSelectionType either same selection or individual selection
        /// </summary>
        public bool IsIndividualSelection { get => isIndividualSelection; set => isIndividualSelection = value; }
        public List<SimilarHotelSourceInfo> SimilarHotels { get => similarHotels; set => similarHotels = value; }
        /// <summary>
        /// To store HotelPolicy details
        /// </summary>
        public string HotelPolicyDetails { get => hotelPolicyDetails; set => hotelPolicyDetails = value; }
        /// <summary>
        /// To store result reference key
        /// </summary>
        public string HotelRefKey { get => hotelRefKey; set => hotelRefKey = value; }
        /// <summary>
        /// To store discount value
        /// </summary>
        public decimal Discount { get; set; }

        #endregion

        #region METHODS

        /// <summary>
        /// Save the search result into DB for the given session
        /// </summary>
        /// <param name="sessionId">sessionId</param>
        /// <param name="searchResult">searchResult</param>
        /// <param name="noOfRooms">noOfRooms</param>
        /// <param name="request">request</param>
        public void Save(string sessionId, HotelSearchResult[] searchResult, int noOfRooms, HotelRequest request, ResultCacheType resultType)
        {
            //Trace.TraceInformation("HotelSearchResult[].Save entered : sessionId = " + sessionId);
            if (sessionId == null)
            {
                throw new ArgumentException("The current session expired");
            }
            //order the results on the selling price

            if (searchResult.Length > 0)
            {
                //Array.Sort(searchResult, searchResult[0]);
                Array.Sort(searchResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
            }
            //SortResultsByPrice(ref searchResult, noOfRooms, request);
            //searchResult = FilterByTBOConnect(searchResult);
            if (searchResult != null && searchResult.Length > 0)
            {
                byte[] data = GetByteArrayWithObject(searchResult);
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@sessionId", sessionId);
                paramList[1] = new SqlParameter("@resultData", data);
                paramList[2] = new SqlParameter("@ResulType", resultType.ToString());

                DBGateway.ExecuteNonQuerySP(SPNames.AddHotelSearchResultCache, paramList);
            }  // ELSE no data to save.

            //Trace.TraceInformation("HotelSearchResult.Save exited : sessionId = " + sessionId);
        }
        /// <summary>
        /// Save the TBOConnect results into database
        /// </summary>
        /// <param name="searchResult">HotelSearchResult object</param>
        /// <param name="cityName">City Name</param>
        public void Save(HotelSearchResult[] searchResult, string cityName)
        {
            //Trace.TraceInformation("HotelSearchResult.Save entered:city=" + cityName);
            List<HotelSearchResult> lstResults = new List<HotelSearchResult>();
            //Check for search result exists or not
            if (searchResult != null && searchResult.Length > 0)
            {
                foreach (HotelSearchResult result in searchResult)
                {
                    if (result.IsFeaturedHotel)
                    {
                        lstResults.Add(result);
                    }
                }

                decimal minutes = Convert.ToDecimal(CT.Configuration.ConfigurationSystem.HotelConnectConfig["cacheTimeinMinutes"]);
                byte[] data = GetByteArrayWithObject(lstResults.ToArray());
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@cityName", cityName);
                paramList[1] = new SqlParameter("@resultData", data);
                paramList[2] = new SqlParameter("@timeToExpire", minutes);
                if (lstResults.Count > 0)
                {
                    DBGateway.ExecuteNonQuerySP(SPNames.AddTBOConnectSearchResultCache, paramList);
                }
            }
            //Trace.TraceInformation("HotelSearchResult.Save exited:city=" + cityName);
        }
        /// <summary>
        /// Loads the tboconnect results from the db for displaying on the e-ticket
        /// </summary>
        /// <param name="cityName">City Name</param>
        /// <returns>HotelSearchResult[]</returns>
        public HotelSearchResult[] LoadTBOResults(string cityName)
        {
            //Trace.TraceInformation("HotelSearchResult.Load entered:cityName=" + cityName);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityName", cityName);
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader reader = DBGateway.ExecuteReaderSP(SPNames.GetTBOConnectSearchResultCache, paramList,con);
            byte[] data = new byte[0];
            HotelSearchResult[] searchResultCache = new HotelSearchResult[0];
            if (reader.Read())
            {
                data = (byte[])reader["resultData"];
            }
            reader.Close();
            con.Close();
            if (data.Length > 0)
            {
                searchResultCache = (HotelSearchResult[])GetObjectWithByteArray(data);
            }
            //Trace.TraceInformation("HotelSearchResult.Load exited:cityName=" + cityName);
            return searchResultCache;
        }
        /// <summary>
        /// sorting by price 
        /// </summary>
        /// <param name="finalResult">finalResult</param>
        /// <param name="noOfRooms">noOfRooms</param>
        /// <param name="request">request</param>
        private void SortResultsByPrice(ref HotelSearchResult[] finalResult, int noOfRooms, HotelRequest request)
        {
            //For checking whether all rooms have equal pax or not to use RezLive, HotelConnect
           
            int adults = request.RoomGuest[0].noOfAdults;
           
            //since all the sources does not give price sorted results
            //rate of exchange to compare the price in INR
            CT.Core.StaticData staticInfo = new CT.Core.StaticData();
            Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            for (int n = 0; n < finalResult.Length - 1; n++)
            {
                for (int m = n + 1; m < finalResult.Length; m++)
                {
                    decimal exRateN = 1, exRateM = 1;
                    if (rateOfEx.ContainsKey(finalResult[n].Currency))
                    {
                        exRateN = rateOfEx[finalResult[n].Currency];
                    }
                    if (rateOfEx.ContainsKey(finalResult[m].Currency))
                    {
                        exRateM = rateOfEx[finalResult[m].Currency];
                    }
                    decimal priceN = 0, priceM = 0;
                    if (noOfRooms > 1)
                    {
                        //decimal price = 0;
                        //decimal sellPrice = 0;
                        decimal tempPrice = 0;
                        if (finalResult[n].BookingSource == HotelBookingSource.Desiya)
                        {
                            CT.BookingEngine.HotelRoomsDetails roomInfo = finalResult[n].RoomDetails[0];
                            priceN = roomInfo.SellingFare + roomInfo.TotalTax + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length;
                            priceN *= exRateN;
                        }
                        else if (finalResult[n].BookingSource == HotelBookingSource.IAN)
                        {
                            CT.BookingEngine.HotelRoomsDetails roomInfo = finalResult[n].RoomDetails[0];
                            //price = Math.Round((roomInfo.TotalPrice + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length), 2);
                            priceN = Math.Round((roomInfo.SellingFare + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                            priceN *= exRateN;
                        }
                        else
                        {
                            CT.BookingEngine.HotelRoomsDetails[] roomInfo = finalResult[n].RoomDetails;
                            for (int x = 0; x < noOfRooms; x++)
                            {
                                for (int y = 0; y < finalResult[n].RoomDetails.Length; y++)
                                {
                                    tempPrice = 0;
                                    if (finalResult[n].BookingSource == HotelBookingSource.DOTW)
                                    {
                                        if ((finalResult[n].RoomDetails[y].SequenceNo != null && finalResult[n].RoomDetails[y].SequenceNo.Contains((x + 1).ToString())))
                                        {
                                            if (finalResult[n].BookingSource != HotelBookingSource.TBOConnect)
                                            {
                                                priceN += finalResult[n].RoomDetails[y].SellingFare + (finalResult[n].RoomDetails[y].SellExtraGuestCharges * finalResult[n].RoomDetails[y].Rates.Length) + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                            }
                                            else
                                            {
                                                tempPrice += finalResult[n].RoomDetails[y].SellingFare + finalResult[n].RoomDetails[y].SellExtraGuestCharges + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                                //tempPrice += tempPrice * finalResult[n].Price.AgentCommission / 100;
                                                priceN += tempPrice;
                                            }
                                            break;
                                        }
                                    }
                                    else if (finalResult[n].BookingSource == HotelBookingSource.RezLive)
                                    {
                                        if (finalResult[n].RoomDetails[0].SequenceNo.Contains("|"))
                                        {
                                            if ((finalResult[n].RoomDetails[0].SequenceNo != null && finalResult[n].RoomDetails[0].SequenceNo.Split('|')[x] == request.RoomGuest[x].noOfAdults.ToString()))
                                            {
                                                //if (finalResult[n].RoomDetails[0].RoomTypeName.Contains("|"))
                                                //{
                                                //    string[] names = finalResult[n].RoomDetails[0].RoomTypeName.Split('|');
                                                //    priceN += (finalResult[n].RoomDetails[0].TotalPrice / names.Length) + (finalResult[n].RoomDetails[y].SellExtraGuestCharges) + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                                //}
                                                //else
                                                {
                                                    priceN = finalResult[n].RoomDetails[0].SellingFare + (finalResult[n].RoomDetails[y].SellExtraGuestCharges ) + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ((finalResult[n].RoomDetails[0].SequenceNo != null && finalResult[n].RoomDetails[0].SequenceNo == request.RoomGuest[x].noOfAdults.ToString()))
                                            {
                                                priceN = finalResult[n].RoomDetails[0].SellingFare + (finalResult[n].RoomDetails[y].SellExtraGuestCharges) + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                            }
                                        }
                                    }
                                    else if (finalResult[n].BookingSource == HotelBookingSource.LOH)
                                    {
                                        if (finalResult[n].RoomDetails[0].SequenceNo != null && finalResult[n].RoomDetails[0].SequenceNo.Split(',').Length > 1)
                                        {
                                            //if (y + 1 > finalResult[n].RoomDetails[0].SequenceNo.Split(',').Length)
                                            //{
                                            //    break;
                                            //}
                                            //for (int k = 0; k < finalResult[n].RoomDetails[0].SequenceNo.Split(',').Length; k++)
                                            {
                                                if (finalResult[n].RoomDetails[0].SequenceNo != null && Convert.ToInt32(finalResult[n].RoomDetails[0].SequenceNo.Split(',')[x]) == (x + 1))
                                                {
                                                    priceN = (finalResult[n].SellingFare ) + (finalResult[n].RoomDetails[y].SellExtraGuestCharges) + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ((finalResult[n].RoomDetails[0].SequenceNo != null && finalResult[n].RoomDetails[0].SequenceNo == (x + 1).ToString()))
                                            {
                                                priceN = finalResult[n].SellingFare + (finalResult[n].RoomDetails[y].SellExtraGuestCharges) + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                            }
                                        }
                                    }
                                }
                            }
                            priceN *= exRateN;
                        }

                        if (finalResult[m].BookingSource == HotelBookingSource.Desiya)
                        {
                            CT.BookingEngine.HotelRoomsDetails roomInfo = finalResult[m].RoomDetails[0];
                            priceM = roomInfo.SellingFare + roomInfo.TotalTax + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length;
                            priceM *= exRateM;
                        }
                        else if (finalResult[m].BookingSource == HotelBookingSource.IAN)
                        {
                            CT.BookingEngine.HotelRoomsDetails roomInfo = finalResult[m].RoomDetails[0];
                            //price = Math.Round((roomInfo.TotalPrice + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length), 2);
                            priceM = Math.Round((roomInfo.SellingFare + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                            priceM *= exRateM;
                        }
                        else
                        {
                            CT.BookingEngine.HotelRoomsDetails[] roomInfo = finalResult[m].RoomDetails;
                            for (int x = 0; x < noOfRooms; x++)
                            {
                                for (int y = 0; y < finalResult[m].RoomDetails.Length; y++)
                                {
                                    tempPrice = 0;
                                    if (finalResult[m].BookingSource == HotelBookingSource.DOTW)
                                    {
                                        if ((finalResult[m].RoomDetails[y].SequenceNo != null && finalResult[m].RoomDetails[y].SequenceNo.Contains((x + 1).ToString())))
                                        {
                                            if (finalResult[m].BookingSource != HotelBookingSource.TBOConnect)
                                            {
                                                priceM += finalResult[m].RoomDetails[y].SellingFare + (finalResult[m].RoomDetails[y].SellExtraGuestCharges * finalResult[m].RoomDetails[y].Rates.Length) + finalResult[m].RoomDetails[y].TotalTax - finalResult[m].RoomDetails[y].Discount;
                                            }
                                            else
                                            {
                                                tempPrice += finalResult[m].RoomDetails[y].SellingFare + finalResult[m].RoomDetails[y].SellExtraGuestCharges + finalResult[m].RoomDetails[y].TotalTax - finalResult[m].RoomDetails[y].Discount;
                                                //tempPrice += tempPrice * finalResult[n].Price.AgentCommission / 100;
                                                priceM += tempPrice;
                                            }
                                            break;
                                        }
                                    }
                                    else if (finalResult[m].BookingSource == HotelBookingSource.RezLive)
                                    {
                                        if (finalResult[m].RoomDetails[0].SequenceNo.Contains("|"))
                                        {
                                            if ((finalResult[m].RoomDetails[0].SequenceNo != null && finalResult[m].RoomDetails[0].SequenceNo.Split('|')[x] == request.RoomGuest[x].noOfAdults.ToString()))
                                            {
                                                //if (finalResult[m].RoomDetails[y].RoomTypeName.Contains("|"))
                                                //{
                                                //    string[] names = finalResult[m].RoomDetails[y].RoomTypeName.Split('|');
                                                //    priceM += (finalResult[m].RoomDetails[0].TotalPrice / names.Length) + (finalResult[m].RoomDetails[y].SellExtraGuestCharges) + finalResult[m].RoomDetails[y].TotalTax - finalResult[m].RoomDetails[y].Discount;
                                                //}
                                                //else
                                                {
                                                    priceM = finalResult[m].RoomDetails[0].SellingFare + (finalResult[m].RoomDetails[y].SellExtraGuestCharges) + finalResult[m].RoomDetails[y].TotalTax - finalResult[m].RoomDetails[y].Discount;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ((finalResult[m].RoomDetails[0].SequenceNo != null && finalResult[m].RoomDetails[0].SequenceNo == request.RoomGuest[x].noOfAdults.ToString()))
                                            {
                                                priceM = finalResult[m].RoomDetails[0].SellingFare + (finalResult[m].RoomDetails[y].SellExtraGuestCharges) + finalResult[m].RoomDetails[y].TotalTax - finalResult[m].RoomDetails[y].Discount;
                                            }
                                        }
                                    }
                                    else if (finalResult[m].BookingSource == HotelBookingSource.LOH)
                                    {
                                        if (finalResult[m].RoomDetails[0].SequenceNo != null && finalResult[m].RoomDetails[0].SequenceNo.Split(',').Length > 1)
                                        {
                                            //if (y + 1 > finalResult[m].RoomDetails[0].SequenceNo.Split(',').Length)
                                            //{
                                            //    break;
                                            //}
                                            //for (int k = 0; k < finalResult[n].RoomDetails[0].SequenceNo.Split(',').Length; k++)
                                            {
                                                if (finalResult[m].RoomDetails[0].SequenceNo != null && Convert.ToInt32(finalResult[m].RoomDetails[0].SequenceNo.Split(',')[x]) == (x + 1))
                                                {
                                                    priceM = (finalResult[m].SellingFare ) + finalResult[m].RoomDetails[y].TotalTax;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ((finalResult[m].RoomDetails[0].SequenceNo != null && finalResult[m].RoomDetails[0].SequenceNo == (x + 1).ToString()))
                                            {
                                                priceM = finalResult[m].SellingFare + finalResult[m].RoomDetails[y].TotalTax;
                                            }
                                        }
                                    }
                                }
                            }
                            priceM *= exRateM;
                        }
                    }
                    else
                    {
                        if (finalResult[n].bookingSource == HotelBookingSource.IAN)
                        {
                            priceN = (finalResult[n].RoomDetails[0].SellingFare) * exRateN;
                            //priceN /= noOfRooms;
                        }
                        else if (finalResult[n].bookingSource == HotelBookingSource.TBOConnect)
                        {
                            priceN = (finalResult[n].RoomDetails[0].SellingFare + finalResult[n].RoomDetails[0].TotalTax - finalResult[n].RoomDetails[0].Discount + finalResult[n].RoomDetails[0].SellExtraGuestCharges) * exRateN;
                        }
                        else
                        {
                            priceN = (finalResult[n].RoomDetails[0].SellingFare + finalResult[n].RoomDetails[0].TotalTax + finalResult[n].RoomDetails[0].SellExtraGuestCharges * finalResult[n].RoomDetails[0].Rates.Length) * exRateN;
                        }
                        if (finalResult[m].bookingSource == HotelBookingSource.IAN)
                        {
                            priceM = (finalResult[m].RoomDetails[0].SellingFare) * exRateM;
                            //priceM /= noOfRooms;
                        }
                        else if (finalResult[m].bookingSource == HotelBookingSource.TBOConnect)
                        {
                            priceM = (finalResult[m].RoomDetails[0].SellingFare + finalResult[m].RoomDetails[0].TotalTax - finalResult[m].RoomDetails[0].Discount + finalResult[m].RoomDetails[0].SellExtraGuestCharges) * exRateN;
                        }
                        else if (finalResult[m].bookingSource == HotelBookingSource.RezLive)
                        {
                            priceM = (finalResult[m].RoomDetails[0].SellingFare + finalResult[m].RoomDetails[0].TotalTax - finalResult[m].RoomDetails[0].Discount + finalResult[m].RoomDetails[0].SellExtraGuestCharges) * exRateN;
                        }
                        else if (finalResult[m].bookingSource == HotelBookingSource.LOH)
                        {
                            priceM = (finalResult[m].RoomDetails[0].SellingFare + finalResult[m].RoomDetails[0].TotalTax - finalResult[m].RoomDetails[0].Discount + finalResult[m].RoomDetails[0].SellExtraGuestCharges) * exRateN;
                        }
                        else
                        {
                            priceM = (finalResult[m].RoomDetails[0].SellingFare + finalResult[m].RoomDetails[0].TotalTax + finalResult[m].RoomDetails[0].SellExtraGuestCharges * finalResult[m].RoomDetails[0].Rates.Length) * exRateM;
                        }
                    }
                    if (priceN > priceM)
                    {
                        HotelSearchResult temp = finalResult[n];
                        finalResult[n] = finalResult[m];
                        finalResult[m] = temp;
                    }
                }
            }
        }

        /// <summary>
        /// Load the data from DB for the given session
        /// </summary>
        /// <param name="sessionId">sessionId</param>
        /// <returns>HotelSearchResult[]</returns>
        public HotelSearchResult[] Load(string sessionId)
        {
            ////Trace.TraceInformation("HotelSearchResult.Load entered : sessionId = " + sessionId);
            if (sessionId == null)
            {
                throw new ArgumentException("The current session expired");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sessionId", sessionId);

            //SqlConnection connection = DBGateway.GetConnection();
            //SqlDataReader dReader = DBGateway.ExecuteReaderSP(SPNames.GetHotelSearchResultCache, paramList,connection);

            byte[] data = new byte[0];
            using (DataTable dtHotelSearchCache = DBGateway.FillDataTableSP(SPNames.GetHotelSearchResultCache, paramList))
            {
                if (dtHotelSearchCache != null && dtHotelSearchCache.Rows.Count > 0)
                {
                    DataRow dr = dtHotelSearchCache.Rows[0];
                    if (dr["resultData"] !=DBNull.Value)
                    {
                        //if (dReader.Read())
                        //{
                        data = (byte[])dr["resultData"];
                        //}
                    }
                }
            }
            //dReader.Close();
            //connection.Close();
            HotelSearchResult[] searchResultCache = new HotelSearchResult[0];
            if (data.Length > 0)
            {
                searchResultCache = (HotelSearchResult[])GetObjectWithByteArray(data);
            }
            return searchResultCache;
        }

        /// <summary>
        /// Over load method to get hotel result using session and result type
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="resultType"></param>
        /// <returns></returns>
        public HotelSearchResult[] Load(string sessionId, ResultCacheType resultCacheType)
        {
            if (string.IsNullOrEmpty(sessionId))
                throw new ArgumentException("The current session expired");

            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@sessionId", sessionId));
            paramList.Add(new SqlParameter("@ResulType", resultCacheType.ToString()));

            byte[] data = new byte[0];
            using (DataTable dt = DBGateway.FillDataTableSP(SPNames.GetHotelSearchResultCache, paramList.ToArray()))
            {
                if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["resultData"] != DBNull.Value)
                    data = (byte[])dt.Rows[0]["resultData"];
            }

            HotelSearchResult[] searchResultCache = new HotelSearchResult[0];

            if (data.Length > 0)
                searchResultCache = (HotelSearchResult[])GetObjectWithByteArray(data);

            return searchResultCache;
        }

        /// <summary>
        /// Return the HotelResult for the sessionId on selected filter criteria
        /// </summary>
        /// <param name="sessionId">sessionId</param>
        /// <param name="filterCriteria">filterCriteria</param>
        /// <param name="noOfPages">noOfPages</param>
        /// <returns>HotelSearchResult[]</returns>
        public HotelSearchResult[] GetFilteredResult(string sessionId, Dictionary<string, string> filterCriteria, ref int noOfPages)
        {
            HotelSearchResult[] noResult = new HotelSearchResult[0];
            if (string.IsNullOrEmpty(sessionId))
            {
                return noResult;
            }
            int recordsPerPage;

            if (filterCriteria.ContainsKey("recordsPerPage"))
            {
                recordsPerPage = Convert.ToInt32(filterCriteria["recordsPerPage"]);
            }
            else
            {
                throw new ArgumentException("Incomplete filter criteria. Need value of recordsPerPage.");
            }
            HotelSearchResult[] searchResultCache = Load(sessionId);
            if (searchResultCache.Length > 0)
            {
                #region get the filter criteria

                string hAddress = filterCriteria.ContainsKey("hotelAddress") ? filterCriteria["hotelAddress"] : string.Empty;
                string hName = filterCriteria.ContainsKey("hotelName") ? filterCriteria["hotelName"] : string.Empty;
                //int hRating = filterCriteria.ContainsKey("hotelRating") ? Convert.ToInt32(filterCriteria["hotelRating"]) : 0;
                string[] hRating = filterCriteria.ContainsKey("hotelRating") ? (filterCriteria["hotelRating"]).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries) : new string[0];
                int orderBy = filterCriteria.ContainsKey("orderBy") ? Convert.ToInt32(filterCriteria["orderBy"]) : 0;
                int pageNo = filterCriteria.ContainsKey("pageNo") ? Convert.ToInt32(filterCriteria["pageNo"]) : 1;
                string location = filterCriteria.ContainsKey("location") ? filterCriteria["location"] : string.Empty;
                #endregion

                HotelSearchResult[] sRFiltered = new HotelSearchResult[searchResultCache.Length];
                if (hAddress.Length > 0 || hName.Length > 0 || hRating.Length != 0)
                {
                    int count = 0;
                    for (int k = 0; k < searchResultCache.Length; k++)
                    {
                        HotelSearchResult hotel = new HotelSearchResult();
                        hotel = searchResultCache[k];
                        //only the hotel with rating equal to the requested one
                        foreach (string rating in hRating)
                        {
                            if (Convert.ToInt32(hotel.Rating) == Convert.ToInt32(rating))
                            {
                                if (hotel.bookingSource == HotelBookingSource.Desiya)
                                {
                                    if ((hAddress.Length > 0 && hotel.HotelLocation != null && hotel.HotelLocation.Length > 0 && hAddress == hotel.HotelLocation) || (hotel.hotelName != null && hotel.hotelAddress != null && hotel.hotelName.ToUpper().Contains(hName.ToUpper()) && hotel.hotelAddress.ToUpper().Contains(hAddress.ToUpper())))
                                    {
                                        sRFiltered[count++] = hotel;
                                    }
                                }
                                //else if (hotel.bookingSource == HotelBookingSource.GTA && hotel.hotelLocation != null && hotel.hotelLocation.Length > 0)
                                //{
                                //    if (hotel.hotelName != null && hotel.hotelName.ToUpper().Contains(hName.ToUpper())) // removed by brahmam req First time Static static download avoid && hotel.hotelLocation.ToUpper().Contains(hAddress.ToUpper()
                                //    {
                                //        sRFiltered[count++] = hotel;
                                //    }
                                //}
                                ////Dotw and WST at the time only we are filtering location oather source filtering Address
                                else if (hotel.hotelName != null && hotel.hotelName.ToUpper().Contains(hName.ToUpper())) //removed by brahmam req First time Static static download avoid  && hotel.hotelAddress != null && && hotel.hotelAddress.ToUpper().Contains(hAddress.ToUpper()
                                {
                                    if (!string.IsNullOrEmpty(location))
                                    {
                                        if (hotel.bookingSource == HotelBookingSource.DOTW || hotel.bookingSource == HotelBookingSource.WST)
                                        {
                                            if (!string.IsNullOrEmpty(hotel.hotelLocation) && hotel.hotelLocation.ToUpper().Contains(location.ToUpper()))
                                            {
                                                sRFiltered[count++] = hotel;
                                            }
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(hotel.hotelAddress) && hotel.hotelAddress.ToUpper().Contains(location.ToUpper()))
                                            {
                                                sRFiltered[count++] = hotel;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        sRFiltered[count++] = hotel;
                                    }
                                }
                            }
                        }
                    }

                    if (sRFiltered.Length > count)
                    {
                        Array.Resize(ref sRFiltered, count);
                    }
                }
                else
                {
                    sRFiltered = searchResultCache;
                }
                //update the no of pages
                int totalFilteredRes = sRFiltered.Length;

                noOfPages = totalFilteredRes / recordsPerPage + Convert.ToInt16(totalFilteredRes % recordsPerPage > 0);

                //sorting the data by the given parameter
                switch (orderBy)
                {
                    case 0: sRFiltered = SortByPrice(sRFiltered, 0);
                        break;
                    case 1: sRFiltered = SortByPrice(sRFiltered, 1);
                        break;
                    case 2: sRFiltered = SortByName(sRFiltered, 0);
                        break;
                    case 3: sRFiltered = SortByName(sRFiltered, 1);
                        break;
                    case 4: sRFiltered = SortByRating(sRFiltered, 0);
                        break;
                    case 5: sRFiltered = SortByRating(sRFiltered, 1);
                        break;
                    case 6: sRFiltered = SortByDiscounts(sRFiltered, 0);
                        break;
                    default:
                        break;
                }
                sRFiltered = FilterByTBOConnect(sRFiltered);
                int endIndex = 0;
                int startIndex = recordsPerPage * (pageNo - 1);
                if ((startIndex + recordsPerPage) - 1 < totalFilteredRes)
                {
                    endIndex = (startIndex + recordsPerPage) - 1;
                }
                else
                {
                    endIndex = totalFilteredRes - 1;
                }
                if (startIndex > endIndex)
                {
                    pageNo = 1;
                }
                startIndex = recordsPerPage * (pageNo - 1);
                if ((startIndex + recordsPerPage) - 1 < totalFilteredRes)
                {
                    endIndex = (startIndex + recordsPerPage) - 1;
                }
                else
                {
                    endIndex = totalFilteredRes - 1;
                }
                //copy the required result for the requested page
                HotelSearchResult[] returnSearchResult = new HotelSearchResult[endIndex - startIndex + 1];
                for (int iX = 0; iX <= endIndex - startIndex; iX++)
                {
                    returnSearchResult[iX] = sRFiltered[startIndex + iX];
                }
                return returnSearchResult;
            }
            else
            {
                return noResult;
            }
        }

        /// <summary>
        /// Deserialize the ByteArray to object
        /// </summary>
        /// <param name="theByteArray">theByteArray</param>
        /// <returns>object</returns>
        private object GetObjectWithByteArray(byte[] theByteArray)
        {
            MemoryStream ms = new MemoryStream(theByteArray);
            BinaryFormatter bf1 = new BinaryFormatter();
            ms.Position = 0;
            return bf1.Deserialize(ms);
        }

        /// <summary>
        /// Serialize the object to ByteArray
        /// </summary>
        /// <param name="o">o</param>
        /// <returns>byte[]</returns>
        private byte[] GetByteArrayWithObject(Object o)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf1 = new BinaryFormatter();
            bf1.Serialize(ms, o);
            return ms.ToArray();
        }

        /// <summary>
        /// Sort the result set order by price
        /// </summary>
        /// <param name="result"></param>
        /// <param name="order">if 0 low to high,1 high to low</param>
        /// <returns>HotelSearchResult[]</returns>
        private HotelSearchResult[] SortByPrice(HotelSearchResult[] result, int order)
        {
            //Assumption : data from actual source comes sorted on Price in Ascending order
            if (order == 1)
            {
                Array.Reverse(result);
            }
            return result;
        }
        /// <summary>
        /// This Method is used to sort the results based on discounts.
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="order">order</param>
        /// <returns>HotelSearchResult[]</returns>
        private HotelSearchResult[] SortByDiscounts(HotelSearchResult[] result, int order)
        {
            result = SortByPrice(result, 0);
            int count = 0, k = 0;
            for (int n = 0; n < result.Length - 1; n++)
            {
                if (result[n].PromoMessage.Length > 0)
                {
                    count = n - 1;
                    k = n;
                    while (count >= 0)
                    {
                        if (result[count].PromoMessage.Length > 0)
                        {
                            break;
                        }
                        else
                        {
                            HotelSearchResult temp = result[k];
                            result[k] = result[count];
                            result[count] = temp;
                            count--;
                            k--;
                        }
                    }

                }

            }

            return result;

        }

        /// <summary>
        /// sort the result order by name
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="order">if 0 A-Z,1 Z-A</param>
        /// <returns>HotelSearchResult[]</returns>
        private HotelSearchResult[] SortByName(HotelSearchResult[] result, int order)
        {
            for (int n = 0; n < result.Length - 1; n++)
            {
                for (int m = n + 1; m < result.Length; m++)
                {
                    if (result[n].hotelName.CompareTo(result[m].hotelName) > 0)
                    {
                        HotelSearchResult temp = result[n];
                        result[n] = result[m];
                        result[m] = temp;
                    }
                }
            }
            if (order == 1)
            {
                Array.Reverse(result);
            }
            return result;
        }

        /// <summary>
        /// order by star rating
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="order">if 0 0-5,1 5-0</param>
        /// <returns>HotelSearchResult[]</returns>
        private HotelSearchResult[] SortByRating(HotelSearchResult[] result, int order)
        {
            for (int n = 0; n < result.Length - 1; n++)
            {
                for (int m = n + 1; m < result.Length; m++)
                {
                    if ((int)result[n].rating > (int)result[m].rating)
                    {
                        HotelSearchResult temp = result[n];
                        result[n] = result[m];
                        result[m] = temp;
                    }
                }
            }
            if (order == 1)
            {
                Array.Reverse(result);
            }
            return result;
        }
        /// <summary>
        /// Filter By TBo Hotels
        /// </summary>
        /// <param name="searchResult">searchResult</param>
        /// <returns>HotelSearchResult[]</returns>
        private HotelSearchResult[] FilterByTBOConnect(HotelSearchResult[] searchResult)
        {
            HotelSearchResult[] tempRes = new HotelSearchResult[searchResult.Length];
            int j = 0;
            for (int i = 0; i < searchResult.Length; i++)
            {
                if (searchResult[i].BookingSource == HotelBookingSource.TBOConnect)
                {
                    tempRes[j++] = searchResult[i];
                }
            }
            for (int k = 0; k < searchResult.Length; k++)
            {
                if (searchResult[k].BookingSource != HotelBookingSource.TBOConnect)
                {
                    tempRes[j++] = searchResult[k];
                }
            }
            return tempRes;
        }
        //#region Grn          
        //public HotelSearchResult CopyByValue()
        //{
        //    HotelSearchResult searchResult= new HotelSearchResult();
        //    searchResult.roomDetails = roomDetails;
        //    return searchResult;
        //}
        //#endregion
        #endregion

        #region IComparer<HotelSearchResult> Members
        /// <summary>
        /// IComparer object
        /// </summary>
        /// <param name="x">HotelSearchResult</param>
        /// <param name="y">HotelSearchResult</param>
        /// <returns>int</returns>
        public int Compare(HotelSearchResult x, HotelSearchResult y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (y == null)
                {
                    return 1;
                }
                else
                {
                    int retVal = x.roomDetails[0].TotalPrice.CompareTo(y.roomDetails[0].TotalPrice);

                    if (retVal != 0)
                    {
                        return retVal;
                    }
                    else
                    {
                        return x.roomDetails[0].TotalPrice.CompareTo(y.roomDetails[0].TotalPrice);
                    }
                }
            }
        }

        /// <summary>
        /// To compare itinerary price component with hotel search result
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static void ValItineraryPrice(HotelItinerary x, HotelSearchResult y)
        {
            if (x == null)
                throw new Exception("Invalid hotel itinerary.");

            if (y == null)
                throw new Exception("Invalid search cache data.");

            string hdrMsg = y.ApiSessionId + "<br>UID:" + x.CreatedBy.ToString() + "<br>City:" + x.CityRef +
                    "<br>Hotel:" + x.HotelName + "<br>Code:" + x.HotelCode + "<br>IN:" + x.StartDate + "<br>OUT:" + x.EndDate + "<br>" + x.Source.ToString() + "<br>" + x.PassengerNationality + "<br>";

            if (x.Roomtype == null)
                throw new Exception(hdrMsg + "Invalid hotel itinerary roomtype.");

            if (x.Roomtype.Length == 0)
                throw new Exception(hdrMsg + "Invalid hotel itinerary rooms.");

            if (y.RoomDetails == null)
                throw new Exception(hdrMsg + "Invalid search cache rooms data.");

            if (y.RoomDetails.Length == 0)
                throw new Exception(hdrMsg + "Invalid search cache rooms.");

            string errMsg = string.Empty;

            for (int i = 0; i < x.Roomtype.Length; i++)
            {
                if (x.Roomtype[i].RoomTypeCode == y.RoomDetails[i].RoomTypeCode)
                {
                    if (x.Roomtype[i].Price.Discount != y.RoomDetails[i].Discount)
                        errMsg += "Room-" + (i + 1).ToString() + ",Discount Orignal(" + y.RoomDetails[i].Discount.ToString() + "),New(" +
                            x.Roomtype[i].Price.Discount.ToString() + ")<br>";

                    if (x.Roomtype[i].Price.Markup != y.RoomDetails[i].Markup)
                        errMsg += "Room-" + (i + 1).ToString() + ",MarkUp Orignal(" + y.RoomDetails[i].Markup.ToString() + "),New(" +
                            x.Roomtype[i].Price.Markup.ToString() + ")<br>";

                    if (x.Roomtype[i].Price.InputVATAmount != y.RoomDetails[i].InputVATAmount)
                        errMsg += "Room-" + (i + 1).ToString() + ",VAT Origna(" + y.RoomDetails[i].InputVATAmount.ToString() + "), New(" +
                            x.Roomtype[i].Price.InputVATAmount.ToString() + ")<br>";

                    if (x.Roomtype[i].Price.Tax != y.RoomDetails[i].TotalTax)
                        errMsg += "Room-" + (i + 1).ToString() + ",TAX Orignal(" + y.RoomDetails[i].TotalTax.ToString() + "),New(" +
                            x.Roomtype[i].Price.Tax.ToString() + ")<br>";

                    if (x.Roomtype[i].Price.NetFare != y.RoomDetails[i].SellingFare + y.RoomDetails[i].SellExtraGuestCharges + y.RoomDetails[i].ChildCharges)
                        errMsg += "Room-" + (i + 1).ToString() + ",NetFare Orignal" + (y.RoomDetails[i].SellingFare + y.RoomDetails[i].SellExtraGuestCharges +
                            y.RoomDetails[i].ChildCharges).ToString() + "),New(" + x.Roomtype[i].Price.NetFare.ToString() + ")<br>";

                    if (!string.IsNullOrEmpty(errMsg))
                        throw new Exception(hdrMsg + errMsg);
                }
            }
        }

        /// <summary>
        /// To compare search result price component with hotel search result
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static void ValResultPrice(HotelSearchResult x, HotelSearchResult y)
        {
            if (x == null || y == null || x.RoomDetails == null || y.RoomDetails == null || x.RoomDetails.Length == 0 || y.RoomDetails.Length == 0)
                throw new Exception("Invalid price component.");

            for (int i = 0; i < x.RoomDetails.Length; i++)
            {
                if (x.RoomDetails[i].Discount != y.RoomDetails[i].Discount || x.RoomDetails[i].Markup != y.RoomDetails[i].Markup)
                    throw new Exception("Invalid price component.");

                if (x.RoomDetails[i].InputVATAmount != y.RoomDetails[i].InputVATAmount || x.RoomDetails[i].TotalTax != y.RoomDetails[i].TotalTax)
                    throw new Exception("Invalid price component.");

                if (x.RoomDetails[i].SellingFare != y.RoomDetails[i].SellingFare || x.RoomDetails[i].SellExtraGuestCharges != y.RoomDetails[i].SellExtraGuestCharges)
                    throw new Exception("Invalid price component.");

                if (x.RoomDetails[i].ChildCharges != y.RoomDetails[i].ChildCharges || x.RoomDetails[i].TotalPrice != y.RoomDetails[i].TotalPrice)
                    throw new Exception("Invalid price component.");
            }
        }
        #endregion
    }
}
