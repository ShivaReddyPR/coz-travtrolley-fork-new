﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine
{
    public class TransferQueueRequest
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public List<int> BookingStatus { get; set; }
        public string TransferFilter { get; set; }
        public List<int> Source { get; set; }
        public string agentFilter { get; set; }
        public string agentType { get; set; }
        public string paxFilter { get; set; }

        public string pnrFilter { get; set; }
        public string transType { get; set; }
        public List<int> bookingModes { get; set; }
        public string Voucherd { get; set; }
        public string transferName { get; set; }
        public string isLcc { get; set; }
        public int pageNo { get; set; }

        public TransferQueueResponse GetTransferQueueResult()
        {
            
            TransferQueueResponse QueueResponse = new TransferQueueResponse();
            DataTable dtQueue = new DataTable();
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");

            DateTime DtFromDate = Convert.ToDateTime(Convert.ToDateTime(FromDate,provider).ToString("yyyy/MM/dd 00:00"), provider);
            DateTime DtToDate = Convert.ToDateTime(Convert.ToDateTime(ToDate, provider).ToString("yyyy/MM/dd 23:59"), provider);
            try
            {
                int noOfRecords = Convert.ToInt32(CT.Configuration.ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);
                string orderByString = string.Empty;
                orderByString = " order by lastCancellationDate asc ";
                int queueCount = 0;
                string whereString = CT.Core.Queue.GetWhereStringForTransferBookingQueue(BookingStatus, TransferFilter, paxFilter, pnrFilter,
                                                                                    Convert.ToInt32(agentFilter), bookingModes, isLcc, Voucherd, true,
                                                                                    Source, DtFromDate, DtToDate, agentType, transType, transferName);
               
                queueCount = CT.Core.Queue.GetTotalFilteredRecordsCount(whereString, orderByString, "Transfer");
                
                dtQueue = CT.Core.Queue.GetTotalFilteredRecords(pageNo, noOfRecords, queueCount, whereString, orderByString, "Transfer");
                int noOfPages = 0;
                if ((queueCount % noOfRecords) > 0)
                {
                    noOfPages = (queueCount / noOfRecords) + 1;
                }
                else
                {
                    noOfPages = (queueCount / noOfRecords);
                }
                QueueResponse.data = dtQueue;
                QueueResponse.status = true;
                QueueResponse.noOfPages = noOfPages;
                
                return QueueResponse;
            }
            catch (Exception e)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.High, 0, "Inside Transfer Queue ERror:"+e.ToString(), "");
                QueueResponse.data = dtQueue;
                QueueResponse.status = false;
                QueueResponse.noOfPages = 0;

                return QueueResponse;
            }

        }
    }
    public class TransferQueueResponse
    {
        public bool status { get; set; }
        public DataTable data { get; set; }
        public int noOfPages { get; set; }
    }
}
