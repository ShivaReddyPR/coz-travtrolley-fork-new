﻿using System;
using System.Collections.Generic;
//using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for ProductReport
/// </summary>

namespace CT.BookingEngine
{
    /// <summary>
    /// Class for generating reports for various modules
    /// </summary>
    public class ProductReport
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProductReport()
        {
        }

        # region HotelAcctReport
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="agentId"></param>
        /// <param name="source"></param>
        /// <param name="status"></param>
        /// <param name="acctStatus"></param>
        /// <param name="agentType"></param>
        /// <param name="transType"></param>
        /// <returns></returns>
        public static DataTable HotelReportGetList(DateTime startDate, DateTime endDate, int agentId, int source, int status, int acctStatus, string agentType, string transType, DateTime CheckinDate, DateTime CheckoutDate,string agentref)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[11];
                    paramList[0] = new SqlParameter("@P_FROM_DATE", startDate);
                    paramList[1] = new SqlParameter("@P_TO_DATE", endDate);
                    if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                    if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
                    if (status > 0) paramList[4] = new SqlParameter("@P_Status", status);
                    if (acctStatus >= 0) paramList[5] = new SqlParameter("@P_IsAccounted", acctStatus);
                    paramList[6] = new SqlParameter("@P_AGENT_TYPE", agentType);
                    if (!string.IsNullOrEmpty(transType)) paramList[7] = new SqlParameter("@P_TransType", transType);
                    paramList[8] = new SqlParameter("@P_Checkin_DATE", CheckinDate);
                    paramList[9] = new SqlParameter("@P_Checkout_DATE", CheckoutDate);
                    if (agentref != string.Empty)
                        paramList[10] = new SqlParameter("@P_agentRef", agentref);
                    else paramList[10] = new SqlParameter("@P_agentRef", DBNull.Value);
                    data = DBGateway.ExecuteReaderSP(SPNames.HotelAcctReportGetlist, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="modifiedBy"></param>
        public static void UpdateHotelAcctStatus(long hotelId, int modifiedBy)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_HOTEL_ID", hotelId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[3].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("usp_UpdateHotelAccountedStatus", paramList);
            }
            catch { throw; }
        }
        # endregion

        # region FlightAcctReport
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="agentId"></param>
        /// <param name="source"></param>
        /// <param name="acctStatus"></param>
        /// <param name="agentType"></param>
        /// <param name="transType"></param>
        /// <returns></returns>
        public static DataTable FlightReportGetList(DateTime fromDate, DateTime toDate, int agentId, int source, int acctStatus, string agentType, string transType)
        {
            //SqlDataReader data = null;
            //DataTable dt = new DataTable();
            //using (SqlConnection connection = DBGateway.GetConnection())
            //{
            //    SqlParameter[] paramList;
            //    try
            //    {
            //        paramList = new SqlParameter[7];
            //        paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
            //        paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
            //        if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
            //        if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
            //        if (acctStatus >= 0) paramList[4] = new SqlParameter("@P_IsAccounted", acctStatus);
            //        paramList[5] = new SqlParameter("@P_AGENT_TYPE", agentType);
            //        if (!string.IsNullOrEmpty(transType)) paramList[6] = new SqlParameter("@P_TransType", transType);
            //        data = DBGateway.ExecuteReaderSP(SPNames.FlightAcctReportGetlist, paramList, connection);
            //        if (data != null)
            //        {
            //            dt.Load(data);
            //        }
            //        connection.Close();
            //    }
            //    catch
            //    {
            //        throw;
            //    }
            //}
            //return dt;
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
            paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
            if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
            if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
            if (acctStatus >= 0) paramList[4] = new SqlParameter("@P_IsAccounted", acctStatus);
            paramList[5] = new SqlParameter("@P_AGENT_TYPE", agentType);
            if (!string.IsNullOrEmpty(transType)) paramList[6] = new SqlParameter("@P_TransType", transType);
            return DBGateway.FillDataTableSP(SPNames.FlightAcctReportGetlist, paramList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="modifiedBy"></param>
        public static void UpdateFlightAcctStatus(long ticketId, int modifiedBy)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_TICKET_ID", ticketId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[3].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("usp_UpdateFlightAccountedStatus", paramList);
            }
            catch { throw; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="agentId"></param>
        /// <param name="source"></param>
        /// <param name="pnr"></param>
        /// <param name="agentType"></param>
        /// <param name="transType"></param>
        /// <returns></returns>
        public static DataTable FlightTranxReportGetList(DateTime fromDate, DateTime toDate, int agentId, int source, string pnr, string agentType, string transType)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[7];
                    paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                    paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                    if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                    if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
                   if(!string.IsNullOrEmpty(pnr)) paramList[4] = new SqlParameter("@P_PNR", pnr);
                    paramList[5] = new SqlParameter("@P_AGENT_TYPE", agentType);
                    if (!string.IsNullOrEmpty(transType)) paramList[6] = new SqlParameter("@P_TransType", transType);
                    data = DBGateway.ExecuteReaderSP("usp_flight_tranx_acct_report_getlist", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="agentId"></param>
        /// <param name="source"></param>
        /// <param name="acctStatus"></param>
        /// <param name="agentType"></param>
        /// <param name="transType"></param>
        /// <param name="locationcountry"></param>
        /// <param name="logintype"></param>
        /// <returns></returns>
        public static DataTable FlightAccountReportGetList(DateTime fromDate, DateTime toDate, int agentId, int source, int acctStatus, string agentType, string transType, string locationcountry, string logintype)
        {
            DataTable dt = new DataTable();

            SqlParameter[] paramList;
            try
            {
                paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
                if (acctStatus >= 0) paramList[4] = new SqlParameter("@P_IsAccounted", acctStatus);
                paramList[5] = new SqlParameter("@P_AGENT_TYPE", agentType);
                if (!string.IsNullOrEmpty(transType)) paramList[6] = new SqlParameter("@P_TransType", transType);
                paramList[7] = new SqlParameter("@P_loginCountry", locationcountry);
                paramList[8] = new SqlParameter("@P_LOGINTYPE", logintype);

                dt = DBGateway.FillDataTableSP(SPNames.FlightAccounttReportGetlist, paramList);
            }
            catch
            {
                throw;
            }

            return dt;
        }
        # endregion

        public static DataTable GetV1FlightReport(DateTime fromDate, DateTime toDate, int agentId, int source, int acctStatus, string agentType)
        {
            DataTable reportData = new DataTable();
            SqlParameter[] paramList;
            try
            {
                paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (agentId > 0)
                {
                    paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                }
                else
                {
                    paramList[2] = new SqlParameter("@P_AGENT_ID", DBNull.Value);
                }
                if (source > 0)
                {
                    paramList[3] = new SqlParameter("@P_Source", source);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_Source", DBNull.Value);
                }
                paramList[4] = new SqlParameter("@P_AGENT_TYPE", agentType);

                reportData = DBGateway.FillDataTableSP(SPNames.GetV1FlightsReport, paramList);
                //data = DBGateway.FillDataTableSP(SPNames.GetV1FlightsReport, paramList, connection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reportData;
        }

        #region HotelCommissionReport
        public static DataTable HotelCommissionReportGetList(DateTime fromDate, DateTime toDate, int agentId, int source, int status, int acctStatus, string agentType, string transType)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[8];
                    paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                    paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                    if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                    if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
                    if (status > 0) paramList[4] = new SqlParameter("@P_Status", status);
                    if (acctStatus >= 0) paramList[5] = new SqlParameter("@P_IsAccounted", acctStatus);
                    paramList[6] = new SqlParameter("@P_AGENT_TYPE", agentType);
                    if (!string.IsNullOrEmpty(transType)) paramList[7] = new SqlParameter("@P_TransType", transType);
                    data = DBGateway.ExecuteReaderSP(SPNames.HotelCommReportGetlist, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
           #endregion 


        # region FlightAcctReport
        public static DataTable FlightCommissionReportGetList(DateTime fromDate, DateTime toDate, int agentId, int source, int acctStatus, string agentType, string transType)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[7];
                    paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                    paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                    if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                    if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
                    if (acctStatus >= 0) paramList[4] = new SqlParameter("@P_IsAccounted", acctStatus);
                    paramList[5] = new SqlParameter("@P_AGENT_TYPE", agentType);
                    if (!string.IsNullOrEmpty(transType)) paramList[6] = new SqlParameter("@P_TransType", transType);
                    data = DBGateway.ExecuteReaderSP(SPNames.FlightCommReportGetlist, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        #endregion

        # region SightseeingAcctReport
        /* added for get and update Sightseeing Account status <28032016> by chandan*/
        public static DataTable SightseeingReportGetList(DateTime fromDate, DateTime toDate, int agentId, int source, int status, int acctStatus, string agentType, string transType)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[8];
                    paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                    paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                    if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                    if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
                    if (status > 0) paramList[4] = new SqlParameter("@P_Status", status);
                    if (acctStatus >= 0) paramList[5] = new SqlParameter("@P_IsAccounted", acctStatus);
                    paramList[6] = new SqlParameter("@P_AGENT_TYPE", agentType);
                    if (!string.IsNullOrEmpty(transType)) paramList[7] = new SqlParameter("@P_TransType", transType);
                    data = DBGateway.ExecuteReaderSP(SPNames.SightseeingAcctReportGetlist, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public static void UpdateSightseeingAcctStatus(long sightseeingId, int modifiedBy)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_SIGHTSEEING_ID", sightseeingId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[3].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery(SPNames.UpdateSightseeingAccountedStatus, paramList);
            }
            catch { throw; }
        }
        # endregion

        # region TransferAcctReport
        /* added for get and update Transfer Account status <29032016> by chandan*/
        public static DataTable TransferReportGetList(DateTime fromDate, DateTime toDate, int agentId, int source, int status, int acctStatus, string agentType, string transType)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[8];
                    paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                    paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                    if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                    if (source > 0) paramList[3] = new SqlParameter("@P_Source", source);
                    if (status > 0) paramList[4] = new SqlParameter("@P_Status", status);
                    if (acctStatus >= 0) paramList[5] = new SqlParameter("@P_IsAccounted", acctStatus);
                    paramList[6] = new SqlParameter("@P_AGENT_TYPE", agentType);
                    if (!string.IsNullOrEmpty(transType)) paramList[7] = new SqlParameter("@P_TransType", transType);
                    data = DBGateway.ExecuteReaderSP(SPNames.TransferAcctReportGetlist, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public static void UpdateTransferAcctStatus(long transferId, int modifiedBy)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_TRANSFER_ID", transferId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[3].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery(SPNames.UpdateTransferAccountedStatus, paramList);
            }
            catch { throw; }
        }
        # endregion
    }
       
}


