using System;
using System.Collections.Generic;
using System.Data;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.BookingEngine
{
    public class TransferSource
   {
       # region private variables
        int transferSourceId;
        string transferSourceName;
        decimal agenctCommission;
        decimal ourCommission;
        CommissionType commissionTypeId;
        FareType fareTypeId;
        bool domestic;
        # endregion

        # region public properties
        /// <summary>
        /// Unique transfer source ID
        /// </summary>
        public int TransferSourceId
        {
            get { return transferSourceId; }
            set { transferSourceId = value; }
        }
        /// <summary>
        /// Name of the Transfer Source
        /// </summary>
        public string TransferSourceName
        {
            get { return transferSourceName; }
            set { transferSourceName = value; }
        }
        /// <summary>
        /// Agenct commission
        /// </summary>
        public decimal AgentCommission
        {
            get { return agenctCommission; }
            set { agenctCommission = value; }
        }
        /// <summary>
        /// Our commission.
        /// </summary>
        public decimal OurCommission
        {
            get { return ourCommission; }
            set { ourCommission = value; }
        }

        /// <summary>
        /// Our commission.
        /// </summary>
        public CommissionType CommissionTypeId
        {
            get { return commissionTypeId; }
            set { commissionTypeId = value; }
        }
        /// <summary>
        /// fare Type
        /// </summary>
        public FareType FareTypeId
        {
            get { return fareTypeId; }
            set { fareTypeId = value; }
        }
        /// <summary>
        /// Domestic or not
        /// </summary>
        public bool Domestic
        {
            get {return domestic; }
            set { domestic = value; }
        }

        # endregion

        # region Methods

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
            //Trace.TraceInformation("TransferSource.Save entered.");
            SqlParameter[] paramList = new SqlParameter[9];
            paramList[0] = new SqlParameter("@transferSource", transferSourceName);
            paramList[1] = new SqlParameter("@agentComm", agenctCommission);
            paramList[2] = new SqlParameter("@ourComm", ourCommission);
            paramList[3] = new SqlParameter("@commissionTypeId", commissionTypeId);
            paramList[4] = new SqlParameter("@CommissionType", Enum.Parse(typeof(CommissionType), commissionTypeId.ToString(), true).ToString()); // enum type value parsing
            paramList[5] = new SqlParameter("@fareTypeId", fareTypeId);
            paramList[6] = new SqlParameter("@fareType", Enum.Parse(typeof(FareType), fareTypeId.ToString(), true).ToString());
            paramList[7] = new SqlParameter("@domestic", domestic);
            paramList[8] = new SqlParameter("@transferSourceId", SqlDbType.Int);
            paramList[8].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddTransferSource, paramList);
            //Trace.TraceInformation("TransferSource.Save exiting");
            return (int)paramList[3].Value;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="hSourceId"></param>
        public void Load(int tSourceId)
        {
            //Trace.TraceInformation("TransferSource.Load entered : transferSourceId = " + tSourceId);
            if (tSourceId <= 0)
            {
                throw new ArgumentException("TransferSourceId Id should be positive integer", "transferSourceId");
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transferSourceId", tSourceId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetTransferSourceById, paramList, connection);
            if (data.Read())
            {
                transferSourceName = data["transferSource"].ToString();
                agenctCommission = Convert.ToDecimal(data["agentComm"]);
                ourCommission = Convert.ToDecimal(data["ourComm"]);
                commissionTypeId = (CommissionType)data["commissionTypeId"];
                fareTypeId = (FareType)data["fareTypeId"];
                domestic = Convert.ToBoolean(data["domestic"]);
                data.Close();
                connection.Close();
                //Trace.TraceInformation("TransferSource.Load exiting :" + tSourceId.ToString());
            }
            else
            {
                data.Close();
                connection.Close();
                //Trace.TraceInformation("TransferSource.Load exiting : transferSourceId does not exist.transferSourceId = " + transferSourceId.ToString());
                throw new ArgumentException("Transfer Source id does not exist in database");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hSourceId"></param>
        public void Update(int tSourceId)
        {
            //Trace.TraceInformation("TransferSource.Update entered : transferSourceId = " + tSourceId);
            if (tSourceId <= 0)
            {
                throw new ArgumentException("TransferSourceId Id should be positive integer", "transferSourceId");
            }
            SqlParameter[] paramList = new SqlParameter[9];
            paramList[0] = new SqlParameter("@transferSourceId", tSourceId);
            paramList[1] = new SqlParameter("@transferSource", transferSourceName);
            paramList[2] = new SqlParameter("@agentComm", agenctCommission);
            paramList[3] = new SqlParameter("@ourComm", ourCommission);
            paramList[4] = new SqlParameter("@commissionTypeId", commissionTypeId);
            paramList[5] = new SqlParameter("@commissionType", Enum.Parse(typeof(CommissionType), commissionTypeId.ToString(), true).ToString());
            paramList[6] = new SqlParameter("@fareTypeId", fareTypeId);
            paramList[7] = new SqlParameter("@fareType", Enum.Parse(typeof(FareType), fareTypeId.ToString(), true).ToString());
            paramList[8] = new SqlParameter("@domestic", domestic);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateTransferSource, paramList);
            //Trace.TraceInformation("TransferSource.Update exiting");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hSourceName"></param>
        public void Load(string tSourceName)
        {
            //Trace.TraceInformation("TransferSource.Load entered : transferSourceName = " + tSourceName);
            if (tSourceName.Length == 0)
            {
                throw new ArgumentException("TransferSourceName should be having Information", "hSourceName");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transferSourceName", tSourceName);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetTransferSourceByName, paramList, connection);
            using (DataTable dtTransfer = DBGateway.FillDataTableSP(SPNames.GetTransferSourceByName, paramList))
            {
                if (dtTransfer != null && dtTransfer.Rows.Count > 0)
                {
                    foreach (DataRow  data in dtTransfer.Rows)
                    {
                        transferSourceId = Convert.ToInt32(data["transferSourceId"]);
                        agenctCommission = Convert.ToDecimal(data["agentComm"]);
                        ourCommission = Convert.ToDecimal(data["ourComm"]);
                        commissionTypeId = (CommissionType)data["commissionTypeId"];
                        fareTypeId = (FareType)data["fareTypeId"];
                        domestic = Convert.ToBoolean(data["domestic"]);
                        //data.Close();
                        //connection.Close();
                        //Trace.TraceInformation("TransferSource.Load exiting :" + tSourceName);
                    }
                }
                else
                {
                    //data.Close();
                    //connection.Close();
                    //Trace.TraceInformation("TransferSource.Load exiting : transferSourceName does not exist.transferSourceName = " + transferSourceName);
                    throw new ArgumentException("Transfer Source Name does not exist in database");
                }
            }
        }
        public List<int> Load()
        {
            //Trace.TraceInformation("TransferSource.Load entered : ");
            List<int> idList = new List<int>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAllTransferSourceId, paramList, connection);
           while(data.Read())
            {
                idList.Add( Convert.ToInt32(data["transferSourceId"]));
               
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("TransferSource.Load exiting :");
            return idList;
        }
        #endregion
    }
}
