﻿using System;
//using System.Linq;

namespace CT.BookingEngine
{
    public class fleetAgentBookingBO
    {
        /// <summary>
        /// Internal members
        /// </summary>
        int _bookingId;
        int _clientId;
        string _bookingStatus;
        DateTime _bookingDate;
        DateTime _fromDate;
        DateTime _toDate;
        string _fromLocation;
        string _toLocation;
        string _bookingRef;
        decimal _bookingNetAmount;
        string _payStatus;
        string _firstName;
        string _lastName;
        string _payRefId;
        decimal _amount;
        decimal _cardcharges;
        string _source;
        string _remarks;
        string serviceRequestStatus;
        int serviceReqId;
        //Properties
        #region properties
        public int BookingId
        {
            get { return _bookingId; }
            set { _bookingId = value; }
        }
        public int ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }
        public string BookingStatus
        {
            get { return _bookingStatus; }
            set { _bookingStatus = value; }
        }

        public DateTime BookingDate
        {
            get { return _bookingDate; }
            set { _bookingDate = value; }
        }

        public DateTime FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        public DateTime ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        public string FromLocation
        {
            get { return _fromLocation; }
            set { _fromLocation = value; }
        }

        public string ToLocation
        {
            get { return _toLocation; }
            set { _toLocation = value; }
        }
        public string BookingRef
        {
            get { return _bookingRef; }
            set { _bookingRef = value; }
        }
 
        public string PayStatus
        {
            get { return _payStatus; }
            set { _payStatus = value; }
        }
        public decimal BookingNetAmount
        {
            get { return _bookingNetAmount; }
            set { _bookingNetAmount = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string PayRefId
        {
            get { return _payRefId; }
            set { _payRefId = value; }
        }

        public Decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public decimal Cardcharges
        {
            get { return _cardcharges; }
            set { _cardcharges = value; }
        }
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
        public string ServiceRequestStatus
        {
            get { return serviceRequestStatus; }
            set { serviceRequestStatus = value; }
        }
        public int ServiceReqId
        {
            get { return serviceReqId; }
            set { serviceReqId = value; }
        }
        #endregion

        /// <summary>
        /// Default Constructor
        /// </summary>
        public fleetAgentBookingBO()
        {

        }
    }
}
