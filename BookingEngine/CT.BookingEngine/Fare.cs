using System;

namespace CT.BookingEngine
{
    [Serializable]
    public class Fare
    {
        private PassengerType passengerType;
        private int passengerCount;
        private double baseFare;
        private double totalFare;
        private double sellingFare;
        private decimal airlineTransFee;
        private decimal agentMarkup;
        private decimal agentDiscount;
        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare
        /// </summary>
        private decimal additionalTxnFee;
        private double supplierFare;
        private string fareType;
        private decimal b2cMarkup;
        private double supplierTax;// For PK FARE booking request
        private decimal handlingFee;
        private decimal dynamicMarkup;
      

        public PassengerType PassengerType
        {
            get
            {
                return passengerType;
            }
            set
            {
                passengerType = value;
            }
        }

        public int PassengerCount
        {
            get
            {
                return passengerCount;
            }
            set
            {
                passengerCount = value;
            }
        }

        public double BaseFare
        {
            get
            {
                return baseFare;
            }
            set
            {
                baseFare = value;
            }
        }

        public double TotalFare
        {
            get
            {
                return totalFare;
            }
            set
            {
                totalFare = value;
            }
        }
        public double SellingFare
        {
            get 
            {
                return this.sellingFare;
            }
            set
            {
                this.sellingFare = value;
            }
        }

        public decimal AirlineTransFee
        {
            get
            {
                return this.airlineTransFee;
            }
            set
            {
                this.airlineTransFee = value;
            }
        }

        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare.
        /// </summary>
        public decimal AdditionalTxnFee
        {
            get
            {
                return additionalTxnFee;
            }
            set
            {
                additionalTxnFee = value;
            }
        }
        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare.
        /// </summary>
        public decimal AgentMarkup
        {
            get
            {
                return agentMarkup;
            }
            set
            {
                agentMarkup = value;
            }
        }
        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare.
        /// </summary>
        public decimal AgentDiscount
        {
            get
            {
                return agentDiscount;
            }
            set
            {
                agentDiscount = value;
            }
        }

        public decimal Tax
        {
            get
            {
                return Convert.ToDecimal(totalFare - baseFare);
            }
        }

        public double SupplierFare
        {
            get { return supplierFare; }
            set { supplierFare = value; }
        }

        /// <summary>
        /// FareType usually Economy, Business, FirstClass or Pay to Change, Free to Change, Basic for FlyDubai.
        /// </summary>
        public string FareType
        {
            get { return fareType; }
            set { fareType = value; }
        }

        public decimal B2CMarkup
        {
            get { return b2cMarkup; }
            set { b2cMarkup = value; }
        }

        /// <summary>
        /// For sending PaxType wise fares while booking PKFare itinerary
        /// </summary>
        public double SupplierTax
        {
            get
            {
                return supplierTax;
            }

            set
            {
                supplierTax = value;
            }
        }

        public decimal HandlingFee
        {
            get
            {
                return handlingFee;
            }

            set
            {
                handlingFee = value;
            }
        }

        public decimal DynamicMarkup { get => dynamicMarkup; set => dynamicMarkup = value; }
        
    }
}
