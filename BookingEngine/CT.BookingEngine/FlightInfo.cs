using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;

namespace CT.BookingEngine
{

    public enum FlightStatus
    {
        Default=0,
        Confirmed = 1,
        Waitlisted = 2
    }
    [Serializable] 
    public class FlightInfo
    {
        #region Members
        /// <summary>
        /// Unique identity number for UAPIS Segment Key. Used also for FlyDubai LFID(Logical FlightID). Later need rename it to Generic.
        /// </summary>
        string uapiSegmentRefKey;
        /// <summary>
        /// Unique identity number for a leg of a booking
        /// </summary>
        int segmentId;
        /// <summary>
        /// Flight id to which the leg belongs
        /// </summary>
        int flightId;
        /// <summary>
        /// Name of the airline
        /// </summary>
        string airline;
        /// <summary>
        /// Origin Airport
        /// </summary>
        Airport origin;
        /// <summary>
        /// Destination Airport
        /// </summary>
        Airport destination;
        /// <summary>
        /// Flight number
        /// </summary>
        string flightNumber;
        /// <summary>
        /// Departure time at orgin airport
        /// </summary>
        DateTime departureTime;
        /// <summary>
        /// Arrival time at the destination
        /// </summary>
        DateTime arrivalTime;
        /// <summary>
        /// Booking Class
        /// </summary>
        string bookingClass;
        /// <summary>
        /// Booking Cabin Class
        /// </summary>
        string cabinClass;
        /// <summary>
        /// Availability in different classes.
        /// </summary>
        Dictionary<string, byte> availabiLity;
        /// <summary>
        /// Arrival terminal of destination airport
        /// </summary>
        string arrTerminal;
        /// <summary>
        /// Departure terminal at origin airport
        /// </summary>
        string depTerminal;
        /// <summary>
        /// Status of flight;
        /// </summary>
        FlightStatus flightStatus ;
        /// <summary>
        /// status of flight booking
        /// </summary>
        string status;
        /// <summary>
        /// Meal Type to be served in flight
        /// </summary>
        string mealType;
        /// <summary>
        /// Indicates if e-ticket can be issued
        /// </summary>
        bool eTicketEligible;
        /// <summary>
        /// Duration of the flight
        /// </summary>
        TimeSpan duration;
        /// <summary>
        /// Ground time at origin 
        /// </summary>
        TimeSpan groundTime;
        /// <summary>
        /// Total accumulated duration. Including ground time.
        /// </summary>
        TimeSpan accumulatedDuration;
        /// <summary>
        /// Indicates the origin is stop over, if true.
        /// </summary>
        bool stopOver;
        /// <summary>
        /// Stops in the flight
        /// </summary>
        int stops;
        /// <summary>
        /// Aircraft type code
        /// </summary>
        string craft;
        /// <summary>
        /// Distance in miles
        /// </summary>
        int mile;
        /// <summary>
        /// Airline Direct Resource Locator.
        /// </summary>
        string airlinePNR;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// conjunction number in case of cunjunction ticket.
        /// </summary>
        string conjunctionNo;


        /// <summary>
        /// Fare Info Key for a segment (UAPI)
        /// </summary>
        string fareInfoKey;

        /// <summary>
        /// to store UAPI Departure Date Format
        /// </summary>
        string uapiDepartureTime;

        /// <summary>
        /// to store UAPI Arrival Date Format
        /// </summary>
        string uapiArrivalTime;


        /// <summary>
        /// Group - 0 outbount,1 inbound(UAPI)
        /// </summary>
        int group;

        string operatingCarrier;
// To store UAPI private fare values
        Hashtable uapiReservationValues; 
 /// <summary>
        /// Stores fare types for FlyDubai like Pay To Change, Free To Change or Basic
        /// </summary>
        string segmentFareType;

        /// <summary>
        /// LCC Segment Price for TBO Air
        /// </summary>
        PriceAccounts segmentPrice;
        /// <summary>
        /// LCC Segment Fare Breakdown for TBO Air
        /// </summary>
        List<Fare> segmentFareBreakdown;
        /// <summary>
        /// LCC Segment Fare Rule for TBO Air
        /// </summary>
        List<FareRule> segmentFareRule;

        /// <summary>
        /// TourCode returned for Negotiated Fare from UAPI
        /// </summary>
        string tourCode;

        /// <summary>
        /// for Default Baggage Details
        /// </summary>
        string defaultBaggage;

        public string DefaultBaggage { get => defaultBaggage; set => defaultBaggage = value; }

        string segmentKey;

        public string SegmentKey { get => segmentKey; set => segmentKey = value; }

        int serviceBundleId;
        public int ServiceBundleId { get => serviceBundleId; set => serviceBundleId = value; } //Added for G9 to store the Service Bundle Id

        #endregion

        #region Properties
        public int Group
        {
            get { return group; }
            set { group = value; }
        }


        public string UapiDepartureTime
        {
            get { return uapiDepartureTime; }
            set { uapiDepartureTime = value; }
        }

        public string UapiArrivalTime
        {
            get { return uapiArrivalTime; }
            set { uapiArrivalTime = value; }
        }

        public string ConjunctionNo
        {
            get { return conjunctionNo; }
            set { conjunctionNo=value;}
        }

        public string OperatingCarrier
        {
            get
            {
                return operatingCarrier;
            }
            set
            {
                operatingCarrier = value;
            }
        }

        public int SegmentId
        {
            get
            {
                return segmentId;
            }
            set
            {
                segmentId = value;
            }
        }

       

        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }

        public string Airline
        {
            get 
            { 
                return airline; 
            }
            set 
            { 
                airline = value; 
            }
        }

        public Airport Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        public Airport Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }
                
        public string FlightNumber
        {
            get 
            { 
                return flightNumber; 
            }
            set 
            { 
                flightNumber = value; 
            }
        }

        public DateTime DepartureTime
        {
            get 
            { 
                return departureTime; 
            }
            set 
            { 
                departureTime = value; 
            }
        }

        public DateTime ArrivalTime
        {
            get 
            { 
                return arrivalTime; 
            }
            set 
            { 
                arrivalTime = value; 
            }
        }

        public string BookingClass
        {
            get 
            { 
                return bookingClass; 
            }
            set 
            { 
                bookingClass = value; 
            }
        }
        public string CabinClass
        {
            get
            {
                return cabinClass;
            }
            set
            {
                cabinClass = value;
            }
        }

        public string FareInfoKey
        {
            get
            {
                return fareInfoKey;
            }
            set
            {
                fareInfoKey = value;
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<string, byte> AvailabiLity
        {
            get
            {
                return availabiLity;
            }
            set
            {
                availabiLity = value;
            }
        }
        
        public string ArrTerminal
        {
            get
            {
                return arrTerminal;
            }
            set
            {
                arrTerminal = value;
            }
        }

        public string DepTerminal
        {
            get 
            { 
                return depTerminal; 
            }
            set 
            { 
                depTerminal = value; 
            }
        }

        public FlightStatus FlightStatus
        {
            get 
            { 
                return flightStatus; 
            }
            set 
            { 
                flightStatus = value; 
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string MealType
        {
            get
            {
                return mealType;
            }
            set
            {
                mealType = value;
            }
        }

        public bool ETicketEligible
        {
            get
            {
                return eTicketEligible;
            }
            set
            {
                eTicketEligible = value;
            }
        }

        public string AirlinePNR
        {
            get
            {
                return airlinePNR;
            }
            set
            {
                airlinePNR = value;
            }
        }

        public string Craft
        {
            get
            {
                return craft;
            }
            set
            {
                craft = value;
            }
        }

        public bool StopOver
        {
            get
            {
                return stopOver;
            }
            set
            {
                stopOver = value;
            }
        }

        public int Stops
        {
            get
            {
                return stops;
            }
            set
            {
                stops = value;
            }
        }

        public int Mile
        {
            get
            {
                return mile;
            }
            set
            {
                mile = value;
            }
        }

        [System.Xml.Serialization.XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan Duration
        {
            get
            {
                return duration;
            }
            set
            {
                duration = value;
            }
        }
        [System.Xml.Serialization.XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan GroundTime
        {
            get
            {
                return groundTime;
            }
            set
            {
                groundTime = value;
            }
        }
        [System.Xml.Serialization.XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan AccumulatedDuration
        {
            get
            {
                return accumulatedDuration;
            }
            set
            {
                accumulatedDuration = value;
            }
        }

        public string UapiSegmentRefKey
        {
            get
            {
                return uapiSegmentRefKey;
            }
            set
            {
                uapiSegmentRefKey = value;
            }
        }
        /// <summary>
        /// Gets Identity of flight in format 
        /// Airline code + flight number + departure date time in format "ddMMMyyyyHHmm". 
        /// eg. BA030227MAR20070910 for (BA 0302 27MAR2007 1410);
        /// </summary>
        public string FlightKey
        {
            get
            {
                StringBuilder key = new StringBuilder();
                key.Append(airline);
                //if (flightNumber.Length == 3)
                //{
                //    key.Append("0");
                //}
                key.Append(flightNumber.PadLeft(4, '0'));
                key.Append(departureTime.ToString("dd"));
                key.Append(departureTime.ToString("MMM").ToUpper());
                key.Append(departureTime.ToString("yyyyHHmm"));
                return key.ToString();
            }
        }

        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public Hashtable UAPIReservationValues
        {
            get { return uapiReservationValues; }
            set { uapiReservationValues = value; }
        }

        /// <summary>
        /// Stores fare types for Fly Dubai like Pay To Change or Free To Change or Basic.
        /// </summary>
        public string SegmentFareType
        {
            get { return segmentFareType; }
            set { segmentFareType = value; }
        }
        /// <summary>
        /// LCC Segment Price for TBO Air
        /// </summary>
        public PriceAccounts SegmentPrice
        {
            get { return segmentPrice; }
            set { segmentPrice = value; }
        }

        /// <summary>
        /// LCC Segment Fare Breakdown for TBO Air
        /// </summary>
        public List<Fare> SegmentFareBreakdown
        {
            get { return segmentFareBreakdown; }
            set { segmentFareBreakdown = value; }
        }

        /// <summary>
        /// LCC Segment Fare Rules
        /// </summary>
        public List<FareRule> SegmentFareRule
        {
            get { return segmentFareRule; }
            set { segmentFareRule = value; }
        }

        /// <summary>
        /// Tour Code returned for Negotiated Fare
        /// </summary>
        public string TourCode
        {
            get { return tourCode; }
            set { tourCode = value; }
        }

        /// <summary>
        /// Row status for active or inactive
        /// </summary>
        int rowStatus = 1;
        public int RowStatus
        {
            get { return rowStatus; }
            set { rowStatus = value; }
        }
        #endregion


        public void Save()
        {
            //Trace.TraceInformation("FlightInfo.Save entered : ");
            if (flightId == 0)
            {
                throw new ArgumentException("FlightId should have a value", "flightId");
            }
            if (airline == null || airline.Length == 0)
            {
                throw new ArgumentException("Airline should have a value", "airline");
            }
            if (origin.AirportCode == null || origin.AirportCode.Length == 0)
            {
                throw new ArgumentException("Origin ariport code should have a value", "origin.AirportCode");
            }
            if (destination.AirportCode == null || destination.AirportCode.Length == 0)
            {
                throw new ArgumentException("Destination ariport code should have a value", "destination.AirportCode");
            }
            if (departureTime == DateTime.MinValue)
            {
                throw new ArgumentException("departure time should have a value", "departureTime");
            }
            if (arrivalTime == DateTime.MinValue)
            {
                throw new ArgumentException("arrival time should have a value", "arrivalTime");
            }
            if (bookingClass == null || bookingClass.Length == 0)
            {
                throw new ArgumentException("bookingClasss should have a value", "bookingClass");
            }
            SqlParameter[] paramList = new SqlParameter[28];
            paramList[0] = new SqlParameter("@flightId", flightId);
            paramList[1] = new SqlParameter("@airline", airline);
            paramList[2] = new SqlParameter("@depAirport", origin.AirportCode);
            paramList[3] = new SqlParameter("@arrAirport", destination.AirportCode);
            paramList[4] = new SqlParameter("@flightNumber", flightNumber);
            paramList[5] = new SqlParameter("@depDateTime", departureTime);
            paramList[6] = new SqlParameter("@arrDateTime", arrivalTime);
            paramList[7] = new SqlParameter("@class", bookingClass);
            paramList[8] = new SqlParameter("@cabinClass", cabinClass);
            paramList[9] = new SqlParameter("@arrTerminal", arrTerminal);
            paramList[10] = new SqlParameter("@depTerminal", depTerminal);
            paramList[11] = new SqlParameter("@flightStatus", (int)flightStatus);
            paramList[12] = new SqlParameter("@status", status);
            paramList[13] = new SqlParameter("@eTicketEligible", eTicketEligible);
            paramList[14] = new SqlParameter("@duration", Util.GetHHMM(duration));
            paramList[15] = new SqlParameter("@groundTime", Util.GetHHMM(groundTime));
            paramList[16] = new SqlParameter("@accumulatedDuration", Util.GetHHMM(accumulatedDuration));
            paramList[17] = new SqlParameter("@stopOver", stopOver);
            paramList[18] = new SqlParameter("@stop", stops);
            paramList[19] = new SqlParameter("@craft", craft);
            paramList[20] = new SqlParameter("@mile", group); //Stores group no for segment Onward = 0, Inward = 1
            paramList[21] = new SqlParameter("@airlinePNR", airlinePNR);

            if (operatingCarrier == null || operatingCarrier.Length == 0)
            {
                paramList[22] = new SqlParameter("@operatingCarrier", DBNull.Value);
            }
            else
            {
                paramList[22] = new SqlParameter("@operatingCarrier", operatingCarrier);
            }

            if (conjunctionNo == null || conjunctionNo == "")
            {
                paramList[25] = new SqlParameter("@conjunctionNo",DBNull.Value);
            }
            else
            {
                paramList[25] = new SqlParameter("@conjunctionNo", conjunctionNo);
            }

            if (segmentFareType != null)
            {
                paramList[26] = new SqlParameter("@fareType", segmentFareType);
            }
            else
            {
                paramList[26] = new SqlParameter("@fareType", DBNull.Value);
            }

            if (segmentId > 0)
            { 
                //Trace.TraceInformation("FlightInfo.Save : Update section entered");
                if (segmentId <= 0)
                {
                    throw new ArgumentException("segmentId must have a positive integer value", "segmentId");
                }
                if (lastModifiedBy <= 0)
                {
                    throw new ArgumentException("lastModifiedBy must have a positive integer value", "lastModifiedBy");
                }
                paramList[23] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramList[24] = new SqlParameter("@segmentId", segmentId);
                paramList[25] = new SqlParameter("@rowStatus", rowStatus);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateFlightInfo, paramList);
                //Trace.TraceInformation("FlightInfo.Save : Update section exiting. rowsAffected = " + rowsAffected);
            }
            else
            {
                //Trace.TraceInformation("FlightInfo.Save : Add new section entered");
                if (createdBy <= 0)
                {
                    throw new ArgumentException("createdBy must have a positive integer value", "createdBy");
                }
                paramList[23] = new SqlParameter("@createdBy", createdBy);
                paramList[24] = new SqlParameter("@segmentId", SqlDbType.Int);
                paramList[25] = new SqlParameter("@rowStatus", rowStatus);
                paramList[24].Direction = ParameterDirection.Output;
               
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddFlightInfo, paramList);
                segmentId = (int)paramList[24].Value;
                //Trace.TraceInformation("FlightInfo.Save : Add new section exiting segmentId = " + segmentId);
            }
            //Trace.TraceInformation("FlightInfo.Save : exiting segmentId = " + segmentId );
        }

        public static void RefreshFlightInfoData(FlightInfo[] segments)
        {
            //Trace.TraceInformation("FlightInfo.RefreshFlightInfoData entered");

            SqlConnection conn = DBGateway.GetConnection();
            SqlDataAdapter adapter = null;
            DataTable table = new DataTable();

            try
            {
                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@flightId", segments[0].flightId);

                SqlCommand cmd = new SqlCommand(SPNames.GetSelectedColumnsFromFlightInfoAgainstFlightId, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(paramlist);
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(table);
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    table.Rows[i]["flightNum"] = segments[i].FlightNumber;
                    table.Rows[i]["airlineCode"] = segments[i].airline;
                    table.Rows[i]["depAirport"] = segments[i].origin.CityCode;
                    table.Rows[i]["arrAirport"] = segments[i].destination.AirportCode;
                    table.Rows[i]["depDateTime"] = segments[i].departureTime;
                    table.Rows[i]["arrDateTime"] = segments[i].arrivalTime;
                    table.Rows[i]["class"] = segments[i].BookingClass;
                    table.Rows[i]["cabinClass"] = segments[i].cabinClass;
                    table.Rows[i]["status"] = segments[i].status;
                    table.Rows[i]["flightStatus"] = segments[i].FlightStatus;
                    table.Rows[0]["lastModifiedOn"] = DateTime.UtcNow;
                    table.Rows[0]["lastModifiedBy"] = segments[i].lastModifiedBy;
                }

                SqlCommandBuilder command = new SqlCommandBuilder(adapter);
                adapter.Update(table);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.ChangeRequest, Severity.High, 0, "Error: while updating Ticket in case of reissuance.| " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            }
            finally
            {
                conn.Close();
                adapter.Dispose();
                table.Dispose();
            }
            //Trace.TraceInformation("FlightInfo.RefreshFlightInfoData exited");
        }
        /// <summary>
        /// Method Gets segment information for particular filght Id
        /// </summary>
        /// <param name="flightId">flightId</param>
        /// <returns>FlightInfo[] contains information for all segments</returns>
        public static FlightInfo[] GetSegments(int flightId)
        {
            //Trace.TraceInformation("FlightInfo.GetSegments entered : flightId = " + flightId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            DataSet dataSet = DBGateway.FillSP(SPNames.GetSegmentsInfo, paramList);
            FlightInfo[] segments = new FlightInfo[dataSet.Tables[0].Rows.Count];
            int count = 0;
            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                segments[count] = new FlightInfo();
                if (dr["accumulatedDuration"] != DBNull.Value)
                {
                    segments[count].accumulatedDuration = Util.GetTimeSpan(dr["accumulatedDuration"].ToString().Trim());
                }
                segments[count].airline = dr["airlineCode"].ToString();
                segments[count].arrivalTime = Convert.ToDateTime(dr["arrDateTime"]);
                if (dr["arrTerminal"] != DBNull.Value)
                {
                    segments[count].arrTerminal = dr["arrTerminal"].ToString();
                }
                segments[count].bookingClass = dr["class"].ToString().Trim();
                if (dr["cabinClass"] != DBNull.Value)
                {
                    segments[count].cabinClass= dr["cabinClass"].ToString();
                }
                if (dr["craft"] != DBNull.Value)
                {
                    segments[count].craft = dr["craft"].ToString();
                }
                segments[count].departureTime = Convert.ToDateTime(dr["depDateTime"]);
                if (dr["depTerminal"] != DBNull.Value)
                {
                    segments[count].depTerminal = dr["depTerminal"].ToString();
                }
                segments[count].destination = new Airport(dr["arrAirport"].ToString());
                if (dr["duration"] != DBNull.Value)
                {
                    segments[count].duration = Util.GetTimeSpan(dr["duration"].ToString().Trim ());
                }

                segments[count].eTicketEligible = Convert.ToBoolean(dr["eTicketEligible"]);
                segments[count].flightId = flightId;
                if (dr["flightNum"] != DBNull.Value)
                {
                    segments[count].flightNumber = dr["flightNum"].ToString();
                }
                if (dr["flightStatus"] != DBNull.Value)
                {
                    segments[count].flightStatus = (FlightStatus)((int)dr["flightStatus"]);
                }
                if (dr["status"] != DBNull.Value)
                {
                    segments[count].status = (string)dr["status"];
                }
                else
                {
                    segments[count].status = string.Empty;
                }
                if (dr["groundTime"] != DBNull.Value)
                {
                    segments[count].groundTime = Util.GetTimeSpan(dr["groundTime"].ToString().Trim());
                }
                if (dr["mile"] != DBNull.Value)//Stores Group number for segments, i.e. all Onward = 0, all Inward = 1. changed on 01 June 2015
                {
                    segments[count].group = Convert.ToInt32(dr["mile"]);
                }
                if (dr["airlinePNR"] != DBNull.Value)
                {
                    segments[count].airlinePNR = Convert.ToString(dr["airlinePNR"]);
                }
                else
                {
                    segments[count].airlinePNR = string.Empty;
                }
                if (dr["operatingCarrier"] != DBNull.Value)
                {
                    segments[count].operatingCarrier = Convert.ToString(dr["operatingCarrier"]);
                }
                else
                {
                    segments[count].operatingCarrier = string.Empty;
                }
                segments[count].segmentId = Convert.ToInt32(dr["segmentId"]);
                segments[count].origin = new Airport(dr["depAirport"].ToString());
                if (dr["stop"] != DBNull.Value)
                {
                    segments[count].stops = Convert.ToInt32(dr["stop"]);
                }
                if (dr["conjunctionNo"] != DBNull.Value)
                {
                    segments[count].conjunctionNo =Convert.ToString(dr["conjunctionNo"]);
                }

                if (dr["FareType"] != DBNull.Value)
                {
                    segments[count].segmentFareType = dr["FareType"].ToString();
                }

                segments[count].stopOver = Convert.ToBoolean(dr["stopOver"]);
                segments[count].createdBy = Convert.ToInt32(dr["createdBy"]);
                segments[count].createdOn = Convert.ToDateTime(dr["createdOn"]);
                segments[count].lastModifiedBy = Convert.ToInt32(dr["lastModifiedBy"]);
                segments[count].lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
                count++;
            }
            //Trace.TraceInformation("FlightInfo.GetSegments exiting : Rows=" + dataSet.Tables[0].Rows.Count);
            return segments;
        }

        public static FlightInfo Copy(FlightInfo flight)
        {
            FlightInfo newFlight = new FlightInfo();
            newFlight.accumulatedDuration = flight.accumulatedDuration;
            newFlight.airline = flight.airline;
            newFlight.airlinePNR = flight.airlinePNR;
            newFlight.arrivalTime = flight.arrivalTime;
            newFlight.arrTerminal = flight.arrTerminal;
            newFlight.availabiLity = flight.availabiLity;
            newFlight.bookingClass = flight.bookingClass;
            newFlight.cabinClass= flight.cabinClass;
            newFlight.craft = flight.craft;
            newFlight.createdBy = flight.createdBy;
            newFlight.createdOn = flight.createdOn;
            newFlight.departureTime = flight.departureTime;
            newFlight.depTerminal = flight.depTerminal;
            newFlight.destination = flight.destination;
            newFlight.duration = flight.duration;
            newFlight.eTicketEligible = flight.eTicketEligible;
            newFlight.flightId = flight.flightId;
            newFlight.flightNumber = flight.flightNumber;
            newFlight.flightStatus = flight.flightStatus;
            newFlight.groundTime = flight.groundTime;
            newFlight.lastModifiedBy = flight.lastModifiedBy;
            newFlight.lastModifiedOn = flight.lastModifiedOn;
            newFlight.mealType = flight.mealType;
            newFlight.mile = flight.mile;
            newFlight.operatingCarrier = flight.operatingCarrier;
            newFlight.origin = flight.origin;
            newFlight.segmentId = flight.segmentId;
            newFlight.status = flight.status;
            newFlight.stopOver = flight.stopOver;
            newFlight.stops = flight.stops;
            if(flight.fareInfoKey!=null)newFlight.fareInfoKey = flight.fareInfoKey;
            /*if (flight.group !=null) */newFlight.Group= flight.Group;
            if (!string.IsNullOrEmpty(flight.UapiDepartureTime)) newFlight.UapiDepartureTime = flight.UapiDepartureTime;
            if (!string.IsNullOrEmpty(flight.UapiArrivalTime)) newFlight.uapiArrivalTime= flight.uapiArrivalTime;
            if (flight.UapiSegmentRefKey != null) newFlight.UapiSegmentRefKey = flight.UapiSegmentRefKey;
            newFlight.UAPIReservationValues = flight.UAPIReservationValues;
            return newFlight;
        }

        public override string ToString()
        {
            StringBuilder flightString = new StringBuilder(100);
            flightString.Append(segmentId);
            flightString.Append(" ");
            flightString.Append(airline);
            flightString.Append(" ");
            flightString.Append(flightNumber);
            flightString.Append(" ");
            flightString.Append(bookingClass);
            flightString.Append(departureTime.ToString(" ddMMM "));
            flightString.Append(origin.AirportCode);
            flightString.Append(destination.AirportCode);
            flightString.Append(departureTime.ToString(" HHmm "));
            flightString.Append(arrivalTime.ToString("HHmm "));
            flightString.Append(eTicketEligible ? "E " : "  ");
            flightString.Append(airlinePNR);
            return flightString.ToString();
        }
    }

    /// <summary>
    /// Serializes TimeSpan for XML
    /// </summary>
    public class XmlTimeSpan
    {
        private TimeSpan m_internal = TimeSpan.Zero;

        public XmlTimeSpan()
            : this(TimeSpan.Zero)
        {
        }

        public XmlTimeSpan(TimeSpan input)
        {
            m_internal = input;
        }

        public static implicit operator TimeSpan(XmlTimeSpan input)
        {
            return (input != null) ? input.m_internal : TimeSpan.Zero;
        }

        // Alternative to the implicit operator TimeSpan(XmlTimeSpan input)
        public TimeSpan ToTimeSpan()
        {
            return m_internal;
        }

        public static implicit operator XmlTimeSpan(TimeSpan input)
        {
            return new XmlTimeSpan(input);
        }

        // Alternative to the implicit operator XmlTimeSpan(TimeSpan input)
        public void FromTimeSpan(TimeSpan input)
        {
            this.m_internal = input;
        }

        [XmlText]
        public string Value
        {
            get
            {
                return XmlConvert.ToString(m_internal);
            }
            set
            {
                m_internal = XmlConvert.ToTimeSpan(value);
            }
        }
    }
}
