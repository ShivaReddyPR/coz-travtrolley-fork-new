﻿using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Data;
using System.Data.SqlClient;

namespace CT.BookingEngine
{
    public class BaggageMaster
    {

        #region Variables
        int baggageId;
        BookingSource sourceId;
        string code;
        decimal price;
        string currency;       
        bool isActive;
        bool isDomestic;
        int createdBy;
        #endregion
        #region Properties
        public int BaggageId
        {
            get
            {
                return baggageId;
            }
            set
            {
                baggageId = value;
            }
        }
        public BookingSource SourceId
        {
            get
            {
                return sourceId;
            }
            set
            {
                sourceId = value;
            }
        }
        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                code = value;
            }
        }
        public decimal Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }
        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        public bool IsDomestic
        {
            get
            {
                return isDomestic;
            }
            set
            {
                isDomestic = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        #endregion
        #region Constructors
        public BaggageMaster()
        {
            baggageId = -1;
        }
        public BaggageMaster(int id)
        {
            baggageId = id;
            GetDetails(baggageId);
        }
        #endregion
        #region Public Methods
        public void Save()
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[10];
                paramlist[0] = new SqlParameter("@P_BaggageId",baggageId);
                paramlist[1] = new SqlParameter("@P_BaggageSourceId",sourceId);
                paramlist[2] = new SqlParameter("@P_BaggageCode",code);
                paramlist[3] = new SqlParameter("@P_BaggagePrice",price);
                paramlist[4] = new SqlParameter("@P_BaggageCurrency",currency);
                paramlist[5] = new SqlParameter("@P_IsActive",isActive);
                paramlist[6] = new SqlParameter("@P_IsDomestic",isDomestic);
                paramlist[7] = new SqlParameter("@P_CreatedBy",createdBy);
                paramlist[8] = new SqlParameter("@P_MSG_TYPE",SqlDbType.NVarChar,10);
                paramlist[8].Direction = ParameterDirection.Output;
                paramlist[9] = new SqlParameter("@P_MSG_text", SqlDbType.NVarChar, 100);
                paramlist[9].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("USP_BKE_SOURCE_BAGGAGE_ADD_UPDATE", paramlist);
                string messagetype = Utility.ToString(paramlist[8].Value);
                if (messagetype == "E")
                {
                    string message = Utility.ToString(paramlist[9].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        #region Private Methods
        private void GetDetails(int id)
        {
            try
            {
                DataTable dt = GetData(id);
                UpdataBusinessData(dt);
            }
            catch
            {
                throw;
            }
        }
        private DataTable GetData(int id)
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@P_BaggageId", id);
                return DBGateway.FillDataTableSP("USP_BKE_SOURCE_BAGGAGE_GET_DATA", paramlist);
            }
            catch
            {
                throw;
            }
        }
        private void UpdataBusinessData(DataTable dt)
        {
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["BaggageId"] != null)
                        {
                            baggageId = Utility.ToInteger(dr["BaggageId"]);
                        }
                        if (dr["BaggageSourceId"] != null)
                        {
                            sourceId = (BookingSource)Utility.ToInteger(dr["BaggageSourceId"]);                          
                        }
                        if (dr["BaggageCode"] != null)
                        {
                            code = Utility.ToString(dr["BaggageCode"]);
                        }
                        if (dr["BaggagePrice"] != null)
                        {
                            price = Utility.ToDecimal(dr["BaggagePrice"]);
                        }
                        if (dr["BaggageCurrency"] != null)
                        {
                            currency = Utility.ToString(dr["BaggageCurrency"]);
                        }
                        if (dr["IsActive"] != null)
                        {
                            isActive = Utility.ToBoolean(dr["IsActive"]);
                        }
                        if (dr["IsDomestic"] != null)
                        {
                            isDomestic = Utility.ToBoolean(dr["IsDomestic"]);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            
        }
        #endregion
        #region Static Methods
        public static DataTable GetList(ListStatus status,RecordStatus rstatus)
        {
            try
            {
                SqlParameter[] paramslist= new SqlParameter[2];
                paramslist[0] = new SqlParameter("@P_LISTSTATUS", status);
                paramslist[1] = new SqlParameter("@P_RECORD_STATUS", rstatus);
                return DBGateway.FillDataTableSP("USP_BKE_SOURCE_BAGGAGE_GET_LIST", paramslist);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
