using System;
using System.Collections.Generic;
using CT.Core;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Collections;

namespace CT.BookingEngine
{
    public enum HotelSourceGeographyType
    {
        Domestic=1,
        International=2,
        Both=3
    }
    public class HotelSource
    {
        # region private variables
        int hotelSourceId;
        string hotelSourceName;
        decimal agenctCommission;
        decimal ourCommission;
        CommissionType commissionTypeId;
        FareType fareTypeId;
        bool domestic;
        HotelSourceGeographyType type;
        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;

        //stattic data for cache.
        private static Dictionary<string,HotelSource> sourceList=new Dictionary<string,HotelSource>();
        # endregion

        # region public properties
        /// <summary>
        /// Unique hotel source ID
        /// </summary>
        public int HotelSourceId
        {
            get { return hotelSourceId; }
            set { hotelSourceId = value; }
        }
        /// <summary>
        /// Name of the Hotel Source
        /// </summary>
        public string HotelSourceName
        {
            get { return hotelSourceName; }
            set { hotelSourceName = value; }
        }
        /// <summary>
        /// Agenct commission
        /// </summary>
        public decimal AgentCommission
        {
            get { return agenctCommission; }
            set { agenctCommission = value; }
        }
        /// <summary>
        /// Our commission.
        /// </summary>
        public decimal OurCommission
        {
            get { return ourCommission; }
            set { ourCommission = value; }
        }

        /// <summary>
        /// Our commission.
        /// </summary>
        public CommissionType CommissionTypeId
        {
            get { return commissionTypeId; }
            set { commissionTypeId = value; }
        }
        /// <summary>
        /// fare Type
        /// </summary>
        public FareType FareTypeId
        {
            get { return fareTypeId; }
            set { fareTypeId = value; }
        }
        /// <summary>
        /// Domestic or not
        /// </summary>
        public bool Domestic
        {
            get { return domestic; }
            set { domestic = value; }
        }
        /// <summary>
        /// domestic or International
        /// </summary>
        public HotelSourceGeographyType GeoType
        {
            get { return type; }
            set { type = value; }
        }
        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        # endregion

        # region Methods

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
            //Trace.TraceInformation("HotelSource.Save entered.");
            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@hotelSource", hotelSourceName);
            paramList[1] = new SqlParameter("@agentComm", agenctCommission);
            paramList[2] = new SqlParameter("@ourComm", ourCommission);
            paramList[3] = new SqlParameter("@commissionTypeId", commissionTypeId);
            paramList[4] = new SqlParameter("@CommissionType", Enum.Parse(typeof(CommissionType), commissionTypeId.ToString(), true).ToString()); // enum type value parsing
            paramList[5] = new SqlParameter("@fareTypeId", fareTypeId);
            paramList[6] = new SqlParameter("@fareType", Enum.Parse(typeof(FareType), fareTypeId.ToString(), true).ToString());
            paramList[7] = new SqlParameter("@domestic", domestic);
            paramList[8] = new SqlParameter("@hotelSourceId", SqlDbType.Int);
            paramList[8].Direction = ParameterDirection.Output;
            paramList[9] = new SqlParameter("@geoType", (int)type);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHotelSource, paramList);
            //Trace.TraceInformation("HotelSource.Save exiting");
            return (int)paramList[3].Value;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="hSourceId"></param>
        public void Load(int hSourceId)
        {
            //Trace.TraceInformation("HotelSource.Load entered : hotelSourceId = " + hSourceId);
            if (hSourceId <= 0)
            {
                throw new ArgumentException("HotelSourceId Id should be positive integer", "hoteSourceId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelSourceId", hSourceId);

            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelSourceById, paramList,connection);
            using (DataTable dtHotelSource = DBGateway.FillDataTableSP(SPNames.GetHotelSourceById, paramList))
            {
                //if (data.Read())
                if (dtHotelSource != null && dtHotelSource.Rows.Count > 0)
                {
                    DataRow data = dtHotelSource.Rows[0];
                    hotelSourceId = Convert.ToInt32(data["hotelSourceId"]);
                    hotelSourceName = Convert.ToString(data["hotelSource"]);
                    agenctCommission = Convert.ToDecimal(data["agentComm"]);
                    ourCommission = Convert.ToDecimal(data["ourComm"]);
                    commissionTypeId = (CommissionType)data["commissionTypeId"];
                    fareTypeId = (FareType)data["fareTypeId"];
                    domestic = Convert.ToBoolean(data["domestic"]);
                    type = (HotelSourceGeographyType)Convert.ToInt16(data["geoType"]);
                    //data.Close();
                    //connection.Close();
                    //Trace.TraceInformation("HotelSource.Load exiting :" + hSourceId.ToString());
                }
                else
                {
                    //data.Close();
                    //connection.Close();
                    //Trace.TraceInformation("HotelSource.Load exiting : hotelSourceId does not exist.hotelSourceId = " + hotelSourceId.ToString());
                    throw new ArgumentException("Hotel Source id does not exist in database");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hSourceId"></param>
        public void Update(int hSourceId)
        {
            //Trace.TraceInformation("HotelSource.Update entered : hotelSourceId = " + hSourceId);
            if (hSourceId <= 0)
            {
                throw new ArgumentException("HotelSourceId Id should be positive integer", "hotelSourceId");
            }
            // if the static data already contains particular source.Then remove it.
            if (sourceList.ContainsKey(hotelSourceName))
            {
                sourceList.Remove(hotelSourceName);
            }
            SqlParameter[] paramList = new SqlParameter[11];
            paramList[0] = new SqlParameter("@hotelSourceId", hSourceId);
            paramList[1] = new SqlParameter("@hotelSource", hotelSourceName);
            paramList[2] = new SqlParameter("@agentComm", agenctCommission);
            paramList[3] = new SqlParameter("@ourComm", ourCommission);
            paramList[4] = new SqlParameter("@commissionTypeId", commissionTypeId);
            paramList[5] = new SqlParameter("@commissionType", Enum.Parse(typeof(CommissionType), commissionTypeId.ToString(), true).ToString());
            paramList[6] = new SqlParameter("@fareTypeId", fareTypeId);
            paramList[7] = new SqlParameter("@fareType", Enum.Parse(typeof(FareType), fareTypeId.ToString(), true).ToString());
            paramList[8] = new SqlParameter("@domestic", domestic);
            paramList[9] = new SqlParameter("@geoType", (int)type);
            paramList[10] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelSource, paramList);
            //Trace.TraceInformation("HotelSource.Update exiting");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hSourceName"></param>
        public void Load(string hSourceName)
        {
            //Trace.TraceInformation("HotelSource.Load entered : hotelSourceName = " + hSourceName);
            if (hSourceName.Length == 0)
            {
                throw new ArgumentException("HotelSourceName should be having Information", "hSourceName");
            }
            // if it contains static data then retriave it. else load from DB
            if (sourceList != null && sourceList.ContainsKey(hSourceName))
            {
                HotelSource sourceInfo = new HotelSource();
                sourceInfo = sourceList[hSourceName];
                hotelSourceId = sourceInfo.HotelSourceId;
                hotelSourceName = hSourceName;
                agenctCommission = sourceInfo.AgentCommission;
                commissionTypeId = sourceInfo.CommissionTypeId;
                ourCommission = sourceInfo.OurCommission;
                fareTypeId = sourceInfo.FareTypeId;
                domestic = sourceInfo.Domestic;
                type = sourceInfo.GeoType;
            }
            else
            {
                //SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@hotelSourceName", hSourceName);
                //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelSourceByName, paramList,connection );
                using (DataTable dtHotelSource = DBGateway.FillDataTableSP(SPNames.GetHotelSourceByName, paramList))
                {
                    //if (data.Read())
                    if(dtHotelSource != null && dtHotelSource.Rows.Count>0)
                    {
                        //foreach (DataRow data in dtHotelSource.Rows)
                        {
                            DataRow data = dtHotelSource.Rows[0];
                            hotelSourceId = Convert.ToInt32(data["hotelSourceId"]);
                            hotelSourceName = data["hotelSource"].ToString();
                            agenctCommission = Convert.ToDecimal(data["agentComm"]);
                            ourCommission = Convert.ToDecimal(data["ourComm"]);
                            commissionTypeId = (CommissionType)data["commissionTypeId"];
                            fareTypeId = (FareType)data["fareTypeId"];
                            domestic = Convert.ToBoolean(data["domestic"]);
                            type = (HotelSourceGeographyType)Convert.ToInt16(data["geoType"]);
                            //data.Close();
                            //connection.Close();
                            sourceList.Add(hSourceName, this);
                            //Trace.TraceInformation("HotelSource.Load exiting :" + hSourceName);
                        }
                    }
                    else
                    {
                        //data.Close();
                        //connection.Close();
                        //Trace.TraceInformation("HotelSource.Load exiting : hotelSourceName does not exist.hotelSourceName = " + hotelSourceName);
                        throw new ArgumentException("Hotel Source Name does not exist in database");
                    }
                }
            }
        }

        public void Load(string hSourceName, int agencyId)
        {
            Load(hSourceName);
            //in case of admin agency id is 0 or 1(selfAgencyId).
            if (agencyId <= 1)
            {
                return;
            }
            else
            {
                HotelAgents hAgents = new HotelAgents();
                hAgents.LoadAgentCommission(this.HotelSourceId, agencyId);
                if (hAgents.AgencyId > 1)
                {
                    if (fareTypeId == FareType.Net)
                    {
                        this.ourCommission = hAgents.Commission;
                    }
                    else if (fareTypeId == FareType.Published)
                    {
                        this.agenctCommission = hAgents.Commission;
                    }
                }
            }
        }

        public List<int> Load()
        {
            //Trace.TraceInformation("HotelSource.Load entered : ");
            List<int> idList = new List<int>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAllHotelSourceId, paramList,connection );
            while (data.Read())
            {
                idList.Add(Convert.ToInt32(data["hotelSourceId"]));
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("HotelSource.Load exiting :");
            return idList;
        }

        public static SortedList GetAllHotelSourceList()
        {
            //Trace.TraceInformation("HotelSource.GetAllHotelSourceList entered");
            SortedList sourceList = new SortedList();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAllHotelSourceList, paramList, connection );
            while (data.Read())
            {
                sourceList.Add((string)data["hotelSource"], (int)data["hotelSourceId"]);
            }
            data.Close();
            connection.Close();
            sourceList.TrimToSize();
            //Trace.TraceInformation("HotelSource.GetAllHotelSourceList exited : number of source returned = " + sourceList.Count);
            return sourceList;
        }
        /// <summary>
        /// This Method is used to get list of sources based on geoType.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<string> Load(HotelSourceGeographyType gtype)
        {
            //Trace.TraceInformation("HotelSource.Load entered");
            List<string> sourceList = new List<string>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@geoType", (int)gtype);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelSourceOnGeoType, paramList, connection );

            while (data.Read())
            {
                sourceList.Add((string)data["hotelSource"]);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("HotelSource.Load exited : number of source returned = " + sourceList.Count);
            return sourceList;
        }
        /// <summary>
        /// This Method is used to get GimminixSuppliers
        /// </summary>
        /// <returns>returns of gimmonixSuppliers datatable</returns>
        public DataTable LoadGimmonixSuppliers()
        {
            try
            {
                return LoadOfflineSuppliers(true, true);
                //SqlParameter[] paramList = new SqlParameter[0];
                //return DBGateway.FillDataTableSP(SPNames.GetGimmonixHotelSources, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at LoadGimmonixSuppliers():due to reason is:" + ex.ToString(), "");
                throw ex;
            }
        }
        /// <summary>
        /// This Method is used to get GimminixSuppliers
        /// </summary>
        /// <returns>returns of gimmonixSuppliers datatable</returns>
        public DataTable LoadOfflineSuppliers(bool isGimmonix,bool isOnlineSource)
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@P_isGimmonix", isGimmonix));
                paramList.Add(new SqlParameter("@P_isOnlineSource", isOnlineSource));
                return DBGateway.FillDataTableSP(SPNames.GetGimmonixHotelSources, paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at LoadGimmonixSuppliers():due to reason is:" + ex.ToString(), "");
                throw ex;
            }
        }

        #endregion
    }
}
