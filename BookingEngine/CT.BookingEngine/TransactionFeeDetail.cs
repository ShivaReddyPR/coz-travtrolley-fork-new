namespace CT.BookingEngine
{
    public class TransactionFeeDetail
    {
        /// <summary>
        /// Transaction Fee. If IsPercentage is true then This is percentage of base fare.
        /// </summary>
        public decimal Amount;
        /// <summary>
        /// Min transaction fee. If txn fee is less than MinAmount then MinAmount will be applicable Txn fee.
        /// </summary>
        public decimal MinAmount;
        /// <summary>
        /// Max transaction fee. If txn fee is more than MsxAmount then MaxAmount will be applicable Txn fee.
        /// </summary>
        public decimal MaxAmount;
        /// <summary>
        /// If true then the amount is percentage of base fare.
        /// </summary>
        public bool IsPercentage;

        /// <summary>
        /// Gets the Transaction applicable for the given baseFare.
        /// </summary>
        /// <param name="baseFare">base fare for the passenger.</param>
        /// <returns></returns>
        public decimal GetTransactionFee(decimal baseFare)
        {
            decimal txnFee;
            if (IsPercentage)
            {
                // Assumption: if IsPercentage is true then Amount is the multiplication factor of baseFare.
                txnFee = baseFare * Amount;
            }
            else
            {
                txnFee = Amount;
            }

            if (txnFee < MinAmount)
            {
                txnFee = MinAmount;
            }
            else if (txnFee > MaxAmount)
            {
                txnFee = MaxAmount;
            }
            return txnFee;
        }
    }
}
