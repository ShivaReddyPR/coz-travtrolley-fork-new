﻿#region Referenced Assemblies
using System;
using System.Data;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Collections.Generic;
#endregion

namespace CT.BookingEngine
{

    /// <summary>
    ///  AirlineMinimalFields Header Info Class
    /// </summary>
    /// 
    public class AirlineMinimalFields
    {

        #region Fields and their Properties
        private int _mandateId;
        public int MandateId
        {
            get { return _mandateId; }
            set { _mandateId = value; }
        }

        private int _productId;
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }


        private string _source;
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private string _country;
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }


        private string _airline;
        public string Airline
        {
            get { return _airline; }
            set { _airline = value; }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        private string _flightType;
        public string FlightType
        {
            get { return _flightType; }
            set { _flightType = value; }
        }

        private bool isSavedSuccessfully;

        public bool IsSavedSuccessfully
        {
            get { return isSavedSuccessfully; }
            set { isSavedSuccessfully = value; }
        }


        List<AirlineMandateSourceFields> _mandateSourcesFieldsList;

        public List<AirlineMandateSourceFields> MandateSourcesFieldsList
        {
            get { return _mandateSourcesFieldsList; }
            set { _mandateSourcesFieldsList = value; }
        }
        private string transType;
        public string TransType
        {
            get { return transType; }
            set { transType = value; }

        }


        public AirlineMinimalFields()
        {
            _mandateId = -1;

        }

        public AirlineMinimalFields(long id)
        {
            _mandateId = Convert.ToInt32(id);
            Load(_mandateId);

        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the header details for the supplied mandate Id along with details
        /// </summary>
        /// <param name="mandateId"></param>
        public void Load(int mandateId)
        {

            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_MandateId", mandateId);
            try
            {
                DataTable dtAPISMandateIdData = DBGateway.FillDataTableSP("usp_APISMandateIdGetData", paramList);

                if (dtAPISMandateIdData != null && dtAPISMandateIdData.Rows.Count > 0)
                {
                    foreach (DataRow data in dtAPISMandateIdData.Rows)
                    {
                        if (data["MandateId"] != DBNull.Value)
                        {
                            _mandateId = Convert.ToInt32(data["MandateId"]);
                        }
                        if (data["ProductId"] != DBNull.Value)
                        {
                            _productId = Convert.ToInt32(data["ProductId"]);
                        }
                        if (data["Source"] != DBNull.Value)
                        {
                            _source = Convert.ToString(data["Source"]);
                        }
                        if (data["Country"] != DBNull.Value)
                        {
                            _country = Convert.ToString(data["Country"]);
                        }
                        if (data["Airline"] != DBNull.Value)
                        {
                            _airline = Convert.ToString(data["Airline"]);
                        }
                        if (data["Status"] != DBNull.Value)
                        {
                            _status = Convert.ToString(data["Status"]);
                        }
                        if (data["TransType"] != DBNull.Value)
                        {
                            transType = Convert.ToString(data["TransType"]);
                        }
                        if (data["FlightType"] != DBNull.Value)
                        {
                            _flightType = Convert.ToString(data["FlightType"]);
                        }

                        AirlineMandateSourceFields mandateFields = new AirlineMandateSourceFields();
                        _mandateSourcesFieldsList = mandateFields.Load(_mandateId);
                    }
                }


            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 0, "(APISMandate)Failed to Load Mandate Fields. Error:" + ex.ToString(), "");
            }
        }


        /// <summary>
        /// Gets the APIS MandateSources with respect to Flight Product
        /// </summary>
        /// <param name="product"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static DataTable GetFlightAPISMandateSources(ProductType product, string status)
        {
            DataTable dtMandateSources = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_ProductId", (int)product);
                paramList[1] = new SqlParameter("@P_Status", status);
                dtMandateSources = DBGateway.FillDataTableSP("usp_GetAllAPISMandateSources", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(APISMandate)Failed to read APIS Mandate sources. Error:" + ex.ToString(), "");
            }
            return dtMandateSources;
        }

        /// <summary>
        /// Method to retrieve mandatory fields for a Source. Pass either airlineCode or CountryCode.
        /// If you pass both then countryCode will be considered and will return mandatory fields for that region.
        /// Always countryCode will be prioritized if countryCode is supplied irrespective of the airline specified.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="source"></param>
        /// <param name="airlineCode"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public DataTable GetFlightPassengerMandateFields(ProductType product, BookingSource source, string airlineCode, string countryCode, string flightType, string transType)
        {
            DataTable dtPageFields = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_ProductId", (int)product);
                paramList[1] = new SqlParameter("@P_Source", source.ToString());
                paramList[2] = new SqlParameter("@P_AirlineCode", airlineCode);
                if (string.IsNullOrEmpty(countryCode))
                    paramList[3] = new SqlParameter("@P_CountryCode", DBNull.Value);
                else
                    paramList[3] = new SqlParameter("@P_CountryCode", countryCode);
                paramList[4] = new SqlParameter("@P_FlightType", flightType);
                paramList[5] = new SqlParameter("@P_TransType", transType);



                dtPageFields = DBGateway.FillDataTableSP("usp_GetAPISMandateFields", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(APISMandate)Failed to read APIS Mandate fields for " + source.ToString() + ". Error:" + ex.ToString(), "");
            }

            return dtPageFields;
        }

        /// <summary>
        /// Gets the AirSources List
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public static DataTable GetAllAPISAirSources(int product)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_ProductId", product);
            DataTable dtAirSources = DBGateway.FillDataTableSP("usp_GetAPISAirSources", paramList);
            return dtAirSources;
        }

        /// <summary>
        /// Gets the APIS Mandate Fields irrespective of Country or Airline
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllPageFields()
        {
            SqlParameter[] paramList = new SqlParameter[0];

            DataTable pageFields = DBGateway.FillDataTableSP("usp_GetAllAPISMandateFields", paramList);

            return pageFields;
        }

        public static DataTable GetApiMandateSources(int productId, string source)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@P_ProductId", productId);
            paramList[1] = new SqlParameter("@P_Source", source);
            DataTable dt = DBGateway.FillDataTableSP("usp_GetApiMandateSources", paramList);
            return dt;

        }

        /// <summary>
        /// Saves the APIS Header Details as well as Details.
        /// </summary>
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_ProductId", _productId);
                paramList[1] = new SqlParameter("@P_Source", _source);
                if (string.IsNullOrEmpty(_country))
                {
                    paramList[2] = new SqlParameter("@P_Country", DBNull.Value);
                }
                else
                {
                    paramList[2] = new SqlParameter("@P_Country", _country);
                }
                if (string.IsNullOrEmpty(_airline))
                {
                    paramList[3] = new SqlParameter("@P_Airline", DBNull.Value);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_Airline", _airline);
                }
                paramList[4] = new SqlParameter("@P_Status", _status);
                paramList[5] = new SqlParameter("@P_MandateId_Ret", SqlDbType.Int, 200);
                paramList[5].Direction = ParameterDirection.Output;
                paramList[6] = new SqlParameter("@P_MandateId", _mandateId);
                paramList[7] = new SqlParameter("@P_TransType", transType);
                paramList[8] = new SqlParameter("@P_FlightType", _flightType);


                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdateAirlineMinimalFields", paramList);

                if ((count > 0 && paramList[5].Value != DBNull.Value))
                {
                    _mandateId = Convert.ToInt32(paramList[5].Value);

                    try
                    {
                        foreach (AirlineMandateSourceFields sourceField in _mandateSourcesFieldsList)
                        {
                            sourceField.MandateId = _mandateId;
                            sourceField.Save(cmd);
                        }
                        IsSavedSuccessfully = true;


                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 0, "(APISMandate)Failed to save. Error:" + ex.ToString(), "");

                    }
                }


                trans.Commit();
            }
            catch (Exception ex)
            {

                trans.Rollback();
                Audit.Add(EventType.Exception, Severity.High, 0, "(APISMandate)Failed to save. Error:" + ex.ToString(), "");

            }

        }

        #endregion
    }


    /// <summary>
    /// AirlineMinimalFields Details Class
    /// </summary>
    public class AirlineMandateSourceFields
    {

        #region Fields and their Properties
        private int _mfId;
        public int MFId
        {
            get { return _mfId; }
            set { _mfId = value; }
        }

        private int _mandateId;
        public int MandateId
        {
            get { return _mandateId; }
            set { _mandateId = value; }
        }
        private string _status;
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _field;
        public string Field
        {
            get { return _field; }
            set { _field = value; }
        }

        public string _fieldName;
        public string FieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }

        private string _flightType;

        public string FlightType
        {
            get { return _flightType; }
            set { _flightType = value; }

        }

        private string transType;
        public string TransType
        {
            get { return transType; }
            set { transType = value; }

        }


        public AirlineMandateSourceFields()
        {
        }

        #endregion


        /// <summary>
        /// Saves the Mandate Fields against to the Mandate Id
        /// </summary>
        public void Save(SqlCommand cmd)
        {


            try
            {

                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@P_MandateId", _mandateId);
                paramList[1] = new SqlParameter("@P_Field", _field);
                paramList[2] = new SqlParameter("@P_Status", _status);
                paramList[3] = new SqlParameter("@P_MFId", _mfId);
                //paramList[4] = new SqlParameter("@P_IsDomestic",isDomestic);


                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdateAirlineMandateSourceFields", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(APISMandate)Failed to save. Error:" + ex.ToString(), "");

            }
        }

        /// <summary>
        /// Returns the Mandate Fields For the supplied mandateId.
        /// </summary>
        /// <param name="mandateId"></param>
        /// <returns></returns>
        public List<AirlineMandateSourceFields> Load(int mandateId)
        {


            List<AirlineMandateSourceFields> fieldsList = new List<AirlineMandateSourceFields>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_MandateId", mandateId);
            DataTable dtFieldsData = DBGateway.FillDataTableSP("usp_GetAPISMandateFieldsData", paramList);
            if (dtFieldsData != null && dtFieldsData.Rows.Count > 0)
            {
                foreach (DataRow data in dtFieldsData.Rows)
                {
                    AirlineMandateSourceFields objField = new AirlineMandateSourceFields();
                    if (data["MFId"] != DBNull.Value)
                    {
                        objField._mfId = Convert.ToInt32(data["MFId"]);
                    }
                    if (data["MandateId"] != DBNull.Value)
                    {
                        objField._mandateId = Convert.ToInt32(data["MandateId"]);
                    }
                    if (data["Field"] != DBNull.Value)
                    {
                        objField._field = Convert.ToString(data["Field"]);
                    }
                    if (data["Status"] != DBNull.Value)
                    {
                        objField._status = Convert.ToString(data["Status"]);
                    }
                    if (data["FieldName"] != DBNull.Value)
                    {
                        objField.FieldName = Convert.ToString(data["FieldName"]);
                    }
                    if (data["FlightType"] != DBNull.Value)
                    {
                        objField._flightType = Convert.ToString(data["FlightType"]);
                    }
                    if (data["TransType"] != DBNull.Value)
                    {
                        objField.transType = Convert.ToString(data["TransType"]);
                    }
                    fieldsList.Add(objField);
                }
            }
            return fieldsList;

        }
    }
}
