using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public class TicketQueue
    {
        /// <summary>
        /// pnr of the booking
        /// </summary>
        private string pnr = string.Empty;
        /// <summary>
        /// Ticket number to be searched
        /// </summary>
        private string ticketNumber = string.Empty;
        /// <summary>
        /// Agency for which bookings have to be searched
        /// </summary>
        private string agencyId = string.Empty;
        /// <summary>
        /// Passenger name to be searched
        /// </summary>
        private string paxName = string.Empty;
        /// <summary>
        /// Comma seperated airline codes
        /// </summary>
        private string airlineCodes = string.Empty;
        /// <summary>
        /// Comma seperated sorce codes
        /// </summary>
        private string sourceCodes = string.Empty;
        /// <summary>
        /// Set true to search domestic bookings
        /// </summary>
        private bool domestic;
        /// <summary>
        /// Set true to search international bookings
        /// </summary>
        private bool international;
        /// <summary>
        /// Page Number to be shown
        /// </summary>
        private int pageNumber;
        /// <summary>
        /// Maximum nuber of data on each page
        /// </summary>
        private int numberOfDataOnEachPage;
       

        /// <summary>
        /// Field to store invoice date
        /// </summary>
        private DateTime invoiceDate;
        public DateTime InvoiceDate
        {
            set { invoiceDate=value;}
            get {return invoiceDate ;}
        }

        /// <summary>
        /// Field to store Invoice Number
        /// </summary>
        private int invoiceNumber;
        public int InvoiceNumber
        {
            set { invoiceNumber = value; }
            get { return invoiceNumber; }
        }

        /// <summary>
        /// Field to store FlightNumber 
        /// </summary>
        private int flightNumber;
        public int FlightNumber
        {
            set { flightNumber = value; }
            get { return flightNumber; }
        }

        /// <summary>
        /// total number of rows found for a given search request
        /// </summary>
        private int queueCount;
        /// <summary>
        /// to find if agent or admin
        /// </summary>
        private bool isAgent = false;
        public int QueueCount
        {
            get
            {
                return queueCount;
            }
        }
        public string Pnr
        {
            set
            {
                pnr = value;
            }
        }
        public string TicketNumber
        {
            set
            {
                ticketNumber = value;
            }
        }
        public string AgencyId
        {
            set
            {
                agencyId = value;
            }
        }
        public string PaxName
        {
            set
            {
                paxName = value;
            }
        }
        public string AirlineCodes
        {
            set
            {
                airlineCodes = value;
            }
        }
        public string SourceCodes
        {
            set
            {
                sourceCodes = value;
            }
        }
        public bool Domestic
        {
            set
            {
                domestic = value;
            }
        }
        public bool International
        {
            set
            {
                international = value;
            }
        }
        public int PageNumber
        {
            get
            {
                return pageNumber;
            }
            set
            {
                pageNumber = value;
            }
        }
        public int NumberOfDataOnEachPage
        {
            set
            {
                numberOfDataOnEachPage = value;
            }
        }
        public bool IsAgent
        {
            set
            {
                isAgent = value;
            }
        }
        DataTable dt;
        /// <summary>
        /// Gets the data according to the filter criteria
        /// </summary>
        /// <returns></returns>
        public List<DataRow[]> GetData()
        {
            //Trace.TraceInformation("Entered TicketQueue.GetData");
            dt = new DataTable();
            List<DataRow[]> listOfFlightBookings = new List<DataRow[]>();
            bool dbHit = false;
            //If ticket number is provided then bring the booking with the provided ticket number and leave rest of the filter criterias
            if (ticketNumber.Trim().Length > 0)
            {
                dt = SearchByTicketNumber();
            }
            //If pnr is provided then bring the booking with the provided pnr and leave rest of the filter criterias
            else if (pnr.Trim().Length > 0)
            {
                dt = SearchByPnr();
            }
            //If both ticket number and pnr are not provided then search for other criterias
            else
            {
                //If pax name is provided search the booking by pax name and set the flag dbHit to true so that no further db hits are there
                if (paxName.Trim().Length > 0)
                {
                    dt = SearchByPaxName();
                    dbHit = true;
                }
                //If agency id is provided 
                if (agencyId.Trim().Length > 0)
                {
                    //If till now no db hit is there then make a db hit for agency
                    if (!dbHit)
                    {
                        dt = SearchByAgencyId();
                        dbHit = true;
                    }
                    //If already a db hit has been done then use the result of that db bit and extracy all bookings belonging to the provided agency.
                    else
                    {
                        string[] splittedAgencyId = agencyId.Split(',');
                        DataRow[] tempDataRow = new DataRow[dt.Rows.Count];
                        int count = 0;
                        foreach (string singleAgencyId in splittedAgencyId)
                        {
                            if (dt.Select("agencyId = '" + singleAgencyId + "'") != null && dt.Select("agencyId = '" + singleAgencyId + "'").Length > 0)
                            {
                                dt.Select("agencyId = '" + singleAgencyId + "'").CopyTo(tempDataRow, count);
                                count += dt.Select("agencyId = '" + singleAgencyId + "'").Length;
                            }
                        }
                        GetDataTableFromDataRow(tempDataRow);
                    }
                }
                //If airline code is provided
                if (airlineCodes.Trim().Length > 0)
                {
                    if (!dbHit)
                    {
                        dt = SearchByAirlineCodes();
                        dbHit = true;
                    }
                    else
                    {
                        string[] splittedAirlineCodes = airlineCodes.Split(',');
                        DataRow[] tempDataRow = new DataRow[dt.Rows.Count];
                        int count = 0;
                        foreach (string airlineCode in splittedAirlineCodes)
                        {
                            if (dt.Select("airlineCode = '" + airlineCode + "'") != null && dt.Select("airlineCode = '" + airlineCode + "'").Length > 0)
                            {
                                dt.Select("airlineCode = '" + airlineCode + "'").CopyTo(tempDataRow, count);
                                count += dt.Select("airlineCode = '" + airlineCode + "'").Length;
                            }
                        }
                        GetDataTableFromDataRow(tempDataRow);
                    }
                }
                //If source code is provided
                if (sourceCodes.Trim().Length > 0)
                {
                    if (!dbHit)
                    {
                        dt = SearchBySourceCodes();
                        dbHit = true;
                    }
                    else
                    {
                        string[] splittedSourceCodes = sourceCodes.Split(',');
                        DataRow[] tempDataRow = new DataRow[dt.Rows.Count];
                        int count = 0;
                        foreach (string sourceCode in splittedSourceCodes)
                        {
                            if (dt.Select("source = " + Convert.ToInt32(sourceCode)) != null && dt.Select("source = " + Convert.ToInt32(sourceCode)).Length > 0)
                            {
                                dt.Select("source = " + Convert.ToInt32(sourceCode)).CopyTo(tempDataRow, count);
                                count += dt.Select("source = " + Convert.ToInt32(sourceCode)).Length;
                            }
                        }
                        GetDataTableFromDataRow(tempDataRow);
                    }
                }
                //If domestic or international booking is provided
                if (domestic != international)
                {
                    if (!dbHit)
                    {
                        dt = SearchByDomesticInternational();
                        dbHit = true;
                    }
                    else
                    {
                        DataRow[] tempDataRow = dt.Select("isDomestic = " + domestic);
                        GetDataTableFromDataRow(tempDataRow);
                    }
                }
                //If no criteria is provided then search to get latest 100 tickets
                if (!dbHit)
                {
                    dt = SearchWithoutFilters();
                    dbHit = true;
                }
            }
            //If data table contains result then convert the result into list of data row array
            if (dt.Columns.Count > 1 && dt.Rows.Count > 0)
            {
                listOfFlightBookings = GetFinalResult(dt);
            }
            //Trace.TraceInformation("TicketQueue.GetData Exited");
            return listOfFlightBookings;
        }

        /// <summary>
        /// This function will get agent Data according to filter criteria
        /// </summary>
        /// <param name="dataRowArray"></param>
        public List<DataRow[]> GetAgentData()
        {
            //Trace.TraceInformation("Entered TicketQueue.GetAgentData");
            dt = new DataTable();
            List<DataRow[]> listOfFlightBookings = new List<DataRow[]>();
            bool dbHit = false;

            if (ticketNumber.Trim().Length > 0)
            {
                dt = SearchByTicketNumberForAgent();
            }

            //If pnr is provided then bring the booking with the provided pnr and leave rest of the filter criterias
            else if (pnr.Trim().Length > 0)
            {
                dt = SearchByPnrForAgent();
            }
            //If both ticket number and pnr are not provided then search for other criterias
            else
            {
                //If pax name is provided search the booking by pax name and set the flag dbHit to true so that no further db hits are there
                if (paxName.Trim().Length > 0)
                {
                    dt = SearchByPaxNameForAgent();
                    dbHit = true;
                }

                //If airline code is provided
                if (airlineCodes.Trim().Length > 0)
                {
                    if (!dbHit)
                    {
                        dt = SearchByAirlineCodeForAgent();
                        dbHit = true;
                    }
                    else
                    {
                        string[] splittedAirlineCodes = airlineCodes.Split(',');
                        DataRow[] tempDataRow = new DataRow[dt.Rows.Count];
                        int count = 0;
                        foreach (string airlineCode in splittedAirlineCodes)
                        {
                            if (dt.Select("airlineCode = '" + airlineCode + "'") != null && dt.Select("airlineCode = '" + airlineCode + "'").Length > 0)
                            {
                                dt.Select("airlineCode = '" + airlineCode + "'").CopyTo(tempDataRow, count);
                                count += dt.Select("airlineCode = '" + airlineCode + "'").Length;
                            }
                        }
                        GetDataTableFromDataRow(tempDataRow);
                    }
                }

                //If agency id is provided 
                if (agencyId.Trim().Length > 0)
                {
                    //If till now no db hit is there then make a db hit for agency
                    if (!dbHit)
                    {
                        dt = SearchForAgentWithOutFilter();
                        dbHit = true;
                    }
                    //If already a db hit has been done then use the result of that db bit and extracy all bookings belonging to the provided agency.
                    else
                    {
                        string[] splittedAgencyId = agencyId.Split(',');
                        DataRow[] tempDataRow = new DataRow[dt.Rows.Count];
                        int count = 0;
                        foreach (string singleAgencyId in splittedAgencyId)
                        {
                            if (dt.Select("agencyId = '" + singleAgencyId + "'") != null && dt.Select("agencyId = '" + singleAgencyId + "'").Length > 0)
                            {
                                dt.Select("agencyId = '" + singleAgencyId + "'").CopyTo(tempDataRow, count);
                                count += dt.Select("agencyId = '" + singleAgencyId + "'").Length;
                            }
                        }
                        GetDataTableFromDataRow(tempDataRow);
                    }
                }
            }
            //If data table contains result then convert the result into list of data row array
            if (isAgent || (dt.Columns.Count > 1 && dt.Rows.Count > 0))
            {
                listOfFlightBookings = GetFinalResult(dt);
            }
            //Trace.TraceInformation("TicketQueue.GetAgentData Exited");
            return listOfFlightBookings;
        }

       

        /// <summary>
        /// The function searches the bookiing By paxname for agent
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByPaxNameForAgent()
        {
            //Trace.TraceInformation("TicketQueue.SearchByPaxNameForAgent entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@paxName", paxName);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAgentQueueByPaxName, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByPaxNameForAgent exited");
            return tempDataTable;
        }

        /// <summary>
        /// Function to search booking by TicketNumber for Agent
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByTicketNumberForAgent()
        {
            //Trace.TraceInformation("TicketQueue.SearchByTicketNumberForAgent entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@ticketNumber", ticketNumber);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAgentTicketQueueByTicketNumber, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByTicketNumberForAgent exited");
            return tempDataTable;
        }

        /// <summary>
        /// Function to search booking by AirlineCode(s) for Agent
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAirlineCodeForAgent()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodeForAgent entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@airlineCodes", airlineCodes);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAgentByAirlinesCode, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodeForAgent exited");
            return tempDataTable;
        }

        /// <summary>
        /// Function to search booking by pnr for Agent
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByPnrForAgent()
        {
            //Trace.TraceInformation("TicketQueue.SearchByPnrForAgent entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@pnr", pnr);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAgentQueueByPnr, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByPnrForAgent exited");
            return tempDataTable;
        }

        /// <summary>
        /// Function to search booking by pnr for Agent
        /// </summary>
        /// <returns></returns>
        private DataTable SearchForAgentWithOutFilter()
        {
            //Trace.TraceInformation("TicketQueue.SearchForAgentWithOutFilter entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAgentQueue, paramList);
            //Trace.TraceInformation("TicketQueue.SearchForAgentWithOutFilter exited");
            return tempDataTable;
        }


        /// <summary>
        /// This function converts data row array to data table
        /// </summary>
        /// <param name="dataRowArray"></param>
        private void GetDataTableFromDataRow(DataRow[] dataRowArray)
        {
            DataTable tempDataTable = new DataTable();
            tempDataTable = dt.Clone();
            foreach (DataRow dataRow in dataRowArray)
            {
                tempDataTable.ImportRow(dataRow);
            }
            dt.Clear();
            dt = tempDataTable;
        }
        /// <summary>
        ///This function formats the result into list of data row array
        /// </summary>
        /// <param name="tempDataTable"></param>
        /// <returns></returns>
        private List<DataRow[]> GetFinalResult(DataTable tempDataTable)
        {
            //Getting any Failed Bookings for Agents
            if (isAgent)
            {
                tempDataTable.Columns.Add("pendingId", typeof(int));
                int count = FailedBooking.GetCount(Convert.ToInt32(agencyId));
                List<FailedBooking> lstFailedBooking = FailedBooking.GetAllFailedBooking(Convert.ToInt32(agencyId), 0, count);
                foreach (FailedBooking fb in lstFailedBooking)
                {
                    FlightItinerary itinerary = FailedBooking.LoadItinerary(fb.FailedBookingId);
                    if (itinerary != null)
                    {
                        for (int i = 0; i < itinerary.Passenger.Length; i++)
                        {
                            DataRow dr = tempDataTable.NewRow();
                            dr["bookingDetailCreatedOn"] = fb.CreatedOn;
                            dr["source"] = (int)itinerary.FlightBookingSource;
                            if (itinerary.Passenger[i].Type == PassengerType.Adult)
                            {
                                dr["paxType"] = "ADT";
                            }
                            else if (itinerary.Passenger[i].Type == PassengerType.Child)
                            {
                                dr["paxType"] = "CNN";
                            }
                            else if (itinerary.Passenger[i].Type == PassengerType.Infant)
                            {
                                dr["paxType"] = "INF";
                            }
                            else if (itinerary.Passenger[i].Type == PassengerType.Senior)
                            {
                                dr["paxType"] = "SNN";
                            }
                            dr["agencyId"] = fb.AgencyId;
                            dr["pnr"] = fb.PNR;
                            dr["airlineCode"] = itinerary.AirlineCode;
                            dr["origin"] = itinerary.Origin;
                            dr["destination"] = itinerary.Destination;
                            dr["travelDate"] = itinerary.TravelDate;
                            dr["nonRefundable"] = true;
                            dr["ticketId"] = 0;
                            dr["paxFirstName"] = itinerary.Passenger[i].FirstName;
                            dr["paxLastName"] = itinerary.Passenger[i].LastName;
                            dr["bookingId"] = 0;
                            dr["flightId"] = 0;
                            dr["ticketNumber"] = itinerary.PNR.ToString();
                            dr["agencyName"] = fb.AgencyName;
                            dr["isDomestic"] = itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
                            dr["status"] = "OK";
                            dr["pendingId"] = fb.FailedBookingId;
                            tempDataTable.Rows.Add(dr);
                        }
                    }
                }
                DataView dvTicket = tempDataTable.DefaultView;

                if (ticketNumber.Trim().Length > 0)
                {
                    dvTicket.RowFilter = "ticketNumber='" + ticketNumber.Trim() + "'";
                }

                //If pnr is provided then bring the booking with the provided pnr and leave rest of the filter criterias
                else if (pnr.Trim().Length > 0)
                {
                    dvTicket.RowFilter = "pnr='" + pnr.Trim() + "'";
                }
                //If both ticket number and pnr are not provided then search for other criterias
                else
                {
                    //If pax name is provided search the booking by pax name and set the flag dbHit to true so that no further db hits are there
                    //if (paxName.Trim().Length > 0)
                    //{
                    //    string paxNameFilter = string.Empty;
                    //    string[] spltPaxName = paxName.Split(' ');
                    //    for (int i = 0; i < spltPaxName.Length; i++)
                    //    {
                    //        if (i == 0)
                    //        {
                    //            paxNameFilter += "paxFirstName='" + spltPaxName[i].Trim() + "' or paxLastName='" + spltPaxName[i].Trim() + "'";
                    //        }
                    //        else
                    //        {
                    //            paxNameFilter += "or paxFirstName='" + spltPaxName[i].Trim() + "' or paxLastName='" + spltPaxName[i].Trim() + "'";
                    //        }
                    //    }
                    //    dvTicket.RowFilter = paxNameFilter;
                    //}

                    //If airline code is provided
                    if (airlineCodes.Trim().Length > 0)
                    {
                        string airlineFilter = string.Empty;
                        string[] spltAirline = airlineCodes.Split(',');
                        for (int i = 0; i < spltAirline.Length; i++)
                        {
                            if (i == 0)
                            {
                                airlineFilter += "airlineCode='" + spltAirline[i].Trim() + "'";
                            }
                            else
                            {
                                airlineFilter += "or airlineCode='" + spltAirline[i].Trim() + "'";
                            }
                        }
                        dvTicket.RowFilter = airlineFilter;
                    }
                }
                dvTicket.Sort = "bookingDetailCreatedOn Desc";
                tempDataTable = dvTicket.ToTable();
            }
           
            int startIndex = (pageNumber - 1) * numberOfDataOnEachPage;
            List<DataRow[]> listOfFlightBookings = new List<DataRow[]>();
            List<string> distinctId = SelectDistinctId(tempDataTable);
            queueCount = distinctId.Count;
            if (queueCount < (pageNumber - 1) * numberOfDataOnEachPage)
            {
                pageNumber = 1;
                startIndex = 0;
            }
            if ((startIndex + (numberOfDataOnEachPage - 1)) >= queueCount)
            {
                numberOfDataOnEachPage = queueCount - (startIndex);
            }
            List<string> distinctIdInRange = distinctId.GetRange(startIndex, numberOfDataOnEachPage);
            foreach (string id in distinctIdInRange)
            {
                DataRow[] arrayOfDataRowForId = null;
                string[] idFilter = id.Split('-');
                if (idFilter[0] == "2")
                {
                    arrayOfDataRowForId = tempDataTable.Select("flightId ='" + idFilter[1] + "'");
                }
                else if (idFilter[0] == "1")
                {
                    arrayOfDataRowForId = tempDataTable.Select("pendingId ='" + idFilter[1] + "'");
                }
                listOfFlightBookings.Add(arrayOfDataRowForId);
            }
            return listOfFlightBookings;
        }

        /// <summary>
        /// Gets distinct pnr's in a given table
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<DataRow[]> GetSearchResult(string[] filters,ProductType productType)
        {
            DataTable tempDataTable=null;
            if (productType == ProductType.Flight)
            {
                tempDataTable = SearchResults(filters[0], filters[1]);
            }
            else if (productType == ProductType.Insurance)
            {
                string whereString= filters[1];
                tempDataTable = SearchResultForInsuredTickets(whereString);
            }

            List<DataRow[]> listOfFlightBookings = new List<DataRow[]>();

            if (tempDataTable.Columns.Count > 1 && tempDataTable.Rows.Count > 0)
            {
                int startIndex = (pageNumber - 1) * numberOfDataOnEachPage;
               
                List<string> distinctId = SelectDistinctFlightId(tempDataTable);
                queueCount = distinctId.Count;
                if (queueCount < (pageNumber - 1) * numberOfDataOnEachPage)
                {
                    pageNumber = 1;
                    startIndex = 0;
                }
                if ((startIndex + (numberOfDataOnEachPage - 1)) >= queueCount)
                {
                    numberOfDataOnEachPage = queueCount - (startIndex);
                }
                List<string> distinctFlightInRange = distinctId.GetRange(startIndex, numberOfDataOnEachPage);
                foreach (string flightId in distinctFlightInRange)
                {
                    DataRow[] arrayOfDataRowForPNR = tempDataTable.Select("flightId ='" + flightId + "'");
                    listOfFlightBookings.Add(arrayOfDataRowForPNR);
                }
            }
            //Trace.TraceInformation("TicketQueue.GetData Exited");
            return listOfFlightBookings;
        }

        /// <summary>
        /// Gets distinct pnr in a given table
        /// </summary>
        /// <param name="tempDataTable"></param>
        /// <returns></returns>
        private List<string> SelectDistinctFlightId(DataTable tempDataTable)
        {
            List<string> distinctId = new List<string>();
            foreach (DataRow dr in tempDataTable.Rows)
            {
                if (!distinctId.Contains(Convert.ToString(dr["flightId"])))
                {
                    distinctId.Add(Convert.ToString(dr["flightId"]));
                }
            }
            return distinctId;
        }

        /// <summary>
        /// Gets distinct pnr's in a given table
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private List<string> SelectDistinctId(DataTable tempDataTable)
        {
            List<string> distinctId = new List<string>();
            foreach (DataRow dr in tempDataTable.Rows)
            {
                if (isAgent)
                {
                    if (dr["pendingId"] != DBNull.Value)
                    {
                        if (!distinctId.Contains("1-" + Convert.ToString(dr["pendingId"])))
                        {
                            distinctId.Add("1-" + Convert.ToString(dr["pendingId"]));
                        }
                    }
                    else
                    {
                        if (!distinctId.Contains("2-" + Convert.ToString(dr["flightId"])))
                        {
                            distinctId.Add("2-" + Convert.ToString(dr["flightId"]));
                        }
                    }
                }
                else
                {
                    if (!distinctId.Contains("2-" + Convert.ToString(dr["flightId"])))
                    {
                        distinctId.Add("2-" + Convert.ToString(dr["flightId"]));
                    }
                }
            }
            return distinctId;
        }

        
        /// <summary>
        /// This function will get the insured ticket on the basis of filter criteria
        /// <param name="whereString">the filter criteria for insured tickets</param>
        /// </summary>
        /// <returns></returns>
        private DataTable SearchResultForInsuredTickets(string whereString)
        {
            //Trace.TraceInformation("TicketQueue.SearchResultForInsuredTickets entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString",whereString);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForInsuredTickets, paramList);
            //Trace.TraceInformation("TicketQueue.SearchResultForInsuredTickets exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches on the basis of wherestring.
        /// </summary>
        /// <returns></returns>
        private DataTable SearchResults(string tables, string whereString)
        {
            //Trace.TraceInformation("TicketQueue.SearchResults entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@tables", tables);
            paramList[1] = new SqlParameter("@whereString", whereString);

            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForFlightSearch, paramList);
            //Trace.TraceInformation("TicketQueue.SearchResults exited");
            return tempDataTable;
        }

        /// <summary>
        /// This function searches when no filter criteria is provided
        /// </summary>
        /// <returns></returns>
        private DataTable SearchWithoutFilters()
        {
            //Trace.TraceInformation("TicketQueue.SearchWithoutFilters entered");
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueue, new SqlParameter[0]);
            //Trace.TraceInformation("TicketQueue.SearchWithoutFilters exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by pnr
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByPnr()
        {
            //Trace.TraceInformation("TicketQueue.SearchByPnr entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@pnr", pnr);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByPnr, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByPnr exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by ticket number 
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByTicketNumber()
        {
            //Trace.TraceInformation("TicketQueue.SearchByTicketNumber entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketNumber", ticketNumber);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByTicketNumber, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByTicketNumber exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by agencyid
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyId()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAgencyId entered");
            DataTable tempDataTable = new DataTable();
            if (airlineCodes.Trim().Length > 0 && sourceCodes.Trim().Length > 0 && domestic != international)
            {
                tempDataTable = SearchByAgencyIdAirlineCodeSourceCodeAndIsDomestic();
                airlineCodes = string.Empty;
                sourceCodes = string.Empty;
                domestic = international;
            }
            else if (airlineCodes.Trim().Length > 0 && sourceCodes.Trim().Length > 0)
            {
                tempDataTable = SearchByAgencyIdAirlineCodeAndSourceCode();
                airlineCodes = string.Empty;
                sourceCodes = string.Empty;
            }
            else if (airlineCodes.Trim().Length > 0 && domestic != international)
            {
                tempDataTable = SearchByAgencyIdAirlineCodeAndIsDomestic();
                airlineCodes = string.Empty;
                domestic = international;
            }
            else if (sourceCodes.Trim().Length > 0 && domestic != international)
            {
                tempDataTable = SearchByAgencyIdSourceCodeAndIsDomestic();
                sourceCodes = string.Empty;
                domestic = international;
            }
            else if (sourceCodes.Trim().Length > 0)
            {
                tempDataTable = SearchByAgencyIdAndSourceCodes();
                sourceCodes = string.Empty;
            }
            else if (domestic != international)
            {
                tempDataTable = SearchByAgencyIdAndIsDomestic();
                domestic = international;
            }
            else if (airlineCodes.Trim().Length > 0)
            {
                tempDataTable = SearchByAgencyIdAndAirlineCode();
                airlineCodes = string.Empty;
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@agencyId", agencyId);
                tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAgencyId, paramList);
            }
            //Trace.TraceInformation("TicketQueue.SearchByAgencyId exited");
            return tempDataTable;
        }

        /// <summary>
        /// This function searches the booking by pax name
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByPaxName()
        {
            //Trace.TraceInformation("TicketQueue.SearchByPaxName entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxname", paxName);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByPaxName, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByPaxName exited");
            return tempDataTable;
        }

        /// <summary>
        /// This function searches the booking by airline code
        /// </summary>
        /// <returns></returns>
        /// 
        private DataTable SearchByAirlineCodes()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodes entered");
            DataTable tempDataTable = new DataTable();
            if (sourceCodes.Trim().Length > 0 && domestic != international)
            {
                tempDataTable = SearchByAirlineCodesSourceCodesAndIsDomestic();
                sourceCodes = string.Empty;
                domestic = international;
            }
            else if (sourceCodes.Trim().Length > 0)
            {
                tempDataTable = SearchByAirlineCodesAndSourceCodes();
                sourceCodes = string.Empty;
            }
            else if (domestic != international)
            {
                tempDataTable = SearchByAirlineCodesAndIsDomestic();
                domestic = international;
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@airlineCodes", airlineCodes);
                tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAirlineCodes, paramList);
            }
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodes exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by source code
        /// </summary>
        /// <returns></returns>
        private DataTable SearchBySourceCodes()
        {
            //Trace.TraceInformation("TicketQueue.SearchBySourceCodes entered");
            DataTable tempDataTable;
            if (domestic != international)
            {
                tempDataTable = SearchBySourceCodesAndIsDomestic();
                domestic = international;
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@source", sourceCodes);
                tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueBySource, paramList);
            }
            //Trace.TraceInformation("TicketQueue.SearchBySource exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by domestic or international
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByDomesticInternational()
        {
            //Trace.TraceInformation("TicketQueue.SearchByDomesticInternational entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@isDomestic", domestic);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByIsDomestic, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByDomesticInternational exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by agencyid and airline code
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyIdAndAirlineCode()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAndAirlineCode entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@airlineCodes", airlineCodes);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAirlineCodesAndAgencyId, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAndAirlineCode exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by agencyid and SourceCodes
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyIdAndSourceCodes()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAndSourceCodes entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@source", sourceCodes);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAgencyIdAndSourceCodes, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAndSourceCodes exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by agencyid and IsDomestic
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyIdAndIsDomestic()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAndIsDomestic entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@isDomestic", domestic);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAgencyIdAndIsDomestic, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAndIsDomestic exited");
            return tempDataTable;

        }
        /// <summary>
        ///  This function searches the booking by AirlineCodes and SourceCodes
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAirlineCodesAndSourceCodes()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodesAndSourceCodes entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@airlineCodes", airlineCodes);
            paramList[1] = new SqlParameter("@source", sourceCodes);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAirlineCodesAndSourceCodes, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodesAndSourceCodes exited");
            return tempDataTable;

        }
        /// <summary>
        /// This function searches the booking by AirlineCodes and IsDomestic
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAirlineCodesAndIsDomestic()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodesAndIsDomestic entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@airlineCodes", airlineCodes);
            paramList[1] = new SqlParameter("@isDomestic", domestic);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAirlineCodesAndIsDomestic, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodesAndIsDomestic exited");
            return tempDataTable;

        }
        /// <summary>
        /// This function searches the booking by SourceCodes and IsDomestic
        /// </summary>
        /// <returns></returns>
        private DataTable SearchBySourceCodesAndIsDomestic()
        {
            //Trace.TraceInformation("TicketQueue.SearchBySourceCodesAndIsDomestic entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@source", sourceCodes);
            paramList[1] = new SqlParameter("@isDomestic", domestic);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueBySourceCodeAndIsDomestic, paramList);
            //Trace.TraceInformation("TicketQueue.SearchBySourceCodesAndIsDomestic exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by AgencyId,AirlineCode,SourceCodes and IsDomestic
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyIdAirlineCodeSourceCodeAndIsDomestic()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAirlineCodeSourceCodeAndIsDomestic entered");
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@source", sourceCodes);
            paramList[1] = new SqlParameter("@isDomestic", domestic);
            paramList[2] = new SqlParameter("@agencyId", agencyId);
            paramList[3] = new SqlParameter("@airlineCodes", airlineCodes);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAgencyIdAirlineCodeSourceCodeAndIsDomestic, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAirlineCodeSourceCodeAndIsDomestic exited");
            return tempDataTable;
        }
        /// <summary>
        ///  This function searches the booking by AgencyId,AirlineCode and SourceCodes  
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyIdAirlineCodeAndSourceCode()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAirlineCodeAndSourceCode entered");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@airlineCodes", airlineCodes);
            paramList[1] = new SqlParameter("@source", sourceCodes);
            paramList[2] = new SqlParameter("@agencyId", agencyId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAgencyIdAirlineCodeAndSourceCode, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAirlineCodeAndSourceCode exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by AgencyId,AirlineCode and IsDomestic
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyIdAirlineCodeAndIsDomestic()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAirlineCodeAndIsDomestic entered");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@airlineCodes", airlineCodes);
            paramList[1] = new SqlParameter("@isDomestic", domestic);
            paramList[2] = new SqlParameter("@agencyId", agencyId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAgencyIdAirlineCodeAndIsDomestic, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdAirlineCodeAndIsDomestic exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by AgencyId,SourceCode and IsDomestic
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyIdSourceCodeAndIsDomestic()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdSourceCodeAndIsDomestic entered");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@source", sourceCodes);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@isDomestic", domestic);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAgencyIdSourceCodeAndIsDomestic, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAgencyIdSourceCodeAndIsDomestic exited");
            return tempDataTable;
        }
        /// <summary>
        /// This function searches the booking by AirlineCodes,SourceCode and IsDomestic
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAirlineCodesSourceCodesAndIsDomestic()
        {
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodesSourceCodesAndIsDomestic entered");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@source", sourceCodes);
            paramList[1] = new SqlParameter("@isDomestic", domestic);
            paramList[2] = new SqlParameter("@airlineCodes", airlineCodes);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueueByAirlineCodeSourceCodeAndIsDomestic, paramList);
            //Trace.TraceInformation("TicketQueue.SearchByAirlineCodesSourceCodesAndIsDomestic exited");
            return tempDataTable;
        }

        public static CT.Core.Queue[] GetRelevantQueuesForAgentBookingQueues(int pageNumber, int noOfRecordsPerPage, int totalrecords, string whereString,ref FlightItinerary[] itnerary,ref BookingDetail[] booking)
        {
            //Trace.TraceInformation("Queue.GetRelevantQueuesForAgentBookingQueues entered");
            DataSet dataset = new DataSet();
            SqlParameter[] paramList = new SqlParameter[3];
            int endrow = 0;
            int startrow = ((noOfRecordsPerPage * (pageNumber - 1)) + 1);
            if ((startrow + noOfRecordsPerPage) - 1 < totalrecords)
            {
                endrow = (startrow + noOfRecordsPerPage) - 1;
            }
            else
            {
                endrow = totalrecords;
            }
            paramList[0] = new SqlParameter("@startrow", startrow);
            paramList[1] = new SqlParameter("@endrow", endrow);// changes for paging
            paramList[2] = new SqlParameter("@whereString", whereString);
            dataset = DBGateway.FillSP(SPNames.GetRelevantQueuesForAgentBookingQueue, paramList);

            int queueCount = 0;
            if (dataset.Tables.Count != 0)
            {
                queueCount = dataset.Tables[0].Rows.Count;
            }
            CT.Core.Queue[] queues = new CT.Core.Queue[queueCount];
            itnerary = new FlightItinerary[queueCount];
            booking = new BookingDetail[queueCount];
            int i = 0;
            foreach (DataRow row in dataset.Tables[0].Rows)
            {
                queues[i] = new CT.Core.Queue();
                if (row["assignedBy"] != DBNull.Value)
                {
                    queues[i].AssignedBy = Convert.ToInt32(row["assignedBy"]);
                }
                if (row["assignedDate"] != DBNull.Value)
                {
                    queues[i].AssignedDate = Convert.ToDateTime(row["assignedDate"]);
                }
                if (row["assignedTo"] != DBNull.Value)
                {
                    queues[i].AssignedTo = Convert.ToInt32(row["assignedTo"]);
                }
                if (row["completionDate"] != DBNull.Value)
                {
                    queues[i].CompletionDate = Convert.ToDateTime(row["completionDate"]);
                }
                queues[i].CreatedBy = Convert.ToInt32(row["createdBy"]);
                queues[i].CreatedOn = Convert.ToDateTime(row["createdOn"]);
                queues[i].ItemId = Convert.ToInt32(row["itemId"]);
                if (row["lastModifiedBy"] != DBNull.Value)
                {
                    queues[i].ModifiedBy = Convert.ToInt32(row["lastModifiedBy"]);
                }
                queues[i].ModifiedOn = Convert.ToDateTime(row["lastModifiedOn"]);
                queues[i].QueueId = Convert.ToInt32(row["queueId"]);
                queues[i].QueueTypeId = 1;
                queues[i].StatusId = 2;


                itnerary[i] = new FlightItinerary();

                itnerary[i].FlightId = Convert.ToInt32(row["flightid"]);
                itnerary[i].Destination = row["destination"].ToString();
                itnerary[i].Origin = row["origin"].ToString();
                itnerary[i].FareType = row["fareType"].ToString();
                if (row["lastTicketingDate"] != DBNull.Value)
                {
                    itnerary[i].LastTicketDate = Convert.ToDateTime(row["lastTicketingDate"]);
                }
                else
                {
                    itnerary[i].LastTicketDate = DateTime.MinValue;
                }
                if (row["ticketAdvisory"] != DBNull.Value)
                {
                    itnerary[i].TicketAdvisory = Convert.ToString(row["ticketAdvisory"]);
                }
                else
                {
                    itnerary[i].TicketAdvisory = string.Empty;
                }
                string pnrnumber = row["pnr"].ToString();
                itnerary[i].PNR = pnrnumber.Trim();
                itnerary[i].ETicketHit = Convert.ToInt32(row["eTicketHit"]);
                itnerary[i].Segments = FlightInfo.GetSegments(itnerary[i].FlightId);
                itnerary[i].Passenger = FlightPassenger.GetPassengers(itnerary[i].FlightId);
                itnerary[i].FareRules = FareRule.GetFareRuleList(itnerary[i].FlightId);
                itnerary[i].IsDomestic = Convert.ToBoolean(row["isDomestic"]);
                itnerary[i].AirlineCode = row["airlineCode"].ToString();
                if (row["travelDate"] != DBNull.Value)
                {
                    itnerary[i].TravelDate = Convert.ToDateTime(row["travelDate"]);
                }
                else
                {
                    itnerary[i].TravelDate = itnerary[i].Segments[0].DepartureTime;
                }
                itnerary[i].BookingMode = Product.GetBookingMode(itnerary[i].FlightId, ProductType.Flight);
                if (row["paymentMode"] != DBNull.Value)
                {
                    itnerary[i].PaymentMode = (ModeOfPayment)(Convert.ToInt16(row["paymentMode"]));
                }// else paymentMode will have the default value Null = 0.
                if (itnerary[i].PaymentMode == ModeOfPayment.CreditCard)
                {
                    //itnerary[i].CcPayment = CCPayment.Load(itnerary[i].FlightId);
                }
                if (row["validatingAirlineCode"] != DBNull.Value)
                {
                    itnerary[i].ValidatingAirlineCode = Convert.ToString(row["validatingAirlineCode"]);
                }
                itnerary[i].NonRefundable = Convert.ToBoolean(row["nonRefundable"]);
                itnerary[i].CreatedBy = Convert.ToInt32(row["itineraryCreatedBy"]);
                itnerary[i].CreatedOn = Convert.ToDateTime(row["itineraryCreatedOn"]);
                itnerary[i].LastModifiedBy = Convert.ToInt32(row["itinerarylastModifiedBy"]);
                itnerary[i].LastModifiedOn = Convert.ToDateTime(row["itinerarylastModifiedOn"]);
                itnerary[i].FlightBookingSource = (BookingSource)(Convert.ToInt32(row["source"]));
                itnerary[i].BookingId = Convert.ToInt32(row["itinerarybookingId"]);
                itnerary[i].AgencyId = Convert.ToInt32(row["itineraryAgencyId"]);
                itnerary[i].LocationId = Convert.ToInt32(row["itineraryLocationId"]);
                itnerary[i].AliasAirlineCode = Convert.ToString(row["aliasAirlineCode"]);
                itnerary[i].IsOurBooking = Convert.ToBoolean(row["isOurBooking"]);
                itnerary[i].LocationName = Convert.ToString(row["locationName"]);
                itnerary[i].TransactionType = Convert.ToString(row["TransType"]);//
                if (row["TripId"] != DBNull.Value)//Used for corporate bookings
                {
                    itnerary[i].TripId = Convert.ToString(row["TripId"]);
                }
                if(row["routingTripId"] != DBNull.Value)
                {
                    itnerary[i].RoutingTripId = row["routingTripId"].ToString();
                }
                if(bool.Parse(row["isEdit"].ToString()))
                {
                    itnerary[i].IsEdit = 1;
                }
                else
                {
                    itnerary[i].IsEdit = 0;
                }
                booking[i] = new BookingDetail();

                booking[i].BookingId = Convert.ToInt32(row["bookingId"]);
                booking[i].Status = (BookingStatus)Enum.Parse(typeof(BookingStatus), row["bookingstatusId"].ToString()); ;
                if (row["bookingAgencyId"] != DBNull.Value)
                {
                    booking[i].AgencyId = Convert.ToInt32(row["bookingAgencyId"]);
                }
                else
                {
                    booking[i].AgencyId = 0;
                }
                if (row["lockedBy"] != DBNull.Value)
                {
                    booking[i].LockedBy = (int)row["lockedBy"];
                }
                else
                {
                    booking[i].LockedBy = 0;
                }
                if (row["prevStatusId"] != DBNull.Value)
                {
                    booking[i].PrevStatus = (BookingStatus)Convert.ToInt32(row["prevStatusId"]);
                }
                booking[i].ProductsList = BookingDetail.GetProductsLine(booking[i].BookingId);
                if (row["parentBookingId"] != DBNull.Value)
                {
                    booking[i].ParentBookingId = Convert.ToInt32(row["parentBookingId"]);
                }
                booking[i].CreatedBy = Convert.ToInt32(row["bookingCreatedby"]);
                booking[i].CreatedOn = Convert.ToDateTime(row["bookingCreatedOn"]);
                booking[i].LastModifiedBy = Convert.ToInt32(row["bookinglastModifiedBy"]);
                booking[i].LastModifiedOn = Convert.ToDateTime(row["bookinglastModifiedOn"]);

                i++;
            }
            //Trace.TraceInformation("Queue.GetRelevantQueuesForAgentBookingQueues Exited  ");
            return queues;
        }
        //Added By Hari Malla 20-02-2019
        public static DataTable Get_ApiPendingQueueDetails(string paymentsourceId,string paymentId,string orderId,string pnr,DateTime fromDate,DateTime  toDate,string bookingStatus,string agentId)
        {
            try
            {
                string whereString = " where CreatedOn >= '" + fromDate.ToString("yyyy-MM-dd") + "' and CreatedOn <= '" + toDate.ToString("yyyy-MM-dd")+"'";
                if (paymentsourceId != "0")
                    whereString += " and paymentGatewaySourceId='"+ paymentsourceId+"'";
                if (agentId != "0")
                    whereString += " and agencyId='" + agentId + "'";
                if (bookingStatus != "0")
                    whereString += " and bookingStatus='" + bookingStatus + "'";
                if (!string.IsNullOrEmpty(paymentId ))
                    whereString += " and paymentId like '%" + paymentId + "%'";
                if (!string.IsNullOrEmpty(orderId))
                    whereString += " and orderId like '%" + orderId + "%'";
                if (!string.IsNullOrEmpty(pnr))
                    whereString += " and outPNR like '%" + pnr + "%'";

                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@whereString", whereString);
                return DBGateway.FillDataTableSP("usp_GetApiPendingQueueDetails", paramlist);
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Getting the Flight pending queue Data :" + ex.ToString(),"");
                throw ex;
            }
        }

        public static DataTable Get_HotelApiPendingQueueDetails(string paymentsourceId, string paymentId, string orderId, string hotelName, DateTime fromDate, DateTime toDate, string bookingStatus, string agentId)
        {
            try
            {
                string whereString = " where CreatedOn >= '" + fromDate.ToString("yyyy-MM-dd") + "' and CreatedOn <= '" + toDate.ToString("yyyy-MM-dd") + "'";
                if (paymentsourceId != "0")
                    whereString += " and paymentGatewaySourceId='" + paymentsourceId + "'";
                if (agentId != "0")
                    whereString += " and agencyId='" + agentId + "'";
                if (bookingStatus != "0")
                    whereString += " and bookingStatus='" + bookingStatus + "'";
                if (!string.IsNullOrEmpty(paymentId))
                    whereString += " and paymentId like '%" + paymentId + "%'";
                if (!string.IsNullOrEmpty(orderId))
                    whereString += " and orderId like '%" + orderId + "%'";
                if (!string.IsNullOrEmpty(hotelName))
                    whereString += " and hotelName like '%" + hotelName + "%'";
                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@whereString", whereString);
                return DBGateway.FillDataTableSP("usp_GetHotelPendingQueueDetails", paramlist);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Getting the Hotel pending queue Data :" + ex.ToString(), "");
                throw ex;
            }
        }

    }
}
