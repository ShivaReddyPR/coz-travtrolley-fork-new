using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.BookingEngine
{
    public class HolidayPackageSeason
    {
        //static string masterDB = System.Configuration.ConfigurationSettings.AppSettings["MasterDB"].ToString();
        //static string masterDBagentId = System.Configuration.ConfigurationSettings.AppSettings["MasterDBAgentId"].ToString();
        #region privateFields
        int seasonId;
        int dealId;
        DateTime startDate;
        DateTime endDate;
        string hotelName;
        string hotelDescription;
        string supplierName;
        string supplierEmail;
        //HolidayPackageHotelRating star;
        decimal singlePrice;
        decimal twinSharingPrice;
        decimal tripleSharingPrice;
        decimal childWithBedPrice;
        decimal childWithoutBedPrice;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        string supplierCurrency;//added on 25052016
        #endregion

        #region publicProperties
        public int SeasonId
        {
            get
            {
                return seasonId;
            }
            set
            {
                seasonId = value;
            }
        }

        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }


        public string HotelDescription
        {
            get
            {
                return hotelDescription;
            }
            set
            {
                hotelDescription = value;
            }
        }

        public string SupplierName
        {
            get
            {
                return supplierName;
            }
            set
            {
                supplierName = value;
            }
        }

        public string SupplierEmail
        {
            get
            {
                return supplierEmail;
            }
            set
            {
                supplierEmail = value;
            }
        }


        //public HolidayPackageHotelRating Star
        //{
        //    get
        //    {
        //        return star;
        //    }
        //    set
        //    {
        //        star = value;
        //    }
        //}


        public decimal SinglePrice
        {
            get
            {
                return singlePrice;
            }
            set
            {
                singlePrice = value;
            }
        }

        public decimal TwinSharingPrice
        {
            get
            {
                return twinSharingPrice;
            }
            set
            {
                twinSharingPrice = value;
            }
        }

        public decimal TripleSharingPrice
        {
            get
            {
                return tripleSharingPrice;
            }
            set
            {
                tripleSharingPrice = value;
            }
        }

        public decimal ChildWithBedPrice
        {
            get
            {
                return childWithBedPrice;
            }
            set
            {
                childWithBedPrice = value;
            }
        }

        public decimal ChildWithoutBedPrice
        {
            get
            {
                return childWithoutBedPrice;
            }
            set
            {
                childWithoutBedPrice = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
               lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        //added on 25052016
        public string SupplierCurrency
        {
            get { return supplierCurrency; }
            set { supplierCurrency = value; }
        }
        #endregion

        #region publicMethods
        public static List<HolidayPackageSeason> LoadhotelSeasons(int dealId)
        {
            List<HolidayPackageSeason> tempList = new List<HolidayPackageSeason>();
            //SqlDataReader data = null;
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@dealId", dealId);
                try
                {

                    //data = DBGateway.ExecuteReaderSP(SPNames.GetHolidayPackageSeasons, paramList, connection);
                    using (DataTable dtHoliday = DBGateway.FillDataTableSP(SPNames.GetHolidayPackageSeasons, paramList))
                    {
                        if (dtHoliday != null && dtHoliday.Rows.Count > 0)
                        {
                            foreach (DataRow data in dtHoliday.Rows)
                            {
                                HolidayPackageSeason ssn = new HolidayPackageSeason();
                                ssn.seasonId = Convert.ToInt32(data["seasonId"]);
                                ssn.dealId = Convert.ToInt32(data["dealId"]);
                                ssn.startDate = Convert.ToDateTime(data["startDate"]);
                                ssn.endDate = Convert.ToDateTime(data["endDate"]);
                                ssn.hotelName = Convert.ToString(data["hotelName"]);
                                ssn.hotelDescription = Convert.ToString(data["HotelDescription"]);
                                ssn.supplierName = Convert.ToString(data["SupplierName"]);
                                ssn.supplierEmail = Convert.ToString(data["SupplierEmail"]);
                                //ssn.star = (HolidayPackageHotelRating)(Enum.Parse(typeof(HolidayPackageHotelRating), Convert.ToString(data["star"])));
                                ssn.singlePrice = Convert.ToDecimal(data["singlePrice"]);
                                ssn.twinSharingPrice = Convert.ToDecimal(data["twinSharingPrice"]);
                                ssn.tripleSharingPrice = Convert.ToDecimal(data["tripleSharingPrice"]);
                                ssn.childWithBedPrice = Convert.ToDecimal(data["childWithBedPrice"]);
                                ssn.childWithoutBedPrice = Convert.ToDecimal(data["childWithoutBedPrice"]);
                                ssn.supplierCurrency = Convert.ToString(data["SupplierCurrency"]); //added on 25/05/2016
                                tempList.Add(ssn);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, "Exception in LoadhotelSeasons(int dealId) for HolidayPackageSeason.cs dealId= " + dealId + " , Error Message:" + ex.Message + "| Stack Trace:" + ex.StackTrace, "");
                    throw new Exception("Unable to load holiday package.");
                }
                finally
                {
                    //if (data != null)
                    //{
                    //    data.Close();
                    //}
                    //connection.Close();
                }
            }
            return tempList;
        }
        #endregion

    }

}
