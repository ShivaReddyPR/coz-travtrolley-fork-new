using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    /// <summary>
    /// HotelBookingStatus
    /// </summary>
    public enum HotelBookingStatus
    {
        /// <summary>
        /// if Booking is Confirmed
        /// </summary>
        Confirmed = 1,
        /// <summary>
        /// if booking is canceled
        /// </summary>
        Cancelled = 2,
        /// <summary>
        /// If booking is failed
        /// </summary>
        Failed = 0,
        /// <summary>
        /// if booking is Pending
        /// </summary>
        Pending = 3,
        /// <summary>
        /// if booking is error
        /// </summary>
        Error = 4,
            /// <summary>
            /// if booking is PartiallyConfirmed
            /// </summary>
        PartiallyConfirmed = 5,
            /// <summary>
            /// if booking is PartiallyCancelled
            /// </summary>
        PartiallyCancelled = 6,
        /// <summary>
        /// if booking is awaiting for payment
        /// </summary>
        AwaitingForPayment = 7
    }

    /// <summary>
    ///HotelBookingSource
    /// </summary>
    public enum HotelBookingSource
    {
        /// <summary>
        /// Desiya
        /// </summary>
        Desiya = 1,
        /// <summary>
        /// GTA
        /// </summary>
        GTA = 2,
        /// <summary>
        /// HotelBeds
        /// </summary>
        HotelBeds = 3,
        /// <summary>
        /// Tourico
        /// </summary>
        Tourico = 4,
        /// <summary>
        /// IAN
        /// </summary>
        IAN = 5,
        /// <summary>
        /// TBOConnect
        /// </summary>
        TBOConnect = 6,
        /// <summary>
        /// Miki
        /// </summary>
        Miki = 7,
        /// <summary>
        /// Travco
        /// </summary>
        Travco = 8,
        /// <summary>
        /// DOTW
        /// </summary>
        DOTW = 9,
        /// <summary>
        /// WST
        /// </summary>
        WST = 10,
        /// <summary>
        /// Cozmo Hotel inventory
        /// </summary>
        HotelConnect = 11,
        /// <summary>
        /// RezLive
        /// </summary>
        RezLive = 12,
        /// <summary>
        /// LOH
        /// </summary>
        LOH = 13,
        /// <summary>
        /// TBOHotel
        /// </summary>
        TBOHotel = 14,
        /// <summary>
        /// JAC
        /// </summary>
        JAC = 15,
        /// <summary>
        /// EET
        /// </summary>
        EET = 16,
        /// <summary>
        /// Agoda
        /// </summary>
        Agoda = 17,
        /// <summary>
        /// Yatra  --  Added by somasekhar on 24/08/2018
        /// </summary>
        Yatra = 18,

 /// <summary>
        ///  GRN
        /// </summary>
        GRN = 19,
        /// <summary>
        ///  OYO  --  Added by somasekhar on 05/12/2018 
        /// </summary>
        OYO = 20,
        /// <summary>
        ///  TBO
        /// </summary>
        TBO= 21,
        /// <summary>
        ///  GIMMONIX
        /// </summary>
        GIMMONIX = 22,
        /// <summary>
        ///  UAPI Hotel
        /// </summary>
        UAH = 23,
            /// <summary>
            ///  Offline Source
            /// </summary>
        Offline = 24,
        /// <summary>
        ///  illusions Source
        /// </summary>
        Illusions = 25,
         /// <summary>
         ///  HotelExtranet Source
         /// </summary>
        HotelExtranet = 26
    }
    /// <summary>
    /// CreditCardInfo
    /// </summary>
    [Serializable]
    public struct CreditCardInfo
    {
        /// <summary>
        /// mail
        /// </summary>
        public string eMail;
        /// <summary>
        /// firstName
        /// </summary>
        public string fName;
        /// <summary>
        /// LastName
        /// </summary>
        public string lName;
        /// <summary>
        /// HomePhoneNo
        /// </summary>
        public string homePhoneNo;
        /// <summary>
        /// WorkPhoneNo
        /// </summary>
        public string workPhoneNo;
        /// <summary>
        /// CardType
        /// </summary>
        public string cardType;
        /// <summary>
        /// CardNo
        /// </summary>
        public string cardNo;
        /// <summary>
        /// ExpMonth
        /// </summary>
        public string expMonth;
        /// <summary>
        /// ExpYear
        /// </summary>
        public string expYear;
        /// <summary>
        /// CVV
        /// </summary>
        public string cvv;
        /// <summary>
        /// Address1
        /// </summary>
        public string address1;
        /// <summary>
        /// Address2
        /// </summary>
        public string address2;
        /// <summary>
        /// City
        /// </summary>
        public string city;
        /// <summary>
        /// stateCode
        /// </summary>
        public string stateCode;
        /// <summary>
        /// countryCode
        /// </summary>
        public string countryCode;
        /// <summary>
        /// postalCode
        /// </summary>
        public string postalCode;
        /// <summary>
        /// creditCardPasHttpUserAgent
        /// </summary>
        public string creditCardPasHttpUserAgent;
        /// <summary>
        /// creditCardPasHttpAccept
        /// </summary>
        public string creditCardPasHttpAccept;
        /// <summary>
        /// creditCardPasPaRes
        /// </summary>
        public string creditCardPasPaRes;
    }
    /// <summary>
    /// This class is using to save the HotelItinerary in DB and retrive the HotelItinerary from DB
    /// </summary>
    [Serializable]
    public class HotelItinerary : CT.BookingEngine.Product
    {
        # region private variables
        /// <summary>
        /// BookingConfirmationNo
        /// </summary>
        string confirmationNo;
        /// <summary>
        /// HotelId(unique table Id)
        /// </summary>
        int hotelId;
        /// <summary>
        /// HotelCode
        /// </summary>
        string hotelCode;
        /// <summary>
        /// HotelName
        /// </summary>
        string hotelName;
        /// <summary>
        /// chainCode
        /// </summary>
        string chainCode;
        /// <summary>
        /// hotelCategory
        /// </summary>
        string hotelCategory;
        /// <summary>
        /// HotelRating
        /// </summary>
        HotelRating rating;
        /// <summary>
        ///  roomtype
        /// </summary>
        HotelRoom[] roomtype;
        /// <summary>
        /// CheckInDate
        /// </summary>
        DateTime startDate;
        /// <summary>
        /// CheckOutDate
        /// </summary>
        DateTime endDate;
        /// <summary>
        /// hotelPassenger 
        /// </summary>
        HotelPassenger hotelPassenger;
        /// <summary>
        /// hotelAddress1
        /// </summary>
        string hotelAddress1;
        /// <summary>
        /// hotelAddress2
        /// </summary>
        string hotelAddress2;
        /// <summary>
        /// cityCode
        /// </summary>
        string cityCode;
        /// <summary>
        /// cityRef
        /// </summary>
        string cityRef;
        /// <summary>
        /// specialRequest
        /// </summary>
        string specialRequest;
        /// <summary>
        /// status
        /// </summary>
        HotelBookingStatus status;
        /// <summary>
        /// hotelPolicyDetails
        /// </summary>
        string hotelPolicyDetails;
        /// <summary>
        /// hotelCancelPolicy
        /// </summary>
        string hotelCancelPolicy;
        /// <summary>
        /// noOfRooms
        /// </summary>
        int noOfRooms;
        /// <summary>
        /// createdBy
        /// </summary>
        int createdBy;
        /// <summary>
        /// source
        /// </summary>
        HotelBookingSource source;
        /// <summary>
        /// isDomestic
        /// </summary>
        private bool isDomestic;
        /// <summary>
        /// cancelId
        /// </summary>
        string cancelId;
        /// <summary>
        /// flightInfo
        /// </summary>
        string flightInfo;
        /// <summary>
        /// bookingRefNo
        /// </summary>
        string bookingRefNo;
        /// <summary>
        /// map
        /// </summary>
        string map;
        /// <summary>
        /// lastCancellationDate(Deadline date)
        /// </summary>
        DateTime lastCancellationDate;
        /// <summary>
        /// voucherStatus(true/false)
        /// </summary>
        bool voucherStatus;
        /// <summary>
        /// penalityInfo
        /// </summary>
        List<HotelPenality> penalityInfo;
        /// <summary>
        /// agencyReference(our unique Ref)
        /// </summary>
        string agencyReference;
        /// <summary>
        /// vatDescription
        /// </summary>
        string vatDescription;
        /// <summary>
        /// ccInfo
        /// </summary>
        CreditCardInfo ccInfo;
        /// <summary>
        /// propertyType
        /// </summary>
        string propertyType;
        /// <summary>
        /// supplierType
        /// </summary>
        string supplierType;
        /// <summary>
        /// passengerNationality
        /// </summary>
        string passengerNationality;// For DOTW req objects
        /// <summary>
        /// passengerCountryOfResidence
        /// </summary>
        string passengerCountryOfResidence;
        /// <summary>
        /// paymentGuaranteedBy
        /// </summary>
        string paymentGuaranteedBy =string.Empty;
        /// <summary>
        /// Booking Currency
        /// </summary>
        string currency;
        /// <summary>
        /// Login locationId
        /// </summary>
        int locationId;
        /// <summary>
        /// Booing AgencyId
        /// </summary>
        int agencyId;
        /// <summary>
        /// transType(B2B/B2C) 
        /// </summary>
        string transType ="B2B";
        /// <summary>
        /// Specific for LOH used to store USD price
        /// </summary>
        decimal totalPrice;
        /// <summary>
        /// Specific for LOH used for booking Hotel
        /// </summary>
        string sequenceNumber;

        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;

        /// <summary>
        /// rezlive purpose
        /// </summary>
        string destinationCountryCode;

        ModeOfPayment paymentMode = ModeOfPayment.Credit; //Default should 2  means credit Added by brahmam
        string gxsupplier;
        /// <summary>
        /// Remarks
        /// </summary>
        string remarks;

        /// <summary>
        /// Remarks
        /// </summary>
        FlightPolicy hotelPolicy;
        /// <summary>
        /// To store universal PNR
        /// </summary>
        string _URLocator;
        /// <summary>
        /// To store Booking Remarks
        /// </summary>
        string bookingRemarks;
        /// <summary>
        /// To store Booking user machine IP address
        /// </summary>
        string _BookUserIP;
        /// <summary>
        /// AgentRef
        /// </summary>
        string agentRef;
        /// <summary>
       
        /// loginCountryCode
        /// </summary>
        string loginCountryCode;  //for illusions purpose
        

        #endregion

        #region public properties

        /// <summary>
        /// ConfirmationNo
        /// </summary>
        public string ConfirmationNo
        {
            get { return confirmationNo; }
            set { confirmationNo = value; }
        }
        /// <summary>
        /// HotelId
        /// </summary>
        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }
        /// <summary>
        /// HotelCode
        /// </summary>
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }
        /// <summary>
        /// HotelName
        /// </summary>
        public string HotelName
        {
            get { return hotelName; }
            set { hotelName = value; }
        }
        /// <summary>
        /// ChainCode
        /// </summary>
        public string ChainCode
        {
            get { return chainCode; }
            set { chainCode = value; }
        }
        /// <summary>
        /// HotelCategory
        /// </summary>
        public string HotelCategory
        {
            get { return hotelCategory; }
            set { hotelCategory = value; }
        }
        /// <summary>
        /// Rating
        /// </summary>
        public HotelRating Rating
        {
            get { return rating; }
            set { rating = value; }
        }

        /// <summary>
        /// Roomtype
        /// </summary>
        public HotelRoom[] Roomtype
        {
            get { return roomtype; }
            set { roomtype = value; }
        }
        /// <summary>
        /// StartDate
        /// </summary>
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        /// <summary>
        /// EndDate
        /// </summary>
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        /// <summary>
        /// HotelPassenger
        /// </summary>
        public HotelPassenger HotelPassenger
        {
            get { return hotelPassenger; }
            set { hotelPassenger = value; }
        }
        /// <summary>
        /// HotelAddress1
        /// </summary>
        public string HotelAddress1
        {
            get { return hotelAddress1; }
            set { hotelAddress1 = value; }
        }
        /// <summary>
        /// HotelAddress2
        /// </summary>
        public string HotelAddress2
        {
            get { return hotelAddress2; }
            set { hotelAddress2 = value; }
        }
        /// <summary>
        /// CityCode
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        /// <summary>
        /// CityRef
        /// </summary>
        public string CityRef
        {
            get { return cityRef; }
            set { cityRef = value; }
        }
        /// <summary>
        /// Status
        /// </summary>
        public HotelBookingStatus Status
        {
            get { return status; }
            set { status = value; }
        }
        /// <summary>
        /// SpecialRequest
        /// </summary>
        public string SpecialRequest
        {
            get { return specialRequest; }
            set { specialRequest = value; }
        }

        /// <summary>
        /// Hotel Policy details - will be added with some delimeter or new line.
        /// </summary>
        public string HotelPolicyDetails
        {
            get { return hotelPolicyDetails; }
            set { hotelPolicyDetails = value; }
        }
        /// <summary>
        /// HotelCancelPolicy
        /// </summary>
        public string HotelCancelPolicy
        {
            get { return hotelCancelPolicy; }
            set { hotelCancelPolicy = value; }
        }

        /// <summary>
        /// Total number of rooms requested
        /// </summary>
        public int NoOfRooms
        {
            get
            {
                return noOfRooms;
            }
            set
            {
                noOfRooms = value;
            }
        }
        /// <summary>
        /// CreatedBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        /// <summary>
        /// Source
        /// </summary>
        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        /// <summary>
        /// IsDomestic
        /// </summary>
        public bool IsDomestic
        {
            get { return isDomestic; }
            set { isDomestic = value; }
        }
        /// <summary>
        /// CancelId(if booking cancelled)
        /// </summary>
        public string CancelId
        {
            get { return cancelId; }
            set { cancelId = value; }
        }
        /// <summary>
        /// FlightInfo
        /// </summary>
        public string FlightInfo
        {
            get { return flightInfo; }
            set { flightInfo = value; }
        }
        /// <summary>
        /// BookingRefNo
        /// </summary>
        public string BookingRefNo
        {
            get { return bookingRefNo; }
            set { bookingRefNo = value; }
        }
        /// <summary>
        /// Map
        /// </summary>
        public string Map
        {
            get { return map; }
            set { map = value; }
        }
        /// <summary>
        /// LastCancellationDate
        /// </summary>
        public DateTime LastCancellationDate
        {
            get { return lastCancellationDate; }
            set { lastCancellationDate = value; }
        }
        /// <summary>
        /// VoucherStatus
        /// </summary>
        public bool VoucherStatus
        {
            get { return voucherStatus; }
            set { voucherStatus = value; }
        }
        /// <summary>
        /// PenalityInfo
        /// </summary>
        public List<HotelPenality> PenalityInfo
        {
            get { return penalityInfo; }
            set { penalityInfo = value; }
        }
        /// <summary>
        /// AgencyReference
        /// </summary>
        public string AgencyReference
        {
            get { return agencyReference; }
            set { agencyReference = value; }
        }
        /// <summary>
        /// VatDescription
        /// </summary>
        public string VatDescription
        {
            get { return vatDescription; }
            set { vatDescription = value; }
        }
        /// <summary>
        /// CCInfo
        /// </summary>
        public CreditCardInfo CCInfo
        {
            get { return ccInfo; }
            set { ccInfo = value; }
        }
        /// <summary>
        /// PropertyType
        /// </summary>
        public string PropertyType
        {
            get { return propertyType; }
            set { propertyType = value; }
        }
        /// <summary>
        /// SupplierType
        /// </summary>
        public string SupplierType
        {
            get { return supplierType; }
            set { supplierType = value; }
        }
        /// <summary>
        /// PassengerNationality
        /// </summary>
        public string PassengerNationality
        {
            get { return passengerNationality; }
            set { passengerNationality = value; }
        }
        /// <summary>
        /// PassengerCountryOfResidence
        /// </summary>
        public string PassengerCountryOfResidence
        {
            get { return passengerCountryOfResidence; }
            set { passengerCountryOfResidence = value; }
        }
        /// <summary>
        /// PaymentGuaranteedBy
        /// </summary>
        public string PaymentGuaranteedBy
        {
            get { return paymentGuaranteedBy; }
            set { paymentGuaranteedBy = value; }
        }
        /// <summary>
        /// Currency
        /// </summary>
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        /// <summary>
        /// LocationId
        /// </summary>
        public int LocationId
        {
            get { return locationId; }
            set { locationId = value; }
        }
        /// <summary>
        /// AgencyId
        /// </summary>
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }

        /// <summary>
        /// Specifically used for LOH for storing USD price.
        /// </summary>
        public decimal TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }
        /// <summary>
        /// Specifically used for LOH to send while booking a hotel.
        /// </summary>
        public string SequenceNumber
        {
            get { return sequenceNumber; }
            set { sequenceNumber = value; }
        }
        /// <summary>
        /// TransType
        /// </summary>
        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }

        //RezLive purpose
        /// <summary>
        /// DestinationCountryCode
        /// </summary>
        public string DestinationCountryCode
        {
            get { return destinationCountryCode; }
            set { destinationCountryCode = value; }
        }
        //Checking card or credit added by brahmam 15.09.2016
        /// <summary>
        /// PaymentMode
        /// </summary>
        public ModeOfPayment PaymentMode
        {
            get{return paymentMode;}
            set{paymentMode=value;}
        }

        public string Gxsupplier { get => gxsupplier; set => gxsupplier = value; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }

        /// <summary>
        /// AgentRef
        /// </summary>
        public string AgentRef
        {
            get { return agentRef; }
            set { agentRef = value; }
        }

        public FlightPolicy HotelPolicy { get => hotelPolicy; set => hotelPolicy = value; }
        /// <summary>
        /// To store universal PNR
        /// </summary>
        public string URLocator { get => _URLocator; set => _URLocator = value; }
        /// <summary>
        /// To store Booking Remarks
        /// </summary>
        public string BookingRemarks { get => bookingRemarks; set => bookingRemarks = value; }
        /// <summary>
        /// To store Booking Remarks
        /// </summary>
        public string BookUserIP { get => _BookUserIP; set => _BookUserIP = value; }
        /// <summary>
        /// To hold B2C order Id
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// To hold B2C payment gateway source
        /// </summary>
        public PaymentGatewaySource PGSoruce { get; set; }
        /// <summary>
        /// To hold B2C Payment Id
        /// </summary>
        public string PaymentId { get; set; }
        /// <summary>
        /// To hold B2C Pending queue id
        /// </summary>
        public int PendingQueueId { get; set; }
        /// <summary>
        /// To hold booking id
        /// </summary>
        public int BookingId { get; set; }
        /// <summary>
        /// To save payment details
        /// </summary>
        public PaymentInformation PaymentInfo { get; set; }
        /// <summary>
        /// To save promotion details
        /// </summary>
        public PromoDetails PromoInfo { get; set; }
        /// <summary>
        /// To hold hotel policy details in bytes
        /// </summary>
        public byte[] HotelEssentialBytes { get; set; }
        /// <summary>

        /// <summary>
        /// LoginCountryCode
        /// </summary>
        public string LoginCountryCode
        {
            get { return loginCountryCode; }
            set { loginCountryCode = value; }
        }
                

        /// <summary>
        #endregion
        /// <summary>
        /// Default Constructor
        /// </summary>
        public HotelItinerary()
        {
            paymentMode = ModeOfPayment.Credit;
        }

        # region Methods
        /// <summary>
        ///  This method is to save the Record into HotelItinerary DB
        /// </summary>
        /// <param name="prod">Product</param>
        public override void Save(Product prod)
        {
            try
            {
                HotelItinerary itinerary = (HotelItinerary)prod;

            //TODO: validate that the fields are properly set before calling the SP
            //Trace.TraceInformation("HotelItinerary.Save entered.");
            SqlParameter[] paramList = new SqlParameter[36];
            if (itinerary.confirmationNo != null)
            {
                paramList[0] = new SqlParameter("@confirmationNo", itinerary.ConfirmationNo);
            }
            else
            {
                paramList[0] = new SqlParameter("@confirmationNo", DBNull.Value);
            }
            paramList[1] = new SqlParameter("@starRating", itinerary.Rating);
            if (itinerary.CityCode != null)
            {
                paramList[2] = new SqlParameter("@cityCode", itinerary.CityCode);
            }
            else
            {
                paramList[2] = new SqlParameter("@cityCode", DBNull.Value);
            }
            paramList[3] = new SqlParameter("@hotelName", itinerary.HotelName);
            paramList[4] = new SqlParameter("@hotelCode", itinerary.HotelCode);

            paramList[5] = new SqlParameter("@addressLine1", itinerary.HotelAddress1);

            if (itinerary.HotelAddress2 != null)
            {
                paramList[6] = new SqlParameter("@addressLine2", itinerary.HotelAddress2);
            }
            else
            {
                paramList[6] = new SqlParameter("@addressLine2", string.Empty);
            }

            paramList[7] = new SqlParameter("@checkInDate", itinerary.StartDate);
            paramList[8] = new SqlParameter("@checkOutDate", itinerary.EndDate);
            paramList[9] = new SqlParameter("@noOfRooms", itinerary.NoOfRooms);
            paramList[10] = new SqlParameter("@status", itinerary.Status);
            paramList[11] = new SqlParameter("@specialRequest", itinerary.SpecialRequest);
            paramList[12] = new SqlParameter("@createdBy", itinerary.CreatedBy);
            paramList[13] = new SqlParameter("@lastModifiedBy", itinerary.LastModifiedBy);
            //temp fix to avoid html tags issue in hotel policy details    
            string sHotelPolicyDetails = itinerary.HotelEssentialBytes != null && string.IsNullOrEmpty(itinerary.HotelPolicyDetails) ?
                        Convert.ToString(GenericStatic.GetObjectWithByteArray(itinerary.HotelEssentialBytes)) : itinerary.HotelPolicyDetails;                    
            paramList[14] = new SqlParameter("@hotelPolicyDetails", sHotelPolicyDetails);
            paramList[15] = new SqlParameter("@source", itinerary.Source);
            paramList[16] = new SqlParameter("@hotelId", SqlDbType.Int);
            paramList[17] = new SqlParameter("@isDomestic", itinerary.isDomestic);
            paramList[18] = new SqlParameter("@hotelCancelPolicy", itinerary.hotelCancelPolicy);
            paramList[19] = new SqlParameter("@cityRef", itinerary.cityRef);
            paramList[20] = new SqlParameter("@cancelId", itinerary.cancelId);
            paramList[21] = new SqlParameter("@flightInfo", itinerary.flightInfo);
            paramList[22] = new SqlParameter("@bookingRefNo", itinerary.bookingRefNo);
            paramList[23] = new SqlParameter("@map", itinerary.map);
            paramList[24] = new SqlParameter("@lastCancellationDate", itinerary.lastCancellationDate);
            paramList[25] = new SqlParameter("@voucherStatus", itinerary.voucherStatus);
            paramList[26] = new SqlParameter("@hotelCategory", itinerary.hotelCategory);
            paramList[27] = new SqlParameter("@agencyReference", itinerary.agencyReference);
            paramList[28] = new SqlParameter("@vatDescription", itinerary.vatDescription);
            paramList[29] = new SqlParameter("@paymentGuaranteedBy", itinerary.paymentGuaranteedBy);
            paramList[30] = new SqlParameter("@locationId", itinerary.LocationId);
            paramList[31] = new SqlParameter("@agencyId", itinerary.AgencyId);
            paramList[32] = new SqlParameter("@TransType", itinerary.transType);
            paramList[33] = new SqlParameter("@PaymentMode", (int)itinerary.paymentMode);
        if (!string.IsNullOrEmpty(itinerary.Gxsupplier)) paramList[34] = new SqlParameter("@GXsupplier", itinerary.Gxsupplier);
            paramList[35] = new SqlParameter("@remarks", itinerary.remarks);
                paramList[16].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHotelItinerary, paramList);
            hotelId = (int)paramList[16].Value;

            itinerary.HotelId = hotelId;
                // Saving rooms details
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    //decimal discount = itinerary.Roomtype[i].Price.Discount / itinerary.noOfRooms;//Commented By Harish
                    decimal discount = itinerary.Roomtype[i].Price.Discount;
                    itinerary.Roomtype[i].Price.Discount = discount;
                    itinerary.Roomtype[i].Price.Save();
                    itinerary.Roomtype[i].HotelId = hotelId;

                    //Cancel Policy Updates
                    //itinerary.Roomtype[i].Save();
                    int roomid = itinerary.Roomtype[i].Save();
                    if (itinerary.roomtype[i].CancelPolicy != null)
                    {
                        foreach (HotelCancelPolicy policy in itinerary.roomtype[i].CancelPolicy)
                        {
                            policy.HotelID = hotelId;
                            policy.RoomID = roomid;
                            policy.Save(policy);
                        }
                    }
                }

                // saving Lead passenger details 
                //itinerary.HotelPassenger.RoomId = 0;  
                //itinerary.HotelPassenger.Save();
                if (itinerary.penalityInfo != null)
                {
                    foreach (HotelPenality penality in itinerary.PenalityInfo)
                    {
                        penality.HotelId = hotelId;
                        penality.Save();
                    }
                }

            //Save Corporate profile flight policy
            if (hotelPolicy != null)
            {
                hotelPolicy.Flightid = hotelId;
                hotelPolicy.ProductId = 2;
                    try
                    {
                        //Selecting profile is client/vendor then need to find profileid based on hotelid
                        if (hotelPolicy.ProfileId < 0 && !string.IsNullOrEmpty(itinerary.Roomtype[0].PassenegerInfo[0].CorpProfileId))
                        {
                            hotelPolicy.ProfileId =Convert.ToInt32( itinerary.Roomtype[0].PassenegerInfo[0].CorpProfileId);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 0, "Exception at get hotelPolicy.ProfileId,reason is: "+ex.ToString(), "");
                    }
                    hotelPolicy.Save();
            }

                //Save promotion details
                if (PromoInfo != null)
                {
                    PromoInfo.CreatedBy = CreatedBy;
                    PromoInfo.ProductId = 2;
                    PromoInfo.ReferenceId = HotelId;
                    PromoInfo.Save();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            //Trace.TraceInformation("HotelItinerary.Save exiting");
        }

        /// <summary>
        /// This method is to retrieve the result from DB corresponding to hotelId
        /// </summary>
        /// <param name="hotelId">hotelId</param>
        public void Load(int hotelId)
        {
            //Trace.TraceInformation("HotelItinerary.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId Id should be positive integer", "hotelId");
            }
            this.hotelId = hotelId;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            try
            {
                //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelItinerary, paramList,connection);
                using (DataTable dtHotelItinerary = DBGateway.FillDataTableSP(SPNames.GetHotelItinerary, paramList))
                {
                    //if (data.Read())
                    if (dtHotelItinerary != null && dtHotelItinerary.Rows.Count > 0)
                    {
                        DataRow data = dtHotelItinerary.Rows[0];
                        confirmationNo = Convert.ToString(data["confirmationNo"]);
                        rating = (HotelRating)(Convert.ToInt32(data["starRating"]));
                        cityCode = Convert.ToString(data["cityCode"]);
                        hotelName = Convert.ToString(data["hotelName"]);
                        hotelCode = Convert.ToString(data["hotelCode"]);
                        hotelAddress1 = Convert.ToString(data["addressLine1"]);
                        hotelAddress2 = Convert.ToString(data["addressLine2"]);
                        startDate = Convert.ToDateTime(data["checkInDate"]);
                        endDate = Convert.ToDateTime(data["checkOutDate"]);
                        noOfRooms = Convert.ToInt16(data["noOfRooms"]);
                        hotelPolicyDetails = Convert.ToString(data["hotelPolicyDetails"]);
                        status = (HotelBookingStatus)(Convert.ToInt16(data["status"]));
                        if (data["specialRequest"] != null)
                        {
                            specialRequest = Convert.ToString(data["specialRequest"]);
                        }
                        isDomestic = Convert.ToBoolean(data["isDomestic"]);
                        createdBy = Convert.ToInt32(data["createdBy"]);
                        createdOn = Convert.ToDateTime(data["createdOn"]);
                        lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                        lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                        source = (HotelBookingSource)(Convert.ToInt32(data["source"]));
                        hotelCancelPolicy = Convert.ToString(data["hotelCancelPolicy"]);
                        cityRef = Convert.ToString(data["cityRef"]);
                        cancelId = Convert.ToString(data["cancellationId"]);
                        flightInfo = Convert.ToString(data["flightInfo"]);
                        bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                        map = Convert.ToString(data["map"]);
                        voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                        if (data["paymentGuaranteedBy"] != DBNull.Value)
                        {
                            paymentGuaranteedBy = Convert.ToString(data["paymentGuaranteedBy"]);
                        }
                        else
                        {
                            paymentGuaranteedBy = "";
                        }
                        if (data["lastCancellationDate"] != DBNull.Value)
                        {
                            lastCancellationDate = Convert.ToDateTime(data["lastCancellationDate"]);
                        }
                        if (!data["hotelCategory"].Equals(DBNull.Value))
                        {
                            hotelCategory = Convert.ToString(data["hotelCategory"]);
                        }
                        if (!data["agencyReference"].Equals(DBNull.Value))
                        {
                            agencyReference = Convert.ToString(data["agencyReference"]);
                        }
                        if (!data["vatDescription"].Equals(DBNull.Value))
                        {
                            vatDescription = Convert.ToString(data["vatDescription"]);
                        }
                        locationId = Convert.ToInt32(data["locationId"]);
                        agencyId = Convert.ToInt32(data["agencyId"]);
                        transType = Convert.ToString(data["TransType"]);
                        if (!data["paymentMode"].Equals(DBNull.Value))
                        {
                            paymentMode = (ModeOfPayment)Convert.ToInt32(data["paymentMode"]);
                        }
                       Gxsupplier= Convert.ToString(data["gxsupplier"]);
                        if (data["Remarks"] != DBNull.Value)
                        {
                            remarks = Convert.ToString(data["Remarks"]);
                        }
                        if(data["BookingRemarks"] !=DBNull.Value)
                        {
                            bookingRemarks = Convert.ToString(data["BookingRemarks"]);
                        }
                        if (data["systemreference"] != DBNull.Value)
                        {
                            AgentRef = Convert.ToString(data["systemreference"]);
                        }
                        else
                        {
                            AgentRef = "";
                        }
                        //data.Close();
                        //connection.Close();
                        //Trace.TraceInformation("HotelItinerary.Load exiting :" + hotelId.ToString());
                    }
                }
                //else
                //{
                //    //data.Close();
                //    //connection.Close();
                //    Trace.TraceInformation("HotelItinerary.Load exiting : hotelId does not exist.hotelId = " + hotelId.ToString());

                //}
                HotelPenality penality = new HotelPenality();
                penalityInfo = penality.GetHotelPenality(hotelId);
            }
            catch (Exception exp)
            {
                //connection.Close();
                throw new ArgumentException("Hotel id does not exist in database",exp);
            }

        }

        /// <summary>
        /// This method is to retrieve the HotelId from DB corresponding to confNo        /// </summary>
        /// <param name="confNo"></param>
        /// <returns>int</returns>
        public static int GetHotelId(string confNo)
        {
            //Trace.TraceInformation("HotelItinerary.GetHotelId entered : confNo = " + confNo);
            int hotelId = 0;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@confirmationNo", confNo);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelId, paramList,connection);
            using (DataTable dtHotelId = DBGateway.FillDataTableSP(SPNames.GetHotelId, paramList))
            {
                //if (data.Read())
                if (dtHotelId != null && dtHotelId.Rows.Count > 0)
                {
                    DataRow data = dtHotelId.Rows[0];
                    if (data != null && data["hotelId"] != DBNull.Value)
                    {
                        hotelId = Convert.ToInt32(data["hotelId"]);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("HotelItinerary.GetHotelId exiting :" + hotelId.ToString());
            return hotelId;
        }
        /// <summary>
        /// This method is to Update the Record into HotelItinerary DB
        /// </summary>
        public void Update()
        {
            //Trace.TraceInformation("HotelItinerary.Update entered  ");

            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@status", status);
            paramList[1] = new SqlParameter("@cancelId", cancelId);
            paramList[2] = new SqlParameter("@hotelId", hotelId);

            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelItineary, paramList);
            //Trace.TraceInformation("HotelItinerary.Update exiting count" + retVal);
        }
        /// <summary>
        /// This method is to UpdateVoucherStatus the Record into HotelItinerary DB
        /// </summary>
        public void UpdateVoucherStatus()
        {
            //Trace.TraceInformation("HotelItinerary.UpdateVoucherStatus entered  ");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@status", voucherStatus);
            paramList[1] = new SqlParameter("@hotelId", hotelId);

            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateVoucherStatus, paramList);
            //Trace.TraceInformation("HotelItinerary.UpdateVoucherStatus exiting count" + retVal);
        }
        /// <summary>
        /// This method is used for sending selected Hotel Itineararies.
        /// </summary>
        /// <param name="request">HotelRequest</param>
        /// <param name="result">HotelSearchResult</param>
        /// <param name="isPublishPrice">isPublishPrice</param>
        /// <param name="email">email</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="memberId">memberId</param>
        public static void CreateMail(HotelRequest request, HotelSearchResult[] result, Boolean isPublishPrice, string email, int agencyId, int memberId)
        {
           /* if (result != null) ziya-todo
            {

                string checkIn = string.Empty;
                string checkOut = string.Empty;
                Member member = new Member(memberId);
                Agency agency = new Agency(agencyId);
                checkIn = request.StartDate.ToShortDateString();
                checkOut = request.EndDate.ToString();
                string pax = string.Empty;
                decimal totalSellRate;
                decimal avgFare;
                string paxString = string.Empty;
                decimal rateofExchange = 1;
                Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
                StaticData staticInfo = new StaticData();
                rateOfExList = staticInfo.CurrencyROE;
                pax = Convert.ToString(request.RoomGuest[0].noOfAdults + request.RoomGuest[0].noOfChild);
                if (Convert.ToInt32(pax) > 1)
                {
                    paxString = "Passengers";
                }
                System.TimeSpan diffResult = request.EndDate.Subtract(request.StartDate);
                int noDays = diffResult.Days;
                StringBuilder sb = new StringBuilder();
                sb.Append("<body style=\"font-family:arial;font-size: 12px;margin: 0px;padding: 0px;background-color: White;color:#4c4c4b;\">");
                sb.Append("<div style=\"width:778px;margin: auto;\">");
                sb.Append("<div style=\"width:778px;float:left;\">");
                sb.Append("<div style=\"float:left;width:100%;border: solid 1px #c0c0c0;\">");
                sb.Append("<div style=\"float:left;width:97%;margin:0px;padding-top:20px;padding-left:0px;\">");
                sb.Append("<div style=\"float:left;width:100%;\">");
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\"><span style=\"float:left;font-size:15px;font-weight:bold;width:90px;\">City:</span><span style=\"float:left;font-weight:bold;width:0px;font-size:15px;\">" + request.CityName + "</span></p>");
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
                sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:90px;\">Check in:</span> ");
                sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + request.StartDate.ToString("dd MMM yy") + "</span></p>");
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
                sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:90px;\">Check out:</span> ");
                sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + request.EndDate.ToString("dd MMM yy") + " <em style=\"font-style:normal;font-size:12px;\">(" + noDays.ToString() + " Nights)</em></span></p>");
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:20px 0 0;\"><span style=\"float:left;font-size:15px;font-weight:bold;width:90px;\">Room(s):" + request.NoOfRooms.ToString() + "</span></p>");

                int adults = 0, chldren = 0;
                for (int i = 0; i < request.RoomGuest.Length; i++)
                {
                    adults += request.RoomGuest[i].noOfAdults;
                    chldren += request.RoomGuest[i].noOfChild;
                }
                sb.Append(" <p style=\"float:left; width:100%; margin:0; padding:0;\"><span style=\"float:left;font-size:15px;font-weight:bold;width:110px;\"> No.of Guest(s):</span><span style=\"float:left;width:80px;font-weight:bold;font-size:15px;\">" + (adults + chldren) + "  </span></p>");
                for (int j = 0; j < request.RoomGuest.Length; j++)
                {
                    sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\"><span style=\"float:left;font-size:15px;font-weight:bold;width:140px;\">Room " + (j + 1) + ":");
                    sb.Append("<b style=\"padding-left:3px;\">" + request.RoomGuest[j].noOfAdults + "</b> Adult(s)</span>");
                    if (request.RoomGuest[j].noOfChild > 0)
                    {
                        sb.Append("<span style=\"float:left;width:120px;font-weight:bold;font-size:15px;\">" + request.RoomGuest[j].noOfChild.ToString() + " Children</span>");
                    }
                    sb.Append("</p>");
                }
                sb.Append("</div></div>");

                foreach (HotelSearchResult sr in result)
                {
                    sb.Append("<div style=\"float:left; width:100%; border:solid 1px #ccc; margin:10px 0 0; padding:0 0 5px 0;\"><div style=\"float:left; width:98%; padding:5px;\"><div style=\"float:left; width:500px;\"><p style=\"float:left;font-size:17px;font-weight:bold; width:500px; margin:0; padding:0;\">");
                    sb.Append(sr.HotelName);
                    sb.Append("<span style=\"float:left;font-size:13px;font-weight:bold; padding:0; width:350px;\">");
                    CT.BookingEngine.HotelRoomsDetails room = new CT.BookingEngine.HotelRoomsDetails();
                    room = sr.RoomDetails[0];
                    if (rateOfExList.ContainsKey(sr.Currency))
                    {
                        rateofExchange = rateOfExList[sr.Currency];
                    }
                    string symbol = "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
                    if (sr.Currency == "USD")
                    {
                        symbol = "$";
                    }
                    else if (sr.Currency == "GBP")
                    {
                        symbol = "�";
                    }
                    else if (sr.Currency == "EUR")
                    {
                        symbol = "�";
                    }
                    if (Convert.ToInt16(sr.Rating) > 0)
                    {
                        sb.Append("(" + sr.Rating + ")");
                    }
                    sb.Append("<br />");
                    sb.Append(sr.HotelAddress);
                    sb.Append("</span><br /><span style=\"float:left;font-size:13px;font-weight:bold; padding:0; width:350px;\">");
                    sb.Append(request.CityName);
                    sb.Append(" </span></p></div>");
                    sb.Append("<div style=\"float:right;text-align:right;font-size:13px;font-weight:bold; padding:0; width:200px;\">");

                    decimal totFare = 0;
                    //if it is an international but not ian it will have sequence no.
                    if (!request.IsDomestic && sr.BookingSource != HotelBookingSource.IAN)
                    {
                        for (int x = 0; x < request.NoOfRooms; x++)
                        {
                            for (int y = 0; y < sr.RoomDetails.Length; y++)
                            {
                                if (sr.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                {
                                    totFare += sr.RoomDetails[y].SellingFare;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        totFare = room.TotalPrice;
                        if (sr.BookingSource == HotelBookingSource.TBOConnect)
                        {
                            totFare += (room.TotalPrice - room.ChildCharges) * sr.Price.AgentCommission / 100;
                            totFare = totFare * request.NoOfRooms;
                        }
                    }
                    if (!request.IsMultiRoom)
                    {
                        avgFare = totFare / request.NoOfRooms;
                        if (sr.BookingSource != HotelBookingSource.IAN)
                        {
                        decimal finalFare = Math.Round(avgFare * rateofExchange, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                            sb.Append("<h2 style=\"float:right;width:100%;margin:0;padding:0;\">" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "");
                            sb.Append(finalFare.ToString());
                            if (symbol != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                            {
                                sb.Append("(" + symbol + avgFare.ToString( Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + ")");
                            }
                        }
                        else
                        {
                            sb.Append("<h2 style=\"float:right;width:100%;margin:0;padding:0;\">");
                            if (room.SellingFare == room.TotalPrice)
                            {
                            sb.Append(symbol + Math.Round(room.TotalPrice, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                            }
                            else
                            {
                                sb.Append("<del style=\"font-size:13px; font-weight: bold;\">");
                                sb.Append(symbol + Math.Round(room.TotalPrice, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                                sb.Append("</del> <br />");
                                sb.Append(symbol + Math.Round(room.SellingFare, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                            }
                        }
                        sb.Append("</h2> <p style=\"float:right;width:100%;margin:0;padding:0;\">Lowest Total Rate</p><span style=\"float:right;width:100%;margin:0;padding:0;\">(per room)");
                    }
                    else
                    {
                        //in case of generalized multi room booking
                        sb.Append((Math.Round(totFare * rateofExchange, 0)).ToString());
                        if (symbol != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                        {
                            sb.Append("(" + symbol + totFare.ToString( Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + ")");
                        }
                        sb.Append("</h2> <p style=\"float:right;width:100%;margin:0;padding:0;\">Minimum Total Price</p><span style=\"float:right;width:100%;margin:0;padding:0;\">(Incl. of All Taxes)");
                    }
                    if (symbol != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                    {
                        sb.Append("<span style=\"float:right;width:100%;margin:0;padding:0;\">(converted from " + sr.Currency + ")</span>");
                    }
                    sb.Append("</span></div>");
                    sb.Append("<div style=\"float:left;width:99.5%;margin:0;padding:10px 0 0 5px;\">");
                    sb.Append(sr.HotelDescription + "</div></div>");
                    if (sr.BookingSource != HotelBookingSource.TBOConnect || sr.BookingSource == HotelBookingSource.TBOConnect && !request.IsMultiRoom)
                    {
                        sb.Append("<div style=\"float:left;width:100%;margin:10px 0 0;padding:3px 0;background:#eee;font-weight:bold;\"><span style=\"float:left;width:400px;margin:0;padding:0 0 0 5px;\">Room Type</span>");

                        if ((sr.Price != null && sr.Price.AccPriceType == PriceType.PublishedFare) || sr.BookingSource == HotelBookingSource.IAN)
                        {
                            sb.Append("<span style=\"float:right;width:120px;margin:0;padding:0;\">Published Rate</span></div>");
                        }
                        else if (sr.Price.AccPriceType == PriceType.NetFare)
                        {
                            sb.Append("<span style=\"float:right;width:120px;margin:0;padding:0;\"> Rate</span></div>");
                        }
                        decimal tempPrice = 0;
                        foreach (HotelRoomsDetails roomInfo in sr.RoomDetails)
                        {
                            sb.Append("<p style=\"width:100%;float:left;margin:0;padding:5px 0 0;\"><span style=\"width:400px;float:left;margin:0;padding:0 0 0 5px;\">");
                            sb.Append(roomInfo.RoomTypeName);
                            sb.Append("<br /><em style=\"font-style:normal;\">");
                            if (roomInfo.Amenities != null && roomInfo.Amenities.Count > 0)
                            {
                                sb.Append("Incl:");
                                foreach (string amenity in roomInfo.Amenities)
                                {
                                    sb.Append(amenity + " ");
                                }
                            }
                            if ((sr.Price != null && sr.Price.AccPriceType == PriceType.PublishedFare) || sr.BookingSource == HotelBookingSource.IAN)
                            {
                                sb.Append("</em></span>");
                                sb.Append("<span style=\"float:right;width:120px;margin:0;padding:0;\">" + symbol + " ");
                                if (sr.BookingSource != HotelBookingSource.TBOConnect)
                                {
                                    sb.Append(roomInfo.TotalPrice.ToString( Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
                                }
                                else
                                {
                                    tempPrice = roomInfo.TotalPrice + (roomInfo.TotalPrice * sr.Price.AgentCommission / 100);
                                    sb.Append(tempPrice.ToString( Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
                                }
                                sb.Append("</span></p>");
                            }
                            else
                            {
                                decimal netSellingRate = roomInfo.SellingFare + (roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length) + roomInfo.TotalTax;
                                sb.Append("</em></span><span style=\"float:right;width:100px;margin:0;padding:0;\">" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + " ");
                                sb.Append("" + Math.Round(netSellingRate * rateofExchange, 0).ToString("N2"));
                                if (symbol != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                                {
                                    sb.Append("(" + symbol + netSellingRate.ToString("N2") + ")");
                                }
                                sb.Append("</span></p>");
                            }

                        }
                    }
                    sb.Append("</div>");
                }
                sb.Append("</div></div></div></body>");
                List<string> toArray = new List<string>();
                string addressList = email;
                string[] addressArray = addressList.Split(',');
                for (int k = 0; k < addressArray.Length; k++)
                {
                    toArray.Add(addressArray[k]);
                }
                string from = Agency.GetAgencyEmail(agency.AgencyId);
                string subjectLine = "Your itineraries for Hotels";
                try
                {
                    Email.Send(from, from, toArray, subjectLine, sb.ToString(), new Hashtable());
                }
                catch (System.Net.Mail.SmtpException)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
            }*/
        }

        /// <summary>
        /// This menthod is used to update the Itinearary whille Amendments
        /// </summary>
        /// <param name="prod">Product</param>
        /// <param name="isItemChange">isItemChange</param>
        /// <param name="isPaxChange">isPaxChange</param>
        public void UpdateItinearary(Product prod, bool isItemChange, bool isPaxChange)
        {
            //Trace.TraceInformation("HotelItinerary.UpdateItinearary entered  ");
            HotelItinerary itinerary = (HotelItinerary)prod;
            // Loading Previous Itineary room Data to remember the Room ID
            HotelItinerary prevItineary = new HotelItinerary();
            prevItineary.Load(itinerary.hotelId);
            HotelRoom[] prevRoomData = new HotelRoom[prevItineary.noOfRooms];
            HotelRoom roomInfo = new HotelRoom();
            prevItineary.Roomtype = roomInfo.Load(itinerary.hotelId);
            prevRoomData = prevItineary.Roomtype;
            if (isItemChange)
            {
                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@confirmationNo", itinerary.ConfirmationNo);
                paramList[1] = new SqlParameter("@checkInDate", itinerary.StartDate);
                paramList[2] = new SqlParameter("@checkOutDate", itinerary.EndDate);
                paramList[3] = new SqlParameter("@status", itinerary.Status);
                paramList[4] = new SqlParameter("@lastModifiedBy", itinerary.LastModifiedBy);
                //temp fix to avoid html tags issue in hotel policy details    
                string sHotelPolicyDetails = itinerary.HotelEssentialBytes != null && string.IsNullOrEmpty(itinerary.HotelPolicyDetails) ?
                            Convert.ToString(GenericStatic.GetObjectWithByteArray(itinerary.HotelEssentialBytes)) : itinerary.HotelPolicyDetails;
                paramList[5] = new SqlParameter("@HotelPolicyDetails", sHotelPolicyDetails);
                paramList[6] = new SqlParameter("@hotelId", itinerary.hotelId);
                paramList[7] = new SqlParameter("@hotelCancelPolicy", itinerary.hotelCancelPolicy);
                paramList[8] = new SqlParameter("@bookingRefNo", itinerary.bookingRefNo);
                paramList[9] = new SqlParameter("@lastCancellationDate", itinerary.lastCancellationDate);
                paramList[10] = new SqlParameter("@voucherStatus", itinerary.voucherStatus);
                int retVal = DBGateway.ExecuteNonQuerySP(SPNames.AmendHotelItineary, paramList);

                //updating room Info
                //for (int i = 0; i < itinerary.Roomtype.Length; i++)
                //{
                //    itinerary.Roomtype[i].Price.PriceId = prevRoomData[i].PriceId;
                //    itinerary.Roomtype[i].Price.Save();
                //    itinerary.Roomtype[i].HotelId = hotelId;
                //    itinerary.Roomtype[i].UpdateRoom(prevRoomData[i].RoomId);

                //}
                ////Deleting Old Penality Info.
                //HotelPenality penalityInfo = new HotelPenality();
                //penalityInfo.DeletePenalityInfo(itinerary.hotelId);
                //// Adding new  Penality Info.
                //if (itinerary.penalityInfo != null)
                //{
                //    foreach (HotelPenality penality in itinerary.PenalityInfo)
                //    {
                //        penality.HotelId = hotelId;
                //        penality.Save();
                //    }
                //}
            }
            //if Pax change then update onll Pax information.
            if (isPaxChange)
            {
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    itinerary.Roomtype[i].UpdatePax(prevRoomData[i].RoomId, prevItineary.HotelId);
                }
            }
            //Trace.TraceInformation("HotelItinerary.UpdateItinearary exiting ");

        }

        /// <summary>
        /// whether booking exists against given bookingRefNo
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <param name="source">source</param>
        /// <returns>bool</returns>
        public bool IsBookingAlreadyExist(string confirmationNo, HotelBookingSource source)
        {
            bool isExists = false;
            List<int> hotelIdList = new List<int>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@confNo", confirmationNo);
                paramList[1] = new SqlParameter("@source", (int)source);
                //SqlConnection sqlConn = DBGateway.GetConnection();
                //SqlDataReader reader = DBGateway.ExecuteReaderSP(SPNames.IsBookingAlreadyExist, paramList,sqlConn);
                using (DataTable dtIsBookingExists = DBGateway.FillDataTableSP(SPNames.IsBookingAlreadyExist,paramList))
                {
                    if (dtIsBookingExists != null && dtIsBookingExists.Rows.Count > 0)
                    {
                        //while (reader.Read())
                        foreach(DataRow dr in dtIsBookingExists.Rows)
                        {
                            if (dr["hotelId"] != DBNull.Value)
                            {
                                hotelIdList.Add(Convert.ToInt32(dr["hotelId"]));
                            }
                        }
                    }
                }
                //reader.Close();
                //sqlConn.Close();
                if (hotelIdList.Count >= 1)
                {
                    isExists = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message,ex);
            }
            return isExists;
        }
        /// <summary>
        /// finding cancellation charges for  given bookingRefNo(GRN)
        /// </summary>
        /// <param name="bookingreference">bookingreference</param>
        /// <returns>string</returns>
        public static DataTable GetCancellationDetails(string bookingreference)
        {
            string cancelltionDetails = string.Empty;
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@bookingReference", bookingreference);
                return DBGateway.FillDataTableSP("usp_Get_HotelCancellationCharges", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return cancelltionDetails;
        }

        public static List<HotelItinerary> GetBookingData(DateTime startingDate, DateTime endingDate, string agentRef)
        {
            List<HotelItinerary> tempList = new List<HotelItinerary>();
            try
            {
                DateTime startDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
                DateTime endDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
                SqlParameter[] paramList = new SqlParameter[3];
                
                paramList[0] = new SqlParameter("@startDate", startDate);
                paramList[1] = new SqlParameter("@endDate", endDate);
                paramList[2] = new SqlParameter("@P_AGENT_Ref", agentRef);
                DataTable dtBookingdetail = DBGateway.ExecuteQuery("usp_GetAgentRefBooking", paramList).Tables[0];
                if (dtBookingdetail != null && dtBookingdetail.Rows.Count > 0)
                {
                    tempList = GetTransactionList(dtBookingdetail);
                }
            }
            catch (Exception ex) { throw ex; }
            return tempList;
        }

        private static List<HotelItinerary> GetTransactionList(DataTable dtBookingdetail)
        {

            List<HotelItinerary> tempList = new List<HotelItinerary>();
            try
            {

                if (dtBookingdetail != null)
                {
                    foreach (DataRow data in dtBookingdetail.Rows)
                    {
                        HotelItinerary instance = new HotelItinerary();
                        if (data["HotelName"] != DBNull.Value)
                        {
                            instance.HotelName = data["HotelName"].ToString();
                        }
                        if (data["confirmationNo"] != DBNull.Value)
                        {
                            instance.confirmationNo = data["confirmationNo"].ToString();
                        }
                        //if (data["source"] != DBNull.Value)
                        //{
                        //    instance.source = (HotelBookingSource)(Convert.ToInt32(data["source"]));
                        //}
                        //else
                        //{
                        //    instance.source = NUll;
                        //}
                        
                        tempList.Add(instance);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, " Error: " + ex.ToString(), "0");
                throw;
            }
            return tempList;
        }

        /// <summary>
        /// To get corporate trip summary email
        /// </summary>
        /// <returns></returns>
        public static DataTable GetCorpTripEmail(string sConfirmation)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@BookRefNo", sConfirmation);
                return DBGateway.FillDataTableSP("usp_GetCorpHotelTripEmail", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// To update hotel booking status and booking remarks if any
        /// </summary>
        /// <param name="iHotelId"></param>
        /// <param name="iStatus"></param>
        /// <param name="sRemrks"></param>
        /// <param name="iModifedBy"></param>
        public static void UpdateHotelBookingStatus(int iHotelId, int iStatus, string sRemrks, int iModifedBy)
        {
            try
            {
                List<SqlParameter> liSQLParams = new List<SqlParameter>();
                liSQLParams.Add(new SqlParameter("@HotelId", iHotelId));
                liSQLParams.Add(new SqlParameter("@ModifiedBy", iModifedBy));
                liSQLParams.Add(new SqlParameter("@BookRemarks", sRemrks));
                liSQLParams.Add(new SqlParameter("@Status", iStatus));

                int retVal = DBGateway.ExecuteNonQuerySP("usp_UpdateHotelBooking", liSQLParams.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.High, iModifedBy, "(HotelItinerary)UpdateHotelBookingStatus. Error: " + ex.Message, "0");
            }            
        }
               
        /// <summary>
        /// To get hotel voucher email for non coporate agents
        /// </summary>
        /// <returns></returns>
        public static DataTable GetHotelVoucherEmail(string sConfirmation)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@BookRefNo", sConfirmation);
                return DBGateway.FillDataTableSP("usp_GetHotelVoucherEmail", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get hotel voucher email for non coporate agents
        /// </summary>
        /// <returns></returns>
        public static DataTable CheckHotelBooking(HotelItinerary clsHI)
        {
            try
            {
                List<SqlParameter> liSQLParams = new List<SqlParameter>();
                liSQLParams.Add(new SqlParameter("@HotelName", clsHI.HotelName));
                liSQLParams.Add(new SqlParameter("@checkInDate", clsHI.StartDate));
                liSQLParams.Add(new SqlParameter("@checkOutDate", clsHI.EndDate));
                liSQLParams.Add(new SqlParameter("@noOfRooms", clsHI.NoOfRooms));
                liSQLParams.Add(new SqlParameter("@PaxFName", clsHI.Roomtype[0].PassenegerInfo[0].Firstname));
                liSQLParams.Add(new SqlParameter("@PaxLName", clsHI.Roomtype[0].PassenegerInfo[0].Lastname));
                return DBGateway.FillDataTableSP("usp_HotelDuplicateBook", liSQLParams.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// This method is to retrieve the get the status based on ConfirmationNumber
        /// </summary>
        /// <param name="Confno">Confno</param>
        public static string GetStatusbyConfirmationNo(string Confno)
        {
            string Status = "";

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@flag", "Select");
                paramList[1] = new SqlParameter("@confirmationno", Confno);

                using (DataTable dtstatus = DBGateway.FillDataTableSP(SPNames.UptStatusbyConfirmationNo, paramList))
                {
                    if (dtstatus != null && dtstatus.Rows.Count > 0)
                    {
                        DataRow data = dtstatus.Rows[0];
                        if (data["Status"] != DBNull.Value)
                        {
                            Status = Convert.ToString(data["Status"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Convert.ToString(Status);
        }
        /// <summary>
        /// This method is to update the status based on confirmationNumber
        /// </summary>
        /// <param name="Confno">Confno</param>
        /// <param name="sts">sts</param>
        /// <param name="lastmodifiedby">lastmodifiedby</param>
        public static int UpdateStatusbyConfirmationNo(string Confno, int sts, int lastmodifiedby,string remarks)
        {
            int rowsAffected = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@flag", "Update");
                paramList[1] = new SqlParameter("@confirmationno", Confno);
                paramList[2] = new SqlParameter("@statusId", sts);
                paramList[3] = new SqlParameter("@lastmodifiedby", lastmodifiedby);
                paramList[4] = new SqlParameter("@remarks", remarks);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UptStatusbyConfirmationNo, paramList);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Convert.ToInt32(rowsAffected);
        }
        #endregion

        /// <summary>
        /// This method is Get HotelItineraryInfo
        /// </summary>
        /// <param name="HotelId">HotelId</param>

        public static DataTable GetHotelItineraryInfo(int HotelId)
        {
            try
            {
                DataTable dtQ = new DataTable();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@hotelId", HotelId);
                dtQ = DBGateway.FillDataTableSP("usp_GetHotelItinerary", paramList);
                return dtQ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Saves pending queue details for hotel.
        /// </summary>
        /// <returns>Queue Id/Number of rows affected.</returns>
        public static HotelPendingQueue PreparePendingQueue(HotelItinerary clsHI)
        {
            HotelPendingQueue clsHPQ = new HotelPendingQueue();
            try
            {
                clsHPQ.AgencyId = clsHI.AgencyId;
                clsHPQ.Address1 = clsHI.HotelAddress1;
                clsHPQ.Address2 = clsHI.HotelAddress2;
                clsHPQ.AdultCount = clsHI.roomtype.Sum(x => x.AdultCount);
                clsHPQ.ChildCount = clsHI.roomtype.Sum(x => x.ChildCount);
                clsHPQ.Country = clsHI.DestinationCountryCode;
                clsHPQ.BookingId = 0;
                clsHPQ.BookingStatus = HotelBookingStatus.Pending;
                clsHPQ.CheckInDate = clsHI.StartDate;
                clsHPQ.CheckOutDate = clsHI.EndDate;
                clsHPQ.CityCode = clsHI.CityCode;
                clsHPQ.CityRef = clsHI.CityRef;
                clsHPQ.CreatedBy = clsHI.CreatedBy;
                clsHPQ.Email = clsHI.Roomtype[0].PassenegerInfo[0].Email;
                clsHPQ.HotelCode = clsHI.HotelCode;
                clsHPQ.HotelName = clsHI.HotelName;
                clsHPQ.IPAddress = clsHI.BookUserIP;
                clsHPQ.IsDomestic = false;
                clsHPQ.NumberOfRooms = clsHI.NoOfRooms;
                clsHPQ.OrderId = clsHI.OrderId;
                clsHPQ.PaymentAmount = clsHI.TotalPrice;
                clsHPQ.PaymentId = string.Empty;
                clsHPQ.PaymentStatus = (PayStatus)Enum.Parse(typeof(PayStatus), Convert.ToString(PayStatus.InProcess));
                clsHPQ.PaySource = (PaymentGatewaySource)Enum.Parse(typeof(PaymentGatewaySource), Convert.ToString(clsHI.PGSoruce));
                clsHPQ.Phone = clsHI.Roomtype[0].PassenegerInfo[0].Phoneno;
                clsHPQ.Remarks = clsHI.Remarks;
                clsHPQ.RoomName = string.Join("|", clsHI.Roomtype.Select(x => x.RoomName).ToArray()).Trim('|');
                clsHPQ.Source = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), clsHI.Source.ToString()); ;
                clsHPQ.StarRating = clsHI.Rating;
                clsHPQ.HotelQueueId = clsHI.PendingQueueId;

                List<HotelGuest> liHotelGuest = new List<HotelGuest>();
                for (int i = 0; i < clsHI.Roomtype.Length; i++)
                {
                    clsHI.Roomtype[i].PassenegerInfo.ForEach(y => {

                        liHotelGuest.Add(new HotelGuest
                        {
                            Title = y.Title,
                            FirstName = y.Firstname,
                            LastName = y.Lastname,
                            Email = y.Email,
                            Phoneno = y.Phoneno,
                            Addressline1 = y.Addressline1,
                            Addressline2 = y.Addressline2,
                            GuestType = y.PaxType,
                            Age = y.Age,
                            City = y.City,
                            State = y.State,
                            Country = y.Countrycode,
                            Zipcode = y.Zipcode,
                            Nationality = y.Nationality,
                            RoomIndex = i,
                            LeadGuest = y.LeadPassenger,
                            Countrycode = y.Country
                        });
                    });
                }

                clsHPQ.PassengerInfo = GenericStatic.GetXMLFromObject<List<HotelGuest>>(liHotelGuest, "Guest");
            }
            catch (Exception ex)
            {
                clsHPQ = new HotelPendingQueue();
            }
            return clsHPQ;
        }

        /// <summary>
        /// To prepare hotel item details object from hotel itinerary
        /// </summary>
        /// <param name="clsHI"></param>
        /// <param name="clsBookResp"></param>
        /// <returns></returns>
        public static HotelBookingDetails PrepareHotelBookDetails(HotelItinerary clsHI, BookingResponse clsBookResp)
        {
            HotelBookingDetails clsHBD = new HotelBookingDetails();
            try
            {
                clsHBD.AgencyId = clsHI.AgencyId;
                clsHBD.BookingId = clsBookResp.BookingId;
                clsHBD.ConfirmationNo = clsBookResp.ConfirmationNo;
                clsHBD.BookingRefNo = clsHI.BookingRefNo;
                clsHBD.BookingRefNo = clsHI.BookingRefNo;
                clsHBD.BookingStatus = HotelBookingStatus.Confirmed;
                clsHBD.Source = clsHI.Source;
                clsHBD.HotelCode = clsHI.HotelCode;
                clsHBD.HotelName = clsHI.HotelName;
                clsHBD.CheckInDate = clsHI.StartDate;
                clsHBD.CheckOutDate = clsHI.EndDate;
                clsHBD.CityCode = clsHI.CityCode;
                clsHBD.CityRef = clsHI.CityRef;
                clsHBD.Address1 = clsHI.HotelAddress1;
                clsHBD.Address2 = clsHI.HotelAddress2;
                clsHBD.StarRating = clsHI.Rating;
                clsHBD.NoOfRooms = clsHI.NoOfRooms;
                clsHBD.AdultCount = clsHI.Roomtype.Sum(x => x.AdultCount);
                clsHBD.ChildCount = clsHI.Roomtype.Sum(x => x.ChildCount);
                clsHBD.Country = clsHI.DestinationCountryCode;
                clsHBD.Email = clsHI.Roomtype[0].PassenegerInfo[0].Email;
                clsHBD.CreatedBy = clsHI.CreatedBy;
                clsHBD.LastModifiedBy = clsHI.CreatedBy;
                clsHBD.IPAddress = clsHI.BookUserIP;
                clsHBD.IsDomestic = clsHI.IsDomestic;
                clsHBD.PaymentAmount = clsHI.TotalPrice;
                clsHBD.PaySource = (PaymentGatewaySource)Enum.Parse(typeof(PaymentGatewaySource), Convert.ToString(clsHI.PGSoruce));
                clsHBD.PaymentId = clsHI.PaymentId;
                clsHBD.Phone = clsHI.Roomtype[0].PassenegerInfo[0].Phoneno;
                clsHBD.Remarks = clsBookResp.Status == BookingResponseStatus.Successful ? "Hotel Booked Successfully" : "Hotel Booking Failed";
                clsHBD.VoucherStatus = true;

                clsHBD.RoomName = string.Join("|", clsHI.Roomtype.Select(x => x.RoomName).ToArray()).Trim('|');

                List<HotelGuest> liHotelGuest = new List<HotelGuest>();
                for (int i = 0; i < clsHI.Roomtype.Length; i++)
                {
                    clsHI.Roomtype[i].PassenegerInfo.ForEach(y => {

                        liHotelGuest.Add(new HotelGuest
                        {
                            Title = y.Title,
                            FirstName = y.Firstname,
                            LastName = y.Lastname,
                            Email = y.Email,
                            Phoneno = y.Phoneno,
                            Addressline1 = y.Addressline1,
                            Addressline2 = y.Addressline2,
                            GuestType = y.PaxType,
                            Age = y.Age,
                            City = y.City,
                            State = y.State,
                            Country = y.Countrycode,
                            Zipcode = y.Zipcode,
                            Nationality = y.Nationality,
                            RoomIndex = i,
                            LeadGuest = y.LeadPassenger,
                            Countrycode = y.Country
                        });
                    });
                }

                clsHBD.PassengerInfo = GenericStatic.GetXMLFromObject<List<HotelGuest>>(liHotelGuest, "Guest");
            }
            catch (Exception ex1)
            {
                //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "WSAddHotelBookingResponse Error message " + ex1.ToString(), "");
            }
            return clsHBD;
        }

        /// <summary>
        /// To get hotel itinerary complete details
        /// </summary>
        /// <param name="sConfNo"></param>
        /// <returns></returns>
        public static HotelItinerary GetItineraryByConfNo(string sConfNo)
        {
            HotelItinerary clsHI = new HotelItinerary();
            try
            {
                BookingDetail clsBD = new BookingDetail(BookingDetail.GetBookingIdByProductId(GetHotelId(sConfNo), ProductType.Hotel));
                Product[] products = BookingDetail.GetProductsLine(clsBD.BookingId);

                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i].ProductTypeId == (int)ProductType.Hotel)
                    {
                        clsHI.Load(products[i].ProductId);
                        clsHI.Roomtype = new HotelRoom().Load(products[i].ProductId);
                        // clsHI.PaymentMode = ModeOfPayment.CreditCard;  
                        clsHI.BookingId = clsBD.BookingId;
                        clsHI.PromoInfo = new PromoDetails();
                        clsHI.PromoInfo.Load(clsHI.hotelId);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return clsHI;
        }

    }
}
