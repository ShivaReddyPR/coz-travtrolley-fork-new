﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;
using CT.Core;

namespace CT.BookingEngine
{
    public class FleetPassenger
    {

        int paxId;
        int fleetId;
        string title;
        string firstName;
        string lastName;
        string email;
        string documentsPath;
        int clientId;
        int agentId;
        string mobileNo;
        string licenseNumber;
        DateTime licenseExpiryDate;
        string licenseIssuePlace;
        string fileNames;
        string pickupAddress;
        string landmark;
        string mobileNo2;

        #region properities
        public int PaxId
        {
            get { return paxId; }
            set { paxId = value; }
        }
        public int FleetId
        {
            get { return fleetId; }
            set { fleetId = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string DocumentsPath
        {
            get { return documentsPath; }
            set { documentsPath = value; }
        }
        public int ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }
        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }
        public string MobileNo
        {
            get { return mobileNo; }
            set { mobileNo = value; }
        }

        public string LicenseNumber
        {

            get { return licenseNumber; }
            set { licenseNumber = value; }
        }

        public string LicenseIssuePlace
        {

            get { return licenseIssuePlace; }
            set { licenseIssuePlace = value; }
        }

        public DateTime LicenseExpiryDate
        {

            get { return licenseExpiryDate; }
            set { licenseExpiryDate = value; }
        }

        public string FileNames
        {

            get { return fileNames; }
            set { fileNames = value; }
        }
        public string PickupAddress
        {
            get { return pickupAddress; }
            set { pickupAddress = value; }
        }
        public string Landmark
        {
            get { return landmark; }
            set { landmark = value; }
        }
        public string MobileNo2
        {
            get { return mobileNo2; }
            set { mobileNo2 = value; }
        }

        #endregion

        #region Method

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[17];
                paramList[0] = new SqlParameter("@fleetId", Convert.ToInt32(fleetId));
                paramList[1] = new SqlParameter("@title", title);
                paramList[2] = new SqlParameter("@firstName", firstName);
                paramList[3] = new SqlParameter("@lastName", lastName);
                paramList[4] = new SqlParameter("@email", email);
                if(!string.IsNullOrEmpty(documentsPath)) paramList[5] = new SqlParameter("@documentsPath", documentsPath);
                paramList[6] = new SqlParameter("@clientId", clientId);
                paramList[7] = new SqlParameter("@agentID", agentId);
                paramList[8] = new SqlParameter("@mobileNo", mobileNo);
                if (!string.IsNullOrEmpty(licenseNumber)) paramList[9] = new SqlParameter("@licenseNumber", licenseNumber);
                if (licenseExpiryDate != DateTime.MinValue) paramList[10] = new SqlParameter("@licenseExpiryDate", licenseExpiryDate);
                if (!string.IsNullOrEmpty(licenseIssuePlace)) paramList[11] = new SqlParameter("@licenseIssuePlace",licenseIssuePlace);
                paramList[12] = new SqlParameter("@pxId", paxId);
                paramList[12].Direction = ParameterDirection.Output;
                if (!string.IsNullOrEmpty(licenseNumber)) paramList[13] = new SqlParameter("@fileNames", fileNames);
                if (!string.IsNullOrEmpty(PickupAddress)) paramList[14] = new SqlParameter("@pickupAddress", pickupAddress);
                if (!string.IsNullOrEmpty(landmark)) paramList[15] = new SqlParameter("@landmark", landmark);
                if (!string.IsNullOrEmpty(mobileNo2)) paramList[16] = new SqlParameter("@mobilNo2", mobileNo2);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.saveFleetPassengerDetails, paramList);
                PaxId = (int)paramList[12].Value;
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FleetPassenger> Load(int fleetId)
        {
            try
            {
                List<FleetPassenger> lstPaxDet = new List<FleetPassenger>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@fleetId", fleetId);
                DataTable dt = DBGateway.FillDataTableSP(SPNames.GetFleetPassengerDetails, paramList);
                foreach (DataRow dr in dt.Rows)
                {
                    FleetPassenger fleetPassengerDet = new FleetPassenger();
                    fleetPassengerDet.paxId = Convert.ToInt32(dr["paxid"]);
                    fleetPassengerDet.fleetId = Convert.ToInt32(dr["fleetId"]);
                    fleetPassengerDet.title = Convert.ToString(dr["title"]);
                    fleetPassengerDet.firstName = Convert.ToString(dr["firstName"]);
                    fleetPassengerDet.lastName = Convert.ToString(dr["lastName"]);
                    fleetPassengerDet.email = Convert.ToString(dr["email"]);
                    fleetPassengerDet.documentsPath = Convert.ToString(dr["documentsPath"]);
                    fleetPassengerDet.clientId = Convert.ToInt32(dr["clientId"]);
                    fleetPassengerDet.agentId = Convert.ToInt32(dr["agentId"]);
                    fleetPassengerDet.mobileNo = Convert.ToString(dr["MobileNo"]);
                    if (dr["licenseIssuePlace"] != DBNull.Value)
                    {
                        fleetPassengerDet.licenseIssuePlace = Convert.ToString(dr["licenseIssuePlace"]);
                    }
                    if (dr["licenseNo"] != DBNull.Value)
                    {
                        fleetPassengerDet.licenseNumber = Convert.ToString(dr["licenseNo"]);
                    }
                    if (dr["licenseExpiryDate"] != DBNull.Value)
                    {
                        fleetPassengerDet.licenseExpiryDate = Convert.ToDateTime(dr["licenseExpiryDate"]);
                    }
                    if (dr["fileNames"] != DBNull.Value)
                    {
                        fleetPassengerDet.fileNames = Convert.ToString(dr["fileNames"]);
                    }
                    if (dr["pickupAddress"] != DBNull.Value)
                    {
                        fleetPassengerDet.pickupAddress = Convert.ToString(dr["pickupAddress"]);
                    }
                    if (dr["landmark"] != DBNull.Value)
                    {
                        fleetPassengerDet.landmark = Convert.ToString(dr["landmark"]);
                    }
                    if (dr["MobileNo2"] != DBNull.Value)
                    {
                        fleetPassengerDet.mobileNo2 = Convert.ToString(dr["MobileNo2"]);
                    }
                    lstPaxDet.Add(fleetPassengerDet);
                }
                return lstPaxDet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetPaxDetailsByPaxName(int agencyId,string searchText)
        {
            DataTable dtCustomer = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@searchText", searchText);
                paramList[1] = new SqlParameter("@agencyId", agencyId);
                dtCustomer = DBGateway.FillSP(SPNames.GetPaxDetailsByPaxName, paramList).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, agencyId, "Not able to Get Customers Name while searching for agencyId +" + agencyId + " Search Text was " + searchText + " .Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
            }
            return dtCustomer;    
        }
        public static DataTable LoadPaxDetails(int paxId)
        {
            DataTable dtPaxDetails = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@paxId", paxId);
                dtPaxDetails = DBGateway.FillSP(SPNames.GetPaxDetailsBypaxId, paramList).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, 0, "Not able to Get paxDetails by paxid"+ " .Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
            }
            return dtPaxDetails;   
        }
        #endregion
    }
}
