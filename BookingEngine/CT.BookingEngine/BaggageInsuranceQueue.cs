﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using System.Linq;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine
{
    public class BaggageInsuranceQueue
    {
        public DataTable GetBaggageInsuranceQueueList(int agentId, DateTime date, string Pnr,string agentType,string Transtype)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param= new SqlParameter[5];
            param[0] = new SqlParameter("@AgentId", agentId);
            if(!string.IsNullOrEmpty(Pnr))
            {
                param[1]= new SqlParameter("@PNR", Pnr);
            }
            else
            {
                param[1] = new SqlParameter("@PNR", DBNull.Value);
            }
           
            param[2] = new SqlParameter("@CreatedDate", date);
          
            if(!string.IsNullOrEmpty(agentType))
            {
                param[3] = new SqlParameter("@AgentType", agentType);
            }
            else
            {
                param[3] = new SqlParameter("@AgentType", DBNull.Value);
            }
            if (!string.IsNullOrEmpty(Transtype))
            {
                param[4] = new SqlParameter("@TransType", Transtype);
            }
            else
            {
                param[4] = new SqlParameter("@TransType", DBNull.Value);
            }
            dt = DBGateway.FillDataTableSP(SPNames.GetbaggageQueue, param);

            return dt;
        }
        public DataTable GetBaggageInsuranceChangeReqQueueList(int agentId, DateTime fromdate, DateTime todate, string Pnr, string policyNo, int UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@AgentId", agentId);
            if(!string.IsNullOrEmpty(Pnr))
            {
                param[1] = new SqlParameter("@PNR", Pnr);
            }
            else
            {
                param[1] = new SqlParameter("@PNR", DBNull.Value);
            }
            param[2] = new SqlParameter("@FromDate", fromdate);
            param[3] = new SqlParameter("@ToDATE", todate);
            if (!string.IsNullOrEmpty(policyNo))
            {
                param[4] = new SqlParameter("@PolicyNo", policyNo);
            }
            else
            {
                param[4] = new SqlParameter("@PolicyNo", DBNull.Value);
            }
            param[5] = new SqlParameter("@UserID", UserID);

            dt = DBGateway.FillDataTableSP(SPNames.GetBaggageInsChangeReqQueue, param);

            DataTable dtMarkup = UpdateMarkup.Load(agentId, BookingSource.Baggage.ToString(), (int)ProductType.Insurance);

            return dt;
        }
        public DataTable GetPlanDetailsById(int planId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                parm[0] = new SqlParameter("@PlanId", planId);
                dt = DBGateway.FillDataTableSP(SPNames.GetPlanDetailsByPlanID, parm);  
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
