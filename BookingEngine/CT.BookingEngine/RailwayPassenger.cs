using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    //Offline Train Booking
    /// <summary>
    /// This class is using to save the RailwayPassenger in DB and retrive the RailwayPassenger from DB
    /// </summary>
    [Serializable ]
    public class RailwayPassenger
    {
        #region public properties
        /// <summary>
        /// RBDId - PaxId
        /// </summary>
        public long PaxId { get; set; }
        /// <summary>
        /// RBId - BooingID
        /// </summary>
        public long BookingID { get; set; }
        /// <summary>
        /// PaxName
        /// </summary>
        public string PaxName { get; set; }
        /// <summary>
        /// Age
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// Gender
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// BerthPreference
        /// </summary>
        public string BerthPreference { get; set; }
        /// <summary>
        /// IsSeniorCitizen
        /// </summary>
        public bool IsSeniorCitizen { get; set; }
        /// <summary>
        /// MealPreference
        /// </summary>
        public string MealPreference { get; set; }
        /// <summary>
        /// IsChild
        /// </summary>
        public bool IsChild { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        public int CreatedBy { get; set; }
        /// <summary>
        /// CreatedOn
        /// </summary>
        public DateTime CreatedOn { get; set; }
        /// <summary>
        /// ModifiedBy
        /// </summary>
        public int? ModifiedBy { get; set; }
        /// <summary>
        /// ModifiedOn
        /// </summary>
        public DateTime? ModifiedOn { get; set; }
        /// <summary>
        /// IsLeadPassenger
        /// </summary>
        public bool IsLeadPassenger { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// This Method is used to Save the Hotel Passenger details
        /// </summary>
        /// <returns>int</returns>
        public void SavePassengers()
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@RBId", BookingID));
                paramList.Add(new SqlParameter("@PaxName", PaxName));
                paramList.Add(new SqlParameter("@Age", Age));
                paramList.Add(new SqlParameter("@Gender", Gender));
                if (BerthPreference != null)
                    paramList.Add(new SqlParameter("@BerthPreference", BerthPreference));
                else
                    paramList.Add(new SqlParameter("@BerthPreference", DBNull.Value));
                paramList.Add(new SqlParameter("@IsSeniorCitizen", IsSeniorCitizen));
                paramList.Add(new SqlParameter("@MealPreference", MealPreference));
                paramList.Add(new SqlParameter("@IsChild", IsChild));
                paramList.Add(new SqlParameter("@CreatedBy", CreatedBy));
                paramList.Add(new SqlParameter("@IsLeadPassenger", IsLeadPassenger));
                DBGateway.ExecuteNonQuerySP("usp_AddRailwayPassenger", paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        # endregion
    }
}
