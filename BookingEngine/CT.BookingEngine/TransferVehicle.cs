using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    [Serializable]
    public class TransferVehicle
    {
        private string vehicle;
        private int transferId;
        private string vehicleCode;
        private int vehicleMaximumPassengers;
        private int vehicleMaximumLuggage;
        private PriceAccounts itemPrice;
        private string currency;
        private int occPax;

        public string Vehicle
        {
            get { return vehicle; }
            set { vehicle = value; }
        }
        public int TransferId
        {
            get { return transferId; }
            set { transferId = value; }

        }
        public string VehicleCode
        {
            get
            {
                return vehicleCode;
            }
            set { vehicleCode = value; }
        }
        public int VehicleMaximumPassengers
        {
            get { return vehicleMaximumPassengers; }
            set { vehicleMaximumPassengers = value; }
        }

        public int VehicleMaximumLuggage
        {
            get { return vehicleMaximumLuggage; }
            set { vehicleMaximumLuggage = value; }
        }
        public PriceAccounts ItemPrice
        {
            get { return itemPrice; }
            set { itemPrice = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public int OccupiedPax
        {
            get { return occPax; }
            set { occPax = value; }
        }

        public string CabinClass { get; set; }
        public int AnimalLuggage { get; set; }
        public int SportLuggage { get; set; }
        public string ChildType { get; set; }
        public string Distance { get; set; }
        public string TravelTime { get; set; }

        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string AirlineCode { get; set; }
        public string FlightNumber { get; set; }
        public string DepartureCity { get; set; }
        //public string VehicleId { get; set; }

        /// <summary>
        /// This Method is used to save the Transfer Vehicle Details
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("TransferVehicle.Save entered.");
            SqlParameter[] paramList = new SqlParameter[15];
            paramList[0] = new SqlParameter("@transferId", transferId);
            paramList[1] = new SqlParameter("@vehicleCode", vehicleCode);
            paramList[2] = new SqlParameter("@vehicleName", vehicle);
            paramList[3] = new SqlParameter("@maxPax", vehicleMaximumPassengers);
            paramList[4] = new SqlParameter("@maxLauggage", vehicleMaximumLuggage);
            paramList[5] = new SqlParameter("@occPax", occPax);
            paramList[6] = new SqlParameter("@currency", currency);
            paramList[7] = new SqlParameter("@priceId", ItemPrice.PriceId);
            //New Field for Talixo
            paramList[8] = new SqlParameter("@distance", Distance);
            paramList[9] = new SqlParameter("@travelTime", TravelTime);
            paramList[10] = new SqlParameter("@startTime", StartTime);
            paramList[11] = new SqlParameter("@endTime", EndTime);
            paramList[12] = new SqlParameter("@airlineCode", AirlineCode);
            paramList[13] = new SqlParameter("@flightNumber", FlightNumber);
            paramList[14] = new SqlParameter("@departureCity", DepartureCity);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddTransferVehicle, paramList);
            //Trace.TraceInformation("TransferVehicle.Save exiting");    
        }

        /// <summary>
        /// This Method is used to Load the Vehicle Details based on transferID.
        /// </summary>
        /// <param name="transferId"></param>
        public List<TransferVehicle> Load(int trId)
        {
            //Trace.TraceInformation("TransferVehicle.Load entered.");
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transferId", trId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetVehicleByTransferId, paramList, connection);
            List<TransferVehicle> vehList = new List<TransferVehicle>();
            using (DataTable dtVehicle = DBGateway.FillDataTableSP(SPNames.GetVehicleByTransferId, paramList))
            {
                if (dtVehicle != null && dtVehicle.Rows.Count > 0)
                {
                    foreach(DataRow data in dtVehicle.Rows)
                    {
                        TransferVehicle vehInfo = new TransferVehicle();
                        vehInfo.TransferId = trId;
                        vehInfo.VehicleCode =Convert.ToString(data["vehicleCode"]);
                        vehInfo.Vehicle = Convert.ToString(data["vehicleName"]);
                        vehInfo.VehicleMaximumPassengers = Convert.ToInt32(data["maxPax"]);
                        vehInfo.VehicleMaximumLuggage = Convert.ToInt32(data["maxLauggage"]);
                        vehInfo.OccupiedPax = Convert.ToInt32(data["occPax"]);
                        vehInfo.Currency = Convert.ToString(data["currency"]);
                        PriceAccounts pAcc = new PriceAccounts();
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        gstTaxList = GSTTaxDetail.LoadGSTDetailsByPriceId(long.Parse(data["priceId"].ToString()));
                        pAcc.Load(Convert.ToInt32(data["priceId"]));
                        pAcc.GSTDetailList = gstTaxList;
                        vehInfo.ItemPrice = pAcc;
                        vehInfo.Distance = data["distance"].ToString();
                        vehInfo.TravelTime = data["travelTime"].ToString();
                        vehInfo.AirlineCode = data["airlineCode"].ToString();
                        vehInfo.FlightNumber = data["flightNumber"].ToString();
                        vehInfo.DepartureCity = data["departureCity"].ToString();
                        vehInfo.StartTime =DateTimeOffset.ParseExact(data["startTime"].ToString(), "yyyy-MM-dd'T'HH:mm:sszzz", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd'T'HH:mm");
                        vehInfo.EndTime = DateTimeOffset.ParseExact(data["endTime"].ToString(), "yyyy-MM-dd'T'HH:mm:sszzz", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd'T'HH:mm");
                        vehList.Add(vehInfo);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("TransferVehicle.Load exiting");
            return vehList;
        }


    }

}
