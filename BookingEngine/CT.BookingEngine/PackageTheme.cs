﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.BookingEngine
{
    public class PackageTheme
    {
        static string masterDB = System.Configuration.ConfigurationManager.AppSettings["MasterDB"].ToString();
        private int themeId;
        private string themeName;
        private bool isActive;
        public int ThemeId
        {
            get
            {
                return this.themeId;
            }
            set
            {
                this.themeId = value;
            }
        }
        public string ThemeName
        {
            get
            {
                return this.themeName;
            }
            set
            {
                this.themeName = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return this.isActive;
            }
            set
            {
                this.isActive = value;
            }
        }
        public static List<PackageTheme> Load()
        {
            List<PackageTheme> list = new List<PackageTheme>();
            SqlDataReader sqlDataReader = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[0];
                try
                {
                    sqlDataReader = DBGateway.ExecuteReaderSP(masterDB+"usp_GetAllActiveHotelThemes", paramList, connection);
                    while (sqlDataReader.Read())
                    {
                        list.Add(new PackageTheme
                        {
                            themeId = Convert.ToInt32(sqlDataReader["themeId"]),
                            themeName = Convert.ToString(sqlDataReader["themeName"]),
                            isActive = Convert.ToBoolean(sqlDataReader["isActive"])
                        });
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, "Exception in Load() for Theme.cs , Error Message:" + ex.Message + "| Stack //Trace:" + ex.StackTrace, "");
                    throw new Exception("Unable to load themes for StaticPackages.aspx.");
                }
                finally
                {
                    if (sqlDataReader != null)
                    {
                        sqlDataReader.Close();
                    }
                    connection.Close();
                }
            }
            return list;
        }
    }
}
