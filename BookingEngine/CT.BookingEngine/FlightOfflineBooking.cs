﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public class FlightOfflineBooking
    {
        #region properties
        int _id;
        string _depAirport;
        string _arrAirport;
        DateTime _depDateTime;
        DateTime _arrDateTime;
        string _depAirPre;
        string _arrAirPre;
        string _depFlightNo;
        string _arrFlightNo;
        int _agentId;
        int _locationId;
        int _userId;
        int _createdBy;
        DataTable _offlineDetails;
        int _retid;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string DepAirport
        {
            get { return _depAirport; }
            set { _depAirport = value; }
        }
        public string ArrAirport
        {
            get { return _arrAirport; }
            set { _arrAirport = value; }
        }
        public DateTime DepDateTime
        {
            get { return _depDateTime; }
            set { _depDateTime = value; }
        }
        public DateTime ArrDateTime
        {
            get { return _arrDateTime; }
            set { _arrDateTime = value; }
        }
        public string DepAirPre
        {
            get { return _depAirPre; }
            set { _depAirPre = value; }
        }
        public string ArrAirPre
        {
            get { return _arrAirPre; }
            set { _arrAirPre = value; }
        }
        public string DepFlightNo
        {
            get { return _depFlightNo; }
            set { _depFlightNo = value; }
        }
        public string ArrFlightNo
        {
            get { return _arrFlightNo; }
            set { _arrFlightNo = value; }
        }
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public int LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public DataTable OfflineDetails
        {
            get { return _offlineDetails; }
            set { _offlineDetails = value; }
        }

        #endregion
        #region Constructors
        public FlightOfflineBooking()
        {
            getDetails(-1);
        }
        public FlightOfflineBooking(int id)
        {
            if (id > 0)
            {
                getDetails(id);
            }
        }
        #endregion
        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
             UpdateBusinessData(ds);
        }
        private DataSet GetData(long id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_FOId_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_Get_Flight_OfflineData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                    if (ds.Tables[1] != null)
                    {
                        _offlineDetails = ds.Tables[1];
                    }
                }
            }
            catch
            { throw; }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            _depAirport = Convert.ToString(dr["DepAirport"]);
            _arrAirport = Convert.ToString(dr["ArrAirport"]);
            _depDateTime = Convert.ToDateTime(dr["DepDateTime"]);
            if (dr["ArrDateTime"] != DBNull.Value) _arrDateTime = Convert.ToDateTime(dr["ArrDateTime"]);
            if (dr["DepAirPre"] != DBNull.Value) _depAirPre = Convert.ToString(dr["DepAirPre"]);
            if (dr["ArrAirPre"] != DBNull.Value) _arrAirPre = Convert.ToString(dr["ArrAirPre"]);
            _depFlightNo = Convert.ToString(dr["DepFlightNo"]);
            if (dr["ArrFlightNo"] != DBNull.Value) _arrFlightNo = Convert.ToString(dr["ArrFlightNo"]);
            _agentId = Convert.ToInt32(dr["AgentId"]);
            _locationId = Convert.ToInt32(dr["LocationId"]);
            _userId = Convert.ToInt32(dr["UserId"]);
            _createdBy = Convert.ToInt32(dr["CreatedBy"]);
            //_offlineDetails = Convert.ToString(dr["DepAirport"]);
           if(dr["FOId"] != DBNull.Value) _retid = Convert.ToInt32(dr["FOId"]);
        }
        #endregion

        #region Public Methods
        //Saving Header
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                SqlParameter[] paramList = new SqlParameter[14];

                paramList[0] = new SqlParameter("@P_FOId", _id);
                paramList[1] = new SqlParameter("@P_DepAirport", _depAirport);
                paramList[2] = new SqlParameter("@P_ArrAirport", _arrAirport);
                paramList[3] = new SqlParameter("@P_DepDateTime", _depDateTime);
                if (_arrDateTime != DateTime.MinValue) paramList[4] = new SqlParameter("@p_ArrDateTime", _arrDateTime);
                if (!string.IsNullOrEmpty(_depAirPre)) paramList[5] = new SqlParameter("@P_DepAirPre", _depAirPre);
                if (!string.IsNullOrEmpty(_arrAirPre)) paramList[6] = new SqlParameter("@P_ArrAirPre", _arrAirPre);
                if (!string.IsNullOrEmpty(_depFlightNo)) paramList[7] = new SqlParameter("@P_DepFlightNo", _depFlightNo);
                if (!string.IsNullOrEmpty(_arrFlightNo)) paramList[8] = new SqlParameter("@P_ArrFlightNo", _arrFlightNo);
                paramList[9] = new SqlParameter("@P_AgentId", _agentId);
                paramList[10] = new SqlParameter("@P_LocationId", _locationId);
                paramList[11] = new SqlParameter("@P_UserId", _userId);
                paramList[12] = new SqlParameter("@P_CreatedBy", _createdBy);
                paramList[13] = new SqlParameter("@P_FOId_RET", SqlDbType.Int, 200);
                paramList[13].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQueryDetails(cmd, "usp_Flight_OfflineHeader_Add", paramList);
                _retid = Convert.ToInt32(paramList[13].Value);

                foreach (DataRow dr in _offlineDetails.Rows)
                {
                    SaveOfflineDetails(cmd, _retid, Convert.ToString(dr["PaxType"]), Convert.ToString(dr["Title"]), Convert.ToString(dr["SurName"]), Convert.ToString(dr["FirstName"]), Convert.ToDateTime(dr["DateOfBirth"]), Convert.ToString(dr["PassportNo"]), Convert.ToDateTime(dr["PassportExpdate"]), Convert.ToString(dr["PlaceOfIssue"]), Convert.ToInt32(dr["CreatedBy"]), Convert.ToString(dr["Email"]));
                }
                trans.Commit();
            }
            catch
            {
                throw;
            }
        }
        //Saving Details
        public void SaveOfflineDetails(SqlCommand cmd, int retId, string paxType, string title, string surName, string firstName, DateTime dateOfBirth, string passportNo, DateTime passportExpDate, string placeIssue, int createdBy,string email)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@P_FOId", retId);
                paramList[1] = new SqlParameter("@P_PaxType", paxType);
                paramList[2] = new SqlParameter("@P_Title", title);
                paramList[3] = new SqlParameter("@P_SurName", surName);
                paramList[4] = new SqlParameter("@p_FirstName", firstName);
                paramList[5] = new SqlParameter("@P_DateOfBirth", dateOfBirth);
                paramList[6] = new SqlParameter("@P_PassportNo", passportNo);
                paramList[7] = new SqlParameter("@P_PassportExpdate", passportExpDate);
                paramList[8] = new SqlParameter("@P_PlaceOfIssue", placeIssue);
                paramList[9] = new SqlParameter("@P_CreatedBy", createdBy);
                paramList[10] = new SqlParameter("@P_Email", email);
                DBGateway.ExecuteNonQueryDetails(cmd, "usp_Flight_OfflineDetails_Add", paramList);
            }
            catch
            {
                throw;
            }
        }
        //Added By Chandan
        public DataSet getOfflineBookingDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
            return ds;
        }
        #endregion

        #region Static Methods
        public static DataTable GetOfflineBookingList(DateTime _fromDate, DateTime _toDate, int AgentId, long UserId, string agentType)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[5];
                    paramList[0] = new SqlParameter("@FROMDATE", _fromDate);
                    paramList[1] = new SqlParameter("@TODATE", _toDate);
                    if (AgentId > 0) paramList[2] = new SqlParameter("@AgencyId", AgentId);
                   paramList[3] = new SqlParameter("@USER_ID", UserId);
                   paramList[4] = new SqlParameter("@AGENT_TYPE", agentType);
                    data = DBGateway.ExecuteReaderSP("BKE_GetOfflineBookingList", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        // public static DataTable GetOfflineBookingDetails(int OfflineBookingId)
        //{
        //    SqlDataReader data = null;
        //    DataTable dt = new DataTable();
        //    using (SqlConnection connection = DBGateway.GetConnection())
        //    {
        //        SqlParameter[] paramList;
        //        try
        //        {
        //            paramList = new SqlParameter[1];
        //            paramList[0] = new SqlParameter("@OfflinBookingId", OfflineBookingId);
        //            data = DBGateway.ExecuteReaderSP("BKE_GetOfflineBookingDetails", paramList, connection);
        //            if (data != null)
        //            {
        //                dt.Load(data);
        //            }
        //            connection.Close();
        //        }
        //        catch
        //        {
        //            throw;
        //        }
        //    }
        //    return dt;
        //}
        
        #endregion
    }
   
}
