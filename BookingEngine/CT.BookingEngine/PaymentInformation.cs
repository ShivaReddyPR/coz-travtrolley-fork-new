﻿using System;

namespace CT.BookingEngine
{
    [Serializable]
    public class PaymentInformation
    {
        private string paymentId;
        public string PaymentId
        {
            get
            {
                return paymentId;
            }
            set
            {
                paymentId = value;
            }
        }

        private decimal amount;
        public decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }

        private string ipAddress;
        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }

        private string trackId;
        public string TrackId
        {
            get
            {
                return trackId;
            }
            set
            {
                trackId = value;
            }
        }

        private PaymentGatewaySource paymentGateway;
        public PaymentGatewaySource PaymentGateway
        {
            get
            {
                return paymentGateway;
            }
            set
            {
                paymentGateway = value;
            }
        }

        private PaymentModeType paymentModeType;
        public PaymentModeType PaymentModeType
        {
            get
            {
                return paymentModeType;
            }
            set
            {
                paymentModeType = value;
            }
        }
        //CreditCaredCharges
        private decimal creditCardCharges;
        public decimal CreditCardCharges
        {
            get { return creditCardCharges; }
            set { creditCardCharges = value; }
        }



        public PaymentInformation()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
    public enum PaymentModeType
    {
        Default=0,
        CreditCard = 1,
        Deposited = 2
    }
}
