using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using System.Net;
namespace CT.BookingEngine
{
    /// <summary>
    /// This class is using to save the HotelImages in DB and retrive the HotelImages from DB
    /// </summary>
    public class HotelImages
    {
        #region variables
        /// <summary>
        /// UniqueId
        /// </summary>
        private int id;
        /// <summary>
        /// Hotelcode
        /// </summary>
        private string hotelCode;
        /// <summary>
        /// Images
        /// </summary>
        private string images;
        /// <summary>
        /// cityCode
        /// </summary>
        private string cityCode;
        /// <summary>
        /// downloadedImgs
        /// </summary>
        private string downloadedImgs;
        /// <summary>
        /// Hotel source
        /// </summary>
        private HotelBookingSource source;
        #endregion
        #region properities
        /// <summary>
        /// unique Id
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        /// <summary>
        /// HotelCode
        /// </summary>
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }
        /// <summary>
        /// Images
        /// </summary>
        public string Images
        {
            get { return images; }
            set { images = value; }
        }
        /// <summary>
        /// CityCode
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        /// <summary>
        /// DownloadedImgs
        /// </summary>
        public string DownloadedImgs
        {
            get { return downloadedImgs; }
            set { downloadedImgs = value; }
        }
        /// <summary>
        /// Source
        /// </summary>
        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        #endregion
        /// <summary>
        /// This method Load the a images based on citycode and hotelCode
        /// </summary>
        /// <param name="hotel">hotel</param>
        /// <param name="cityCode">cityCode</param>
        /// <param name="hSource">Source</param>
        public void Load(string hotel, string cityCode, HotelBookingSource hSource)
        {
            ////Trace.TraceInformation("HotelImages.Load entered : hotelCode = " + hotel + " hotelSource = " + hSource);
            if (hotelCode != null && hotelCode.Length == 0)
            {
                throw new ArgumentException("hotelCode Should be enterd !", "hotelCode");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@hotelCode", hotel);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@source", (int)hSource);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelImagesByCode, paramList,connection);
            using (DataTable dtImages = DBGateway.FillDataTableSP(SPNames.GetHotelImagesByCode, paramList))
            {
                //if (data.Read())
                if (dtImages != null && dtImages.Rows.Count > 0)
                {

                    DataRow data = dtImages.Rows[0];
                    this.cityCode = cityCode;
                    hotelCode = Convert.ToString(data["hotelCode"]);
                    images = Convert.ToString(data["imageFile"]);
                    downloadedImgs = Convert.ToString(data["images"]);
                    source = (HotelBookingSource)Convert.ToInt16(data["source"]);
                }
            }
            //data.Close();
            //connection.Close();
            //Trace.TraceInformation("HotelImages.Load Exit ");
        }

        /// <summary>
        ///  This method is to save the Record into HotelImages DB
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("HotelImages.Save entered.");

            // no  need to Download Images here since we download it there in source dll
            //downloadedImgs = DownloadImage(images, source);
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@imageFile", images);
            paramList[3] = new SqlParameter("@images", downloadedImgs);
            paramList[4] = new SqlParameter("@source", (int)source);

            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHotelImages, paramList);
            //Trace.TraceInformation("HotelImages.Save Exit.");
        }

        /// <summary>
        ///  This method is used to Download the Image
        /// </summary>
        /// <param name="images">images</param>
        /// <param name="hSource">HotelSource</param>
        /// <returns>string</returns>
        public string DownloadImage(string images, HotelBookingSource hSource)
        {
            //Trace.TraceInformation("HotelImages.DownloadImage entered.");
            // Download image from given URL and save image name only in database.
            string imagesURL = string.Empty;
            string[] imgURL = images.Replace("\\", "/").Split('|');
            string imgPath = string.Empty;
            if (hSource == HotelBookingSource.GTA)
            {
                imgPath = Configuration.ConfigurationSystem.GTAConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.HotelBeds)
            {
                imgPath = Configuration.ConfigurationSystem.HotelBedsConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.Tourico)
            {
                imgPath = Configuration.ConfigurationSystem.TouricoConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.IAN)
            {
                imgPath = Configuration.ConfigurationSystem.IANConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.Miki)
            {
                imgPath = Configuration.ConfigurationSystem.MikiConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.Travco)
            {
                imgPath = Configuration.ConfigurationSystem.TravcoConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.DOTW)
            {
                imgPath = Configuration.ConfigurationSystem.DOTWConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.WST)
            {
                imgPath = Configuration.ConfigurationSystem.WSTConfig["downloadImagesPath"].ToString() + "\\";
            }
            
            for (int k = 0; k < imgURL.Length - 1; k++)
            {
                WebClient Client = new WebClient();
                string guidValue = Guid.NewGuid().ToString();
                string[] fileName = imgURL[k].Split('/');
                string imageFileName = fileName[fileName.Length - 1];
                try
                {
                    Client.DownloadFile(imgURL[k], imgPath + guidValue + imageFileName);
                    if (imagesURL.Length > 0)
                    {
                        imagesURL += "|";
                    }
                    imagesURL += guidValue + imageFileName;
                }
                catch (Exception excep)
                {
                    Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Low, 0, "HotelImage.DownloadImage Downloading Image Failed Message : " + excep.Message, "");
                }
            }
            //Trace.TraceInformation("HotelImages.DownloadImage Exit.");
            return imagesURL;
        }
        /// <summary>
        /// This method is to Update the Record into HotelImages DB
        /// </summary>
        public void Update()
        {
            //Trace.TraceInformation("HotelImages.Update entered.");

            // no  need to Download Images here since we download it there in source dll
            //downloadedImgs = DownloadImage(images, source);
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@imageFile", images);
            paramList[3] = new SqlParameter("@images", downloadedImgs);
            paramList[4] = new SqlParameter("@source", (int)source);

            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelImages, paramList);
            if (rowsAffected == 0)
            {
                Save();
            }
            //Trace.TraceInformation("HotelImages.Update Exit.");
        }
        //Getting All images Citywise
        /// <summary>
        ///  This method is to retrieve the result from DB corresponding to cityCode
        /// </summary>
        /// <param name="cityCode">cityCode</param>
        /// <param name="source">source</param>
        /// <returns>DataTable</returns>
        public static DataTable GetImagesByCityCode(string cityCode, HotelBookingSource source)
        {
            DataTable dtImages = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_CityCode", cityCode);
                paramList[1] = new SqlParameter("@P_Source", (int)source);
                dtImages = DBGateway.FillDataTableSP("usp_GetImagesByCityCode", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtImages;
        }
    }
}
