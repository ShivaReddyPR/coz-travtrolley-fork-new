﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;


namespace CT.BookingEngine
{
    public class Enquiry
    {
        public Enquiry()
        {

        }
        public static DataTable GetList(DateTime _fromDate, DateTime _toDate, int AgentId, string ProductId, long UserId, string MemberType)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[6];
                    paramList[0] = new SqlParameter("@FROMDATE", _fromDate);
                    paramList[1] = new SqlParameter("@TODATE", _toDate);
                    if (AgentId > 0) paramList[2] = new SqlParameter("@AgencyId", AgentId);
                    if (!string.IsNullOrEmpty(ProductId)) paramList[3] = new SqlParameter("@ProductType", ProductId);
                    paramList[4] = new SqlParameter("@USER_ID", UserId);
                    paramList[5] = new SqlParameter("@USER_TYPE", MemberType);
                    data = DBGateway.ExecuteReaderSP(SPNames.GetEnquiry, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        public void SaveRemarks(int EnqID, string Remarks,long loginId)
        {
            try
            {
                SqlParameter[] parameterList = new SqlParameter[3];
                parameterList[0] = new SqlParameter("@EnquiryId", EnqID);
                parameterList[1] = new SqlParameter("@Remarks", Remarks);
                parameterList[2] = new SqlParameter("@CreatedBy", loginId);
                DBGateway.ExecuteNonQuerySP(SPNames.Enquiry_Remarks_ADD, parameterList);
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetRemarks(int EnquireId)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@EnquiryId", EnquireId);
                    data = DBGateway.ExecuteReaderSP(SPNames.Enquiry_Remarks_GetList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
    }
}
