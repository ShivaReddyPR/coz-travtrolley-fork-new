﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using System.Data;
using CT.Corporate;

namespace CT.BookingEngine
{
    [Serializable]
    public class FlightPolicy
    {

        #region Members
        int id;
        int flightid;
        int profileId;
        string profileGrade;
        int travelReasonid;
        bool isUnderPolicy;
        int policyReasonCode;
        string policyBreakingRules;
        int policyReasonId;
        string status;
        string flightNumbers;
        DateTime travelDate;

        //Added by Lokesh on : 24June2017 For Trip Approval Process
        List<CorpProfileApproval> _profileApproversList = new List<CorpProfileApproval>();
        string _approvalStatus;
        bool _selectedTrip;
        int _approvedBy;
        int _approverHierarchy;
        string _approverComment;
        string _preTripApproval;
        decimal _preTripAmount;
        string _preTripCurrency;
        string _reLaunchApproval;
        string _reLaunchCurrency;
        //decimal _reLaunchAmount;
        int _reLaunchTripId;
        int _createdBy;
        int _productId;        
        /// <summary>
        /// Added for saving flight booking as auto approved
        /// </summary>
        bool isFlightPolicyAutoApproved =false;

        /// <summary>
        /// Added for storing LLF details to pass Flight DI Entries
        /// </summary>
        bool isLLFApplied;
        double llfAmount;
        string lowestFlight;
        string cheapestFlight;
        double cheapestAmount;
        /// <summary>
        /// used for evaluating approval Journey type while setting approval status
        /// </summary>
        bool isDomestic;
        /// <summary>
        /// used for evaluating approval budget amount while setting approval status
        /// </summary>
        decimal totalBookingAmount;

        #endregion

        #region Properties

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public int Flightid
        {
            get { return flightid; }
            set { flightid = value; }
        }
        public int ProfileId
        {
            get { return profileId; }
            set { profileId = value; }
        }
        public string ProfileGrade
        {
            get { return profileGrade; }
            set { profileGrade = value; }
        }
        public int TravelReasonId
        {
            get { return travelReasonid; }
            set { travelReasonid = value; }
        }
        public bool IsUnderPolicy
        {
            get { return isUnderPolicy; }
            set { isUnderPolicy = value; }
        }
        public int PolicyReasonCode
        {
            get { return policyReasonCode; }
            set { policyReasonCode = value; }
        }
        public string PolicyBreakingRules
        {
            get { return policyBreakingRules; }
            set { policyBreakingRules = value; }
        }
        public int PolicyReasonId
        {
            get { return policyReasonId; }
            set { policyReasonId = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public string FlightNumbers
        {
            get { return flightNumbers; }
            set { flightNumbers = value; }
        }

        public DateTime TravelDate
        {
            get { return travelDate; }
            set { travelDate = value; }
        }

        //Added by Lokesh on : 24June2017 For Trip Approval Process
        public List<CorpProfileApproval> ProfileApproversList
        {
            get { return _profileApproversList; }
            set { _profileApproversList = value; }
        }

        public string ApprovalStatus
        {
            get { return _approvalStatus; }
            set { _approvalStatus = value; }
        }

        public bool SelectedTrip
        {
            get { return _selectedTrip; }
            set { _selectedTrip = value; }
        }
        public int ApprovedBy
        {
            get { return _approvedBy; }
            set { _approvedBy = value; }
        }

        public int ApproverHierarchy
        {
            get { return _approverHierarchy; }
            set { _approverHierarchy = value; }
        }
        public string ApproverComment
        {
            get { return _approverComment; }
            set { _approverComment = value; }
        }

        public string PreTripApproval
        {
            get { return _preTripApproval; }
            set { _preTripApproval = value; }
        }

        public decimal PreTripAmount
        {
            get { return _preTripAmount; }
            set { _preTripAmount = value; }
        }
        public string PreTripCurrency
        {
            get { return _preTripCurrency; }
            set { _preTripCurrency = value; }
        }


        public string ReLaunchApproval
        {
            get { return _reLaunchApproval; }
            set { _reLaunchApproval = value; }
        }

        public string ReLaunchCurrency
        {
            get { return _reLaunchCurrency; }
            set { _reLaunchCurrency = value; }
        }
        //public decimal ReLaunchAmount
        //{

        //    get { return ReLaunchAmount; }
        //    set { ReLaunchAmount = value; }
        //}

        public int ReLaunchTripId
        {
            get { return _reLaunchTripId; }
            set { _reLaunchTripId = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public int ProductId { get => _productId; set => _productId = value; }
        

        /// <summary>
        /// Whether LLF applicable or not
        /// </summary>
        public bool IsLLFApplied { get => isLLFApplied; set => isLLFApplied = value; }
        /// <summary>
        /// If LLF applicable store lowest amount for DI entry
        /// </summary>
        public double LLFAmount { get => llfAmount; set => llfAmount = value; }
        /// <summary>
        /// If LLF applicable store flight details         
        /// </summary>
        /// <example>EK456 DXB-JED,EK457 JED-DXB</example>
        public string LowestFlight { get => lowestFlight; set => lowestFlight = value; }
        /// <summary>
        /// Assign lowest fare flight details based on price ascending sort for DI entry.
        /// </summary>
        /// <example>EK456 DXB-JED,EK457 JED-DXB</example>
        public string CheapestFlight { get => cheapestFlight; set => cheapestFlight = value; }
        /// <summary>
        /// Assign lowest fare flight amount based on price ascending sort for DI entry.
        /// </summary>
        public double CheapestAmount { get => cheapestAmount; set => cheapestAmount = value; }
        /// <summary>
        /// Set to True if corporate agent doesn't need approval process. Default false.
        /// </summary>
        public bool IsFlightPolicyAutoApproved { get => isFlightPolicyAutoApproved; set => isFlightPolicyAutoApproved = value; }
        /// <summary>
        /// used for evaluating approval Journey type while setting approval status. Assigned while saving Flight policy from Itinerary.save()
        /// </summary>
        public bool IsDomestic { get => isDomestic; set => isDomestic = value; }
        /// <summary>
        /// used for evaluating approval budget amount while setting approval status.  Assigned while saving Flight policy from Itinerary.save()
        /// </summary>
        public decimal TotalBookingAmount { get => totalBookingAmount; set => totalBookingAmount = value; }
        #endregion

        public FlightPolicy()
        {

        }

        public FlightPolicy(int ProfileId)
        {
            this.profileId = ProfileId;
            Load();
        }

        #region Methods

        /// <summary>
        /// Load Policy by ProfileId 
        /// </summary>
        void Load()
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_ProfileId", profileId);
            DataTable dt = DBGateway.FillDataTableSP("", paramList);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                if (row["Id"] != DBNull.Value)
                {
                    id = Convert.ToInt32(row["Id"]);
                }
                if (row["flightId"] != DBNull.Value)
                {
                    flightid = Convert.ToInt32(row["flightId"]);
                }
                if (row["profileGrade"] != DBNull.Value)
                {
                    profileGrade = row["profileGrade"].ToString();
                }
                if (row["travelReasonId"] != DBNull.Value)
                {
                    travelReasonid = Convert.ToInt32(row["travelReasonId"]);
                }
                if (row["isUnderPolicy"] != DBNull.Value)
                {
                    isUnderPolicy = Convert.ToBoolean(row["isUnderPolicy"]);
                }
                if (row["policyReasonCode"] != DBNull.Value)
                {
                    policyReasonCode = Convert.ToInt32(row["policyReasonCode"]);
                }
                if (row["policyBreakingRules"] != DBNull.Value)
                {
                    policyBreakingRules = row["policyBreakingRules"].ToString();
                }
                if (row["policyReasonId"] != DBNull.Value)
                {
                    policyReasonId = Convert.ToInt32(row["policyReasonId"]);
                }
                if (row["status"] != DBNull.Value)
                {
                    status = row["status"].ToString();
                }
                _productId = row["ProductId"] != DBNull.Value ? Convert.ToInt32(row["ProductId"]) : _productId;

            }
        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@P_Flightid", flightid);
                paramList[1] = new SqlParameter("@P_ProfileId", profileId);
                paramList[2] = new SqlParameter("@P_ProfileGrade", profileGrade);
                paramList[3] = new SqlParameter("@P_TravelReasonid", travelReasonid);
                paramList[4] = new SqlParameter("@P_IsUnderPolicy", (isUnderPolicy ? "Y" : "N"));
                paramList[5] = new SqlParameter("@P_PolicyBreakingRules", policyBreakingRules);
                if (policyReasonId > 0)
                {
                    paramList[6] = new SqlParameter("@P_PolicyReasonId", policyReasonId);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_PolicyReasonId", DBNull.Value);
                }
                paramList[7] = new SqlParameter("@P_Status", status);
                paramList[8] = new SqlParameter("@P_FlightNumbers", flightNumbers);
                paramList[9] = new SqlParameter("@P_TravelDate", travelDate);
                paramList[10] = new SqlParameter("@P_ProductId", ProductId);
                List<SqlParameter> parameters = new List<SqlParameter>(paramList);

                parameters.Add(new SqlParameter("@P_IsLLFApplied", isLLFApplied));
                parameters.Add(new SqlParameter("@P_LLFAmount", llfAmount));
                parameters.Add(new SqlParameter("@P_LowestFlight", lowestFlight));

                int rec = DBGateway.ExecuteNonQuerySP("usp_AddFlightPolicy", parameters.ToArray());
                InsertTripApprovers(flightid, isDomestic, totalBookingAmount);
                UpdatePolicyDetails(flightid, profileId);

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(FlightPolicy)Failed to save. Reason :" + ex.ToString(), "");
            }
        }

        public static List<FlightPolicy> GetFlightPolicies(long profileId, string flightNumbers, DateTime travelDate)
        {
            List<FlightPolicy> FlightPolicies = new List<FlightPolicy>();

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_FlightNumbers", flightNumbers);
                paramList[1] = new SqlParameter("@P_TravelDate", travelDate);
                paramList[2] = new SqlParameter("@P_profileId", profileId);
                DataTable dt = DBGateway.FillDataTableSP("usp_GetFlightPolicies", paramList);

                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        FlightPolicy policy = new FlightPolicy();
                        if (row["Id"] != DBNull.Value)
                        {
                            policy.ID = Convert.ToInt32(row["Id"]);
                        }
                        if (row["flightId"] != DBNull.Value)
                        {
                            policy.Flightid = Convert.ToInt32(row["flightId"]);
                        }
                        if (row["ProfileId"] != DBNull.Value)
                        {
                            policy.ProfileId = Convert.ToInt32(row["ProfileId"]);
                        }
                        if (row["profileGrade"] != DBNull.Value)
                        {
                            policy.ProfileGrade = row["profileGrade"].ToString();
                        }
                        if (row["travelReasonId"] != DBNull.Value)
                        {
                            policy.TravelReasonId = Convert.ToInt32(row["travelReasonId"]);
                        }
                        if (row["isUnderPolicy"] != DBNull.Value)
                        {
                            if (row["isUnderPolicy"].ToString() == "Y")
                            {
                                policy.IsUnderPolicy = true;
                            }
                            else
                            {
                                policy.IsUnderPolicy = false;
                            }
                        }
                        //if (row["policyReasonCode"] != DBNull.Value)
                        //{
                        //    policy.PolicyReasonCode = Convert.ToInt32(row["policyReasonCode"]);
                        //}
                        if (row["policyBreakingRules"] != DBNull.Value)
                        {
                            policy.PolicyBreakingRules = row["policyBreakingRules"].ToString();
                        }
                        else
                        {
                            policy.policyBreakingRules = string.Empty;
                        }
                        if (row["policyReasonId"] != DBNull.Value)
                        {
                            policy.PolicyReasonId = Convert.ToInt32(row["policyReasonId"]);
                        }
                        if (row["status"] != DBNull.Value)
                        {
                            policy.Status = row["status"].ToString();
                        }
                        else
                        {
                            policy.status = string.Empty;
                        }
                        if (row["FlightNumbers"] != DBNull.Value)
                        {
                            policy.flightNumbers = row["FlightNumbers"].ToString();
                        }
                        else
                        {
                            policy.flightNumbers = string.Empty;
                        }
                        if (row["TravelDate"] != DBNull.Value)
                        {
                            policy.travelDate = Convert.ToDateTime(row["travelDate"]);
                        }
                        if (row["IsLLFApplied"] != DBNull.Value)
                            policy.IsLLFApplied = (row["IsLLFApplied"].ToString() == "1" ? true : false);

                        if (row["LLFAmount"] != DBNull.Value)
                            policy.LLFAmount = Convert.ToDouble(row["LLFAmount"]);

                        FlightPolicies.Add(policy);
                    }
                }
            }
            catch (Exception ex) { throw ex; }
            return FlightPolicies;
        }


        private void UpdatePolicyDetails(int flightId, int profileId)
        {
            DataTable dtUserPoliciesList = PolicyMaster.GetCorpPolicyLinkList(profileId);
            if (dtUserPoliciesList != null && dtUserPoliciesList.Rows.Count > 0)
            {
                List<PolicyDetails> policyDetailsList = new List<PolicyDetails>();
                foreach (DataRow dr in dtUserPoliciesList.Rows)
                {
                    if (dr["PolicyId"] != DBNull.Value)
                    {
                        //PRETRIPAPPROVAL -- NOAPPROVAL
                        //PRETRIPAPPROVAL -- ALWAYS
                        //PRETRIPAPPROVAL -- COSTEXCEEDS -- AED -- 300

                        //RELAUNCHAPPROVAL -- COSTEXCEEDS -- AED -- 150
                        //RELAUNCHAPPROVAL -- CHANGEITINERARY

                        PolicyDetails details = new PolicyDetails();
                        policyDetailsList.AddRange(details.Load(Convert.ToInt32(dr["PolicyId"])));
                    }
                }
                string preTripApproval = string.Empty;
                string reLaunchApproval = string.Empty;


                //If the user is linked with multiple policies save them by "|" symbol.

                if (policyDetailsList != null && policyDetailsList.Count > 0)
                {
                    foreach (PolicyDetails pd in policyDetailsList)
                    {
                        if (pd.Category == Category.PRETRIPAPPROVAL)
                        {
                            //PRETRIPAPPROVAL --AlWAYS && NOAPPROVAL
                            if (pd.FilterType == "NOAPPROVAL" || pd.FilterType == "ALWAYS")
                            {
                                if (preTripApproval.Length > 0)
                                {
                                    preTripApproval += "|" + pd.FilterType;
                                }
                                else
                                {
                                    preTripApproval = pd.FilterType;
                                }
                            }
                            //PRETRIPAPPROVAL --COSTEXCEEDS
                            else if (pd.FilterType == "COSTEXCEEDS")
                            {
                                if (preTripApproval.Length > 0)
                                {
                                    //FilterType~Currency~Amount
                                    preTripApproval += "|" + pd.FilterType + "~" + pd.FilterValue1 + "~" + pd.FilterValue2;
                                }
                                else
                                {
                                    preTripApproval = pd.FilterType + "~" + pd.FilterValue1 + "~" + pd.FilterValue2;
                                }
                            }
                        }
                        //RELAUNCHAPPROVAL --  COSTEXCEEDS
                        else if (pd.Category == Category.RELAUNCHAPPROVAL)
                        {
                            if (pd.FilterType == "COSTEXCEEDS")
                            {
                                if (reLaunchApproval.Length > 0)
                                {

                                    reLaunchApproval += "|" + pd.FilterType + "~" + pd.FilterValue1 + "~" + pd.FilterValue2;
                                }
                                else
                                {
                                    reLaunchApproval = pd.FilterType + "~" + pd.FilterValue1 + "~" + pd.FilterValue2;
                                }
                            }
                            //RELAUNCHAPPROVAL --  CHANGEITINERARY
                            else if (pd.FilterType == "CHANGEITINERARY")
                            {
                                if (reLaunchApproval.Length > 0)
                                {
                                    reLaunchApproval += "|" + pd.FilterType;
                                }
                                else
                                {
                                    reLaunchApproval = pd.FilterType;
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(preTripApproval) || !string.IsNullOrEmpty(reLaunchApproval))
                {
                    SqlParameter[] paramList = new SqlParameter[7];
                    paramList[0] = new SqlParameter("@P_flightId", flightId);
                    paramList[1] = new SqlParameter("@P_profileId", profileId);
                    if (preTripApproval.Length > 0)
                    {
                        paramList[2] = new SqlParameter("@P_preTripApproval", preTripApproval);
                    }
                    else
                    {
                        paramList[2] = new SqlParameter("@P_preTripApproval", DBNull.Value);
                    }
                    if (reLaunchApproval.Length > 0)
                    {
                        paramList[3] = new SqlParameter("@P_reLaunchApproval", reLaunchApproval);
                    }
                    else
                    {
                        paramList[3] = new SqlParameter("@P_reLaunchApproval", DBNull.Value);
                    }
                    paramList[4] = new SqlParameter("@P_createdBy", profileId);

                    //paramList[5] = new SqlParameter("@P_approvalStatus", "P");//Initially pending state.
                    paramList[5] = new SqlParameter("@P_approvalStatus", policyBreakingRules.Contains("AUTOTICKETING:Y") || isFlightPolicyAutoApproved ? "A" : _approvalStatus);//As per status set, IF Autoticketing then auto Approval.
                    paramList[6] = new SqlParameter("@P_selectedTrip", SelectedTrip);
                    DBGateway.ExecuteNonQuerySP("Corp_Profile_Flight_Policy_Update", paramList);
                }

            }
        }

        private void InsertTripApprovers(int flightId, bool isDomestic, decimal totalBookingAmount)
        {
            //Get the List of Trip Approvers for the particular profile Id from the table "corp_profile_approval";
            //Then insert each approver into the table "Corp_Profile_Transc_Approval" for the particular flightId;           
            CorporateProfileExpenseDetails expDetails = new CorporateProfileExpenseDetails();
            _profileApproversList = expDetails.Load(profileId, "T");
            string approvalStatus = "P", remarks=string.Empty;
            if (_profileApproversList != null && _profileApproversList.Count > 0)
            {
                if (this._productId == 1)
                {
                    if (CheckTripApprovalStatus(isDomestic, totalBookingAmount, _profileApproversList))
                        approvalStatus = "P";
                    else
                    { approvalStatus = "A"; remarks = "AutoApproved due to not matching condition"; }
                }

                foreach (CorpProfileApproval approver in _profileApproversList)
                {
                    Corp_Profile_Transc_Approval transcApproval = new Corp_Profile_Transc_Approval();
                    //transcApproval.ApprovalStatus = "P";//Initial Approval Status 'Pending'
                    transcApproval.ApprovalStatus = policyBreakingRules.Contains("AUTOTICKETING:Y") ? "A" : approvalStatus;// If autotocketing then Autoapproval
                    transcApproval.ApproverHierarchy = approver.Hierarchy;
                    transcApproval.ApproverId = approver.ApproverId;
                    transcApproval.CreatedBy = profileId;
                    transcApproval.ExpDetailId = flightid;
                    transcApproval.Status = "A";
                    transcApproval.Id = -1;                    
                    transcApproval.TranxType = "T"; //Trip Category
                    transcApproval.Remarks = remarks;
                    transcApproval.SaveCorpProfileTripApprovals();
                }                
            }
            else if(isFlightPolicyAutoApproved)// For Auto Approval
            {
                Corp_Profile_Transc_Approval transcApproval = new Corp_Profile_Transc_Approval();                
                transcApproval.ApprovalStatus = "A";
                transcApproval.ApproverHierarchy = 0;
                transcApproval.ApproverId = profileId;//In order to show trip in details/approver queue
                transcApproval.CreatedBy = profileId;
                transcApproval.ExpDetailId = flightid;
                transcApproval.Status = "A";
                transcApproval.Id = -1;
                transcApproval.TranxType = "T"; //Trip Category 
                transcApproval.Remarks = "AutoApproved";
                transcApproval.SaveCorpProfileTripApprovals();
            }
            else // Defeult entry to focus corp queue details
            {
                Corp_Profile_Transc_Approval transcApproval = new Corp_Profile_Transc_Approval();
                transcApproval.ApprovalStatus = "A";
                transcApproval.ApproverHierarchy = 0;
                transcApproval.ApproverId = profileId;//In order to show trip in details/approver queue
                transcApproval.CreatedBy = profileId;
                transcApproval.ExpDetailId = flightid;
                transcApproval.Status = "A";
                transcApproval.Id = -1;
                transcApproval.TranxType = "T"; //Trip Category    
                transcApproval.Remarks = "No Approvers Defined";
                transcApproval.SaveCorpProfileTripApprovals();
            }

            _approvalStatus = approvalStatus;//Assign approvalStatus for updating bke_flight_policy
        }

        #endregion

        public static string GetTripApprovalStatus(int flightId, int productId)
        {
            string approvalStatus = string.Empty;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_FlightId", flightId);
                paramList[1] = new SqlParameter("@P_ProductId", productId); //Added product Id as param to fetch the status based on product id
                paramList[2] = new SqlParameter("@P_App_Status", SqlDbType.NVarChar, 1);
                paramList[2].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQueryDetails(cmd, "usp_Corp_GetTripApprovalStatus", paramList);
                if (paramList[2].Value != DBNull.Value)
                {
                    approvalStatus = Convert.ToString(paramList[2].Value);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return approvalStatus;
        }




        public void GetPolicyByFlightId()
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FlightId", flightid));
                parameters.Add(new SqlParameter("@ProductId", ProductId == 2 ? 2 : 1));
                DataTable dtPolicy = DBGateway.FillDataTableSP("usp_GetFlightPoliciesByFlightId", parameters.ToArray());

                if(dtPolicy.Rows.Count > 0)
                {
                    DataRow row = dtPolicy.Rows[0];
                    if (row["Id"] != DBNull.Value)
                    {
                        ID = Convert.ToInt32(row["Id"]);
                    }
                    if (row["flightId"] != DBNull.Value)
                    {
                        Flightid = Convert.ToInt32(row["flightId"]);
                    }
                    if (row["ProfileId"] != DBNull.Value)
                    {
                        ProfileId = Convert.ToInt32(row["ProfileId"]);
                    }
                    if (row["profileGrade"] != DBNull.Value)
                    {
                        ProfileGrade = row["profileGrade"].ToString();
                    }
                    if (row["travelReasonId"] != DBNull.Value)
                    {
                        TravelReasonId = Convert.ToInt32(row["travelReasonId"]);
                    }
                    if (row["isUnderPolicy"] != DBNull.Value)
                    {
                        if (row["isUnderPolicy"].ToString() == "Y")
                        {
                            IsUnderPolicy = true;
                        }
                        else
                        {
                            IsUnderPolicy = false;
                        }
                    }
                    //if (row["policyReasonCode"] != DBNull.Value)
                    //{
                    //    policy.PolicyReasonCode = Convert.ToInt32(row["policyReasonCode"]);
                    //}
                    if (row["policyBreakingRules"] != DBNull.Value)
                    {
                        PolicyBreakingRules = row["policyBreakingRules"].ToString();
                    }
                    else
                    {
                        policyBreakingRules = string.Empty;
                    }
                    if (row["policyReasonId"] != DBNull.Value)
                    {
                        PolicyReasonId = Convert.ToInt32(row["policyReasonId"]);
                    }
                    if (row["status"] != DBNull.Value)
                    {
                        Status = row["status"].ToString();
                    }
                    else
                    {
                        status = string.Empty;
                    }
                    if (row["FlightNumbers"] != DBNull.Value)
                    {
                        flightNumbers = row["FlightNumbers"].ToString();
                    }
                    else
                    {
                        flightNumbers = string.Empty;
                    }
                    if (row["TravelDate"] != DBNull.Value)
                    {
                        travelDate = Convert.ToDateTime(row["travelDate"]);
                    }
                    if (row["ApprovedBy"] != DBNull.Value)
                    {
                        ApprovedBy = Convert.ToInt32(row["ApprovedBy"]);
                    }
                    if (row["SelectedTrip"] != DBNull.Value)
                    {
                        _selectedTrip = Convert.ToBoolean(row["SelectedTrip"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(FlightPolicy)Failed to get policy by flightId:" + flightid + ". Reason: " + ex.ToString(), "");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public void UpdateStatusManually()
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FlightId", flightid));
                parameters.Add(new SqlParameter("@ApprovedBy", _approvedBy));
                parameters.Add(new SqlParameter("@ProductId", _productId));
                parameters.Add(new SqlParameter("@ApprovalStatus", _approvalStatus));
                parameters.Add(new SqlParameter("@ApprovalRemarks", _approverComment));

                DBGateway.ExecuteNonQuerySP("usp_ManualUpdateFlightPolicy", parameters.ToArray());

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, CreatedBy, "(CorpManualApproval)Failed to do manual approval for " + (_productId == 1 ? "Flight" : "Hotel") + " : " + flightid + ". Reason : " + ex.ToString(), "");
            }

        }

        /// <summary>
        /// This method is used to evaluate Approval status for trip based on ApprovalType, JourneyType & Budget amount. 
        /// If all the conditions defined in the table are satisfying then approval is required otherwise approval is not required. 
        /// If none of the values are defined in the Corp_Profile_Approval table then approval is required by default.
        /// </summary>
        /// <example>
        /// case 1 ApprovalType='BOTH', JourneyType='INTL', Amount=50000, if the trip is Outside policy, International, total booking amount = 50000 then approval is not required
        /// case 2 ApprovalType='BOTH', JourneyType='INTL', Amount=50000, if the trip is Outside policy, International, total booking amount = 51000 then approval is required
        /// </example>
        /// <param name="isDomestic"></param>
        /// <param name="totalAmount"></param>
        /// <param name="_profileApproversList"></param>
        /// <returns></returns>
        public bool CheckTripApprovalStatus(bool isDomestic, decimal totalAmount, List<CorpProfileApproval> _profileApproversList)
        {
            bool sendForApproval = true;

            foreach (CorpProfileApproval approver in _profileApproversList)
            {
                if (!string.IsNullOrEmpty(approver.ApprovalType) && !string.IsNullOrEmpty(approver.JourneyType) && approver.Amount > 0)
                {
                    if (((isUnderPolicy && (approver.ApprovalType.ToUpper() == "BOTH" || approver.ApprovalType.ToUpper() == "IN")) ||
                   (!isUnderPolicy && (approver.ApprovalType.ToUpper() == "BOTH" || approver.ApprovalType.ToUpper() == "OUT"))) &&
                   ((approver.JourneyType.ToUpper() == "INTL" && !isDomestic) ||
                   (approver.JourneyType.ToUpper() == "DOM" && isDomestic) ||
                   approver.JourneyType.ToUpper() == "BOTH") && totalAmount > approver.Amount)
                        sendForApproval = true;
                    else
                        sendForApproval = false;
                }
                else if (!string.IsNullOrEmpty(approver.ApprovalType) && !string.IsNullOrEmpty(approver.JourneyType))
                {
                    if (((isUnderPolicy && (approver.ApprovalType.ToUpper() == "BOTH" || approver.ApprovalType.ToUpper() == "IN")) ||
                   (!isUnderPolicy && (approver.ApprovalType.ToUpper() == "BOTH" || approver.ApprovalType.ToUpper() == "OUT"))) &&
                   ((approver.JourneyType.ToUpper() == "INTL" && !isDomestic) ||
                   (approver.JourneyType.ToUpper() == "DOM" && isDomestic) ||
                   approver.JourneyType.ToUpper() == "BOTH"))
                        sendForApproval = true;
                    else
                        sendForApproval = false;
                }
                else if (!string.IsNullOrEmpty(approver.ApprovalType) && approver.Amount > 0)
                {
                    if ((isUnderPolicy && (approver.ApprovalType.ToUpper() == "BOTH" || approver.ApprovalType.ToUpper() == "IN")) ||
                    (!isUnderPolicy && (approver.ApprovalType.ToUpper() == "BOTH" || approver.ApprovalType.ToUpper() == "OUT")) && totalAmount > approver.Amount)
                        sendForApproval = true;
                    else
                        sendForApproval = false;
                }
                else if (!string.IsNullOrEmpty(approver.JourneyType) && approver.Amount > 0)
                {
                    if (((approver.JourneyType.ToUpper() == "INTL" && !isDomestic) ||
                     (approver.JourneyType.ToUpper() == "DOM" && isDomestic) ||
                     approver.JourneyType.ToUpper() == "BOTH") && totalAmount > approver.Amount)
                        sendForApproval = true;
                    else
                        sendForApproval = false;
                }
                else if (!string.IsNullOrEmpty(approver.ApprovalType))
                {
                    if ((isUnderPolicy && (approver.ApprovalType.ToUpper() == "BOTH" || approver.ApprovalType.ToUpper() == "IN")) ||
                    (!isUnderPolicy && (approver.ApprovalType.ToUpper() == "BOTH" || approver.ApprovalType.ToUpper() == "OUT")))
                        sendForApproval = true;
                    else
                        sendForApproval = false;
                }
                else if (!string.IsNullOrEmpty(approver.JourneyType))
                {
                    if ((approver.JourneyType.ToUpper() == "INTL" && !isDomestic) ||
                     (approver.JourneyType.ToUpper() == "DOM" && isDomestic) ||
                     approver.JourneyType.ToUpper() == "BOTH")
                        sendForApproval = true;
                    else
                        sendForApproval = false;
                }
                else if (approver.Amount > 0)
                {
                    if (totalAmount > approver.Amount)
                        sendForApproval = true;
                    else
                        sendForApproval = false;
                }
            }

                return sendForApproval;
        }
    }

    [Serializable]
    public class FlightLLFPolicy
    {
        #region region Members
        int lfid;
        int agentId;
        string regionType;
        int fromHoursCap;
        int toHoursCap;
        decimal amountCap;
        
        bool status;
        int createdBy;
        int modifiedBy;
        #endregion


        #region Properties
        public int Lfid { get => lfid; set => lfid = value; }
        public int AgentId { get => agentId; set => agentId = value; }
        public string RegionType { get => regionType; set => regionType = value; }
        public int FromHoursCap { get => fromHoursCap; set => fromHoursCap = value; }
        public int ToHoursCap { get => toHoursCap; set => toHoursCap = value; }
        public decimal AmountCap { get => amountCap; set => amountCap = value; }
        public bool Status { get => status; set => status = value; }
        public int CreatedBy { get => createdBy; set => createdBy = value; }
        public int ModifiedBy { get => modifiedBy; set => modifiedBy = value; }
        
        
        #endregion


        public static List<FlightLLFPolicy> GetFlightLLFPolicies(int agentId)
        {
            List<FlightLLFPolicy> flightLLFPolicies = new List<FlightLLFPolicy>();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_AgentId", agentId));

                DataTable dtPolicies = DBGateway.FillDataTableSP("usp_GetFlightLLFPoliciesByAgentId", parameters.ToArray());

                foreach (DataRow row in dtPolicies.Rows)
                {
                    FlightLLFPolicy policy = new FlightLLFPolicy();
                    policy.AgentId = agentId;
                    policy.AmountCap = Convert.ToDecimal(row["amountCap"]);
                    policy.CreatedBy = Convert.ToInt32(row["createdBy"]);
                    policy.FromHoursCap = Convert.ToInt32(row["FromHoursCap"]);
                    policy.ToHoursCap = Convert.ToInt32(row["ToHoursCap"]);
                    policy.Lfid = Convert.ToInt32(row["lfid"]);
                    policy.RegionType = Convert.ToString(row["regionType"]);
                    policy.Status = Convert.ToBoolean(row["Status"]);                    

                    flightLLFPolicies.Add(policy);
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Flight LLF Policies)Failed to retrieve LLF Policies for Agent : " + agentId + ". Reason : " + ex.ToString(), "");
            }

            return flightLLFPolicies;
        }

    }
}
