﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.BookingEngine
{
   public class OffLineBO
    {
        public static DataTable GetSourcesForOffLine(int agentId, int productId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                if (agentId > 0) paramList[0] = new SqlParameter("@P_AgentId", agentId);
                paramList[1] = new SqlParameter("@P_ProductId", productId);
                return DBGateway.ExecuteQuery("usp_Get_Sources_ForOffLine", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataSet GetOfflineEntryData(long flightId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];               
                paramList[0] = new SqlParameter("@P_flight_id", flightId);
                return DBGateway.ExecuteQuery("usp_get_offline_entry_getdata", paramList);
            }
            catch { throw; }


        }
    }
}
