﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;


/// <summary>
/// Summary description for Activity
/// </summary>
/// 
namespace CT.BookingEngine
{
    public enum PGSource
    {
        /// <summary>
        /// This is payment gateway for HDFC
        /// </summary>
        HDFC = 1,
        /// <summary>
        /// This is payment gateway for American Express
        /// </summary>
        AMEX = 2,
        /// <summary>
        /// This is payment gateway for ICICI
        /// </summary>
        ICICI = 3,
        /// <summary>
        /// This is payment gateway for SBI
        /// </summary>
        OXICASH = 4,
        /// <summary>
        /// This is payment gateway for APICustomer
        /// </summary>
        APICustomer = 5,
        /// <summary>
        /// This is payment gateway for SBI
        /// </summary>
        SBI = 6,
        /// <summary>
        /// This is payment gateway for CCAvenue
        /// </summary>
        CCAvenue = 7,
        /// <summary>
        /// This is payment gateway for Beam
        /// </summary>
        Beam = 9,
        /// <summary>
        /// This is payment gateway for TicketVala
        /// </summary>
        TicketVala = 10,
        /// <summary>
        /// This is payment gateway for Axis
        /// </summary>
        Axis = 11,
        /// <summary>
        /// This is payment gateway for BillDesk
        /// </summary>
        BillDesk = 12,
        /// <summary>
        /// This is payment gateway for Cozmo
        /// </summary>
        CozmoPG = 13,
        /// <summary>
        /// This is payment gateway for NEO
        /// </summary>
        NEOPG = 14
    }
    public enum ReferenceIdType
    {
        /// <summary>
        /// payment by agent
        /// </summary>
        Payment = 1,
        /// <summary>
        /// invoice for the ticket
        /// </summary>
        Invoice = 2,

    }

    public class Activity
    {

        //static string masterDB = System.Configuration.ConfigurationSettings.AppSettings["MasterDB"].ToString();
        #region Variables

        long id;
        string name;
        int days;
        int stockInHand;
        int stockUsed;
        int agencyId;
        decimal startingFrom;
        string city;
        string country;
        string imagePath1;
        string imagePath2;
        string imagePath3;
        ActivityTheme theme;
        string introduction;
        string overview;
        DateTime startFrom;
        DateTime endTo;
        string durationHours;
        DateTime availableFrom;
        DateTime availableTo;
        List<DateTime> unavailableDates;
        string itinerary1;
        string itinerary2;
        string details;
        string supplierName;
        string supplierEmail;
        string mealsIncluded;
        string transferIncluded;
        string pickupLocation;
        DateTime pickupDate;
        string dropoffLocation;
        DateTime dropoffDate;
        List<string> exclusions;
        List<string> inclusions;
        string cancelPolicy;
        string thingsToBring;
        int unavailableDays;
        int bookingCutOff;
        string status;
        int createdBy;
        DateTime createdOn;
        int modifiedBy;
        DateTime modifiedOn;

        DataTable dtFlexMaster;
        DataTable dtPriceDetails;
        DataTable dtTransactionHeader;
        DataTable dtTransactionDetail;
        DataTable dtTransactionPrice;
        DataTable dtFlexDetails;
        DataTable dtFDPaymentDetails;
        DataTable dtFDItineraryDetails;
        DataTable dtinitPriceDetails;
        DataTable dtFDVisaFollowup;
        decimal agentBalance;

        public static Dictionary<string, decimal> ExchangeRates;
        public static string AgentCurrency;
        public static long AgentId;
        string isFixedDeparture;
        string discountCode;
        decimal discount;
        #endregion

        #region Properties

        public long Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int Days
        {
            get { return days; }
            set { days = value; }
        }
        public int StockInHand
        {
            get { return stockInHand; }
            set { stockInHand = value; }
        }
        public int StockUsed
        {
            get { return stockUsed; }
            set { stockUsed = value; }
        }
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }
        public decimal StartingFrom
        {
            get { return startingFrom; }
            set { startingFrom = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string ImagePath1
        {
            get { return imagePath1; }
            set { imagePath1 = value; }
        }
        public string ImagePath2
        {
            get { return imagePath2; }
            set { imagePath2 = value; }
        }
        public string ImagePath3
        {
            get { return imagePath3; }
            set { imagePath3 = value; }
        }

        public ActivityTheme Theme
        {
            get { return theme; }
            set { theme = value; }
        }
        public string Introduction
        {
            get { return introduction; }
            set { introduction = value; }
        }
        public string Overview
        {
            get { return overview; }
            set { overview = value; }
        }
        public DateTime StartFrom
        {
            get { return startFrom; }
            set { startFrom = value; }
        }
        public DateTime EndTo
        {
            get { return endTo; }
            set { endTo = value; }
        }
        public string DurationHours
        {
            get { return durationHours; }
            set { durationHours = value; }
        }
        public DateTime AvailableFrom
        {
            get { return availableFrom; }
            set { availableFrom = value; }
        }
        public DateTime AvailableTo
        {
            get { return availableTo; }
            set { availableTo = value; }
        }
        public List<DateTime> UnavailableDates
        {
            get { return unavailableDates; }
            set { unavailableDates = value; }
        }
        public string Itinerary1
        {
            get { return itinerary1; }
            set { itinerary1 = value; }
        }
        public string Itinerary2
        {
            get { return itinerary2; }
            set { itinerary2 = value; }
        }
        public string Details
        {
            get { return details; }
            set { details = value; }
        }
        public string SupplierName
        {
            get { return supplierName; }
            set { supplierName = value; }
        }
        public string SupplierEmail
        {
            get { return supplierEmail; }
            set { supplierEmail = value; }
        }
        public string MealsIncluded
        {
            get { return mealsIncluded; }
            set { mealsIncluded = value; }
        }
        public string TransferIncluded
        {
            get { return transferIncluded; }
            set { transferIncluded = value; }
        }
        public string PickupLocation
        {
            get { return pickupLocation; }
            set { pickupLocation = value; }
        }
        public DateTime PickupDate
        {
            get { return pickupDate; }
            set { pickupDate = value; }
        }
        public string DropoffLocation
        {
            get { return dropoffLocation; }
            set { dropoffLocation = value; }
        }
        public DateTime DropoffDate
        {
            get { return dropoffDate; }
            set { dropoffDate = value; }
        }
        public List<string> Exclusions
        {
            get { return exclusions; }
            set { exclusions = value; }
        }
        public List<string> Inclusions
        {
            get { return inclusions; }
            set { inclusions = value; }
        }
        public string CancelPolicy
        {
            get { return cancelPolicy; }
            set { cancelPolicy = value; }
        }
        public string ThingsToBring
        {
            get { return thingsToBring; }
            set { thingsToBring = value; }
        }
        public int UnavailableDays
        {
            get { return unavailableDays; }
            set { unavailableDays = value; }
        }
        public int BookingCutOff
        {
            get { return bookingCutOff; }
            set { bookingCutOff = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }
        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }

        public DataTable FlexMaster
        {
            get { return dtFlexMaster; }
            set { dtFlexMaster = value; }
        }

        public DataTable PriceDetails
        {
            get { return dtPriceDetails; }
            set { dtPriceDetails = value; }
        }

        public DataTable TransactionHeader
        {
            get { return dtTransactionHeader; }
            set { dtTransactionHeader = value; }
        }
        public DataTable TransactionDetail
        {
            get { return dtTransactionDetail; }
            set { dtTransactionDetail = value; }
        }

        public DataTable TransactionPrice
        {
            get { return dtTransactionPrice; }
            set { dtTransactionPrice = value; }
        }

        public DataTable FlexDetails
        {
            get { return dtFlexDetails; }
            set { FlexDetails = value; }
        }

        public decimal AgentBalance
        {
            get { return agentBalance; }
            set { agentBalance = value; }
        }
        public string IsFixedDeparture
        {
            get { return isFixedDeparture; }
            set { isFixedDeparture = value; }
        }

        public DataTable FixedDepartureDetails
        {
            get { return dtFDPaymentDetails; }
            set { dtFDPaymentDetails = value; }
        }
        public DataTable FixedItineraryDetails
        {
            get { return dtFDItineraryDetails; }
            set { dtFDItineraryDetails = value; }
        }
        public DataTable FixedinitPriceDetails
        {
            get { return dtinitPriceDetails; }
            set { dtinitPriceDetails = value; }
        }
        public DataTable FixedVisaFollowup
        {
            get { return dtFDVisaFollowup; }
            set { dtFDVisaFollowup = value; }
        }
        public string DiscountCode
        {
            get { return discountCode; }
            set { discountCode = value; }
        }
        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        //public static Dictionary<string, decimal> ExchangeRates
        //{
        //    get { return exchangeRates; }
        //    set { exchangeRates = value; }
        //}
        //public static string AgentCurrency
        //{
        //    get { return agentCurrency; }
        //    set { agentCurrency = value; }
        //}
        //public static long AgentId
        //{
        //    get { return agentId; }
        //    set { agentId = value; }
        //}


        #endregion

        public Activity()
        {
            Id = -1;
            Load(Id);
            
        }
        public Activity(long id)
        {
            Id = id;
            Load(Id);
        }

        private void Load(long id)
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            DataSet ds = new DataSet();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@ActivityId", id);
                    //SqlDataReader reader = DBGateway.ExecuteReaderSP(SPNames.GetActivity, paramList, connection);
                    using (DataTable dtActivity = DBGateway.FillDataTableSP(SPNames.GetActivity, paramList))
                    {
                        if (dtActivity != null && dtActivity.Rows.Count > 0)
                        {
                            DataRow reader = dtActivity.Rows[0];
                            this.id = id;
                            if (reader["activityName"] != DBNull.Value)
                            {
                                name = reader["activityName"].ToString();
                            }
                            if (reader["activityDays"] != DBNull.Value)
                            {
                                days = Convert.ToInt32(reader["activityDays"]);
                            }
                            if (reader["stockInHand"] != DBNull.Value)
                            {
                                stockInHand = Convert.ToInt32(reader["stockInHand"]);
                            }
                            if (reader["stockUsed"] != DBNull.Value)
                            {
                                stockUsed = Convert.ToInt32(reader["stockUsed"]);
                            }
                            if (reader["agencyId"] != DBNull.Value)
                            {
                                agencyId = Convert.ToInt32(reader["agencyId"]);
                            }
                            if (reader["isFixedDeparture"] != DBNull.Value)
                            {
                                isFixedDeparture = Convert.ToString(reader["isFixedDeparture"]);
                            }
                            string supplierCurrency = "AED";
                            decimal rateOfExchange = (ExchangeRates.ContainsKey(supplierCurrency) ? ExchangeRates[supplierCurrency] : 1);
                            DataTable dtMarkup;
                            if (isFixedDeparture == "N")
                            {
                                dtMarkup = CT.BookingEngine.UpdateMarkup.Load((int)AgentId, string.Empty, (int)ProductType.Activity);
                            }
                            else
                            {
                                dtMarkup = CT.BookingEngine.UpdateMarkup.Load((int)AgentId, string.Empty, (int)ProductType.FixedDeparture);
                            }
                            decimal sourceamount;
                            decimal markupValue;
                            DataView dv = dtMarkup.DefaultView;
                            dv.RowFilter = "TransType IN('B2B')";
                            dtMarkup = dv.ToTable();
                            decimal markup = 0; string markupType = string.Empty;
                            if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                            {
                                markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                                markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                            }
                            if (reader["activityStartingFrom"] != DBNull.Value)
                            {
                                sourceamount = Convert.ToDecimal(reader["activityStartingFrom"]) * rateOfExchange;
                                markupValue = ((markupType == "F") ? markup : (Convert.ToDecimal(sourceamount) * (markup / 100)));
                                startingFrom = sourceamount + markupValue;
                            }
                            if (reader["ActivityCity"] != DBNull.Value)
                            {
                                city = Convert.ToString(reader["ActivityCity"]);
                            }
                            if (reader["activityCountry"] != DBNull.Value)
                            {
                                country = Convert.ToString(reader["activityCountry"]);
                            }
                            if (reader["imagePath1"] != DBNull.Value)
                            {
                                imagePath1 = Convert.ToString(reader["imagePath1"]);
                            }
                            if (reader["imagePath2"] != DBNull.Value)
                            {
                                imagePath2 = Convert.ToString(reader["imagePath2"]);
                            }
                            if (reader["imagePath3"] != DBNull.Value)
                            {
                                imagePath3 = Convert.ToString(reader["imagePath3"]);
                            }
                            //if (reader["themeId"] != DBNull.Value)
                            //{
                            //    theme = new ActivityTheme(reader["themeId"].ToString());
                            //}
                            if (reader["introduction"] != DBNull.Value)
                            {
                                introduction = Convert.ToString(reader["introduction"]);
                            }
                            if (reader["overview"] != DBNull.Value)
                            {
                                overview = Convert.ToString(reader["overview"]);
                            }
                            if (reader["startFrom"] != DBNull.Value)
                            {
                                startFrom = Convert.ToDateTime(reader["startFrom"]);
                            }
                            if (reader["endTo"] != DBNull.Value)
                            {
                                endTo = Convert.ToDateTime(reader["endTo"]);
                            }
                            if (reader["durationHours"] != DBNull.Value)
                            {
                                durationHours = Convert.ToString(reader["durationHours"]);
                            }
                            if (reader["availableFrom"] != DBNull.Value)
                            {
                                availableFrom = Convert.ToDateTime(reader["availableFrom"]);
                            }
                            if (reader["availableTo"] != DBNull.Value)
                            {
                                availableTo = Convert.ToDateTime(reader["availableTo"]);
                            }
                            unavailableDates = new List<DateTime>();
                            if (reader["unavailableDates"] != DBNull.Value)
                            {
                                //string[] dates = reader["unavailableDates"].ToString().Split(',');
                                string[] dates = Convert.ToString(reader["unavailableDates"]).Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string date in dates)
                                {
                                    unavailableDates.Add(Convert.ToDateTime(date, dateFormat));
                                }
                            }
                            if (reader["itinerary1"] != DBNull.Value)
                            {
                                itinerary1 = Convert.ToString(reader["itinerary1"]);
                            }
                            if (reader["itinerary2"] != DBNull.Value)
                            {
                                itinerary2 = Convert.ToString(reader["itinerary2"]);
                            }
                            if (reader["details"] != DBNull.Value)
                            {
                                details = Convert.ToString(reader["details"]);
                            }
                            if (reader["supplierName"] != DBNull.Value)
                            {
                                supplierName = Convert.ToString(reader["supplierName"]);
                            }
                            if (reader["supplierEmail"] != DBNull.Value)
                            {
                                supplierEmail = Convert.ToString(reader["supplierEmail"]);
                            }
                            if (reader["mealsIncluded"] != DBNull.Value)
                            {
                                mealsIncluded = Convert.ToString(reader["mealsIncluded"]);
                            }
                            if (reader["transferIncluded"] != DBNull.Value)
                            {
                                transferIncluded = Convert.ToString(reader["transferIncluded"]);
                            }
                            if (reader["pickupLocation"] != DBNull.Value)
                            {
                                pickupLocation = Convert.ToString(reader["pickupLocation"]);
                            }
                            if (reader["pickupDate"] != DBNull.Value)
                            {
                                pickupDate = Convert.ToDateTime(reader["pickupDate"]);
                            }
                            if (reader["dropOffLocation"] != DBNull.Value)
                            {
                                dropoffLocation = Convert.ToString(reader["dropOffLocation"]);
                            }
                            if (reader["dropOffDate"] != DBNull.Value)
                            {
                                dropoffDate = Convert.ToDateTime(reader["dropOffDate"]);
                            }
                            if (reader["exclusions"] != DBNull.Value)
                            {
                                exclusions = new List<string>();
                                string[] list = reader["exclusions"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string data in list)
                                {
                                    exclusions.Add(data);
                                }
                            }
                            if (reader["inclusions"] != DBNull.Value)
                            {
                                inclusions = new List<string>();
                                string[] list1 = reader["inclusions"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string data in list1)
                                {
                                    inclusions.Add(data);
                                }
                            }

                            if (reader["cancellPolicy"] != DBNull.Value)
                            {
                                cancelPolicy = Convert.ToString(reader["cancellPolicy"]);
                            }
                            if (reader["thingsToBring"] != DBNull.Value)
                            {
                                thingsToBring = Convert.ToString(reader["thingsToBring"]);
                            }
                            if (reader["unavailableDays"] != DBNull.Value && !string.IsNullOrEmpty(reader["unavailableDays"].ToString()))
                            {
                                unavailableDays = Convert.ToInt32(reader["unavailableDays"]);
                            }
                            if (reader["bookingCutOff"] != DBNull.Value)
                            {
                                bookingCutOff = Convert.ToInt32(reader["bookingCutOff"]);
                            }
                            if (reader["activityStatus"] != DBNull.Value)
                            {
                                status = Convert.ToString(reader["activitystatus"]);
                            }
                            if (reader["createdBy"] != DBNull.Value)
                            {
                                createdBy = Convert.ToInt32(reader["createdBy"]);
                            }
                            if (reader["createdOn"] != DBNull.Value)
                            {
                                createdOn = Convert.ToDateTime(reader["createdOn"]);
                            }
                            if (reader["modifiedBy"] != DBNull.Value)
                            {
                                modifiedBy = Convert.ToInt32(reader["modifiedBy"]);
                            }
                            if (reader["modifiedOn"] != DBNull.Value)
                            {
                                modifiedOn = Convert.ToDateTime(reader["modifiedOn"]);
                            }
                            if (reader["discountCode"] != DBNull.Value)
                            {
                                discountCode = Convert.ToString(reader["discountCode"]);
                            }
                            if (reader["discount"] != DBNull.Value)
                            {
                                discount = Convert.ToDecimal(reader["discount"]);
                            }

                            //Load the Flex details
                            SqlParameter[] paramListFlex = new SqlParameter[1];
                            paramListFlex[0] = new SqlParameter("@flexActivityId", id);
                            dtFlexMaster = DBGateway.FillDataTableSP(SPNames.GetActivityFlexMaster, paramListFlex);

                            //Load the Price details
                            SqlParameter[] paramListPrice = new SqlParameter[1];
                            paramListPrice[0] = new SqlParameter("@priceActivityId", id);
                            dtPriceDetails = DBGateway.FillDataTableSP(SPNames.GetActivityPrice, paramListPrice);

                            if (dtPriceDetails != null && dtPriceDetails.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dtPriceDetails.Rows)
                                {
                                    sourceamount = Convert.ToDecimal(dr["Total"]) * rateOfExchange;
                                    markupValue = ((markupType == "F") ? markup : (Convert.ToDecimal(sourceamount) * (markup / 100)));
                                    dr["Total"] = sourceamount + markupValue;
                                }
                            }
                            dtinitPriceDetails = dtPriceDetails;
                            //Load the Transaction Header
                            SqlParameter[] paramListHeader = new SqlParameter[1];
                            paramListHeader[0] = new SqlParameter("@ATHId", -1);
                            dtTransactionHeader = DBGateway.FillDataTableSP(SPNames.GetActivityTransactionHeader, paramListHeader);

                            //Load the Transaction Detail

                            SqlParameter[] paramListDetail = new SqlParameter[1];
                            paramListDetail[0] = new SqlParameter("@athdId", -1);
                            dtTransactionDetail = DBGateway.FillDataTableSP(SPNames.GetActivityTransactionDetail, paramListDetail);


                            //Load the Transaction Price Details
                            SqlParameter[] paramListTransPrice = new SqlParameter[1];
                            paramListTransPrice[0] = new SqlParameter("@ATHId", -1);
                            dtTransactionPrice = DBGateway.FillDataTableSP(SPNames.GetActivityTransactionPrice, paramListTransPrice);

                            //Load the Flex Details
                            SqlParameter[] paramListFlexDetails = new SqlParameter[1];
                            paramListFlexDetails[0] = new SqlParameter("@ATHId", -1);
                            dtFlexDetails = DBGateway.FillDataTableSP(SPNames.GetActivityFlexDetails, paramListFlexDetails);

                            if (isFixedDeparture == "Y")
                            {
                                SqlParameter[] paramListFDDetails = new SqlParameter[1];
                                paramListFDDetails[0] = new SqlParameter("@FDPDId", -1);
                                dtFDPaymentDetails = DBGateway.FillDataTableSP("usp_GetFDDetails", paramListFDDetails);

                                SqlParameter[] paramListFDItineraryDetails = new SqlParameter[1];
                                paramListFlexDetails[0] = new SqlParameter("@FDId", id);
                                dtFDItineraryDetails = DBGateway.FillDataTableSP("usp_GetFDItineraryDetails", paramListFlexDetails);

                                SqlParameter[] paramListFDVisaDetails = new SqlParameter[1];
                                paramListFDVisaDetails[0] = new SqlParameter("@visa_id", -1);
                                dtFDVisaFollowup = DBGateway.FillDataTableSP("usp_GetVisaFollowup", paramListFDVisaDetails);
                            }
                        }
                    }
                    //reader.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), "");
                }
            }
        }

        private void Load(long id, bool loadData)
        {
            DataSet ds = new DataSet();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramListHeader = new SqlParameter[1];
                    paramListHeader[0] = new SqlParameter("@ATHId", id);

                    dtTransactionHeader = DBGateway.FillDataTableSP(SPNames.GetActivityTransactionHeader, paramListHeader);
                    if (dtTransactionHeader.Rows.Count > 0)
                    {
                        Int64 activityId = Convert.ToInt64(dtTransactionHeader.Rows[0]["ActivityId"]);
                        SqlParameter[] paramList = new SqlParameter[1];
                        paramList[0] = new SqlParameter("@ActivityId", activityId);
                        //SqlDataReader reader = DBGateway.ExecuteReaderSP(SPNames.GetActivity, paramList, connection);
                        using (DataTable dtActivity = DBGateway.FillDataTableSP(SPNames.GetActivity, paramList))
                        {
                            if (dtActivity != null && dtActivity.Rows.Count > 0)
                            {
                                DataRow reader = dtActivity.Rows[0];
                                this.id = id;
                                if (reader["activityName"] != DBNull.Value)
                                {
                                    name = Convert.ToString(reader["activityName"]);
                                }
                                if (reader["activityDays"] != DBNull.Value)
                                {
                                    days = Convert.ToInt32(reader["activityDays"]);
                                }
                                if (reader["stockInHand"] != DBNull.Value)
                                {
                                    stockInHand = Convert.ToInt32(reader["stockInHand"]);
                                }
                                if (reader["stockUsed"] != DBNull.Value)
                                {
                                    stockUsed = Convert.ToInt32(reader["stockUsed"]);
                                }
                                if (reader["agencyId"] != DBNull.Value)
                                {
                                    agencyId = Convert.ToInt32(reader["agencyId"]);
                                }
                                if (reader["activityStartingFrom"] != DBNull.Value)
                                {
                                    startingFrom = Convert.ToInt32(reader["activityStartingFrom"]);
                                }
                                if (reader["ActivityCity"] != DBNull.Value)
                                {
                                    city = Convert.ToString(reader["ActivityCity"]);
                                }
                                if (reader["activityCountry"] != DBNull.Value)
                                {
                                    country = Convert.ToString(reader["activityCountry"]);
                                }
                                if (reader["imagePath1"] != DBNull.Value)
                                {
                                    imagePath1 = Convert.ToString(reader["imagePath1"]);
                                }
                                if (reader["imagePath2"] != DBNull.Value)
                                {
                                    imagePath2 = Convert.ToString(reader["imagePath2"]);
                                }
                                if (reader["imagePath3"] != DBNull.Value)
                                {
                                    imagePath3 = Convert.ToString(reader["imagePath3"]);
                                }
                                //if (reader["themeId"] != DBNull.Value)
                                //{
                                //    theme = new ActivityTheme(reader["themeId"].ToString());
                                //}
                                if (reader["introduction"] != DBNull.Value)
                                {
                                    introduction = Convert.ToString(reader["introduction"]);
                                }
                                if (reader["overview"] != DBNull.Value)
                                {
                                    overview = Convert.ToString(reader["overview"]);
                                }
                                if (reader["startFrom"] != DBNull.Value)
                                {
                                    startFrom = Convert.ToDateTime(reader["startFrom"]);
                                }
                                if (reader["endTo"] != DBNull.Value)
                                {
                                    endTo = Convert.ToDateTime(reader["endTo"]);
                                }
                                if (reader["durationHours"] != DBNull.Value)
                                {
                                    durationHours = Convert.ToString(reader["durationHours"]);
                                }
                                if (reader["availableFrom"] != DBNull.Value)
                                {
                                    availableFrom = Convert.ToDateTime(reader["availableFrom"]);
                                }
                                if (reader["availableTo"] != DBNull.Value)
                                {
                                    availableTo = Convert.ToDateTime(reader["availableTo"]);
                                }
                                unavailableDates = new List<DateTime>();
                                if (reader["unavailableDates"] != DBNull.Value)
                                {
                                    string[] dates = reader["unavailableDates"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string date in dates)
                                    {
                                        try
                                        {
                                            unavailableDates.Add(Convert.ToDateTime(date));
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                if (reader["itinerary1"] != DBNull.Value)
                                {
                                    itinerary1 = Convert.ToString(reader["itinerary1"]);
                                }
                                if (reader["itinerary2"] != DBNull.Value)
                                {
                                    itinerary2 = Convert.ToString(reader["itinerary2"]);
                                }
                                if (reader["details"] != DBNull.Value)
                                {
                                    details = Convert.ToString(reader["details"]);
                                }
                                if (reader["supplierName"] != DBNull.Value)
                                {
                                    supplierName = Convert.ToString(reader["supplierName"]);
                                }
                                if (reader["supplierEmail"] != DBNull.Value)
                                {
                                    supplierEmail = Convert.ToString(reader["supplierEmail"]);
                                }
                                if (reader["mealsIncluded"] != DBNull.Value)
                                {
                                    mealsIncluded = Convert.ToString(reader["mealsIncluded"]);
                                }
                                if (reader["transferIncluded"] != DBNull.Value)
                                {
                                    transferIncluded = Convert.ToString(reader["transferIncluded"]);
                                }
                                if (reader["pickupLocation"] != DBNull.Value)
                                {
                                    pickupLocation = Convert.ToString(reader["pickupLocation"]);
                                }
                                if (reader["pickupDate"] != DBNull.Value)
                                {
                                    pickupDate = Convert.ToDateTime(reader["pickupDate"]);
                                }
                                if (reader["dropOffLocation"] != DBNull.Value)
                                {
                                    dropoffLocation = Convert.ToString(reader["dropOffLocation"]);
                                }
                                if (reader["dropOffDate"] != DBNull.Value)
                                {
                                    dropoffDate = Convert.ToDateTime(reader["dropOffDate"]);
                                }
                                if (reader["exclusions"] != DBNull.Value)
                                {
                                    exclusions = new List<string>();
                                    string[] list = reader["exclusions"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string data in list)
                                    {
                                        exclusions.Add(data);
                                    }
                                }
                                if (reader["inclusions"] != DBNull.Value)
                                {
                                    inclusions = new List<string>();
                                    string[] list1 = reader["inclusions"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string data in list1)
                                    {
                                        inclusions.Add(data);
                                    }
                                }
                                if (reader["cancellPolicy"] != DBNull.Value)
                                {
                                    cancelPolicy = Convert.ToString(reader["cancellPolicy"]);
                                }
                                if (reader["thingsToBring"] != DBNull.Value)
                                {
                                    thingsToBring = Convert.ToString(reader["thingsToBring"]);
                                }
                                if (reader["unavailableDays"] != DBNull.Value && !string.IsNullOrEmpty(reader["unavailableDays"].ToString()))
                                {
                                    unavailableDays = Convert.ToInt32(reader["unavailableDays"]);
                                }
                                if (reader["bookingCutOff"] != DBNull.Value)
                                {
                                    bookingCutOff = Convert.ToInt32(reader["bookingCutOff"]);
                                }
                                if (reader["activityStatus"] != DBNull.Value)
                                {
                                    status = Convert.ToString(reader["activitystatus"]);
                                }
                                if (reader["createdBy"] != DBNull.Value)
                                {
                                    createdBy = Convert.ToInt32(reader["createdBy"]);
                                }
                                if (reader["createdOn"] != DBNull.Value)
                                {
                                    createdOn = Convert.ToDateTime(reader["createdOn"]);
                                }
                                if (reader["modifiedBy"] != DBNull.Value)
                                {
                                    modifiedBy = Convert.ToInt32(reader["modifiedBy"]);
                                }
                                if (reader["modifiedOn"] != DBNull.Value)
                                {
                                    modifiedOn = Convert.ToDateTime(reader["modifiedOn"]);
                                }
                                if (reader["IsFixedDeparture"] != DBNull.Value)
                                {
                                    isFixedDeparture = Convert.ToString(reader["IsFixedDeparture"]);
                                }
                                if (reader["discountCode"] != DBNull.Value)
                                {
                                    discountCode = Convert.ToString(reader["discountCode"]);
                                }
                                if (reader["discount"] != DBNull.Value)
                                {
                                    discount = Convert.ToDecimal(reader["discount"]);
                                }
                                //Load the Flex details
                                SqlParameter[] paramListFlex = new SqlParameter[1];
                                paramListFlex[0] = new SqlParameter("@flexActivityId", activityId);
                                dtFlexMaster = DBGateway.FillDataTableSP(SPNames.GetActivityFlexMaster, paramListFlex);

                                //Load the Price details
                                SqlParameter[] paramListPrice = new SqlParameter[1];
                                paramListPrice[0] = new SqlParameter("@priceActivityId", activityId);
                                dtPriceDetails = DBGateway.FillDataTableSP(SPNames.GetActivityPrice, paramListPrice);




                                //Load the Transaction Detail
                                SqlParameter[] paramListDetail = new SqlParameter[1];
                                if (!loadData)
                                {
                                    paramListDetail[0] = new SqlParameter("@athdId", -1);
                                }
                                else
                                {
                                    paramListDetail[0] = new SqlParameter("@athdId", id);
                                }
                                dtTransactionDetail = DBGateway.FillDataTableSP(SPNames.GetActivityTransactionDetail, paramListDetail);


                                //Load the Transaction Price Details
                                SqlParameter[] paramListTransPrice = new SqlParameter[1];
                                if (!loadData)
                                {
                                    paramListTransPrice[0] = new SqlParameter("@ATHId", -1);
                                }
                                else
                                {
                                    paramListTransPrice[0] = new SqlParameter("@ATHId", id);
                                }
                                dtTransactionPrice = DBGateway.FillDataTableSP(SPNames.GetActivityTransactionPrice, paramListTransPrice);

                                //Load the Flex Details
                                SqlParameter[] paramListFlexDetails = new SqlParameter[1];
                                if (!loadData)
                                {
                                    paramListFlexDetails[0] = new SqlParameter("@ATHId", -1);
                                }
                                else
                                {
                                    paramListFlexDetails[0] = new SqlParameter("@ATHId", id);
                                }
                                dtFlexDetails = DBGateway.FillDataTableSP(SPNames.GetActivityFlexDetails, paramListFlexDetails);

                                if (isFixedDeparture == "Y")
                                {
                                    SqlParameter[] paramListFDDetails = new SqlParameter[1];
                                    if (!loadData)
                                    {
                                        paramListFDDetails[0] = new SqlParameter("@TranxId", -1);
                                    }
                                    else
                                    {
                                        paramListFDDetails[0] = new SqlParameter("@TranxId", id);
                                    }
                                    dtFDPaymentDetails = DBGateway.FillDataTableSP("usp_GetFDPayments", paramListFDDetails);

                                    SqlParameter[] paramListFDVisaDetails = new SqlParameter[1];
                                    if (!loadData)
                                    {
                                        paramListFDVisaDetails[0] = new SqlParameter("@ATHId", -1);
                                    }
                                    else
                                    {
                                        paramListFDVisaDetails[0] = new SqlParameter("@ATHId", id);
                                    }
                                    dtFDVisaFollowup = DBGateway.FillDataTableSP("usp_GetFDVisaFollowUp", paramListFDVisaDetails);
                                }
                            }
                        }
                        //reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "");
                }
            }
        }

        public void GetActivityForQueue(Int64 id)
        {
            Load(id, true);
        }

        public static DataTable GetCountry()
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[0];
                    data = DBGateway.ExecuteReaderSP(SPNames.GetCountryList1, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public static DataTable GetCity(string id)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlParameter[] paramList;
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@countryCode", id);

                    data = DBGateway.ExecuteReaderSP(SPNames.GetCityByCountryCode, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public void SaveActivity()
        {
            
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[39];
                    paramList[0] = new SqlParameter("@activityName", name);
                    paramList[1] = new SqlParameter("@activityDays", days);
                    paramList[2] = new SqlParameter("@activityStartingFrom", startFrom);
                    paramList[3] = new SqlParameter("@stockinHand", stockInHand);
                    paramList[4] = new SqlParameter("@activityCity", city);
                    paramList[5] = new SqlParameter("@activityCountry", country);
                    paramList[6] = new SqlParameter("@imagePath1", imagePath1);
                    paramList[7] = new SqlParameter("@imagePath2", imagePath2);
                    paramList[8] = new SqlParameter("@imagePath3", imagePath3);
                    paramList[9] = new SqlParameter("@themeId", theme.ThemeId);
                    paramList[10] = new SqlParameter("@introduction", introduction);

                    paramList[11] = new SqlParameter("@overview", overview);
                    paramList[12] = new SqlParameter("@startFrom", startFrom);
                    paramList[13] = new SqlParameter("@endTo", endTo);
                    paramList[14] = new SqlParameter("@ActivityDuration", durationHours);
                    paramList[15] = new SqlParameter("@availableFrom", startFrom);
                    paramList[16] = new SqlParameter("@availableTo", endTo);
                    paramList[17] = new SqlParameter("@unavailableDates", unavailableDates);
                    paramList[18] = new SqlParameter("@itinerary1", itinerary1);
                    paramList[19] = new SqlParameter("@itinerary2", itinerary2);
                    paramList[20] = new SqlParameter("@details", details);
                    paramList[21] = new SqlParameter("@supplierName", supplierName);

                    paramList[22] = new SqlParameter("@supplierEmail", supplierEmail);
                    paramList[23] = new SqlParameter("@mealsIncluded", mealsIncluded);
                    paramList[24] = new SqlParameter("@transferIncluded", transferIncluded);
                    paramList[25] = new SqlParameter("@pickupLocation", pickupLocation);
                    paramList[26] = new SqlParameter("@pickupDate", pickupDate);
                    paramList[27] = new SqlParameter("@dropoffLocation", dropoffLocation);
                    paramList[28] = new SqlParameter("@dropoffDate", dropoffLocation);

                    paramList[29] = new SqlParameter("@exclusions", exclusions);
                    paramList[30] = new SqlParameter("@inclusions", inclusions);
                    paramList[31] = new SqlParameter("@cancellPolicy", cancelPolicy);
                    paramList[32] = new SqlParameter("@thingsToBring", thingsToBring);
                    paramList[33] = new SqlParameter("@unavailableDays", unavailableDays);
                    paramList[34] = new SqlParameter("@bookingCutOff", bookingCutOff);
                    paramList[35] = new SqlParameter("@createdBy", string.Empty);
                    paramList[36] = new SqlParameter("@activityId", SqlDbType.BigInt);
                    paramList[36].Direction = ParameterDirection.Output;
                    paramList[37] = new SqlParameter("@A_MSG_TYPE", SqlDbType.VarChar, 10);
                    paramList[37].Direction = ParameterDirection.Output;
                    paramList[38] = new SqlParameter("@A_MSG_TEXT", SqlDbType.VarChar, 200);
                    paramList[38].Direction = ParameterDirection.Output;

                    DBGateway.ExecuteNonQuerySP(SPNames.AddActivity, paramList);

                    string messageType = Convert.ToString(paramList[37].Value);
                    if (messageType == "E")
                    {
                        string message = Convert.ToString(paramList[38].Value);

                        if (message != string.Empty) throw new Exception(message);
                    }

                    id = Convert.ToInt64(paramList[36].Value);
                    if (dtFlexMaster != null)
                    {
                        DataTable dt = dtFlexMaster.GetChanges();
                        //int recordStatus = 0;
                        if (dt != null && dt.Rows.Count > 0)
                        {

                            foreach (DataRow dr in dt.Rows)
                            {
                                //switch (dr.RowState)
                                //{
                                //    case DataRowState.Added: recordStatus = 1;
                                //        break;
                                //    case DataRowState.Modified: recordStatus = 2;
                                //        break;
                                //    case DataRowState.Deleted: recordStatus = -1;
                                //        dr.RejectChanges();
                                //        break;
                                //    default: break;

                                //}
                                SaveFlexDetails(id, Convert.ToInt32(dr["flexOrder"]), Convert.ToString(dr["flexLabel"]), Convert.ToString(dr["flexControl"]), Convert.ToString(dr["flexSqlQuery"]), Convert.ToString(dr["flexDataType"]), Convert.ToString(dr["flexMandatoryStatus"]));

                            }
                        }
                    }
                    if (dtPriceDetails != null)
                    {
                        DataTable dt = dtPriceDetails.GetChanges();
                        //int recordStatus = 0;
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                //switch (dr.RowState)
                                //{
                                //    case DataRowState.Added: recordStatus = 1;
                                //        break;
                                //    case DataRowState.Modified: recordStatus = 2;
                                //        break;
                                //    case DataRowState.Deleted: recordStatus = -1;
                                //        dr.RejectChanges();
                                //        break;
                                //    default: break;

                                //}
                                SavePriceDetails(id, Convert.ToString(dr["Label"]), Convert.ToInt32(dr["amount"]), Convert.ToString(dr["Type"]));

                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    throw new Exception("Failed to save Activity", ex);
                }
            }
        }

        public void SaveFlexDetails(long activityid, int flexorder, string flexlabel, string flexcontrol, string flexquery, string flexdatatype, string flexmandatorystatus)
        {
            using (SqlConnection connection = DBGateway.GetConnection())
                try
                {
                    SqlParameter[] paramList = new SqlParameter[10];
                    paramList[0] = new SqlParameter("@flexActivityId", activityid);
                    paramList[1] = new SqlParameter("@flexOrder", flexorder);
                    paramList[2] = new SqlParameter("@flexLabel", flexlabel);
                    paramList[3] = new SqlParameter("@flexControl", flexcontrol);
                    paramList[4] = new SqlParameter("@flexSqlQuery", flexquery);
                    paramList[5] = new SqlParameter("@flexDataType", flexdatatype);
                    paramList[6] = new SqlParameter("@flexMandatoryStatus", flexmandatorystatus);
                    paramList[7] = new SqlParameter("@flexCreatedBy", string.Empty);
                    SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                    paramMsgType.Size = 10;
                    paramMsgType.Direction = ParameterDirection.Output;
                    paramList[8] = paramMsgType;

                    SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                    paramMsgText.Size = 100;
                    paramMsgText.Direction = ParameterDirection.Output;
                    paramList[9] = paramMsgText;

                    DBGateway.ExecuteReaderSP(SPNames.addActivityFlexMaster, paramList, connection);
                    if (Convert.ToString(paramMsgType.Value) == "E")
                        throw new Exception(Convert.ToString(paramMsgText.Value));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }


        public void SavePriceDetails(long activityid, string label, int amount, string type)
        {


            using (SqlConnection connection = DBGateway.GetConnection())
                try
                {
                    SqlParameter[] paramList = new SqlParameter[7];
                    paramList[0] = new SqlParameter("@priceActivityId", activityid);
                    paramList[1] = new SqlParameter("@Label", label);
                    paramList[2] = new SqlParameter("@amount", amount);
                    paramList[3] = new SqlParameter("@type", type);
                    paramList[4] = new SqlParameter("@PriceCreatedBy", string.Empty);
                    SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                    paramMsgType.Size = 10;
                    paramMsgType.Direction = ParameterDirection.Output;
                    paramList[5] = paramMsgType;

                    SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                    paramMsgText.Size = 100;
                    paramMsgText.Direction = ParameterDirection.Output;
                    paramList[6] = paramMsgText;

                    DBGateway.ExecuteReaderSP(SPNames.AddActivityPrice, paramList, connection);
                    if (Convert.ToString(paramMsgType.Value) == "E")
                        throw new Exception(Convert.ToString(paramMsgText.Value));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }

        public void SaveActivityTransaction()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                {

                    long headerId = 0;
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    if (dtTransactionHeader != null && dtTransactionHeader.Rows.Count > 0)
                    {
                        DataRow dr = dtTransactionHeader.Rows[0];
                        //if (dr["ATHId"] != DBNull.Value)
                        //{
                        //    headerId = Convert.ToInt64(dr["ATHId"]);
                        //}


                        SqlParameter[] paramList = new SqlParameter[23];

                        //if (headerId > 0)
                        {
                            paramList[0] = new SqlParameter("@ActivityId", Convert.ToInt32(dr["ActivityId"]));
                            paramList[1] = new SqlParameter("@TripId", "");
                            paramList[2] = new SqlParameter("@TransactionDate", DateTime.Now);
                            paramList[3] = new SqlParameter("@Adult", Convert.ToInt32(dr["Adult"]));
                            paramList[4] = new SqlParameter("@Child", Convert.ToInt32(dr["Child"]));
                            paramList[5] = new SqlParameter("@Infant", Convert.ToInt32(dr["Infant"]));
                            paramList[6] = new SqlParameter("@Booking", Convert.ToDateTime(dr["Booking"], dateFormat));
                            paramList[7] = new SqlParameter("@TotalPrice", Convert.ToDecimal(dr["TotalPrice"]));
                            paramList[8] = new SqlParameter("@CreatedBy", Convert.ToInt32(dr["CreatedBy"]));
                            paramList[9] = new SqlParameter("@CreatedDate", DateTime.Now);
                            paramList[10] = new SqlParameter("@Status", dr["Status"].ToString());
                            paramList[11] = new SqlParameter("@AgencyId", Convert.ToInt32(dr["AgencyId"]));
                            paramList[12] = new SqlParameter("@TransactionType", dr["TransactionType"].ToString());
                            paramList[13] = new SqlParameter("@ATHId", headerId);
                            paramList[13].Direction = ParameterDirection.Output;
                            paramList[14] = new SqlParameter("@P_RET_AGENT_BALANCE", 0);
                            paramList[14].Direction = ParameterDirection.Output;
                            paramList[15] = new SqlParameter("@LocationId", Convert.ToInt32(dr["LocationId"]));
                            paramList[16] = new SqlParameter("@ActivityName", dr["ActivityName"].ToString());
                            //For setting Booking Ref no AD or FD 
                            paramList[17] = new SqlParameter("@IsFixedDeparture", dr["IsFixedDeparture"].ToString());
                            if (isFixedDeparture == "Y")
                            {
                                if (dr["RoomCount"] != DBNull.Value)
                                {
                                    paramList[18] = new SqlParameter("@RoomCount", Convert.ToInt32(dr["RoomCount"]));
                                }
                                else
                                {
                                    paramList[18] = new SqlParameter("@RoomCount", null);
                                }
                                if (dr["QuotedStatus"] != DBNull.Value)
                                {
                                    paramList[19] = new SqlParameter("@QuotedStatus", Convert.ToString(dr["QuotedStatus"]));
                                }
                                else
                                {
                                    paramList[19] = new SqlParameter("@QuotedStatus", null);
                                }
                                if (dr["ATHId"] != DBNull.Value)
                                {
                                    paramList[20] = new SqlParameter("@ATID", Convert.ToInt32(dr["ATHId"]));
                                }
                                else
                                {
                                    paramList[20] = new SqlParameter("@ATID", -1);
                                }

                                if (dr["PaymentStatus"] != DBNull.Value)
                                {
                                    paramList[21] = new SqlParameter("@PaymentStatus", Convert.ToInt32(dr["PaymentStatus"]));
                                }
                                else
                                {
                                    paramList[21] = new SqlParameter("@PaymentStatus", 1);
                                }
                            }
                            else
                            {
                                paramList[18] = new SqlParameter("@RoomCount", null);
                                paramList[19] = new SqlParameter("@QuotedStatus", null);
                                paramList[20] = new SqlParameter("@ATID", -1);
                                paramList[21] = new SqlParameter("@PaymentStatus", 0);
                            }
                            if (dr["CustomerId"] != DBNull.Value)
                            {
                                paramList[22] = new SqlParameter("@CustomerId", Convert.ToInt32(dr["CustomerId"].ToString()));
                            }
                            else
                            {
                                paramList[22] = new SqlParameter("@CustomerId", Convert.ToInt32(dr["CreatedBy"]));
                            }
                            
                            DBGateway.ExecuteNonQueryDetails(cmd, SPNames.AddActivityTransactionHeader, paramList);

                            headerId = Convert.ToInt64(paramList[13].Value);
                            if (isFixedDeparture == "N")
                            {
                                agentBalance = Convert.ToDecimal(paramList[14].Value);
                            }
                            dtTransactionHeader.Rows[0]["ATHId"] = headerId;
                        }

                        //Save Payment breakup details
                        if (isFixedDeparture=="Y")
                        {
                            SaveFixedDeparturePaymentDetails(cmd, headerId, Convert.ToInt32(dtTransactionHeader.Rows[0]["ActivityId"]));
                            SaveVisaFallowUp(cmd, headerId);
                        }

                    }

                    //Save Transaction Detail
                    if (dtTransactionDetail.Rows.Count > 0)
                    {
                        SqlParameter[] paramList = new SqlParameter[21];

                        foreach (DataRow dr in dtTransactionDetail.Rows)
                        {
                            dr["ATHDId"] = headerId;
                            paramList[0] = new SqlParameter("@FirstName", dr["FirstName"].ToString());
                            paramList[1] = new SqlParameter("@LastName", dr["LastName"].ToString());
                            paramList[2] = new SqlParameter("@Phone", dr["Phone"].ToString());
                            paramList[3] = new SqlParameter("@PhoneCountryCode", dr["PhoneCountryCode"].ToString());
                            paramList[4] = new SqlParameter("@Email", dr["Email"].ToString());
                            paramList[5] = new SqlParameter("@Nationality", dr["Nationality"].ToString());
                            paramList[6] = new SqlParameter("@PaxSerial", dr["PaxSerial"].ToString());
                            paramList[7] = new SqlParameter("@CreatedBy", dr["CreatedBy"].ToString());
                            paramList[8] = new SqlParameter("@CreatedOn", dr["CreatedDate"].ToString());
                            paramList[9] = new SqlParameter("@ATHDId", headerId);
                            paramList[10] = new SqlParameter("@ATDId", 0);
                            paramList[10].Direction = ParameterDirection.Output;
                            if (isFixedDeparture == "Y")
                            {
                                if (dr["Title"] != DBNull.Value)
                                {
                                    paramList[11] = new SqlParameter("@Title", dr["Title"].ToString());
                                }
                                else
                                {
                                    paramList[11] = new SqlParameter("@Title", null);
                                }
                                if (dr["MealChoice"] != DBNull.Value)
                                {
                                    paramList[12] = new SqlParameter("@MealChoice", dr["MealChoice"].ToString());
                                }
                                else
                                {
                                    paramList[12] = new SqlParameter("@MealChoice", null);
                                }
                                if (dr["PassportNo"] != DBNull.Value)
                                {
                                    paramList[13] = new SqlParameter("@PassportNo", dr["PassportNo"].ToString());
                                }
                                else
                                {
                                    paramList[13] = new SqlParameter("@PassportNo", null);
                                }
                                if (dr["DateOfBirth"] != DBNull.Value)
                                {
                                    paramList[14] = new SqlParameter("@DateOfBirth", dr["DateOfBirth"].ToString());
                                }
                                else
                                {
                                    paramList[14] = new SqlParameter("@DateOfBirth", null);
                                }
                                if (dr["PassportExpDate"] != DBNull.Value)
                                {
                                    paramList[15] = new SqlParameter("@PassportExpDate", dr["PassportExpDate"].ToString());
                                }
                                else
                                {
                                    paramList[15] = new SqlParameter("@PassportExpDate", null);
                                }

                                if (dr["RoomType"] != DBNull.Value)
                                {
                                    paramList[16] = new SqlParameter("@RoomType", dr["RoomType"].ToString());
                                }
                                else
                                {
                                    paramList[16] = new SqlParameter("@RoomType", null);
                                }
                                
                                if (dr["ReceiptNo"] != null)
                                {
                                    paramList[17] = new SqlParameter("@ReceiptNo", dr["ReceiptNo"].ToString());
                                }
                                else
                                {
                                    paramList[17] = new SqlParameter("@ReceiptNo", null);
                                }
                                if (dr["RoomNo"] != null)
                                {
                                    paramList[18] = new SqlParameter("@RoomNo", dr["RoomNo"].ToString());
                                }
                                else
                                {
                                    paramList[18] = new SqlParameter("@RoomNo", null);
                                }
                                if (dr["ATDId"] != DBNull.Value)
                                {
                                    paramList[19] = new SqlParameter("@ATId", Convert.ToString(dr["ATDId"]));
                                }
                                else
                                {
                                    paramList[19] = new SqlParameter("@ATId", -1);
                                }
                                if (dr["PaxType"] != DBNull.Value)
                                {
                                    paramList[20] = new SqlParameter("@PaxType", dr["PaxType"].ToString());
                                }
                                else
                                {
                                    paramList[20] = new SqlParameter("@PaxType", null);
                                }
                            }
                            else
                            {
                                paramList[11] = new SqlParameter("@Title", null);
                                paramList[12] = new SqlParameter("@MealChoice", null);
                                paramList[13] = new SqlParameter("@PassportNo", null);
                                paramList[14] = new SqlParameter("@DateOfBirth", null);
                                paramList[15] = new SqlParameter("@PassportExpDate", null);
                                paramList[16] = new SqlParameter("@RoomType", null);
                               
                                paramList[17] = new SqlParameter("@ReceiptNo", null);
                                paramList[18] = new SqlParameter("@RoomNo", null);
                                paramList[19] = new SqlParameter("@ATId", -1);
                                paramList[20] = new SqlParameter("@PaxType", null);
                            }
                            DBGateway.ExecuteNonQueryDetails(cmd, SPNames.AddActivityTransactionDetail, paramList);
                            dr["ATDId"] = paramList[10].Value;
                        }
                    }

                    //Save Transaction Price
                    if (dtTransactionPrice.Rows.Count > 0)
                    {
                        int i=0;
                        SqlParameter[] paramList = new SqlParameter[24];
                        foreach (DataRow dr in dtTransactionPrice.Rows)
                        {
                            dr["ATHId"] = headerId;
                            paramList[0] = new SqlParameter("@ATHId", headerId);
                            paramList[1] = new SqlParameter("@Label", dr["Label"].ToString());
                            
                            paramList[2] = new SqlParameter("@Amount", (Convert.ToDecimal(dr["Amount"]) - Convert.ToDecimal(dr["Markup"])));
                            
                            paramList[3] = new SqlParameter("@LabelQty", Convert.ToInt32(dr["LabelQty"]));
                            paramList[4] = new SqlParameter("@LabelAmount", Convert.ToDecimal(dr["LabelAmount"]));
                            paramList[5] = new SqlParameter("@CreatedBy", Convert.ToInt32(dr["CreatedBy"]));
                            paramList[6] = new SqlParameter("@CreatedDate", Convert.ToDateTime(dr["CreatedDate"]));
                            paramList[7] = new SqlParameter("@LastModifiedBy", Convert.ToInt32(dr["CreatedBy"]));
                            paramList[8] = new SqlParameter("@LastModifiedDate", Convert.ToDateTime(dr["CreatedDate"]));
                            paramList[9] = new SqlParameter("@ATPId", 0);
                            paramList[9].Direction = ParameterDirection.Output;
                            paramList[10] = new SqlParameter("@AgentCurrency", Convert.ToString(dr["AgentCurrency"]));
                            paramList[11] = new SqlParameter("@AgentROE", Convert.ToDecimal(dr["AgentROE"]));
                            paramList[12] = new SqlParameter("@MarkupValue", Convert.ToDecimal(dr["MarkupValue"]));
                            paramList[13] = new SqlParameter("@MarkupType", Convert.ToString(dr["MarkupType"]));
                            paramList[14] = new SqlParameter("@SourceCurrency", Convert.ToString(dr["SourceCurrency"]));
                            paramList[15] = new SqlParameter("@SourceAmount", Convert.ToDecimal(dr["SourceAmount"]));
                            paramList[16] = new SqlParameter("@Markup", Convert.ToDecimal(dr["Markup"]));
                            if (isFixedDeparture == "Y")
                            {
                                //if (dr["RoomNo"] != DBNull.Value)
                                //{
                                //    paramList[17] = new SqlParameter("@RoomNo", Convert.ToInt32(dr["RoomNo"]));
                                //}
                                //else
                                //{
                                //    paramList[17] = new SqlParameter("@RoomNo", null);
                                //}
                                //if (dr["PaxType"] != DBNull.Value)
                                //{
                                //    paramList[18] = new SqlParameter("@PaxType", Convert.ToString(dr["PaxType"]));
                                //}
                                //else
                                //{
                                //    paramList[18] = new SqlParameter("@PaxType", null);
                                //}
                                if (dr["ATPId"] != DBNull.Value)
                                {
                                    paramList[17] = new SqlParameter("@ATId", Convert.ToString(dr["ATPId"]));
                                }
                                else
                                {
                                    paramList[17] = new SqlParameter("@ATId", -1);
                                }
                                if (dr["ActivityPriceId"] != DBNull.Value)
                                {
                                    paramList[18] = new SqlParameter("@ActivityPriceId", Convert.ToString(dr["ActivityPriceId"]));
                                }
                                else
                                {
                                    paramList[18] = new SqlParameter("@ActivityPriceId", null);
                                }
                                if (dr["DiscountType"] != DBNull.Value)
                                {
                                    paramList[19] = new SqlParameter("@DiscountType", dr["DiscountType"].ToString());
                                }
                                else
                                {
                                    paramList[19] = new SqlParameter("@DiscountType", null);
                                }
                                if (dr["discountRemarks"] != DBNull.Value)
                                {
                                    paramList[20] = new SqlParameter("@DiscountRemarks", dr["discountRemarks"].ToString());
                                }
                                else
                                {
                                    paramList[20] = new SqlParameter("@DiscountRemarks", null);
                                }
                                if (dr["promotionCode"] != DBNull.Value)
                                {
                                    paramList[21] = new SqlParameter("@PromotionCode", dr["promotionCode"].ToString());
                                }
                                else
                                {
                                    paramList[21] = new SqlParameter("@PromotionCode", null);
                                }
                                if (dr["promotionAmount"] != null)
                                {
                                    paramList[22] = new SqlParameter("@PromotionAmount", dr["promotionAmount"].ToString());
                                }
                                else
                                {
                                    paramList[22] = new SqlParameter("@PromotionAmount", null);
                                }
                                paramList[23] = new SqlParameter("@ATDID", Convert.ToInt32(dtTransactionDetail.Rows[i]["ATDId"]));
                            }
                            else
                            {
                                //paramList[17] = new SqlParameter("@RoomNo", null);
                                //paramList[18] = new SqlParameter("@PaxType", null);
                                paramList[17] = new SqlParameter("@ATId", -1);
                                paramList[18] = new SqlParameter("@ActivityPriceId", null);
                                paramList[19] = new SqlParameter("@DiscountType", null);
                                paramList[20] = new SqlParameter("@DiscountRemarks", null);
                                paramList[21] = new SqlParameter("@PromotionCode", null);
                                paramList[22] = new SqlParameter("@PromotionAmount", null);
                                paramList[23] = new SqlParameter("@ATDID", Convert.ToInt32(dtTransactionDetail.Rows[0]["ATDId"]));
                            }
                            
                            i++;
                            DBGateway.ExecuteNonQueryDetails(cmd, SPNames.AddActivityTransactionPrice, paramList);

                            dr["ATPId"] = paramList[9].Value;
                        }
                    }

                    //Save Flex Details
                    if (dtFlexDetails.Rows.Count > 0)
                    {
                        SqlParameter[] paramList = new SqlParameter[8];
                        foreach (DataRow dr in dtFlexDetails.Rows)
                        {
                            paramList[0] = new SqlParameter("@ATHId", headerId);
                            paramList[1] = new SqlParameter("@ActivityId", Convert.ToInt32(dtTransactionHeader.Rows[0]["ActivityId"]));
                            paramList[2] = new SqlParameter("@FlexId", Convert.ToInt64(dr["flexId"]));
                            paramList[3] = new SqlParameter("@FlexLabel", dr["flexLabel"].ToString());
                            paramList[4] = new SqlParameter("@FlexData", dr["FlexData"].ToString());
                            paramList[5] = new SqlParameter("@flexCreatedBy", Convert.ToInt64(dr["flexCreatedBy"]));
                            paramList[6] = new SqlParameter("@flexCreatedOn", Convert.ToDateTime(dr["flexCreatedOn"]));
                            paramList[7] = new SqlParameter("@ATFDId", 0);
                            paramList[7].Direction = ParameterDirection.Output;

                            DBGateway.ExecuteNonQueryDetails(cmd, SPNames.AddActivityFlexDetails, paramList);

                            dr["ATFDId"] = paramList[7].Value;
                        }
                    }
                    if (isFixedDeparture == "N")
                    {
                        //Update Stock Used
                        SqlParameter[] paramListStock = new SqlParameter[1];
                        paramListStock[0] = new SqlParameter("@ActivityId", id);
                        DBGateway.ExecuteNonQuerySP(SPNames.UpdateActivityStock, paramListStock);
                    }
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            finally
            {
                DBGateway.CloseConnection(cmd);
            }
        }

        void SaveFixedDeparturePaymentDetails(SqlCommand cmd, long headerId,int activityId)
        {
            try
            {
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                if (dtFDPaymentDetails.Rows.Count > 0)
                {
                    foreach (DataRow row in dtFDPaymentDetails.Rows)
                    {
                        SqlParameter[] paramList = new SqlParameter[13];
                        paramList[0] = new SqlParameter("@P_TranxId", headerId);
                        paramList[1] = new SqlParameter("@P_ATHDId", activityId);
                        paramList[2] = new SqlParameter("@P_SettlmentMode", Convert.ToInt32(row["SettlementMode"]));
                        paramList[3] = new SqlParameter("@P_Amount", Convert.ToDecimal(row["Amount"]));
                        paramList[4] = new SqlParameter("@P_BalanceAmount", Convert.ToDecimal(row["BalanceAmount"]));
                        paramList[5] = new SqlParameter("@P_Currency", row["Currency"].ToString());
                        paramList[6] = new SqlParameter("@P_ExchangeRate", Convert.ToDecimal(row["ExchangeRate"]));
                        paramList[7] = new SqlParameter("@P_CreditCardId", Convert.ToInt32(row["CreditCardId"]));
                        paramList[8] = new SqlParameter("@P_CreatedBy", Convert.ToInt32(row["CreatedBy"]));
                        if (row["CCCharge"] != DBNull.Value)
                        {
                            paramList[9] = new SqlParameter("@P_CCCharge", Convert.ToDecimal(row["CCCharge"]));
                        }
                        else
                        {
                            paramList[9] = new SqlParameter("@P_CCCharge", DBNull.Value);
                        }
                        if (row["NextPaymentDate"] != DBNull.Value)
                        {
                            paramList[10] = new SqlParameter("@P_NextPaymentDate", Convert.ToDateTime(row["NextPaymentDate"], dateFormat));
                        }
                        else
                        {
                            paramList[10] = new SqlParameter("@P_NextPaymentDate", DBNull.Value);
                        }
                        paramList[11] = new SqlParameter("@P_PaymentType", Convert.ToInt32(row["PaymentType"]));
                        if (row["PromotionAmount"] != DBNull.Value)
                        {
                            paramList[12] = new SqlParameter("@P_PromotionAmount", Convert.ToInt32(row["PromotionAmount"]));
                        }
                        else
                        {
                            paramList[12] = new SqlParameter("@P_PromotionAmount", 0);
                        }
                        
                        DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddFixedDeparturePaymentDetails", paramList);
                    }
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        void SaveVisaFallowUp(SqlCommand cmd, long headerId)
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (dtFDVisaFollowup.Rows.Count > 0)
            {
                foreach (DataRow row in dtFDVisaFollowup.Rows)
                {
                    SqlParameter[] paramList = new SqlParameter[4];
                    paramList[0] = new SqlParameter("@P_ATHId", headerId);
                    paramList[1] = new SqlParameter("@P_Visa_date", Convert.ToDateTime(row["Visa_date"], dateFormat));
                    paramList[2] = new SqlParameter("@P_MailRequired", Convert.ToString(row["MailRequired"]));
                    paramList[3] = new SqlParameter("@P_Visa_created_by", Convert.ToInt32(row["Visa_created_by"]));
                    DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddFixedDepartureVisaDetails", paramList);
                }
            }
        }


        public static void SaveFixedDeparturePaymentDetails(long headerId,long activityId, double total, double paidAmount, DataTable dtPaymentDetails,double discount)
        {
            try
            {
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                foreach (DataRow row in dtPaymentDetails.Rows)
                {
                    SqlParameter[] paramList = new SqlParameter[12];
                    paramList[0] = new SqlParameter("@P_TranxId", headerId);
                    paramList[1] = new SqlParameter("@P_ATHDId", activityId);
                    paramList[2] = new SqlParameter("@P_SettlmentMode", Convert.ToInt32(row["SettlementMode"]));
                    paramList[3] = new SqlParameter("@P_Amount", Convert.ToDecimal(row["Amount"]));
                    paramList[4] = new SqlParameter("@P_BalanceAmount", Convert.ToDecimal(row["BalanceAmount"]));
                    paramList[5] = new SqlParameter("@P_Currency", row["Currency"].ToString());
                    paramList[6] = new SqlParameter("@P_ExchangeRate", Convert.ToDecimal(row["ExchangeRate"]));
                    paramList[7] = new SqlParameter("@P_CreditCardId", Convert.ToInt32(row["CreditCardId"]));
                    paramList[8] = new SqlParameter("@P_CreatedBy", Convert.ToInt32(row["CreatedBy"]));
                    if (row["CCCharge"] != DBNull.Value)
                    {
                        paramList[9] = new SqlParameter("@P_CCCharge", Convert.ToDecimal(row["CCCharge"]));
                    }
                    else
                    {
                        paramList[9] = new SqlParameter("@P_CCCharge", DBNull.Value);
                    }
                    if (row["NextPaymentDate"] != DBNull.Value)
                    {
                        paramList[10] = new SqlParameter("@P_NextPaymentDate", Convert.ToDateTime(row["NextPaymentDate"], dateFormat));
                    }
                    else
                    {
                        paramList[10] = new SqlParameter("@P_NextPaymentDate", DBNull.Value);
                    }
                    paramList[11] = new SqlParameter("@P_PaymentType", Convert.ToInt32(row["PaymentType"]));
                    paidAmount += Convert.ToDouble(row["Amount"]);
                    
                    DBGateway.ExecuteNonQuerySP("usp_AddFixedDeparturePaymentDetails", paramList);
                }

                if ((total - Math.Ceiling(paidAmount))- Math.Ceiling(discount) == 0)
                {
                    UpdatePaymentStatusForTransactionHeader(headerId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void SaveVisaFallowUp(DataTable dtFDVisaFollowup)
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (dtFDVisaFollowup.Rows.Count > 0)
            {
                foreach (DataRow row in dtFDVisaFollowup.Rows)
                {
                    SqlParameter[] paramList = new SqlParameter[4];
                    paramList[0] = new SqlParameter("@P_ATHId", Convert.ToInt32(row["ATHId"]));
                    paramList[1] = new SqlParameter("@P_Visa_date", Convert.ToDateTime(row["Visa_date"], dateFormat));
                    paramList[2] = new SqlParameter("@P_MailRequired", Convert.ToString(row["MailRequired"]));
                    paramList[3] = new SqlParameter("@P_Visa_created_by", Convert.ToInt32(row["Visa_created_by"]));
                    DBGateway.ExecuteNonQuerySP("usp_AddFixedDepartureVisaDetails", paramList);
                }
            }
        }

        static void UpdatePaymentStatusForTransactionHeader(long tranxId)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@TranxId", tranxId);
            DBGateway.ExecuteNonQuerySP("usp_UpdatePaymentStatusFDTransactionHeader", paramList);
            
        }

        public static void UpdateStatusForTransactionHeader(long fixId, string status)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@fixId", fixId);
            paramList[1] = new SqlParameter("@Status", status);
            DBGateway.ExecuteNonQuerySP("usp_UpdateStatusFDTransactionHeader", paramList);
        }
        

        public void UpdatePaymentForTransactionHeader()
        {
            long headerId = 0;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (dtTransactionHeader != null && dtTransactionHeader.Rows.Count > 0)
            {
                DataRow dr = dtTransactionHeader.Rows[0];
                if (dr["ATHId"] != DBNull.Value)
                {
                    headerId = Convert.ToInt64(dr["ATHId"]);
                }

                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@ActivityId", id);
                paramList[1] = new SqlParameter("@TripId", dr["TripId"].ToString());
                paramList[2] = new SqlParameter("@TransactionDate", DateTime.Now);
                paramList[3] = new SqlParameter("@OrderId", Convert.ToInt32(dr["OrderId"]));
                paramList[4] = new SqlParameter("@PaymentId", dr["PaymentId"].ToString());
                paramList[5] = new SqlParameter("@LastModifiedBy", Convert.ToInt32(dr["CreatedBy"]));
                paramList[6] = new SqlParameter("@LastModifiedDate", Convert.ToDateTime(DateTime.Now, dateFormat));                
                paramList[7] = new SqlParameter("@ActivityStatus", 1);
                paramList[8] = new SqlParameter("@ATHId", headerId);

                DBGateway.ExecuteNonQuerySP(SPNames.UpdateActivityTransactionHeader, paramList);
            }
        }

        public void AddCreditCardInformation(string paymentId, int referenceId, ReferenceIdType referenceType, double amount, string ipAddress, long trackId, PGSource paymentGatewayId, string address, string zip)
        {
            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@paymentInformationId", 0);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@referenceIdType",(int) referenceType);
            paramList[2] = new SqlParameter("@referenceId", referenceId);
            paramList[3] = new SqlParameter("@paymentId", paymentId);
            paramList[4] = new SqlParameter("@amount", amount);
            paramList[5] = new SqlParameter("@ipAddress", ipAddress);
            paramList[6] = new SqlParameter("@trackId", trackId);
            paramList[7] = new SqlParameter("@paymentGatewayId",(int) paymentGatewayId);
            paramList[8] = new SqlParameter("@address", address);
            paramList[9] = new SqlParameter("@zip", zip);

            DBGateway.ExecuteNonQuerySP("usp_AddCreditCardPayment", paramList);
        }

        //public static DataTable GetActivityQueue(Int64 customerId, int startRowIndex, ref int totalRecordCount, int recordsPerPage)
        //{
        //    DataTable dtQueue = null;

        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[4];
        //        paramList[0] = new SqlParameter("@CustomerId", customerId);
        //        paramList[1] = new SqlParameter("@StartRowIndex", startRowIndex);
        //        paramList[2] = new SqlParameter("@TotalRecord", totalRecordCount);
        //        paramList[2].Direction = ParameterDirection.Output;
        //        paramList[3] = new SqlParameter("@RecordsPerPage", recordsPerPage);

        //        dtQueue = DBGateway.FillDataTableSP(SPNames.GetActivityQueue, paramList);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return dtQueue;
        //}

        public static DataTable GetActivityBookingQueue(DateTime fromDate, DateTime toDate, string tripId, string paxName, decimal price, int agencyId, string memberType, decimal locationId, long loginUserId, string isFixedDeparture, string agentType, string status, string transType)
        {
            DataTable dtBookingQueue = null;

            try
            {
                SqlParameter[] paramList = new SqlParameter[13];
                //if (fromDate != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                if (fromDate != DateTime.MinValue) paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (!String.IsNullOrEmpty(tripId)) paramList[2] = new SqlParameter("@P_TRIP_ID", tripId);
                if (!String.IsNullOrEmpty(paxName)) paramList[3] = new SqlParameter("@P_PAX_NAME", paxName);
                if (price > 0) paramList[4] = new SqlParameter("@P_PRICE", price);
                if (agencyId > 0) paramList[5] = new SqlParameter("@P_AGENCY_ID", agencyId);
                paramList[6] = new SqlParameter("@P_USER_TYPE", memberType);
                paramList[7] = new SqlParameter("@P_LOCATION_ID", locationId);
                paramList[8] = new SqlParameter("@P_USER_ID", loginUserId);
                paramList[9] = new SqlParameter("@P_ISFIXED_DEPARTURE", isFixedDeparture);
                if (!String.IsNullOrEmpty(status)) paramList[10] = new SqlParameter("@P_STATUS", status);
                if (!String.IsNullOrEmpty(agentType)) paramList[11] = new SqlParameter("@P_AGENT_TYPE", agentType);
                paramList[12] = new SqlParameter("@P_TransType", transType);
                dtBookingQueue = DBGateway.FillDataTableSP(SPNames.GetActivityQueue, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
            return dtBookingQueue;
        }

        public static DataTable GetFDChangeRequestQueue(DateTime fromDate, DateTime toDate, string tripId, string paxName, decimal price, int agencyId, string memberType, decimal locationId, long loginUserId, string isFixedDeparture)
        {
            DataTable dtBookingQueue = null;

            try
            {
                SqlParameter[] paramList = new SqlParameter[10];
                if (fromDate != DateTime.MinValue) paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (!String.IsNullOrEmpty(tripId)) paramList[2] = new SqlParameter("@P_TRIP_ID", tripId);
                if (!String.IsNullOrEmpty(paxName)) paramList[3] = new SqlParameter("@P_PAX_NAME", paxName);
                if (price > 0) paramList[4] = new SqlParameter("@P_PRICE", price);
                if (agencyId > 0) paramList[5] = new SqlParameter("@P_AGENCY_ID", agencyId);
                paramList[6] = new SqlParameter("@P_USER_TYPE", memberType);
                paramList[7] = new SqlParameter("@P_LOCATION_ID", locationId);
                paramList[8] = new SqlParameter("@P_USER_ID", loginUserId);
                paramList[9] = new SqlParameter("@P_ISFIXED_DEPARTURE", isFixedDeparture);
                dtBookingQueue = DBGateway.FillDataTableSP(SPNames.GetFixedDepartureChangeRequestData, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
            return dtBookingQueue;
        }


        public static DataSet GetActivityQueueDetails(long id)
        {
            try
            {
                DataSet ds = null;
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ACT_HDR_ID", id);
                ds = DBGateway.FillSP(SPNames.GetActivityQueueDetials , paramList);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetFixedDeparturePayments(long tranxId)
        {
            DataTable dtPayments = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@TranxId", tranxId);
                dtPayments = DBGateway.FillDataTableSP("usp_GetFDPayments", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtPayments;
        }

        public static DataTable GetFixedDepartureVisaFollowUp(long ATHId)
        {
            DataTable dtVisaFollowUp = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ATHId", ATHId);
                dtVisaFollowUp = DBGateway.FillDataTableSP("usp_GetFDVisaFollowUp", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtVisaFollowUp;
        }

        public static int SaveDocumentHeader(long ATHId,string filePath,string fileName)
        {
            int returnValue = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@ATHId", ATHId);
                paramList[1] = new SqlParameter("@FilePath", filePath);
                paramList[2] = new SqlParameter("@FileName", fileName);
                returnValue = DBGateway.ExecuteNonQuerySP("usp_SaveDocumentHeader", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }
        //B2C FixedDepartureQueue
        public static DataTable GetB2CFixedDepartureBookingQueue(DateTime fromDate, DateTime toDate, string tripId, string paxName, decimal price, int agencyId,string isFixedDeparture)
        {
            DataTable dtBookingQueue = null;
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                if (fromDate != DateTime.MinValue) paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (!String.IsNullOrEmpty(tripId)) paramList[2] = new SqlParameter("@P_TRIP_ID", tripId);
                if (!String.IsNullOrEmpty(paxName))paramList[3] = new SqlParameter("@P_PAX_NAME", paxName);
                if (price > 0) paramList[4] = new SqlParameter("@P_PRICE", price);
                if (agencyId > 0) paramList[5] = new SqlParameter("@P_AGENCY_ID", agencyId);
                dtBookingQueue = DBGateway.FillDataTableSP(SPNames.GetB2CFixDepartureQueueList, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
            return dtBookingQueue;
        }

    }
}

