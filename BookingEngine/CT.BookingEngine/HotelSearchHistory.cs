﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;
using CT.Core;

namespace CT.BookingEngine
{
    /// <summary>
    /// HotelSearchInfo Properties 
    /// </summary>
    public class HotelSearchInfo
    {
        public long HistoryId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CityName { get; set; }
        public long CityId { get; set; }
        public int NoOfRooms { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public int MinRating { get; set; }
        public int MaxRating { get; set; }
        public string PassengerNationality { get; set; }
        public string Sources { get; set; }
        public DateTime SearchDateTime { get; set; }
        public long AgentId { get; set; }
        public long LocationId { get; set; }
        public long UserId { get; set; }
        public string HotelName { get; set; }
        public string AgentName { get; set; }
        public string LocationName { get; set; }
        public string UserName { get; set; }
        public string TransType { get; set; }

        #region PUBLIC METHODS
        /// <summary>
        /// To Save Hotel search Information
        /// </summary>
        /// <param name="req"></param>
        /// <param name="SearchData"></param>
        /// <param name="AgentId"></param>
        /// <param name="UserId"></param>
        /// <param name="LocationId"></param>
        public void SaveSearchInfo(HotelRequest req, string SearchData, long AgentId, long UserId, long LocationId)
        {
            SqlTransaction trans = DBGateway.GetConnection().BeginTransaction();
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@P_HistoryId", HistoryId));
                if (HistoryId == 0)
                    paramList[0].Direction = ParameterDirection.Output;
                paramList.Add(new SqlParameter("@P_SearchData", SearchData));
                paramList.Add(new SqlParameter("@P_agentId", AgentId));
                paramList.Add(new SqlParameter("@P_userId", UserId));
                paramList.Add(new SqlParameter("@P_LocationId", LocationId));
                paramList.Add(new SqlParameter("@P_CheckInDate", req.StartDate));
                paramList.Add(new SqlParameter("@P_CheckOutDate", req.EndDate));
                paramList.Add(new SqlParameter("@P_CityId", req.CityId));
                paramList.Add(new SqlParameter("@P_CityName", req.CityName));
                paramList.Add(new SqlParameter("@P_PassengerNationality", req.PassengerNationality));
                paramList.Add(new SqlParameter("@P_CountryName", req.CountryName));
                paramList.Add(new SqlParameter("@P_CountryCode", req.CountryCode));
                paramList.Add(new SqlParameter("@P_MinRating", req.MinRating));
                paramList.Add(new SqlParameter("@P_MaxRating", req.MaxRating));
                paramList.Add(new SqlParameter("@P_NoOfRooms", req.NoOfRooms));
                paramList.Add(new SqlParameter("@P_Sources", string.Join(",", req.Sources.ToArray())));
                paramList.Add(new SqlParameter("@P_TransType", req.Transtype));
                HistoryId = DBGateway.ExecuteNonQuerySP("usp_SaveHotelSearchHistory_Details", paramList.ToArray());
                HistoryId = (long)paramList[0].Value;

                for (int i = 0; i < req.RoomGuest.Length; i++)
                {
                    RoomGuest roomGuestObj = new RoomGuest();
                    roomGuestObj.Adults = req.RoomGuest[i].noOfAdults;
                    roomGuestObj.childs = req.RoomGuest[i].noOfChild;
                    if(req.RoomGuest[i].childAge != null)
                    for (int j = 0; j < req.RoomGuest[i].childAge.Count; j++)
                    {
                        if (j == 0)
                        {
                            roomGuestObj.ChildAge1 = req.RoomGuest[i].childAge[j];
                        }
                        if (j == 1)
                        {
                            roomGuestObj.ChildAge2 = req.RoomGuest[i].childAge[j];
                        }
                        if (j == 2)
                        {
                            roomGuestObj.ChildAge3 = req.RoomGuest[i].childAge[j];
                        }
                        if (j == 3)
                        {
                            roomGuestObj.ChildAge4 = req.RoomGuest[i].childAge[j];
                        }
                        if (j == 4)
                        {
                            roomGuestObj.ChildAge5 = req.RoomGuest[i].childAge[j];
                        }
                        if (j == 5)
                        {
                            roomGuestObj.ChildAge6 = req.RoomGuest[i].childAge[j];
                        }
                    }
                    roomGuestObj.SaveRoomGuestInfo(HistoryId);
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to Save HotelSearch Info. Reason: " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// To Get Hotel Search Information by dafault today's date and respective filter search
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="userId"></param>
        /// <param name="agentId"></param>
        /// <param name="LocationId"></param>
        /// <param name="CityName"></param>
        /// <param name="CheckInDate"></param>
        /// <param name="CheckOutDate"></param>
        /// <param name="Nationality"></param>
        /// <returns></returns>       
        public static DataSet GetHotelSearchDetails(DateTime fromDate, DateTime toDate, int userId, int agentId, int LocationId, string CityName, DateTime CheckInDate, DateTime CheckOutDate, int Nationality)
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                if (fromDate != DateTime.MinValue)
                    paramList.Add(new SqlParameter("@P_FromDate", fromDate));
                if (toDate != DateTime.MinValue)
                    paramList.Add(new SqlParameter("@P_ToDate", toDate));
                if (userId > 0) paramList.Add(new SqlParameter("@p_UserId", userId));
                if (agentId > 0) paramList.Add(new SqlParameter("@P_AgentId", agentId));
                if (LocationId != -1) paramList.Add(new SqlParameter("@P_LocationId", LocationId));
                if (!string.IsNullOrEmpty(CityName)) paramList.Add(new SqlParameter("@P_CityName", CityName));
                if (CheckInDate != DateTime.MinValue)
                    paramList.Add(new SqlParameter("@P_CheckIn", CheckInDate));
                if (CheckOutDate != DateTime.MinValue)
                    paramList.Add(new SqlParameter("@P_CheckOut", CheckOutDate));
                if (Nationality > 0) paramList.Add(new SqlParameter("@P_PassengerNationality", Nationality));
                return DBGateway.ExecuteQuery("usp_HotelSearchInfo_Details", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to Get HotelSearch Info. Reason: " + ex.ToString(), "");
                return null;
            }
        }

        /// <summary>
        /// To Get Room Guest details to pass parameter historyId from Hotel search details
        /// </summary>
        /// <param name="HistoryId"></param>
        /// <returns></returns>
        public static DataTable GetRoomGuestDetails(int HistoryId)
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@P_HistoryId", HistoryId));
                return DBGateway.FillDataTableSP("usp_HotelSearch_RoomGuestInfo_Details", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to Get RoomGuest Details Info. Reason: " + ex.ToString(), "");
                return null;
            }
        }

        #endregion
    }

    /// <summary>
    /// RoomGuest Properties
    /// </summary>
    public class RoomGuest
    {
        public long GuestId { get; set; }
        public int Adults { get; set; }
        public int childs { get; set; }
        public int ChildAge1 { get; set; }
        public int ChildAge2 { get; set; }
        public int ChildAge3 { get; set; }
        public int ChildAge4 { get; set; }
        public int ChildAge5 { get; set; }
        public int ChildAge6 { get; set; }

        /// <summary>
        ///  To Save Room Guest Information
        /// </summary>
        /// <param name="HistoryId"></param>
        public void SaveRoomGuestInfo(long HistoryId)
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@P_HistoryId", HistoryId));
                paramList.Add(new SqlParameter("@P_Adults", Adults));
                paramList.Add(new SqlParameter("@P_Childs", childs));
                if (ChildAge1 > 0) paramList.Add(new SqlParameter("@P_ChildAge1", ChildAge1));
                if (ChildAge2 > 0) paramList.Add(new SqlParameter("@P_ChildAge2", ChildAge2));
                if (ChildAge3 > 0) paramList.Add(new SqlParameter("@P_ChildAge3", ChildAge3));
                if (ChildAge4 > 0) paramList.Add(new SqlParameter("@P_ChildAge4", ChildAge4));
                if (ChildAge5 > 0) paramList.Add(new SqlParameter("@P_ChildAge5", ChildAge5));
                if (ChildAge6 > 0) paramList.Add(new SqlParameter("@P_ChildAge6", ChildAge6));
                DBGateway.ExecuteNonQuerySP("usp_SaveRoomGuestInfo", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to Save RoomGuest Details Info. Reason: " + ex.ToString(), "");               
            }
        }

    }
}
