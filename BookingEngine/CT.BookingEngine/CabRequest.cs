﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.BookingEngine
{
    public class CabRequestHeader
    {
        # region properties
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public int AgentId { get; set; }
        public int LocationId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public string ReferenceNumber { get; set; }

        #endregion

        public int Save()
        {
            int rowCount = 0;
            try
            {
                SqlParameter[] parametersList = new SqlParameter[7];
                parametersList[0] = new SqlParameter("@CR_ID", SqlDbType.Int);
                parametersList[0].Direction = ParameterDirection.Output;
                parametersList[1] = new SqlParameter("@Name", Name);
                parametersList[2] = new SqlParameter("@MobileNumber", MobileNumber);
                parametersList[3] = new SqlParameter("@AgentId", AgentId);
                parametersList[4] = new SqlParameter("@LocationId", LocationId);
                parametersList[5] = new SqlParameter("@CreatedBy", CreatedBy);
                parametersList[6] = new SqlParameter("@ReferenceNumber", ReferenceNumber);

                rowCount = DBGateway.ExecuteNonQuerySP(SPNames.SaveCabRequestHeader, parametersList);
                return Convert.ToInt32(parametersList[0].Value);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.ChangeRequest, CT.Core.Severity.High, 0, "Error: while Inserting Cab Request Details.| " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            }
            return rowCount;

        }

        public DataTable GetCabPreferences()
        {
            SqlParameter[] paramList = new SqlParameter[0];
            
            return  DBGateway.FillDataTableSP(SPNames.GetCabPreference, paramList);
        }

        public string GetCabReferenceNumber()
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@RefNumber", SqlDbType.VarChar,30);
            paramList[0].Direction = ParameterDirection.Output;
            DBGateway.ExecuteQuery(SPNames.GetCabReferenceNumber, paramList);
            return Convert.ToString(paramList[0].Value);
            
        }
    }

    public class CabRequestDetails
    {
        public int DetId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime PickUpTime { get; set; }
        public string RentalType { get; set; }
        public string CabPreference { get; set; }
        public string CabModel { get; set; }
        public string PickUpFrom { get; set; }
        public string DropAt { get; set; }
        public string SpecialRequest { get; set; }
        public string Email { get; set; }
        public int? AgentId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public string ReferenceNumber { get; set; }
        public string Status { get; set; }
        public string FlightNumber { get; set; }

        public void Save()
        {
            try
            {
                SqlParameter[] parametersList = new SqlParameter[16];
                parametersList[0] = new SqlParameter("@detId", DetId);
                parametersList[1] = new SqlParameter("@FromDate", FromDate);
                parametersList[2] = new SqlParameter("@ToDate", ToDate);
                parametersList[3] = new SqlParameter("@PickUpTime", PickUpTime.ToShortTimeString());
                parametersList[4] = new SqlParameter("@RentalType", RentalType);
                parametersList[5] = new SqlParameter("@CabModel", CabModel);
                parametersList[6] = new SqlParameter("@CabPreference", CabPreference);
                parametersList[7] = new SqlParameter("@PickUpFrom", PickUpFrom);
                parametersList[8] = new SqlParameter("@DropAt", DropAt);
                parametersList[9] = new SqlParameter("@SpecialRequest", SpecialRequest);
                parametersList[10] = new SqlParameter("@ReferenceNumber", ReferenceNumber);
                parametersList[11] = new SqlParameter("@Email", Email);
                parametersList[12] = new SqlParameter("@AgentId", AgentId);
                parametersList[13] = new SqlParameter("@CreatedBy", CreatedBy);
                parametersList[14] = new SqlParameter("@Status", Status);
                parametersList[15] = new SqlParameter("@FlightNumber", FlightNumber);

                DBGateway.ExecuteNonQuerySP(SPNames.SaveCabRequestDetails, parametersList);
            }
            catch(Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.ChangeRequest, CT.Core.Severity.High, 0, "Error: while Saving Cab Request Details.| " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            }

        }

        public DataTable GetCabCharge(string rentalType,string cabModel,string cabPreference)
        {      
                SqlParameter[] sqlParameters = new SqlParameter[3];
                sqlParameters[0] = new SqlParameter("@rentalType", rentalType);
                sqlParameters[1] = new SqlParameter("@cabPreference", cabPreference);
                sqlParameters[2] = new SqlParameter("@cabModel", cabModel);
                return DBGateway.FillDataTableSP(SPNames.GetCabCharge, sqlParameters);
        }
    }
}
