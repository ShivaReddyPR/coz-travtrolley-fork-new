using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    [Serializable]
    public class Ticket
    {
        #region Private members and Public Variables
        /// <summary>
        /// Unique Id of a ticket.
        /// </summary>
        private int ticketId;
        /// <summary>
        /// Gets or sets ticketId
        /// </summary>
        public int TicketId
        {
            get { return ticketId; }
            set { ticketId = value; }
        }

        /// <summary>
        /// PaxId of passenger
        /// </summary>
        private int paxId;
        /// <summary>
        /// Gets or sets PaxId of passenger
        /// </summary>
        public int PaxId
        {
            get { return paxId; }
            set { paxId = value; }
        }

        /// <summary>
        /// Flight Id to which the ticket is associated.
        /// Flight means collection of flight segments
        /// </summary>
        private int flightId;
        /// <summary>
        /// Gets or sets flight Id to which the ticket is associated.
        /// </summary>
        public int FlightId
        {
            get { return flightId; }
            set { flightId = value; }
        }

        /// <summary>
        /// Ticket number
        /// </summary>
        private string ticketNumber;
        /// <summary>
        /// Gets or sets ticket number
        /// </summary>
        public string TicketNumber
        {
            get { return ticketNumber; }
            set { ticketNumber = value; }
        }

        string remarks = string.Empty;

        string fareRule = string.Empty;

        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        public string FareRule
        {
            get
            {
                return fareRule;
            }
            set
            {
                fareRule = value;
            }
        }

        /// <summary>
        /// Last two digits of the last ticket number in case more than one ticket is issued. eg when segment count is more than 4
        /// </summary>
        private string conjunctionNumber;
        /// <summary>
        /// Gets or sets the conjunction Number. 
        /// </summary>
        public string ConjunctionNumber
        {
            get { return conjunctionNumber; }
            set { conjunctionNumber = value; }
        }

        /// <summary>
        /// First Name of the passenger
        /// </summary>
        private string paxFirstName;
        /// <summary>
        /// Gets or set first name of the passenger
        /// </summary>
        public string PaxFirstName
        {
            get { return paxFirstName; }
            set { paxFirstName = value; }
        }

        /// <summary>
        /// Last Name of the passenger
        /// </summary>
        private string paxLastName;
        /// <summary>
        /// Gets or sets the last name of the passenger
        /// </summary>
        public string PaxLastName
        {
            get { return paxLastName; }
            set { paxLastName = value; }
        }

        /// <summary>
        /// type of passenger
        /// </summary>
        private PassengerType paxType;
        /// <summary>
        /// Gets or sets the passenger type.
        /// </summary>
        public PassengerType PaxType
        {
            get { return paxType; }
            set { paxType = value; }
        }

        /// <summary>
        /// Ticket number against which the reissue is done
        /// </summary>
        private string issueInExchange;
        /// <summary>
        /// Gets or sets the the issueInExchange. Ticket number against which the reissue is done.
        /// </summary>
        public string IssueInExchange
        {
            get { return issueInExchange; }
            set { issueInExchange = value; }
        }

        /// <summary>
        /// The original ticket number for which the reissue is made
        /// </summary>
        private string originalIssue;
        /// <summary>
        /// Gets or sets the original ticket number for which the reissue is made
        /// </summary>
        public string OriginalIssue
        {
            get { return originalIssue; }
            set { originalIssue = value; }
        }

        /// <summary>
        /// Tour code of the flight
        /// </summary>
        private string tourCode;
        /// <summary>
        /// Gets or sets the tour code.
        /// </summary>
        public string TourCode
        {
            get { return tourCode; }
            set { tourCode = value; }
        }


        /// <summary>
        /// Form of payment
        /// </summary>
        private string fop;
        /// <summary>
        /// Gets or sets the form of payment.
        /// </summary>
        public string FOP
        {
            get { return fop; }
            set { fop = value; }
        }

        /// <summary>
        /// Fare calculation as string
        /// </summary>
        private string fareCalculation;
        /// <summary>
        /// Gets or sets the fareCalculation
        /// </summary>
        public string FareCalculation
        {
            get { return fareCalculation; }
            set { fareCalculation = value; }
        }

        /// <summary>
        /// Airlines ticket designator
        /// </summary>
        private string ticketDesignator;
        /// <summary>
        /// Gets or sets ticketDesignator field
        /// </summary>
        public string TicketDesignator
        {
            get { return ticketDesignator; }
            set { ticketDesignator = value; }
        }

        /// <summary>
        /// Endorsement for a ticket
        /// </summary>
        private string endorsement;
        /// <summary>
        /// Gets or sets endorsement for a ticket
        /// </summary>
        public string Endorsement
        {
            get { return endorsement; }
            set { endorsement = value; }
        }

        /// <summary>
        /// Validating airline code. Two character airline code
        /// </summary>
        private string validatingAriline;
        /// <summary>
        /// Gets or sets Validating Airline code.
        /// </summary>
        public string ValidatingAriline
        {
            get { return validatingAriline; }
            set { validatingAriline = value; }
        }

        /// <summary>
        /// Date of issue of ticket
        /// </summary>
        private DateTime issueDate;
        /// <summary>
        /// Gets or sets the issue date of ticket
        /// </summary>
        public DateTime IssueDate
        {
            get { return issueDate; }
            set { issueDate = value; }
        }

        /// <summary>
        /// Status of the ticket
        /// </summary>
        private string status;
        /// <summary>
        /// Gets or sets status of the ticket
        /// </summary>
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        /// <summary>
        /// Price of ticket containing details of selling price, net price and tax
        /// </summary>
        private PriceAccounts price;

        /// <summary>
        /// Gets or sets the price of ticket
        /// </summary>

        public PriceAccounts Price
        {
            get { return price; }
            set { price = value; }
        }

        /// <summary>
        /// string key stands for Tax Type.
        /// double value is the Tax Value.
        /// </summary>
        private List<KeyValuePair<string, decimal>> taxBreakup;

        public List<KeyValuePair<string, decimal>> TaxBreakup
        {
            get { return taxBreakup; }
            set { taxBreakup = value; }
        }

        /// <summary>
        /// True if this is an e-ticket.
        /// </summary>
        private bool eTikcet;
        /// <summary>
        /// Gets or sets eTicket.
        /// </summary>
        public bool ETicket
        {
            get { return eTikcet; }
            set { eTikcet = value; }
        }

        /// <summary>
        /// True if this is an e-ticket.
        /// </summary>
        private decimal serviceFee;
        /// <summary>
        /// Gets or sets eTicket.
        /// </summary>
        public decimal ServiceFee
        {
            get { return serviceFee; }
            set { serviceFee = value; }
        }
        /// <summary>
        /// True if this is an e-ticket.
        /// </summary>
        private ServiceFeeDisplay showServiceFee;
        /// <summary>
        /// Gets or sets eTicket.
        /// </summary>
        public ServiceFeeDisplay ShowServiceFee
        {
            get { return showServiceFee; }
            set { showServiceFee = value; }
        }
        /// <summary>
        /// LastName.FirstName.Title spaces replaced with '.'.
        /// Used to identify a passenger in a booking.
        /// </summary>
        private string paxKey;
        /// <summary>
        /// Gets PaxKey. LastName.FirstName.Title with spaces replaced with '.'.
        /// </summary>
        public string PaxKey
        {
            get
            {
                if (paxKey == null || paxKey.Length == 0)
                {
                    StringBuilder key = new StringBuilder();
                    key.Append(paxLastName);
                    key.Append(".");
                    key.Append(paxFirstName);
                    if (title != null && title.Length > 0)
                    {
                        //key.Append(".");
                        key.Append(title);
                    }
                    paxKey = key.ToString().Replace(' ', '.').Trim().ToUpper();
                }
                paxKey = paxKey.Replace(".", "");
                return paxKey;
            }
        }

        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        private string title;
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                this.title = value;
            }
        }

        private List<SegmentPTCDetail> ptcDetail;

        public List<SegmentPTCDetail> PtcDetail
        {
            get { return ptcDetail; }
            set { ptcDetail = value; }
        }
        private string ticketAdvisory;

        public string TicketAdvisory
        {
            get
            {
                return ticketAdvisory;
            }
            set
            {
                ticketAdvisory = value;
            }
        }

        /// <summary>
        /// Hold the stock type eg B,O,C,X,S
        /// </summary>
        private string stockType;
        ///
        /// Property to hold the stocktype
        ///
        public string StockType
        {
            get { return stockType; }
            set { stockType = value; }
        }
        /// <summary>
        /// Field to Hold Ticket type
        /// </summary>

        private string ticketType;
        /// <summary>
        /// Property for ticketType field
        /// </summary>
        public string TicketType
        {
            get { return ticketType; }
            set { ticketType = value; }
        }
        /// <summary>
        /// Field to Hold supplier id
        /// </summary>
        private int supplierId;
        /// <summary>
        /// Property to Hold supplier id
        /// </summary>
        public int SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private SupplierRemmitance supplierRemmitance;
        public SupplierRemmitance SupplierRemmitance
        {
            get { return supplierRemmitance; }
            set { supplierRemmitance = value; }
        }
        /// <summary>
        /// Field to Hold corporateCode
        /// </summary>
        private string corporateCode;
        /// <summary>
        /// Property to Hold corporateCode
        /// </summary>
        public string CorporateCode
        {
            get { return corporateCode; }
            set { corporateCode = value; }
        }
        #endregion

        /// <summary>
        /// Saves a Ticket. Makes a new entry if ticketId does not exist and Updates entry if already exists.
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("Ticket.Save entered : flightId = " + flightId + ", paxId = " + paxId);
            if (paxId <= 0)
            {
                throw new ArgumentException("paxId must be a positive integer", "paxId");
            }
            if (flightId <= 0)
            {
                throw new ArgumentException("flightId must be a positive integer", "flightId");
            }
            if (ticketNumber == null || ticketNumber.Length == 0)
            {
                throw new ArgumentException("ticket Number must have a value", "ticketNumber");
            }
            if (paxLastName == null || paxLastName.Length == 0)
            {
                throw new ArgumentException("paxLastName must have a value", "paxLastName");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive integer value", "createdBy");
            }
            GetAdvisory();
            SqlParameter[] paramList = new SqlParameter[31];
            if (ticketId > 0)
            {
                //Trace.TraceInformation("Ticket.Save Update Mode");
                paramList[0] = new SqlParameter("@ticketId", ticketId);
            }
            else
            {
                //Trace.TraceInformation("Ticket.Save Add new mode");
                paramList[0] = new SqlParameter("@ticketId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
            }
            paramList[1] = new SqlParameter("@paxId", paxId);
            paramList[2] = new SqlParameter("@flightId", flightId);
            paramList[3] = new SqlParameter("@ticketNumber", ticketNumber);
            paramList[4] = new SqlParameter("@conjunctionNo", conjunctionNumber);
            paramList[5] = new SqlParameter("@title", title);
            paramList[6] = new SqlParameter("@paxFirstName", paxFirstName);
            paramList[7] = new SqlParameter("@paxLastName", paxLastName);
            paramList[8] = new SqlParameter("@paxType", FlightPassenger.GetPTC(paxType));
            paramList[9] = new SqlParameter("@issueInExchange", issueInExchange);
            paramList[10] = new SqlParameter("@originalIssue", originalIssue);
            paramList[11] = new SqlParameter("@tourCode", tourCode);
            paramList[12] = new SqlParameter("@fop", fop);
            paramList[13] = new SqlParameter("@fareCalculation", fareCalculation);
            paramList[14] = new SqlParameter("@ticketDesignator", ticketDesignator);
            paramList[15] = new SqlParameter("@endorsement", endorsement);
            paramList[16] = new SqlParameter("@validatingAirline", validatingAriline);
            paramList[17] = new SqlParameter("@issueDate", issueDate);
            paramList[18] = new SqlParameter("@status", status);
            paramList[19] = new SqlParameter("@priceId", Price.PriceId);
            paramList[20] = new SqlParameter("@eTicket", eTikcet);
            paramList[21] = new SqlParameter("@serviceFee", serviceFee);
            paramList[22] = new SqlParameter("@showServiceFee", (int)showServiceFee);
            paramList[23] = new SqlParameter("@ticketAdvisory", ticketAdvisory);
            paramList[24] = new SqlParameter("@savedBy", createdBy);
            if (remarks == null || remarks.Length == 0)
            {
                paramList[25] = new SqlParameter("@remarks", DBNull.Value);
            }
            else
            {
                paramList[25] = new SqlParameter("remarks", remarks);
            }
            if (fareRule == null || fareRule.Length == 0)
            {
                paramList[26] = new SqlParameter("fareRule", DBNull.Value);
            }
            else
            {
                paramList[26] = new SqlParameter("fareRule", fareRule);
            }
            if (stockType == null)
            {
                paramList[27] = new SqlParameter("@stockType", DBNull.Value);
            }
            else
            {
                paramList[27] = new SqlParameter("@stockType", stockType);
            }

            if (ticketType == null)
            {
                paramList[28] = new SqlParameter("@ticketType", DBNull.Value);
            }
            else
            {
                paramList[28] = new SqlParameter("@ticketType", ticketType);
            }
            paramList[29] = new SqlParameter("@supplierId", supplierId);

            if (corporateCode == null)
            {
                paramList[30] = new SqlParameter("@corporateCode ", DBNull.Value);
            }
            else
            {
                paramList[30] = new SqlParameter("@corporateCode ", corporateCode);
            }
            if (ticketId > 0)
            {
                // updated section
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateTicket, paramList);
                // While updating ticket, all the taxbreakup is deleted first then added again.
                SqlParameter[] pList = new SqlParameter[1];
                pList[0] = new SqlParameter("@ticketId", ticketId);
                int rowsDeleted = DBGateway.ExecuteNonQuerySP(SPNames.DeleteTaxBreakupForTicket, pList);
            }
            else
            {
                // insert section
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddTicket, paramList);
                ticketId = (int)paramList[0].Value;
                if (rowsAffected < 0 && ticketId > 0)
                {
                    throw new BookingEngineException("Ticket for this passenger is already created.");
                }
            }
            if (taxBreakup != null)
            {
                foreach (KeyValuePair<string, decimal> tax in taxBreakup)
                {
                    paramList = new SqlParameter[4];
                    paramList[0] = new SqlParameter("@ticketId", ticketId);
                    paramList[1] = new SqlParameter("@taxValue", tax.Value);
                    paramList[2] = new SqlParameter("@taxType", tax.Key);
                    paramList[3] = new SqlParameter("@savedBy", createdBy);

                    DBGateway.ExecuteNonQuerySP(SPNames.AddTicketTaxBreakup, paramList);
                }
                //Trace.TraceInformation("Ticket.Save exiting");
            }
        }

        /// <summary>
        /// Loads a tikcet corresponding to a given ticketId.
        /// </summary>
        /// <param name="ticketId">ticketId of the ticket to be loaded.</param>
        public void Load(int ticketId)
        {
            this.ticketId = ticketId;
            Load();
        }

        /// <summary>
        /// Loads a ticket. TicketId has to set before calling Load().
        /// </summary>
        public void Load()
        {
            //Trace.TraceInformation("Ticket.Load entered : ticketID = " + ticketId);
            if (ticketId <= 0)
            {
                throw new ArgumentException("ticketId must have a positive non-zero value");
            }
           // SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.LoadTicket, paramList, connection);
            using (DataTable dtTicket = DBGateway.FillDataTableSP(SPNames.LoadTicket, paramList))
            //TODO: read data for ticket. this part of code is also same in ReadList method. make it a new method.
            {
                if (dtTicket != null && dtTicket.Rows.Count > 0)
                {
                    foreach (DataRow dataReader in dtTicket.Rows)
                    {
                        ticketId = Convert.ToInt32(dataReader["ticketId"]);
                        paxId = Convert.ToInt32(dataReader["paxId"]);
                        flightId = Convert.ToInt32(dataReader["flightId"]);
                        ticketNumber = Convert.ToString(dataReader["ticketNumber"]);
                        if (dataReader["conjunctionNo"] == DBNull.Value)
                        {
                            conjunctionNumber = string.Empty;
                        }
                        else
                        {
                            conjunctionNumber =Convert.ToString(dataReader["conjunctionNo"]);
                        }
                        if (dataReader["title"] == DBNull.Value)
                        {
                            title = string.Empty;
                        }
                        else
                        {
                            title =Convert.ToString(dataReader["title"]);
                        }
                        paxFirstName = Convert.ToString(dataReader["paxFirstName"]);
                        paxLastName = Convert.ToString(dataReader["paxLastName"]);
                        if (dataReader["remarks"] != DBNull.Value)
                        {
                            remarks = Convert.ToString(dataReader["remarks"]);
                        }
                        if (dataReader["fareRule"] != DBNull.Value)
                        {
                            fareRule = Convert.ToString(dataReader["fareRule"]);
                        }
                        paxType = FlightPassenger.GetPassengerType(Convert.ToString(dataReader["paxType"]));
                        if (dataReader["issueInExchange"] == DBNull.Value)
                        {
                            issueInExchange = string.Empty;
                        }
                        else
                        {
                            issueInExchange = Convert.ToString(dataReader["issueInExchange"]);
                        }
                        if (dataReader["originalIssue"] == DBNull.Value)
                        {
                            originalIssue = string.Empty;
                        }
                        else
                        {
                            originalIssue = Convert.ToString(dataReader["originalIssue"]);
                        }
                        if (dataReader["tourCode"] != DBNull.Value)
                        {
                            tourCode = Convert.ToString(dataReader["tourCode"]);
                        }
                        else
                        {
                            tourCode = string.Empty;
                        }
                        if (dataReader["FOP"] == DBNull.Value)
                        {
                            fop = string.Empty;
                        }
                        else
                        {
                            fop = (string)dataReader["FOP"];
                        }
                        if (dataReader["fareCalculation"] == DBNull.Value)
                        {
                            fareCalculation = string.Empty;
                        }
                        else
                        {
                            fareCalculation = Convert.ToString(dataReader["fareCalculation"]);
                        }
                        if (dataReader["ticketDesignator"] == DBNull.Value)
                        {
                            ticketDesignator = string.Empty;
                        }
                        else
                        {
                            ticketDesignator = Convert.ToString(dataReader["ticketDesignator"]);
                        }
                        if (dataReader["endorsement"] == DBNull.Value)
                        {
                            endorsement = string.Empty;
                        }
                        else
                        {
                            endorsement = Convert.ToString(dataReader["endorsement"]);
                        }
                        if (dataReader["ticketAdvisory"] == DBNull.Value)
                        {
                            ticketAdvisory = string.Empty;
                        }
                        else
                        {
                            ticketAdvisory = Convert.ToString(dataReader["ticketAdvisory"]);
                        }
                        validatingAriline = Convert.ToString(dataReader["validatingAirline"]);
                        issueDate =Convert.ToDateTime(dataReader["issueDate"]);
                        status = Convert.ToString(dataReader["status"]);
                        price = new PriceAccounts();
                        price.Load(Convert.ToInt32(dataReader["priceId"]));
                        if (dataReader["eTicket"] != DBNull.Value)
                        {
                            eTikcet =Convert.ToBoolean(dataReader["eTicket"]);
                        }
                        //Load Supplier Code
                        if (dataReader["supplierId"] != DBNull.Value)
                        {
                            supplierId =Convert.ToInt32(dataReader["supplierId"]);
                        }
                        serviceFee = Convert.ToDecimal(dataReader["serviceFee"]);
                        showServiceFee = (ServiceFeeDisplay)Enum.Parse(typeof(ServiceFeeDisplay), Convert.ToString(dataReader["showServiceFee"]));
                        if (dataReader["corporateCode"] == DBNull.Value)
                        {
                            corporateCode = string.Empty;
                        }
                        else
                        {
                            corporateCode = Convert.ToString(dataReader["corporateCode"]);
                        }
                        if (dataReader["stockType"] == DBNull.Value)
                        {
                            stockType = string.Empty;
                        }
                        else
                        {
                            stockType = Convert.ToString(dataReader["stockType"]);
                        }
                        ptcDetail = SegmentPTCDetail.GetSegmentPTCDetail(flightId);
                        createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                        createdBy =Convert.ToInt32(dataReader["createdBy"]);
                        lastModifiedOn =Convert.ToDateTime(dataReader["lastModifiedOn"]);
                        lastModifiedBy =Convert.ToInt32(dataReader["lastModifiedBy"]);
                        //dataReader.Close();
                        //connection.Close();
                        // Loading TaxBreakup for the ticket.
                        LoadTaxBreakup();
                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("ticketId " + ticketId + " does not exist in database");
                }
            }
        }

        /// <summary>
        /// Loads a ticket for credit note. TicketId has to set before calling Load().
        /// </summary>
        public void LoadTicketForCreditNote(int ticketId)
        {
            //Trace.TraceInformation("Ticket.LoadTicketForCreditNote entered : ticketID = " + ticketId);
            if (ticketId <= 0)
            {
                throw new ArgumentException("ticketId must have a positive non-zero value");
            }

            try
            {
                this.ticketId = ticketId;
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ticketId", ticketId);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.LoadTicket, paramList, connection);
                //TODO: read data for ticket. this part of code is also same in ReadList method. make it a new method.
                if (dataReader.Read())
                {
                    ticketId = (int)dataReader["ticketId"];
                    paxId = (int)dataReader["paxId"];
                    flightId = (int)dataReader["flightId"];
                    ticketNumber = (string)dataReader["ticketNumber"];
                    if (dataReader["conjunctionNo"] == DBNull.Value)
                    {
                        conjunctionNumber = string.Empty;
                    }
                    else
                    {
                        conjunctionNumber = (string)dataReader["conjunctionNo"];
                    }
                    if (dataReader["title"] == DBNull.Value)
                    {
                        title = string.Empty;
                    }
                    else
                    {
                        title = dataReader["title"].ToString();
                    }
                    paxFirstName = (string)dataReader["paxFirstName"];
                    paxLastName = (string)dataReader["paxLastName"];
                    if (dataReader["remarks"] != DBNull.Value)
                    {
                        remarks = (string)dataReader["remarks"];
                    }
                    if (dataReader["fareRule"] != DBNull.Value)
                    {
                        fareRule = (string)dataReader["fareRule"];
                    }
                    paxType = FlightPassenger.GetPassengerType((string)dataReader["paxType"]);
                    if (dataReader["issueInExchange"] == DBNull.Value)
                    {
                        issueInExchange = string.Empty;
                    }
                    else
                    {
                        issueInExchange = (string)dataReader["issueInExchange"];
                    }
                    if (dataReader["originalIssue"] == DBNull.Value)
                    {
                        originalIssue = string.Empty;
                    }
                    else
                    {
                        originalIssue = (string)dataReader["originalIssue"];
                    }
                    if (dataReader["tourCode"] != DBNull.Value)
                    {
                        tourCode = (string)dataReader["tourCode"];
                    }
                    else
                    {
                        tourCode = string.Empty;
                    }
                    if (dataReader["FOP"] == DBNull.Value)
                    {
                        fop = string.Empty;
                    }
                    else
                    {
                        fop = (string)dataReader["FOP"];
                    }
                    if (dataReader["fareCalculation"] == DBNull.Value)
                    {
                        fareCalculation = string.Empty;
                    }
                    else
                    {
                        fareCalculation = (string)dataReader["fareCalculation"];
                    }
                    if (dataReader["ticketDesignator"] == DBNull.Value)
                    {
                        ticketDesignator = string.Empty;
                    }
                    else
                    {
                        ticketDesignator = (string)dataReader["ticketDesignator"];
                    }
                    if (dataReader["endorsement"] == DBNull.Value)
                    {
                        endorsement = string.Empty;
                    }
                    else
                    {
                        endorsement = (string)dataReader["endorsement"];
                    }
                    if (dataReader["ticketAdvisory"] == DBNull.Value)
                    {
                        ticketAdvisory = string.Empty;
                    }
                    else
                    {
                        ticketAdvisory = (string)dataReader["ticketAdvisory"];
                    }
                    validatingAriline = (string)dataReader["validatingAirline"];
                    issueDate = (DateTime)dataReader["issueDate"];
                    status = (string)dataReader["status"];
                    price = new PriceAccounts();
                    price.LoadForCreditNote(ticketId, (int)InvoiceItemTypeId.Ticketed);
                    if (dataReader["eTicket"] != DBNull.Value)
                    {
                        eTikcet = (bool)dataReader["eTicket"];
                    }
                    if (dataReader["supplierId"] != DBNull.Value)
                    {
                        supplierId = (int)dataReader["supplierId"];
                    }
                    serviceFee = Convert.ToDecimal(dataReader["serviceFee"]);
                    showServiceFee = (ServiceFeeDisplay)Enum.Parse(typeof(ServiceFeeDisplay), dataReader["showServiceFee"].ToString());
                    if (dataReader["corporateCode"] == DBNull.Value)
                    {
                        corporateCode = string.Empty;
                    }
                    else
                    {
                        corporateCode = (string)dataReader["corporateCode"];
                    }
                    ptcDetail = SegmentPTCDetail.GetSegmentPTCDetail(flightId);
                    createdOn = (DateTime)dataReader["createdOn"];
                    createdBy = (int)dataReader["createdBy"];
                    lastModifiedOn = (DateTime)dataReader["lastModifiedOn"];
                    lastModifiedBy = (int)dataReader["lastModifiedBy"];
                    dataReader.Close();
                    connection.Close();
                    // Loading TaxBreakup for the ticket.
                    LoadTaxBreakup();
                }
                else
                {
                    dataReader.Close();
                    connection.Close();
                    throw new ArgumentException("ticketId " + ticketId + " does not exist in database");
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.High, 0, "Error during LoadTicketForCreditNote due to: " + ex.Message, "");
            }
        }

        /// <summary>
        /// Loads tax breakup for a ticket.
        /// </summary>
        private void LoadTaxBreakup()
        {
            //Trace.TraceInformation("Ticket.LoadTaxBreakup entered: ticketId = " + ticketId);
            if (ticketId <= 0)
            {
                throw new ArgumentException("ticketId must have a positive non-zero value");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.LoadTaxBreakup, paramList, connection);
            string tempTaxType;
            decimal tempTaxValue;
            List<KeyValuePair<string, decimal>> tempTaxBreakup = new List<KeyValuePair<string, decimal>>();
            using (DataTable dtTax = DBGateway.FillDataTableSP(SPNames.LoadTaxBreakup, paramList))
            {
                if (dtTax != null && dtTax.Rows.Count > 0)
                {
                    foreach(DataRow dataReader in dtTax.Rows)
                    {
                        tempTaxType =Convert.ToString(dataReader["taxType"]);
                        tempTaxValue = Convert.ToDecimal(dataReader["taxValue"]);
                        tempTaxBreakup.Add(new KeyValuePair<string, decimal>(tempTaxType, tempTaxValue));
                    }
                }
            }
            //dataReader.Close();
            //connection.Close();
            taxBreakup = tempTaxBreakup;
            //Trace.TraceInformation("Ticket.LoadTaxBreakup exiting: taxBreakup.Count = " + taxBreakup.Count);
        }

        /// <summary>
        /// Gets a List of tickets for a given flight.
        /// </summary>
        /// <param name="flightId">flightId of the flight for which tickets are needed.</param>
        /// <returns>List of Ticket</returns>
        public static List<Ticket> GetTicketList(int flightId)
        {
            //Trace.TraceInformation("Ticket.GetTicketList entered : flightId = " + flightId);
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTicketListByFlightId, paramList, connection);
             List<Ticket> ticketList=new List<Ticket>() ;
            using (DataTable dtTicket = DBGateway.FillDataTableSP(SPNames.GetTicketListByFlightId, paramList))
            {
                try
                {
                    if (dtTicket != null && dtTicket.Rows.Count > 0)
                    {
                        //List<Ticket> ticketList = ReadList(dataReader);
                        ticketList = ReadList(dtTicket);
                    }
                }
                catch { throw; }
            }
            //dataReader.Close();
            //connection.Close();
            //Trace.TraceInformation("Ticket.GetTicketList exiting : ticketList.Count = " + ticketList.Count);
            return ticketList;
        }

        /// <summary>
        /// Reads the list of Ticket from dataReader.
        /// </summary>
        /// <param name="dataReader">SqlDataReader to read the Ticket information</param>
        /// <returns>List of Tickets</returns>
        private static List<Ticket> ReadList(DataTable dtTicket)
        {
            //Trace.TraceInformation("Ticket.ReadList entered");
            List<Ticket> ticketList = new List<Ticket>();
            foreach(DataRow dataReader in dtTicket.Rows)
            {
                Ticket tempTicket = new Ticket();
                tempTicket.ticketId =Convert.ToInt32(dataReader["ticketId"]);
                tempTicket.paxId = Convert.ToInt32(dataReader["paxId"]);
                tempTicket.flightId = Convert.ToInt32(dataReader["flightId"]);
                tempTicket.ticketNumber = Convert.ToString(dataReader["ticketNumber"]);
                if (dataReader["conjunctionNo"] == DBNull.Value)
                {
                    tempTicket.conjunctionNumber = string.Empty;
                }
                else
                {
                    tempTicket.conjunctionNumber = Convert.ToString(dataReader["conjunctionNo"]);
                }
                if (dataReader["title"] == DBNull.Value)
                {
                    tempTicket.title = string.Empty;
                }
                else
                {
                    tempTicket.title = Convert.ToString(dataReader["title"]);
                }
                tempTicket.paxFirstName = Convert.ToString(dataReader["paxFirstName"]);
                tempTicket.paxLastName = Convert.ToString(dataReader["paxLastName"]);
                tempTicket.paxType = FlightPassenger.GetPassengerType(Convert.ToString(dataReader["paxType"]));
                if (dataReader["issueInExchange"] == DBNull.Value)
                {
                    tempTicket.issueInExchange = string.Empty;
                }
                else
                {
                    tempTicket.issueInExchange = Convert.ToString(dataReader["issueInExchange"]);
                }
                if (dataReader["originalIssue"] == DBNull.Value)
                {
                    tempTicket.originalIssue = string.Empty;
                }
                else
                {
                    tempTicket.originalIssue = Convert.ToString(dataReader["originalIssue"]);
                }
                if (dataReader["FOP"] == DBNull.Value)
                {
                    tempTicket.fop = string.Empty;
                }
                else
                {
                    tempTicket.fop = Convert.ToString(dataReader["FOP"]);
                }
                if (dataReader["fareCalculation"] == DBNull.Value)
                {
                    tempTicket.fareCalculation = string.Empty;
                }
                else
                {
                    tempTicket.fareCalculation = Convert.ToString(dataReader["fareCalculation"]);
                }
                if (dataReader["ticketDesignator"] == DBNull.Value)
                {
                    tempTicket.ticketDesignator = string.Empty;
                }
                else
                {
                    tempTicket.ticketDesignator = Convert.ToString(dataReader["ticketDesignator"]);
                }
                if (dataReader["endorsement"] == DBNull.Value)
                {
                    tempTicket.endorsement = string.Empty;
                }
                else
                {
                    tempTicket.endorsement = Convert.ToString(dataReader["endorsement"]);
                }
                if (dataReader["remarks"] == DBNull.Value)
                {
                    tempTicket.remarks = string.Empty;
                }
                else
                {
                    tempTicket.remarks = Convert.ToString(dataReader["remarks"]);
                }
                if (dataReader["tourCode"] == DBNull.Value)
                {
                    tempTicket.tourCode = string.Empty;
                }
                else
                {
                    tempTicket.tourCode = Convert.ToString(dataReader["tourCode"]);
                }
                if (dataReader["ticketAdvisory"] == DBNull.Value)
                {
                    tempTicket.ticketAdvisory = string.Empty;
                }
                else
                {
                    tempTicket.ticketAdvisory = Convert.ToString(dataReader["ticketAdvisory"]);
                }
                tempTicket.validatingAriline = Convert.ToString(dataReader["validatingAirline"]);
                tempTicket.issueDate = Convert.ToDateTime(dataReader["issueDate"]);
                tempTicket.serviceFee = Convert.ToDecimal(dataReader["serviceFee"]);
                tempTicket.status = Convert.ToString(dataReader["status"]);
                tempTicket.price = new PriceAccounts();
                tempTicket.price.Load(Convert.ToInt32(dataReader["priceId"]));
                tempTicket.LoadTaxBreakup();
                if (dataReader["eTicket"] != DBNull.Value)
                {
                    tempTicket.eTikcet = Convert.ToBoolean(dataReader["eTicket"]);
                }
                if (dataReader["supplierId"] != DBNull.Value)
                {
                    tempTicket.supplierId = Convert.ToInt32(dataReader["supplierId"]);
                }
                if (dataReader["TicketType"] != DBNull.Value)
                {
                    tempTicket.ticketType = Convert.ToString(dataReader["TicketType"]);
                }
                if (dataReader["corporateCode"] != DBNull.Value)
                {
                    tempTicket.corporateCode = Convert.ToString(dataReader["corporateCode"]);
                }
                else
                {
                    tempTicket.corporateCode = string.Empty;
                }

                if (dataReader["stockType"] != DBNull.Value)
                {
                    tempTicket.stockType = Convert.ToString(dataReader["stockType"]);
                }
                else
                {
                    tempTicket.stockType = string.Empty;
                }
                tempTicket.createdOn = (DateTime)dataReader["createdOn"];
                tempTicket.createdBy = Convert.ToInt32(dataReader["createdBy"]);
                tempTicket.lastModifiedOn = (DateTime)dataReader["lastModifiedOn"];
                tempTicket.lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                tempTicket.ptcDetail = SegmentPTCDetail.GetSegmentPTCDetail(tempTicket.flightId);
                ticketList.Add(tempTicket);
            }
            return ticketList;
        }

        public static int GetAgencyIdForTicket(int ticketId)
        {
            int agencyId = 0;
            //Trace.TraceInformation("Ticket.GetAgencyIdForTicket entered : ticketId = " + ticketId);
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAgencyIdForTicket, paramList, connection);
            using (DataTable dtAgency = DBGateway.FillDataTableSP(SPNames.GetAgencyIdForTicket, paramList))
            {
                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    DataRow dataReader = dtAgency.Rows[0];
                    if (dataReader !=null && dataReader["agencyId"] !=DBNull.Value)
                    {
                        agencyId = Convert.ToInt32(dataReader["agencyId"]);
                    }
                }
            }
            //dataReader.Close();
            //connection.Close();
            return agencyId;
        }

        public static int GetTicketIdForPax(int paxId)
        {
            int ticketId = 0;
            //Trace.TraceInformation("Ticket.GetTicketIdForPax entered : paxId = " + paxId);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTicketIdForPax, paramList, connection);
            if (dataReader.Read())
            {
                ticketId = Convert.ToInt32(dataReader["ticketId"]);
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("Ticket.GetTicketIdForPax entered : ticketId = " + ticketId);
            return ticketId;
        }

        /// <summary>
        /// Gets a List of Price Id of Tickets for a given flight.
        /// </summary>  
        /// <param name="flightId">flightId of the flight for which priceId are needed.</param>
        /// <returns>List of PriceId</returns>
        public static List<int> GetPriceIdList(int flightId)
        {
            //Trace.TraceInformation("Ticket.GetPriceIdList entered : flightId = " + flightId);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetPriceIdByFlightId, paramList, connection);
            List<int> priceId = new List<int>();

            while (dataReader.Read())
            {
                if (dataReader["priceId"] != DBNull.Value)
                {
                    priceId.Add((int)dataReader["priceId"]);
                }
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("Ticket.GetPriceIdList exiting : priceId.Count = " + priceId.Count);
            return priceId;
        }

        /// <summary>
        /// To Get Price of a booking
        /// </summary>
        /// <param name="bookingId"> Unique Id of a booking</param>
        /// <returns></returns>
        public static decimal GetServiceFeeFromPriceId(int priceId)
        {
            decimal serviceFee = 0;
            List<int> priceIdList = new List<int>();
            if (priceId <= 0)
            {
                throw new ArgumentException("PriceId should be positive integer", "priceId");
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@priceId", priceId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetServiceFeeFromPriceId, paramList, connection);
            if (data.Read())
            {
                serviceFee = Convert.ToDecimal(data["serviceFee"]);
            }
            data.Close();
            connection.Close();
            return serviceFee;
        }

        /// <summary>
        /// Method will change the status of the ticket(e.g OK to Voided or Cancelled)
        /// </summary>
        /// <param name="ticketId">TicketId</param>
        /// <param name="status">Status to be setted</param>
        /// <param name="modifiedBy">Modified By</param>
        /// <returns>Rows Affected after seeting the status</returns>
        public static int SetTicketStatus(int ticketId, string status, int modifiedBy)
        {
            //Trace.TraceInformation("Ticket.SetTicketStatus entered : new status = " + status);
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            paramList[1] = new SqlParameter("@status", status);
            paramList[2] = new SqlParameter("@lastModifiedBy", modifiedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SetTicketStatus, paramList);
            //Trace.TraceInformation("Ticket.SetStatus exiting : rowsAffected = " + rowsAffected);
            return rowsAffected;
        }
        /// <summary>
        /// Method gets the ticket from the ticket number.This is specially for price so it'll not 
        /// guarantee for the same ticket
        /// </summary>
        /// <param name="ticketNumber">ticket number(13 digit validating carrier+ticketNumber)</param>
        /// <param name="paxType">pax Type required when there are two person having same ticket number
        /// (mostly in LCC</param>
        /// <returns>Ticket of the corresponding ticket number</returns>
        public static Ticket GetTicketFromTicketNumber(string ticketNumber, string paxType)
        {
            //Trace.TraceInformation("Ticket.GetTicketFromTicketNo entered : new ticket number = " + ticketNumber);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@ticketNumber", ticketNumber);
            paramList[1] = new SqlParameter("@paxType", paxType);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTicketFromTicketNo, paramList, connection);
            Ticket tempTicket = new Ticket();
            if (dataReader.Read())
            {
                int priceId = 0;
                tempTicket.ticketId = (int)dataReader["ticketId"];
                tempTicket.paxId = (int)dataReader["paxId"];
                tempTicket.flightId = (int)dataReader["flightId"];
                tempTicket.ticketNumber = (string)dataReader["ticketNumber"];
                if (dataReader["conjunctionNo"] == DBNull.Value)
                {
                    tempTicket.conjunctionNumber = string.Empty;
                }
                else
                {
                    tempTicket.conjunctionNumber = (string)dataReader["conjunctionNo"];
                }
                if (dataReader["title"] == DBNull.Value)
                {
                    tempTicket.title = string.Empty;
                }
                else
                {
                    tempTicket.title = dataReader["title"].ToString();
                }
                tempTicket.paxFirstName = (string)dataReader["paxFirstName"];
                tempTicket.paxLastName = (string)dataReader["paxLastName"];
                tempTicket.paxType = FlightPassenger.GetPassengerType((string)dataReader["paxType"]);
                if (dataReader["issueInExchange"] == DBNull.Value)
                {
                    tempTicket.issueInExchange = string.Empty;
                }
                else
                {
                    tempTicket.issueInExchange = (string)dataReader["issueInExchange"];
                }
                if (dataReader["originalIssue"] == DBNull.Value)
                {
                    tempTicket.originalIssue = string.Empty;
                }
                else
                {
                    tempTicket.originalIssue = (string)dataReader["originalIssue"];
                }
                if (dataReader["FOP"] == DBNull.Value)
                {
                    tempTicket.fop = string.Empty;
                }
                else
                {
                    tempTicket.fop = (string)dataReader["FOP"];
                }
                if (dataReader["fareCalculation"] == DBNull.Value)
                {
                    tempTicket.fareCalculation = string.Empty;
                }
                else
                {
                    tempTicket.fareCalculation = (string)dataReader["fareCalculation"];
                }
                if (dataReader["ticketDesignator"] == DBNull.Value)
                {
                    tempTicket.ticketDesignator = string.Empty;
                }
                else
                {
                    tempTicket.ticketDesignator = (string)dataReader["ticketDesignator"];
                }
                if (dataReader["endorsement"] == DBNull.Value)
                {
                    tempTicket.endorsement = string.Empty;
                }
                else
                {
                    tempTicket.endorsement = (string)dataReader["endorsement"];
                }
                if (dataReader["ticketAdvisory"] == DBNull.Value)
                {
                    tempTicket.ticketAdvisory = string.Empty;
                }
                else
                {
                    tempTicket.ticketAdvisory = (string)dataReader["ticketAdvisory"];
                }
                tempTicket.validatingAriline = (string)dataReader["validatingAirline"];
                tempTicket.issueDate = (DateTime)dataReader["issueDate"];
                tempTicket.status = (string)dataReader["status"];
                priceId = Convert.ToInt32(dataReader["priceId"]);
                if (dataReader["eTicket"] != DBNull.Value)
                {
                    tempTicket.eTikcet = (bool)dataReader["eTicket"];
                }
                if (dataReader["supplierId"] != DBNull.Value)
                {
                    tempTicket.supplierId = (int)dataReader["supplierId"];
                }
                if (dataReader["stockType"] != DBNull.Value)
                {
                    tempTicket.stockType = dataReader["stockType"].ToString();
                }
                tempTicket.createdOn = (DateTime)dataReader["createdOn"];
                tempTicket.createdBy = (int)dataReader["createdBy"];
                tempTicket.lastModifiedOn = (DateTime)dataReader["lastModifiedOn"];
                tempTicket.lastModifiedBy = (int)dataReader["lastModifiedBy"];
                tempTicket.price = new PriceAccounts();
                tempTicket.price.Load(priceId);
            }
            dataReader.Close();
            connection.Close();
            return tempTicket;
        }
        private void GetAdvisory()
        {
            //Trace.TraceInformation("Ticket.GetAdvisory entered flightId=" + flightId);
            bool isDomestic = FlightItinerary.IsDomesticFlight(flightId);
            Advisory advisory = new Advisory();
            if (isDomestic)
            {
                advisory.Load("Domestic");
            }
            else
            {
                advisory.Load("International");
            }
            ticketAdvisory = advisory.AdvisoryText;
            //Trace.TraceInformation("Ticket.GetAdvisory entered ticketAdvisory=" + ticketAdvisory);
        }
        public static Dictionary<string, string> GetTicketNoAndSource(int ticketId)
        {
            Dictionary<string, string> paramGroup = new Dictionary<string, string>();
            string source = string.Empty;
            string ticketNumber = string.Empty;
            string[] ticketNo = new string[2];
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTicketNoAndSource, paramList, connection);
            if (dataReader.Read())
            {
                source = Convert.ToString(dataReader["BookingSource"]);
                ticketNo[0] = (string)dataReader["ValidatingAirline"];
                ticketNo[1] = (string)dataReader["TicketNumber"];
                ticketNumber = ticketNo[0] + ticketNo[1];
            }
            paramGroup.Add("source", source);
            paramGroup.Add("ticketNumber", ticketNumber);
            dataReader.Close();
            connection.Close();
            return paramGroup;
        }

        public static DataTable GetSelectedColumnsFromTicketAgainstFlightId(int flightId)
        {
            //Trace.TraceInformation("Ticket.GetSelectedColumnsFromTicketAgainstFlightId entered:" + flightId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            DataTable table = DBGateway.FillDataTableSP(SPNames.GetSelectedColumnsFromTicketAgainstFlightId, paramList);
            //Trace.TraceInformation("Ticket.GetSelectedColumnsFromTicketAgainstFlightId exit, rows return:" + table.Rows.Count);
            return table;
        }

        public static void RefreshTicketData(Ticket[] ticket)
        {
            //Trace.TraceInformation("Ticket.RefreshTicketData entered");

            SqlConnection conn = DBGateway.GetConnection();
            SqlDataAdapter adapter = null;
            DataTable table = new DataTable();

            try
            {
                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@flightId", ticket[0].flightId);

                SqlCommand cmd = new SqlCommand(SPNames.GetSelectedColumnsFromTicketAgainstFlightId, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(paramlist);
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(table);
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    table.Rows[i]["ticketNumber"] = ticket[i].ticketNumber;
                    table.Rows[i]["status"] = ticket[i].status;
                    table.Rows[0]["lastModifiedOn"] = DateTime.UtcNow;
                    table.Rows[0]["lastModifiedBy"] = ticket[i].lastModifiedBy;
                }

                SqlCommandBuilder command = new SqlCommandBuilder(adapter);
                adapter.Update(table);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.ChangeRequest, CT.Core.Severity.High, 0, "Error: while updating Ticket in case of reissuance.| " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            }
            finally
            {
                conn.Close();
                adapter.Dispose();
                table.Dispose();
            }
            //Trace.TraceInformation("Itinerary.RefreshItineraryData exited");
        }

        public static string GetItineraryString(FlightItinerary flightItinerary)
        {
            string itenaryString = string.Empty;
            if (flightItinerary.Segments.Length > 0)
            {
                itenaryString = flightItinerary.Segments[0].Origin.AirportCode;
                for (int k = 0; k < flightItinerary.Segments.Length; k++)
                {
                    itenaryString = itenaryString + "-" + flightItinerary.Segments[k].Destination.AirportCode;
                }
            }
            return itenaryString;
        }

        public static void UpdateSupplierAgainstTicket(String ticketNumber, int supplierId)
        {
            //Trace.TraceInformation("Ticket.UpdateSupplierAgainstTicket entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@ticketNumber", ticketNumber);
            paramList[1] = new SqlParameter("@supplierId", supplierId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateSupplierAgainstTicket, paramList);
            //Trace.TraceInformation("Ticket.SetStatus exiting : rowsAffected = " + rowsAffected);

        }

        /// <summary>
        /// Update TicketNumber from PKFares TicketNumPush service
        /// </summary>
        public int UpdateTicketNumber()
        {
            int counter = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_TicketId", ticketId);
                paramList[1] = new SqlParameter("@P_TicketNumber", ticketNumber);
                counter = DBGateway.ExecuteNonQuerySP("usp_UpdateTicketNumber", paramList);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return counter;
        }

        /// <summary>
        /// This method retrieves Ticket names for TicketNumPush service
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public static DataTable GetTicketsForPKFare(int flightId)
        {
            DataTable dtTickets = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_FlightId", flightId);
                dtTickets = DBGateway.FillDataTableSP("usp_GetTicketsForPKFare", paramList);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return dtTickets;
        }

        public void UpdateTicketByPax()
        {
            if (paxId <= 0)
            {
                throw new ArgumentException("paxId must be a positive integer", "paxId");
            }
            if (flightId <= 0)
            {
                throw new ArgumentException("flightId must be a positive integer", "flightId");
            }
            if (ticketNumber == null || ticketNumber.Length == 0)
            {
                throw new ArgumentException("ticket Number must have a value", "ticketNumber");
            }
            if (paxLastName == null || paxLastName.Length == 0)
            {
                throw new ArgumentException("paxLastName must have a value", "paxLastName");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive integer value", "createdBy");
            }
            GetAdvisory();
            SqlParameter[] paramList = new SqlParameter[31];
            if (ticketId > 0)
            {
                //Trace.TraceInformation("Ticket.Save Update Mode");
                paramList[0] = new SqlParameter("@ticketId", ticketId);
            }
            else
            {
                //Trace.TraceInformation("Ticket.Save Add new mode");
                paramList[0] = new SqlParameter("@ticketId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
            }
            paramList[1] = new SqlParameter("@paxId", paxId);
            paramList[2] = new SqlParameter("@flightId", flightId);
            paramList[3] = new SqlParameter("@ticketNumber", ticketNumber);
            paramList[4] = new SqlParameter("@conjunctionNo", conjunctionNumber);
            paramList[5] = new SqlParameter("@title", title);
            paramList[6] = new SqlParameter("@paxFirstName", paxFirstName);
            paramList[7] = new SqlParameter("@paxLastName", paxLastName);
            paramList[8] = new SqlParameter("@paxType", FlightPassenger.GetPTC(paxType));
            paramList[9] = new SqlParameter("@issueInExchange", issueInExchange);
            paramList[10] = new SqlParameter("@originalIssue", originalIssue);
            paramList[11] = new SqlParameter("@tourCode", tourCode);
            paramList[12] = new SqlParameter("@fop", fop);
            paramList[13] = new SqlParameter("@fareCalculation", fareCalculation);
            paramList[14] = new SqlParameter("@ticketDesignator", ticketDesignator);
            paramList[15] = new SqlParameter("@endorsement", endorsement);
            paramList[16] = new SqlParameter("@validatingAirline", validatingAriline);
            paramList[17] = new SqlParameter("@issueDate", issueDate);
            paramList[18] = new SqlParameter("@status", status);
            paramList[19] = new SqlParameter("@priceId", Price.PriceId);
            paramList[20] = new SqlParameter("@eTicket", eTikcet);
            paramList[21] = new SqlParameter("@serviceFee", serviceFee);
            paramList[22] = new SqlParameter("@showServiceFee", (int)showServiceFee);
            paramList[23] = new SqlParameter("@ticketAdvisory", ticketAdvisory);
            paramList[24] = new SqlParameter("@savedBy", createdBy);
            if (remarks == null || remarks.Length == 0)
            {
                paramList[25] = new SqlParameter("@remarks", DBNull.Value);
            }
            else
            {
                paramList[25] = new SqlParameter("remarks", remarks);
            }
            if (fareRule == null || fareRule.Length == 0)
            {
                paramList[26] = new SqlParameter("fareRule", DBNull.Value);
            }
            else
            {
                paramList[26] = new SqlParameter("fareRule", fareRule);
            }
            if (stockType == null)
            {
                paramList[27] = new SqlParameter("@stockType", DBNull.Value);
            }
            else
            {
                paramList[27] = new SqlParameter("@stockType", stockType);
            }

            if (ticketType == null)
            {
                paramList[28] = new SqlParameter("@ticketType", DBNull.Value);
            }
            else
            {
                paramList[28] = new SqlParameter("@ticketType", ticketType);
            }
            paramList[29] = new SqlParameter("@supplierId", supplierId);

            if (corporateCode == null)
            {
                paramList[30] = new SqlParameter("@corporateCode ", DBNull.Value);
            }
            else
            {
                paramList[30] = new SqlParameter("@corporateCode ", corporateCode);
            }
            
                // updated section
            int rowsAffected = DBGateway.ExecuteNonQuerySP("USP_UPDATETICKET", paramList);
            ticketId = (int)paramList[0].Value;
            // While updating ticket, all the taxbreakup is deleted first then added again.
            SqlParameter[] pList = new SqlParameter[1];
            pList[0] = new SqlParameter("@ticketId", ticketId);
            int rowsDeleted = DBGateway.ExecuteNonQuerySP("USP_DEACTIVATETAXBREAKUP", pList);
           
            
            if (taxBreakup != null)
            {
                foreach (KeyValuePair<string, decimal> tax in taxBreakup)
                {
                    paramList = new SqlParameter[4];
                    paramList[0] = new SqlParameter("@ticketId", ticketId);
                    paramList[1] = new SqlParameter("@taxValue", tax.Value);
                    paramList[2] = new SqlParameter("@taxType", tax.Key);
                    paramList[3] = new SqlParameter("@savedBy", createdBy);

                    DBGateway.ExecuteNonQuerySP(SPNames.AddTicketTaxBreakup, paramList);
                }
                //Trace.TraceInformation("Ticket.Save exiting");
            }
        }
    }
}
