﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CT.BookingEngine
{
    [Serializable]
    public class GSTTaxDetail
    {
        #region variables
        int _detId;
        long _priceId;
        string _taxCode;
        decimal _taxValue;
        decimal _taxAmount;
        string _status;
        long _createdBy;
        int _productID=0;
        #endregion

        #region properities

        public int ProductID
        {
        get { return _productID; }
            set { _productID = value; }
        }
        public int DetId
        {
            get
            {
                return _detId;
            }

            set
            {
                _detId = value;
            }
        }

        public long PriceId
        {
            get
            {
                return _priceId;
            }

            set
            {
                _priceId = value;
            }
        }

        public string TaxCode
        {
            get
            {
                return _taxCode;
            }

            set
            {
                _taxCode = value;
            }
        }

        public decimal TaxValue
        {
            get
            {
                return _taxValue;
            }

            set
            {
                _taxValue = value;
            }
        }

        public decimal TaxAmount
        {
            get
            {
                return _taxAmount;
            }

            set
            {
                _taxAmount = value;
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
            }
        }
        public long CreatedBy
        {
            get
            {
                return _createdBy;
            }

            set
            {
                _createdBy = value;
            }
        }
        #endregion

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@P_DetId", -1);
                paramList[1] = new SqlParameter("@P_PriceId", _priceId);
                paramList[2] = new SqlParameter("@P_TaxCode", _taxCode);
                paramList[3] = new SqlParameter("@P_TaxValue", _taxValue);
                paramList[4] = new SqlParameter("@P_TaxAmount", _taxAmount);
                paramList[5] = new SqlParameter("@P_CreatedBy", _createdBy);
                if(_productID>0)
                {
                    paramList[6] = new SqlParameter("@P_ProductID", _productID);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_ProductID", DBNull.Value);
                }
                
                DBGateway.ExecuteNonQuerySP("CT_P_TAXDET_ADD_UPDATE", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Price, Severity.High, 0, "Failed to Save GST Detail. Reason : " + ex.ToString(), "");
            }
        }

        public static Decimal LoadGSTValues(ref List<GSTTaxDetail> GSTDetails, decimal markup, long locationId)
        {
            decimal totGstAmount = 0;
            try
            {
                DataTable dtGst = LocationMaster.GetGSTList(locationId);

                if (dtGst != null && dtGst.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtGst.Rows)
                    {
                        if (dr["tax_code"] != DBNull.Value && dr["tax_value"] != DBNull.Value)
                        {
                            GSTTaxDetail gsttaxDet = new GSTTaxDetail();
                            gsttaxDet.TaxCode = Utility.ToString(dr["tax_code"]);
                            gsttaxDet.TaxValue = Utility.ToInteger(dr["tax_value"]);
                            decimal value = Convert.ToDecimal(dr["tax_value"]);
                            decimal gstCal = (markup * value / 100);
                            totGstAmount += gstCal;
                            gsttaxDet.TaxAmount = gstCal;
                            gsttaxDet.CreatedBy = Settings.LoginInfo.UserID;
                            GSTDetails.Add(gsttaxDet);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return totGstAmount;
        }

        public static Decimal LoadGSTValues(ref List<GSTTaxDetail> GSTDetails, decimal markup, long locationId, long CreatedBy)
        {
            decimal totGstAmount = 0;
            try
            {
                DataTable dtGst = LocationMaster.GetGSTList(locationId);

                if (dtGst != null && dtGst.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtGst.Rows)
                    {
                        if (dr["tax_code"] != DBNull.Value && dr["tax_value"] != DBNull.Value)
                        {
                            GSTTaxDetail gsttaxDet = new GSTTaxDetail();
                            gsttaxDet.TaxCode = Utility.ToString(dr["tax_code"]);
                            gsttaxDet.TaxValue = Utility.ToInteger(dr["tax_value"]);
                            decimal value = Convert.ToDecimal(dr["tax_value"]);
                            decimal gstCal = (markup * value / 100);
                            totGstAmount += gstCal;
                            gsttaxDet.TaxAmount = gstCal;
                            gsttaxDet.CreatedBy = CreatedBy;
                            GSTDetails.Add(gsttaxDet);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return totGstAmount;
        }

        public static List<GSTTaxDetail> LoadGSTDetailsByPriceId(long priceId)
        {
            List<GSTTaxDetail> objList = new List<GSTTaxDetail>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_PriceId", priceId);
                DataTable dtDetails = DBGateway.ExecuteQuery("usp_LoadGSTDetailsByPriceId", paramList).Tables[0];
                if (dtDetails != null && dtDetails.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtDetails.Rows)
                    {
                        GSTTaxDetail objGST = new GSTTaxDetail();
                        objGST.DetId = Convert.ToInt32(dr["detId"]);
                        objGST.TaxCode = Convert.ToString(dr["tax_code"]);
                        objGST.TaxValue = Convert.ToDecimal(dr["tax_value"]);
                        objGST.TaxAmount = Convert.ToDecimal(dr["tax_amount"]);
                        objList.Add(objGST);
                    }
                }
            }
            catch
            {
                throw;
            }
            return objList;
        }
        // Added By HAri Malla  for Religare on 07-02-2019.
        // Getting The Insurance Product Gst Details.
        public static List<GSTTaxDetail> LoadGSTDetailsByProductId(long priceId,int type)
        {
            List<GSTTaxDetail> objList = new List<GSTTaxDetail>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_PriceId", priceId);
                paramList[1] = new SqlParameter("@ProductId", type);
                DataTable dtDetails = DBGateway.ExecuteQuery("usp_LoadGSTDetailsByProductId", paramList).Tables[0];
                if (dtDetails != null && dtDetails.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtDetails.Rows)
                    {
                        GSTTaxDetail objGST = new GSTTaxDetail();
                        objGST.DetId = Convert.ToInt32(dr["detId"]);
                        objGST.TaxCode = Convert.ToString(dr["tax_code"]);
                        objGST.TaxValue = Convert.ToDecimal(dr["tax_value"]);
                        objGST.TaxAmount = Convert.ToDecimal(dr["tax_amount"]);
                        objList.Add(objGST);
                    }
                }
            }
            catch
            {
                throw;
            }
            return objList;
        }
    }
}
