﻿using System;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for PackageQueries
/// </summary>
namespace CT.BookingEngine
{
    public class PackageQueries
    {
        #region privateFields
        int queryId;
        int dealId;
        int agencyId;
        int nights;
        string dealName;
        bool isInternational;
        string paxName;
        string email;
        string phone;
        string paxMessage;
        DateTime createdOn;
        int createdBy;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        string countryOfResidence;
        string cityOfResidence;
        DateTime departureDate;
        int adults;
        int childs;
        int infants;
        string productType;
        int roomCount;
        string transType;
        #endregion

        #region publicProperties
        public int QueryId
        {
            get
            {
                return queryId;
            }
            set
            {
                queryId = value;
            }
        }

        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }


        public int Nights
        {
            get
            {
                return nights;
            }
            set
            {
                nights = value;
            }
        }

        public string DealName
        {
            get
            {
                return dealName;
            }
            set
            {
                dealName = value;
            }
        }

        public bool IsInternational
        {
            get
            {
                return isInternational;
            }
            set
            {
                isInternational = value;
            }
        }

        public string PaxName
        {
            get
            {
                return paxName;
            }
            set
            {
                paxName = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public string PaxMessage
        {
            get
            {
                return paxMessage;
            }
            set
            {
                paxMessage = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public string CountryOfResidence
        {
            get { return countryOfResidence; }
            set { countryOfResidence = value; }
        }
        public string CityOfResidence
        {
            get { return cityOfResidence; }
            set { cityOfResidence = value; }
        }
        public DateTime DepartureDate
        {
            get { return departureDate; }
            set { departureDate = value; }
        }
        public int Adults
        {
            get { return adults; }
            set { adults = value; }
        }
        public int Childs
        {
            get { return childs; }
            set { childs = value; }
        }
        public int Infants
        {
            get { return infants; }
            set { infants = value; }
        }
        public string ProductType
        {
            get
            {
                return productType;
            }
            set
            {
                productType = value;
            }
        }
        public int RoomCount
        {
            get { return roomCount; }
            set { roomCount = value; }
        }
        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }
        #endregion

        #region publicMethods
        public int Save()
        {
            int queryIdReturnValue = 0;
            SqlParameter[] paramList = new SqlParameter[19];

            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@ProductType", productType);
            paramList[2] = new SqlParameter("@ProductName", dealName);
            paramList[3] = new SqlParameter("@ProductId", dealId);
            if(!string.IsNullOrEmpty(paxName)) paramList[4] = new SqlParameter("@PaxName", paxName);
            
            if(!string.IsNullOrEmpty(email)) paramList[5] = new SqlParameter("@Email", email);
           
            if(!string.IsNullOrEmpty(phone)) paramList[6] = new SqlParameter("@Phone", phone);
            
            if(!string.IsNullOrEmpty(paxMessage)) paramList[7] = new SqlParameter("@EnquiryMsg", paxMessage);
            paramList[8] = new SqlParameter("@CountryOfResidence", countryOfResidence);
            paramList[9] = new SqlParameter("@CityOfResidence", cityOfResidence);
            paramList[10] = new SqlParameter("@DepartureDate", departureDate);
            paramList[11] = new SqlParameter("@Adult", adults);
            paramList[12] = new SqlParameter("@Child", childs);
            paramList[13] = new SqlParameter("@Infant", infants);
            paramList[14] = new SqlParameter("@CreatedBy", createdBy);
            paramList[15] = new SqlParameter("@CreatedOn", DateTime.Now);
            paramList[16] = new SqlParameter("@EnquiryStatus", 1);
            if (roomCount > 0) paramList[17] = new SqlParameter("@RoomCount", roomCount);
            paramList[18] = new SqlParameter("@TransType", transType);
            //paramList[10].Direction = ParameterDirection.Output;
            try
            {
                queryIdReturnValue = DBGateway.ExecuteNonQuerySP("usp_SavePackageEnquiry", paramList);
                //queryIdReturnValue = (int)paramList[10].Value;
            }
            catch (Exception ex)
            {
               // Audit.Add(EventType.PakageQueries, Severity.High, 0, "Unable to save user query, dealId:" + dealId + ", paxName:" + paxName + ", email:" + email + ", phone:" + phone + ", paxMessage:" + paxMessage + ", Error Message:" + ex.Message + "| Stack Trace:" + ex.StackTrace, "");
                throw ex;
            }
            return queryIdReturnValue;
        }
        #endregion
    }
}

