using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    /// <summary>
    /// HotelPaxType(like Adult, Child)
    /// </summary>
    public enum HotelPaxType
    {
        /// <summary>
        /// Adult
        /// </summary>
        Adult=1,
        /// <summary>
        /// Child
        /// </summary>
        Child = 2
    }
    /// <summary>
    /// This class is using to save the HotelPassenger in DB and retrive the HotelPassenger from DB
    /// </summary>
    [Serializable ]
    public class HotelPassenger
    {
        #region Member Properties List
        /// <summary>
        /// paxId
        /// </summary>
        int paxId;
        /// <summary>
        /// hotelId
        /// </summary>
        int hotelId;
        /// <summary>
        /// title(Mr,Ms..)
        /// </summary>
        string title;
        /// <summary>
        /// Pax firstname
        /// </summary>
        string firstname;
        /// <summary>
        /// Pax middlename
        /// </summary>
        string middlename;
        /// <summary>
        /// lastname
        /// </summary>
        string lastname;
        /// <summary>
        /// phoneno
        /// </summary>
        string phoneno; // number node will take the same value
        /// <summary>
        /// countrycode
        /// </summary>
        string countrycode;
        /// <summary>
        /// nationalityCode
        /// </summary>
        string nationalityCode;
        /// <summary>
        /// areacode
        /// </summary>
        string areacode;
        /// <summary>
        /// email
        /// </summary>
        string email;
        /// <summary>
        /// addressline1
        /// </summary>
        string addressline1;
        /// <summary>
        /// addressline2
        /// </summary>
        string addressline2;
        /// <summary>
        /// city
        /// </summary>
        string city;
        /// <summary>
        /// zipcode
        /// </summary>
        string zipcode;
        /// <summary>
        /// state
        /// </summary>
        string state;
        /// <summary>
        /// country
        /// </summary>
        string country;
        /// <summary>
        /// nationality
        /// </summary>
        string nationality;
        /// <summary>
        /// paxType
        /// </summary>
        HotelPaxType paxType;
        /// <summary>
        /// roomId
        /// </summary>
        int roomId;
        /// <summary>
        /// leadPasseneger
        /// </summary>
        bool leadPasseneger;
        /// <summary>
        /// age
        /// </summary>
        int age;
        /// <summary>
        /// department
        /// </summary>
        string department;
        /// <summary>
        /// employee
        /// </summary>
        string employee;
        /// <summary>
        /// division
        /// </summary>
        string division;
        /// <summary>
        /// purpose
        /// </summary>
        string purpose;
        /// <summary>
        /// employeeID
        /// </summary>
        string employeeID;
        /// <summary>
        /// isGST
        /// </summary>
        Boolean isGST;
        /// <summary>
        /// hotelPassengerGST Details
        /// </summary>
        HotelPassengerGST hotelPassengerGST;
        /// <summary>
        /// Flex Fields
        /// </summary>
        List<FlightFlexDetails> flexDetailsList;
        /// <summary>
        /// corporate profile id
        /// </summary>
        string _corpProfileId;


        #endregion
        #region public properties
        /// <summary>
        /// PaxId
        /// </summary>
        public int PaxId
        {
            get { return paxId; }
            set { paxId = value; }
        }
        /// <summary>
        /// HotelId
        /// </summary>
        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }
        /// <summary>
        /// Title
        /// </summary>
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        /// <summary>
        /// Firstname
        /// </summary>
        public string Firstname
        {
            get { return firstname; }
            set { firstname = value; }
        }
        /// <summary>
        /// Middlename
        /// </summary>
        public string Middlename
        {
            get { return middlename; }
            set { middlename = value; }
        }
        /// <summary>
        /// Lastname
        /// </summary>
        public string Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }
        /// <summary>
        /// Phoneno
        /// </summary>
        public string Phoneno
        {
            get { return phoneno; }
            set { phoneno = value; }
        }
        /// <summary>
        /// Countrycode
        /// </summary>
        public string Countrycode
        {
            get { return countrycode; }
            set { countrycode = value; }
        }
        /// <summary>
        /// NationalityCode
        /// </summary>
        public string NationalityCode
        {
            get { return nationalityCode; }
            set { nationalityCode = value; }
        }
        /// <summary>
        /// Areacode
        /// </summary>
        public string Areacode
        {
            get { return areacode; }
            set { areacode = value; }
        }
        /// <summary>
        /// Email
        /// </summary>
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        /// <summary>
        /// Addressline1
        /// </summary>
        public string Addressline1
        {
            get { return addressline1; }
            set { addressline1 = value; }
        }
        /// <summary>
        /// Addressline2
        /// </summary>
        public string Addressline2
        {
            get { return addressline2; }
            set { addressline2 = value; }
        }
        /// <summary>
        /// City
        /// </summary>
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        /// <summary>
        /// Zipcode
        /// </summary>
        public string Zipcode
        {
            get { return zipcode; }
            set { zipcode = value; }
        }
        /// <summary>
        /// State
        /// </summary>
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        /// <summary>
        /// Country
        /// </summary>
        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        /// <summary>
        /// Nationality
        /// </summary>
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        /// <summary>
        /// PaxType
        /// </summary>
        public HotelPaxType PaxType
        {
            get { return paxType; }
            set { paxType = value; }
        }
        /// <summary>
        /// RoomId
        /// </summary>
        public int RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }
        /// <summary>
        /// LeadPassenger
        /// </summary>
        public bool LeadPassenger
        {
            get { return leadPasseneger; }
            set { leadPasseneger = value; }
        }
        /// <summary>
        /// Age
        /// </summary>
        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        /// <summary>
        /// Employee
        /// </summary>
        public string Employee
        {
            get { return employee; }
            set { employee = value; }
        }
        /// <summary>
        /// Division
        /// </summary>
        public string Division
        {
            get { return division; }
            set { division = value; }
        }
        /// <summary>
        /// Department
        /// </summary>
        public string Department
        {
            get { return department; }
            set { department = value; }
        }
        /// <summary>
        /// Purpose
        /// </summary>
        public string Purpose
        {
            get { return purpose; }
            set { purpose = value; }
        }
        /// <summary>
        /// EmployeeID
        /// </summary>
        public string EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }
        /// <summary>
        /// IsGST
        /// </summary>
        public Boolean IsGST
        {
            get { return isGST; }
            set { isGST = value; }
        }
        /// <summary>
        /// HotelPassengerGST Details
        /// </summary>
        public HotelPassengerGST HotelPassengerGST
        {
            get { return hotelPassengerGST; }
            set { hotelPassengerGST = value; }
        }

        /// <summary>
        /// Flex Details
        /// </summary>
        public List<FlightFlexDetails> FlexDetailsList
        {
            get { return flexDetailsList; }
            set { flexDetailsList = value; }
        }

        public string CorpProfileId { get => _corpProfileId; set => _corpProfileId = value; }

        public string roomRefKey { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// This Method is used to Save the Hotel Passenger details
        /// </summary>
        /// <returns>int</returns>
        public int Save()
        { 
            //TODO: validate that the fields are properly set before calling the SP
            //Trace.TraceInformation("HotelPassenger.Save entered.");
            SqlParameter[] paramList = new SqlParameter[28];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            paramList[1] = new SqlParameter("@firstName", firstname);
            paramList[2] = new SqlParameter("@lastName", lastname);
            paramList[3] = new SqlParameter("@email", email);
            paramList[4] = new SqlParameter("@phone", phoneno);

            paramList[5] = new SqlParameter("@countryCode", countrycode);
    
            paramList[6] = new SqlParameter("@address1", addressline1);

            if (addressline2 != null)
            {
                paramList[7] = new SqlParameter("@address2", addressline2);
            }
            else
            {
                paramList[7] = new SqlParameter("@address2", ""); 
            }

            paramList[8] = new SqlParameter("@areaCode", areacode);
            paramList[9] = new SqlParameter("@city", city);
            paramList[10] = new SqlParameter("@zipCode", zipcode);
            paramList[11] = new SqlParameter("@state", state);
            paramList[12] = new SqlParameter("@country", country);          
           
            paramList[13] = new SqlParameter("@paxId", SqlDbType.Int);
            paramList[13].Direction = ParameterDirection.Output;
            paramList[14] = new SqlParameter("@paxType", paxType);
            paramList[15] = new SqlParameter("@title", title);
            paramList[16] = new SqlParameter("@leadPassenger", leadPasseneger);
            paramList[17] = new SqlParameter("@age", age);
            paramList[18] = new SqlParameter("@roomId", roomId);
            paramList[19] = new SqlParameter("@nationality", nationality);
            paramList[20] = new SqlParameter("@nationalityCode", nationalityCode);
            paramList[21] = new SqlParameter("@department", department);
            paramList[22] = new SqlParameter("@employee", employee);
            paramList[23] = new SqlParameter("@division", division);
            paramList[24] = new SqlParameter("@purpose", purpose);
            paramList[25] = new SqlParameter("@employeeID", employeeID);
            paramList[26] = new SqlParameter("@isGst", IsGST);
          //  paramList[27] = new SqlParameter("@CorpProfileId", CorpProfileId);
            if (!string.IsNullOrEmpty(CorpProfileId))
            {
                paramList[27] = new SqlParameter("@CorpProfileId", CorpProfileId);
            }
            else
            {
                paramList[27] = new SqlParameter("@CorpProfileId", DBNull.Value);
            }
            paramList[27].Direction = ParameterDirection.InputOutput;
            paramList[27].Size = 10;
            //int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHotelPassenger, paramList);
            int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddHotelPassenger", paramList);
            PaxId = (int)paramList[13].Value;
            if (!string.IsNullOrEmpty(paramList[27].Value.ToString()))
                CorpProfileId = Convert.ToString(paramList[27].Value);
            //====== Added by somasekhar on 20/09/2018 for HotelPassenger GST Details (Yatra source only) =====
            if (IsGST && PaxId>0)
            {
                try
                {
                    HotelPassengerGST.GSTPaxId = PaxId;
                    HotelPassengerGST.GSTHotelId = hotelId;
                    HotelPassengerGST.SaveHotelPassengerGST();
                }
                catch {  }
            }

            //Flex details Saving only lead Pax
            if (flexDetailsList != null && flexDetailsList.Count > 0)
            {
                flexDetailsList.Where(x => !string.IsNullOrEmpty(x.FlexLabel)).ToList().ForEach(y => {

                    y.FlexData = string.IsNullOrEmpty(y.FlexData) ? string.Empty : y.FlexData;
                    y.PaxId = PaxId;
                    y.Save();
                });
            }
            //=========================
            //Trace.TraceInformation("HotelPassenger.Save exiting");

            return PaxId;
        }
        /// <summary>
        /// This Method is used to load Lead Passenger details
        /// </summary>
        /// <param name="hotelId"></param>
        public void Load(int hotelId)
        {
            //Trace.TraceInformation("HotelPassenger.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId should be positive integer", "hotelId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);

            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelPassengerByHotelId, paramList,connection);
            using (DataTable dtHotelPax = DBGateway.FillDataTableSP(SPNames.GetHotelPassengerByHotelId, paramList))
            {
                if (dtHotelPax != null && dtHotelPax.Rows.Count > 0)
                {
                    //if (data.Read())
                    DataRow data = dtHotelPax.Rows[0];
                    paxId = Convert.ToInt32(data["paxId"]);
                    paxType = (HotelPaxType)Convert.ToInt16(data["paxType"]);
                    hotelId = Convert.ToInt32(data["hotelId"]);
                    firstname = Convert.ToString(data["firstname"]);
                    if (data["lastname"] != null)
                    {
                        lastname = Convert.ToString(data["lastname"]);
                    }
                    email = Convert.ToString(data["email"]);
                    phoneno = Convert.ToString(data["phone"]);
                    countrycode = Convert.ToString(data["countrycode"]);
                    nationalityCode = Convert.ToString(data["nationalityCode"]);
                    addressline1 = Convert.ToString(data["address1"]);
                    if (data["address2"] != DBNull.Value)
                    {
                        addressline2 = Convert.ToString(data["address2"]);
                    }
                    else
                    {
                        addressline2 = "";
                    }
                    areacode = Convert.ToString(data["areacode"]);
                    city = Convert.ToString(data["city"]);
                    zipcode = Convert.ToString(data["zipcode"]);
                    state = Convert.ToString(data["state"]);
                    country = Convert.ToString(data["country"]);
                    nationality = Convert.ToString(data["nationality"]);
                    if (data["title"] != null)
                    {
                        title = Convert.ToString(data["title"]);
                    }
                    if (data["age"] != DBNull.Value)
                    {
                        age = Convert.ToInt32(data["age"]);
                    }
                    if (data["roomId"] != DBNull.Value)
                    {
                        roomId = Convert.ToInt32(data["roomId"]);
                    }
                    leadPasseneger = Convert.ToBoolean(data["leadPassenger"]);
                    department = Convert.ToString(data["department"]);
                    employee = Convert.ToString(data["employee"]);
                    division = Convert.ToString(data["division"]);
                    purpose = Convert.ToString(data["purpose"]);
                    employeeID = Convert.ToString(data["employeeId"]);
                    flexDetailsList = FlightFlexDetails.GetFlightPaxDetails(paxId, 2);
                    if (data["CorpProfileId"] != DBNull.Value)
                    {
                        _corpProfileId = Convert.ToString(data["CorpProfileId"]);
                    }
                    //data.Close();
                    //connection.Close();
                    //Trace.TraceInformation("HotelPassenger.Load exiting :" + Convert.ToString(hotelId));
                }
                else
                {
                    //data.Close();
                    //connection.Close();
                    //Trace.TraceInformation("HotelPassenger.Load exiting : hotelId does not exist.hotelId = " + hotelId.ToString());
                    throw new ArgumentException("Hotel id does not exist in database");
                }
            }
            //Trace.TraceInformation("HotelPassenger.Load exiting.");
        }
        /// <summary>
        /// This Method is used to load all Passenger details of a particular Room
        /// </summary>
        /// <param name="hotelId">hotelId</param>
        /// <param name="roomId">roomId</param>
        /// <returns>List HotelPassenger</returns>
        public List<HotelPassenger>  Load(int hotelId, int roomId)
        {
            //Trace.TraceInformation("HotelPassenger.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId should be positive integer", "hotelId");
            }
            List<HotelPassenger> hotelPaxList=new List<HotelPassenger>();
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            paramList[1]=new SqlParameter("@roomId",roomId);

            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelPassengerByHotelIdRoomId, paramList,connection);
            using (DataTable dtHotelPax = DBGateway.FillDataTableSP(SPNames.GetHotelPassengerByHotelIdRoomId, paramList))
            {
                if (dtHotelPax != null && dtHotelPax.Rows.Count > 0)
                {
                    //while (data.Read())
                    foreach (DataRow data in dtHotelPax.Rows)
                    {
                        HotelPassenger passInfo = new HotelPassenger();
                        passInfo.PaxId = Convert.ToInt32(data["paxId"]);
                        passInfo.PaxType = (HotelPaxType)Convert.ToInt16(data["paxType"]);
                        passInfo.HotelId = Convert.ToInt32(data["hotelId"]);
                        if (data["firstname"] != DBNull.Value)
                        {
                            passInfo.Firstname =Convert.ToString(data["firstname"]);
                        }
                        if (data["lastname"] != DBNull.Value)
                        {
                            passInfo.Lastname = Convert.ToString(data["lastname"]);
                        }
                        if (data["email"] != DBNull.Value)
                        {
                            passInfo.Email = Convert.ToString(data["email"]);
                        }
                        if (data["phone"] != DBNull.Value)
                        {
                            passInfo.Phoneno = Convert.ToString(data["phone"]);
                        }
                        if (data["countrycode"] != DBNull.Value)
                        {
                            passInfo.Countrycode = Convert.ToString(data["countrycode"]);
                        }
                        if (data["address1"] != DBNull.Value)
                        {
                            passInfo.Addressline1 = Convert.ToString(data["address1"]);
                        }
                        if (data["address2"] != DBNull.Value)
                        {
                            passInfo.Addressline2 = Convert.ToString(data["address2"]);
                        }
                        else
                        {
                            passInfo.Addressline2 = "";
                        }
                        if (data["areacode"] != DBNull.Value)
                        {
                            passInfo.Areacode = Convert.ToString(data["areacode"]);
                        }
                        if (data["city"] != DBNull.Value)
                        {
                            passInfo.City = Convert.ToString(data["city"]);
                        }
                        if (data["zipcode"] != DBNull.Value)
                        {
                            passInfo.Zipcode = Convert.ToString(data["zipcode"]);
                        }
                        if (data["state"] != DBNull.Value)
                        {
                            passInfo.State = Convert.ToString(data["state"]);
                        }
                        if (data["country"] != DBNull.Value)
                        {
                            passInfo.Country = Convert.ToString(data["country"]);
                        }
                        if (data["title"] != null)
                        {
                            passInfo.Title = Convert.ToString(data["title"]);
                        }
                        if (data["age"] != DBNull.Value)
                        {
                            passInfo.Age = Convert.ToInt32(data["age"]);
                        }
                        if (data["department"] != DBNull.Value)
                        {
                            passInfo.department = Convert.ToString(data["department"]);
                        }
                        if (data["employee"] != DBNull.Value)
                        {
                            passInfo.employee = Convert.ToString(data["employee"]);
                        }
                        if (data["division"] != DBNull.Value)
                        {
                            passInfo.division = Convert.ToString(data["division"]);
                        }
                        if (data["purpose"] != DBNull.Value)
                        {
                            passInfo.purpose = Convert.ToString(data["purpose"]);
                        }
                        if (data["employeeId"] != DBNull.Value)
                        {
                            passInfo.employeeID = Convert.ToString(data["employeeId"]);
                        }
                        passInfo.RoomId = Convert.ToInt32(data["roomId"]);
                        passInfo.LeadPassenger = Convert.ToBoolean(data["leadPassenger"]);
                        passInfo.flexDetailsList = FlightFlexDetails.GetFlightPaxDetails(passInfo.paxId, 2);
                        hotelPaxList.Add(passInfo);
                    }
                }
            }
           //data.Close();
           //connection.Close();
           //Trace.TraceInformation("HotelPassenger.Load exiting.");
           return hotelPaxList;
        }
        /// <summary>
        /// This menthod is used to update the Hotel Passeneger Information.
        /// </summary>
        public void Update()
        {
            //Trace.TraceInformation("HotelPassenger.Update entered.");
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@firstName", firstname);
            paramList[1] = new SqlParameter("@lastName", lastname);
            paramList[2] = new SqlParameter("@paxId", paxId);
            paramList[3] = new SqlParameter("@leadPassenger", leadPasseneger);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelPassenger, paramList);
           //Trace.TraceInformation("HotelPassenger.Update exiting");


        }

        /// <summary>
        /// This Method is used to load DeptList
        /// </summary>
        /// <returns></returns>
        public static DataTable GetBkeDeptList()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                DataTable dt = DBGateway.FillDataTableSP("usp_bke_department_GetList", paramList);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// This method is Get HotelItineraryPaxInfo
        /// </summary>
        /// <param name="HotelId">HotelId</param>
        public static DataTable GetHotelItineraryPaxInfo(int HotelId)
        {
            try
            {
                DataTable dtP = new DataTable();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@hotelId", HotelId);
                dtP = DBGateway.FillDataTableSP("usp_GetHotelPassengerByHotelId", paramList);
                return dtP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// This method is Get All HotelItineraryPaxInfo
        /// </summary>
        /// <param name="HotelId">HotelId</param>
        public static DataTable GetHotelAllPaxInfo(int HotelId)
        {
            try
            {
                DataTable dtP = new DataTable();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@hotelId", HotelId);
                dtP = DBGateway.FillDataTableSP("usp_GetHotelAllPassengersByHotelId", paramList);
                return dtP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    /// <summary>
    /// This class is using to save the HotelPassengerGST in DB and retrive the HotelPassengerGST Details from DB
    /// </summary>
    [Serializable]
    public class HotelPassengerGST
    {
        //Added by somasekhar on 12/09/2018 
        // GST Details -- YATRA
        #region Member properties
        /// <summary>
        /// GSTHotelId
        /// </summary>
        int gstHotelId;
        /// <summary>
        /// GSTPaxId
        /// </summary>
        int gstPaxId;
        /// <summary>
        /// GSTNumber
        /// </summary>
        string gstNumber;
        /// <summary>
        /// GSTCompanyName
        /// </summary>
        string gstCompanyName;
        /// <summary>
        /// GSTCompanyEmailId
        /// </summary>
        string gstCompanyEmailId;
        /// <summary>
        /// GSTCompanyAddress
        /// </summary>
        string gstCompanyAddress;
        /// <summary>
        /// GSTCity
        /// </summary>
        string gstCity;
        /// <summary>
        /// GSTPinCode
        /// </summary>
        string gstPinCode;
        /// <summary>
        /// GSTState Code (29,36,37)
        /// </summary>
        string gstState;
        /// <summary>
        /// GSTPhoneISD
        /// </summary>
        string gstPhoneISD;
        /// <summary>
        /// GSTPhoneNumber
        /// </summary>
        string gstPhoneNumber;
        /// <summary>
        /// GSTCustomerName
        /// </summary>
        string gstCustomerName;
        /// <summary>
        /// GSTCustomerAddress
        /// </summary>
        string gstCustomerAddress;
        /// <summary>
        /// GSTCustomerState
        /// </summary>
        string gstCustomerState;
        #endregion 
        #region  public properties
        /// <summary>
        /// GSTNumber
        /// </summary>
        public string GSTNumber
        {
            get { return gstNumber; }
            set { gstNumber = value; }
        }
        /// <summary>
        /// GSTCompanyName
        /// </summary>
        public string GSTCompanyName
        {
            get { return gstCompanyName; }
            set { gstCompanyName = value; }
        }
        /// <summary>
        /// GSTCompanyEmailId
        /// </summary>
        public string GSTCompanyEmailId
        {
            get { return gstCompanyEmailId; }
            set { gstCompanyEmailId = value; }
        }
        /// <summary>
        /// GSTCompanyAddress
        /// </summary>
        public string GSTCompanyAddress
        {
            get { return gstCompanyAddress; }
            set { gstCompanyAddress = value; }
        }
        /// <summary>
        /// GSTCity
        /// </summary>
        public string GSTCity
        {
            get { return gstCity; }
            set { gstCity = value; }
        }
        /// <summary>
        /// GSTPinCode
        /// </summary>
        public string GSTPinCode
        {
            get { return gstPinCode; }
            set { gstPinCode = value; }
        }
        /// <summary>
        /// GSTState
        /// </summary>
        public string GSTState
        {
            get { return gstState; }
            set { gstState = value; }
        }
        /// <summary>
        /// GSTPhoneISD
        /// </summary>
        public string GSTPhoneISD
        {
            get { return gstPhoneISD; }
            set { gstPhoneISD = value; }
        }
        /// <summary>
        /// GSTPhoneNumber
        /// </summary>
        public string GSTPhoneNumber
        {
            get { return gstPhoneNumber; }
            set { gstPhoneNumber = value; }
        }
        /// <summary>
        /// gstCustomerName
        /// </summary>
        public string GSTCustomerName
        {
            get { return gstCustomerName; }
            set { gstCustomerName = value; }
        }
        /// <summary>
        /// gstCustomerAddress
        /// </summary>
        public string GSTCustomerAddress
        {
            get { return gstCustomerAddress; }
            set { gstCustomerAddress = value; }
        }
        /// <summary>
        /// gstCustomerState
        /// </summary>
        public string GSTCustomerState
        {
            get { return gstCustomerState; }
            set { gstCustomerState = value; }
        }
        /// <summary>
        /// gstHotelId
        /// </summary>
        public int GSTHotelId
        {
            get { return gstHotelId; }
            set { gstHotelId = value; }
        }
        /// <summary>
        /// gstPaxId
        /// </summary>
        public int GSTPaxId
        {
            get { return gstPaxId; }
            set { gstPaxId = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// This Method is used to Save the Hotel Passenger details
        /// </summary>
        /// <returns>int</returns>
        public void SaveHotelPassengerGST()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[14];
                //====== Added by somasekhar on 14/09/2018 -- Yatra GST Details =========
                paramList[0] = new SqlParameter("@gstPaxId", gstPaxId);
                paramList[1] = new SqlParameter("@gstHotelId", gstHotelId);
                paramList[2] = new SqlParameter("@gstNumber", gstNumber);
                paramList[3] = new SqlParameter("@gstCompanyName", gstCompanyName);
                paramList[4] = new SqlParameter("@gstCompanyEmailId", gstCompanyEmailId);
                paramList[5] = new SqlParameter("@gstCompanyAddress", gstCompanyAddress);
                paramList[6] = new SqlParameter("@gstCity", gstCity);
                paramList[7] = new SqlParameter("@gstPinCode", gstPinCode);
                paramList[8] = new SqlParameter("@gstState", gstState);
                paramList[9] = new SqlParameter("@gstPhoneISD", gstPhoneISD);
                paramList[10] = new SqlParameter("@gstPhoneNumber", gstPhoneNumber);
                paramList[11] = new SqlParameter("@gstCustomerName", gstCustomerName);
                paramList[12] = new SqlParameter("@gstCustomerAddress", gstCustomerAddress);
                paramList[13] = new SqlParameter("@gstCustomerState", gstCustomerState);

                DBGateway.ExecuteNonQuerySP("usp_AddHotelPassengerGST", paramList); 
            }
            catch  {   }

        }

        #endregion
    }
}
