using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.BookingEngine
{
    public class HolidayPackage
    {
        //static string masterDB = System.Configuration.ConfigurationSettings.AppSettings["MasterDB"].ToString();
        //static string masterDBagentId = System.Configuration.ConfigurationSettings.AppSettings["MasterDBAgentId"].ToString();
        #region privateFields
        int dealId;
        string dealName;
        string city;
        string country;
        string description;
        bool isInternational;
        int nights;
        string mainImagePath;
        string imagePath;
        string thumbnailPath;
        bool hasAirfare;
        bool hasHotel;
        bool hasSightSeeing;
        bool hasMeals;
        bool hasTransport;
        string overview;
        string inclusions;
        string itinerary1;
        string itinerary2;
        string itinerary3;
        string itinerary4;
        string itinerary5;
        string itinerary6;
        string itinerary7;
        string itinerary8;
        string itinerary9;
        string itinerary10;
        string itinerary11;
        string itinerary12;
        string itinerary13;
        string notes;
        string priceExclude;
        string termsAndConditions;
        //int priority;
        int paging;
        //int leftDeal1;
        //int leftDeal2;
        bool isActive;
        bool isDeleted;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        List<HolidayPackageSeason> hotelSeasonList;
        HolidayPackageSeason hotelSeason;
        int themeId;
        string themeName;
        string roomRates;
        string classification;
        decimal startRate;
        decimal endRate;
        string packageSearchCity;
        //adding new varaibles 24/05/2016
        public static Dictionary<string, decimal> ExchangeRates;
        public static string AgentCurrency;
 
        #endregion

        #region publicProperties
        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public string DealName
        {
            get
            {
                return dealName;
            }
            set
            {
                dealName = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public bool IsInternational
        {
            get
            {
                return isInternational;
            }
            set
            {
                isInternational = value;
            }
        }

        public int Nights
        {
            get
            {
                return nights;
            }
            set
            {
                nights = value;
            }
        }

        public string MainImagePath
        {
            get
            {
                return mainImagePath;
            }
            set
            {
                mainImagePath = value;
            }
        }

        public string ImagePath
        {
            get
            {
                return imagePath;
            }
            set
            {
                imagePath = value;
            }
        }

        public string ThumbnailPath
        {
            get
            {
                return thumbnailPath;
            }
            set
            {
                thumbnailPath = value;
            }
        }

        public bool HasAirfare
        {
            get
            {
                return hasAirfare;
            }
            set
            {
                hasAirfare = value;
            }
        }

        public bool HasHotel
        {
            get
            {
                return hasHotel;
            }
            set
            {
                hasHotel = value;
            }
        }

        public bool HasSightSeeing
        {
            get
            {
                return hasSightSeeing;
            }
            set
            {
                hasSightSeeing = value;
            }
        }

        public bool HasMeals
        {
            get
            {
                return hasMeals;
            }
            set
            {
                hasMeals = value;
            }
        }

        public bool HasTransport
        {
            get
            {
                return hasTransport;
            }
            set
            {
                hasTransport = value;
            }
        }

        public string Overview
        {
            get
            {
                return overview;
            }
            set
            {
                overview = value;
            }
        }

        public string Inclusions
        {
            get
            {
                return inclusions;
            }
            set
            {
                inclusions = value;
            }
        }

        public string Itinerary1
        {
            get
            {
                return itinerary1;
            }
            set
            {
                itinerary1 = value;
            }
        }

        public string Itinerary2
        {
            get
            {
                return itinerary2;
            }
            set
            {
                itinerary2 = value;
            }
        }

        public string Itinerary3
        {
            get
            {
                return itinerary3;
            }
            set
            {
                itinerary3 = value;
            }
        }

        public string Itinerary4
        {
            get
            {
                return itinerary4;
            }
            set
            {
                itinerary4 = value;
            }
        }

        public string Itinerary5
        {
            get
            {
                return itinerary5;
            }
            set
            {
                itinerary5 = value;
            }
        }

        public string Itinerary6
        {
            get
            {
                return itinerary6;
            }
            set
            {
                itinerary6 = value;
            }
        }

        public string Itinerary7
        {
            get
            {
                return itinerary7;
            }
            set
            {
                itinerary7 = value;
            }
        }

        public string Itinerary8
        {
            get
            {
                return itinerary8;
            }
            set
            {
                itinerary8 = value;
            }
        }

        public string Itinerary9
        {
            get
            {
                return itinerary9;
            }
            set
            {
                itinerary9 = value;
            }
        }

        public string Itinerary10
        {
            get
            {
                return itinerary10;
            }
            set
            {
                itinerary10 = value;
            }
        }

        public string Itinerary11
        {
            get
            {
                return itinerary11;
            }
            set
            {
                itinerary11 = value;
            }
        }

        public string Itinerary12
        {
            get { return itinerary12; }
            set { itinerary12 = value; }
        }

        public string Itinerary13
        {
            get { return itinerary13; }
            set { itinerary13 = value; }
        }

        public string Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }

        public string PriceExclude
        {
            get
            {
                return priceExclude;
            }
            set
            {
                priceExclude = value;
            }
        }

        public string TermsAndConditions
        {
            get
            {
                return termsAndConditions;
            }
            set
            {
                termsAndConditions = value;
            }
        }

        //public int Priority
        //{
        //    get
        //    {
        //        return priority;
        //    }
        //    set
        //    {
        //        priority = value;
        //    }
        //}

        public int Paging
        {
            get
            {
                return paging;
            }
            set
            {
                paging = value;
            }
        }

        //public int LeftDeal1
        //{
        //    get
        //    {
        //        return leftDeal1;
        //    }
        //    set
        //    {
        //        leftDeal1 = value;
        //    }
        //}

        //public int LeftDeal2
        //{
        //    get
        //    {
        //        return leftDeal2;
        //    }
        //    set
        //    {
        //        leftDeal2 = value;
        //    }
        //}

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return isDeleted;
            }
            set
            {
                isDeleted = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public List<HolidayPackageSeason> HotelSeasonList
        {
            get
            {
                return hotelSeasonList;
            }
            set
            {
                hotelSeasonList = value;
            }
        }

        public HolidayPackageSeason HotelSeason
        {
            get
            {
                return hotelSeason;
            }
            set
            {
                hotelSeason = value;
            }
        }

        public int ThemeId
        {
            get
            {
                return themeId;
            }
            set
            {
                themeId = value;
            }
        }

        public string ThemeName
        {
            get
            {
                return themeName;
            }
            set
            {
                themeName = value;
            }
        }

        public string RoomRates
        {
            get { return roomRates; }
            set { roomRates = value; }
        }

        public string Classification
        {
            get { return classification; }
            set{classification=value;}
        }
        public decimal StartRate
        {
            get { return startRate; }
            set { startRate = value; }
        }
        public decimal EndRate
        {
            get { return endRate; }
            set { endRate = value; }
        }
        public string PackageSearchCity
        {
            get { return packageSearchCity; }
            set { packageSearchCity = value; }
        }
        #endregion

        #region publicMethods

         

        public static HolidayPackage LoadHotelDeal(int dealId, int agencyId)
        {
            HolidayPackage htl = new HolidayPackage();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@dealId", dealId);
                paramList[1] = new SqlParameter("@agencyId", agencyId);
                //paramList[1] = new SqlParameter("@agencyId", Convert.ToInt32(masterDBagentId));
                try
                {
                    data = DBGateway.ExecuteReaderSP(SPNames.GetHolidayPackageDeal, paramList, connection);
                    if (data.Read())
                    {
                        htl.dealId = Convert.ToInt32(data["dealId"]);
                        htl.dealName = Convert.ToString(data["dealName"]);
                        htl.city = Convert.ToString(data["city"]);
                        htl.country = Convert.ToString(data["country"]);
                        htl.description = Convert.ToString(data["description"]);
                        htl.isInternational = Convert.ToBoolean(data["isInternational"]);
                        htl.nights = Convert.ToInt32(data["nights"]);
                        htl.mainImagePath = Convert.ToString(data["mainImagePath"]);
                        htl.imagePath = Convert.ToString(data["imagePath"]);
                        htl.thumbnailPath = Convert.ToString(data["thumbnailPath"]);
                        htl.hasAirfare = Convert.ToBoolean(data["hasAirfare"]);
                        htl.hasHotel = Convert.ToBoolean(data["hasHotel"]);
                        htl.hasSightSeeing = Convert.ToBoolean(data["hasSightSeeing"]);
                        htl.hasMeals = Convert.ToBoolean(data["hasMeals"]);
                        htl.hasTransport = Convert.ToBoolean(data["hasTransport"]);
                        htl.overview = Convert.ToString(data["overview"]);
                        htl.inclusions = Convert.ToString(data["inclusions"]);
                        htl.RoomRates = data["roomRates"].ToString();
                        if (data["itinerary1"] != DBNull.Value)
                        {
                            htl.itinerary1 = Convert.ToString(data["itinerary1"]);
                        }
                        else
                        {
                            htl.itinerary1 = string.Empty;
                        }
                        if (data["itinerary2"] != DBNull.Value)
                        {
                            htl.itinerary2 = Convert.ToString(data["itinerary2"]);
                        }
                        else
                        {
                            htl.itinerary2 = string.Empty;
                        }
                        if (data["itinerary3"] != DBNull.Value)
                        {
                            htl.itinerary3 = Convert.ToString(data["itinerary3"]);
                        }
                        else
                        {
                            htl.itinerary3 = string.Empty;
                        }
                        if (data["itinerary4"] != DBNull.Value)
                        {
                            htl.itinerary4 = Convert.ToString(data["itinerary4"]);
                        }
                        else
                        {
                            htl.itinerary4 = string.Empty;
                        }
                        if (data["itinerary5"] != DBNull.Value)
                        {
                            htl.itinerary5 = Convert.ToString(data["itinerary5"]);
                        }
                        else
                        {
                            htl.itinerary5 = string.Empty;
                        }
                        if (data["itinerary6"] != DBNull.Value)
                        {
                            htl.itinerary6 = Convert.ToString(data["itinerary6"]);
                        }
                        else
                        {
                            htl.itinerary6 = string.Empty;
                        }
                        if (data["itinerary7"] != DBNull.Value)
                        {
                            htl.itinerary7 = Convert.ToString(data["itinerary7"]);
                        }
                        else
                        {
                            htl.itinerary7 = string.Empty;
                        }
                        if (data["itinerary8"] != DBNull.Value)
                        {
                            htl.itinerary8 = Convert.ToString(data["itinerary8"]);
                        }
                        else
                        {
                            htl.itinerary8 = string.Empty;
                        }
                        if (data["itinerary9"] != DBNull.Value)
                        {
                            htl.itinerary9 = Convert.ToString(data["itinerary9"]);
                        }
                        else
                        {
                            htl.itinerary9 = string.Empty;
                        }
                        if (data["itinerary10"] != DBNull.Value)
                        {
                            htl.itinerary10 = Convert.ToString(data["itinerary10"]);
                        }
                        else
                        {
                            htl.itinerary10 = string.Empty;
                        }
                        if (data["itinerary11"] != DBNull.Value)
                        {
                            htl.itinerary11 = Convert.ToString(data["itinerary11"]);
                        }
                        else
                        {
                            htl.itinerary11 = string.Empty;
                        }
                        if (data["itinerary12"] != DBNull.Value)
                        {
                            htl.itinerary12 = data["itinerary12"].ToString();
                        }
                        else
                        {
                            htl.itinerary12 = "";
                        }
                        if (data["itinerary13"] != DBNull.Value)
                        {
                            htl.itinerary13 = data["itinerary13"].ToString();
                        }
                        else
                        {
                            htl.itinerary13 = "";
                        }
                        if (data["notes"] != DBNull.Value)
                        {
                            htl.notes = Convert.ToString(data["notes"]);
                        }
                        else
                        {
                            htl.notes = string.Empty;
                        }
                        if (data["priceExclude"] != DBNull.Value)
                        {
                            htl.priceExclude = Convert.ToString(data["priceExclude"]);
                        }
                        else
                        {
                            htl.priceExclude = string.Empty;
                        }
                        htl.termsAndConditions = Convert.ToString(data["termsAndConditions"]);
                        htl.paging = Convert.ToInt32(data["paging"]);
                        htl.isActive = Convert.ToBoolean(data["dealId"]);
                    }
                    if (htl.dealId > 0)
                    {
                        //modified on 24/05/2016
                        
                       
                        string supplierCurrency = string.Empty;
                        decimal rateOfExchange = 1;
                        htl.hotelSeasonList = HolidayPackageSeason.LoadhotelSeasons(htl.dealId);
                        if ((!string.IsNullOrEmpty(htl.hotelSeasonList[0].SupplierCurrency)) && AgentCurrency != htl.hotelSeasonList[0].SupplierCurrency)
                        {
                            supplierCurrency = htl.hotelSeasonList[0].SupplierCurrency;
                            rateOfExchange = (ExchangeRates.ContainsKey(supplierCurrency) ? ExchangeRates[supplierCurrency] : 1);
                            htl.hotelSeasonList[0].TwinSharingPrice = htl.hotelSeasonList[0].TwinSharingPrice * rateOfExchange;
                        }
                        else if (string.IsNullOrEmpty(htl.hotelSeasonList[0].SupplierCurrency))
                        {
                            supplierCurrency = CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                            rateOfExchange = (ExchangeRates.ContainsKey(supplierCurrency) ? ExchangeRates[supplierCurrency] : 1);
                            htl.hotelSeasonList[0].TwinSharingPrice = htl.hotelSeasonList[0].TwinSharingPrice * rateOfExchange;
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, "Exception in LoadHotelDeal(int dealId) for HolidayPackage.cs dealId= " + dealId + " , Error Message:" + ex.Message + "| Stack Trace:" + ex.StackTrace, "");
                    throw new Exception("Unable to load hotel deal for India Times Home Page. dealId =" + dealId);
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return htl;
        }

        public static List<string> LoadPackageCity(int agencyId)
        {
            List<string> cityList = new List<string>();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@agencyId", agencyId);
                    //paramList[0] = new SqlParameter("@agencyId", Convert.ToInt32(masterDBagentId));
                    data = DBGateway.ExecuteReaderSP(SPNames.GetPackageCityList, paramList, connection);
                    while (data.Read())
                    {
                        cityList.Add((string)data["City"]);

                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, "Exception in LoadPackageCity(for AgencyId) = " + agencyId + " , Error Message:" + ex.Message + "| Stack Trace:" + ex.StackTrace, "");
                    throw new Exception("Unable to load City for HolidayHome.aspx.");
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return cityList;
        }

        public static DataTable GetHolidayPackage(string city, string themeId, int agentId, string tourType, decimal? startPrice, decimal? endPrice) 
        {
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                SqlParameter[] paramList = new SqlParameter[6];
                if (!string.IsNullOrEmpty(city)) paramList[0] = new SqlParameter("@cityName", city);
                paramList[1] = new SqlParameter("@selectedThemeId", themeId);
                paramList[2] = new SqlParameter("@agencyId", agentId);
                //paramList[2] = new SqlParameter("@agencyId", Convert.ToInt32(masterDBagentId));
                if (!string.IsNullOrEmpty(tourType)) paramList[3] = new SqlParameter("@tourType", tourType);
                if (startPrice > 0) paramList[4] = new SqlParameter("@startPrice", startPrice);
                if (endPrice > 0) paramList[5] = new SqlParameter("@endPrice", endPrice);
                return DBGateway.ExecuteQuery(SPNames.GetHolidayPackages, paramList).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception in GetHolidayPackage(for AgencyId) = " + agentId + " , Error Message:" + ex.Message + "| Stack Trace:" + ex.StackTrace, "");
                throw new Exception("Unable to load Packages for StaticPage.aspx.");
            }
            
        }
        
        //Added on 25/05/2016
        public static DataTable GetHolidayPackagesWithCurrency(string city, string themeId, int agentId, string tourType, decimal? startPrice, decimal? endPrice)
        {
            DataTable dtPackages = null;
            SqlCommand cmd = null;
           
           try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                SqlParameter[] paramList = new SqlParameter[4];
                if (!string.IsNullOrEmpty(city)) paramList[0] = new SqlParameter("@cityName", city);
                paramList[1] = new SqlParameter("@selectedThemeId", themeId);
                paramList[2] = new SqlParameter("@agencyId", agentId);
                //paramList[2] = new SqlParameter("@agencyId", Convert.ToInt32(masterDBagentId));
                if (!string.IsNullOrEmpty(tourType)) paramList[3] = new SqlParameter("@tourType", tourType);
               // if (startPrice > 0 & startPrice != null) paramList[4] = new SqlParameter("@startPrice", startPrice);
               // if (endPrice > 0 & endPrice != null) paramList[5] = new SqlParameter("@endPrice", endPrice);

                dtPackages = DBGateway.ExecuteQuery(SPNames.GetHolidayPackagesWithCurrency, paramList).Tables[0];

                decimal rateOfExchange = 1;
                //DataTable dtPackageFilter = null;
                foreach (DataRow dr in dtPackages.Rows)
                {
                    if ((!string.IsNullOrEmpty(dr["SupplierCurrency"].ToString())) && AgentCurrency != dr["SupplierCurrency"].ToString())
                    {
                        string supplierCurrency = dr["SupplierCurrency"].ToString();
                        rateOfExchange = (ExchangeRates.ContainsKey(supplierCurrency) ? ExchangeRates[supplierCurrency] : 1);
                        dtPackages.Columns["price"].ReadOnly = false;
                        dr["price"] = Convert.ToDecimal(dr["price"]) * rateOfExchange;

                    }
                    else if(string.IsNullOrEmpty(dr["SupplierCurrency"].ToString()))
                    {
                        string supplierCurrency = CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                        rateOfExchange = (ExchangeRates.ContainsKey(supplierCurrency) ? ExchangeRates[supplierCurrency] : 1);
                        dtPackages.Columns["price"].ReadOnly = false;
                        dr["price"] = Convert.ToDecimal(dr["price"])*rateOfExchange;
                    }
                    
                }

                string filterPrice = string.Empty;
                if (startPrice > 0 || endPrice > 0)
                {
                    if (startPrice > 0 && endPrice > 0)
                    {
                        filterPrice = "price >=" + startPrice + " and price <= " + endPrice;
                    }
                    else if (startPrice > 0)
                    {
                         filterPrice = "price >=" + startPrice;
                    }
                    else if (endPrice > 0)
                    {
                        filterPrice = "price <= " + endPrice;
                    }
                        DataView dv = dtPackages.DefaultView;
                        dv.RowFilter = filterPrice;

                        dtPackages = dv.ToTable();
                   
                    //return dtPackageFilter;


                }

                dtPackages.DefaultView.Sort = "price";
                dtPackages = dtPackages.DefaultView.ToTable();

            }

            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception in GetHolidayPackageWithCurrency(for AgencyId) = " + agentId + " , Error Message:" + ex.Message + "| Stack Trace:" + ex.StackTrace, "");
                throw new Exception("Unable to load Packages for StaticPage.aspx.");
            }
            return dtPackages;
        }
       
        #endregion

    }
}
