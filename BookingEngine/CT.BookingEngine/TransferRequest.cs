using System;
using System.Collections.Generic;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.BookingEngine
{
    public enum TransferLocationType
    {
        PickUp=0,
        Dropoff=1
    }
    [Serializable] 
    public class TransferRequest
    {
        private bool immediateConfirmationOnly;
        private string itemName;
        private string itemCode;
        private string cityName;
        private DateTime trasnferDate;
        private int numberOfPassengers;
        private string preferredLanguage;
        private string alternateLanguage;
        private string pickUpCityCode;
        private string pickUpCode;
        private string pickUpPointCode;
        private string dropOffCityCode;
        private string dropOffCode;
        private string dropOffPointCode;
        //Added by brahmam 
        private string currency;
        //Added on <14/04/2016> by chandan
        private string nationality;
        private string nationalityCode;

        //Added on <15/04/2016> by chandan
        private string pickUpTime;

        //Added on <08/06/2016>
        private string countryName;

        private List<string> sources;

        private string loginCountryCode;

        public bool ImmediateConfirmationOnly
        {
            get { return immediateConfirmationOnly; }
            set { immediateConfirmationOnly = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }

        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }

        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }

        public DateTime TrasnferDate
        {
            get {return trasnferDate; }
            set { trasnferDate = value; }
        }
        public int NumberOfPassengers
        {
            get { return numberOfPassengers; }
            set { numberOfPassengers = value; }
        }
        public string PreferredLanguage
        {
            get { return preferredLanguage; }
            set { preferredLanguage = value; }
        }
        public string AlternateLanguage
        {
            get { return alternateLanguage; }
            set { alternateLanguage = value; }
        }
        public string PickUpCityCode
        {
            get { return pickUpCityCode; }
            set { pickUpCityCode = value; }
        }
        public string PickUpCode
        {
            get { return pickUpCode; }
            set { pickUpCode = value; }
        }
        public string PickUpPointCode
        {
            get { return pickUpPointCode; }
            set { pickUpPointCode = value; }
        }
        public string DropOffCityCode
        {
            get { return dropOffCityCode; }
            set { dropOffCityCode = value; }
        }
        public string DropOffCode
        {
            get { return dropOffCode; }
            set { dropOffCode = value; }
        }
        public string DropOffPointCode
        {
            get { return dropOffPointCode; }
            set { dropOffPointCode = value; }
        }

        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        public string NationalityCode
        {
            get { return nationalityCode; }
            set { nationalityCode = value; }
        }

        public string PickUpTime
        {
            get { return pickUpTime; }
            set { pickUpTime = value; }
        }

        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }

        public PicUp PickUpPoint { get; set; }
        public DropOff DropOffPoint { get; set; }

        public int AnimalLuggage { get; set; }
        public int SportLuggage { get; set; }
        public int Luggage { get; set; }

        public List<string> Sources
        {
            get { return sources; }
            set { sources = value; }
        }
        public string LoginCountryCode
        {
            get { return loginCountryCode; }
            set { loginCountryCode = value; }
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Mobile { get; set; }
        public string Email { get; set; }

        public string PicUpBuffer { get; set; }
        public string VehicleID { get; set; }

        public string StartPointInstruction { get; set; }
        public string EndpointInstruction { get; set; }

        public int ChildSeat1 { get; set; }
        public int ChildSeat2 { get; set; }
        public string VoucherCode { get; set; }

        public TransferRequest CopyByValue()
        {
            TransferRequest tRequest = new TransferRequest();
            tRequest.cityName = cityName;
            tRequest.alternateLanguage = alternateLanguage;
            tRequest.currency = currency;
            tRequest.dropOffCityCode = dropOffCityCode;
            tRequest.dropOffCode = dropOffCode;
            tRequest.dropOffPointCode = dropOffPointCode;
            tRequest.immediateConfirmationOnly = immediateConfirmationOnly;
            tRequest.itemCode = itemCode;
            tRequest.itemName = itemName;
            tRequest.nationality = nationality;
            tRequest.nationalityCode = nationalityCode;
            tRequest.numberOfPassengers = numberOfPassengers;
            tRequest.pickUpCityCode = pickUpCityCode;
            tRequest.pickUpCode = pickUpCode;
            tRequest.pickUpPointCode = pickUpPointCode;
            tRequest.pickUpTime = pickUpTime;
            tRequest.preferredLanguage = preferredLanguage;
            tRequest.trasnferDate = trasnferDate;
            
            return tRequest;
        }
        /// <summary>
        /// This Method is used to get All Transfer Locations
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetTransferLocation()
        {
            //Trace.TraceInformation("TransferRequest.GetTransferLocation entered ");
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            Dictionary<string, string> locList = new Dictionary<string, string>();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetTransferLocations, paramList, connection);
            try
            {
                while(data.Read())
                {
                    locList.Add(data["locationCode"].ToString(), data["locationName"].ToString());
                }
                data.Close();
                connection.Close();
            }
            catch (Exception exp)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Exception in Loading Transfer Location details , Message: " + exp.Message, "");
                data.Close();
                connection.Close();
            }
            //Trace.TraceInformation("TransferRequest.GetTransferLocation exit ");
            return locList;
          
        }

        public class PicUp
        {
            //public string PickUpIATACode { get; set; }
            public string PickUpLatitude { get; set; }
            public string PickUpLongitude { get; set; }
        }
        public class DropOff
        {
            //public string DropOffIATACode { get; set; }
            public string DropOffLatitude { get; set; }
            public string DropOffLongitude { get; set; }
        }

    }
   
}
