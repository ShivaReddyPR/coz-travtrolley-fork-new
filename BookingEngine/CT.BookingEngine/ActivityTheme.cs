﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;


namespace CT.BookingEngine
{
    /// <summary>
    /// Summary description for ActivityTheme
    /// </summary>
    public class ActivityTheme
    {
        static string masterDB = System.Configuration.ConfigurationManager.AppSettings["MasterDB"].ToString();
        public ActivityTheme()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ActivityTheme(string code)
        {
            Load(code);
        }

        int themeId;
        string themeCode;
        string themeName;
        string themeStatus;
        int createdBy;
        DateTime createdOn;
        int modifiedBy;
        DateTime modifiedOn;

        public int ThemeId
        {
            get { return themeId; }
            set { themeId = value; }
        }

        public string ThemeCode
        {
            get { return themeCode; }
            set { themeCode = value; }
        }

        public string ThemeName
        {
            get { return themeName; }
            set { themeName = value; }
        }

        public string ThemeStatus
        {
            get { return themeStatus; }
            set { themeStatus = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }

        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }



        void Load(string code)
        {
            try
            {
                //using (SqlConnection connection = DBGateway.GetConnection())
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    //paramList[0] = new SqlParameter("@themeCode", code);
                    //SqlDataReader reader = DBGateway.ExecuteReaderSP(masterDB+SPNames.GetTheme, paramList, connection);
                    using (DataTable dtTheme = DBGateway.FillDataTableSP(masterDB + SPNames.GetTheme, paramList))
                    {
                        if(dtTheme !=null && dtTheme.Rows.Count > 0)
                        {
                            DataRow reader = dtTheme.Rows[0];
                            themeId = Convert.ToInt32(reader["themeId"]);
                            themeCode = Convert.ToString(reader["themeCode"]);
                            themeName = Convert.ToString(reader["themeName"]);
                            themeStatus = Convert.ToString(reader["themeStatus"]);
                            createdBy = Convert.ToInt32(reader["createdBy"]);
                            createdOn = Convert.ToDateTime(reader["createdOn"]);
                            modifiedBy = Convert.ToInt32(reader["modifiedBy"]);
                            modifiedOn = Convert.ToDateTime(reader["modifiedOn"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Theme Error:"+ex.Message, "0");
            }

        }

    }
}
