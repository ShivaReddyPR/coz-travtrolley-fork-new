using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Net;
using CT.Configuration;
using CT.Core;
using System.IO.Compression;


namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// This class is used to interact with HotelBeds
    /// HotelBookingSource Value for HotelBeds=3
    /// Following is the steps in Booking Flow for this source
    /// STEP-1:HotelSearch(Availability)
    /// STEP-2:Every 15days(what we configuraed in App.config) Need to download StaticData
    /// STEP-3:Loading Cancellation policy
    /// STEP-4:Booking (Booking)
    /// STEP-5:Purchase
    /// STEP-6:Cancel
    /// </summary>
    public class HotelBeds //: Engine
    {
        #region variables
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        private string XmlPath = string.Empty;
        /// <summary>
        /// clientId will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string clientId = string.Empty;
        /// <summary>
        /// email will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string email = string.Empty;
        /// <summary>
        /// password will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string password = string.Empty;
        /// <summary>
        /// language will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string language = string.Empty;
        /// <summary>
        /// interfaceURL will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string interfaceURL = string.Empty;
        /// <summary>
        /// responseURL will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string responseURL = string.Empty;
        /// <summary>
        /// currency will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string currency = string.Empty;
        /// <summary>
        /// country will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string country = string.Empty;
        /// <summary>
        /// itemsPerPage will be sent in each and every request, which is read from api configuration file
        /// </summary>
        int itemsPerPage = 10;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string sessionId;
        //bool testMode = true;
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        private decimal rateOfExchange =1;
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        private Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        private int decimalPoint;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        private string agentCurrency;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        #endregion

        #region properities
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int AgentDecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        #endregion

        /// <summary>
        /// instance of roomTypeDetails
        /// </summary>
        List<string> roomTypeDetails = new List<string>();

        /// <summary>
        /// Default Constructor
        /// also Creating Day wise folder
        /// </summary>
        public HotelBeds()
        {
            Init();
            XmlPath = ConfigurationSystem.HotelBedsConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }

        /// <summary>
        /// To loading initial values, which is read from api config
        /// </summary>
        private void Init()
        {
            //if (ConfigurationSystem.HotelBedsConfig["TestMode"].Equals("true"))
            //{
            //    testMode = true;
            //}
            //else
            //{
            //    testMode = false;
            //}
            interfaceURL = ConfigurationSystem.HotelBedsConfig["InterfaceURL"];
            //clientId = ConfigurationSystem.HotelBedsConfig["ClientID"];
            language = ConfigurationSystem.HotelBedsConfig["lang"];
            email = ConfigurationSystem.HotelBedsConfig["email"];
            password = ConfigurationSystem.HotelBedsConfig["Password"];
            responseURL = ConfigurationSystem.HotelBedsConfig["ResponseURL"];
            country = ConfigurationSystem.HotelBedsConfig["Country"];
            if (ConfigurationSystem.HotelBedsConfig["ItemsPerPage"] != null)
            {
                itemsPerPage = Convert.ToInt16(ConfigurationSystem.HotelBedsConfig["ItemsPerPage"]);
            }
        }


        /// <summary>
        /// parse the date from the incoming string
        /// </summary>
        /// <param name="str">string converting dateFormat</param>
        /// <returns>DateTime</returns>
        private DateTime parseDate(string str)
        {
            int year = int.Parse(str.Substring(0, 4));
            int month = int.Parse(str.Substring(4, 2));
            int day = int.Parse(str.Substring(6, 2));
            int hour = 0;
            int minute = 0;
            if (str.Length > 8)
            {
                hour = int.Parse(str.Substring(8, 2));
                minute = int.Parse(str.Substring(10, 2));
            }

            DateTime dt = new DateTime(year, month, day, hour, minute, 0);
            return dt;
        }

        /// <summary>
        /// read a single xml node
        /// </summary>
        /// <param name="xmlNode">XmlNode</param>
        /// <returns>string</returns>
        /// <remarks>this method is not using right now</remarks>
        private string readXmlNode(XmlNode xmlNode)
        {
            string retStr = "";
            switch (xmlNode.NodeType)
            {
                case XmlNodeType.Element:
                    int n = xmlNode.ChildNodes.Count;
                    for (int i = 0; i < n; i++)
                        retStr += readXmlNode(xmlNode.ChildNodes[i]);
                    break;
                case XmlNodeType.Text:
                    retStr = xmlNode.Value + "|";
                    break;
            }
            return retStr;
        }

        /// <summary>
        /// send a request to the HotelBeds server to get the response
        /// </summary>
        /// <param name="requestXML">Request Object</param>
        /// <returns>XmlDocument</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private XmlDocument SendRequest(string requestXML)
        {
            //string responseFromServer = "";
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(interfaceURL);
                request.Method = "POST"; //Using POST method       
                string postData = requestXML;// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }

                //// handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("HotelBeds-Failed to get response for request : " + ex.Message, ex);
            }
            return xmlDoc;
        }

        /// <summary>
        /// add the value if not exist otherwise replace
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">value</param>
        /// <returns>bool</returns>
        /// <remarks>if key is notexist need to Save data</remarks>
        private bool AddToBasket(string key, object value)
        {
            object v;
            bool exist;
            exist = Basket.BookingSession[sessionId].TryGetValue(key, out v);

            if (exist == true)
            {
                Basket.BookingSession[sessionId].Remove(key);
            }

            Basket.BookingSession[sessionId].Add(key, value);

            return exist;
        }

        /// <summary>
        /// get the value from the basket if exist otherwise null
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>object</returns>
        /// <remarks>if key is exist need to load data</remarks>
        private object GetFromBasket(string key)
        {
            object ret;
            bool exist;
            exist = Basket.BookingSession[sessionId].TryGetValue(key, out ret);

            if (exist == false) return null;

            return ret;
        }


        /// <summary>
        /// get the available hotel results for the given req
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">This is B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        public HotelSearchResult[] GetHotelAvailability(HotelRequest req, decimal markup, string markupType)
        {
            HotelSearchResult[] resultValues;
            //Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 1, "HotelBeds.GetHotelAvailability entered", "0");

            int pageIndex = 1, totalPage = 0;
            int totalResultToShow = Configuration.ConfigurationSystem.ResultCountBySources["HotelBeds"];
            int resultCount = 0;
            HotelSearchResult[] searchRes = new HotelSearchResult[0];

            DataTable dtHotels = HotelStaticData.GetStaticHotelIds(req.CityCode, HotelBookingSource.HotelBeds);

            //for now we are requesting only the config driven result when paging will be implemented then show all
            while (pageIndex == 1 || (pageIndex <= totalPage && resultCount < totalResultToShow))
            {
                string request = GenerateAvailabilityRequest(req, pageIndex);
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(request);
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsSearchRequest.xml";
                    xmlDoc.Save(filePath);
                    //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsSearchRequest.xml");
                }
                catch { }
                //Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 1, "HotelBeds request message generated PageIndex=" + pageIndex.ToString(), "0");
                //string resp = string.Empty;
                XmlDocument xmlResp = new XmlDocument();
                try
                {
                    xmlResp = SendRequest(request);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, "Exception returned from HotelBeds.GetHotelAvailability Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                    ////Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        HotelSearchResult[] tempResult = GenerateSearchResult(xmlResp, req, ref pageIndex, ref totalPage, markup, markupType, dtHotels);
                        //HotelSearchResult[] partialSearchRes = GetFilteredResult(req, tempResult);

                        if (pageIndex == 1)
                        {
                            searchRes = new HotelSearchResult[itemsPerPage * totalPage];
                        }
                        foreach (HotelSearchResult partialSearch in tempResult)
                        {
                            searchRes[resultCount++] = partialSearch;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, "Exception returned from HotelBeds.GetHotelAvailability Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                    //Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.ToString());
                }
                finally
                {
                    Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 1, "HotelBeds Response for PageIndex=" + pageIndex.ToString() + "requestXML : " + request + " responseXML : " + xmlResp.OuterXml, "0");
                }
                pageIndex++;
            }
            resultValues = new HotelSearchResult[resultCount];
            for (int i = 0; i < resultCount; i++)
            {
                resultValues[i] = searchRes[i];
            }
            return resultValues;
        }

        /// <summary>
        ///This method is converting ContentEncoding to Decompress(i.e GZIP, Deflate, default)
        /// </summary>
        /// <param name="webResponse">Response Object</param>
        /// <returns>Stream</returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }

        /// <summary>
        /// generate the availability request page by page
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="pageIndex">pageIndex(itemsPerPage)</param>
        /// <returns>string</returns>
        private string GenerateAvailabilityRequest(HotelRequest req, int pageIndex)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("HotelValuedAvailRQ", "http://www.hotelbeds.com/schemas/2005/06/messages");
            xmlString.WriteAttributeString("echoToken", "DummyEchoToken");
            xmlString.WriteAttributeString("sessionId", "DummySessionId");
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance ../xsd/HotelValuedAvailRQ.xsd");
            //xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.hotelbeds.com/schemas/2005/06/messages ../xsd/HotelValuedAvailRQ.xsd");
            xmlString.WriteAttributeString("version", "2013/12");
            xmlString.WriteAttributeString("showDiscountsList", "Y");
            //writing language info
            xmlString.WriteElementString("Language", language);
            //writing credential info, username/password
            xmlString.WriteStartElement("Credentials");
            xmlString.WriteElementString("User", email);
            xmlString.WriteElementString("Password", password);
            xmlString.WriteEndElement();
            //order by price low to high
            xmlString.WriteStartElement("ExtraParamList");
            xmlString.WriteStartElement("ExtendedData");
            xmlString.WriteAttributeString("type", "EXT_ORDER");
            xmlString.WriteElementString("Name", "ORDER_CONTRACT_PRICE");
            xmlString.WriteElementString("Value", "ASC");
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ExtendedData");
            xmlString.WriteAttributeString("type", "EXT_DISPLAYER");
            xmlString.WriteElementString("Name", "DISPLAYER_DEFAULT");
            xmlString.WriteElementString("Value", "PROMOTION:Y");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            //pagenation data
            xmlString.WriteStartElement("PaginationData");
            xmlString.WriteAttributeString("pageNumber", pageIndex.ToString());
            xmlString.WriteAttributeString("itemsPerPage", itemsPerPage.ToString());
            xmlString.WriteEndElement();

            //xmlString.WriteElementString("ShowDirectPayment", "Y");

            //checkin date
            xmlString.WriteStartElement("CheckInDate");
            xmlString.WriteAttributeString("date", req.StartDate.ToString("yyyyMMdd"));
            xmlString.WriteEndElement();
            //checkout date
            xmlString.WriteStartElement("CheckOutDate");
            xmlString.WriteAttributeString("date", req.EndDate.ToString("yyyyMMdd"));
            xmlString.WriteEndElement();
            //Destination Info
            xmlString.WriteStartElement("Destination");
            xmlString.WriteAttributeString("type", "SIMPLE");
            xmlString.WriteAttributeString("code", req.CityCode);
            xmlString.WriteEndElement();
            //Occupation List
            xmlString.WriteStartElement("OccupancyList");

            List<RoomGuestData> roomsList = new List<RoomGuestData>();
            for (int p = 0; p < req.RoomGuest.Length; p++)
            {
                RoomGuestData room = new RoomGuestData();
                room.childAge = new List<int>();
                room.childAge.AddRange(req.RoomGuest[p].childAge);
                room.noOfAdults = req.RoomGuest[p].noOfAdults;
                room.noOfChild = req.RoomGuest[p].noOfChild;
                roomsList.Add(room);
            }
            Dictionary<RoomGuestData, int> filteredroomsList = new Dictionary<RoomGuestData, int>();
            foreach (RoomGuestData container in roomsList)
            {
                List<RoomGuestData> duplicateRoom = roomsList.FindAll(delegate(RoomGuestData checkRoom)
                { return (checkRoom.noOfAdults == container.noOfAdults && checkRoom.noOfChild == container.noOfChild); });
                if (!filteredroomsList.ContainsKey(duplicateRoom[0]))
                {
                    filteredroomsList.Add(duplicateRoom[0], duplicateRoom.Count);
                }
                else
                {
                    foreach (RoomGuestData roomGuest in filteredroomsList.Keys)
                    {
                        if (container.noOfAdults == roomGuest.noOfAdults && container.noOfChild == roomGuest.noOfChild)
                        {
                            roomGuest.childAge.AddRange(container.childAge);
                        }
                    }
                }
            }
            foreach(KeyValuePair<RoomGuestData, int> roomGuest in filteredroomsList)
            {
                RoomGuestData roomGuestData = roomGuest.Key;
                xmlString.WriteStartElement("HotelOccupancy");
                xmlString.WriteElementString("RoomCount", roomGuest.Value.ToString());
                xmlString.WriteStartElement("Occupancy");
                xmlString.WriteElementString("AdultCount", roomGuestData.noOfAdults.ToString());
                xmlString.WriteElementString("ChildCount", roomGuestData.noOfChild.ToString());
                if (roomGuestData.noOfChild > 0)
                {
                    xmlString.WriteStartElement("GuestList");
                    foreach (int childAge in roomGuestData.childAge)
                    {
                        xmlString.WriteStartElement("Customer");
                        xmlString.WriteAttributeString("type", "CH");
                        xmlString.WriteElementString("Age", childAge.ToString());
                        xmlString.WriteEndElement();
                    }
                    xmlString.WriteEndElement();
                }
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
            }
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// generate search result from the incoming reponse page by page
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="pageIndex">HotelSearchResult[]</param>
        /// <param name="totalPage">totalPage</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">This is B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <param name="dtHotels">TotalHotels based on the city wise Loading from DB</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here only we are calucating B2B markup and InputVat
        ///   Here source Currency means supplierCurrency and source amount means supplier Amount
        /// </remarks>
        private HotelSearchResult[] GenerateSearchResult(XmlDocument xmlDoc, HotelRequest request, ref int pageIndex, ref int totalPage, decimal markup, string markupType, DataTable dtHotels)
        {
            HotelSearchResult[] hotelResults;

            int index, hotelCount = 0;
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsSearchResponse.xml";
                xmlDoc.Save(filePath);
                //xmlDoc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsSearchResponse.xml");

                //Audit.Add(EventType.HotelSearch, Severity.Normal, 1, filePath, "127.0.0.1");
            }
            catch { }

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("HARS", "http://www.hotelbeds.com/schemas/2005/06/messages");

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//HARS:HotelValuedAvailRS/HARS:ErrorList/HARS:Error/HARS:Message/text()", nsmgr);
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, " HotelBeds:GenerateSearchResult,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }

            //Pagination Data
            tempNode = xmlDoc.SelectSingleNode("//HARS:HotelValuedAvailRS/HARS:PaginationData", nsmgr);
            pageIndex = int.Parse(tempNode.Attributes["currentPage"].Value);
            totalPage = int.Parse(tempNode.Attributes["totalPages"].Value);
            int totResultToShow = Configuration.ConfigurationSystem.ResultCountBySources["HotelBeds"]; ;
            //Hotel Result Data
            XmlNodeList hotelList = xmlDoc.SelectNodes("//HARS:HotelValuedAvailRS/HARS:ServiceHotel", nsmgr);
            if (hotelList.Count >= totResultToShow)
            {
                hotelResults = new HotelSearchResult[totResultToShow];
            }
            else
            {
                hotelResults = new HotelSearchResult[hotelList.Count];
            }

            Dictionary<string, List<object>> contractList;
            Dictionary<string, HotelRating> hotelRatingList;

            object objCList = GetFromBasket("contractList");
            if (objCList == null)
            {
                contractList = new Dictionary<string, List<object>>();
            }
            else
            {
                contractList = (Dictionary<string, List<object>>)objCList;
            }
            objCList = GetFromBasket("hotelRating");
            if (objCList == null)
            {
                hotelRatingList = new Dictionary<string, HotelRating>();
            }
            else
            {
                hotelRatingList = (Dictionary<string, HotelRating>)objCList;
            }
            string availToken = "";
            string pattern = request.HotelName;
            string expr = string.Empty;
            //price list will be used in case of different rate for same hotel
            Dictionary<string, List<decimal>> priceList = new Dictionary<string, List<decimal>>();
            PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
            foreach (XmlNode hotelNode in hotelList)
            {
                HotelSearchResult hotel = new HotelSearchResult();
                //Hotel Code & Name
                tempNode = hotelNode.SelectSingleNode("HARS:HotelInfo/HARS:Code/text()", nsmgr);
                hotel.HotelCode = tempNode.Value;
                tempNode = hotelNode.SelectSingleNode("HARS:HotelInfo/HARS:Name/text()", nsmgr);
                hotel.HotelName = tempNode.Value;
                //Hotel Rating
                tempNode = hotelNode.SelectSingleNode("HARS:HotelInfo/HARS:Category", nsmgr);
                if (tempNode != null)
                {
                    HotelRating rating;

                    if (hotelRatingList.TryGetValue(hotel.HotelCode, out rating) == false)
                    {
                        string str = tempNode.Attributes["code"].Value;
                        switch (str)
                        {
                            case "1EST": rating = HotelRating.OneStar;
                                break;
                            case "2EST": rating = HotelRating.TwoStar;
                                break;
                            case "3EST": rating = HotelRating.ThreeStar;
                                break;
                            case "4EST": rating = HotelRating.FourStar;
                                break;
                            case "5EST": rating = HotelRating.FiveStar;
                                break;
                            default: rating = HotelRating.All;
                                break;
                        }
                        hotelRatingList.Add(hotel.HotelCode, rating);
                    }
                    hotel.Rating = rating;
                    hotel.HotelCategory = tempNode.InnerText;
                }
                if (Convert.ToInt16(hotel.Rating) >= (int)request.MinRating && Convert.ToInt16(hotel.Rating) <= (int)request.MaxRating)
                {
                    if (!(pattern != null && pattern.Length > 0 ? hotel.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }
                //booking source for the search result
                hotel.BookingSource = HotelBookingSource.HotelBeds;
                //Start date & end date
                tempNode = hotelNode.SelectSingleNode("HARS:DateFrom", nsmgr);
                hotel.StartDate = parseDate(tempNode.Attributes["date"].Value);
                tempNode = hotelNode.SelectSingleNode("HARS:DateTo", nsmgr);
                hotel.EndDate = parseDate(tempNode.Attributes["date"].Value);
                //Currency
                tempNode = hotelNode.SelectSingleNode("HARS:Currency", nsmgr);
                hotel.Currency = tempNode.Attributes["code"].Value;
                //City Code
                tempNode = hotelNode.SelectSingleNode("HARS:HotelInfo/HARS:Destination", nsmgr);
                hotel.CityCode = tempNode.Attributes["code"].Value;
                //store availToken
                if (availToken == "")
                {
                    availToken = hotelNode.Attributes["availToken"].Value;
                }
                //Hotel Thumbnail Picture
                tempNode = hotelNode.SelectSingleNode("HARS:HotelInfo/HARS:ImageList/HARS:Image/HARS:Url/text()", nsmgr);
                if (tempNode != null)
                {
                    hotel.HotelPicture = tempNode.Value;
                }
                //Hotel Address
                tempNode = hotelNode.SelectSingleNode("HARS:HotelInfo/HARS:Destination/HARS:Name/text()", nsmgr);


                #region Modified by brahmam Avoid Downloading static data
                //hotel.HotelAddress = tempNode.Value;
                //Get the Hotel Static data,fill it if not exixt
                //HotelDetails hotelInfo = new HotelDetails();
                //hotelInfo = GetItemInformation(hotel.CityCode, hotel.HotelName, hotel.HotelCode);
                DataRow[] hotelStaticData = new DataRow[0];
                try
                {
                    hotelStaticData = dtHotels.Select("hotelCode='" + hotel.HotelCode + "'");
                    if (hotelStaticData != null && hotelStaticData.Length > 0)
                    {
                        //if (hotelStaticData.Length <= 0)
                        //{
                        //    hotelInfo = GetItemInformation(hotel.CityCode, hotel.HotelName, hotel.HotelCode);
                        //}
                        // Bug Id: 0029983  set complete Address on voucher
                        hotel.HotelAddress = hotelStaticData[0]["address"].ToString();//hotelInfo.Address;
                        //if (string.IsNullOrEmpty(hotelInfo.HotelCode))
                        //if (hotelStaticData.Length <= 0 && string.IsNullOrEmpty(hotelInfo.HotelCode))
                        //{
                        //    Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 1, "HotelDetail Not Available for : City Code = " + hotel.CityCode + " , Hotel Name = " + hotel.HotelName + " Hotel Code = " + hotel.HotelCode, "0");
                        //    continue;//dont include the hotel whose item info is not available
                        //}
                        //hotel.HotelPicture = hotelInfo.Image;
                        hotel.HotelDescription = hotelStaticData[0]["description"].ToString();//hotelInfo.Description;
                        hotel.HotelMap = hotelStaticData[0]["Location"].ToString();//hotelInfo.Map;
                    }
                }
                catch { continue; }
                #endregion

                //Guest List
                XmlNodeList roomNodeList = hotelNode.SelectNodes("HARS:AvailableRoom", nsmgr);
                hotel.RoomDetails = new HotelRoomsDetails[roomNodeList.Count];
                index = 0;

                hotel.RoomGuest = request.RoomGuest;
                rateOfExchange = exchangeRates[hotel.Currency];
                int roomCount = 0;
                foreach (XmlNode roomNode in roomNodeList)
                {
                    //Occupancy Data
                    hotel.RoomDetails[index].Occupancy = new Dictionary<string, int>();
                    tempNode = roomNode.SelectSingleNode("HARS:HotelOccupancy/HARS:RoomCount/text()", nsmgr);
                    roomCount = Convert.ToInt32(tempNode.Value);
                    hotel.RoomDetails[index].Occupancy.Add("RoomCount", int.Parse(tempNode.Value));
                    tempNode = roomNode.SelectSingleNode("HARS:HotelOccupancy/HARS:Occupancy/HARS:AdultCount/text()", nsmgr);
                    hotel.RoomDetails[index].Occupancy.Add("AdultCount", int.Parse(tempNode.Value));
                    tempNode = roomNode.SelectSingleNode("HARS:HotelOccupancy/HARS:Occupancy/HARS:ChildCount/text()", nsmgr);
                    hotel.RoomDetails[index].Occupancy.Add("ChildCount", int.Parse(tempNode.Value));
                    hotel.RoomDetails[index].SequenceNo = GetSequenceNo(hotel.RoomDetails[index].Occupancy, request);
                    //Hotel Room Data
                    string roomTypeCode = "";
                    tempNode = roomNode.SelectSingleNode("HARS:HotelRoom", nsmgr);
                    string SHRUI = tempNode.Attributes["SHRUI"].Value;
                    //Board Type
                    tempNode = roomNode.SelectSingleNode("HARS:HotelRoom/HARS:Board", nsmgr);
                    string boardType = tempNode.Attributes["type"].Value;
                    string boardCode = tempNode.Attributes["code"].Value;
                    string boardShortName = tempNode.Attributes["shortname"].Value;
                    tempNode = roomNode.SelectSingleNode("HARS:HotelRoom/HARS:Board/text()", nsmgr);
                    string boardText = tempNode.Value;
                    //Hotel Info
                    tempNode = roomNode.SelectSingleNode("HARS:HotelRoom/HARS:RoomType", nsmgr);
                    hotel.RoomDetails[index].RoomTypeName = tempNode.SelectSingleNode("text()", nsmgr).Value;
                    List<string> facilities = new List<string>();

                    hotel.RoomDetails[index].mealPlanDesc = boardText;
                    hotel.RoomDetails[index].Amenities = facilities;//hotelInfo.RoomFacilities;
                    roomTypeCode = boardType + "|" + boardCode + "|" + boardShortName + "|" + tempNode.Attributes["code"].Value + "|" + tempNode.Attributes["characteristic"].Value + "|" + SHRUI;
                    hotel.RoomDetails[index].RoomTypeCode = roomTypeCode;
                    //Rate plan code for this hotel
                    hotel.RoomDetails[index].RatePlanCode = roomTypeCode + "|" + request.CityCode;
                    tempNode = roomNode.SelectSingleNode("HARS:HotelRoom/HARS:Price/HARS:Amount/text()", nsmgr);

                    //VAT Calucation Changes Modified 14.12.2017
                    decimal hotelTotalPrice = 0m;
                    decimal vatAmount = 0m;
                    hotelTotalPrice = Math.Round(((Decimal.Parse(tempNode.Value) / roomCount) * rateOfExchange), decimalPoint);

                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                    {
                        hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                    }
                    hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                    hotel.RoomDetails[index].TotalPrice = hotelTotalPrice;
                    hotel.RoomDetails[index].Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(hotel.RoomDetails[index].TotalPrice) * (markup / 100m)));
                    hotel.RoomDetails[index].MarkupType = markupType;
                    hotel.RoomDetails[index].MarkupValue = markup;
                    hotel.RoomDetails[index].SellingFare = hotel.RoomDetails[index].TotalPrice;
                    hotel.RoomDetails[index].supplierPrice = (Decimal.Parse(tempNode.Value) / roomCount);
                    hotel.RoomDetails[index].TaxDetail = new PriceTaxDetails();
                    hotel.RoomDetails[index].TaxDetail = priceTaxDet;
                    hotel.RoomDetails[index].InputVATAmount = vatAmount;
                    hotel.Price = new PriceAccounts();
                    hotel.Price.SupplierCurrency = hotel.Currency;
                    hotel.Price.SupplierPrice = (Decimal.Parse(tempNode.Value) / roomCount);
                    hotel.Price.RateOfExchange = rateOfExchange;

                    System.TimeSpan diffResult = hotel.EndDate.Subtract(hotel.StartDate);
                    RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                    decimal totalprice = hotel.RoomDetails[index].TotalPrice;

                    for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                    {
                        decimal price = hotel.RoomDetails[index].TotalPrice / diffResult.Days;
                        if (fareIndex == diffResult.Days - 1)
                        {
                            price = totalprice;
                        }
                        totalprice -= price;
                        hRoomRates[fareIndex].Amount = price;
                        hRoomRates[fareIndex].BaseFare = price;
                        hRoomRates[fareIndex].SellingFare = price;
                        hRoomRates[fareIndex].Totalfare = price;
                        hRoomRates[fareIndex].RateType = RateType.Negotiated;
                        hRoomRates[fareIndex].Days = hotel.StartDate.AddDays(fareIndex);

                    }
                    hotel.RoomDetails[index].Rates = hRoomRates;
                    index++;
                }
                
                //hotel.Currency = "AED";
                hotel.Currency = agentCurrency;
                List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();
                foreach (HotelRoomsDetails roomDetail in hotel.RoomDetails)
                {
                    if (roomDetail.SequenceNo.Contains("|"))
                    {
                        string[] sequences = roomDetail.SequenceNo.Split('|');
                        foreach (string sequence in sequences)
                        {
                            HotelRoomsDetails rd = new HotelRoomsDetails();
                            rd = roomDetail;
                            rd.RoomTypeCode = rd.RoomTypeCode + "|" + sequence;
                            rd.SequenceNo = sequence;
                            int z = 0;
                            foreach (HotelRoomsDetails roomData in roomDetails)
                            {
                                if (roomData.RoomTypeCode == rd.RoomTypeCode && roomData.SequenceNo == rd.SequenceNo)
                                {
                                    z++;
                                }
                            }
                            if (z == 0)
                            {
                                roomDetails.Add(rd);
                            }
                        }
                    }
                    else
                    {
                        HotelRoomsDetails rd = new HotelRoomsDetails();
                        rd = roomDetail;
                        rd.RoomTypeCode = rd.RoomTypeCode + "|" + rd.SequenceNo;
                        roomDetails.Add(rd);
                    }
                }
                hotel.RoomDetails = roomDetails.ToArray();
                Array.Sort(hotel.RoomDetails, delegate(HotelRoomsDetails rd1, HotelRoomsDetails rd2) { return rd1.TotalPrice.CompareTo(rd2.TotalPrice); });
                bool isDuplicateResult = false;
                List<decimal> priceInfo = new List<decimal>();
                //Contract list
                XmlNodeList contractNodes = hotelNode.SelectNodes("HARS:ContractList/HARS:Contract", nsmgr);
                foreach (XmlNode contractNode in contractNodes)
                {
                    Dictionary<string, object> newContract = new Dictionary<string, object>();
                    tempNode = contractNode.SelectSingleNode("HARS:Name/text()", nsmgr);
                    if (tempNode != null)
                    {
                        newContract["Name"] = tempNode.Value;
                    }
                    tempNode = contractNode.SelectSingleNode("HARS:IncomingOffice", nsmgr);
                    if (tempNode != null)
                    {
                        newContract["IncomingOffice"] = tempNode.Attributes["code"].Value;
                    }
                    tempNode = contractNode.SelectSingleNode("HARS:Classification/text()", nsmgr);
                    if (tempNode != null)
                    {
                        newContract["Classification"] = tempNode.Value;
                    }
                    if (contractList.ContainsKey(hotel.HotelCode) && priceList.ContainsKey(hotel.HotelCode))
                    {
                        priceInfo = priceList[hotel.HotelCode];
                        if (priceInfo[0] <= hotel.RoomDetails[0].TotalPrice)
                        {
                            isDuplicateResult = true;
                            break;//do nothing
                        }
                        else
                        {
                            contractList[hotel.HotelCode] = new List<object>();
                            contractList[hotel.HotelCode].Add(newContract);
                            //replace the previous result
                            hotelResults[(int)priceInfo[1]] = hotel;
                        }
                    }
                    else
                    {
                        contractList[hotel.HotelCode] = new List<object>();
                        contractList[hotel.HotelCode].Add(newContract);
                        if (!priceList.ContainsKey(hotel.HotelCode))
                        {
                            priceInfo.Add(hotel.RoomDetails[0].TotalPrice);
                            priceInfo.Add((decimal)hotelCount);
                            priceList.Add(hotel.HotelCode, priceInfo);
                        }
                    }
                }
                tempNode = hotelNode.SelectSingleNode("HARS:PromotionList", nsmgr);
                string offerText = string.Empty;
                if (tempNode != null)
                {
                    XmlNodeList offers = tempNode.SelectNodes("HARS:Promotion", nsmgr);
                    foreach (XmlNode offer in offers)
                    {
                        offerText = offer.SelectSingleNode("HARS:Observations", nsmgr).InnerText;
                    }
                }
                if (offerText.Length > 0)
                {
                    hotel.PromoMessage = offerText;
                }
                //add only if not duplicating result
                if (!isDuplicateResult)
                {
                    if (hotelCount < hotelResults.Length)
                    {
                        hotelResults[hotelCount] = hotel;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    continue;
                }

                hotelCount++;
            }


            AddToBasket("availToken", availToken);
            AddToBasket("contractList", contractList);
            AddToBasket("hotelRating", hotelRatingList);
            
            if (hotelResults.Length > hotelCount)
            {
                Array.Resize(ref hotelResults, hotelCount);
            }
            
            foreach (HotelSearchResult hotelResult in hotelResults)
            {
                
                for (int i = 0; i < request.NoOfRooms; i++)
                {
                    for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                    {
                        if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                        {
                            hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                            hotelResult.Price.NetFare = hotelResult.TotalPrice;
                            hotelResult.Price.AccPriceType = PriceType.NetFare;                            
                            break;
                        }
                    }
                }
            }
            return hotelResults;
        }

        /// <summary>
        /// GetSequenceNo
        /// </summary>
        /// <param name="dictionary">dictionary(Adult and ChildCount)</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <returns>string</returns>
        /// <remarks>Based on the sequence no only need to to diffrenative rooms</remarks>
        private string GetSequenceNo(Dictionary<string, int> dictionary, HotelRequest request)
        {
            string seqNo = string.Empty;
            for (int i = 0; i < request.RoomGuest.Length; i++)
            {
                if (request.RoomGuest[i].noOfAdults == dictionary["AdultCount"])
                {
                    if (request.RoomGuest[i].noOfChild == dictionary["ChildCount"])
                    {
                        if (seqNo.Length > 0) seqNo += "|";
                        seqNo += (i + 1);
                    }
                }
            }
            return seqNo;
        }

        /// <summary>
        /// generate the request to get the item information
        /// </summary>
        /// <param name="hotelcode">Selected HotelCode</param>
        /// <returns>string</returns>
        private string GenerateGetItemInformation(string hotelcode)
        {

            StringBuilder strWriter = new StringBuilder();

            XmlWriter xmlString = XmlTextWriter.Create(strWriter);

            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("HotelDetailRQ", "http://www.hotelbeds.com/schemas/2005/06/messages");
            xmlString.WriteAttributeString("echoToken", "DummyEchoToken");
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.hotelbeds.com/schemas/2005/06/messages ../xsd/HotelDetailRQ.xsd");
            xmlString.WriteAttributeString("version", "2013/12");

            xmlString.WriteElementString("Language", language);
            //writing credential info, username/password
            xmlString.WriteStartElement("Credentials");
            xmlString.WriteElementString("User", email);
            xmlString.WriteElementString("Password", password);
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("HotelCode");
            xmlString.WriteString(hotelcode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.Close();

            return strWriter.ToString();
        }

        /// <summary>
        /// Parse the response xml and returns the HotelDetail
        /// </summary>
        /// <param name="xmlDoc">Xml Response</param>
        /// <returns>HotelDetails</returns>
        private HotelDetails ReadGetItemInformationResponse(XmlDocument xmlDoc)
        {
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
           
            HotelDetails hotel = new HotelDetails();
            try
            {

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("HDRS", "http://www.hotelbeds.com/schemas/2005/06/messages");


                XmlNode hotelInfo = xmlDoc.SelectSingleNode("//HDRS:HotelDetailRS/HDRS:Hotel", nsmgr);

                string hotelType = hotelInfo.Attributes["xsi:type"].Value;

                tempNode = xmlDoc.SelectSingleNode("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:Code", nsmgr);
                string HotelCode = tempNode.FirstChild.Value;
                hotel.HotelCode = HotelCode;

                //star rating
                //Dictionary<string, HotelRating> hotelRatingList;
                //object objCList = GetFromBasket("hotelRating");
                //if (objCList == null)
                //{
                //    hotelRatingList = new Dictionary<string, HotelRating>();
                //}
                //else
                //{
                //    hotelRatingList = (Dictionary<string, HotelRating>)objCList;
                //}
                //if (hotelRatingList.TryGetValue(hotel.HotelCode, out hotel.hotelRating) == false)
                //{
                //    hotel.hotelRating = HotelRating.All;
                //}

                tempNode = xmlDoc.SelectSingleNode("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:Name", nsmgr);
                string HotelName = tempNode.FirstChild.Value;
                hotel.HotelName = HotelName;

                XmlNodeList tempNodeList = xmlDoc.SelectNodes("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:DescriptionList/HDRS:Description", nsmgr);
                foreach (XmlNode temp in tempNodeList)
                {
                    string desc_type = temp.Attributes["type"].Value;
                    string Description = temp.FirstChild.Value;
                    hotel.Description += desc_type + ":" + Description + " | ";
                }

                XmlNodeList FeatureList = xmlDoc.SelectNodes("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:FacilityList/HDRS:Feature", nsmgr);
                List<string> FacilityList = new List<string>();
                Dictionary<string, string> AttractionList = new Dictionary<string, string>();
                Dictionary<string, string> dicFacilityList = new Dictionary<string, string>();
                string featureType;
                for (int i = 0; i < FeatureList.Count; i++)
                {
                    featureType = FeatureList[i].Attributes["xsi:type"].Value;
                    if (featureType == "ProductFeatureDistance")
                    {
                        string key = string.Empty;
                        string value = string.Empty;
                        tempNode = FeatureList[i].SelectSingleNode("HDRS:Description/text()", nsmgr);
                        if (tempNode != null)
                        {
                            key = tempNode.Value;
                        }
                        tempNode = FeatureList[i].SelectSingleNode("HDRS:DistanceList/HDRS:Distance", nsmgr);
                        if (tempNode != null)
                        {
                            value = tempNode.FirstChild.Value + tempNode.Attributes["unit"].Value;
                        }
                        if (key.Length > 0)
                        {
                            AttractionList.Add(key, value);
                        }
                    }
                    else
                    {
                        string featureFee = null;
                        tempNode = FeatureList[i].Attributes["fee"];
                        if (tempNode != null)
                        {
                            featureFee = tempNode.Value;
                        }
                        string tempFacility = string.Empty;
                        if (dicFacilityList.TryGetValue(featureType, out tempFacility) == true)
                        {
                            dicFacilityList.Remove(featureType);
                        }
                        string facility = string.Empty;
                        tempNode = FeatureList[i].SelectSingleNode("HDRS:Description/text()", nsmgr);
                        if (tempNode != null)
                        {
                            if (featureFee != null)
                            {
                                if (featureFee == "Y" || featureFee == "S")
                                {
                                    facility = tempNode.Value + "<label style='font-size: 11px; color: Red'>(*)</label>";
                                }
                                else
                                {
                                    facility = tempNode.Value;
                                }
                            }
                            else
                            {
                                facility = tempNode.Value;
                            }
                            if (tempFacility != string.Empty && tempFacility != "" && tempFacility != null) tempFacility += " , ";
                            tempFacility += facility;
                        }
                        dicFacilityList.Add(featureType, tempFacility);
                    }
                }
                foreach (string facilityKey in dicFacilityList.Keys)
                {
                    FacilityList.Add(dicFacilityList[facilityKey]);
                }
                hotel.HotelFacilities = FacilityList;
                hotel.Attractions = AttractionList;

                XmlNodeList ImageList = xmlDoc.SelectNodes("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:ImageList/HDRS:Image", nsmgr);
                List<string> Images = new List<string>();
                string image = string.Empty;
                for (int i = 0; i < ImageList.Count; i++)
                {
                    XmlNode temp_url_Node = ImageList[i].SelectSingleNode("HDRS:Url", nsmgr);
                    string temp = temp_url_Node.FirstChild.Value;
                    Images.Add(temp);
                }
                hotel.Images = Images;
                hotel.Image = (Images.Count > 0) ? Images[0] : " ";

                //destination
                tempNode = xmlDoc.SelectSingleNode("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:Destination", nsmgr);
                string destination_code = tempNode.Attributes["code"].Value;
                tempNode = tempNode.SelectSingleNode("HDRS:Name", nsmgr);
                string destination_name = tempNode.FirstChild.Value;



                XmlNode contactInfo = xmlDoc.SelectSingleNode("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:Contact", nsmgr);
                //address
                tempNode = contactInfo.SelectSingleNode("HDRS:Address/HDRS:StreetTypeId", nsmgr);
                string StreetTypeId = (tempNode != null && tempNode.InnerText.Length > 0) ? tempNode.FirstChild.Value : " ";
                tempNode = contactInfo.SelectSingleNode("HDRS:Address/HDRS:StreetTypeName", nsmgr);
                string StreetTypeName = (tempNode != null && tempNode.InnerText.Length > 0) ? tempNode.FirstChild.Value : " ";
                tempNode = contactInfo.SelectSingleNode("HDRS:Address/HDRS:StreetName", nsmgr);
                string StreetName = (tempNode != null && tempNode.InnerText.Length > 0) ? tempNode.FirstChild.Value : " ";
                tempNode = contactInfo.SelectSingleNode("HDRS:Address/HDRS:Number", nsmgr);
                string Number = (tempNode != null && tempNode.InnerText.Length > 0) ? tempNode.FirstChild.Value : " ";
                tempNode = contactInfo.SelectSingleNode("HDRS:Address/HDRS:PostalCode", nsmgr);
                string PostalCode = (tempNode != null && tempNode.InnerText.Length > 0) ? tempNode.FirstChild.Value : " ";
                tempNode = contactInfo.SelectSingleNode("HDRS:Address/HDRS:City", nsmgr);
                string City = (tempNode != null && tempNode.InnerText.Length > 0) ? tempNode.FirstChild.Value : " ";
                tempNode = contactInfo.SelectSingleNode("HDRS:Address/HDRS:State", nsmgr);
                string State = (tempNode != null && tempNode.InnerText.Length > 0) ? tempNode.FirstChild.Value : " ";
                tempNode = contactInfo.SelectSingleNode("HDRS:Address/HDRS:CountryCode", nsmgr);
                string CountryCode = (tempNode != null && tempNode.InnerText.Length > 0) ? tempNode.FirstChild.Value : " ";
                // Bug id: 0029983 correction of address , remove Comma(,) and dot(.)                
                if (StreetTypeId != " " && StreetTypeId != ".")
                {
                     hotel.Address=StreetTypeId+",";
                }
                if (StreetTypeName != " " && StreetTypeName != ".")
                {
                     hotel.Address=hotel.Address+StreetTypeName+",";
                }
                if (StreetName != " " && StreetName != ".")
                {
                     hotel.Address=hotel.Address+StreetName+",";
                }
                if (Number != " " && Number != ".")
                {
                     hotel.Address=hotel.Address+Number+",";
                }
                if (PostalCode != " " && PostalCode != ".")
                {
                     hotel.Address=hotel.Address+PostalCode+",";
                }
                if (City != " " && City != ".")
                {
                    hotel.Address = hotel.Address + City + ",";
                }
                if (State != " " && State != ".")
                {
                     hotel.Address=hotel.Address+State+",";
                }
                if (CountryCode != " " && CountryCode != ".")
                {
                     hotel.Address=hotel.Address+CountryCode;
                }
                
                    //hotel.Address = StreetTypeId + "," + StreetTypeName + ", " + StreetName + " " + Number + ", " + PostalCode + "," + City + " " + State + " " + CountryCode;
                
               
                //email list
                tempNodeList = contactInfo.SelectNodes("HDRS:EmailList/HDRS:Email", nsmgr);
                string eMailString = string.Empty;
                foreach (XmlNode temp in tempNodeList)
                {
                    if (eMailString.Length > 0)
                    {
                        eMailString += " , ";
                    }
                    eMailString += temp.FirstChild.Value;
                }
                hotel.Email = eMailString;
                //phone list
                tempNodeList = contactInfo.SelectNodes("HDRS:PhoneList/HDRS:ContactNumber", nsmgr);
                string phoneString = string.Empty;
                foreach (XmlNode temp in tempNodeList)
                {
                    if (phoneString.Length > 0)
                    {
                        phoneString += " , ";
                    }
                    phoneString += temp.FirstChild.Value;
                }
                hotel.PhoneNumber = phoneString;
                //Fax list
                tempNodeList = contactInfo.SelectNodes("HDRS:FaxList/HDRS:ContactNumber", nsmgr);
                foreach (XmlNode temp in tempNodeList)
                {
                    hotel.FaxNumber += temp.FirstChild.Value + " ";
                }
                //web list
                tempNodeList = contactInfo.SelectNodes("HDRS:WebList/HDRS:Web", nsmgr);
                foreach (XmlNode temp in tempNodeList)
                {
                    hotel.URL += temp.FirstChild.Value + " ";
                }


                //Added by brahmam
                tempNode = xmlDoc.SelectSingleNode("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:Category", nsmgr);
                if (tempNode != null)
                {
                    string str = tempNode.Attributes["code"].Value;
                    switch (str)
                    {
                        case "1EST": hotel.hotelRating = HotelRating.OneStar;
                            break;
                        case "2EST": hotel.hotelRating = HotelRating.TwoStar;
                            break;
                        case "3EST": hotel.hotelRating = HotelRating.ThreeStar;
                            break;
                        case "4EST": hotel.hotelRating = HotelRating.FourStar;
                            break;
                        case "5EST": hotel.hotelRating = HotelRating.FiveStar;
                            break;
                        default: hotel.hotelRating = HotelRating.All;
                            break;
                    }
                }

                //map
                tempNode = xmlDoc.SelectSingleNode("//HDRS:HotelDetailRS/HDRS:Hotel/HDRS:Position", nsmgr);
                if (tempNode != null)
                {
                    string latitude = tempNode.Attributes["latitude"].Value;
                    string longitude = tempNode.Attributes["longitude"].Value;
                    hotel.Map = latitude + "|" + longitude;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Low, 0, "Error in Hotel Details XML Response : "+ ex.Message + xmlDoc.OuterXml, "0");
            }
            return hotel;
        }

        /// <summary>
        /// get the booking done for the created itinerary
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.GetBooking entered");
            string requestXml = GenerateBookingRequest(itinerary);
            try
            {

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(requestXml);
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsBookingRequest.xml";
                xmlDoc.Save(filePath);
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsBookingRequest.xml");
            }
            catch { }

            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            BookingResponse searchRes = new BookingResponse();
            try
            {
                // Send request xml and get booking response
                xmlResp = SendRequest(requestXml);
                if (xmlResp != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseBooking(xmlResp, ref itinerary);
                }
                //Booking Confirmation after adding it to the service queue
                PurchaseConfirm(ref itinerary);
                //now put the real confirmation no in
                searchRes.ConfirmationNo = itinerary.ConfirmationNo;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsBooking, Severity.High, 0, "Exception returned from HotelBeds.GetBooking Error Message:" + ex.Message + " | Request XML:" + requestXml + "| Response XML :" + xmlResp.OuterXml + DateTime.Now, "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Audit.Add(EventType.HotelBedsBooking, Severity.High, 0, "Booking Response from HotelBeds. Response:" + resp + " | Request XML:" + requestXml + DateTime.Now, "");
            //Trace.TraceInformation("HotelBeds.GetBooking exiting");
            return searchRes;
        }

        /// <summary>
        /// generate the booking request for the itinerary created
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateBookingRequest(HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.GenerateBookingRequest entered");

            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            //writing security infos
            xmlString.WriteStartElement("ServiceAddRQ", "http://www.hotelbeds.com/schemas/2005/06/messages");
            xmlString.WriteAttributeString("echoToken", "token");
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.hotelbeds.com/schemas/2005/06/messages ../xsd/ServiceAddRQ.xsd");
            xmlString.WriteAttributeString("version", "2013/12");
            //writing language info
            xmlString.WriteElementString("Language", language);
            //writing credential info, username/password
            xmlString.WriteStartElement("Credentials");
            xmlString.WriteElementString("User", email);
            xmlString.WriteElementString("Password", password);
            xmlString.WriteEndElement();

            //writing service
            xmlString.WriteStartElement("Service");
            xmlString.WriteAttributeString("xsi", "type", null, "ServiceHotel");
            xmlString.WriteAttributeString("availToken", Basket.BookingSession[sessionId]["availToken"].ToString());
            //writing contracts list
            xmlString.WriteStartElement("ContractList");
            Dictionary<string, List<object>> contractList = (Dictionary<string, List<object>>)Basket.BookingSession[sessionId]["contractList"];
            foreach (object contractItem in contractList[itinerary.HotelCode])
            {
                xmlString.WriteStartElement("Contract");
                Dictionary<string, object> contract = (Dictionary<string, object>)contractItem;
                xmlString.WriteElementString("Name", contract["Name"].ToString());
                xmlString.WriteStartElement("IncomingOffice");
                xmlString.WriteAttributeString("code", contract["IncomingOffice"].ToString());
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            //Start date and end date
            xmlString.WriteStartElement("DateFrom");
            xmlString.WriteAttributeString("date", itinerary.StartDate.ToString("yyyyMMdd"));
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("DateTo");
            xmlString.WriteAttributeString("date", itinerary.EndDate.ToString("yyyyMMdd"));
            xmlString.WriteEndElement();

            //hotel info
            xmlString.WriteStartElement("HotelInfo");
            xmlString.WriteAttributeString("xsi", "type", null, "ProductHotel");
            xmlString.WriteElementString("Code", itinerary.HotelCode);
            //xmlString.WriteElementString("Name", itinerary.HotelName);
            xmlString.WriteStartElement("Destination");
            xmlString.WriteAttributeString("type", "SIMPLE");
            xmlString.WriteAttributeString("code", itinerary.CityCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            //available room

            Dictionary<int,List<int>> agesList=new Dictionary<int,List<int>>();
            for (int k = 0; k < itinerary.Roomtype.Length; k++)
            {
                if (itinerary.Roomtype[k].ChildAge != null && itinerary.Roomtype[k].ChildAge.Count > 0)
                {
                    List<int> ageList = new List<int>();
                    foreach (int age in itinerary.Roomtype[k].ChildAge)
                    {
                        ageList.Add(age);
                    }
                    agesList.Add(k, ageList);
                }
            }
            List<HotelRoom> roomsList = new List<HotelRoom>();
            roomsList.AddRange(itinerary.Roomtype);
            Dictionary<HotelRoom, int> filteredroomsList = new Dictionary<HotelRoom, int>();
            //List<int> childAges = new List<int>();
            foreach (var container in roomsList)
            {
                List<HotelRoom> duplicateRoom = roomsList.FindAll(delegate(HotelRoom checkRoom)
                { return (checkRoom.AdultCount == container.AdultCount && checkRoom.ChildCount == container.ChildCount); });
                if (!filteredroomsList.ContainsKey(duplicateRoom[0]))
                {
                    filteredroomsList.Add(duplicateRoom[0], duplicateRoom.Count);
                }
                else
                {
                    foreach (HotelRoom roomGuest in filteredroomsList.Keys)
                    {
                        if (container.AdultCount == roomGuest.AdultCount && container.ChildCount == roomGuest.ChildCount)
                        {
                            if (container.ChildAge != null && container.ChildAge.Count > 0)
                            {
                                roomGuest.ChildAge.AddRange(container.ChildAge);
                            }
                        }
                    }
                }
            }

            //int j = 1;
            foreach(KeyValuePair<HotelRoom,int> room in filteredroomsList)
            {
                xmlString.WriteStartElement("AvailableRoom");
                //hotel occupuancy
                xmlString.WriteStartElement("HotelOccupancy");
                if (room.Key.NoOfUnits == "1")
                {
                    xmlString.WriteElementString("RoomCount", room.Value.ToString());
                }
                else
                {
                    xmlString.WriteElementString("RoomCount", room.Key.NoOfUnits);
                }
                xmlString.WriteStartElement("Occupancy");
                xmlString.WriteElementString("AdultCount", room.Key.AdultCount.ToString());
                xmlString.WriteElementString("ChildCount", room.Key.ChildCount.ToString());

                xmlString.WriteStartElement("GuestList");
                int i;
                for (i = 0; i < room.Key.AdultCount; i++)
                {
                    xmlString.WriteStartElement("Customer");
                    xmlString.WriteAttributeString("type", "AD");
                    xmlString.WriteEndElement();
                }
                if (room.Value > 1 || room.Key.NoOfUnits != "1")
                {
                    if (room.Key.ChildCount > 0)
                    {
                        for (i = 0; i < room.Key.ChildAge.Count; i++)
                        {
                            xmlString.WriteStartElement("Customer");
                            xmlString.WriteAttributeString("type", "CH");
                            xmlString.WriteElementString("Age", room.Key.ChildAge[i].ToString());
                            xmlString.WriteEndElement();
                        }
                    }
                }
                else
                {
                    for (i = 0; i < room.Key.ChildCount; i++)
                    {
                        xmlString.WriteStartElement("Customer");
                        xmlString.WriteAttributeString("type", "CH");
                        xmlString.WriteElementString("Age", room.Key.ChildAge[i].ToString());
                        xmlString.WriteEndElement();
                    }
                }
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();

                xmlString.WriteEndElement();

                //hotelrooms
                xmlString.WriteStartElement("HotelRoom");
                string[] roomCodeList = room.Key.RoomTypeCode.Split('|');
                xmlString.WriteAttributeString("SHRUI", roomCodeList[5]);
                xmlString.WriteStartElement("Board");
                xmlString.WriteAttributeString("type", roomCodeList[0]);
                xmlString.WriteAttributeString("code", roomCodeList[1]);
                xmlString.WriteEndElement();
                xmlString.WriteStartElement("RoomType");
                xmlString.WriteAttributeString("type", "SIMPLE");
                xmlString.WriteAttributeString("code", roomCodeList[3]);
                xmlString.WriteAttributeString("characteristic", roomCodeList[4]);
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
            }
            for (int k = 0; k < itinerary.Roomtype.Length; k++)
            {
                if (itinerary.Roomtype[k] != null && itinerary.Roomtype[k].ChildAge != null && itinerary.Roomtype[k].ChildAge.Count > 0)
                {
                    itinerary.Roomtype[k].ChildAge = agesList[k];
                }
            }
            xmlString.WriteEndElement();

            xmlString.Close();
            //Trace.TraceInformation("HotelBeds.GenerateBookingRequest exited");
            return strWriter.ToString();
        }

        /// <summary>
        /// parse the response for the itinerary booking request
        /// </summary>
        /// <param name="xmlDoc">Xml response</param>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>Here Reading booking response object
        /// here only we need to get confirmationNo and BookingId
        /// </remarks>
        private BookingResponse ReadResponseBooking(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.ReadResponseBooking entered");
            BookingResponse bookResponse = new BookingResponse();

            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsBookingResponse.xml";
                xmlDoc.Save(filePath);
                //xmlDoc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsBookingResponse.xml");
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("HARS", "http://www.hotelbeds.com/schemas/2005/06/messages");

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//HARS:ServiceAddRS/HARS:Purchase/HARS:ServiceList/HARS:Service/HARS:ErrorList/HARS:Error", nsmgr);
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, " HotelBeds:GenerateSearchResult,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                string errorText = "";
                tempNode = ErrorInfo.SelectSingleNode("HARS:Message/text()", nsmgr);
                errorText = "Message : " + tempNode.Value + "<br>";
                tempNode = ErrorInfo.SelectSingleNode("HARS:DetailedMessage/text()", nsmgr);
                errorText += "Detailed Message : " + tempNode.Value;
                throw new BookingEngineException("<br>" + errorText);
            }
            ErrorInfo = xmlDoc.SelectSingleNode("//HARS:ServiceAddRS/HARS:ErrorList/HARS:Error", nsmgr);
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, " HotelBeds:GenerateSearchResult,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");

                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                string errorText = "";
                tempNode = ErrorInfo.SelectSingleNode("HARS:Message/text()", nsmgr);
                errorText = "Message : " + tempNode.Value + "<br>";
                tempNode = ErrorInfo.SelectSingleNode("HARS:DetailedMessage/text()", nsmgr);
                errorText += "Detailed Message : " + tempNode.Value;
                throw new BookingEngineException("<br>" + errorText);
            }

            //purchase unique token to identify each purchase in the shopping cart
            tempNode = xmlDoc.SelectSingleNode("//HARS:ServiceAddRS/HARS:Purchase", nsmgr);
            bookResponse.ConfirmationNo = tempNode.Attributes["purchaseToken"].Value;
            itinerary.ConfirmationNo = bookResponse.ConfirmationNo;
            //service SPUI
            tempNode = xmlDoc.SelectSingleNode("//HARS:ServiceAddRS/HARS:Purchase/HARS:ServiceList/HARS:Service", nsmgr);
            itinerary.BookingRefNo = tempNode.Attributes["SPUI"].Value;
            //purchase status : SHOPPING_CART for current status
            tempNode = xmlDoc.SelectSingleNode("//HARS:ServiceAddRS/HARS:Purchase/HARS:Status/text()", nsmgr);
            string status = tempNode.InnerText;
            itinerary.Status = GetItineraryStatus(status);
            //product type : Hotel for now
            bookResponse.ProdType = ProductType.Hotel;
            //booking status : Successful
            bookResponse.Status = BookingResponseStatus.Successful;

            
            
            //Trace.TraceInformation("HotelBeds.ReadResponseBooking exited");
            return bookResponse;
        }

        /// <summary>
        /// Amend : It will cancel the first one then create a new one.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        public BookingResponse AmendPaxDetails(ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.AmendPaxDetails entered");
            try
            {
                CancelBooking(itinerary);
                BookingResponse respDetail = GetBooking(ref itinerary);
                return respDetail;
            }
            catch (Exception e)
            {
                throw new BookingEngineException("Error in AmendPaxDetails : " + e.ToString());
            }
        }

        /// <summary>
        /// cancel the existing booking
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>Dictionary</returns>
        public Dictionary<string, string> CancelBooking(HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.CancelBooking entered");
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();

            string request = GenerateCancelBookingRequest(itinerary);
            TextReader stringRead = new StringReader(request);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsCancelBookingRequest.xml";
                xmlDoc.Save(filePath);
                //xmlDoc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsCancelBookingRequest.xml");
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    cancelInfo = ReadCancelBookingResponse(xmlResp, itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsCancel, Severity.High, 0, "Exception returned from HotelBeds.CancelBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlDoc.OuterXml, "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            finally
            {
                Audit.Add(EventType.HotelBedsCancel, Severity.High, 0, "HotelBeds.CancelBooking : request XML" + request + "|response XML" + xmlDoc.OuterXml + " " + DateTime.Now, "");
            }
            return cancelInfo;
        }

        /// <summary>
        /// generate cancel booking request
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateCancelBookingRequest(HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.GenerateCancelBookingRequest entered");
            string type = "C";
            string fileNumber = itinerary.ConfirmationNo;
            string inOfCode = itinerary.BookingRefNo;

            StringBuilder strWriter = new StringBuilder();

            XmlWriter xmlString = XmlTextWriter.Create(strWriter);

            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("PurchaseCancelRQ", "http://www.hotelbeds.com/schemas/2005/06/messages");
            xmlString.WriteAttributeString("echoToken", "DummyEchoToken");
            xmlString.WriteAttributeString("type", type);
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance ../xsd/PurchaseCancelRQ.xsd");
            //xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.w3.org/2001/XMLSchema-instance ../xsd/PurchaseCancelRQ.xsd");
            xmlString.WriteAttributeString("version", "2013/12");

            xmlString.WriteElementString("Language", language);
            //writing credential info, username/password
            xmlString.WriteStartElement("Credentials");
            xmlString.WriteElementString("User", email);
            xmlString.WriteElementString("Password", password);
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("PurchaseReference");
            xmlString.WriteStartElement("FileNumber");
            xmlString.WriteString(fileNumber);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("IncomingOffice");
            xmlString.WriteAttributeString("code", inOfCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.WriteEndElement();

            xmlString.Close();
            //Trace.TraceInformation("HotelBeds.GenerateCancelBookingRequest exited");
            return strWriter.ToString();
        }

        /// <summary>
        /// parse the response for cancel booking
        /// </summary>
        /// <param name="xmlDoc">response xml object</param>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> ReadCancelBookingResponse(XmlDocument xmlDoc, HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.ReadCancelBookingResponse entered");
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsCancelBookingResponse.xml";
                xmlDoc.Save(filePath);
                //xmlDoc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsCancelBookingResponse.xml");
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("PCRS", "http://www.hotelbeds.com/schemas/2005/06/messages");

            tempNode = xmlDoc.SelectSingleNode("//PCRS:PurchaseCancelRS/PCRS:Purchase", nsmgr);
            string purchaseToken = tempNode.Attributes["purchaseToken"].Value;
            cancelInfo.Add("ID", purchaseToken);

            tempNode = xmlDoc.SelectSingleNode("//PCRS:PurchaseCancelRS/PCRS:Purchase/PCRS:ServiceList/PCRS:Service", nsmgr);
            if (tempNode != null)
            {
                string SPUI = tempNode.Attributes["SPUI"].Value;
                cancelInfo.Add("SPUI", SPUI);
            }
            //get booking status now
            tempNode = xmlDoc.SelectSingleNode("//PCRS:PurchaseCancelRS/PCRS:Purchase/PCRS:Status", nsmgr);
            if (tempNode != null)
            {
                cancelInfo.Add("Status", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("//PCRS:PurchaseCancelRS/PCRS:Purchase/PCRS:Currency", nsmgr);
            if (tempNode != null)
            {
                cancelInfo.Add("Currency", tempNode.Attributes[0].Value);
            }
            tempNode = xmlDoc.SelectSingleNode("//PCRS:PurchaseCancelRS/PCRS:Purchase/PCRS:TotalPrice", nsmgr);
            if (tempNode != null)
            {
                cancelInfo.Add("Amount", tempNode.InnerText);
            }
            //Trace.TraceInformation("HotelBeds.ReadCancelBookingResponse exited");
            return cancelInfo;
        }

        /// <summary>
        /// get the hotel charge condition
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <param name="penalityList">penalityList</param>
        /// <param name="savePenality">savePenality</param>
        /// <returns>Dictionary</returns>
        public Dictionary<string, string> GetHotelChargeCondition(HotelItinerary itinerary, ref List<HotelPenality> penalityList, bool savePenality)
        {
            //Trace.TraceInformation("HotelBeds.GetHotelChargeCondition entered");
            string request = string.Empty;
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            Dictionary<string, string> searchRes = new Dictionary<string, string>();
            try
            {
                request = GenerateGetHotelChargeConditionRequest(itinerary);
                TextReader stringRead = new StringReader(request);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stringRead);
                try
                {
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsChargeConditionRequest.xml";
                    xmlDoc.Save(filePath);
                    //xmlDoc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsChargeConditionRequest.xml");
                }
                catch { }
                xmlResp = SendRequest(request);
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadGetHotelChargeConditionResponse(xmlResp, ref penalityList, savePenality, itinerary);
                }
                //ReadResponseBooking(resp, ref itinerary);
                //FlushBooking(itinerary.ConfirmationNo);
            }
            catch (BookingEngineException ex)
            {
                Audit.Add(EventType.HotelBedsCancel, Severity.High, 0, "Exception returned from HotelBeds.GetHotelChargeCondition Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                //Trace.TraceError("Error: " + ex.Message);
                //throw new BookingEngineException("There is some temporary server error.");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsCancel, Severity.High, 0, "Exception returned from HotelBeds.GetHotelChargeCondition Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                //Trace.TraceError("Error: " + ex.Message);
                // throw new BookingEngineException(ex.Message);
            }
            finally
            {
                Audit.Add(EventType.HotelBedsCancel, Severity.High, 0, "Cancellation Request and Response from HotelBeds:" + " | " + DateTime.Now + "| request XML:" + request + "|response XML:" + xmlResp.OuterXml, "");
            }
            //Trace.TraceInformation("HotelBeds.GetHotelChargeCondition exited");
            return searchRes;
        }

        /// <summary>
        /// we need to send the dumy booking request to get the cancellation details
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateGetHotelChargeConditionRequest(HotelItinerary itinerary)
        {
            return GenerateBookingRequest(itinerary);
        }

        /// <summary>
        /// parse the response for the hotel charge condition
        /// </summary>
        /// <param name="xmlDoc">response xml object</param>
        /// <param name="itinerary">itinerary</param>
        /// <param name="penalityList">penalityList</param>
        /// <param name="savePenality">savePenality</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Loading cancellation policy and update price</remarks>
        private Dictionary<string, string> ReadGetHotelChargeConditionResponse(XmlDocument xmlDoc, ref List<HotelPenality> penalityList, bool savePenality, HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.ReadGetHotelChargeConditionResponse entered");
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsChargeConditionResponse.xml";
                xmlDoc.Save(filePath);
                //xmlDoc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsChargeConditionResponse.xml");
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("PDRS", "http://www.hotelbeds.com/schemas/2005/06/messages");

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//PDRS:ServiceAddRS/PDRS:ErrorList/PDRS:Error", nsmgr);
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, " HotelBeds:GenerateSearchResult,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");

                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                string errorText = "";
                tempNode = ErrorInfo.SelectSingleNode("PDRS:Message/text()", nsmgr);
                errorText = "Message : " + tempNode.Value + "<br>";
                tempNode = ErrorInfo.SelectSingleNode("PDRS:DetailedMessage/text()", nsmgr);
                errorText += "Detailed Message : " + tempNode.Value;

                throw new BookingEngineException("<br>" + errorText);
            }

            Dictionary<string, string> polList = new Dictionary<string, string>();
            string lastCancelllation = string.Empty;
            string message = string.Empty;
            string amendmentMessage = string.Empty;
            string currencyCode = "";
            string hotelPolicy = string.Empty;
            DateTime startDate=itinerary.StartDate;
            //get currency symbol
            tempNode = xmlDoc.SelectSingleNode("PDRS:ServiceAddRS/PDRS:Purchase/PDRS:Currency", nsmgr);
            if (tempNode != null)
            {
                currencyCode = tempNode.Attributes["code"].Value;
            }
            rateOfExchange = exchangeRates[currencyCode];

            XmlNodeList contractNode = xmlDoc.SelectNodes("PDRS:ServiceAddRS/PDRS:Purchase/PDRS:ServiceList/PDRS:Service/PDRS:ContractList/PDRS:Contract/PDRS:CommentList/PDRS:Comment", nsmgr);
            if (contractNode != null)
            {
                foreach (XmlNode temp in contractNode)
                {
                    if (temp != null && temp.Attributes["type"].Value == "CONTRACT")
                    {
                        hotelPolicy += temp.InnerText;
                    }
                }
            }
            XmlNodeList availableRoomList = xmlDoc.SelectNodes("PDRS:ServiceAddRS/PDRS:Purchase/PDRS:ServiceList/PDRS:Service/PDRS:AvailableRoom", nsmgr);
            //int i = 0;
            int roomCount = 0;
            //int adltCount = 0;
            //int childCont = 0;
            //decimal checkPrice = 0;
            List<string> cancel = new List<string>();
            foreach (XmlNode node in availableRoomList)
            {
                tempNode = node.SelectSingleNode("PDRS:HotelOccupancy/PDRS:RoomCount", nsmgr);
                if (tempNode != null)
                {
                    roomCount = Convert.ToInt32(tempNode.InnerText);
                }
                decimal price = 0;
                XmlNodeList CancelTag = node.SelectNodes("PDRS:HotelRoom/PDRS:CancellationPolicies/PDRS:CancellationPolicy", nsmgr);
                if (CancelTag.Count > 0)
                {
                    foreach (XmlNode temp in CancelTag)
                    {
                        HotelPenality hp = new HotelPenality();
                        price = decimal.Parse(temp.Attributes["amount"].Value) / roomCount;
                        hp.Price = price;
                        string date = "";
                        date += temp.Attributes["dateFrom"].Value;
                        if (temp.Attributes.Count > 1)
                        {
                            date += temp.Attributes["time"].Value;
                        }
                        hp.FromDate = parseDate(date);
                        date = "";


                        int toDay = 0;
                        TimeSpan diffTo;
                        if (temp.Attributes["dateTo"] !=null)
                        {
                            date += temp.Attributes["dateTo"].Value;
                            if (temp.Attributes.Count > 1)
                            {
                                date += temp.Attributes["time"].Value;
                            }
                            hp.ToDate = parseDate(date);
                            diffTo = startDate.Subtract(hp.ToDate);
                        }
                        //hp.ToDate = parseDate(date);
                        TimeSpan diffFrom = startDate.Subtract(hp.FromDate);
                        int fromDay = diffFrom.Days + (diffFrom.TotalDays > diffFrom.Days ? 1 : 0);//1 day added if the time defferences.
                        
                        //get the last cancellation date
                        //lastCancelllation = fromDay.ToString();
                        lastCancelllation = startDate.ToString();
                        //calculate buffer day 
                        double buffer = 0;
                        if (ConfigurationSystem.HotelBedsConfig.ContainsKey("buffer"))
                        {
                            buffer = Convert.ToDouble(ConfigurationSystem.HotelBedsConfig["buffer"]);
                        }
                        hp.PolicyType = ChangePoicyType.Amendment;
                        hp.Remarks = amendmentMessage;
                        
                        decimal chargeAmt = Math.Ceiling(hp.Price * exchangeRates[currencyCode] + ((hp.Price * exchangeRates[currencyCode]) * Convert.ToDecimal(ConfigurationSystem.HotelBedsConfig["CancellationMarkup"]) / 100));
                        string symbol = string.Empty;
                        symbol = agentCurrency;
                        if (toDay == 0)
                        {
                            hp.FromDate = hp.FromDate.AddDays(-buffer);
                            if (hp.ToDate != DateTime.MinValue)
                            {
                                cancel.Add("Amount of " + symbol + " " + chargeAmt + " will be charged from " + hp.FromDate.ToString("dd MMM yyyy HH:mm:ss") + " to " + hp.ToDate.ToString("dd MMM yyyy HH:mm:ss"));
                            }
                            else
                            {
                                cancel.Add("Amount of " + symbol + " " + chargeAmt + " will be charged after " + hp.FromDate.ToString("dd MMM yyyy HH:mm:ss"));
                            }
                        }
                        else
                        {
                            cancel.Add("Amount of " + symbol + " " + chargeAmt + " will be charged till " + hp.ToDate.ToString("dd MMM yyyy HH:mm:ss"));
                        }

                        //penalityList.Add(hp); as per brahm
                    }
                }

            }
            decimal PreviousTotal = 0;
            for (int k = 0; k < itinerary.Roomtype.Length; k++)
            {
                HotelRoom room = Convert.ChangeType(itinerary.Roomtype[k], typeof(HotelRoom)) as HotelRoom;
                PreviousTotal += room.Price.SupplierPrice;
            }

            decimal TotalPrice = 0;
            tempNode = xmlDoc.SelectSingleNode("PDRS:ServiceAddRS/PDRS:Purchase/PDRS:TotalPrice", nsmgr);
            if (tempNode != null)
            {
                TotalPrice = (Decimal.Parse(tempNode.InnerText));
            }

            //price validation
            if ((TotalPrice - PreviousTotal) > 1)   
            {
                itinerary.Roomtype[0].PreviousFare = PreviousTotal * rateOfExchange;
                itinerary.Roomtype[0].Price.NetFare= TotalPrice*rateOfExchange;
            }
            cancel.Add("Date and time is calculated based on local time of destination");
            List<string> cancellation = new List<string>();
            foreach (string canceldata in cancel)
            {
                if (!cancellation.Contains(canceldata))
                {
                    cancellation.Add(canceldata);
                    if (message != string.Empty)
                    {
                        message += "|" + canceldata;
                    }
                    else
                    {
                        message = canceldata;
                    }
                }
            }
            polList.Add("lastCancellationDate", lastCancelllation);
            polList.Add("CancelPolicy", message);
            polList.Add("HotelPolicy", hotelPolicy);

            //Trace.TraceInformation("HotelBeds.ReadGetHotelChargeConditionResponse exited");
            return polList;
        }

        /// <summary>
        /// remove the purchase token from the service queue
        /// </summary>
        /// <param name="purchaseToken">purchaseToken</param>
        /// <param name="SPUI">SPUI</param>
        /// <returns>string</returns>
        private string ServiceRemove(string purchaseToken, string SPUI)
        {
            //Trace.TraceInformation("HotelBeds.ServiceRemove entered");
            string request = string.Empty;
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                request = GenerateServiceRemoveRQ(purchaseToken, SPUI);
                xmlResp = SendRequest(request);
                string retValue = string.Empty;
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    retValue = ReadServiceRemoveResponse(xmlResp);
                }
                return retValue;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsBooking, Severity.High, 0, "Exception returned from HotelBeds.ServiceRemove Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error in GetSeriveRemove : " + ex.ToString());
            }
        }
        /// <summary>
        /// generate ServiceRemove request
        /// </summary>
        /// <param name="purchaseToken">purchaseToken</param>
        /// <param name="SPUI">SPUI</param>
        /// <returns>string</returns>
        private string GenerateServiceRemoveRQ(string purchaseToken, string SPUI)
        {
            //Trace.TraceInformation("HotelBeds.GenerateServiceRemoveRQ entered");
            StringBuilder strWriter = new StringBuilder();

            XmlWriter xmlString = XmlTextWriter.Create(strWriter);

            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("ServiceRemoveRQ", "http://www.hotelbeds.com/schemas/2005/06/messages");
            xmlString.WriteAttributeString("purchaseToken", purchaseToken);
            xmlString.WriteAttributeString("SPUI", SPUI);
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.hotelbeds.com/schemas/2005/06/messages ServiceRemoveRQ.xsd");

            xmlString.WriteElementString("Language", language);
            //writing credential info, username/password
            xmlString.WriteStartElement("Credentials");
            xmlString.WriteElementString("User", email);
            xmlString.WriteElementString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.Close();
            //Trace.TraceInformation("HotelBeds.GenerateServiceRemoveRQ exited");
            return strWriter.ToString();
        }

        /// <summary>
        /// ReadServiceRemoveResponse
        /// </summary>
        /// <param name="response">xmlResponse</param>
        /// <returns>string</returns>
        private string ReadServiceRemoveResponse(XmlDocument response)
        {
            return response.OuterXml;
        }

        /// <summary>
        /// import the booked itinerary if exixt on server
        /// </summary>
        /// <param name="itinerary">BookingrefNo</param>
        /// <returns>bool</returns>
        public bool GetBookedHotel(ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.GetBookedHotel entered");
            bool itemFound = false;
            string request = GenerateGetBookedHotelRequest(itinerary);
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    itemFound = ReadGetBookedHotelResponse(xmlResp, ref itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsImport, Severity.High, 0, "Exception returned from HotelBeds.GetBookedHotel Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("HotelBeds.GetBookedHotel exited");
            return itemFound;
        }

        /// <summary>
        /// generate the request to import the booking
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateGetBookedHotelRequest(HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.GenerateGetBookedHotelRequest entered");
            StringBuilder strWriter = new StringBuilder();

            XmlWriter xmlString = XmlTextWriter.Create(strWriter);

            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("PurchaseDetailRQ", "http://www.hotelbeds.com/schemas/2005/06/messages");
            xmlString.WriteAttributeString("echoToken", "DummyEchoToken");
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.hotelbeds.com/schemas/2005/06/messages PurchaseDetailRQ.xsd");

            xmlString.WriteStartElement("Language");
            xmlString.WriteString(language);
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("Credentials");
            xmlString.WriteStartElement("User");
            xmlString.WriteString(email);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("Password");
            xmlString.WriteString(password);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("PurchaseReference");
            xmlString.WriteStartElement("FileNumber");
            xmlString.WriteString(itinerary.ConfirmationNo);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("IncomingOffice");
            xmlString.WriteAttributeString("code", itinerary.BookingRefNo);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.WriteEndElement();

            xmlString.Close();
            //Trace.TraceInformation("HotelBeds.GenerateGetBookedHotelRequest exited");
            return strWriter.ToString();
        }

        /// <summary>
        /// parse the response to get the itinerary filled
        /// </summary>
        /// <param name="xmlDoc">Response Xml object</param>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>bool</returns>
        private bool ReadGetBookedHotelResponse(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.ReadGetBookedHotelResponse entered");
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("PDRS", "http://www.hotelbeds.com/schemas/2005/06/messages");
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//PDRS:PurchaseDetailRS/PDRS:ErrorList/PDRS:Error", nsmgr);
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, " HotelBeds:GenerateSearchResult,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");

                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                string errorText = "";
                tempNode = ErrorInfo.SelectSingleNode("PDRS:Message/text()", nsmgr);
                errorText = "Message : " + tempNode.Value + "<br>";
                tempNode = ErrorInfo.SelectSingleNode("PDRS:DetailedMessage/text()", nsmgr);
                errorText += "Detailed Message : " + tempNode.Value;

                throw new BookingEngineException("<br>" + errorText);
            }

            XmlNode purchaeInfo = xmlDoc.SelectSingleNode("//PDRS:PurchaseDetailRS/PDRS:Purchase", nsmgr);
            if (purchaeInfo != null && purchaeInfo.InnerText.Length > 0)
            {
                HotelSource sourceInfo = new HotelSource();
                sourceInfo.Load("HotelBeds");
                decimal ourCommission = sourceInfo.OurCommission;
                int commissionType = 0;
                commissionType = (int)sourceInfo.CommissionTypeId;
                
                
                string cancelPolicy = string.Empty;
                decimal addCost = 0;
                //status
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:Status", nsmgr);
                if (tempNode != null)
                {
                    string bookingStatus = tempNode.FirstChild.Value;
                    itinerary.Status = GetItineraryStatus(bookingStatus);
                }
                itinerary.HotelAddress1 = string.Empty;
                //HotelCode and HotelName
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:HotelInfo/PDRS:Code", nsmgr);
                itinerary.HotelCode = tempNode.FirstChild.Value;
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:HotelInfo/PDRS:Name", nsmgr);
                itinerary.HotelName = tempNode.InnerText;
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:HotelInfo/PDRS:Destination/PDRS:Name/text()", nsmgr);
                itinerary.HotelAddress2 = tempNode.Value;
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:HotelInfo/PDRS:Destination/PDRS:ZoneList/PDRS:Zone/text()", nsmgr);
                itinerary.HotelAddress1 = tempNode.Value;

                //search for CityCode and CityName
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:HotelInfo/PDRS:Destination", nsmgr);
                itinerary.CityCode = tempNode.Attributes["code"].Value;
                itinerary.CityRef = tempNode.SelectSingleNode("PDRS:Name", nsmgr).FirstChild.Value;
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:SupplementList", nsmgr);
                if (tempNode != null)
                {
                    XmlNodeList priceNodes = tempNode.SelectNodes("PDRS:Price", nsmgr);
                    if (priceNodes != null)
                    {
                        foreach (XmlNode priceNode in priceNodes)
                        {
                            XmlNode Amount = priceNode.SelectSingleNode("PDRS:Amount", nsmgr);
                            if (Amount != null)
                            {
                                addCost += Convert.ToDecimal(Amount.InnerText);
                            }
                        }
                    }
                }
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:DiscountList", nsmgr);
                if (tempNode != null)
                {
                    XmlNodeList priceNodes = tempNode.SelectNodes("PDRS:Price", nsmgr);
                    if (priceNodes != null)
                    {
                        foreach (XmlNode priceNode in priceNodes)
                        {
                            XmlNode Amount = priceNode.SelectSingleNode("PDRS:Amount", nsmgr);
                            if (Amount != null)
                            {
                                addCost += Convert.ToDecimal(Amount.InnerText);
                            }
                        }
                    }
                }
                //search for ItemPrice
                tempNode = purchaeInfo.SelectSingleNode("PDRS:TotalPrice", nsmgr);
                double price = Convert.ToDouble(tempNode.FirstChild.Value);
                //search for Check-In Date
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:DateFrom", nsmgr);
                string datefrom = tempNode.Attributes["date"].Value;
                itinerary.StartDate = parseDate(datefrom);

                //search for Check-Out Date
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:DateTo", nsmgr);
                string DateTo = tempNode.Attributes["date"].Value;
                itinerary.EndDate = parseDate(DateTo);
                //currency details
                tempNode = purchaeInfo.SelectSingleNode("PDRS:Currency", nsmgr);
                string cCode = tempNode.Attributes["code"].Value;
                //search for Passenger Details
                List<HotelPassenger> paxList = new List<HotelPassenger>();
                List<int> childAge = new List<int>();
                tempNode = purchaeInfo.SelectSingleNode("PDRS:ServiceList/PDRS:Service/PDRS:AvailableRoom/PDRS:HotelOccupancy/PDRS:Occupancy/PDRS:GuestList", nsmgr);
                XmlNodeList guests = tempNode.SelectNodes("PDRS:Customer", nsmgr);
                foreach (XmlNode node in guests)
                {
                    HotelPassenger pax = new HotelPassenger();

                    int id = Convert.ToInt32(node.SelectSingleNode("PDRS:CustomerId", nsmgr).FirstChild.Value);
                    pax.PaxId = id;
                    pax.Age = Convert.ToInt16(node.SelectSingleNode("PDRS:Age", nsmgr).FirstChild.Value);
                    tempNode = node.SelectSingleNode("PDRS:Name/text()", nsmgr);
                    if (tempNode != null)
                    {
                        pax.Firstname = tempNode.Value;
                    }
                    else
                    {
                        pax.Firstname = string.Empty;
                    }
                    tempNode = node.SelectSingleNode("PDRS:LastName/text()", nsmgr);
                    if (tempNode != null)
                    {
                        pax.Lastname = tempNode.Value;
                    }
                    else
                    {
                        pax.Lastname = string.Empty;
                    }
                    if (node.Attributes["type"].Value.Equals("CH"))
                    {
                        pax.PaxType = HotelPaxType.Child;
                        childAge.Add(pax.Age);
                    }
                    else if (node.Attributes["type"].Value.Equals("AD"))
                    {
                        pax.PaxType = HotelPaxType.Adult;
                    }
                    pax.Addressline1 = string.Empty;
                    paxList.Add(pax);
                }
                paxList[0].LeadPassenger = true;
                itinerary.HotelPassenger = paxList[0];

                //hotel rooms details here

                XmlNodeList roomList = purchaeInfo.SelectNodes("PDRS:ServiceList/PDRS:Service/PDRS:AvailableRoom/PDRS:HotelRoom", nsmgr);
                itinerary.NoOfRooms = roomList.Count;
                addCost = addCost / itinerary.NoOfRooms;
                HotelRoom[] hotelRooms = new HotelRoom[itinerary.NoOfRooms];
                int iRoom = 0;
                TimeSpan timeDiff = itinerary.EndDate.Subtract(itinerary.StartDate);
                int noOfDays = timeDiff.Days;
                if (exchangeRates.ContainsKey(cCode))
                {
                    rateOfExchange = exchangeRates[cCode];
                }
                else
                {
                    rateOfExchange = 1;
                }
                XmlNodeList occList = purchaeInfo.SelectNodes("PDRS:ServiceList/PDRS:Service/PDRS:AvailableRoom/PDRS:HotelOccupancy/PDRS:Occupancy", nsmgr);
                foreach (XmlNode room in roomList)
                {
                    if (room != null)
                    {
                        hotelRooms[iRoom] = new HotelRoom();
                    }
                    XmlNode board = room.SelectSingleNode("PDRS:Board", nsmgr);
                    XmlNode roomType = room.SelectSingleNode("PDRS:RoomType", nsmgr);
                    if (occList != null && occList[iRoom] != null)
                    {
                        XmlNode guestInfo = occList[iRoom];
                        XmlNode adultInfo = guestInfo.SelectSingleNode("PDRS:AdultCount", nsmgr);
                        if (adultInfo != null)
                        {
                            hotelRooms[iRoom].AdultCount = Convert.ToInt16(adultInfo.InnerText);
                        }
                        XmlNode childInfo = guestInfo.SelectSingleNode("PDRS:ChildCount", nsmgr);
                        if (childInfo != null)
                        {
                            hotelRooms[iRoom].ChildCount = Convert.ToInt16(childInfo.InnerText);
                        }
                    }
                    string roomTypeCode = board.Attributes["type"].Value + "|" + board.Attributes["code"].Value + "|" + " " + "|" + roomType.Attributes["code"].Value + "|" + roomType.Attributes["characteristic"].Value;
                    hotelRooms[iRoom].RoomTypeCode = roomTypeCode;
                    hotelRooms[iRoom].RoomName = roomType.FirstChild.Value + "/" + board.InnerText;
                    hotelRooms[iRoom].PassenegerInfo = paxList;
                    hotelRooms[iRoom].ChildAge = childAge;
                    hotelRooms[iRoom].RatePlanCode = roomTypeCode;
                    hotelRooms[iRoom].NoOfUnits = "1";
                    hotelRooms[iRoom].Ameneties = new List<string>();
                    XmlNode cancelNode = room.SelectSingleNode("PDRS:CancellationPolicy/PDRS:Price", nsmgr);
                    decimal cancelAmount = 0;
                    if (commissionType == (int)CommissionType.Percentage)
                    {
                        cancelAmount = Math.Round(decimal.Parse(cancelNode.InnerText), decimalPoint) / 100;
                    }
                    if (commissionType == (int)CommissionType.Fixed)
                    {
                        cancelAmount = Math.Round(decimal.Parse(cancelNode.InnerText), decimalPoint) + Convert.ToDecimal(ourCommission / rateOfExchange);
                    }
                    XmlNode fromDateNode = room.SelectSingleNode("PDRS:CancellationPolicy/PDRS:Price/PDRS:DateTimeFrom", nsmgr);
                    DateTime fromDate = parseDate(fromDateNode.Attributes["date"].Value);
                    XmlNode toDateNode = room.SelectSingleNode("PDRS:CancellationPolicy/PDRS:Price/PDRS:DateTimeTo", nsmgr);
                    DateTime toDate = parseDate(toDateNode.Attributes["date"].Value);
                    cancelPolicy += "Room " + (iRoom + 1) + " :";
                    cancelPolicy += "Cancellation made between " + fromDate.ToString("dd-MM-yy") + " - " + toDate.ToString("dd-MM-yy") + "  " + HotelCity.GetCurrencySymbol(cCode) + cancelAmount + " will be charged.|";
                    itinerary.LastCancellationDate = fromDate;
                    PriceAccounts pAc = new PriceAccounts();
                    XmlNode priceNode = room.SelectSingleNode("PDRS:Price/PDRS:Amount/text()", nsmgr);
                    pAc.NetFare = Math.Round(decimal.Parse(priceNode.Value) + addCost, decimalPoint);
                    pAc.Currency = cCode;
                    pAc.AccPriceType = PriceType.NetFare;
                    pAc.RateOfExchange = rateOfExchange;
                    hotelRooms[iRoom].RoomFareBreakDown = new HotelRoomFareBreakDown[noOfDays];
                    decimal totMarkUp = 0;
                    for (int i = 0; i < noOfDays; i++)
                    {
                        hotelRooms[iRoom].RoomFareBreakDown[i] = new HotelRoomFareBreakDown();
                        hotelRooms[iRoom].RoomFareBreakDown[i].Date = itinerary.StartDate.AddDays(i);
                        if (commissionType == (int)CommissionType.Percentage)
                        {
                            hotelRooms[iRoom].RoomFareBreakDown[i].RoomPrice = pAc.NetFare / noOfDays + Convert.ToDecimal(ourCommission * (pAc.NetFare / noOfDays)) / 100;
                            totMarkUp += Convert.ToDecimal(ourCommission * (pAc.NetFare / noOfDays)) / 100;
                        }
                        if (commissionType == (int)CommissionType.Fixed)
                        {
                            hotelRooms[iRoom].RoomFareBreakDown[i].RoomPrice = pAc.NetFare / noOfDays + Convert.ToDecimal(ourCommission / rateOfExchange);
                            totMarkUp += Convert.ToDecimal(ourCommission / rateOfExchange);
                        }
                    }
                    pAc.Markup = totMarkUp;
                    hotelRooms[iRoom].Price = pAc;
                    iRoom++;
                }
                itinerary.Roomtype = hotelRooms;
                itinerary.HotelCancelPolicy = cancelPolicy;
            }
            else
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// Hotel Booking Basket Flush
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <returns>bool</returns>
        private bool FlushBooking(string confirmationNo)
        {
            //Trace.TraceInformation("HotelBeds.FlushBooking entered");
            try
            {
                string request; //response
                request = GenerateFlushBookingRequest(confirmationNo);
                XmlDocument xmlResp = SendRequest(request);
                bool retValue = ReadFlushBookingRequest(xmlResp);
                return retValue;
            }
            catch (Exception e)
            {
                throw new BookingEngineException("Error in CancelBooking : " + e.ToString());
            }
        }

        /// <summary>
        /// generate request to flush shop cart 
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <returns>string</returns>
        private string GenerateFlushBookingRequest(string confirmationNo)
        {
            //Trace.TraceInformation("HotelBeds.GenerateFlushBookingRequest entered");
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            //writing security infos
            xmlString.WriteStartElement("PurchaseFlushRQ", "http://www.hotelbeds.com/schemas/2005/06/messages");
            xmlString.WriteAttributeString("echoToken", "token");
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.hotelbeds.com/schemas/2005/06/messages PurchaseFlushRQ.xsd");
            //writing purchase token for cancelation
            xmlString.WriteAttributeString("purchaseToken", confirmationNo);

            //writing language info
            xmlString.WriteElementString("Language", language);
            //writing credential info, username/password
            xmlString.WriteStartElement("Credentials");
            xmlString.WriteElementString("User", email);
            xmlString.WriteElementString("Password", password);
            xmlString.WriteEndElement();

            xmlString.WriteEndElement();

            xmlString.Close();

            //Trace.TraceInformation("HotelBeds.GenerateFlushBookingRequest exited");
            return strWriter.ToString();
        }

        /// <summary>
        /// parse the response for flush shop cart
        /// </summary>
        /// <param name="xmlDoc">xml response object</param>
        /// <returns>bool</returns>
        private bool ReadFlushBookingRequest(XmlDocument xmlDoc)
        {
            //Trace.TraceInformation("HotelBeds.ReadFlushBookingRequest entered");
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("HARS", "http://www.hotelbeds.com/schemas/2005/06/messages");

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//HARS:PurchaseFlushRS/HARS:ErrorList/HARS:Error/HARS:Message/text()", nsmgr);
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, " HotelBeds:GenerateSearchResult,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");

                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }

            tempNode = xmlDoc.SelectSingleNode("//HARS:PurchaseFlushRS/HARS:Status/text()", nsmgr);
            if (tempNode != null && tempNode.Value == "Y")
            {
                return true;
            }
            else
            {
                throw new BookingEngineException("Error in CancelBooking->ReadResponse : PurchaseToken not found!");
            }
        }

        /// <summary>
        /// Confirm the purchase basket
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>bool</returns>
        private bool PurchaseConfirm(ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.PurchaseConfirm entered");
            string requestXml = GeneratePurchaseConfirmRequest(ref itinerary);
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(requestXml);
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsPurchaseRequest.xml";
                xmlDoc.Save(filePath);
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedspurchaseReq.xml");
            }
            catch { }
            // Send request xml and get response
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            bool purchaseConfirmRes=false;
            try
            {
                xmlResp = SendRequest(requestXml);
                try
                {
                    //XmlDocument xmlDoc = new XmlDocument();
                    //xmlDoc.LoadXml(xmlResp);
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsPurchaseResponse.xml";
                    xmlResp.Save(filePath);
                    //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedspurchaseRes.xml");
                }
                catch { }
                // Process the Response.
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    purchaseConfirmRes = ReadPurchaseConfirmResponse(xmlResp, ref itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsBooking, Severity.High, 0, "Exception returned from HotelBeds.PurchaseConfirm Error Message:" + ex.Message + " | Request XML:" + requestXml + "| Response XML :" + xmlResp.OuterXml + DateTime.Now, "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            finally
            {
                Audit.Add(EventType.HotelBedsBooking, Severity.High, 0, "HotelBeds.PurchaseConfirm Request XML:" + requestXml + "| Response XML :" + xmlResp.OuterXml + DateTime.Now, "");
            }

            //Trace.TraceInformation("HotelBeds.PurchaseConfirm exiting");
            return purchaseConfirmRes;
        }

        /// <summary>
        /// generate the request to confirm the purchase
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GeneratePurchaseConfirmRequest(ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.GeneratePurchaseConfirmRequest entered");
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            //writing security infos
            xmlString.WriteStartElement("PurchaseConfirmRQ", "http://www.hotelbeds.com/schemas/2005/06/messages");
            xmlString.WriteAttributeString("echoToken", "token");
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            //xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRQ.xsd");
            xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://www.hotelbeds.com/schemas/2005/06/messages ../xsd/PurchaseConfirmRQ.xsd");
            xmlString.WriteAttributeString("version", "2013/12");
            //writing language info
            xmlString.WriteElementString("Language", language);
            //writing credential info, username/password
            xmlString.WriteStartElement("Credentials");
            xmlString.WriteElementString("User", email);
            xmlString.WriteElementString("Password", password);
            xmlString.WriteEndElement();

            //writing purchase info
            xmlString.WriteStartElement("ConfirmationData");
            xmlString.WriteAttributeString("purchaseToken", itinerary.ConfirmationNo);
            //writing holder name
            xmlString.WriteStartElement("Holder");
            if (itinerary.HotelPassenger.PaxType == HotelPaxType.Adult)
            {
                xmlString.WriteAttributeString("type", "AD");
            }
            else
            {
                xmlString.WriteAttributeString("type", "CH");
            }
            //xmlString.WriteElementString("CustomerId", itinerary.HotelPassenger.PaxId.ToString());
            if (itinerary.HotelPassenger.PaxType == HotelPaxType.Child)
            {
                xmlString.WriteElementString("Age", itinerary.HotelPassenger.Age.ToString());
            }
            xmlString.WriteElementString("Name", itinerary.HotelPassenger.Firstname);
            xmlString.WriteElementString("LastName", itinerary.HotelPassenger.Lastname);
            xmlString.WriteEndElement();
            //writing agency
            itinerary.AgencyReference = TraceIdGeneration();
            xmlString.WriteElementString("AgencyReference", itinerary.AgencyReference);
            //writing service list
            xmlString.WriteStartElement("ConfirmationServiceDataList");
            xmlString.WriteStartElement("ServiceData");
            xmlString.WriteAttributeString("xsi", "type", null, "ConfirmationServiceDataHotel");
            xmlString.WriteAttributeString("SPUI", itinerary.BookingRefNo);
            //customer info
            xmlString.WriteStartElement("CustomerList");
            int j = 1;
            for (int index = 0; index < itinerary.Roomtype.Length; index++)
            {
                if (itinerary.Roomtype[index].PassenegerInfo != null)
                {
                    foreach (HotelPassenger passenger in itinerary.Roomtype[index].PassenegerInfo)
                    {
                        xmlString.WriteStartElement("Customer");
                        if (passenger.PaxType == HotelPaxType.Adult)
                        {
                            xmlString.WriteAttributeString("type", "AD");
                        }
                        else if (passenger.PaxType == HotelPaxType.Child)
                        {
                            xmlString.WriteAttributeString("type", "CH");
                        }
                        xmlString.WriteElementString("CustomerId", j.ToString());
                        if (passenger.PaxType == HotelPaxType.Child)
                        {
                            xmlString.WriteElementString("Age", passenger.Age.ToString());
                        }
                        if (passenger.Firstname.Length > 0)
                        {
                            xmlString.WriteElementString("Name", passenger.Firstname);
                            xmlString.WriteElementString("LastName", passenger.Lastname);
                        }
                        xmlString.WriteEndElement();
                        j++;
                    }
                }
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.WriteEndElement();
            xmlString.Close();
            //Trace.TraceInformation("HotelBeds.GeneratePurchaseConfirmRequest exited");
            return strWriter.ToString();
        }

        /// <summary>
        /// parse the response whether it has been confirmed
        /// </summary>
        /// <param name="xmlDoc">response xml object</param>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>bool</returns>
        private bool ReadPurchaseConfirmResponse(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("HotelBeds.ReadPurchaseConfirmResponse Entered");
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("HARS", "http://www.hotelbeds.com/schemas/2005/06/messages");

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//HARS:PurchaseConfirmRS/HARS:ErrorList/HARS:Error/HARS:Message/text()", nsmgr);
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, " HotelBeds:GenerateSearchResult,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");

                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("//HARS:PurchaseConfirmRS/HARS:Purchase/HARS:Reference/HARS:FileNumber/text()", nsmgr);
            string fileNo = tempNode.Value;
            itinerary.ConfirmationNo = fileNo;
            tempNode = xmlDoc.SelectSingleNode("//HARS:PurchaseConfirmRS/HARS:Purchase/HARS:Reference/HARS:IncomingOffice", nsmgr);
            string incomingOffice = tempNode.Attributes["code"].Value;
            itinerary.BookingRefNo = incomingOffice;
            //itinerary.BookingRefNo = fileNo + "|" + incomingOffice;
            //update booking status now
            tempNode = xmlDoc.SelectSingleNode("//HARS:PurchaseConfirmRS/HARS:Purchase", nsmgr);
            XmlNode status = tempNode.SelectSingleNode("HARS:ServiceList/HARS:Service/HARS:Status", nsmgr);
            if (status != null)
            {
                itinerary.Status = GetItineraryStatus(status.InnerText);
            }
            //set supplier information
            tempNode = xmlDoc.SelectSingleNode("//HARS:PurchaseConfirmRS/HARS:Purchase", nsmgr);
            XmlNode supplier = tempNode.SelectSingleNode("HARS:ServiceList/HARS:Service/HARS:Supplier", nsmgr);
            if (supplier != null)
            {
                string vatInfo = string.Empty;

                if (supplier.Attributes["name"] != null)
                {
                    vatInfo += "Payable through : " + supplier.Attributes["name"].Value;
                }
                vatInfo += "<span style='font-size: 20px; color: Red'>,Supplier Reference No:" + itinerary.BookingRefNo + "-" + itinerary.ConfirmationNo + "</span>";
                if (supplier.Attributes["name"] != null)
                {
                    vatInfo += " VAT : " + supplier.Attributes["vatNumber"].Value;
                }
                itinerary.PaymentGuaranteedBy = vatInfo;
            }
            return true;
        }

        /// <summary>
        /// get the itinerary status
        /// </summary>
        /// <param name="status">HotelBookingStatus</param>
        /// <returns>HotelBookingStatus</returns>
        private HotelBookingStatus GetItineraryStatus(string status)
        {
            HotelBookingStatus bookingStatus;
            switch (status.ToUpper())
            {
                case "CANCELLED": bookingStatus = HotelBookingStatus.Cancelled;
                    break;
                case "CONFIRMED": bookingStatus = HotelBookingStatus.Confirmed;
                    break;
                case "NEW": bookingStatus = HotelBookingStatus.Pending;
                    break;
                default: bookingStatus = HotelBookingStatus.Pending;
                    break;
            }
            return bookingStatus;
        }
        /// <summary>
        /// This Method is used for getting Individual Hotel Information.
        /// </summary>
        /// <param name="cityCode">Selected CityCode</param>
        /// <param name="itemName">Selected HotelName</param>
        /// <param name="itemCode">Selected HotelCode</param>
        /// <returns>HotelDetails</returns>
        public HotelDetails GetItemInformation(string cityCode, string itemName, string itemCode)
        {
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData staticInfo = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();

            //get the info whether to update the static data
            int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.HotelBedsConfig["TimeStamp"]);
            bool imageUpdate = true;
            bool isUpdateReq = false;
            bool isReqSend = false;

            string request = string.Empty;
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();

            try
            {
                staticInfo.Load(itemCode, cityCode, HotelBookingSource.HotelBeds);
                imageInfo.Load(itemCode, cityCode, HotelBookingSource.HotelBeds);
                if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                {
                    imageUpdate = false;
                }
                if (staticInfo.HotelName != null && !string.IsNullOrEmpty(staticInfo.HotelDescription) && !string.IsNullOrEmpty(staticInfo.HotelAddress) && staticInfo.HotelName.Length > 0)
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(staticInfo.TimeStamp);
                    //if it is less than REquired time stamp days then load from DB
                    if (diffRes.Days > timeStampDays)
                    {
                        // Set the variable as true
                        isUpdateReq = true;
                        imageUpdate = true;
                    }
                    else
                    {
                        hotelInfo.HotelCode = staticInfo.HotelCode;
                        hotelInfo.HotelName = staticInfo.HotelName;
                        hotelInfo.Map = staticInfo.HotelMap;
                        
                        
                        // Bug Id: 0029983 remove HotelDescription: word from Hotel Details?
                        string tempHotelDescription = staticInfo.HotelDescription.Substring(0, 17).ToUpper();
                        if (tempHotelDescription.Equals("HOTELDESCRIPTION:"))
                        {
                            staticInfo.HotelDescription = staticInfo.HotelDescription.Remove(0, 17);
                            hotelInfo.Description = staticInfo.HotelDescription;
                        }
                        else
                            hotelInfo.Description = staticInfo.HotelDescription;
                        //

                        string[] image = new string[0];
                        List<string> dldImgList = new List<string>();
                        //if (!imageUpdate) // reading physical image name
                        //{
                        //    image = imageInfo.DownloadedImgs.Split('|');
                        //    string[] dldImages = imageInfo.DownloadedImgs.Split('|');
                        //    foreach (string img in dldImages)
                        //    {
                        //        dldImgList.Add(img);
                        //    }
                        //}
                        //else
                        { // Now reading image URL instead of physical image name
                            image = imageInfo.Images.Split('|');
                            string[] dldImages = imageInfo.Images.Split('|');
                            foreach (string img in dldImages)
                            {
                                if(!string.IsNullOrEmpty(img))dldImgList.Add(img);
                            }
                        }
                        hotelInfo.Images = dldImgList;
                        hotelInfo.Image = dldImgList.Count > 0 ? dldImgList[0] : "";
                        hotelInfo.Address = staticInfo.HotelAddress;
                        hotelInfo.PhoneNumber = staticInfo.PhoneNumber;
                        hotelInfo.PinCode =staticInfo.PinCode;
                        hotelInfo.Email = staticInfo.EMail;
                        hotelInfo.URL = staticInfo.URL;
                        hotelInfo.FaxNumber = staticInfo.FaxNumber;
                        Dictionary<string, string> attractions = new Dictionary<string, string>();
                        string[] attrList = staticInfo.SpecialAttraction.Split('|');
                        for (int i = 1; i < attrList.Length; i++)
                        {
                            attractions.Add(i.ToString() + ")", attrList[i - 1]);
                        }
                        hotelInfo.Attractions = attractions;
                        //if (imageInfo.DownloadedImgs != null)
                        //{
                        //    string[] dwldedImages = (imageInfo.DownloadedImgs.Length > 0 ? imageInfo.DownloadedImgs.Split('|') : imageInfo.Images.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries));
                        //    hotelInfo.Images = new List<string>(dwldedImages);
                        //}
                        //else
                        //{
                        //    hotelInfo.Images = new List<string>();
                        //}
                        List<string> facilities = new List<string>();
                        string[] facilList = staticInfo.HotelFacilities.Split('|');
                        foreach (string facl in facilList)
                        {
                            facilities.Add(facl);
                        }
                        hotelInfo.HotelFacilities = facilities;
                        List<string> roomfacilities = new List<string>();
                        string[] roomfacilList = facilList[0].Split(',');
                        foreach (string roomfacl in roomfacilList)
                        {
                            roomfacilities.Add(roomfacl);
                        }
                        hotelInfo.RoomFacilities = roomfacilities;
                         
                    }
                }
                else
                {
                    isReqSend = true;
                }
                if (isReqSend || isUpdateReq)
                {
                    request = GenerateGetItemInformation(itemCode);
                    try
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(request);
                        string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsItemInformationRequest.xml";
                        xmlDoc.Save(filePath);
                        //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsItemInformationReq.xml");
                    }
                    catch { }
                    xmlResp = SendRequest(request);

                    try
                    {
                        //XmlDocument xmlDoc = new XmlDocument();
                        //xmlDoc.LoadXml(resp);
                        string filePath =XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsItemInformationResponse.xml";
                        xmlResp.Save(filePath);
                        //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsItemInformationRes.xml");
                    }
                    catch { }
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        hotelInfo = ReadGetItemInformationResponse(xmlResp);
                    }
                    // Saving in Cache
                    if (hotelInfo.HotelName == null)
                    {
                        return hotelInfo;
                    }
                    else
                    {
                        staticInfo.HotelCode = itemCode;
                        staticInfo.HotelName = hotelInfo.HotelName;
                        staticInfo.HotelAddress = hotelInfo.Address;
                        staticInfo.CityCode = cityCode;
                        List<string> facilities = hotelInfo.HotelFacilities;
                        string facilty = string.Empty;
                        foreach (string facl in facilities)
                        {
                            facilty += facl + "|";
                        }
                        staticInfo.HotelFacilities = facilty;
                        Dictionary<string, string> attrList = hotelInfo.Attractions;
                        string attraction = string.Empty;
                        foreach (string attr in attrList.Keys)
                        {
                            attraction += attr + " : " + attrList[attr] + "|";
                        }
                        staticInfo.SpecialAttraction = attraction;
                        staticInfo.HotelLocation = hotelInfo.PhoneNumber;
                        staticInfo.HotelMap = hotelInfo.Map;
                        # region bug Id : 0029983 remove hotel description word
                        string tempHotelDescription = hotelInfo.Description.Substring(0, 17).ToUpper();
                        if (tempHotelDescription.Equals("HOTELDESCRIPTION:"))
                        {
                            hotelInfo.Description = hotelInfo.Description.Remove(0, 17);
                            staticInfo.HotelDescription = hotelInfo.Description;
                        }
                        else
                            staticInfo.HotelDescription = hotelInfo.Description;               
                        # endregion
                        staticInfo.FaxNumber = hotelInfo.FaxNumber;
                        staticInfo.EMail = hotelInfo.Email;
                        staticInfo.PinCode = hotelInfo.PinCode;
                        staticInfo.PhoneNumber = hotelInfo.PhoneNumber;
                        staticInfo.URL = hotelInfo.URL;
                        staticInfo.Source = HotelBookingSource.HotelBeds;
                        staticInfo.Rating = hotelInfo.hotelRating;
                        staticInfo.Status = true;
                        staticInfo.Save();
                        //images
                        List<string> imgList = hotelInfo.Images;
                        string images = string.Empty;
                        foreach (string img in imgList)
                        {
                            images += img + "|";
                        }
                        imageInfo.Images = images;
                        imageInfo.CityCode = cityCode;
                        imageInfo.HotelCode = itemCode;
                        imageInfo.Source = HotelBookingSource.HotelBeds;
                        //if (imageUpdate)// earlier reading image physical path
                        //{
                        //    //download the images from the server
                        //    string imageData = imageInfo.DownloadImage(imageInfo.Images, HotelBookingSource.HotelBeds);
                        //    imageInfo.DownloadedImgs = imageData;
                        //}
                        //else
                        { //Now reading image URL instead of physical image name
                            imageInfo.DownloadedImgs = images;
                        }
                        string[] dldImages = imageInfo.DownloadedImgs.Split('|');
                        List<string> dldImgList = new List<string>();
                        foreach (string img in dldImages)
                        {
                            if (!string.IsNullOrEmpty(img)) dldImgList.Add(img);
                        }
                        hotelInfo.Images = dldImgList;
                        hotelInfo.Image = dldImgList.Count > 0 ? dldImgList[0] : "";
                        //putting static data into database if exist then update only
                        
                        if (isUpdateReq)
                        {                           
                            if (imageUpdate)
                            {
                                imageInfo.Update();
                            }
                        }
                        else
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Save();
                            }
                        }
                    }
                    
                    //Audit xml only when it is request to the server otherwise blank
                    //Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, "Response HotelBeds.GetItemInformation request XML : " + request + "|response XML : " + resp, "");

                }//for request to the server
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, "Exception returned from HotelBeds.GetItemInformation Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                //Trace.TraceError("Error: " + ex.Message);
            }

            return hotelInfo;
        }

        /// <summary>
        /// UpdateHotelStaticData
        /// </summary>
        /// <param name="request">Hotel Request</param>
        /// <returns>List HotelStaticData</returns>
        public List<HotelStaticData> UpdateHotelStaticData(HotelRequest request)
        {
            List<HotelStaticData> staticData = new List<HotelStaticData>();
            try
            {
                string requestXML = GenerateGetItemInformation(request.CityCode);
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(requestXML);
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsItemInformationRequest.xml";
                    xmlDoc.Save(filePath);
                    //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsItemInformationReq.xml");
                }
                catch { }
                XmlDocument xmlResp = new XmlDocument();
                xmlResp = SendRequest(requestXML);

                try
                {
                    //XmlDocument xmlDoc = new XmlDocument();
                    //xmlDoc.LoadXml(resp);
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelBedsItemInformationResponse.xml";
                    xmlResp.Save(filePath);
                    //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\HotelBedsItemInformationRes.xml");
                }
                catch { }

                HotelDetails hotelInfo = ReadGetItemInformationResponse(xmlResp);
                // Saving in Cache
                if (hotelInfo.HotelName != null)
                {
                    HotelStaticData staticInfo = new HotelStaticData();
                    staticInfo.HotelCode = hotelInfo.HotelCode;
                    staticInfo.HotelName = hotelInfo.HotelName;
                    staticInfo.HotelAddress = hotelInfo.Address;
                    staticInfo.CityCode = request.CityCode;
                    List<string> facilities = hotelInfo.HotelFacilities;
                    string facilty = string.Empty;
                    foreach (string facl in facilities)
                    {
                        facilty += facl + "|";
                    }
                    staticInfo.HotelFacilities = facilty;
                    Dictionary<string, string> attrList = hotelInfo.Attractions;
                    string attraction = string.Empty;
                    foreach (string attr in attrList.Keys)
                    {
                        attraction += attr + " : " + attrList[attr] + "|";
                    }
                    staticInfo.SpecialAttraction = attraction;
                    staticInfo.HotelLocation = hotelInfo.PhoneNumber;
                    staticInfo.HotelMap = hotelInfo.Map;
                    # region bug Id : 0029983 remove hotel description word
                    string tempHotelDescription = hotelInfo.Description.Substring(0, 17).ToUpper();
                    if (tempHotelDescription.Equals("HOTELDESCRIPTION:"))
                    {
                        hotelInfo.Description = hotelInfo.Description.Remove(0, 17);
                        staticInfo.HotelDescription = hotelInfo.Description;
                    }
                    else
                        staticInfo.HotelDescription = hotelInfo.Description;
                    # endregion
                    staticInfo.FaxNumber = hotelInfo.FaxNumber;
                    staticInfo.EMail = hotelInfo.Email;
                    staticInfo.PinCode = hotelInfo.PinCode;
                    staticInfo.PhoneNumber = hotelInfo.PhoneNumber;
                    staticInfo.URL = hotelInfo.URL;
                    staticInfo.Source = HotelBookingSource.HotelBeds;
                    staticInfo.Rating = hotelInfo.hotelRating;
                    //images
                    List<string> imgList = hotelInfo.Images;
                    string images = string.Empty;
                    foreach (string img in imgList)
                    {
                        images += img + "|";
                    }
                    HotelImages imageInfo = new HotelImages();
                    imageInfo.Images = images;
                    imageInfo.CityCode = request.CityCode;
                    imageInfo.HotelCode = hotelInfo.HotelCode;
                    imageInfo.Source = HotelBookingSource.HotelBeds;
                    //download the images from the server
                    string imageData = imageInfo.DownloadImage(imageInfo.Images, HotelBookingSource.HotelBeds);
                    imageInfo.DownloadedImgs = imageData;

                    string[] dldImages = imageInfo.DownloadedImgs.Split('|');
                    List<string> dldImgList = new List<string>();
                    foreach (string img in dldImages)
                    {
                        dldImgList.Add(img);
                    }
                    hotelInfo.Images = dldImgList;
                    hotelInfo.Image = dldImgList.Count > 0 ? dldImgList[0] : "";
                    //putting static data into database if exist then update only
                    staticInfo.Save();
                    try
                    {
                        imageInfo.Save();
                    }
                    catch { imageInfo.Update(); }
                }

                //Audit xml only when it is request to the server otherwise blank
                //Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, "Response HotelBeds.GetItemInformation request XML : " + request + "|response XML : " + resp, "");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.High, 0, "Failed to get HotelBeds GetItemInformation : " + ex.ToString(), "");
            }
            return staticData;
        }


        /// <summary>
        /// This Method is used for Generating Unique Id
        /// </summary>
        /// <returns>string</returns>
        private string TraceIdGeneration()
        {
            string traceid = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
            traceid = "CZT" + traceid;
            return traceid;
        }
    }
}
