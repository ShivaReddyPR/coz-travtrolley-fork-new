﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;
using System.Xml;
using CT.Configuration;
using Newtonsoft.Json;
using System.Linq;
using CT.Core;

namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// This class is used to interact with GRNApi.
    /// HotelBookingSource Value for GRN=19    
    /// </summary>
    public class GRN
    {
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        string xmlPath = string.Empty;
        /// <summary>
        /// apiKey will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string apiKey = string.Empty;
        /// <summary>
        /// This variable is used, to send the language(response will be returned in same language Ex:en-us) to api, which is read from api configuration file
        /// </summary>
        string language = string.Empty;
        /// <summary>
        /// This variable is used,to send the currency(response will be returned in same currency Ex:AED) to api, which is read from api configuration file
        /// </summary>
        string currency = string.Empty;
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        decimal rateOfExchange = 1;


        #region Variables
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        int decimalPoint;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        string agentCurrency;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        string sessionId;
        /// <summary>
        /// This variable is used,to store the login userid,which is read from MetaserchEngine  
        /// </summary>
        long grnUserId;
        #endregion
        #region Properties
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>

        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get { return sourceCountryCode; }
            set { sourceCountryCode = value; }
        }
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }
        /// <summary>
        /// this GrnUserId is store user id
        /// </summary>
        public long GrnUserId
        {
            get { return grnUserId; }
            set { grnUserId = value; }
        }



        #endregion
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public GRN()
        {
            LoadCredentials();
        }
        #endregion
        /// <summary>
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        private void LoadCredentials()
        {
            //Create Hotel Xml Log path folder per day wise
            xmlPath = ConfigurationSystem.GrnConfig["XmlLogPath"];
            xmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            apiKey = ConfigurationSystem.GrnConfig["APIKey"];
            language = ConfigurationSystem.GrnConfig["Lang"];
            currency = ConfigurationSystem.GrnConfig["Currency"];
            try
            {
                if (!System.IO.Directory.Exists(xmlPath))
                {
                    System.IO.Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
        }
        #region Common Methods
        /// <summary>
        /// Method for retriving response based on the POST request and url
        /// </summary>
        /// <param name="requestData">Request Object</param>
        /// <param name="url">Api url</param>
        /// <returns>responseFromServer</returns>     
        private string GetResponse(string requestData, string url)
        {
            string responseFromServer = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = "POST"; //Using POST method  

                string postData = requestData;// GETTING JSON STRING...               
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                //byte[] bytes1 = System.Text.ASCIIEncoding.UTF8.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.ContentType = "application/json";
                request.Accept = "application/json";
                //request.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                //request.Headers.Add(HttpRequestHeader.Accept, "application/json");
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "application/gzip");
                request.Headers.Add("api-key", apiKey);
                request.ContentLength = bytes.Length;
                Stream requestWriter = (request.GetRequestStream());
                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();
               if(url.StartsWith("https"))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }
                
                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
            }
            catch (WebException webEx)
            {
                //get the response stream
                Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.GetResponse Err :" + webEx.ToString() + url, "0");
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
            }
            return responseFromServer;
        }
        /// <summary>
        /// Method for retriving response based on the GET() by using url
        /// </summary>       
        /// <param name="url">Api url</param>
        /// <returns>responseFromServer</returns>   
        private string ReadGetResponse(string url)
        {
            string responseFromServer = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET"; //Using GET method  
                // request for compressed response. This does not seem to have any effect now.
                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "application/gzip");
                request.Headers.Add("api-key", apiKey);
                if (url.StartsWith("https"))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }
                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }
            catch (WebException webEx)
            {
                //get the response stream
                Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.SendGetRequest Err :" + webEx.ToString() + url, "0");
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
            }
            return responseFromServer;
        }
        /// <summary>
        /// Method for retriving response based on the DELETE() by using url
        /// </summary>       
        /// <param name="url">Api url</param>
        /// <returns>responseFromServer</returns>   
        private string SendDeleteRequest(string url)
        {
            string responseFromServer = string.Empty;
            string responseXML = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "DELETE"; //Using Delete method  
                // request for compressed response. This does not seem to have any effect now.
                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "application/gzip");
                request.Headers.Add("api-key", apiKey);
                if (url.StartsWith("https"))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }
                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
            }
            catch (WebException webEx)
            {
                //get the response stream
                Audit.Add(EventType.GrnCancel, Severity.Normal, 1, "GRN.SendDeleteRequest Err :" + webEx.ToString() + url, "0");
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
            }
            return responseFromServer;
        }

        //convert compressed
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        #endregion

        #region HotelSearch
        HotelRequest request = new HotelRequest();
        decimal markup = 0;
        string markupType = string.Empty;
        //int rowStartIndex = 0, rowEndIndex = 0;
        DataTable dtHotels = new DataTable();
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        List<HotelSearchResult> results = new List<HotelSearchResult>();
        public HotelSearchResult[] GetHotelAvailability(HotelRequest req, decimal markup, string markupType)
        {
            this.request = req;
            this.markup = markup;
            this.markupType = markupType;
            results = new List<HotelSearchResult>();

            //This dictionary will hold what fare search we want to call
            Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
            //ReadySources will hold same source orderings i.e SG1, SG2 ....
            Dictionary<string, int> readySources = new Dictionary<string, int>();
            Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.GetHotelAvailability Entered", "0");
            //records means number of hotels passed in each request
            int records = Convert.ToInt32(ConfigurationSystem.GrnConfig["hotelCount"]), rowsCount = 0, startIndex = 0, endIndex = 0;
            HotelSearchResult[] searchRes = new HotelSearchResult[0];
            HotelSearchResult[] result = new HotelSearchResult[0];
            dtHotels = HotelStaticData.GetStaticHotelIds(req.CityCode, HotelBookingSource.GRN, 0);
            rowsCount = dtHotels.Rows.Count;

            int i = 0;
            int requestHotelCount = Convert.ToInt32(ConfigurationSystem.GrnConfig["hotelCount"]);
            while (rowsCount > 0)
            {
                if (records <= requestHotelCount)
                {
                    startIndex = 0;
                    endIndex = records;
                }
                else
                {
                    startIndex += requestHotelCount;
                    endIndex = records;
                }
                string key = i.ToString() + "-" + startIndex + "-" + endIndex;
                listOfThreads.Add(key, new WaitCallback(GenarateSearchResults));
                readySources.Add(key, 10);
                i++;
                rowsCount -= requestHotelCount;
                records += requestHotelCount;
            }
            eventFlag = new AutoResetEvent[readySources.Count];
            int j = 0;
            //Start each fare search within a Thread which will automatically terminate after the results are received.
            foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
            {
                if (readySources.ContainsKey(deThread.Key))
                {
                    ThreadPool.QueueUserWorkItem(deThread.Value, deThread.Key);
                    eventFlag[j] = new AutoResetEvent(false);
                    j++;
                }
            }
            if (j != 0)
            {
                if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 300), true))
                {
                    //TODO: audit which thread is timed out                
                }
            }
            searchRes = results.ToArray();
            return searchRes;
        }
        private void GenarateSearchResults(object eventNumber)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string request = string.Empty;
            string response = string.Empty;
            string[] values = eventNumber.ToString().Split('-');
            try
            {
                request = GenerateHotelSerchRequest(this.request, Convert.ToInt32(values[1]), Convert.ToInt32(values[2]), dtHotels);
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_AvailabilityREQ_" + values[0] + ".xml";
                    XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(request, "Root");
                    xmlDocRes.Save(filePath);
                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_AvailabilityREQ_" + values[0] + ".txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(request.ToString());
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.GenerateHotelSerchRequest Err :" + ex.ToString(), "0");
                throw ex;
            }
            #region Get Resposne from GRN
            try
            {
                response = GetResponse(request, ConfigurationSystem.GrnConfig["Search"]);

                try
                {
                    string filePath = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_AvailabilityRES_" + values[0] + ".xml";
                    xmlDoc = JsonConvert.DeserializeXmlNode(response, "Root");
                    // xmlDocRes.LoadXml(request);
                    //xmlDoc.Save(filePath);
                    //JsonFormat
                    string PathJson = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_AvailabilityRES_" + values[0] + ".txt";
                    StreamWriter sw = new StreamWriter(PathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.GetResponse Err :" + ex.ToString(), "0");
                // throw new BookingEngineException("Error: " + ex.Message);
            }
            #region Generate HotelSearchResult Object
            try
            {
                if (xmlDoc != null && xmlDoc.ChildNodes != null && xmlDoc.ChildNodes.Count > 0)
                {
                    GenerateHotelSearchResult(xmlDoc, this.request, ref results, ref dtHotels, markup, markupType, values[0]);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.GenerateHotelSearchResult  Err :" + ex.ToString(), "0");
                throw ex;
            }
            #endregion
            //Returns:   true if the operation succeeds; otherwise, false.
            eventFlag[Convert.ToInt32(values[0])].Set();
        }

        private void GenerateHotelSearchResult(XmlDocument xmlDoc, HotelRequest req, ref List<HotelSearchResult> hotelResults, ref DataTable dtHotels, decimal markup, string markupType, string eventNumber)
        {
            try
            {

                DataTable dtImages = HotelImages.GetImagesByCityCode(request.CityCode, HotelBookingSource.GRN);
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_AvailabilityRES_" + eventNumber + ".xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                XmlNode errorInfo = xmlDoc.SelectSingleNode("Root/errors/messages");
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.GrnSearch, Severity.High, 0, "Error Message:" + errorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                }
                else
                {
                    //Loading all Vat charges
                    bool isBedTypes = false;
                    PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                    string searchId = string.Empty;
                    XmlNode tempNode = xmlDoc.SelectSingleNode("Root/search_id");
                    if (tempNode != null)
                    {
                        searchId = tempNode.InnerText;
                    }
                    else
                    {
                        return;
                    }
                    XmlNodeList xmlhotellist = xmlDoc.SelectNodes("Root/hotels");
                    if (xmlhotellist != null)
                    {
                        foreach (XmlNode hotels in xmlhotellist)
                        {
                            HotelSearchResult hotelResult = new HotelSearchResult();
                            hotelResult.PropertyType = searchId;
                            hotelResult.BookingSource = HotelBookingSource.GRN;
                            hotelResult.CityCode = req.CityCode;
                            hotelResult.StartDate = request.StartDate;
                            hotelResult.EndDate = request.EndDate;

                            tempNode = hotels.SelectSingleNode("hotel_code");
                            if (tempNode != null)
                            {
                                hotelResult.HotelCode = tempNode.InnerText;
                            }
                            string pattern = request.HotelName;
                            #region search time avoid Staticinfo downloading
                            DataRow[] hotelStaticData = new DataRow[0];
                            try
                            {
                                hotelStaticData = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");
                                if (hotelStaticData != null && hotelStaticData.Length > 0)
                                {
                                    hotelResult.HotelDescription = hotelStaticData[0]["description"].ToString();
                                    hotelResult.HotelAddress = hotelStaticData[0]["address"].ToString();
                                    hotelResult.HotelMap = hotelStaticData[0]["hotelMaps"].ToString();
                                    hotelResult.HotelLocation = hotelStaticData[0]["location"].ToString();
                                    hotelResult.HotelName = hotelStaticData[0]["hotelName"].ToString();

                                }
                            }
                            catch { continue; }

                            tempNode = hotels.SelectSingleNode("category");
                            if (tempNode != null)
                            {
                                hotelResult.Rating = (HotelRating)Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(tempNode.InnerText)));
                                //some times rating given 3.5 that time we need to show 4.0 only                        
                            }
                            DataRow[] hotelImages = new DataRow[0];
                            try
                            {
                                hotelImages = dtImages.Select("hotelCode='" + hotelResult.HotelCode + "'");
                                string hImages = (hotelImages != null && hotelImages.Length > 0 ? hotelImages[0]["images"].ToString() : string.Empty);
                                hotelResult.HotelPicture = string.Empty;
                                if (!string.IsNullOrEmpty(hImages))
                                {
                                    hotelResult.HotelPicture = hImages.Split('|')[0];
                                }
                            }
                            catch { continue; }

                            #endregion
                            #region To get only those satisfy the search conditions
                            if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                            {
                                if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                continue;
                            }
                            #endregion

                            ///Rooms Binding

                            HotelRoomsDetails[] rooms = new HotelRoomsDetails[0];
                            bool isNonBundeled = false;
                            XmlNodeList rateList = hotels.SelectNodes("rates");
                            hotelResult.RoomGuest = request.RoomGuest;
                            List<HotelRoomsDetails> roomdetailList = new List<HotelRoomsDetails>();
                            int noOfDays = req.EndDate.Subtract(req.StartDate).Days;
                            if (rateList != null)
                            {
                                int i = 0;
                                foreach (XmlNode roomList in rateList)
                                {
                                    int tootalrooms = req.NoOfRooms;
                                    XmlNodeList roomNodeList = roomList.SelectNodes("rooms");
                                    if (roomNodeList.Count != request.NoOfRooms)
                                    {
                                        isNonBundeled = true;
                                    }
                                    isBedTypes = false;
                                    XmlNodeList bedTypeList = roomList.SelectNodes("rooms/bed_types");
                                    HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                                    foreach (XmlNode roomNode in roomNodeList)
                                    {
                                        XmlNodeList bedTypeCount = roomNode.SelectNodes("bed_types");
                                        if (bedTypeCount != null && bedTypeCount.Count > 0)
                                        {
                                            isBedTypes = true;
                                        }
                                        string adultCount = string.Empty;

                                        string childCount = string.Empty;
                                        tempNode = roomNode.SelectSingleNode("no_of_adults");
                                        if (tempNode != null)
                                        {
                                            adultCount = tempNode.InnerText;
                                        }
                                        tempNode = roomNode.SelectSingleNode("no_of_children");
                                        if (tempNode != null)
                                        {
                                            childCount = tempNode.InnerText;
                                        }
                                        List<RoomGuestData> hotelRooms = new List<RoomGuestData>(request.RoomGuest);
                                        var equalRooms = hotelRooms.All(h => h.noOfAdults == Convert.ToInt32(adultCount) && h.noOfChild == Convert.ToInt32(childCount));
                                        if (equalRooms)
                                        {
                                            roomDetail.SequenceNo = (i + 1).ToString();
                                            i++;
                                        }
                                        else
                                        {
                                            for (int k = 0; k < request.NoOfRooms; k++)
                                            {
                                                if (request.RoomGuest[k].noOfAdults == Convert.ToInt32(adultCount) && request.RoomGuest[k].noOfChild == Convert.ToInt32(childCount))
                                                {
                                                    roomDetail.SequenceNo = (k + 1).ToString();
                                                    break;
                                                }
                                            }

                                        }
                                        //else
                                        //{
                                        //    roomDetail.SequenceNo = (tootalrooms).ToString();
                                        //    tootalrooms--;
                                        //}
                                        tempNode = roomNode.SelectSingleNode("description");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeName = tempNode.InnerText;
                                        }
                                        else
                                        {
                                            tempNode = roomNode.SelectSingleNode("room_type");
                                            if (tempNode != null)
                                            {
                                                roomDetail.RoomTypeName = tempNode.InnerText;
                                            }
                                        }
                                        tempNode = roomList.SelectSingleNode("room_code");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeCode = roomDetail.SequenceNo + "|" + hotelResult.PropertyType + "|" + tempNode.InnerText;

                                        }
                                        tempNode = roomList.SelectSingleNode("rate_type");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                        }
                                        tempNode = roomList.SelectSingleNode("rate_key");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RatePlanCode = tempNode.InnerText;
                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                        }
                                        tempNode = roomList.SelectSingleNode("group_code");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                        }
                                        tempNode = roomNode.SelectSingleNode("room_reference");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                        }
                                        else
                                        {
                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|";
                                        }

                                        tempNode = roomList.SelectSingleNode("has_promotions");
                                        if (tempNode != null && tempNode.InnerText == "true")
                                        {
                                            tempNode = roomList.SelectSingleNode("promotions_details");
                                            if (tempNode != null)
                                            {
                                                roomDetail.PromoMessage = tempNode.InnerText;
                                                hotelResult.PromoMessage = tempNode.InnerText;
                                            }
                                        }
                                        tempNode = roomList.SelectSingleNode("includes_boarding");
                                        if (tempNode != null && tempNode.InnerText == "true")
                                        {
                                            tempNode = roomList.SelectSingleNode("boarding_details");
                                            if (tempNode != null)
                                            {
                                                roomDetail.mealPlanDesc = tempNode.InnerText;
                                            }
                                        }
                                        if (string.IsNullOrEmpty(roomDetail.mealPlanDesc))
                                        {
                                            roomDetail.mealPlanDesc = "Room Only";
                                        }

                                        #region Price Calculation   
                                        tempNode = roomList.SelectSingleNode("currency");
                                        if (tempNode != null)
                                        {
                                            hotelResult.Currency = (tempNode.InnerText);
                                        }
                                        rateOfExchange = (exchangeRates.ContainsKey(hotelResult.Currency) ? exchangeRates[hotelResult.Currency] : 1);
                                        XmlNodeList roomprices = roomList.SelectNodes("price_details/hotel_charges");
                                        if (roomprices != null)
                                        {
                                            foreach (XmlNode surprices in roomprices)
                                            {
                                                tempNode = surprices.SelectSingleNode("included");
                                                if (tempNode != null && tempNode.InnerText == "false")
                                                {
                                                    decimal mandaterycharge = 0m;
                                                    tempNode = surprices.SelectSingleNode("amount");
                                                    if (tempNode != null)
                                                    {
                                                        mandaterycharge = Convert.ToDecimal(tempNode.InnerText);
                                                        mandaterycharge = mandaterycharge * rateOfExchange;
                                                    }
                                                    tempNode = surprices.SelectSingleNode("name");
                                                    if (tempNode != null)
                                                    {
                                                        roomDetail.EssentialInformation = tempNode.InnerText + " " + mandaterycharge.ToString();
                                                    }

                                                    if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                                    {
                                                        roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + "<b> Not Included in price(collected by the properity)</b>:";
                                                    }
                                                    else
                                                    {
                                                        roomDetail.EssentialInformation = " <b> Not Included in price(collected by the properity)</b>:";
                                                    }
                                                }
                                            }

                                            decimal totPrice = 0m;
                                            tempNode = roomList.SelectSingleNode("price");
                                            if (tempNode != null)
                                            {
                                                totPrice = Convert.ToDecimal(tempNode.InnerText);
                                            }
                                            tempNode = roomList.SelectSingleNode("no_of_rooms");
                                            if (tempNode != null && tempNode.InnerText == req.NoOfRooms.ToString())
                                            {
                                                totPrice = totPrice / req.NoOfRooms;
                                            }
                                            decimal hotelTotalPrice = 0m;
                                            decimal vatAmount = 0m;
                                            hotelTotalPrice = Math.Round((totPrice) * rateOfExchange, decimalPoint);
                                            if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                            {
                                                hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                            }
                                            hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                                            roomDetail.TotalPrice = hotelTotalPrice;
                                            roomDetail.TotalPrice = hotelTotalPrice;
                                            roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                                            roomDetail.MarkupType = markupType;
                                            roomDetail.MarkupValue = markup;
                                            roomDetail.SellingFare = roomDetail.TotalPrice;
                                            roomDetail.supplierPrice = Math.Round((totPrice));
                                            roomDetail.TaxDetail = new PriceTaxDetails();
                                            roomDetail.TaxDetail = priceTaxDet;
                                            roomDetail.InputVATAmount = vatAmount;
                                            hotelResult.Price = new PriceAccounts();
                                            hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                            hotelResult.Price.SupplierPrice = Math.Round((totPrice));
                                            hotelResult.Price.RateOfExchange = rateOfExchange;
                                        }
                                        //day wise price calucation
                                        System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                        RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                        decimal totalprice = roomDetail.TotalPrice;

                                        for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                        {
                                            decimal price = roomDetail.TotalPrice / diffResult.Days;
                                            if (fareIndex == diffResult.Days - 1)
                                            {
                                                price = totalprice;
                                            }
                                            totalprice -= price;
                                            hRoomRates[fareIndex].Amount = price;
                                            hRoomRates[fareIndex].BaseFare = price;
                                            hRoomRates[fareIndex].SellingFare = price;
                                            hRoomRates[fareIndex].Totalfare = price;
                                            hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                            hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                                        }
                                        roomDetail.Rates = hRoomRates;

                                        #region Cancellation Policy
                                        XmlNodeList cancellationPolicyList = roomList.SelectNodes("cancellation_policy");
                                        //int buffer = Convert.ToInt32(ConfigurationSystem.AgodaConfig["buffer"]);need to discuss with vinay
                                        string message = string.Empty;
                                        decimal chargeAmt = 0m;
                                        if (cancellationPolicyList != null)
                                        {
                                            foreach (XmlNode cancelNode in cancellationPolicyList)
                                            {
                                                XmlNode undercancellation = cancelNode.SelectSingleNode("details");
                                                if (undercancellation != null)
                                                {
                                                    XmlNode chargeValue = undercancellation.SelectSingleNode("flat_fee");
                                                    XmlNode chargeCurrency = undercancellation.SelectSingleNode("currency");
                                                    if (chargeValue != null && chargeCurrency != null)
                                                    {
                                                        chargeAmt = Convert.ToDecimal((Convert.ToDecimal(chargeValue.InnerText) * rateOfExchange).ToString("N" + decimalPoint));
                                                    }
                                                    tempNode = roomList.SelectSingleNode("no_of_rooms");
                                                    if (tempNode != null && tempNode.InnerText == request.NoOfRooms.ToString())
                                                    {
                                                        chargeAmt = chargeAmt / request.NoOfRooms;
                                                    }
                                                    XmlNode details = undercancellation.SelectSingleNode("from");
                                                    if (details != null)
                                                    {
                                                        int buffer = Convert.ToInt32(ConfigurationSystem.GrnConfig["buffer"]);
                                                        DateTime beforeDate = Convert.ToDateTime(details.InnerText);
                                                        beforeDate = beforeDate.AddDays(-buffer);

                                                        if (!string.IsNullOrEmpty(message))
                                                        {
                                                            message = message + "|" + "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ", you will be charged " + agentCurrency + " " + chargeAmt;
                                                        }
                                                        else
                                                        {
                                                            message = "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ", you will be charged " + agentCurrency + " " + chargeAmt;
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        roomDetail.CancellationPolicy = message;
                                        #endregion
                                        #endregion
                                        if (isBedTypes)
                                        {
                                            foreach (XmlNode bedTypesnode in bedTypeCount)
                                            {
                                                tempNode = bedTypesnode.SelectSingleNode("type");
                                                if (tempNode != null)
                                                {
                                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                                }
                                                tempNode = bedTypesnode.SelectSingleNode("id");
                                                if (tempNode != null)
                                                {
                                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                                }



                                                for (int k = 0; k < request.NoOfRooms; k++)
                                                {
                                                    if (request.RoomGuest[k].noOfAdults == Convert.ToInt32(adultCount) && request.RoomGuest[k].noOfChild == Convert.ToInt32(childCount))
                                                    {
                                                        roomDetail.SequenceNo = (k + 1).ToString();
                                                        List<string> sequenceList = roomDetail.RoomTypeCode.Split('|').ToList();
                                                        sequenceList[0] = (k + 1).ToString();
                                                        roomDetail.RoomTypeCode = string.Join("|", sequenceList);
                                                        roomdetailList.Add(roomDetail);
                                                    }
                                                }

                                            }
                                        }
                                        else
                                        {

                                            for (int k = 0; k < request.NoOfRooms; k++)
                                            {
                                                if (request.RoomGuest[k].noOfAdults == Convert.ToInt32(adultCount) && request.RoomGuest[k].noOfChild == Convert.ToInt32(childCount))
                                                {
                                                    roomDetail.SequenceNo = (k + 1).ToString();
                                                    List<string> sequenceList = roomDetail.RoomTypeCode.Split('|').ToList();
                                                    sequenceList[0] = (k + 1).ToString();
                                                    roomDetail.RoomTypeCode = string.Join("|", sequenceList);
                                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + "|";
                                                    roomdetailList.Add(roomDetail);
                                                }
                                            }
                                        }

                                    }
                                    roomdetailList = roomdetailList.OrderBy(p => p.supplierPrice).ToList();
                                    hotelResult.RoomDetails = roomdetailList.ToArray();
                                }

                                hotelResult.Currency = agentCurrency;
                                for (int m = 0; m < req.NoOfRooms; m++)
                                {
                                    for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                                    {
                                        if (hotelResult.RoomDetails[j].SequenceNo.Contains((m + 1).ToString()))
                                        {
                                            hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                            hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                            hotelResult.Price.AccPriceType = PriceType.NetFare;
                                            hotelResult.Price.RateOfExchange = rateOfExchange;
                                            hotelResult.Price.Currency = agentCurrency;
                                            hotelResult.Price.CurrencyCode = agentCurrency;
                                            break;
                                        }
                                    }

                                }
                                bool validHotelResult = true;
                                for (int p = 0; p < req.NoOfRooms; p++)
                                {

                                    if (!(hotelResult.RoomDetails.Any(hotel => hotel.SequenceNo == (p + 1).ToString())))
                                    {
                                        validHotelResult = false;
                                        break;
                                    }
                                }
                                if (validHotelResult)
                                {
                                    hotelResults.Add(hotelResult);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.GenerateHotelSearchResult Err :" + ex.ToString(), "0");
                throw ex;
            }
        }
        #endregion

        private struct SearchRequest
        {
            public string[] hotel_codes;
            // public string destination_code;
            public string checkin;
            public string checkout;
            public string client_nationality;
            public string currency;
            public bool hotel_info;
            public string rates;
            // public string response;
            public bool more_results;
            public int[] hotel_category;
            public RoomGuest[] rooms;
            public int cutoff_time;
        }
        private struct RoomGuest
        {
            public int adults;
            public int[] children_ages;
        }
        private struct RecheckRequest
        {
            public string rate_key;
            public string group_code;
            public bool hotel_info;
        }
        private struct BookRequest
        {
            public string search_id;
            public string hotel_code;
            public string city_code;
            public string group_code;
            public string checkin;
            public string checkout;
            public string payment_type;
            public Holder holder;
            public BookingItems[] booking_items;
        }
        private struct Holder
        {
            public string title;
            public string name;
            public string surname;
            public string email;
            public string phone_number;
            public string client_nationality;
        }
        private struct BookingItems
        {
            public string rate_key;
            public string room_code;
            public BookingRooms[] rooms;
        }
        private struct BookingRooms
        {
            public string room_reference;
            public Pax[] paxes;

        }
        private struct Pax
        {
            public string title;
            public string name;
            public string surname;
            public string type;
            public int age;
        }
        private struct Refetch
        {
            public string sid;
            public string hcode;
            public bool hotel_info;
        }
        private struct CancellationPolicy
        {
            public string rate_key;
            public string cp_code;
        }
        private struct BookingCancel
        {
            public string bookingReference;
        }
        /// <summary>
        /// Creating HotelReuest object
        /// </summary>
        public string GenerateHotelSerchRequest(HotelRequest req, int startIndex, int endIndex, DataTable dtHotels)
        {
            SearchRequest structRequest = new SearchRequest();
            var request = string.Empty;
            try
            {
                string hotelIds = string.Empty;
                List<string> hotelcodes = new List<string>();

                for (int i = startIndex; i < endIndex; i++)
                {
                    if (i < dtHotels.Rows.Count && dtHotels.Rows[i]["HotelCode"] != DBNull.Value)
                    {
                        //hotelcodes.Add("H!0075039");
                        hotelcodes.Add(dtHotels.Rows[i]["HotelCode"].ToString().Trim());
                    }
                }
                structRequest.hotel_codes = hotelcodes.ToArray();
                structRequest.checkin = req.StartDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                structRequest.checkout = req.EndDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                structRequest.client_nationality = req.PassengerNationality;
                structRequest.currency = ConfigurationSystem.GrnConfig["Currency"];// As per Supplier ,we are hard coding
                structRequest.more_results = false;//get the results within cutofftime only
                structRequest.cutoff_time = 50000;
                structRequest.hotel_info = false;//requesting without hotel information(all ready static data available)
                structRequest.rates = "comprehensive";// As per Supplier ,we are hard coding for hotelwise all Roomrates
                structRequest.hotel_category = new int[2];
                structRequest.hotel_category[0] = 2;//min * rate
                structRequest.hotel_category[1] = req.MaxRating;//max * rate
                RoomGuest[] roomGuest = new RoomGuest[req.NoOfRooms];
                for (int i = 0; i < req.RoomGuest.Length; i++)
                {
                    roomGuest[i].adults = req.RoomGuest[i].noOfAdults;
                    if (req.RoomGuest[i].noOfChild > 0)
                    {
                        roomGuest[i].children_ages = new int[req.RoomGuest[i].noOfChild];
                        roomGuest[i].children_ages = req.RoomGuest[i].childAge.ToArray();
                    }
                }
                structRequest.rooms = roomGuest;
                request = JsonConvert.SerializeObject(structRequest,
               new JsonSerializerSettings()
               {
                   NullValueHandling = NullValueHandling.Ignore
               });
                // To convert JSON text contained in string json into an XML node

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.GenerateHotelSerchRequest Err :" + ex.ToString(), "0");
                throw ex;
            }
            return request;

        }
        #endregion
        #region recheck
        /// <summary>
        /// Price Validating before booking
        /// </summary>
        public void GetRoomPriceValidate(ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType, string searchid, string ratekey, string groupcode)
        {
            try
            {
                RecheckRequest recheckRequest = new RecheckRequest();
                recheckRequest.group_code = groupcode;
                recheckRequest.rate_key = ratekey;
                recheckRequest.hotel_info = true;
                var requestrecheck = JsonConvert.SerializeObject(recheckRequest,
               new JsonSerializerSettings()
               {
                   NullValueHandling = NullValueHandling.Ignore
               });
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(requestrecheck, "Root");
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + searchid + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_RoomRateRecheckDataRequest.xml"; ;
                    xmlDocRes.Save(filePath);
                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + searchid + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_RoomRateRecheckDataRequest.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(requestrecheck.ToString());
                    sw.Close();
                }
                catch { }
                string resp = string.Empty;
                try
                {
                    resp = GetResponse(requestrecheck, ConfigurationSystem.GrnConfig["Search"] + "/" + searchid + "/rates/?action=recheck");
                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    ReadRecheckRoomDetails(resp, ref hotelResult, request, markup, markupType);

                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnRecheck, Severity.Normal, 1, "GRN.GetRoomPriceValidate Err :" + ex.Message, "0");
                throw ex;
            }

        }
        /// <summary>
        ///Read the Price details of selected Rooms
        /// </summary>
        private void ReadRecheckRoomDetails(string response, ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType)
        {
            try
            {
                HotelRoomsDetails[] rmRecheck = new HotelRoomsDetails[0];
                string searchId = string.Empty;
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + searchId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_RoomRateRecheckData.xml"; ;
                    xmlDocRes.Save(filePath);

                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + searchId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_RoomRateRecheckData.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }
                catch { }
                XmlNode tempNode = xmlDocRes.SelectSingleNode("Root/search_id");
                if (tempNode != null)
                {
                    searchId = tempNode.InnerText;
                }
                //else
                //{
                //    return;
                //}

                XmlNode errorInfo = xmlDocRes.SelectSingleNode("Root/errors/messages");
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    throw new Exception(errorInfo.InnerText);
                }
                else
                {
                    //Loading all Vat charges
                    PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());

                    XmlNode hotels = xmlDocRes.SelectSingleNode("Root/hotel");
                    if (hotels != null)
                    {
                        //HotelSearchResult hotelRoomResult = new HotelSearchResult();
                        hotelResult.PropertyType = searchId;
                        hotelResult.BookingSource = HotelBookingSource.GRN;
                        tempNode = hotels.SelectSingleNode("city_code");
                        if (tempNode != null)
                        {
                            hotelResult.CityCode = tempNode.InnerText;
                        }
                        hotelResult.StartDate = request.StartDate;
                        hotelResult.EndDate = request.EndDate;

                        tempNode = hotels.SelectSingleNode("hotel_code");
                        if (tempNode != null)
                        {
                            hotelResult.HotelCode = tempNode.InnerText;
                        }
                        string pattern = request.HotelName;

                        tempNode = hotels.SelectSingleNode("category");
                        if (tempNode != null)
                        {
                            hotelResult.Rating = (HotelRating)Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(tempNode.InnerText)));
                            //some times rating given 3.5 that time we need to show 4.0 only                        
                        }
                        DataRow[] hotelImages = new DataRow[0];
                        try
                        {
                            tempNode = hotels.SelectSingleNode("images/url");
                            if (tempNode != null)
                            {
                                hotelResult.HotelPicture = tempNode.InnerText;
                            }
                        }
                        catch { }
                        ///Rooms Binding
                        XmlNode roomList = hotels.SelectSingleNode("rate");
                        tempNode = roomList.SelectSingleNode("currency");
                        rateOfExchange = (exchangeRates.ContainsKey(tempNode.InnerText) ? exchangeRates[tempNode.InnerText] : 1);
                        hotelResult.RoomGuest = request.RoomGuest;
                        int noOfDays = request.EndDate.Subtract(request.StartDate).Days;
                        if (roomList != null)
                        {
                            List<HotelRoomsDetails> rooms = new List<HotelRoomsDetails>();
                            bool isBedTypes = false;
                            int index = 0;
                            XmlNodeList roomNodeList = roomList.SelectNodes("rooms");
                            XmlNodeList bedTypeList = roomList.SelectNodes("rooms/bed_types");
                            if (bedTypeList != null && bedTypeList.Count > 0)
                            {
                                isBedTypes = true;
                            }
                            HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                            int i = 0;
                            foreach (XmlNode roomNode in roomNodeList)
                            {
                                string adultCount = string.Empty;

                                string childCount = string.Empty;
                                tempNode = roomNode.SelectSingleNode("no_of_adults");
                                if (tempNode != null)
                                {
                                    adultCount = tempNode.InnerText;
                                }
                                tempNode = roomNode.SelectSingleNode("no_of_children");
                                if (tempNode != null)
                                {
                                    childCount = tempNode.InnerText;
                                }
                                XmlNodeList bedTypeCount = roomNode.SelectNodes("bed_types");
                                for (int k = 0; k < request.NoOfRooms; k++)
                                {
                                    if (request.RoomGuest[k].noOfAdults == Convert.ToInt32(adultCount) && request.RoomGuest[k].noOfChild == Convert.ToInt32(childCount))
                                    {
                                        roomDetail.SequenceNo = (k + 1).ToString();
                                        break;
                                    }
                                }
                                tempNode = roomNode.SelectSingleNode("description");
                                if (tempNode != null)
                                {
                                    roomDetail.RoomTypeName = tempNode.InnerText;
                                }
                                else
                                {
                                    tempNode = roomNode.SelectSingleNode("room_type");
                                    if (tempNode != null)
                                    {
                                        roomDetail.RoomTypeName = tempNode.InnerText;
                                    }
                                }
                                tempNode = roomList.SelectSingleNode("room_code");
                                if (tempNode != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.SequenceNo + "|" + hotelResult.PropertyType + "|" + tempNode.InnerText;

                                }
                                tempNode = roomList.SelectSingleNode("rate_type");
                                if (tempNode != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                }
                                tempNode = roomList.SelectSingleNode("rate_key");
                                if (tempNode != null)
                                {
                                    roomDetail.RatePlanCode = tempNode.InnerText;
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                }
                                tempNode = roomList.SelectSingleNode("group_code");
                                if (tempNode != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                }
                                tempNode = roomNode.SelectSingleNode("room_reference");
                                if (tempNode != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                }
                                else
                                {
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|";
                                }
                                //RoomType Code--- Seq|SearchId|RoomCode|Ratetype|RateKey|groupcode|Room Reference|BedType|BedId                                
                                tempNode = roomList.SelectSingleNode("has_promotions");
                                if (tempNode != null && tempNode.InnerText == "true")
                                {
                                    tempNode = roomList.SelectSingleNode("promotions_details");
                                    if (tempNode != null)
                                    {
                                        roomDetail.PromoMessage = tempNode.InnerText;
                                    }
                                }
                                tempNode = roomList.SelectSingleNode("includes_boarding");
                                if (tempNode != null && tempNode.InnerText == "true")
                                {
                                    tempNode = roomList.SelectSingleNode("boarding_details");
                                    if (tempNode != null)
                                    {
                                        roomDetail.mealPlanDesc = tempNode.InnerText;
                                    }
                                }
                                if (string.IsNullOrEmpty(roomDetail.mealPlanDesc))
                                {
                                    roomDetail.mealPlanDesc = "Room Only";
                                }

                                #region Price Calculation                                   
                                XmlNodeList roomprices = roomList.SelectNodes("price_details/hotel_charges");
                                if (roomprices != null)
                                {
                                    foreach (XmlNode surprices in roomprices)
                                    {
                                        tempNode = surprices.SelectSingleNode("included");
                                        if (tempNode != null && tempNode.InnerText == "false")
                                        {
                                            decimal mandaterycharge = 0m;
                                            tempNode = surprices.SelectSingleNode("amount");
                                            if (tempNode != null)
                                            {
                                                mandaterycharge = Convert.ToDecimal(tempNode.InnerText);
                                                mandaterycharge = mandaterycharge * rateOfExchange;
                                            }
                                            tempNode = surprices.SelectSingleNode("name");
                                            if (tempNode != null)
                                            {
                                                roomDetail.EssentialInformation = tempNode.InnerText + " " + mandaterycharge.ToString();
                                            }

                                            if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                            {
                                                roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + "<b>Not Included in price(collected by the properity)</b>:";
                                            }
                                            else
                                            {
                                                roomDetail.EssentialInformation = "<b>Not Included in price(collected by the properity)</b>:";
                                            }
                                        }
                                    }
                                    XmlNode rateComments = roomList.SelectSingleNode("rate_comments");
                                    if (rateComments != null)
                                    {
                                        tempNode = rateComments.SelectSingleNode("spl_checkin_instructions");
                                        if (tempNode != null)
                                        {
                                            if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                            {
                                                roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + tempNode.InnerText;
                                            }
                                            else
                                            {
                                                roomDetail.EssentialInformation = tempNode.InnerText;
                                            }
                                        }
                                        tempNode = rateComments.SelectSingleNode("checkin_instructions");
                                        if (tempNode != null)
                                        {
                                            if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                            {
                                                roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + tempNode.InnerText;
                                            }
                                            else
                                            {
                                                roomDetail.EssentialInformation = tempNode.InnerText;
                                            }
                                        }
                                    }
                                    XmlNodeList  otherInclusions = roomList.SelectNodes("other_inclusions");
                                    foreach(XmlNode inclusions in otherInclusions)
                                    {
                                        tempNode = inclusions.SelectSingleNode("other_inclusions");
                                        if (tempNode != null)
                                        {
                                            if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                            {
                                                roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + tempNode.InnerText;
                                            }
                                            else
                                            {
                                                roomDetail.EssentialInformation = tempNode.InnerText;
                                            }
                                        }
                                    }

                                    roomDetail.Amenities = new List<string>();
                                    tempNode = hotels.SelectSingleNode("facilities");
                                    if (tempNode != null)
                                    {
                                        roomDetail.Amenities.Add(tempNode.InnerText);
                                    }
                                    tempNode = roomList.SelectSingleNode("payment_type");
                                    decimal totPrice = 0m;
                                    tempNode = roomList.SelectSingleNode("price");
                                    if (tempNode != null)
                                    {
                                        totPrice = Convert.ToDecimal(tempNode.InnerText);
                                    }
                                    tempNode = roomList.SelectSingleNode("no_of_rooms");
                                    if (tempNode != null && tempNode.InnerText == request.NoOfRooms.ToString())
                                    {
                                        totPrice = totPrice / request.NoOfRooms;
                                    }
                                    decimal hotelTotalPrice = 0m;
                                    decimal vatAmount = 0m;
                                    hotelTotalPrice = Math.Round((totPrice) * rateOfExchange, decimalPoint);
                                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                    {
                                        hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                    }
                                    hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                                    roomDetail.TotalPrice = hotelTotalPrice;
                                    roomDetail.TotalPrice = hotelTotalPrice;
                                    roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                                    roomDetail.MarkupType = markupType;
                                    roomDetail.MarkupValue = markup;
                                    roomDetail.SellingFare = roomDetail.TotalPrice;
                                    roomDetail.supplierPrice = Math.Round((totPrice));
                                    roomDetail.TaxDetail = new PriceTaxDetails();
                                    roomDetail.TaxDetail = priceTaxDet;
                                    roomDetail.InputVATAmount = vatAmount;
                                    hotelResult.Price = new PriceAccounts();
                                    hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                    hotelResult.Price.SupplierPrice = Math.Round((totPrice));
                                    hotelResult.Price.RateOfExchange = rateOfExchange;
                                }
                                //day wise price calucation
                                System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                decimal totalprice = roomDetail.TotalPrice;

                                for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                {
                                    decimal price = roomDetail.TotalPrice / diffResult.Days;
                                    if (fareIndex == diffResult.Days - 1)
                                    {
                                        price = totalprice;
                                    }
                                    totalprice -= price;
                                    hRoomRates[fareIndex].Amount = price;
                                    hRoomRates[fareIndex].BaseFare = price;
                                    hRoomRates[fareIndex].SellingFare = price;
                                    hRoomRates[fareIndex].Totalfare = price;
                                    hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                    hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                                }
                                roomDetail.Rates = hRoomRates;


                                #region Cancellation Policy
                                decimal chargeAmt = 0m;
                                XmlNodeList cancellationPolicyList = roomList.SelectNodes("cancellation_policy");
                                string message = string.Empty;
                                if (cancellationPolicyList != null)
                                {
                                    foreach (XmlNode cancelNode in cancellationPolicyList)
                                    {
                                        XmlNodeList undercancellationList = cancelNode.SelectNodes("details");
                                        foreach (XmlNode undercancellation in undercancellationList)
                                        {
                                            if (undercancellation != null)
                                            {
                                                XmlNode chargeValue = undercancellation.SelectSingleNode("flat_fee");
                                                XmlNode chargeCurrency = undercancellation.SelectSingleNode("currency");
                                                XmlNode ratetype = cancelNode.SelectSingleNode("amount_type");
                                                int buffer = Convert.ToInt32(ConfigurationSystem.GrnConfig["buffer"]);
                                                XmlNode details = undercancellation.SelectSingleNode("from");
                                                if (details != null)
                                                {
                                                    DateTime beforeDate = Convert.ToDateTime(details.InnerText);
                                                    beforeDate = beforeDate.AddDays(-buffer);
                                                    if (ratetype != null && ratetype.InnerText == "percent")
                                                    {
                                                        XmlNode percentAmount = undercancellation.SelectSingleNode("percent");
                                                        if (percentAmount != null)
                                                        {
                                                            message = "From the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + " " + percentAmount.InnerText + "%";
                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (chargeValue != null && chargeCurrency != null)
                                                        {
                                                            chargeAmt = Convert.ToDecimal((Convert.ToDecimal(chargeValue.InnerText) * rateOfExchange).ToString("N" + decimalPoint));
                                                            tempNode = roomList.SelectSingleNode("no_of_rooms");
                                                            if (tempNode != null && tempNode.InnerText == request.NoOfRooms.ToString())
                                                            {
                                                                chargeAmt = Convert.ToDecimal((chargeAmt / request.NoOfRooms).ToString("N" + decimalPoint));
                                                            }
                                                            if (!string.IsNullOrEmpty(message))
                                                            {
                                                                message = message + "|" + "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                                            }
                                                            else
                                                            {
                                                                message = "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                                if (string.IsNullOrEmpty(message))
                                {
                                    tempNode = roomList.SelectSingleNode("cancellation_policy_code");
                                    string canellationPolicyDetails = string.Empty;
                                    if (tempNode != null)
                                    {
                                        CancellationPolicy cancelPolicy = new CancellationPolicy();
                                        cancelPolicy.cp_code = tempNode.InnerText;
                                        cancelPolicy.rate_key = roomDetail.RatePlanCode;
                                        var requestPolicy = JsonConvert.SerializeObject(cancelPolicy, new JsonSerializerSettings()
                                        {
                                            NullValueHandling = NullValueHandling.Ignore
                                        });
                                        XmlDocument xmlDocCancelRes = JsonConvert.DeserializeXmlNode(requestPolicy, "Root");
                                        try
                                        {
                                            string filePath = xmlPath + grnUserId + "_" + hotelResult.PropertyType + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancellationPolicyRequest.xml"; ;
                                            xmlDocCancelRes.Save(filePath);

                                            //JsonFormat
                                            string filePathJson = xmlPath + grnUserId + "_" + hotelResult.PropertyType + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancellationPolicyRequest.txt";
                                            StreamWriter sw = new StreamWriter(filePathJson);
                                            sw.Write(requestPolicy.ToString());
                                            sw.Close();
                                        }
                                        catch { }
                                        canellationPolicyDetails = GetResponse(requestPolicy, ConfigurationSystem.GrnConfig["Search"] + "/" + hotelResult.PropertyType + "/rates/cancellation_policies/");
                                        XmlDocument xmlPolicyResponse = JsonConvert.DeserializeXmlNode(canellationPolicyDetails, "Root");
                                        try
                                        {
                                            string filePath = xmlPath + grnUserId + "_" + hotelResult.PropertyType + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancellationPolicyResponse.xml"; ;
                                            xmlPolicyResponse.Save(filePath);
                                            //JsonFormat
                                            string filePathJson = xmlPath + grnUserId + "_" + hotelResult.PropertyType + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancellationPolicyResponse.txt";
                                            StreamWriter sw = new StreamWriter(filePathJson);
                                            sw.Write(canellationPolicyDetails.ToString());
                                            sw.Close();
                                        }
                                        catch { }
                                        if (xmlPolicyResponse != null)
                                        {
                                            // XmlNode cancellationNode = xmlPolicyResponse.SelectSingleNode("Root/under_cancellation");
                                            XmlNodeList undercancellations = xmlPolicyResponse.SelectNodes("Root/details");
                                            foreach (XmlNode undercancellation in undercancellations)
                                            {
                                                if (undercancellation != null)
                                                {
                                                    XmlNode details = undercancellation.SelectSingleNode("from");
                                                    int buffer = Convert.ToInt32(ConfigurationSystem.GrnConfig["buffer"]);
                                                    DateTime beforeDate = Convert.ToDateTime(details.InnerText);
                                                    beforeDate = beforeDate.AddDays(-buffer);
                                                    XmlNode chargeValue = undercancellation.SelectSingleNode("flat_fee");
                                                    XmlNode chargeCurrency = undercancellation.SelectSingleNode("currency");
                                                    XmlNode ratetype = xmlPolicyResponse.SelectSingleNode("Root/amount_type");
                                                    if (ratetype != null && ratetype.InnerText == "percent")
                                                    {
                                                        XmlNode percentAmount = undercancellation.SelectSingleNode("percent");
                                                        if (percentAmount != null)
                                                        {
                                                            message = "From the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + " " + percentAmount.InnerText + "%";
                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (chargeValue != null && chargeCurrency != null)
                                                        {
                                                            chargeAmt = Convert.ToDecimal((Convert.ToDecimal(chargeValue.InnerText) * rateOfExchange).ToString("N" + decimalPoint));
                                                            tempNode = roomList.SelectSingleNode("no_of_rooms");
                                                            if (tempNode != null && tempNode.InnerText == request.NoOfRooms.ToString())
                                                            {
                                                                chargeAmt = Convert.ToDecimal((chargeAmt / request.NoOfRooms).ToString("N" + decimalPoint));
                                                            }
                                                            if (!string.IsNullOrEmpty(message))
                                                            {
                                                                message = message + "|" + "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                                            }
                                                            else
                                                            {
                                                                message = "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }

                                    }

                                }
                                roomDetail.CancellationPolicy = message;
                                #endregion
                                //#region Cancellation Policy
                                //XmlNodeList cancellationPolicyList = roomList.SelectNodes("cancellation_policy");
                                ////int buffer = Convert.ToInt32(ConfigurationSystem.AgodaConfig["buffer"]);need to discuss with vinay
                                //string message = string.Empty;
                                //if (cancellationPolicyList != null)
                                //{
                                //    foreach (XmlNode cancelNode in cancellationPolicyList)
                                //    {
                                //        decimal chargeAmt = 0m;
                                //        XmlNode undercancellation = cancelNode.SelectSingleNode("details");
                                //        if (undercancellation != null)
                                //        {
                                //            XmlNode chargeValue = undercancellation.SelectSingleNode("flat_fee");
                                //            XmlNode chargeCurrency = undercancellation.SelectSingleNode("currency");
                                //            if (chargeValue != null && chargeCurrency != null)
                                //            {
                                //                chargeAmt = Convert.ToDecimal((Convert.ToDecimal(chargeValue.InnerText) * rateOfExchange).ToString("N" + decimalPoint));
                                //            }
                                //            XmlNode details = undercancellation.SelectSingleNode("from");
                                //            if (details != null)
                                //            {
                                //                int buffer = Convert.ToInt32(ConfigurationSystem.GrnConfig["buffer"]);
                                //                DateTime beforeDate = Convert.ToDateTime(details.InnerText);
                                //                beforeDate = beforeDate.AddDays(-buffer);
                                //                if (!string.IsNullOrEmpty(message))
                                //                {
                                //                    message = message + "|" + "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                //                }
                                //                else
                                //                {
                                //                    message = "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                //                }
                                //            }
                                //        }

                                //    }
                                //}
                                //roomDetail.CancellationPolicy = message;
                                //#endregion
                                #endregion
                                if (isBedTypes)
                                {
                                    foreach (XmlNode bedTypesnode in bedTypeCount)
                                    {
                                        List<string> sequenceList = roomDetail.RoomTypeCode.Split('|').ToList();
                                        if (sequenceList.Count > 7)
                                        {
                                            sequenceList.Remove(sequenceList[7]);
                                            if (sequenceList.Count == 8)
                                            {
                                                sequenceList.Remove(sequenceList[7]);
                                            }
                                        }
                                        roomDetail.RoomTypeCode = string.Join("|", sequenceList);
                                        tempNode = bedTypesnode.SelectSingleNode("type");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                        }
                                        tempNode = bedTypesnode.SelectSingleNode("id");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                        }
                                        rooms.Add(roomDetail);
                                    }
                                }
                                else
                                {
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + "|";
                                    rooms.Add(roomDetail);
                                }

                            }
                            hotelResult.RoomDetails = rooms.ToArray();
                        }

                        hotelResult.Currency = agentCurrency;
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                            {
                                if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                                {
                                    hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                    hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                    hotelResult.Price.AccPriceType = PriceType.NetFare;
                                    hotelResult.Price.RateOfExchange = rateOfExchange;
                                    hotelResult.Price.Currency = agentCurrency;
                                    hotelResult.Price.CurrencyCode = agentCurrency;
                                    break;
                                }
                            }

                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnRecheck, Severity.Normal, 1, "GRN.ReadRecheckRoomDetails Err :" + ex.Message, "0");
                throw ex;
            }


        }
        #endregion
        #region Refetch Hotel Details
        /// <summary>
        /// Complete Room Details For Selected Hotel
        /// </summary>
        public void GetRoomRefetchDetails(ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType, string searchid)
        {
            try
            {
                string resp = string.Empty;
                try
                {
                    Refetch refetchRequest = new Refetch();
                    refetchRequest.sid = searchid;
                    refetchRequest.hcode = hotelResult.HotelCode;
                    refetchRequest.hotel_info = true;
                    var roomRefetch = JsonConvert.SerializeObject(refetchRequest,
          new JsonSerializerSettings()
          {
              NullValueHandling = NullValueHandling.Ignore
          });

                    XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(roomRefetch, "Root");
                    {
                        string filePath = xmlPath + grnUserId + "_" + searchid + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_RefetchDataRequest.xml"; ;
                        xmlDocRes.Save(filePath);


                        //JsonFormat
                        string filePathJson = xmlPath + grnUserId + "_" + searchid + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_RefetchDataRequest.txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(roomRefetch.ToString());
                        sw.Close();
                    }
                }
                catch { }
                resp = ReadGetResponse(ConfigurationSystem.GrnConfig["Search"] + "/" + searchid + "?hcode=" + hotelResult.HotelCode + "&hotel_info =true");
                try
                {
                    ReadRefetchRoomDetails(resp, ref hotelResult, request, markup, markupType);

                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnRefetch, Severity.Normal, 1, "GRN.GetRoomRefetchDetails Err :" + ex.ToString(), "0");
                throw ex;
            }
        }
        /// <summary>
        ///Read the Complete Room Details For Selected Hotel
        /// </summary>
        private void ReadRefetchRoomDetails(string response, ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType)
        {
            try
            {
                HotelRoomsDetails[] rmRecheck = new HotelRoomsDetails[0];
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + hotelResult.PropertyType +  "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_RoomRefetchDataResponse.xml"; ;
                    xmlDocRes.Save(filePath);
                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + hotelResult.PropertyType + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_RoomRefetchDataResponse.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }
                catch { }
                XmlNode errorInfo = xmlDocRes.SelectSingleNode("Root/errors/messages");
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    hotelResult.HotelDescription = errorInfo.InnerText;
                    throw new BookingEngineException("Error: " + errorInfo.InnerText.Length);
                }
                else
                {
                    //Loading all Vat charges
                    PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                    string searchId = string.Empty;
                    XmlNode tempNode = xmlDocRes.SelectSingleNode("Root/search_id");
                    if (tempNode != null)
                    {
                        searchId = tempNode.InnerText;
                    }
                    else
                    {
                        return;
                    }
                    XmlNode hotels = xmlDocRes.SelectSingleNode("Root/hotel");
                    if (hotels != null)
                    {
                        hotelResult.PropertyType = searchId;
                        hotelResult.BookingSource = HotelBookingSource.GRN;
                        tempNode = hotels.SelectSingleNode("city_code");
                        if (tempNode != null)
                        {
                            hotelResult.CityCode = tempNode.InnerText;
                        }
                        hotelResult.StartDate = request.StartDate;
                        hotelResult.EndDate = request.EndDate;

                        tempNode = hotels.SelectSingleNode("hotel_code");
                        if (tempNode != null)
                        {
                            hotelResult.HotelCode = tempNode.InnerText;
                        }
                        string pattern = request.HotelName;

                        tempNode = hotels.SelectSingleNode("category");
                        if (tempNode != null)
                        {
                            hotelResult.Rating = (HotelRating)Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(tempNode.InnerText)));
                            //some times rating given 3.5 that time we need to show 4.0 only                        
                        }
                        DataRow[] hotelImages = new DataRow[0];
                        try
                        {
                            tempNode = hotels.SelectSingleNode("images/url");
                            if (tempNode != null)
                            {
                                hotelResult.HotelPicture = tempNode.InnerText;
                            }
                        }
                        catch { }
                        ///Rooms Binding
                        bool equalRooms = false;
                        bool isNonBundeled = false;
                        XmlNodeList roomList = hotels.SelectNodes("rates");
                        XmlNodeList roomNodeList = hotels.SelectNodes("rates/rooms");
                        List<XmlNode> bundledRoomList = roomList.Cast<XmlNode>().Where(c => c.SelectSingleNode("no_of_rooms").InnerText == request.NoOfRooms.ToString()).ToList();
                        List<XmlNode> nonBundledRoomList = roomList.Cast<XmlNode>().Where(c => c.SelectSingleNode("no_of_rooms").InnerText != request.NoOfRooms.ToString()).ToList();
                        if (bundledRoomList.Count == roomList.Count)
                        {
                            isNonBundeled = false;
                            hotelResult.SupplierType = "Bundeled";
                        }
                        else if (nonBundledRoomList.Count == roomList.Count)
                        {
                            isNonBundeled = true;
                            hotelResult.SupplierType = "NonBundeled";
                        }
                        else
                        {
                            hotelResult.SupplierType = "BothCombination";
                        }
                        XmlNodeList bedTypeList = hotels.SelectNodes("rates/rooms/bed_types");
                        List<HotelRoomsDetails> rooms = new List<HotelRoomsDetails>();
                        foreach (XmlNode refetchRooms in roomList)
                        {
                            int roomcount = request.NoOfRooms;
                            HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                            XmlNodeList nonBundeledCount = refetchRooms.SelectNodes("rooms");
                            if (nonBundeledCount.Count != request.NoOfRooms)
                            {
                                roomDetail.BedTypeCode = "NonBundeled";
                                isNonBundeled = true;
                            }
                            else
                            {
                                roomDetail.BedTypeCode = "Bundeled";
                            }
                            tempNode = refetchRooms.SelectSingleNode("currency");
                            rateOfExchange = (exchangeRates.ContainsKey(tempNode.InnerText) ? exchangeRates[tempNode.InnerText] : 1);
                            hotelResult.RoomGuest = request.RoomGuest;
                            int noOfDays = request.EndDate.Subtract(request.StartDate).Days;

                            if (roomList != null)
                            {
                                XmlNodeList refetchRateRooms = refetchRooms.SelectNodes("rooms");
                                List<HotelRoomsDetails> bundeledRooms = new List<HotelRoomsDetails>();
                                foreach (XmlNode roomNode in refetchRateRooms)
                                {
                                    string adultCount = string.Empty;

                                    string childCount = string.Empty;
                                    tempNode = roomNode.SelectSingleNode("no_of_adults");
                                    if (tempNode != null)
                                    {
                                        adultCount = tempNode.InnerText;
                                    }
                                    tempNode = roomNode.SelectSingleNode("no_of_children");
                                    if (tempNode != null)
                                    {
                                        childCount = tempNode.InnerText;
                                    }


                                    List<RoomGuestData> hotelRooms = new List<RoomGuestData>(request.RoomGuest);

                                    equalRooms = hotelRooms.All(h => h.noOfAdults == Convert.ToInt32(adultCount) && h.noOfChild == Convert.ToInt32(childCount));
                                    if (isNonBundeled)
                                    {
                                        for (int k = 0; k < request.NoOfRooms; k++)
                                        {
                                            if (request.RoomGuest[k].noOfAdults == Convert.ToInt32(adultCount) && request.RoomGuest[k].noOfChild == Convert.ToInt32(childCount))
                                            {
                                                roomDetail.SequenceNo = (k + 1).ToString();
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < request.NoOfRooms; k++)
                                        {
                                            if (request.RoomGuest[k].noOfAdults == Convert.ToInt32(adultCount) && request.RoomGuest[k].noOfChild == Convert.ToInt32(childCount))
                                            {
                                                bool isMatched = false;
                                                XmlNodeList roomChildAges = roomNode.SelectNodes("children_ages");
                                                List<string> childAgesList = new List<string>();
                                                if (roomChildAges != null && roomChildAges.Count > 0)
                                                {
                                                    foreach (XmlNode ages in roomChildAges)
                                                    {
                                                        if (ages != null)
                                                        {
                                                            childAgesList.Add(ages.InnerText);
                                                        }
                                                    }
                                                    childAgesList = childAgesList.OrderBy(a => a.ToString()).ToList();
                                                    if (childAgesList != null && childAgesList.Count > 0)
                                                    {
                                                        for (int l = 0; l < childAgesList.Count; l++)
                                                        {
                                                            request.RoomGuest[k].childAge = request.RoomGuest[k].childAge.OrderBy(a => a).ToList();
                                                            if (request.RoomGuest[k].childAge[l].ToString() == childAgesList[l])
                                                            {
                                                                isMatched = true;
                                                                continue;
                                                            }
                                                            else
                                                            {
                                                                isMatched = false;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    isMatched = true;
                                                }
                                                if (isMatched)
                                                {
                                                    roomDetail.SequenceNo = (k + 1).ToString();
                                                    if(bundeledRooms!=null && bundeledRooms.Count>0)
                                                    {
                                                        if(bundeledRooms.Where(w => w.SequenceNo == roomDetail.SequenceNo).Count().ToString() == "0")
                                                        {
                                                            bundeledRooms.Add(roomDetail);
                                                            break;
                                                        }
                                                    }
                                                    else if( bundeledRooms.Count == 0 && isMatched )
                                                    {
                                                        bundeledRooms.Add(roomDetail);
                                                        break;
                                                    }

                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    tempNode = roomNode.SelectSingleNode("description");
                                    if (tempNode != null)
                                    {
                                        roomDetail.RoomTypeName = tempNode.InnerText;
                                    }
                                    else
                                    {
                                        tempNode = roomNode.SelectSingleNode("room_type");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeName = tempNode.InnerText;
                                        }
                                    }

                                    tempNode = refetchRooms.SelectSingleNode("room_code");
                                    if (tempNode != null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.SequenceNo + "|" + hotelResult.PropertyType + "|" + tempNode.InnerText;
                                    }
                                    tempNode = refetchRooms.SelectSingleNode("rate_type");
                                    if (tempNode != null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                    }
                                    tempNode = refetchRooms.SelectSingleNode("rate_key");
                                    if (tempNode != null)
                                    {
                                        roomDetail.RatePlanCode = tempNode.InnerText;
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                    }
                                    tempNode = refetchRooms.SelectSingleNode("group_code");
                                    if (tempNode != null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                    }
                                    tempNode = roomNode.SelectSingleNode("room_reference");
                                    if (tempNode != null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                    }
                                    else
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|";
                                    }
                                    //RoomType Code--- Seq|SearchId|RoomCode|Ratetype|RateKey|groupcode|reference id|BedType|BedId                                           
                                    tempNode = refetchRooms.SelectSingleNode("has_promotions");
                                    if (tempNode != null && tempNode.InnerText == "true")
                                    {
                                        tempNode = refetchRooms.SelectSingleNode("promotions_details");
                                        if (tempNode != null)
                                        {
                                            roomDetail.PromoMessage = tempNode.InnerText;
                                        }
                                    }
                                    tempNode = refetchRooms.SelectSingleNode("includes_boarding");
                                    if (tempNode != null && tempNode.InnerText == "true")
                                    {
                                        tempNode = refetchRooms.SelectSingleNode("boarding_details");
                                        if (tempNode != null)
                                        {
                                            roomDetail.mealPlanDesc = tempNode.InnerText;
                                        }
                                    }
                                    if (string.IsNullOrEmpty(roomDetail.mealPlanDesc))
                                    {
                                        roomDetail.mealPlanDesc = "Room Only";
                                    }

                                    #region Price Calculation 
                                    decimal hotelTotalPrice = 0m;
                                    roomDetail.EssentialInformation = string.Empty;
                                    XmlNodeList roomprices = refetchRooms.SelectNodes("price_details/hotel_charges");
                                    if (roomprices != null)
                                    {
                                        foreach (XmlNode surprices in roomprices)
                                        {
                                            tempNode = surprices.SelectSingleNode("included");
                                            if (tempNode != null && tempNode.InnerText == "false")
                                            {
                                                decimal mandaterycharge = 0m;
                                                tempNode = surprices.SelectSingleNode("amount");
                                                if (tempNode != null)
                                                {
                                                    mandaterycharge = Convert.ToDecimal(tempNode.InnerText);
                                                    mandaterycharge = mandaterycharge * rateOfExchange;
                                                }
                                                tempNode = surprices.SelectSingleNode("name");
                                                if (tempNode != null)
                                                {
                                                    if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                                    {
                                                        roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + tempNode.InnerText + " " + mandaterycharge.ToString()+ "<b>Not Included in price(collected by the properity)</b>:";
                                                    }
                                                    else
                                                    {
                                                        roomDetail.EssentialInformation = tempNode.InnerText + " " + mandaterycharge.ToString()+ " <b>Not Included in price(collected by the properity)</b>:";
                                                    }
                                                }
                                            }
                                        }
                                        XmlNode rateComments = refetchRooms.SelectSingleNode("rate_comments");
                                        if (rateComments != null)
                                        {
                                            tempNode = rateComments.SelectSingleNode("spl_checkin_instructions");
                                            if (tempNode != null)
                                            {
                                                if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                                {
                                                    roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + tempNode.InnerText;
                                                }
                                                else
                                                {
                                                    roomDetail.EssentialInformation = tempNode.InnerText;
                                                }
                                            }
                                            tempNode = rateComments.SelectSingleNode("checkin_instructions");
                                            if (tempNode != null)
                                            {
                                                if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                                {
                                                    roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + tempNode.InnerText;
                                                }
                                                else
                                                {
                                                    roomDetail.EssentialInformation = tempNode.InnerText;
                                                }
                                            }
                                            tempNode = rateComments.SelectSingleNode("pax_comments");
                                            if (tempNode != null)
                                            {
                                                if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                                {
                                                    roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + tempNode.InnerText;
                                                }
                                                else
                                                {
                                                    roomDetail.EssentialInformation = tempNode.InnerText;
                                                }
                                            }
                                        }


                                        XmlNodeList otherInclusions = refetchRooms.SelectNodes("other_inclusions");
                                        foreach (XmlNode inclusions in otherInclusions)
                                        {

                                            if (inclusions != null)
                                            {
                                                if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                                {
                                                    roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + inclusions.InnerText;
                                                }
                                                else
                                                {
                                                    roomDetail.EssentialInformation = inclusions.InnerText;
                                                }
                                            }
                                        }
                                        roomDetail.Amenities = new List<string>();
                                        tempNode = hotels.SelectSingleNode("facilities");
                                        if (tempNode != null)
                                        {
                                            roomDetail.Amenities.Add(tempNode.InnerText);
                                        }

                                        decimal totPrice = 0m;
                                        tempNode = refetchRooms.SelectSingleNode("price");
                                        if (tempNode != null)
                                        {
                                            totPrice = Convert.ToDecimal(tempNode.InnerText);
                                        }
                                        tempNode = refetchRooms.SelectSingleNode("no_of_rooms");
                                        if (tempNode != null && tempNode.InnerText == request.NoOfRooms.ToString())
                                        {
                                            totPrice = totPrice / request.NoOfRooms;
                                        }
                                        decimal vatAmount = 0m;
                                        hotelTotalPrice = Math.Round((totPrice) * rateOfExchange, decimalPoint);
                                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                        {
                                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                        }
                                        hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                                        roomDetail.TotalPrice = hotelTotalPrice;
                                        roomDetail.TotalPrice = hotelTotalPrice;
                                        roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                                        roomDetail.MarkupType = markupType;
                                        roomDetail.MarkupValue = markup;
                                        roomDetail.SellingFare = roomDetail.TotalPrice;
                                        roomDetail.supplierPrice = Math.Round((totPrice));
                                        roomDetail.TaxDetail = new PriceTaxDetails();
                                        roomDetail.TaxDetail = priceTaxDet;
                                        roomDetail.InputVATAmount = vatAmount;
                                        hotelResult.Price = new PriceAccounts();
                                        hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                        hotelResult.Price.SupplierPrice = Math.Round((totPrice));
                                        hotelResult.Price.RateOfExchange = rateOfExchange;
                                    }
                                    //day wise price calucation
                                    System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                    RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                    decimal totalprice = roomDetail.TotalPrice;

                                    for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                    {
                                        decimal price = roomDetail.TotalPrice / diffResult.Days;
                                        if (fareIndex == diffResult.Days - 1)
                                        {
                                            price = totalprice;
                                        }
                                        totalprice -= price;
                                        hRoomRates[fareIndex].Amount = price;
                                        hRoomRates[fareIndex].BaseFare = price;
                                        hRoomRates[fareIndex].SellingFare = price;
                                        hRoomRates[fareIndex].Totalfare = price;
                                        hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                        hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                                    }
                                    roomDetail.Rates = hRoomRates;

                                    #region Cancellation Policy
                                    decimal chargeAmt = 0m;
                                    XmlNodeList cancellationPolicyList = refetchRooms.SelectNodes("cancellation_policy");
                                    string message = string.Empty;
                                    if (cancellationPolicyList != null)
                                    {
                                        foreach (XmlNode cancelNode in cancellationPolicyList)
                                        {
                                            XmlNodeList undercancellationList = cancelNode.SelectNodes("details");
                                            foreach (XmlNode undercancellation in undercancellationList)
                                            {
                                                if (undercancellation != null)
                                                {
                                                    XmlNode chargeValue = undercancellation.SelectSingleNode("flat_fee");
                                                    XmlNode chargeCurrency = undercancellation.SelectSingleNode("currency");
                                                    XmlNode ratetype = cancelNode.SelectSingleNode("amount_type");
                                                    int buffer = Convert.ToInt32(ConfigurationSystem.GrnConfig["buffer"]);
                                                    XmlNode details = undercancellation.SelectSingleNode("from");
                                                    if (details != null)
                                                    {
                                                        DateTime beforeDate = Convert.ToDateTime(details.InnerText);
                                                        beforeDate = beforeDate.AddDays(-buffer);
                                                        if (ratetype != null && ratetype.InnerText == "percent")
                                                        {
                                                            XmlNode percentAmount = undercancellation.SelectSingleNode("percent");
                                                            if (percentAmount != null)
                                                            {
                                                                message = "From the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + " " + percentAmount.InnerText + "%";
                                                            }

                                                        }
                                                        else
                                                        {
                                                            if (chargeValue != null && chargeCurrency != null)
                                                            {
                                                                chargeAmt = Convert.ToDecimal((Convert.ToDecimal(chargeValue.InnerText) * rateOfExchange).ToString("N" + decimalPoint));
                                                                tempNode = refetchRooms.SelectSingleNode("no_of_rooms");
                                                                if (tempNode != null && tempNode.InnerText == request.NoOfRooms.ToString())
                                                                {
                                                                    chargeAmt = Convert.ToDecimal((chargeAmt / request.NoOfRooms).ToString("N" + decimalPoint));
                                                                }
                                                                if (!string.IsNullOrEmpty(message))
                                                                {
                                                                    message = message + "|" + "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                                                }
                                                                else
                                                                {
                                                                    message = "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (string.IsNullOrEmpty(message))
                                    {
                                        tempNode = refetchRooms.SelectSingleNode("cancellation_policy_code");
                                        string canellationPolicyDetails = string.Empty;
                                        if (tempNode != null)
                                        {
                                            CancellationPolicy cancelPolicy = new CancellationPolicy();
                                            cancelPolicy.cp_code = tempNode.InnerText;
                                            cancelPolicy.rate_key = roomDetail.RatePlanCode;
                                            var requestPolicy = JsonConvert.SerializeObject(cancelPolicy, new JsonSerializerSettings()
                                            {
                                                NullValueHandling = NullValueHandling.Ignore
                                            });
                                            XmlDocument xmlDocCancelRes = JsonConvert.DeserializeXmlNode(requestPolicy, "Root");
                                            try
                                            {
                                                string filePath = xmlPath + grnUserId + "_" + hotelResult.PropertyType + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancellationPolicyRequest.xml"; ;
                                                xmlDocCancelRes.Save(filePath);

                                                //JsonFormat
                                                string filePathJson = xmlPath + grnUserId + "_" + hotelResult.PropertyType + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancellationPolicyRequest.txt";
                                                StreamWriter sw = new StreamWriter(filePathJson);
                                                sw.Write(requestPolicy.ToString());
                                                sw.Close();
                                            }
                                            catch { }
                                            canellationPolicyDetails = GetResponse(requestPolicy, ConfigurationSystem.GrnConfig["Search"] + "/" + hotelResult.PropertyType + "/rates/cancellation_policies/");
                                            XmlDocument xmlPolicyResponse = JsonConvert.DeserializeXmlNode(canellationPolicyDetails, "Root");
                                            try
                                            {
                                                string filePath = xmlPath + grnUserId + "_" + hotelResult.PropertyType + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancellationPolicyResponse.xml"; ;
                                                xmlPolicyResponse.Save(filePath);
                                                //JsonFormat
                                                string filePathJson = xmlPath + grnUserId + "_" + hotelResult.PropertyType + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancellationPolicyResponse.txt";
                                                StreamWriter sw = new StreamWriter(filePathJson);
                                                sw.Write(canellationPolicyDetails.ToString());
                                                sw.Close();
                                            }
                                            catch { }
                                            if (xmlPolicyResponse != null)
                                            {
                                                // XmlNode cancellationNode = xmlPolicyResponse.SelectSingleNode("Root/under_cancellation");
                                                XmlNodeList undercancellations = xmlPolicyResponse.SelectNodes("Root/details");
                                                foreach (XmlNode undercancellation in undercancellations)
                                                {
                                                    if (undercancellation != null)
                                                    {
                                                        XmlNode details = undercancellation.SelectSingleNode("from");
                                                        int buffer = Convert.ToInt32(ConfigurationSystem.GrnConfig["buffer"]);
                                                        DateTime beforeDate = Convert.ToDateTime(details.InnerText);
                                                        beforeDate = beforeDate.AddDays(-buffer);
                                                        XmlNode chargeValue = undercancellation.SelectSingleNode("flat_fee");
                                                        XmlNode chargeCurrency = undercancellation.SelectSingleNode("currency");
                                                        XmlNode ratetype = xmlPolicyResponse.SelectSingleNode("Root/amount_type");
                                                        if (ratetype != null && ratetype.InnerText == "percent")
                                                        {
                                                            XmlNode percentAmount = undercancellation.SelectSingleNode("percent");
                                                            if (percentAmount != null)
                                                            {
                                                                message = "From the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + " " + percentAmount.InnerText + "%";
                                                            }

                                                        }
                                                        else
                                                        {
                                                            if (chargeValue != null && chargeCurrency != null)
                                                            {
                                                                chargeAmt = Convert.ToDecimal((Convert.ToDecimal(chargeValue.InnerText) * rateOfExchange).ToString("N" + decimalPoint));
                                                                tempNode = refetchRooms.SelectSingleNode("no_of_rooms");
                                                                if (tempNode != null && tempNode.InnerText == request.NoOfRooms.ToString())
                                                                {
                                                                    chargeAmt = Convert.ToDecimal((chargeAmt / request.NoOfRooms).ToString("N" + decimalPoint));
                                                                }
                                                                if (!string.IsNullOrEmpty(message))
                                                                {
                                                                    message = message + "|" + "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                                                }
                                                                else
                                                                {
                                                                    message = "Cancellation is applied till the date:" + beforeDate.ToString("dd/MMM/yyyy") + ",you will be charged " + agentCurrency + " " + chargeAmt;
                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                        }

                                    }
                                    roomDetail.CancellationPolicy = message;
                                    #endregion
                                    #endregion                                  
                                    bool isBedTypes = false;

                                    XmlNodeList bedTypeLists = roomNode.SelectNodes("bed_types");
                                    if (bedTypeLists != null && bedTypeLists.Count > 0)
                                    {
                                        isBedTypes = true;
                                    }
                                    if (isBedTypes)
                                    {
                                        foreach (XmlNode bedTypesnode in bedTypeLists)
                                        {

                                            List<string> sequenceList = roomDetail.RoomTypeCode.Split('|').ToList();
                                            if (sequenceList.Count > 7)
                                            {
                                                sequenceList.Remove(sequenceList[7]);
                                                if (sequenceList.Count==8)
                                                {
                                                    sequenceList.Remove(sequenceList[7]);
                                                }
                                            }
                                            roomDetail.RoomTypeName = roomDetail.RoomTypeName.Split(new string[] { " with- " }, StringSplitOptions.None)[0];//Split the RoomTypeName with BedType Name 
                                            roomDetail.RoomTypeCode = string.Join("|", sequenceList);
                                            tempNode = bedTypesnode.SelectSingleNode("type");
                                            if (tempNode != null)
                                            {
                                                roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                                roomDetail.RoomTypeName = roomDetail.RoomTypeName + " with- " + tempNode.InnerText;//combine RoomTypeName with BedType Name
                                            }
                                            tempNode = bedTypesnode.SelectSingleNode("id");
                                            if (tempNode != null)
                                            {
                                                roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                                            }


                                            if (isNonBundeled)
                                            {
                                                for (int k = 0; k < request.NoOfRooms; k++)
                                                {
                                                    if (request.RoomGuest[k].noOfAdults == Convert.ToInt32(adultCount) && request.RoomGuest[k].noOfChild == Convert.ToInt32(childCount))
                                                    {
                                                        roomDetail.SequenceNo = (k + 1).ToString();
                                                        bool isMatched = false;
                                                        XmlNodeList roomChildAges = roomNode.SelectNodes("children_ages");
                                                        List<string> childAgesList = new List<string>();
                                                        if (roomChildAges != null && roomChildAges.Count > 0)
                                                        {
                                                            foreach (XmlNode ages in roomChildAges)
                                                            {
                                                                if (ages != null)
                                                                {
                                                                    childAgesList.Add(ages.InnerText);
                                                                }

                                                            }
                                                            childAgesList = childAgesList.OrderBy(a => a.ToString()).ToList();
                                                            if (childAgesList != null && childAgesList.Count > 0)
                                                            {
                                                                for (int l = 0; l < childAgesList.Count; l++)
                                                                {

                                                                    request.RoomGuest[k].childAge = request.RoomGuest[k].childAge.OrderBy(a => a).ToList();
                                                                    if (request.RoomGuest[k].childAge[l].ToString() == childAgesList[l])
                                                                    {
                                                                        isMatched = true;
                                                                        continue;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            isMatched = true;
                                                        }
                                                        if (isMatched)
                                                        {
                                                            sequenceList = roomDetail.RoomTypeCode.Split('|').ToList();
                                                            sequenceList[0] = (k + 1).ToString();
                                                            roomDetail.RoomTypeCode = string.Join("|", sequenceList);
                                                            rooms.Add(roomDetail);

                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                rooms.Add(roomDetail);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (isNonBundeled)
                                        {
                                            for (int k = 0; k < request.NoOfRooms; k++)
                                            {
                                                if (request.RoomGuest[k].noOfAdults == Convert.ToInt32(adultCount) && request.RoomGuest[k].noOfChild == Convert.ToInt32(childCount))
                                                {
                                                    roomDetail.SequenceNo = (k + 1).ToString();
                                                    bool isMatched = false;
                                                    XmlNodeList roomChildAges = roomNode.SelectNodes("children_ages");
                                                    List<string> childAgesList = new List<string>();
                                                    if (roomChildAges != null && roomChildAges.Count > 0)
                                                    {
                                                        foreach (XmlNode ages in roomChildAges)
                                                        {
                                                            if (ages != null)
                                                            {
                                                                childAgesList.Add(ages.InnerText);
                                                            }

                                                        }

                                                        childAgesList = childAgesList.OrderBy(a => a.ToString()).ToList();
                                                        if (childAgesList != null && childAgesList.Count > 0)
                                                        {
                                                            for (int l = 0; l < childAgesList.Count; l++)
                                                            {

                                                                request.RoomGuest[k].childAge = request.RoomGuest[k].childAge.OrderBy(a => a).ToList();
                                                                if (request.RoomGuest[k].childAge[l].ToString() == childAgesList[l])
                                                                {
                                                                    isMatched = true;
                                                                    continue;
                                                                }
                                                                else
                                                                {
                                                                    isMatched = false;
                                                                }

                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        isMatched = true;
                                                    }
                                                    if (isMatched)
                                                    {
                                                        List<string> sequenceList = roomDetail.RoomTypeCode.Split('|').ToList();
                                                        roomDetail.SequenceNo = (k + 1).ToString();
                                                        sequenceList = roomDetail.RoomTypeCode.Split('|').ToList();
                                                        sequenceList[0] = (k + 1).ToString();
                                                        roomDetail.RoomTypeCode = string.Join("|", sequenceList);
                                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + "|";
                                                        rooms.Add(roomDetail);

                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }
                                            }

                                        }
                                        else
                                        {
                                            rooms.Add(roomDetail);
                                        }
                                    }
                                }
                            }
                        }
                        rooms= rooms.OrderBy(o => o.supplierPrice).ToList();
                        hotelResult.RoomDetails = rooms.ToArray();
                        hotelResult.Currency = agentCurrency;
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                            {
                                if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                                {
                                    hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                    hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                    hotelResult.Price.AccPriceType = PriceType.NetFare;
                                    hotelResult.Price.RateOfExchange = rateOfExchange;
                                    hotelResult.Price.Currency = agentCurrency;
                                    hotelResult.Price.CurrencyCode = agentCurrency;
                                    break;
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnRefetch, Severity.Normal, 1, "GRN.ReadRefetchRoomDetails Err :" + ex.ToString(), "0");
                throw ex;
            }

        }
        #endregion
        #region Booking 
        /// <summary>
        /// Booking Method
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>       
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            BookingResponse bookingResult = new BookingResponse();
            try
            {
                string request = GenerateBookingRequest(itinerary);
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + itinerary.PropertyType + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_BookRequest.xml";
                    XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(request, "Root");
                    xmlDocRes.Save(filePath);
                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + itinerary.PropertyType + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_BookRequest.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(request.ToString());
                    sw.Close();
                }
                catch { }

                XmlDocument xmlResp = new XmlDocument();
                string bookingResponse = string.Empty;
                try
                {
                    bookingResponse = GetResponse(request, ConfigurationSystem.GrnConfig["Booking"]);
                }
                catch (Exception ex)
                {

                    throw new BookingEngineException("Error: " + ex.ToString());
                }
                try
                {
                    bookingResult = readBookingResponse(bookingResponse, itinerary.PropertyType);
                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.ToString());
                }
                string viewBookingResponse = string.Empty;
                if (!string.IsNullOrEmpty(bookingResult.ConfirmationNo))
                {
                    try
                    {
                        string confirmationNumber = bookingResult.ConfirmationNo;
                        string filePath = xmlPath + grnUserId + "_" + itinerary.PropertyType + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ViewBookRequest.xml";

                        XmlDocument xmlDocRes = new XmlDocument();
                        xmlDocRes.LoadXml(string.Format("<Root>{0}</Root>", confirmationNumber));
                        xmlDocRes.Save(filePath);
                    }
                    catch { }
                    bookingResponse = ReadGetResponse(ConfigurationSystem.GrnConfig["Booking"] + "/" + bookingResult.ConfirmationNo);
                }
                try
                {
                    bookingResult = BookingResponse(bookingResponse, itinerary);
                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnBooking, Severity.Normal, 1, "GRN.GetBooking Err :" + ex.ToString(), "0");
                throw ex;
            }
            return bookingResult;
        }
        /// <summary>
        ///Read the Booking response For Booked Hotel
        /// </summary>
        private BookingResponse readBookingResponse(string response,string searchid)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                XmlDocument xmlDocRes = new XmlDocument();
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + searchid + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_BookResponse.xml";
                    xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                    xmlDocRes.Save(filePath);
                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + searchid + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_BookResponse.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }
                catch { }
                XmlNode tempnode = xmlDocRes.SelectSingleNode("Root/errors/messages");
                if (tempnode != null)
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                }
                else
                {
                    tempnode = xmlDocRes.SelectSingleNode("Root/booking_reference");
                    if (tempnode != null)
                    {
                        bookResponse.ConfirmationNo = tempnode.InnerText;
                    }
                    tempnode = xmlDocRes.SelectSingleNode("Root/booking_id");
                    //if (tempnode != null)
                    //{
                    //    bookResponse.BookingId = Convert.ToInt32(tempnode.InnerText);
                    //}
                    string status = string.Empty;
                    tempnode = xmlDocRes.SelectSingleNode("Root/status");
                    if (tempnode != null)
                    {
                        status = (tempnode.InnerText);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnBooking, Severity.Normal, 1, "GRN.readBookingResponse Err :" + ex.ToString(), "0");
                throw ex;
            }

            return bookResponse;
        }
        /// <summary>
        ///Read the View Booking response For Booked Hotel
        /// </summary>
        private BookingResponse BookingResponse(string response, HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                string searchid = string.Empty;
                XmlDocument xmlDocRes = new XmlDocument();
                xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                XmlNode tempnode = xmlDocRes.SelectSingleNode("Root/search_id");
                if (tempnode != null)
                {
                    searchid = tempnode.InnerText;
                }
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + itinerary.PropertyType + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ViewBookResponse.xml";
                    xmlDocRes.Save(filePath);
                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + itinerary.PropertyType + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ViewBookResponse.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }
                catch { }
                tempnode = xmlDocRes.SelectSingleNode("Root/errors/messages");
                if (tempnode != null)
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                    bookResponse = new BookingResponse(BookingResponseStatus.Failed, tempnode.InnerText, string.Empty, ProductType.Hotel, "");
                }
                else
                {
                    tempnode = xmlDocRes.SelectSingleNode("Root/booking_reference");
                    if (tempnode != null)
                    {
                        itinerary.BookingRefNo = tempnode.InnerText;

                    }
                    tempnode = xmlDocRes.SelectSingleNode("Root/booking_id");
                    if (tempnode != null)
                    {
                        itinerary.ConfirmationNo = tempnode.InnerText;
                    }
                    string status = string.Empty;
                    tempnode = xmlDocRes.SelectSingleNode("Root/booking_status");
                    if (tempnode != null)
                    {
                        status = (tempnode.InnerText);
                    }
                    bookResponse.ConfirmationNo = itinerary.ConfirmationNo;
                    if (status == "confirmed")
                    {
                        bookResponse.Status = BookingResponseStatus.Successful;
                        itinerary.Status = HotelBookingStatus.Confirmed;
                        itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: GRN as per final booking form Confirmation No: " + itinerary.ConfirmationNo;
                    }
                    else
                    {
                        bookResponse.Status = BookingResponseStatus.Successful;
                        itinerary.Status = HotelBookingStatus.Confirmed;
                        itinerary.VatDescription = "Pending";
                    }
                    tempnode = xmlDocRes.SelectSingleNode("Root/additional_info");
                    if(tempnode!=null && tempnode.HasChildNodes)
                    {
                        foreach(XmlNode childs in tempnode.ChildNodes)
                        {
                            if (childs != null)
                            {
                                if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
                                {
                                    itinerary.HotelPolicyDetails = itinerary.HotelPolicyDetails + "|" + childs.InnerText;
                                }
                                else
                                {
                                    itinerary.HotelPolicyDetails = childs.InnerText;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnBooking, Severity.Normal, 1, "GRN.BookingResponse Err :" + ex.ToString(), "0");
                throw new Exception("GRN: Failed to Read booking response ", ex);
            }
            return bookResponse;
        }
        /// <summary>
        ///BookingRequest Object 
        /// </summary>
        private string GenerateBookingRequest(HotelItinerary itinerary)
        {
            var request = string.Empty;
            try
            {
                BookRequest bookingRequest = new BookRequest();
                bookingRequest.search_id = itinerary.PropertyType;
                bookingRequest.hotel_code = itinerary.HotelCode;
                bookingRequest.city_code = itinerary.CityCode;
                Dictionary<HotelRoom, int> filteredroomsList = new Dictionary<HotelRoom, int>();
                List<HotelRoom> roomsList = new List<HotelRoom>();
                roomsList.AddRange(itinerary.Roomtype);
                if (itinerary.Roomtype[0].BedTypeCode == "NonBundeled")
                {
                    if (itinerary.Roomtype[0] != null)
                    {
                        string[] roomTypeCode = itinerary.Roomtype[0].RoomTypeCode.Split('|');
                        //RoomType Code--- Seq|SearchId|RoomCode|Ratetype|RateKey|groupcode|roomreference|BedType|BedId

                        string roomCode = string.Empty;
                        string rateType = string.Empty;
                        string rateKey = string.Empty;
                        string groupcode = string.Empty;
                        string bedType = string.Empty;
                        string bedId = string.Empty;

                        groupcode = roomTypeCode[5];

                        if (!string.IsNullOrEmpty(groupcode))
                        {
                            bookingRequest.group_code = groupcode;
                        }

                        IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                        string[] startdate = itinerary.StartDate.ToString().Split(' ');
                        string[] enddate = itinerary.EndDate.ToString().Split(' ');
                        bookingRequest.checkin = itinerary.StartDate.ToString("yyyy-MM-dd");
                        bookingRequest.checkout = itinerary.EndDate.ToString("yyyy-MM-dd"); ;
                        bookingRequest.payment_type = "AT_WEB";
                        BookingItems[] bookingitems;

                        bookingitems = new BookingItems[itinerary.NoOfRooms];
                        BookingRooms[] bookingRooms = new BookingRooms[itinerary.NoOfRooms];
                        for (int i = 0; i < bookingRooms.Length; i++)
                        {
                            bookingitems[i].rate_key = itinerary.Roomtype[i].RoomTypeCode.Split('|')[4];
                            bookingitems[i].room_code = itinerary.Roomtype[i].RoomTypeCode.Split('|')[2];
                            bookingitems[i].rooms = new BookingRooms[1];
                            bookingitems[i].rooms[0] = new BookingRooms();
                            if (!string.IsNullOrEmpty(itinerary.Roomtype[i].RoomTypeCode.Split('|')[6]))
                            {
                                bookingitems[i].rooms[0].room_reference = itinerary.Roomtype[i].RoomTypeCode.Split('|')[6];
                            }
                            Pax[] pax = new Pax[itinerary.Roomtype[i].PassenegerInfo.Count];
                            for (int j = 0; j < pax.Length; j++)
                            {
                                pax[j].title = itinerary.Roomtype[i].PassenegerInfo[j].Title;
                                pax[j].name = itinerary.Roomtype[i].PassenegerInfo[j].Firstname;
                                pax[j].surname = itinerary.Roomtype[i].PassenegerInfo[j].Lastname;
                                if (itinerary.Roomtype[i].PassenegerInfo[j].PaxType == HotelPaxType.Adult)
                                {
                                    pax[j].type = "AD";
                                }
                                else
                                {
                                    pax[j].type = "CH";
                                    pax[j].age = itinerary.Roomtype[i].PassenegerInfo[j].Age;
                                }
                            }


                            bookingitems[i].rooms[0].paxes = pax;
                            //bookingRequest.booking_items[i].rooms = bookingRooms;
                        }
                        bookingRequest.booking_items = bookingitems;
                        Holder leadPax = new Holder();
                        leadPax.title = itinerary.Roomtype[0].PassenegerInfo[0].Title;
                        leadPax.name = itinerary.Roomtype[0].PassenegerInfo[0].Firstname;
                        leadPax.surname = itinerary.Roomtype[0].PassenegerInfo[0].Lastname;
                        leadPax.phone_number = itinerary.Roomtype[0].PassenegerInfo[0].Phoneno;
                        leadPax.email = itinerary.Roomtype[0].PassenegerInfo[0].Email;
                        leadPax.client_nationality = itinerary.Roomtype[0].PassenegerInfo[0].NationalityCode;
                        bookingRequest.holder = leadPax;
                    }
                }
                else
                {
                    foreach (HotelRoom container in itinerary.Roomtype)
                    {
                        List<HotelRoom> duplicateRoom = roomsList.FindAll(delegate (HotelRoom checkRoom)
                        {
                            return (checkRoom.RatePlanCode == container.RatePlanCode);
                        });
                        if (duplicateRoom != null && duplicateRoom.Count > 0 && !filteredroomsList.ContainsKey(duplicateRoom[0]))
                        {
                            filteredroomsList.Add(duplicateRoom[0], duplicateRoom.Count);
                            string[] roomTypeCode = container.RoomTypeCode.Split('|');
                            //RoomType Code--- Seq|SearchId|RoomCode|Ratetype|RateKey|groupcode|Room Reference|BedType|BedId

                            string roomCode = string.Empty;

                            string rateKey = string.Empty;
                            string groupcode = string.Empty;
                            string bedId = string.Empty;
                            if (roomTypeCode.Length > 7)
                            {
                                roomCode = roomTypeCode[2];
                                rateKey = roomTypeCode[4];
                                groupcode = roomTypeCode[5];
                                if (!string.IsNullOrEmpty(roomTypeCode[8]) )
                                {
                                    bedId = roomTypeCode[8];
                                }


                            }
                            bookingRequest.group_code = groupcode;
                            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                            string[] startdate = itinerary.StartDate.ToString().Split(' ');
                            string[] enddate = itinerary.EndDate.ToString().Split(' ');
                            bookingRequest.checkin = itinerary.StartDate.ToString("yyyy-MM-dd");
                            bookingRequest.checkout = itinerary.EndDate.ToString("yyyy-MM-dd");
                            bookingRequest.payment_type = "AT_WEB";
                            BookingItems[] bookingitems;
                            bookingitems = new BookingItems[1];
                            bookingitems[0].room_code = roomCode;
                            bookingitems[0].rate_key = rateKey;
                            BookingRooms[] bookingRooms = new BookingRooms[itinerary.NoOfRooms];
                            for (int i = 0; i < bookingRooms.Length; i++)
                            {
                                Pax[] pax = new Pax[itinerary.Roomtype[i].PassenegerInfo.Count];
                                if (!string.IsNullOrEmpty(itinerary.Roomtype[i].RoomTypeCode.Split('|')[6]))
                                {
                                    bookingRooms[i].room_reference = itinerary.Roomtype[i].RoomTypeCode.Split('|')[6];
                                }
                                for (int j = 0; j < pax.Length; j++)
                                {
                                    pax[j].title = itinerary.Roomtype[i].PassenegerInfo[j].Title;
                                    pax[j].name = itinerary.Roomtype[i].PassenegerInfo[j].Firstname;
                                    pax[j].surname = itinerary.Roomtype[i].PassenegerInfo[j].Lastname;
                                    if (itinerary.Roomtype[i].PassenegerInfo[j].PaxType == HotelPaxType.Adult)
                                    {
                                        pax[j].type = "AD";
                                    }
                                    else
                                    {
                                        pax[j].type = "CH";
                                        pax[j].age = itinerary.Roomtype[i].PassenegerInfo[j].Age;
                                    }
                                }
                                bookingRooms[i].paxes = pax;
                            }
                            bookingitems[0].rooms = bookingRooms;
                            bookingRequest.booking_items = bookingitems;
                        }
                        Holder leadPax = new Holder();
                        leadPax.title = container.PassenegerInfo[0].Title;
                        leadPax.name = container.PassenegerInfo[0].Firstname;
                        leadPax.surname = container.PassenegerInfo[0].Lastname;
                        leadPax.phone_number = container.PassenegerInfo[0].Phoneno;
                        leadPax.email = container.PassenegerInfo[0].Email;
                        leadPax.client_nationality = container.PassenegerInfo[0].NationalityCode;
                        bookingRequest.holder = leadPax;
                    }
                }

                request = JsonConvert.SerializeObject(bookingRequest,
           new JsonSerializerSettings()
           {
               NullValueHandling = NullValueHandling.Ignore
           });
                request = request.Replace(",\"age\":0", " ");
            }

            catch (Exception ex)
            {
                Audit.Add(EventType.GrnBooking, Severity.Normal, 1, "GRN.GenerateBookingRequest Err :" + ex.ToString(), "0");
                throw ex;
            }

            return request;
        }
        #endregion

        #region Booking Cancel
        /// <summary>
        ///Booking Cancellation 
        /// </summary>
        public Dictionary<string, string> CancelHotelBooking(string bookingrefno)
        {
            Dictionary<string, string> cancelRes = new Dictionary<string, string>();
            string requestXml = GenerateCancellationRequest(bookingrefno);
            try
            {
                string filePath = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_GRNCancellationRequest.xml";
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(requestXml, "Root");
                xmlDocRes.Save(filePath);
                //JsonFormat
                string filePathJson = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_GRNCancellationRequest.txt";
                StreamWriter sw = new StreamWriter(filePathJson);
                sw.Write(requestXml.ToString());
                sw.Close();
            }
            catch { }
            Dictionary<string, string> searchRes = new Dictionary<string, string>();
            //string resp;
            string cancelResponse = string.Empty;
            try
            {
                cancelResponse = SendDeleteRequest(ConfigurationSystem.GrnConfig["Booking"] + "/" + bookingrefno);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnCancel, Severity.Normal, 1, "GRN.CancelHotelBooking Err :" + ex.ToString(), "0");
                throw ex;
            }
            try
            {
                cancelRes = readCancelResponse(cancelResponse, bookingrefno);
            }
            catch (Exception ex)
            {
                throw new BookingEngineException("Error: " + ex.ToString());
            }

            return cancelRes;
        }

        /// <summary>
        /// This private function is used for Generates the Hotel cancel request
        /// </summary>
        /// <param name="bookingreference">sending BookingReference for BookingCancellation</param>
        /// <returns>string</returns>
        private string GenerateCancellationRequest(string bookingreference)
        {
            string cancellationRequest= string.Empty;
            BookingCancel bookingCancellation = new BookingCancel();
            bookingCancellation.bookingReference = bookingreference;
            cancellationRequest = JsonConvert.SerializeObject(bookingCancellation,
          new JsonSerializerSettings()
          {
              NullValueHandling = NullValueHandling.Ignore
          });
            return cancellationRequest;
        }

        /// <summary>
        /// This private function is used for Read the Hotel cancel Response
        /// </summary>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> readCancelResponse(string response,string referenceNumber)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            try
            {
                XmlDocument xmlDocRes = new XmlDocument();
                try
                {
                    string filePath = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_BookingCancelResponse.xml";
                    xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                    xmlDocRes.Save(filePath);
                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_BookingCancelResponse.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }
                catch { }
                XmlNode errorNode = xmlDocRes.SelectSingleNode("Root/errors/messages");
                if (errorNode != null)
                {
                    cancellationCharges.Add("Status", errorNode.InnerText);
                }
                else
                {
                    XmlNode refNode = xmlDocRes.SelectSingleNode("Root/booking_reference");
                    if(refNode != null)
                    {
                        if (referenceNumber == refNode.InnerText)
                        {
                            XmlNode tempNode = xmlDocRes.SelectSingleNode("Root/status");
                            if (tempNode != null)
                            {
                                if (tempNode.InnerText == "confirmed")
                                {
                                    cancellationCharges.Add("Status", "Cancelled");
                                }
                                else
                                {
                                    cancellationCharges.Add("Status", tempNode.InnerText);
                                }

                            }
                            tempNode = xmlDocRes.SelectSingleNode("Root/cancellation_reference");
                            if (tempNode != null)
                            {
                                cancellationCharges.Add("ID", tempNode.InnerText);
                            }
                            //item price
                            tempNode = xmlDocRes.SelectSingleNode("Root/cancellation_charges/amount");
                            if (tempNode != null)
                            {
                                cancellationCharges.Add("Amount", tempNode.InnerText);
                            }
                            tempNode = xmlDocRes.SelectSingleNode("Root/cancellation_charges/currency");
                            if (tempNode != null)
                            {
                                cancellationCharges.Add("Currency", tempNode.InnerText);
                            }
                            tempNode = xmlDocRes.SelectSingleNode("Root/booking_id");
                            if (tempNode != null)
                            {
                                cancellationCharges.Add("BookingId", tempNode.InnerText);
                            }
                        }
                        else
                        {
                            cancellationCharges.Add("Status", "FAILED");
                        }
                    }
                    else
                    {
                        cancellationCharges.Add("Status", "FAILED");
                    }
                    string bookingResponse = string.Empty;
                }
                if (cancellationCharges["Status"] == "Booking has already been cancelled")
                {
                    cancellationCharges["Status"] = "Cancelled";
                    DataTable hotelCharges = HotelItinerary.GetCancellationDetails(referenceNumber);
                    if (hotelCharges != null && hotelCharges.Rows.Count > 0)
                    {
                        DataRow drcharges = hotelCharges.Rows[0];
                        string cancelCharges = Convert.ToString(drcharges["hotelCancelPolicy"]);
                        string[] charges = cancelCharges.Split('|');
                        if (charges.Count() > 0)
                        {
                            DateTime currentDate = DateTime.Now;
                            string chargedetails = string.Empty;
                            for (int i = 0; i < charges.Count(); i++)
                            {
                                string amount = string.Empty;
                                chargedetails = charges[i].Split(',')[0].Split(':')[1];
                                string month = chargedetails.Split('/')[1];
                                if (month == "Jan")
                                {
                                    chargedetails = chargedetails.Replace(month, "01");
                                }
                                if (month == "Feb")
                                {
                                    chargedetails = chargedetails.Replace(month, "02");
                                }
                                if (month == "Mar")
                                {
                                    chargedetails = chargedetails.Replace(month, "03");
                                }
                                if (month == "Apr")
                                {
                                    chargedetails = chargedetails.Replace(month, "04");
                                }
                                if (month == "May")
                                {
                                    chargedetails = chargedetails.Replace(month, "05");
                                }
                                if (month == "Jun")
                                {
                                    chargedetails = chargedetails.Replace(month, "06");
                                }
                                if (month == "Jul")
                                {
                                    chargedetails = chargedetails.Replace(month, "07");
                                }
                                if (month == "Aug")
                                {
                                    chargedetails = chargedetails.Replace(month, "08");
                                }
                                if (month == "Sep")
                                {
                                    chargedetails = chargedetails.Replace(month, "09");
                                }
                                if (month == "Oct")
                                {
                                    chargedetails = chargedetails.Replace(month, "10");
                                }
                                if (month == "Nov")
                                {
                                    chargedetails = chargedetails.Replace(month, "11");
                                }
                                if (month == "Dec")
                                {
                                    chargedetails = chargedetails.Replace(month, "12");
                                }

                                DateTime cancellationDate = DateTime.ParseExact(chargedetails, "dd/MM/yyyy", null);
                                if(charges[i].Contains("%"))
                                {
                                    if(currentDate.CompareTo(cancellationDate) >= 0)
                                    {
                                        if (!string.IsNullOrEmpty(charges[i].Split(',')[1].Split(' ')[5]))
                                        {
                                            amount = charges[i].Split(',')[1].Split(' ')[5];
                                            cancellationCharges.Add("Amount", amount);
                                            cancellationCharges.Add("BookingId", Convert.ToString(drcharges["confirmationNo"]));
                                            cancellationCharges.Add("ID", Convert.ToString(drcharges["confirmationNo"]));
                                            cancellationCharges.Add("Currency", string.Empty);
                                            break;
                                        }
                                    }
                                }
                                else if (cancellationDate.CompareTo(currentDate) <= 0)
                                {
                                    if (!string.IsNullOrEmpty(charges[i].Split(',')[1].Split(' ')[5]))
                                    {
                                        amount = charges[i].Split(',')[1].Split(' ')[5];
                                        cancellationCharges.Add("Amount", amount);
                                        cancellationCharges.Add("BookingId", Convert.ToString(drcharges["confirmationNo"]));
                                        cancellationCharges.Add("ID", Convert.ToString(drcharges["confirmationNo"]));
                                        cancellationCharges.Add("Currency", string.Empty);
                                        break;
                                    }                                                      
                                }
                                
                            }
                            if (!cancellationCharges.ContainsKey("Amount") && !cancellationCharges.ContainsKey("BookingId") && !cancellationCharges.ContainsKey("ID"))
                            {
                                cancellationCharges.Add("Amount", "0");
                                cancellationCharges.Add("BookingId", Convert.ToString(drcharges["confirmationNo"]));
                                cancellationCharges.Add("ID", Convert.ToString(drcharges["confirmationNo"]));
                                cancellationCharges.Add("Currency", string.Empty);
                            }

                        }
                    }


                    
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnCancel, Severity.Normal, 1, "GRN.readCancelResponse Err :" + ex.ToString(), "0");
                throw ex;
            }

            return cancellationCharges;
        }

        #endregion

        #region StaticData Download 
        /// <summary>
        /// This public function is used for download the all hotel static data based on City
        ///    /// <param name="cityId">sending cityId for GetHotelStaticData</param>
        /// </summary>       
        public void GetHotelStaticData(string cityId)
        {
            try
            {

                string resp = string.Empty;
                try
                {
                    resp = ReadGetResponse(ConfigurationSystem.GrnConfig["StaticImages"] + "?city=" + cityId);
                }
                catch (Exception ex)
                {

                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    ReadHotelStaticData(resp);

                }
                catch (Exception ex)
                {

                    throw new BookingEngineException("Error: " + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnStatic, Severity.Normal, 1, "GRN.GetHotelStaticData Err :" + ex.ToString(), "0");
                throw ex;
            }
        }
        private void ReadHotelStaticData(string response)
        {
            try
            {
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    string filePath = xmlPath + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CityWiseHotelsData.xml"; ;
                    xmlDocRes.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                    //JsonFormat
                    string filePathJson = xmlPath + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CityWiseHotelsData.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }
                catch { }
                XmlNodeList xmlHotels = xmlDocRes.SelectNodes("Root/hotels");
                foreach (XmlNode hotels in xmlHotels)
                {
                    string cityCode = string.Empty;
                    string hotelCode = string.Empty;
                    string hotelName = string.Empty;
                    XmlNode tempNode = hotels.SelectSingleNode("code");
                    if (tempNode != null)
                    {
                        hotelCode = tempNode.InnerText;
                    }
                    tempNode = hotels.SelectSingleNode("name");
                    if (tempNode != null)
                    {
                        hotelName = tempNode.InnerText;
                    }
                    tempNode = hotels.SelectSingleNode("city_code");
                    if (tempNode != null)
                    {
                        cityCode = tempNode.InnerText;
                    }
                    GetHotelDetailsInformation(cityCode, hotelName, hotelCode);

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnStatic, Severity.Normal, 1, "GRN.ReadHotelStaticData Err :" + ex.ToString(), "0");
                throw ex;
            }

        }
        public HotelDetails GetHotelDetailsInformation(string cityCode, string hotelName, string hotelCode)
        {
            int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.GrnConfig["TimeStamp"]);
            bool imageUpdate = true;
            bool isUpdateReq = false;
            bool isReqSend = false;

            string request = string.Empty;
            string resp = string.Empty;
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData staticInfo = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();

            try
            {
                staticInfo.Load(hotelCode, cityCode, HotelBookingSource.GRN);
                imageInfo.Load(hotelCode, cityCode, HotelBookingSource.GRN);
                if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                {
                    imageUpdate = false;
                }
                if (staticInfo.HotelName != null && staticInfo.HotelName.Length > 0 && !string.IsNullOrEmpty(staticInfo.HotelDescription) && !string.IsNullOrEmpty(staticInfo.HotelAddress))
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(staticInfo.TimeStamp);
                    //if it is less than REquired time stamp days then load from DB
                    if (diffRes.Days > timeStampDays)
                    {
                        // Set the variable as true
                        isUpdateReq = true;
                        imageUpdate = true;
                    }
                    else
                    {
                        hotelInfo.HotelCode = staticInfo.HotelCode;
                        hotelInfo.HotelName = staticInfo.HotelName;
                        hotelInfo.Map = staticInfo.HotelMap;
                        hotelInfo.Description = staticInfo.HotelDescription;
                        hotelInfo.HotelPolicy = staticInfo.HotelPolicy;

                        string[] image = new string[0];
                        List<string> dldImgList = new List<string>();
                        {
                            string[] dldImages = imageInfo.Images.Split('|');
                            foreach (string img in dldImages)
                            {
                                if (!string.IsNullOrEmpty(img)) dldImgList.Add(img);
                            }
                        }
                        hotelInfo.Images = dldImgList;
                        //hotelInfo.Image = dldImgList.Count > 0 ? dldImgList[0] : "";
                        hotelInfo.Address = staticInfo.HotelAddress;
                        hotelInfo.PhoneNumber = staticInfo.PhoneNumber;
                        if (!string.IsNullOrEmpty(staticInfo.PinCode))
                        {
                            hotelInfo.PinCode = staticInfo.PinCode;
                        }
                        else
                        {
                            hotelInfo.PinCode = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(staticInfo.EMail))
                        {
                            hotelInfo.Email = staticInfo.EMail;
                        }
                        if (!string.IsNullOrEmpty(staticInfo.URL))
                        {
                            hotelInfo.URL = staticInfo.URL;
                        }
                        List<string> facilities = new List<string>();
                        List<string> roomFacilities = new List<string>();
                        if (!string.IsNullOrEmpty(staticInfo.HotelFacilities))
                        {
                            string[] facilList = staticInfo.HotelFacilities.Split(';');
                            foreach (string facl in facilList)
                            {
                                facilities.Add(facl);
                            }

                        }
                        hotelInfo.HotelFacilities = facilities;
                    }
                }
                else
                {
                    isReqSend = true;
                }
                if (isReqSend || isUpdateReq)
                {
                    HotelDetails htldetails = new HotelDetails();
                    hotelInfo = GetHotelDetails(hotelCode);
                    htldetails = GetHotelImageDetails(hotelCode);
                    hotelInfo.Images = htldetails.Images;
                    if (hotelInfo.HotelName == null)
                    {
                        return hotelInfo;
                    }
                    else
                    {
                        if (htldetails.Images != null && htldetails.Images.Count > 0)
                        {
                            string htlimagespath = string.Empty;
                            HotelImages images = new HotelImages();
                            images.CityCode = cityCode;
                            images.HotelCode = hotelCode;
                            images.Source = HotelBookingSource.GRN;
                            foreach (string img in htldetails.Images)
                            {
                                if (!String.IsNullOrEmpty(htlimagespath))
                                {
                                    htlimagespath = htlimagespath + "|" + img;
                                }
                                else
                                {
                                    htlimagespath = img;
                                }
                            }
                            images.Images = htlimagespath;
                            images.DownloadedImgs = htlimagespath;
                            if (isUpdateReq)
                            {
                                if (imageUpdate)
                                {
                                    images.Update();
                                }
                            }
                            else
                            {
                                if (imageUpdate)
                                {
                                    images.Save();
                                }
                            }
                        }
                        staticInfo.HotelCode = hotelCode;
                        staticInfo.HotelName = hotelName;
                        staticInfo.CityCode = cityCode;
                        staticInfo.HotelAddress = hotelInfo.Address;
                        if (hotelInfo.HotelFacilities != null && hotelInfo.HotelFacilities.Count > 0)
                        {
                            string hotelFacilities = string.Empty;
                            foreach (string facilities in hotelInfo.HotelFacilities)
                            {

                                if (!String.IsNullOrEmpty(hotelFacilities))
                                {
                                    hotelFacilities = hotelFacilities + "|" + facilities;
                                }
                                else
                                {
                                    hotelFacilities = facilities;
                                }
                            }
                            staticInfo.HotelFacilities = hotelFacilities;
                        }
                        else
                        {
                            staticInfo.HotelFacilities = string.Empty;
                        }

                        staticInfo.HotelMap = hotelInfo.Map;
                        staticInfo.Rating = HotelRating.All;
                        staticInfo.HotelLocation = string.Empty;
                        staticInfo.SpecialAttraction = string.Empty;
                        staticInfo.Source = HotelBookingSource.GRN;
                        staticInfo.Status = true;
                        staticInfo.HotelDescription = hotelInfo.Description;
                        staticInfo.PinCode = hotelInfo.PinCode;
                        staticInfo.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnStatic, Severity.Normal, 1, "GRN.GetHotelDetailsInformation Err :" + ex.ToString(), "0");
                throw ex;
            }
            return hotelInfo;
        }
        private HotelDetails GetHotelImageDetails(string hotelCode)
        {
            HotelDetails hotelDetailImages = new HotelDetails();
            try
            {
                string resp = string.Empty;
                try
                {
                    resp = ReadGetResponse(ConfigurationSystem.GrnConfig["StaticImages"] + hotelCode + "/images");
                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    hotelDetailImages = ReadImageDetailsResponse(resp);
                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnStatic, Severity.Normal, 1, "GRN.GetHotelImageDetails Err :" + ex.ToString(), "0");
                throw ex;
            }
            return hotelDetailImages;
        }

        private HotelDetails ReadImageDetailsResponse(string imgreponse)
        {
            HotelDetails hotelimagedetails = new HotelDetails();
            try
            {
                HotelImages htlimages = new HotelImages();
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(imgreponse, "Root");
                try
                {
                    string filePath = xmlPath+grnUserId+"_"+ DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_HotelWiseImagesData.xml"; ;
                    xmlDocRes.Save(filePath);
                    //JsonFormat
                    string filePathJson = xmlPath + "_"+grnUserId+"_"+ DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_HotelWiseImagesData.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(imgreponse.ToString());
                    sw.Close();
                }
                catch { }
                hotelimagedetails.Images = new List<string>();
                XmlNodeList xmlHotelsimages = xmlDocRes.SelectNodes("Root/images/regular");
                if (xmlHotelsimages != null)
                {
                    string imagepath = string.Empty;
                    foreach (XmlNode htlimage in xmlHotelsimages)
                    {
                        XmlNode tempNode = htlimage.SelectSingleNode("url");
                        if (tempNode != null)
                        {
                            imagepath = tempNode.InnerText;
                            hotelimagedetails.Images.Add(imagepath);
                        }
                    }
                }
                else
                {
                    XmlNode errorInfo = xmlDocRes.SelectSingleNode("Root/errors/messages");
                    if (errorInfo != null && errorInfo.InnerText.Length > 0)
                    {
                        htlimages.Images = errorInfo.InnerText;
                    }
                    errorInfo = xmlDocRes.SelectSingleNode("root/hotel_code");
                    if (errorInfo != null && errorInfo.InnerText.Length > 0)
                    {
                        htlimages.HotelCode = errorInfo.InnerText;
                    }
                    hotelimagedetails.Images.Add(htlimages.Images);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnStatic, Severity.Normal, 1, "GRN.ReadImageDetailsResponse Err :" + ex.ToString(), "0");
                throw ex;
            }
            return hotelimagedetails;
        }
        private HotelDetails GetHotelDetails(string hotelCode)
        {

            HotelDetails hoteldetails = new HotelDetails();
            try
            {
                string resp = string.Empty;
                try
                {
                    resp = ReadGetResponse(ConfigurationSystem.GrnConfig["StaticImages"] + "?hcode=" + hotelCode);
                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    hoteldetails = ReadHotelDetailsResponse(resp);
                }
                catch (Exception ex)
                {
                    throw new BookingEngineException("Error: " + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnStatic, Severity.Normal, 1, "GRN.GetHotelDetails Err :" + ex.ToString(), "0");
                throw ex;
            }
            return hoteldetails;
        }
        private HotelDetails ReadHotelDetailsResponse(string response)
        {
            HotelDetails hotelidetails = new HotelDetails();
            try
            {
                hotelidetails.HotelFacilities = new List<string>();
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    string filePath = xmlPath +grnUserId+"_"+ DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_HotelWiseData.xml"; ;
                    xmlDocRes.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                    //JsonFormat
                    string filePathJson = xmlPath + grnUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_HotelWiseData.txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }
                catch { }
                XmlNode detailnode = xmlDocRes.SelectSingleNode("Root/hotels");
                XmlNode tempNode = detailnode.SelectSingleNode("description");
                if (tempNode != null)
                {
                    hotelidetails.Description = tempNode.InnerText;
                }
                else
                {
                    hotelidetails.Description = string.Empty;
                }
                tempNode = detailnode.SelectSingleNode("name");
                if (tempNode != null)
                {
                    hotelidetails.HotelName = tempNode.InnerText;
                }
                string address = string.Empty;
                tempNode = detailnode.SelectSingleNode("address");
                if (tempNode != null)
                {
                    address = tempNode.InnerText;
                }
                tempNode = detailnode.SelectSingleNode("postal_code");
                if (tempNode != null)
                {
                    hotelidetails.PinCode = tempNode.InnerText;
                    if (!string.IsNullOrEmpty(address))
                    {
                        address = address + "," + tempNode.InnerText;
                    }
                    else
                    {
                        address = tempNode.InnerText;
                    }
                }
                hotelidetails.Address = address;
                hotelidetails.HotelFacilities = new List<string>();
                tempNode = detailnode.SelectSingleNode("facilities");
                if (tempNode != null)
                {
                    hotelidetails.HotelFacilities.Add(tempNode.InnerText);
                }
                string Map = string.Empty;
                XmlNode longitude = detailnode.SelectSingleNode("longitude");
                XmlNode latitude = detailnode.SelectSingleNode("latitude");
                if (longitude != null && latitude != null)
                {
                    hotelidetails.Map = latitude.InnerText + "|" + longitude.InnerText;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GrnStatic, Severity.Normal, 1, "GRN.GetHotelDetails Err :" + ex.ToString(), "0");
                throw ex;
            }
            return hotelidetails;
        }
        #endregion
    }

}

