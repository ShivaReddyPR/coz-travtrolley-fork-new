﻿using System;
using System.Collections.Generic;
using System.Text;
using CT.Configuration;
using System.Xml;
using System.Net;
using System.IO;
using System.IO.Compression;
using CT.Core;


namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// This class is used to interact with EETApi(JuniperXML)
    /// HotelBookingSource Value for EET=16
    /// Following is the steps in Booking Flow for this source
    /// STEP-1:HotelSearch(Availability)
    /// STEP-2:Every 15days(what we configuraed in App.config) Need to download StaticData
    /// STEP-3:Loading Cancellation policy
    /// STEP-4:Booking (Booking)
    /// STEP-5:Cancel
    /// </summary>
    public class EET:IDisposable
    {
        #region Members
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        private string XmlPath = string.Empty;
        /// <summary>
        /// agentCode will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string agentCode;
        /// <summary>
        /// password will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string password;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        private string sessionId;
        /// <summary>
        /// This variable is used,to store the login userid,which is read from MetaserchEngine  
        /// </summary>
        private int appUserId;
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        decimal rateOfExchange = 1;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        string agentCurrency = "";
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        int decimalPoint;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        #endregion

        #region Constructors
        /// <summary>
        /// Default Constrcutor for EET
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        public EET()
        {
            XmlPath = ConfigurationSystem.EETConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
            agentCode = ConfigurationSystem.EETConfig["AgentCode"];
            password = ConfigurationSystem.EETConfig["Password"];
        }
        /// <summary>
        /// Parametarized Constrcutor for EET
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        /// <param name="SessionId">SessionId(Unique SessionId Every Request will be Genarated New Id)</param>
        public EET(string SessionId)
        {
            agentCode = ConfigurationSystem.EETConfig["AgentCode"];
            password = ConfigurationSystem.EETConfig["Password"];
            XmlPath = ConfigurationSystem.EETConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
            this.sessionId = SessionId;
        }
        #endregion
        #region Properties
        /// <summary>
        ///login UserId
        /// </summary>
        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        /// <summary>
        /// For storing current agent Base Currency against which conversion need to be applied.
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        #endregion

        #region Common Methods
        /// <summary>
        ///This method is converting ContentEncoding to Decompress(i.e GZIP, Deflate, default) 
        /// </summary>
        /// <param name="webResponse">Response Object</param>
        /// <returns>Stream</returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        #endregion

        #region Get Hotel Results
        /// <summary>
        /// Returns Hotel Results for the Search Criteria.
        /// </summary>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">B2B is MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public HotelSearchResult[] GetSearchResults(HotelRequest request, decimal markup, string markupType)
        {
            List<HotelSearchResult> Results = new List<HotelSearchResult>();
            try
            {
                DateTime before = DateTime.Now;
                XmlDocument responseXML = GetHotelSearchResponse(request);
                DateTime after = DateTime.Now;
                TimeSpan ts = after - before;

                //Audit.Add(EventType.EETAvailSearch, Severity.Normal, 1, "EET DLL Search returned in " + ts.Seconds + " Seconds", "1");
                if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0)
                {
                    Results = ReadHotelSearchResponse(responseXML, ref request, markup, markupType);
                }

                if (request.HotelName != null && request.HotelName.Trim().Length > 0)
                {
                    Results = Results.FindAll(delegate(HotelSearchResult obj) { return obj.HotelName.ToLower().Contains(request.HotelName.ToLower()); });
                }

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get Hotel Results : " + ex.Message, ex);
            }

            return Results.ToArray();
        }

        /// <summary>
        /// This private function is used for generating the Search Request of EET
        /// </summary>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <returns>string</returns>
        private string GenerateHotelSearchRequest(HotelRequest request)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_HotelAvailService", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_HotelAvailRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS
                writer.WriteStartElement("AvailRequestSegments");
                writer.WriteStartElement("AvailRequestSegment");
                writer.WriteStartElement("StayDateRange");
                writer.WriteAttributeString("Start", request.StartDate.ToString("yyyy-MM-dd"));
                writer.WriteAttributeString("End", request.EndDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();//StayDateRange

                writer.WriteStartElement("RoomStayCandidates");
                foreach (RoomGuestData guest in request.RoomGuest)
                {
                    writer.WriteStartElement("RoomStayCandidate");
                    writer.WriteAttributeString("Quantity", "1");
                    writer.WriteStartElement("GuestCounts");
                    writer.WriteStartElement("GuestCount");
                    //writer.WriteAttributeString("Age", "40"); not mandatory
                    writer.WriteAttributeString("Count", guest.noOfAdults.ToString());
                    writer.WriteEndElement();//GuestCount
                    if (guest.noOfChild > 0)
                    {
                        foreach (int age in guest.childAge)
                        {
                            writer.WriteStartElement("GuestCount");
                            writer.WriteAttributeString("Age", age.ToString());
                            writer.WriteAttributeString("Count", "1");
                            writer.WriteEndElement();//GuestCount
                        }
                    }
                    writer.WriteEndElement();//RoomStayCandidate
                    writer.WriteEndElement();//RoomStayCandidate    
                }

                writer.WriteEndElement();//RoomStayCandidates
                writer.WriteStartElement("HotelSearchCriteria");
                writer.WriteStartElement("Criterion");
                writer.WriteStartElement("HotelRef");
                writer.WriteAttributeString("HotelCityCode", request.CityCode);
                writer.WriteAttributeString("HotelCode", "");
                writer.WriteEndElement();//HotelRef
                writer.WriteStartElement("TPA_Extensions");
                writer.WriteElementString("ShowBasicInfo", "1");
                //writer.WriteElementString("ShowPromotions", "1");
                //writer.WriteElementString("ShowCatalogueData", "1");
                //writer.WriteElementString("ShowInternalRoomInfo", "1");
                writer.WriteElementString("ShowOnlyAvailable", "1");
                writer.WriteElementString("PaxCountry", request.PassengerNationality);
                writer.WriteElementString("SearchByPaymentType", "ExcludePaymentInDestination");
                writer.WriteEndElement();//TPA_Extensions
                writer.WriteEndElement();//Criterion
                writer.WriteEndElement();//HotelSearchCriterion
                writer.WriteEndElement();//AvailabilitySegment
                writer.WriteEndElement();//AvailabilitySegments
                writer.WriteEndElement();//OTA_HotelAvailRQ
                writer.WriteEndElement();//OTA_HotelAvailService
                writer.WriteEndElement();//Soap:Body
                writer.WriteEndElement();//Soap:Envelope

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath =XmlPath + "HotelSearchRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                }
                catch { }


            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to generate Hotel Request message : " + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// Generates the Response XML for the Request string.
        /// </summary>
        /// <param name="hotelRequest">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <returns>XmlDocument</returns>
        private XmlDocument GetHotelSearchResponse(HotelRequest hotelRequest)
        {
            XmlDocument document = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.EETConfig["HotelAvailReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelSearchRequest(hotelRequest);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    document.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get response from Hotel Search request : " + ex.Message, ex);
            }
            return document;
        }


        /// <summary>
        /// Reads Hotel Search Response XML and returns HotelSearchResult Array.
        /// </summary>
        /// <param name="doc">xml Response Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>List HotelSearchResult</returns>
        /// <remarks>Here Only we are calucating B2B Markup and InputVat and also Genarting Hotel Results object</remarks>
        private List<HotelSearchResult> ReadHotelSearchResponse(XmlDocument doc, ref HotelRequest request, decimal markup, string markupType)
        {
            List<HotelSearchResult> hotelResults = new List<HotelSearchResult>();

            try
            {
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "HotelSearchResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                }
                catch { }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");

                XmlNode root = doc.DocumentElement;
                XmlNodeList Errors = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                if (Errors != null && Errors[0].Name == "Errors")
                {
                    hotelResults = new List<HotelSearchResult>();
                }
                else
                {
                    PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                    CatalogueData cd = new CatalogueData(HotelBookingSource.EET);
                    XmlNode HotelAvailResult = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild;

                    string sequenceNumber = HotelAvailResult.Attributes["SequenceNmbr"].Value;

                    XmlNodeList roomStays = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes[1].ChildNodes;
                    //Depicts a Hotel Room list
                    //DateTime totalBefore = DateTime.Now;
                    //DateTime before = DateTime.Now;
                    foreach (XmlNode roomStay in roomStays)
                    {
                        // before = DateTime.Now;

                        HotelSearchResult result = new HotelSearchResult();
                        result.RoomGuest = request.RoomGuest;
                        result.SequenceNumber = sequenceNumber;
                        result.StartDate = request.StartDate;
                        result.EndDate = request.EndDate;
                        result.Currency = agentCurrency;//childRate.Attributes["CurrencyCode"].Value;
                        result.Price = new PriceAccounts();
                        foreach (XmlNode child in roomStay)
                        {
                            result.BookingSource = HotelBookingSource.EET;
                            result.RateType = RateType.Negotiated;
                            switch (child.Name)
                            {
                                case "RoomRates":
                                    #region Room Details
                                    int rooms = 0;
                                    List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();

                                    foreach (XmlNode rate in child.ChildNodes)
                                    {


                                        //Avoid OnRequest room types in the result
                                        if (rate.Attributes["AvailabilityStatus"].Value == "AvailableForSale")
                                        {
                                            HotelRoomsDetails room = new HotelRoomsDetails();
                                            room.RatePlanCode = rate.Attributes["RatePlanCode"].Value;
                                            room.RoomTypeCode = rate.Attributes["RatePlanCategory"].Value;
                                            room.mealPlanDesc = room.RoomTypeCode;//If MealPlan is not available, this will be shown and stored in DB.
                                            foreach (XmlNode childRate in rate.ChildNodes)
                                            {
                                                switch (childRate.Name)
                                                {
                                                    case "Rates":
                                                        foreach (XmlNode rrNode in childRate.ChildNodes)
                                                        {
                                                            List<RoomRates> roomRates = new List<RoomRates>();

                                                            foreach (XmlNode ratesNode in rrNode.ChildNodes)
                                                            {
                                                                room.SequenceNo = rrNode.Attributes["RateSource"].Value;
                                                                string cc = "USD";

                                                                switch (ratesNode.Name)
                                                                {
                                                                    case "Total":
                                                                        #region Room Rates
                                                                        cc = (ratesNode.Attributes["CurrencyCode"] != null ? ratesNode.Attributes["CurrencyCode"].Value : "USD");
                                                                        rateOfExchange = (exchangeRates.ContainsKey(cc) ? exchangeRates[cc] : 1);
                                                                        result.Price = new PriceAccounts();
                                                                        result.Price.SupplierCurrency = cc;
                                                                        result.Price.RateOfExchange = rateOfExchange;
                                                                        int days = request.EndDate.Subtract(request.StartDate).Days;

                                                                        //VAT Calucation Changes Modified 14.12.2017
                                                                        decimal hotelTotalPrice = 0m;
                                                                        decimal vatAmount = 0m;
                                                                        hotelTotalPrice = Math.Round(Convert.ToDecimal(ratesNode.Attributes["AmountAfterTax"].Value) * rateOfExchange, decimalPoint);
                                                                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                                                        {
                                                                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                                                        }
                                                                        hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                                                                        for (int i = 0; i < days; i++)
                                                                        {
                                                                            RoomRates roomRate = new RoomRates();
                                                                            roomRate.Amount = hotelTotalPrice;
                                                                            roomRate.RateType = RateType.Negotiated;
                                                                            roomRate.SellingFare = roomRate.Amount;
                                                                            roomRate.Totalfare = roomRate.Amount;
                                                                            roomRate.BaseFare = roomRate.Amount;
                                                                            roomRate.Days = request.EndDate;
                                                                            roomRates.Add(roomRate);
                                                                        }
                                                                        room.supplierPrice = Convert.ToDecimal(ratesNode.Attributes["AmountAfterTax"].Value);
                                                                        room.SellingFare = hotelTotalPrice;
                                                                        room.TotalPrice = room.SellingFare;
                                                                        room.Markup = (markupType == "F" ? markup : room.SellingFare * (markup / 100));
                                                                        room.MarkupType = markupType;
                                                                        room.MarkupValue = markup;
                                                                        room.TaxDetail = new PriceTaxDetails();
                                                                        room.TaxDetail = priceTaxDet;
                                                                        room.InputVATAmount = vatAmount;
                                                                        room.Rates = roomRates.ToArray();
                                                                        #endregion
                                                                        break;
                                                                    case "RateDescription":
                                                                        room.RoomTypeName = ratesNode.FirstChild.InnerText;
                                                                        break;
                                                                    case "TPA_Extensions":
                                                                        if (ratesNode.ChildNodes != null)
                                                                        {
                                                                            foreach (XmlNode extNode in ratesNode.ChildNodes)
                                                                            {
                                                                                switch (extNode.Name.ToUpper())
                                                                                {
                                                                                    case "MEALPLAN":
                                                                                        //room.mealPlanDesc = (extNode.InnerText == "3" ? "Room Only" : extNode.InnerText);//TODO: code is stored now. need to retrieve from  data later.
                                                                                        try
                                                                                        {
                                                                                            // CatalogueData cd = new CatalogueData();
                                                                                            room.mealPlanDesc = cd.GetStaticData(extNode.InnerText, "MP", HotelBookingSource.EET);
                                                                                        }
                                                                                        catch { }
                                                                                        break;
                                                                                    //case "RECOMMENDEDPRICE":
                                                                                    //    room.SellingFare = Convert.ToDecimal(extNode.InnerText) * rateOfExchange;
                                                                                    //    room.TotalPrice = room.SellingFare;
                                                                                    //    room.Markup = (markupType == "F" ? markup : room.SellingFare * (markup / 100));
                                                                                    //    room.MarkupType = markupType;
                                                                                    //    room.MarkupValue = markup;
                                                                                    //    for (int i = 0; i < room.Rates.Length; i++)
                                                                                    //    {
                                                                                    //        RoomRates rr = room.Rates[i];
                                                                                    //        rr.Amount = room.SellingFare;
                                                                                    //        rr.BaseFare = room.SellingFare;
                                                                                    //        rr.SellingFare = room.SellingFare;
                                                                                    //        rr.Totalfare = room.SellingFare;
                                                                                    //    }
                                                                                    //    break;
                                                                                }
                                                                            }
                                                                        }
                                                                        break;
                                                                }

                                                            }

                                                            room.Rates = roomRates.ToArray();
                                                            if (!roomDetails.Contains(room))
                                                            {
                                                                roomDetails.Add(room);
                                                            }
                                                            if (Convert.ToInt32(rrNode.Attributes["NumberOfUnits"].Value) > 1)
                                                            {
                                                                int rc = Convert.ToInt32(rrNode.Attributes["NumberOfUnits"].Value) - 1;
                                                                for (int i = rc; i < Convert.ToInt32(rrNode.Attributes["NumberOfUnits"].Value); i++)
                                                                {
                                                                    roomDetails.Add(room);
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    case "Features":
                                                        //TODO: need to discuss with Ziyad.
                                                        foreach (XmlNode offerNode in childRate.ChildNodes)
                                                        {
                                                            switch (offerNode.Attributes["RoomViewCode"].Value)
                                                            {
                                                                case "PROMO":
                                                                    result.PromoMessage = offerNode.FirstChild.FirstChild.InnerText;
                                                                    for (int i = 0; i < roomDetails.Count; i++)
                                                                    {
                                                                        HotelRoomsDetails rd = roomDetails[i];
                                                                        if (room.RatePlanCode == rd.RatePlanCode)
                                                                        {
                                                                            rd.PromoMessage = offerNode.FirstChild.FirstChild.InnerText;
                                                                            roomDetails[i] = rd;
                                                                        }
                                                                    }
                                                                    break;
                                                                //case "RECOMMENDED_PRICE":
                                                                //    string cur = (childRate.Attributes["CurrencyCode"] != null ? childRate.Attributes["CurrencyCode"].Value : "USD");
                                                                //    result.Price.SupplierPrice = Convert.ToDecimal(offerNode.FirstChild.FirstChild.InnerText);
                                                                //    result.Price.SupplierCurrency = cur;
                                                                //    if (rooms == 0)
                                                                //    {
                                                                //        result.SellingFare = Convert.ToDecimal(offerNode.FirstChild.FirstChild.InnerText) * rateOfExchange;
                                                                //        result.TotalPrice = result.SellingFare + (markupType == "F" ? markup * request.NoOfRooms : result.SellingFare * (markup / 100));
                                                                //    }
                                                                //    break;
                                                            }
                                                        }
                                                        break;
                                                    case "Total":
                                                        if (rooms == 0 && result.TotalPrice == 0)
                                                        {
                                                            string ccode = (childRate.Attributes["CurrencyCode"] != null ? childRate.Attributes["CurrencyCode"].Value : "USD");
                                                            //VAT Calucation Changes Modified 14.12.2017
                                                            decimal hotelTotalPrice = 0m;
                                                            decimal vatAmount = 0m;
                                                            hotelTotalPrice = Math.Round(Convert.ToDecimal(childRate.Attributes["AmountAfterTax"].Value) * rateOfExchange, decimalPoint);
                                                            if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                                            {
                                                                hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                                            }
                                                            hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                                                            result.Price.SupplierPrice = Convert.ToDecimal(childRate.Attributes["AmountAfterTax"].Value);
                                                            result.SellingFare = hotelTotalPrice;
                                                            result.TotalPrice = result.SellingFare + (markupType == "F" ? markup * request.NoOfRooms : result.SellingFare * (markup / 100));
                                                        }
                                                        break;
                                                    case "TPA_Extensions":
                                                        //Avoid Direct Payment room types in the result
                                                        if (childRate != null && childRate.FirstChild != null)
                                                        {
                                                            if (childRate.FirstChild.Name.ToUpper() == "PAYMENTINDESTINATION")
                                                            {
                                                                room.RatePlanCode = string.Empty;
                                                            }
                                                            else if (childRate.FirstChild.Name.ToUpper() == "MEALPLAN")
                                                            {
                                                                //room.mealPlanDesc = (childRate.FirstChild.InnerText == "3" ? "Room Only" : "Other Meal Plan");//Need to fetch Meal Plan Desc                                                            
                                                                try
                                                                {
                                                                    // CatalogueData cd = new CatalogueData();
                                                                    room.mealPlanDesc = cd.GetStaticData(childRate.FirstChild.InnerText, "MP", HotelBookingSource.EET);
                                                                }
                                                                catch { }
                                                            }
                                                        }
                                                        break;
                                                }

                                            }
                                            rooms++;
                                        }
                                    }
                                    result.RoomDetails = roomDetails.ToArray();
                                    #endregion
                                    break;
                                case "Total":
                                    // result.Currency = agentCurrency;//childRate.Attributes["CurrencyCode"].Value;
                                    break;
                                case "BasicPropertyInfo":
                                    result.HotelName = child.Attributes["HotelName"].Value;
                                    result.HotelCode = child.Attributes["HotelCode"].Value;
                                    //result.SellingFare = (result.RoomDetails[0].SellingFare > 0 ? result.RoomDetails[0].SellingFare : 0);//Assuming lowest room fare comes first
                                    //result.TotalPrice = result.SellingFare;

                                    result.Price.NetFare = result.SellingFare;
                                    result.Price.AccPriceType = PriceType.NetFare;
                                    result.Price.CurrencyCode = result.Currency;
                                    result.Price.Currency = result.Currency;
                                    break;
                                case "TPA_Extensions":
                                    #region Hotel Info
                                    foreach (XmlNode subChild in child)
                                    {
                                        switch (subChild.Name)
                                        {
                                            case "HotelInfo":
                                                foreach (XmlNode childs in subChild.ChildNodes)
                                                {
                                                    switch (childs.Name)
                                                    {
                                                        #region Rating
                                                        case "Category":
                                                            switch (childs.Attributes["Code"].Value)
                                                            {
                                                                case "1":
                                                                    result.Rating = HotelRating.OneStar;
                                                                    break;
                                                                case "2":
                                                                    result.Rating = HotelRating.TwoStar;
                                                                    break;
                                                                case "3":
                                                                    result.Rating = HotelRating.ThreeStar;
                                                                    break;
                                                                case "4":
                                                                    result.Rating = HotelRating.FourStar;
                                                                    break;
                                                                case "5":
                                                                    result.Rating = HotelRating.FiveStar;
                                                                    break;
                                                                case "":
                                                                case "0":
                                                                    result.Rating = HotelRating.OneStar;
                                                                    break;
                                                            }
                                                            break;
                                                        #endregion
                                                        case "Description":
                                                            result.HotelDescription = System.Web.HttpUtility.HtmlDecode(childs.InnerText);
                                                            break;
                                                        case "Thumb":
                                                            result.HotelPicture = System.Web.HttpUtility.HtmlDecode(childs.InnerText);
                                                            break;
                                                        case "Address":
                                                            result.HotelAddress = System.Web.HttpUtility.HtmlDecode(childs.InnerText);
                                                            break;
                                                        case "AreaID":
                                                            //result.CityCode = (childs.InnerText == "96952" || childs.InnerText == "53607" ? "Dubai City" : childs.InnerText); //commented by brahmam Location filtering purpose only using(shiva said)
                                                            result.CityCode = request.CityCode;
                                                            break;
                                                        case "Provider":
                                                            break;
                                                        case "Longitude":
                                                            break;
                                                        case "Latitude":
                                                            break;

                                                    }
                                                }
                                                break;
                                        }
                                    }
                                    #endregion
                                    break;
                            }
                        }
                        #region To get only those satisfy the search conditions
                        if (Convert.ToInt16(result.Rating) >= (int)request.MinRating && Convert.ToInt16(result.Rating) <= (int)request.MaxRating)
                        {
                            if (!(request.HotelName != null && request.HotelName.Length > 0 ? result.HotelName.ToUpper().Contains(request.HotelName.ToUpper()) : true))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        #endregion
                        if (result.SellingFare > 0)
                        {
                            hotelResults.Add(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read Hotel Search Response: " + ex.Message, ex);
            }

            return hotelResults;
        }
        #endregion

        #region Get Hotel Details
        /// <summary>
        /// Gets the Hotel Details for the Hotel.
        /// </summary>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <param name="cityCode">cityCode</param>
        /// <returns>HotelDetails</returns>
        public HotelDetails GetHotelDetails(string hotelCode, string cityCode)
        {
            HotelDetails hotelDetails = new HotelDetails();
            try
            {
                bool dataAvailable = false, imagesAvailable = true, imagesUpdate = false;
                int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.EETConfig["UpdateInterval"]);
                HotelStaticData data = new HotelStaticData();
                data.Load(hotelCode, cityCode, HotelBookingSource.EET);

                if (data.HotelName != null && data.HotelName.Length > 0 && !string.IsNullOrEmpty(data.HotelAddress) && !string.IsNullOrEmpty(data.HotelDescription))
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(data.TimeStamp);
                    //if it is greater than REquired time stamp days then load from DB
                    if (diffRes.Days < timeStampDays)
                    {
                        dataAvailable = true;
                    }
                }

                HotelImages images = new HotelImages();
                images.Load(hotelCode, cityCode, HotelBookingSource.EET);

                if (images.Images != null && images.Images.Length > 0)
                {
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(data.TimeStamp);
                    //if it is greater than REquired time stamp days then load from DB
                    if (diffRes.Days < timeStampDays)
                    {
                        imagesAvailable = true;
                    }
                    else
                    {
                        imagesUpdate = true;
                    }
                }
                if (imagesAvailable)
                {
                    try
                    {
                        hotelDetails.Images = new List<string>();
                        string[] imageData = images.Images.Split('|');
                        hotelDetails.Image = imageData[0];
                        foreach (string img in imageData)
                        {
                            hotelDetails.Images.Add(img);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.EETDetailsSearch, Severity.High, 1, ex.ToString(), "");
                    }
                }

                if (dataAvailable)
                {
                    //read static data from HTL_HOTEL_DATA 
                    try
                    {
                        hotelDetails.Email = data.EMail;
                        hotelDetails.FaxNumber = data.FaxNumber;
                        hotelDetails.Address = data.HotelAddress;
                        hotelDetails.HotelCode = hotelCode;
                        hotelDetails.Description = data.HotelDescription;
                        hotelDetails.HotelFacilities = new List<string>();
                        if (data.HotelFacilities != null && data.HotelFacilities.Length > 0)
                        {
                            foreach (string fac in data.HotelFacilities.Split('|'))
                            {
                                hotelDetails.HotelFacilities.Add(fac);
                            }
                        }

                        hotelDetails.Map = data.HotelMap;
                        hotelDetails.HotelName = data.HotelName;
                        hotelDetails.Image = data.HotelPicture;
                        hotelDetails.HotelPolicy = data.HotelPolicy;
                        hotelDetails.PhoneNumber = data.PhoneNumber;
                        hotelDetails.PinCode = data.PinCode;
                        hotelDetails.hotelRating = data.Rating;

                        hotelDetails.Attractions = new Dictionary<string, string>();

                        if (data.SpecialAttraction != null && data.SpecialAttraction.Length > 0)
                        {
                            foreach (string sa in data.SpecialAttraction.Split('|'))
                            {
                                string key = (sa.Split('#').Length > 0 ? sa.Split('#')[0] : "");
                                string value = (sa.Split('#').Length > 1 ? sa.Split('#')[1] : "");
                                hotelDetails.Attractions.Add(key, value);
                            }
                        }
                        hotelDetails.Map = data.HotelMap;
                        hotelDetails.URL = data.URL;
                        data.CityCode = cityCode;
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.EETDetailsSearch, Severity.High, 1, ex.ToString(), "");
                    }
                }

                if (!dataAvailable || !imagesAvailable)
                {
                    XmlDocument responseXML = GetHotelDescriptiveInfoResponse(hotelCode);
                    if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0)
                    {
                        hotelDetails = ReadHotelDescriptiveInfoResponse(responseXML);
                    }

                    if (hotelDetails.HotelName != null)
                    {
                        HotelStaticData hsd = new HotelStaticData();
                        hsd.EMail = hotelDetails.Email;
                        hsd.FaxNumber = hotelDetails.FaxNumber;
                        hsd.HotelAddress = hotelDetails.Address;
                        hsd.HotelCode = hotelCode;
                        hsd.HotelDescription = hotelDetails.Description;
                        hsd.HotelFacilities = "";
                        if (hotelDetails.HotelFacilities != null)
                        {
                            foreach (string fac in hotelDetails.HotelFacilities)
                            {
                                if (hsd.HotelFacilities.Length > 0)
                                {
                                    hsd.HotelFacilities += "|" + fac;
                                }
                                else
                                {
                                    hsd.HotelFacilities = fac;
                                }
                            }
                        }
                        hsd.HotelLocation = cityCode;
                        hsd.HotelMap = hotelDetails.Map;
                        hsd.HotelName = hotelDetails.HotelName;
                        hsd.HotelPicture = hotelDetails.Image;
                        hsd.HotelPolicy = hotelDetails.HotelPolicy;
                        hsd.PhoneNumber = hotelDetails.PhoneNumber;
                        hsd.PinCode = hotelDetails.PinCode;
                        hsd.Rating = hotelDetails.hotelRating;
                        hsd.Source = HotelBookingSource.EET;
                        hsd.SpecialAttraction = "";

                        if (hotelDetails.Attractions != null)
                        {
                            foreach (KeyValuePair<string, string> pair in hotelDetails.Attractions)
                            {
                                if (hsd.SpecialAttraction.Length > 0)
                                {
                                    hsd.SpecialAttraction += "|" + pair.Key + "#" + pair.Value;
                                }
                                else
                                {
                                    hsd.SpecialAttraction = pair.Key + "#" + pair.Value;
                                }
                            }
                        }
                        hsd.Status = true;
                        hsd.TimeStamp = DateTime.Now;
                        hsd.URL = hotelDetails.URL;
                        hsd.CityCode = cityCode;
                        try
                        {
                            hsd.Save();
                        }
                        catch { }


                        images = new HotelImages();
                        images.CityCode = cityCode;
                        images.DownloadedImgs = "";
                        foreach (string image in hotelDetails.Images)
                        {
                            if (images.DownloadedImgs.Length > 0)
                            {
                                images.DownloadedImgs += "|" + image;
                            }
                            else
                            {
                                images.DownloadedImgs = image;
                            }
                        }
                        images.HotelCode = hotelCode;
                        images.Images = images.DownloadedImgs;
                        images.Source = HotelBookingSource.EET;
                        try
                        {
                            if (images.Images != null && images.Images.Length > 0)
                            {
                                if (!imagesUpdate)
                                {
                                    images.Save();
                                }
                                else
                                {
                                    images.Update();
                                }
                            }
                        }
                        catch { }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get Hotel Details : " + ex.ToString(), ex);
            }
            return hotelDetails;
        }

        /// <summary>
        /// This private function is used for Generates the Hotel Detail request.
        /// </summary>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <returns>string</returns>
        private string GenerateHotelDescriptiveInfoRequest(string hotelCode)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_HotelDescriptiveInfoService", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_HotelDescriptiveInfoRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS

                writer.WriteStartElement("HotelDescriptiveInfos");
                writer.WriteStartElement("HotelDescriptiveInfo");
                writer.WriteAttributeString("HotelCode", hotelCode);
                writer.WriteEndElement();//HotelDescriptiveInfo
                writer.WriteEndElement();//HotelDescriptiveInfos
                writer.WriteEndElement();//OTA_HotelDescriptiveInfoRQ
                writer.WriteEndElement();//OTA_HotelDescriptiveInfoService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "HotelDescInfoRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    //Audit.Add(EventType.EETAvailSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to generate Hotel Descriptive request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// Generates the Response XML for the Request string.
        /// </summary>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <returns>XmlDocument</returns>
        private XmlDocument GetHotelDescriptiveInfoResponse(string hotelCode)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.EETConfig["HotelDescInfoReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelDescriptiveInfoRequest(hotelCode);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return xmlDoc;
        }

        /// <summary>
        /// Reads the Hotel Detail response XML.
        /// </summary>
        /// <param name="doc">Response xml</param>
        /// <returns>HotelDetails</returns>
        private HotelDetails ReadHotelDescriptiveInfoResponse(XmlDocument doc)
        {
            HotelDetails hotelDetails = new HotelDetails();

            try
            {
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "HotelDescInfoResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                }
                catch { }
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");

                XmlNode root = doc.DocumentElement;
                XmlNodeList HotelDescContents = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                if (HotelDescContents[0].Name == "Errors")
                {
                    XmlNode Error = HotelDescContents[0];

                    throw new Exception("EET-Failed to retrieve Hotel Descriptive Info for Hotel :" + Error.FirstChild.Attributes["ShortText"].Value);
                }
                else
                {
                    hotelDetails.HotelCode = HotelDescContents[0].Attributes["HotelCode"].Value;
                    hotelDetails.HotelName = HotelDescContents[0].Attributes["HotelName"].Value;
                    XmlNode DescContent = HotelDescContents[0].FirstChild;
                    foreach (XmlNode node in DescContent.ChildNodes)
                    {

                        switch (node.Name)
                        {
                            case "HotelInfo":
                                #region HotelInfo
                                XmlNode HotelInfo = node;
                                XmlNode HotelCategory = HotelInfo.FirstChild.FirstChild;
                                if (HotelCategory.Attributes["CodeDetail"] != null)
                                {
                                    //Rating
                                    switch (HotelCategory.Attributes["CodeDetail"].Value)
                                    {
                                        case "1":
                                            hotelDetails.hotelRating = HotelRating.OneStar;
                                            break;
                                        case "2":
                                            hotelDetails.hotelRating = HotelRating.TwoStar;
                                            break;
                                        case "3":
                                            hotelDetails.hotelRating = HotelRating.ThreeStar;
                                            break;
                                        case "4":
                                            hotelDetails.hotelRating = HotelRating.FourStar;
                                            break;
                                        case "5":
                                            hotelDetails.hotelRating = HotelRating.FiveStar;
                                            break;
                                        case "0":
                                            hotelDetails.hotelRating = HotelRating.All;
                                            break;
                                    }
                                }

                                //HotelImages
                                XmlNodeList MultimediaDescs = HotelInfo["Descriptions"].FirstChild.ChildNodes;

                                foreach (XmlNode multiDesc in MultimediaDescs)
                                {
                                    switch (multiDesc.FirstChild.Name)
                                    {
                                        case "ImageItems":
                                            hotelDetails.Images = new List<string>();
                                            foreach (XmlNode image in multiDesc.FirstChild.ChildNodes)
                                            {
                                                hotelDetails.Images.Add(image.FirstChild.Attributes["FileName"].Value);
                                            }
                                            break;
                                        case "TextItems":
                                            hotelDetails.RoomData = new Room[1];
                                            foreach (XmlNode textItem in multiDesc.FirstChild.ChildNodes)
                                            {
                                                switch (textItem.Attributes[0].Value)
                                                {
                                                    case "LongDescription":
                                                        hotelDetails.Description = textItem.FirstChild.InnerText;
                                                        break;
                                                    case "RoomDescription":
                                                        hotelDetails.RoomData[0].Description = textItem.FirstChild.InnerText;
                                                        break;
                                                    case "ZoneDescription":
                                                        hotelDetails.Description += " " + textItem.FirstChild.InnerText;
                                                        break;
                                                }
                                            }
                                            break;
                                    }
                                }

                                XmlNode Position = HotelInfo["Position"];

                                hotelDetails.Map = Position.Attributes["Latitude"].Value;
                                hotelDetails.Map += "," + Position.Attributes["Longitude"].Value;

                                XmlNodeList Services = HotelInfo["Services"].ChildNodes;

                                hotelDetails.HotelFacilities = new List<string>();
                                hotelDetails.RoomFacilities = new List<string>();

                                foreach (XmlNode Service in Services)
                                {
                                    switch (Service.Attributes["Sort"].Value)
                                    {
                                        case "Hotel":
                                        case "Other":
                                            hotelDetails.HotelFacilities.Add(Service.Attributes["CodeDetail"].Value);
                                            break;
                                        case "Room":
                                            hotelDetails.RoomFacilities.Add(Service.Attributes["CodeDetail"].Value);
                                            break;
                                    }
                                }

                                #endregion
                                break;
                            case "AreaInfo":
                                hotelDetails.Attractions = new Dictionary<string, string>();
                                foreach (XmlNode refPoint in node.FirstChild.ChildNodes)
                                {
                                    hotelDetails.Attractions.Add(refPoint.Attributes["Name"].Value, refPoint.Attributes["Distance"].Value);
                                }
                                break;
                            case "Policies":
                                XmlNodeList policies = node.ChildNodes;
                                foreach (XmlNode penalty in policies)
                                {
                                    hotelDetails.HotelPolicy += "|" + penalty.FirstChild.FirstChild.FirstChild.Attributes["Name"].Value;
                                }
                                break;
                            case "ContactInfos":
                                foreach (XmlNode contactInfo in node.FirstChild.ChildNodes)
                                {
                                    switch (contactInfo.Name)
                                    {
                                        case "Addresses":
                                            foreach (XmlNode address in contactInfo.FirstChild.ChildNodes)
                                            {
                                                switch (address.Name)
                                                {
                                                    case "AddressLine":
                                                        hotelDetails.Address = address.InnerText;
                                                        break;
                                                    case "CityName":
                                                        break;
                                                    case "PostalCode":
                                                        break;
                                                    case "County":
                                                        break;
                                                    case "CountryName":
                                                        break;
                                                }
                                            }
                                            break;
                                        case "Phones":
                                            foreach (XmlNode phone in contactInfo.ChildNodes)
                                            {
                                                if (hotelDetails.PhoneNumber != null && hotelDetails.PhoneNumber.Length > 0)
                                                {
                                                    hotelDetails.PhoneNumber += ", " + phone.Attributes["ID"].Value;
                                                }
                                                else
                                                {
                                                    hotelDetails.PhoneNumber = phone.Attributes["ID"].Value;
                                                }
                                            }
                                            break;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to read Hotel Descriptive response :" + ex.Message, ex);
            }

            return hotelDetails;
        }
        #endregion

        #region Get Hotel Cancellation policy
        /// <summary>
        /// Returns the Cancellation policy for the room of a Hotel.
        /// </summary>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <param name="ratePlanCode">Selected RoomId</param>
        /// <param name="startDate">CheckInDate</param>
        /// <param name="endDate">CheckOutDate</param>
        /// <param name="sequenceNumber">sequenceNo(what they Given from HotelSearch Response SequenceNmbr)</param>
        /// <param name="supplierPrice">supplierPrice(what they Given from HotelSearch Response TotalPrice)</param>
        /// <param name="PassengerNationality">Selected Nationality</param>
        /// <returns>Dictionary Object</returns>
        public Dictionary<string, string> GetCancellationDetails(string hotelCode, string ratePlanCode, DateTime startDate, DateTime endDate, string sequenceNumber, decimal supplierPrice, string PassengerNationality)
        {
            Dictionary<string, string> cancellationPolicy = new Dictionary<string, string>();

            try
            {
                XmlDocument responseXML = GenerateHotelBookingRuleResponse(hotelCode, ratePlanCode, startDate, endDate, sequenceNumber, PassengerNationality);
                if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0)
                {
                    cancellationPolicy = ReadCancellationPolicyResponse(responseXML, startDate, supplierPrice);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get Cancellation policy : " + ex.Message, ex);
            }

            return cancellationPolicy;
        }

        /// <summary>
        /// Generates Hotel Booking Rule request.
        /// </summary>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <param name="ratePlanCode">Selected RoomId</param>
        /// <param name="startDate">CheckInDate</param>
        /// <param name="endDate">CheckOutDate</param>
        /// <param name="sequenceNumber">sequenceNo(what they Given from HotelSearch Response SequenceNmbr)</param>
        /// <param name="passengerNationality">Selected Nationality</param>
        /// <returns>string</returns>
        private string GenerateHotelBookingRuleRequest(string hotelCode, string ratePlanCode, DateTime startDate, DateTime endDate, string sequenceNumber, string passengerNationality)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_HotelBookingRuleService", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_HotelBookingRuleRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteAttributeString("SequenceNmbr", sequenceNumber);
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS

                writer.WriteStartElement("RuleMessage");
                writer.WriteAttributeString("HotelCode", hotelCode);
                writer.WriteStartElement("StatusApplication");
                writer.WriteAttributeString("RatePlanCode", ratePlanCode);
                writer.WriteAttributeString("Start", startDate.ToString("yyyy-MM-dd"));
                writer.WriteAttributeString("End", endDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();//StatusApplication
                //cancelation policy is coming based on the nationality   Added by brahmam  31.03.2016
                writer.WriteStartElement("TPA_Extensions");
                writer.WriteElementString("PaxCountry", passengerNationality);
                writer.WriteElementString("SearchByPaymentType", "ExcludePaymentInDestination");
                writer.WriteEndElement();//TPA_Extensions

                writer.WriteEndElement();//RuleMessage
                writer.WriteEndElement();//OTA_HotelBookingRuleRQ
                writer.WriteEndElement();//OTA_HotelBookingRuleService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "HotelBookingRuleRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to generate Hotel Descriptive request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// Generates response xml from the given request string.
        /// </summary>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <param name="ratePlanCode">Selected RoomId</param>
        /// <param name="startDate">CheckInDate</param>
        /// <param name="endDate">CheckOutDate</param>
        /// <param name="sequenceNumber">sequenceNo(what they Given from HotelSearch Response SequenceNmbr)</param>
        /// <param name="PassengerNationality">Selected Nationality</param>
        /// <returns>XmlDocument</returns>
        private XmlDocument GenerateHotelBookingRuleResponse(string hotelCode, string ratePlanCode, DateTime startDate, DateTime endDate, string sequenceNumber, string PassengerNationality)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.EETConfig["HotelBookingRuleReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelBookingRuleRequest(hotelCode, ratePlanCode, startDate, endDate, sequenceNumber, PassengerNationality);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get response from Hotel Booking Rule Request: " + ex.ToString(), ex);
            }

            return xmlDoc;
        }

        /// <summary>
        /// ReadCancellationPolicyResponse
        /// </summary>
        /// <param name="doc">XmlResponse Object</param>
        /// <param name="startDate">CheckinDate</param>
        /// <param name="supplierPrice">supplierPrice(what they Given from HotelSearch Response TotalPrice)</param>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> ReadCancellationPolicyResponse(XmlDocument doc, DateTime startDate, decimal supplierPrice)
        {
            Dictionary<string, string> CancellationPolicy = new Dictionary<string, string>();

            try
            {
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "HotelBookingRuleResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");

                XmlNode root = doc.DocumentElement;
                XmlNodeList RuleMessage = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                if (RuleMessage[0].Name == "Errors")
                {
                    XmlNode Error = RuleMessage[0].FirstChild;

                    throw new Exception("EET-Failed to retrieve Hotel Descriptive Info for Hotel :" + Error.Attributes["ShortText"].Value);
                }
                else
                {
                    string cancelPolicy = string.Empty, cancelRule = string.Empty;
                    foreach (XmlNode node in RuleMessage)
                    {
                        switch (node.Name)
                        {
                            case "Warnings"://We didnt come across this node in the test so we never tested this condition.
                                CancellationPolicy.Add(node.FirstChild.Attributes["Code"].Value, node.FirstChild.Attributes["ShortText"].Value);
                                break;
                            case "RuleMessage":
                                foreach (XmlNode rules in node.ChildNodes)
                                {
                                    switch (rules.Name)
                                    {
                                        case "StatusApplication":
                                            CancellationPolicy.Add("RatePlanCode", rules.Attributes["RatePlanCode"].Value);
                                            break;
                                        case "BookingRules":
                                            foreach (XmlNode rule in rules.ChildNodes)
                                            {
                                                switch (rule.Name)
                                                {
                                                    case "BookingRule":
                                                        int buffer = Convert.ToInt32(ConfigurationSystem.EETConfig["CancellationBuffer"]);
                                                        DateTime lastCanDate = Convert.ToDateTime(rule.Attributes["AbsoluteCutoff"].Value);
                                                        CancellationPolicy.Add("lastCancellationDate", Convert.ToDateTime(rule.Attributes["AbsoluteCutoff"].Value).AddDays(-buffer).ToString());
                                                        foreach (XmlNode brule in rule.ChildNodes)
                                                        {
                                                            switch (brule.Name)
                                                            {
                                                                case "CancelPenalties":

                                                                    //cancelPolicy = brule.FirstChild.FirstChild.FirstChild.InnerText.Trim();
                                                                    //decimal cancelAmount = 0; string cancelValue = string.Empty;
                                                                    //cancelPolicy = cancelPolicy.TrimStart(new char[] { '*' });
                                                                    //cancelPolicy = cancelPolicy.TrimEnd(new char[] { '$' });
                                                                    //cancelPolicy = cancelPolicy.Replace("$ *", "|");
                                                                    //if (cancelPolicy.Contains(": $"))
                                                                    //{
                                                                    //    cancelValue = cancelPolicy.Substring(cancelPolicy.IndexOf(": $") + 4);
                                                                    //    cancelAmount = Math.Round(Convert.ToDecimal(cancelValue.Replace(",", ".")) * rateOfExchange["USD"], 2);
                                                                    //    cancelPolicy = cancelPolicy.Replace(cancelValue, " " + cancelAmount.ToString());
                                                                    //}
                                                                    //cancelPolicy = cancelPolicy.Replace("$", "AED");
                                                                    break;
                                                                case "Description":
                                                                    CancellationPolicy.Add("HotelPolicy", System.Web.HttpUtility.HtmlDecode(brule.FirstChild.InnerText.Replace(".", "|")));
                                                                    break;
                                                                case "TPA_Extensions":
                                                                    foreach (XmlNode ext in brule.ChildNodes)
                                                                    {
                                                                        switch (ext.Name)
                                                                        {
                                                                            case "CancellationPolicyRules":
                                                                                string type = string.Empty, dateFrom = string.Empty, dateTo = string.Empty;
                                                                                int nights = 0, days = 0;
                                                                                decimal price = 0, percentage = 0;
                                                                                foreach (XmlNode crule in ext.ChildNodes)
                                                                                {
                                                                                    dateFrom = string.Empty; dateTo = string.Empty;
                                                                                    foreach (XmlAttribute attrib in crule.Attributes)
                                                                                    {
                                                                                        switch (attrib.Name)
                                                                                        {
                                                                                            case "DateFrom":
                                                                                                dateFrom = attrib.Value;
                                                                                                //cancelRule = "Cancelling from " + Convert.ToDateTime(attrib.Value).ToString("dd/MMM/yyyy");
                                                                                                break;
                                                                                            case "DateTo":
                                                                                                dateTo = attrib.Value;
                                                                                                //cancelRule += " to " + Convert.ToDateTime(attrib.Value).ToString("dd/MMM/yyyy") + " you will have ";
                                                                                                break;
                                                                                            case "Type":
                                                                                                type = attrib.Value;
                                                                                                //switch (type)
                                                                                                //{
                                                                                                //    case "V":                                                                                                        
                                                                                                //        break;
                                                                                                //    case "R":
                                                                                                //        cancelRule = "Cancelling from " + attrib.Value + " days after confirmation you will have ";
                                                                                                //        break;
                                                                                                //    case "S":
                                                                                                //        cancelRule = "Cancelling No Show : ";                                                                                                        
                                                                                                //        break;
                                                                                                //}
                                                                                                break;
                                                                                            case "From":
                                                                                                days = Convert.ToInt32(attrib.Value);
                                                                                                //if (Convert.ToInt32(attrib.Value) > 0)
                                                                                                //{
                                                                                                //    switch (type)
                                                                                                //    {
                                                                                                //        case "V":
                                                                                                //            cancelRule = "Cancelling before " + attrib.Value + " days you will have ";
                                                                                                //            break;
                                                                                                //        case "R":
                                                                                                //            cancelRule = "Cancelling from " + attrib.Value + " days after confirmation you will have ";
                                                                                                //            break;
                                                                                                //        case "S":
                                                                                                //            cancelRule = "No Show incurs into ";
                                                                                                //            break;
                                                                                                //    }
                                                                                                //}
                                                                                                //else
                                                                                                //{
                                                                                                //    switch (type)
                                                                                                //    {
                                                                                                //        case "V":
                                                                                                //            cancelRule = "Cancelling from " + Convert.ToDateTime(dateFrom).ToString("dd/MMM/yyyy") + " to " + Convert.ToDateTime(dateTo).ToString("dd/MMM/yyyy") + " you will have ";
                                                                                                //            break;
                                                                                                //        case "R":
                                                                                                //            cancelRule = "Cancelling from " + attrib.Value + " days after confirmation you will have ";
                                                                                                //            break;
                                                                                                //        case "S":
                                                                                                //            cancelRule = "Cancelling No Show " + nights + "nights";
                                                                                                //            break;
                                                                                                //    }
                                                                                                //}
                                                                                                break;
                                                                                            case "To":
                                                                                                break;
                                                                                            case "StayLengthFrom":
                                                                                                break;
                                                                                            case "StayLengthTo":
                                                                                                break;
                                                                                            case "FixedPrice":
                                                                                                price = Math.Round(Convert.ToDecimal(attrib.Value) * (exchangeRates.ContainsKey("USD") ? exchangeRates["USD"] : 1), decimalPoint);
                                                                                                //cancelRule += "AED " + Math.Round(Convert.ToDecimal(attrib.Value) * rateOfExchange["USD"], 2) + " Penalty";
                                                                                                break;
                                                                                            case "PercentPrice":
                                                                                                percentage = Convert.ToDecimal(attrib.Value);
                                                                                                if (Convert.ToDecimal(attrib.Value) > 0)
                                                                                                {
                                                                                                    //cancelRule += attrib.Value + "% Penalty";
                                                                                                }
                                                                                                break;
                                                                                            case "Nights":
                                                                                                nights = Convert.ToInt32(attrib.Value);

                                                                                                break;
                                                                                            case "ApplicationTypeNights":
                                                                                                if (attrib.Value.Length > 0)
                                                                                                {
                                                                                                    //cancelRule += attrib.Value + " of the penalty";
                                                                                                }
                                                                                                break;
                                                                                        }
                                                                                    }
                                                                                    if (dateFrom.Length > 0)
                                                                                    {
                                                                                        try
                                                                                        {
                                                                                            dateFrom = Convert.ToDateTime(dateFrom).ToString("dd/MMM/yyyy hh:mm:ss");
                                                                                            //if (lastCanDate.ToString("dd/MMM/yyyy hh:mm:ss") == dateFrom)
                                                                                            {
                                                                                                dateFrom = Convert.ToDateTime(dateFrom).AddDays(-buffer).ToString("dd/MMM/yyyy hh:mm:ss");
                                                                                            }
                                                                                        }
                                                                                        catch { }
                                                                                    }
                                                                                    if (dateTo.Length > 0)
                                                                                    {
                                                                                        try
                                                                                        {
                                                                                            dateTo = Convert.ToDateTime(dateTo).AddDays(-buffer).ToString("dd/MMM/yyyy hh:mm:ss");

                                                                                            ////if (lastCanDate.ToString("dd/MMM/yyyy hh:mm:ss") == dateTo)
                                                                                            //{
                                                                                            //    dateTo = Convert.ToDateTime(dateTo).AddDays(-buffer).ToString("dd/MMM/yyyy hh:mm:ss");
                                                                                            //}
                                                                                        }
                                                                                        catch { }
                                                                                    }

                                                                                    if (nights > 0 && price == 0)
                                                                                    {
                                                                                        if (type != "S")
                                                                                        {
                                                                                            cancelRule += "Cancelling after " + dateFrom + " charges applicable for " + nights + " nights";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            cancelRule += "Cancelling No Show: " + nights + " nights";
                                                                                        }
                                                                                    }
                                                                                    else if (nights <= 0 && price > 0 && dateTo.Length == 0)
                                                                                    {
                                                                                        cancelRule += "Cancelling after " + dateFrom + " charges applicable " + agentCurrency + " " + price;
                                                                                    }
                                                                                    else if (nights <= 0 && price > 0 && dateTo.Length > 0)
                                                                                    {
                                                                                        if (dateTo.Length > 0 && startDate.Subtract(Convert.ToDateTime(dateTo).AddDays(buffer)).TotalDays <= 0)
                                                                                        {
                                                                                            cancelRule += "Cancelling after " + dateFrom + " " + agentCurrency + " " + price;
                                                                                        }
                                                                                        else if (dateTo.Length > 0 && startDate.Subtract(Convert.ToDateTime(dateTo).AddDays(buffer)).TotalDays > 0)
                                                                                        {
                                                                                            cancelRule += "Cancelling from " + dateFrom + " to " + dateTo + " " + agentCurrency + " " + price;
                                                                                        }
                                                                                    }
                                                                                    else if (nights <= 0)
                                                                                    {
                                                                                        if (percentage <= 0)
                                                                                        {
                                                                                            if (price <= 0)
                                                                                            {
                                                                                                if (type != "S")
                                                                                                {
                                                                                                    if (dateTo.Length > 0 && startDate.Subtract(Convert.ToDateTime(dateTo).AddDays(buffer)).TotalDays <= 0)
                                                                                                    {
                                                                                                        cancelRule += "Cancelling after " + dateFrom + " " + agentCurrency + " " + price;
                                                                                                    }
                                                                                                    else if (dateTo.Length > 0 && startDate.Subtract(Convert.ToDateTime(dateTo).AddDays(buffer)).TotalDays > 0)
                                                                                                    {
                                                                                                        cancelRule += "Cancelling from " + dateFrom + " to " + dateTo + " " + agentCurrency + " " + price;
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (type != "S")
                                                                                                {
                                                                                                    if (dateTo.Length > 0 && startDate.Subtract(Convert.ToDateTime(dateTo).AddDays(buffer)).TotalDays <= 0)
                                                                                                    {
                                                                                                        cancelRule += "Cancelling after " + dateFrom + " " + agentCurrency + " " + price;
                                                                                                    }
                                                                                                    else if (dateTo.Length > 0 && startDate.Subtract(Convert.ToDateTime(dateTo).AddDays(buffer)).TotalDays > 0)
                                                                                                    {
                                                                                                        cancelRule += "Cancelling from " + dateFrom + " to " + dateTo + " " + agentCurrency + " " + price;
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    cancelRule = "Cancelling No Show : " + agentCurrency + " " + price;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (type != "S")
                                                                                            {
                                                                                                if (dateTo.Length > 0 && startDate.Subtract(Convert.ToDateTime(dateTo).AddDays(buffer)).TotalDays > 0)
                                                                                                {
                                                                                                    cancelRule += "Cancelling from " + dateFrom + " to " + dateTo + " " + percentage + " %";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    cancelRule += "Cancelling from " + dateFrom + " " + percentage + " %";
                                                                                                }

                                                                                                if (cancelPolicy.Contains("non-refundable"))
                                                                                                {
                                                                                                    cancelRule += ", This rate is non-refundable";
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                cancelRule += "Cancelling No Show : " + percentage + " %";
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    cancelRule += "|";
                                                                                }
                                                                                cancelPolicy += cancelRule.TrimEnd('|');
                                                                                break;
                                                                            case "TotalPrice":
                                                                                //Earlier we used to compare search result price in the page, 
                                                                                //now passing supplier price i.e. USD price and comparing here
                                                                                if (Convert.ToDecimal(ext.InnerText) - supplierPrice > 1)
                                                                                {
                                                                                    if (!CancellationPolicy.ContainsKey("PRICE_CHANGED"))
                                                                                    {
                                                                                        CancellationPolicy.Add("PRICE_CHANGED", ext.InnerText);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        CancellationPolicy["PRICE_CHANGED"] = ext.InnerText;
                                                                                    }
                                                                                }
                                                                                break;
                                                                            case "RequirePaxName":
                                                                                CancellationPolicy.Add("RequirePaxName", "true");
                                                                                break;
                                                                            case "RequirePaxSurName":
                                                                                CancellationPolicy.Add("RequirePaxSurName", "true");
                                                                                break;
                                                                            case "RequireFullAddress":
                                                                                CancellationPolicy.Add("RequireFullAddress", "true");
                                                                                break;
                                                                            case "RequirePostalCode":
                                                                                CancellationPolicy.Add("RequirePostalCode", "true");
                                                                                break;
                                                                            case "RequireCityCode":
                                                                                CancellationPolicy.Add("RequireCityCode", "true");
                                                                                break;
                                                                            case "RequireCountryCode":
                                                                                CancellationPolicy.Add("RequireCountryCode", "true");
                                                                                break;
                                                                            case "RequireTelephoneHolder":
                                                                                CancellationPolicy.Add("RequireTelephoneHolder", "true");
                                                                                break;
                                                                        }
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                }
                                            }
                                            break;
                                    }
                                }
                                break;

                        }
                    }

                    //if (cancelRule.Length > 0)
                    //{
                    //    CancellationPolicy.Add("CancelPolicy", cancelRule);
                    //}
                    //else
                    {
                        CancellationPolicy.Add("CancelPolicy", cancelPolicy);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to Read the Hotel Booking Rule Response : " + ex.Message, ex);
            }

            return CancellationPolicy;
        }
        #endregion

        #region Book Hotel Reservation

        /// <summary>
        /// Used to Book a Hotel using HotelItinerary.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        public BookingResponse BookHotel(ref HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();

            try
            {
                XmlDocument responseXML = GenerateHotelBookingResponse(itinerary);
                if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0)
                {
                    bookResponse = ReadHotelBookingResponse(responseXML, ref itinerary);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to Book Hotel : " + ex.Message, ex);
            }

            return bookResponse;
        }

        /// <summary>
        /// Generates Hotel Booking Request XML.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateHotelBookingRequest(HotelItinerary itinerary)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_HotelResV2Service", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_HotelResRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteAttributeString("SequenceNmbr", itinerary.SequenceNumber);
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS
                writer.WriteStartElement("HotelReservations");
                writer.WriteStartElement("HotelReservation");
                writer.WriteStartElement("UniqueID");
                writer.WriteAttributeString("ID_Context", string.Empty);//Optional Reservation ID
                writer.WriteEndElement();//UniqueID
                writer.WriteStartElement("RoomStays");
                writer.WriteStartElement("RoomStay");
                writer.WriteStartElement("RoomTypes");

                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    writer.WriteStartElement("RoomType");
                    writer.WriteStartElement("TPA_Extensions");
                    writer.WriteStartElement("Guests");
                    foreach (HotelPassenger pax in room.PassenegerInfo)
                    {
                        writer.WriteStartElement("Guest");
                        writer.WriteAttributeString("Name", pax.Firstname);
                        writer.WriteAttributeString("Surname", pax.Lastname);
                        if (pax.PaxType == HotelPaxType.Child)
                        {
                            writer.WriteAttributeString("Age", pax.Age.ToString());
                        }
                        writer.WriteEndElement();//Guest
                    }
                    writer.WriteEndElement();//Guests
                    writer.WriteEndElement();//TPA_Extensions
                    writer.WriteEndElement();//RoomType                   
                }
                writer.WriteEndElement();//RoomTypes
                writer.WriteStartElement("RatePlans");
                //foreach (HotelRoom room in itinerary.Roomtype)
                {
                    writer.WriteStartElement("RatePlan");
                    writer.WriteAttributeString("RatePlanCode", itinerary.Roomtype[0].RatePlanCode);
                    writer.WriteEndElement();//RatePlan                   
                }
                writer.WriteEndElement();//RatePlans

                writer.WriteStartElement("TimeSpan");
                writer.WriteAttributeString("Start", itinerary.StartDate.ToString("yyyy-MM-dd"));
                writer.WriteAttributeString("End", itinerary.EndDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();//TimeSpan
                writer.WriteStartElement("Total");
                writer.WriteAttributeString("AmountAfterTax", itinerary.TotalPrice.ToString("0.00"));
                writer.WriteAttributeString("CurrencyCode", "USD");
                writer.WriteEndElement();//Total
                writer.WriteStartElement("BasicPropertyInfo");
                writer.WriteAttributeString("HotelCode", itinerary.HotelCode);
                writer.WriteEndElement();//BasicPropertyInfo
                writer.WriteStartElement("TPA_Extensions");
                writer.WriteStartElement("ExpectedPriceRange");
                writer.WriteAttributeString("min", "0");
                //writer.WriteAttributeString("max", (itinerary.TotalPrice).ToString("0.00"));
                writer.WriteEndElement();//ExpectedPriceRange
                writer.WriteEndElement();//TPA_Extensions
                writer.WriteStartElement("Comments");
                writer.WriteStartElement("Comment");
                writer.WriteElementString("Text", string.Empty);
                writer.WriteEndElement();//Comment
                writer.WriteEndElement();//Comments
                writer.WriteStartElement("Reference");
                writer.WriteEndElement();//Reference
                writer.WriteEndElement();//RoomStay
                writer.WriteEndElement();//RoomStays               
                writer.WriteStartElement("ResGuests");//Lead Pax Information
                writer.WriteStartElement("ResGuest");
                writer.WriteStartElement("Profiles");
                writer.WriteStartElement("ProfileInfo");
                writer.WriteStartElement("Profile");
                writer.WriteAttributeString("ProfileType", "1");
                writer.WriteStartElement("Customer");
                writer.WriteStartElement("PersonName");
                writer.WriteElementString("GivenName", itinerary.HotelPassenger.Firstname);
                writer.WriteElementString("Surname", itinerary.HotelPassenger.Lastname);
                writer.WriteEndElement();//PersonName
                writer.WriteStartElement("Telephone");
                writer.WriteAttributeString("PhoneNumber", itinerary.HotelPassenger.Phoneno);
                writer.WriteEndElement();//Telephone
                writer.WriteElementString("Email", itinerary.HotelPassenger.Email);
                writer.WriteStartElement("Address");
                if (itinerary.HotelPassenger.Addressline1.Length > 0)
                {
                    writer.WriteElementString("AddressLine", itinerary.HotelPassenger.Addressline1);
                }
                if (itinerary.HotelPassenger.Zipcode.Length > 0)
                {
                    writer.WriteElementString("PostalCode", itinerary.HotelPassenger.Zipcode);
                }
                if (itinerary.HotelPassenger.City.Length > 0)
                {
                    writer.WriteElementString("CityName", itinerary.HotelPassenger.City);
                }

                writer.WriteStartElement("CountryName");
                writer.WriteAttributeString("Code", itinerary.HotelPassenger.NationalityCode);
                writer.WriteString(itinerary.HotelPassenger.Nationality);
                writer.WriteEndElement();//Country
                writer.WriteEndElement();//Address
                writer.WriteEndElement();//Customer
                writer.WriteEndElement();//Profile
                writer.WriteEndElement();//ProfileInfo
                writer.WriteEndElement();//Profiles
                writer.WriteEndElement();//ResGuest
                writer.WriteEndElement();//ResGuests
                writer.WriteStartElement("TPA_Extensions");
                writer.WriteElementString("PaxCountry", itinerary.HotelPassenger.NationalityCode);
                writer.WriteEndElement();//TPA_Extensions
                writer.WriteEndElement();//HotelReservation
                writer.WriteEndElement();//HotelReservations
                writer.WriteEndElement();//OTA_HotelResRQ
                writer.WriteEndElement();//OTA_HotelResV2Service
                writer.WriteEndElement();//soap:Body
                writer.WriteEndElement();//soap:Envelope

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "HotelBookingRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to generate Hotel Booking Request : " + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// Generates Hotel Booking Response XML.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>XmlDocument</returns>
        private XmlDocument GenerateHotelBookingResponse(HotelItinerary itinerary)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.EETConfig["HotelReservationReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelBookingRequest(itinerary);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                //// handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get response from Hotel Search request : " + ex.Message, ex);
            }
            return xmlDoc;
        }

        /// <summary>
        /// ReadHotelBookingResponse
        /// </summary>
        /// <param name="doc">Booking Response XMl</param>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        private BookingResponse ReadHotelBookingResponse(XmlDocument doc, ref HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "HotelBookingResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";

                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                //nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
                //nsmgr.AddNamespace(string.Empty, "");

                XmlNode root = doc.DocumentElement;
                XmlNodeList HotelRes = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                if (HotelRes[0].Name == "Errors")
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                    bookResponse.Error = HotelRes[0].FirstChild.Attributes["ShortText"].Value;
                }
                else
                {
                    foreach (XmlNode node in HotelRes)
                    {
                        switch (node.Name)
                        {
                            case "Success":
                                break;
                            case "HotelReservations":
                                foreach (XmlNode resNode in node.ChildNodes)
                                {
                                    switch (resNode.Name)
                                    {
                                        case "HotelReservation":
                                            switch (resNode.Attributes["ResStatus"].Value)
                                            {
                                                case "Pag":
                                                case "Ok":
                                                    bookResponse.Status = BookingResponseStatus.Successful;//Confirmed and Paid
                                                    itinerary.Status = HotelBookingStatus.Confirmed;
                                                    break;
                                                case "Con":
                                                case "Con*":
                                                    bookResponse.Status = BookingResponseStatus.Successful;//Confirmed
                                                    itinerary.Status = HotelBookingStatus.Confirmed;
                                                    break;
                                                case "Tar"://Credit Card Reservation
                                                    break;
                                                case "Can":
                                                case "CanC":
                                                    bookResponse.Status = BookingResponseStatus.Failed;
                                                    itinerary.Status = HotelBookingStatus.Failed;
                                                    bookResponse.Error = "Cancelled From Supplier";
                                                    bookResponse.SSRMessage = "Cancelled From Supplier";
                                                    break;
                                                case "PRe":
                                                case "PDi":
                                                    bookResponse.Status = BookingResponseStatus.BookedOther;//OnRequest Reservation
                                                    bookResponse.Error = "Not Confirmed.Reservation ON request";
                                                    bookResponse.SSRMessage = "Not Confirmed.Reservation ON request";
                                                    break;
                                                case "Ini":
                                                    bookResponse.Status = BookingResponseStatus.Failed;//Initialized but not confirmed
                                                    itinerary.Status = HotelBookingStatus.Failed;
                                                    bookResponse.Error = "Initialized but not confirmed From supplier";
                                                    bookResponse.SSRMessage = "Initialized but not confirmed From supplier";
                                                    break;
                                            }
                                            if (resNode.FirstChild.Attributes["ID"] != null)
                                            {
                                                bookResponse.ConfirmationNo = resNode.FirstChild.Attributes["ID"].Value;
                                                itinerary.ConfirmationNo = bookResponse.ConfirmationNo;
                                            }
                                            foreach (XmlNode childs in resNode.ChildNodes)
                                            {
                                                switch (childs.Name)
                                                {
                                                    case "RoomStays":
                                                        foreach (XmlNode roomStay in childs.ChildNodes)
                                                        {
                                                            foreach (XmlNode rsChilds in roomStay.FirstChild.ChildNodes)
                                                            {
                                                                switch (rsChilds.Name)
                                                                {
                                                                    case "BasicPropertyInfo":
                                                                        foreach (XmlNode bpChilds in rsChilds.ChildNodes)
                                                                        {
                                                                            switch (bpChilds.Name)
                                                                            {
                                                                                case "VendorMessages":
                                                                                    if (bpChilds.FirstChild.Attributes["Title"] != null)
                                                                                    {
                                                                                        itinerary.HotelPolicyDetails = "<b>" + bpChilds.FirstChild.Attributes["Title"].Value + "</b>";
                                                                                    }
                                                                                    itinerary.HotelPolicyDetails += System.Web.HttpUtility.HtmlDecode(bpChilds.FirstChild.FirstChild.FirstChild.FirstChild.InnerText);
                                                                                    break;
                                                                            }
                                                                        }
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    case "ResGuests":
                                                        break;
                                                    case "ResGlobalInfo":
                                                        foreach (XmlNode rgiNode in childs.ChildNodes)
                                                        {
                                                            switch (rgiNode.Name)
                                                            {
                                                                case "Total":
                                                                    break;
                                                                case "HotelReservationIDs":
                                                                    //bookResponse.ConfirmationNo += "|" + rgiNode.FirstChild.Attributes["ResID_Source"].Value;
                                                                    if (rgiNode.FirstChild.Attributes["ResID_Source"] != null)
                                                                    {
                                                                        itinerary.BookingRefNo += rgiNode.FirstChild.Attributes["ResID_Source"].Value;
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                    case "TPA_Extensions":
                                                        foreach (XmlNode tpaNode in childs.ChildNodes)
                                                        {
                                                            switch (tpaNode.Name)
                                                            {
                                                                case "ElementStatus":
                                                                    break;
                                                                case "CancellationPolicy":
                                                                    break;
                                                                case "ExternalReference":
                                                                    break;
                                                                case "Payable":
                                                                    itinerary.PaymentGuaranteedBy = "Payment Guaranteed by : " + System.Web.HttpUtility.HtmlDecode(tpaNode.InnerText);
                                                                    break;
                                                                case "FirstDateCancCost":
                                                                    break;
                                                                case "Delegation"://Point of Contact for Hotel
                                                                    string POC = string.Empty;
                                                                    foreach (XmlNode delNode in tpaNode.ChildNodes)
                                                                    {
                                                                        if (POC.Length > 0)
                                                                        {
                                                                            POC += delNode.Name + ":" + delNode.InnerText;
                                                                        }
                                                                        else
                                                                        {
                                                                            POC = delNode.Name + ":" + delNode.InnerText;
                                                                        }
                                                                    }
                                                                    itinerary.SpecialRequest = POC;
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                }
                                            }
                                            break;

                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to read Hotel Booking response : " + ex.Message, ex);
            }

            return bookResponse;
        }
        #endregion

        #region Hotel Cancellation

        /// <summary>
        /// Used to Cancel a Hotel using confirmationNumber.
        /// </summary>
        /// <param name="confirmationNumber">confirmationNumber</param>
        /// <returns>Dictionary</returns>
        public Dictionary<string, string> CancelHotel(string confirmationNumber)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            try
            {
                XmlDocument responseXML = GenerateHotelCancellationResponse(confirmationNumber);
                if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0)
                {
                    cancellationCharges = ReadHotelCancellationResponse(responseXML);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to Cancel Hotel : " + ex.Message, ex);
            }

            return cancellationCharges;
        }

        /// <summary>
        /// Generates Hotel Cancel Request XML.
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <returns>string</returns>
        private string GenerateHotelCancellationRequest(string confirmationNo)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_CancelService", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_CancelRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS


                writer.WriteStartElement("UniqueID");
                writer.WriteAttributeString("ID", confirmationNo);
                writer.WriteEndElement();//UniqueID
                writer.WriteEndElement();//OTA_CancelRQ
                writer.WriteEndElement();//OTA_CancellationService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath =XmlPath + "HotelCancelRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to generate Hotel Cancellation request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// Generates Hotel Cancel Response XML.
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <returns>XmlDocument</returns>
        private XmlDocument GenerateHotelCancellationResponse(string confirmationNo)
        {
            //string responseFromServer = "";
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.EETConfig["HotelCancellationReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelCancellationRequest(confirmationNo);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return xmlDoc;
        }

        /// <summary>
        /// ReadHotelCancellationResponse
        /// </summary>
        /// <param name="doc">Cancel Response XMl</param>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> ReadHotelCancellationResponse(XmlDocument doc)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            try
            {
                //XmlDocument doc = new XmlDocument();

                //doc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "HotelCancelResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");

                XmlNode root = doc.DocumentElement;
                XmlNodeList CancelRS = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                foreach (XmlNode node in CancelRS)
                {
                    switch (node.Name)
                    {
                        case "Errors":
                            cancellationCharges.Add("Status", "Failed");
                            cancellationCharges.Add("Description", node.FirstChild.Attributes["ShortText"].Value);
                            break;
                        case "UniqueID":
                            break;
                        case "Warnings":
                            //cancellationCharges.Add("Amount", (Convert.ToDecimal(node.FirstChild.InnerText.Split(' ')[0]) * (exchangeRates.ContainsKey(node.FirstChild.InnerText.Split(' ')[1]) ? exchangeRates[node.FirstChild.InnerText.Split(' ')[1]] : 1)).ToString());
                            //cancellationCharges.Add("Currency", agentCurrency);
                            cancellationCharges.Add("Amount", node.FirstChild.InnerText.Split(' ')[0]);
                            cancellationCharges.Add("Currency", node.FirstChild.InnerText.Split(' ')[1]);
                            cancellationCharges.Add("Description", node.FirstChild.Attributes["ShortText"].Value);
                            if (node.FirstChild.Attributes["ShortText"].Value.Contains("Reservation was cancelled"))
                            {
                                cancellationCharges.Add("Status", "Cancelled");
                            }
                            else
                            {
                                cancellationCharges.Add("Status", "Failed");
                            }
                            break;
                        case "Success":
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to read Hotel Cancellation response : " + ex.Message, ex);
            }
            return cancellationCharges;
        }
        #endregion

        #region Download ZoneList

        /// <summary>
        /// First Time Need to download All Zones
        /// </summary>
        public void GetZones()
        {
            XmlDocument responseXML = GenerateZoneListResponse();

            if (responseXML !=null && responseXML.ChildNodes !=null && responseXML.ChildNodes.Count > 0)
            {
                try
                {
                    string filePath = XmlPath + "HotelZoneListResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    //XmlDocument doc = new XmlDocument();
                    //doc.LoadXml(responseXML);
                    responseXML.Save(filePath);
                }
                catch { }
            }
        }
        /// <summary>
        /// Generates Hotel Zones Request XML.
        /// </summary>
        /// <returns>string</returns>
        private string GenerateZoneListRequest()
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("JP_ZoneListService", "http://www.juniper.es/webservice/2007/");
                writer.WriteStartElement("JP_ZoneListRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS


                writer.WriteStartElement("TPA_Extensions");
                writer.WriteElementString("ShowHotels", "", "1");
                writer.WriteEndElement();//TPA_Extensions
                writer.WriteEndElement();//JP_ZoneListRQ
                writer.WriteEndElement();//JP_ZoneListService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "HotelZoneListRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    // //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to generate Zone List request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// Generates Hotel Zones Response XML.
        /// </summary>
        /// <returns>XmlDocument</returns>
        private XmlDocument GenerateZoneListResponse()
        {
            //string responseFromServer = "";
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.EETConfig["ZoneListReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateZoneListRequest();// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return xmlDoc;
        }
        #endregion

        /// <summary>
        /// First Time Need to download All Room Types
        /// </summary>
        public void GetRoomTypeList()
        {
            XmlDocument responseXML = GenerateRoomTypeListResponse();

            if (responseXML !=null && responseXML.ChildNodes !=null && responseXML.ChildNodes.Count >0)
            {
                try
                {
                    string filePath = XmlPath + "HotelRoomTypeListResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    //XmlDocument doc = new XmlDocument();
                    //doc.LoadXml(responseXML);
                    responseXML.Save(filePath);
                }
                catch { }
            }
        }

        /// <summary>
        /// Generates Hotel RoomTypes Request XML.
        /// </summary>
        /// <returns>string</returns>
        private string GenerateRoomTypeListRequest()
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("JP_RoomTypeListService", "http://www.juniper.es/webservice/2007/");
                writer.WriteStartElement("JP_RoomTypeListRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS
                writer.WriteEndElement();//JP_ZoneListRQ
                writer.WriteEndElement();//JP_ZoneListService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "HotelRoomTypeListRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    // Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to generate Zone List request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }
        /// <summary>
        /// Generates Hotel RoomTypes Response XML.
        /// </summary>
        /// <returns>string</returns>
        private XmlDocument GenerateRoomTypeListResponse()
        {
            //string responseFromServer = "";
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.EETConfig["RoomTypeListReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateRoomTypeListRequest();// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return xmlDoc;
        }

        /// <summary>
        /// GetHotelDetails
        /// </summary>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <param name="cityCode">Selected CityCode</param>
        /// <param name="staticData">StaticData Object</param>
        /// <returns>HotelDetails</returns>
        public HotelDetails GetHotelDetails(string hotelCode, string cityCode, ref List<HotelStaticData> staticData)
        {
            HotelDetails hotelDetails = new HotelDetails();
            try
            {
                XmlDocument responseXML = GetHotelDescriptiveInfoResponse(hotelCode);
                if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0)
                {
                    hotelDetails = ReadHotelDescriptiveInfoResponse(responseXML);
                }

                if (hotelDetails.HotelName != null)
                {
                    HotelStaticData hsd = new HotelStaticData();
                    hsd.EMail = hotelDetails.Email;
                    hsd.FaxNumber = hotelDetails.FaxNumber;
                    hsd.HotelAddress = hotelDetails.Address;
                    hsd.HotelCode = hotelCode;
                    hsd.HotelDescription = hotelDetails.Description;
                    hsd.HotelFacilities = "";
                    if (hotelDetails.HotelFacilities != null)
                    {
                        foreach (string fac in hotelDetails.HotelFacilities)
                        {
                            if (hsd.HotelFacilities.Length > 0)
                            {
                                hsd.HotelFacilities += "|" + fac;
                            }
                            else
                            {
                                hsd.HotelFacilities = fac;
                            }
                        }
                    }
                    hsd.HotelLocation = cityCode;
                    hsd.HotelMap = hotelDetails.Map;
                    hsd.HotelName = hotelDetails.HotelName;
                    hsd.HotelPicture = hotelDetails.Image;
                    hsd.HotelPolicy = hotelDetails.HotelPolicy;
                    hsd.PhoneNumber = hotelDetails.PhoneNumber;
                    hsd.PinCode = hotelDetails.PinCode;
                    hsd.Rating = hotelDetails.hotelRating;
                    hsd.Source = HotelBookingSource.EET;
                    hsd.SpecialAttraction = "";

                    if (hotelDetails.Attractions != null)
                    {
                        foreach (KeyValuePair<string, string> pair in hotelDetails.Attractions)
                        {
                            if (hsd.SpecialAttraction.Length > 0)
                            {
                                hsd.SpecialAttraction += "|" + pair.Key + "#" + pair.Value;
                            }
                            else
                            {
                                hsd.SpecialAttraction = pair.Key + "#" + pair.Value;
                            }
                        }
                    }
                    hsd.Status = true;
                    hsd.TimeStamp = DateTime.Now;
                    hsd.URL = hotelDetails.URL;
                    hsd.CityCode = cityCode;
                    try
                    {
                        hsd.Save();
                    }
                    catch { }
                    staticData.Add(hsd);

                    HotelImages images = new HotelImages();
                    images.CityCode = cityCode;
                    foreach (string image in hotelDetails.Images)
                    {
                        if (images.DownloadedImgs.Length > 0)
                        {
                            images.DownloadedImgs += "|" + image;
                        }
                        else
                        {
                            images.DownloadedImgs = image;
                        }
                    }
                    images.HotelCode = hotelCode;
                    images.Images = images.DownloadedImgs;
                    images.Source = HotelBookingSource.EET;
                    try
                    {
                        images.Save();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EET-Failed to get Hotel Details : " + ex.Message, ex);
                //Console.WriteLine(ex.ToString());
            }
            return hotelDetails;
        }

        #region IDisposable Support
        /// <summary>
        /// To detect redundant calls
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Dispose(dispose managed state (managed objects))
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// <summary>
        /// override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// </summary>
        ~EET()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary>
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
