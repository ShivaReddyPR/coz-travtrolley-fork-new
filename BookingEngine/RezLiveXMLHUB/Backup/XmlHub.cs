﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;
using System.Net;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using CT.BookingEngine;
using CT.Configuration;
using System.Web;
using CT.Core;

namespace RezLive
{
    public class XmlHub
    {
        

        private string _agentCode;
        private string _username;
        private string _password;
        private SearchRequest _searchRequest;
        private int appUserId;
        private string sessionId;

        private string SEARCH_HOTEL = ConfigurationSystem.RezLiveConfig["SearchHotel"];

        public XmlHub()
        {
            _agentCode = ConfigurationSystem.RezLiveConfig["AgentCode"];
            _username = ConfigurationSystem.RezLiveConfig["Username"];
            _password = ConfigurationSystem.RezLiveConfig["Password"];
        }

        public XmlHub(string SessionId)
        {
            _agentCode = ConfigurationSystem.RezLiveConfig["AgentCode"];
            _username = ConfigurationSystem.RezLiveConfig["Username"];
            _password = ConfigurationSystem.RezLiveConfig["Password"];
            this.sessionId = SessionId;
        }

        public SearchRequest HotelSearchRequest
        {
            get { return _searchRequest; }
            set { _searchRequest = value; }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

       

        private static Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }

        #region Get Hotel Results

        /// <summary>
        /// Get the Hotels for the Search criteria.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<HotelSearchResult> GetHotelResults(HotelRequest request)
        {
            List<HotelSearchResult> hotelResults = new List<HotelSearchResult>();

            SearchResponse hotelResponse = SearchHotels(request);

            if (hotelResponse.ErrorMessage.Length > 0)
            {
                //No Hotels found. Some error occured
            }
            else
            {
                //Hotels found. Fill the object and return collection.

                if (request.HotelName.Trim().Length > 0)
                {
                    List<Hotel> filteredHotels = hotelResponse.Hotels.FindAll(delegate(Hotel h) { return h.Name.ToLower().Contains(request.HotelName.ToLower()); });

                    foreach (Hotel hotel in filteredHotels)
                    {
                        FillHotelSearchResultObject(hotel, ref hotelResults, ref request, hotelResponse.CurrencyCode);
                    }
                }
                else
                {
                    foreach (Hotel hotel in hotelResponse.Hotels)
                    {
                        FillHotelSearchResultObject(hotel, ref hotelResults, ref request, hotelResponse.CurrencyCode);
                    }
                }
            }

            return hotelResults;
        }

        void FillHotelSearchResultObject(Hotel hotel, ref List<HotelSearchResult> hotelResults, ref HotelRequest request,string currencyCode)
        {
            HotelSearchResult result = new HotelSearchResult();
            result.BookingSource = HotelBookingSource.RezLive;
            result.CityCode = hotel.City;
            result.Currency = currencyCode;
            result.EndDate = request.EndDate;
            result.HotelAddress = hotel.Address;
            result.HotelCategory = "";
            result.HotelCode = hotel.ID;
            result.HotelContactNo = "";
            result.HotelDescription = hotel.Description;
            result.HotelLocation = hotel.Location;
            result.HotelMap = "";
            result.HotelName = hotel.Name;
            result.HotelFacilities = hotel.Amenities;
            result.HotelPicture = "";
            foreach (string pic in hotel.ThumbImages)
            {
                if (result.HotelPicture.Length == 0)
                {
                    result.HotelPicture = pic;
                }
                else
                {
                    result.HotelPicture += "," + pic;
                }
            }
            result.IsFeaturedHotel = false;
            result.Price = new PriceAccounts();
            result.Price.NetFare = (decimal)hotel.Price;
            result.PromoMessage = "";
            result.PropertyType = "";
            result.RateType = RateType.Negotiated;
            result.Rating = (HotelRating)hotel.Rating;
            result.RoomDetails = new HotelRoomsDetails[hotel.Rooms.Count];
            for (int i = 0; i < hotel.Rooms.Count; i++)
            {
                HotelRoom hroom = hotel.Rooms[i];
                HotelRoomsDetails room = new HotelRoomsDetails();
                room.Amenities = hotel.RoomAmenities;
                room.CancellationPolicy = hroom.CancellationPolicy;
                room.ChildCharges = 0;
                room.Discount = 0;
                room.Occupancy = new Dictionary<string, int>();
                room.Occupancy.Add("Adults", hroom.Adults);
                room.Occupancy.Add("Childs", hroom.Childs);
                room.PubExtraGuestCharges = 0;
                room.RoomTypeCode = hroom.RoomDescription;
                room.RatePlanCode = hroom.BookingKey;
                int days = request.EndDate.Subtract(request.StartDate).Days;
                room.Rates = new RoomRates[days];
                for (int j = 0; j < days; j++)
                {
                    RoomRates rate = new RoomRates();
                    rate.RateType = RateType.Negotiated;
                    rate.SellingFare = (decimal)hroom.TotalRate;
                    rate.Totalfare = (decimal)hroom.TotalRate;
                    rate.BaseFare = (decimal)hroom.TotalRate;
                    rate.Amount = (decimal)hroom.TotalRate;
                    rate.Days = request.EndDate;
                    room.Rates[j] = rate;
                }
                room.RoomTypeName = hroom.RoomType;
                room.SellExtraGuestCharges = 0;
                room.SellingFare = Convert.ToDecimal(hroom.TotalRate);
                //if (hroom.RoomPax.Contains("|"))
                //{
                //    string[] pax = hroom.RoomPax.Split('|');

                        //    for (int p = 0; p < pax.Length; p++)
                        //    {
                        //        room.SequenceNo = pax[p];
                        //    }
                        //}
                        //else
                        //{
                        //    room.SequenceNo = request.RoomGuest[0].noOfAdults.ToString();
                        //}
                        room.SequenceNo = hroom.RoomPax;
                        room.TotalPrice = Convert.ToDecimal(hroom.TotalRate);
                        room.TotalTax = 0;

                        result.RoomDetails[i] = room;
                    }
                    result.RoomGuest = new RoomGuestData[hotel.Rooms.Count];
                    for (int k = 0; k < hotel.Rooms.Count; k++)
                    {
                        RoomGuestData guestData = new RoomGuestData();
                        guestData.noOfAdults = hotel.Rooms[k].Adults;
                        guestData.noOfChild = hotel.Rooms[k].Childs;
                        guestData.childAge = hotel.Rooms[k].ChildAges;

                        result.RoomGuest[k] = guestData;
                    }
                    result.RPH = 0;
                    result.SellingFare = hotel.Price;
                    result.StartDate = request.StartDate;
                    result.SupplierType = "";
                    result.TotalPrice = hotel.Price;
                    result.TotalTax = 0;

            hotelResults.Add(result);
        }

        /// <summary>
        /// Connects the RezLive API to get the results for the Search request and returns the response.        /// 
        /// </summary>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private SearchResponse SearchHotels(HotelRequest hotelRequest)
        {
            SearchResponse hotelResponse = new SearchResponse();

            //to load from file

            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load("C:\\temp\\misc\\temp\\RezLiveHotelSearchResponse.xml");
            //string strResult = xmlDoc.InnerXml;

	   DateTime before = DateTime.Now;
            string strResult = GetHotelSearchResponse(hotelRequest);
            DateTime after = DateTime.Now;
            TimeSpan ts = after - before;
           //Audit.Add(EventType.Search, Severity.Normal, 1, "LotsOfHotels Search returned in " + ts.Milliseconds, "1");
            
            hotelResponse = ReadHotelSearchResponse(strResult);

            return hotelResponse;
        }

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GenerateHotelSearchRequest(HotelRequest hotelRequest)
        {
            StringBuilder request = new StringBuilder();

            XmlWriter writer = XmlWriter.Create(request);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
            writer.WriteStartElement("HotelFindRequest");
            writer.WriteStartElement("Authentication");
            writer.WriteStartElement("AgentCode");
            writer.WriteValue(_agentCode);
            writer.WriteEndElement();
            writer.WriteStartElement("UserName");
            writer.WriteValue(_username);
            writer.WriteEndElement();
            writer.WriteStartElement("Password");
            writer.WriteValue(_password);
            writer.WriteEndElement();
            writer.WriteEndElement();//Authentication

            writer.WriteStartElement("Booking");
            writer.WriteStartElement("ArrivalDate");
            writer.WriteValue(hotelRequest.StartDate.ToString("dd/MM/yyyy"));
            writer.WriteEndElement();
            writer.WriteStartElement("DepartureDate");
            writer.WriteValue(hotelRequest.EndDate.ToString("dd/MM/yyyy"));
            writer.WriteEndElement();
            writer.WriteStartElement("CountryCode");
            writer.WriteValue(hotelRequest.CountryName);
            writer.WriteEndElement();
            writer.WriteStartElement("City");
            writer.WriteValue(hotelRequest.CityName.Replace("city", ""));
            writer.WriteEndElement();
            writer.WriteStartElement("GuestNationality");
            writer.WriteValue(hotelRequest.PassengerNationality);
            writer.WriteEndElement();
            writer.WriteStartElement("HotelRatings");
            switch (hotelRequest.Rating)
            {
                case HotelRating.All:
                case HotelRating.FiveStar:
                    for (int i = 1; i <= 5; i++)
                    {
                        writer.WriteStartElement("HotelRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
                case HotelRating.FourStar:
                    for (int i = 1; i <= 4; i++)
                    {
                        writer.WriteStartElement("HotelRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
                case HotelRating.ThreeStar:
                    for (int i = 1; i <= 3; i++)
                    {
                        writer.WriteStartElement("HotelRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
                case HotelRating.TwoStar:
                    for (int i = 1; i <= 2; i++)
                    {
                        writer.WriteStartElement("HotelRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
                case HotelRating.OneStar:
                    for (int i = 1; i <= 1; i++)
                    {
                        writer.WriteStartElement("HotelRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
            }
            writer.WriteEndElement();//HotelRatings
            writer.WriteStartElement("Rooms");
            for (int j = 0; j < hotelRequest.RoomGuest.Length; j++)
            {
                RoomGuestData room = hotelRequest.RoomGuest[j];
                writer.WriteStartElement("Room");
                writer.WriteStartElement("Type");
                writer.WriteValue("Type-" + (j + 1));
                writer.WriteEndElement();
                writer.WriteStartElement("NoOfAdults");
                writer.WriteValue(room.noOfAdults);
                writer.WriteEndElement();
                writer.WriteStartElement("NoOfChilds");
                writer.WriteValue(room.noOfChild);
                writer.WriteEndElement();
                if (room.noOfChild > 0)
                {
                    writer.WriteStartElement("ChildrenAges");
                    foreach (int age in room.childAge)
                    {
                        writer.WriteStartElement("ChildAge");
                        writer.WriteValue(age.ToString());
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();//Room
            }
            writer.WriteEndElement();//Rooms
            writer.WriteEndElement();//Booking
            writer.WriteEndElement();//HotelFindRequest
            writer.Flush();
            writer.Close();
            if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "HotelSearchRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(request.ToString());
                xmldoc.Save(filePath);
                Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }

            return "XML=" + Uri.EscapeUriString(request.ToString());
        }

        /// <summary>
        /// Used to send the request XML to the server and pulls the response XML.
        /// </summary>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GetHotelSearchResponse(HotelRequest hotelRequest)
        {
            string responseFromServer = string.Empty;
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["SearchHotel"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelSearchRequest(hotelRequest);// GETTING XML STRING...


                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                //Response.Write(responseFromServer);


                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            return responseFromServer;
        }

        /// <summary>
        /// Used in reading response XML and assigning hotel data to the response.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private SearchResponse ReadHotelSearchResponse(string response)
        {
            SearchResponse hotelResults = new SearchResponse();

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(response);
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "HotelSearchResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";                        
                        
                        xmlDoc.Save(filePath);
                        Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                    }
                }
                catch (Exception ex)
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "HotelSearchResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    sw.Write(response);
                    sw.Flush();
                    sw.Close();
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                    throw ex;
                }

                XmlNode errorNode = xmlDoc.SelectSingleNode("/HotelFindResponse/error");

                if (errorNode != null)
                {
                    hotelResults.ErrorMessage = errorNode.InnerText;
                }
                else
                {
                    hotelResults.ErrorMessage = "";
                    hotelResults.CurrencyCode = xmlDoc.SelectSingleNode("/HotelFindResponse/Currency").InnerText;
                    List<Hotel> hotels = new List<Hotel>();

                    XmlNodeList hotelNodes = xmlDoc.SelectNodes("/HotelFindResponse/Hotels/Hotel");

                    if (hotelNodes != null)
                    {
                        foreach (XmlNode node in hotelNodes)
                        {
                            Hotel hotelObj = new Hotel();
                            hotelObj.SearchSessionId = node.SelectSingleNode("SearchSessionId").InnerText;
                            hotelObj.ID = node.SelectSingleNode("Id").InnerText;
                            hotelObj.Name = node.SelectSingleNode("Name").InnerText;
                            hotelObj.Rating = Convert.ToInt32(node.SelectSingleNode("Rating").InnerText);
                            hotelObj.Description = HttpUtility.HtmlDecode(node.SelectSingleNode("Description").InnerText);
                            hotelObj.Location = node.SelectSingleNode("Location").InnerText;
                            hotelObj.Address = node.SelectSingleNode("Address").InnerText;
                            hotelObj.City = node.SelectSingleNode("City").InnerText;
                            hotelObj.Lattitude = node.SelectSingleNode("latitude").InnerText;
                            hotelObj.Longitude = node.SelectSingleNode("longitude").InnerText;
                            string[] amenities = System.Web.HttpUtility.HtmlDecode(node.SelectSingleNode("HotelAmenities").InnerText).Split(',');
                            hotelObj.Amenities = new List<string>();
                            hotelObj.Amenities.AddRange(amenities);
                            string[] roomAmenities = HttpUtility.HtmlDecode(node.SelectSingleNode("RoomAmenities").InnerText).Split(',');
                            hotelObj.RoomAmenities = new List<string>();
                            hotelObj.RoomAmenities.AddRange(roomAmenities);
                            hotelObj.ThumbImages = new List<string>();
                            hotelObj.ThumbImages.AddRange(node.SelectSingleNode("ThumbImages").InnerText.Split(','));
                            hotelObj.Price = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);                            
                            hotelObj.Rooms = new List<HotelRoom>();
                            XmlNodeList roomNodes = node.SelectNodes("RoomDetails/RoomDetail");
                            foreach (XmlNode rnode in roomNodes)
                            {
                                HotelRoom room = new HotelRoom();
                                room.RoomType = rnode.SelectSingleNode("Type").InnerText;
                                room.RoomDescription = rnode.SelectSingleNode("RoomDescription").InnerText.Split('|')[0];
                                room.BookingKey = rnode.SelectSingleNode("BookingKey").InnerText;
                                if (rnode.SelectSingleNode("Adults").InnerText.Contains("|"))
                                {
                                    room.RoomPax = rnode.SelectSingleNode("Adults").InnerText;
                                    string[] adults = rnode.SelectSingleNode("Adults").InnerText.Split('|');
                                    foreach (string adt in adults)
                                    {
                                        room.Adults += Convert.ToInt32(adt);
                                    }
                                }
                                else
                                {
                                    room.Adults = Convert.ToInt32(rnode.SelectSingleNode("Adults").InnerText);
                                    room.RoomPax = rnode.SelectSingleNode("Adults").InnerText;
                                }
                                if (rnode.SelectSingleNode("Children").InnerText.Contains("|"))
                                {
                                    string[] childs = rnode.SelectSingleNode("Children").InnerText.Split('|');
                                    foreach (string chd in childs)
                                    {
                                        room.Childs += Convert.ToInt32(chd);
                                    }
                                }
                                else
                                {
                                    room.Childs = Convert.ToInt32(rnode.SelectSingleNode("Children").InnerText);
                                }
                                room.ChildAges = new List<int>();
                                if (room.Childs > 0)
                                {
                                    if (rnode.SelectSingleNode("ChildrenAges").InnerText.Contains("|"))
                                    {
                                        string[] ages = rnode.SelectSingleNode("ChildrenAges").InnerText.Split('|');

                                        foreach (string age in ages)
                                        {
                                            string[] roomages = age.Split('*');

                                            if (roomages.Length > 0)
                                            {
                                                foreach (string roomage in roomages)
                                                {
                                                    room.ChildAges.Add(Convert.ToInt32(roomage));
                                                }
                                            }
                                            else
                                            {
                                                room.ChildAges.Add(Convert.ToInt32(age));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (rnode.SelectSingleNode("ChildrenAges").InnerText.Contains("*"))
                                        {
                                            string[] ages = rnode.SelectSingleNode("ChildrenAges").InnerText.Split('*');

                                            foreach (string age in ages)
                                            {
                                                room.ChildAges.Add(Convert.ToInt32(age));
                                            }
                                        }
                                        else
                                        {
                                            room.ChildAges.Add(Convert.ToInt32(rnode.SelectSingleNode("ChildrenAges").InnerText));
                                        }
                                    }
                                }
                                room.TotalRooms = Convert.ToInt32(rnode.SelectSingleNode("TotalRooms").InnerText);
                                if (rnode.SelectSingleNode("TotalRate").InnerText.Contains("|"))
                                {
                                    string[] totalRates = rnode.SelectSingleNode("TotalRate").InnerText.Split('|');
                                    foreach (string rate in totalRates)
                                    {
                                        room.TotalRate += Convert.ToDecimal(rate);
                                    }
                                }
                                else
                                {
                                    room.TotalRate = Convert.ToDecimal(rnode.SelectSingleNode("TotalRate").InnerText);
                                }
                                room.CancellationPolicy = rnode.SelectSingleNode("CancellationPolicy").InnerText;
                                room.Description = rnode.SelectSingleNode("RoomDescription").InnerText;
                                room.TermsAndConditions = rnode.SelectSingleNode("TermsAndConditions").InnerText;
                                hotelObj.Rooms.Add(room);
                            }
                            hotels.Add(hotelObj);
                        }
                    }

                    hotelResults.Hotels = hotels;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Read Hotel Search Response", ex);
            }

            return hotelResults;
        } 

        #endregion

        
        #region GetCancellationPolicy

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetCancellationInfo(HotelItinerary itinerary, HotelRequest hotelRequest, decimal price)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();

            string responseXML = GetCancellationInfoResponse(itinerary, hotelRequest);
            if (responseXML.Length > 0 && responseXML.Contains("CancellationPolicyResponse"))
            {
                cancellationInfo = ReadCancellationInfoResponse(responseXML, price, itinerary);
            }
            return cancellationInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GenerateCancellationInfoRequest(HotelItinerary itinerary, HotelRequest hotelRequest)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("CancellationPolicyRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("HotelId");
                writer.WriteValue(itinerary.HotelCode);
                writer.WriteEndElement();
                writer.WriteStartElement("ArrivalDate");
                writer.WriteValue(itinerary.StartDate.ToString("dd/MM/yyyy"));
                writer.WriteEndElement();
                writer.WriteStartElement("DepartureDate");
                writer.WriteValue(itinerary.EndDate.ToString("dd/MM/yyyy"));
                writer.WriteEndElement();
                writer.WriteStartElement("CountryCode");
                writer.WriteValue(hotelRequest.CountryName);
                writer.WriteEndElement();
                writer.WriteStartElement("City");
                writer.WriteValue(hotelRequest.CityName);
                writer.WriteEndElement();
                writer.WriteStartElement("GuestNationality");
                writer.WriteValue(hotelRequest.PassengerNationality);
                writer.WriteEndElement();
                writer.WriteStartElement("Currency");
                writer.WriteValue(hotelRequest.Currency);
                writer.WriteEndElement();
                writer.WriteStartElement("RoomDetails");
                if (itinerary.Roomtype.Length == 1)
                {                    
                    for (int i = 0; i < itinerary.Roomtype.Length; i++)
                    {
                        CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];
                        string bookingKey = room.RatePlanCode;
                        writer.WriteStartElement("RoomDetail");
                        writer.WriteStartElement("BookingKey");
                        writer.WriteValue(bookingKey);
                        writer.WriteEndElement();//BookingKey
                        writer.WriteStartElement("Adults");
                        if (room.RoomName.Contains("|"))
                        {
                            string[] names = room.RoomName.Split('|');
                            string adults = "";
                            for (int j = 1; j < names.Length; j++)
                            {
                                if (adults.Length > 0)
                                {
                                    adults += "|" + i.ToString();
                                }
                                else
                                {
                                    adults = i.ToString();
                                }
                            }
                            writer.WriteValue(adults);
                        }
                        else
                        {
                            writer.WriteValue(room.AdultCount);
                        }
                        writer.WriteEndElement();
                        writer.WriteStartElement("Children");
                        if (room.RoomName.Contains("|"))
                        {
                            string[] names = room.RoomName.Split('|');
                            string childs = "";
                            for (int j = 1; j < names.Length; j++)
                            {
                                if (childs.Length > 0)
                                {
                                    childs += "|" + i.ToString();
                                }
                                else
                                {
                                    childs = i.ToString();
                                }
                            }
                            writer.WriteValue(childs);
                        }
                        else
                        {
                            writer.WriteValue(room.ChildCount);
                        }
                        writer.WriteEndElement();
                        writer.WriteStartElement("ChildrenAges");
                        string childAges = "";
                        if (room.ChildAge != null)
                        {
                            foreach (int cage in room.ChildAge)
                            {
                                if (childAges.Length > 0)
                                {
                                    childAges += "*" + cage;
                                }
                                else
                                {
                                    childAges = cage.ToString();
                                }
                            }
                        }
                        if (childAges.Length > 0)
                        {
                            writer.WriteValue(childAges);
                        }
                        else
                        {
                            writer.WriteValue("0");
                        }
                        writer.WriteEndElement();//ChildrenAges
                        writer.WriteStartElement("Type");
                        writer.WriteValue(room.RoomName);
                        writer.WriteEndElement();//Type
                        writer.WriteEndElement();//RoomDetail
                    }
                }
                else
                {
                    string roomName="",bookingKey = "", adults = "", childs = "", childAges = "";

                    for (int i = 0; i < itinerary.Roomtype.Length; i++)
                    {
                        CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];
                        roomName = room.RoomName;
                        bookingKey = room.RatePlanCode;

                        if (adults.Length > 0)
                        {
                            adults += "|" + room.AdultCount.ToString();
                        }
                        else
                        {
                            adults = room.AdultCount.ToString();
                        }

                        if (childs.Length > 0)
                        {
                            childs += "|" + room.ChildCount.ToString();
                        }
                        else
                        {
                            childs = room.ChildCount.ToString();
                        }
                        if (room.ChildAge.Count > 0)
                        {
                            foreach (int cage in room.ChildAge)
                            {
                                if (childAges.Length > 0)
                                {
                                    childAges += "|" + cage;
                                }
                                else
                                {
                                    childAges = cage.ToString();
                                }
                            }
                        }
                        else
                        {
                            if (childAges.Length > 0)
                            {
                                childAges += "|0";
                            }
                            else
                            {
                                childAges = "0";
                            }
                        }
                    }
                    writer.WriteStartElement("RoomDetail");
                    writer.WriteStartElement("BookingKey");
                    writer.WriteValue(bookingKey);
                    writer.WriteEndElement();//BookingKey
                    writer.WriteStartElement("Adults");
                    writer.WriteValue(adults);
                    writer.WriteEndElement();//adults
                    writer.WriteStartElement("Children");
                    writer.WriteValue(childs);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ChildrenAges");
                    if (childAges.Length > 0)
                    {
                        writer.WriteValue(childAges);
                    }
                    else
                    {
                        writer.WriteValue("0");
                    }
                    writer.WriteEndElement();
                    writer.WriteStartElement("Type");
                    writer.WriteValue(roomName);
                    writer.WriteEndElement();//Type
                    writer.WriteEndElement();//RoomDetail
                }
                writer.WriteEndElement();//RoomDetails
                writer.WriteEndElement();//CancellationPolicyRequest
                writer.Flush();
                writer.Close();

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "CancellationPolicyRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    Audit.Add(EventType.GetCancellationPolicy, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed generate cancellation info request for Hotel : " + itinerary.HotelName, ex);
            }
            return "XML=" + HttpUtility.UrlEncode(request.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GetCancellationInfoResponse(HotelItinerary itinerary, HotelRequest hotelRequest)
        {
            string responseFromServer = string.Empty;
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["CancellationPolicy"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateCancellationInfoRequest(itinerary, hotelRequest);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                //Response.Write(responseFromServer);


                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to get response for cancellation policy request", ex);
            }
            return responseFromServer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadCancellationInfoResponse(string responseXML, decimal hotelPrice, HotelItinerary itinerary)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(responseXML);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "CancellationPolicyResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";                                        
                    xmldoc.Save(filePath);
                    Audit.Add(EventType.GetCancellationPolicy, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                XmlNodeList cancellationNodes = xmldoc.SelectNodes("/CancellationPolicyResponse/CancellationInformations/CancellationInformation");

                if (cancellationNodes != null)
                {
                    string cancelInfo = "";
                    string endDate = "";
                    foreach (XmlNode cancelNode in cancellationNodes)
                    {
                        string startDate = cancelNode.SelectSingleNode("StartDate").InnerText;
                        endDate = cancelNode.SelectSingleNode("EndDate").InnerText;
                        string ChargeType = cancelNode.SelectSingleNode("ChargeType").InnerText;
                        decimal amount = Convert.ToDecimal(cancelNode.SelectSingleNode("ChargeAmount").InnerText);
                        string currency = cancelNode.SelectSingleNode("Currency").InnerText;

                        if (cancelInfo.Length == 0)
                        {
                            if (amount > 0)
                            {
                                if (ChargeType == "Amount")
                                {
                                    if (currency == "USD")
                                    {
                                        cancelInfo = "AED " + Math.Round((amount * (decimal)3.67),2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                    else 
                                    {
                                        cancelInfo = "AED " + Math.Round(amount, 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                    
                                }
                                else if (ChargeType == "Percentage")
                                {
                                    amount = hotelPrice * (amount / 100);
                                    if (currency == "USD")
                                    {
                                        cancelInfo = "AED " + Math.Round((amount * (decimal)3.67), 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                    else
                                    {
                                        cancelInfo = "AED " + Math.Round(amount, 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                }
                                else if (ChargeType == "Nightly")
                                {
                                    int duration = itinerary.EndDate.Subtract(itinerary.StartDate).Days;
                                    decimal perNight = hotelPrice / duration;
                                    if (currency == "USD")
                                    {
                                        cancelInfo = "AED " + Math.Round((perNight * (decimal)3.67), 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                    else
                                    {
                                        cancelInfo = "AED " + Math.Round(perNight, 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                }
                            }
                            else
                            {
                                cancelInfo = "No charges applicable between " + startDate + " and " + endDate;
                            }
                        }
                        else
                        {
                            if (amount > 0)
                            {
                                if (ChargeType == "Amount")
                                {
                                    if (currency == "USD")
                                    {
                                        cancelInfo += "| AED " + Math.Round((amount * (decimal)3.67),2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                    else
                                    {
                                        cancelInfo += "| AED " + Math.Round(amount,2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                }
                                else if (ChargeType == "Percentage")
                                {
                                    amount = hotelPrice * (amount / 100);
                                    if (currency == "USD")
                                    {
                                        cancelInfo += "| AED " + Math.Round((amount * (decimal)3.67), 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                    else
                                    {
                                        cancelInfo += "| AED " + Math.Round(amount, 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                }
                                else if (ChargeType == "Nightly")
                                {
                                    int duration = itinerary.EndDate.Subtract(itinerary.StartDate).Days;
                                    decimal perNight = hotelPrice / duration;
                                    if (currency == "USD")
                                    {
                                        cancelInfo += "| AED " + Math.Round((perNight * (decimal)3.67), 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                    else
                                    {
                                        cancelInfo += "| AED " + Math.Round(perNight, 2) + " will be charged between " + startDate + " and " + endDate;
                                    }
                                }
                            }
                            else
                            {
                                cancelInfo += "| No charges applicable between " + startDate + " and " + endDate;
                            }
                        }
                    }
                    cancellationInfo.Add("lastCancellationDate", endDate);
                    cancellationInfo.Add("CancelPolicy", cancelInfo);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to Read cancellation response ", ex);
            }
            return cancellationInfo;
        }

        #endregion
        
        #region Book Hotel

        public BookingResponse Book(ref HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();

            string responseXML = GetHotelBookingResponse(itinerary, itinerary.HotelPassenger.City, itinerary.HotelPassenger.Countrycode, itinerary.Currency, itinerary.PassengerNationality);
            if (responseXML.Length > 0 && responseXML.Contains("BookingResponse"))
            {
                bookResponse = ReadHotelBookingResponse(responseXML, ref itinerary);
            }
            else
            {
                bookResponse.Error = "Booking Failed";                
            }


            return bookResponse;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GenerateHotelBookingRequest(HotelItinerary itinerary, string city, string countryCode, string currency, string nationality)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                decimal price = 0;
                foreach (CT.BookingEngine.HotelRoom room in itinerary.Roomtype)
                {
                    foreach (HotelRoomFareBreakDown fare in room.RoomFareBreakDown)
                    {
                        price += fare.RoomPrice;
                    }
                }

                HotelDetails hotelDetails = GetHotelDetails(itinerary.HotelCode);

                hotelDetails.Description = hotelDetails.Description.Replace("&lt;", "<");
                hotelDetails.Description = hotelDetails.Description.Replace("&gt;", ">");


                #region XML Code
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\"");
                writer.WriteStartElement("BookingRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("Booking");
                writer.WriteStartElement("ArrivalDate");
                writer.WriteValue(itinerary.StartDate.ToString("dd/MM/yyyy"));
                writer.WriteEndElement();

                writer.WriteStartElement("DepartureDate");
                writer.WriteValue(itinerary.EndDate.ToString("dd/MM/yyyy"));
                writer.WriteEndElement();

                writer.WriteStartElement("GuestNationality");
                writer.WriteValue(nationality);
                writer.WriteEndElement();

                writer.WriteStartElement("CountryCode");
                writer.WriteValue(countryCode);
                writer.WriteEndElement();

                writer.WriteStartElement("City");
                writer.WriteValue(city);
                writer.WriteEndElement();

                writer.WriteStartElement("HotelId");
                writer.WriteValue(itinerary.HotelCode);
                writer.WriteEndElement();

                writer.WriteStartElement("Name");
                writer.WriteValue(itinerary.HotelName);
                writer.WriteEndElement();

                writer.WriteStartElement("Address");
                writer.WriteValue(itinerary.HotelAddress1);
                writer.WriteEndElement();

                writer.WriteStartElement("Description");
                writer.WriteValue(hotelDetails.Description);
                writer.WriteEndElement();

                writer.WriteStartElement("Latitude");
                if (hotelDetails.Location.Count > 0)
                {
                    writer.WriteValue(hotelDetails.Location[0]);
                }
                else
                {
                    writer.WriteValue("");
                }
                writer.WriteEndElement();

                writer.WriteStartElement("Longitude");
                if (hotelDetails.Location.Count > 1)
                {
                    writer.WriteValue(hotelDetails.Location[1]);
                }
                else
                {
                    writer.WriteValue("");
                }
                writer.WriteEndElement();

                writer.WriteStartElement("ThumbImages");
                if (hotelDetails.Images.Count > 0)
                {
                    writer.WriteValue(hotelDetails.Images[0]);
                }
                else
                {
                    writer.WriteValue("");
                }
                writer.WriteEndElement();

                writer.WriteStartElement("HotelImages");
                writer.WriteValue("");
                writer.WriteEndElement();

                writer.WriteStartElement("Currency");
                writer.WriteValue(currency);
                writer.WriteEndElement();

                writer.WriteStartElement("RoomDetails");
                if (itinerary.Roomtype.Length == 1)
                {                    
                    foreach (CT.BookingEngine.HotelRoom room in itinerary.Roomtype)
                    {
                        writer.WriteStartElement("RoomDetail");

                        writer.WriteStartElement("Type");
                        writer.WriteValue(room.RoomName);
                        writer.WriteEndElement();

                        writer.WriteStartElement("BookingKey");
                        writer.WriteValue(room.RatePlanCode);
                        writer.WriteEndElement();

                        writer.WriteStartElement("Adults");
                        if (room.RoomName.Contains("|"))
                        {
                            string[] names = room.RoomName.Split('|');
                            string adults = "";
                            for (int i = 1; i <= names.Length; i++)
                            {
                                if (adults.Length > 0)
                                {
                                    adults += "|" + i.ToString();
                                }
                                else
                                {
                                    adults = i.ToString();
                                }
                            }
                            writer.WriteValue(adults);
                        }
                        else
                        {
                            writer.WriteValue(room.AdultCount);
                        }
                        writer.WriteEndElement();

                        writer.WriteStartElement("Children");
                        if (room.RoomName.Contains("|"))
                        {
                            string[] names = room.RoomName.Split('|');
                            string childs = "";
                            for (int i = 1; i <= names.Length; i++)
                            {
                                if (childs.Length > 0)
                                {
                                    childs += "|" + i.ToString();
                                }
                                else
                                {
                                    childs = i.ToString();
                                }
                            }
                            writer.WriteValue(childs);
                        }
                        else
                        {
                            writer.WriteValue(room.ChildCount);
                        }
                        writer.WriteEndElement();

                        writer.WriteStartElement("ChildrenAges");
                        string childAge = "";
                        
                        foreach (int age in room.ChildAge)
                        {
                            if (childAge.Length > 0)
                            {
                                childAge += "*" + age;
                            }
                            else
                            {
                                childAge = age.ToString();
                            }
                        }
                        if (childAge.Length > 0)
                        {
                            writer.WriteValue(childAge);
                        }
                        else
                        {
                            writer.WriteValue("0");
                        }
                        writer.WriteEndElement();

                        writer.WriteStartElement("TotalRooms");
                        writer.WriteValue(itinerary.Roomtype.Length);
                        writer.WriteEndElement();

                        writer.WriteStartElement("TotalRate");
                        if (room.RoomName.Contains("|"))
                        {
                            string[] names = room.RoomName.Split('|');
                            decimal amount = room.Price.NetFare / names.Length;

                            writer.WriteValue(amount + "|" + amount);
                        }
                        else
                        {
                            writer.WriteValue(room.Price.NetFare);
                        }
                        writer.WriteEndElement();

                        writer.WriteStartElement("CancellationPolicy");
                        
                        if(itinerary.HotelCancelPolicy.Contains("|"))
                        {
                            itinerary.HotelCancelPolicy = itinerary.HotelCancelPolicy.Replace("|", ". ");
                        }
                        writer.WriteValue(itinerary.HotelCancelPolicy);
                        writer.WriteEndElement();

                        writer.WriteStartElement("Guests");
                        foreach (HotelPassenger pax in room.PassenegerInfo)
                        {
                            string title = pax.Title;
                            title = title.Replace(".", "");
                            if (title.ToLower() == "mr")
                            {
                                title = "Mr";
                            }
                            else if (title.ToLower() == "ms")
                            {
                                title = "Ms";
                            }
                            else if (title.ToLower() == "mrs")
                            {
                                title = "Mrs";
                            }
                            writer.WriteStartElement("Guest");
                            writer.WriteStartElement("Salutation");
                            writer.WriteValue(title);
                            writer.WriteEndElement();

                            writer.WriteStartElement("FirstName");
                            writer.WriteValue(pax.Firstname);
                            writer.WriteEndElement();

                            writer.WriteStartElement("LastName");
                            writer.WriteValue(pax.Lastname);
                            writer.WriteEndElement();

                            if (pax.PaxType == HotelPaxType.Child)
                            {
                                writer.WriteStartElement("IsChild");
                                writer.WriteValue("1");
                                writer.WriteEndElement();
                                writer.WriteStartElement("Age");
                                writer.WriteValue(pax.Age);
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();//Guest
                        }
                        writer.WriteEndElement();//Guests

                        writer.WriteEndElement();//RoomDetail
                    }
                }
                else
                {
                    string roomName = "", bookingKey = "", adults = "", childs = "", childAges = "", amount="";

                    for (int i = 0; i < itinerary.Roomtype.Length; i++)
                    {
                        CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];
                        roomName = room.RoomName;
                        bookingKey = room.RatePlanCode;

                        if (adults.Length > 0)
                        {
                            adults += "|" + room.AdultCount.ToString();
                        }
                        else
                        {
                            adults = room.AdultCount.ToString();
                        }

                        if (childs.Length > 0)
                        {
                            childs += "|" + room.ChildCount.ToString();
                        }
                        else
                        {
                            childs = room.ChildCount.ToString();
                        }
                        if (room.ChildAge.Count > 0)
                        {
                            if (childAges.Length > 0 && !childAges.EndsWith("|"))
                            {
                                childAges += "|";
                            }                            
                            
                            foreach (int cage in room.ChildAge)
                            {
                                if (room.ChildCount <= 1)
                                {
                                    if (childAges.Length > 0)
                                    {
                                        if (!childAges.EndsWith("|"))
                                        {
                                            childAges += "|";
                                        }
                                        childAges += cage;
                                    }
                                    else
                                    {
                                        childAges = cage.ToString();
                                    }
                                }
                                else
                                {
                                    if (childAges.Length > 0 )
                                    {
                                        if (!childAges.EndsWith("|"))
                                        {
                                            childAges += "*" + cage;
                                        }
                                        else
                                        {
                                            childAges += cage.ToString();
                                        }
                                    }
                                    else
                                    {
                                        childAges = cage.ToString();
                                    }
                                }
                            }                            
                        }
                        else
                        {
                            if (childAges.Length > 0)
                            {
                                childAges += "|0";
                            }
                            else
                            {
                                childAges = "0";
                            }
                        }
                        if (amount.Length > 0)
                        {
                            amount += "|" + (room.Price.NetFare).ToString();
                        }
                        else
                        {
                            amount = (room.Price.NetFare).ToString();
                        }
                    }
                    if (childAges.Contains("|"))
                    {
                        writer.WriteStartElement("RoomDetail");
                        writer.WriteStartElement("Type");
                        writer.WriteValue(roomName);
                        writer.WriteEndElement();//Type
                        writer.WriteStartElement("BookingKey");
                        writer.WriteValue(bookingKey);
                        writer.WriteEndElement();//BookingKey
                        writer.WriteStartElement("Adults");
                        writer.WriteValue(adults);
                        writer.WriteEndElement();//adults
                        writer.WriteStartElement("Children");
                        writer.WriteValue(childs);
                        writer.WriteEndElement();
                        writer.WriteStartElement("ChildrenAges");
                        if (childAges.Length > 0)
                        {
                            writer.WriteValue(childAges);
                        }
                        else
                        {
                            writer.WriteValue("0");
                        }
                        writer.WriteEndElement();
                        writer.WriteStartElement("TotalRooms");
                        writer.WriteValue(itinerary.Roomtype.Length);
                        writer.WriteEndElement();

                        writer.WriteStartElement("TotalRate");
                        writer.WriteValue(amount);
                        writer.WriteEndElement();
                        writer.WriteStartElement("CancellationPolicy");
                        if (itinerary.HotelCancelPolicy.Contains("|"))
                        {
                            itinerary.HotelCancelPolicy = itinerary.HotelCancelPolicy.Replace("|", ". ");
                        }
                        writer.WriteValue(itinerary.HotelCancelPolicy);
                        writer.WriteEndElement();

                        for (int i = 0; i < itinerary.Roomtype.Length; i++)
                        {
                            CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];
                            writer.WriteStartElement("Guests");
                            foreach (HotelPassenger pax in room.PassenegerInfo)
                            {
                                string title = pax.Title;
                                title = title.Replace(".", "");
                                if (title.ToLower() == "mr")
                                {
                                    title = "Mr";
                                }
                                else if (title.ToLower() == "ms")
                                {
                                    title = "Ms";
                                }
                                else if (title.ToLower() == "mrs")
                                {
                                    title = "Mrs";
                                }
                                writer.WriteStartElement("Guest");
                                writer.WriteStartElement("Salutation");
                                writer.WriteValue(title);
                                writer.WriteEndElement();

                                writer.WriteStartElement("FirstName");
                                writer.WriteValue(pax.Firstname);
                                writer.WriteEndElement();

                                writer.WriteStartElement("LastName");
                                writer.WriteValue(pax.Lastname);
                                writer.WriteEndElement();

                                if (pax.PaxType == HotelPaxType.Child)
                                {
                                    writer.WriteStartElement("IsChild");
                                    writer.WriteValue("1");
                                    writer.WriteEndElement();
                                    writer.WriteStartElement("Age");
                                    writer.WriteValue(pax.Age);
                                    writer.WriteEndElement();
                                }
                                writer.WriteEndElement();//Guest
                            }
                            writer.WriteEndElement();//Guests
                        }
                        writer.WriteEndElement();//RoomDetail
                    }                    
                }
                writer.WriteEndElement();//RoomDetails
                writer.WriteEndElement();//Booking
                writer.WriteEndElement();//BookingRequest

                writer.Flush();
                writer.Close(); 
                #endregion


                request = request.Replace("&lt;", "<");
                request = request.Replace("&gt;", ">");

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "HotelBookingRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to generate hotel booking request for " + itinerary.HotelName, ex);
            }

            return "XML=" + HttpUtility.UrlEncode(request.ToString());
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GetHotelBookingResponse(HotelItinerary itinerary, string city, string countryCode, string currency, string nationality)
        {
            string responseFromServer = string.Empty;
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["HotelBooking"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelBookingRequest(itinerary, city, countryCode, currency, nationality);// GETTING XML STRING...
                                
                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                //Response.Write(responseFromServer);


                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                Audit.Add(EventType.HotelBook, Severity.Normal, 1, "LotsOfHotels" + err, "");
            }
            return responseFromServer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private BookingResponse ReadHotelBookingResponse(string response,ref HotelItinerary itinerary)
        {
            BookingResponse bookRes = new BookingResponse();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "HotelBookingResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDoc.Save(filePath);
                    Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                XmlNode status = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/status");
                XmlNode bookingStatus = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/BookingStatus");
                XmlNode confirmationNo = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/BookingId");
                XmlNode bookingCode = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/BookingCode");



                if (confirmationNo != null && confirmationNo.InnerText.Length > 0)
                {
                    itinerary.ConfirmationNo = confirmationNo.InnerText;
                    bookRes.ConfirmationNo = confirmationNo.InnerText;
                    itinerary.BookingRefNo = bookingCode.InnerText;

                    if (bookingStatus.InnerText == "Confirmed")
                    {
                        bookRes.Status = BookingResponseStatus.Successful;
                        itinerary.Status = HotelBookingStatus.Confirmed;
                        bookRes.Error = "";

                        itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: RezLive, as per final booking form reference No: " + confirmationNo.InnerText;
                    }
                    else
                    {
                        bookRes.Status = BookingResponseStatus.Failed;
                        if (bookingStatus.InnerText == "fail")
                        {
                            bookRes.Error = "Error from RezLive:Booking Failed";
                        }
                        else
                        {
                            bookRes.Error = bookingStatus.InnerText;
                        }
                    }
                }
                else
                {
                    bookRes.Status = BookingResponseStatus.Failed;
                    if (status != null)
                    {
                        XmlNode reason = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/reason");
                        bookRes.Error = reason.InnerText;
                        //if (status.InnerText == "fail")
                        //{
                        //    //bookRes.Error = "Error from RezLive: Booking Failed";
                        //    bookRes.Error = reason.InnerText;
                        //}
                        //else
                        //{
                        //    bookRes.Error = status.InnerText;
                        //}
                    }
                    else
                    {
                        bookRes.Error = "status node is null";
                    }
                }
                
            }
            catch (Exception ex)
            {
                bookRes.Error = "Error from RezLive: " + ex.Message;
                bookRes.Status = BookingResponseStatus.Failed;
                throw new Exception("Booking failed reason :" + ex.Message, ex);                
            }

            return bookRes;
        }

        #endregion

        #region Get Hotel Details

        public HotelDetails GetHotelDetails(string hotelId)
        {
            HotelDetails hotelDetails = new HotelDetails();

            string responseXML = GetHotelDetailResponse(hotelId);

            if (responseXML.Length > 0)
            {
                hotelDetails = ReadHotelDetailsResponse(responseXML);
            }

            return hotelDetails;
        }

        private string GenerateHotelDetailRequest(string hotelId)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("HotelDetailsRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("Hotels");
                writer.WriteStartElement("HotelId");
                writer.WriteValue(hotelId);
                writer.WriteEndElement();//HotelId
                writer.WriteEndElement();//Hotels
                writer.WriteEndElement();//HotelDetailsRequest

                writer.Flush();
                writer.Close();

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "HotelDetailsRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to generate hotel detail request for Hotel : " + hotelId, ex);
            }

            return "XML=" + HttpUtility.UrlEncode(request.ToString());
        }

        private string GetHotelDetailResponse(string hotelId)
        {
            string responseFromServer = string.Empty;
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["HotelDetails"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelDetailRequest(hotelId);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                //Response.Write(responseFromServer);


                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to get response for Hotel Detail request for HotelId : " + hotelId, ex);
            }
            return responseFromServer;
        }

        private HotelDetails ReadHotelDetailsResponse(string response)
        {
            HotelDetails hotelDetails = new HotelDetails();

            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(response);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "HotelDetailsResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";                    
                    
                    xmldoc.Save(filePath);
                    Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                XmlNode hotelDetail = xmldoc.SelectSingleNode("/HotelDetailsResponse/Hotels");

                string error = string.Empty;
                if (hotelDetail.FirstChild.Name == "Error")
                {
                    error = hotelDetail.SelectSingleNode("Error").InnerText;
                }

                if (error.Length <= 0)
                {
                    hotelDetails.Address = hotelDetail.SelectSingleNode("HotelAddress").InnerText;
                    hotelDetails.Description = hotelDetail.SelectSingleNode("Description").InnerText;
                    string[] hotelFacilities = hotelDetail.SelectSingleNode("HotelAmenities").InnerText.Split(',');
                    hotelDetails.HotelFacilities = new List<string>();
                    foreach (string fac in hotelFacilities)
                    {
                        hotelDetails.HotelFacilities.Add(fac);
                    }
                    hotelDetails.RoomFacilities = new List<string>();
                    string[] roomFacilities = hotelDetail.SelectSingleNode("RoomAmenities").InnerText.Split(',');
                    foreach (string fac in roomFacilities)
                    {
                        hotelDetails.RoomFacilities.Add(fac);
                    }
                    hotelDetails.Images = new List<string>();
                    XmlNodeList images = hotelDetail.SelectNodes("Images/Image");

                    foreach (XmlNode img in images)
                    {
                        hotelDetails.Images.Add(img.InnerText);
                    }
                    hotelDetails.FaxNumber = hotelDetail.SelectSingleNode("Fax").InnerText;
                    hotelDetails.PhoneNumber = hotelDetail.SelectSingleNode("Phone").InnerText;
                    hotelDetails.Location = new List<string>();
                    hotelDetails.Location.Add(hotelDetail.SelectSingleNode("Latitude").InnerText);
                    hotelDetails.Location.Add(hotelDetail.SelectSingleNode("Longitude").InnerText);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to read Hotel Details response ", ex);
            }

            return hotelDetails;
        }

        #endregion


        #region Hotel Booking Cancellation

        public Dictionary<string, string> CancelHotelBooking(string hotelBookingId, string hotelBookingCode)
        {
            Dictionary<string, string> cancelCharges = new Dictionary<string, string>();

            string responseXML = GetHotelCancellationResponse(hotelBookingId, hotelBookingCode);

            if (responseXML.Length > 0 && responseXML.Contains("CancellationResponse"))
            {
                cancelCharges = ReadHotelCancellationResponse(responseXML);
            }

            return cancelCharges;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelBookingId"></param>
        /// <param name="hotelBookingCode"></param>
        /// <returns></returns>
        private string GenerateHotelCancellationRequest(string hotelBookingId, string hotelBookingCode)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("CancellationRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("Cancellation");
                writer.WriteStartElement("BookingId");
                writer.WriteValue(hotelBookingId);
                writer.WriteEndElement();//BookingId
                writer.WriteStartElement("BookingCode");
                writer.WriteValue(hotelBookingCode);
                writer.WriteEndElement();//BookingCode
                writer.WriteEndElement();//Cancellation
                writer.WriteEndElement();//CancellationRequest

                writer.Flush();
                writer.Close();

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "BookingCancelRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    Audit.Add(EventType.CancellBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to generate hotel detail request for Hotel : " + hotelBookingId, ex);
            }

            return "XML=" + HttpUtility.UrlEncode(request.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelBookingId"></param>
        /// <param name="hotelBookingCode"></param>
        /// <returns></returns>
        private string GetHotelCancellationResponse(string hotelBookingId, string hotelBookingCode)
        {
            string responseFromServer = string.Empty;
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["HotelCancel"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelCancellationRequest(hotelBookingId, hotelBookingCode);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                //Response.Write(responseFromServer);


                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to get response for Hotel Detail request for HotelId : " + hotelBookingId, ex);
            }
            return responseFromServer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadHotelCancellationResponse(string response)
        {
            Dictionary<string, string> cancelData = new Dictionary<string, string>();
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(response);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.RezLiveConfig["XmlLogPath"] + "BookingCancelResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    
                    xmldoc.Save(filePath);
                    Audit.Add(EventType.CancellBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                XmlNode error = xmldoc.SelectSingleNode("/CancellationResponse/Error");
                XmlNode status = xmldoc.SelectSingleNode("/CancellationResponse/Status");
                XmlNode amount = xmldoc.SelectSingleNode("/CancellationResponse/CancellationCharges");
                XmlNode currency = xmldoc.SelectSingleNode("/CancellationResponse/Currency");
                
                if (error != null && error.InnerText.Length > 0)
                {
                    cancelData.Add("Error", error.InnerText);
                }
                else
                {
                    if (status != null && status.InnerText == "true")
                    {
                        cancelData.Add("Status", "Cancelled");
                        if (amount != null)
                        {
                            cancelData.Add("Amount", amount.InnerText);
                        }
                        else
                        {
                            cancelData.Add("Amount", "0");
                        }
                        if (currency != null)
                        {
                            cancelData.Add("Currency", currency.InnerText);
                        }
                        else
                        {
                            cancelData.Add("Currency", "AED");
                        }
                    }
                    else
                    {
                        cancelData.Add("Status", "Failed");
                    }
                }
            }
            catch (Exception ex)
            {
                cancelData.Add("Error", ex.Message);
                throw new Exception("Failed to read Hotel cancellation response", ex);
            }

            return cancelData;
        }

        #endregion


        #region Get Hotel Details

        //Not implemented

        #endregion
    }

    public class SearchRequest
    {
        private DateTime _arrivalDate;
        private DateTime _departureDate;
        private string _countryCode;
        private string _cityName;
        private string _nationality;
        private List<int> _hotelRating;
        private List<HotelRoom> _hotelRooms;

        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set { _arrivalDate = value; }
        }

        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set { _departureDate = value; }
        }

        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }

        public string Nationality
        {
            get { return _nationality; }
            set { _nationality = value; }
        }

        public List<int> HotelRating
        {
            get { return _hotelRating; }
            set { _hotelRating = value; }
        }

        public List<HotelRoom> HotelRooms
        {
            get { return _hotelRooms; }
            set { _hotelRooms = value; }
        }
    }

    public class HotelRoom
    {
        private string _roomType;
        private string _roomDescription;
        private int _adults;
        private int _childs;
        private List<int> _childAges;
        private string _bookingKey;
        private int _totalRooms;
        private decimal _totalRate;
        private string _cancellationPolicy;
        private string _description;
        private string _termsAndConditions;
        private string _roomPax;

        public string RoomType
        {
            get { return _roomType; }
            set { _roomType = value; }
        }

        public string RoomDescription
        {
            get { return _roomDescription; }
            set { _roomDescription = value; }
        }

        public int Adults
        {
            get { return _adults; }
            set { _adults = value; }
        }

        public int Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }

        public List<int> ChildAges
        {
            get { return _childAges; }
            set { _childAges = value; }
        }

        public string BookingKey
        {
            get { return _bookingKey;}
            set { _bookingKey = value;}
        }
        public int TotalRooms
        {
            get { return _totalRooms; }
            set { _totalRooms = value; }
        }
        public decimal TotalRate
        {
            get { return _totalRate; }
            set { _totalRate = value; }
        }
        public string CancellationPolicy
        {
            get { return _cancellationPolicy; }
            set { _cancellationPolicy = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string TermsAndConditions
        {
            get { return _termsAndConditions; }
            set { _termsAndConditions = value; }
        }

        public string RoomPax
        {
            get { return _roomPax; }
            set { _roomPax = value; }
        }
    }

    public class SearchResponse
    {
        private DateTime _arrivalDate;
        private DateTime _departureDate;
        private string _currencyCode;
        private string _guestNationality;
        private List<Hotel> _hotels;
        private string _errorMessage;

        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set { _arrivalDate = value; }
        }

        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set { _departureDate = value; }
        }

        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }

        public string GuestNationality
        {
            get { return _guestNationality; }
            set { _guestNationality = value; }
        }

        public List<Hotel> Hotels
        {
            get { return _hotels; }
            set { _hotels = value; }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
    }

    public class Hotel
    {
        private string _searchSessionId;
        private string _id;
        private string _name;
        private int _rating;
        private string _description;
        private string _location;
        private string _address;
        private string _city;
        private string _lattitude;
        private string _longitude;
        private List<string> _amenities;
        private List<string> _roomAmenities;
        private List<string> _thumbImages;
        private decimal _price;
        private List<HotelRoom> _rooms;

        public string SearchSessionId
        {
            get { return _searchSessionId; }
            set { _searchSessionId = value; }
        }
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int Rating
        {
            get { return _rating; }
            set { _rating = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string Lattitude
        {
            get { return _lattitude; }
            set { _lattitude = value; }
        }
        public string Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public List<string> Amenities
        {
            get { return _amenities; }
            set { _amenities = value; }
        }
        public List<string> RoomAmenities
        {
            get { return _roomAmenities; }
            set { _roomAmenities = value; }
        }
        public List<string> ThumbImages
        {
            get { return _thumbImages; }
            set { _thumbImages = value; }
        }
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public List<HotelRoom> Rooms
        {
            get { return _rooms; }
            set { _rooms = value; }
        }
    }    
}
