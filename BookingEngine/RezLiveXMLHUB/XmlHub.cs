﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using System.IO;
using System.IO.Compression;
using CT.BookingEngine;
using CT.Configuration;
using System.Web;
using CT.Core;
using System.Data;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;

namespace RezLive
{
    public class XmlHub:IDisposable
    {

        private string XmlPath = string.Empty;
        private string _agentCode;
        private string _username;
        private string _password;
        private SearchRequest _searchRequest;
        private int appUserId;
        private string sessionId;
        decimal rateOfExchange = 1;
        string agentCurrency = "";
        Dictionary<string, decimal> exchangeRates;
        int decimalPoint;
        string sourceCountryCode;
        /// <summary>
        /// This variable is used,to store the Cancellation buffer,which is read from AppConfig table  
        /// </summary>
        double cnclBuffer;
        public List<HotelStaticData> StaticData { get; set; }
        private string SEARCH_HOTEL = ConfigurationSystem.RezLiveConfig["SearchHotel"];

        public XmlHub()
        {
            _agentCode = ConfigurationSystem.RezLiveConfig["AgentCode"];
            _username = ConfigurationSystem.RezLiveConfig["Username"];
            _password = ConfigurationSystem.RezLiveConfig["Password"];
            //Create Xml Log path folder
            XmlPath = ConfigurationSystem.RezLiveConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }

        public XmlHub(string SessionId)
        {
            _agentCode = ConfigurationSystem.RezLiveConfig["AgentCode"];
            _username = ConfigurationSystem.RezLiveConfig["Username"];
            _password = ConfigurationSystem.RezLiveConfig["Password"];
            //Create Xml Log path folder
            XmlPath = ConfigurationSystem.RezLiveConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
            this.sessionId = SessionId;
        }

        public SearchRequest HotelSearchRequest
        {
            get { return _searchRequest; }
            set { _searchRequest = value; }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        public string Username { get => _username; set => _username = value; }
        public string Password { get => _password; set => _password = value; }
        public double CnclBuffer { get => cnclBuffer; set => cnclBuffer = value; }

        private static Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }

        #region Get Hotel Results
        //List<HotelStaticData> StaticData = new List<HotelStaticData>();
        /// <summary>
        /// Get the Hotels for the destination Search criteria.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //public HotelSearchResult[] GetHotelResults(HotelRequest request, decimal markup, string markupType, decimal discount, string discountType)
        //{
        //    HotelStaticData staticInfo = new HotelStaticData();
        //    StaticData = staticInfo.StaticDataLoad(request.CityName, request.CountryCode, null, (int)HotelBookingSource.RezLive);
        //    request.CityName = request.CityCode.Trim();// Rez Live using city code to search
        //    HotelSearchResult[] results;
        //    DateTime before = DateTime.Now;
        //    XmlDocument strResult = GetHotelSearchResponse(request);
        //    //Audit.Add(EventType.Search, Severity.Normal, 1, "RezLive Search returned in " + ts.Milliseconds, "1");
        //    results = ReadHotelSearchResponse(strResult, request, markup, markupType,discount,discountType);
        //    return results;
        //}

        /// <summary>
        /// Get the Hotels for the destination Search criteria.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        HotelRequest request = new HotelRequest();
        decimal markup = 0;
        string markupType = string.Empty;
        decimal discount = 0;
        string discounttype = string.Empty;
        List<long> hotelid = new List<long>();
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        List<HotelSearchResult> hotelresults = new List<HotelSearchResult>();
        public HotelSearchResult[] GetHotelResults(HotelRequest request, decimal markup, string markupType, decimal discount, string discountType)
        {
           // HotelSearchResult[] searchRes = new HotelSearchResult[0];
            try
            {
                this.request = request;
                this.markup = markup;
                this.markupType = markupType;
                this.discount = discount;
                this.discounttype = discountType;
                hotelresults = new List<HotelSearchResult>();
                XmlDocument xmlDoc = new XmlDocument();
                string searchresponse = string.Empty;
                //HotelStaticData hotelStaticData = new HotelStaticData();
                //records means number of hotels passed in each request(this value is hard coded by default with 2999)
                //maxrecords means number of hotels passed in each request(this value is hard coded by default with 2999)
                int records = 50, rowsCount = 0, startIndex = 0, endIndex = 0, maxrecords = 50;
                //int records = 999, rowsCount = 0, startIndex = 0, endIndex = 0, maxrecords = 999;// For Testing
                HotelSearchResult[] result = new HotelSearchResult[0];
                //ReadySources will hold same source orderings i.e Rezlive1, Rezlive12 ....
                Dictionary<string, int> readySources = new Dictionary<string, int>();
                //This dictionary will hold what fare search we want to call
                Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();


                //New Testing StaticId on 28/09/2020 starts
                if (StaticData != null && StaticData.Count > 0)
                {

                    hotelid = StaticData.Select(i => i.RezliveID).ToList().Select(long.Parse).ToList();
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, "There is No Hotelid's for :" + request.CityName, "");
                    throw new Exception("No Hotels found :" + request.CityName);
                }
                request.CityName = request.CityCode.Trim();// Rez Live using city code to search
                                                           //Number of hotels for search city
                rowsCount = hotelid.Count;

                int j = 0;
                while (rowsCount > 0)
                {
                    //records are <= default hotel request count in each request
                    //
                    if (records <= maxrecords)
                    {
                        startIndex = 0;
                        endIndex = (rowsCount <= records) ? rowsCount : records;
                    }
                    //records are > default hotel request count in each request
                    else
                    {
                        startIndex += maxrecords;
                        if ((records) < hotelid.Count)
                        {
                            //each request  need to pass 2999 records only
                            endIndex = maxrecords;
                        }
                        else
                        {
                            endIndex = rowsCount;
                        }
                    }
                    string key = j.ToString() + "-" + startIndex + "-" + endIndex;
                    listOfThreads.Add(key, new WaitCallback(GenarateSearchResults));
                    readySources.Add(key, 300);
                    j++;
                    rowsCount -= maxrecords;
                    if ((records + maxrecords) > hotelid.Count)
                    {
                        records += hotelid.Count - records;
                    }
                    else
                    {
                        records += maxrecords;
                    }
                }
                eventFlag = new AutoResetEvent[readySources.Count];
                int k = 0;
                //Start each fare search within a Thread which will automatically terminate after the results are received.
                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                {
                    if (readySources.ContainsKey(deThread.Key))
                    {
                        ThreadPool.QueueUserWorkItem(deThread.Value, deThread.Key);
                        eventFlag[k] = new AutoResetEvent(false);
                        k++;
                    }
                }
                if (k != 0)
                {//waiting time for all threads
                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 1500), true))
                    {
                        //TODO: audit which thread is timed out                
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "Rezlive.GetHotelResults() Err :" + ex.ToString(), "0");
               // throw ex;
            }
            return hotelresults.ToArray();
        }
        private void GenarateSearchResults(object eventNumber)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                //get the range values for request
                string[] values = eventNumber.ToString().Split('-');
                string postData = GenerateHotelSearchRequest(this.request, Convert.ToInt32(values[0]), Convert.ToInt32(values[1]), Convert.ToInt32(values[2]), hotelid);
                 xmlDoc = GetHotelSearchResponse(postData, Convert.ToInt32(values[0]));              
                List<HotelSearchResult> results = new List<HotelSearchResult>();
                if (xmlDoc!=null)
                { 
                results = ReadHotelSearchResponse(xmlDoc, request, markup, markupType, discount, discounttype);
                if(results!=null && results.Count>0)
                hotelresults.AddRange(results);
                }
                eventFlag[Convert.ToInt32(eventNumber.ToString().Split('-')[0])].Set();
            }
            catch (Exception ex)
            {
                eventFlag[Convert.ToInt32(eventNumber.ToString().Split('-')[0])].Set();
                Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "RezLive.GenerateHotelSearchResult  Err :" + ex.ToString(), "0");
               // throw ex;
            }
        }
        void FillHotelSearchResultObject(Hotel hotel, ref List<HotelSearchResult> hotelResults, ref HotelRequest request, string currencyCode)
        {
            HotelSearchResult result = new HotelSearchResult();
            result.BookingSource = HotelBookingSource.RezLive;
            result.CityCode = hotel.City;
            result.Currency = currencyCode;
            result.EndDate = request.EndDate;
            result.HotelAddress = hotel.Address;
            result.HotelCategory = "";
            result.HotelCode = hotel.ID;
            result.HotelContactNo = "";
            result.HotelDescription = hotel.Description;
            result.HotelLocation = hotel.Location;
            result.HotelMap = "";
            result.HotelName = hotel.Name;
            result.HotelFacilities = hotel.Amenities;
            result.HotelPicture = "";
            foreach (string pic in hotel.ThumbImages)
            {
                if (result.HotelPicture.Length == 0)
                {
                    result.HotelPicture = pic;
                }
                else
                {
                    result.HotelPicture += "," + pic;
                }
            }
            result.IsFeaturedHotel = false;
            result.Price = new PriceAccounts();
            result.Price.NetFare = (decimal)hotel.Price;
            result.PromoMessage = "";
            result.PropertyType = "";
            result.RateType = RateType.Negotiated;
            result.Rating = (HotelRating)hotel.Rating;
            result.RoomDetails = new HotelRoomsDetails[hotel.Rooms.Count];
            for (int i = 0; i < hotel.Rooms.Count; i++)
            {
                HotelRoom hroom = hotel.Rooms[i];
                HotelRoomsDetails room = new HotelRoomsDetails();
                room.Amenities = hotel.RoomAmenities;
                room.CancellationPolicy = hroom.CancellationPolicy;
                room.ChildCharges = 0;
                room.Discount = 0;
                room.Occupancy = new Dictionary<string, int>();
                room.Occupancy.Add("Adults", hroom.Adults);
                room.Occupancy.Add("Childs", hroom.Childs);
                room.PubExtraGuestCharges = 0;
                room.RoomTypeCode = hroom.RoomDescription;
                room.RatePlanCode = hroom.BookingKey;
                int days = request.EndDate.Subtract(request.StartDate).Days;
                room.Rates = new RoomRates[days];
                for (int j = 0; j < days; j++)
                {
                    RoomRates rate = new RoomRates();
                    rate.RateType = RateType.Negotiated;
                    rate.SellingFare = (decimal)hroom.TotalRate;
                    rate.Totalfare = (decimal)hroom.TotalRate;
                    rate.BaseFare = (decimal)hroom.TotalRate;
                    rate.Amount = (decimal)hroom.TotalRate;
                    rate.Days = request.EndDate;
                    room.Rates[j] = rate;
                }
                room.RoomTypeName = hroom.RoomType;
                room.SellExtraGuestCharges = 0;
                room.SellingFare = Convert.ToDecimal(hroom.TotalRate);
                //if (hroom.RoomPax.Contains("|"))
                //{
                //    string[] pax = hroom.RoomPax.Split('|');

                //    for (int p = 0; p < pax.Length; p++)
                //    {
                //        room.SequenceNo = pax[p];
                //    }
                //}
                //else
                //{
                //    room.SequenceNo = request.RoomGuest[0].noOfAdults.ToString();
                //}
                room.SequenceNo = hroom.RoomPax;
                room.TotalPrice = Convert.ToDecimal(hroom.TotalRate);
                room.TotalTax = 0;

                result.RoomDetails[i] = room;
            }
            result.RoomGuest = new RoomGuestData[hotel.Rooms.Count];
            for (int k = 0; k < hotel.Rooms.Count; k++)
            {
                RoomGuestData guestData = new RoomGuestData();
                guestData.noOfAdults = hotel.Rooms[k].Adults;
                guestData.noOfChild = hotel.Rooms[k].Childs;
                guestData.childAge = hotel.Rooms[k].ChildAges;

                result.RoomGuest[k] = guestData;
            }
            result.RPH = 0;
            result.SellingFare = hotel.Price;
            result.StartDate = request.StartDate;
            result.SupplierType = "";
            result.TotalPrice = hotel.Price;
            result.TotalTax = 0;

            hotelResults.Add(result);
        }

        ///// <summary>
        ///// Connects the RezLive API to get the results for the Search request and returns the response.        /// 
        ///// </summary>
        ///// <param name="hotelRequest"></param>
        ///// <returns></returns>
        //private SearchResponse SearchHotels(HotelRequest hotelRequest)
        //{
        //    SearchResponse hotelResponse = new SearchResponse();
        //    DateTime before = DateTime.Now;
        //    XmlDocument strResult = GetHotelSearchResponse(hotelRequest);
        //    return hotelResponse;
        //}

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GenerateHotelSearchRequest(HotelRequest hotelRequest, int index, int startIndex, int endIndex, List<long> hotelid)
        {
            StringBuilder request = new StringBuilder();
            try
            {

                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("HotelFindRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("Booking");
                writer.WriteStartElement("ArrivalDate");
                writer.WriteValue(hotelRequest.StartDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                //writer.WriteValue("28/01/2016");
                writer.WriteEndElement();
                writer.WriteStartElement("DepartureDate");
                writer.WriteValue(hotelRequest.EndDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                //writer.WriteValue("30/01/2016");
                writer.WriteEndElement();
                writer.WriteStartElement("CountryCode");
                writer.WriteValue(hotelRequest.CountryName);
                writer.WriteEndElement();
                writer.WriteStartElement("City");
                writer.WriteValue(hotelRequest.CityName.Replace("city", ""));
                writer.WriteEndElement();
                writer.WriteStartElement("HotelIDs");
                for (int i = startIndex; i < startIndex + endIndex; i++)
                {
                    writer.WriteElementString("Int", Convert.ToString(hotelid[i]));
                }

                //   writer.WriteValue(hotelid.GetRange(startIndex, endIndex).ToArray());
                writer.WriteEndElement();
                writer.WriteStartElement("GuestNationality");
                writer.WriteValue(hotelRequest.PassengerNationality);
                writer.WriteEndElement();
                writer.WriteStartElement("HotelRatings");
                switch (hotelRequest.Rating)
                {
                    case HotelRating.All:
                    case HotelRating.FiveStar:
                        for (int i = 1; i <= 5; i++)
                        {
                            writer.WriteStartElement("HotelRating");
                            writer.WriteValue(i);
                            writer.WriteEndElement();
                        }
                        break;
                    case HotelRating.FourStar:
                        for (int i = 1; i <= 4; i++)
                        {
                            writer.WriteStartElement("HotelRating");
                            writer.WriteValue(i);
                            writer.WriteEndElement();
                        }
                        break;
                    case HotelRating.ThreeStar:
                        for (int i = 1; i <= 3; i++)
                        {
                            writer.WriteStartElement("HotelRating");
                            writer.WriteValue(i);
                            writer.WriteEndElement();
                        }
                        break;
                    case HotelRating.TwoStar:
                        for (int i = 1; i <= 2; i++)
                        {
                            writer.WriteStartElement("HotelRating");
                            writer.WriteValue(i);
                            writer.WriteEndElement();
                        }
                        break;
                    case HotelRating.OneStar:
                        for (int i = 1; i <= 1; i++)
                        {
                            writer.WriteStartElement("HotelRating");
                            writer.WriteValue(i);
                            writer.WriteEndElement();
                        }
                        break;
                }
                writer.WriteEndElement();//HotelRatings
                writer.WriteStartElement("Rooms");
                for (int j = 0; j < hotelRequest.RoomGuest.Length; j++)
                {
                    RoomGuestData room = hotelRequest.RoomGuest[j];
                    writer.WriteStartElement("Room");
                    writer.WriteStartElement("Type");
                    writer.WriteValue("Type-" + (j + 1));
                    writer.WriteEndElement();
                    writer.WriteStartElement("NoOfAdults");
                    writer.WriteValue(room.noOfAdults);
                    writer.WriteEndElement();
                    writer.WriteStartElement("NoOfChilds");
                    writer.WriteValue(room.noOfChild);
                    writer.WriteEndElement();
                    if (room.noOfChild > 0)
                    {
                        writer.WriteStartElement("ChildrenAges");
                        foreach (int age in room.childAge)
                        {
                            writer.WriteStartElement("ChildAge");
                            writer.WriteValue(age.ToString());
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();//Room
                }
                writer.WriteEndElement();//Rooms
                writer.WriteEndElement();//Booking
                writer.WriteEndElement();//HotelFindRequest
                writer.Flush();
                writer.Close();
                try
                {
                    string filePath = XmlPath + "HotelSearchRequest_" + index + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "XML=" + Uri.EscapeUriString(request.ToString());
        }

        /// <summary>
        /// Used to send the request XML to the server and pulls the response XML.
        /// </summary>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private XmlDocument GetHotelSearchResponse(string postData, int index)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["SearchHotelbyid"]);
                request.Method = "POST"; //Using POST method       
                request.Method = "POST";
                request.ContentType = contentType;
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();
                WebResponse response = request.GetResponse();
                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                try
                {
                    string filePath = XmlPath + "HotelSearchResponse_" + index + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xmlDoc;
        }




        /// <summary>
        /// Used in reading response XML and assigning hotel data to the response.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private List<HotelSearchResult> ReadHotelSearchResponse(XmlDocument xmlDoc, HotelRequest request, decimal markup, string markupType, decimal discount, string discountType)
        {
            HotelSearchResult[] hotelResults = null;
            //Loading All Hotels Static data city wise
            //optimized Code
            //  DataTable dtHotels = HotelStaticData.GetStaticHotelIds(request.CityCode, HotelBookingSource.RezLive);           
            //XmlDocument xmlDoc = new XmlDocument();
            try
            {
                //xmlDoc.LoadXml(response);
                XmlNode errorNode = xmlDoc.SelectSingleNode("/HotelFindResponse/error");
                if (errorNode != null && errorNode.InnerText.Length > 0)
                {
                    Audit.Add(EventType.RezLiveAvailSearch, Severity.High, 0, string.Concat(new object[]
                    {
                    " Rezlive:HotelSearchResponse,Error Message:",
                    errorNode.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + errorNode.InnerText);
                    throw new BookingEngineException("<br>" + errorNode.InnerText);
                }
                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                XmlNodeList hotelNodes = xmlDoc.SelectNodes("/HotelFindResponse/Hotels/Hotel");
                hotelResults = new HotelSearchResult[hotelNodes.Count];
                int n = 0;
                string pattern = request.HotelName;
                if (hotelNodes != null)
                {
                    foreach (XmlNode node in hotelNodes)
                    {
                        HotelSearchResult hotelResult = new HotelSearchResult();
                        hotelResult.HotelCode = node.SelectSingleNode("Id").InnerText;
                        try
                        {

                            //HotelStaticData hotelStaticData = StaticData.Find(h => h.HotelCode.Trim() == hotelResult.HotelCode);
                            HotelStaticData hotelStaticData = StaticData.Find(h => h.RezliveID.Trim() == hotelResult.HotelCode);
                            if (hotelStaticData != null)
                            {
                                hotelResult.HotelStaticID = hotelStaticData.HotelStaticID;
                                hotelResult.HotelDescription = !string.IsNullOrEmpty(hotelStaticData.HotelDescription) ? (GenericStatic.ReplaceNonASCII(hotelStaticData.HotelDescription.ToString())).Replace("\\", "") : "";
                                hotelResult.HotelAddress = !string.IsNullOrEmpty(hotelStaticData.HotelAddress) ? hotelStaticData.HotelAddress : "";
                                hotelResult.HotelMap = hotelStaticData.HotelMap;
                                hotelResult.HotelLocation = hotelStaticData.HotelLocation;
                                hotelResult.HotelContactNo = !string.IsNullOrEmpty(hotelStaticData.PhoneNumber) ? hotelStaticData.PhoneNumber : "";
                                hotelResult.HotelFacilities = !string.IsNullOrEmpty(hotelStaticData.HotelFacilities) ? GenericStatic.ReplaceNonASCII(hotelStaticData.HotelFacilities).Replace("\\", "").Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).Select(Convert.ToString).ToList() : null;
                                hotelResult.HotelImages = hotelStaticData.Images;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        catch { continue; }
                        hotelResult.Currency = xmlDoc.SelectSingleNode("/HotelFindResponse/Currency").InnerText;
                        hotelResult.StartDate = request.StartDate;
                        hotelResult.EndDate = request.EndDate;
                        hotelResult.HotelName = node.SelectSingleNode("Name").InnerText;
                        hotelResult.HotelPicture = node.SelectSingleNode("ThumbImages").InnerText;
                        //modified by brahmam some time rating given 3.5 that time we need to show 3.0 only
                        if (!string.IsNullOrEmpty(node.SelectSingleNode("Rating").InnerText))
                        {
                            hotelResult.Rating = (HotelRating)Convert.ToInt32(Math.Floor(Convert.ToDecimal(node.SelectSingleNode("Rating").InnerText)));
                        }
                        //hotelResult.Rating = (HotelRating)Convert.ToInt32(node.SelectSingleNode("Rating").InnerText);
                        hotelResult.CityCode = request.CityCode;
                        //Get the Hotel Static data,fill it if not exixt
                        hotelResult.RoomGuest = request.RoomGuest;

                        #region Modified by brahmam search time avoid Staticinfo downloading
                        //HotelDetails hotelInfo = new HotelDetails();
                        //    hotelInfo = GetHotelDetailsInformation(hotelResult.CityCode, hotelResult.HotelName, hotelResult.HotelCode);
                        //    if (string.IsNullOrEmpty(hotelInfo.HotelName))
                        //    {
                        //        Audit.Add(EventType.HotelSearch, Severity.High, 1, "Hotel Detail not found for RezLive : cityCode = " + hotelResult.CityCode + " itemCode = " + hotelResult.HotelCode, "0");
                        //        continue;
                        //    }
                        hotelResult.BookingSource = HotelBookingSource.RezLive;
                        #endregion

                        #region To get only those satisfy the search conditions
                        if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                        {
                            if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        #endregion

                        XmlNodeList roomNodes = node.SelectNodes("RoomDetails/RoomDetail");
                        HotelRoomsDetails[] rooms = new HotelRoomsDetails[roomNodes.Count * request.NoOfRooms];
                        HotelRoomsDetails roomdetail = default(HotelRoomsDetails);
                        rateOfExchange = exchangeRates[hotelResult.Currency];
                        int index = 0;
                        foreach (XmlNode rnode in roomNodes)
                        {
                            string noOfRooms = rnode.SelectSingleNode("TotalRooms").InnerText;
                            for (int i = 0; i < Convert.ToInt32(noOfRooms); i++)
                            {
                                roomdetail.RoomTypeName = rnode.SelectSingleNode("Type").InnerText.Split('|')[i];
                                roomdetail.SequenceNo = (i + 1).ToString();
                                roomdetail.CommonRoomKey=roomdetail.RatePlanCode = rnode.SelectSingleNode("BookingKey").InnerText;                                
                                roomdetail.IsIndividualSelection = false;//Here IsIndividualSelection = false means roomselection is combination.
                                string mealPlanDesc = rnode.SelectSingleNode("RoomDescription").InnerText;
                                if (!string.IsNullOrEmpty(mealPlanDesc))
                                {
                                    try
                                    {
                                        roomdetail.mealPlanDesc = mealPlanDesc.Split('|')[i];//2 rooms they giving one pipe symbol only
                                    }
                                    catch { roomdetail.mealPlanDesc = "Room Only"; }
                                }
                                else
                                {
                                    roomdetail.mealPlanDesc = "Room Only";
                                }

                                //if (!string.IsNullOrEmpty(mealPlanDesc))
                                //{
                                //    string[] mealLenght = mealPlanDesc.Split('|');
                                //    if (mealLenght.Length > i)
                                //    {
                                //        roomdetail.mealPlanDesc = mealPlanDesc.Split('|')[i];
                                //    }
                                //    else
                                //    {
                                //        roomdetail.mealPlanDesc = "Room Only";
                                //    }
                                //}
                                //else
                                //{
                                //    roomdetail.mealPlanDesc = "Room Only";
                                //}
                                roomdetail.RoomTypeCode = index.ToString() + "||" + roomdetail.RatePlanCode + "^" + roomdetail.SequenceNo;
                                roomdetail.Occupancy = new Dictionary<string, int>();
                                roomdetail.Occupancy.Add("MaxAdult", Convert.ToInt32(rnode.SelectSingleNode("Adults").InnerText.Split('|')[i]));
                                roomdetail.Occupancy.Add("MaxChild", Convert.ToInt32(rnode.SelectSingleNode("Children").InnerText.Split('|')[i]));
                                decimal totPrice = 0m;
                                totPrice = Convert.ToDecimal(rnode.SelectSingleNode("TotalRate").InnerText.Split('|')[i]);

                                //VAT Calucation Changes Modified 14.12.2017
                                decimal hotelTotalPrice = 0m;
                                decimal vatAmount = 0m;
                                hotelTotalPrice = Math.Round((totPrice) * rateOfExchange, decimalPoint);
                                if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                {
                                    hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                }
                                hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                                //price calculation
                                roomdetail.TotalPrice = hotelTotalPrice;
                                roomdetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomdetail.TotalPrice) * (markup / 100m)));
                                roomdetail.MarkupType = markupType;
                                roomdetail.MarkupValue = markup;
                                roomdetail.SellingFare = roomdetail.TotalPrice;
                                roomdetail.supplierPrice = totPrice;
                                roomdetail.TaxDetail = new PriceTaxDetails();
                                roomdetail.TaxDetail = priceTaxDet;
                                roomdetail.InputVATAmount = vatAmount;
                                roomdetail.TotalPrice = roomdetail.TotalPrice + roomdetail.Markup;
                                roomdetail.Discount = 0;
                                if (markup > 0 && discount > 0)
                                {
                                    roomdetail.Discount = discountType == "F" ? discount : roomdetail.Markup * (discount / 100);
                                }
                                hotelResult.Price = new PriceAccounts();
                                hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                hotelResult.Price.SupplierPrice = totPrice;
                                hotelResult.Price.RateOfExchange = rateOfExchange;

                                System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                decimal totalprice = roomdetail.TotalPrice;

                                for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                {
                                    decimal price = roomdetail.TotalPrice / diffResult.Days;
                                    if (fareIndex == diffResult.Days - 1)
                                    {
                                        price = totalprice;
                                    }
                                    totalprice -= price;
                                    hRoomRates[fareIndex].Amount = price;
                                    hRoomRates[fareIndex].BaseFare = price;
                                    hRoomRates[fareIndex].SellingFare = price;
                                    hRoomRates[fareIndex].Totalfare = price;
                                    hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                    hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);

                                }
                                roomdetail.Rates = hRoomRates;
                                roomdetail.SupplierName = HotelBookingSource.RezLive.ToString();
                                roomdetail.SupplierId = Convert.ToString((int)HotelBookingSource.RezLive);
                                rooms[index] = roomdetail;
                                index++;
                            }
                            hotelResult.RoomDetails = rooms.OrderBy(h => h.TotalPrice).ToArray();
                        }
                        hotelResult.Currency = agentCurrency;
                        hotelResults[n++] = hotelResult;
                    }
                    if (hotelResults.Length > n)
                    {
                        Array.Resize(ref hotelResults, n);
                    }

                    foreach (HotelSearchResult hotelResult in hotelResults)
                    {
                        //hotelResult.Price = new PriceAccounts();
                        for (int k = 0; k < request.NoOfRooms; k++)
                        {
                            for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                            {
                                if (hotelResult.RoomDetails[j].SequenceNo.Contains((k + 1).ToString()))
                                {
                                    hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice;
                                    hotelResult.Price.Discount += hotelResult.RoomDetails[j].Discount;
                                    hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                    hotelResult.Price.AccPriceType = PriceType.NetFare;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, 0, "Exception returned from Rezlive.ReadHotelSearchResponse() Error Message:" + ex.Message ,"");
            }
            return hotelResults.ToList();
        }

        public HotelDetails GetHotelDetailsInformation(string cityCode, string hotelName, string hotelCode)
        {
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData staticInfo = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();

            //get the info whether to update the static data
            int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.RezLiveConfig["TimeStamp"]);
            bool imageUpdate = true;
            bool isUpdateReq = false;
            bool isReqSend = false;

            string request = string.Empty;
            string resp = string.Empty;

            try
            {
                staticInfo.Load(hotelCode, cityCode, HotelBookingSource.RezLive);
                imageInfo.Load(hotelCode, cityCode, HotelBookingSource.RezLive);
                if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                {
                    imageUpdate = false;
                }
                if (staticInfo.HotelName != null && staticInfo.HotelName.Length > 0 && !string.IsNullOrEmpty(staticInfo.HotelDescription) && !string.IsNullOrEmpty(staticInfo.HotelAddress))
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(staticInfo.TimeStamp);
                    //if it is less than REquired time stamp days then load from DB
                    if (diffRes.Days > timeStampDays)
                    {
                        // Set the variable as true
                        isUpdateReq = true;
                        imageUpdate = true;
                    }
                    else
                    {
                        hotelInfo.HotelCode = staticInfo.HotelCode;
                        hotelInfo.HotelName = staticInfo.HotelName;
                        hotelInfo.Map = staticInfo.HotelMap;
                        hotelInfo.Description = staticInfo.HotelDescription;


                        string[] image = new string[0];
                        List<string> dldImgList = new List<string>();
                        {
                            string[] dldImages = imageInfo.Images.Split('|');
                            foreach (string img in dldImages)
                            {
                                if (!string.IsNullOrEmpty(img)) dldImgList.Add(img);
                            }
                        }
                        hotelInfo.Images = dldImgList;
                        //hotelInfo.Image = dldImgList.Count > 0 ? dldImgList[0] : "";
                        hotelInfo.Address = staticInfo.HotelAddress;
                        hotelInfo.PhoneNumber = staticInfo.PhoneNumber;
                        hotelInfo.PinCode = staticInfo.PinCode;
                        hotelInfo.Email = staticInfo.EMail;
                        hotelInfo.URL = staticInfo.URL;
                        hotelInfo.FaxNumber = staticInfo.FaxNumber;
                        Dictionary<string, string> attractions = new Dictionary<string, string>();
                        string[] attrList = staticInfo.SpecialAttraction.Split('|');
                        for (int i = 1; i < attrList.Length; i++)
                        {
                            attractions.Add(i.ToString() + ")", attrList[i - 1]);
                        }
                        hotelInfo.Attractions = attractions;
                        List<string> facilities = new List<string>();
                        string[] facilList = staticInfo.HotelFacilities.Split('|');
                        foreach (string facl in facilList)
                        {
                            facilities.Add(facl);
                        }
                        hotelInfo.HotelFacilities = facilities;

                        List<string> locations = new List<string>();
                        string[] locationList = staticInfo.HotelLocation.Split('|');
                        foreach (string loc in locationList)
                        {
                            locations.Add(loc);
                        }
                        hotelInfo.Location = locations;
                    }
                }
                else
                {
                    isReqSend = true;
                }
                if (isReqSend || isUpdateReq)
                {
                    hotelInfo = GetHotelDetails(hotelCode);
                    // Saving in Cache
                    if (hotelInfo.HotelName == null)
                    {
                        return hotelInfo;
                    }
                    else
                    {
                        staticInfo.HotelCode = hotelCode;
                        staticInfo.HotelName = hotelInfo.HotelName;
                        staticInfo.HotelAddress = hotelInfo.Address;
                        staticInfo.CityCode = cityCode;
                        List<string> facilities = hotelInfo.HotelFacilities;
                        string facilty = string.Empty;
                        foreach (string facl in facilities)
                        {
                            facilty += facl + "|";
                        }
                        staticInfo.HotelFacilities = GenericStatic.ReplaceNonASCII( facilty);
                        staticInfo.SpecialAttraction = string.Empty;

                        List<string> locations2 = hotelInfo.Location;
                        string location = string.Empty;
                        foreach (string loc in locations2)
                        {
                            location = location + loc + "|";
                        }
                        staticInfo.HotelLocation = location;
                        //staticInfo.HotelLocation = hotelInfo.PhoneNumber;
                        staticInfo.HotelMap = hotelInfo.Map;
                        staticInfo.HotelDescription = GenericStatic.ReplaceNonASCII( hotelInfo.Description);
                        staticInfo.FaxNumber = hotelInfo.FaxNumber;
                        staticInfo.EMail = hotelInfo.Email;
                        staticInfo.PinCode = !string.IsNullOrEmpty(hotelInfo.PinCode)? hotelInfo.PinCode.ToString():"";
                        staticInfo.PhoneNumber = hotelInfo.PhoneNumber;
                        staticInfo.URL = hotelInfo.URL;
                        staticInfo.Source = HotelBookingSource.RezLive;
                        staticInfo.Rating = hotelInfo.hotelRating;
                        staticInfo.Status = true;

                        staticInfo.Save();
                        //images
                        List<string> imgList = hotelInfo.Images;
                        string images = string.Empty;
                        foreach (string img in imgList)
                        {
                            images += img + "|";
                        }
                        imageInfo.Images = images;
                        imageInfo.CityCode = cityCode;
                        imageInfo.HotelCode = hotelCode;
                        imageInfo.Source = HotelBookingSource.RezLive;
                        imageInfo.DownloadedImgs = images;

                        if (isUpdateReq)
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Update();
                            }
                        }
                        else
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Save();
                            }
                        }
                    }

                    //Audit.Add(EventType.HotelSearch, Severity.High, 0, "Response Rezlive.HotelDetailsRequest request XML : " + request + "|response XML : " + resp, "");

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, 0, "Exception returned from Rezlive.HotelDetailsRequest Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                //Trace.TraceError("Error: " + ex.Message);
            }

            return hotelInfo;
        }

        #endregion


        #region GetCancellationPolicy
       //Overload method , not required to change any .aspx.cs pages. 
        public Dictionary<string, string> GetCancellationInfo(HotelItinerary itinerary, decimal price)
        {
            decimal markup = 0; string markuptype = "F";
            return GetCancellationInfo(itinerary, price, markup, markuptype);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetCancellationInfo(HotelItinerary itinerary, decimal price,decimal markup,string markupType)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();

            XmlDocument responseXML = GetCancellationInfoResponse(itinerary);
            if (responseXML != null && responseXML.ChildNodes !=null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("CancellationPolicyResponse"))
            {
                cancellationInfo = ReadCancellationInfoResponse(responseXML, price, itinerary,markup,markupType);
            }
            return cancellationInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GenerateCancellationInfoRequest(HotelItinerary itinerary)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("CancellationPolicyRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("HotelId");
                writer.WriteValue(itinerary.HotelCode);
                writer.WriteEndElement();
                writer.WriteStartElement("ArrivalDate");
                writer.WriteValue(itinerary.StartDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                //writer.WriteValue("28/01/2016");
                writer.WriteEndElement();
                writer.WriteStartElement("DepartureDate");
                writer.WriteValue(itinerary.EndDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                //writer.WriteValue("30/01/2016");
                writer.WriteEndElement();
                writer.WriteStartElement("CountryCode");
                writer.WriteValue(itinerary.DestinationCountryCode);
                writer.WriteEndElement();
                writer.WriteStartElement("City");
                writer.WriteValue(itinerary.CityCode);
                writer.WriteEndElement();
                writer.WriteStartElement("GuestNationality");
                writer.WriteValue(itinerary.PassengerNationality);
                writer.WriteEndElement();
                writer.WriteStartElement("Currency");
                writer.WriteValue(itinerary.Roomtype[0].Price.SupplierCurrency);
                writer.WriteEndElement();
                writer.WriteStartElement("RoomDetails");


                string roomName = "", bookingKey = "", adults = "", childs = "", childAges = "";

                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];

                    bookingKey = room.RatePlanCode;

                    if (roomName.Length > 0)
                    {
                        roomName += "|" + room.RoomName;
                    }
                    else
                    {
                        roomName = room.RoomName;
                    }

                    if (adults.Length > 0)
                    {
                        adults += "|" + room.AdultCount.ToString();
                    }
                    else
                    {
                        adults = room.AdultCount.ToString();
                    }

                    if (childs.Length > 0)
                    {
                        childs += "|" + room.ChildCount.ToString();
                    }
                    else
                    {
                        childs = room.ChildCount.ToString();
                    }

                    if (room.ChildAge!=null && room.ChildAge.Count > 0)
                    {
                        if (childAges.Length > 0)
                        {
                            childAges = childAges + "|";
                        }
                        string childrenAge = "";
                        foreach (int cage in room.ChildAge)
                        {
                            if (childrenAge.Length > 0)
                            {
                                childrenAge += "*" + cage;
                            }
                            else
                            {
                                childrenAge = cage.ToString();
                            }
                        }
                        childAges += childrenAge;
                    }
                    else
                    {
                        if (childAges.Length > 0)
                        {
                            childAges += "|0";
                        }
                        else
                        {
                            childAges = "0";
                        }
                    }
                }
                writer.WriteStartElement("RoomDetail");
                writer.WriteStartElement("BookingKey");
                writer.WriteValue(bookingKey);
                writer.WriteEndElement();//BookingKey
                writer.WriteStartElement("Adults");
                writer.WriteValue(adults);
                writer.WriteEndElement();//adults
                writer.WriteStartElement("Children");
                writer.WriteValue(childs);
                writer.WriteEndElement();
                writer.WriteStartElement("ChildrenAges");
                if (childAges.Length > 0)
                {
                    writer.WriteValue(childAges);
                }
                else
                {
                    writer.WriteValue("0");
                }
                writer.WriteEndElement();
                writer.WriteStartElement("Type");
                writer.WriteValue(roomName);
                writer.WriteEndElement();//Type
                writer.WriteEndElement();//RoomDetail

                writer.WriteEndElement();//RoomDetails
                writer.WriteEndElement();//CancellationPolicyRequest
                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "CancellationPolicyRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.GetCancellationPolicy, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed generate cancellation info request for Hotel : " + itinerary.HotelName, ex);
            }
            return "XML=" + HttpUtility.UrlEncode(request.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private XmlDocument GetCancellationInfoResponse(HotelItinerary itinerary)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["CancellationPolicy"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateCancellationInfoRequest(itinerary);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();
                ////Response.Write(responseFromServer);
                //reader.Close();
                //dataStream.Close();
                //response.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to get response for cancellation policy request", ex);
            }
            return xmlDoc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadCancellationInfoResponse(XmlDocument xmldoc, decimal hotelPrice, HotelItinerary itinerary,decimal markup, string markupType)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
            try
            {
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(responseXML);
                try
                {
                    string filePath = XmlPath + "CancellationPolicyResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.GetCancellationPolicy, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNodeList cancellationNodes = xmldoc.SelectNodes("/CancellationPolicyResponse/CancellationInformations/CancellationInformation");
                //double buffer = 0;
                //if (ConfigurationSystem.RezLiveConfig.ContainsKey("Buffer"))
                //{
                //    buffer = Convert.ToDouble(ConfigurationSystem.RezLiveConfig["Buffer"]);
                //}
                if (cancellationNodes != null)
                {
                    string cancelInfo = "";
                    string endDate = "";
                    //Cancel Policy Updates
                    List<HotelCancelPolicy> hotelPolicyList = new List<HotelCancelPolicy>();
                    foreach (XmlNode cancelNode in cancellationNodes)
                    {
                        //Cancel Policy Updates
                        HotelCancelPolicy hotelPolicy = new HotelCancelPolicy();

                        string startDate = cancelNode.SelectSingleNode("StartDate").InnerText;
                        startDate = (Convert.ToDateTime(startDate).AddDays(-(CnclBuffer))).ToString("dd MMM yyyy HH:mm:ss");
                        endDate = cancelNode.SelectSingleNode("EndDate").InnerText;
                        endDate = (Convert.ToDateTime(endDate).AddDays(-(CnclBuffer))).ToString("dd MMM yyyy HH:mm:ss");
                        string ChargeType = cancelNode.SelectSingleNode("ChargeType").InnerText;
                        decimal amount = Convert.ToDecimal(cancelNode.SelectSingleNode("ChargeAmount").InnerText);
                        string currency = cancelNode.SelectSingleNode("Currency").InnerText;
                        decimal rateOfExchange = exchangeRates[currency];
                      
                       amount= amount + ((markupType == "F") ? markup : ((amount) * (markup / 100m)));
                        //Cancel Policy Updates
                        string remarks = cancelNode.SelectSingleNode("Info") !=null ? cancelNode.SelectSingleNode("Info").InnerText : "";
                        hotelPolicy.FromDate = Convert.ToDateTime(startDate);
                        hotelPolicy.Todate = Convert.ToDateTime(endDate);
                        hotelPolicy.BufferDays = Convert.ToInt32(Math.Round(CnclBuffer));
                        hotelPolicy.ChargeType = ChargeType;
                        hotelPolicy.Currency = agentCurrency;

                        if (cancelInfo.Length == 0)
                        {
                            if (amount > 0)
                            {
                                if (ChargeType == "Amount")
                                {
                                    cancelInfo = agentCurrency + " " + Math.Round((amount/itinerary.NoOfRooms * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;
                                    //Cancel Policy Updates
                                    hotelPolicy.Amount = Math.Round((amount / itinerary.NoOfRooms * rateOfExchange), decimalPoint);
                                    hotelPolicy.Remarks = remarks != "" ? cancelInfo : remarks;
                                }
                                else if (ChargeType == "Percentage")
                                {       
                                    cancelInfo=amount + "% of amount will be charged between " + startDate + " and " + endDate;
                                    //Cancel Policy Updates
                                    hotelPolicy.Amount = amount;
                                    hotelPolicy.Remarks = remarks == "" ? cancelInfo : remarks;
                                }
                                else if (ChargeType == "Nightly")
                                {
                                    int duration = itinerary.EndDate.Subtract(itinerary.StartDate).Days;
                                    decimal perNight = (hotelPrice/ itinerary.NoOfRooms) / duration;
                                    cancelInfo = agentCurrency + " " + Math.Round((perNight * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;
                                    //Cancel Policy Updates
                                    hotelPolicy.Amount = Math.Round((perNight * rateOfExchange), decimalPoint);
                                    hotelPolicy.Remarks = remarks == "" ? cancelInfo : remarks;
                                }
                            }
                            else
                            {
                                cancelInfo = "No charges applicable between " + startDate + " and " + endDate;
                                hotelPolicy.Remarks = remarks == "" ? cancelInfo : remarks;
                            }
                        }
                        else
                        {
                            if (amount > 0)
                            {
                                if (ChargeType == "Amount")
                                {
                                    cancelInfo += "|" + agentCurrency + " " + Math.Round((amount / itinerary.NoOfRooms * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;
                                    //Cancel Policy Updates
                                    hotelPolicy.Amount = Math.Round((amount / itinerary.NoOfRooms * rateOfExchange), decimalPoint);
                                    hotelPolicy.Remarks = remarks == "" ? agentCurrency + " " + Math.Round((amount / itinerary.NoOfRooms * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate : remarks;
                                }
                                else if (ChargeType == "Percentage")
                                {                                    
                                    cancelInfo +="|" +amount + "% of amount will be charged between " + startDate + " and " + endDate;
                                    //Cancel Policy Updates
                                    hotelPolicy.Amount = amount;
                                    hotelPolicy.Remarks = remarks == "" ? amount + "% of amount will be charged between " + startDate + " and " + endDate : remarks;
                                }
                                else if (ChargeType == "Nightly")
                                {
                                    int duration = itinerary.EndDate.Subtract(itinerary.StartDate).Days;
                                    decimal perNight = (hotelPrice / itinerary.NoOfRooms) / duration;
                                    cancelInfo += "|" + agentCurrency + " " + Math.Round((perNight * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;
                                    //Cancel Policy Updates
                                    hotelPolicy.Amount = Math.Round((perNight * rateOfExchange), decimalPoint);
                                    hotelPolicy.Remarks = remarks == "" ? agentCurrency + " " + Math.Round((perNight * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate : remarks;
                                }
                            }
                            else
                            {
                                cancelInfo += "| No charges applicable between " + startDate + " and " + endDate;
                                hotelPolicy.Remarks = remarks == "" ? "No charges applicable between " + startDate + " and " + endDate : remarks;
                            }
                        }
                        //Cancel Policy Updates
                        hotelPolicyList.Add(hotelPolicy);
                    }
                    cancellationInfo.Add("lastCancellationDate", endDate);
                    cancellationInfo.Add("CancelPolicy", cancelInfo);
                    //Cancel Policy Updates
                    if (hotelPolicyList.Count > 0)
                    {
                        string cancelPolicyJsonString = JsonConvert.SerializeObject(hotelPolicyList);
                        cancellationInfo.Add("CancelPolicyJsonString", cancelPolicyJsonString);
                    }
                }
                string HotelPolicy = xmldoc.SelectSingleNode("/CancellationPolicyResponse/CancellationInformations/Info").InnerText;
                if (!string.IsNullOrEmpty(HotelPolicy))
                {
                    cancellationInfo.Add("HotelPolicy", HotelPolicy);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to Read cancellation response ", ex);
            }
            return cancellationInfo;
        }
        
            public Dictionary<string, string> getApiGetCancellationInfo(ref HotelSearchResult hResult, string roomTypeCode, HotelRequest request,decimal markup ,string markupType)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
            try
            {
                SimilarHotelSourceInfo rezliveHotel = hResult.SimilarHotels.Where(f => f.Source == HotelBookingSource.RezLive).FirstOrDefault();              
                HotelItinerary hotelItinerary = new HotelItinerary();
                hotelItinerary.EndDate = hResult.EndDate;
                hotelItinerary.StartDate = hResult.StartDate;
                hotelItinerary.HotelCode = (rezliveHotel == null) ? hResult.HotelCode : rezliveHotel.HotelCode; 
                hotelItinerary.CityCode = (rezliveHotel == null) ? hResult.CityCode: rezliveHotel.CityCode;
                hotelItinerary.DestinationCountryCode = (request.CountryName.Length!=2)?Country.GetCountryCodeFromCountryName(request.CountryName):request.CountryName;
                Dictionary<string, string> Countries = null;
                DOTWCountry dotw = new DOTWCountry();
                Countries = dotw.GetAllCountries();
                string nationality = string.Empty;
                if (request.PassengerNationality.Length == 2)
                {
                    nationality = request.PassengerNationality;
                }
                else
                {
                    nationality = Country.GetCountryCodeFromCountryName((request.PassengerNationality != null && Countries.ContainsKey(request.PassengerNationality)) ? Countries[request.PassengerNationality] : string.Empty);
                }
                hotelItinerary.PassengerNationality = nationality;
                hotelItinerary.PassengerCountryOfResidence = nationality;
                List<HotelRoomsDetails> RoomDetails = new List<HotelRoomsDetails>();
                string ratePlan = roomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1].Split('^')[0];
                RoomDetails.AddRange(hResult.RoomDetails.Where(x => x.RatePlanCode== ratePlan).ToList());
                List<CT.BookingEngine.HotelRoom> rooms = new List<CT.BookingEngine.HotelRoom>();
                for (int i = 0; i < RoomDetails.Count; i++)
                {
                    CT.BookingEngine.HotelRoom hotelRoom = new CT.BookingEngine.HotelRoom();
                    hotelRoom.RoomName = RoomDetails[i].RoomTypeName;
                    hotelRoom.MealPlanDesc = RoomDetails[i].mealPlanDesc;
                    hotelRoom.RatePlanCode = RoomDetails[i].RatePlanCode;
                    hotelRoom.Ameneties = RoomDetails[i].Amenities;
                    hotelRoom.RoomTypeCode = RoomDetails[i].RoomTypeCode;
                    hotelRoom.AdultCount = request.RoomGuest[i].noOfAdults;
                    hotelRoom.ChildCount = request.RoomGuest[i].noOfChild;
                    hotelRoom.ChildAge = request.RoomGuest[i].childAge;
                    hotelRoom.Price = new PriceAccounts();
                    hotelRoom.Price.SupplierCurrency = hResult.Price.SupplierCurrency;
                    hotelRoom.Price.SupplierPrice = hResult.Price.SupplierPrice;
                    rooms.Add(hotelRoom);
                }
                hotelItinerary.Roomtype = rooms.ToArray();
                hotelItinerary.NoOfRooms = rooms.Count;
                decimal supplierPrice = 0;
                foreach (CT.BookingEngine.HotelRoom room in hotelItinerary.Roomtype)
                {
                    supplierPrice += room.Price.SupplierPrice;
                }
                cancellationInfo= GetCancellationInfo(hotelItinerary, supplierPrice,markup,markupType);
                for (int i = 0; i < hResult.RoomDetails.Length; i++)
                {
                    if (hResult.RoomDetails[i].RatePlanCode == ratePlan)
                    {
                        hResult.RoomDetails[i].CancellationPolicy = cancellationInfo.ContainsKey("CancelPolicy")? cancellationInfo["CancelPolicy"]:"";
                        //Cancel Policy Updates
                        if(cancellationInfo.ContainsKey("CancelPolicyJsonString"))
                        {
                            List<HotelCancelPolicy> deserializedCancelPolicyList = JsonConvert.DeserializeObject<List<HotelCancelPolicy>>(cancellationInfo["CancelPolicyJsonString"]);
                            hResult.RoomDetails[i].CancelPolicyList = deserializedCancelPolicyList;
                        }
                    }

                }
            }
            catch (Exception ex)
            {               
                throw new Exception("RezLive: Failed to get Apicancellation response ", ex);
            }
            return cancellationInfo;
        }
        #endregion

        #region Book Hotel

        public BookingResponse Book(ref HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();

            XmlDocument responseXML = GetHotelBookingResponse(itinerary);
            if (responseXML != null && responseXML.ChildNodes !=null && responseXML.ChildNodes.Count >0 && responseXML.OuterXml.Contains("BookingResponse"))
            {
                bookResponse = ReadHotelBookingResponse(responseXML, ref itinerary);
            }
            else
            {
                bookResponse.Error = "Booking Failed";
            }
            return bookResponse;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GenerateHotelBookingRequest(HotelItinerary itinerary)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                #region XML Code
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\"");
                writer.WriteStartElement("BookingRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("Booking");
                writer.WriteStartElement("ArrivalDate");
                writer.WriteValue(itinerary.StartDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                //writer.WriteValue("28/01/2016");
                writer.WriteEndElement();

                writer.WriteStartElement("DepartureDate");
                writer.WriteValue(itinerary.EndDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                //writer.WriteValue("30/01/2016");
                writer.WriteEndElement();

                writer.WriteStartElement("GuestNationality");
                writer.WriteValue(itinerary.PassengerNationality);
                writer.WriteEndElement();

                writer.WriteStartElement("CountryCode");
                writer.WriteValue(itinerary.DestinationCountryCode);
                writer.WriteEndElement();

                writer.WriteStartElement("City");
                writer.WriteValue(itinerary.CityCode);
                writer.WriteEndElement();

                writer.WriteStartElement("HotelId");
                writer.WriteValue(itinerary.HotelCode);
                writer.WriteEndElement();

                writer.WriteStartElement("Name");
                writer.WriteValue(itinerary.HotelName);
                writer.WriteEndElement();

                writer.WriteStartElement("Currency");
                writer.WriteValue(itinerary.Roomtype[0].Price.SupplierCurrency);
                writer.WriteEndElement();

                writer.WriteStartElement("RoomDetails");


                string roomName = "", bookingKey = "", adults = "", childs = "", childAges = "", amount = "";

                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];

                    bookingKey = room.RatePlanCode;

                    if (roomName.Length > 0)
                    {
                        roomName += "|" + room.RoomName;
                    }
                    else
                    {
                        roomName = room.RoomName;
                    }


                    if (adults.Length > 0)
                    {
                        adults += "|" + room.AdultCount.ToString();
                    }
                    else
                    {
                        adults = room.AdultCount.ToString();
                    }

                    if (childs.Length > 0)
                    {
                        childs += "|" + room.ChildCount.ToString();
                    }
                    else
                    {
                        childs = room.ChildCount.ToString();
                    }
                    if (room.ChildAge!=null && room.ChildAge.Count > 0)
                    {
                        if (childAges.Length > 0 && !childAges.EndsWith("|"))
                        {
                            childAges += "|";
                        }

                        foreach (int cage in room.ChildAge)
                        {
                            if (room.ChildCount <= 1)
                            {
                                if (childAges.Length > 0)
                                {
                                    if (!childAges.EndsWith("|"))
                                    {
                                        childAges += "|";
                                    }
                                    childAges += cage;
                                }
                                else
                                {
                                    childAges = cage.ToString();
                                }
                            }
                            else
                            {
                                if (childAges.Length > 0)
                                {
                                    if (!childAges.EndsWith("|"))
                                    {
                                        childAges += "*" + cage;
                                    }
                                    else
                                    {
                                        childAges += cage.ToString();
                                    }
                                }
                                else
                                {
                                    childAges = cage.ToString();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (childAges.Length > 0)
                        {
                            childAges += "|0";
                        }
                        else
                        {
                            childAges = "0";
                        }
                    }
                    if (amount.Length > 0)
                    {
                        amount += "|" + (room.Price.SupplierPrice).ToString();
                    }
                    else
                    {
                        amount = (room.Price.SupplierPrice).ToString();
                    }
                }

                writer.WriteStartElement("RoomDetail");
                writer.WriteStartElement("Type");
                writer.WriteValue(roomName);
                writer.WriteEndElement();//Type
                writer.WriteStartElement("BookingKey");
                writer.WriteValue(bookingKey);
                writer.WriteEndElement();//BookingKey
                writer.WriteStartElement("Adults");
                writer.WriteValue(adults);
                writer.WriteEndElement();//adults
                writer.WriteStartElement("Children");
                writer.WriteValue(childs);
                writer.WriteEndElement();
                writer.WriteStartElement("ChildrenAges");
                if (childAges.Length > 0)
                {
                    writer.WriteValue(childAges);
                }
                else
                {
                    writer.WriteValue("0");
                }
                writer.WriteEndElement();
                writer.WriteStartElement("TotalRooms");
                writer.WriteValue(itinerary.NoOfRooms);
                writer.WriteEndElement();

                writer.WriteStartElement("TotalRate");
                writer.WriteValue(amount);
                writer.WriteEndElement();

                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];
                    writer.WriteStartElement("Guests");
                    foreach (HotelPassenger pax in room.PassenegerInfo)
                    {
                        string title = pax.Title;
                        title = title.Replace(".", "");
                        if (title.ToLower() == "mr")
                        {
                            title = "Mr";
                        }
                        else if (title.ToLower() == "ms")
                        {
                            title = "Ms";
                        }
                        else if (title.ToLower() == "mrs")
                        {
                            title = "Mrs";
                        }
                        writer.WriteStartElement("Guest");
                        writer.WriteStartElement("Salutation");
                        writer.WriteValue(title);
                        writer.WriteEndElement();

                        writer.WriteStartElement("FirstName");
                        writer.WriteValue(pax.Firstname);
                        writer.WriteEndElement();

                        writer.WriteStartElement("LastName");
                        writer.WriteValue(pax.Lastname);
                        writer.WriteEndElement();

                        if (pax.PaxType == HotelPaxType.Child)
                        {
                            writer.WriteStartElement("IsChild");
                            writer.WriteValue("1");
                            writer.WriteEndElement();
                            writer.WriteStartElement("Age");
                            writer.WriteValue(pax.Age);
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();//Guest
                    }
                    writer.WriteEndElement();//Guests
                }
                writer.WriteEndElement();//RoomDetail


                writer.WriteEndElement();//RoomDetails
                writer.WriteEndElement();//Booking
                writer.WriteEndElement();//BookingRequest

                writer.Flush();
                writer.Close();
                #endregion

                request = request.Replace("&lt;", "<");
                request = request.Replace("&gt;", ">");

                try
                {
                    string filePath = XmlPath + "HotelBookingRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to generate hotel booking request for " + itinerary.HotelName, ex);
            }

            return "XML=" + HttpUtility.UrlEncode(request.ToString());

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private XmlDocument GetHotelBookingResponse(HotelItinerary itinerary)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["HotelBooking"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelBookingRequest(itinerary);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();
                ////Response.Write(responseFromServer);
                //reader.Close();
                //dataStream.Close();
                //response.Close();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                Audit.Add(EventType.HotelBook, Severity.Normal, 1, "RezliveHotels" + err, "");
            }
            return xmlDoc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private BookingResponse ReadHotelBookingResponse(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {
            BookingResponse bookRes = new BookingResponse();
            try
            {
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "HotelBookingResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDoc.Save(filePath);
                    //Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
                XmlNode errorNode= xmlDoc.SelectSingleNode("HotelBookingResponse/error");
                if (errorNode != null && errorNode.InnerText.Length > 0)
                {
                    throw new BookingEngineException(errorNode.InnerText);
                }
                else
                {
                XmlNode status = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/status");
                XmlNode bookingStatus = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/BookingStatus");
                XmlNode bookingId = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/BookingId");
                XmlNode confirmationNo = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/BookingCode");



                if (confirmationNo != null && confirmationNo.InnerText.Length > 0)
                {
                    itinerary.ConfirmationNo = confirmationNo.InnerText;
                    bookRes.ConfirmationNo = confirmationNo.InnerText;
                    itinerary.BookingRefNo = bookingId.InnerText;

                    if (bookingStatus.InnerText == "Confirmed")
                    {
                        bookRes.Status = BookingResponseStatus.Successful;
                        itinerary.Status = HotelBookingStatus.Confirmed;
                        bookRes.Error = "";

                        itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: RezLive, as per final booking form reference No: " + confirmationNo.InnerText;
                    }
                    else
                    {
                        bookRes.Status = BookingResponseStatus.Failed;
                        if (bookingStatus.InnerText == "fail")
                        {
                            bookRes.Error = "Error from RezLive:Booking Failed";
                        }
                        else
                        {
                            bookRes.Error = bookingStatus.InnerText;
                        }
                    }
                }
                else
                {
                    bookRes.Status = BookingResponseStatus.Failed;
                    if (status != null)
                    {
                        XmlNode reason = xmlDoc.SelectSingleNode("BookingResponse/BookingDetails/reason");
                        bookRes.Error = reason.InnerText;
                    }
                    else
                    {
                        bookRes.Error = "status node is null";
                    }
                }
                    if (bookRes.Status == BookingResponseStatus.Failed && bookRes.Error.Length>0)
                    {
                        throw new BookingEngineException(bookRes.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                bookRes.Error = "Error from RezLive: " + ex.Message;
                bookRes.Status = BookingResponseStatus.Failed;
                throw new Exception("Booking failed reason :" + ex.Message, ex);
            }

            return bookRes;
        }

        #endregion


        #region Booking Confirmation

        public BookingResponse Confirmation(ref HotelItinerary itinerary)
        {
            BookingResponse bookRes = new BookingResponse();
            XmlDocument responseXML = GetHotelConfirmationResponse(itinerary);
            if (responseXML !=null && responseXML.ChildNodes !=null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("HotelConfirmationResponse"))
            {
                bookRes = ReadHotelConfirmationResponse(responseXML, ref itinerary);
            }
            else
            {
                bookRes.Status = BookingResponseStatus.Failed;
            }
            return bookRes;
        }

        private BookingResponse ReadHotelConfirmationResponse(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {
            BookingResponse bookRes = new BookingResponse();
            try
            {
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "HotelBookingConfirmResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDoc.Save(filePath);
                    // Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
                XmlNode hotelTelephoneNo = xmlDoc.SelectSingleNode("HotelConfirmationResponse/ConfirmationDetails/HotelTelephoneNo");
                XmlNode hotelStaffName = xmlDoc.SelectSingleNode("HotelConfirmationResponse/ConfirmationDetails/HotelStaffName");
                XmlNode confirmationNo = xmlDoc.SelectSingleNode("HotelConfirmationResponse/ConfirmationDetails/HotelConfirmationNo");
                XmlNode confirmationStatus = xmlDoc.SelectSingleNode("HotelConfirmationResponse/ConfirmationDetails/ConfirmationStatus");



                if (confirmationNo != null && confirmationNo.InnerText.Length > 0)
                {
                    itinerary.ConfirmationNo = confirmationNo.InnerText;
                    if (hotelTelephoneNo != null && hotelTelephoneNo.InnerText.Length > 0)
                    {
                        itinerary.SpecialRequest = "HotelTelephoneNo:" + hotelTelephoneNo.InnerText;
                    }
                    if (hotelStaffName != null && hotelStaffName.InnerText.Length > 0)
                    {
                        itinerary.SpecialRequest = itinerary.SpecialRequest + "-" + "HotelStaffName:" + hotelTelephoneNo.InnerText;
                    }

                    if (confirmationStatus.InnerText == "Confirmed")
                    {
                        bookRes.Status = BookingResponseStatus.Successful;
                        itinerary.Status = HotelBookingStatus.Confirmed;
                        bookRes.Error = "";

                        // itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: RezLive, as per final booking form reference No: " + confirmationNo.InnerText;
                    }
                    else
                    {
                        bookRes.Status = BookingResponseStatus.Failed;
                        if (confirmationStatus.InnerText == "fail")
                        {
                            bookRes.Error = "Error from RezLive:Booking Failed";
                        }
                        else
                        {
                            bookRes.Error = confirmationStatus.InnerText;
                        }
                    }
                }
                else
                {
                    bookRes.Status = BookingResponseStatus.Failed;
                }

            }
            catch (Exception ex)
            {
                bookRes.Error = "Error from RezLive: " + ex.Message;
                bookRes.Status = BookingResponseStatus.Failed;
                throw new Exception("Confirm failed reason :" + ex.Message, ex);
            }
            return bookRes;
        }

        /// <summary>
        /// GetHotelConfirmationResponse
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private XmlDocument GetHotelConfirmationResponse(HotelItinerary itinerary)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["HotelConfirm"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelConfirmRequest(itinerary);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();
                ////Response.Write(responseFromServer);


                //reader.Close();
                //dataStream.Close();
                //response.Close();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                Audit.Add(EventType.HotelBook, Severity.Normal, 1, "RezliveHotels" + err, "");
            }
            return xmlDoc;
        }

        /// <summary>
        /// genarate HoteConfirm Request
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private string GenerateHotelConfirmRequest(HotelItinerary itinerary)
        {
            StringBuilder request = new StringBuilder();
            string bookingId = itinerary.BookingRefNo.Split('|')[0];
            string bookingCode = itinerary.BookingRefNo.Split('|')[1];

            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("HotelConfirmationRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("Confirmation");
                writer.WriteStartElement("BookingId");
                writer.WriteValue(bookingId);
                writer.WriteEndElement();//BookingId
                writer.WriteStartElement("BookingCode");
                writer.WriteValue(bookingCode);
                writer.WriteEndElement();//BookingCode
                writer.WriteEndElement();//Confirmation
                writer.WriteEndElement(); //HotelConfirmationResponse
                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "HotelBookingConfirmRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    // Audit.Add(EventType.CancellBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to generate hotel Confirm request for Hotel : " + itinerary.HotelName, ex);
            }
            return "XML=" + HttpUtility.UrlEncode(request.ToString());
        }

        #endregion


        #region Get Hotel Details

        public HotelDetails GetHotelDetails(string hotelId)
        {
            HotelDetails hotelDetails = new HotelDetails();

            XmlDocument responseXML = GetHotelDetailResponse(hotelId);

            if (responseXML !=null && responseXML.ChildNodes !=null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("HotelDetailsResponse"))
            {
                hotelDetails = ReadHotelDetailsResponse(responseXML);
            }
            else
            {
                Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, "HotelDetails Not found" + hotelId.ToString(), "127.0.0.1");
            }

            return hotelDetails;
        }

        private string GenerateHotelDetailRequest(string hotelId)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("HotelDetailsRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("Hotels");
                writer.WriteStartElement("HotelId");
                writer.WriteValue(hotelId);
                writer.WriteEndElement();//HotelId
                writer.WriteEndElement();//Hotels
                writer.WriteEndElement();//HotelDetailsRequest

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "HotelDetailsRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to generate hotel detail request for Hotel : " + hotelId, ex);
            }

            return "XML=" + HttpUtility.UrlEncode(request.ToString());
        }

        private XmlDocument GetHotelDetailResponse(string hotelId)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["HotelDetails"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelDetailRequest(hotelId);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();
                //Response.Write(responseFromServer);


                //reader.Close();
                //dataStream.Close();
                //response.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to get response for Hotel Detail request for HotelId : " + hotelId, ex);
            }
            return xmlDoc;
        }

        private HotelDetails ReadHotelDetailsResponse(XmlDocument xmldoc)
        {
            HotelDetails hotelDetails = new HotelDetails();

            try
            {
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "HotelDetailsResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    try
                    {
                        xmldoc.Save(filePath);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.HotelSearch, Severity.Normal, 0, "HotelDetailsRezLiveXml Not Saveing : " + ex.Message + "", "1");
                    }
                    //Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNode hotelDetail = xmldoc.SelectSingleNode("/HotelDetailsResponse/Hotels");

                string error = string.Empty;
                if (hotelDetail.FirstChild.Name == "Error")
                {
                    error = hotelDetail.SelectSingleNode("Error").InnerText;
                }

                if (error.Length <= 0)
                {
                    hotelDetails.HotelCode = hotelDetail.SelectSingleNode("HotelId").InnerText;
                    hotelDetails.HotelName = hotelDetail.SelectSingleNode("HotelName").InnerText;
                    //modified by brahmam some time rating given 3.5 that time we need to show 3.0 only
                    if (!string.IsNullOrEmpty(hotelDetail.SelectSingleNode("Rating").InnerText))
                    {
                        hotelDetails.hotelRating = (HotelRating)Convert.ToInt32(Math.Floor(Convert.ToDecimal(hotelDetail.SelectSingleNode("Rating").InnerText)));
                    }
                    hotelDetails.CountryName = hotelDetail.SelectSingleNode("Country").InnerText;
                    hotelDetails.Address = hotelDetail.SelectSingleNode("HotelAddress").InnerText;
                    hotelDetails.URL = hotelDetail.SelectSingleNode("Website").InnerText;
                    hotelDetails.Email = hotelDetail.SelectSingleNode("Email").InnerText;
                    hotelDetails.Description = hotelDetail.SelectSingleNode("Description").InnerText;
                    string[] hotelFacilities = hotelDetail.SelectSingleNode("HotelAmenities").InnerText.Split(',');

                    hotelDetails.HotelFacilities = new List<string>();
                    foreach (string fac in hotelFacilities)
                    {
                        hotelDetails.HotelFacilities.Add(fac);
                    }
                    hotelDetails.RoomFacilities = new List<string>();
                    string[] roomFacilities = hotelDetail.SelectSingleNode("RoomAmenities").InnerText.Split(',');
                    foreach (string fac in roomFacilities)
                    {
                        hotelDetails.RoomFacilities.Add(fac);
                    }
                    hotelDetails.Images = new List<string>();
                    XmlNodeList images = hotelDetail.SelectNodes("Images/Image");

                    foreach (XmlNode img in images)
                    {
                        hotelDetails.Images.Add(img.InnerText);
                    }
                    hotelDetails.FaxNumber = hotelDetail.SelectSingleNode("Fax").InnerText;
                    hotelDetails.PhoneNumber = hotelDetail.SelectSingleNode("Phone").InnerText;
                    hotelDetails.Location = new List<string>();
                    //hotelDetails.Location.Add(hotelDetail.SelectSingleNode("Latitude").InnerText);
                    //hotelDetails.Location.Add(hotelDetail.SelectSingleNode("Longitude").InnerText);
                    hotelDetails.Location.Add(hotelDetail.SelectSingleNode("Location").InnerText);
                    hotelDetails.Map = hotelDetail.SelectSingleNode("Latitude").InnerText + "|" + hotelDetail.SelectSingleNode("Longitude").InnerText;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to read Hotel Details response ", ex);
            }

            return hotelDetails;
        }

        #endregion


        #region Hotel Booking Cancellation

        public Dictionary<string, string> CancelHotelBooking(string hotelBookingCode,string hotelBookingId)
        {
            Dictionary<string, string> cancelCharges = new Dictionary<string, string>();

            XmlDocument responseXML = GetHotelCancellationResponse(hotelBookingId, hotelBookingCode);

            if (responseXML !=null && responseXML.ChildNodes !=null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("CancellationResponse"))
            {
                cancelCharges = ReadHotelCancellationResponse(responseXML);
            }

            return cancelCharges;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelBookingId"></param>
        /// <param name="hotelBookingCode"></param>
        /// <returns></returns>
        private string GenerateHotelCancellationRequest(string hotelBookingId, string hotelBookingCode)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("CancellationRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("Cancellation");
                writer.WriteStartElement("BookingId");
                writer.WriteValue(hotelBookingId);
                writer.WriteEndElement();//BookingId
                writer.WriteStartElement("BookingCode");
                writer.WriteValue(hotelBookingCode);
                writer.WriteEndElement();//BookingCode
                writer.WriteEndElement();//Cancellation
                writer.WriteEndElement();//CancellationRequest

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath =XmlPath + "BookingCancelRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.CancellBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to generate hotel detail request for Hotel : " + hotelBookingId, ex);
            }

            return "XML=" + HttpUtility.UrlEncode(request.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelBookingId"></param>
        /// <param name="hotelBookingCode"></param>
        /// <returns></returns>
        private XmlDocument GetHotelCancellationResponse(string hotelBookingId, string hotelBookingCode)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["HotelCancel"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelCancellationRequest(hotelBookingId, hotelBookingCode);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();
                ////Response.Write(responseFromServer);


                //reader.Close();
                //dataStream.Close();
                //response.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to get response for Hotel Detail request for HotelId : " + hotelBookingId, ex);
            }
            return xmlDoc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadHotelCancellationResponse(XmlDocument xmldoc)
        {
            Dictionary<string, string> cancelData = new Dictionary<string, string>();
            try
            {
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(response);
                try
                {
                    string filePath = XmlPath + "BookingCancelResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.CancellBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNode error = xmldoc.SelectSingleNode("/CancellationResponse/Error");
                XmlNode status = xmldoc.SelectSingleNode("/CancellationResponse/Status");
                XmlNode amount = xmldoc.SelectSingleNode("/CancellationResponse/CancellationCharges");
                XmlNode currency = xmldoc.SelectSingleNode("/CancellationResponse/Currency");

                if (error != null && error.InnerText.Length > 0)
                {
                    cancelData.Add("Error", error.InnerText);
                }
                else
                {
                    if (status != null && status.InnerText == "true")
                    {
                        cancelData.Add("Status", "Cancelled");
                        if (amount != null)
                        {
                            cancelData.Add("Amount", amount.InnerText);
                        }
                        else
                        {
                            cancelData.Add("Amount", "0");
                        }
                        if (currency != null)
                        {
                            cancelData.Add("Currency", currency.InnerText);
                        }
                        else
                        {
                            cancelData.Add("Currency", "AED");
                        }
                    }
                    else
                    {
                        cancelData.Add("Status", "Failed");
                    }
                }
            }
            catch (Exception ex)
            {
                cancelData.Add("Error", ex.Message);
                throw new Exception("Failed to read Hotel cancellation response", ex);
            }

            return cancelData;
        }

        #endregion



        #region Get Pre Booking Info

        /// <summary>
        ///  Get the Hotels PreBooking Information.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="hotelRequest"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetPreBooking(HotelItinerary itinerary, decimal price)
        {
            Dictionary<string, string> preBookInfo = new Dictionary<string, string>();

            XmlDocument responseXML = GetPreBookInfoResponse(itinerary);
            if (responseXML !=null && responseXML.ChildNodes.Count > 0 && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("PreBookingResponse"))
            {
                preBookInfo = ReadPreBookingInfoResponse(responseXML, price, itinerary);
            }
            return preBookInfo;
        }


        /// <summary>
        ///  
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="hotelRequest"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        private XmlDocument GetPreBookInfoResponse(HotelItinerary itinerary)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.RezLiveConfig["PreBooking"]);
                request.Method = "POST"; //Using POST method       
                string postData = GeneratePreBookingInfoRequest(itinerary);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();
                //Response.Write(responseFromServer);


                //reader.Close();
                //dataStream.Close();
                //response.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to get response for prebooking request", ex);
            }
            return xmlDoc;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="hotelRequest"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        private string GeneratePreBookingInfoRequest(HotelItinerary itinerary)
        {
            StringBuilder request = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" ");
                writer.WriteStartElement("PreBookingRequest");
                writer.WriteStartElement("Authentication");
                writer.WriteStartElement("AgentCode");
                writer.WriteValue(_agentCode);
                writer.WriteEndElement();
                writer.WriteStartElement("UserName");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteEndElement();//Authentication

                writer.WriteStartElement("PreBooking");

                writer.WriteStartElement("ArrivalDate");
                writer.WriteValue(itinerary.StartDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                //writer.WriteValue("28/01/2016");
                writer.WriteEndElement();
                writer.WriteStartElement("DepartureDate");
                writer.WriteValue(itinerary.EndDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                //writer.WriteValue("30/01/2016");
                writer.WriteEndElement();
                writer.WriteStartElement("GuestNationality");
                writer.WriteValue(itinerary.PassengerNationality);
                writer.WriteEndElement();
                writer.WriteStartElement("CountryCode");
                writer.WriteValue(itinerary.DestinationCountryCode);
                writer.WriteEndElement();
                writer.WriteStartElement("City");
                writer.WriteValue(itinerary.CityCode);
                writer.WriteEndElement();
                writer.WriteStartElement("HotelId");
                writer.WriteValue(itinerary.HotelCode);
                writer.WriteEndElement();
                writer.WriteStartElement("Currency");
                writer.WriteValue(itinerary.Roomtype[0].Price.SupplierCurrency);
                writer.WriteEndElement();
                writer.WriteStartElement("RoomDetails");
                {
                    string roomName = "", bookingKey = "", adults = "", childs = "", childAges = "", amount = "";

                    for (int i = 0; i < itinerary.Roomtype.Length; i++)
                    {
                        CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];

                        bookingKey = room.RatePlanCode;

                        if (roomName.Length > 0)
                        {
                            roomName += "|" + room.RoomName;
                        }
                        else
                        {
                            roomName = room.RoomName;
                        }

                        if (amount.Length > 0)
                        {
                            amount += "|" + room.Price.SupplierPrice.ToString();
                        }
                        else
                        {
                            amount = room.Price.SupplierPrice.ToString();
                        }

                        if (adults.Length > 0)
                        {
                            adults += "|" + room.AdultCount.ToString();
                        }
                        else
                        {
                            adults = room.AdultCount.ToString();
                        }

                        if (childs.Length > 0)
                        {
                            childs += "|" + room.ChildCount.ToString();
                        }
                        else
                        {
                            childs = room.ChildCount.ToString();
                        }
                        if (room.ChildAge!=null && room.ChildAge.Count > 0)
                        {
                            if (childAges.Length > 0)
                            {
                                childAges = childAges + "|";
                            }
                            string childrenAge = "";
                            foreach (int cage in room.ChildAge)
                            {
                                if (childrenAge.Length > 0)
                                {
                                    childrenAge += "*" + cage;
                                }
                                else
                                {
                                    childrenAge = cage.ToString();
                                }
                            }
                            childAges += childrenAge;
                        }
                        else
                        {
                            if (childAges.Length > 0)
                            {
                                childAges += "|0";
                            }
                            else
                            {
                                childAges = "0";
                            }
                        }
                    }
                    writer.WriteStartElement("RoomDetail");
                    writer.WriteStartElement("Type");
                    writer.WriteValue(roomName);
                    writer.WriteEndElement();//Type
                    writer.WriteStartElement("BookingKey");
                    writer.WriteValue(bookingKey);
                    writer.WriteEndElement();//BookingKey
                    writer.WriteStartElement("Adults");
                    writer.WriteValue(adults);
                    writer.WriteEndElement();//adults
                    writer.WriteStartElement("Children");
                    writer.WriteValue(childs);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ChildrenAges");
                    if (childAges.Length > 0)
                    {
                        writer.WriteValue(childAges);
                    }
                    else
                    {
                        writer.WriteValue("0");
                    }
                    writer.WriteEndElement();
                    writer.WriteStartElement("TotalRooms");
                    writer.WriteValue(itinerary.NoOfRooms);
                    writer.WriteEndElement();//no of rooms
                    writer.WriteStartElement("TotalRate");
                    writer.WriteValue(amount);
                    writer.WriteEndElement();//TotalRate

                    writer.WriteStartElement("TermsAndConditions");
                    writer.WriteEndElement();//TermsAndConditions
                    writer.WriteEndElement();//RoomDetail
                }
                writer.WriteEndElement();//RoomDetails
                writer.WriteEndElement();//PreBooking
                writer.WriteEndElement();//PreBookingRequest
                writer.Flush();
                writer.Close();

                try
                {
                    string filePath =XmlPath + "PreBookingRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(request.ToString());
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed generate prebooking info request for Hotel : " + itinerary.HotelName, ex);
            }
            return "XML=" + HttpUtility.UrlEncode(request.ToString());
        }

        /// <summary>
        /// Read preBooking Info
        /// </summary>
        /// <param name="responseXML"></param>
        /// <param name="hotelPrice"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadPreBookingInfoResponse(XmlDocument xmldoc, decimal hotelPrice, HotelItinerary itinerary)
        {
            Dictionary<string, string> preBookInfo = new Dictionary<string, string>();
            try
            {
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(responseXML);
                try
                {
                    string filePath = XmlPath + "preBookingResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmldoc.Save(filePath);
                    // Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                string bookingKey = xmldoc.SelectSingleNode("/PreBookingResponse/PreBookingRequest/PreBooking/RoomDetails/RoomDetail/BookingKey").InnerText;
                if (!string.IsNullOrEmpty(bookingKey))
                {
                    preBookInfo.Add("BookingKey", bookingKey);
                }
                string termsAndConditions = xmldoc.SelectSingleNode("/PreBookingResponse/PreBookingRequest/PreBooking/RoomDetails/RoomDetail/TermsAndConditions").InnerText;
                double buffer = 0;
                if (ConfigurationSystem.RezLiveConfig.ContainsKey("Buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.RezLiveConfig["Buffer"]);
                }
                XmlNodeList cancellationNodes = xmldoc.SelectNodes("/PreBookingResponse/PreBookingRequest/PreBooking/CancellationInformations/CancellationInformation");
                if (cancellationNodes != null)
                {
                    string cancelInfo = "";
                    string endDate = "";
                    foreach (XmlNode cancelNode in cancellationNodes)
                    {
                        string startDate = cancelNode.SelectSingleNode("StartDate").InnerText;
                        startDate = (Convert.ToDateTime(startDate).AddDays(-(buffer))).ToString("dd MMM yyyy HH:mm:ss");
                        endDate = cancelNode.SelectSingleNode("EndDate").InnerText;
                        endDate = (Convert.ToDateTime(endDate).AddDays(-(buffer))).ToString("dd MMM yyyy HH:mm:ss");
                        string ChargeType = cancelNode.SelectSingleNode("ChargeType").InnerText;
                        decimal amount = Convert.ToDecimal(cancelNode.SelectSingleNode("ChargeAmount").InnerText);
                        string currency = cancelNode.SelectSingleNode("Currency").InnerText;
                        decimal rateOfExchange = exchangeRates[currency];
                        if (cancelInfo.Length == 0)
                        {
                            if (amount > 0)
                            {
                                if (ChargeType == "Amount")
                                {

                                    cancelInfo = agentCurrency + " " + Math.Round((amount * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;


                                }
                                else if (ChargeType == "Percentage")
                                {
                                    cancelInfo = amount + "% of amount will be charged between " + startDate + " and " + endDate;

                                }
                                else if (ChargeType == "Nightly")
                                {
                                    int duration = itinerary.EndDate.Subtract(itinerary.StartDate).Days;
                                    decimal perNight = hotelPrice / duration;

                                    cancelInfo = agentCurrency + " " + Math.Round((perNight * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;

                                }
                            }
                            else
                            {
                                cancelInfo = "No charges applicable between " + startDate + " and " + endDate;
                            }
                        }
                        else
                        {
                            if (amount > 0)
                            {
                                if (ChargeType == "Amount")
                                {

                                    cancelInfo += "|" + agentCurrency + " " + Math.Round((amount * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;

                                }
                                else if (ChargeType == "Percentage")
                                {
                                    cancelInfo += "|" +amount + "% of amount will be charged between " + startDate + " and " + endDate;

                                }
                                else if (ChargeType == "Nightly")
                                {
                                    int duration = itinerary.EndDate.Subtract(itinerary.StartDate).Days;
                                    decimal perNight = hotelPrice / duration;
                                    cancelInfo += "|" + agentCurrency + " " + Math.Round((perNight * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;

                                }
                            }
                            else
                            {
                                cancelInfo += "| No charges applicable between " + startDate + " and " + endDate;
                            }
                        }
                    }
                    preBookInfo.Add("lastCancellationDate", endDate);
                    preBookInfo.Add("CancelPolicy", cancelInfo);
                }
                string HotelPolicy = xmldoc.SelectSingleNode("/PreBookingResponse/PreBookingRequest/PreBooking/CancellationInformations/Info").InnerText;

                if (!string.IsNullOrEmpty(termsAndConditions))
                {
                    HotelPolicy += "|" + termsAndConditions;
                }
                if (!string.IsNullOrEmpty(HotelPolicy))
                {
                    preBookInfo.Add("HotelPolicy", HotelPolicy);
                }
                string status = xmldoc.SelectSingleNode("/PreBookingResponse/PreBookingDetails/Status").InnerText;
                string difference = xmldoc.SelectSingleNode("/PreBookingResponse/PreBookingDetails/Difference").InnerText;
                if (status == "True")
                {
                    if (Convert.ToDecimal(difference) <= 1)
                    {
                        preBookInfo.Add("Status", "true");
                        preBookInfo.Add("Error", "");
                    }
                    else
                    {
                        preBookInfo.Add("Status", "false");
                        preBookInfo.Add("Error", "PriceChanged");
                    }
                }
                else
                {
                    preBookInfo.Add("Status", "false");
                    XmlNode reason = xmldoc.SelectSingleNode("/PreBookingResponse/PreBookingDetails/Reason");
                    if (reason != null)
                    {
                        preBookInfo.Add("Error", reason.InnerText);
                    }
                    else
                    {
                        preBookInfo.Add("Error", "status node is null");
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("RezLive: Failed to Read cancellation response ", ex);
            }
            return preBookInfo;
        }
        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~XmlHub()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class SearchRequest
    {
        private DateTime _arrivalDate;
        private DateTime _departureDate;
        private string _countryCode;
        private string _cityName;
        private string _nationality;
        private List<int> _hotelRating;
        private List<HotelRoom> _hotelRooms;

        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set { _arrivalDate = value; }
        }

        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set { _departureDate = value; }
        }

        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }

        public string Nationality
        {
            get { return _nationality; }
            set { _nationality = value; }
        }

        public List<int> HotelRating
        {
            get { return _hotelRating; }
            set { _hotelRating = value; }
        }

        public List<HotelRoom> HotelRooms
        {
            get { return _hotelRooms; }
            set { _hotelRooms = value; }
        }
    }

    public class HotelRoom
    {
        private string _roomType;
        private string _roomDescription;
        private int _adults;
        private int _childs;
        private List<int> _childAges;
        private string _bookingKey;
        private int _totalRooms;
        private decimal _totalRate;
        private string _cancellationPolicy;
        private string _description;
        private string _termsAndConditions;
        private string _roomPax;

        public string RoomType
        {
            get { return _roomType; }
            set { _roomType = value; }
        }

        public string RoomDescription
        {
            get { return _roomDescription; }
            set { _roomDescription = value; }
        }

        public int Adults
        {
            get { return _adults; }
            set { _adults = value; }
        }

        public int Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }

        public List<int> ChildAges
        {
            get { return _childAges; }
            set { _childAges = value; }
        }

        public string BookingKey
        {
            get { return _bookingKey; }
            set { _bookingKey = value; }
        }
        public int TotalRooms
        {
            get { return _totalRooms; }
            set { _totalRooms = value; }
        }
        public decimal TotalRate
        {
            get { return _totalRate; }
            set { _totalRate = value; }
        }
        public string CancellationPolicy
        {
            get { return _cancellationPolicy; }
            set { _cancellationPolicy = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string TermsAndConditions
        {
            get { return _termsAndConditions; }
            set { _termsAndConditions = value; }
        }

        public string RoomPax
        {
            get { return _roomPax; }
            set { _roomPax = value; }
        }
    }

    public class SearchResponse
    {
        private DateTime _arrivalDate;
        private DateTime _departureDate;
        private string _currencyCode;
        private string _guestNationality;
        private List<Hotel> _hotels;
        private string _errorMessage;

        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set { _arrivalDate = value; }
        }

        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set { _departureDate = value; }
        }

        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }

        public string GuestNationality
        {
            get { return _guestNationality; }
            set { _guestNationality = value; }
        }

        public List<Hotel> Hotels
        {
            get { return _hotels; }
            set { _hotels = value; }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
    }

    public class Hotel
    {
        private string _searchSessionId;
        private string _id;
        private string _name;
        private int _rating;
        private string _description;
        private string _location;
        private string _address;
        private string _city;
        private string _lattitude;
        private string _longitude;
        private List<string> _amenities;
        private List<string> _roomAmenities;
        private List<string> _thumbImages;
        private decimal _price;
        private List<HotelRoom> _rooms;

        public string SearchSessionId
        {
            get { return _searchSessionId; }
            set { _searchSessionId = value; }
        }
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int Rating
        {
            get { return _rating; }
            set { _rating = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string Lattitude
        {
            get { return _lattitude; }
            set { _lattitude = value; }
        }
        public string Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public List<string> Amenities
        {
            get { return _amenities; }
            set { _amenities = value; }
        }
        public List<string> RoomAmenities
        {
            get { return _roomAmenities; }
            set { _roomAmenities = value; }
        }
        public List<string> ThumbImages
        {
            get { return _thumbImages; }
            set { _thumbImages = value; }
        }
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public List<HotelRoom> Rooms
        {
            get { return _rooms; }
            set { _rooms = value; }
        }
    }
}
