﻿using CT.Configuration;
using CT.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;

namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// This class is used to interact with AgodaApi.
    /// HotelBookingSource Value for Agoda=17
    /// Following is the steps in Booking Flow for this source
    /// STEP-1:HotelSearch(Availability)
    /// STEP-2:Every 15days(nwhat we configuraed in App.config) Need to download StaticData
    /// STEP-3:Booking (Booking)
    /// STEP-4:Cancel
    /// </summary>
    public class Agoda:IDisposable
    {
        /***************COMPLETE BOOKING FLOW OF Agoda---- Develop by Brahmam****************************************
        * 
        * STEP-1:HotelSearch(Availability)
        * STEP-2:Booking (Booking)
        */
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        private string XmlPath = string.Empty;
        /// <summary>
        /// siteId will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string siteId = string.Empty;
        /// <summary>
        ///  apiKey will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string apiKey = string.Empty;
        /// <summary>
        /// This variable is used, to send the language(response will be returned in same language Ex:en-us) to api, which is read from api configuration file
        /// </summary>
        private string language = string.Empty;
        /// <summary>
        /// This variable is used to store the uniqueId(booking time need to pass our uniqueId)
        /// </summary>
        private string clientReferenceNo = string.Empty;
        /// <summary>
        /// This variable is used,to send the currency(response will be returned in same currency Ex:AED) to api, which is read from api configuration file
        /// </summary>
        string currency = string.Empty;
        /// <summary>
        /// This variable is used,to Load exchange rates
        /// </summary>
        private StaticData staticInfo = new StaticData();

        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        private decimal rateOfExchange = 1;
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        private Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        private int decimalPoint;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        private string agentCurrency;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        /// <summary>
        /// This variable is used,to store the login userid,which is read from MetaserchEngine  
        /// </summary>
        int appUserId;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        string sessionId;
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int AgentDecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        /// <summary>
        ///login UserId
        /// </summary>
        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string SessionId
        {
            get
            {
                return sessionId;
            }

            set
            {
                sessionId = value;
            }
        }
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public Agoda()
        {
            LoadCredentials();
        }

        /// <summary>
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        private void LoadCredentials()
        {
            //Create Hotel Xml Log path folder per day wise
            XmlPath = ConfigurationSystem.AgodaConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            siteId = ConfigurationSystem.AgodaConfig["SiteID"];
            apiKey = ConfigurationSystem.AgodaConfig["APIKey"];
            language = ConfigurationSystem.AgodaConfig["lang"];
            currency = ConfigurationSystem.AgodaConfig["currency"];
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }
        #endregion
        #region commonMethods

        /// <summary>
        /// Method for retriving response based on the POST request and url
        /// </summary>
        /// <param name="requeststring">Request Object</param>
        /// <param name="url">Api url</param>
        /// <returns>XmlDocument</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private XmlDocument SendRequest(string requeststring, string url)
        {
            XmlDocument xmlDoc = new XmlDocument();
            //string responseFromServer = "";
            try
            {
                string contentType = "application/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST"; //Using POST method       
                string postData = requeststring;// GETTING XML STRING...
                request.ContentType = contentType;
                //ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //ServicePointManager.DefaultConnectionLimit = 9999;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                //Basic Authentication required to process the request.               
                string _auth = string.Format("{0}:{1}", siteId, apiKey);
                request.Headers.Add(HttpRequestHeader.Authorization, _auth);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaAvailSearch, Severity.Normal, 1, "Agoda.SendReguest Err :"+ex.ToString() + url, "0");
                throw new Exception("Agoda Failed to get response from Hotel request : " + ex.Message, ex);
            }
            return xmlDoc;
        }
        /// <summary>
        ///This method is converting ContentEncoding to Decompress(i.e GZIP, Deflate, default)
        /// </summary>
        /// <param name="webResponse">Response Object</param>
        /// <returns>Stream</returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        /// <summary>
        /// Method for retrieving response based on the GET request and url
        /// </summary>
        /// <param name="url">url</param>
        /// <returns>string</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private string SendGetRequest(string url)
        {
            string responseFromServer = "";
            try
            {
                string contentType = "application/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET"; //Using GET method       
                //string postData = requeststring;// GETTING XML STRING...
                request.ContentType = contentType;
                //ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //ServicePointManager.DefaultConnectionLimit = 9999;
               // byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                //Basic Authentication required to process the request.               
                string _auth = string.Format("{0}:{1}", siteId, apiKey);
                request.Headers.Add(HttpRequestHeader.Authorization, _auth);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                //request.ContentLength = bytes.Length;

                //Stream requestWriter = (request.GetRequestStream());

                //requestWriter.Write(bytes, 0, bytes.Length);
                //requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;


                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Agoda Failed to get response from Hotel request : " + ex.Message, ex);
            }
            return responseFromServer;
        }
        #endregion

        #region GetHotelAvailabilityRequestV2
        /*********************multithreading **********************/
        /// <summary>
        /// Creating HotelReuest object
        /// </summary>
        HotelRequest request = new HotelRequest();
        /// <summary>
        /// B2B Markup
        /// </summary>
        decimal markup = 0;
        /// <summary>
        /// Markup Type like (F OR P)
        /// </summary>
        string markupType = string.Empty;
        //int rowStartIndex = 0, rowEndIndex = 0;
        DataTable dtHotels = new DataTable();
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        /// <summary>
        /// createing instance List HotelSearchResult ..this is only final HotelResultresponse object
        /// </summary>
        List<HotelSearchResult> results = new List<HotelSearchResult>();
        /**********************************************************/


        /// <summary>
        ///  This method is used to get all the available hotels from AGoda
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">This is B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        ///<remarks>Here We need to send 30 HotelCodes for Every Request asyncronous
        /// Creting Request Object and call methods using multithreading
        /// </remarks>
        public HotelSearchResult[] GetHotelAvailabilityV2(HotelRequest req, decimal markup, string markupType)
        {
            this.request = req;
            this.markup = markup;
            this.markupType = markupType;
            results = new List<HotelSearchResult>();

            //This dictionary will hold what fare search we want to call
            Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
            //ReadySources will hold same source orderings i.e SG1, SG2 ....
            Dictionary<string, int> readySources = new Dictionary<string, int>();
            //Audit.Add(EventType.AgodaAvailSearch, Severity.Normal, 1, "Agoda.GetHotelAvailabilityV2 Entered", "0");
            int records = 30, rowsCount = 0, startIndex = 0, endIndex = 0;
            HotelSearchResult[] searchRes = new HotelSearchResult[0];
            int maxChildAge = 0;
            if (req != null)
            {
                if (req.RoomGuest !=null && req.RoomGuest.Length > 0)
                {
                    for (int m = 0; m < req.RoomGuest.Length; m++)
                    {
                        if (req.RoomGuest[m].childAge != null && req.RoomGuest[m].childAge.Count > 0)
                        {
                            int childAge = req.RoomGuest[m].childAge.Max();
                            if (childAge > maxChildAge)
                            {
                                maxChildAge = childAge;
                            }
                        }
                    }
                }
            }
            //Getting All HotelId's Based on the Agodacode from DB
            dtHotels = HotelStaticData.GetStaticHotelIds(req.CityCode, HotelBookingSource.Agoda, maxChildAge);
            rowsCount = dtHotels.Rows.Count;

            //Every request is sending 30 records same method we calling (dtHotels/30) times
            int i = 0;
            while (rowsCount > 0)
            {
                if (records <= 30)
                {
                    startIndex = 0;
                    endIndex = records;
                }
                else
                {
                    startIndex += 30;
                    endIndex = records;
                }
                string key = i.ToString() + "-" + startIndex + "-" + endIndex;
                listOfThreads.Add(key, new WaitCallback(GenarateSearchResults));
                readySources.Add(key, 10);
                i++;
                rowsCount -= 30;
                records += 30;
            }
            eventFlag = new AutoResetEvent[readySources.Count];
            int j = 0;
            //Start each fare search within a Thread which will automatically terminate after the results are received.
            foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
            {
                if (readySources.ContainsKey(deThread.Key))
                {
                    ThreadPool.QueueUserWorkItem(deThread.Value, deThread.Key);
                    eventFlag[j] = new AutoResetEvent(false);
                    j++;
                }
            }
            if (j != 0)
            {
                if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 300), true))
                {
                    //TODO: audit which thread is timed out                
                }
            }
            searchRes = results.ToArray();
            return searchRes;
        }

        /// <summary>
        /// this multiple time calling inside GetHotelAvailabilityV2(using Multithread)
        /// This method is used to get all the available hotels from AGoda
        /// </summary>
        /// <param name="eventNumber">to identify the sequence number</param>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        /// <remarks>This method only we are sending request and to get response</remarks>
        private void GenarateSearchResults(object eventNumber)
        {
            //string resp = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            string request = string.Empty;
            string[] values = eventNumber.ToString().Split('-');
            #region Generate Request
            try
            {
                //every request we are sending 30 records 
                //if (eventNumber.ToString() == "0")
                //{
                //    rowStartIndex = 0;
                //    rowEndIndex = 30;
                //}
                //else
                //{
                //    rowStartIndex += 30;
                //    rowEndIndex += 30;
                //}
                request = GenerateAvailabilityRequestV2(this.request,Convert.ToInt32(values[1]), Convert.ToInt32(values[2]), dtHotels);
                //Audit.Add(EventType.AgodaAvailSearch, Severity.Normal, 1, "request message generated", "0");
                try
                {
                    string filePath = XmlPath + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_AvailabilityREQ_" + values[0] + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(request);
                    doc.Save(filePath);
                    //Audit.Add(EventType.AgodaAvailSearch, Severity.Normal, appUserId, filePath, "0");
                }
                catch { }
            }
            catch (Exception Ex)
            {
                Audit.Add(EventType.AgodaAvailSearch, CT.Core.Severity.High, 0, "Exception returned in creation of XML request" + Ex.Message, "");
            }
            #endregion

            #region Get Resposne from Agoda
            try
            {
                xmlDoc = SendRequest(request, ConfigurationSystem.AgodaConfig["search"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaAvailSearch, CT.Core.Severity.High, 0, "Exception returned from Agoda.GetHotelAvailability Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlDoc.OuterXml, "");
            }
            #endregion

            #region Generate HotelSearchResult Object
            try
            {
                if (xmlDoc != null && xmlDoc.ChildNodes != null && xmlDoc.ChildNodes.Count > 0)
                {
                    GenerateSearchResultV2(xmlDoc, this.request, ref results, ref dtHotels, markup, markupType, values[0]);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaAvailSearch, CT.Core.Severity.High, 0, "Exception returned from Agoda.GetHotelAvailability Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlDoc.OuterXml, "");
            }
            #endregion
            //Returns:   true if the operation succeeds; otherwise, false.
            eventFlag[Convert.ToInt32(values[0])].Set();
        }
        /// <summary>
        /// Genarting Request string
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="startIndex">Each Request StartIndex(Getting based on the Thread)</param>
        /// <param name="endIndex">Each Request EndIndex(Getting based on the Thread)</param>
        /// <param name="dtHotels">TotalHotels based on the city wise Loading from DB</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object </remarks>
        private string GenerateAvailabilityRequestV2(HotelRequest req, int startIndex, int endIndex, DataTable dtHotels)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("AvailabilityRequestV2", "http://xml.agoda.com"); //Adding NameSpaces
            xmlString.WriteAttributeString("siteid", siteId);
            xmlString.WriteAttributeString("apikey", apiKey);
            xmlString.WriteAttributeString("async", "false");
            xmlString.WriteAttributeString("waittime", "10");
            xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");//Adding NameSpaces
            xmlString.WriteElementString("Type", "6");//Here 6 Menas Multiple Hotelid's passing
            string hotelIds = string.Empty;
            for (int i = startIndex; i < endIndex; i++)
            {
                if (i < dtHotels.Rows.Count && dtHotels.Rows[i]["HotelCode"] != DBNull.Value)
                {
                    if (string.IsNullOrEmpty(hotelIds))
                    {
                        hotelIds = dtHotels.Rows[i]["HotelCode"].ToString().Trim();
                    }
                    else
                    {
                        hotelIds = hotelIds + "," + dtHotels.Rows[i]["HotelCode"].ToString().Trim();
                    }
                }
            }
            xmlString.WriteElementString("Id", hotelIds);
            xmlString.WriteElementString("Radius", "0");
            xmlString.WriteElementString("Latitude", "0");
            xmlString.WriteElementString("Longitude", "0");
            xmlString.WriteElementString("CheckIn", req.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("CheckOut", req.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("Rooms", req.RoomGuest.Length.ToString());
            int adultsCount = 0;
            int childsCount = 0;
            for (int i = 0; i < req.RoomGuest.Length; i++)
            {
                adultsCount += req.RoomGuest[i].noOfAdults;
                childsCount += req.RoomGuest[i].noOfChild;
            }
            xmlString.WriteElementString("Adults", adultsCount.ToString());
            xmlString.WriteElementString("Children", childsCount.ToString());
            xmlString.WriteElementString("Language", language);
            xmlString.WriteElementString("Currency", currency);
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// This method is convert xmlResponse to HotelSearchResult Object 
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="hotelResults">Response object</param>
        /// <param name="dtHotels">TotalHotels based on the city wise Loading from DB</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <param name="eventNumber">to identify the sequence number</param>
        /// <exception cref="Exception"></exception>
        /// <remarks>Here only we are calucating B2B markup and InputVat
        ///   and genarting rooms object,if we search multiple rooms we are genarting sequence no
        ///   based on the sequence no only we are identify which room we need to bind
        ///   Here source Currency means supplierCurrency and source amount means supplier Amount
        ///   and also loading cancellation policy here, in cancellation policy price we are not calucating markup
        ///   means without markup only we are showing cancellation markup
        ///   in cancellation we are adding days buffer,which we are loading api config
        /// </remarks>
        private void GenerateSearchResultV2(XmlDocument xmlDoc, HotelRequest req, ref List<HotelSearchResult> hotelResults, ref DataTable dtHotels, decimal markup, string markupType, string eventNumber)
        {
            DataTable dtImages = HotelImages.GetImagesByCityCode(request.CityCode, HotelBookingSource.Agoda);
            //resp = resp.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(resp);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_AvailabilityRES_" + eventNumber + ".xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("Agoda", "http://xml.agoda.com");
            //nsmgr.AddNamespace("tns", "http://xml.agoda.com");
            //nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            //nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//Agoda:AvailabilityLongResponseV2/Agoda:ErrorMessages/Agoda:ErrorMessage", nsmgr);
            if (ErrorInfo != null)
            {
                Audit.Add(EventType.AgodaAvailSearch, Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
            }
            else
            {
                //Loading all Vat charges
                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                XmlNode tempNode = xmlDoc.SelectSingleNode("//Agoda:AvailabilityLongResponseV2", nsmgr).Attributes["status"];
                string searchId = string.Empty;
                if (tempNode != null && tempNode.InnerText == "200") //Here Status 200 Means Response Success
                {
                    tempNode = xmlDoc.SelectSingleNode("//Agoda:AvailabilityLongResponseV2", nsmgr).Attributes["searchid"];
                    searchId = tempNode.InnerText;
                }
                else
                {
                    Audit.Add(EventType.AgodaAvailSearch, CT.Core.Severity.High, 0, "Error returned from Agoda. Error Message: searchId not mentioned in response.| " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                    return;
                }
                XmlNodeList hotelList = xmlDoc.SelectNodes("//Agoda:AvailabilityLongResponseV2/Agoda:Hotels/Agoda:Hotel", nsmgr);
                string pattern = request.HotelName;
                if (hotelList != null)
                {
                    foreach (XmlNode hotel in hotelList)
                    {
                        HotelSearchResult hotelResult = new HotelSearchResult();
                        hotelResult.PropertyType = searchId; //Multiple Search Time they Given Multiple Search Id's...This Id We Need To Send Booking tIme
                        hotelResult.BookingSource = HotelBookingSource.Agoda;
                        hotelResult.CityCode = req.CityCode;
                        hotelResult.Currency = currency;
                        hotelResult.StartDate = request.StartDate;
                        hotelResult.EndDate = request.EndDate;
                        tempNode = hotel.SelectSingleNode("Agoda:Id/text()", nsmgr);
                        if (tempNode != null)
                        {
                            hotelResult.HotelCode = tempNode.InnerText;
                        }
                        #region search time avoid Staticinfo downloading
                        DataRow[] hotelStaticData = new DataRow[0];
                        try
                        {
                            hotelStaticData = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");
                            if (hotelStaticData != null && hotelStaticData.Length > 0)
                            {
                                hotelResult.HotelDescription = hotelStaticData[0]["description"].ToString();
                                hotelResult.HotelAddress = hotelStaticData[0]["address"].ToString();
                                hotelResult.HotelMap = hotelStaticData[0]["hotelMaps"].ToString();
                                hotelResult.HotelLocation = hotelStaticData[0]["location"].ToString();
                                hotelResult.HotelName = hotelStaticData[0]["hotelName"].ToString();
                                //some times rating given 3.5 that time we need to show 3.0 only
                                if (!string.IsNullOrEmpty(hotelStaticData[0]["hotelRating"].ToString()))
                                {
                                    hotelResult.Rating = (HotelRating)Convert.ToInt32(Math.Floor(Convert.ToDecimal(hotelStaticData[0]["hotelRating"].ToString())));
                                }
                            }
                        }
                        catch { continue; }

                        DataRow[] hotelImages = new DataRow[0];
                        try
                        {
                            hotelImages = dtImages.Select("hotelCode='" + hotelResult.HotelCode + "'");
                            string hImages = (hotelImages != null && hotelImages.Length > 0 ? hotelImages[0]["images"].ToString() : string.Empty);
                            hotelResult.HotelPicture = string.Empty;
                            if (!string.IsNullOrEmpty(hImages))
                            {
                                hotelResult.HotelPicture = hImages.Split('|')[0];
                            }
                        }
                        catch { continue; }

                        #endregion
                        #region To get only those satisfy the search conditions
                        if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                        {
                            if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        #endregion

                        ///Rooms Binding
                        XmlNodeList roomNodeList = hotel.SelectNodes("Agoda:Rooms/Agoda:Room", nsmgr);
                        hotelResult.RoomGuest = request.RoomGuest;
                        rateOfExchange = (exchangeRates.ContainsKey(hotelResult.Currency) ? exchangeRates[hotelResult.Currency] : 1);
                        int noOfDays = req.EndDate.Subtract(req.StartDate).Days;
                        if (roomNodeList != null)
                        {
                            HotelRoomsDetails[] rooms = new HotelRoomsDetails[roomNodeList.Count * request.NoOfRooms];
                            int index = 0;
                            foreach (XmlNode roomNode in roomNodeList)
                            {
                                for (int i = 0; i < Convert.ToInt32(request.NoOfRooms); i++)
                                {
                                    HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                                    roomDetail.SequenceNo = (i + 1).ToString();
                                    XmlAttribute lineitemid = roomNode.Attributes["lineitemid"];
                                    if (lineitemid !=null)
                                    {
                                        roomDetail.RoomIndex =Convert.ToInt32(lineitemid.Value);
                                    }
                                    XmlAttribute name = roomNode.Attributes["name"];
                                    if (name != null)
                                    {
                                        roomDetail.RoomTypeName = name.Value;
                                    }
                                    XmlAttribute Id = roomNode.Attributes["id"];
                                    if (Id !=null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.SequenceNo + "|" + roomDetail.RoomIndex + "|" + Id.Value;
                                    }
                                    XmlAttribute rateplan = roomNode.Attributes["rateplan"];
                                    if (rateplan !=null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + rateplan.Value;
                                    }
                                    XmlAttribute ratetype = roomNode.Attributes["ratetype"];
                                    if (ratetype !=null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + ratetype.Value;
                                    }
                                    XmlAttribute currency = roomNode.Attributes["currency"];
                                    if (currency != null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + currency.Value;
                                    }
                                    XmlAttribute model = roomNode.Attributes["model"];
                                    if (model != null)
                                    {
                                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + model.Value;
                                    }
                                    XmlAttribute ratecategoryid = roomNode.Attributes["ratecategoryid"];
                                    if (ratecategoryid !=null)
                                    {
                                       roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + ratecategoryid.Value;
                                    }
                                    //RoomType Code--- Seq|lineitemid|id|rateplan|rateType|currency|model|ratecategoryid
                                    XmlAttribute blockid = roomNode.Attributes["blockid"];
                                    if (blockid != null)
                                    {
                                        roomDetail.RatePlanCode = blockid.Value;
                                    }
                                    XmlAttribute promotionid = roomNode.Attributes["promotionid"];
                                    if (promotionid !=null)
                                    {
                                        roomDetail.SmokingPreference = promotionid.Value;//promotionid storing SmokingPreference Booking tIme Required
                                    }
                                    //Promotion
                                    tempNode = roomNode.SelectSingleNode("Agoda:RateInfo/Agoda:Promotion", nsmgr);
                                    if (tempNode != null)
                                    {
                                        XmlAttribute text = tempNode.Attributes["text"];
                                        if (text != null && !string.IsNullOrEmpty(text.Value))
                                        {
                                            roomDetail.PromoMessage = text.Value;
                                        }
                                    }

                                    XmlNodeList BenefitList = roomNode.SelectNodes("Agoda:Benefits/Agoda:Benefit", nsmgr);
                                    if (BenefitList != null && BenefitList.Count > 0)
                                    {
                                        string mealPlan = string.Empty;
                                        foreach (XmlNode Benefit in BenefitList)
                                        {
                                            tempNode = Benefit.SelectSingleNode("Agoda:Name", nsmgr);
                                            if (tempNode != null)
                                            {
                                                if (string.IsNullOrEmpty(mealPlan))
                                                {
                                                    mealPlan = tempNode.InnerText;
                                                }
                                                else
                                                {
                                                    mealPlan = mealPlan + "," + tempNode.InnerText;
                                                }
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(mealPlan))
                                        {
                                            roomDetail.mealPlanDesc = mealPlan;
                                        }
                                        else
                                        {
                                            roomDetail.mealPlanDesc = "Room Only";
                                        }
                                    }
                                    if (string.IsNullOrEmpty(roomDetail.mealPlanDesc))
                                    {
                                        roomDetail.mealPlanDesc = "Room Only";
                                    }
                                    #region Price Calucation
                                    //SurCharges
                                    XmlNodeList SurchargeList = roomNode.SelectNodes("Agoda:RateInfo/Agoda:Surcharges/Agoda:Surcharge", nsmgr);
                                    string surExcludedRemarks = string.Empty;
                                    string surMandatoryRemarks = string.Empty;
                                    roomDetail.SurChargeList = new List<Surcharges>();
                                    decimal mandatoryAmount = 0m;
                                    if (SurchargeList != null && SurchargeList.Count > 0)
                                    {
                                        foreach(XmlNode surcharge in SurchargeList)
                                        {
                                            if(surcharge !=null)
                                            {
                                                XmlAttribute scharge = surcharge.Attributes["charge"];
                                                //Exclude
                                                if (scharge != null && scharge.Value == "Excluded")
                                                {
                                                    XmlNode surChargeName = surcharge.SelectSingleNode("Agoda:Name", nsmgr);
                                                    if (surChargeName != null && !string.IsNullOrEmpty(surChargeName.InnerText))
                                                    {
                                                        tempNode = surcharge.SelectSingleNode("Agoda:Rate", nsmgr);
                                                        if (tempNode != null)
                                                        {
                                                            XmlAttribute sInclusive = tempNode.Attributes["inclusive"];
                                                            decimal sExcluded = 0m;
                                                            if (sInclusive !=null)
                                                            {
                                                                sExcluded = Convert.ToDecimal(sInclusive.Value);
                                                                sExcluded = sExcluded * rateOfExchange;
                                                                if (string.IsNullOrEmpty(surExcludedRemarks))
                                                                {
                                                                    surExcludedRemarks =  surChargeName.InnerText + " " + agentCurrency + " " + sExcluded.ToString("N" + AgentDecimalPoint);
                                                                }
                                                                else
                                                                {
                                                                    surExcludedRemarks = surExcludedRemarks + "," + surChargeName.InnerText + " " + agentCurrency + " " + sExcluded.ToString("N" + AgentDecimalPoint);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //Mandatory
                                                if (scharge != null && scharge.Value == "Mandatory")
                                                {
                                                    Surcharges surCharge = new Surcharges();
                                                    surCharge.Charge = scharge.Value;
                                                    XmlAttribute surId = surcharge.Attributes["id"];
                                                    if (surId != null)
                                                    {
                                                        surCharge.Id = Convert.ToInt32(surId.Value);
                                                    }
                                                    XmlAttribute surMethod = surcharge.Attributes["method"];
                                                    if (surMethod != null)
                                                    {
                                                        surCharge.Method = (surMethod.Value);
                                                    }
                                                    XmlAttribute surMargin = surcharge.Attributes["margin"];
                                                    if (surMargin != null)
                                                    {
                                                        surCharge.Margin = (surMargin.Value);
                                                    }
                                                    XmlNode surChargeName = surcharge.SelectSingleNode("Agoda:Name", nsmgr);
                                                    if (surChargeName != null && !string.IsNullOrEmpty(surChargeName.InnerText))
                                                    {
                                                        surCharge.Name = surChargeName.InnerText;
                                                        tempNode = surcharge.SelectSingleNode("Agoda:Rate", nsmgr);
                                                        if (tempNode != null)
                                                        {
                                                            XmlAttribute surExclusive = tempNode.Attributes["exclusive"];
                                                            if (surExclusive != null)
                                                            {
                                                                surCharge.Exclusive =Convert.ToDecimal(surExclusive.Value);
                                                            }
                                                            XmlAttribute surTax = tempNode.Attributes["tax"];
                                                            if (surTax != null)
                                                            {
                                                                surCharge.tax = Convert.ToDecimal(surTax.Value);
                                                            }
                                                            XmlAttribute surFees = tempNode.Attributes["tax"];
                                                            if (surFees != null)
                                                            {
                                                                surCharge.Fees = Convert.ToDecimal(surFees.Value);
                                                            }
                                                            XmlAttribute sInclusive = tempNode.Attributes["inclusive"];
                                                            decimal sIncluded = 0m;
                                                            if (sInclusive != null)
                                                            {
                                                                sIncluded = Convert.ToDecimal(sInclusive.Value);
                                                                surCharge.Inclusive = sIncluded;
                                                                mandatoryAmount = mandatoryAmount + sIncluded;
                                                                sIncluded = sIncluded * rateOfExchange;
                                                                if (string.IsNullOrEmpty(surMandatoryRemarks))
                                                                {
                                                                    surMandatoryRemarks = surChargeName.InnerText + " " + agentCurrency + " " + sIncluded.ToString("N" + AgentDecimalPoint);
                                                                }
                                                                else
                                                                {
                                                                    surMandatoryRemarks = surMandatoryRemarks + "," + surChargeName.InnerText + " " + agentCurrency + " " + sIncluded.ToString("N" + AgentDecimalPoint);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    roomDetail.SurChargeList.Add(surCharge);
                                                }
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(surMandatoryRemarks))
                                    {
                                        roomDetail.EssentialInformation = "<b>Included in price</b>: " + surMandatoryRemarks;
                                    }
                                    if (!string.IsNullOrEmpty(surExcludedRemarks))
                                    {
                                        if (!string.IsNullOrEmpty(roomDetail.EssentialInformation))
                                        {
                                            roomDetail.EssentialInformation = roomDetail.EssentialInformation + "|" + "<b>Not Included in price(collected by the properity)</b>:" + surExcludedRemarks;
                                        }
                                        else
                                        {
                                            roomDetail.EssentialInformation = "<b>Not Included in price(collected by the properity)</b>:" + surExcludedRemarks;
                                        }
                                    }
                                    tempNode = roomNode.SelectSingleNode("Agoda:RateInfo/Agoda:Rate", nsmgr);
                                    if (tempNode != null)
                                    {

                                        decimal totPrice = 0m;
                                        decimal inclusiveAmount = 0;
                                        XmlAttribute inclusive = tempNode.Attributes["inclusive"];
                                        if (inclusive != null)
                                        {
                                            inclusiveAmount = Convert.ToDecimal(inclusive.Value);
                                        }
                                        //Mandatory Amount Alos Should Required Add MAin Amount 
                                        totPrice = inclusiveAmount + mandatoryAmount;

                                        //Booking Time Required
                                        roomDetail.TBOPrice = new PriceAccounts();
                                        roomDetail.TBOPrice.NetFare = inclusiveAmount;
                                        XmlAttribute tax = tempNode.Attributes["tax"];
                                        if (tax != null)
                                        {
                                            roomDetail.TBOPrice.Tax = Convert.ToDecimal(tax.Value);
                                        }
                                        XmlAttribute exclusive = tempNode.Attributes["exclusive"];
                                        if (exclusive != null)
                                        {
                                            roomDetail.TBOPrice.SeviceTax = Convert.ToDecimal(exclusive.Value);
                                        }
                                        XmlAttribute fees = tempNode.Attributes["fees"];
                                        if (fees != null)
                                        {
                                            roomDetail.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(fees.Value);
                                        }
                                        decimal hotelTotalPrice = 0m;
                                        decimal vatAmount = 0m;
                                        hotelTotalPrice = Math.Round((totPrice * noOfDays) * rateOfExchange, decimalPoint);
                                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                        {
                                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                        }
                                        hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                                        roomDetail.TotalPrice = hotelTotalPrice;
                                        roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                                        roomDetail.MarkupType = markupType;
                                        roomDetail.MarkupValue = markup;
                                        roomDetail.SellingFare = roomDetail.TotalPrice;
                                        roomDetail.supplierPrice = Math.Round((totPrice * noOfDays));
                                        roomDetail.TaxDetail = new PriceTaxDetails();
                                        roomDetail.TaxDetail = priceTaxDet;
                                        roomDetail.InputVATAmount = vatAmount;
                                        hotelResult.Price = new PriceAccounts();
                                        hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                        hotelResult.Price.SupplierPrice = Math.Round((totPrice * noOfDays));
                                        hotelResult.Price.RateOfExchange = rateOfExchange;
                                    }
                                    //day wise price calucation
                                    System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                    RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                    decimal totalprice = roomDetail.TotalPrice;

                                    for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                    {
                                        decimal price = roomDetail.TotalPrice / diffResult.Days;
                                        if (fareIndex == diffResult.Days - 1)
                                        {
                                            price = totalprice;
                                        }
                                        totalprice -= price;
                                        hRoomRates[fareIndex].Amount = price;
                                        hRoomRates[fareIndex].BaseFare = price;
                                        hRoomRates[fareIndex].SellingFare = price;
                                        hRoomRates[fareIndex].Totalfare = price;
                                        hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                        hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                                    }
                                    roomDetail.Rates = hRoomRates;
                                    #endregion
                                    #region Cancellation Policy
                                    XmlNodeList cancellationPolicyList = roomNode.SelectNodes("Agoda:Cancellation/Agoda:PolicyDates/Agoda:PolicyDate", nsmgr);
                                    int buffer = Convert.ToInt32(ConfigurationSystem.AgodaConfig["buffer"]);
                                    string message = string.Empty;
                                    if (cancellationPolicyList != null)
                                    {
                                        foreach (XmlNode cancelNode in cancellationPolicyList)
                                        {
                                            decimal chargeAmt = 0m;
                                            XmlAttribute before = cancelNode.Attributes["before"];
                                            if(before !=null)
                                            {
                                                DateTime beforeDate = Convert.ToDateTime(before.Value);
                                                beforeDate = beforeDate.AddDays(-buffer);
                                                XmlNode rate = cancelNode.SelectSingleNode("Agoda:Rate", nsmgr);
                                                if (rate != null)
                                                {
                                                    XmlAttribute inclusive = rate.Attributes["inclusive"];
                                                    if (inclusive != null)
                                                    {
                                                        chargeAmt = Convert.ToDecimal((Convert.ToDecimal(inclusive.Value) * rateOfExchange).ToString("N" + decimalPoint));
                                                    }
                                                    if (!string.IsNullOrEmpty(message))
                                                    {
                                                        message = message + "|" + beforeDate.ToString("dd/MMM/yyyy") + " earlier You will be charged " + agentCurrency + " " + chargeAmt;
                                                    }
                                                    else
                                                    {
                                                        message = beforeDate.ToString("dd/MMM/yyyy") + " earlier You will be charged " + agentCurrency + " " + chargeAmt;
                                                    }
                                                }
                                            }
                                            XmlAttribute after = cancelNode.Attributes["after"];
                                            if (after != null)
                                            {
                                                DateTime afterDate = Convert.ToDateTime(after.Value);
                                                afterDate = afterDate.AddDays(-buffer);
                                                XmlNode rate = cancelNode.SelectSingleNode("Agoda:Rate", nsmgr);
                                                if (rate != null)
                                                {
                                                    XmlAttribute inclusive = rate.Attributes["inclusive"];
                                                    if (inclusive != null)
                                                    {
                                                        chargeAmt = Convert.ToDecimal((Convert.ToDecimal(inclusive.Value) * rateOfExchange).ToString("N" + decimalPoint));
                                                    }
                                                    if (!string.IsNullOrEmpty(message))
                                                    {
                                                        message = message + "|" + afterDate.ToString("dd/MMM/yyyy") + " onwards You will be charged " + agentCurrency + " " + chargeAmt;
                                                    }
                                                    else
                                                    {
                                                        message = afterDate.ToString("dd/MMM/yyyy") + " onwards You will be charged " + agentCurrency + " " + chargeAmt;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    roomDetail.CancellationPolicy = message;
                                    #endregion
                                    rooms[index] = roomDetail;
                                    index++;
                                }
                            }
                            hotelResult.RoomDetails = rooms;
                        }
                        hotelResult.Currency = agentCurrency;
                        for (int i = 0; i < req.NoOfRooms; i++)
                        {
                            for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                            {
                                if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                                {
                                    hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                    hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                    hotelResult.Price.AccPriceType = PriceType.NetFare;
                                    hotelResult.Price.RateOfExchange = rateOfExchange;
                                    hotelResult.Price.Currency = agentCurrency;
                                    hotelResult.Price.CurrencyCode = agentCurrency;
                                    break;
                                }
                            }
                        }
                        hotelResults.Add(hotelResult);
                    }
                }
            }
        }
        #endregion
        #region Booking
        /// <summary>
        /// Booking Method
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        /// <remarks>Here we are preparing Booking request object and sending book request to api </remarks>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            string request = GenerateBookingRequest(itinerary);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(request);
            try
            {
                string filePath = @"" + XmlPath + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_BookRequest.xml";
                doc.Save(filePath);
            }
            catch { }
            XmlDocument xmlResp = new XmlDocument();
            //string resp = string.Empty;
            BookingResponse searchRes = new BookingResponse();
            try
            {
                xmlResp = SendRequest(request, ConfigurationSystem.AgodaConfig["Booking"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaBooking, Severity.High, 0, "Exception returned from Agoda.GetBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseBooking(xmlResp, itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaBooking, Severity.High, 0, "Exception returned from Agoda.GetBooking Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return searchRes;
        }

        /// <summary>
        /// GenerateBookingRequest
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenerateBookingRequest(HotelItinerary itinerary)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlTextWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("BookingRequestV3", "http://xml.agoda.com");//BookingRequestV3 started
                xmlString.WriteAttributeString("siteid", siteId);
                xmlString.WriteAttributeString("apikey", apiKey);
                xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                xmlString.WriteAttributeString("xsi", "schemaLocation", null, "http://xml.agoda.com BookingRequestV3.xsd");

                xmlString.WriteStartElement("BookingDetails");//Booking Details Started
                xmlString.WriteAttributeString("searchid", itinerary.PropertyType);
                Guid traceid = Guid.NewGuid();
                clientReferenceNo = traceid.ToString();
                xmlString.WriteAttributeString("tag", clientReferenceNo);
                xmlString.WriteAttributeString("AllowDuplication", "false"); //Need to Check Vijay
                xmlString.WriteAttributeString("CheckIn", itinerary.StartDate.ToString("yyyy-MM-dd"));
                xmlString.WriteAttributeString("CheckOut", itinerary.EndDate.ToString("yyyy-MM-dd"));
                xmlString.WriteStartElement("Hotel");//Hotel Node Started
                xmlString.WriteAttributeString("id", itinerary.HotelCode);
                xmlString.WriteStartElement("Rooms");//Rooms Started

                Dictionary<HotelRoom, int> filteredroomsList = new Dictionary<HotelRoom, int>();
                List<HotelRoom> roomsList = new List<HotelRoom>();
                roomsList.AddRange(itinerary.Roomtype);
                foreach (HotelRoom container in itinerary.Roomtype)
                {
                    List<HotelRoom> duplicateRoom = roomsList.FindAll(delegate (HotelRoom checkRoom)
                    { return (checkRoom.RatePlanCode == container.RatePlanCode); });
                    if (duplicateRoom != null && duplicateRoom.Count > 0 && !filteredroomsList.ContainsKey(duplicateRoom[0]))
                    {
                        filteredroomsList.Add(duplicateRoom[0], duplicateRoom.Count);
                        string[] roomTypeCode = container.RoomTypeCode.Split('|');
                        //RoomType Code--- Seq|lineitemid|id|rateplan|rateType|currency|model|ratecategoryid
                        string id = string.Empty;
                        string lineitemid = string.Empty;
                        string rateplan = string.Empty;
                        string ratetype = string.Empty;
                        string currency = string.Empty;
                        string model = string.Empty;
                        string ratecategoryid = string.Empty;
                        if (roomTypeCode.Length > 7)
                        {
                            lineitemid = roomTypeCode[1];
                            id = roomTypeCode[2];
                            rateplan = roomTypeCode[3];
                            ratetype = roomTypeCode[4];
                            currency = roomTypeCode[5];
                            model = roomTypeCode[6];
                            ratecategoryid = roomTypeCode[7];
                        }
                        xmlString.WriteStartElement("Room");//Room Started
                        xmlString.WriteAttributeString("id", id);
                        if (!string.IsNullOrEmpty(container.SmokingPreference))
                        {
                            xmlString.WriteAttributeString("promotionid", container.SmokingPreference);
                        }
                        xmlString.WriteAttributeString("name", container.RoomName);
                        xmlString.WriteAttributeString("lineitemid", lineitemid);
                        xmlString.WriteAttributeString("rateplan", rateplan);
                        xmlString.WriteAttributeString("ratetype", ratetype);
                        xmlString.WriteAttributeString("currency", currency);
                        xmlString.WriteAttributeString("model", model);
                        xmlString.WriteAttributeString("ratecategoryid", ratecategoryid);
                        xmlString.WriteAttributeString("blockid", container.RatePlanCode);
                        xmlString.WriteAttributeString("count", duplicateRoom.Count.ToString());
                        int AdultCount = 0;
                        int ChildCount = 0;
                        for (int i = 0; i < duplicateRoom.Count; i++)
                        {
                            AdultCount += duplicateRoom[i].AdultCount;
                            ChildCount += duplicateRoom[i].ChildCount;
                        }
                        xmlString.WriteAttributeString("adults", AdultCount.ToString());
                        xmlString.WriteAttributeString("children", ChildCount.ToString());
                        xmlString.WriteStartElement("Rate");//Rate Element Started
                        xmlString.WriteAttributeString("exclusive", container.TBOPrice.SeviceTax.ToString());
                        xmlString.WriteAttributeString("tax", container.TBOPrice.Tax.ToString());
                        xmlString.WriteAttributeString("fees", container.TBOPrice.AdditionalTxnFee.ToString());
                        xmlString.WriteAttributeString("inclusive", container.TBOPrice.NetFare.ToString());
                        xmlString.WriteEndElement();//Rate Ended
                        if (container.SurChargeList != null && container.SurChargeList.Count > 0)
                        {
                            xmlString.WriteStartElement("Surcharges");//Surcharges Element Started
                            foreach (Surcharges surcha in container.SurChargeList)
                            {
                                xmlString.WriteStartElement("Surcharge");//Surcharge Element Started
                                xmlString.WriteAttributeString("id", surcha.Id.ToString());
                                xmlString.WriteStartElement("Rate");//SurRate Element Started
                                xmlString.WriteAttributeString("exclusive", surcha.Exclusive.ToString());
                                xmlString.WriteAttributeString("tax", surcha.tax.ToString());
                                xmlString.WriteAttributeString("fees", surcha.Fees.ToString());
                                xmlString.WriteAttributeString("inclusive", surcha.Inclusive.ToString());
                                xmlString.WriteEndElement();//Rate Ended
                                xmlString.WriteEndElement();//Surcharge Ended
                            }
                            xmlString.WriteEndElement();//Surcharges Ended
                        }
                        xmlString.WriteStartElement("GuestDetails");//GuestDetails Started
                        xmlString.WriteStartElement("GuestDetail");//GuestDetail started
                        xmlString.WriteAttributeString("Primary", "true");
                        xmlString.WriteElementString("Title", container.PassenegerInfo[0].Title);
                        xmlString.WriteElementString("FirstName", container.PassenegerInfo[0].Firstname);
                        xmlString.WriteElementString("LastName", container.PassenegerInfo[0].Lastname);
                        xmlString.WriteElementString("CountryOfPassport", container.PassenegerInfo[0].NationalityCode);
                        xmlString.WriteEndElement();//GuestDetail Ended
                        xmlString.WriteEndElement();//GuestDetails Ended
                        xmlString.WriteEndElement();//Room Ended
                    }
                }
                xmlString.WriteEndElement();//Rooms Ended
                xmlString.WriteEndElement();//Hotel Ended
                xmlString.WriteEndElement();//Booking Details Ended
                                            //Customer Details
                xmlString.WriteStartElement("CustomerDetail");
                xmlString.WriteElementString("Language", language);
                xmlString.WriteElementString("Title", itinerary.Roomtype[0].PassenegerInfo[0].Title);
                xmlString.WriteElementString("FirstName", "Cozmo");
                xmlString.WriteElementString("LastName", "Travel");
                xmlString.WriteElementString("Email", "tech@cozmotravel.com");
                xmlString.WriteStartElement("Phone");//Phone Started
                xmlString.WriteElementString("CountryCode", "00");
                xmlString.WriteElementString("Number", "9715065074592");
                xmlString.WriteEndElement();//Phone Ended
                xmlString.WriteElementString("Newsletter", "false");
                xmlString.WriteElementString("IpAddress", "103.20.214.135");
                xmlString.WriteEndElement();//Customer Details Ended
                //Card Details
                xmlString.WriteStartElement("PaymentDetails");
                //xmlString.WriteStartElement("CreditCardInfo");
                //xmlString.WriteElementString("Cardtype", "Visa");
                //xmlString.WriteElementString("Number", "4111111111111111");
                //xmlString.WriteElementString("ExpiryDate", "122020");
                //xmlString.WriteElementString("Cvc", "123");
                //xmlString.WriteElementString("HolderName", "Test");
                //xmlString.WriteElementString("CountryOfIssue", "DE");
                //xmlString.WriteElementString("IssuingBank", "WIRECARD BANK AG");
                //xmlString.WriteEndElement();//CreditCardInfo Details Ended
                xmlString.WriteEndElement();//PaymentDetails Details Ended
                xmlString.WriteEndElement();//BookingRequestV3 Ended
                xmlString.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Agoda: Failed generate booking request for Hotel : " + itinerary.HotelName, ex);
            }
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadBookingResponse
        /// </summary>
        /// <param name="xmlDoc">Booking response xml</param>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>Here Reading booking response object
        /// here only we need to get confirmationNo and BookingId
        /// </remarks>
        private BookingResponse ReadResponseBooking(XmlDocument xmlDoc, HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                XmlNode tempNode;
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                try
                {
                    string filePath = @"" + XmlPath + SessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_BookResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("Agoda", "http://xml.agoda.com");
                tempNode = xmlDoc.SelectSingleNode("//Agoda:BookingResponseV3", nsmgr).Attributes["status"];
                if (tempNode != null && tempNode.InnerText == "200") //Here Status 200 Means Response Success
                {
                    XmlNode bookingResponse = xmlDoc.SelectSingleNode("//Agoda:BookingDetails", nsmgr);
                    if (bookingResponse != null)
                    {
                        bookingResponse = xmlDoc.SelectSingleNode("//Agoda:Booking", nsmgr);
                        XmlAttribute id = bookingResponse.Attributes["id"];
                        if (id != null)
                        {
                            itinerary.ConfirmationNo = id.Value;
                        }
                        itinerary.AgencyReference = clientReferenceNo;
                        XmlAttribute ItineraryID = bookingResponse.Attributes["ItineraryID"];
                        if (ItineraryID != null)
                        {
                            itinerary.BookingRefNo = ItineraryID.Value;
                        }
                        bookResponse.ConfirmationNo = itinerary.ConfirmationNo;
                        bookResponse.Status = BookingResponseStatus.Successful;
                        itinerary.Status = HotelBookingStatus.Confirmed;
                        itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: Agoda as per final booking form Confirmation No: " + itinerary.ConfirmationNo;
                    }
                    else
                    {
                        bookResponse.Status = BookingResponseStatus.Failed;
                    }
                }
                else
                {
                    XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//Agoda:BookingResponseV3/Agoda:ErrorMessages/Agoda:ErrorMessage", nsmgr);
                    if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                    {
                        Audit.Add(EventType.AgodaBooking, Severity.High, 0, "Error Returned from Agoda. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                        bookResponse = new BookingResponse(BookingResponseStatus.Failed, ErrorInfo.InnerText, string.Empty, ProductType.Hotel, "");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Agoda: Failed to Read booking response ", ex);
            }
            return bookResponse;
        }

        #endregion

        #region cancel Booking
        /// <summary>
        /// CancelHotelBooking
        /// </summary>
        /// <param name="confirmationNo">Booking confirmationNo</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Here we are preparing CancelHotelBooking request object and sending CancelHotelBooking to api</remarks>

        public Dictionary<string, string> CancelHotelBooking(string confirmationNo)
        {
            Dictionary<string, string> cancelRes = new Dictionary<string, string>();
            string request = GeneratePreCanceRequest(confirmationNo);
            //Audit.Add(EventType.AgodaCancel, Severity.Normal, 1, "Agoda CancelHotelBooking request message generated", "0");
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CancellationRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlRes = new XmlDocument();
            try
            {
                xmlRes = SendRequest(request, ConfigurationSystem.AgodaConfig["Cancellation"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaCancel, Severity.High, 0, "Exception returned from Agoda.CancelHotelBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlRes != null && xmlRes.ChildNodes != null && xmlRes.ChildNodes.Count > 0)
                {
                    cancelRes = ReadPreCanceResponse(xmlRes);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaCancel, Severity.High, 0, "Exception returned from Agoda.ReadPreCanceResponse Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancelRes;
        }
        /// <summary>
        /// GeneratePreCanceRequest
        /// </summary>
        /// <param name="confirmationNo">Booking ConfirmationNo</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GeneratePreCanceRequest(string confirmationNo)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("CancellationRequestV2", "http://xml.agoda.com");//CancellationRequestV2 started
                xmlString.WriteAttributeString("siteid", siteId);
                xmlString.WriteAttributeString("apikey", apiKey);
                xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                xmlString.WriteElementString("BookingID", confirmationNo);
                xmlString.WriteEndElement();//CancellationRequestV2
                xmlString.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Agoda: Failed generate PreCanceRequest info request for ConfirmationNo : " + confirmationNo, ex);
            }
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadPreCanceResponse
        /// </summary>
        /// <param name="xmlDoc">PreCanceResponse xml</param>
        /// <returns>Dictionary</returns>
        /// <remarks>First here we need to take cancel token once get cancel token nee to call cancel status method</remarks>
        
        private Dictionary<string, string> ReadPreCanceResponse(XmlDocument xmlDoc)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CancellationResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("Agoda", "http://xml.agoda.com");
            tempNode = xmlDoc.SelectSingleNode("//Agoda:CancellationResponseV2", nsmgr).Attributes["status"];
            if (tempNode != null && tempNode.InnerText == "200") //Here Status 200 Means Response Success
            {
                string bookingRefNo = string.Empty;
                decimal cancelCost = 0;
                decimal bookingCost = 0;
                string cancelToken = string.Empty;
                string currency = string.Empty;
                XmlNode CancellationSummary = xmlDoc.SelectSingleNode("//Agoda:CancellationSummary", nsmgr);
                if (CancellationSummary != null)
                {
                    tempNode = CancellationSummary.SelectSingleNode("//Agoda:BookingID",nsmgr);
                    if (tempNode != null)
                    {
                        bookingRefNo = tempNode.InnerText;
                    }
                    tempNode = CancellationSummary.SelectSingleNode("//Agoda:Reference",nsmgr);
                    if (tempNode != null)
                    {
                        cancelToken = tempNode.InnerText;
                    }
                    XmlNode PaymentRateInclusive = CancellationSummary.SelectSingleNode("//Agoda:Payment/Agoda:PaymentRateInclusive", nsmgr);
                    if (PaymentRateInclusive != null)
                    {
                        bookingCost = Convert.ToDecimal(PaymentRateInclusive.InnerText);
                    }
                    XmlNode RefundRateInclusive = CancellationSummary.SelectSingleNode("//Agoda:Refund/Agoda:RefundRateInclusive", nsmgr);
                    if (RefundRateInclusive != null)
                    {
                        cancelCost = Convert.ToDecimal(RefundRateInclusive.InnerText);
                    }
                    XmlAttribute tmpCurrency = PaymentRateInclusive.Attributes["currency"];
                    if (tmpCurrency != null)
                    {
                        currency = tmpCurrency.Value;
                    }
                    bool isCancel = false;
                    if (!string.IsNullOrEmpty(bookingRefNo) && !string.IsNullOrEmpty(cancelToken))
                    {
                        //Here Calling CancelStatus
                        isCancel = GetCancelStatus(bookingRefNo, cancelCost, cancelToken, currency);
                        if (isCancel)
                        {
                            cancellationCharges.Add("Currency", currency);
                            decimal refundAmount = (bookingCost - cancelCost);
                            cancellationCharges.Add("Amount", Convert.ToString(refundAmount));
                            cancellationCharges.Add("Status", "Cancelled");
                            cancellationCharges.Add("ID", cancelToken);
                        }
                        else
                        {
                            cancellationCharges.Add("Status", "Failed");
                        }
                    }
                    else
                    {
                        cancellationCharges.Add("Status", "Failed");
                    }
                }
            }
            else
            {
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//Agoda:CancellationResponseV2/Agoda:ErrorMessages/Agoda:ErrorMessage", nsmgr);
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.AgodaCancel, Severity.High, 0, "Error Returned from Agoda. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
            }
            return cancellationCharges;

        }
        /// <summary>
        /// GetCancelStatus
        /// </summary>
        /// <param name="bookingRefNo">Booking ConfirmationNo</param>
        /// <param name="cancelCost">if booking is Canceled return amount</param>
        /// <param name="cancelToken">what we get preCancelRequest token</param>
        /// <param name="currency">what we get preCancelRequest Currency </param>
        /// <returns>bool</returns>
        /// <remarks>Here we are preparing CancelStatus request object and sending CancelStatus to api</remarks>
        private bool GetCancelStatus(string bookingRefNo, decimal cancelCost, string cancelToken,string currency)
        {
            bool isCancel = false;
            string request = GenarateCanStatusRequest(bookingRefNo, cancelCost, cancelToken, currency);
            //Audit.Add(EventType.AgodaCancel, Severity.Normal, 1, "Agoda  ConfirmCancellationRequestV2 request message generated", "0");
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_ConfirmCancellationRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                //if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                //{
                xmlResp = SendRequest(request, ConfigurationSystem.AgodaConfig["confirmcancel"]);
                //}
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaCancel, Severity.High, 0, "Exception returned from Agoda.GetCancelStatus Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    isCancel = ReadCanceResponse(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaCancel, Severity.High, 0, "Exception returned from Agoda.GetCancelStatus Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return isCancel;
        }
        /// <summary>
        /// GenarateCanStatusRequest
        /// </summary>
        /// <param name="bookingRefNo">Booking ConfirmationNo</param>
        /// <param name="cancelCost">if booking is Canceled return amount</param>
        /// <param name="cancelToken">what we get preCancelRequest token</param>
        /// <param name="currency">what we get preCancelRequest Currency </param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenarateCanStatusRequest(string bookingRefNo, decimal cancelCost, string cancelToken,string currency)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("ConfirmCancellationRequestV2", "http://xml.agoda.com");//CancellationRequestV2 started
                xmlString.WriteAttributeString("siteid", siteId);
                xmlString.WriteAttributeString("apikey", apiKey);
                xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                xmlString.WriteElementString("BookingID", bookingRefNo);
                xmlString.WriteElementString("Reference", cancelToken);
                xmlString.WriteElementString("CancelReason", "0");
                xmlString.WriteStartElement("Refund");
                xmlString.WriteStartElement("RefundRateInclusive");
                xmlString.WriteAttributeString("currency", currency);
                xmlString.WriteString(cancelCost.ToString());
                xmlString.WriteEndElement();//RefundRateInclusive
                xmlString.WriteEndElement();//Refund
                xmlString.WriteEndElement();//ConfirmCancellationRequestV2
                xmlString.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Agoda: Failed generate CanStatusRequest info request for cancelToken : " + cancelToken, ex);
            }
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadCanceResponse
        /// </summary>
        /// <param name="xmlDoc">cancelReponse xml</param>
        /// <returns>bool</returns>
        /// <remarks>Here checking status if status is true booking cancelled else booking not cancelled</remarks>
        private bool ReadCanceResponse(XmlDocument xmlDoc)
        {
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_ConfirmCancellationResponseV2.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("Agoda", "http://xml.agoda.com");
            XmlNode tempNode = xmlDoc.SelectSingleNode("//Agoda:ConfirmCancellationResponseV2", nsmgr).Attributes["status"];
            bool isException = false;
            if (tempNode != null && tempNode.InnerText == "200") //Here Status 200 Means Response Success
            {
                isException = true;
            }
            else
            {
                XmlNode errorInfo = xmlDoc.SelectSingleNode("ConfirmCancellationResponseV2/Agoda:ErrorMessages/Agoda:ErrorMessage", nsmgr);
                Audit.Add(EventType.AgodaCancel, Severity.High, 0, " Agoda:ReadCanceResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                throw new BookingEngineException("<br>" + errorInfo.InnerText);
            }
            return isException;
        }
        #endregion
        #region downloading static data First Time Purpose
        /// <summary>
        /// GetHotelStaticData 
        /// </summary>
        /// <param name="cityId">HotelCity</param>
        /// <exception cref="Exception"></exception>
        /// <remarks>Here only we are send request Getting response
        /// First we need to download all Hotel Id's and corresponding cities
        /// </remarks>
        public void GetHotelStaticData(string cityId)
        {
            string resp = string.Empty;
            try
            {
                resp = SendGetRequest(ConfigurationSystem.AgodaConfig["GetHotels"] + "&apikey=" + apiKey + "&mCity_id=" + cityId + "&olanguage_id=1"); //Here 1 Means English
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaStatic, Severity.High, 0, "Exception returned from Agoda.GetHotelStaticData Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + cityId + "|response XML" + resp, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                ReadHotelStaticData(resp);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaStatic, Severity.High, 0, "Exception returned from Agoda.GetHotelStaticData Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + cityId + "|response XML" + resp, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
        }
        /// <summary>
        /// ReadHotelStaticData
        /// </summary>
        /// <param name="response">Hotel data response</param>
        /// <remarks>first we are getting here hotelcode and hotelname based on the hotel code we are download static data </remarks>
        private void ReadHotelStaticData(string response)
        {
            TextReader stringRead = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CityWiseHotelsData.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNodeList xmlHotels = xmlDoc.SelectNodes("Hotel_feed/hotels/hotel");
            foreach (XmlNode hotel in xmlHotels)
            {
                string cityCode = string.Empty;
                string hotelCode = string.Empty;
                string hotelName = string.Empty;
                XmlNode tempNode = hotel.SelectSingleNode("hotel_id");
                if (tempNode != null)
                {
                    hotelCode = tempNode.InnerText;
                }
                tempNode = hotel.SelectSingleNode("hotel_name");
                if (tempNode != null)
                {
                    hotelName = tempNode.InnerText;
                }
                tempNode = hotel.SelectSingleNode("city_id");
                if (tempNode != null)
                {
                    cityCode = tempNode.InnerText;
                }
                try
                {
                    //calling static data
                    GetHotelDetailsInformation(cityCode, hotelName, hotelCode);
                }
                catch { }
            }
        }
        /// <summary>
        /// GetHotelDetailsInformation
        /// </summary>
        /// <param name="cityCode">HotelCityCode</param>
        /// <param name="hotelName">HotelName</param>
        /// <param name="hotelCode">HotelCode</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Every 15days (what we configuraed in App.config) Need to download StaticData
        /// here we are checking if static data is not there our DB we are sending static data request and get the response save our database
        /// </remarks>
        public HotelDetails GetHotelDetailsInformation(string cityCode, string hotelName, string hotelCode)
        {
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData staticInfo = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();

            //get the info whether to update the static data
            int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.AgodaConfig["TimeStamp"]);
            bool imageUpdate = true;
            bool isUpdateReq = false;
            bool isReqSend = false;

            string request = string.Empty;
            string resp = string.Empty;

            try
            {
                staticInfo.Load(hotelCode, cityCode, HotelBookingSource.Agoda);
                imageInfo.Load(hotelCode, cityCode, HotelBookingSource.Agoda);
                if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                {
                    imageUpdate = false;
                }
                if (staticInfo.HotelName != null && staticInfo.HotelName.Length > 0 && !string.IsNullOrEmpty(staticInfo.HotelDescription) && !string.IsNullOrEmpty(staticInfo.HotelAddress))
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(staticInfo.TimeStamp);
                    //if it is less than REquired time stamp days then load from DB
                    if (diffRes.Days > timeStampDays)
                    {
                        // Set the variable as true
                        isUpdateReq = true;
                        imageUpdate = true;
                    }
                    else
                    {
                        hotelInfo.HotelCode = staticInfo.HotelCode;
                        hotelInfo.HotelName = staticInfo.HotelName;
                        hotelInfo.Map = staticInfo.HotelMap;
                        hotelInfo.Description = staticInfo.HotelDescription;
                        hotelInfo.HotelPolicy = staticInfo.HotelPolicy;

                        string[] image = new string[0];
                        List<string> dldImgList = new List<string>();
                        {
                            string[] dldImages = imageInfo.Images.Split('|');
                            foreach (string img in dldImages)
                            {
                                if (!string.IsNullOrEmpty(img)) dldImgList.Add(img);
                            }
                        }
                        hotelInfo.Images = dldImgList;
                        //hotelInfo.Image = dldImgList.Count > 0 ? dldImgList[0] : "";
                        hotelInfo.Address = staticInfo.HotelAddress;
                        hotelInfo.PhoneNumber = staticInfo.PhoneNumber;
                        hotelInfo.PinCode = staticInfo.PinCode;
                        hotelInfo.Email = staticInfo.EMail;
                        hotelInfo.URL = staticInfo.URL;
                        hotelInfo.FaxNumber = staticInfo.FaxNumber;
                        Dictionary<string, string> attractions = new Dictionary<string, string>();
                        hotelInfo.Attractions = attractions;
                        List<string> facilities = new List<string>();
                        List<string> roomFacilities = new List<string>();
                        if (!string.IsNullOrEmpty(staticInfo.HotelFacilities))
                        {
                            string[] hotelFacility = staticInfo.HotelFacilities.Split('$');

                            string[] facilList = hotelFacility[0].Split('|');
                            foreach (string facl in facilList)
                            {
                                facilities.Add(facl);
                            }
                            if (hotelFacility.Length > 1)
                            {
                                string[] roomFacilList = hotelFacility[1].Split('|');
                                foreach (string roomFacl in roomFacilList)
                                {
                                    roomFacilities.Add(roomFacl);
                                }
                            }
                        }
                        hotelInfo.HotelFacilities = facilities;
                        hotelInfo.RoomFacilities = roomFacilities;
                        List<string> locations = new List<string>();
                        string[] locationList = staticInfo.HotelLocation.Split('|');
                        foreach (string loc in locationList)
                        {
                            locations.Add(loc);
                        }
                        hotelInfo.Location = locations;
                    }
                }
                else
                {
                    isReqSend = true;
                }
                if (isReqSend || isUpdateReq)
                {
                    AgodaStatic childPolicy = new AgodaStatic();
                    hotelInfo = GetHotelDetails(hotelCode,ref childPolicy);
                    childPolicy.CityCode = cityCode;
                    childPolicy.Source = (int)HotelBookingSource.Agoda;
                    // Saving in Cache
                    if (hotelInfo.HotelName == null)
                    {
                        return hotelInfo;
                    }
                    else
                    {
                        staticInfo.HotelCode = hotelCode;
                        staticInfo.HotelName = hotelInfo.HotelName;
                        staticInfo.HotelAddress = hotelInfo.Address;
                        staticInfo.CityCode = cityCode;
                        List<string> facilities = hotelInfo.HotelFacilities;
                        string facilty = string.Empty;
                        foreach (string facl in facilities)
                        {
                            facilty += facl + "|";
                        }
                        List<string> roomFacilities = hotelInfo.RoomFacilities;
                        string roomFacilty = string.Empty;
                        foreach (string roomFacl in roomFacilities)
                        {
                            roomFacilty += roomFacl + "|";
                        }
                        staticInfo.HotelFacilities = facilty + "$" + roomFacilty;
                        staticInfo.SpecialAttraction = string.Empty;

                        List<string> locations2 = hotelInfo.Location;
                        string location = string.Empty;
                        foreach (string loc in locations2)
                        {
                            location = location + loc + "|";
                        }
                        staticInfo.HotelLocation = location;
                        staticInfo.HotelMap = hotelInfo.Map;
                        staticInfo.HotelDescription = hotelInfo.Description;
                        staticInfo.FaxNumber = hotelInfo.FaxNumber;
                        staticInfo.EMail = hotelInfo.Email;
                        staticInfo.PinCode = hotelInfo.PinCode;
                        staticInfo.PhoneNumber = hotelInfo.PhoneNumber;
                        staticInfo.URL = hotelInfo.URL;
                        staticInfo.Source = HotelBookingSource.Agoda;
                        staticInfo.Rating = hotelInfo.hotelRating;
                        staticInfo.HotelPolicy = hotelInfo.HotelPolicy;
                        staticInfo.Status = true;
                        staticInfo.Save();
                        //images
                        List<string> imgList = hotelInfo.Images;
                        string images = string.Empty;
                        foreach (string img in imgList)
                        {
                            images += img + "|";
                        }
                        imageInfo.Images = images;
                        imageInfo.CityCode = cityCode;
                        imageInfo.HotelCode = hotelCode;
                        imageInfo.Source = HotelBookingSource.Agoda;
                        imageInfo.DownloadedImgs = images;

                        if (isUpdateReq)
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Update();
                            }
                        }
                        else
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Save();
                            }
                        }
                        try
                        {
                            //child policy saving 
                            if (!string.IsNullOrEmpty(childPolicy.CityCode) && !string.IsNullOrEmpty(childPolicy.HotelCode))
                            {
                                childPolicy.Save();
                            }
                        }
                        catch { }

                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaStatic, Severity.High, 0, "Exception returned from Agoda.HotelStatic Error Message:" + ex.Message + " | " + DateTime.Now, "");
            }
            return hotelInfo;
        }
        /// <summary>
        /// GetHotelDetails
        /// </summary>
        /// <param name="hotelCode">HotelCode</param>
        /// <param name="childPolicy">Child Policy Means All Child Details Collecting</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Here we are preparing HotelDetails request object and sending HotelDetail to api</remarks>
        public HotelDetails GetHotelDetails(string hotelCode,ref AgodaStatic childPolicy)
        {
            HotelDetails hotelDetails = new HotelDetails();
            string resp = string.Empty;
            try
            {
                resp = SendGetRequest(ConfigurationSystem.AgodaConfig["HotelStatic"] + "&apikey=" + apiKey + "&mhotel_id=" + hotelCode); //Here 1 Means English
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaStatic, Severity.High, 0, "Exception returned from Agoda.GetHotelDetails Error Message:" + ex.Message + " | " + DateTime.Now, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                hotelDetails= ReadHotelDetailsResponse(resp,ref childPolicy);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaStatic, Severity.High, 0, "Exception returned from Agoda.GetHotelDetails Error Message:" + ex.ToString() + " | " + DateTime.Now, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return hotelDetails;
        }
        /// <summary>
        /// ReadHotelDetailsResponse
        /// </summary>
        /// <param name="response">hotelDetails Response</param>
        /// <param name="childPolicy">childPolicy(Here Child policy Means child Ages based on the searhc crateria)</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Here only we are Reading all static data</remarks>
        private HotelDetails ReadHotelDetailsResponse(string response, ref AgodaStatic childPolicy)
        {
            HotelDetails hotelDetails = new HotelDetails();
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(response);
                try
                {
                    string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelStaticDataResponse.xml";
                    xmldoc.Save(filePath);
                }
                catch { }

                XmlNode hotelDetail = xmldoc.SelectSingleNode("Hotel_feed_full/hotels/hotel");
                if (hotelDetail != null)
                {
                    XmlNode tempNode = hotelDetail.SelectSingleNode("hotel_id");
                    if (tempNode != null)
                    {
                        hotelDetails.HotelCode = tempNode.InnerText;
                    }
                    tempNode = hotelDetail.SelectSingleNode("hotel_name");
                    if (tempNode != null)
                    {
                        hotelDetails.HotelName = tempNode.InnerText;
                    }
                    tempNode = hotelDetail.SelectSingleNode("star_rating");
                    //modified by brahmam some time rating given 3.5 that time we need to show 3.0 only
                    if (tempNode != null && !string.IsNullOrEmpty(tempNode.InnerText))
                    {
                        hotelDetails.hotelRating = (HotelRating)Convert.ToInt32(Math.Floor(Convert.ToDecimal(tempNode.InnerText)));
                    }
                    XmlNode longitude = hotelDetail.SelectSingleNode("longitude");
                    XmlNode latitude = hotelDetail.SelectSingleNode("latitude");
                    if (longitude != null && latitude != null)
                    {
                        hotelDetails.Map = latitude.InnerText + "|" + longitude.InnerText;
                    }
                    tempNode = hotelDetail.SelectSingleNode("phone_no");
                    if (tempNode != null)
                    {
                        hotelDetails.PhoneNumber = tempNode.InnerText;
                    }
                    tempNode = hotelDetail.SelectSingleNode("hotel_url");
                    if (tempNode != null)
                    {
                        hotelDetails.URL = tempNode.InnerText;
                    }
                    tempNode = hotelDetail.SelectSingleNode("remark");
                    if (tempNode != null)
                    {
                        hotelDetails.HotelPolicy = tempNode.InnerText;
                    }
                    tempNode = hotelDetail.SelectSingleNode("nationality_restrictions");
                    if (tempNode != null)
                    {
                        if (!string.IsNullOrEmpty(tempNode.InnerText))
                        {
                            string note = "The following nationalities " + tempNode.InnerText + "are restricted to book this Hotel.";
                            if (!string.IsNullOrEmpty(hotelDetails.HotelPolicy))
                            {
                                hotelDetails.HotelPolicy = hotelDetails.HotelPolicy + "," + note;
                            }
                            else
                            {
                                hotelDetails.HotelPolicy = note;
                            }
                        }
                    }
                    hotelDetails.FaxNumber = string.Empty;
                    XmlNode childBedPolicy = hotelDetail.SelectSingleNode("child_and_extra_bed_policy");
                    if (childBedPolicy != null)
                    {
                        childPolicy.HotelCode = hotelDetails.HotelCode;
                        tempNode = childBedPolicy.SelectSingleNode("infant_age");
                        if (tempNode != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode.InnerText))
                            {
                                childPolicy.InfantAge = Convert.ToInt32(tempNode.InnerText);
                            }
                            else
                            {
                                childPolicy.InfantAge = 0;
                            }
                        }
                        tempNode = childBedPolicy.SelectSingleNode("children_age_from");
                        if (tempNode != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode.InnerText))
                            {
                                childPolicy.ChildAgeFrom = Convert.ToInt32(tempNode.InnerText);
                            }
                            else
                            {
                                childPolicy.ChildAgeFrom = 0;
                            }
                        }
                        tempNode = childBedPolicy.SelectSingleNode("children_age_to");
                        if (tempNode != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode.InnerText))
                            {
                                childPolicy.ChildAgeTo = Convert.ToInt32(tempNode.InnerText);
                            }
                            else
                            {
                                childPolicy.ChildAgeTo = 0;
                            }
                        }
                        tempNode = childBedPolicy.SelectSingleNode("children_stay_free");
                        if (tempNode != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode.InnerText))
                            {
                                childPolicy.ChildStayFee = Convert.ToBoolean(tempNode.InnerText);
                            }
                            else
                            {
                                childPolicy.ChildStayFee = false;
                            }
                        }
                        tempNode = childBedPolicy.SelectSingleNode("min_guest_age");
                        if (tempNode != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode.InnerText))
                            {
                                childPolicy.MinGuestAge = Convert.ToInt32(tempNode.InnerText);
                            }
                            else
                            {
                                childPolicy.MinGuestAge = 0;
                            }
                        }
                    }
                }

                XmlNodeList addressList = xmldoc.SelectNodes("Hotel_feed_full/addresses/address");
                string address = string.Empty;
                if (addressList != null)
                {
                    foreach (XmlNode tempAddress in addressList)
                    {
                        XmlNode tempNode1 = tempAddress.SelectSingleNode("address_line_1");
                        if (tempNode1 != null)
                        {
                            address = tempNode1.InnerText;
                        }
                        tempNode1 = tempAddress.SelectSingleNode("address_line_2");
                        if (tempNode1 != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode1.InnerText))
                            {
                                if (!string.IsNullOrEmpty(address))
                                {
                                    address = address + "," + tempNode1.InnerText;
                                }
                                else
                                {
                                    address = tempNode1.InnerText;
                                }
                            }
                        }
                        tempNode1 = tempAddress.SelectSingleNode("postal_code");
                        if (tempNode1 != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode1.InnerText))
                            {
                                if (!string.IsNullOrEmpty(address))
                                {
                                    address = address + "," + tempNode1.InnerText;
                                }
                                else
                                {
                                    address = tempNode1.InnerText;
                                }
                            }
                        }
                        tempNode1 = tempAddress.SelectSingleNode("state");
                        if (tempNode1 != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode1.InnerText))
                            {
                                if (!string.IsNullOrEmpty(address))
                                {
                                    address = address + "," + tempNode1.InnerText;
                                }
                                else
                                {
                                    address = tempNode1.InnerText;
                                }
                            }
                        }
                        tempNode1 = tempAddress.SelectSingleNode("city");
                        if (tempNode1 != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode1.InnerText))
                            {
                                if (!string.IsNullOrEmpty(address))
                                {
                                    address = address + "," + tempNode1.InnerText;
                                }
                                else
                                {
                                    address = tempNode1.InnerText;
                                }
                            }
                        }
                        tempNode1 = tempAddress.SelectSingleNode("country");
                        if (tempNode1 != null)
                        {
                            if (!string.IsNullOrEmpty(tempNode1.InnerText))
                            {
                                hotelDetails.CountryName = tempNode1.InnerText;
                                if (!string.IsNullOrEmpty(address))
                                {
                                    address = address + "," + tempNode1.InnerText;
                                }
                                else
                                {
                                    address = tempNode1.InnerText;
                                }
                            }
                        }
                        break;
                    }
                }
                hotelDetails.Address = address;
                XmlNode tempNode2 = xmldoc.SelectSingleNode("Hotel_feed_full/hotel_descriptions/hotel_description/overview");
                if (tempNode2 != null)
                {
                    hotelDetails.Description = tempNode2.InnerText;
                }
                tempNode2 = xmldoc.SelectSingleNode("Hotel_feed_full/hotel_descriptions/hotel_description/snippet");
                if (tempNode2 != null)
                {
                    hotelDetails.Description = hotelDetails.Description + "</br>" + tempNode2.InnerText;
                }
                hotelDetails.Email = string.Empty;
                hotelDetails.HotelFacilities = new List<string>();
                hotelDetails.RoomFacilities = new List<string>();
                XmlNodeList facilityList = xmldoc.SelectNodes("Hotel_feed_full/facilities/facility");
                if (facilityList != null)
                {
                    foreach (XmlNode facility in facilityList)
                    {
                        XmlNode tempNode2typeFacility = facility.SelectSingleNode("property_group_description");
                        if (tempNode2typeFacility != null)
                        {
                            tempNode2 = facility.SelectSingleNode("property_name");
                            if (tempNode2 != null)
                            {
                                if (tempNode2typeFacility.InnerText == "Hotel Facilities")
                                {
                                    hotelDetails.HotelFacilities.Add(tempNode2.InnerText);
                                }
                                else if (tempNode2typeFacility.InnerText == "Room Facilities")
                                {
                                    hotelDetails.RoomFacilities.Add(tempNode2.InnerText);
                                }
                            }
                        }
                    }
                }
                hotelDetails.Images = new List<string>();
                XmlNodeList imageList = xmldoc.SelectNodes("Hotel_feed_full/pictures/picture");
                foreach (XmlNode img in imageList)
                {
                    tempNode2 = img.SelectSingleNode("URL");
                    if (tempNode2 != null)
                    {
                        hotelDetails.Images.Add(tempNode2.InnerText);
                    }
                }
                hotelDetails.Location = new List<string>();
            }
            catch (Exception ex)
            {
                throw new Exception("Agoda: Failed to read Hotel Details response ", ex);
            }

            return hotelDetails;
        }
        #endregion

        #region HotelBooking LIst
        /// <summary>
        /// GetHotelDetailsList
        /// </summary>
        /// <param name="fromDate">selected From Date</param>
        /// <param name="toDate">selected to Date</param>
        /// <param name="bookRefNo">Hotel Booking RefNo</param>
        /// <returns>List Agoda_HotelsList </returns>
        /// <remarks>Here Two ways we are calling api
        /// First one If bookRefNo is there we need to call BookingDetailsRequestV2
        /// Second one booking ref is not there need to call BookingListRequestV2
        /// </remarks>
        public List<Agoda_HotelsList> GetHotelDetailsList(string fromDate, string toDate, string bookRefNo)
        {
            List<Agoda_HotelsList> resBookingList = new List<Agoda_HotelsList>();
            string strRequest = string.Empty, strUrl = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                if (!string.IsNullOrEmpty(bookRefNo))
                {
                    strUrl = ConfigurationSystem.AgodaConfig["BookingDetails"];
                    strRequest = GenerateBookingDetailsRequest(bookRefNo);
                    try
                    {
                        string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_AgodaHotelBookingDetailsRequest.xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(strRequest);
                        doc.Save(filePath);
                    }
                    catch { }
                    xmlResp = SendRequest(strRequest, strUrl);
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        resBookingList = GetHotelDetails(xmlResp);
                    }
                }
                else
                {
                    strUrl = ConfigurationSystem.AgodaConfig["BookingList"];
                    strRequest = GenerateBookingListRequest(fromDate, toDate);
                    try
                    {
                        string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_AgodaHotelBookingListRequest.xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(strRequest);
                        doc.Save(filePath);
                    }
                    catch { }
                    xmlResp = SendRequest(strRequest, strUrl);
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        resBookingList = GetHotelList(xmlResp);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaStatic, Severity.Normal, 0, "Exception returned from Agoda.GetHotelDetailsList Error Message:" + ex.ToString(), "");
                Agoda_HotelsList errorHotelList = new Agoda_HotelsList();
                errorHotelList.ErrorMessage = ex.Message;
                resBookingList.Add(errorHotelList);
            }
            return resBookingList;
        }
        /// <summary>
        /// GenerateBookingDetailsRequest
        /// </summary>
        /// <param name="bookingId">BookingId</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenerateBookingDetailsRequest(string bookingId)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("BookingDetailsRequestV2", "http://xml.agoda.com");//CancellationRequestV2 started
                xmlString.WriteAttributeString("siteid", siteId);
                xmlString.WriteAttributeString("apikey", apiKey);
                xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                xmlString.WriteElementString("BookingID", bookingId);
                xmlString.WriteEndElement();//CancellationRequestV2
                xmlString.Close();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaStatic, Severity.Normal, 0, "Exception returned from Agoda.GenerateBookingDetailsRequest Error Message:" + ex.ToString(), "");
            }
            return strWriter.ToString();
        }
        /// <summary>
        /// GenerateBookingListRequest
        /// </summary>
        /// <param name="strFromDate">From Date</param>
        /// <param name="strToDate">To Date</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenerateBookingListRequest(string strFromDate, string strToDate)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("BookingListRequestV2", "http://xml.agoda.com");//CancellationRequestV2 started
                xmlString.WriteAttributeString("siteid", siteId);
                xmlString.WriteAttributeString("apikey", apiKey);
                xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                xmlString.WriteStartElement("DateRange");
                xmlString.WriteElementString("FromDate", strFromDate);
                xmlString.WriteElementString("ToDate", strToDate);
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();//CancellationRequestV2
                xmlString.Close();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgodaStatic, Severity.Normal, 0, "Exception returned from Agoda.GenerateBookingListRequest Error Message:" + ex.ToString(), "");
            }
            return strWriter.ToString();
        }
        /// <summary>
        /// GetHotelDetails
        /// </summary>
        /// <param name="xmlDoc">Booked HotelDetails Xml</param>
        /// <returns>List Agoda_HotelsList</returns>
        /// <remarks>Here Only we are reading particulat HotelDetails</remarks>
        private List<Agoda_HotelsList> GetHotelDetails(XmlDocument xmlDoc)
        {
            List<Agoda_HotelsList> ResultList = new List<Agoda_HotelsList>();
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_AgodaHotelBookingDetailsResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            try
            {
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("Agoda", "http://xml.agoda.com");
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//Agoda:BookingDetailsResponseV2/Agoda:ErrorMessages/Agoda:ErrorMessage ", nsmgr);
                if (ErrorInfo != null)
                {
                    Agoda_HotelsList errorHotelList = new Agoda_HotelsList();
                    errorHotelList.ErrorMessage = ErrorInfo.InnerText;
                    ResultList.Add(errorHotelList);
                    Audit.Add(EventType.AgodaAvailSearch, Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                }
                else
                {
                    XmlNode tempNode = xmlDoc.SelectSingleNode("//Agoda:BookingDetailsResponseV2", nsmgr).Attributes["status"];
                    if (tempNode != null && tempNode.InnerText == "200") //Here Status 200 Means Response Success
                    {
                        XmlNodeList hotelList = xmlDoc.SelectNodes("//Agoda:BookingDetailsResponseV2/Agoda:Bookings/Agoda:Booking", nsmgr);
                        foreach (XmlNode xmlHotel in hotelList)
                        {
                            Agoda_HotelsList agodaHotelList = new Agoda_HotelsList();
                            tempNode = xmlHotel.SelectSingleNode("//Agoda:BookingID",nsmgr);
                            if (tempNode != null)
                            {
                                agodaHotelList.BookingId = tempNode.InnerText;
                            }
                            tempNode = xmlHotel.SelectSingleNode("//Agoda:Hotel", nsmgr);
                            if (tempNode != null)
                            {
                                agodaHotelList.HotelName = tempNode.InnerText;
                            }
                            tempNode = xmlHotel.SelectSingleNode("//Agoda:City", nsmgr);
                            if (tempNode != null)
                            {
                                agodaHotelList.City = tempNode.InnerText;
                            }
                            tempNode = xmlHotel.SelectSingleNode("//Agoda:CheckInDate", nsmgr);
                            if (tempNode != null)
                            {
                                agodaHotelList.Checkindate = tempNode.InnerText;
                            }
                            tempNode = xmlHotel.SelectSingleNode("//Agoda:CheckOutDate", nsmgr);
                            if (tempNode != null)
                            {
                                agodaHotelList.Checkoutdate = tempNode.InnerText;
                            }
                            tempNode = xmlHotel.SelectSingleNode("//Agoda:Status", nsmgr);
                            if (tempNode != null)
                            {
                                agodaHotelList.Status = tempNode.InnerText;
                            }
                            ResultList.Add(agodaHotelList);
                        }
                    }
                    else
                    {
                        Agoda_HotelsList errorHotelList = new Agoda_HotelsList();
                        errorHotelList.ErrorMessage = "Invalid status returned.";// ErrorInfo.InnerText;
                        ResultList.Add(errorHotelList);
                        Audit.Add(EventType.AgodaAvailSearch, CT.Core.Severity.High, 0, "Error returned from Agoda. Error Message: Invalid status returned.| " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                    }
                }
            }
            catch (Exception ex)
            {
                Agoda_HotelsList errorHotelList = new Agoda_HotelsList();
                errorHotelList.ErrorMessage = ex.Message;// ErrorInfo.InnerText;
                ResultList.Add(errorHotelList);
                Audit.Add(EventType.AgodaAvailSearch, CT.Core.Severity.High, 0, "Error returned from Agoda GetHotelDetails method. Error Message:" + ex.ToString(), "");
            }
            return ResultList;
        }
        /// <summary>
        /// GetHotelList
        /// </summary>
        /// <param name="xmlDoc">Booking Hotels Xml</param>
        /// <returns>List Agoda_HotelsList</returns>
        /// <remarks>Here Only we are reading All Booked HotelDetails</remarks>
        private List<Agoda_HotelsList> GetHotelList(XmlDocument xmlDoc)
        {
            List<Agoda_HotelsList> ResultList = new List<Agoda_HotelsList>();
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_AgodaHotelBookingListResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            try
            {
               
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("Agoda", "http://xml.agoda.com");

                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("//Agoda:BookingListResponseV2/Agoda:ErrorMessages/Agoda:ErrorMessage ", nsmgr);
                if (ErrorInfo != null)
                {
                    Agoda_HotelsList ObjA = new Agoda_HotelsList();
                    ObjA.ErrorMessage = ErrorInfo.InnerText;
                    ResultList.Add(ObjA);
                    Audit.Add(EventType.AgodaAvailSearch, Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                }
                else
                {
                    XmlNode tempNode = xmlDoc.SelectSingleNode("//Agoda:BookingListResponseV2", nsmgr).Attributes["status"];
                    //string searchId = string.Empty;
                    if (tempNode != null && tempNode.InnerText == "200") //Here Status 200 Means Response Success
                    {
                        //tempNode = xmlDoc.SelectSingleNode("//Agoda:BookingListResponseV2", nsmgr).Attributes["searchid"];
                        //searchId = tempNode.InnerText;

                        XmlNodeList hotelList = xmlDoc.SelectNodes("//Agoda:BookingListResponseV2/Agoda:Bookings/Agoda:Booking", nsmgr);
                        foreach (XmlNode nodeHotel in hotelList)
                        {
                            Agoda_HotelsList ObjA = new Agoda_HotelsList();
                            XmlAttribute xmlAttri = nodeHotel.Attributes["id"];
                            if (xmlAttri != null && !string.IsNullOrEmpty(xmlAttri.Value))
                            {
                                ObjA.BookingId = xmlAttri.Value;
                            }
                            xmlAttri = nodeHotel.Attributes["hotelname"];
                            if (xmlAttri != null && !string.IsNullOrEmpty(xmlAttri.Value))
                            {
                                ObjA.HotelName = xmlAttri.Value;
                            }
                            xmlAttri = nodeHotel.Attributes["cityname"];
                            if (xmlAttri != null && !string.IsNullOrEmpty(xmlAttri.Value))
                            {
                                ObjA.City = xmlAttri.Value;
                            }
                            xmlAttri = nodeHotel.Attributes["arrival"];
                            if (xmlAttri != null && !string.IsNullOrEmpty(xmlAttri.Value))
                            {
                                ObjA.Checkindate = xmlAttri.Value;
                            }
                            xmlAttri = nodeHotel.Attributes["departure"];
                            if (xmlAttri != null && !string.IsNullOrEmpty(xmlAttri.Value))
                            {
                                ObjA.Checkoutdate = xmlAttri.Value;
                            }
                            xmlAttri = nodeHotel.Attributes["status"];
                            if (xmlAttri != null && !string.IsNullOrEmpty(xmlAttri.Value))
                            {
                                ObjA.Status = xmlAttri.Value;
                            }
                            ResultList.Add(ObjA);
                        }
                    }
                    else
                    {
                        Agoda_HotelsList ObjA = new Agoda_HotelsList();
                        ObjA.ErrorMessage = "Invalid status returned.";// ErrorInfo.InnerText;
                        ResultList.Add(ObjA);
                        Audit.Add(EventType.AgodaAvailSearch, CT.Core.Severity.High, 0, "Error returned from Agoda. Error Message: Invalid status returned.| " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                    }
                }
            }
            catch (Exception ex)
            {
                Agoda_HotelsList errorHotelList = new Agoda_HotelsList();
                errorHotelList.ErrorMessage = ex.Message;// ErrorInfo.InnerText;
                ResultList.Add(errorHotelList);
                Audit.Add(EventType.AgodaAvailSearch, CT.Core.Severity.High, 0, "Error returned from Agoda. Error Message: GetHotelList exception :" + ex.ToString(), "");
            }
            return ResultList;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls
        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// <summary>
        ///  ~Agoda
        /// </summary>
        ~Agoda()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// Dispose
        /// </summary>
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #endregion
    }
    /// <summary>
    /// This class is used to get Booked HotelList
    /// </summary>
    public class Agoda_HotelsList
    {
        #region variables 
        /// <summary>
        /// BookingID
        /// </summary>
        string bookingId;
        /// <summary>
        /// BookingStatus
        /// </summary>
        string status;
        /// <summary>
        /// Booking Country
        /// </summary>
        string country;
        /// <summary>
        /// Booking City
        /// </summary>
        string city;
        /// <summary>
        /// booked Hotel Name
        /// </summary>
        string hotelname;
        /// <summary>
        /// Checkin Date
        /// </summary>
        string checkindate;
        /// <summary>
        /// CheckOut Date
        /// </summary>
        string checkoutdate;
        /// <summary>
        /// Error Message
        /// </summary>
        string errorMessage;
        #endregion

        #region Properties
        /// <summary>
        /// BookingId
        /// </summary>
        public string BookingId
        {
            get
            {
                return bookingId;
            }

            set
            {
                bookingId = value;
            }
        }
        /// <summary>
        /// Booking status
        /// </summary>

        public string Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }
        /// <summary>
        /// Booked Country
        /// </summary>

        public string Country
        {
            get
            {
                return country;
            }

            set
            {
                country = value;
            }
        }
        /// <summary>
        /// Booked City
        /// </summary>
        public string City
        {
            get
            {
                return city;
            }

            set
            {
                city = value;
            }
        }
        /// <summary>
        /// Booked Hotel Name
        /// </summary>

        public string HotelName
        {
            get
            {
                return hotelname;
            }

            set
            {
                hotelname = value;
            }
        }
        /// <summary>
        /// CheckIn Date
        /// </summary>
        public string Checkindate
        {
            get
            {
                return checkindate;
            }

            set
            {
                checkindate = value;
            }
        }

        /// <summary>
        /// CheckOut Date
        /// </summary>
        public string Checkoutdate
        {
            get
            {
                return checkoutdate;
            }

            set
            {
                checkoutdate = value;
            }
        }
        /// <summary>
        /// Any Error
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }

            set
            {
                errorMessage = value;
            }
        }


        #endregion
    }
}

