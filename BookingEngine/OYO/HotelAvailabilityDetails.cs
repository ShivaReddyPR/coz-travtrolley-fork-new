﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine.GDS
{
    class HotelAvailabilityDetails
    {
        public HotelAvailability HotelAvailability { get; set; }

    }
    public class HotelAvailability
    {
        public string checkInDate { get; set; }
        public string checkOutDate { get; set; }
        public int adults { get; set; }
        public int rooms { get; set; }
        public string City { get; set; }
        public List<string> HotelID { get; set; }
        public int children { get; set; }
        public int child_1_age { get; set; }
        public int child_2_age { get; set; }
        public int child_3_age { get; set; }
        public int child_4_age { get; set; }
        public int child_5_age { get; set; }
        public int child_6_age { get; set; }
        public int child_7_age { get; set; }
        public int child_8_age { get; set; }
        public List<int> room_combinations { get; set; }
    }
}
