﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.BookingEngine.GDS
{
    class HTLBookingDetails
    {
        public Guest guest { get; set; }
        public Booking booking { get; set; }
    }
    #region Classes for Provisional Booking  

    public class Guest
    {
        public string first_name { get; set; }
        public string last_name { get; set; } 
        public string email { get; set; }
        public string phone { get; set; }
        public string country_code { get; set; }
    }
        public class Booking
    {
        
        public int single { get; set; }
        public int Double { get; set; }
        public int extra { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
        public string hotel_id { get; set; }  
        public string external_reference_id { get; set; }
        public string is_provisional { get; set; } 
    }
    #endregion
    #region Classes for Provisional Booking Confirm 
    public class ProvisionalBooking
    {
        public Payments payments { get; set; }
        public ProBooking booking { get; set; }
    }

    public class ProBooking
    { 
        public string external_reference_id { get; set; } 
        public string status { get; set; }
    }


    public class Payments
    {        
        public string bill_to_affiliate { get; set; } 
    }

    #endregion

    #region Class for Update GSTIN Invoice against Booking Confirm 
    public class UpdateGSTIN
    {
        public string name { get; set; }
        public string gstin { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string contact { get; set; } 
    }
   
    #endregion

    #region Class for Cancellation confirmation
    public class ConfirmCancellation
    {
        public Cancellation booking { get; set; }
    }
    public class Cancellation
    { 
        public string status { get; set; }
    }
    #endregion
}
