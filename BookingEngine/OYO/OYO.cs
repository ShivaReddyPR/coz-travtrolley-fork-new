﻿using CT.Configuration;
using CT.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

namespace CT.BookingEngine.GDS
{
    public class OYO : IDisposable
    {
        #region Members
        /// <summary>
        /// To get Login userId
        /// </summary>
        private long appUserId;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        private string sessionId;
        decimal rateOfExchange = 1;
        string agentCurrency = "";
        Dictionary<string, decimal> exchangeRates;
        int decimalPoint;
        string sourceCountryCode;
        /// <summary>
        /// To Store logs file path of Request and Response of whole Booking Process(Search to Cancellation)
        /// </summary>
        private string XmlPath = string.Empty;
        /// <summary>
        /// language which is accepted by Supplier(OYO) , by default(en). Configured from OYO.Config.xml and Loaded from ConfigurationSystem.cs OYOConfig 
        /// </summary>
        private string language = string.Empty;
        /// <summary>
        /// currency which is accepted by Supplier(OYO - INR) ,configured in OYO.Config.xml and Loaded from ConfigurationSystem.cs OYOConfig 
        /// </summary>
        private string currency = string.Empty;
        /// <summary>
        /// This Id is given by Supplier(OYO) ,configured in OYO.Config.xml and Loaded from ConfigurationSystem.cs OYOConfig 
        /// </summary>
        private string apiKey = string.Empty;
        /// <summary>
        /// City/Hotel Details Search end point URL given by Supplier(OYO) ,configured in OYO.Config.xml and Loaded from ConfigurationSystem.cs OYOConfig 
        /// </summary>
        private string searchAvailability = string.Empty;
        /// <summary>
        /// Provisional Booking end point URL given by Supplier(OYO) ,configured in OYO.Config.xml and Loaded from ConfigurationSystem.cs OYOConfig  
        /// /// </summary>
        private string provisionalBooking = string.Empty;
        ///Provisional Booking Confirmation end point URL given by Supplier(OYO) ,configured in OYO.Config.xml and Loaded from ConfigurationSystem.cs OYOConfig  
        /// </summary>
        private string provisionalBookingConfirm = string.Empty;
        ///Update GST invoice details(customer details) against booking id , for this end point URL is given by Supplier(OYO)
        ///and configured in OYO.Config.xml and Loaded from ConfigurationSystem.cs OYOConfig  
        /// </summary>
        private string updateGSTINBooking = string.Empty;
        /// <summary>
        /// Initiate to get Booking Cancellation charges end point URL given by Supplier(OYO) ,configured in OYO.Config.xml 
        /// and Loaded from ConfigurationSystem.cs OYOConfig 
        /// </summary>
        private string cancellationCharge = string.Empty;
        /// <summary>
        /// Confirm cancellation end point URL given by Supplier(OYO) ,configured in OYO.Config.xml and Loaded from ConfigurationSystem.cs OYOConfig 
        /// </summary>
        private string bookingCancellation = string.Empty;
        #endregion

        #region Constructors
        /// <summary>
        ///  Constructor ,here loading App config details.
        /// </summary>
        /// <param name="sessionId"></param>
        public OYO()
        {
            LoadCredentials();
        }
        /// <summary>
        /// Parmeterized Constructor here only loading first time sessionId
        /// </summary>
        /// <param name="sessionId"></param>
        public OYO(string sessionId) : this()
        {
            this.sessionId = sessionId;
        }
        private void LoadCredentials()
        {
            //Create Hotel Xml Log path folder per day wise
            XmlPath = Path.Combine(ConfigurationSystem.OYOConfig["XmlLogPath"], DateTime.Now.ToString("dd-MM-yyyy"));
            apiKey = ConfigurationSystem.OYOConfig["APIKey"];
            language = ConfigurationSystem.OYOConfig["lang"];
            currency = ConfigurationSystem.OYOConfig["currency"];
            searchAvailability = ConfigurationSystem.OYOConfig["SearchAvailabity"];
            provisionalBooking = ConfigurationSystem.OYOConfig["ProvisionalBooking"];
            provisionalBookingConfirm = ConfigurationSystem.OYOConfig["ProvisionalBookingConfirm"];
            updateGSTINBooking = ConfigurationSystem.OYOConfig["UpdateGSTIN"];
            cancellationCharge = ConfigurationSystem.OYOConfig["CancellationCharge"];
            bookingCancellation = ConfigurationSystem.OYOConfig["BookingCancellation"];
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }
        #endregion

        #region Properties
        public long AppUserId
        {
            get
            {
                return appUserId;
            }

            set
            {
                appUserId = value;
            }
        }
        public string SessionId
        {
            get
            {
                return sessionId;
            }

            set
            {
                sessionId = value;
            }
        }
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        #endregion

        #region Common Methods         
        /// <summary>
        ///PUT/POST : Send the json format request to the server and pulls the json format response . 
        /// </summary>
        /// <param name="requeststring">Xml formatted string</param>
        /// <param name="url">CitySearch/HotelDetail/ProvisionalBooking/FinalBooking/InitiateCancellation/ConfirmCancellation end point Url</param>
        /// <param name="reqType">Type of request(City Search/Hotel Details/Provisional Booking/Final Booking/Update GSTIN/Confirm Cancellation)</param>
        /// <returns>json string</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private string SendRequest(string requeststring, string url, string reqType)
        {
            string responseFromServer = string.Empty;
            HttpWebRequest request = null;
            try
            {
                #region HttpWebResponse    
                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = reqType == "Update GSTIN" ? "PUT" : "POST"; //Using PUT/POST method     
                request.Headers.Add("ACCESS-TOKEN", this.apiKey);
                request.ContentType = "application/json";
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(requeststring);
                // request for compressed response. This does not seem to have any effect now.
                //request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;
                Stream requestWriter = (request.GetRequestStream());
                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();
                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                #endregion
            }
            catch (WebException webEx)
            {
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                responseFromServer = new StreamReader(stream).ReadToEnd();
            }
            return responseFromServer;
        }

        /// <summary>
        ///GET : Send the json format request to the server and pulls the json format response . 
        /// </summary>
        /// <param name="url">Cancellation end point url</param>
        /// <returns>json string</returns>
        private string SendGetRequest(string url)
        {
            string responseFromServer = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET"; //Using GET method       
                //string postData = requeststring;// GETTING XML STRING...
                request.ContentType = "application/json"; ;
                //ServicePointManager.Expect100Continue = true;
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //ServicePointManager.DefaultConnectionLimit = 9999;
                // byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                ////Basic Authentication required to process the request.               
                //string _auth = string.Format("{0}:{1}", siteId, apiKey);
                //request.Headers.Add(HttpRequestHeader.Authorization, _auth);
                request.Headers.Add("ACCESS-TOKEN", apiKey);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                //request.ContentLength = bytes.Length;

                //Stream requestWriter = (request.GetRequestStream());
                //requestWriter.Write(bytes, 0, bytes.Length);
                //requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("OYO-Failed to get response from  Cancellation request : " + ex.Message, ex);
            }
            return responseFromServer;
        }

        //convert compressed
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;

            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }

            return stream;
        }

        /// <summary>
        /// To get no of single rooms , double rooms  and extra beds
        /// </summary>
        /// <param name="roomsList"> No of requested rooms and pax count</param>
        /// <returns>list of rooms </returns>
        private List<int> GetRoomCombinations(List<HotelRoom> roomsList)
        {
            List<int> combinations = new List<int>();
            try
            {
                int roomCount = roomsList.Count;
                int paxCount = 0;
                for (int i = 0; i < roomsList.Count; i++)
                {
                    paxCount += roomsList[i].AdultCount;

                    for (int k = 0; k < roomsList[i].ChildAge.Count; k++)
                    {
                        if (roomsList[i].ChildAge[k] >= 6)
                        {
                            paxCount += 1;
                        }
                    }
                }

                switch (paxCount.ToString())
                {
                    //{ 0, 1, 0 } --> { Noof Single Room(s), Noof Double Room(s), Noof Extra bed(s) }                    
                    case "1": // paxCount
                        if (roomCount == 1)
                            combinations = new List<int> { 1, 0, 0 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    case "2":// paxCount
                        if (roomCount == 1)
                            combinations = new List<int> { 0, 1, 0 };
                        else if (roomCount == 2)
                            combinations = new List<int> { 2, 0, 0 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    case "3":// paxCount
                        if (roomCount == 1)
                            combinations = new List<int> { 0, 1, 1 };
                        else if (roomCount == 2)
                            combinations = new List<int> { 1, 1, 0 };
                        else if (roomCount == 3)
                            combinations = new List<int> { 3, 0, 0 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    case "4":// paxCount
                        if (roomCount == 2)
                            combinations = new List<int> { 0, 2, 0 };
                        else if (roomCount == 3)
                            combinations = new List<int> { 2, 1, 0 };
                        else if (roomCount == 4)
                            combinations = new List<int> { 4, 0, 0 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    case "5":// paxCount
                        if (roomCount == 2)
                            combinations = new List<int> { 0, 2, 1 };
                        else if (roomCount == 3)
                            combinations = new List<int> { 1, 2, 0 };
                        else if (roomCount == 4)
                            combinations = new List<int> { 3, 1, 0 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    case "6":// paxCount
                        if (roomCount == 2)
                            combinations = new List<int> { 0, 2, 2 };
                        else if (roomCount == 3)
                            combinations = new List<int> { 0, 3, 0 };
                        else if (roomCount == 4)
                            combinations = new List<int> { 2, 2, 0 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    case "7":// paxCount
                        if (roomCount == 3)
                            combinations = new List<int> { 0, 3, 1 };
                        else if (roomCount == 4)
                            combinations = new List<int> { 1, 3, 0 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    case "8":// paxCount
                        if (roomCount == 3)
                            combinations = new List<int> { 0, 3, 2 };
                        else if (roomCount == 4)
                            combinations = new List<int> { 2, 2, 0 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    case "9":// paxCount
                        if (roomCount == 3)
                            combinations = new List<int> { 0, 3, 3 };
                        else if (roomCount == 4)
                            combinations = new List<int> { 0, 4, 1 };
                        else
                            combinations = new List<int> { 0, 0, 0 };
                        break;
                    default:
                        combinations = new List<int> { 0, 0, 0 };
                        break;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, "Exception returned from OYO.GetRooms Error Message:" + ex.Message + " | " + DateTime.Now, "");
            }
            return combinations;
        }
        #endregion

        #region Get Hotel Results
        /// <summary>
        /// Returns Hotel Results for the Search Criteria.
        /// </summary>
        /// <param name="req">Hotel Request object</param>
        /// <param name="markup">B2B Markup Type comes from table  against agent - B2B markup</param>
        /// <param name="markupType">B2B Markup Type comes from table against agent - F: Fixed, P: Percentage</param>
        /// <returns>HotelSearchResult[]</returns>
        public HotelSearchResult[] GetHotelAvailability(HotelRequest req, decimal markup, string markupType)
        {
            HotelSearchResult[] searchRes = new HotelSearchResult[0];
            HotelSearchResult[] result;

            // Prapare json format string city search request  
            string request = GenerateHotelAvailabilityRequest(req, string.Empty);
            try
            {
                #region writing  of hotel request into json file 

                // For writing of request data as json file    
                string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CitySearchRequest.json");
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine(request);
                writer.Close();
                #endregion
            }
            catch { }
            #region Send Reqest for Get Hotels
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                //Sending request to OYO end point url for City Search
                string response = string.Empty;
                response = SendRequest(request, searchAvailability, "City Search");

                if (!string.IsNullOrEmpty(response))
                {
                    #region writing  of hotel request into json file 
                    // For writing of request data as json file    
                    string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CitySearchResponse.json");
                    StreamWriter writer = new StreamWriter(filePath);
                    writer.WriteLine(response);
                    writer.Close();
                    #endregion
                    //converting json response in to Xml response 
                    xmlResp = JsonConvert.DeserializeXmlNode(response);

                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                        "Exception returned from OYO.GetHotelAvailability Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request json",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                }), "");

                throw new BookingEngineException("Error: " + ex.Message);
            }
            #endregion
            #region Generate Serch Result
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = GenerateSearchResult(xmlResp, req, markup, markupType);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                        "Exception returned from OYO.GetHotelAvailability Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                }), "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
            {
                    " Response from OYO for City Search Request:| ",
                    DateTime.Now,
                    "| request json:",
                    request,
                    "|response XML:",
                    xmlResp.OuterXml
            }), "");
            result = searchRes;
            #endregion
            return result;
        }

        /// <summary>
        /// Generates Hotel Search Request json for City Search and Hotel Search.
        /// </summary>
        /// <param name="request">Hotel request</param>
        /// <param name="hotelCode">Hotel Code</param>
        /// <returns>json string</returns>
        private string GenerateHotelAvailabilityRequest(HotelRequest request, string hotelCode)
        {
            string requestData = string.Empty;
            HotelAvailabilityDetails hotelDetails = new HotelAvailabilityDetails();
            HotelAvailability hotelAvailability = new HotelAvailability();
            List<int> chldAges = new List<int>();
            try
            {
                hotelAvailability.HotelID = !string.IsNullOrEmpty(hotelCode) ? new List<string> { hotelCode } : new List<string>();
                hotelAvailability.City = CityName(request.CityName);//request.CityName;
                hotelAvailability.checkInDate = request.StartDate.ToString("dd'/'MM'/'yyyy");
                hotelAvailability.checkOutDate = request.EndDate.ToString("dd'/'MM'/'yyyy");
                hotelAvailability.rooms = request.NoOfRooms;
                hotelAvailability.room_combinations = new List<int>();

                for (int i = 0; i < request.NoOfRooms; i++)
                {
                    hotelAvailability.adults += request.RoomGuest[i].noOfAdults;
                    hotelAvailability.children += request.RoomGuest[i].noOfChild;

                    for (int j = 0; j < request.RoomGuest[i].noOfChild; j++)
                    {

                        chldAges.Add(request.RoomGuest[i].childAge[j]);
                    }
                }

                #region ChildAges
                if (hotelAvailability.children > 0)
                {
                    switch (hotelAvailability.children.ToString())
                    {
                        case "1":
                            hotelAvailability.child_1_age = chldAges[0];
                            break;
                        case "2":
                            hotelAvailability.child_1_age = chldAges[0];
                            hotelAvailability.child_2_age = chldAges[1];
                            break;
                        case "3":
                            hotelAvailability.child_1_age = chldAges[0];
                            hotelAvailability.child_2_age = chldAges[1];
                            hotelAvailability.child_3_age = chldAges[2];
                            break;
                        case "4":
                            hotelAvailability.child_1_age = chldAges[0];
                            hotelAvailability.child_2_age = chldAges[1];
                            hotelAvailability.child_3_age = chldAges[2];
                            hotelAvailability.child_4_age = chldAges[3];
                            break;
                        case "5":
                            hotelAvailability.child_1_age = chldAges[0];
                            hotelAvailability.child_2_age = chldAges[1];
                            hotelAvailability.child_3_age = chldAges[2];
                            hotelAvailability.child_4_age = chldAges[3];
                            hotelAvailability.child_5_age = chldAges[4];
                            break;
                        case "6":
                            hotelAvailability.child_1_age = chldAges[0];
                            hotelAvailability.child_2_age = chldAges[1];
                            hotelAvailability.child_3_age = chldAges[2];
                            hotelAvailability.child_4_age = chldAges[3];
                            hotelAvailability.child_5_age = chldAges[4];
                            hotelAvailability.child_6_age = chldAges[5];
                            break;
                        case "7":
                            hotelAvailability.child_1_age = chldAges[0];
                            hotelAvailability.child_2_age = chldAges[1];
                            hotelAvailability.child_3_age = chldAges[2];
                            hotelAvailability.child_4_age = chldAges[3];
                            hotelAvailability.child_5_age = chldAges[4];
                            hotelAvailability.child_6_age = chldAges[5];
                            hotelAvailability.child_7_age = chldAges[6];
                            break;
                        case "8":
                            hotelAvailability.child_1_age = chldAges[0];
                            hotelAvailability.child_2_age = chldAges[1];
                            hotelAvailability.child_3_age = chldAges[2];
                            hotelAvailability.child_4_age = chldAges[3];
                            hotelAvailability.child_5_age = chldAges[4];
                            hotelAvailability.child_6_age = chldAges[5];
                            hotelAvailability.child_7_age = chldAges[6];
                            hotelAvailability.child_8_age = chldAges[7];
                            break;
                    }
                }
                #endregion                

                hotelDetails.HotelAvailability = hotelAvailability;

                requestData = JsonConvert.SerializeObject(hotelDetails);

            }
            catch (Exception ex)
            {
                throw new Exception("OYO-Failed to generate Hotel Request message : " + ex.Message, ex);
            }


            return requestData;
        }
        //for First Char ToUpper of cityName)      
        private string CityName(string cityName)
        {
            // Check for empty string.  
            if (string.IsNullOrEmpty(cityName))
            {
                return string.Empty;
            }
            // Return char and concat substring.  
            cityName = cityName.Trim().ToLower();
            return char.ToUpper(cityName[0]) + cityName.Substring(1);
        }
        /// <summary>
        /// Reading XML response, assigning hotel data to the HotelSearchResult and Displaly Result in SearchResult Page.
        /// </summary>
        /// <param name="xmlDoc">xml Response Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here  we are calucating B2B Markup and InputVat and also Genarting Hotel Results object</remarks>
        private HotelSearchResult[] GenerateSearchResult(XmlDocument xmlDoc, HotelRequest request, decimal markup, string markupType)
        {
            //Loading All Hotels Static data and images city wise
            //optimized Code
            DataTable dtHotels = HotelStaticData.GetStaticHotelIds(request.CityCode, HotelBookingSource.OYO);
            DataTable dtImages = HotelImages.GetImagesByCityCode(request.CityCode, HotelBookingSource.OYO);

            XmlNode tempNode = null;
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/error/message");
            if (ErrorInfo != null)
            {
                Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                    " OYO:GenerateSearchResult,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                }), "");
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);

            }
            PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());

            #region Read Hotel(s) response
            XmlNodeList hotels = xmlDoc.SelectNodes("Hotels/Hotel");
            List<HotelSearchResult> hotelResults = new List<HotelSearchResult>();

            if (hotels.Count > 0)
            {
                foreach (XmlNode hotel in hotels)
                {
                    HotelSearchResult hotelResult = new HotelSearchResult();

                    #region Hotel Basic Details
                    //request hotelResult.PropertyType = searchId; //Multiple Search Time they Given Multiple Search Id's...This Id We Need To Send Booking tIme
                    hotelResult.BookingSource = HotelBookingSource.OYO;
                    hotelResult.CityCode = request.CityCode;
                    hotelResult.Currency = currency;
                    hotelResult.StartDate = request.StartDate;
                    hotelResult.EndDate = request.EndDate;

                    // roomStay.ChildNodes[4];//xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_HotelAvailRS/ns1:RoomStays/ns1:RoomStay/ns1:BasicPropertyInfo", nsmgr);
                    tempNode = hotel.SelectSingleNode("id");
                    if (tempNode != null)
                    {
                        hotelResult.HotelCode = tempNode.InnerText;
                    }
                    #endregion
                    #region search time avoid Staticinfo downloading  -- Hotel Details and Hotel Images                  

                    DataRow[] hotelStaticData = new DataRow[0];
                    try
                    {
                        hotelStaticData = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");
                        if (hotelStaticData != null && hotelStaticData.Length > 0)
                        {

                            hotelResult.HotelName = hotelStaticData[0]["hotelName"].ToString();
                            hotelResult.HotelDescription = hotelStaticData[0]["description"].ToString();
                            hotelResult.HotelAddress = hotelStaticData[0]["address"].ToString();
                            hotelResult.HotelMap = hotelStaticData[0]["hotelMaps"].ToString();
                            hotelResult.HotelLocation = hotelStaticData[0]["location"].ToString();
                            if (!string.IsNullOrEmpty(hotelStaticData[0]["hotelRating"].ToString()))
                            {
                                hotelResult.Rating = (HotelRating)Convert.ToInt32(Math.Floor(Convert.ToDecimal(hotelStaticData[0]["hotelRating"].ToString())));
                            }
                            //Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, " Hotel static OK", "");
                        }
                    }
                    //catch { continue; }
                    catch (Exception exp)
                    {
                        Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, " Hotel static" + exp.ToString(), "");
                    }
                    DataRow[] hotelImages = new DataRow[0];
                    try
                    {
                        hotelImages = dtImages.Select("hotelCode='" + hotelResult.HotelCode + "'");
                        string hImages = (hotelImages != null && hotelImages.Length > 0 ? hotelImages[0]["images"].ToString() : string.Empty);
                        hotelResult.HotelPicture = string.Empty;
                        if (!string.IsNullOrEmpty(hImages))
                        {
                            hotelResult.HotelPicture = hImages.Split('|')[0];
                        }
                        // Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, " Hotel picture OK", "");
                    }
                    //catch { continue; }
                    catch (Exception exp1)
                    {
                        Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, " Hotel image" + exp1.ToString(), "");
                    }

                    #endregion
                    #region To get only those satisfy the search conditions
                    if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                    {
                        if (!(hotelResult.HotelName != null && hotelResult.HotelName.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(hotelResult.HotelName.ToUpper()) : true))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    #endregion
                    #region Rooms Binding
                    /////
                    //Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, "Hotel Name : " + hotelResult.HotelName, "");

                    XmlNodeList roomNodeList = hotel.SelectNodes("rooms/room");
                    XmlNodeList restrictions = hotel.SelectNodes("restrictions");
                    hotelResult.RoomGuest = request.RoomGuest;
                    rateOfExchange = (exchangeRates.ContainsKey(hotelResult.Currency) ? exchangeRates[hotelResult.Currency] : 1);
                    int noOfDays = request.EndDate.Subtract(request.StartDate).Days;
                    if (roomNodeList != null)
                    {
                        HotelRoomsDetails[] rooms = new HotelRoomsDetails[roomNodeList.Count * request.NoOfRooms];
                        int index = 0;
                        foreach (XmlNode roomNode in roomNodeList)
                        {
                            for (int i = 0; i < Convert.ToInt32(request.NoOfRooms); i++)
                            {
                                HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                                #region Room Types
                                roomDetail.SequenceNo = (i + 1).ToString();
                                tempNode = roomNode.SelectSingleNode("description");
                                if (tempNode != null)
                                {
                                    roomDetail.RoomTypeName = tempNode.InnerText;
                                    roomDetail.RatePlanCode = "OYO-" + tempNode.InnerText;
                                }
                                roomDetail.RoomTypeCode = roomDetail.SequenceNo + "|" + roomDetail.RoomTypeName;
                                XmlNode currency = hotel.SelectSingleNode("currency_code");
                                if (currency != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + currency.InnerText;
                                }

                                #endregion

                                #region Promotions
                                //tempNode = roomNode.SelectSingleNode("");
                                //if (tempNode != null)
                                //{
                                //    XmlAttribute text = tempNode.Attributes["text"];
                                //    if (text != null && !string.IsNullOrEmpty(text.Value))
                                //    {
                                //roomDetail.PromoMessage = text.Value;
                                roomDetail.PromoMessage = string.Empty;
                                //    }
                                //}
                                #endregion

                                #region  Meal Plans       
                                string mealPlan = string.Empty;
                                tempNode = hotel.SelectSingleNode("mealplan");
                                if (tempNode != null)
                                {
                                    if (string.IsNullOrEmpty(mealPlan))
                                    {
                                        mealPlan = tempNode.InnerText;
                                    }

                                }
                                if (!string.IsNullOrEmpty(mealPlan))
                                {
                                    roomDetail.mealPlanDesc = mealPlan;
                                }
                                else
                                {
                                    roomDetail.mealPlanDesc = "Room Only";
                                }
                                #endregion

                                #region EssentialInformation

                                foreach (XmlNode restriction in restrictions)
                                {
                                    tempNode = restriction.SelectSingleNode("display_name");
                                    if (tempNode != null && tempNode.InnerText != "Max OYO Money allowed" && tempNode.InnerText != "specific_restrictions")
                                    {
                                        roomDetail.EssentialInformation += tempNode.InnerText + "|";
                                    }
                                    if (tempNode != null && tempNode.InnerText == "specific_restrictions")
                                    {
                                        XmlNodeList notes = hotel.SelectNodes("restrictions/notes");
                                        foreach (XmlNode note in notes)
                                        {
                                            // tempNode = note.SelectSingleNode("notes");
                                            if (note != null)
                                            {
                                                roomDetail.EssentialInformation += note.InnerText + "|";
                                            }
                                        }

                                    }
                                }
                                #endregion

                                #region Price Calucation

                                tempNode = roomNode.SelectSingleNode("rate_per_night");
                                if (tempNode != null)
                                {

                                    decimal totPrice = 0m;
                                    decimal inclusiveAmount = 0;

                                    inclusiveAmount = Convert.ToDecimal(tempNode.InnerText);


                                    totPrice = inclusiveAmount;

                                    //Booking Time Required
                                    roomDetail.TBOPrice = new PriceAccounts();
                                    roomDetail.TBOPrice.NetFare = inclusiveAmount;

                                    decimal hotelTotalPrice = 0m;
                                    decimal vatAmount = 0m;
                                    hotelTotalPrice = Math.Round((totPrice * noOfDays) * rateOfExchange, decimalPoint);
                                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                    {
                                        hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                    }
                                    hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                                    roomDetail.TotalPrice = hotelTotalPrice;
                                    roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                                    roomDetail.MarkupType = markupType;
                                    roomDetail.MarkupValue = markup;
                                    roomDetail.SellingFare = roomDetail.TotalPrice;
                                    roomDetail.supplierPrice = Math.Round((totPrice * noOfDays));
                                    roomDetail.TaxDetail = new PriceTaxDetails();
                                    roomDetail.TaxDetail = priceTaxDet;
                                    roomDetail.InputVATAmount = vatAmount;
                                    hotelResult.Price = new PriceAccounts();
                                    hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                    hotelResult.Price.SupplierPrice = Math.Round((totPrice * noOfDays));
                                    hotelResult.Price.RateOfExchange = rateOfExchange;
                                }
                                #region day wise price calucation
                                System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                decimal totalprice = roomDetail.TotalPrice;

                                for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                {
                                    decimal price = roomDetail.TotalPrice / diffResult.Days;
                                    if (fareIndex == diffResult.Days - 1)
                                    {
                                        price = totalprice;
                                    }
                                    totalprice -= price;
                                    hRoomRates[fareIndex].Amount = price;
                                    hRoomRates[fareIndex].BaseFare = price;
                                    hRoomRates[fareIndex].SellingFare = price;
                                    hRoomRates[fareIndex].Totalfare = price;
                                    hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                    hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                                }
                                roomDetail.Rates = hRoomRates;
                                #endregion

                                #endregion

                                #region Cancellation Policy
                                XmlNode cancelPolicy = hotel.SelectSingleNode("cancellation_policy");
                                int buffer = Convert.ToInt32(ConfigurationSystem.OYOConfig["buffer"]);
                                DateTime bufferedCancelDate = DateTime.MinValue;
                                string message = string.Empty;
                                string cancelDate = string.Empty;
                                if (cancelPolicy != null)
                                {
                                    if (string.IsNullOrEmpty(cancelDate))
                                    {
                                        try
                                        {
                                            //for finding Date (dd-MMM-yy format -- 13 Dec 2018 / 13 DEC 2018  )
                                            Regex rgx = new Regex(@"\d{2}\s*[A-Za-z]{3}\s*\d{4}");
                                            Match mat = rgx.Match(cancelPolicy.InnerText);
                                            if (string.IsNullOrEmpty(mat.ToString()))
                                            {
                                                rgx = new Regex(@"\d{1}\s*[A-Za-z]{3}\s*\d{4}");
                                                mat = rgx.Match(cancelPolicy.InnerText);
                                            }
                                            cancelDate = mat.ToString();
                                            if (!string.IsNullOrEmpty(cancelDate))
                                            {
                                                bufferedCancelDate = Convert.ToDateTime(cancelDate).AddDays(-buffer);
                                            }
                                        }
                                        catch { }
                                    }
                                    message += cancelPolicy.InnerText + ".";
                                }

                                roomDetail.CancellationPolicy = bufferedCancelDate != DateTime.MinValue ? message.Replace(cancelDate, " " + bufferedCancelDate.ToString("dd-MMM-yyyy")) : message;

                                #endregion

                                rooms[index] = roomDetail;
                                index++;
                            }
                        }
                        hotelResult.RoomDetails = rooms.OrderBy(o => o.supplierPrice).ToArray();
                    }
                    #endregion
                    hotelResult.Currency = agentCurrency;

                    for (int i = 0; i < request.NoOfRooms; i++)
                    {
                        for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                        {
                            if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                            {
                                hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                hotelResult.Price.AccPriceType = PriceType.NetFare;
                                hotelResult.Price.RateOfExchange = rateOfExchange;
                                hotelResult.Price.Currency = agentCurrency;
                                hotelResult.Price.CurrencyCode = agentCurrency;
                                break;
                            }
                        }
                    }
                    hotelResults.Add(hotelResult);
                }
            }
            return hotelResults.Where(h => !string.IsNullOrEmpty(h.HotelName)).ToArray();

            #endregion

        }

        /// <summary>
        /// GetRooms
        /// </summary>
        /// <param name="hotelResult">HotelResults object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This B2B Markup</param>
        /// <param name="markupType">this B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <remarks>Here Only we are getting real live inventory and price</remarks>
        public void GetRooms(ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType)
        {
            #region Request string
            // Prapare json format string request  
            string reqestrooms = GenerateHotelAvailabilityRequest(request, hotelResult.HotelCode);
            try
            {
                #region writing  of hotel details request into json file 

                // For writing of request data as json file    
                string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelDetailRequest.json");
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine(reqestrooms);
                writer.Close();
                #endregion
            }
            catch { }
            #endregion

            #region Send Reqest for Get Hotels
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                string resonse = string.Empty;
                resonse = SendRequest(reqestrooms, searchAvailability, "Hotel Details");

                if (!string.IsNullOrEmpty(resonse))
                {
                    //Sending request to OYO end point url for Hotel Detail Search
                    xmlResp = JsonConvert.DeserializeXmlNode(resonse);
                    #region writing  of hotel request into json file 

                    // For writing of request data as json file    
                    string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_HotelDetailResponse.json");
                    StreamWriter writer = new StreamWriter(filePath);
                    writer.WriteLine(resonse);
                    writer.Close();
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                        "Exception returned from OYO.GetRooms Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request json",
                        reqestrooms,
                        "|response XML",
                        xmlResp.OuterXml
                }), "");

                throw new BookingEngineException("Error: " + ex.Message);
            }
            #endregion
            #region Generate Serch Result
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    hotelResult = GenerateRoomsSearchResult(xmlResp, ref hotelResult, request, markup, markupType); //TODO
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                        "Exception returned from OYO.GetRooms Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request json",
                        reqestrooms,
                        "|response XML",
                        xmlResp.OuterXml
                }), "");

                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
            {
                    " Response from OYO for Hotel Rooms Search Request:| ",
                    DateTime.Now,
                    "| request XML:",
                    reqestrooms,
                    "|response XML:",
                    xmlResp.OuterXml
            }), "");
            // result = searchRes;
            #endregion
        }

        /// <summary>
        /// Reading XML response, assigning hotel data to the HotelSearchResult and Displaly Result in SearchResult Page.
        /// </summary>
        /// <param name="xmlDoc">xml Response Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here Only we are calucating B2B Markup and InputVat and also Genarting Hotel Results object</remarks>
        private HotelSearchResult GenerateRoomsSearchResult(XmlDocument xmlDoc, ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType)
        {
            XmlNode tempNode = null;
            #region Error response from API
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("error/message");
            if (ErrorInfo != null)
            {
                Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                {
                    " OYO:GenerateSearchResult,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                }), "");
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);


            }
            #endregion

            PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());

            #region Read Hotel Room(s) response
            XmlNodeList hotels = xmlDoc.SelectNodes("Hotels/Hotel");

            if (hotels.Count > 0)
            {
                foreach (XmlNode hotel in hotels)
                {
                    #region Rooms Binding
                    /////
                    // Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, "Hotel Name : " + hotelResult.HotelName, "");

                    XmlNodeList roomNodeList = hotel.SelectNodes("rooms/room");
                    XmlNodeList restrictions = hotel.SelectNodes("restrictions");
                    hotelResult.RoomGuest = request.RoomGuest;
                    XmlNode supplierCurrency = hotel.SelectSingleNode("currency_code");
                    rateOfExchange = (exchangeRates.ContainsKey(supplierCurrency.InnerText) ? exchangeRates[supplierCurrency.InnerText] : 1);
                    int noOfDays = request.EndDate.Subtract(request.StartDate).Days;
                    if (roomNodeList != null)
                    {
                        HotelRoomsDetails[] rooms = new HotelRoomsDetails[roomNodeList.Count * request.NoOfRooms];
                        int index = 0;
                        foreach (XmlNode roomNode in roomNodeList)
                        {
                            for (int i = 0; i < Convert.ToInt32(request.NoOfRooms); i++)
                            {
                                HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                                #region Room Types
                                roomDetail.SequenceNo = (i + 1).ToString();
                                tempNode = roomNode.SelectSingleNode("description");
                                if (tempNode != null)
                                {
                                    roomDetail.RoomTypeName = tempNode.InnerText;
                                    roomDetail.RatePlanCode = "OYO-" + tempNode.InnerText;
                                }
                                roomDetail.RoomTypeCode = roomDetail.SequenceNo + "|" + roomDetail.RoomTypeName;
                                XmlNode currency = hotel.SelectSingleNode("currency_code");
                                if (currency != null)
                                {
                                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + currency.InnerText;
                                }

                                #endregion

                                #region Promotions
                                //tempNode = roomNode.SelectSingleNode("");
                                //if (tempNode != null)
                                //{
                                //    XmlAttribute text = tempNode.Attributes["text"];
                                //    if (text != null && !string.IsNullOrEmpty(text.Value))
                                //    {
                                //roomDetail.PromoMessage = text.Value;
                                roomDetail.PromoMessage = string.Empty;
                                //    }
                                //}
                                #endregion

                                #region  Meal Plans       
                                string mealPlan = string.Empty;
                                tempNode = hotel.SelectSingleNode("mealplan");
                                if (tempNode != null)
                                {
                                    if (string.IsNullOrEmpty(mealPlan))
                                    {
                                        mealPlan = tempNode.InnerText;
                                    }

                                }
                                if (!string.IsNullOrEmpty(mealPlan))
                                {
                                    roomDetail.mealPlanDesc = mealPlan;
                                }
                                else
                                {
                                    roomDetail.mealPlanDesc = "Room Only";
                                }
                                #endregion

                                #region EssentialInformation

                                foreach (XmlNode restriction in restrictions)
                                {
                                    tempNode = restriction.SelectSingleNode("display_name");
                                    if (tempNode != null && tempNode.InnerText != "Max OYO Money allowed" && tempNode.InnerText != "specific_restrictions")
                                    {
                                        roomDetail.EssentialInformation += tempNode.InnerText + "|";
                                    }
                                    if (tempNode != null && tempNode.InnerText == "specific_restrictions")
                                    {
                                        XmlNodeList notes = hotel.SelectNodes("restrictions/notes");
                                        foreach (XmlNode note in notes)
                                        {
                                            // tempNode = note.SelectSingleNode("notes");
                                            if (note != null)
                                            {
                                                roomDetail.EssentialInformation += note.InnerText + "|";
                                            }
                                        }

                                    }
                                }
                                #endregion

                                #region Price Calucation

                                tempNode = roomNode.SelectSingleNode("rate_per_night");
                                if (tempNode != null)
                                {

                                    decimal totPrice = 0m;
                                    decimal inclusiveAmount = 0;

                                    inclusiveAmount = Convert.ToDecimal(tempNode.InnerText);


                                    totPrice = inclusiveAmount;

                                    //Booking Time Required
                                    roomDetail.TBOPrice = new PriceAccounts();
                                    roomDetail.TBOPrice.NetFare = inclusiveAmount;

                                    decimal hotelTotalPrice = 0m;
                                    decimal vatAmount = 0m;
                                    hotelTotalPrice = Math.Round((totPrice * noOfDays) * rateOfExchange, decimalPoint);
                                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                    {
                                        hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                    }
                                    hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                                    roomDetail.TotalPrice = hotelTotalPrice;
                                    roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                                    roomDetail.MarkupType = markupType;
                                    roomDetail.MarkupValue = markup;
                                    roomDetail.SellingFare = roomDetail.TotalPrice;
                                    roomDetail.supplierPrice = Math.Round((totPrice * noOfDays));
                                    roomDetail.TaxDetail = new PriceTaxDetails();
                                    roomDetail.TaxDetail = priceTaxDet;
                                    roomDetail.InputVATAmount = vatAmount;
                                    hotelResult.Price = new PriceAccounts();
                                    hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                    hotelResult.Price.SupplierPrice = Math.Round((totPrice * noOfDays));
                                    hotelResult.Price.RateOfExchange = rateOfExchange;
                                }
                                #region day wise price calucation
                                System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                                decimal totalprice = roomDetail.TotalPrice;

                                for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                                {
                                    decimal price = roomDetail.TotalPrice / diffResult.Days;
                                    if (fareIndex == diffResult.Days - 1)
                                    {
                                        price = totalprice;
                                    }
                                    totalprice -= price;
                                    hRoomRates[fareIndex].Amount = price;
                                    hRoomRates[fareIndex].BaseFare = price;
                                    hRoomRates[fareIndex].SellingFare = price;
                                    hRoomRates[fareIndex].Totalfare = price;
                                    hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                    hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                                }
                                roomDetail.Rates = hRoomRates;
                                #endregion

                                #endregion

                                #region Cancellation Policy
                                XmlNode cancelPolicy = hotel.SelectSingleNode("cancellation_policy");
                                int buffer = Convert.ToInt32(ConfigurationSystem.OYOConfig["buffer"]);
                                DateTime bufferedCancelDate = DateTime.MinValue;
                                string message = string.Empty;
                                string cancelDate = string.Empty;
                                if (cancelPolicy != null)
                                {
                                    if (string.IsNullOrEmpty(cancelDate))
                                    {
                                        try
                                        {
                                            //for finding Date (dd-MMM-yy format -- 13 Dec 2018 / 13 DEC 2018  )
                                            Regex rgx = new Regex(@"\d{2}\s*[A-Za-z]{3}\s*\d{4}");
                                            Match mat = rgx.Match(cancelPolicy.InnerText);
                                            if (string.IsNullOrEmpty(mat.ToString()))
                                            {
                                                rgx = new Regex(@"\d{1}\s*[A-Za-z]{3}\s*\d{4}");
                                                mat = rgx.Match(cancelPolicy.InnerText);
                                            }
                                            cancelDate = mat.ToString();
                                            if (!string.IsNullOrEmpty(cancelDate))
                                            {
                                                bufferedCancelDate = Convert.ToDateTime(cancelDate).AddDays(-buffer);
                                            }
                                        }
                                        catch { }
                                    }
                                    message += cancelPolicy.InnerText + ".";
                                }

                                roomDetail.CancellationPolicy = bufferedCancelDate != DateTime.MinValue ? message.Replace(cancelDate, " " + bufferedCancelDate.ToString("dd-MMM-yyyy")) : message;

                                #endregion

                                rooms[index] = roomDetail;
                                index++;
                            }
                        }
                        hotelResult.RoomDetails = rooms.OrderBy(o => o.supplierPrice).ToArray();
                    }
                    #endregion
                    hotelResult.Currency = agentCurrency;
                    for (int i = 0; i < request.NoOfRooms; i++)
                    {
                        for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                        {
                            if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                            {
                                hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                hotelResult.Price.AccPriceType = PriceType.NetFare;
                                hotelResult.Price.RateOfExchange = rateOfExchange;
                                hotelResult.Price.Currency = agentCurrency;
                                hotelResult.Price.CurrencyCode = agentCurrency;
                                break;
                            }
                        }
                    }
                }

            }
            return hotelResult;
            #endregion
        }
        #endregion

        #region Booking
        /// <summary>
        /// Booking Method
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(booking correspondent room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        /// <remarks>Here we are preparing Booking request object and sending book request to api </remarks>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            //Provisional Booking Req generation
            string request = GenerateProBookingRequest(itinerary);

            XmlDocument xmlResp = new XmlDocument();
            string response = string.Empty;
            BookingResponse bookingRes = new BookingResponse();
            try
            {
                //send Provisional Booking req 
                response = SendRequest(request, provisionalBooking, "Provisional Booking");

                if (!string.IsNullOrEmpty(response))
                {
                    // For writing of response data as json file   
                    #region writing  of Provisional Booking Response into json file                     
                    try
                    {
                        string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_ProvisionalBookingResponse.json");
                        StreamWriter writer = new StreamWriter(filePath);
                        writer.WriteLine(response);
                        writer.Close();
                    }
                    catch { }
                    #endregion
                    //converting json response in Xml response
                    xmlResp = JsonConvert.DeserializeXmlNode(response, "Root");
                    Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, string.Concat(new object[]
                               {
                    " Response from OYO for Provisional Booking Request:| ",
                    DateTime.Now,
                    "| request json:",
                    request,
                    "|response XML:",
                    xmlResp.OuterXml
                               }), "");
                }
                #region Final Booking 
                try
                {
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        string Invoice = string.Empty;
                        string ID = string.Empty;
                        string ExtrReferenceId = string.Empty;
                        decimal FinalAmount = 0;
                        //Provisional booking response( Here we get Provisional booking ID, Invoice, Final Amount of booking )
                        ReadProvisionalBookingResponse(xmlResp, ref Invoice, ref ID, ref ExtrReferenceId, ref FinalAmount);
                        // Checking for reprice before Provisional booking Confirmation
                        if (FinalAmount > (itinerary.Roomtype[0].Price.SupplierPrice * itinerary.NoOfRooms))
                        {
                            Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, "Exception returned from OYO.GetBooking Error Message:  Price has been changed.", "");
                            throw new BookingEngineException("Error: Price has been changed.");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Invoice) && !string.IsNullOrEmpty(ID))
                            {
                                //Generating Req for Final Booking 
                                request = GenerateProBookingConfirmRequest(itinerary, Invoice, ID, ExtrReferenceId); // Invoice - Provisional Booking Res Invoice, ID - Provisional Booking Res ID,

                                //Send Request for Final Booking 
                                provisionalBookingConfirm = provisionalBookingConfirm.Replace("{id}", ID); //Url
                                response = SendRequest(request, provisionalBookingConfirm, "Confirm Provisional Booking");
                                try
                                {
                                    //converting json response in to xml response
                                    xmlResp = JsonConvert.DeserializeXmlNode(response, "Root");
                                    if (!string.IsNullOrEmpty(response))
                                    {
                                        // For writing of response data as json file
                                        #region writing  of Provisional Booking Confirm Response into json file 
                                        try
                                        {
                                            string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_ProvisionalBookingConfirmResponse.json");
                                            StreamWriter writer = new StreamWriter(filePath);
                                            writer.WriteLine(response);
                                            writer.Close();
                                        }
                                        catch { }
                                        #endregion
                                    }

                                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                                    {
                                        Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, string.Concat(new object[]
                                            {
                                                    " Response from OYO for Confirm Provisional Booking Request:| ",DateTime.Now,
                                                    "| request json:",  request,  "|response XML:", xmlResp.OuterXml
                                            }), "");
                                        //Reading Final booking response (Here we get Confirmation No)
                                        itinerary.BookingRefNo = ID;// for  Supplier booking id  
                                        bookingRes = ReadBookingResponse(xmlResp, Invoice, itinerary);

                                        #region Generating Req for GSTIN (customer GST Invoice ) Details update                                         
                                        if (!string.IsNullOrEmpty(itinerary.ConfirmationNo)) //Supplier booking Invoice No
                                        {
                                            string messageText = string.Empty;// Mail text
                                            List<HotelRoom> roomsList = new List<HotelRoom>();
                                            roomsList.AddRange(itinerary.Roomtype);
                                            if (roomsList.Count > 0 && roomsList[0].PassenegerInfo.Count > 0 && roomsList[0].PassenegerInfo[0].IsGST)
                                            {
                                                try
                                                {
                                                    //Generating Req for Update GSTIN (Customer Comapany GST Details) Details 
                                                    request = GenerateUpdateGSTINBookingRequest(roomsList, ID);// Supplier booking id
                                                                                                               //Send Request for Update GSTIN (Customer Comapany GST Details) Details 
                                                    updateGSTINBooking = updateGSTINBooking.Replace("{bookingid}", ID); //Url for Update GSTIN
                                                    response = SendRequest(request, updateGSTINBooking, "Update GSTIN");
                                                    //converting json response in to xml response
                                                    xmlResp = JsonConvert.DeserializeXmlNode(response, "Root");
                                                    if (!string.IsNullOrEmpty(response))
                                                    {
                                                        // For writing of response data as json file
                                                        #region writing of response Update GSTIN(Customet Company GST) Details aginst Booking Confirm  
                                                        try
                                                        {
                                                            string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + ID + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_UpdateGSTINDetailsResponse.json");
                                                            StreamWriter writer = new StreamWriter(filePath);
                                                            writer.WriteLine(response);
                                                            writer.Close();
                                                        }
                                                        catch { }
                                                        #endregion
                                                    }
                                                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                                                    {
                                                        Audit.Add(EventType.OYOGSTIN, Severity.High, (int)appUserId,
                                                            string.Concat(new object[] { " Response from OYO for Update GSTIN aginst Booking  Request:| ",DateTime.Now,
                                                        "| request json:",  request,  "|response XML:", xmlResp.OuterXml  }),
                                                            "");
                                                        //Read Update GSTIN Status
                                                        response = ReadUpdateGSTINResponse(xmlResp, ID);
                                                        if (response.Split('|')[0] == "true")
                                                        {
                                                            Audit.Add(EventType.OYOGSTIN, Severity.High, (int)appUserId,
                                                             string.Concat(new object[] { " Response from OYO for Update GSTIN aginst Booking :  " + response, DateTime.Now, }), "");
                                                        }
                                                        else
                                                        {
                                                            response = response.Replace("false|", " ");
                                                            //if GSTIN Details Update fails Send a mail 
                                                            messageText = "Booking is created successfully, but failed to Update GSTN Details, due to following error "
                                                               + response.Replace("|", "\r\n").ToString()
                                                               + "\r\n\r\nThe following GSTN details are not updated for OYO Hotel Confirmation No: " + Invoice + " (Reference No:" + ID + ")"
                                                               + "\r\n Company Name : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyName
                                                               + "\r\n GSTN Number : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTNumber
                                                               + "\r\n Company e-mail : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyEmailId
                                                               + "\r\n Company Address : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyAddress
                                                               + "\r\n Company Contact Number : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTPhoneNumber;
                                                            try
                                                            {
                                                                Email.Send("Update GSTN failed(" + Invoice + ")", messageText);
                                                            }
                                                            catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                                                            Audit.Add(EventType.Exception, Severity.High, (int)appUserId, "Update GSTN failed. Error : " + response.Replace("|", "\r\n").ToString(), "0");

                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Audit.Add(EventType.OYOGSTIN, Severity.High, (int)appUserId, "Update GSTN failed.Error : " + ex.ToString(), "");
                                                    //if GSTIN Details Update fails Send a mail 
                                                    messageText = "Booking is created successfully, but failed to Update GSTN Details."
                                                                + "\r\n\r\nThe following GSTN details are not updated for OYO Hotel Confirmation No: " + Invoice + " (Reference No:" + ID + ")"
                                                                + "\r\n Company Name : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyName
                                                                + "\r\n GSTN Number : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTNumber
                                                                + "\r\n Company e-mail : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyEmailId
                                                                + "\r\n Company Address : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyAddress
                                                                + "\r\n Company Contact Number : " + roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTPhoneNumber;
                                                    try
                                                    {
                                                        // Email. Send(ConfigurationSystem.Email["fromEmail"], roomsList[0].PassenegerInfo[0].Email, "Update GSTN failed(" + ID + ")", mailBody);
                                                        Email.Send("Update GSTN failed(" + Invoice + ")", messageText);
                                                    }
                                                    catch (System.Net.Mail.SmtpException) { } // Do nothing       

                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new BookingEngineException("Error: " + ex.Message);
                                }
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, "Exception returned from OYO.GetBooking Error Message:" + ex.ToString() + " | " + DateTime.Now + "| provisional booking response XML" + request + "|provisional booking response XML" + xmlResp.OuterXml, "");
                    throw new BookingEngineException("Error: " + ex.ToString());
                }

                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, "Exception returned from OYO.GetBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| provisional booking request XML" + request + "|provisional booking response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }

            return bookingRes;
        }

        /// <summary>
        /// GenerateBookingRequest(Provisional)
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>json string</returns>
        /// <remarks>First need to genarate json string object</remarks>
        private string GenerateProBookingRequest(HotelItinerary itinerary)
        {
            string bookingRequest = string.Empty;
            HTLBookingDetails bookingDetails = new HTLBookingDetails();
            Guest guest = new Guest();
            Booking booking = new Booking();
            List<HotelRoom> roomsList = new List<HotelRoom>();
            try
            {
                roomsList.AddRange(itinerary.Roomtype);
                List<int> rCombination = GetRoomCombinations(roomsList);


                #region Guest Details 
                guest.first_name = roomsList[0].PassenegerInfo[0].Firstname;
                guest.last_name = roomsList[0].PassenegerInfo[0].Lastname;
                guest.phone = roomsList[0].PassenegerInfo[0].Phoneno;
                guest.country_code = roomsList[0].PassenegerInfo[0].Countrycode;
                guest.email = roomsList[0].PassenegerInfo[0].Email;
                bookingDetails.guest = guest;
                #endregion
                #region Booking Details
                booking.single = rCombination[0];
                booking.Double = rCombination[1];
                booking.extra = rCombination[2];
                booking.checkin = itinerary.StartDate.ToString("dd/MM/yyyy");
                booking.checkout = itinerary.EndDate.ToString("dd/MM/yyyy");
                booking.hotel_id = itinerary.HotelCode;
                booking.external_reference_id = "HTL" + DateTime.Now.ToString("yyyyMMddhhmmss");
                booking.is_provisional = "true";
                bookingDetails.booking = booking;
                #endregion

            }
            catch (Exception ex)
            {
                throw new Exception("OYO: Failed generate  Provisinal Booking request for Hotel : " + itinerary.HotelName, ex);
            }
            bookingRequest = JsonConvert.SerializeObject(bookingDetails);
            bookingRequest = bookingRequest.Replace("Double", "double");

            // For writing of request data as json file  
            #region writing  of hotel request into json file              
            try
            {
                string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_ProvisionalBookingRequest.json");
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine(bookingRequest);
                writer.Close();
            }
            catch { }

            #endregion

            return bookingRequest;
        }

        /// <summary>
        /// GenerateProBookingConfirmRequest(Provisional booking confirmation)
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>json string</returns>
        /// <remarks>First need to genarate json string object</remarks>
        private string GenerateProBookingConfirmRequest(HotelItinerary itinerary, string invoice, string id, string extrRefrenceId)
        {
            string bookingRequest = string.Empty;
            ProvisionalBooking bookingDetails = new ProvisionalBooking();
            Payments payments = new Payments();
            ProBooking booking = new ProBooking();

            try
            {

                #region Payment Details 
                payments.bill_to_affiliate = "true";
                bookingDetails.payments = payments;
                #endregion
                #region Booking Details

                booking.external_reference_id = extrRefrenceId;
                booking.status = "Confirm Booking";
                bookingDetails.booking = booking;
                #endregion

            }
            catch (Exception ex)
            {
                throw new Exception("OYO: Failed generate  Provisinal Booking Comfirmation request for Hotel : " + itinerary.HotelName, ex);
            }
            bookingRequest = JsonConvert.SerializeObject(bookingDetails);

            // For writing of request data as json file    
            #region writing  of Provisional Booking Confirm Request into json file  
            try
            {
                string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_ProvisionalBookingConfirmRequest.json");
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine(bookingRequest);
                writer.Close();
            }
            catch { }
            #endregion

            return bookingRequest;
        }

        /// <summary>
        ///  ReadProvisionalBookingResponse
        /// </summary>
        /// <param name="xmlDoc">ProvisionalBooking xml Response Object </param>
        /// <param name="Invoice">Provisional booking type Provided by supplier </param>
        /// <param name="ID">Provisional booking ID Provided by supplier</param>
        /// <param name="ExtrRefrenceId">Our booking reference id</param>
        /// <returns>string</returns>
        private void ReadProvisionalBookingResponse(XmlDocument xmlDoc, ref string Invoice, ref string ID, ref string ExtrRefrenceId, ref decimal FinalAmount)
        {
            try
            {
                XmlNode tempNode = null;
                #region Error response from API
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Root/error/message");
                if (ErrorInfo != null)
                {
                    Audit.Add(EventType.OYOBooking, Severity.High, (int)appUserId, string.Concat(new object[]
                      {
                    " OYO:ReadResponseProvisionalBooking,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                      }), "");
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);

                }
                #endregion
                tempNode = xmlDoc.SelectSingleNode("Root/status");
                if (tempNode != null)
                {

                    tempNode = xmlDoc.SelectSingleNode("Root/invoice");

                    if (tempNode != null)
                    {
                        Invoice = tempNode.InnerText;
                    }
                    tempNode = xmlDoc.SelectSingleNode("Root/id");
                    if (tempNode != null)
                    {
                        ID = tempNode.InnerText;
                    }
                    tempNode = xmlDoc.SelectSingleNode("Root/external_reference_id");
                    if (tempNode != null)
                    {
                        ExtrRefrenceId = tempNode.InnerText;
                    }
                    tempNode = xmlDoc.SelectSingleNode("Root/final_amount");
                    if (tempNode != null)
                    {
                        FinalAmount = Convert.ToDecimal(tempNode.InnerText);

                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception("OYO: Failed to Read provisional booking response ", ex);
            }
        }

        /// <summary>
        /// ReadBookingResponse
        /// </summary>
        /// <param name="xmlDoc">Booking response xml</param>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>Here Reading booking response object
        /// here only we need to get confirmationNo and BookingId
        /// </remarks>
        private BookingResponse ReadBookingResponse(XmlDocument xmlDoc, string invoiceNo, HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                #region Error response from API
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Root/error/message");
                if (ErrorInfo != null)
                {
                    Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)appUserId, string.Concat(new object[]
                          {
                    " OYO:ReadBookingResponse,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                          }), "");
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);

                }
                #endregion

                XmlNode finalRes = xmlDoc.SelectSingleNode("Root/status");

                if (finalRes != null && finalRes.InnerText == "Confirm Booking")
                {
                    if (invoiceNo != null)
                    {
                        itinerary.ConfirmationNo = invoiceNo;
                    }

                    bookResponse.ConfirmationNo = itinerary.ConfirmationNo;
                    bookResponse.Status = BookingResponseStatus.Successful;
                    itinerary.Status = HotelBookingStatus.Confirmed;
                    itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: OYO as per final booking form Confirmation No: " + itinerary.ConfirmationNo;
                }
                else
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                }


            }
            catch (Exception ex)
            {
                throw new Exception("OYO: Failed to Read booking response " + ex, ex);
            }
            return bookResponse;
        }
        #endregion

        #region Update GSTIN Invoice Details
        /// <summary>
        /// Update GSTIN (Customer Company GST Details) Invoice Details against Booking Confirmation Id
        /// </summary>
        /// <param name="roomsList">Hotel Room object(corresponding booking passanger gstin details)</param>
        ///  <param name="ConfirmationNo">Hotel Booking Confirmation No</param>
        /// <returns>json object</returns>
        /// <remarks>Genarating json  object</remarks>
        private string GenerateUpdateGSTINBookingRequest(List<HotelRoom> roomsList, string ID)
        {
            string updateGSTINRequest = string.Empty;
            UpdateGSTIN updateGSTINDetails = new UpdateGSTIN();
            try
            {
                #region GSTIN Details 
                updateGSTINDetails.name = roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyName;
                updateGSTINDetails.gstin = roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTNumber;
                updateGSTINDetails.email = roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyEmailId;
                updateGSTINDetails.address = roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTCompanyAddress;
                updateGSTINDetails.contact = roomsList[0].PassenegerInfo[0].HotelPassengerGST.GSTPhoneNumber;
                #endregion
                updateGSTINRequest = JsonConvert.SerializeObject(updateGSTINDetails);

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOGSTIN, Severity.High, (int)appUserId, "OYO: Failed generate request for Update GSTIN Details against Hotel Booking ID  : " + ID + " Error Message:" + ex.ToString() + " | " + DateTime.Now, "");

            }
            // For writing of request data as json file    
            #region writing  of Provisional Booking Confirm Request into json file  
            try
            {
                string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + ID + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_UpdateGSTINDetailsRequest.json");
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine(updateGSTINRequest);
                writer.Close();
            }
            catch { }
            #endregion

            return updateGSTINRequest;
        }
        /// <summary>
        ///Reading response of Update GSTIN (Customer Company GST Details) Invoice Details against Booking Confirmation Id
        /// </summary>
        /// <param name="roomsList">Hotel Room object(corresponding booking passanger gstin details)</param>
        ///  <param name="ConfirmationNo">Hotel Booking Confirmation No</param>
        /// <returns>json object</returns>
        /// <remarks>Genarating json  object</remarks>
        private string ReadUpdateGSTINResponse(XmlDocument xmlDoc, string ID)
        {
            string updateGSTINResponse = string.Empty;

            try
            {
                XmlNode tempNode = null;
                #region Error response from API
                XmlNodeList ErrorInfo = xmlDoc.SelectNodes("Root/errors");
                if (ErrorInfo.Count > 0)
                {
                    tempNode = xmlDoc.SelectSingleNode("Root/success");
                    if (tempNode != null)
                    {
                        updateGSTINResponse = tempNode.InnerText + "|";
                    }
                    foreach (XmlNode errNode in ErrorInfo)
                    {
                        tempNode = errNode.SelectSingleNode("errorMessage");
                        if (tempNode != null)
                        {
                            updateGSTINResponse += tempNode.InnerText + "|";
                        }
                    }
                    Audit.Add(EventType.OYOGSTIN, Severity.High, (int)appUserId, string.Concat(new object[]
                          {
                    " OYO:ReadUpdateGSTINResponse,Error Message:",
                   updateGSTINResponse.TrimEnd('|'),
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                          }), "");
                }
                #endregion
                #region Update Successful
                else
                {
                    tempNode = xmlDoc.SelectSingleNode("Root/success");
                    if (tempNode != null)
                    {
                        updateGSTINResponse += tempNode.InnerText + "|";
                    }
                    tempNode = xmlDoc.SelectSingleNode("Root/message");
                    if (tempNode != null)
                    {
                        updateGSTINResponse += tempNode.InnerText;
                    }

                }
                #endregion
            }
            catch (Exception ex)
            {
            }
            return updateGSTINResponse.TrimEnd('|');
        }
        #endregion

        #region Offline Update GSTIN Invoice Details
        /// <summary>
        ///Offline Update GSTIN (Customer Company GST Details) Invoice Details against OYO BookingId (Booking Reference)
        /// </summary>
        /// <param name="updateGSTINDetails">Lead  passanger gstin details</param>
        ///  <param name="oyoReference">OYO BookingId</param>
        /// <returns> </returns>
        /// <remarks> </remarks>
        public void OfflineUpadateGSTIN(UpdateGSTIN updateGSTINDetails, string oyoReference)
        {
            XmlDocument xmlResp = new XmlDocument();
            string request = string.Empty;
            string response = string.Empty;
            try
            {
                request = JsonConvert.SerializeObject(updateGSTINDetails);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOGSTIN, Severity.High, (int)appUserId, "OYO: Failed generate request for Offline Update GSTIN Details against OYO Booking Reference  : " + oyoReference + " Error Message:" + ex.ToString() + " | " + DateTime.Now, "");
            }
            // For writing of request data as json file    
            #region writing  of Request into json file  
            try
            {
                string filePath = Path.Combine(XmlPath, oyoReference + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_OfflineUpdateGSTINDetailsRequest.json");
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine(request);
                writer.Close();
            }
            catch { }
            #endregion

            updateGSTINBooking = updateGSTINBooking.Replace("{bookingid}", oyoReference); //Url for Update GSTIN
            //Sending Request
            response = SendRequest(request, updateGSTINBooking, "Update GSTIN");
            #region writing of response Update GSTIN(Customet Company GST) Details aginst OYO Booking ID(Booking Reference)  
            if (!string.IsNullOrEmpty(response))
            {
                // For writing of response data as json file
                try
                {
                    string filePath = Path.Combine(XmlPath, oyoReference + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_OfflineUpdateGSTINDetailsResponse.json");
                    StreamWriter writer = new StreamWriter(filePath);
                    writer.WriteLine(response);
                    writer.Close();
                }
                catch { }
            }
            #endregion

            //converting json response in to xml response
            xmlResp = JsonConvert.DeserializeXmlNode(response, "Root");
            if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
            {
                Audit.Add(EventType.OYOGSTIN, Severity.High, 1,
                    string.Concat(new object[] { " Response from OYO for Offline Update GSTIN aginst BookingId :| ",DateTime.Now,
                                                        "| request json:",  request,  "|response XML:", xmlResp.OuterXml  }),
                    "");
                //Read Update GSTIN Status
                response = ReadUpdateGSTINResponse(xmlResp, oyoReference);
                if (response.Split('|')[0] == "true")
                {
                    Audit.Add(EventType.OYOGSTIN, Severity.High, 1,
                     string.Concat(new object[] { " Response from OYO for Offline Update GSTIN aginst BookingID :  " + response, DateTime.Now, }), "");
                }
                else
                {
                    response = response.Replace("false|", " ");
                    //if GSTIN Details Update fails Send a mail 
                    string messageText = "GSTN Details for Booking Reference No: " + oyoReference + "\r\n Company Name is " + updateGSTINDetails.name + "\r\n GSTN Number is " + updateGSTINDetails.gstin
                        + "\r\n Company e-mail is " + updateGSTINDetails.email + "\r\n Company Address is " + updateGSTINDetails.address + "\r\n Company Contact Number is " + updateGSTINDetails.contact
                        + "\r\n\r\nFailed to Offline Update GSTN Details, due to following error "
                        + response.Replace("|", "\r\n").ToString();
                    string mailBody = "Dear Executive,\n\n" + messageText + "\n\nThis is automated mail alert generated by Cozmo system.\n\n---\nCOZMO System";
                    try
                    {
                        Email.Send("Offline Update GSTN failed(" + oyoReference + ")", messageText);
                    }
                    catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 1, "Offling Update GSTN failed. Error : " + response.Replace("|", "\r\n").ToString(), "0");
                }
            }
        }
        #endregion

        #region cancel Booking
        /// <summary>
        /// CancelHotelBooking
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Preparing CancelHotelBooking request object and sending CancelHotelBooking to api</remarks>
        public Dictionary<string, string> CancelHotelBooking(HotelItinerary itinerary)
        {
            Dictionary<string, string> cancelRes = new Dictionary<string, string>();
            //Initiate Cancellation charges Req 
            #region writing  of hotel Cancellation charges request into json file  
            // For writing of request data as json file    
            string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CancellationChargesRequest.json");

            try
            {
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine("Get Cancellation charges request for Confirmation number : " + itinerary.ConfirmationNo);
                writer.Close();
            }
            catch { }
            #endregion
            string response = string.Empty;
            XmlDocument xmlRes = new XmlDocument();
            try
            {
                //Get  Cancellation Charges Response
                cancellationCharge = cancellationCharge.Replace("{id}", itinerary.ConfirmationNo);
                response = SendGetRequest(cancellationCharge);
                #region writing  of hotel Cancellation charges response into json file  
                // For writing of response data as json file    
                filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CancellationChargesResponse.json");
                try
                {
                    StreamWriter writer = new StreamWriter(filePath);
                    writer.WriteLine("Get Cancellation charges for Confirmation number : " + itinerary.ConfirmationNo + "  : " + response);
                    writer.Close();
                }
                catch { }
                #endregion
                xmlRes = JsonConvert.DeserializeXmlNode(response, "Root");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOCancel, Severity.High, (int)appUserId, "Exception returned from OYO.CancelHotelBooking Get Cancellation charges. Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + "Get Cancellation charges request for Confirmation number : " + itinerary.ConfirmationNo + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlRes != null && xmlRes.ChildNodes != null && xmlRes.ChildNodes.Count > 0)
                {
                    cancelRes = ReadCancelChargesResponse(itinerary, xmlRes);
                    Audit.Add(EventType.OYOCancel, Severity.High, (int)appUserId, string.Concat(new object[]
                    {
                        " Response from OYO for  Cancellation charge of Booking Request:| ",DateTime.Now,
                        "| request for Confirmation number :",  itinerary.ConfirmationNo,  "|response XML:", xmlRes.OuterXml
                    }), "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOCancel, Severity.High, (int)appUserId, "Exception returned from OYO.ReadCancelChargesResponse Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + xmlRes + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancelRes;
        }

        /// <summary>
        /// ReadCancelChargesResponse
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <param name="xmlDoc">PreCanceResponse xml</param>
        /// <returns>Dictionary</returns>
        /// <remarks> call cancel status method</remarks>
        private Dictionary<string, string> ReadCancelChargesResponse(HotelItinerary itinerary, XmlDocument xmlDoc)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            XmlNode tempNode;
            #region Error response from API
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Root/error/message");
            if (ErrorInfo != null)
            {
                Audit.Add(EventType.OYOCancel, Severity.High, (int)appUserId, string.Concat(new object[]
                   {
                    "Error Returned from OYO. Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                   }), "");
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);

            }
            #endregion



            //Applicable Cancellation Charges Node
            tempNode = xmlDoc.SelectSingleNode("Root/applicable_cancellation_charge");

            string bookingRefNo = string.Empty;

            string cancelToken = string.Empty;
            decimal cancelCost = 0;
            string currencyCode = string.Empty;
            if (tempNode != null)// Here we get the Cancellation charges 
            {
                bookingRefNo = itinerary.ConfirmationNo;

                cancelCost = Convert.ToDecimal(tempNode.InnerText);

                currencyCode = currency;

                if (!string.IsNullOrEmpty(bookingRefNo) && cancelCost >= 0)
                {
                    //Here Calling CancelStatus and get cancellation number
                    cancelToken = GetCancellationNumber(itinerary);
                    if (!string.IsNullOrEmpty(cancelToken))
                    {
                        cancellationCharges.Add("Currency", currencyCode);
                        cancellationCharges.Add("Amount", Convert.ToString(cancelCost));
                        cancellationCharges.Add("Status", "Cancelled");
                        cancellationCharges.Add("ID", cancelToken);
                    }
                    else
                    {
                        cancellationCharges.Add("Status", "Failed");
                    }
                }
                else
                {
                    cancellationCharges.Add("Status", "Failed");
                }
            }

            return cancellationCharges;

        }

        /// <summary>
        /// GetCancellationNumber
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param> 
        /// <returns>string (cancellation number)</returns>
        /// <remarks>Here we are preparing CancelStatus request object and sending CancelStatus to api</remarks>
        private string GetCancellationNumber(HotelItinerary itinerary)
        {
            string cancelToken = string.Empty;
            //Confirm Cancel Req Generation
            string request = GenerateCancelRequest(itinerary);
            XmlDocument xmlRes = new XmlDocument();
            string response = string.Empty;
            try
            {
                //Get Confirm Cancel Response
                bookingCancellation = bookingCancellation.Replace("{id}", itinerary.ConfirmationNo);
                response = SendRequest(request, bookingCancellation, "Confirm Cancellation");
                #region writing  of hotel Cancellation charges response into json file  
                // For writing of response data as json file    
                string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CancellationConfirmResponse.json");
                try
                {
                    StreamWriter writer = new StreamWriter(filePath);
                    writer.WriteLine(response);
                    writer.Close();
                }
                catch { }
                #endregion
                xmlRes = JsonConvert.DeserializeXmlNode(response, "Root");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOCancel, Severity.High, (int)appUserId, "Exception returned from OYO.CancelHotelBooking Confirm Cancellation Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlRes != null && xmlRes.ChildNodes != null && xmlRes.ChildNodes.Count > 0)
                {
                    cancelToken = ReadCancelResponse(itinerary, xmlRes);
                    Audit.Add(EventType.OYOCancel, Severity.High, (int)appUserId, string.Concat(new object[]
                   {
                        " Response from OYO for Confirm Cancellation of Booking Request:| ",DateTime.Now,
                        "| request json:",  request,  "|response XML:", xmlRes.OuterXml
                   }), "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOCancel, Severity.High, 0, "Exception returned from OYO.GetCancelStatus Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlRes.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancelToken;
        }

        /// <summary>
        /// GenerateCanceRequest
        /// </summary>
        /// <param name="itinerary">HotelItinerary object</param> 
        /// <returns>string</returns>
        /// <remarks>First need to genarate json string object</remarks>
        private string GenerateCancelRequest(HotelItinerary itinerary)
        {
            string request = string.Empty;
            ConfirmCancellation confirmCancellation = new ConfirmCancellation();
            Cancellation booking = new Cancellation();
            try
            {
                #region Confirm Cancellation 
                booking.status = "Cancelled Booking";
                confirmCancellation.booking = booking;
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception("OYO: Failed generate  Confirm Booking Cancellation  request for Hotel : " + itinerary.HotelName, ex);
            }
            request = JsonConvert.SerializeObject(confirmCancellation);

            try
            {
                #region writing  of hotel Cancellation confirm request into json file  
                // For writing of request data as json file    
                string filePath = Path.Combine(XmlPath, appUserId + "_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CancellationConfirmRequest.json");
                try
                {
                    StreamWriter writer = new StreamWriter(filePath);
                    writer.WriteLine(request);
                    writer.Close();
                }
                catch { }
                #endregion
            }
            catch { }

            return request;
        }

        /// <summary>
        /// ReadCancelResponse
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <param name="xmlDoc">cancelReponse xml</param>
        /// <returns>string (Cancellation Number)</returns>
        /// <remarks>Here checking status if status is true booking cancelled else booking not cancelled</remarks>
        private string ReadCancelResponse(HotelItinerary itinerary, XmlDocument xmlDoc)
        {
            string cancellationNumber = string.Empty;

            XmlNode tempNode = xmlDoc.SelectSingleNode("Root/status");


            if (tempNode != null && tempNode.InnerText == "Cancelled Booking") //Here Status 200 Means Response Success
            {
                tempNode = xmlDoc.SelectSingleNode("Root/invoice");
                if (tempNode != null)
                {
                    cancellationNumber = tempNode.InnerText;
                }
            }
            else
            {
                #region Error response from API
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Root/error/message");
                if (ErrorInfo != null)
                {
                    Audit.Add(EventType.OYOCancel, Severity.High, (int)appUserId, string.Concat(new object[]
                        {
                    "Error Returned from OYO. Error Message:",
                    ErrorInfo.InnerText,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                        }), "");
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);

                }
                #endregion
            }
            return cancellationNumber;
        }
        #endregion

        #region Get Hotel Static Data
        /// <summary>
        /// GetHotelDetailsInformation
        /// </summary>
        /// <param name="cityCode">HotelCityCode</param>
        /// <param name="hotelName">HotelName</param>
        /// <param name="hotelCode">HotelCode</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Every 15days (configuraed in App.config -- OYO.config.xml) Need to download StaticData
        /// here we are checking if static data is not there our DB we are sending static data request and get the response save our database
        /// </remarks>
        public HotelDetails GetHotelDetailsInformation(string cityCode, string hotelName, string hotelCode)
        {
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData staticInfo = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();

            //get the info whether to update the static data
            int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.OYOConfig["TimeStamp"]);
            bool imageUpdate = true;
            bool isUpdateReq = false;
            bool isReqSend = false;

            string request = string.Empty;
            string resp = string.Empty;

            try
            {
                staticInfo.Load(hotelCode, cityCode, HotelBookingSource.OYO);
                imageInfo.Load(hotelCode, cityCode, HotelBookingSource.OYO);
                if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                {
                    imageUpdate = false;
                }
                if (staticInfo.HotelName != null && staticInfo.HotelName.Length > 0 && !string.IsNullOrEmpty(staticInfo.HotelDescription) && !string.IsNullOrEmpty(staticInfo.HotelAddress))
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(staticInfo.TimeStamp);
                    //if it is less than REquired time stamp days then load from DB
                    //TO DO 
                    //if (diffRes.Days > timeStampDays)
                    //{
                    //    // Set the variable as true
                    //    isUpdateReq = true;
                    //    imageUpdate = true;
                    //}
                    //else
                    {
                        hotelInfo.HotelCode = staticInfo.HotelCode;
                        hotelInfo.HotelName = staticInfo.HotelName;
                        hotelInfo.Map = staticInfo.HotelMap;
                        hotelInfo.Description = staticInfo.HotelDescription;
                        hotelInfo.HotelPolicy = staticInfo.HotelPolicy;

                        string[] image = new string[0];
                        List<string> dldImgList = new List<string>();
                        {
                            string[] dldImages = imageInfo.Images.Split('|');
                            foreach (string img in dldImages)
                            {
                                if (!string.IsNullOrEmpty(img)) dldImgList.Add(img);
                            }
                        }
                        hotelInfo.Images = dldImgList;
                        //hotelInfo.Image = dldImgList.Count > 0 ? dldImgList[0] : "";
                        hotelInfo.Address = staticInfo.HotelAddress;
                        hotelInfo.PhoneNumber = staticInfo.PhoneNumber;
                        hotelInfo.PinCode = staticInfo.PinCode;
                        hotelInfo.Email = staticInfo.EMail;
                        hotelInfo.URL = staticInfo.URL;
                        hotelInfo.FaxNumber = staticInfo.FaxNumber;
                        Dictionary<string, string> attractions = new Dictionary<string, string>();
                        hotelInfo.Attractions = attractions;
                        List<string> facilities = new List<string>();
                        List<string> roomFacilities = new List<string>();
                        if (!string.IsNullOrEmpty(staticInfo.HotelFacilities))
                        {
                            string[] hotelFacility = staticInfo.HotelFacilities.Split('$');

                            string[] facilList = hotelFacility[0].Split('|');
                            foreach (string facl in facilList)
                            {
                                facilities.Add(facl);
                            }
                            if (hotelFacility.Length > 1)
                            {
                                string[] roomFacilList = hotelFacility[1].Split('|');
                                foreach (string roomFacl in roomFacilList)
                                {
                                    roomFacilities.Add(roomFacl);
                                }
                            }
                        }
                        hotelInfo.HotelFacilities = facilities;
                        hotelInfo.RoomFacilities = roomFacilities;
                        List<string> locations = new List<string>();
                        string[] locationList = staticInfo.HotelLocation.Split('|');
                        foreach (string loc in locationList)
                        {
                            locations.Add(loc);
                        }
                        hotelInfo.Location = locations;
                    }
                }
                else
                {
                    isReqSend = true;
                }
                if (isReqSend || isUpdateReq)
                {
                    // Saving in Cache
                    if (hotelInfo.HotelName == null)
                    {
                        return hotelInfo;
                    }
                    else
                    {
                        staticInfo.HotelCode = hotelCode;
                        staticInfo.HotelName = hotelInfo.HotelName;
                        staticInfo.HotelAddress = hotelInfo.Address;
                        staticInfo.CityCode = cityCode;
                        List<string> facilities = hotelInfo.HotelFacilities;
                        string facilty = string.Empty;
                        foreach (string facl in facilities)
                        {
                            facilty += facl + "|";
                        }
                        List<string> roomFacilities = hotelInfo.RoomFacilities;
                        string roomFacilty = string.Empty;
                        foreach (string roomFacl in roomFacilities)
                        {
                            roomFacilty += roomFacl + "|";
                        }
                        staticInfo.HotelFacilities = facilty + "$" + roomFacilty;
                        staticInfo.SpecialAttraction = string.Empty;

                        List<string> locations2 = hotelInfo.Location;
                        string location = string.Empty;
                        foreach (string loc in locations2)
                        {
                            location = location + loc + "|";
                        }
                        staticInfo.HotelLocation = location;
                        staticInfo.HotelMap = hotelInfo.Map;
                        staticInfo.HotelDescription = hotelInfo.Description;
                        staticInfo.FaxNumber = hotelInfo.FaxNumber;
                        staticInfo.EMail = hotelInfo.Email;
                        staticInfo.PinCode = hotelInfo.PinCode.ToString();
                        staticInfo.PhoneNumber = hotelInfo.PhoneNumber;
                        staticInfo.URL = hotelInfo.URL;
                        staticInfo.Source = HotelBookingSource.OYO;
                        staticInfo.Rating = hotelInfo.hotelRating;
                        staticInfo.HotelPolicy = hotelInfo.HotelPolicy;
                        staticInfo.Status = true;
                        staticInfo.Save();
                        //images
                        List<string> imgList = hotelInfo.Images;
                        string images = string.Empty;
                        foreach (string img in imgList)
                        {
                            images += img + "|";
                        }
                        imageInfo.Images = images;
                        imageInfo.CityCode = cityCode;
                        imageInfo.HotelCode = hotelCode;
                        imageInfo.Source = HotelBookingSource.OYO;
                        imageInfo.DownloadedImgs = images;

                        if (isUpdateReq)
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Update();
                            }
                        }
                        else
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Save();
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOStatic, Severity.High, (int)appUserId, "Exception returned from Agoda.HotelStatic Error Message:" + ex.Message + " | " + DateTime.Now, "");
            }
            return hotelInfo;
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }
        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~OYO()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }
        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
