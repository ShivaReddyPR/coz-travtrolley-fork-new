﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;


//using Technology.BookingEngine;
using System.Collections.Generic;

/// <summary>
/// Summary description for package
/// </summary>
/// 
namespace CT.HolidayDeals
{
    public class Package
    {
        private const long NEW_RECORD = -1;

        #region Members
        private long _id;
        long _packageId;
        string _packageName;
        int _stockInHand;
        int _packageDays;
        decimal _startingFrom;
        string _cityCode;
        string _countryCode;
        string _city;
        string _country;
        string _region = string.Empty;
        string _image1;
        string _image2;
        string _image3;
        string _theme;
        string _introduction;
        string _overview;
        DateTime _startFrom;

        DateTime _endTo;
        string _packageDuration;
        DateTime _availableDateStart;
        DateTime _availableDateEnd;
        string _unvailableDates;
        string _itinerary1;
        string _itinerary2;
        string _details;
        string _supplierName;
        string _supplierEmail;
        string _mealsIncluded;
        string _transferInclude;
        string _pickLocation;
        DateTime _pickUpDateTime;
        string _dropOffLocation;
        DateTime _dropOffDateTime;
        string _exclusion;
        string _inclusion;
        string _cancellationPolicy;
        string _unAvailableDays;
        string _thingsToBring;
        int _bookingCutOff;

        int _flexOrder;
        string _flexLabel;
        string _flexControl;
        string _flexSqlQuery;
        string _flexDataType;
        string _flexMandatoryStatus;
        //string _FlexStatus;
        DataTable _dtFlexDetails;
        DataTable _dtPriceDetails;
        string _image1Extn;
        string _image2Extn;
        string _image3Extn;
        int _createdBy;
        string _isFixedDeparture;
        DataTable _dtItineraryDetails;
        int _agencyId;
        string _fdInclusions;
        string _hasAir;
        string _hasHotels;
        string _hasTours;
        string _hasTransfers;
        string _hasMeals;
        DataTable _dtSuppliers;
        string _originCity;
        string _originCountry;
        string _currency;
        bool _isInternational;
        int _paging;
        string _packageStatus;
        string _culture;
        #endregion

        #region Properties
        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public long packageId
        {
            get { return _packageId; }
            set { _packageId = value; }

        }
        public string packageName
        {
            get { return _packageName; }
            set { _packageName = value; }
        }

        public int packageDays
        {
            get { return _packageDays; }
            set { _packageDays = value; }
        }
        public string packageStatus
        {
            get { return _packageStatus; }
            set { _packageStatus = value; }
        }
        public int StockInHand
        {
            get { return _stockInHand; }
            set { _stockInHand = value; }
        }
        public decimal StartingFrom
        {
            get { return _startingFrom; }
            set { _startingFrom = value; }
        }
        public string CityCode
        {
            get { return _cityCode; }
            set { _cityCode = value; }
        }
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }
        public string Image1
        {
            get { return _image1; }
            set { _image1 = value; }
        }
        public string Image2
        {
            get { return _image2; }
            set { _image2 = value; }
        }
        public string Image3
        {
            get { return _image3; }
            set { _image3 = value; }
        }
        public string Theme
        {
            get { return _theme; }
            set { _theme = value; }
        }
        public string Introduction
        {
            get { return _introduction; }
            set { _introduction = value; }
        }
        public string Overview
        {
            get { return _overview; }
            set { _overview = value; }
        }
        public DateTime StartFrom
        {
            get { return _startFrom; }
            set { _startFrom = value; }
        }
        public DateTime EndTo
        {
            get { return _endTo; }
            set { _endTo = value; }
        }
        public string packageDuration
        {
            get { return _packageDuration; }
            set { _packageDuration = value; }
        }
        public DateTime AvailableDateStart
        {
            get { return _availableDateStart; }
            set { _availableDateStart = value; }
        }
        public DateTime AvailableDateEnd
        {
            get { return _availableDateEnd; }
            set { _availableDateEnd = value; }
        }
        public string UnvailableDates
        {
            get { return _unvailableDates; }
            set { _unvailableDates = value; }
        }
        public string Itinerary1
        {
            get { return _itinerary1; }
            set { _itinerary1 = value; }
        }
        public string Itinerary2
        {
            get { return _itinerary2; }
            set { _itinerary2 = value; }
        }
        public string Details
        {
            get { return _details; }
            set { _details = value; }
        }
        public string SupplierName
        {
            get { return _supplierName; }
            set { _supplierName = value; }
        }
        public string SupplierEmail
        {
            get { return _supplierEmail; }
            set { _supplierEmail = value; }
        }
        public string MealsIncluded
        {
            get { return _mealsIncluded; }
            set { _mealsIncluded = value; }
        }
        public string TransferInclude
        {
            get { return _transferInclude; }
            set { _transferInclude = value; }
        }
        public string PickLocation
        {
            get { return _pickLocation; }
            set { _pickLocation = value; }
        }
        public DateTime PickUpDateTime
        {
            get { return _pickUpDateTime; }
            set { _pickUpDateTime = value; }
        }
        public string DropOffLocation
        {
            get { return _dropOffLocation; }
            set { _dropOffLocation = value; }
        }
        public DateTime DropOffDateTime
        {
            get { return _dropOffDateTime; }
            set { _dropOffDateTime = value; }
        }
        public string Exclusion
        {
            get { return _exclusion; }
            set { _exclusion = value; }
        }
        public string Inclusion
        {
            get { return _inclusion; }
            set { _inclusion = value; }
        }
        public string CancellationPolicy
        {
            get { return _cancellationPolicy; }
            set { _cancellationPolicy = value; }
        }
        public string UnAvailableDays
        {
            get { return _unAvailableDays; }
            set { _unAvailableDays = value; }
        }
        public string ThingsToBring
        {
            get { return _thingsToBring; }
            set { _thingsToBring = value; }
        }
        public int BookingCutOff
        {
            get { return _bookingCutOff; }
            set { _bookingCutOff = value; }
        }

        public int FlexOrder
        {
            get { return _flexOrder; }
            set { _flexOrder = value; }
        }

        public string FlexLabel
        {
            get { return _flexLabel; }
            set { _flexLabel = value; }
        }
        public string FlexControl
        {
            get { return _flexControl; }
            set { _flexControl = value; }
        }
        public string FlexSqlQuery
        {
            get { return _flexSqlQuery; }
            set { _flexSqlQuery = value; }
        }
        public string FlexDataType
        {
            get { return _flexDataType; }
            set { _flexDataType = value; }
        }
        public string FlexMandatoryStatus
        {
            get { return _flexMandatoryStatus; }
            set { _flexMandatoryStatus = value; }
        }
        public DataTable DTFlexDetails
        {
            get { return _dtFlexDetails; }
            set { _dtFlexDetails = value; }
        }
        public DataTable DTPriceDetails
        {
            get { return _dtPriceDetails; }
            set { _dtPriceDetails = value; }
        }


        public string Image1Extn
        {
            get { return _image1Extn; }
            set { _image1Extn = value; }
        }
        public string Image2Extn
        {
            get { return _image2Extn; }
            set { _image2Extn = value; }
        }
        public string Image3Extn
        {
            get { return _image3Extn; }
            set { _image3Extn = value; }
        }

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public string IsFixedDeparture
        {
            get { return _isFixedDeparture; }
            set { _isFixedDeparture = value; }
        }
        public DataTable DTItineraryDetails
        {
            get { return _dtItineraryDetails; }
            set { _dtItineraryDetails = value; }
        }

        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }
        public string FDInclusions
        {
            get { return _fdInclusions; }
            set { _fdInclusions = value; }
        }

        public string HasAir
        {
            get { return _hasAir; }
            set { _hasAir = value; }
        }

        public string HasHotels
        {
            get { return _hasHotels; }
            set { _hasHotels = value; }
        }

        public string HasTours
        {
            get { return _hasTours; }
            set { _hasTours = value; }
        }

        public string HasTransfers
        {
            get { return _hasTransfers; }
            set { _hasTransfers = value; }
        }

        public string HasMeals
        {
            get { return _hasMeals; }
            set { _hasMeals = value; }
        }

        public DataTable DTSuppliers
        {
            get { return _dtSuppliers; }
            set { _dtSuppliers = value; }
        }

        public string OriginCity
        {
            get { return _originCity; }
            set { _originCity = value; }
        }
        public string OriginCountry
        {
            get { return _originCountry; }
            set { _originCountry = value; }
        }

        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        public int Paging
        {
            get { return _paging; }
            set { _paging = value; }
        }
        public bool IsInternational
        {
            get { return _isInternational; }
            set { _isInternational = value; }
        }

        public string Culture
        {
            get
            {
                return _culture;
            }

            set
            {
                _culture = value;
            }
        }
        #endregion
        #region Constructors
        public Package()
        {
            _id = -1;
            DataSet ds = GetData(_id);
            _dtFlexDetails = ds.Tables[1];
            _dtPriceDetails = ds.Tables[2];
            _dtSuppliers = ds.Tables[4];
        }

        public Package(long id)
        {
            _id = id;
            GetDetails(id);
        }
        #endregion
        #region Private Methods
        private void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null && ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);

            _dtFlexDetails = ds.Tables[1];
            _dtPriceDetails = ds.Tables[2];
            _dtItineraryDetails = ds.Tables[3];
            _dtSuppliers = ds.Tables[4];
        }

        private DataSet GetData(long id)
        {
            DataSet ds = new DataSet();
            //using (SqlConnection connection = DBGateway.Dal.GetConnection()) 
            //{
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_Package_ID", id);
                ds = DBGateway.FillSP(SPNames.PackageGetData, paramList);
                return ds;
            }
            catch
            {
                throw;
            }

        }

        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                if (_id >= 1)
                {

                    _id = Convert.ToInt64(dr["packageId"]);
                    _agencyId = Convert.ToInt16(dr["agencyId"]);
                    _packageName = Convert.ToString(dr["packageName"]);
                    _packageDays = Convert.ToInt16(dr["packageDays"]);
                    _startingFrom = Convert.ToDecimal(dr["packageStartingFrom"]);
                    _stockInHand = Convert.ToInt32(dr["stockInHand"]);
                    _cityCode = Convert.ToString(dr["citycode"]);
                    _countryCode = Convert.ToString(dr["countrycode"]);
                    _city = Convert.ToString(dr["packageCity"]);
                    _country = Convert.ToString(dr["packageCountry"]);
                    _region = dr["region"].ToString();
                    _image1 = Convert.ToString(dr["imagePath1"]);
                    _image2 = Convert.ToString(dr["imagePath2"]);
                    _image3 = Convert.ToString(dr["imagePath3"]);
                    _theme = Convert.ToString(dr["themeId"]);
                    _introduction = Convert.ToString(dr["introduction"]);
                    _overview = Convert.ToString(dr["overview"]);
                    //_startFrom = Convert.ToDateTime(dr["startFrom"]);
                    // _endTo = Convert.ToDateTime(dr["endTo"]);
                    _packageDuration = Convert.ToString(dr["durationHours"]);
                    _availableDateStart = Convert.ToDateTime(dr["availableFrom"]);
                    _availableDateEnd = Convert.ToDateTime(dr["availableTo"]);
                    _unAvailableDays = Convert.ToString(dr["unavailableDays"]);
                    _itinerary1 = Convert.ToString(dr["itinerary1"]);
                    _itinerary2 = Convert.ToString(dr["itinerary2"]);
                    _details = Convert.ToString(dr["details"]);
                    _supplierName = Convert.ToString(dr["supplierName"]);
                    _supplierEmail = Convert.ToString(dr["supplierEmail"]);
                    _mealsIncluded = Convert.ToString(dr["mealsIncluded"]);
                    _transferInclude = Convert.ToString(dr["transferIncluded"]);
                    _pickLocation = Convert.ToString(dr["pickupLocation"]);
                    if (dr["pickupDate"] != DBNull.Value)
                    {
                        _pickUpDateTime = Convert.ToDateTime(dr["pickupDate"]);
                    }
                    _dropOffLocation = Convert.ToString(dr["dropoffLocation"]);
                    if (dr["dropoffDate"] != DBNull.Value)
                    {
                        _dropOffDateTime = Convert.ToDateTime(dr["dropoffDate"]);
                    }
                    _exclusion = Convert.ToString(dr["exclusions"]);
                    _inclusion = Convert.ToString(dr["inclusions"]);
                    _cancellationPolicy = Convert.ToString(dr["cancellPolicy"]);
                    _thingsToBring = Convert.ToString(dr["thingsToBring"]);
                    _unvailableDates = Convert.ToString(dr["unavailableDates"]);
                    _bookingCutOff = Convert.ToInt16(dr["bookingCutOff"]);
                    if (dr["FDInclusions"] != DBNull.Value)
                    {
                        _fdInclusions = dr["FDInclusions"].ToString();
                    }
                    else
                    {
                        _fdInclusions = "";
                    }
                    _hasAir = Convert.ToString(dr["hasAir"]);
                    _hasHotels = Convert.ToString(dr["hasHotels"]);
                    _hasTours = Convert.ToString(dr["hasTours"]);
                    _hasTransfers = Convert.ToString(dr["hasTransfers"]);
                    _hasMeals = Convert.ToString(dr["hasMeals"]);
                    _originCity = Convert.ToString(dr["packageOriginCity"]);
                    _originCountry = Convert.ToString(dr["packageOriginCountry"]);
                    _currency = Convert.ToString(dr["currency"]);
                    if (dr["paging"] != DBNull.Value)
                    {
                        _paging = Convert.ToInt32(dr["paging"]);
                    }

                    if (dr["isInternational"] != DBNull.Value)
                    {
                        _isInternational = Convert.ToBoolean(dr["isInternational"]);
                    }
                    if (dr["cultureCode"] != DBNull.Value)
                    {
                        _culture = Convert.ToString(dr["cultureCode"]);
                    }
                }

            }
            catch
            {
                throw;
            }
        }
        #endregion

        public static DataTable GetTheme(string themeType)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@themeType", themeType);
                    data = DBGateway.ExecuteReaderSP(SPNames.GetTheme, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public static DataTable GetCountry()
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[0];
                    data = DBGateway.ExecuteReaderSP(SPNames.GetCountryList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }


        public static DataTable GetCity(string id)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@countryCode", id);

                    data = DBGateway.ExecuteReaderSP(SPNames.GetCityByCountryCode, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        public static DataTable GetList(string isFixedDep, int agentId)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@isFixedDep", isFixedDep);
                    //paramList[1] = new SqlParameter( "@P_AGENT_ID",  agentId);
                    if (agentId > 0) paramList[1] = new SqlParameter("@P_AGENT_ID", agentId);
                    else paramList[1] = new SqlParameter("@P_AGENT_ID", DBNull.Value);
                    data = DBGateway.ExecuteReaderSP(SPNames.PackageGetList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        public static string GetAgencyImagePath(int agencyId, string imageType)
        {

            SqlDataReader data = null;
            string imagePath = string.Empty;
            using (SqlConnection conn = CT.TicketReceipt.DataAccessLayer.DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@agencyId", agencyId);
                    paramList[1] = new SqlParameter("@imagetype", imageType);
                    data = DBGateway.ExecuteReaderSP(SPNames.GetPackageImagePath, paramList, conn);
                    if (data != null)
                    {
                        while (data.Read())
                        {
                            //imagePath = Convert.ToString(data["PackageImagePath"]);
                            imagePath = Convert.ToString(data["agent_img_path"]);
                        }
                    }
                    conn.Close();
                }

                catch { }
            }

            return imagePath;

        }
        public void Savepackage()
        {

            //SqlTransaction trans = null;
            //SqlCommand cmd = null;

            //using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            //{
            try
            {
                using (SqlConnection connection = DBGateway.GetConnection())
                    try
                    {
                        SqlParameter[] paramList = new SqlParameter[55];
                        paramList[0] = new SqlParameter("@packageName", _packageName);
                        paramList[1] = new SqlParameter("@packageDays", _packageDays);
                        paramList[2] = new SqlParameter("@packageStartingFrom", _startingFrom);
                        paramList[3] = new SqlParameter("@stockinHand", _stockInHand);
                        paramList[4] = new SqlParameter("@packageCity", _city);
                        paramList[5] = new SqlParameter("@packageCountry", _country);
                        paramList[6] = new SqlParameter("@imagePath1", _image1);
                        paramList[7] = new SqlParameter("@imagePath2", _image2);
                        if (_image3 == string.Empty)
                        {
                            paramList[8] = new SqlParameter("@imagePath3", DBNull.Value);//_image3 == string.Empty ? DBNull.Value.ToString() : _image3);
                        }
                        else
                        {
                            paramList[8] = new SqlParameter("@imagePath3", _image3);
                        }
                        paramList[9] = new SqlParameter("@themeId", _theme == string.Empty ? DBNull.Value.ToString() : _theme);
                        paramList[10] = new SqlParameter("@introduction", _introduction);

                        paramList[11] = new SqlParameter("@overview", _overview);
                        // paramList[12] = new SqlParameter("@startFrom", _startFrom);
                        // paramList[13] = new SqlParameter("@endTo", _endTo);
                        paramList[12] = new SqlParameter("@packageDuration", _packageDuration == string.Empty ? DBNull.Value.ToString() : _packageDuration);
                        paramList[13] = new SqlParameter("@availableFrom", _availableDateStart);
                        paramList[14] = new SqlParameter("@availableTo", _availableDateEnd);
                        paramList[15] = new SqlParameter("@unavailableDates", _unvailableDates == string.Empty ? DBNull.Value.ToString() : _unvailableDates);
                        paramList[16] = new SqlParameter("@itinerary1", _itinerary1);
                        paramList[17] = new SqlParameter("@itinerary2", _itinerary2 == string.Empty ? DBNull.Value.ToString() : _itinerary2);
                        paramList[18] = new SqlParameter("@details", _details == string.Empty ? DBNull.Value.ToString() : _details);
                        paramList[19] = new SqlParameter("@supplierName", _supplierName == string.Empty ? DBNull.Value.ToString() : _supplierName);

                        paramList[20] = new SqlParameter("@supplierEmail", _supplierEmail);
                        paramList[21] = new SqlParameter("@mealsIncluded", _mealsIncluded);
                        paramList[22] = new SqlParameter("@transferIncluded", _transferInclude);
                        paramList[23] = new SqlParameter("@pickupLocation", _pickLocation);
                        if (_pickUpDateTime == DateTime.MinValue)
                            paramList[24] = new SqlParameter("@pickupDate", DBNull.Value);
                        else
                            paramList[24] = new SqlParameter("@pickupDate", _pickUpDateTime);
                        // paramList[26] = new SqlParameter("@pickupDate", _pickUpDateTime == DateTime.MinValue ? DateTime.MinValue : _pickUpDateTime);
                        paramList[25] = new SqlParameter("@dropoffLocation", _dropOffLocation);

                        if (_dropOffDateTime == DateTime.MinValue)
                            paramList[26] = new SqlParameter("@dropoffDate", DBNull.Value);
                        else
                            paramList[26] = new SqlParameter("@dropoffDate", _dropOffDateTime);

                        // paramList[28] = new SqlParameter("@dropoffDate", _dropOffDateTime == DateTime.MinValue ? DateTime.MinValue : _dropOffDateTime);

                        paramList[27] = new SqlParameter("@exclusions", _exclusion);
                        paramList[28] = new SqlParameter("@inclusions", _inclusion);
                        paramList[29] = new SqlParameter("@cancellPolicy", _cancellationPolicy);
                        paramList[30] = new SqlParameter("@thingsToBring", _thingsToBring);
                        paramList[31] = new SqlParameter("@unavailableDays", _unAvailableDays);
                        paramList[32] = new SqlParameter("@bookingCutOff", _bookingCutOff);
                        paramList[33] = new SqlParameter("@createdBy", _createdBy);
                        paramList[34] = new SqlParameter("@img1_extn", _image1Extn);
                        paramList[35] = new SqlParameter("@img2_extn", _image2Extn);
                        paramList[36] = new SqlParameter("@img3_extn", _image3Extn == null ? DBNull.Value.ToString() : _image3Extn);
                        paramList[37] = new SqlParameter("@packageId", SqlDbType.BigInt);
                        paramList[37].Direction = ParameterDirection.Output;
                        paramList[38] = new SqlParameter("@A_MSG_TYPE", SqlDbType.VarChar, 10);
                        paramList[38].Direction = ParameterDirection.Output;
                        paramList[39] = new SqlParameter("@A_MSG_TEXT", SqlDbType.VarChar, 200);
                        paramList[39].Direction = ParameterDirection.Output;

                        paramList[40] = new SqlParameter("@id", _id);
                        paramList[41] = new SqlParameter("@isfixedDeparture", _isFixedDeparture);
                        paramList[42] = new SqlParameter("@region", _region == string.Empty ? DBNull.Value.ToString() : _region);
                        paramList[43] = new SqlParameter("@agencyid", _agencyId);
                        paramList[44] = new SqlParameter("@fdInclusions", _fdInclusions);
                        paramList[45] = new SqlParameter("@hasAir", _hasAir);
                        paramList[46] = new SqlParameter("@hasHotels", _hasHotels);
                        paramList[47] = new SqlParameter("@hasTours", _hasTours);
                        paramList[48] = new SqlParameter("@hasTransfers", _hasTransfers);
                        paramList[49] = new SqlParameter("@hasMeals", _hasMeals);
                        paramList[50] = new SqlParameter("@PackageOriginCountry", _originCountry);
                        paramList[51] = new SqlParameter("@PackageOriginCity", _originCity);
                        paramList[52] = new SqlParameter("@currency", _currency);
                        paramList[53] = new SqlParameter("@isInternational", _isInternational);
                        paramList[54] = new SqlParameter("@Culture", _culture);


                        DBGateway.ExecuteReaderSP(SPNames.PACKAGE_ADD, paramList, connection);

                        string messageType = Convert.ToString(paramList[38].Value);
                        if (messageType == "E")
                        {
                            string message = Convert.ToString(paramList[39].Value);

                            if (message != string.Empty) throw new Exception(message);
                        }

                        _packageId = Convert.ToInt64(paramList[37].Value);
                        _id = _packageId;
                        if (_dtFlexDetails != null)
                        {
                            DataTable dt = _dtFlexDetails.GetChanges();

                            int recordStatus = 0;
                            if (dt != null && dt.Rows.Count > 0)
                            {

                                foreach (DataRow dr in dt.Rows)
                                {
                                    switch (dr.RowState)
                                    {
                                        case DataRowState.Added: recordStatus = 1;
                                            break;
                                        case DataRowState.Modified: recordStatus = 2;
                                            break;
                                        case DataRowState.Deleted: recordStatus = -1;
                                            dr.RejectChanges();
                                            break;
                                        default: break;

                                    }
                                    if (Convert.ToInt32(dr["flexid"]) > 0)
                                    {
                                        SaveFlexDeatils(_packageId, Convert.ToInt32(dr["flexOrder"]), Convert.ToString(dr["flexLabel"]), Convert.ToString(dr["flexControl"]), Convert.ToString(dr["flexSqlQuery"]), Convert.ToString(dr["flexDataType"]), Convert.ToString(dr["flexMandatoryStatus"]), Convert.ToInt32(dr["flexid"]), Convert.ToInt32(dr["flexCreatedBy"]), recordStatus);
                                    }

                                }
                            }
                        }
                        if (_dtPriceDetails != null)
                        {
                            DataTable dt = _dtPriceDetails.GetChanges();
                            int recordStatus = 0;
                            if (dt != null && dt.Rows.Count > 0)
                            {


                                foreach (DataRow dr in dt.Rows)
                                {
                                    switch (dr.RowState)
                                    {
                                        case DataRowState.Added: recordStatus = 1;
                                            break;
                                        case DataRowState.Modified: recordStatus = 2;
                                            break;
                                        case DataRowState.Deleted: recordStatus = -1;
                                            dr.RejectChanges();
                                            break;
                                        default: break;


                                    }
                                    if (Convert.ToInt32(dr["priceId"]) > 0)
                                    {
                                        int stockinHand = 0;
                                        if (dr["StockInHand"] != DBNull.Value && Convert.ToInt32(dr["StockInHand"]) > 0)
                                        {
                                            stockinHand = Convert.ToInt32(dr["StockInHand"]);
                                        }
                                        DateTime priceDate = DateTime.MinValue;
                                        if (dr["PriceDate"] != DBNull.Value)
                                        {
                                            priceDate = Convert.ToDateTime(dr["PriceDate"]);
                                        }
                                        SavePriceDeatils(_packageId, Convert.ToString(dr["Label"]), Convert.ToInt32(dr["amount"]), Convert.ToDecimal(dr["SupplierCost"]), Convert.ToInt32(dr["tax"]), Convert.ToDecimal(dr["markup"]), Convert.ToDateTime(priceDate), recordStatus, Convert.ToInt32(dr["priceId"]), Convert.ToString(dr["PaxType"]), Convert.ToInt32(stockinHand), Convert.ToInt32(dr["createdBy"]), Convert.ToInt32(dr["MinPax"]), Convert.ToString(dr["roomType"]));

                                    }
                                }
                            }
                        }
                        if (_dtSuppliers != null)
                        {
                            DataTable dt = _dtSuppliers.GetChanges();
                            int recordStatus = 0;
                            if (dt != null && dt.Rows.Count > 0)
                            {


                                foreach (DataRow dr in dt.Rows)
                                {
                                    switch (dr.RowState)
                                    {
                                        case DataRowState.Added: recordStatus = 1;
                                            break;
                                        case DataRowState.Modified: recordStatus = 2;
                                            break;
                                        case DataRowState.Deleted: recordStatus = -1;
                                            dr.RejectChanges();
                                            break;
                                        default: break;


                                    }
                                    //if (Convert.ToInt32(dr["packSuppId"]) > 0)
                                    //{

                                    SaveSupplierDeatils(_packageId, Convert.ToInt32(dr["packSuppId"]), Convert.ToString(dr["packSuppName"]), Convert.ToString(dr["packSuppEmail"]), Convert.ToString(dr["packProductType"]), recordStatus, Convert.ToInt16(dr["packSuppCreatedBy"]));
                                    //}
                                }
                            }

                        }
                        SavePackageItineraryDetails();

                        connection.Close();
                    }

                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                //scope.Complete();
            }
            catch (System.Transactions.TransactionAbortedException ex)
            {
                throw ex;
            }

            //}
        }

        public void SaveFlexDeatils(long packageid, int flexorder, string flexlabel, string flexcontrol, string flexquery, string flexdatatype, string flexmandatorystatus, long flexId, int createdBy, int recordStatus)
        {
            using (SqlConnection connection = DBGateway.GetConnection())
                try
                {
                    SqlParameter[] paramList = new SqlParameter[12];
                    paramList[0] = new SqlParameter("@flexpackageId", packageid);
                    paramList[1] = new SqlParameter("@flexOrder", flexorder);
                    paramList[2] = new SqlParameter("@flexLabel", flexlabel);
                    paramList[3] = new SqlParameter("@flexControl", flexcontrol);
                    paramList[4] = new SqlParameter("@flexSqlQuery", flexquery);
                    paramList[5] = new SqlParameter("@flexDataType", flexdatatype);
                    paramList[6] = new SqlParameter("@flexMandatoryStatus", flexmandatorystatus);
                    paramList[7] = new SqlParameter("@flexCreatedBy", createdBy);
                    SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                    paramMsgType.Size = 10;
                    paramMsgType.Direction = ParameterDirection.Output;
                    paramList[8] = paramMsgType;

                    SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                    paramMsgText.Size = 100;
                    paramMsgText.Direction = ParameterDirection.Output;
                    paramList[9] = paramMsgText;
                    paramList[10] = new SqlParameter("@flexId", flexId);
                    paramList[11] = new SqlParameter("@recordStatus", recordStatus);
                    DBGateway.ExecuteReaderSP(SPNames.addPackageFlexMaster, paramList, connection);
                    if (Convert.ToString(paramMsgType.Value) == "E")
                        throw new Exception(Convert.ToString(paramMsgText.Value));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }

        public void SavePriceDeatils(long packageid, string label, decimal amount, decimal supplierCost, decimal tax, decimal markup, DateTime priceDate, int recordStatus, long priceId, string paxType, int stockInHand, int createdBy, int minPax, string roomType)
        {


            using (SqlConnection connection = DBGateway.GetConnection())
                try
                {
                    SqlParameter[] paramList = new SqlParameter[16];
                    paramList[0] = new SqlParameter("@pricePackageId", packageid);
                    paramList[1] = new SqlParameter("@Label", label);
                    paramList[2] = new SqlParameter("@amount", amount);
                    paramList[3] = new SqlParameter("@SupplierCost", supplierCost);
                    paramList[4] = new SqlParameter("@tax", tax);
                    paramList[5] = new SqlParameter("@markup", markup);
                    paramList[6] = new SqlParameter("@PriceCreatedBy", createdBy);
                    SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                    paramMsgType.Size = 10;
                    paramMsgType.Direction = ParameterDirection.Output;
                    paramList[7] = paramMsgType;

                    SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                    paramMsgText.Size = 100;
                    paramMsgText.Direction = ParameterDirection.Output;
                    paramList[8] = paramMsgText;
                    paramList[9] = new SqlParameter("@priceId", priceId);
                    paramList[10] = new SqlParameter("@recordStatus", recordStatus);
                    if (priceDate == DateTime.MinValue) paramList[11] = new SqlParameter("@priceDate", DBNull.Value);
                    else
                        paramList[11] = new SqlParameter("@priceDate", priceDate);
                    if (paxType == string.Empty)
                    {
                        paramList[12] = new SqlParameter("@PaxType", DBNull.Value);
                    }
                    else
                    {
                        paramList[12] = new SqlParameter("@PaxType", paxType);
                    }
                    if (stockInHand == 0)
                    {
                        paramList[13] = new SqlParameter("@StockInHand", DBNull.Value);
                    }
                    else
                    {
                        paramList[13] = new SqlParameter("@StockInHand", stockInHand);
                    }
                    //newly added minPax    Added by brahmam 17.10.2016
                    if (minPax == 0)
                    {
                        paramList[14] = new SqlParameter("@MinPax", DBNull.Value);
                    }
                    else
                    {
                        paramList[14] = new SqlParameter("@MinPax", minPax);
                    }
                    if (string.IsNullOrEmpty(roomType))
                    {
                        paramList[15] = new SqlParameter("@roomType", DBNull.Value);
                    }
                    else
                    {
                        paramList[15] = new SqlParameter("@roomType", roomType);
                    }
                    DBGateway.ExecuteReaderSP(SPNames.PACKAGE_PRICE_ADD, paramList, connection);
                    if (Convert.ToString(paramMsgType.Value) == "E")
                        throw new Exception(Convert.ToString(paramMsgText.Value));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }

        public void SaveSupplierDeatils(long packageid, long packSuppId, string suppName, string suppMail, string productType, int recordStatus, int createdBy)
        {


            using (SqlConnection connection = DBGateway.GetConnection())
                try
                {
                    SqlParameter[] paramList = new SqlParameter[9];
                    paramList[0] = new SqlParameter("@P_packageId", packageid);
                    paramList[1] = new SqlParameter("@P_packSuppName", suppName);
                    paramList[2] = new SqlParameter("@P_packSuppEmail", suppMail);
                    paramList[3] = new SqlParameter("@P_packProductType", productType);
                    paramList[4] = new SqlParameter("@P_packSuppCreatedBy", createdBy);
                    SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                    paramMsgType.Size = 10;
                    paramMsgType.Direction = ParameterDirection.Output;
                    paramList[5] = paramMsgType;

                    SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                    paramMsgText.Size = 100;
                    paramMsgText.Direction = ParameterDirection.Output;
                    paramList[6] = paramMsgText;
                    paramList[7] = new SqlParameter("@id", packSuppId);
                    paramList[8] = new SqlParameter("@recordStatus", recordStatus);

                    DBGateway.ExecuteReaderSP(SPNames.PackageSupplier, paramList, connection);
                    if (Convert.ToString(paramMsgType.Value) == "E")
                        throw new Exception(Convert.ToString(paramMsgText.Value));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }
        void SavePackageItineraryDetails()
        {
            if (_dtItineraryDetails != null)
            {
                using (SqlConnection connection = DBGateway.GetConnection())

                    for (int i = 0; i < _dtItineraryDetails.Rows.Count; i++)
                    {
                        try
                        {
                            DataRow row = _dtItineraryDetails.Rows[i];
                            SqlParameter[] paramList = new SqlParameter[11];
                            paramList[0] = new SqlParameter("@PackageId", _id);
                            paramList[1] = new SqlParameter("@ItineraryName", row["ItineraryName"].ToString());
                            paramList[2] = new SqlParameter("@Meals", row["meals"].ToString());
                            paramList[3] = new SqlParameter("@Itinerary", row["itinerary"].ToString());
                            paramList[4] = new SqlParameter("@Day", (i + 1));
                            paramList[5] = new SqlParameter("@Status", "A");
                            paramList[6] = new SqlParameter("@CreatedBy", _createdBy);
                            SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                            paramMsgType.Size = 10;
                            paramMsgType.Direction = ParameterDirection.Output;
                            paramList[7] = paramMsgType;

                            SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                            paramMsgText.Size = 100;
                            paramMsgText.Direction = ParameterDirection.Output;
                            paramList[8] = paramMsgText;
                            if (row.RowState == DataRowState.Added)
                            {
                                paramList[9] = new SqlParameter("@Id", -1);
                            }
                            else
                            {
                                paramList[9] = new SqlParameter("@Id", row["Id"]);
                            }
                            paramList[10] = new SqlParameter("@NoOfDays", _packageDays);
                            DBGateway.ExecuteNonQuerySP(SPNames.PackageItineraryAdd, paramList);
                            if (Convert.ToString(paramMsgType.Value) == "E")
                                throw new Exception(Convert.ToString(paramMsgText.Value));
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
            }
        }


        void SavePackageSuppliers()
        {
            if (_dtSuppliers != null)
            {
                using (SqlConnection connection = DBGateway.GetConnection())

                    for (int i = 0; i < _dtSuppliers.Rows.Count; i++)
                    {
                        try
                        {
                            DataRow row = _dtSuppliers.Rows[i];
                            SqlParameter[] paramList = new SqlParameter[10];
                            paramList[0] = new SqlParameter("@P_packageId", _id);
                            paramList[1] = new SqlParameter("@P_packSuppName", row["packSuppName"].ToString());
                            paramList[2] = new SqlParameter("@P_packSuppEmail", row["packSuppEmail"].ToString());
                            paramList[3] = new SqlParameter("@P_packProductType", row["packProductType"].ToString());
                            paramList[4] = new SqlParameter("@P_packSuppStatus", "A");
                            paramList[5] = new SqlParameter("@P_packSuppCreatedBy", _createdBy);
                            SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                            paramMsgType.Size = 10;
                            paramMsgType.Direction = ParameterDirection.Output;
                            paramList[6] = paramMsgType;

                            SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                            paramMsgText.Size = 100;
                            paramMsgText.Direction = ParameterDirection.Output;
                            paramList[7] = paramMsgText;
                            if (row.RowState == DataRowState.Added)
                            {
                                paramList[8] = new SqlParameter("@Id", -1);
                            }
                            else
                            {
                                paramList[8] = new SqlParameter("@Id", row["Id"]);
                            }

                            DBGateway.ExecuteNonQuerySP(SPNames.PackageSupplier, paramList);
                            if (Convert.ToString(paramMsgType.Value) == "E")
                                throw new Exception(Convert.ToString(paramMsgText.Value));
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
            }
        }

        //Added by brahmam package Status Changing
        public static void packageStatusUpdate(int packageId, string packageStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_packageId", packageId);
                paramList[1] = new SqlParameter("@P_packageStatus", packageStatus);
                DBGateway.FillDataTableSP("usp_packageStatus_Update", paramList);
            }
            catch { throw; }
        }

        public static DataTable GetPackageListByAgentId(int agentId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@agentId", agentId);
                dt = DBGateway.FillDataTableSP("usp_PackageListByAgentId", paramList);
            }
            catch { throw; }
            return dt;
        }
        public static string GenerateWhereString(bool active, bool inactive)
        {
            string whereString = string.Empty;
            if (active && inactive)
            {
                whereString = "where";
            }
            else
            {
                if (active)
                {
                    whereString = "where packagestatus = 'A' and";
                }
                else
                {
                    whereString = "where packagestatus = 'D' and";
                }
            }

            return whereString;
        }
        //public static DataTable UpdatePackageSettings(string whereString, List<int> pagingList, int agencyId)
        public static void UpdatePackageSettings(Dictionary<int, int> packageList, int agentId)
        {
            SqlCommand cmd = null;
            SqlTransaction trans = null;

            try
            {
                
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                foreach (KeyValuePair<int, int> kvPair in packageList)
                {
                    SqlParameter[] paramList = new SqlParameter[3];
                    paramList[0] = new SqlParameter("@PackageId", kvPair.Key);
                    paramList[1] = new SqlParameter("@PagingOrder", kvPair.Value);
                    paramList[2] = new SqlParameter("@ModifiedBy", agentId);

                    DBGateway.ExecuteNonQueryDetails(cmd, "usp_Update_PackageListPaging", paramList);
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }

            //DataTable dataTable = new DataTable();
            //SqlDataAdapter adapter = new SqlDataAdapter();
            //using (SqlConnection connection = DBGateway.GetConnection())
            //{
            //    SqlParameter[] paramList = new SqlParameter[3];
            //    paramList[0] = new SqlParameter("@whereString", whereString);
            //    paramList[1] = new SqlParameter("@agencyId", agencyId);
            //    paramList[2] = new SqlParameter("@Status", "UPDATE");
            //    try
            //    {
            //        SqlCommand command = new SqlCommand(SPNames.GetAllPackageDetailsUsingWhereString, connection);
            //        command.CommandType = CommandType.StoredProcedure;
            //        command.CommandTimeout = 60;
            //        command.Parameters.AddRange(paramList);
            //        adapter = new SqlDataAdapter(command);
            //        int rowsAdded = adapter.Fill(dataTable);

            //        for (int i = 0; i < pagingList.Count; i++)
            //        {
            //            dataTable.Rows[i]["paging"] = pagingList[i];
            //        }
            //        SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);
            //        adapter.Update(dataTable);
            //    }
            //    catch (Exception ex)
            //    {
            //        Audit.Add(CT.Core.EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during UpdatePackageSettings() in HolidayPackage.cs Unable to update Package deals setting for home page and paging on India Times. | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            //        throw new Exception("Unable to update Package deals setting for home page and paging on India Times.");
            //    }
            //    finally
            //    {
            //        connection.Close();
            //        adapter.Dispose();
            //    }
            //}
           // return dataTable;
        }
        public static int UpdatePackageStatus(int packageId, string status)
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@packageId", packageId);
            paramList[1] = new SqlParameter("@status", status);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdatePackageStatus", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(CT.Core.EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during UpdatePackageStatus() in Package.cs for packageId = " + packageId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }
        public static int DeletePackageStatus(int packageId)
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@packageId", packageId);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_DeletePackage", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(CT.Core.EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during DeletePackageStatus() in Package.cs for packageId = " + packageId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }
        public static DataTable GetPackageListByAgentId(int agentId,int themeId, string whereString, string cultureCode)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@agencyId", agentId);
                paramList[1] = new SqlParameter("@whereString", whereString);
                if (themeId > 0) paramList[2] = new SqlParameter("@themeId", themeId.ToString());
                paramList[3] = new SqlParameter("@cultureCode", cultureCode);
                dt = DBGateway.FillDataTableSP(SPNames.GetAllPackageDetailsUsingWhereString, paramList);
            }
            catch { throw; }
            return dt;
        }
    }
}
