using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.HolidayDeals
{
    public class HolidayPackageSeason
    {
        #region privateFields
        int seasonId;
        int dealId;
        DateTime startDate;
        DateTime endDate;
        string hotelName;
        HolidayPackageHotelRating star;
        decimal singlePrice;
        decimal twinSharingPrice;
        decimal tripleSharingPrice;
        decimal childWithBedPrice;
        decimal childWithoutBedPrice;
        string hotelDescription;
        string hotelSupplierName;
        string hotelSupplierEmail;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        //added on 25/05/2016 by chandan
        string supplierCurrency;

       
        #endregion

        #region publicProperties
        public int SeasonId
        {
            get
            {
                return seasonId;
            }
            set
            {
                seasonId = value;
            }
        }

        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        public string HotelDescription
        {
            get { return hotelDescription; }
            set { hotelDescription = value; }
        }

        public string HotelSupplierName
        {
            get { return hotelSupplierName; }
            set { hotelSupplierName = value; }
        }

        public string HotelSupplierEmail
        {
            get { return hotelSupplierEmail; }
            set { hotelSupplierEmail = value; }
        }

        
        public HolidayPackageHotelRating Star
        {
            get
            {
                return star;
            }
            set
            {
                star = value;
            }
        }

        public decimal SinglePrice
        {
            get{return singlePrice;}
            set { singlePrice = value; }
        }

        public decimal TwinSharingPrice
        {
            get
            {
                return twinSharingPrice;
            }
            set
            {
                twinSharingPrice = value;
            }
        }

        public decimal TripleSharingPrice
        {
            get { return tripleSharingPrice; }
            set { tripleSharingPrice = value; }
        }

        public decimal ChildWithBedPrice
        {
            get
            {
                return childWithBedPrice;
            }
            set
            {
                childWithBedPrice = value;
            }
        }

        public decimal ChildWithoutBedPrice
        {
            get
            {
                return childWithoutBedPrice;
            }
            set
            {
                childWithoutBedPrice = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
               lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        //added on 25/05/2016 by chandan
        public string SupplierCurrency
        {
            get { return supplierCurrency; }
            set { supplierCurrency = value; }
        }
        #endregion

        #region publicMethods

        /// <summary>
        /// Saves and updates seasons for india times pakage deals.
        /// </summary>
        /// <returns>season Id in case of save and rows affected in case of update</returns>
        public int Save()
        {
            int returnSeasonId = 0;
            try
            {
               
                SqlParameter[] paramList;
                if (seasonId == 0)
                {
                    paramList = new SqlParameter[16];
                }
                else
                {
                    paramList = new SqlParameter[15];
                }
                paramList[0] = new SqlParameter("@startDate", startDate);
                paramList[1] = new SqlParameter("@endDate", endDate);
                paramList[2] = new SqlParameter("@hotelName", hotelName);
                paramList[3] = new SqlParameter("@star", (int)star);
                paramList[4] = new SqlParameter("@twinSharingPrice", twinSharingPrice);
                paramList[5] = new SqlParameter("@childWithBedPrice", childWithBedPrice);
                paramList[6] = new SqlParameter("@childWithoutBedPrice", childWithoutBedPrice);
                paramList[7] = new SqlParameter("@createdBy", createdBy);
                paramList[8] = new SqlParameter("@seasonId", seasonId);
                paramList[9] = new SqlParameter("@hotelDescription", hotelDescription);
                paramList[10] = new SqlParameter("@supplierName", hotelSupplierName);
                paramList[11] = new SqlParameter("@supplierEmail", hotelSupplierEmail);
                paramList[12] = new SqlParameter("@singlePrice", singlePrice);
                paramList[13] = new SqlParameter("@tripleSharingPrice", tripleSharingPrice);
                paramList[14] = new SqlParameter("@supplierCurrency", supplierCurrency);//added on 25/05/2016
                if (seasonId == 0)
                {
                    paramList[8].Direction = ParameterDirection.Output;
                    paramList[15] = new SqlParameter("@dealId", dealId);

                    try
                    {
                        DBGateway.ExecuteNonQuerySP(SPNames.HolidayPackageSeasonSave, paramList);
                        returnSeasonId = (int)paramList[8].Value;
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during Save() in HolidayPackageSeason.cs for dealId=" + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                        returnSeasonId = 0;
                    }
                }
                else
                {
                    try
                    {
                        returnSeasonId = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHolidayPackageSeason, paramList);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during update in Save() in HolidayPackageSeason.cs for dealId=" + dealId + ", seasonId=" + seasonId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                        returnSeasonId = 0;
                    }

                }
            }
            catch (Exception ex1)
            {
                Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Holiday Error| " + ex1.ToString() + " | ", "");

            }
            return returnSeasonId;
        }

        /// <summary>
        /// Loads list of hotel season against a deal id
        /// </summary>
        /// <param name="dealId">deal id</param>
        /// <returns>List for HolidayPackageSeason</returns>
        public static List<HolidayPackageSeason> LoadhotelSeasons(int dealId)
        {
            SqlDataReader data = null;
            List<HolidayPackageSeason> tempList = new List<HolidayPackageSeason>();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@dealId", dealId);
                try
                {
                    data = DBGateway.ExecuteReaderSP(SPNames.GetHolidayPackageSeasons, paramList, connection);
                    while (data.Read())
                    {
                        HolidayPackageSeason ssn = new HolidayPackageSeason();
                        ssn.seasonId = Convert.ToInt32(data["seasonId"]);
                        ssn.dealId = Convert.ToInt32(data["dealId"]);
                        ssn.startDate = Convert.ToDateTime(data["startDate"]);
                        ssn.endDate = Convert.ToDateTime(data["endDate"]);
                        ssn.hotelName = Convert.ToString(data["hotelName"]);
                        ssn.hotelDescription = Convert.ToString(data["HotelDescription"]);
                        ssn.hotelSupplierName = Convert.ToString(data["SupplierName"]);
                        ssn.hotelSupplierEmail = Convert.ToString(data["SupplierEmail"]);
                        ssn.star = (HolidayPackageHotelRating)(Enum.Parse(typeof(HolidayPackageHotelRating), Convert.ToString(data["star"])));
                        ssn.singlePrice = Convert.ToDecimal(data["singlePrice"]);
                        ssn.twinSharingPrice = Convert.ToDecimal(data["twinSharingPrice"]);
                        ssn.tripleSharingPrice = Convert.ToDecimal(data["tripleSharingPrice"]);
                        ssn.childWithBedPrice = Convert.ToDecimal(data["childWithBedPrice"]);
                        ssn.childWithoutBedPrice = Convert.ToDecimal(data["childWithoutBedPrice"]);
                        ssn.supplierCurrency = Convert.ToString(data["SupplierCurrency"]);//added on 25/05/2016
                        tempList.Add(ssn);
                    }
                }
                catch (Exception ex)
                {
                   Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during LoadhotelSeasons() in HolidayPackageSeason.cs for dealId=" + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load hotel seasons for deal id: " + dealId);
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return tempList;
        }

        public static int Delete(string seasonId)
        {
            int returnSeasonId = 0;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@seasonId", seasonId);
            try
            {
                returnSeasonId = DBGateway.ExecuteNonQuerySP(SPNames.DeleteHolidayPackageSeason, paramList);
            }
            catch (Exception ex)
            {
               Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during Delete() in HolidayPackageSeason.cs for season id:" + seasonId + "| " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                returnSeasonId = 0;
            }
            return returnSeasonId;
        }
        #endregion

    }

}
