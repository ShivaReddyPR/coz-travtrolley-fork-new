namespace CT.HolidayDeals
{
    public enum HolidayPackageHotelRating
    {
        All = 0,
        OneStar = 1,
        TwoStar = 2,
        ThreeStar = 3,
        FourStar = 4,
        FiveStar = 5
    }

    public enum HolidayPackageQueriesStatus
    {
        New = 1,
        Opened = 2,
        Processed = 3,
        Rejected = 4,
        Closed = 5,
        Duplicate = 6
    }

    public enum ScriptType
    {
        Analytics = 1,
        PPC = 2
    }

    public enum HeaderType
    {
        Image = 1,
        Text = 2
    }


    public enum PackagePaymentStatus
    {
        InProgress = 1,
        Paid = 2,
        Failed = 3
    }

    public enum PackageStatus
    {
        Booked = 1,
        Confirmed = 2,
        Cancelled = 3
    }


    public enum HolidayPaxGender
    {
        Male = 1,
        Female = 2
    }

    public enum PaxType
    {
        Adult = 1,
        Child = 2
    }
}
