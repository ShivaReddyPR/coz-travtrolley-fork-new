using System;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CT.Core;

namespace CT.HolidayDeals
{
    public class HolidaySettings
    {
        int id;
        string siteAddress;
        int agencyId;
        int memberId;
        string siteTitle;
        string tradingName;
        string email;
        string logo;
        string phone;
        string fax;
        ScriptType scriptType;
        string script;

        #region Properties
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public string SiteAddress
        {
            get
            {
                return siteAddress;
            }
            set
            {
                siteAddress = value;
            }
        }
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        public int MemberId
        {
            get
            {
                return memberId;
            }
            set
            {
                memberId = value;
            }
        }
        public string SiteTitle
        {
            get
            {
                return siteTitle;
            }
            set
            {
                siteTitle = value;
            }
        }
        public string TradingName
        {
            get
            {
                return tradingName;
            }
            set
            {
                tradingName = value;
            }
        }
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public string Logo
        {
            get
            {
                return logo;
            }
            set
            {
                logo = value;
            }
        }
        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }
        public string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                fax = value;
            }
        }
        public ScriptType ScriptType
        {
            get
            {
                return scriptType;
            }
            set
            {
                scriptType = value;
            }
        }
        public string Script
        {
            get
            {
                return script;
            }
            set
            {
                script = value;
            }
        }

        #endregion

        private void Validate()
        {
            if (siteAddress == null || siteAddress.Trim().Length == 0 || siteAddress.Trim().Length > 150)
            {
                throw new ArgumentException(" siteAddress cannot be null,empty and should be less than 151 characters.");
            }
            if (siteTitle == null || siteTitle.Trim().Length == 0 || siteTitle.Trim().Length > 150)
            {
                throw new ArgumentException(" siteTitle cannot be null,empty and should be less than 151 characters.");
            }
            if (tradingName == null || tradingName.Trim().Length == 0 || tradingName.Trim().Length > 150)
            {
                throw new ArgumentException(" tradingName cannot be null,empty and should be less than 151 characters.");
            }
            if (email == null || email.Trim().Length == 0 || email.Trim().Length > 150)
            {
                throw new ArgumentException(" email cannot be null,empty and should be less than 151 characters.");
            }
            if (phone == null || phone.Trim().Length == 0 || phone.Trim().Length > 20)
            {
                throw new ArgumentException(" phone cannot be null,empty and should be less than 21 characters.");
            }
        }


        public int Save()
        {
            Validate();
            int rowsAffected = 0;
            SqlParameter[] paramList;
            if (id > 0)
            {
                paramList = new SqlParameter[11];
            }
            else
            {
                paramList = new SqlParameter[12];
            }
            
            paramList[0] = new SqlParameter("@siteAddress", siteAddress);
            paramList[1] = new SqlParameter("@siteTitle", siteTitle);
            paramList[2] = new SqlParameter("@tradingName", tradingName);
            paramList[3] = new SqlParameter("@email", email);
            if (logo == null)
            {
                paramList[4] = new SqlParameter("@logo", DBNull.Value);
            }
            else
            {
                paramList[4] = new SqlParameter("@logo", logo);
            }
            paramList[5] = new SqlParameter("@phone", phone);
            if (fax == null)
            {
                paramList[6] = new SqlParameter("@fax", DBNull.Value);
            }
            else
            {
                paramList[6] = new SqlParameter("@fax", fax);
            }
            if ((int)scriptType == 0)
            {
                paramList[7] = new SqlParameter("@scriptType", DBNull.Value);
            }
            else
            {
                paramList[7] = new SqlParameter("@scriptType", scriptType);
            }
            if (script == null)
            {
                paramList[8] = new SqlParameter("@script", DBNull.Value);
            }
            else
            {
                paramList[8] = new SqlParameter("@script", script);
            }
            if (id > 0)
            {
                int errorId = 0;

                paramList[9] = new SqlParameter("@id", id);
                paramList[10] = new SqlParameter("@errorId", SqlDbType.Int);
                paramList[10].Direction = ParameterDirection.Output;
                try
                {
                    rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHolidaySettings, paramList);
                    errorId = (int)paramList[10].Value;
                }
                catch (Exception ex)
                {
                   Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during Save() in HolidaySettings.cs Unable to update holiday settings for white lable home page. | " + ex.Message + " | " + ex.InnerException + " | " + ex.StackTrace + " | " + DateTime.Now, "");
                    throw new Exception("Unable to update holiday settings. Please try again later.");
                }

                if (errorId < 0)
                {
                    if (errorId == -1)
                    {
                       Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Error during Save() update in HolidaySettings.cs Site name already exist. | " + DateTime.Now, "");
                        throw new Exception("Unable to update holiday settings. Site name already exist.");
                    }
                    if (errorId == -4)
                    {
                       Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Error during Save() update in HolidaySettings.cs Email already exist. | " + DateTime.Now, "");
                        throw new Exception("Unable to update holiday settings. Email already exist.");
                    }
                }
            }
            else
            {
                paramList[9] = new SqlParameter("@agencyId", agencyId);
                paramList[10] = new SqlParameter("@memberId", memberId);
                paramList[11] = new SqlParameter("@id", SqlDbType.Int);
                paramList[11].Direction = ParameterDirection.Output;

                try
                {
                    rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHolidaySettings, paramList);
                    id = (int)paramList[11].Value;
                }
                catch (Exception ex)
                {
                   Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during Save() in HolidaySettings.cs Unable to Save holiday settings for white lable home page. | " + ex.Message + " | " + ex.InnerException + " | " + ex.StackTrace + " | " + DateTime.Now, "");
                    throw new Exception("Unable to save holiday settings. Please try again later.");
                }
                if (id < 0)
                {
                    if (id == -1)
                    {
                       Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Error during Save() in HolidaySettings.cs Site name already exist. | " + DateTime.Now, "");
                        throw new Exception("Unable to save holiday settings. Site name already exist.");
                    }
                    if (id == -2)
                    {
                       Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Error during Save() in HolidaySettings.cs AgencyId already exist. | " + DateTime.Now, "");
                        throw new Exception("Unable to save holiday settings. AgencyId already exist.");
                    }
                    if (id == -3)
                    {
                       Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Error during Save() in HolidaySettings.cs MemberId already exist. | " + DateTime.Now, "");
                        throw new Exception("Unable to save holiday settings. MemberId already exist.");
                    }
                    if (id == -4)
                    {
                       Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Error during Save() in HolidaySettings.cs Email already exist. | " + DateTime.Now, "");
                        throw new Exception("Unable to save holiday settings. Email already exist.");
                    }
                }

            }
            return rowsAffected;
        }

        private void ReadDataReader(SqlDataReader reader)
        {
            if (reader.Read())
            {
                id = Convert.ToInt32(reader["id"]);
                siteAddress = Convert.ToString(reader["siteAddress"]);
                agencyId = Convert.ToInt32(reader["agencyId"]);
                memberId = Convert.ToInt32(reader["memberId"]);
                siteTitle = Convert.ToString(reader["siteTitle"]);
                tradingName = Convert.ToString(reader["tradingName"]);
                email = Convert.ToString(reader["email"]);
                if (reader["logo"] != DBNull.Value)
                {
                    logo = Convert.ToString(reader["logo"]);
                }
                phone = Convert.ToString(reader["phone"]);
                if (reader["fax"] != DBNull.Value)
                {
                    fax = Convert.ToString(reader["fax"]);
                }
                if (reader["scriptType"] != DBNull.Value)
                {
                    scriptType = (ScriptType)Convert.ToInt32(reader["scriptType"]);
                }
                if (reader["script"] != DBNull.Value)
                {
                    script = Convert.ToString(reader["script"]);
                }
            }
        }
        public void Load(int agencyId)
        {
            SqlDataReader reader = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@agencyId", agencyId);
                try
                {
                    reader = DBGateway.ExecuteReaderSP(SPNames.GetHolidaySettings, paramList, connection);
                    ReadDataReader(reader);
                }
                catch(Exception ex)
                {
                   Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during Load() in HolidaySettings.cs Unable to load holiday settings for white lable home page. | " + ex.Message + " | " + ex.InnerException + " | " + ex.StackTrace + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load holiday settings. Please try again later.");
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
            }
        }
    }
}