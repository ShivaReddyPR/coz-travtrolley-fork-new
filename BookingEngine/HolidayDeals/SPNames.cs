using System;

namespace CT.HolidayDeals
{
    internal static class SPNames
    {
        //IPAddressDetails
        public const string GetIPAddressDetailsByIPAddress = "usp_GetIPAddressDetailsByIPAddress";
        public const string AddIPAddressDetail = "usp_AddIPAddressDetail";
        public const string UpdateIPAddressStatus = "usp_UpdateIPAddressStatus";
        public const string GetIPAddressStatus = "usp_GetIPAddressStatus";
        public const string UpdateIPAddressDetail = "usp_UpdateIPAddressDetail";
        public const string IsIPAddressExists = "usp_IsIPAddressExists";


        // For HolidayPackage deal automation
        public const string HolidayPackageSave = "usp_HolidayPackageSave";
        public const string UpdateImagePath = "usp_UpdateImagePath";
        public const string HolidayPackageSeasonSave = "usp_HolidayPackageSeasonSave";
        public const string GetAllHolidayPackageDeals = "usp_GetAllHolidayPackageDeals";
        public const string GetAllHolidayPackageDealsUsingWhereString = "usp_GetAllHolidayPackageDealsUsingWhereString";


        public const string UpdateHolidayPackageDealStatus = "usp_UpdateHolidayPackageDealStatus";
        public const string DeleteHolidayPackageDeal = "usp_DeleteHolidayPackageDeal";
        public const string GetAllNamesOfHolidayPackageDeals = "usp_GetAllNamesOfHolidayPackageDeals";
        public const string GetMappedHotelDealIDs = "usp_GetMappedHotelDealIDs";
        public const string GetHolidayPackageDealForEdit = "usp_GetHolidayPackageDealForEdit";
        public const string GetHolidayPackageSeasons = "usp_GetHolidayPackageSeasons";
        public const string UpdateHolidayPackage = "usp_UpdateHolidayPackage";
        public const string UpdateHolidayPackageSeason = "usp_UpdateHolidayPackageSeason";
        public const string UpdateMainImagePath = "usp_UpdateMainImagePath";
        public const string UpdateSideImagePath = "usp_UpdateSideImagePath";
        public const string UpdateThumbnailImagePath = "usp_UpdateThumbnailImagePath";
        public const string DeleteHolidayPackageSeason = "usp_DeleteHolidayPackageSeason";

        //added on 02032016 for Main image1 & MainImage2
        public const string UpdateMainImage1Path = "usp_UpdateMainImage1Path";
        public const string UpdateMainImage2Path = "usp_UpdateMainImage2Path";

        //For package queries
        public const String GetPackageQueries = "usp_GetPackageQueries";
        public const String GetPackageQueriesUsingWhereString = "usp_GetPackageQueriesUsingWhereString";
        public const String GetPackageQueriesCount = "usp_GetPackageQueriesCount";
        public const String PackageQueryStatusUpdate = "usp_PackageQueryStatusUpdate";
        public const String GetPackageQueriesRemarks = "usp_GetPackageQueriesRemarks";
        public const String PackageQueryDetailsUpdate = "usp_PackageQueryDetailsUpdate";
        public const String GetAllPackageQueries = "usp_GetAllPackageQueries";

        //For Holiday Package Home page.
        public const String GetAllActiveHotelThemes = "usp_GetAllActiveHotelThemes";
        public const String GetDealThemeIndexTableSchema = "usp_GetDealThemeIndexTableSchema";
        public const String GetSelectedThemesForDeal = "usp_GetSelectedThemesForDeal";
        public const String DeleteFromThemeIndexTableSchema = "usp_DeleteFromThemeIndexTableSchema";

        //Holiday Settings
        public const String GetHolidaySettings = "usp_GetHolidaySettings";
        public const String AddHolidaySettings = "usp_AddHolidaySettings";
        public const String UpdateHolidaySettings = "usp_UpdateHolidaySettings";

        public const string GetAgencyList = "usp_GetAgencyList";
        public const string GetPackageList = "usp_GetPackageList";
       // public const string GetPackageImagePath = "usp_GetAgencyImagePath";
        public const string GetPackageImagePath = "usp_GetAgentImgPath"; // changed by chandan on 15/10/2015

        //HolidayDealLayout
        public const String GetHolidayDealLayout = "usp_GetHolidayDealLayout";
        public const String AddHolidayDealLayout = "usp_AddHolidayDealLayout";
        public const String UpdateHolidayDealLayout = "usp_UpdateHolidayDealLayout";

        public const String GetHolidayPackageQueues = "usp_GetHolidaypackagequeue";
        public const String GetHolidayPackageQueuesWithCustomePaging = "usp_GetHolidayPackageQueueWithCustomePaging";
        public const String GetHolidayPackageQueuesById = "usp_GetHolidayPackageQueuesById";
        public const String UpdateHolidaypackagequeue = "usp_UpdateHolidaypackagequeue";

        public const String GetListPackageQueueHistory = "usp_GetListPackageQueueHistory";
        public const String GetPackagePaxQueue = "usp_GetPackagePaxQueue";
        
        // Enquiry
        public const String GetEnquiry = "usp_getEnquiry";

        //PackageMasterusp_PackageMaster_getdata
        public const string PackageGetData = "usp_PackageMaster_getdata";
        public const string GetTheme = "usp_GetTheme";
        public const string GetCountryList = "usp_GetCountryList";
        public const string GetCityByCountryCode = "usp_GetCityByCountryCode";
        public const string PackageGetList = "usp_PackageMaster_getList";
        public const string PACKAGE_ADD = "usp_Package_Add";
        public const string addPackageFlexMaster = "usp_addPackageFlexMaster";
        public const string PACKAGE_PRICE_ADD = "usp_addPackagePricedetails";
        public const string PackageSupplier = "usp_AddPackageSupplier";
        public const string PackageItineraryAdd = "usp_AddPackageItinerary";
        public const string GetAllPackageDetailsUsingWhereString="usp_GetAllPackageDetailsUsingWhereString";
    }
}
