using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.HolidayDeals
{
    public class HolidayPackage
    {
        #region privateFields
        int dealId;
        string dealName;
        string city;
        string country;
        string description;
        bool isInternational;
        int nights;
        string mainImagePath;
        string imagePath;
        string thumbnailPath;
        

        bool hasAirfare;
        bool hasHotel;
        bool hasSightSeeing;
        bool hasMeals;
        bool hasTransport;
        string overview;
        string inclusions;
        string itinerary1;
        string itinerary2;
        string itinerary3;
        string itinerary4;
        string itinerary5;
        string itinerary6;
        string itinerary7;
        string itinerary8;
        string itinerary9;
        string itinerary10;
        string itinerary11;
        string itinerary12;
        string itinerary13;
        string notes;
        string priceExclude;
        string termsAndConditions;
        int paging;
        bool isActive;
        bool isDeleted;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        List<HolidayPackageSeason> hotelSeasonList;
        int agencyId;
        string roomRates;
        string tourType;

        //adding two more variable for save image path
        string mainImage1Path;
        string mainImage2Path;
        #endregion

        #region publicProperties
        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public string DealName
        {
            get
            {
                return dealName;
            }
            set
            {
                dealName = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public bool IsInternational
        {
            get
            {
                return isInternational;
            }
            set
            {
                isInternational = value;
            }
        }

        public int Nights
        {
            get
            {
                return nights;
            }
            set
            {
                nights = value;
            }
        }

        public string MainImagePath
        {
            get
            {
                return mainImagePath;
            }
            set
            {
                mainImagePath = value;
            }
        }

        public string ImagePath
        {
            get
            {
                return imagePath;
            }
            set
            {
                imagePath = value;
            }
        }

        public string ThumbnailPath
        {
            get
            {
                return thumbnailPath;
            }
            set
            {
                thumbnailPath = value;
            }
        }

        public bool HasAirfare
        {
            get
            {
                return hasAirfare;
            }
            set
            {
                hasAirfare = value;
            }
        }

        public bool HasHotel
        {
            get
            {
                return hasHotel;
            }
            set
            {
                hasHotel = value;
            }
        }

        public bool HasSightSeeing
        {
            get
            {
                return hasSightSeeing;
            }
            set
            {
                hasSightSeeing = value;
            }
        }

        public bool HasMeals
        {
            get
            {
                return hasMeals;
            }
            set
            {
                hasMeals = value;
            }
        }

        public bool HasTransport
        {
            get
            {
                return hasTransport;
            }
            set
            {
                hasTransport = value;
            }
        }

        public string Overview
        {
            get
            {
                return overview;
            }
            set
            {
                overview = value;
            }
        }

        public string Inclusions
        {
            get
            {
                return inclusions;
            }
            set
            {
                inclusions = value;
            }
        }

        public string Itinerary1
        {
            get
            {
                return itinerary1;
            }
            set
            {
                itinerary1 = value;
            }
        }

        public string Itinerary2
        {
            get
            {
                return itinerary2;
            }
            set
            {
                itinerary2 = value;
            }
        }

        public string Itinerary3
        {
            get
            {
                return itinerary3;
            }
            set
            {
                itinerary3 = value;
            }
        }

        public string Itinerary4
        {
            get
            {
                return itinerary4;
            }
            set
            {
                itinerary4 = value;
            }
        }

        public string Itinerary5
        {
            get
            {
                return itinerary5;
            }
            set
            {
                itinerary5 = value;
            }
        }

        public string Itinerary6
        {
            get
            {
                return itinerary6;
            }
            set
            {
                itinerary6 = value;
            }
        }

        public string Itinerary7
        {
            get
            {
                return itinerary7;
            }
            set
            {
                itinerary7 = value;
            }
        }

        public string Itinerary8
        {
            get
            {
                return itinerary8;
            }
            set
            {
                itinerary8 = value;
            }
        }

        public string Itinerary9
        {
            get
            {
                return itinerary9;
            }
            set
            {
                itinerary9 = value;
            }
        }

        public string Itinerary10
        {
            get
            {
                return itinerary10;
            }
            set
            {
                itinerary10 = value;
            }
        }

        public string Itinerary11
        {
            get
            {
                return itinerary11;
            }
            set
            {
                itinerary11 = value;
            }
        }

        public string Itinerary12
        {
            get { return itinerary12; }
            set { itinerary12 = value; }            
        }

        public string Itinerary13
        {
            get { return itinerary13; }
            set { itinerary13 = value; }
        }

        public string Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }

        public string PriceExclude
        {
            get
            {
                return priceExclude;
            }
            set
            {
                priceExclude = value;
            }
        }

        public string TermsAndConditions
        {
            get
            {
                return termsAndConditions;
            }
            set
            {
                termsAndConditions = value;
            }
        }

        public int Paging
        {
            get
            {
                return paging;
            }
            set
            {
                paging = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return isDeleted;
            }
            set
            {
                isDeleted = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public List<HolidayPackageSeason> HotelSeasonList
        {
            get
            {
                return hotelSeasonList;
            }
            set
            {
                hotelSeasonList = value;
            }
        }

        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }

        public string RoomRates
        {
            get { return roomRates; }
            set { roomRates = value; }
        }

        public string TourType
        {
            get { return tourType; }
            set { tourType = value; }
        }

        //added two more properties for get image name
        public string MainImage1Path
        {
            get
            {
                return mainImage1Path;
            }
            set
            {
                mainImage1Path = value;
            }
        }
        public string MainImage2Path
        {
            get
            {
                return mainImage2Path;
            }
            set
            {
                mainImage2Path = value;
            }
        }
        #endregion

        #region publicMethods
        /// <summary>
        /// Saves and updates hotel deal pakage.
        /// </summary>
        /// <returns>dealId when save and number of rows affected when update</returns>
        public int Save()
        {
            int dealIdReturnValue = 0;
            SqlParameter[] paramList;
            if (dealId == 0)
            {
                paramList = new SqlParameter[39];
            }
            else
            {
                paramList = new SqlParameter[34];
            }

            paramList[0] = new SqlParameter("@dealName", dealName);
            paramList[1] = new SqlParameter("@city", city);
            paramList[2] = new SqlParameter("@country", country);
            paramList[3] = new SqlParameter("@description", description);
            paramList[4] = new SqlParameter("@isInternational", isInternational);
            paramList[5] = new SqlParameter("@nights", nights);
            paramList[6] = new SqlParameter("@hasAirfare", hasAirfare);
            paramList[7] = new SqlParameter("@hasHotel", hasHotel);
            paramList[8] = new SqlParameter("@hasSightSeeing", hasSightSeeing);
            paramList[9] = new SqlParameter("@hasMeals", hasMeals);
            paramList[10] = new SqlParameter("@hasTransport", hasTransport);
            paramList[11] = new SqlParameter("@overview", overview);
            paramList[12] = new SqlParameter("@inclusions", inclusions);
            if (itinerary1 != null && itinerary1.Trim() != "")
            {
                paramList[13] = new SqlParameter("@itinerary1", itinerary1);
            }
            else
            {
                paramList[13] = new SqlParameter("@itinerary1", DBNull.Value);
            }
            if (itinerary2 != null && itinerary2.Trim() != "")
            {
                paramList[14] = new SqlParameter("@itinerary2", itinerary2);
            }
            else
            {
                paramList[14] = new SqlParameter("@itinerary2", DBNull.Value);
            }
            if (itinerary3 != null && itinerary3.Trim() != "")
            {
                paramList[15] = new SqlParameter("@itinerary3", itinerary3);
            }
            else
            {
                paramList[15] = new SqlParameter("@itinerary3", DBNull.Value);
            }
            if (itinerary4 != null && itinerary4.Trim() != "")
            {
                paramList[16] = new SqlParameter("@itinerary4", itinerary4);
            }
            else
            {
                paramList[16] = new SqlParameter("@itinerary4", DBNull.Value);
            }
            if (itinerary5 != null && itinerary5.Trim() != "")
            {
                paramList[17] = new SqlParameter("@itinerary5", itinerary5);
            }
            else
            {
                paramList[17] = new SqlParameter("@itinerary5", DBNull.Value);
            }
            if (itinerary6 != null && itinerary6.Trim() != "")
            {
                paramList[18] = new SqlParameter("@itinerary6", itinerary6);
            }
            else
            {
                paramList[18] = new SqlParameter("@itinerary6", DBNull.Value);
            }
            if (itinerary7 != null && itinerary7.Trim() != "")
            {
                paramList[19] = new SqlParameter("@itinerary7", itinerary7);
            }
            else
            {
                paramList[19] = new SqlParameter("@itinerary7", DBNull.Value);
            }
            if (itinerary8 != null && itinerary8.Trim() != "")
            {
                paramList[20] = new SqlParameter("@itinerary8", itinerary8);
            }
            else
            {
                paramList[20] = new SqlParameter("@itinerary8", DBNull.Value);
            }
            if (itinerary9 != null && itinerary9.Trim() != "")
            {
                paramList[21] = new SqlParameter("@itinerary9", itinerary9);
            }
            else
            {
                paramList[21] = new SqlParameter("@itinerary9", DBNull.Value);
            }
            if (itinerary10 != null && itinerary10.Trim() != "")
            {
                paramList[22] = new SqlParameter("@itinerary10", itinerary10);
            }
            else
            {
                paramList[22] = new SqlParameter("@itinerary10", DBNull.Value);
            }
            if (itinerary11 != null && itinerary11.Trim() != "")
            {
                paramList[23] = new SqlParameter("@itinerary11", itinerary11);
            }
            else
            {
                paramList[23] = new SqlParameter("@itinerary11", DBNull.Value);
            }
            if (notes != null && notes.Trim() != "")
            {
                paramList[24] = new SqlParameter("@notes", notes);
            }
            else
            {
                paramList[24] = new SqlParameter("@notes", DBNull.Value);
            }
            if (priceExclude != null && priceExclude.Trim() != "")
            {
                paramList[25] = new SqlParameter("@priceExclude", priceExclude);
            }
            else
            {
                paramList[25] = new SqlParameter("@priceExclude", DBNull.Value);
            }
            paramList[26] = new SqlParameter("@termsAndConditions", termsAndConditions);
            paramList[27] = new SqlParameter("@agencyId", agencyId);
            paramList[28] = new SqlParameter("@dealId", dealId);
            //Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "begin bo : " + dealId.ToString(), "");

            if (dealId == 0)
            {
                paramList[28].Direction = ParameterDirection.Output;
                paramList[29] = new SqlParameter("@createdBy", createdBy);
                if (mainImagePath != null && mainImagePath.Trim() != "")
                {
                    paramList[30] = new SqlParameter("@mainImagePath", mainImagePath);
                }
                else
                {
                    paramList[30] = new SqlParameter("@mainImagePath", DBNull.Value);
                }
                if (imagePath != null && imagePath.Trim() != "")
                {
                    paramList[31] = new SqlParameter("@imagePath", imagePath);
                }
                else
                {
                    paramList[31] = new SqlParameter("@imagePath", DBNull.Value);
                }
                if (thumbnailPath != null && thumbnailPath.Trim() != "")
                {
                    paramList[32] = new SqlParameter("@thumbnailPath", thumbnailPath);
                }
                else
                {
                    paramList[32] = new SqlParameter("@thumbnailPath", DBNull.Value);
                }
                paramList[33] = new SqlParameter("@roomRates",roomRates);
                paramList[34] = new SqlParameter("@tourType", tourType);

                if (itinerary12 != null && itinerary12.Trim() != "")
                {
                    paramList[35] = new SqlParameter("@itinerary12", itinerary12);
                }
                else
                {
                    paramList[35] = new SqlParameter("@itinerary12", DBNull.Value);
                }
                if (itinerary13 != null && itinerary12.Trim() != "")
                {
                    paramList[36] = new SqlParameter("@itinerary13", itinerary13);
                }
                else
                {
                    paramList[36] = new SqlParameter("@itinerary13", DBNull.Value);
                }

                //paramList[35] = new SqlParameter("@itinerary12", itinerary12);
                //paramList[36] = new SqlParameter("@itinerary13", itinerary13);

                //Adding two more main images on  02032016
                if (mainImage1Path != null && mainImage1Path.Trim() != "")
                {
                    paramList[37] = new SqlParameter("@mainImage1Path", mainImage1Path);
                }
                else
                {
                    paramList[37] = new SqlParameter("@mainImage1Path", DBNull.Value);
                }
                if (mainImage2Path != null && mainImage2Path.Trim() != "")
                {
                    paramList[38] = new SqlParameter("@mainImage2Path", mainImage2Path);
                }
                else
                {
                    paramList[38] = new SqlParameter("@mainImage2Path", DBNull.Value);
                }
                try
                {
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        try
                        {
                            // For Temp
                            for (int i = 0; i < paramList.Length; i++)
                            {
                                if (paramList[i].Value != null)
                                {
                                    Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Not NULL value is :" + paramList[i].Value, "");
                                }
                                else
                                    Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "NULL value :" + paramList[i].ToString(), "");
                            }
                            //  end temp

                            //Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: 101" , "");
                            DBGateway.ExecuteNonQuerySP(SPNames.HolidayPackageSave, paramList);
                            //Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: 102", "");
                        }
                        catch (Exception exM)
                        {
                            Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: HolidayPackageSave| " + exM.ToString(), "");
                        }
                        try
                        {
                            for (int i = 0; i < hotelSeasonList.Count; i++)
                            {
                                hotelSeasonList[i].DealId = (int)paramList[28].Value;
                                hotelSeasonList[i].Save();
                            }
                        }
                        catch (Exception ex1)
                        {
                            Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Holiday Error 1| " + ex1.ToString() + " | "  , "");
                         
                        }
                        updateTransaction.Complete();
                        dealIdReturnValue = (int)paramList[28].Value;
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Holiday Error 2| " + ex.ToString()+ " | " , "");
                   Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during Save() in HolidayPackage.cs | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    dealIdReturnValue = 0;
                }
            }
            else
            {
                paramList[29] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramList[30] = new SqlParameter("@roomRates", roomRates);
                paramList[31] = new SqlParameter("@tourType", tourType);
                paramList[32] = new SqlParameter("@itinerary12", itinerary12);
                paramList[33] = new SqlParameter("@itinerary13", itinerary13);
                try
                {
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        dealIdReturnValue = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHolidayPackage, paramList);
                        for (int i = 0; i < hotelSeasonList.Count; i++)
                        {
                            hotelSeasonList[i].DealId = dealId;
                            hotelSeasonList[i].Save();
                        }
                        updateTransaction.Complete();
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Exception during update in Save() in HolidayPackage.cs for dealId=" + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    dealIdReturnValue = 0;
                }
            }
            //return dealId;
            return dealIdReturnValue;
        }

        /// <summary>
        /// updates image path in HolidayPackage table
        /// </summary>
        /// <param name="mainImagePath">Main image path</param>
        /// <param name="imagePath">Side image path</param>
        /// <param name="thumbnailPath">Thumbnail image path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        public static int UpdateImagePath(string mainImagePath, string imagePath, string thumbnailPath, int hotelDealId)// check to use...
        {
            int rowsAffected = 0;
            if (mainImagePath == null || mainImagePath.Trim().Length <= 0)
            {
                throw new ArgumentException("mainImagePath should not be null or empty string", "mainImagePath");
            }
            if (imagePath == null || imagePath.Trim().Length <= 0)
            {
                throw new ArgumentException("imagePath should not be null or empty string", "imagePath");
            }
            if (thumbnailPath == null || thumbnailPath.Trim().Length <= 0)
            {
                throw new ArgumentException("thumbnailPath should not be null or empty string", "thumbnailPath");
            }
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@mainImagePath", mainImagePath);
            paramList[1] = new SqlParameter("@imagePath", imagePath);
            paramList[2] = new SqlParameter("@thumbnailPath", thumbnailPath);
            paramList[3] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateImagePath, paramList);
            }
            catch (Exception ex)
            {
               Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during UpdateImagePath() in HolidayPackage.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// updates image path in HolidayPackage table
        /// </summary>
        /// <param name="mainImagePath">Main image path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        public static int UpdateMainImagePath(string mainImagePath, int hotelDealId)
        {
            int rowsAffected = 0;
            if (mainImagePath == null || mainImagePath.Trim().Length <= 0)
            {
                throw new ArgumentException("mainImagePath should not be null or empty string", "mainImagePath");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@mainImagePath", mainImagePath);
            paramList[1] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateMainImagePath, paramList);
            }
            catch (Exception ex)
            {
               Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during UpdateImagePath() in HolidayPackage.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// updates image path in HolidayPackage table
        /// </summary>
        /// <param name="mainImagePath">Main image path</param>
        /// <param name="imagePath">Side image path</param>
        /// <param name="thumbnailPath">Thumbnail image path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        public static int UpdateSideImagePath(string imagePath, int hotelDealId)
        {
            int rowsAffected = 0;
            if (imagePath == null || imagePath.Trim().Length <= 0)
            {
                throw new ArgumentException("imagePath should not be null or empty string", "imagePath");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@imagePath", imagePath);
            paramList[1] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateSideImagePath, paramList);
            }
            catch (Exception ex)
            {
               Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during UpdateImagePath() in HolidayPackage.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// updates image path in HolidayPackage table
        /// </summary>
        /// <param name="thumbnailPath">Thumbnail image path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        public static int UpdateThumbnailImagePath(string thumbnailPath, int hotelDealId)
        {
            int rowsAffected = 0;
            if (thumbnailPath == null || thumbnailPath.Trim().Length <= 0)
            {
                throw new ArgumentException("thumbnailPath should not be null or empty string", "thumbnailPath");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@thumbnailPath", thumbnailPath);
            paramList[1] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateThumbnailImagePath, paramList);
            }
            catch (Exception ex)
            {
               Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during UpdateImagePath() in HolidayPackage.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }


        /// <summary>
        /// updates MainImage1 path in HolidayPackage table
        /// </summary>
        /// <param name="mainImage1Path">Main image1 path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        /// <addedOn>02032016</addedOn>
        /// <addedBy>Chandan</addedBy>
        public static int UpdateMainImage1Path(string mainImage1Path, int hotelDealId)
        {
            int rowsAffected = 0;
            if (mainImage1Path == null || mainImage1Path.Trim().Length <= 0)
            {
                throw new ArgumentException("mainImage1Path should not be null or empty string", "mainImage1Path");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@mainImage1Path", mainImage1Path);
            paramList[1] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateMainImage1Path, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during UpdateImage1Path() in HolidayPackage.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// updates MainImage2 path in HolidayPackage table
        /// </summary>
        /// <param name="mainImage2Path">Main image2 path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        /// <addedOn>02032016</addedOn>
        /// <addedBy>Chandan</addedBy>
        public static int UpdateMainImage2Path(string mainImage2Path, int hotelDealId)
        {
            int rowsAffected = 0;
            if (mainImage2Path == null || mainImage2Path.Trim().Length <= 0)
            {
                throw new ArgumentException("mainImage2Path should not be null or empty string", "mainImage2Path");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@mainImage2Path", mainImage2Path);
            paramList[1] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateMainImage2Path, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during UpdateImage2Path() in HolidayPackage.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }



        /// <summary>
        /// Loads a specific hotel deal
        /// </summary>
        /// <param name="dealId">deal id to be loaded</param>
        /// <returns>HolidayPackage object</returns>
        public static HolidayPackage LoadHotelDealForEdit(int dealId)
        {
            SqlDataReader data = null;
            HolidayPackage htl = new HolidayPackage();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@dealId", dealId);
                try
                {
                    data = DBGateway.ExecuteReaderSP(SPNames.GetHolidayPackageDealForEdit, paramList, connection);
                    if (data.Read())
                    {
                        htl.dealId = Convert.ToInt32(data["dealId"]);
                        htl.agencyId = Convert.ToInt32(data["agencyId"]);
                        htl.dealName = Convert.ToString(data["dealName"]);
                        htl.city = Convert.ToString(data["city"]);
                        htl.country = Convert.ToString(data["country"]);
                        htl.description = Convert.ToString(data["description"]);
                        htl.isInternational = Convert.ToBoolean(data["isInternational"]);
                        htl.nights = Convert.ToInt32(data["nights"]);
                        htl.mainImagePath = Convert.ToString(data["mainImagePath"]);
                        htl.imagePath = Convert.ToString(data["imagePath"]);
                        htl.thumbnailPath = Convert.ToString(data["thumbnailPath"]);
                        htl.hasAirfare = Convert.ToBoolean(data["hasAirfare"]);
                        htl.hasHotel = Convert.ToBoolean(data["hasHotel"]);
                        htl.hasSightSeeing = Convert.ToBoolean(data["hasSightSeeing"]);
                        htl.hasMeals = Convert.ToBoolean(data["hasMeals"]);
                        htl.hasTransport = Convert.ToBoolean(data["hasTransport"]);
                        htl.overview = Convert.ToString(data["overview"]);
                        htl.inclusions = Convert.ToString(data["inclusions"]);
                        htl.RoomRates = data["roomRates"].ToString();
                        htl.tourType = data["tourType"].ToString();

                        //getting MainImage1 and MainImage2 file name on 02032016
                        htl.mainImage1Path = Convert.ToString(data["mainImagePath1"]);
                        htl.mainImage2Path = Convert.ToString(data["mainImagePath2"]);

                        if (data["itinerary1"] != DBNull.Value)
                        {
                            htl.itinerary1 = Convert.ToString(data["itinerary1"]);
                        }
                        else
                        {
                            htl.itinerary1 = string.Empty;
                        }
                        if (data["itinerary2"] != DBNull.Value)
                        {
                            htl.itinerary2 = Convert.ToString(data["itinerary2"]);
                        }
                        else
                        {
                            htl.itinerary2 = string.Empty;
                        }
                        if (data["itinerary3"] != DBNull.Value)
                        {
                            htl.itinerary3 = Convert.ToString(data["itinerary3"]);
                        }
                        else
                        {
                            htl.itinerary3 = string.Empty;
                        }
                        if (data["itinerary4"] != DBNull.Value)
                        {
                            htl.itinerary4 = Convert.ToString(data["itinerary4"]);
                        }
                        else
                        {
                            htl.itinerary4 = string.Empty;
                        }
                        if (data["itinerary5"] != DBNull.Value)
                        {
                            htl.itinerary5 = Convert.ToString(data["itinerary5"]);
                        }
                        else
                        {
                            htl.itinerary5 = string.Empty;
                        }
                        if (data["itinerary6"] != DBNull.Value)
                        {
                            htl.itinerary6 = Convert.ToString(data["itinerary6"]);
                        }
                        else
                        {
                            htl.itinerary6 = string.Empty;
                        }
                        if (data["itinerary7"] != DBNull.Value)
                        {
                            htl.itinerary7 = Convert.ToString(data["itinerary7"]);
                        }
                        else
                        {
                            htl.itinerary7 = string.Empty;
                        }
                        if (data["itinerary8"] != DBNull.Value)
                        {
                            htl.itinerary8 = Convert.ToString(data["itinerary8"]);
                        }
                        else
                        {
                            htl.itinerary8 = string.Empty;
                        }
                        if (data["itinerary9"] != DBNull.Value)
                        {
                            htl.itinerary9 = Convert.ToString(data["itinerary9"]);
                        }
                        else
                        {
                            htl.itinerary9 = string.Empty;
                        }
                        if (data["itinerary10"] != DBNull.Value)
                        {
                            htl.itinerary10 = Convert.ToString(data["itinerary10"]);
                        }
                        else
                        {
                            htl.itinerary10 = string.Empty;
                        }
                        if (data["itinerary11"] != DBNull.Value)
                        {
                            htl.itinerary11 = Convert.ToString(data["itinerary11"]);
                        }
                        else
                        {
                            htl.itinerary11 = string.Empty;
                        }
                        if (data["itinerary12"] != DBNull.Value)
                        {
                            htl.itinerary12 = Convert.ToString(data["itinerary12"]);
                        }
                        else
                        {
                            htl.itinerary12 = string.Empty;
                        }
                        if (data["itinerary13"] != DBNull.Value)
                        {
                            htl.itinerary13 = Convert.ToString(data["itinerary13"]);
                        }
                        else
                        {
                            htl.itinerary13 = string.Empty;
                        }
                        if (data["notes"] != DBNull.Value)
                        {
                            htl.notes = Convert.ToString(data["notes"]);
                        }
                        else
                        {
                            htl.notes = string.Empty;
                        }
                        if (data["priceExclude"] != DBNull.Value)
                        {
                            htl.priceExclude = Convert.ToString(data["priceExclude"]);
                        }
                        else
                        {
                            htl.priceExclude = string.Empty;
                        }
                        htl.termsAndConditions = Convert.ToString(data["termsAndConditions"]);
                        htl.paging = Convert.ToInt32(data["paging"]);
                        htl.isActive = Convert.ToBoolean(data["dealId"]);
                    }
                    if (htl.dealId > 0)
                    {
                        htl.hotelSeasonList = HolidayPackageSeason.LoadhotelSeasons(htl.dealId);
                    }
                }
                catch (Exception ex)
                {
                   Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during LoadHotelDeal() in HolidayPackage.cs for dealId=" + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load hotel for deal id: " + dealId);
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return htl;
        }

        /// <summary>
        /// Loads list of deals for priority, paging etc of deals for India times 
        /// </summary>
        /// <param name="whereString">where string</param>
        /// <returns>List of HolidayPackage</returns>
        public static List<HolidayPackage> LoadForPageSetting(string whereString, int agencyId)
        {
            SqlDataReader data = null;
            List<HolidayPackage> tempList = new List<HolidayPackage>();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@whereString", whereString);
                paramList[1] = new SqlParameter("@agencyId", agencyId);
                try
                {
                    data = DBGateway.ExecuteReaderSP(SPNames.GetAllHolidayPackageDealsUsingWhereString, paramList, connection);
                    while (data.Read())
                    {
                        HolidayPackage htl = new HolidayPackage();
                        htl.dealId = Convert.ToInt32(data["dealId"]);
                        htl.dealName = Convert.ToString(data["dealName"]);
                        htl.city = Convert.ToString(data["city"]);
                        htl.country = Convert.ToString(data["country"]);
                        htl.nights = Convert.ToInt32(data["nights"]);
                        htl.IsInternational = Convert.ToBoolean(data["isInternational"]);
                        htl.paging = Convert.ToInt32(data["paging"]);
                        htl.isActive = Convert.ToBoolean(data["isActive"]);
                        tempList.Add(htl);
                    }
                }
                catch (Exception ex)
                {
                   Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during LoadForPageSetting() in HolidayPackage.cs for whereString= " + whereString + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load hotel for whereString: " + whereString);
                }
                finally
                {
                    data.Close();
                    connection.Close();
                }
            }
            return tempList;
        }

        /// <summary>
        /// Generates where string
        /// </summary>
        /// <param name="active">bool active</param>
        /// <param name="inactive">bool inactive</param>
        /// <returns>where string</returns>
        public static string GenerateWhereString(bool active, bool inactive)
        {
            string whereString = string.Empty;
            if (active && inactive)
            {
                whereString = "where";
            }
            else
            {
                if (active)
                {
                    whereString = "where isActive = 1 and";
                }
                else
                {
                    whereString = "where isActive = 0 and";
                }
            }
            return whereString;
        }

        /// <summary>
        /// updates all deals for changes made in them on hotel deal setting page.
        /// </summary>
        /// <param name="whereString">where string</param>
        /// <param name="priorityList">priority List</param>
        /// <param name="pagingList">paging List</param>
        /// <param name="leftDeal1List">left panel Deal List</param>
        /// <param name="leftDeal2List">left panel Deal List</param>
        /// <returns>DataTable of HolidayPackage</returns>
        public static DataTable UpdatePackageSettings(string whereString, List<int> priorityList, List<int> pagingList, List<int> leftDeal1List, List<int> leftDeal2List, int agencyId)
        {
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@whereString", whereString);
                paramList[1] = new SqlParameter("@agencyId", agencyId);
                try
                {
                    SqlCommand command = new SqlCommand(SPNames.GetAllHolidayPackageDealsUsingWhereString, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 60;
                    command.Parameters.AddRange(paramList);
                    adapter = new SqlDataAdapter(command);
                    int rowsAdded = adapter.Fill(dataTable);
                    //DataTable dt = DBGateway.FillDataTableSP(SPNames.GetAllIndiaTimesHotelDeals, paramList);
                    for (int i = 0; i < priorityList.Count; i++)
                    {
                        dataTable.Rows[i]["priority"] = priorityList[i];
                        dataTable.Rows[i]["paging"] = pagingList[i];
                        dataTable.Rows[i]["leftDeal1"] = leftDeal1List[i];
                        dataTable.Rows[i]["leftDeal2"] = leftDeal2List[i];
                    }
                    SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);
                    adapter.Update(dataTable);
                }
                catch (Exception ex)
                {
                   Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during UpdatePackageSettings() in HolidayPackage.cs Unable to update deals setting for home page and paging on India Times. | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to update deals setting for home page and paging on India Times.");
                }
                finally
                {
                    connection.Close();
                    adapter.Dispose();
                }
            }
            return dataTable;
        }

        /// <summary>
        /// Updates a hotel deal status
        /// </summary>
        /// <param name="dealId">deal id</param>
        /// <param name="status">status</param>
        /// <param name="leftDeal1">left panel Deal</param>
        /// <param name="leftDeal2">left panel Deal</param>
        /// <returns>no of rows updated</returns>
        public static int UpdateHotelDealStatus(int dealId, bool status, int leftDeal1, int leftDeal2)
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@dealId", dealId);
            paramList[1] = new SqlParameter("@status", status);
            paramList[2] = new SqlParameter("@leftDeal1", leftDeal1);
            paramList[3] = new SqlParameter("@leftDeal2", leftDeal2);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHolidayPackageDealStatus, paramList);
            }
            catch (Exception ex)
            {
               Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during UpdateHotelDealStatus() in HolidayPackage.cs for dealId = " + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// Deletes a deal.
        /// </summary>
        /// <param name="dealId">deal Id</param>
        /// <returns>no of rows affected</returns>
        public static int DeleteHotelDealStatus(int dealId)
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", dealId);
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeleteHolidayPackageDeal, paramList);
            }
            catch (Exception ex)
            {
               Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during DeleteHotelDealStatus() in HolidayPackage.cs for dealId = " + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// chechs if deal to be removed is not mapped with other deals.
        /// </summary>
        /// <param name="dealId">deal Id</param>
        /// <returns>List of HolidayPackage deals with which deal is mapped</returns>
        public static List<HolidayPackage> CheckDealIdMapping(int dealId)
        {
            SqlDataReader data = null;
            List<HolidayPackage> tempList = new List<HolidayPackage>();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@dealId", dealId);
                try
                {
                    data = DBGateway.ExecuteReaderSP(SPNames.GetMappedHotelDealIDs, paramList, connection);
                    while (data.Read())
                    {
                        HolidayPackage htl = new HolidayPackage();
                        htl.dealId = Convert.ToInt32(data["dealId"]);
                        htl.dealName = Convert.ToString(data["dealName"]);
                        htl.nights = Convert.ToInt32(data["nights"]);
                        tempList.Add(htl);
                    }
                }
                catch (Exception ex)
                {
                   Audit.Add(CT.Core.EventType.HotelCMS, Severity.High, 0, "Error: Exception during CheckDealIdMapping() in HolidayPackage.cs for dealId = " + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to check mapping for deal id:" + dealId);
                }
                finally
                {
                    data.Close();
                    connection.Close();
                }
            }
            return tempList;
        }

        public static DataTable GetAgencyList()
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = CT.TicketReceipt.DataAccessLayer.DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[0];
                    data = DBGateway.ExecuteReaderSP(SPNames.GetAgencyList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public static DataTable GetPackageList(int agentId)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = CT.TicketReceipt.DataAccessLayer.DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@AgencyId", agentId);
                    data = DBGateway.ExecuteReaderSP(SPNames.GetPackageList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        public static string GetAgencyImagePath(int agencyId, string imageType)
        { 
           
                SqlDataReader data = null;
                string imagePath = string.Empty;
                using (SqlConnection conn = CT.TicketReceipt.DataAccessLayer.DBGateway.GetConnection())
                {
                    SqlParameter[] paramList;
                    try
                    {
                        paramList = new SqlParameter[2];
                        paramList[0] = new SqlParameter("@agencyId", agencyId);
                        paramList[1] = new SqlParameter("@imagetype", imageType);
                        data = DBGateway.ExecuteReaderSP(SPNames.GetPackageImagePath, paramList, conn);
                        if (data != null)
                        {
                            while (data.Read())
                            {
                                //imagePath = Convert.ToString(data["PackageImagePath"]);
                                imagePath = Convert.ToString(data["agent_img_path"]); // changed by chandan on 15/10/2015
                            }
                        }
                        conn.Close();
                    }

                    catch { }
                }

                return imagePath;
           
        }

        //public static DataTable LoadPackage()
        //{
        //    SqlDataReader data = null;
        //    DataTable dt = new DataTable();
        //    using (SqlConnection connection = Technology.Data.DBGateway.GetConnection())
        //    {
        //        SqlParameter[] paramList;
        //        try
        //        {
        //            paramList = new SqlParameter[0];
        //            data = Technology.Data.DBGateway.ExecuteReaderSP(SPNames1.LoadHoliday, paramList, connection);
        //            if (data != null)
        //            {
        //                dt.Load(data);
        //            }
        //            connection.Close();
        //        }
        //        catch
        //        {
        //            throw;
        //        }
        //    }
        //    return dt;
        //}
        #endregion
    }
}
