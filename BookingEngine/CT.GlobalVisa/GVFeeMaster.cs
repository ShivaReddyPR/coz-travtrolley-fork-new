using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.BusinessLayer;

namespace CT.GlobalVisa
{

    public class GVFeeMaster
    {
      private const long NEW_RECORD = -1;
        
    
        #region Member Variables
        private long _transactionid;       
        private string _code;
        private string _name;        
        private string _countryId;
        private string _nationalityId;
        private long _visaTypeId;
        private string _residenceId;
        
        
        private string _visaCategory;
        private long _centerId;
        private string _presenceStatus;
        private decimal _adultVisaFee;
        private decimal _childVisaFee;
        private decimal _infantVisaFee;
        private decimal _adultService;
        private decimal _childService;
        private decimal _infantService;

        private decimal _adultUrgentFee;
        private decimal _childUrgentFee;
        private decimal _infantUrgentFee;
        private decimal _adultAdd1Fee;
        private decimal _childAdd1Fee;
        private decimal _infantAdd1Fee;
        private decimal _adultAdd2Fee;
        private decimal _childAdd2Fee;
        private decimal _infantAdd2Fee;
        private decimal _adultAdd3Fee;
        private decimal _childAdd3Fee;
        private decimal _infantAdd3Fee;
        
        private long _retOBVId;
        //private string _MsgType;
        //private string _MsgText;
        

        private string _status;                
        private long _createdBy;
        private DateTime _createdOn;
        //Added by venkatesh 30/11/2017
        private int _processingDays;
        //Added by brahmam
        private decimal _totalChargeAdult;
        private decimal _totalChargeChild;
        private decimal _totalChargeInfant;
        #endregion


        #region Properties

        public long TransactionId
        {
            get { return _transactionid ; }
            set { _transactionid = value; }
        }

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
               
        public string CountryID
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        public string NationalityId
        {
            get { return _nationalityId ; }
            set { _nationalityId  = value; }
        }
        public long VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId = value; }
        }

        public string ResidenceId
        {
            get { return _residenceId; }
            set { _residenceId = value; }
        }
        
        public long CenterID
        {
            get { return _centerId ; }
            set { _centerId = value; }
        }
        public string  VisaCategory
        {
            get { return _visaCategory; }
            set { _visaCategory = value; }
        }

        public string PresenceStatus
        {
            get { return _presenceStatus; }
            set { _presenceStatus = value; }
        }

        public decimal  AdultVisaFee
        {
            get { return _adultVisaFee ; }
            set { _adultVisaFee  = value; }
        }
        public decimal ChildVisaFee
        {
            get { return _childVisaFee ; }
            set { _childVisaFee = value; }
        }
        public decimal InfantVisaFee
        {
            get { return _infantVisaFee; }
            set { _infantVisaFee  = value; }
        }
        public decimal AdultService
        {
            get { return _adultService; }
            set { _adultService = value; }
        }
        public decimal   ChildService
        {
            get { return _childService ; }
            set { _childService = value; }
        }

        public decimal InfantService
        {
            get { return _infantService ; }
            set { _infantService  = value; }
        }   
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }
        public decimal AdultUrgentFee
        {
            get { return _adultUrgentFee; }
            set { _adultUrgentFee = value; }
        }

        public decimal ChildUrgentFee
        {
            get { return _childUrgentFee; }
            set { _childUrgentFee = value; }
        }

        public decimal InfantUrgentFee
        {
            get { return _infantUrgentFee; }
            set { _infantUrgentFee = value; }
        }

        public decimal AdultAdd1Fee
        {
            get { return _adultAdd1Fee; }
            set { _adultAdd1Fee = value; }
        }

        public decimal ChildAdd1Fee
        {
            get { return _childAdd1Fee; }
            set { _childAdd1Fee = value; }
        }

        public decimal InfantAdd1Fee
        {
            get { return _infantAdd1Fee; }
            set { _infantAdd1Fee = value; }
        }


        public decimal AdultAdd2Fee
        {
            get { return _adultAdd2Fee; }
            set { _adultAdd2Fee = value; }
        }

        public decimal ChildAdd2Fee
        {
            get { return _childAdd2Fee; }
            set { _childAdd2Fee = value; }
        }

        public decimal InfantAdd2Fee
        {
            get { return _infantAdd2Fee; }
            set { _infantAdd2Fee = value; }
        }

        public decimal AdultAdd3Fee
        {
            get { return _adultAdd3Fee; }
            set { _adultAdd3Fee = value; }
        }
        public decimal ChildAdd3Fee
        {
            get { return _childAdd3Fee; }
            set { _childAdd3Fee = value; }
        }
        public decimal InfantAdd3Fee
        {
            get { return _infantAdd3Fee; }
            set { _infantAdd3Fee = value; }
        }
        
        public long retOBVId
        {
            get { return _retOBVId; }
            set { _retOBVId = value; }
        }
        //Added by venkatesh 30/11/2017
        public int ProcessingDays
        {
            get { return _processingDays; }
            set { _processingDays = value; }
        }

        //Added by brahmam
        public decimal TotalChargeAdult
        {
            get
            {
                return _totalChargeAdult;
            }

            set
            {
                _totalChargeAdult = value;
            }
        }

        public decimal TotalChargeChild
        {
            get
            {
                return _totalChargeChild;
            }

            set
            {
                _totalChargeChild = value;
            }
        }

        public decimal TotalChargeInfant
        {
            get
            {
                return _totalChargeInfant;
            }

            set
            {
                _totalChargeInfant = value;
            }
        }
        #endregion


        #region Constructors
        public GVFeeMaster()
        {
            _transactionid  = NEW_RECORD;
            //Commented by venkatesh
            //DataSet ds = GetData(_transactionid);
            //UpdateBusinessData(ds);
        }
        public GVFeeMaster(long id)
        {
            _transactionid  = id;
            getDetails(_transactionid);
        }


        #endregion

        #region Methods

        private void getDetails(long id)
        {
            try
            {
                DataSet ds = GetData(id);
                //if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
                //{
                UpdateBusinessData(ds);
                //}
            }
            catch { throw; }
        }

        private DataSet GetData(long id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CHARGE_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("GV_p_visa_fee_getData", paramList);
                return dsResult;
            }
            catch { throw; }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (_transactionid < 0)
                {
                    _transactionid = Utility.ToLong(dr["charge_id"]);
                }
                else
                {
                    _transactionid = Utility.ToLong(dr["charge_id"]);
                    _code = Utility.ToString(dr["charge_code"]);
                    _countryId = Utility.ToString(dr["charge_country_id"]);
                    _nationalityId = Utility.ToString(dr["charge_nationality_id"]);

                    _visaTypeId = Utility.ToLong(dr["charge_visa_type_id"]);
                    _residenceId = Utility.ToString(dr["charge_residence_id"]);
                    //_centerId = Utility.ToLong(dr["charge_center_id"]);
                    
                    _visaCategory = Utility.ToString(dr["charge_visa_category"]);
                    _adultVisaFee = Utility.ToDecimal(dr["charge_adult"]);
                    _childVisaFee = Utility.ToDecimal(dr["charge_child"]);
                    _infantVisaFee = Utility.ToDecimal(dr["charge_infant"]);
                    _adultService = Utility.ToDecimal(dr["charge_acs_adult"]);
                    _childService = Utility.ToDecimal(dr["charge_acs_child"]);
                    _infantService = Utility.ToDecimal(dr["charge_acs_infant"]);

                    _adultUrgentFee = Utility.ToDecimal(dr["CHARGE_URG_ADULT"]);
                    _childUrgentFee = Utility.ToDecimal(dr["CHARGE_URG_CHILD"]);
                    _infantUrgentFee = Utility.ToDecimal(dr["CHARGE_URG_INFANT"]);
                    _adultAdd1Fee = Utility.ToDecimal(dr["CHARGE_ADD1_ADULT"]);
                    _childAdd1Fee = Utility.ToDecimal(dr["CHARGE_ADD1_CHILD"]);
                    _infantAdd1Fee = Utility.ToDecimal(dr["CHARGE_ADD1_INFANT"]);
                    _adultAdd2Fee = Utility.ToDecimal(dr["CHARGE_ADD2_ADULT"]);
                    _childAdd2Fee = Utility.ToDecimal(dr["CHARGE_ADD2_CHILD"]);
                    _infantAdd2Fee = Utility.ToDecimal(dr["CHARGE_ADD2_INFANT"]);
                    _adultAdd3Fee = Utility.ToDecimal(dr["CHARGE_ADD3_ADULT"]);
                    _childAdd3Fee = Utility.ToDecimal(dr["CHARGE_ADD3_CHILD"]);
                    _infantAdd3Fee = Utility.ToDecimal(dr["CHARGE_ADD3_INFANT"]);

                    //_presenceStatus = Utility.ToString(dr["charge_presence_status"]);
                    _status = Utility.ToString(dr["charge_status"]);
                    _createdBy = Utility.ToLong(dr["charge_created_by"]);
                    _createdOn = Utility.ToDate(dr["charge_created_on"]);
                    //Added by venkatesh 30/11/2017
                    _processingDays = Utility.ToInteger(dr["charge_process_days"]);
                }
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    UpdateBusinessData(ds.Tables[0].Rows[0]);
                    //_dtOBDetails = ds.Tables[1];
                }
            }
            catch { throw; }
        }
       
        #endregion

        #region Public Methods
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                SqlParameter[] paramList = new SqlParameter[31];

                paramList[0] = new SqlParameter("@P_charge_ID", _transactionid);
                paramList[1] = new SqlParameter("@P_charge_CODE", _code);               
                paramList[2] = new SqlParameter("@P_charge_COUNTRY_ID", _countryId);
                if(_nationalityId != "0") paramList[3] = new SqlParameter("@P_charge_NATIONALITY_ID", _nationalityId);
                if (_visaTypeId != 0) paramList[4] = new SqlParameter("@P_charge_VISA_TYPE_ID", _visaTypeId);
                if(_residenceId != "0") paramList[5] = new SqlParameter("@P_charge_RESIDENCE_ID", _residenceId);
                if (_visaCategory != "0") paramList[6] = new SqlParameter("@P_CHARGE_VISA_CATEGORY", _visaCategory);
                //paramList[7] = new SqlParameter("@P_CHARGE_PRESENCE_STATUS", _presenceStatus );
                //paramList[8] = new SqlParameter("@P_CHARGE_CENTER_ID", _centerId );
                paramList[7] = new SqlParameter("@P_CHARGE_ACS_ADULT", _adultService);               
                paramList[8] = new SqlParameter("@P_CHARGE_ACS_CHILD", _childService );
                paramList[9] = new SqlParameter("@P_CHARGE_ACS_INFANT", _infantService );
                paramList[10] = new SqlParameter("@P_CHARGE_ADULT", _adultVisaFee);
                paramList[11] = new SqlParameter("@P_CHARGE_CHILD", _childVisaFee);
                paramList[12] = new SqlParameter("@P_CHARGE_INFANT", _infantVisaFee);

                paramList[13] = new SqlParameter("@P_CHARGE_URG_ADULT", _adultUrgentFee);
                paramList[14] = new SqlParameter("@P_CHARGE_URG_CHILD", _childUrgentFee);
                paramList[15] = new SqlParameter("@P_CHARGE_URG_INFANT", _infantUrgentFee);
                paramList[16] = new SqlParameter("@P_CHARGE_ADD1_ADULT", _adultAdd1Fee);
                paramList[17] = new SqlParameter("@P_CHARGE_ADD1_CHILD", _childAdd1Fee);
                paramList[18] = new SqlParameter("@P_CHARGE_ADD1_INFANT", _infantAdd1Fee);
                paramList[19] = new SqlParameter("@P_CHARGE_ADD2_ADULT", _adultAdd2Fee);
                paramList[20] = new SqlParameter("@P_CHARGE_ADD2_CHILD", _childAdd2Fee);
                paramList[21] = new SqlParameter("@P_CHARGE_ADD2_INFANT", _infantAdd2Fee);
                paramList[22] = new SqlParameter("@P_charge_add3_adult", _adultAdd3Fee);
                paramList[23] = new SqlParameter("@P_charge_add3_child", _childAdd3Fee);
                paramList[24] = new SqlParameter("@P_charge_add3_infant", _infantAdd3Fee);
                paramList[25] = new SqlParameter("@P_charge_STATUS", _status);
                paramList[26] = new SqlParameter("@P_charge_CREATED_BY", _createdBy);
                //paramList[27] = new SqlParameter("@P_CHARGE_RES_CITY", _CityId);
                
                
                paramList[27] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[27].Direction = ParameterDirection.Output;
                paramList[28] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);                
                paramList[28].Direction = ParameterDirection.Output;

                paramList[29] = new SqlParameter("@P_CHARGE_ID_RETURN", SqlDbType.BigInt);
                paramList[29].Direction = ParameterDirection.Output;
                paramList[30] = new SqlParameter("@P_Charge_Process_Days", _processingDays);

                DBGateway.ExecuteNonQuery("GV_p_visa_fee_add_update", paramList);
                string messageType = Utility.ToString(paramList[28].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[29].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _retOBVId = Utility.ToLong(paramList[30].Value);
                //if (_dtOBDetails != null)
                //{
                //    DataTable dt = _dtOBDetails.GetChanges();
                //    int recordstatus = 0;
                //    if (dt != null && dt.Rows.Count > 0)
                //    {
                //        foreach (DataRow dr in dt.Rows)
                //        {
                //            switch (dr.RowState)
                //            {
                //                case DataRowState.Added: recordstatus = 1;
                //                    break;
                //                case DataRowState.Modified: recordstatus = 2;
                //                    break;
                //                case DataRowState.Deleted: recordstatus = -1;
                //                    dr.RejectChanges();
                //                    break;
                //                default: break;
                //            }
                //            SaveOBVisaDetails(cmd, recordstatus, _retOBVId, Utility.ToLong(dr["adsvc_id"]), Utility.ToString(dr["adsvc_code"]), Utility.ToDecimal(dr["adsvc_adult_amount"]), Utility.ToDecimal(dr["adsvc_child_amount"]), Utility.ToDecimal(dr["adsvc_infant_amount"]), Utility.ToString(dr["adsvc_status"]), Utility.ToInteger(dr["adsvc_created_by"]));
                //        }
                //    }
                //}
                trans.Commit();
            }
            catch(Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void SaveOBVisaDetails(SqlCommand cmd,int recordstatus,long headerId,long detailId,string feeCode,decimal adults,decimal child,decimal infants,string status,int createdBy)
        {
            try
            {
                SqlParameter[] paramArr=new SqlParameter[11];
                paramArr[0] = new SqlParameter("@P_adsvc_id", detailId);
                paramArr[1] = new SqlParameter("@P_adsvc_visafee_id", headerId);
                paramArr[2] = new SqlParameter("@P_adsvc_code", feeCode);
                paramArr[3] = new SqlParameter("@P_adsvc_adult_amount", adults);
                paramArr[4] = new SqlParameter("@P_adsvc_child_amount", child);
                paramArr[5] = new SqlParameter("@P_adsvc_infant_amount", infants);
                paramArr[6] = new SqlParameter("@P_record_status", recordstatus);
                paramArr[7] = new SqlParameter("@P_adsvc_created_by", createdBy);
                paramArr[8] = new SqlParameter("@P_adsvc_status", status);

                SqlParameter paramMsgType = new SqlParameter("@P_adsvc_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[9] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_adsvc_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[10] = paramMsgText;

                DBGateway.ExecuteNonQueryDetails(cmd, "GV_P_VISA_FEE_DETAILS_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));
            }
            catch { throw; }
        }
        //public void Delete()
        //{
        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[2];
        //        paramList[0] = new SqlParameter("@P_adsvc_visafee_id", _transactionid);
        //        paramList[1] = new SqlParameter("@P_adsvc_msg_text", SqlDbType.VarChar,  200);
        //        paramList[1].Direction = ParameterDirection.Output;

        //        DBGateway.ExecuteNonQuery("GV_p_visa_fee_getlist", paramList);
        //        string message = Utility.ToString(paramList[1].Value);
        //        if (message != string.Empty) throw new Exception(message);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
#endregion

       
        #region Static Methods

        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("GV_p_visa_fee_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
    }
}
