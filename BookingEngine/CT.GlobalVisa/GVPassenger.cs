﻿using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Data;
using System.Data.SqlClient;
namespace CT.GlobalVisa
{
    public class GVPassenger
    {
        #region variables
        long fpax_id;
        long fpax_vsh_id;
        string fpax_pax_type;
        string fpax_name;
        string fpax_profession;
        string fpax_marital_status;
        string fpax_gender;
        string fpax_email;
        string fpax_mobile;
        string fpax_phone;
        string fpax_fax;
        string fpax_adds1;
        string fpax_adds2;
        string fpax_city;
        
        string fpax_zip_code;
        string fpax_country_id;
        string fapx_Issue_CountryName;
        string fpax_passport_type;
        string fpax_passport_no;
        DateTime fpax_psp_issue_date;
        //string fpax_psp_issue_place;
        DateTime fpax_psp_expiry_date;
        //string fpax_psp_val_status;
        //string fpax_doc_no;
        //string fpax_tracking_no;
        string fpax_dispatch_status;
        string fpax_dispatch_remarks;
        //string fpax_visa_no;
        //DateTime fpax_visa_issue_date;
        //DateTime fpax_visa_exp_date;
        //DateTime fpax_visa_coll_date;
        //string fpax_remarks;
        //int fpax_add_charges_id;
        //decimal fpax_add_charges_amnt;
        //string fpax_add_charges_remarks;
        //string fpax_form_submit_status;
        string fpax_status;
        long fpax_created_by;
        //string fpax_pan_number;
        //long fpax_manager_id;
        string fpax_docpath;
        #endregion

        #region properities
        public long Fpax_id
        {
            get
            {
                return fpax_id;
            }

            set
            {
                fpax_id = value;
            }
        }
        public long Fpax_vsh_id
        {
            get
            {
                return fpax_vsh_id;
            }

            set
            {
                fpax_vsh_id = value;
            }
        }

        public string Fpax_pax_type
        {
            get
            {
                return fpax_pax_type;
            }

            set
            {
                fpax_pax_type = value;
            }
        }

        public string Fpax_name
        {
            get
            {
                return fpax_name;
            }

            set
            {
                fpax_name = value;
            }
        }

        public string Fpax_profession
        {
            get
            {
                return fpax_profession;
            }

            set
            {
                fpax_profession = value;
            }
        }

        public string Fpax_marital_status
        {
            get
            {
                return fpax_marital_status;
            }

            set
            {
                fpax_marital_status = value;
            }
        }

        public string Fpax_gender
        {
            get
            {
                return fpax_gender;
            }

            set
            {
                fpax_gender = value;
            }
        }

        public string Fpax_email
        {
            get
            {
                return fpax_email;
            }

            set
            {
                fpax_email = value;
            }
        }

        public string Fpax_mobile
        {
            get
            {
                return fpax_mobile;
            }

            set
            {
                fpax_mobile = value;
            }
        }

        public string Fpax_phone
        {
            get
            {
                return fpax_phone;
            }

            set
            {
                fpax_phone = value;
            }
        }

        public string Fpax_fax
        {
            get
            {
                return fpax_fax;
            }

            set
            {
                fpax_fax = value;
            }
        }

        public string Fpax_adds1
        {
            get
            {
                return fpax_adds1;
            }

            set
            {
                fpax_adds1 = value;
            }
        }
        public string Fpax_adds2
        {
            get
            {
                return fpax_adds2;
            }

            set
            {
                fpax_adds2 = value;
            }
        }

        public string Fpax_city
        {
            get
            {
                return fpax_city;
            }

            set
            {
                fpax_city = value;
            }
        }

        public string Fpax_zip_code
        {
            get
            {
                return fpax_zip_code;
            }

            set
            {
                fpax_zip_code = value;
            }
        }

        public string Fpax_country_id
        {
            get
            {
                return fpax_country_id;
            }

            set
            {
                fpax_country_id = value;
            }
        }

        public string Fpax_passport_type
        {
            get
            {
                return fpax_passport_type;
            }

            set
            {
                fpax_passport_type = value;
            }
        }

        public string Fpax_passport_no
        {
            get
            {
                return fpax_passport_no;
            }

            set
            {
                fpax_passport_no = value;
            }
        }

        public DateTime Fpax_psp_issue_date
        {
            get
            {
                return fpax_psp_issue_date;
            }

            set
            {
                fpax_psp_issue_date = value;
            }
        }

        //public string Fpax_psp_issue_place
        //{
        //    get
        //    {
        //        return fpax_psp_issue_place;
        //    }

        //    set
        //    {
        //        fpax_psp_issue_place = value;
        //    }
        //}

        public DateTime Fpax_psp_expiry_date
        {
            get
            {
                return fpax_psp_expiry_date;
            }

            set
            {
                fpax_psp_expiry_date = value;
            }
        }

        //public string Fpax_psp_val_status
        //{
        //    get
        //    {
        //        return fpax_psp_val_status;
        //    }

        //    set
        //    {
        //        fpax_psp_val_status = value;
        //    }
        //}

        //public string Fpax_doc_no
        //{
        //    get
        //    {
        //        return fpax_doc_no;
        //    }

        //    set
        //    {
        //        fpax_doc_no = value;
        //    }
        //}

        //public string Fpax_tracking_no
        //{
        //    get
        //    {
        //        return fpax_tracking_no;
        //    }

        //    set
        //    {
        //        fpax_tracking_no = value;
        //    }
        //}

        public string Fpax_dispatch_status
        {
            get
            {
                return fpax_dispatch_status;
            }

            set
            {
                fpax_dispatch_status = value;
            }
        }

        public string Fpax_dispatch_remarks
        {
            get
            {
                return fpax_dispatch_remarks;
            }

            set
            {
                fpax_dispatch_remarks = value;
            }
        }

        //public string Fpax_visa_no
        //{
        //    get
        //    {
        //        return fpax_visa_no;
        //    }

        //    set
        //    {
        //        fpax_visa_no = value;
        //    }
        //}

        //public DateTime Fpax_visa_issue_date
        //{
        //    get
        //    {
        //        return fpax_visa_issue_date;
        //    }

        //    set
        //    {
        //        fpax_visa_issue_date = value;
        //    }
        //}

        //public DateTime Fpax_visa_exp_date
        //{
        //    get
        //    {
        //        return fpax_visa_exp_date;
        //    }

        //    set
        //    {
        //        fpax_visa_exp_date = value;
        //    }
        //}

        //public DateTime Fpax_visa_coll_date
        //{
        //    get
        //    {
        //        return fpax_visa_coll_date;
        //    }

        //    set
        //    {
        //        fpax_visa_coll_date = value;
        //    }
        //}

        //public string Fpax_remarks
        //{
        //    get
        //    {
        //        return fpax_remarks;
        //    }

        //    set
        //    {
        //        fpax_remarks = value;
        //    }
        //}

        //public int Fpax_add_charges_id
        //{
        //    get
        //    {
        //        return fpax_add_charges_id;
        //    }

        //    set
        //    {
        //        fpax_add_charges_id = value;
        //    }
        //}

        //public decimal Fpax_add_charges_amnt
        //{
        //    get
        //    {
        //        return fpax_add_charges_amnt;
        //    }

        //    set
        //    {
        //        fpax_add_charges_amnt = value;
        //    }
        //}

        //public string Fpax_add_charges_remarks
        //{
        //    get
        //    {
        //        return fpax_add_charges_remarks;
        //    }

        //    set
        //    {
        //        fpax_add_charges_remarks = value;
        //    }
        //}

        //public string Fpax_form_submit_status
        //{
        //    get
        //    {
        //        return fpax_form_submit_status;
        //    }

        //    set
        //    {
        //        fpax_form_submit_status = value;
        //    }
        //}

        public string Fpax_status
        {
            get
            {
                return fpax_status;
            }

            set
            {
                fpax_status = value;
            }
        }

        public long Fpax_created_by
        {
            get
            {
                return fpax_created_by;
            }

            set
            {
                fpax_created_by = value;
            }
        }

        public string Fapx_Issue_CountryName
        {
            get
            {
                return fapx_Issue_CountryName;
            }

            set
            {
                fapx_Issue_CountryName = value;
            }
        }

        public string Fpax_docpath
        {
            get
            {
                return fpax_docpath;
            }
            set
            {
                fpax_docpath = value;
            }
        }

        //public string Fpax_pan_number
        //{
        //    get
        //    {
        //        return fpax_pan_number;
        //    }

        //    set
        //    {
        //        fpax_pan_number = value;
        //    }
        //}

        //public long Fpax_manager_id
        //{
        //    get
        //    {
        //        return fpax_manager_id;
        //    }

        //    set
        //    {
        //        fpax_manager_id = value;
        //    }
        //}
        #endregion
        public GVPassenger()
        {
            fpax_id = -1;
        }
        public void Save(SqlCommand cmd)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[25];
                paramArr[0] = new SqlParameter("@P_fpax_id", fpax_id);
                paramArr[1] = new SqlParameter("@P_fpax_vsh_id", fpax_vsh_id);
                paramArr[2] = new SqlParameter("@P_fpax_type", fpax_pax_type);
                paramArr[3] = new SqlParameter("@P_fpax_name", fpax_name);
                if (!string.IsNullOrEmpty(fpax_profession)) paramArr[4] = new SqlParameter("@P_fpax_profession", fpax_profession);
                if (!string.IsNullOrEmpty(fpax_marital_status)) paramArr[5] = new SqlParameter("@P_fpax_marital_status", fpax_marital_status);
                paramArr[6] = new SqlParameter("@P_fpax_gender", fpax_gender);
                if (!string.IsNullOrEmpty(fpax_email)) paramArr[7] = new SqlParameter("@P_fpax_email", fpax_email);
                if (!string.IsNullOrEmpty(fpax_mobile)) paramArr[8] = new SqlParameter("@P_fpax_mobile", fpax_mobile);
                if (!string.IsNullOrEmpty(fpax_phone)) paramArr[9] = new SqlParameter("@P_fpax_phone", fpax_phone);
                if (!string.IsNullOrEmpty(fpax_fax)) paramArr[10] = new SqlParameter("@P_fpax_fax", fpax_fax);
                if (!string.IsNullOrEmpty(fpax_adds1)) paramArr[11] = new SqlParameter("@p_fpax_adds1", fpax_adds1);
                if (!string.IsNullOrEmpty(fpax_adds1)) paramArr[12] = new SqlParameter("@p_fpax_adds2", fpax_adds2);
                if (!string.IsNullOrEmpty(fpax_city)) paramArr[13] = new SqlParameter("@P_fpax_city", fpax_city);
                if (!string.IsNullOrEmpty(fpax_zip_code)) paramArr[14] = new SqlParameter("@P_fpax_zip_code", fpax_zip_code);
                paramArr[15] = new SqlParameter("@P_fpax_country_id", fpax_country_id);
                paramArr[16] = new SqlParameter("@P_fpax_passport_type", fpax_passport_type);
                paramArr[17] = new SqlParameter("@P_fpax_passport_no", fpax_passport_no);
                if (fpax_psp_issue_date != Utility.ToDate("")) paramArr[18] = new SqlParameter("@P_fpax_psp_issue_date", fpax_psp_issue_date);
                if (fpax_psp_expiry_date != Utility.ToDate("")) paramArr[19] = new SqlParameter("@P_fpax_psp_expiry_date", fpax_psp_expiry_date);
                paramArr[20] = new SqlParameter("@P_fpax_created_by", fpax_created_by);
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[21] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[22] = paramMsgText;
                SqlParameter paramRetID = new SqlParameter("@P_FPAX_ID_RET", SqlDbType.BigInt);
                paramRetID.Size = 12;
                paramRetID.Direction = ParameterDirection.Output;
                paramArr[23] = paramRetID;
                SqlParameter paramRetpt= new SqlParameter("@P_fpax_passport_no_ret", SqlDbType.NVarChar);
                paramRetpt.Size = 15;
                paramRetpt.Direction = ParameterDirection.Output;
                paramArr[24] = paramRetpt;
                DBGateway.ExecuteNonQueryDetails(cmd, "GV_p_Visa_Sales_Details_Add", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));
                fpax_id = Utility.ToLong(paramRetID.Value);
                fpax_passport_no = Utility.ToString(paramRetpt.Value);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
