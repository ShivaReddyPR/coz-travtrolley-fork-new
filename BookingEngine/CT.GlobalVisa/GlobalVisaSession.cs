﻿using System;

namespace CT.GlobalVisa
{
    public class GlobalVisaSession
    {
        #region Variables
        int travelResionId;
        string travelResionName;
        string actionStatus;
        int travelEmployeeId;
        object corpProfileDetails;
        int adult;
        int child;
        int infant;
        string travelToCountryCode;
        string travelToCountryName;
        int visaTypeId;
        string visaTypeName;
        DateTime visaDate;
        object passengerList;
        string nationalityCode;
        string resCountryCode;
        object visaEnquireList;
        string onAccountStatus;
        object visaSale;
        #endregion
        #region Properities
        public int TravelResionId
        {
            get { return travelResionId; }
            set { travelResionId = value; }
        }
        public string TravelResionName
        {
            get { return travelResionName; }
            set { travelResionName = value; }
        }
        public int TravelEmployeeId
        {
            get { return travelEmployeeId; }
            set { travelEmployeeId = value; }
        }
        public object CorpProfileDetails
        {
            get { return corpProfileDetails; }
            set { corpProfileDetails = value; }
        }
        public int Adult
        {
            get { return adult; }
            set { adult=value; }
        }
        public int Child
        {
            get { return child; }
            set { child = value; }
        }
        public int Infant
        {
            get { return infant; }
            set { infant = value; }
        }

        public string TravelToCountryCode
        {
            get
            {
                return travelToCountryCode;
            }

            set
            {
                travelToCountryCode = value;
            }
        }

        public string TravelToCountryName
        {
            get
            {
                return travelToCountryName;
            }

            set
            {
                travelToCountryName = value;
            }
        }

        public int VisaTypeId
        {
            get
            {
                return visaTypeId;
            }

            set
            {
                visaTypeId = value;
            }
        }

        public string VisaTypeName
        {
            get
            {
                return visaTypeName;
            }

            set
            {
                visaTypeName = value;
            }
        }

        public DateTime VisaDate
        {
            get
            {
                return visaDate;
            }

            set
            {
                visaDate = value;
            }
        }

        public object PassengerList
        {
            get
            {
                return passengerList;
            }

            set
            {
                passengerList = value;
            }
        }

        public string NationalityCode
        {
            get
            {
                return nationalityCode;
            }

            set
            {
                nationalityCode = value;
            }
        }

        public string ResCountryCode
        {
            get
            {
                return resCountryCode;
            }

            set
            {
                resCountryCode = value;
            }
        }

        public object VisaEnquireList
        {
            get
            {
                return visaEnquireList;
            }

            set
            {
                visaEnquireList = value;
            }
        }

        public string ActionStatus
        {
            get
            {
                return actionStatus;
            }

            set
            {
                actionStatus = value;
            }
        }

        public string OnAccountStatus
        {
            get
            {
                return onAccountStatus;
            }

            set
            {
                onAccountStatus = value;
            }
        }

        public object VisaSale
        {
            get
            {
                return visaSale;
            }

            set
            {
                visaSale = value;
            }
        }
        #endregion

    }
}
