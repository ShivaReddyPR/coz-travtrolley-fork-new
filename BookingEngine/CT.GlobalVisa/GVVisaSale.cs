﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using CT.Corporate;

namespace CT.GlobalVisa
{
    public enum GlobalVisaDispatchStatus
    {
        All = 0,//All
        Pending = 78,// Value N
        Submitted = 83,//S Submitted
        Dispatched = 84, //P Dispatched
        Rejected = 82, //R
        Approved = 65,//A
        Delivered = 68,//H hand over to Customer
        InQueue = 81, // Q in queue --from XML 
        DocumentReceived = 68,
        ReadyForSubmit = 70,
        DocumentVerified = 86,
        DocumentMismatch = 77
        //Operations=79,//O=Dispatch to Airport
        //BackOffice = 66,//B=Back Office
        //Acknowldeged = 87,//W Acknowldged

    }
    public class GVVisaSale
    {

        #region members
        long _transactionId;
        string _docType;
        string _docNumber;
        DateTime _docDate;
        string _passportType; 
        private string _countryId; 
        //private string _countryName; 
        private long _countryCityId; 
        private string _nationalityId; 
        //private string _nationalityName; 
        private int _visaTypeId;
        //string _visaTypeName;//S
        private string _residencyId;//S
        //private long _resCityId;//S
        private string _visaCategory;//S
        //private long _appCenterId;//S
        private DateTime _ttTravelDate;//S
        int _adults;
        int _children;
        int _infants;
        decimal _totVisaFee;
        decimal _addCharges;
        //string _addChargesRemarks;
        //decimal _discount;
        //string _remarks;
        //string _depositMode;

        long _locationId;
        //string _locationTerms;
        //string _locationAdds;
        long _companyId;
        //string _corpCustomer;
        //string _corpLPNno;
        //RecordStatus _status;
        long _createdBy;
        //string _createdByName;
        //DataTable _dtVisaPaxDetails;
        //DataTable _dtPaxDocDetails;
        //DataTable _dtAddSvcDetails;
        //string _docKeys;
        //string _obVisaDocStatus;

        string _settlementMode;
        string _currencyCode;
        decimal _exchangeRate;
        //PaymentModeInfo _cashMode = new PaymentModeInfo();
        //PaymentModeInfo _creditMode = new PaymentModeInfo();
        //PaymentModeInfo _cardMode = new PaymentModeInfo();
        //PaymentModeInfo _employeeMode = new PaymentModeInfo();
        //PaymentModeInfo _othersMode = new PaymentModeInfo();
        //string _modeRemarks;

        //string _urgentStatus;
        //decimal _adultUrgFee;
        //decimal _childUrgFee;
        //decimal _infantUrgFee;

        //string _add1FeeStatus;
        //decimal _adultAdd1Fee;
        //decimal _childAdd1Fee;
        //decimal _infantAdd1Fee;

        //string _add2FeeStatus;
        //decimal _adultAdd2Fee;
        //decimal _childAdd2Fee;
        //decimal _infantAdd2Fee;

        decimal _adultVisaFee;
        decimal _childVisaFee;
        decimal _infantVisaFee;

        decimal _adultSvcCharge;
        decimal _childSvcCharge;
        decimal _infantSvcCharge;

        decimal _totalVisaCost;
        //decimal _toCollect;


        //long _addServiceId;
        //decimal _addServiceAmount;
        //long _obVisaFeeId;
        //string _docketno;
        //string _crCardName;
        //decimal _crCardRate = 0;
        //decimal _crCardTotal = 0;
        //int _agentId;
        //long _paxId;
        //decimal _agentBalance;
        //string _pickupAddress;
        //string _pickupTimeslot;
        //string _agentPaymentGateway;

        //string _addSVCIdsAmount;

        //Credit Card Payment details
        //private string _paymentId;
        //private decimal _paymentAmount;
        //private string _paymentSourceId;
        //private string _paymentTrackId;
        //private string _paymentIPAddress;
        //private string _paymentRemarks;
        private string _paymentStatus;
        //private int _paymentCardId;
        //private string _waiveOffStatus;
        //private DateTime _returnDate;
        //private string _purposeVisit;

        //decimal _adultAdd3Fee;
        //decimal _childAdd3Fee;
        //decimal _infantAdd3Fee;
        string _approvedStatus;
        int _approvedBy;
        int _agentId;
        List<GVPassenger> _passengerList;
        List<CorpProfileApproval> profileApproversList;
        long _profileId;
        //For Email Purpose..
        string _selCountry;
        string _selNationality;
        string _selResidence;
        string _selVisaType;
        string _selVisaCategory;
        string _travelReason;
        #endregion

        #region Properties
        public long TransactionId
        {
            get { return _transactionId; }
            set { _transactionId = value; }
        }
        public string DocType
        {
            get { return _docType; }
            set { _docType = value; }
        }
        public string DocNumber
        {
            get { return _docNumber; }
            set { _docNumber = value; }
        }

        public DateTime DocDate
        {
            get { return _docDate; }
            set { _docDate = value; }
        }


        public int VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId = value; }
        }
        //public string VisaTypeName
        //{
        //    get { return _visaTypeName; }
        //    set { _visaTypeName = value; }
        //}

        public int Adults
        {
            get { return _adults; }
            set { _adults = value; }
        }
        public int Children
        {
            get { return _children; }
            set { _children = value; }
        }
        public int Infants
        {
            get { return _infants; }
            set { _infants = value; }
        }
        public decimal TotVisaFee
        {
            get { return _totVisaFee; }
            set { _totVisaFee = value; }
        }
        public decimal AddCharges
        {
            get { return _addCharges; }
            set { _addCharges = value; }
        }
        //public string AddChargesRemarks
        //{
        //    get { return _addChargesRemarks; }
        //    set { _addChargesRemarks = value; }
        //}



        public string PassportType
        {
            get { return _passportType; }
            set { _passportType = value; }
        }
        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        //public string CountryName
        //{
        //    get { return _countryName; }
        //    set { _countryName = value; }
        //}

        public long CountryCityId
        {
            get { return _countryCityId; }
            set { _countryCityId = value; }
        }

        public string NationalityId
        {
            get { return _nationalityId; }
            set { _nationalityId = value; }
        }
        //public string NationalityName
        //{
        //    get { return _nationalityName; }
        //    set { _nationalityName = value; }
        //}

        public string ResidencyId
        {
            get { return _residencyId; }
            set { _residencyId = value; }
        }
        //public long ResCityId
        //{
        //    get { return _resCityId; }
        //    set { _resCityId = value; }
        //}

        public string VisaCategory
        {
            get { return _visaCategory; }
            set { _visaCategory = value; }
        }

        //public long AppCenterId
        //{
        //    get { return _appCenterId; }
        //    set { _appCenterId = value; }
        //}

        public DateTime TTTravelDate
        {
            get { return _ttTravelDate; }
            set { _ttTravelDate = value; }
        }






        //public string Remarks
        //{
        //    get { return _remarks; }
        //    set { _remarks = value; }
        //}
        //public string DepositMode
        //{
        //    get { return _depositMode; }
        //    set { _depositMode = value; }
        //}

        //public decimal Discount
        //{
        //    get { return _discount; }
        //    set { _discount = value; }
        //}


        public long LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        //public string LocationTerms
        //{
        //    get { return _locationTerms; }
        //    set { _locationTerms = value; }
        //}
        //public string LocationAdds
        //{
        //    get { return _locationAdds; }
        //    set { _locationAdds = value; }
        //}

        public long CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
        //public RecordStatus Status
        //{
        //    get { return _status; }
        //    set { _status = value; }
        //}
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        //public string CreatedByName
        //{
        //    get { return _createdByName; }
        //    set { _createdByName = value; }
        //}
        //public DataTable PaxDetailsList
        //{
        //    get { return _dtVisaPaxDetails; }
        //    set { _dtVisaPaxDetails = value; }
        //}
        //public DataTable PaxDocDetails
        //{
        //    get { return _dtPaxDocDetails; }
        //    set { _dtPaxDocDetails = value; }
        //}
        //public DataTable AddSvcDetails
        //{
        //    get { return _dtAddSvcDetails; }
        //    set { _dtAddSvcDetails = value; }
        //}


        //public string DocKeys
        //{
        //    get { return _docKeys; }
        //    set { _docKeys = value; }
        //}
        //public string OBVisaDocStatus
        //{
        //    get { return _obVisaDocStatus; }
        //    set { _obVisaDocStatus = value; }
        //}
        //public string CorpCustomer
        //{
        //    get { return _corpCustomer; }
        //    set { _corpCustomer = value; }
        //}

        //public string CorpLPNno
        //{
        //    get { return _corpLPNno; }
        //    set { _corpLPNno = value; }
        //}


        public string SettlementMode
        {
            get { return _settlementMode; }
            set { _settlementMode = value; }
        }
        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }
        public decimal ExchangeRate
        {
            get { return _exchangeRate; }
            set { _exchangeRate = value; }
        }
        //public PaymentModeInfo CashMode
        //{
        //    get { return _cashMode; }
        //    set { _cashMode = value; }
        //}
        //public PaymentModeInfo CreditMode
        //{
        //    get { return _creditMode; }
        //    set { _creditMode = value; }
        //}
        //public PaymentModeInfo CardMode
        //{
        //    get { return _cardMode; }
        //    set { _cardMode = value; }
        //}
        //public PaymentModeInfo EmployeeMode
        //{
        //    get { return _employeeMode; }
        //    set { _employeeMode = value; }
        //}
        //public PaymentModeInfo OthersMode
        //{
        //    get { return _othersMode; }
        //    set { _othersMode = value; }
        //}
        //public string ModeRemarks
        //{
        //    get { return _modeRemarks; }
        //    set { _modeRemarks = value; }
        //}

        //public decimal AdultUrgFee
        //{
        //    get { return _adultUrgFee; }
        //    set { _adultUrgFee = value; }
        //}

        //public decimal ChildUrgFee
        //{
        //    get { return _childUrgFee; }
        //    set { _childUrgFee = value; }
        //}


        //public decimal InfantUrgFee
        //{
        //    get { return _infantUrgFee; }
        //    set { _infantUrgFee = value; }
        //}


        //public decimal AdultAdd1Fee
        //{
        //    get { return _adultAdd1Fee; }
        //    set { _adultAdd1Fee = value; }
        //}

        //public decimal ChildAdd1Fee
        //{
        //    get { return _childAdd1Fee; }
        //    set { _childAdd1Fee = value; }
        //}


        //public decimal InfantAdd1Fee
        //{
        //    get { return _infantAdd1Fee; }
        //    set { _infantAdd1Fee = value; }
        //}


        //public decimal AdultAdd2Fee
        //{
        //    get { return _adultAdd2Fee; }
        //    set { _adultAdd2Fee = value; }
        //}

        //public decimal ChildAdd2Fee
        //{
        //    get { return _childAdd2Fee; }
        //    set { _childAdd2Fee = value; }
        //}


        //public decimal InfantAdd2Fee
        //{
        //    get { return _infantAdd2Fee; }
        //    set { _infantAdd2Fee = value; }
        //}

        //public long AddServiceId
        //{
        //    get { return _addServiceId; }
        //    set { _addServiceId = value; }
        //}

        //public decimal AddServiceAmount
        //{
        //    get { return _addServiceAmount; }
        //    set { _addServiceAmount = value; }
        //}

        //vij

        public decimal AdultVisaFee
        {
            get { return _adultVisaFee; }
            set { _adultVisaFee = value; }
        }

        public decimal ChildVisaFee
        {
            get { return _childVisaFee; }
            set { _childVisaFee = value; }
        }

        public decimal InfantVisaFee
        {
            get { return _infantVisaFee; }
            set { _infantVisaFee = value; }
        }

        public decimal AdultSvcCharge
        {
            get { return _adultSvcCharge; }
            set { _adultSvcCharge = value; }
        }
        public decimal ChildSvcCharge
        {
            get { return _childSvcCharge; }
            set { _childSvcCharge = value; }
        }

        public decimal InfantSvcCharge
        {
            get { return _infantSvcCharge; }
            set { _infantSvcCharge = value; }
        }

        public decimal TotalVisaCost
        {
            get { return _totalVisaCost; }
            set { _totalVisaCost = value; }
        }

        //public decimal ToCollect
        //{
        //    get { return _toCollect; }
        //    set { _toCollect = value; }
        //}

        //public string UrgentStatus
        //{
        //    get { return _urgentStatus; }
        //    set { _urgentStatus = value; }
        //}

        //public string Add1FeeStatus
        //{
        //    get { return _add1FeeStatus; }
        //    set { _add1FeeStatus = value; }
        //}

        //public string Add2FeeStatus
        //{
        //    get { return _add2FeeStatus; }
        //    set { _add2FeeStatus = value; }
        //}

        //public long OBVisaFeeId
        //{
        //    get { return _obVisaFeeId; }
        //    set { _obVisaFeeId = value; }
        //}


        //public string Docketno
        //{
        //    get { return _docketno; }
        //    set { _docketno = value; }
        //}

        //public string CRCardName
        //{
        //    get { return _crCardName; }
        //    set { _crCardName = value; }
        //}
        //public decimal CRCardRate
        //{
        //    get { return _crCardRate; }
        //    set { _crCardRate = value; }
        //}
        //public decimal CRCardTotal
        //{
        //    get { return _crCardTotal; }
        //    set { _crCardTotal = value; }
        //}


        //public string AddSVCIdsAmount
        //{
        //    get { return _addSVCIdsAmount; }
        //    set { _addSVCIdsAmount = value; }
        //}

        //public decimal AgentBalance
        //{
        //    get { return _agentBalance; }
        //    set { _agentBalance = value; }
        //}
        //public string PickupAddress
        //{
        //    get { return _pickupAddress; }
        //    set { _pickupAddress = value; }
        //}
        //public string PickupTimeslot
        //{
        //    get { return _pickupTimeslot; }
        //    set { _pickupTimeslot = value; }
        //}
        //public string AgentPaymentGateway
        //{
        //    get { return _agentPaymentGateway; }
        //    set { _agentPaymentGateway = value; }
        //}

        ////Credit Card Payment details properties
        //public string PaymentId
        //{
        //    get { return _paymentId; }
        //    set { _paymentId = value; }
        //}
        //public decimal PaymentAmount
        //{
        //    get { return _paymentAmount; }
        //    set { _paymentAmount = value; }
        //}
        //public string PaymentSourceId
        //{
        //    get { return _paymentSourceId; }
        //    set { _paymentSourceId = value; }
        //}
        //public string PaymentTrackId
        //{
        //    get { return _paymentTrackId; }
        //    set { _paymentTrackId = value; }
        //}
        //public string PaymentIPAddress
        //{
        //    get { return _paymentIPAddress; }
        //    set { _paymentIPAddress = value; }
        //}
        //public string PaymentRemarks
        //{
        //    get { return _paymentRemarks; }
        //    set { _paymentRemarks = value; }
        //}
        public string PaymentStatus
        {
            get { return _paymentStatus; }
            set { _paymentStatus = value; }
        }
        //public int PaymentCardId
        //{
        //    get { return _paymentCardId; }
        //    set { _paymentCardId = value; }
        //}
        //public string WaiveOffStatus
        //{
        //    get { return _waiveOffStatus; }
        //    set { _waiveOffStatus = value; }

        //}

        //public DateTime ReturnDate
        //{
        //    get { return _returnDate; }
        //    set { _returnDate = value; }

        //}

        //public string PurposeVisit
        //{
        //    get { return _purposeVisit; }
        //    set { _purposeVisit = value; }

        //}


        //public decimal AdultAdd3Fee
        //{
        //    get { return _adultAdd3Fee; }
        //    set { _adultAdd3Fee = value; }
        //}

        //public decimal ChildAdd3Fee
        //{
        //    get { return _childAdd3Fee; }
        //    set { _childAdd3Fee = value; }
        //}


        //public decimal InfantAdd3Fee
        //{
        //    get { return _infantAdd3Fee; }
        //    set { _infantAdd3Fee = value; }
        //}

        public string ApprovedStatus
        {
            get
            {
                return _approvedStatus;
            }

            set
            {
                _approvedStatus = value;
            }
        }

        public int ApprovedBy
        {
            get
            {
                return _approvedBy;
            }

            set
            {
                _approvedBy = value;
            }
        }

        public int AgentId
        {
            get
            {
                return _agentId;
            }

            set
            {
                _agentId = value;
            }
        }

        public List<GVPassenger> PassengerList
        {
            get
            {
                return _passengerList;
            }

            set
            {
                _passengerList = value;
            }
        }

        public List<CorpProfileApproval> ProfileApproversList
        {
            get
            {
                return profileApproversList;
            }

            set
            {
                profileApproversList = value;
            }
        }

        public long ProfileId
        {
            get
            {
                return _profileId;
            }

            set
            {
                _profileId = value;
            }
        }

        public string SelCountry
        {
            get
            {
                return _selCountry;
            }

            set
            {
                _selCountry = value;
            }
        }

        public string SelNationality
        {
            get
            {
                return _selNationality;
            }

            set
            {
                _selNationality = value;
            }
        }

        public string SelResidence
        {
            get
            {
                return _selResidence;
            }

            set
            {
                _selResidence = value;
            }
        }

        public string SelVisaType
        {
            get
            {
                return _selVisaType;
            }

            set
            {
                _selVisaType = value;
            }
        }

        public string SelVisaCategory
        {
            get
            {
                return _selVisaCategory;
            }

            set
            {
                _selVisaCategory = value;
            }
        }

        public string TravelReason
        {
            get
            {
                return _travelReason;
            }

            set
            {
                _travelReason = value;
            }
        }

        #endregion
        public GVVisaSale()
        {
            _transactionId = -1;

        }
        public GVVisaSale(long visaId)
        {
            GetVisaDetails(visaId);

        }
        private void GetVisaDetails(long visaId)
        {

            DataSet ds = GetData(visaId);
            UpdateBusinessData(ds);
        }
        private DataSet GetData(long visaId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_VisaId", visaId);
                DataSet dsResult = DBGateway.ExecuteQuery("GV_P_VIsaSales_Getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {
            try
            {
                if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    UpdateBusinessData(ds.Tables[0].Rows[0]);
                }
                if (ds != null && ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    _passengerList = new List<GVPassenger>();
                    foreach (DataRow data in ds.Tables[1].Rows)
                    {
                        GVPassenger psngr = new GVPassenger();
                        if (data["fpax_id"] != DBNull.Value)
                        {
                            psngr.Fpax_id = Utility.ToLong(data["fpax_id"]);
                        }
                        if (data["fpax_vsh_id"] != DBNull.Value)
                        {
                            psngr.Fpax_vsh_id = Utility.ToLong(data["fpax_vsh_id"]);
                        }
                        if (data["fpax_pax_type"] != DBNull.Value)
                        {
                            psngr.Fpax_pax_type = Utility.ToString(data["fpax_pax_type"]);
                        }
                        if (data["fpax_name"] != DBNull.Value)
                        {
                            psngr.Fpax_name = Utility.ToString(data["fpax_name"]);
                        }
                        if (data["fpax_profession"] != DBNull.Value)
                        {
                            psngr.Fpax_profession = Utility.ToString(data["fpax_profession"]);
                        }
                        if (data["fpax_marital_status"] != DBNull.Value)
                        {
                            psngr.Fpax_marital_status = Utility.ToString(data["fpax_marital_status"]);
                        }
                        if (data["fpax_gender"] != DBNull.Value)
                        {
                            psngr.Fpax_gender = Utility.ToString(data["fpax_gender"]);
                        }
                        if (data["fpax_email"] != DBNull.Value)
                        {
                            psngr.Fpax_email = Utility.ToString(data["fpax_email"]);
                        }
                        if (data["fpax_mobile"] != DBNull.Value)
                        {
                            psngr.Fpax_mobile = Utility.ToString(data["fpax_mobile"]);
                        }
                        if (data["fpax_phone"] != DBNull.Value)
                        {
                            psngr.Fpax_phone = Utility.ToString(data["fpax_phone"]);
                        }
                        if (data["fpax_fax"] != DBNull.Value)
                        {
                            psngr.Fpax_fax = Utility.ToString(data["fpax_fax"]);
                        }
                        if (data["fpax_adds1"] != DBNull.Value)
                        {
                            psngr.Fpax_adds1 = Utility.ToString(data["fpax_adds1"]);
                        }
                        if (data["fpax_adds2"] != DBNull.Value)
                        {
                            psngr.Fpax_adds2 = Utility.ToString(data["fpax_adds2"]);
                        }
                        if (data["fpax_city"] != DBNull.Value)
                        {
                            psngr.Fpax_city = Utility.ToString(data["fpax_city"]);
                        }
                        if (data["fpax_zip_code"] != DBNull.Value)
                        {
                            psngr.Fpax_zip_code = Utility.ToString(data["fpax_zip_code"]);
                        }
                        if (data["fpax_country_id"] != DBNull.Value)
                        {
                            psngr.Fpax_country_id = Utility.ToString(data["fpax_country_id"]);
                        }
                        if (data["passIssueCountry"] != DBNull.Value)
                        {
                            psngr.Fapx_Issue_CountryName = Utility.ToString(data["passIssueCountry"]);
                        }
                        if (data["fpax_passport_type"] != DBNull.Value)
                        {
                            psngr.Fpax_passport_type = Utility.ToString(data["fpax_passport_type"]);
                        }
                        if (data["fpax_passport_no"] != DBNull.Value)
                        {
                            psngr.Fpax_passport_no = Utility.ToString(data["fpax_passport_no"]);
                        }
                        if (data["fpax_psp_issue_date"] != DBNull.Value)
                        {
                            psngr.Fpax_psp_issue_date = Utility.ToDate(data["fpax_psp_issue_date"]);
                        }
                        if (data["fpax_psp_expiry_date"] != DBNull.Value)
                        {
                            psngr.Fpax_psp_expiry_date = Utility.ToDate(data["fpax_psp_expiry_date"]);
                        }
                        if (data["fpax_created_by"] != DBNull.Value)
                        {
                            psngr.Fpax_created_by = Utility.ToLong(data["fpax_created_by"]);
                        }
                        _passengerList.Add(psngr);
                    }
                }
                if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                {
                    profileApproversList = new List<CorpProfileApproval>();
                    foreach (DataRow data in ds.Tables[2].Rows)
                    {
                        CorpProfileApproval approver = new CorpProfileApproval();
                        if (data["ApproverId"] != DBNull.Value)
                        {
                            approver.ApproverId = Convert.ToInt32(data["ApproverId"]);
                        }
                        if (data["ApproverHierarchy"] != DBNull.Value)
                        {
                            approver.Hierarchy = Convert.ToInt32(data["ApproverHierarchy"]);
                        }
                        if (data["ApprovalStatus"] != DBNull.Value)
                        {
                            approver.ApprovalStatus = Convert.ToString(data["ApprovalStatus"]);
                        }
                        if (data["APPNAME"] != DBNull.Value)
                        {
                            approver.ApproverName = Convert.ToString(data["APPNAME"]);
                        }
                        if (data["APPEMAIL"] != DBNull.Value)
                        {
                            approver.ApproverEmail = Convert.ToString(data["APPEMAIL"]);
                        }
                        if (data["email"] != DBNull.Value)
                        {
                            approver.ProfileEmail = Convert.ToString(data["email"]);
                        }
                        profileApproversList.Add(approver);

                    }
                }
            }
            catch { throw; }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (dr["fvs_id"] != DBNull.Value)
                {
                    _transactionId = Utility.ToLong(dr["fvs_id"]);
                }
                if (dr["fvs_doc_type"] != DBNull.Value)
                {
                    _docType = Utility.ToString(dr["fvs_doc_type"]);
                }
                if (dr["fvs_doc_no"] != DBNull.Value)
                {
                    _docNumber = Utility.ToString(dr["fvs_doc_no"]);
                }
                if (dr["fvs_doc_date"] != DBNull.Value)
                {
                    _docDate = Utility.ToDate(dr["fvs_doc_date"]);
                }
                if (dr["fvs_passport_type"] != DBNull.Value)
                {
                    _passportType = Utility.ToString(dr["fvs_passport_type"]);
                }
                if (dr["fvs_country_id"] != DBNull.Value)
                {
                    _countryId = Utility.ToString(dr["fvs_country_id"]);
                }
                if (dr["fvs_country_city_id"] != DBNull.Value)
                {
                    _countryCityId = Utility.ToLong(dr["fvs_country_city_id"]);
                }
                if (dr["fvs_nationality_id"] != DBNull.Value)
                {
                    _nationalityId = Utility.ToString(dr["fvs_nationality_id"]);
                }
                if (dr["fvs_visa_type_id"] != DBNull.Value)
                {
                    _visaTypeId = Utility.ToInteger(dr["fvs_visa_type_id"]);
                }
                if (dr["fvs_residence_id"] != DBNull.Value)
                {
                    _residencyId = Utility.ToString(dr["fvs_residence_id"]);
                }
                if (dr["fvs_visa_category"] != DBNull.Value)
                {
                    _visaCategory = Utility.ToString(dr["fvs_visa_category"]);
                }
                if (dr["fvs_tent_travel_date"] != DBNull.Value)
                {
                    _ttTravelDate = Utility.ToDate(dr["fvs_tent_travel_date"]);
                }
                if (dr["fvs_adult"] != DBNull.Value)
                {
                    _adults = Utility.ToInteger(dr["fvs_adult"]);
                }
                if (dr["fvs_child"] != DBNull.Value)
                {
                    _children = Utility.ToInteger(dr["fvs_child"]);
                }
                if (dr["fvs_infant"] != DBNull.Value)
                {
                    _infants = Utility.ToInteger(dr["fvs_infant"]);
                }
                if (dr["fvs_adult_visa_fee"] != DBNull.Value)
                {
                    _adultVisaFee = Utility.ToDecimal(dr["fvs_adult_visa_fee"]);
                }
                if (dr["fvs_child_visa_fee"] != DBNull.Value)
                {
                    _childVisaFee = Utility.ToDecimal(dr["fvs_child_visa_fee"]);
                }
                if (dr["fvs_infant_visa_fee"] != DBNull.Value)
                {
                    _infantVisaFee = Utility.ToDecimal(dr["fvs_infant_visa_fee"]);
                }
                if (dr["fvs_adult_svc_charge"] != DBNull.Value)
                {
                    _adultSvcCharge = Utility.ToDecimal(dr["fvs_adult_svc_charge"]);
                }
                if (dr["fvs_child_svc_charge"] != DBNull.Value)
                {
                    _childSvcCharge = Utility.ToDecimal(dr["fvs_child_svc_charge"]);
                }
                if (dr["fvs_total_visa_fee"] != DBNull.Value)
                {
                    _totVisaFee = Utility.ToDecimal(dr["fvs_total_visa_fee"]);
                }
                if (dr["fvs_settlement_mode"] != DBNull.Value)
                {
                    _settlementMode = Utility.ToString(dr["fvs_settlement_mode"]);
                }
                if (dr["fvs_currency_code"] != DBNull.Value)
                {
                    _currencyCode = Utility.ToString(dr["fvs_currency_code"]);
                }
                if (dr["fvs_exchange_rate"] != DBNull.Value)
                {
                    _exchangeRate = Utility.ToDecimal(dr["fvs_exchange_rate"]);
                }
                if (dr["fvs_location_id"] != DBNull.Value)
                {
                    _locationId = Utility.ToLong(dr["fvs_location_id"]);
                }
                if (dr["fvs_company_id"] != DBNull.Value)
                {
                    _companyId = Utility.ToLong(dr["fvs_company_id"]);
                }
                if (dr["fvs_agent_id"] != DBNull.Value)
                {
                    _agentId = Utility.ToInteger(dr["fvs_agent_id"]);
                }
                if (dr["fvs_pg_status"] != DBNull.Value)
                {
                   _paymentStatus = Utility.ToString(dr["fvs_pg_status"]);
                }
                if (dr["fvs_created_by"] != DBNull.Value)
                {
                    _createdBy = Utility.ToLong(dr["fvs_created_by"]);
                }
                if (dr["fvs_approve_status"] != DBNull.Value)
                {
                    _approvedStatus = Utility.ToString(dr["fvs_approve_status"]);
                }
                if (dr["fvs_approved_by"] != DBNull.Value)
                {
                    _approvedBy = Utility.ToInteger(dr["fvs_approved_by"]);
                }
                if (dr["countryName"] != DBNull.Value)
                {
                    _selCountry = Utility.ToString(dr["countryName"]);
                }
                if (dr["nationality"] != DBNull.Value)
                {
                    _selNationality = Utility.ToString(dr["nationality"]);
                }
                if (dr["FVS_RESIDENCE_NAME"] != DBNull.Value)
                {
                    _selResidence = Utility.ToString(dr["FVS_RESIDENCE_NAME"]);
                }
                if (dr["fvs_visa_type_name"] != DBNull.Value)
                {
                    _selVisaType = Utility.ToString(dr["fvs_visa_type_name"]);
                }
                if (dr["fvs_Travel_Resson"] != DBNull.Value)
                {
                    _travelReason = Utility.ToString(dr["fvs_Travel_Resson"]);
                }
            }
            catch
            {
                throw;
            }
        }


        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                SqlParameter[] paramArr = new SqlParameter[35];
                paramArr[0] = new SqlParameter("@p_fvs_id", _transactionId);
                paramArr[1] = new SqlParameter("@p_fvs_doc_date", _docDate);
                paramArr[2] = new SqlParameter("@P_FVS_PASSPORT_TYPE", _passportType);
                paramArr[3] = new SqlParameter("@P_FVS_COUNTRY_ID", _countryId);
                paramArr[4] = new SqlParameter("@P_FVS_NATIONALITY_ID", _nationalityId);
                paramArr[5] = new SqlParameter("@P_FVS_VISA_TYPE_ID", _visaTypeId);
                paramArr[6] = new SqlParameter("@P_FVS_RESIDENCE_ID", _residencyId);
                paramArr[7] = new SqlParameter("@P_FVS_VISA_CATEGORY", _visaCategory);
                if (_ttTravelDate != Utility.ToDate("")) paramArr[8] = new SqlParameter("@P_FVS_TENT_TRAVEL_DATE", _ttTravelDate);
                paramArr[9] = new SqlParameter("@P_FVS_ADULT", _adults);
                paramArr[10] = new SqlParameter("@P_FVS_CHILD", _children);
                paramArr[11] = new SqlParameter("@P_FVS_INFANT", _infants);
                paramArr[12] = new SqlParameter("@P_fvs_adult_visa_fee", _adultVisaFee);
                paramArr[13] = new SqlParameter("@P_fvs_child_visa_fee", _childVisaFee);
                paramArr[14] = new SqlParameter("@P_fvs_infant_visa_fee", _infantVisaFee);
                paramArr[15] = new SqlParameter("@P_fvs_adult_svc_charge", _adultSvcCharge);
                paramArr[16] = new SqlParameter("@P_fvs_child_svc_charge", _childSvcCharge);
                paramArr[17] = new SqlParameter("@P_fvs_infant_svc_charge", _infantSvcCharge);
                paramArr[18] = new SqlParameter("@P_FVS_TOTAL_VISA_FEE", _totVisaFee);
                paramArr[19] = new SqlParameter("@P_FVS_SETTLEMENT_MODE", _settlementMode);
                paramArr[20] = new SqlParameter("@P_FVS_CURRENCY_CODE", _currencyCode);
                paramArr[21] = new SqlParameter("@P_FVS_EXCHANGE_RATE", _exchangeRate);
                paramArr[22] = new SqlParameter("@P_fvs_agent_id", _agentId);
                if(!string.IsNullOrEmpty(_paymentStatus)) paramArr[23] = new SqlParameter("@P_fvs_pg_status", _paymentStatus);
                paramArr[24] = new SqlParameter("@P_FVS_LOCATION_ID", _locationId);
                paramArr[25] = new SqlParameter("@P_FVS_COMPANY_ID", _companyId);
                paramArr[26] = new SqlParameter("@P_FVS_CREATED_BY", _createdBy);
                paramArr[27] = new SqlParameter("@P_fvs_Approved_Status", _approvedStatus);
                if(_approvedBy > 0)paramArr[28] = new SqlParameter("@P_fvs_Approved_by", _approvedBy);
                SqlParameter paramDocNo = new SqlParameter("@p_fvs_doc_no", SqlDbType.NVarChar);
                paramDocNo.Direction = ParameterDirection.Output; paramDocNo.Size = 50;
                paramArr[29] = paramDocNo;
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[30] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[31] = paramMsgText;

                SqlParameter paramMsgText1 = new SqlParameter("@p_fvs_id_ret", SqlDbType.BigInt);
                paramMsgText1.Size = 20;
                paramMsgText1.Direction = ParameterDirection.Output;
                paramArr[32] = paramMsgText1;
                paramArr[33] = new SqlParameter("@P_fvs_cropProfileId", _profileId);
                paramArr[34] = new SqlParameter("@P_fvs_TravelReason", _travelReason);
                DBGateway.ExecuteNonQueryDetails(cmd, "GV_p_Visa_Sales_Add", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));
                _docNumber = Utility.ToString(paramDocNo.Value);
                _transactionId = Utility.ToLong(paramMsgText1.Value);
                if (_transactionId > 0)
                {
                    if (PassengerList != null && PassengerList.Count > 0)
                    {
                        foreach (GVPassenger passenger in PassengerList)
                        {
                            passenger.Fpax_vsh_id = _transactionId;
                            passenger.Save(cmd);
                        }
                    }
                    if (!string.IsNullOrEmpty(_approvedStatus) && _approvedStatus == "Y")
                    {
                        if (profileApproversList != null)
                        {
                            foreach (CorpProfileApproval approver in profileApproversList)
                            {
                                Corp_Profile_Transc_Approval transcApproval = new Corp_Profile_Transc_Approval();
                                transcApproval.ApprovalStatus = "P";//Initial Approval Status 'Pending'
                                transcApproval.ApproverHierarchy = approver.Hierarchy;
                                transcApproval.ApproverId = approver.ApproverId;
                                transcApproval.CreatedBy = Utility.ToInteger(_createdBy);
                                transcApproval.ExpDetailId =Utility.ToInteger(_transactionId);
                                transcApproval.Status = "A";
                                transcApproval.Id = -1;
                                transcApproval.TranxType = "V"; //Trip Category
                                int rowId = transcApproval.SaveCorpProfileTransApproval(cmd);
                                approver.TranxApprovalId = rowId;
                            }
                        }
                    }
                }
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        #region Static Methods
        public static void UpdatePaymentStatus(long gvId,string orderId,string paymentId,string paymentStaus,string  remarks)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[5];
                paramArr[0] = new SqlParameter("@P_fvs_id", gvId);
                if (!string.IsNullOrEmpty(orderId)) paramArr[1] = new SqlParameter("@P_orderId", orderId);
                if (!string.IsNullOrEmpty(paymentId)) paramArr[2] = new SqlParameter("@P_PaymentId", paymentId);
                paramArr[3] = new SqlParameter("@P_PaymentStaus", paymentStaus);
                paramArr[4] = new SqlParameter("@P_Remarks", remarks);
                DBGateway.ExecuteNonQuerySP("GV_P_Visa_PayementStaus_Addupdate", paramArr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetDoucmentList(long profileId,long visaId,long paxId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                paramList[1] = new SqlParameter("@P_visaId", visaId);
                paramList[2] = new SqlParameter("@P_PaxId", paxId);
                return DBGateway.ExecuteQuery("GV_P_GetVisaDocuments", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static GVEnquiry GetEnquiryDetails(string countryCode,string nationalityCode,int visaTypeId,string resCountryCode,long AgentId)
        {
            GVEnquiry objGvEnquiry = new GVEnquiry();
            try
            {
                SqlParameter[] paramArr = new SqlParameter[8];
                paramArr[0] = new SqlParameter("@P_COUNTRY_ID", countryCode);
                paramArr[1] = new SqlParameter("@P_NATIONALITY_ID", nationalityCode);
                paramArr[2] = new SqlParameter("@P_VISA_TYPE_ID", visaTypeId);
                paramArr[3] = new SqlParameter("@P_COUNTRY_RES_ID", resCountryCode);
                paramArr[4] = new SqlParameter("@p_record_status", "A");
                paramArr[5] = new SqlParameter("@p_AGENT_ID", AgentId);
                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[6] = paramMsgType;
                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[7] = paramMsgText;
                DataSet ds= DBGateway.ExecuteQuery("GV_p_Get_Enquiry_Details", paramArr);
                if (ds != null && ds.Tables.Count > 2)
                {
                    DataTable dtReq = ds.Tables[0];
                    GVRequirementMaster objGvRequireMents = new GVRequirementMaster();
                    if (dtReq != null && dtReq.Rows.Count > 0)
                    {
                        DataRow drReq = dtReq.Rows[0];
                        if (drReq["REQ_ID"] != DBNull.Value)
                        {
                            objGvRequireMents.TransactionId = Utility.ToLong(drReq["REQ_ID"]);
                        }
                        if (drReq["REQ_HEADER"] != DBNull.Value)
                        {
                            objGvRequireMents.Header = Utility.ToString(drReq["REQ_HEADER"]);
                        }
                        if (drReq["REQ_DESCRIPTION"] != DBNull.Value)
                        {
                            objGvRequireMents.Description = Utility.ToString(drReq["REQ_DESCRIPTION"]);
                        }
                        objGvEnquiry.GvRequirements = objGvRequireMents;
                    }
                    DataTable dtTerms = ds.Tables[1];
                    GVTermsMaster objGvTerms = new GVTermsMaster();
                    if (dtTerms != null && dtTerms.Rows.Count > 0)
                    {
                        DataRow drTerms = dtTerms.Rows[0];
                        if (drTerms["terms_id"] != DBNull.Value)
                        {
                            objGvTerms.TransactionId = Utility.ToLong(drTerms["terms_id"]);
                        }
                        if (drTerms["terms_header"] != DBNull.Value)
                        {
                            objGvTerms.Header = Utility.ToString(drTerms["terms_header"]);
                        }
                        if (drTerms["terms_description"] != DBNull.Value)
                        {
                            objGvTerms.Description = Utility.ToString(drTerms["terms_description"]);
                        }
                        objGvEnquiry.GvTerms = objGvTerms;
                    }
                    DataTable dtFee = ds.Tables[2];
                    List<GVFeeMaster> objGvTermsList = new List<GVFeeMaster>();
                    if (dtFee != null && dtFee.Rows.Count > 0)
                    {
                        foreach (DataRow drFee in dtFee.Rows)
                        {
                            GVFeeMaster objFee = new GVFeeMaster();
                            if (drFee["CHARGE_ID"] != DBNull.Value)
                            {
                                objFee.TransactionId = Utility.ToLong(drFee["CHARGE_ID"]);
                            }
                            if (drFee["CHARGE_CODE"] != DBNull.Value)
                            {
                                objFee.Code = Utility.ToString(drFee["CHARGE_CODE"]);
                            }
                            if (drFee["CHARGE_COUNTRY_ID"] != DBNull.Value)
                            {
                                objFee.CountryID = Utility.ToString(drFee["CHARGE_COUNTRY_ID"]);
                            }
                            if (drFee["CHARGE_NATIONALITY_ID"] != DBNull.Value)
                            {
                                objFee.NationalityId = Utility.ToString(drFee["CHARGE_NATIONALITY_ID"]);
                            }
                            if (drFee["CHARGE_VISA_TYPE_ID"] != DBNull.Value)
                            {
                                objFee.VisaTypeId = Utility.ToInteger(drFee["CHARGE_VISA_TYPE_ID"]);
                            }
                            if (drFee["CHARGE_RESIDENCE_ID"] != DBNull.Value)
                            {
                                objFee.ResidenceId = Utility.ToString(drFee["CHARGE_RESIDENCE_ID"]);
                            }
                            if (drFee["CHARGE_VISA_CATEGORY"] != DBNull.Value)
                            {
                                objFee.VisaCategory = Utility.ToString(drFee["CHARGE_VISA_CATEGORY"]);
                            }
                            if (drFee["TOT_CHARGE_ADULT"] != DBNull.Value)
                            {
                                objFee.TotalChargeAdult = Utility.ToDecimal(drFee["TOT_CHARGE_ADULT"]);
                            }
                            if (drFee["TOT_CHARGE_CHILD"] != DBNull.Value)
                            {
                                objFee.TotalChargeChild = Utility.ToDecimal(drFee["TOT_CHARGE_CHILD"]);
                            }
                            if (drFee["TOT_CHARGE_INFANT"] != DBNull.Value)
                            {
                                objFee.TotalChargeInfant = Utility.ToDecimal(drFee["TOT_CHARGE_INFANT"]);
                            }
                           
                            if (drFee["CHARGE_URG_ADULT"] != DBNull.Value)
                            {
                                objFee.AdultUrgentFee = Utility.ToDecimal(drFee["CHARGE_URG_ADULT"]);
                            }
                            if (drFee["CHARGE_URG_CHILD"] != DBNull.Value)
                            {
                                objFee.ChildUrgentFee = Utility.ToDecimal(drFee["CHARGE_URG_CHILD"]);
                            }
                            if (drFee["CHARGE_URG_INFANT"] != DBNull.Value)
                            {
                                objFee.InfantUrgentFee = Utility.ToDecimal(drFee["CHARGE_URG_INFANT"]);
                            }
                            if (drFee["CHARGE_ADD1_ADULT"] != DBNull.Value)
                            {
                                objFee.AdultAdd1Fee = Utility.ToDecimal(drFee["CHARGE_ADD1_ADULT"]);
                            }
                            if (drFee["CHARGE_ADD1_CHILD"] != DBNull.Value)
                            {
                                objFee.ChildAdd1Fee = Utility.ToDecimal(drFee["CHARGE_ADD1_CHILD"]);
                            }
                            if (drFee["CHARGE_ADD1_INFANT"] != DBNull.Value)
                            {
                                objFee.InfantAdd1Fee = Utility.ToDecimal(drFee["CHARGE_ADD1_INFANT"]);
                            }
                            if (drFee["CHARGE_ADD2_ADULT"] != DBNull.Value)
                            {
                                objFee.AdultAdd2Fee = Utility.ToDecimal(drFee["CHARGE_ADD2_ADULT"]);
                            }
                            if (drFee["CHARGE_ADD2_CHILD"] != DBNull.Value)
                            {
                                objFee.ChildAdd2Fee = Utility.ToDecimal(drFee["CHARGE_ADD2_CHILD"]);
                            }
                            if (drFee["CHARGE_ADD2_INFANT"] != DBNull.Value)
                            {
                                objFee.InfantAdd2Fee = Utility.ToDecimal(drFee["CHARGE_ADD2_INFANT"]);
                            }
                            if (drFee["CHARGE_ADULT"] != DBNull.Value)
                            {
                                objFee.AdultVisaFee = Utility.ToDecimal(drFee["CHARGE_ADULT"]);
                            }
                            if (drFee["CHARGE_CHILD"] != DBNull.Value)
                            {
                                objFee.ChildVisaFee = Utility.ToDecimal(drFee["CHARGE_CHILD"]);
                            }
                            if (drFee["CHARGE_INFANT"] != DBNull.Value)
                            {
                                objFee.InfantVisaFee = Utility.ToDecimal(drFee["CHARGE_INFANT"]);
                            }
                            if (drFee["CHARGE_ACS_ADULT"] != DBNull.Value)
                            {
                                objFee.AdultService = Utility.ToDecimal(drFee["CHARGE_ACS_ADULT"]);
                            }
                            if (drFee["CHARGE_ACS_CHILD"] != DBNull.Value)
                            {
                                objFee.ChildService = Utility.ToDecimal(drFee["CHARGE_ACS_CHILD"]);
                            }
                            if (drFee["CHARGE_ACS_INFANT"] != DBNull.Value)
                            {
                                objFee.InfantService = Utility.ToDecimal(drFee["CHARGE_ACS_INFANT"]);
                            }
                            if (drFee["CHARGE_ADD3_ADULT"] != DBNull.Value)
                            {
                                objFee.AdultAdd3Fee = Utility.ToDecimal(drFee["CHARGE_ADD3_ADULT"]);
                            }
                            if (drFee["CHARGE_ADD2_CHILD"] != DBNull.Value)
                            {
                                objFee.ChildAdd3Fee = Utility.ToDecimal(drFee["CHARGE_ADD3_CHILD"]);
                            }
                            if (drFee["CHARGE_ADD3_INFANT"] != DBNull.Value)
                            {
                                objFee.InfantAdd3Fee = Utility.ToDecimal(drFee["CHARGE_ADD3_INFANT"]);
                            }
                            if (drFee["CHARGE_PROCESS_DAYS"] != DBNull.Value)
                            {
                                objFee.ProcessingDays = Utility.ToInteger(drFee["CHARGE_PROCESS_DAYS"]);
                            }
                            objGvTermsList.Add(objFee);
                        }
                        objGvEnquiry.GvFeeList = objGvTermsList;
                    }
                    DataTable dtHandlingFee = ds.Tables[3];
                    List<GVHandlingFeeMaster> objGvHandlingFeelist = new List<GVHandlingFeeMaster>();
                    if (dtHandlingFee != null && dtHandlingFee.Rows.Count > 0)
                    {
                        foreach (DataRow drHandling in dtHandlingFee.Rows)
                        {
                            GVHandlingFeeMaster objHandlingFee = new GVHandlingFeeMaster();
                            if (drHandling["hand_id"] != DBNull.Value)
                            {
                                objHandlingFee.TransactionId = Utility.ToLong(drHandling["hand_id"]);
                            }
                            if (drHandling["hand_country_id"] != DBNull.Value)
                            {
                                objHandlingFee.CountryID = Utility.ToString(drHandling["hand_country_id"]);
                            }
                            if (drHandling["hand_nationality_id"] != DBNull.Value)
                            {
                                objHandlingFee.NationalityId = Utility.ToString(drHandling["hand_nationality_id"]);
                            }
                            if (drHandling["hand_visa_type_id"] != DBNull.Value)
                            {
                                objHandlingFee.VisaTypeId = Utility.ToLong(drHandling["hand_visa_type_id"]);
                            }
                            if (drHandling["hand_residence_id"] != DBNull.Value)
                            {
                                objHandlingFee.ResidenceId = Utility.ToString(drHandling["hand_residence_id"]);
                            }
                            if (drHandling["hand_res_fee"] != DBNull.Value)
                            {
                                objHandlingFee.HandlingFee = Utility.ToDecimal(drHandling["hand_res_fee"]);
                            }
                            objGvHandlingFeelist.Add(objHandlingFee);
                        }
                        objGvEnquiry.GVHandlFee = objGvHandlingFeelist;
                    }

                    }
            }
            catch { throw; }
            return objGvEnquiry;
        }
        public static DataTable GetDispatchQueueList(DateTime fromDate, DateTime toDate, string countryCode,string nationalityCode, int visaType, string dispatchStatus, int userId,int locationId,int agentId, string agentType,string userType,long loginUserId)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[12];
                    paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                    paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                    if(!string.IsNullOrEmpty(countryCode)) paramList[2] = new SqlParameter("@p_fvs_country_id", countryCode);
                    if (!string.IsNullOrEmpty(nationalityCode)) paramList[3] = new SqlParameter("@P_fvs_nationality_id", nationalityCode);
                    if (visaType > 0) paramList[4] = new SqlParameter("@P_fvs_visa_type_id", visaType);
                    if (!string.IsNullOrEmpty(dispatchStatus)) paramList[5] = new SqlParameter("@P_FPAX_DISPATCH_STATUS", dispatchStatus);
                    if (userId > 0) paramList[6] = new SqlParameter("@p_fvs_user_id", userId);
                    if (locationId > 0) paramList[7] = new SqlParameter("@p_fvs_location_id", locationId);
                    if (agentId > 0) paramList[8] = new SqlParameter("@P_AGENT_ID", agentId);
                    paramList[9] = new SqlParameter("@P_AGENT_TYPE", agentType);
                    paramList[10] = new SqlParameter("@P_USER_TYPE", userType);
                    paramList[11] = new SqlParameter("@P_LOGIN_USER_ID", loginUserId);
                    data = DBGateway.ExecuteReaderSP("GV_p_visa_sales_DispatchQueueList", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public static void SaveDipatchDetails(long vsId, long paxId, GlobalVisaDispatchStatus dispatchStatus, string dispatchNo, string dispatchRemarks, string trackingNo,
            string visaNo, DateTime VisaIssueDate, DateTime VisaExpDate, DateTime VisaCollDate, long createdBy,long assignTo)
        {
            try
            {
                //getting Disptach Document Next Serial


                SqlParameter[] paramArr = new SqlParameter[14];
                if (vsId > 0) paramArr[0] = new SqlParameter("@P_FVS_ID", vsId);
                paramArr[1] = new SqlParameter("@P_FPAX_ID", paxId);
                paramArr[2] = new SqlParameter("@P_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
                paramArr[3] = new SqlParameter("@P_DISPATCH_NO", dispatchNo);
                paramArr[4] = new SqlParameter("@P_DISPATCH_REMARKS", dispatchRemarks);
                if (!string.IsNullOrEmpty(trackingNo)) paramArr[5] = new SqlParameter("@P_TRACKING_NO", trackingNo);
                if (!string.IsNullOrEmpty(visaNo)) paramArr[6] = new SqlParameter("@P_VISA_NO", visaNo);
                if (VisaIssueDate != DateTime.MinValue) paramArr[7] = new SqlParameter("@P_VISA_ISSUE_DATE", VisaIssueDate);
                if (VisaExpDate != DateTime.MinValue) paramArr[8] = new SqlParameter("@P_VISA_EXP_DATE", VisaExpDate);
                if (VisaCollDate != DateTime.MinValue) paramArr[9] = new SqlParameter("@P_VISA_COLL_DATE", VisaCollDate);
                paramArr[10] = new SqlParameter("@P_DISPATCH_MODIFIED_BY", createdBy);
                if(assignTo > 0)paramArr[11] = new SqlParameter("@P_AssignTo", assignTo);
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[12] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[13] = paramMsgText;

                DBGateway.ExecuteNonQuery("Gv_p_dispatch_update", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

            }
            catch { throw; }
        }
        public static void saveDocDetails(long visaId, long paxId, string docCode, bool docStatus, long docprofileId,long profileDocId)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[6];
                paramArr[0] = new SqlParameter("@P_VisaId", visaId);
                paramArr[1] = new SqlParameter("@P_PaxId", paxId);
                if(!string.IsNullOrEmpty(docCode)) paramArr[2] = new SqlParameter("@P_docCode", docCode);
                paramArr[3] = new SqlParameter("@P_docStatus", docStatus);
                paramArr[4] = new SqlParameter("@P_docProfileId", docprofileId);
                paramArr[5] = new SqlParameter("@P_profileDocId", profileDocId);
                DBGateway.ExecuteNonQuery("gv_p_Visa_Sales_Doc_Add_Update", paramArr);
            }
            catch { throw; }
        }
        public static string GetVerifiedDocStatus(long visaId,long paxId,string docName)
        {
            string status = "";
            SqlConnection connection = DBGateway.GetConnection();
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_VisaId", visaId);
                paramList[1] = new SqlParameter("@P_PaxId", paxId);
                paramList[2] = new SqlParameter("@P_docName", docName);
                SqlDataReader data = DBGateway.ExecuteReaderSP("GV_VerifiedDocStatus", paramList, connection);
                if (data.Read())
                {
                    status = Convert.ToString(data["docStatus"]);
                }
                data.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return status;
        }
        public static DataTable GetDispatchHistory(long visaId, long paxId)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@P_VisaId", visaId);
                    paramList[1] = new SqlParameter("@P_PaxId", paxId);
                    data = DBGateway.ExecuteReaderSP("GV_p_visa_DispatchHistoryList", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public static string GetDocumentNo(int visaId)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            if (visaId > 0) paramList[0] = new SqlParameter("@P_VisaId", visaId);
            DataTable dt = DBGateway.ExecuteQuery("GV_p_visa_DocmentNoBy_VisaId", paramList).Tables[0];
            return dt.Rows[0]["fvs_doc_no"].ToString();
        }
        public static DataTable GetVisaSaleList(DateTime fromDate, DateTime toDate, int agent, string agentType, string country, string nationality, int visaType, string dispatchStatus, int location, int consultant)
        {
            DataTable dtVisaSale = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@fromDate", fromDate);
                paramList[1] = new SqlParameter("@toDate", toDate);
                paramList[2] = new SqlParameter("@agentId", agent);
                if (!string.IsNullOrEmpty(agentType))
                {
                    paramList[3] = new SqlParameter("@agentType", agentType);
                }
                else
                {
                    paramList[3] = new SqlParameter("@agentType", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(country) && country != "-1")
                {
                    paramList[4] = new SqlParameter("@countryCode", country);
                }
                else
                {
                    paramList[4] = new SqlParameter("@countryCode", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(nationality) && nationality != "-1")
                {
                    paramList[5] = new SqlParameter("@nationality", nationality);
                }
                else
                {
                    paramList[5] = new SqlParameter("@nationality", DBNull.Value);
                }
                if (visaType > 0)
                {
                    paramList[6] = new SqlParameter("@visaType", visaType);
                }
                else
                {
                    paramList[6] = new SqlParameter("@visaType", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(dispatchStatus) && dispatchStatus != "-1")
                {
                    paramList[7] = new SqlParameter("@dispatchStatus", dispatchStatus);
                }
                else
                {
                    paramList[7] = new SqlParameter("@dispatchStatus", DBNull.Value);
                }
                if (location > 0)
                {
                    paramList[8] = new SqlParameter("@locationId", location);
                }
                else
                {
                    paramList[8] = new SqlParameter("@locationId", DBNull.Value);
                }
                if (consultant > 0)
                {
                    paramList[9] = new SqlParameter("@consultantId", consultant);
                }
                else
                {
                    paramList[9] = new SqlParameter("@consultantId", DBNull.Value);
                }

                paramList[10] = new SqlParameter("@P_USER_ID", CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID);
                

                dtVisaSale = DBGateway.FillDataTableSP("GV_P_Sales_List", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtVisaSale;
        }

        public static void deleteVisaSaleHeader(long fvsId, long loginAgentId, long tranxAgentId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_fvsId", fvsId);
                paramList[1] = new SqlParameter("@P_loginAgentId", loginAgentId);
                int value = DBGateway.ExecuteNonQuerySP("GV_P_DeleteSalesVisaHeader", paramList);
                if (value < 1)
                {
                    throw new Exception("Updating status failed");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

    public class GVEnquiry
    {
        #region variables
        GVRequirementMaster gvRequirements;
        GVTermsMaster gvTerms;
        List<GVFeeMaster> gvFeeList;
      List<GVHandlingFeeMaster> gvHandlingfee;
        #endregion
        #region properities
        public GVRequirementMaster GvRequirements
        {
            get
            {
                return gvRequirements;
            }

            set
            {
                gvRequirements = value;
            }
        }

        public GVTermsMaster GvTerms
        {
            get
            {
                return gvTerms;
            }

            set
            {
                gvTerms = value;
            }
        }

        public List<GVFeeMaster> GvFeeList
        {
            get
            {
                return gvFeeList;
            }

            set
            {
                gvFeeList = value;
            }
        }
        public List<GVHandlingFeeMaster> GVHandlFee
        {
            get
            {
                return gvHandlingfee;
            }

            set
            {
                gvHandlingfee = value;
            }
        }
        #endregion
    }
}
