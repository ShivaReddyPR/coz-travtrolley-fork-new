﻿using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.GlobalVisa
{
   public class GVHandlingFeeMaster
    {
        private const long NEW_RECORD = -1;
        #region Member Variables
        private long _transactionid;
        private long _agentId;
        private string _countryId;
        private string _nationalityId;
        private long _visaTypeId;
        private string _residenceId;
        private string _visaCategory;
        private decimal _handlingFee;
        private string _status;
        private long _createdBy;
        private DateTime _createdOn;
        #endregion
        #region properties
        public long TransactionId
        {
            get { return _transactionid; }
            set { _transactionid = value; }
        }
        public long AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public string CountryID
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        public string NationalityId
        {
            get { return _nationalityId; }
            set { _nationalityId = value; }
        }
        public long VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId = value; }
        }

        public string ResidenceId
        {
            get { return _residenceId; }
            set { _residenceId = value; }
        }
        public string VisaCategory
        {
            get { return _visaCategory; }
            set { _visaCategory = value; }
        }

        public decimal HandlingFee
        {
            get { return _handlingFee; }
            set { _handlingFee = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }
        #endregion
        #region Constructors
        public GVHandlingFeeMaster()
        {
            _transactionid = NEW_RECORD;
        }
        public GVHandlingFeeMaster(long id)
        {
            _transactionid = id;
            getDetails(_transactionid);
        }


        #endregion
        #region Methods

        private void getDetails(long id)
        {
            try
            {
                DataSet ds = GetData(id);
                UpdateBusinessData(ds);
            }
            catch { throw; }
        }

        private DataSet GetData(long id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_hand_id", id);
                DataSet dsResult = DBGateway.ExecuteQuery("GV_p_visa_Handling_fee_getData", paramList);
                return dsResult;
            }
            catch { throw; }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (_transactionid < 0)
                {
                    _transactionid = Utility.ToLong(dr["hand_id"]);
                }
                else
                {
                    _transactionid = Utility.ToLong(dr["hand_id"]);
                   _agentId = Utility.ToInteger(dr["hand_agentId"]);
                    _countryId = Utility.ToString(dr["hand_country_id"]);
                    _nationalityId = Utility.ToString(dr["hand_nationality_id"]);
                    _visaTypeId = Utility.ToLong(dr["hand_visa_type_id"]);
                    _residenceId = Utility.ToString(dr["hand_residence_id"]);
                    _visaCategory = Utility.ToString(dr["charge_visa_category"]);
                    _handlingFee = Utility.ToDecimal(dr["hand_res_fee"]);
                    _status = Utility.ToString(dr["hand_status"]);
                    _createdBy = Utility.ToLong(dr["hand_created_by"]);
                    _createdOn = Utility.ToDate(dr["hand_created_on"]);
                }
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        UpdateBusinessData(ds.Tables[0].Rows[0]);
                }
            }
            catch { throw; }
        }

        #endregion
        public void Save()
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[11];
                paramlist[0] = new SqlParameter("@P_HAND_ID", _transactionid);
                //paramlist[1] = new SqlParameter("@P_HAND_AGENT_ID", _agentId);
                paramlist[1] = new SqlParameter("@P_HAND_COUNTRY_ID", _countryId);
                if (_nationalityId !="0") paramlist[2] = new SqlParameter("@P_HAND_NATIONALITY_ID", _nationalityId);
                if (_residenceId !="0") paramlist[3] = new SqlParameter("@P_HAND_RESIDENCE_ID", _residenceId);
                if (_visaCategory != "0") paramlist[4] = new SqlParameter("@P_HAND_VISA_CATEGORY", _visaCategory);
                paramlist[5] = new SqlParameter("@P_HAND_VISA_TYPE_ID", _visaTypeId);
                paramlist[6] = new SqlParameter("@P_HAND_STATUS", _status);
                paramlist[7] = new SqlParameter("@P_HAND_CREATED_BY", _createdBy);
                paramlist[8] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramlist[8].Direction = ParameterDirection.Output;
                paramlist[9] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramlist[9].Direction = ParameterDirection.Output;
                paramlist[10] = new SqlParameter("@P_HAND_RES_FEE", _handlingFee);
                DBGateway.ExecuteNonQuery("GV_p_HANDLING_FEE_ADD_UPDATE", paramlist);
                string messageType = Utility.ToString(paramlist[8].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramlist[9].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Static Methods

        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("GV_p_visa_Handling_fee_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion
        public static DataTable GetVisaTypesByCombination(string countryid, string nationalityid, string residenceid, string category)
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[4];
                paramlist[0] = new SqlParameter("@P_HAND_COUNTRY_ID", countryid);
                if (nationalityid != "0") paramlist[1] = new SqlParameter("@P_HAND_NATIONALITY_ID", nationalityid);
                if (residenceid != "0") paramlist[2] = new SqlParameter("@P_HAND_RESIDENCE_ID", residenceid);
                if (!string.IsNullOrEmpty(category)) paramlist[3] = new SqlParameter("@P_HAND_VISA_CATEGORY", category);
                return DBGateway.FillDataTableSP("GV_P_GETVISATYPES_COMBINATIONS", paramlist);
            }
            catch
            {
                throw;
            }
        }
    }
}
