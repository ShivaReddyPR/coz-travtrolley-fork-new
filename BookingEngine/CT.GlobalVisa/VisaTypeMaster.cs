using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.BusinessLayer;

namespace CT.GlobasVisa
{
    public class VisaTypeMaster
    {
        private const long NEW_RECORD = -1;
        #region Member Variables
        long _id;
        string _code;
        string _name;
        string _country_Id;
        int _agent_Id;
        string _status;
        long _created_By;
        #endregion
        #region Properties
        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string CountryId
        {
            get { return _country_Id; }
            set { _country_Id = value; }  
        }
        public int AgentId
        {
            get { return _agent_Id; }
            set { _agent_Id = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _created_By; }
            set { _created_By = value; }
        }

        #endregion
        #region Constructors
        public VisaTypeMaster()
        {
            _id = NEW_RECORD;
        }
        public VisaTypeMaster(long id)
        {
            _id = id;
            Getdetails(_id);

        }
        #endregion

        #region Static Methods
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];  
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);       
                return DBGateway.FillDataTableSP("GV_P_VISA_TYPE_MASTER_GET_LIST", paramList);
            }
            catch
            {
                throw;
            }
        }
        # endregion

        #region Private Methods
        private void Getdetails(long id)
        {
            try
            {
                DataTable dt = GetData(id);
                UpdateBusinessData(dt);
            }
            catch
            {
                throw;
            }
        }
        private DataTable GetData(long id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_VISA_TYPE_ID", id);
                DataTable dt= DBGateway.FillDataTableSP("GV_VISA_TYPE_MASTER_GET_DATA", paramList);
                return dt;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataTable dt)
        {
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["VISA_TYPE_ID"] != DBNull.Value)
                        {
                            _id = Utility.ToLong(dr["VISA_TYPE_ID"]);
                        }
                        if (dr["VISA_TYPE_CODE"] != DBNull.Value)
                        {
                            _code = Utility.ToString(dr["VISA_TYPE_CODE"]);
                        }
                        if (dr["VISA_TYPE_NAME"] != DBNull.Value)
                        {
                            _name = Utility.ToString(dr["VISA_TYPE_NAME"]);
                        }
                        if (dr["VISA_TYPE_COUNTRY_ID"] != DBNull.Value)
                        {
                            _country_Id = Utility.ToString(dr["VISA_TYPE_COUNTRY_ID"]);
                        }
                        if (dr["VISA_TYPE_AGENT_ID"] != DBNull.Value)
                        {
                            _agent_Id = Utility.ToInteger(dr["VISA_TYPE_AGENT_ID"]);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

        }
        #endregion
        #region public Methods
        public void Save()
        {
            try
            {
                SqlParameter[] paramlist=new SqlParameter[8];
                paramlist[0]=new SqlParameter("@P_VISA_TYPE_ID",_id);
                 paramlist[1]=new SqlParameter("@P_VISA_TYPE_CODE",_code);
                 paramlist[2]=new SqlParameter("@P_VISA_TYPE_NAME",_name);
                 paramlist[3]=new SqlParameter("@P_VISA_TYPE_COUNTRY_ID",_country_Id);
               //  paramlist[4]=new SqlParameter("@P_VISA_TYPE_AGENT_ID",_agent_Id);
                 paramlist[4]=new SqlParameter("@P_VISA_TYPE_STATUS",_status);
                 paramlist[5]=new SqlParameter("@P_VISA_TYPE_CREATED_BY",_created_By);
                 paramlist[6]=new SqlParameter("@P_MSG_TYPE",SqlDbType.NVarChar,10);
                 paramlist[6].Direction = ParameterDirection.Output;
                 paramlist[7]=new SqlParameter("@P_MSG_TEXT",SqlDbType.NVarChar,100);
                 paramlist[7].Direction = ParameterDirection.Output;
                 DBGateway.ExecuteNonQuery("GV_VISA_TYPE_MASTER_ADD_UPDATE", paramlist);
                string messagetype=Utility.ToString(paramlist[6].Value);
                if (messagetype == "E")
                {
                    string msg = Utility.ToString(paramlist[7].Value);
                    if (msg != string.Empty) throw new Exception(msg);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region static
        public static DataTable GetCountryByVisaType(string countryCode)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CountryCode", countryCode);
                return DBGateway.ExecuteQuery("GV_P_CountryCodeBy_GetVisaType", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetListBYCountryCode(ListStatus status, RecordStatus recordStatus, string countryId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                if (!string.IsNullOrEmpty(countryId))
                {
                    paramList[0] = new SqlParameter("@P_VISA_TYPE_COUNTRY_ID", countryId);
                }
                else
                {
                    paramList[0] = new SqlParameter("@P_VISA_TYPE_COUNTRY_ID", DBNull.Value);
                }
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("visa_p_visa_type_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }

}
