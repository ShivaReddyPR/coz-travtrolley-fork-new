﻿namespace CT.GlobalVisa
{
    public class GVEnquiryRequest
    {
        #region variables      
        string travelToCountryCode;
        string travelToCountryName;
        int visaTypeId;
        string visaTypeName;
        string nationalityCode;
        string nationalityName;
        string residenseCode;
        string residenseName;
        int adult;
        int child;
        int infant;       
        #endregion
        #region Properties
        public string TravelToCountryCode
        {
            get
            {
                return travelToCountryCode;
            }
            set
            {
                travelToCountryCode = value;
            }

        }
        public string  TravelToCountryName
        {
            get
            {
                return travelToCountryName;
            }
            set
            {
                travelToCountryName = value;
            }
        }
        public int VisaTypeId
        {
            get
            {
                return visaTypeId;
            }
            set
            {
                visaTypeId = value;
            }
        }
        public string VisaTypeName
        {
            get
            {
                return visaTypeName;
            }
            set
            {
                visaTypeName = value;
            }
        }
        public string NationalityCode
        {
            get
            {
                return nationalityCode;
            }
            set
            {
                nationalityCode = value;
            }
        }
        public string NationalityName
        {
            get
            {
                return nationalityName;
            }
            set
            {
                nationalityName = value;
            }
        }
        public string ResidenseCode
        {
            get
            {
                return residenseCode;
            }
            set
            {
                residenseCode = value;
            }
        }
        public string ResidenseName
        {
            get
            {
                return residenseName;
            }
            set
            {
                residenseName = value;
            }
        }
        public int AdultCount
        {
            get
            {
                return adult;
            }
            set
            {
                adult = value;
            }
        }
        public int ChildCount
        {
            get
            {
                return child;
            }
            set
            {
                child = value;
            }
        }
        public int InfantCount
        {
            get
            {
                return infant;
            }
            set
            {
                infant = value;
            }
        }
        #endregion
    }
}
