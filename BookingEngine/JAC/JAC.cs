﻿using System;
using System.Collections.Generic;
using System.Text;
using CT.Configuration;
using CT.Core;
using System.Xml;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Data;  

namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// This class is used to interact with JACApi
    /// HotelBookingSource Value for JAC=15
    /// Following is the steps in Booking Flow for this source
    /// STEP-1:HotelSearch(Availability)
    /// STEP-2:Every 15days(what we configuraed in App.config) Need to download StaticData
    /// STEP-3:Loading Cancellation policy(PreBook)
    /// STEP-4:Booking (Booking)
    /// STEP-5:Cancel (PreCancel)
    /// STEP-6:Cancel
    /// </summary>
    public class JAC:IDisposable
    {
        #region variables
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        protected string XmlPath = string.Empty;
        /// <summary>
        /// loginName will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string loginName = string.Empty;
        /// <summary>
        /// password will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string password = string.Empty;
        /// <summary>
        /// interfaceURL will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string interfaceURL = string.Empty;
        /// <summary>
        /// This variable is used,to store the supplierCurrency
        /// </summary>
        string currency = string.Empty;
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        decimal rateOfExchange = 1;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        string agentCurrency = "";
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        int decimalPoint;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        string sessionId;
        /// <summary>
        /// This variable is used,to store the login userid,which is read from MetaserchEngine  
        /// </summary>
        int appUserId;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        #endregion

        #region properities
        /// <summary>
        /// For storing current agent Base Currency against which conversion need to be applied.
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Exchange rates against agent base currency.
        /// </summary>
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }
        /// <summary>
        ///login UserId
        /// </summary>
        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }

        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        #endregion

        #region constructor
        /// <summary>
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        public JAC()
        {
            loginName = ConfigurationSystem.JACConfig["LoginName"];
            password = ConfigurationSystem.JACConfig["Password"];
            XmlPath = ConfigurationSystem.JACConfig["XmlLogPath"];
            interfaceURL = ConfigurationSystem.JACConfig["InterfaceURL"];
            currency = ConfigurationSystem.JACConfig["Currency"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }
        #endregion

        #region common Methods
        /// <summary>
        /// send a request to the JAC server to get the response
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns>XmlDocument</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private XmlDocument SendRequest(string request)
        {
            //string response = "";
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                // Create the web request  
                HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(interfaceURL);
                // Set type to POST  
                HttpWReq.Method = "POST";
                HttpWReq.ContentType = "application/x-www-form-urlencoded";
                HttpWReq.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                // Create the data we want to send  
                string Data = request;
                StringBuilder data = new StringBuilder();
                data.Append("Data=" + HttpUtility.UrlEncode(Data));
                // Create a byte array of the data we want to send  
                byte[] byteData = UTF8Encoding.UTF8.GetBytes(data.ToString());
                // Set the content length in the request headers  
                HttpWReq.ContentLength = byteData.Length;

                // Write data  
                using (Stream postStream = HttpWReq.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                }

                // get response
                HttpWebResponse HttpWRes = HttpWReq.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(HttpWRes))
                {
                    xmlDoc.Load(dataStream);
                }
                //Reading response
                //Stream dataStream = GetStreamForResponse(HttpWRes);
                //StreamReader reader = new StreamReader(dataStream);

                //response = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("JAC-Failed to get response for request : " + ex.Message, ex);
            }
            return xmlDoc;
        }
        /// <summary>
        ///This method is converting ContentEncoding to Decompress(i.e GZIP, Deflate, default)
        /// </summary>
        /// <param name="webResponse">Response Object</param>
        /// <returns>Stream</returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        #endregion

        #region HotelSearch
        /// <summary>
        /// 
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">his is B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public HotelSearchResult[] GetHotelSearch(HotelRequest req, decimal markup, string markupType)
        {
            HotelSearchResult[] searchRes = new HotelSearchResult[0];

            string request = GenerateHotelSerchRequest(req);
            //Audit.Add(EventType.JACAvailSearch, Severity.Normal, 1, "JAC request message generated", "0");
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_SearchRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACAvailSearch, Severity.High, 0, "Exception returned from JAC.GetHotelSearch Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadSearchResultResponse(xmlResp, req, markup, markupType);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACAvailSearch, Severity.High, 0, "Exception returned from JAC.GetHotelSearch Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return searchRes;
        }

        /// <summary>
        /// This method is convert xmlResponse to HotelSearchResult Object 
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here Only we are calucating B2B Markup and InputVat and also Genarting Hotel Results object</remarks>
        private HotelSearchResult[] ReadSearchResultResponse(XmlDocument xmlDoc, HotelRequest request, decimal markup, string markupType)
        {
            //Loading All Hotels Static data and images city wise
            //optimized Code
            DataTable dtHotels = HotelStaticData.GetStaticHotelIds(request.CityCode, HotelBookingSource.JAC);
            DataTable dtImages = HotelImages.GetImagesByCityCode(request.CityCode, HotelBookingSource.JAC);
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_SearchResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            bool isException = Convert.ToBoolean(xmlDoc.SelectSingleNode("SearchResponse/ReturnStatus/Success").InnerText);
            //checking error info
            if (!isException)
            {
                XmlNode errorInfo = xmlDoc.SelectSingleNode("SearchResponse/ReturnStatus/Success/Exception");
                Audit.Add(EventType.JACAvailSearch, Severity.High, 0, " JAC:ReadSearchResultResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                throw new BookingEngineException("<br>" + errorInfo.InnerText);
            }
            PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
            XmlNodeList hotelList = xmlDoc.SelectNodes("SearchResponse/PropertyResults/PropertyResult");
             HotelSearchResult[] hotelResults = new HotelSearchResult[hotelList.Count];
            string pattern = request.HotelName;
            int hotelCount = 0;
            if (hotelList != null)
            {
                foreach (XmlNode hotelNode in hotelList)
                {
                    HotelSearchResult hotelResult = new HotelSearchResult();
                    hotelResult.StartDate = request.StartDate;
                    hotelResult.EndDate = request.EndDate;
                    hotelResult.BookingSource = HotelBookingSource.JAC;
                    tempNode = hotelNode.SelectSingleNode("PropertyID");
                    if (tempNode != null)
                    {
                        hotelResult.HotelCode = tempNode.InnerText;
                    }
                    tempNode = hotelNode.SelectSingleNode("PropertyName");
                    if (tempNode != null)
                    {
                        hotelResult.HotelName = tempNode.InnerText;
                    }
                    tempNode = hotelNode.SelectSingleNode("OurRating");
                    if (tempNode != null)
                    {
                        hotelResult.Rating = (HotelRating)Convert.ToInt32(Convert.ToDecimal(tempNode.InnerText));
                    }

                    #region search time avoid Staticinfo downloading
                    DataRow[] hotelStaticData = new DataRow[0];
                    try
                    {
                        hotelStaticData = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");
                        if (hotelStaticData != null && hotelStaticData.Length > 0)
                        {
                            hotelResult.HotelDescription = hotelStaticData[0]["description"].ToString();
                            hotelResult.HotelAddress = hotelStaticData[0]["address"].ToString();
                            hotelResult.HotelMap = hotelStaticData[0]["hotelMaps"].ToString();
                            hotelResult.HotelLocation = hotelStaticData[0]["location"].ToString();
                        }
                    }
                    catch { continue; }

                    DataRow[] hotelImages = new DataRow[0];
                    try
                    {
                        hotelImages = dtImages.Select("hotelCode='" + hotelResult.HotelCode + "'");
                        string hImages = (hotelImages != null && hotelImages.Length > 0 ? hotelImages[0]["images"].ToString() : string.Empty);
                        hotelResult.HotelPicture = string.Empty;
                        if (!string.IsNullOrEmpty(hImages))
                        {
                            hotelResult.HotelPicture = hImages.Split('|')[0];
                        }
                    }
                    catch { continue; }
                    #endregion
                    hotelResult.RoomGuest = request.RoomGuest;
                    #region To get only those satisfy the search conditions
                    if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                    {
                        if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    #endregion
                    hotelResult.CityCode = request.CityCode;
                    hotelResult.Currency = currency;
                    XmlNodeList roomNodeList = hotelNode.SelectNodes("RoomTypes/RoomType");
                    hotelResult.RoomGuest = request.RoomGuest;
                    rateOfExchange = exchangeRates[hotelResult.Currency];
                    int index = 0;
                    if (roomNodeList != null && roomNodeList.Count >= request.RoomGuest.Length)
                    {
                        HotelRoomsDetails[] rooms = new HotelRoomsDetails[roomNodeList.Count];
                        foreach (XmlNode roomNode in roomNodeList)
                        {
                            HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                            tempNode = roomNode.SelectSingleNode("Seq");
                            if (tempNode != null)
                            {
                                roomDetail.SequenceNo = tempNode.InnerText;
                                roomDetail.RoomTypeCode = roomDetail.SequenceNo;
                            }
                            tempNode = roomNode.SelectSingleNode("PropertyRoomTypeID");
                            if (tempNode != null)
                            {
                                roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                            }
                            tempNode = roomNode.SelectSingleNode("BookingToken");
                            if (tempNode != null)
                            {
                                roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                            }
                            else
                            {
                                roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + "0";
                            }
                            tempNode = roomNode.SelectSingleNode("MealBasisID");
                            if (tempNode != null)
                            {
                                roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + "|" + tempNode.InnerText;
                            }
                            // RoomTypeCode-------- Seq|PropertyRoomTypeID|BookingToken|MealBasisID
                            roomDetail.RatePlanCode = roomDetail.RoomTypeCode + "|" + hotelResult.HotelCode;
                            // RatePlanCode-------- Seq|PropertyRoomTypeID|BookingToken|MealBasisID|PropertyID

                            tempNode = roomNode.SelectSingleNode("RoomType");
                            if (tempNode != null)
                            {
                                roomDetail.RoomTypeName = tempNode.InnerText;
                            }
                            tempNode = roomNode.SelectSingleNode("MealBasis");
                            if (tempNode != null)
                            {
                                roomDetail.mealPlanDesc = tempNode.InnerText;
                            }
                            List<string> amenities = new List<string>();
                            amenities.Add("No Amenitities Found");
                            roomDetail.Amenities = amenities;
                            tempNode = roomNode.SelectSingleNode("SpecialOfferApplied");
                            if (tempNode != null)
                            {
                                roomDetail.PromoMessage = tempNode.InnerText;
                            }
                            XmlNodeList errataList = roomNode.SelectNodes("Errata/Erratum");
                            string essentialInformation = string.Empty;
                            if (errataList != null)
                            {
                                foreach (XmlNode errata in errataList)
                                {
                                    tempNode = errata.SelectSingleNode("Subject");
                                    if (tempNode != null)
                                    {
                                        if (string.IsNullOrEmpty(essentialInformation))
                                        {
                                            essentialInformation = tempNode.InnerText;
                                        }
                                        else
                                        {
                                            essentialInformation = essentialInformation + "|" + tempNode.InnerText;
                                        }
                                    }
                                    tempNode = errata.SelectSingleNode("Description");
                                    if (tempNode != null)
                                    {
                                        if (string.IsNullOrEmpty(essentialInformation))
                                        {
                                            essentialInformation = tempNode.InnerText;
                                        }
                                        else
                                        {
                                            essentialInformation = essentialInformation + "|" + tempNode.InnerText;
                                        }
                                    }
                                }
                            }
                            roomDetail.EssentialInformation = essentialInformation;
                            #region price

                            //Source given Total Price node only Should be take PublishedFare and NetFare same///
                            // if they give Both Total and RSP nodes at the time PublishedFare Consider Total node and NetFare Consider RSP

                            decimal totPrice = 0m;
                            decimal calcMarkup = 0m;
                            tempNode = roomNode.SelectSingleNode("Total");
                            if (tempNode != null)
                            {
                                roomDetail.supplierPrice = Convert.ToDecimal(tempNode.InnerText);
                                totPrice = Math.Round((roomDetail.supplierPrice * rateOfExchange), decimalPoint);
                                roomDetail.TBOPrice = new PriceAccounts();
                                roomDetail.TBOPrice.PublishedFare = totPrice; //Total price is sving published column
                            }
                            tempNode = roomNode.SelectSingleNode("RSP");
                            if (tempNode != null)
                            {
                                roomDetail.supplierPrice = Convert.ToDecimal(tempNode.InnerText);
                                totPrice = Math.Round((roomDetail.supplierPrice * rateOfExchange), decimalPoint);
                            }
                            //VAT Calucation Changes Modified 14.12.2017
                            decimal hotelTotalPrice = 0m;
                            decimal vatAmount = 0m;
                            hotelTotalPrice = Math.Round(totPrice, decimalPoint);
                            if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                            {
                                hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                            }
                            hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                            roomDetail.TotalPrice = hotelTotalPrice;
                            calcMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                            roomDetail.Markup = calcMarkup;
                            roomDetail.MarkupType = markupType;
                            roomDetail.MarkupValue = markup;
                            roomDetail.SellingFare = roomDetail.TotalPrice;
                            roomDetail.TaxDetail = new PriceTaxDetails();
                            roomDetail.TaxDetail = priceTaxDet;
                            roomDetail.InputVATAmount = vatAmount;

                            hotelResult.Price = new PriceAccounts();
                            hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                            hotelResult.Price.SupplierPrice = totPrice;
                            hotelResult.Price.RateOfExchange = rateOfExchange;

                            //Price Details
                            System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                            RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                            decimal totalprice = roomDetail.TotalPrice;

                            for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                            {
                                decimal price = roomDetail.TotalPrice / diffResult.Days;
                                if (fareIndex == diffResult.Days - 1)
                                {
                                    price = totalprice;
                                }
                                totalprice -= price;
                                hRoomRates[fareIndex].Amount = price;
                                hRoomRates[fareIndex].BaseFare = price;
                                hRoomRates[fareIndex].SellingFare = price;
                                hRoomRates[fareIndex].Totalfare = price;
                                hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                            }
                            roomDetail.Rates = hRoomRates;
                            #endregion

                            rooms[index] = roomDetail;
                            index++;
                        }
                        hotelResult.RoomDetails = rooms;
                    }
                    hotelResult.Currency = agentCurrency;
                    hotelResults[hotelCount] = hotelResult;
                    hotelCount++;
                }
            }
            if (hotelResults.Length > hotelCount)
            {
                Array.Resize(ref hotelResults, hotelCount);
            }

            foreach (HotelSearchResult hotelResult in hotelResults)
            {
                for (int k = 0; k < request.NoOfRooms; k++)
                {
                    for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                    {
                        if (hotelResult.RoomDetails[j].SequenceNo.Contains((k + 1).ToString()))
                        {
                            hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                            hotelResult.Price.NetFare = hotelResult.TotalPrice;
                            hotelResult.Price.AccPriceType = PriceType.NetFare;
                            break;
                        }
                    }
                }
            }
            return hotelResults;
        }

        /// <summary>
        /// This private function is used for generating the Search Request of JAC
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <returns>string</returns>
        //Genarting Search Request
        private string GenerateHotelSerchRequest(HotelRequest req)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("SearchRequest");
            xmlString.WriteStartElement("LoginDetails");
            xmlString.WriteElementString("Login",loginName);
            xmlString.WriteElementString("Password", password);
            xmlString.WriteEndElement();//LoginDetails
            xmlString.WriteStartElement("SearchDetails");
            xmlString.WriteElementString("ArrivalDate", req.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("Duration", req.EndDate.Subtract(req.StartDate).Days.ToString());
            xmlString.WriteElementString("RegionID", req.CityCode);
            xmlString.WriteElementString("MealBasisID", "0");
            xmlString.WriteElementString("MinStarRating", "0");
            xmlString.WriteStartElement("RoomRequests");
            for (int i = 0; i < req.NoOfRooms; i++)
            {
                xmlString.WriteStartElement("RoomRequest");
                xmlString.WriteElementString("Adults", req.RoomGuest[i].noOfAdults.ToString());
                xmlString.WriteElementString("Children", req.RoomGuest[i].noOfChild.ToString());
                if (req.RoomGuest[i].noOfChild > 0)
                {
                    xmlString.WriteStartElement("ChildAges");
                    foreach (int childAge in req.RoomGuest[i].childAge)
                    {
                        xmlString.WriteStartElement("ChildAge");
                        xmlString.WriteElementString("Age", childAge.ToString());
                        xmlString.WriteEndElement();//ChildAge
                    }
                    xmlString.WriteEndElement();//ChildAges
                }
                xmlString.WriteEndElement();//RoomRequest
            }
            xmlString.WriteEndElement();//RoomRequests
            xmlString.WriteEndElement();//SearchDetails
            xmlString.WriteEndElement();//SearchRequest
            xmlString.Close();
            return strWriter.ToString();
        }
        #endregion

        #region HotelStatic Data
        /// <summary>
        /// GetPropertyDetails
        /// </summary>
        /// <param name="cityCode">HotelCityCode</param>
        /// <param name="propertyName">HotelName</param>
        /// <param name="propertyID">HotelCode</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Every 15days (what we configuraed in App.config) Need to download StaticData
        /// here we are checking if static data is not there our DB we are sending static data request and get the response save our database
        /// </remarks>
        public HotelDetails GetPropertyDetails(string cityCode, string propertyName, string propertyID)
        {
            HotelDetails hotelInfo = new HotelDetails();
            try
            {
                HotelStaticData staticInfo = new HotelStaticData();
                HotelImages imageInfo = new HotelImages();
                bool dataAvailable = false, imagesAvailable = false, imagesUpdate = false;
                int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.JACConfig["TimeStamp"]);
                //Loading static data
                staticInfo.Load(propertyID, cityCode, HotelBookingSource.JAC);
                if (staticInfo.HotelName != null && staticInfo.HotelName.Length > 0 && !string.IsNullOrEmpty(staticInfo.HotelDescription) && !string.IsNullOrEmpty(staticInfo.HotelAddress))
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(staticInfo.TimeStamp);
                    //if it is greater than REquired time stamp days then load from DB
                    if (diffRes.Days < timeStampDays)
                    {
                        dataAvailable = true;
                    }
                }
                imageInfo.Load(propertyID, cityCode, HotelBookingSource.JAC);
                if (imageInfo.Images != null && imageInfo.Images.Length > 0)
                {
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(staticInfo.TimeStamp);
                    //if it is greater than REquired time stamp days then load from DB
                    if (diffRes.Days < timeStampDays)
                    {
                        imagesAvailable = true;
                    }
                    else
                    {
                        imagesUpdate = true;
                    }
                }
                if (imagesAvailable)
                {
                    try
                    {
                        hotelInfo.Images = new List<string>();
                        string[] imageData = imageInfo.Images.Split('|');
                        hotelInfo.Image = imageData[0];
                        foreach (string img in imageData)
                        {
                            hotelInfo.Images.Add(img);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.JACDetailsSearch, Severity.High, 1, ex.ToString(), "");
                    }
                }
                if (dataAvailable)
                {
                    //read static data from HTL_HOTEL_DATA 
                    try
                    {
                        hotelInfo.FaxNumber = staticInfo.FaxNumber;
                        hotelInfo.Address = staticInfo.HotelAddress;
                        hotelInfo.HotelCode = staticInfo.HotelCode;
                        hotelInfo.Description = staticInfo.HotelDescription;
                        hotelInfo.HotelFacilities = new List<string>();
                        if (staticInfo.HotelFacilities != null && staticInfo.HotelFacilities.Length > 0)
                        {
                            foreach (string fac in staticInfo.HotelFacilities.Split('|'))
                            {
                                hotelInfo.HotelFacilities.Add(fac);
                            }
                        }

                        hotelInfo.Map = staticInfo.HotelMap;
                        hotelInfo.HotelName = staticInfo.HotelName;
                        hotelInfo.Image = staticInfo.HotelPicture;
                        hotelInfo.HotelPolicy = staticInfo.HotelPolicy;
                        hotelInfo.PhoneNumber = staticInfo.PhoneNumber;
                        hotelInfo.PinCode = staticInfo.PinCode;
                        hotelInfo.hotelRating = staticInfo.Rating;
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.JACDetailsSearch, Severity.High, 1, ex.ToString(), "");
                    }
                }
                if (!dataAvailable || !imagesAvailable)
                {
                    string request = string.Empty;
                    //string resp = string.Empty;
                    XmlDocument xmlResp = new XmlDocument();
                    request = GenerateGetPropertyDetails(propertyID);
                    try
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(request);

                        string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_PropertyDetailsRequest.xml";
                        xmlDoc.Save(filePath);
                    }
                    catch { }
                    try
                    {
                        xmlResp = SendRequest(request);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.JACDetailsSearch, Severity.High, 0, "Exception returned from JAC.GetItemInformation Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                        throw new BookingEngineException("Error: " + ex.Message);
                    }
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        hotelInfo = ReadGetPropertyDetailsResponse(xmlResp);
                    }
                    if (hotelInfo.HotelName != null)
                    {

                        staticInfo.FaxNumber = hotelInfo.FaxNumber;
                        staticInfo.HotelAddress = hotelInfo.Address;
                        staticInfo.HotelCode = propertyID;
                        staticInfo.HotelDescription = hotelInfo.Description;
                        staticInfo.HotelFacilities = "";
                        if (hotelInfo.HotelFacilities != null)
                        {
                            foreach (string fac in hotelInfo.HotelFacilities)
                            {
                                if (!string.IsNullOrEmpty(staticInfo.HotelFacilities))
                                {
                                    staticInfo.HotelFacilities += "|" + fac;
                                }
                                else
                                {
                                    staticInfo.HotelFacilities = fac;
                                }
                            }
                        }
                        staticInfo.HotelMap = hotelInfo.Map;
                        staticInfo.HotelName = hotelInfo.HotelName;
                        staticInfo.HotelPicture = hotelInfo.Image;
                        staticInfo.HotelPolicy = hotelInfo.HotelPolicy;
                        staticInfo.PhoneNumber = hotelInfo.PhoneNumber;
                        staticInfo.PinCode = hotelInfo.PinCode;
                        staticInfo.Rating = hotelInfo.hotelRating;
                        staticInfo.Source = HotelBookingSource.JAC;
                        staticInfo.SpecialAttraction = "";
                        staticInfo.Status = true;
                        staticInfo.TimeStamp = DateTime.Now;
                        staticInfo.CityCode = cityCode;
                        try
                        {
                            staticInfo.Save();
                        }
                        catch { }


                        imageInfo = new HotelImages();
                        imageInfo.CityCode = cityCode;
                        imageInfo.DownloadedImgs = "";
                        foreach (string image in hotelInfo.Images)
                        {
                            if (!string.IsNullOrEmpty(imageInfo.DownloadedImgs))
                            {
                                imageInfo.DownloadedImgs += "|" + image;
                            }
                            else
                            {
                                imageInfo.DownloadedImgs = image;
                            }
                        }
                        imageInfo.HotelCode = propertyID;
                        imageInfo.Images = imageInfo.DownloadedImgs;
                        imageInfo.Source = HotelBookingSource.JAC;
                        try
                        {
                            if (imageInfo.Images != null && imageInfo.Images.Length > 0)
                            {
                                if (!imagesUpdate)
                                {
                                    imageInfo.Save();
                                }
                                else
                                {
                                    imageInfo.Update();
                                }
                            }
                        }
                        catch { }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACDetailsSearch, Severity.High, 0, "Exception returned from JAC.HotelDetails Error Message:" + ex.Message, "");
            }
            return hotelInfo;
        }

        /// <summary>
        ///  Reads the Hotel Detail response XML.
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <returns>HotelDetails</returns>
        private HotelDetails ReadGetPropertyDetailsResponse(XmlDocument xmlDoc)
        {
            HotelDetails hotelDetails = new HotelDetails();
            try
            {
                XmlNode tempNode;
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                try
                {
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_PropertyDetailsResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                bool isException = Convert.ToBoolean(xmlDoc.SelectSingleNode("PropertyDetailsResponse/ReturnStatus/Success").InnerText);
                //checking error info
                if (!isException)
                {
                    XmlNode errorInfo = xmlDoc.SelectSingleNode("PropertyDetailsResponse/ReturnStatus/Success/Exception");
                    Audit.Add(EventType.JACDetailsSearch, Severity.High, 0, " JAC:ReadGetPropertyDetailsResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                    throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }
                XmlNode hotelinfo = xmlDoc.SelectSingleNode("PropertyDetailsResponse");
                if (hotelinfo != null)
                {
                    tempNode = hotelinfo.SelectSingleNode("PropertyID");
                    if (tempNode != null)
                    {
                        hotelDetails.HotelCode = tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("PropertyName");
                    if (tempNode != null)
                    {
                        hotelDetails.HotelName = tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("Rating");
                    if (tempNode != null)
                    {
                        hotelDetails.hotelRating = (HotelRating)Convert.ToInt32(Convert.ToDecimal(tempNode.InnerText));
                    }
                    tempNode = hotelinfo.SelectSingleNode("Address1");
                    if (tempNode != null)
                    {
                        hotelDetails.Address = tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("Address2");
                    if (tempNode != null)
                    {
                        hotelDetails.Address += tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("Postcode");
                    if (tempNode != null)
                    {
                        if (!string.IsNullOrEmpty(tempNode.InnerText))
                        {
                            try
                            {
                                hotelDetails.PinCode = tempNode.InnerText;
                            }
                            catch { }
                        }
                    }
                    tempNode = hotelinfo.SelectSingleNode("Telephone");
                    if (tempNode != null)
                    {
                        hotelDetails.PhoneNumber = tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("Fax");
                    if (tempNode != null)
                    {
                        hotelDetails.FaxNumber = tempNode.InnerText;
                    }
                    //map
                    tempNode = hotelinfo.SelectSingleNode("Latitude");
                    if (tempNode != null)
                    {
                        hotelDetails.Map = tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("Longitude");
                    if (tempNode != null)
                    {
                        hotelDetails.Map = hotelDetails.Map + "|" + tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("Description");
                    if (tempNode != null)
                    {
                        hotelDetails.Description = tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("Description");
                    if (tempNode != null)
                    {
                        hotelDetails.Description = tempNode.InnerText;
                        if (!string.IsNullOrEmpty(hotelDetails.Description))
                        {
                            hotelDetails.Description=hotelDetails.Description.Replace("##","</br>");
                        }
                    }
                    string baseUrl = string.Empty;
                    tempNode = hotelinfo.SelectSingleNode("CMSBaseURL");
                    if (tempNode != null)
                    {
                        baseUrl = tempNode.InnerText;
                    }
                    tempNode = hotelinfo.SelectSingleNode("MainImage");
                    if (tempNode != null)
                    {
                        hotelDetails.Image = baseUrl + "" + tempNode.InnerText;
                    }
                    hotelDetails.Images = new List<string>();
                    XmlNodeList images = hotelinfo.SelectNodes("Images/Image");
                    if (images != null)
                    {
                        foreach (XmlNode img in images)
                        {
                            tempNode = img.SelectSingleNode("Image");
                            if (tempNode != null)
                            {
                                hotelDetails.Images.Add(baseUrl + "" + tempNode.InnerText);
                            }
                        }
                    }
                    hotelDetails.HotelFacilities = new List<string>();
                    XmlNodeList facilitiesList = hotelinfo.SelectNodes("Facilities/Facility");
                    foreach (XmlNode facility in facilitiesList)
                    {
                        tempNode = facility.SelectSingleNode("Facility");
                        if (tempNode != null)
                        {
                            hotelDetails.HotelFacilities.Add(tempNode.InnerText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Jac: Failed to read Hotel Details response ", ex);
            }
            return hotelDetails;
        }

        /// <summary>
        /// This private function is used for Generates the Hotel Detail request.
        /// </summary>
        /// <param name="propertyID">HotelCode</param>
        /// <returns>string</returns>
        private string GenerateGetPropertyDetails(string propertyID)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("PropertyDetailsRequest");
                xmlString.WriteStartElement("LoginDetails");
                xmlString.WriteElementString("Login", loginName);
                xmlString.WriteElementString("Password", password);
                xmlString.WriteEndElement();//LoginDetails
                xmlString.WriteElementString("PropertyID", propertyID);
                xmlString.WriteEndElement();//PropertyDetailsRequest
                xmlString.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Jac: Failed generate PropertyDetails info request for HotelCode : " + propertyID, ex);
            }
            return strWriter.ToString();
        }
        #endregion

        #region PreBook
        /// <summary>
        /// GetHotelChargeCondition(Cancellation policy)
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>Dictionary</returns>
        public Dictionary<string, string> GetHotelChargeCondition(HotelItinerary itinerary)
        {
            Dictionary<string, string> cancelRes = new Dictionary<string, string>();
            string request = GenerateHotelChargeCondition(itinerary);
            //Audit.Add(EventType.JacChargeCondition, Severity.Normal, 1, "JAC charge condition request message generated", "0");
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_PreBookRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JacChargeCondition, Severity.High, 0, "Exception returned from JAC.GetHotelChargeCondition Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    cancelRes = ReadResponseChargeCondition(xmlResp, itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JacChargeCondition, Severity.High, 0, "Exception returned from JAC.GetHotelChargeCondition Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancelRes;
        }

        /// <summary>
        /// ReadResponseChargeCondition(Loading xml response object to dictionary)
        /// </summary>
        /// <param name="xmlDoc">Response XML</param>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> ReadResponseChargeCondition(XmlDocument xmlDoc, HotelItinerary itinerary)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
            try
            {
                XmlNode tempNode;
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                try
                {
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_PreBookResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                bool isException = Convert.ToBoolean(xmlDoc.SelectSingleNode("PreBookResponse/ReturnStatus/Success").InnerText);
                //checking error info
                if (!isException)
                {
                    XmlNode errorInfo = xmlDoc.SelectSingleNode("PreBookResponse/ReturnStatus/Success/Exception");
                    Audit.Add(EventType.JacChargeCondition, Severity.High, 0, " JAC:ReadResponseChargeCondition,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                    throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }

                decimal PreviousTotal = 0;
                for (int k = 0; k < itinerary.Roomtype.Length; k++)
                {
                    PreviousTotal += itinerary.Roomtype[k].Price.SupplierPrice;
                }
                XmlNode preBookingNode = xmlDoc.SelectSingleNode("PreBookResponse");
                if (preBookingNode != null)
                {
                    decimal TotalPrice = 0;
                    tempNode = preBookingNode.SelectSingleNode("TotalPrice");
                    if (tempNode != null)
                    {
                        TotalPrice = (Decimal.Parse(tempNode.InnerText));
                    }
                    //price validation
                    if ((TotalPrice - PreviousTotal) > 1)
                    {
                        itinerary.Roomtype[0].PreviousFare = PreviousTotal * rateOfExchange;
                        itinerary.Roomtype[0].Price.NetFare = TotalPrice * rateOfExchange;
                    }
                    string preBookingToken = string.Empty;
                    tempNode = preBookingNode.SelectSingleNode("PreBookingToken");
                    if (tempNode != null)
                    {
                        preBookingToken = tempNode.InnerText;
                    }
                    XmlNodeList cancellationNodes = preBookingNode.SelectNodes("Cancellations/Cancellation");
                    double buffer = 0;
                    string cancelInfo = "";
                    if (ConfigurationSystem.JACConfig.ContainsKey("Buffer"))
                    {
                        buffer = Convert.ToDouble(ConfigurationSystem.JACConfig["Buffer"]);
                    }
                    rateOfExchange = exchangeRates[currency];
                    if (cancellationNodes != null)
                    {

                        foreach (XmlNode cancelNode in cancellationNodes)
                        {
                            string startDate = string.Empty;
                            string endDate = string.Empty;
                            decimal penality = 0;
                            tempNode = cancelNode.SelectSingleNode("StartDate");
                            if (tempNode != null)
                            {
                                startDate = tempNode.InnerText;
                                startDate = (Convert.ToDateTime(startDate).AddDays(-(buffer))).ToString("dd MMM yyyy HH:mm:ss");
                            }
                            tempNode = cancelNode.SelectSingleNode("EndDate");
                            if (tempNode != null)
                            {
                                endDate = tempNode.InnerText;
                                endDate = (Convert.ToDateTime(endDate).AddDays(-(buffer))).ToString("dd MMM yyyy HH:mm:ss");
                            }
                            tempNode = cancelNode.SelectSingleNode("Penalty");
                            if (tempNode != null)
                            {
                                penality = Convert.ToDecimal(tempNode.InnerText);
                            }
                            penality = Math.Round(penality * rateOfExchange, decimalPoint);
                            if (string.IsNullOrEmpty(cancelInfo))
                            {
                                cancelInfo = "Amount of " + agentCurrency + " " + penality + " will be charged from " + startDate + " to " + endDate;
                            }
                            else
                            {
                                cancelInfo = cancelInfo + "| Amount of " + agentCurrency + " " + penality + " will be charged from " + startDate + " to " + endDate;
                            }
                        }
                        cancellationInfo.Add("lastCancellationDate", itinerary.StartDate.ToString());
                        cancellationInfo.Add("CancelPolicy", cancelInfo);
                        cancellationInfo.Add("PreBookingToken", preBookingToken);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("JAC: Failed to Read cancellation response ", ex);
            }
            return cancellationInfo;
        }

        /// <summary>
        /// This private function is used for Generates the Hotel cancellation poicy request.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateHotelChargeCondition(HotelItinerary itinerary)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("PreBookRequest");
                xmlString.WriteStartElement("LoginDetails");
                xmlString.WriteElementString("Login", loginName);
                xmlString.WriteElementString("Password", password);
                xmlString.WriteEndElement();//LoginDetails
                xmlString.WriteStartElement("BookingDetails");
                xmlString.WriteElementString("PropertyID", itinerary.HotelCode);
                xmlString.WriteElementString("ArrivalDate", itinerary.StartDate.ToString("yyyy-MM-dd"));
                xmlString.WriteElementString("Duration", itinerary.EndDate.Subtract(itinerary.StartDate).Days.ToString());
                xmlString.WriteStartElement("RoomBookings");
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    xmlString.WriteStartElement("RoomBooking");
                    // RoomTypeCode-------- Seq|PropertyRoomTypeID|BookingToken|MealBasisID
                    string[] rtCode = itinerary.Roomtype[i].RoomTypeCode.Split('|');
                    if (rtCode[1] == "0") //checking Jac Hotel or not
                    {
                        xmlString.WriteElementString("PropertyRoomTypeID", rtCode[1]);
                        xmlString.WriteElementString("BookingToken", rtCode[2]);
                    }
                    else
                    {
                        xmlString.WriteElementString("PropertyRoomTypeID", rtCode[1]);
                    }
                    xmlString.WriteElementString("MealBasisID", rtCode[3]);
                    xmlString.WriteElementString("Adults", itinerary.Roomtype[i].AdultCount.ToString());
                    xmlString.WriteElementString("Children", itinerary.Roomtype[i].ChildCount.ToString());
                    if (itinerary.Roomtype[i].ChildCount > 0)
                    {
                        xmlString.WriteStartElement("ChildAges");
                        foreach (int childAge in itinerary.Roomtype[i].ChildAge)
                        {
                            xmlString.WriteStartElement("ChildAge");
                            xmlString.WriteElementString("Age", childAge.ToString());
                            xmlString.WriteEndElement();//ChildAge
                        }
                        xmlString.WriteEndElement();//ChildAges
                    }
                    xmlString.WriteEndElement();//RoomBooking
                }
                xmlString.WriteEndElement();//RoomBookings
                xmlString.WriteEndElement();//BookingDetails
                xmlString.WriteEndElement();//PreBookRequest
                xmlString.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("JAC: Failed generate cancellation info request for Hotel : " + itinerary.HotelName, ex);
            }
            return strWriter.ToString();
        }
        #endregion

        #region Booking
        /// <summary>
        /// GetBooking
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        /// <remarks>Here we are preparing Booking request object and sending book request to api </remarks>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            string request = GenerateBookingRequest(itinerary);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(request);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_BookRequest.xml";
                doc.Save(filePath);
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            BookingResponse searchRes = new BookingResponse();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACBooking, Severity.High, 0, "Exception returned from JAC.GetBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseBooking(xmlResp, itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACBooking, Severity.High, 0, "Exception returned from JAC.GetBooking Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return searchRes;
        }

        /// <summary>
        /// ReadBookingResponse
        /// </summary>
        /// <param name="xmlDoc">Booking response xml</param>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>Here Reading booking response object
        /// here only we need to get confirmationNo
        /// </remarks>
        private BookingResponse ReadResponseBooking(XmlDocument xmlDoc, HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                XmlNode tempNode;
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                try
                {
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_BookResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                bool isException = Convert.ToBoolean(xmlDoc.SelectSingleNode("BookResponse/ReturnStatus/Success").InnerText);
                //checking error info
                if (!isException)
                {
                    XmlNode errorInfo = xmlDoc.SelectSingleNode("BookResponse/ReturnStatus/Success/Exception");
                    Audit.Add(EventType.JACBooking, Severity.High, 0, " JAC:ReadResponseBooking,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                    bookResponse.Status = BookingResponseStatus.Failed;
                    throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }
                XmlNode bookingResponse = xmlDoc.SelectSingleNode("BookResponse");
                if (bookingResponse != null)
                {
                    tempNode = bookingResponse.SelectSingleNode("BookingReference");
                    if (tempNode != null)
                    {
                        itinerary.ConfirmationNo = tempNode.InnerText;
                    }
                    tempNode = bookingResponse.SelectSingleNode("TradeReference");
                    if (tempNode != null)
                    {
                        itinerary.AgencyReference = tempNode.InnerText;
                    }
                    XmlNodeList properityBookins = bookingResponse.SelectNodes("PropertyBookings/PropertyBooking");
                    string suppReference=string.Empty;
                    string supplier = string.Empty;
                    foreach (XmlNode probooking in properityBookins)
                    {
                        tempNode = probooking.SelectSingleNode("Supplier");
                        if (string.IsNullOrEmpty(supplier))
                        {
                            supplier = tempNode.InnerText;
                        }
                        else
                        {
                            supplier = supplier + "," + tempNode.InnerText;
                        }
                        tempNode = probooking.SelectSingleNode("SupplierReference");
                        if (string.IsNullOrEmpty(suppReference))
                        {
                            suppReference = tempNode.InnerText;
                        }
                        else
                        {
                            suppReference = suppReference + "," + tempNode.InnerText;
                        }

                    }
                    itinerary.BookingRefNo = suppReference;
                    bookResponse.ConfirmationNo = itinerary.ConfirmationNo;
                    bookResponse.Status = BookingResponseStatus.Successful;
                    itinerary.Status = HotelBookingStatus.Confirmed;
                    string payableBy = string.Empty;
                    if (!string.IsNullOrEmpty(supplier))
                    {
                        payableBy = "This reservation is booked and payable by " + supplier;
                    }
                    if (!string.IsNullOrEmpty(suppReference) && !string.IsNullOrEmpty(supplier))
                    {
                        payableBy = payableBy + " with reference " + suppReference;
                    }
                    if (!string.IsNullOrEmpty(payableBy))
                    {
                        payableBy = payableBy + "- under no circumstances should the customer be charged for this booking";
                        itinerary.PaymentGuaranteedBy=payableBy;
                    }
                    else
                    {
                        itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: JAC, as per final booking form reference No: " + itinerary.ConfirmationNo;
                    }
                }
                else
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("JAC: Failed to Read booking response ", ex);
            }
            return bookResponse;
        }


        /// <summary>
        /// Generates Hotel Booking Request XML string.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateBookingRequest(HotelItinerary itinerary)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("BookRequest");
                xmlString.WriteStartElement("LoginDetails");
                xmlString.WriteElementString("Login", loginName);
                xmlString.WriteElementString("Password", password);
                xmlString.WriteEndElement();//LoginDetails
                xmlString.WriteStartElement("BookingDetails");
                xmlString.WriteElementString("PropertyID", itinerary.HotelCode);
                xmlString.WriteElementString("PreBookingToken", itinerary.SequenceNumber);
                xmlString.WriteElementString("ArrivalDate", itinerary.StartDate.ToString("yyyy-MM-dd"));
                xmlString.WriteElementString("Duration", itinerary.EndDate.Subtract(itinerary.StartDate).Days.ToString());
                xmlString.WriteElementString("LeadGuestTitle", itinerary.Roomtype[0].PassenegerInfo[0].Title.TrimEnd('.'));
                xmlString.WriteElementString("LeadGuestFirstName", itinerary.Roomtype[0].PassenegerInfo[0].Firstname);
                xmlString.WriteElementString("LeadGuestLastName", itinerary.Roomtype[0].PassenegerInfo[0].Lastname);
                string tradeReference = "CZT" + DateTime.Now.ToString("ddMMyyyyHHmmss");
                xmlString.WriteElementString("TradeReference", tradeReference);
                xmlString.WriteStartElement("RoomBookings");
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    xmlString.WriteStartElement("RoomBooking");
                    // RoomTypeCode-------- Seq|PropertyRoomTypeID|BookingToken|MealBasisID
                    string[] rtCode = itinerary.Roomtype[i].RoomTypeCode.Split('|');
                    if (rtCode[1] == "0") //checking Jac Hotel or not
                    {
                        xmlString.WriteElementString("PropertyRoomTypeID", rtCode[1]);
                        xmlString.WriteElementString("BookingToken", rtCode[2]);
                    }
                    else
                    {
                        xmlString.WriteElementString("PropertyRoomTypeID", rtCode[1]);
                    }
                    xmlString.WriteElementString("MealBasisID", rtCode[3]);
                    xmlString.WriteElementString("Adults", itinerary.Roomtype[i].AdultCount.ToString());
                    xmlString.WriteElementString("Children", itinerary.Roomtype[i].ChildCount.ToString());
                    xmlString.WriteStartElement("Guests");
                    for (int j = 0; j < itinerary.Roomtype[i].PassenegerInfo.Count; j++)
                    {
                        xmlString.WriteStartElement("Guest");
                        xmlString.WriteElementString("Type", itinerary.Roomtype[i].PassenegerInfo[j].PaxType.ToString());
                        xmlString.WriteElementString("Title", itinerary.Roomtype[i].PassenegerInfo[j].Title.TrimEnd('.'));
                        xmlString.WriteElementString("FirstName", itinerary.Roomtype[i].PassenegerInfo[j].Firstname);
                        xmlString.WriteElementString("LastName", itinerary.Roomtype[i].PassenegerInfo[j].Lastname);
                        xmlString.WriteElementString("Age", itinerary.Roomtype[i].PassenegerInfo[j].Age.ToString());
                        xmlString.WriteEndElement();//Guest
                    }
                    xmlString.WriteEndElement();//Guests
                    xmlString.WriteEndElement();//RoomBooking
                }
                xmlString.WriteEndElement();//RoomBookings
                xmlString.WriteEndElement();//BookingDetails
                xmlString.WriteEndElement();//BookRequest
                xmlString.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("JAC: Failed generate booking request for Hotel : " + itinerary.HotelName, ex);
            }
            return strWriter.ToString();
        }
        #endregion

        #region Cancebooking
        /// <summary>
        /// cancel a existing booking
        /// </summary>
        /// <param name="confirmationNo">Booking confirmationNo</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Here we are preparing CancelHotelBooking request object and sending CancelHotelBooking to api</remarks>
        public Dictionary<string, string> CancelHotelBooking(string confirmationNo)
        {
            Dictionary<string, string> cancelRes = new Dictionary<string, string>();
            string request = GeneratePreCanceRequest(confirmationNo);
            //Audit.Add(EventType.JacChargeCondition, Severity.Normal, 1, "JAC charge condition request message generated", "0");
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_PreCancelRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACCancel, Severity.High, 0, "Exception returned from JAC.CancelHotelBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    cancelRes = ReadPreCanceResponse(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACCancel, Severity.High, 0, "Exception returned from JAC.ReadPreCanceResponse Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancelRes;
        }

        /// <summary>
        /// ReadPreCanceResponse
        /// </summary>
        /// <param name="xmlDoc">PreCancel Response xml</param>
        /// <returns>Dictionary</returns>
        /// <remarks>First here we need to take cancel token once get cancel token nee to call cancel status method</remarks>
        private Dictionary<string, string> ReadPreCanceResponse(XmlDocument xmlDoc)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_PreCancelResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            bool isException = Convert.ToBoolean(xmlDoc.SelectSingleNode("PreCancelResponse/ReturnStatus/Success").InnerText);
            //checking error info
            if (!isException)
            {
                XmlNode errorInfo = xmlDoc.SelectSingleNode("PreCancelResponse/ReturnStatus/Success/Exception");
                Audit.Add(EventType.JACCancel, Severity.High, 0, " JAC:ReadPreCanceResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                throw new BookingEngineException("<br>" + errorInfo.InnerText);
            }
            string bookingRefNo = string.Empty;
            decimal cancelCost = 0;
            string cancelToken = string.Empty;
            XmlNode preCancel = xmlDoc.SelectSingleNode("PreCancelResponse");
            if (preCancel != null)
            {
                tempNode = preCancel.SelectSingleNode("BookingReference");
                if (tempNode != null)
                {
                    bookingRefNo = tempNode.InnerText;
                }
                tempNode = preCancel.SelectSingleNode("CancellationCost");
                if (tempNode != null)
                {
                    cancelCost = Convert.ToDecimal(tempNode.InnerText);
                }
                tempNode = preCancel.SelectSingleNode("CancellationToken");
                if (tempNode != null)
                {
                    cancelToken = tempNode.InnerText;
                }
                bool isCancel = false;
                if (!string.IsNullOrEmpty(bookingRefNo) && !string.IsNullOrEmpty(cancelToken))
                {
                    isCancel = GetCancelStatus(bookingRefNo, cancelCost, cancelToken);
                    if (isCancel)
                    {
                        cancellationCharges.Add("Currency", "USD");
                        cancellationCharges.Add("Amount",Convert.ToString(cancelCost));
                        cancellationCharges.Add("Status", "Cancelled");
                        cancellationCharges.Add("ID", cancelToken);
                    }
                    else
                    {
                        cancellationCharges.Add("Status", "Failed");
                    }
                }
                else
                {
                    cancellationCharges.Add("Status", "Failed");
                }
            }
            return cancellationCharges;

        }

        /// <summary>
        /// GetCancelStatus
        /// </summary>
        /// <param name="bookingRefNo">Booking ConfirmationNo</param>
        /// <param name="cancelCost">if booking is Canceled return amount</param>
        /// <param name="cancelToken">what we get preCancelRequest token</param>
        /// <returns>bool</returns>
        /// <remarks>Here we are preparing CancelStatus request object and sending CancelStatus to api</remarks>
        private bool GetCancelStatus(string bookingRefNo, decimal cancelCost, string cancelToken)
        {
            bool isCancel = false;
            string request = GenarateCanStatusRequest(bookingRefNo, cancelCost, cancelToken);
            //Audit.Add(EventType.JacChargeCondition, Severity.Normal, 1, "JAC charge condition request message generated", "0");
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CancelRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACCancel, Severity.High, 0, "Exception returned from JAC.GetCancelStatus Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    isCancel = ReadCanceResponse(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.JACCancel, Severity.High, 0, "Exception returned from JAC.ReadCanceResponse Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return isCancel;
        }

        /// <summary>
        /// ReadCanceResponse
        /// </summary>
        /// <param name="xmlDoc">cancelReponse xml</param>
        /// <returns>bool</returns>
        /// <remarks>Here checking status if status is true booking cancelled else booking not cancelled</remarks>
        private bool ReadCanceResponse(XmlDocument xmlDoc)
        {
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CancelResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            bool isException = Convert.ToBoolean(xmlDoc.SelectSingleNode("CancelResponse/ReturnStatus/Success").InnerText);
            //checking error info
            if (!isException)
            {
                XmlNode errorInfo = xmlDoc.SelectSingleNode("CancelResponse/ReturnStatus/Success/Exception");
                Audit.Add(EventType.JACCancel, Severity.High, 0, " JAC:ReadCanceResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                throw new BookingEngineException("<br>" + errorInfo.InnerText);
            }
            return isException;
        }

        /// <summary>
        /// GenarateCanStatusRequest
        /// </summary>
        /// <param name="bookingRefNo">Booking ConfirmationNo</param>
        /// <param name="cancelCost">if booking is Canceled return amount</param>
        /// <param name="cancelToken">what we get preCancelRequest token</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenarateCanStatusRequest(string bookingRefNo, decimal cancelCost, string cancelToken)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("CancelRequest");
                xmlString.WriteStartElement("LoginDetails");
                xmlString.WriteElementString("Login", loginName);
                xmlString.WriteElementString("Password", password);
                xmlString.WriteEndElement();//LoginDetails
                xmlString.WriteElementString("BookingReference", bookingRefNo);
                xmlString.WriteElementString("CancellationCost", cancelCost.ToString());
                xmlString.WriteElementString("CancellationToken", cancelToken);
                xmlString.WriteEndElement();//CancelRequest
                xmlString.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("JAC: Failed generate CanStatusRequest info request for cancelToken : " + cancelToken, ex);
            }
            return strWriter.ToString();
        }

        /// <summary>
        /// GeneratePreCanceRequest
        /// </summary>
        /// <param name="confirmationNo">Booking ConfirmationNo</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GeneratePreCanceRequest(string confirmationNo)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("PreCancelRequest");
                xmlString.WriteStartElement("LoginDetails");
                xmlString.WriteElementString("Login", loginName);
                xmlString.WriteElementString("Password", password);
                xmlString.WriteEndElement();//LoginDetails
                xmlString.WriteElementString("BookingReference", confirmationNo);
                xmlString.WriteEndElement();//PreCancelRequest
                xmlString.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("JAC: Failed generate PreCanceRequest info request for ConfirmationNo : " + confirmationNo, ex);
            }
            return strWriter.ToString();
        }
        #endregion

        #region IDisposable Support
        /// <summary>
        /// To detect redundant calls
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// disposing
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// <summary>
        ///  override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// </summary>
        ~JAC()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary>
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
