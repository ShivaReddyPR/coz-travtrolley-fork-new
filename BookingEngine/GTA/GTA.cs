﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using CT.Configuration;
using CT.Core;
using System.Data;
using System.IO.Compression;

namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// This class is used to interact with GTA.
    /// HotelBookingSource Value for GTA=2
    /// Following is the steps in Booking Flow for this source
    /// STEP-1:HotelSearch(Availability)
    /// STEP-2:Every 15days(what we configuraed in App.config) Need to download StaticData
    /// STEP-3:Booking (Booking)
    /// STEP-4:Cancel
    /// </summary>
    public class GTA:IDisposable
    {
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        private string XmlPath = string.Empty;
        /// <summary>
        /// clientId will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string clientId = string.Empty;
        /// <summary>
        ///  email will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string email = string.Empty;
        /// <summary>
        ///  password will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string password = string.Empty;
        /// <summary>
        ///  language will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string language = string.Empty;
        /// <summary>
        ///  interfaceURL will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string interfaceURL = string.Empty;
        /// <summary>
        ///  responseURL will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string responseURL = string.Empty;
        /// <summary>
        ///  currency will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string currency = string.Empty;
        /// <summary>
        ///  country will be sent in each and every request, which is read from api configuration file
        /// </summary>
        private string country = string.Empty;
        //private bool testMode = true;
        /// <summary>
        /// StaticData instance created
        /// </summary>
        private StaticData staticInfo = new StaticData();
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        private decimal rateOfExchange = 1;
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        private Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        private int decimalPoint;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        private string agentCurrency;
        /// <summary>
        /// this clientReferenceNo is unquie for every Booking
        /// </summary>
        private string clientReferenceNo = string.Empty;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        string sessionId;

        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int AgentDecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string SessionId
        {
            get
            {
                return sessionId;
            }

            set
            {
                sessionId = value;
            }
        }
        /// <summary>
        /// Creating roomTypes Instance
        /// </summary>
        private List<string> roomTypeDetails = new List<string>();

        /// <summary>
        /// Default Constructor
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        public GTA()
        {
            //if (ConfigurationSystem.GTAConfig["TestMode"].Equals("true"))
            //{
            //    testMode = true;
            //}
            //else
            //{
            //    testMode = false;
            //}
            //Create Hotel Xml Log path folder
            //Create Hotel Xml Log path folder per day wise
            XmlPath = ConfigurationSystem.GTAConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
            //try
            //{
            //    if (!System.IO.Directory.Exists(ConfigurationSystem.GTAConfig["XmlLogPath"]))
            //    {
            //        System.IO.Directory.CreateDirectory(ConfigurationSystem.GTAConfig["XmlLogPath"]);
            //    }
            //}
            //catch { }
            //Create SightSeeing Xml Log path folder
            try
            {
                if (!System.IO.Directory.Exists(ConfigurationSystem.GTAConfig["SightLogPath"]))
                {
                    System.IO.Directory.CreateDirectory(ConfigurationSystem.GTAConfig["SightLogPath"]);
                }
            }
            catch { }

            //Create Transfers Xml Log path folder
            try
            {
                if (!System.IO.Directory.Exists(ConfigurationSystem.GTAConfig["TransLogPath"]))
                {
                    System.IO.Directory.CreateDirectory(ConfigurationSystem.GTAConfig["TransLogPath"]);
                }
            }
            catch { }
            interfaceURL = ConfigurationSystem.GTAConfig["InterfaceURL"];
            clientId = ConfigurationSystem.GTAConfig["ClientID"];
            language = ConfigurationSystem.GTAConfig["lang"];
            email = ConfigurationSystem.GTAConfig["email"];
            password = ConfigurationSystem.GTAConfig["Password"];
            responseURL = ConfigurationSystem.GTAConfig["ResponseURL"];
            country = (ConfigurationSystem.LocaleConfig["CountryCode"] ?? "");
        }
        /// <summary>
        /// Used to send the request XML to the server and pulls the response XML.
        /// </summary>
        /// <param name="requeststring">Request Object</param>
        /// <returns>XmlDocument</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private XmlDocument SendRequest(string requeststring)
        {
            //string responseFromServer = "";
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(interfaceURL);
                request.Method = "POST"; //Using POST method       
                string postData = requeststring;// GETTING XML STRING...
                request.ContentType = contentType;
                //ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //ServicePointManager.DefaultConnectionLimit = 9999;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }

                // handle if response is a compressed stream
                //Stream dataStream = GetStreamForResponse(httpResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //reader.Close();
                //dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("GTA Failed to get response from Hotel Search request : " + ex.Message, ex);
            }
            return xmlDoc;
        }
        #region Common Methods
        /// <summary>
        ///This method is converting ContentEncoding to Decompress(i.e GZIP, Deflate, default)
        /// </summary>
        /// <param name="webResponse">Response Object</param>
        /// <returns>Stream</returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        #endregion
        //public string SendRequest(string request)
        //{
        //    ASCIIEncoding encoding = new ASCIIEncoding();
        //    byte[] @byte = encoding.GetBytes(request);
        //    HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(interfaceURL);
        //    HttpWReq.ContentType = "text/xml";
        //    HttpWReq.ContentLength = (long)request.Length;
        //    HttpWReq.Method = "POST";
        //    Stream StreamData = HttpWReq.GetRequestStream();
        //    StreamWriter writer = new StreamWriter(StreamData);
        //    writer.Write(request);
        //    writer.Flush();
        //    WebResponse HttpWRes = HttpWReq.GetResponse();
        //    Stream receiveStream = HttpWRes.GetResponseStream();
        //    StreamReader reader = new StreamReader(receiveStream);
        //    char[] buff = new char[256];
        //    int count = reader.Read(buff, 0, 256);
        //    StringBuilder response = new StringBuilder();
        //    while (count > 0)
        //    {
        //        string str = new string(buff, 0, count);
        //        response.Append(str);
        //        count = reader.Read(buff, 0, 256);
        //    }
        //    return response.ToString();
        //}
        /// <summary>
        /// Get the Hotels for the destination Search criteria.
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">This is B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <param name="hotelCode">SelectedHotelCode</param>
        /// <param name="roomTypeCode">SelectedRoomCode</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public HotelSearchResult[] GetHotelAvailability(HotelRequest req, decimal markup, string markupType, string hotelCode, string roomTypeCode)
        {
            //Audit.Add(EventType.GTAAvailSearch, Severity.Normal, 1, "GTA.GetHotelAvailability Entered", "0");
            HotelSearchResult[] searchRes = new HotelSearchResult[0];
            HotelSearchResult[] result;
            if (!IsValidRequest(req))
            {
                Audit.Add(EventType.GTAAvailSearch, Severity.Normal, 1, "GTA.GetHotelAvailability Error Message : Either a Room Type is not Available or Pax Limit Exceeds.", "0");
                result = searchRes;
            }
            else
            {
                GTACity cityInfo = new GTACity();
                #region Request string
                //currency = cityInfo.GetCurrencyForCountry(req.CountryName);
                string request = GenerateAvailabilityRequest(req, hotelCode, roomTypeCode);
                try
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(request);
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASearchHotelPriceRequest.xml";
                    XmlDoc.Save(filePath);
                }
                catch { }
                #endregion


                //Audit.Add(EventType.GTAAvailSearch, Severity.Normal, 1, "request message generated", "0");
                //string resp = string.Empty;
                XmlDocument xmlResp = new XmlDocument();
                try
                {
                    xmlResp = SendRequest(request);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
					{
						"Exception returned from GTA.GetHotelAvailability Error Message:",
						ex.Message,
						" | ",
						DateTime.Now,
						"| request XML",
						request,
						"|response XML",
                        xmlResp.OuterXml
                    }), "");
                    ////Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        searchRes = GenerateSearchResult(xmlResp, req, markup, markupType);
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
					{
						"Exception returned from GTA.GetHotelAvailability Error Message:",
						ex.Message,
						" | ",
						DateTime.Now,
						"| request XML",
						request,
						"|response XML",
                        xmlResp.OuterXml
                    }), "");
                    ////Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
				{
					" Response from GTA for Search Request:| ",
					DateTime.Now,
					"| request XML:",
					request,
					"|response XML:",
                    xmlResp.OuterXml
                }), "");
                result = searchRes;
            }
            return result;
        }
        /// <summary>
        /// GetImages
        /// </summary>
        /// <param name="cityCode">Selected CityCode</param>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <returns>List of String</returns>
        public List<string> GetImages(string cityCode, string hotelCode)
        {
            string request = GenerateImageRequest(cityCode, hotelCode);
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = @"" + ConfigurationSystem.GTAConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAGenerateImageRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { throw; }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            List<string> searchRes = new List<string>();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImageSearch, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetImages Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                ////Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseImages(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImageSearch, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetImages Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            return searchRes;
        }
        /// <summary>
        /// GetFareBreakDown
        /// </summary>
        /// <param name="req">req</param>
        /// <param name="itemCode">itemCode</param>
        /// <param name="cityCode">cityCode</param>
        /// <param name="discount">discount</param>
        /// <returns>Dictionary</returns>
        /// <remarks>right now this method is not using</remarks>
        public Dictionary<string, RoomRates[]> GetFareBreakDown(HotelRequest req, string itemCode, string cityCode, ref decimal discount)
        {
            GTACity cityInfo = new GTACity();
            currency = cityInfo.GetCurrencyForCountry(req.CountryName);
            if (currency.Length == 0)
            {
                currency = "USD";
            }
            string request = GenerateFareBreakDown(req, itemCode, cityCode);
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            Dictionary<string, RoomRates[]> fareBreakList = new Dictionary<string, RoomRates[]>();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAFareBreakDown, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetFareBreakDown Error Message:",
                    ex.Message,
                    " | ",
                    DateTime.Now,
                    "| request XML",
                    request,
                    "|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    fareBreakList = ReadResponseFareBreakDown(xmlResp, req, ref discount);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAFareBreakDown, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetFareBreakDown Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            return fareBreakList;
        }
        /// <summary>
        /// GetItemInformation
        /// </summary>
        /// <param name="cityCode">cityCode</param>
        /// <param name="itemName">HotelName</param>
        /// <param name="itemCode">HotelCode</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Every 15days (what we configuraed in App.config) Need to download StaticData
        /// here we are checking if static data is not there our DB we are sending static data request and get the response save our database
        /// </remarks>
        public HotelDetails GetItemInformation(string cityCode, string itemName, string itemCode)
        {
            HotelDetails hotelInfo = default(HotelDetails);
            HotelStaticData staticInfo = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();
            int timeStampDays = Convert.ToInt32(ConfigurationSystem.GTAConfig["TimeStamp"]);
            staticInfo.Load(itemCode, cityCode, HotelBookingSource.GTA);
            bool isUpdateReq = false;
            bool imageUpdate = true;
            bool isReqSend = false;
            imageInfo.Load(itemCode, cityCode, HotelBookingSource.GTA);
            if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
            {
                imageUpdate = false;
            }
            if (staticInfo.HotelName != null && staticInfo.HotelName.Length > 0 && !string.IsNullOrEmpty(staticInfo.HotelAddress) && !string.IsNullOrEmpty(staticInfo.HotelDescription))
            {
                if (DateTime.UtcNow.Subtract(staticInfo.TimeStamp).Days < timeStampDays)
                {
                    isUpdateReq = false;
                    imageUpdate = false;
                    //if (staticInfo.HotelLocation.Length == 0 || Regex.IsMatch(staticInfo.HotelLocation, "(0[1-9]|1[0-9]|2[0-9]|3[0-1])"))
                    //{
                    //    isLocUpdate = true;
                    //}
                    hotelInfo.HotelCode = staticInfo.HotelCode;
                    hotelInfo.HotelName = staticInfo.HotelName;
                    hotelInfo.Map = staticInfo.HotelMap;
                    hotelInfo.Description = staticInfo.HotelDescription;
                    //imageInfo.Load(itemCode, cityCode, HotelBookingSource.GTA);
                    string[] image = new string[0];
                    if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                    {
                        image = imageInfo.DownloadedImgs.Split(new char[]
						{
							'|'
						}, StringSplitOptions.RemoveEmptyEntries);
                    }
                    else
                    {
                        List<string> imgList = new List<string>();
                        try
                        {
                            imgList = GetImages(cityCode, itemCode);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.GTAImageSearch, Severity.High, 0, string.Concat(new object[]
							{
								"Exception returned from GTA.GTAImageSearch Error Message:",
								ex.Message,
								" | ",
								DateTime.Now
							}), string.Empty);
                            //Trace.TraceError("Error: " + ex.Message);
                        }
                        string images = string.Empty;
                        foreach (string img in imgList)
                        {
                            images = images + img + "|";
                        }
                        imageInfo.Images = images;
                        imageInfo.CityCode = cityCode;
                        imageInfo.HotelCode = itemCode;
                        imageInfo.Source = HotelBookingSource.GTA;
                        //string imageData = imageInfo.DownloadImage(imageInfo.Images, HotelBookingSource.GTA);
                        imageInfo.DownloadedImgs = images;
                        imageInfo.Update();
                        //imageInfo.Load(itemCode, cityCode, HotelBookingSource.GTA);
                        //if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                        //{
                        //    image = imageInfo.DownloadedImgs.Split(new char[]
                        //    {
                        //        '|'
                        //    });
                        //}
                        image = images.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    List<string> imageList = new List<string>();
                    string[] array = image;
                    for (int j = 0; j < array.Length; j++)
                    {
                        string img = array[j];
                        imageList.Add(img);
                    }
                    hotelInfo.Images = imageList;
                    if (imageList != null && imageList.Count > 0)
                    {
                        hotelInfo.Image = imageList[0];
                    }
                    else
                    {
                        hotelInfo.Image = string.Empty;
                    }
                    hotelInfo.Address = staticInfo.HotelAddress;
                    hotelInfo.PhoneNumber = staticInfo.PhoneNumber;
                    hotelInfo.PinCode = staticInfo.PinCode;
                    hotelInfo.Email = staticInfo.EMail;
                    hotelInfo.URL = staticInfo.URL;
                    hotelInfo.FaxNumber = staticInfo.FaxNumber;
                    Dictionary<string, string> attractions = new Dictionary<string, string>();
                    string[] attrList = staticInfo.SpecialAttraction.Split(new char[]
					{
						'|'
					});
                    for (int i = 1; i < attrList.Length; i++)
                    {
                        attractions.Add(i.ToString() + ")", attrList[i - 1]);
                    }
                    hotelInfo.Attractions = attractions;
                    string[] hotelFaci = staticInfo.HotelFacilities.Split('|');
                    List<string> facilities = new List<string>();
                    string[] facilList = hotelFaci[0].Split(new char[]
					{
						','
					});
                    array = facilList;
                    for (int j = 0; j < array.Length; j++)
                    {
                        string facl = array[j];
                        facilities.Add(facl);
                    }
                    hotelInfo.HotelFacilities = facilities;
                    List<string> roomFacilities = new List<string>();
                    string[] roomFacilList = hotelFaci[1].Split(new char[]
					{
						','
					});
                    array = roomFacilList;
                    for (int j = 0; j < array.Length; j++)
                    {
                        string roomFacl = array[j];
                        roomFacilities.Add(roomFacl);
                    }
                    hotelInfo.RoomFacilities = roomFacilities;
                    List<string> locList = new List<string>();
                    string[] locations = staticInfo.HotelLocation.Split(new char[]
					{
						'|'
					});
                    for (int i = 0; i < locations.Length - 1; i++)
                    {
                        locList.Add(locations[i]);
                    }
                    hotelInfo.Location = locList;

                }
                else
                {
                    isUpdateReq = true;
                    imageUpdate = true;
                }
            }
            else
            {
                isReqSend = true;
            }
            if (isReqSend || isUpdateReq)
            {

                #region Request string
                string request = GenerateItemRequest(cityCode, itemName, itemCode);
                try
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(request);
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASearchItemInformationRequest.xml";
                    XmlDoc.Save(filePath);
                }
                catch { }
                #endregion
                //string resp = string.Empty;
                XmlDocument xmlResp = new XmlDocument();
                try
                {
                    xmlResp = SendRequest(request);
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        hotelInfo = ReadResponseItemInformation(xmlResp);
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
					{
						"Exception returned from GTA.GetHotelAvailability Error Message:",
						ex.Message,
						" | ",
						DateTime.Now,
						"| request XML",
						request,
						"|response XML",
                        xmlResp.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    if (hotelInfo.HotelName != null)
                    {
                        staticInfo.HotelCode = itemCode;
                        staticInfo.HotelName = hotelInfo.HotelName;
                        staticInfo.HotelAddress = hotelInfo.Address;
                        staticInfo.CityCode = cityCode;
                        List<string> facilities = hotelInfo.HotelFacilities;
                        string facilty = string.Empty;
                        foreach (string facl in facilities)
                        {
                            facilty = facilty + facl + ",";
                        }
                        facilty = facilty + "|";
                        List<string> roomFacilities = hotelInfo.RoomFacilities;
                        foreach (string roomFacl in roomFacilities)
                        {
                            facilty = facilty + roomFacl + ",";
                        }
                        staticInfo.HotelFacilities = facilty;
                        List<string> locations2 = hotelInfo.Location;
                        string location = string.Empty;
                        foreach (string loc in locations2)
                        {
                            location = location + loc + "|";
                        }
                        staticInfo.HotelLocation = location;
                        imageInfo.HotelCode = itemCode;
                        List<string> imgList = hotelInfo.Images;
                        string images = string.Empty;
                        foreach (string image2 in imgList)
                        {
                            images = images + image2 + "|";
                        }
                        imageInfo.Images = images;
                        imageInfo.CityCode = cityCode;
                        imageInfo.Source = HotelBookingSource.GTA;
                        Dictionary<string, string> attrList2 = hotelInfo.Attractions;
                        string attraction = string.Empty;
                        foreach (string attr in attrList2.Keys)
                        {
                            attraction = attraction + attrList2[attr] + "|";
                        }
                        staticInfo.SpecialAttraction = attraction;
                        staticInfo.HotelMap = hotelInfo.Map;
                        staticInfo.HotelDescription = hotelInfo.Description;
                        staticInfo.FaxNumber = hotelInfo.FaxNumber;
                        staticInfo.EMail = hotelInfo.Email;
                        staticInfo.PinCode = hotelInfo.PinCode;
                        staticInfo.PhoneNumber = hotelInfo.PhoneNumber;
                        staticInfo.URL = hotelInfo.URL;
                        imageInfo.DownloadedImgs = images;
                        //string imageData = imageInfo.DownloadImage(imageInfo.Images, HotelBookingSource.GTA);
                        //imageInfo.DownloadedImgs = imageData;
                        //string[] image = new string[0];
                        //if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                        //{
                        //    image = imageInfo.DownloadedImgs.Split(new char[]
                        //    {
                        //        '|'
                        //    });
                        //}
                        //List<string> imageList = new List<string>();
                        //string[] array = image;
                        //for (int j = 0; j < array.Length; j++)
                        //{
                        //    string img = array[j];
                        //    imageList.Add(img);
                        //}
                        //hotelInfo.Images = imgList;
                        if (hotelInfo.Images != null && hotelInfo.Images.Count > 0)
                        {
                            hotelInfo.Image = hotelInfo.Images[0];
                        }
                        else
                        {
                            hotelInfo.Image = string.Empty;
                        }

                        //string[] imageList2 = new string[0];
                        //if (imageData != null && imageData.Length > 0)
                        //{
                        //    imageList2 = imageInfo.DownloadedImgs.Split(new char[]
                        //    {
                        //        '|'
                        //    });
                        //    hotelInfo.Image = imageList2[0];
                        //}
                        //else
                        //{
                        //    hotelInfo.Image = string.Empty;
                        //}
                        staticInfo.HotelPicture = hotelInfo.Image;
                        staticInfo.Source = HotelBookingSource.GTA;
                        staticInfo.Status = true;
                        staticInfo.Save();
                        if (isUpdateReq)
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Update();
                            }
                        }
                        else
                        {
                            if (imageUpdate)
                            {
                                imageInfo.Save();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
					{
						"Exception returned from GTA.GetHotelAvailability Error Message:",
						ex.Message,
						" | ",
						DateTime.Now,
						"| request XML",
						request,
						"|response XML",
                        xmlResp.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            return hotelInfo;
        }
        /// <summary>
        /// GetHotelChargeCondition
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <param name="penalityList">penalityList</param>
        /// <param name="savePenality">savePenality</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method not for use right now  </remarks>
        public Dictionary<string, string> GetHotelChargeCondition(HotelItinerary itineary, ref List<HotelPenality> penalityList, bool savePenality)
        {
            string request = GenerateHotelChargeCondition(itineary); try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(request);
                string filePath = @"" + ConfigurationSystem.GTAConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAChargeConditionsHotelRequest.xml";
                XmlDoc.Save(filePath);
            }
            catch { throw; }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            Dictionary<string, string> searchRes = new Dictionary<string, string>();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetHotelAvailability Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseChargeCondition(xmlResp, itineary.StartDate, ref penalityList, savePenality, itineary.Roomtype.Length);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetHotelAvailability Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            finally
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Cancellation Request and Response from GTA: | ",
					DateTime.Now,
					"| request XML:",
					request,
					"|response XML:",
                    xmlResp.OuterXml
                }), "");
            }
            return searchRes;
        }
        /// <summary>
        /// Used to Book a Hotel using HotelItinerary.
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        /// <remarks>Here we are preparing Booking request object and sending book request to api </remarks>
        public BookingResponse GetBooking(ref HotelItinerary itineary)
        {
            //Trace.TraceInformation("GTAApi.GetBooking entered");
            string requestXml = GenerateBookingRequest(itineary);
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(requestXml);
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAGenerateBookingRequest.xml";
                doc.Save(filePath);
            }
            catch { throw; }
            BookingResponse searchRes = default(BookingResponse);
            //string resp;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(requestXml);
            }
            catch (Exception ex)
            {
                //if getting error while booking time need to send Cancel Added by brahmam 03.07.2018
                CancelHotelBooking(itineary);
                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetBooking Error Message:",
					ex.Message,
					" | Request XML:",
					requestXml,
					DateTime.Now
				}), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
			{
				"Booking Response from GTA. Response:",
                xmlResp.OuterXml,
				" | Request XML:",
				requestXml,
				DateTime.Now
			}), "");
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseBooking(xmlResp, ref itineary);
                }
                else //Getting xml repsonse empty
                {
                    CancelHotelBooking(itineary);
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetBooking Error Message:",
					ex.Message,
					" | Request XML:",
					requestXml,
					"| Response XML :",
                    xmlResp.OuterXml,
					DateTime.Now
				}), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("GTA.GetBooking exiting");
            return searchRes;
        }
        /// <summary>
        /// ModifyBooking
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>This method not for use right now  </remarks>
        public BookingResponse ModifyBooking(HotelItinerary itineary)
        {
            //Trace.TraceInformation("GTAApi.ModifyBooking entered");
            string requestXml = GenerateModifyBookingRequest(itineary);
            BookingResponse searchRes = default(BookingResponse);
            //string resp;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(requestXml);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetBooking Error Message:",
					ex.Message,
					" | Request XML:",
					requestXml,
					DateTime.Now
				}), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
			{
				"Booking Response from GTA. Response:",
                xmlResp.OuterXml,
				" | Request XML:",
				requestXml,
				DateTime.Now
			}), "");
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseModifyBooking(xmlResp, ref itineary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetBooking Error Message:",
					ex.Message,
					" | Request XML:",
					requestXml,
					"| Response XML :",
                    xmlResp.OuterXml,
					DateTime.Now
				}), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("GTA.GetBooking exiting");
            return searchRes;
        }

        /// <summary>
        /// Used to Cancel a Hotel using HotelItinerary.
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>Dictionary</returns>
        public Dictionary<string, string> CancelHotelBooking(HotelItinerary itineary)
        {
            //Trace.TraceInformation("GTA.CancelHotelBooking entered");
            string requestXml = GenerateCancellationRequest(itineary);
            try
            {
                XmlDocument Xmldoc = new XmlDocument();
                Xmldoc.LoadXml(requestXml);
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTACancellationRequest.xml";
                Xmldoc.Save(filePath);
            }
            catch { throw; }
            Dictionary<string, string> searchRes = new Dictionary<string, string>();
            //string resp;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(requestXml);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.CancelHotelBooking Error Message:",
					ex.Message,
					" | Request XML:",
					requestXml,
					DateTime.Now
				}), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.GTACancel, Severity.High, 0, string.Concat(new object[]
			{
				"Booking Response from GTA. Response:",
                xmlResp.OuterXml,
				" | Request XML:",
				requestXml,
				"| Response XML :",
                xmlResp.OuterXml,
				DateTime.Now
			}), "");
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseCancelHotel(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTACancel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.CancelHotelBooking Error Message:",
					ex.Message,
					" | Request XML:",
					requestXml,
					"| Response XML :",
                    xmlResp.OuterXml,
					DateTime.Now
				}), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("GTA.GetBooking exiting");
            return searchRes;
        }
        /// <summary>
        /// GetAOTNumber
        /// </summary>
        /// <param name="countryCode">countryCode</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method not for use right now</remarks>
        public Dictionary<string, string> GetAOTNumber(string countryCode)
        {
            //Trace.TraceInformation("GTA.GetAOTNumber entered");
            string request = GenerateAOTRequest(countryCode);

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(request);
                string filePath = @"" + ConfigurationSystem.GTAConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAGenerateAOTRequest.xml";
                doc.Save(filePath);
            }
            catch { throw; }

            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAAOTNumber, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetAOTNumber Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Dictionary<string, string> AOTNumbers = ReadResponseAOTNumber(xmlResp);
            //Trace.TraceInformation("GTA.GetAOTNumber exit");
            return AOTNumbers;
        }
        /// <summary>
        /// AmendPaxDetails
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <remarks>This method not for use right now</remarks>
        public void AmendPaxDetails(ref HotelItinerary itineary)
        {
            string request = GenerateHotelAmendPaxDetails(itineary);
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.AmendPaxDetails  Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    ReadResponseAmendPaxDetails(xmlResp, ref itineary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.AmendPaxDetails Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            finally
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Amendment Pax details Request and Response from GTA: | ",
					DateTime.Now,
					"| request XML:",
					request,
					"|response XML:",
                    xmlResp.OuterXml
                }), "");
            }
        }
        /// <summary>
        /// ModifyBookingItem
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <remarks>This method not for use right now</remarks>
        public void ModifyBookingItem(ref HotelItinerary itineary)
        {
            GTACity cityInfo = new GTACity();
            currency = cityInfo.GetCurrencyForCountry(cityInfo.GetCountryNameforCityCode(itineary.CityCode));
            string request = GenerateModifyBookingItemRequest(itineary);
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.ModifyBookingItem  Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    ReadResponseModifyBookingItem(xmlResp, ref itineary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.ModifyBookingItem Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            finally
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					"Amendment Pax details Request and Response from GTA: | ",
					DateTime.Now,
					"| request XML:",
					request,
					"|response XML:",
                    xmlResp.OuterXml
                }), "");
            }
        }
        /// <summary>
        /// GetBookedHotel
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>This method not for use right now</returns>
        public bool GetBookedHotel(ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("GTA.GetBookedHotel Entered | " + DateTime.Now);
            bool itineraryFound = false;
            string reqXML = string.Empty;
            //string respXML = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                reqXML = GenerateGetBookedHotelRequest(itinerary.BookingRefNo);
                //respXML = string.Empty;
                
                try
                {
                    xmlResp = SendRequest(reqXML);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAImportHotel, Severity.Low, 0, string.Concat(new object[]
					{
						"GTA.GetBookedHotel Send Request Error Message:",
						ex.Message,
						" | ",
						DateTime.Now,
						"| request XML",
						reqXML,
						"|response XML",
                        xmlResp.OuterXml
                    }), "");
                    //Trace.TraceError("GTA.GetBookedHotel SendRequest error : " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    itineraryFound = ReadResponseSearchBooking(xmlResp, ref itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetBookedHotel ReadResponse Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					reqXML,
					"|response XML",
					xmlResp.OuterXml
				}), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("GTA.GetBookedHotel Exit | " + DateTime.Now);
            return itineraryFound;
        }
        /// <summary>
        /// GetUpdatedStatus
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>bool</returns>
        /// <remarks>This method not for use right now</remarks>
        public bool GetUpdatedStatus(ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("GTA.GetUpdatedStatus Entered | " + DateTime.Now);
            bool itineraryFound = false;
            string reqXML = string.Empty;
            //string respXML = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                reqXML = GenerateGetBookedHotelRequest(itinerary.BookingRefNo);
                //respXML = string.Empty;
                try
                {
                    xmlResp = SendRequest(reqXML);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAImportHotel, Severity.Low, 0, string.Concat(new object[]
					{
						"GTA.GetUpdatedStatus Send Request Error Message:",
						ex.Message,
						" | ",
						DateTime.Now,
						"| request XML",
						reqXML,
						"|response XML",
                        xmlResp.OuterXml
                    }), "");
                    //Trace.TraceError("GTA.GetBookedHotel SendRequest error : " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    itineraryFound = ReadResponseUpdatedStatus(xmlResp, ref itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA:GetUpdatedStatus ReadResponse Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					reqXML,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("GTA.GetUpdatedStatus Exit | " + DateTime.Now);
            return itineraryFound;
        }
        /// <summary>
        /// GetBookedHotelAvailability
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>bool</returns>
        /// <remarks>This method not for use right now</remarks>
        public bool GetBookedHotelAvailability(HotelItinerary itineary)
        {
            GTACity cityInfo = new GTACity();
            if (itineary != null)
            {
                if (itineary.Roomtype != null && itineary.Roomtype.Length > 0)
                {
                    HotelRoom roomInfo = itineary.Roomtype[0];
                    currency = roomInfo.Price.Currency;
                }
            }
            else
            {
                currency = "USD";
            }
            string request = GenerateSearchHotelAvailabilityRequest(itineary);
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            bool isAvail = false;
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetBookedHotelAvailability Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    isAvail = ReadResponseSearchHotelAvailability(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetBookedHotelAvailability Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
					xmlResp.OuterXml
				}), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
			{
				"Request and Response from GTA.GetBookedHotelAvailability: | ",
				DateTime.Now,
				"| request XML",
				request,
				"|response XML",
                xmlResp.OuterXml
            }), "");
            return isAvail;
        }
        /// <summary>
        /// GetItemChargeCondition
        /// </summary>
        /// <param name="bookingRefNo">bookingRefNo</param>
        /// <param name="hPenality">hPenality</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method not for use right now</remarks>
        public Dictionary<string, string> GetItemChargeCondition(string bookingRefNo, ref List<HotelPenality> hPenality)
        {
            //Trace.TraceInformation("GTA.GetItemChargeCondition Entered | " + DateTime.UtcNow);
            string request = GenerateGetItemChargeConditionRequest(bookingRefNo);
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            Dictionary<string, string> searchRes = new Dictionary<string, string>();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetItemChargeCondition Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                //searchRes = ReadResponseChargeCondition(resp, DateTime.Now, ref hPenality, true);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetItemChargeCondition Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            finally
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Cancellation Request and Response from GTA:GetItemChargeCondition | ",
					DateTime.Now,
					"| request XML:",
					request,
					"|response XML:",
                    xmlResp.OuterXml
                }), "");
            }
            //Trace.TraceInformation("GTA.GetItemChargeCondition Exit | " + DateTime.UtcNow);
            return searchRes;
        }
        /// <summary>
        /// GetLocations
        /// </summary>
        /// <param name="cityCode">cityCode</param>
        /// <returns>List String</returns>
        /// <remarks>This method not for use right now</remarks>
        public List<string> GetLocations(string cityCode)
        {
            string request = GenerateLocations(cityCode);
            List<string> locationList = new List<string>();
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetLocations Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    locationList = ReadResponseLocations(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetLocations Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            return locationList;
        }
        /// <summary>
        /// Used in reading response XML and assigning hotel data to the response.
        /// </summary>
        /// <param name="xmlDoc">xml Response Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here Only we are calucating B2B Markup and InputVat and also Genarting Hotel Results object</remarks>
        private HotelSearchResult[] GenerateSearchResult(XmlDocument xmlDoc, HotelRequest request, decimal markup, string markupType)
        {
            //Loading All Hotels Static data and images city wise
            //optimized Code
            DataTable dtHotels = HotelStaticData.GetStaticHotelIds(request.CityCode, HotelBookingSource.GTA);
            DataTable dtImages = HotelImages.GetImagesByCityCode(request.CityCode, HotelBookingSource.GTA);
            HotelSearchResult[] hotelResults;
            //Trace.TraceInformation("GTA.GenerateSearchResult entered");
            HotelSearchResult[] searchResult = new HotelSearchResult[0];
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                try
                {
                    string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASearchHotelPriceResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchHotelPriceResponse/Errors/Error/ErrorText/text()");
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
                    {
                    " GTA:GenerateSearchResult,Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                XmlNodeList hotelList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchHotelPriceResponse/HotelDetails/Hotel");
                hotelResults = new HotelSearchResult[hotelList.Count];
                int i = 0;
                string[] itemList = new string[hotelList.Count];
                string pattern = request.HotelName;
                foreach (XmlNode hotel in hotelList)
                {
                    XmlNode tempNode = hotel.SelectSingleNode("Item/text()");
                    HotelSearchResult hotelResult = new HotelSearchResult();
                    bool hasExtra = false;
                    hotelResult.BookingSource = HotelBookingSource.GTA;
                    for (int j = 0; j < hotel.Attributes.Count; j++)  // checking Hotel node is valid
                    {
                        string text = hotel.Attributes[j].Name.ToString();
                        if (text != null)
                        {
                            if (!(text == "HasExtraInfo"))
                            {
                                if (!(text == "HasMap"))
                                {
                                    if (text == "HasPictures")
                                    {
                                        if (hotel.Attributes[j].Value.ToString() == "true")
                                        {
                                        }
                                    }
                                }
                                else
                                {
                                    if (hotel.Attributes[j].Value.ToString() == "true")
                                    {
                                    }
                                }
                            }
                            else
                            {
                                if (hotel.Attributes[j].Value.ToString() == "true")
                                {
                                    hasExtra = true;
                                }
                            }
                        }
                    }
                    tempNode = hotel.SelectSingleNode("City");
                    if (tempNode != null)
                    {
                        hotelResult.CityCode = tempNode.Attributes[0].Value;
                    }
                    hotelResult.StartDate = request.StartDate;
                    hotelResult.EndDate = request.EndDate;
                    hotelResult.Currency = "AED";
                    tempNode = hotel.SelectSingleNode("Item");
                    if (tempNode != null)
                    {
                       
                        hotelResult.HotelCode = tempNode.Attributes[0].Value;
                        itemList[i] = hotelResult.HotelCode;
                        tempNode = hotel.SelectSingleNode("Item/text()");
                        if (tempNode==null)
                        {
                            continue;
                        }
                        else { 
                        hotelResult.HotelName = tempNode.Value;
                        }
                    }
                    tempNode = hotel.SelectSingleNode("StarRating");
                    if (tempNode != null)
                    {
                        hotelResult.Rating = (HotelRating)Convert.ToInt32(tempNode.InnerText);
                    }
                    #region To get only those satisfy the search conditions
                    if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                    {
                        if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    #endregion
                    //Get RoomCategory Count

                    XmlNodeList roomNodeList = hotel.SelectNodes("RoomCategories/RoomCategory");
                    //hotelResult.RoomDetails = new HotelRoomsDetails[roomNodeList.Count * roomTypeDetails.Count];
                    hotelResult.RoomGuest = request.RoomGuest;
                    HotelRoomsDetails[] rooms = new HotelRoomsDetails[roomNodeList.Count * roomTypeDetails.Count];
                    HotelRoomsDetails roomdetail = default(HotelRoomsDetails);
                    int index = 0;
                    rateOfExchange = exchangeRates[hotelResult.Currency];
                    foreach (XmlNode roomNode in roomNodeList)
                    {
                        for (int k = 0; k < roomTypeDetails.Count; k++)
                        {

                            roomdetail.SequenceNo = (k + 1).ToString();
                            string roomId = roomNode.Attributes[0].Value;
                            List<int> childAgeList = new List<int>();
                            string childAgeString = string.Empty;
                            if (request.RoomGuest[k].childAge != null)
                            {
                                childAgeList = request.RoomGuest[k].childAge;
                            }
                            for (int x = 0; x < childAgeList.Count; x++)
                            {
                                if (x == 0)
                                {
                                    childAgeString = childAgeList[x].ToString();
                                }
                                else
                                {
                                    childAgeString = childAgeString + "," + childAgeList[x].ToString();
                                }
                            }
                            string name = string.Empty;
                            string[] tempList = roomTypeDetails[k].Split(new char[]
                        {
                        '|'
                        });
                            //string text = tempList[0];
                            //if (text != null)
                            //{
                            //    if (!(text == "SB"))
                            //    {
                            //        if (!(text == "DB"))
                            //        {
                            //            if (!(text == "TS"))
                            //            {
                            //                if (!(text == "TR"))
                            //                {
                            //                    if (!(text == "TB"))
                            //                    {
                            //                        if (text == "Q")
                            //                        {
                            //                            name = "Quad Room";
                            //                        }
                            //                    }
                            //                    else
                            //                    {
                            //                        name = "Twin Room";
                            //                    }
                            //                }
                            //                else
                            //                {
                            //                    name = "Triple Room";
                            //                }
                            //            }
                            //            else
                            //            {
                            //                name = "Twin for Sole use";
                            //            }
                            //        }
                            //        else
                            //        {
                            //            name = "Double Room";
                            //        }
                            //    }
                            //    else
                            //    {
                            //        name = "Single Room";
                            //    }
                            //}
                            //if (Convert.ToInt16(tempList[1]) != 0)
                            //{
                            //    name = name + " with " + tempList[1] + " Cot(s)";
                            //}
                            //if (Convert.ToBoolean(Convert.ToInt16(tempList[2])))
                            //{
                            //    if (request.IsMultiRoom)
                            //    {
                            //        name = name + " with Child (" + childAgeString + ")";
                            //    }
                            //    else
                            //    {
                            //        name += " with Extra Bed.";
                            //    }
                            //}
                            //roomdetail.RoomTypeName = name;
                            roomdetail.RoomTypeCode = string.Concat(new string[]
                        {
                        tempList[0],
                        "|",
                        tempList[1],
                        "|",
                        tempList[2],
                        "|",
                        childAgeString,
                        "|",
                        hotelResult.HotelCode,
                        "|",
                        roomId,
                         "|",
                        roomdetail.SequenceNo
                        });
                            //roomdetail.RatePlanCode = tempList[0] + hotelResult.HotelCode; Changed BY brahmam 06.08.218
                            roomdetail.RatePlanCode = roomId;
                            roomdetail.NumberOfCots = Convert.ToInt32(tempList[1]);
                            bool extraBed = Convert.ToBoolean(Convert.ToInt16(tempList[2]));
                            roomdetail.MaxExtraBeds = Convert.ToInt32(Convert.ToInt16(tempList[2]));
                            roomdetail.IsExtraBed = extraBed;
                            tempNode = roomNode.SelectSingleNode("Description");
                            if (tempNode != null)
                            {
                                roomdetail.RoomTypeName = tempNode.InnerText;
                            }
                            tempNode = roomNode.SelectSingleNode("Meals");
                            string MealPlan = string.Empty;
                            if (tempNode.InnerText == "None")
                            {
                                MealPlan = "RoomOnly";
                            }
                            else
                            {
                                MealPlan = tempNode.InnerText;
                            }
                            roomdetail.mealPlanDesc = MealPlan;

                            tempNode = roomNode.SelectSingleNode("Offer");
                            if (tempNode != null)
                            {
                                roomdetail.PromoMessage = tempNode.InnerText;
                            }
                            //roomdetail.EssentialInformation = string.Empty;
                            XmlNodeList EssentialList = roomNode.SelectNodes("EssentialInformation/Information");
                            string EssentialInfo = string.Empty;
                            foreach (XmlNode Essential in EssentialList)
                            {
                                tempNode = Essential.SelectSingleNode("Text");
                                if (tempNode != null)
                                {
                                    if (EssentialInfo == string.Empty)
                                    {
                                        EssentialInfo = ((System.Xml.XmlElement)(Essential).FirstChild).InnerText;
                                    }
                                    else
                                    {
                                        EssentialInfo = EssentialInfo + "|" + ((System.Xml.XmlElement)(Essential).FirstChild).InnerText;
                                    }
                                }
                                tempNode = Essential.SelectSingleNode("DateRange/FromDate");
                                if (tempNode != null)
                                {
                                    EssentialInfo = EssentialInfo + " Valid from  " + Convert.ToDateTime(tempNode.InnerText).ToString("dd MMM yyyy");
                                }
                                tempNode = Essential.SelectSingleNode("DateRange/ToDate");
                                if (tempNode != null)
                                {
                                    EssentialInfo = EssentialInfo + " To  " + Convert.ToDateTime(tempNode.InnerText).ToString("dd MMM yyyy");
                                }
                            }
                            if (EssentialInfo != string.Empty)
                            {
                                roomdetail.EssentialInformation = EssentialInfo;
                            }

                            #region Cancelation Policy
                            //Cancelation Policy 

                            XmlNodeList chargeList = roomNode.SelectNodes("ChargeConditions/ChargeCondition");
                            string message = string.Empty;
                            string lastCancelllation = string.Empty;
                            string amendmentMessage = "Amedment Not Allowed";
                            lastCancelllation = "0";
                            List<HotelPenality> penList = new List<HotelPenality>();
                            foreach (XmlNode charge in chargeList)
                            {
                                if (charge.Attributes[0].Value == "cancellation")
                                {
                                    XmlNodeList individualList = charge.SelectNodes("Condition");
                                    int buffer = Convert.ToInt32(ConfigurationSystem.GTAConfig["buffer"]);
                                    foreach (XmlNode chargeInfo in individualList)
                                    {
                                        bool isCharge = false;
                                        int fromDay = 0;
                                        int toDay = 0;
                                        decimal chargeAmt = 0m;
                                        string curr = string.Empty;
                                        for (int m = 0; m < chargeInfo.Attributes.Count; m++)
                                        {
                                            string text = chargeInfo.Attributes[m].Name.ToString();
                                            if (text != null)
                                            {
                                                if (!(text == "Charge"))
                                                {
                                                    if (!(text == "FromDay"))
                                                    {
                                                        if (!(text == "ToDay"))
                                                        {
                                                            if (!(text == "Currency"))
                                                            {
                                                                if (text == "ChargeAmount")
                                                                {
                                                                    chargeAmt = Convert.ToDecimal(chargeInfo.Attributes[m].Value) / request.NoOfRooms;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                curr = chargeInfo.Attributes[m].Value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            toDay = (int)Convert.ToInt16(chargeInfo.Attributes[m].Value);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        fromDay = (int)Convert.ToInt16(chargeInfo.Attributes[m].Value);
                                                    }
                                                }
                                                else
                                                {
                                                    isCharge = Convert.ToBoolean(chargeInfo.Attributes[m].Value);
                                                }
                                            }

                                        }
                                        if (isCharge)
                                        {
                                            rateOfExchange = exchangeRates[curr];
                                            //chargeAmt = Math.Ceiling(chargeAmt * rateOfExchange);
                                            chargeAmt = Convert.ToDecimal((chargeAmt * rateOfExchange).ToString("N" + decimalPoint));
                                            chargeAmt += chargeAmt * Convert.ToDecimal(ConfigurationSystem.GTAConfig["CancellationMarkup"]) / 100m;
                                            string symbol = agentCurrency;
                                            //string symbol = GetCurrencySymbol(curr);
                                            if (fromDay == toDay)
                                            {
                                                //int extraDays = 0;
                                                object obj = message;
                                                message = string.Concat(new object[]
                                    {
                                    obj,
                                    //DateTime.Now.ToString("dd MMM yyyy"),
                                    //" onwards, ",
                                    //symbol,
                                    //" ",
                                    //chargeAmt,
									"Non-refundable and full charge will apply once booking is completed."
                                    });
                                            }
                                            if (fromDay != toDay && fromDay == 0 && toDay > 0)
                                            {
                                                int extraDays = 0;
                                                object obj = message;
                                                message = string.Concat(new object[]
                                    {
                                    obj,request.StartDate.AddDays((double)(-(double)(toDay + buffer + extraDays))).ToString("dd/MMM/yyyy")," onwards You will be charged ",
                                    symbol,
                                    " ",
                                    chargeAmt
                                    });
                                            }
                                            else
                                            {
                                                if (fromDay != toDay)
                                                {
                                                    int extraDays = 0;
                                                    object obj = message;
                                                    message = string.Concat(new object[]
                                        {
                                        obj,request.StartDate.AddDays((double)(-(double)(fromDay + buffer + extraDays))).ToString("dd/MMM/yyyy")," earlier  You will be charged ",
                                    symbol,
                                    " ",
                                    chargeAmt
                                        });
                                                }
                                            }
                                            message += "|";
                                        }
                                    }
                                }
                                else
                                {
                                    if (charge.Attributes[0].Value == "amendment")//Amendent buffer is not being applied from GTAConfig.XML as the same is yet to be implemented
                                    {
                                        amendmentMessage = string.Empty;
                                        XmlNodeList individualList = charge.SelectNodes("Condition");
                                        int buffer = Convert.ToInt32(ConfigurationSystem.GTAConfig["amendmentBuffer"]);
                                        foreach (XmlNode chargeInfo in individualList)
                                        {
                                            HotelPenality penalityInfo = new HotelPenality();
                                            penalityInfo.BookingSource = HotelBookingSource.GTA;
                                            penalityInfo.PolicyType = ChangePoicyType.Amendment;
                                            bool isCharge = false;
                                            bool isAllowed = true;
                                            int fromDay = 0;
                                            int toDay = -1;
                                            decimal chargeAmt = 0m;
                                            string curr = string.Empty;
                                            for (int m = 0; m < chargeInfo.Attributes.Count; m++)
                                            {
                                                string text = chargeInfo.Attributes[m].Name.ToString();
                                                if (text != null)
                                                {
                                                    if (!(text == "Charge"))
                                                    {
                                                        if (!(text == "FromDay"))
                                                        {
                                                            if (!(text == "ToDay"))
                                                            {
                                                                if (!(text == "Currency"))
                                                                {
                                                                    if (!(text == "ChargeAmount"))
                                                                    {
                                                                        if (text == "Allowable")
                                                                        {
                                                                            isAllowed = Convert.ToBoolean(chargeInfo.Attributes[m].Value);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        chargeAmt = Convert.ToDecimal(chargeInfo.Attributes[m].Value) / request.NoOfRooms;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    curr = chargeInfo.Attributes[m].Value;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                toDay = (int)Convert.ToInt16(chargeInfo.Attributes[m].Value);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            fromDay = (int)Convert.ToInt16(chargeInfo.Attributes[m].Value);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        isCharge = Convert.ToBoolean(chargeInfo.Attributes[m].Value);
                                                    }
                                                }
                                            }
                                            if (isAllowed && isCharge)
                                            {
                                                rateOfExchange = exchangeRates[curr];
                                                chargeAmt = Math.Ceiling(chargeAmt * rateOfExchange);
                                                chargeAmt += chargeAmt * Convert.ToDecimal(ConfigurationSystem.GTAConfig["CancellationMarkup"]) / 100m;
                                                chargeAmt = Math.Round(chargeAmt, Convert.ToInt32(ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                                                string symbol = agentCurrency;
                                                //string symbol = GetCurrencySymbol(curr);
                                                if (fromDay == toDay)
                                                {
                                                    int extraDays = 0;
                                                    if (request.StartDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                                    {
                                                        extraDays = 1;
                                                    }
                                                    else
                                                    {
                                                        if (request.StartDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                                        {
                                                            extraDays = 2;
                                                        }
                                                    }
                                                    penalityInfo.FromDate = request.StartDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                                    penalityInfo.ToDate = request.StartDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                                    object obj = amendmentMessage;
                                                    amendmentMessage = string.Concat(new object[]
                                        {
                                        obj,
                                        "Amendment upto ",
                                        fromDay + buffer + extraDays,
                                        " day(s) before the date of arrival, ",
                                        symbol,
                                        " ",
                                        chargeAmt,
                                        " will be charged."
                                        });
                                                    penalityInfo.Remarks = string.Concat(new object[]
                                        {
                                        "Amendment upto ",
                                        fromDay + buffer + extraDays,
                                        " day(s) before the date of arrival, ",
                                        symbol,
                                        " ",
                                        chargeAmt,
                                        " will be charged."
                                        });
                                                }
                                                if (fromDay != toDay && fromDay == 0 && toDay > 0)
                                                {
                                                    int extraDays = 0;
                                                    if (request.StartDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                                    {
                                                        extraDays = 1;
                                                    }
                                                    else
                                                    {
                                                        if (request.StartDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                                        {
                                                            extraDays = 2;
                                                        }
                                                    }
                                                    penalityInfo.FromDate = request.StartDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                                    penalityInfo.ToDate = request.StartDate.AddDays((double)(-(double)(toDay + buffer + extraDays)));
                                                    object obj = amendmentMessage;
                                                    amendmentMessage = string.Concat(new object[]
                                        {
                                        obj,
                                        "Amendment upto ",
                                        toDay + buffer + extraDays,
                                        " day(s) before the date of arrival, ",
                                        symbol,
                                        " ",
                                        chargeAmt,
                                        " will be charged."
                                        });
                                                    penalityInfo.Remarks = string.Concat(new object[]
                                        {
                                        "Amendment upto ",
                                        toDay + buffer + extraDays,
                                        " day(s) before the date of arrival, ",
                                        symbol,
                                        " ",
                                        chargeAmt,
                                        " will be charged."
                                        });
                                                }
                                                else
                                                {
                                                    if (fromDay != toDay)
                                                    {
                                                        int extraDays = 0;
                                                        if (request.StartDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                                        {
                                                            extraDays = 1;
                                                        }
                                                        else
                                                        {
                                                            if (request.StartDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                                            {
                                                                extraDays = 2;
                                                            }
                                                        }
                                                        if (toDay == -1)
                                                        {
                                                            penalityInfo.FromDate = DateTime.Now;
                                                            penalityInfo.ToDate = request.StartDate;
                                                        }
                                                        else
                                                        {
                                                            penalityInfo.FromDate = request.StartDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                                            penalityInfo.ToDate = request.StartDate.AddDays((double)(-(double)(toDay + buffer + extraDays)));
                                                        }
                                                        object obj = amendmentMessage;
                                                        amendmentMessage = string.Concat(new object[]
                                            {
                                            obj,
                                            "Amendment from ",
                                            fromDay + buffer + extraDays,
                                            " days to ",
                                            toDay + buffer,
                                            " days before the date of arrival, ",
                                            symbol,
                                            " ",
                                            chargeAmt,
                                            " will be charged."
                                            });
                                                        penalityInfo.Remarks = string.Concat(new object[]
                                            {
                                            "Amendment from ",
                                            fromDay + buffer + extraDays,
                                            " days to ",
                                            toDay + buffer,
                                            " days before the date of arrival, ",
                                            symbol,
                                            " ",
                                            chargeAmt,
                                            " will be charged."
                                            });
                                                    }
                                                }
                                                penalityInfo.Price = chargeAmt;
                                                penalityInfo.IsAllowed = isAllowed;
                                                penList.Add(penalityInfo);
                                                amendmentMessage += "|";
                                            }
                                            else
                                            {
                                                penalityInfo.IsAllowed = isAllowed;
                                                penalityInfo.FromDate = DateTime.Now;
                                                penalityInfo.ToDate = request.StartDate;
                                                penalityInfo.Price = 0m;
                                                if (isAllowed)
                                                {
                                                    penalityInfo.Remarks = "No Amendment Charges before " + (fromDay + buffer) + " days of CheckIn.";
                                                }
                                                else
                                                {
                                                    penalityInfo.Remarks = "Amendment is Not Allowed.";
                                                }
                                                amendmentMessage = amendmentMessage + "|" + penalityInfo.Remarks;
                                                penList.Add(penalityInfo);
                                            }
                                        }
                                    }
                                }
                            }
                            roomdetail.CancellationPolicy = message + "@#" + amendmentMessage;
                            #endregion

                            tempNode = roomNode.SelectSingleNode("SharingBedding");
                            if (tempNode != null)
                            {
                                roomdetail.SharingBedding = Convert.ToBoolean(tempNode.InnerText);
                            }
                            else
                            {
                                roomdetail.SharingBedding = false;
                            }
                            //tempNode = roomNode.SelectSingleNode("Meals/Basis/text()");
                            //List<string> amList = new List<string>();
                            //if (tempNode != null)
                            //{
                            //    amList.Add(tempNode.Value);
                            //}
                            hotelResult.Price = new PriceAccounts
                            {
                                AccPriceType = PriceType.NetFare
                            };

                            tempNode = roomNode.SelectSingleNode("ItemPrice");
                            decimal totPrice = 0m;
                            if (tempNode != null)
                            {
                                totPrice = Convert.ToDecimal(tempNode.InnerText);
                            }


                            //StaticData staticInfo = new StaticData();
                            //Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
                            //rateOfExList = staticInfo.CurrencyROE;

                            //if (hotelResult.Currency != "AED")
                            //{
                            //    roomdetail.TotalPrice = Math.Round((totPrice / roomTypeDetails.Count) * rateOfExchange,agentCurrency);
                            //    roomdetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomdetail.TotalPrice) * (markup / 100m)));
                            //    roomdetail.MarkupType = markupType;
                            //    roomdetail.MarkupValue = markup;
                            //    roomdetail.SellingFare = roomdetail.TotalPrice;
                            //}
                            //else
                            {
                                //VAT Calucation Changes Modified 14.12.2017
                                decimal hotelTotalPrice = 0m;
                                decimal vatAmount = 0m;
                                hotelTotalPrice = Math.Round((totPrice / roomTypeDetails.Count) * rateOfExchange, decimalPoint);

                                if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                {
                                    hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                }
                                hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                                roomdetail.TotalPrice = hotelTotalPrice;
                                roomdetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomdetail.TotalPrice) * (markup / 100m)));
                                roomdetail.MarkupType = markupType;
                                roomdetail.MarkupValue = markup;
                                roomdetail.SellingFare = roomdetail.TotalPrice;
                                roomdetail.supplierPrice = Math.Round((totPrice / roomTypeDetails.Count));
                                roomdetail.TaxDetail = new PriceTaxDetails();
                                roomdetail.TaxDetail = priceTaxDet;
                                roomdetail.InputVATAmount = vatAmount;
                                hotelResult.Price = new PriceAccounts();
                                hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                hotelResult.Price.SupplierPrice = Math.Round((totPrice / roomTypeDetails.Count));
                                hotelResult.Price.RateOfExchange = rateOfExchange;
                            }

                            System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                            RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                            decimal totalprice = roomdetail.TotalPrice;

                            for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                            {
                                decimal price = roomdetail.TotalPrice / diffResult.Days;
                                if (fareIndex == diffResult.Days - 1)
                                {
                                    price = totalprice;
                                }
                                totalprice -= price;
                                hRoomRates[fareIndex].Amount = price;
                                hRoomRates[fareIndex].BaseFare = price;
                                hRoomRates[fareIndex].SellingFare = price;
                                hRoomRates[fareIndex].Totalfare = price;
                                hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);

                            }


                            //TimeSpan diffResult = request.EndDate.Subtract(request.StartDate);
                            //RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                            //for (int j = 0; j < diffResult.Days; j++)
                            //{
                            //    hRoomRates[j].Amount = totPrice / (diffResult.Days * roomTypeDetails.Count);
                            //    hRoomRates[j].BaseFare = totPrice / (diffResult.Days * roomTypeDetails.Count);
                            //    hRoomRates[j].SellingFare = totPrice / (diffResult.Days * roomTypeDetails.Count);
                            //    hRoomRates[j].Days = request.StartDate.AddDays((double)(j + 1));
                            //    hRoomRates[j].RateType = RateType.Negotiated;

                            //}
                            roomdetail.Rates = hRoomRates;
                            //roomdetail.TotalPrice = totPrice / roomTypeDetails.Count;
                            //roomdetail.RatePlanCode = tempList[0] + hotelResult.HotelCode;
                            //roomdetail.Amenities = amList;
                            //roomdetail.SellingFare = totPrice / roomTypeDetails.Count;
                            rooms[index] = roomdetail;
                            index++;
                        }
                    }
                    hotelResult.Currency = agentCurrency;
                    hotelResult.RoomDetails = rooms;
                    if (hasExtra)
                    {
                        #region Modified by brahmam search time avoid Staticinfo downloading
                        //HotelDetails hotelInfo = default(HotelDetails);
                        //hotelInfo = GetItemInformation(hotelResult.CityCode, hotelResult.HotelName, hotelResult.HotelCode);
                        //if (string.IsNullOrEmpty(hotelInfo.HotelName))
                        //{
                        //    Audit.Add(EventType.GTAItemSearch, Severity.High, 1, "Hotel Detail not found for GTA : cityCode = " + hotelResult.CityCode + " itemCode = " + hotelResult.HotelCode, "0");
                        //    continue;
                        //}
                        DataRow[] hotelStaticData = new DataRow[0];
                        try
                        {
                            hotelStaticData = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");
                            if (hotelStaticData != null && hotelStaticData.Length > 0)
                            {
                                hotelResult.HotelDescription = hotelStaticData[0]["description"].ToString();
                                hotelResult.HotelAddress = hotelStaticData[0]["address"].ToString();
                                hotelResult.HotelMap = hotelStaticData[0]["hotelMaps"].ToString();
                                hotelResult.HotelLocation = hotelStaticData[0]["location"].ToString();
                            }
                        }
                        catch { continue; }

                        DataRow[] hotelImages = new DataRow[0];
                        try
                        {
                            hotelImages = dtImages.Select("hotelCode='" + hotelResult.HotelCode + "'");
                            string hImages = (hotelImages != null && hotelImages.Length > 0 ? hotelImages[0]["images"].ToString() : string.Empty);
                            hotelResult.HotelPicture = string.Empty;
                            if (!string.IsNullOrEmpty(hImages))
                            {
                                hotelResult.HotelPicture = hImages.Split('|')[0];
                            }
                        }
                        catch { continue; }

                        #endregion
                    }
                    hotelResults[i++] = hotelResult;
                }

                if (hotelResults.Length > i)
                {
                    Array.Resize(ref hotelResults, i);
                }

                foreach (HotelSearchResult hotelResult in hotelResults)
                {
                    //hotelResult.Price = new PriceAccounts();
                    for (int k = 0; k < request.NoOfRooms; k++)
                    {
                        for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                        {
                            if (hotelResult.RoomDetails[j].SequenceNo.Contains((k + 1).ToString()))
                            {
                                hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                hotelResult.Price.AccPriceType = PriceType.NetFare;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at reading GTA search results:" + ex.Message, "");
                throw ex;
            }
            //Array.Resize<HotelSearchResult>(ref hotelResults, i);
            return hotelResults;
        }
        /// <summary>
        /// This private function is used for Generates the Hotel Search request.
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="hotelCode">HotelCode</param>
        /// <param name="roomTypeCode">roomTypeCode</param>
        /// <returns>string</returns>
        private string GenerateAvailabilityRequest(HotelRequest req, string hotelCode, string roomTypeCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", "AED");
            xmlString.WriteAttributeString("Country", req.PassengerNationality);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchHotelPriceRequest");
            xmlString.WriteStartElement("ItemDestination");
            if (req.SearchByArea)
            {
                xmlString.WriteAttributeString("DestinationType", "area");
                xmlString.WriteAttributeString("DestinationCode", req.CityCode);
            }
            else
            {
                xmlString.WriteAttributeString("DestinationType", "city");
                xmlString.WriteAttributeString("DestinationCode", req.CityCode);
            }
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ImmediateConfirmationOnly");
            xmlString.WriteEndElement();
            if (hotelCode != null && hotelCode.Length > 0)
            {
                xmlString.WriteStartElement("ItemCode");
                xmlString.WriteCData(hotelCode);
                xmlString.WriteEndElement();
            }
            xmlString.WriteStartElement("PeriodOfStay");
            xmlString.WriteElementString("CheckInDate", req.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("Duration", req.EndDate.Subtract(req.StartDate).Days.ToString());
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("IncludeChargeConditions");
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("Rooms");
            roomTypeDetails = GetRoomType(req);
            for (int i = 0; i < req.NoOfRooms; i++)
            {
                string[] tempList = roomTypeDetails[i].Split(new char[]
				{
					'|'
				});
                string type = tempList[0];
                int noOfCots = (int)Convert.ToInt16(tempList[1]);
                bool extraBed = Convert.ToBoolean(Convert.ToInt16(tempList[2]));
                xmlString.WriteStartElement("Room");
                xmlString.WriteAttributeString("Code", type);
                if (roomTypeCode != null && roomTypeCode.Length > 0)
                {
                    xmlString.WriteAttributeString("Id", roomTypeCode);
                }
                xmlString.WriteAttributeString("NumberOfRooms", "1");
                xmlString.WriteAttributeString("NumberOfCots", noOfCots.ToString());
                if (req.RoomGuest[i].noOfChild > 0 && extraBed)
                {
                    xmlString.WriteStartElement("ExtraBeds");
                    for (int j = 0; j < req.RoomGuest[i].noOfChild; j++)
                    {
                        int childAge = req.RoomGuest[i].childAge[j];
                        if (childAge > 2)
                        {
                            xmlString.WriteElementString("Age", childAge.ToString());
                        }
                    }
                    xmlString.WriteEndElement();
                    //int childAge = req.RoomGuest[i].childAge[0];
                    //if (req.RoomGuest[i].noOfChild == 2)
                    //{
                    //    if (req.RoomGuest[i].childAge[1] > req.RoomGuest[i].childAge[0])
                    //    {
                    //        childAge = req.RoomGuest[i].childAge[1];
                    //    }
                    //}

                }
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("StarRatingRange");
            xmlString.WriteElementString("Min", req.MinRating.ToString());
            xmlString.WriteElementString("Max", req.MaxRating.ToString());
            xmlString.WriteEndElement();
            xmlString.WriteElementString("OrderBy", "pricelowToHigh");
            xmlString.WriteElementString("NumberOfReturnedItems", ConfigurationSystem.ResultCountBySources["GTA"].ToString());
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// This private function is used for Generates the Hotel Details request.
        /// </summary>
        /// <param name="cityCode">cityCode</param>
        /// <param name="itemName">itemName</param>
        /// <param name="itemCode">itemCode</param>
        /// <returns>string</returns>
        private string GenerateItemRequest(string cityCode, string itemName, string itemCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchItemInformationRequest");
            xmlString.WriteAttributeString("ItemType", "hotel");
            xmlString.WriteStartElement("ItemDestination");
            xmlString.WriteAttributeString("DestinationType", "city");
            xmlString.WriteAttributeString("DestinationCode", cityCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemName");
            xmlString.WriteString(itemName);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemCode");
            xmlString.WriteString(itemCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// Used in reading response XML and assigning static data to the HotelDetails.
        /// </summary>
        /// <param name="xmlDoc">Response XmlDocument</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Here only we are Reading all static data</remarks>
        private HotelDetails ReadResponseItemInformation(XmlDocument xmlDoc)
        {
            //Trace.TraceInformation("GTA.ReadResponseItemInformation entered");
            HotelSearchResult[] searchResult = new HotelSearchResult[0];
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASearchItemInformationResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchHotelPriceResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
				{
					" GTA:ReadResponseItemInformation,Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            HotelDetails hotel = default(HotelDetails);
            XmlNode itemInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails");
            if (itemInfo != null && itemInfo.InnerText.Length > 0)
            {
                string cityCode = string.Empty;
                string hotelCode = string.Empty;
                XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/City");
                if (tempNode != null)
                {
                    cityCode = tempNode.Attributes[0].Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/Item");
                if (tempNode != null)
                {
                    hotelCode = tempNode.Attributes[0].Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/Item/text()");
                if (tempNode != null)
                {
                    hotel.HotelName = tempNode.Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/AddressLines/AddressLine1/text()");
                if (tempNode != null)
                {
                    hotel.Address = tempNode.Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/AddressLines/AddressLine2/text()");
                if (tempNode != null)
                {
                    hotel.Address += tempNode.Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/AddressLines/AddressLine3/text()");
                if (tempNode != null)
                {
                    hotel.Address += tempNode.Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/AddressLines/AddressLine4/text()");
                if (tempNode != null)
                {
                    hotel.Address += tempNode.Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/AddressLines/Telephone/text()");
                if (tempNode != null)
                {
                    hotel.PhoneNumber = tempNode.Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/AddressLines/Fax/text()");
                if (tempNode != null)
                {
                    hotel.FaxNumber = tempNode.Value;
                }
                List<string> locInfo = new List<string>();
                XmlNodeList locList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/LocationDetails/Location");
                IEnumerator enumerator;
                if (locList != null)
                {
                    enumerator = locList.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            XmlNode location = (XmlNode)enumerator.Current;
                            tempNode = location.SelectSingleNode("text()");
                            locInfo.Add(tempNode.Value);
                        }
                    }
                    finally
                    {
                        IDisposable disposable = enumerator as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                }
                hotel.Location = locInfo;
                Dictionary<string, string> attractions = new Dictionary<string, string>();
                XmlNodeList attList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/AreaDetails/AreaDetail");
                int i = 1;
                enumerator = attList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        XmlNode attraction = (XmlNode)enumerator.Current;
                        tempNode = attraction.SelectSingleNode("text()");
                        attractions.Add(i.ToString() + ")", tempNode.Value);
                        i++;
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                hotel.Attractions = attractions;
                XmlNodeList hotelDesc = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/Reports/Report");
                string description = string.Empty;
                Room[] roomInfo = new Room[1];
                Room roomDetail = default(Room);
                enumerator = hotelDesc.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        XmlNode desc = (XmlNode)enumerator.Current;
                        tempNode = desc.SelectSingleNode("text()");
                        if (desc.Attributes[0].Value == "rooms")
                        {
                            roomDetail.Description = tempNode.Value;
                            roomDetail.RoomTypeId = hotelCode;
                        }
                        description += tempNode.Value;
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                hotel.Description = description;
                List<string> roomFacils = new List<string>();
                XmlNodeList roomFacList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/RoomFacilities/Facility");
                enumerator = roomFacList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        XmlNode roomFacility = (XmlNode)enumerator.Current;
                        tempNode = roomFacility.SelectSingleNode("text()");
                        roomFacils.Add(tempNode.Value);
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }

                hotel.RoomFacilities = roomFacils;

                List<string> facils = new List<string>();
                XmlNodeList facList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/Facilities/Facility");
                enumerator = facList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        XmlNode facility = (XmlNode)enumerator.Current;
                        tempNode = facility.SelectSingleNode("text()");
                        facils.Add(tempNode.Value);
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                hotel.HotelFacilities = facils;
                //commented by brahmam Map link is not giving right now they are giving only latitude,longitude

                //tempNode = xmlDoc.SelectSingleNode("Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/Links/MapLinks/MapPageLink/text()");
                //if (tempNode != null)
                //{
                //    hotel.Map = tempNode.Value;
                //}

                //map latitude,longitude implementd by brahmam 21.10.2016
                tempNode = xmlDoc.SelectSingleNode("Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/GeoCodes/Latitude");
                string latitude = string.Empty;
                string longitude = string.Empty;
                if (tempNode != null)
                {
                    latitude = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/GeoCodes/Longitude");
                if (tempNode != null)
                {
                    longitude = tempNode.InnerText;
                }
                if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude))
                {
                    hotel.Map = latitude + "|" + longitude;
                }



                List<string> imgList = new List<string>();
                XmlNodeList imgInfo = xmlDoc.SelectNodes("Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/HotelInformation/Links/ImageLinks/ImageLink");
                foreach (XmlNode image in imgInfo)
                {
                    tempNode = image.SelectSingleNode("Image/text()");
                    imgList.Add(tempNode.Value);
                }
                //imgList = GetImages(cityCode, hotelCode);
                if (imgList != null && imgList.Count > 0)
                {
                    hotel.Image = imgList[0];
                }
                else
                {
                    hotel.Image = string.Empty;
                }
                roomDetail.Images = imgList;
                roomInfo[0] = roomDetail;
                hotel.RoomData = roomInfo;
                hotel.Images = imgList;
            }
            else
            {
                Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
				{
					" No Hotels found ! | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
            }
            return hotel;
        }
        /// <summary>
        /// GenerateFareBreakDown
        /// </summary>
        /// <param name="req">HotelRequest</param>
        /// <param name="itemCode">itemCode</param>
        /// <param name="cityCode">cityCode</param>
        /// <returns>string</returns>
        /// <remarks>This method not for use right now</remarks>
        private string GenerateFareBreakDown(HotelRequest req, string itemCode, string cityCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", currency);
            xmlString.WriteAttributeString("Country", country);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("HotelPriceBreakdownRequest");
            xmlString.WriteStartElement("City");
            xmlString.WriteString(cityCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("Item");
            xmlString.WriteString(itemCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("PeriodOfStay");
            xmlString.WriteElementString("CheckInDate", req.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("Duration", req.EndDate.Subtract(req.StartDate).Days.ToString());
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("Rooms");
            roomTypeDetails = GetRoomType(req);
            for (int i = 0; i < req.NoOfRooms; i++)
            {
                string[] tempList = roomTypeDetails[i].Split(new char[]
				{
					'|'
				});
                string type = tempList[0];
                int noOfCots = (int)Convert.ToInt16(tempList[1]);
                bool extraBed = Convert.ToBoolean(Convert.ToInt16(tempList[2]));
                xmlString.WriteStartElement("Room");
                xmlString.WriteAttributeString("Code", type);
                xmlString.WriteAttributeString("NumberOfRooms", "1");
                xmlString.WriteAttributeString("NumberOfCots", noOfCots.ToString());
                if (extraBed)
                {
                    if (req.RoomGuest[i].noOfChild > 0)
                    {
                        xmlString.WriteStartElement("ExtraBeds");
                        xmlString.WriteElementString("Age", req.RoomGuest[i].childAge[0].ToString());
                        xmlString.WriteEndElement();
                    }
                }
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadResponseFareBreakDown
        /// </summary>
        /// <param name="xmlDoc">Response Xml doc</param>
        /// <param name="request">HotelRequest</param>
        /// <param name="discount">discount</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method not for use right now</remarks>
        private Dictionary<string, RoomRates[]> ReadResponseFareBreakDown(XmlDocument xmlDoc, HotelRequest request, ref decimal discount)
        {
            //Trace.TraceInformation("GTA.ReadResponseFareBreakDownentered");
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAFareBreakDown, Severity.High, 0, string.Concat(new object[]
				{
					"GTA:ReadResponseFareBreakDown, Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            TimeSpan diffResult = request.EndDate.Subtract(request.StartDate);
            XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/HotelPriceBreakdownResponse/ItemPrice");
            decimal totPrice = Convert.ToDecimal(tempNode.Attributes[1].Value);
            discount = 0m;
            for (int i = 0; i < tempNode.Attributes.Count; i++)
            {
                if (tempNode.Attributes[i].Name == "IncludedOfferDiscount")
                {
                    discount = Convert.ToDecimal(tempNode.Attributes[i].Value);
                }
            }
            XmlNodeList roomList = xmlDoc.SelectNodes("/Response/ResponseDetails/HotelPriceBreakdownResponse/HotelItem/HotelRooms/HotelRoom");
            Dictionary<string, RoomRates[]> fareBreakList = new Dictionary<string, RoomRates[]>();
            foreach (XmlNode room in roomList)
            {
                RoomRates[] rates = new RoomRates[diffResult.Days];
                int nights = 0;
                decimal gross = 0m;
                int j = 0;
                XmlNodeList priceList = room.SelectNodes("PriceRanges/PriceRange");
                foreach (XmlNode price in priceList)
                {
                    tempNode = price.SelectSingleNode("Price");
                    if (tempNode != null)
                    {
                        for (int k = 0; k < tempNode.Attributes.Count; k++)
                        {
                            string text = tempNode.Attributes[k].Name.ToString();
                            if (text != null)
                            {
                                if (!(text == "Nights"))
                                {
                                    if (text == "Gross")
                                    {
                                        gross = Convert.ToDecimal(tempNode.Attributes[k].Value);
                                    }
                                }
                                else
                                {
                                    nights = Convert.ToInt32(tempNode.Attributes[k].Value);
                                }
                            }
                        }
                        if (nights > 0 && gross >= 0m)
                        {
                            for (int i = 0; i < nights; i++)
                            {
                                RoomRates rate = default(RoomRates);
                                rate.Days = request.StartDate.AddDays(Convert.ToDouble(j));
                                rate.Amount = gross;
                                rate.BaseFare = gross;
                                rate.Totalfare = gross;
                                rate.RateType = RateType.Published;
                                rates[j++] = rate;
                            }
                        }
                    }
                }
                bool isChild = false;
                for (int i = 0; i < room.Attributes.Count; i++)
                {
                    if (room.Attributes[i].Value.Trim() == "CH")
                    {
                        fareBreakList.Add(room.Attributes[0].Value.Trim() + "|" + room.Attributes[1].Value, rates);
                        isChild = true;
                    }
                }
                if (!isChild)
                {
                    fareBreakList.Add(room.Attributes[0].Value.Trim(), rates);
                }
            }
            return fareBreakList;
        }
        /// <summary>
        /// This private function is used for Generates the Hotel Images request
        /// </summary>
        /// <param name="cityCode">cityCode</param>
        /// <param name="hotelCode">hotelCode</param>
        /// <returns>string</returns>
        private string GenerateImageRequest(string cityCode, string hotelCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchLinkRequest");
            xmlString.WriteAttributeString("ItemType", "hotel");
            xmlString.WriteAttributeString("LinkType", "image");
            xmlString.WriteStartElement("ItemDestination");
            xmlString.WriteAttributeString("DestinationType", "city");
            xmlString.WriteAttributeString("DestinationCode", cityCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemCode");
            xmlString.WriteString(hotelCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// Used in reading response XML and assigning static data to the HotelDetails.
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <returns>List</returns>
        private List<string> ReadResponseImages(XmlDocument xmlDoc)
        {
            //Trace.TraceInformation("GTA.ReadResponseImages entered");
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + ConfigurationSystem.GTAConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAGenerateImageResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { throw; }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchLinkResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAImageSearch, Severity.High, 0, string.Concat(new object[]
				{
					"GTA:ReadResponseImages, Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            List<string> imgList = new List<string>();
            XmlNodeList imgInfo = xmlDoc.SelectNodes("Response/ResponseDetails/SearchLinkResponse/LinkDetails/LinkDetail/Links/ImageLinks/ImageLink");
            foreach (XmlNode image in imgInfo)
            {
                XmlNode tempNode = image.SelectSingleNode("Image/text()");
                imgList.Add(tempNode.Value);
            }
            return imgList;
        }
        /// <summary>
        ///  This private function is used for Generates the Hotel ChargeCondition request
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        /// <remarks>This method not for use right now</remarks>
        private string GenerateHotelChargeCondition(HotelItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", itineary.Roomtype[0].Price.SupplierCurrency);
            xmlString.WriteAttributeString("Country", itineary.PassengerNationality);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchChargeConditionsRequest");
            xmlString.WriteStartElement("ChargeConditionsHotel");
            xmlString.WriteElementString("City", itineary.CityCode);
            xmlString.WriteElementString("Item", itineary.HotelCode);
            xmlString.WriteStartElement("PeriodOfStay");
            xmlString.WriteElementString("CheckInDate", itineary.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("Duration", itineary.EndDate.Subtract(itineary.StartDate).Days.ToString());
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("Rooms");
            HotelRoom[] rooms = itineary.Roomtype;
            for (int i = 0; i < rooms.Length; i++)
            {
                xmlString.WriteStartElement("Room");
                string[] roomTypeCode = rooms[i].RoomTypeCode.Split('|');
                xmlString.WriteAttributeString("Code", roomTypeCode[0]);
                xmlString.WriteAttributeString("Id", roomTypeCode[5]);
                xmlString.WriteAttributeString("NumberOfRooms", rooms[i].NoOfUnits);
                xmlString.WriteAttributeString("NumberOfCots", roomTypeCode[1]);
                if (Convert.ToBoolean(Convert.ToInt16(roomTypeCode[2])))
                {
                    if (rooms[i].ChildCount > 0)
                    {
                        xmlString.WriteStartElement("ExtraBeds");
                        xmlString.WriteElementString("Age", rooms[i].ChildAge[0].ToString());
                        xmlString.WriteEndElement();
                    }
                }
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// Used in reading response XML and assigning ChargeCondition
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="startDate">startDate</param>
        /// <param name="penalityList">penalityList</param>
        /// <param name="savePenality">savePenality</param>
        /// <param name="roomCount">roomCount</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method not for use right now</remarks>
        private Dictionary<string, string> ReadResponseChargeCondition(XmlDocument xmlDoc, DateTime startDate, ref List<HotelPenality> penalityList, bool savePenality, int roomCount)
        {
            //Trace.TraceInformation("GTA.ReadResponseChargeCondition entered");
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + ConfigurationSystem.GTAConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAChargeConditionsHotelResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { throw; }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchChargeConditionsResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAChagreCondition, Severity.High, 0, string.Concat(new object[]
				{
					" Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            Dictionary<string, string> polList = new Dictionary<string, string>();
            List<HotelPenality> penList = new List<HotelPenality>();
            XmlNodeList chargeList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchChargeConditionsResponse/ChargeConditions/ChargeCondition");
            string message = string.Empty;
            string lastCancelllation = string.Empty;
            string amendmentMessage = "Amedment Not Allowed";
            lastCancelllation = "0";
            foreach (XmlNode charge in chargeList)
            {
                if (charge.Attributes[0].Value == "cancellation")
                {
                    XmlNodeList individualList = charge.SelectNodes("Condition");
                    int buffer = Convert.ToInt32(ConfigurationSystem.GTAConfig["buffer"]);
                    foreach (XmlNode chargeInfo in individualList)
                    {
                        bool isCharge = false;
                        int fromDay = 0;
                        int toDay = 0;
                        decimal chargeAmt = 0m;
                        string curr = string.Empty;
                        for (int i = 0; i < chargeInfo.Attributes.Count; i++)
                        {
                            string text = chargeInfo.Attributes[i].Name.ToString();
                            if (text != null)
                            {
                                if (!(text == "Charge"))
                                {
                                    if (!(text == "FromDay"))
                                    {
                                        if (!(text == "ToDay"))
                                        {
                                            if (!(text == "Currency"))
                                            {
                                                if (text == "ChargeAmount")
                                                {
                                                    chargeAmt = Convert.ToDecimal(chargeInfo.Attributes[i].Value) / roomCount;
                                                }
                                            }
                                            else
                                            {
                                                curr = chargeInfo.Attributes[i].Value;
                                            }
                                        }
                                        else
                                        {
                                            toDay = (int)Convert.ToInt16(chargeInfo.Attributes[i].Value);
                                        }
                                    }
                                    else
                                    {
                                        fromDay = (int)Convert.ToInt16(chargeInfo.Attributes[i].Value);
                                    }
                                }
                                else
                                {
                                    isCharge = Convert.ToBoolean(chargeInfo.Attributes[i].Value);
                                }
                            }
                        }
                        if (isCharge)
                        {
                            rateOfExchange = exchangeRates[curr];
                            chargeAmt = Math.Ceiling(chargeAmt * rateOfExchange);
                            chargeAmt += chargeAmt * Convert.ToDecimal(ConfigurationSystem.GTAConfig["CancellationMarkup"]) / 100m;
                            string symbol = agentCurrency;
                            //string symbol = GetCurrencySymbol(curr);
                            if (fromDay == toDay)
                            {
                                //int extraDays = 0;
                                //if (startDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                //{
                                //    extraDays = 1;
                                //}
                                //else
                                //{
                                //    if (startDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                //    {
                                //        extraDays = 2;
                                //    }
                                //}
                                object obj = message;
                                message = string.Concat(new object[]
								{
									obj,
									DateTime.Now.ToString("dd MMM yyyy"),
									" onwards, ",
									symbol,
									" ",
									chargeAmt,
									" will be charged."
								});
                            }
                            if (fromDay != toDay && fromDay == 0 && toDay > 0)
                            {
                                int extraDays = 0;
                                //if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                //{
                                //    extraDays = 1;
                                //}
                                //else
                                //{
                                //    if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                //    {
                                //        extraDays = 2;
                                //    }
                                //}
                                object obj = message;
                                message = string.Concat(new object[]
								{
									obj,
									"Cancellation upto ",
									toDay - buffer + extraDays,
									" day(s) before the date of arrival, ",
									symbol,
									" ",
									chargeAmt,
									" will be charged."
								});
                            }
                            else
                            {
                                if (fromDay != toDay)
                                {
                                    int extraDays = 0;
                                    //if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                    //{
                                    //    extraDays = 1;
                                    //}
                                    //else
                                    //{
                                    //    if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                    //    {
                                    //        extraDays = 2;
                                    //    }
                                    //}
                                    object obj = message;
                                    message = string.Concat(new object[]
									{
										obj,
										"Cancellation from ",
										fromDay -buffer + extraDays,
										" days to ",
										toDay - buffer,
										" days before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                }
                            }
                            message += "|";
                            lastCancelllation = startDate.ToString();
                        }
                    }
                }
                else
                {
                    if (charge.Attributes[0].Value == "amendment")   //Amendent buffer is not being applied from GTAConfig.XML as the same is yet to be implemented
                    {
                        amendmentMessage = string.Empty;
                        XmlNodeList individualList = charge.SelectNodes("Condition");
                        int buffer = Convert.ToInt32(ConfigurationSystem.GTAConfig["amendmentBuffer"]);
                        foreach (XmlNode chargeInfo in individualList)
                        {
                            HotelPenality penalityInfo = new HotelPenality();
                            penalityInfo.BookingSource = HotelBookingSource.GTA;
                            penalityInfo.PolicyType = ChangePoicyType.Amendment;
                            bool isCharge = false;
                            bool isAllowed = true;
                            int fromDay = 0;
                            int toDay = -1;
                            decimal chargeAmt = 0m;
                            string curr = string.Empty;
                            for (int i = 0; i < chargeInfo.Attributes.Count; i++)
                            {
                                string text = chargeInfo.Attributes[i].Name.ToString();
                                if (text != null)
                                {
                                    if (!(text == "Charge"))
                                    {
                                        if (!(text == "FromDay"))
                                        {
                                            if (!(text == "ToDay"))
                                            {
                                                if (!(text == "Currency"))
                                                {
                                                    if (!(text == "ChargeAmount"))
                                                    {
                                                        if (text == "Allowable")
                                                        {
                                                            isAllowed = Convert.ToBoolean(chargeInfo.Attributes[i].Value);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        chargeAmt = Convert.ToDecimal(chargeInfo.Attributes[i].Value) / roomCount;
                                                    }
                                                }
                                                else
                                                {
                                                    curr = chargeInfo.Attributes[i].Value;
                                                }
                                            }
                                            else
                                            {
                                                toDay = (int)Convert.ToInt16(chargeInfo.Attributes[i].Value);
                                            }
                                        }
                                        else
                                        {
                                            fromDay = (int)Convert.ToInt16(chargeInfo.Attributes[i].Value);
                                        }
                                    }
                                    else
                                    {
                                        isCharge = Convert.ToBoolean(chargeInfo.Attributes[i].Value);
                                    }
                                }
                            }

                            if (isAllowed && isCharge)
                            {
                                rateOfExchange = exchangeRates[curr];
                                chargeAmt = Math.Ceiling(chargeAmt * rateOfExchange);
                                chargeAmt += chargeAmt * Convert.ToDecimal(ConfigurationSystem.GTAConfig["CancellationMarkup"]) / 100m;
                                chargeAmt = Math.Round(chargeAmt, Convert.ToInt32(ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                                string symbol = agentCurrency;
                                //string symbol = GetCurrencySymbol(curr);
                                if (fromDay == toDay)
                                {
                                    int extraDays = 0;
                                    if (startDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        extraDays = 1;
                                    }
                                    else
                                    {
                                        if (startDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            extraDays = 2;
                                        }
                                    }
                                    penalityInfo.FromDate = startDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                    penalityInfo.ToDate = startDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                    object obj = amendmentMessage;
                                    amendmentMessage = string.Concat(new object[]
									{
										obj,
										"Amendment upto ",
										fromDay + buffer + extraDays,
										" day(s) before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                    penalityInfo.Remarks = string.Concat(new object[]
									{
										"Amendment upto ",
										fromDay + buffer + extraDays,
										" day(s) before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                }
                                if (fromDay != toDay && fromDay == 0 && toDay > 0)
                                {
                                    int extraDays = 0;
                                    if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        extraDays = 1;
                                    }
                                    else
                                    {
                                        if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            extraDays = 2;
                                        }
                                    }
                                    penalityInfo.FromDate = startDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                    penalityInfo.ToDate = startDate.AddDays((double)(-(double)(toDay + buffer + extraDays)));
                                    object obj = amendmentMessage;
                                    amendmentMessage = string.Concat(new object[]
									{
										obj,
										"Amendment upto ",
										toDay + buffer + extraDays,
										" day(s) before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                    penalityInfo.Remarks = string.Concat(new object[]
									{
										"Amendment upto ",
										toDay + buffer + extraDays,
										" day(s) before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                }
                                else
                                {
                                    if (fromDay != toDay)
                                    {
                                        int extraDays = 0;
                                        if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                        {
                                            extraDays = 1;
                                        }
                                        else
                                        {
                                            if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                            {
                                                extraDays = 2;
                                            }
                                        }
                                        if (toDay == -1)
                                        {
                                            penalityInfo.FromDate = DateTime.Now;
                                            penalityInfo.ToDate = startDate;
                                        }
                                        else
                                        {
                                            penalityInfo.FromDate = startDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                            penalityInfo.ToDate = startDate.AddDays((double)(-(double)(toDay + buffer + extraDays)));
                                        }
                                        object obj = amendmentMessage;
                                        amendmentMessage = string.Concat(new object[]
										{
											obj,
											"Amendment from ",
											fromDay + buffer + extraDays,
											" days to ",
											toDay + buffer,
											" days before the date of arrival, ",
											symbol,
											" ",
											chargeAmt,
											" will be charged."
										});
                                        penalityInfo.Remarks = string.Concat(new object[]
										{
											"Amendment from ",
											fromDay + buffer + extraDays,
											" days to ",
											toDay + buffer,
											" days before the date of arrival, ",
											symbol,
											" ",
											chargeAmt,
											" will be charged."
										});
                                    }
                                }
                                penalityInfo.Price = chargeAmt;
                                penalityInfo.IsAllowed = isAllowed;
                                penList.Add(penalityInfo);
                                amendmentMessage += "|";
                            }
                            else
                            {
                                penalityInfo.IsAllowed = isAllowed;
                                penalityInfo.FromDate = DateTime.Now;
                                penalityInfo.ToDate = startDate;
                                penalityInfo.Price = 0m;
                                if (isAllowed)
                                {
                                    penalityInfo.Remarks = "No Amendment Charges before " + (fromDay + buffer) + " days of CheckIn.";
                                }
                                else
                                {
                                    penalityInfo.Remarks = "Amendment is Not Allowed.";
                                }
                                amendmentMessage = amendmentMessage + "|" + penalityInfo.Remarks;
                                penList.Add(penalityInfo);
                            }
                        }
                    }
                }
            }
            XmlNode nameChangeInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchChargeConditionsResponse/ChargeConditions/PassengerNameChange");
            if (nameChangeInfo != null)
            {
                if (nameChangeInfo.Attributes[0].Value.ToLower() == "true")
                {
                    penList.Add(new HotelPenality
                    {
                        BookingSource = HotelBookingSource.GTA,
                        PolicyType = ChangePoicyType.Namechange,
                        IsAllowed = true,
                        FromDate = startDate,
                        ToDate = startDate,
                        Price = 0m
                    });
                }
            }
            if (savePenality)
            {
                penalityList = penList;
            }
            polList.Add("lastCancellationDate", lastCancelllation);
            polList.Add("CancelPolicy", message);
            polList.Add("AmendmentPolicy", amendmentMessage);
            polList.Add("HotelPolicy", "");
            //Trace.TraceInformation("GTA.ReadResponseChargeCondition Exit");
            return polList;
        }

        /// <summary>
        /// TraceIdGeneration(Each request genarating uniqueId)
        /// </summary>
        /// <returns>string</returns>
        private string TraceIdGeneration()
        {
            string traceid = DateTime.Now.ToString("ddMMyyyyHHmmsss");
            return "CZ" + traceid;
        }

        /// <summary>
        /// This private function is used for Generates the Hotel Book request
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateBookingRequest(HotelItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", itineary.Roomtype[0].Price.SupplierCurrency);
            xmlString.WriteAttributeString("Country", itineary.PassengerNationality);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("AddBookingRequest");
            xmlString.WriteElementString("BookingName", "BOOK" + TraceIdGeneration());
            clientReferenceNo = "BookRef" + TraceIdGeneration();
            xmlString.WriteElementString("BookingReference", clientReferenceNo);
            xmlString.WriteElementString("AgentReference", "CZT" + TraceIdGeneration());
            xmlString.WriteElementString("BookingDepartureDate", itineary.StartDate.ToString("yyyy-MM-dd"));
            int paxCount = 1;
            xmlString.WriteStartElement("PaxNames");
            HotelRoom[] room = itineary.Roomtype;
            string name = string.Empty;
            for (int i = 0; i < itineary.Roomtype.Length; i++)
            {
                int childCount = 0;
                for (int j = 0; j < room[i].AdultCount * (int)Convert.ToInt16(room[i].NoOfUnits) + room[i].ChildCount * (int)Convert.ToInt16(room[i].NoOfUnits); j++)
                {
                    name = room[i].PassenegerInfo[j].Firstname + room[i].PassenegerInfo[j].Lastname;
                    if (name.Length == 0)
                    {
                        name = "MBH";
                    }
                    if (room[i].PassenegerInfo[j].PaxType == HotelPaxType.Adult)
                    {
                        xmlString.WriteStartElement("PaxName");
                        xmlString.WriteAttributeString("PaxId", paxCount.ToString());
                        xmlString.WriteCData(name);
                        xmlString.WriteEndElement();
                    }
                    else
                    {
                        if (room[i].PassenegerInfo[j].PaxType == HotelPaxType.Child)
                        {
                            if (Convert.ToInt32(room[i].ChildAge[childCount]) > 2)
                            {
                                xmlString.WriteStartElement("PaxName");
                                xmlString.WriteAttributeString("PaxId", paxCount.ToString());
                                xmlString.WriteAttributeString("PaxType", "child");
                                xmlString.WriteAttributeString("ChildAge", room[i].ChildAge[childCount++].ToString());
                                xmlString.WriteCData(name);
                                xmlString.WriteEndElement();
                            }
                            else
                            {
                                childCount++;
                                paxCount--;
                            }
                        }
                    }
                    paxCount++;
                }
            }
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("BookingItems");
            xmlString.WriteStartElement("BookingItem");
            xmlString.WriteAttributeString("ItemType", "hotel");
            xmlString.WriteAttributeString("ExpectedPrice", (itineary.Roomtype[0].Price.SupplierPrice * itineary.NoOfRooms).ToString());
            xmlString.WriteElementString("ItemReference", "1");
            xmlString.WriteStartElement("ItemCity");
            xmlString.WriteAttributeString("Code", itineary.CityCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("Item");
            xmlString.WriteAttributeString("Code", itineary.HotelCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("HotelItem");
            xmlString.WriteElementString("AlternativesAllowed", "false");
            xmlString.WriteStartElement("PeriodOfStay");
            xmlString.WriteElementString("CheckInDate", itineary.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("CheckOutDate", itineary.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("HotelRooms");
            HotelRoom[] rooms = itineary.Roomtype;
            int k = 1;
            for (int j = 0; j < rooms.Length; j++)
            {
                for (int l = 0; l < (int)Convert.ToInt16(room[j].NoOfUnits); l++)
                {
                    xmlString.WriteStartElement("HotelRoom");
                    string[] roomTypeCode = rooms[j].RoomTypeCode.Split('|');
                    xmlString.WriteAttributeString("Code", roomTypeCode[0]);

                    if (rooms[j].ExtraBed)
                    {
                        xmlString.WriteAttributeString("ExtraBed", "true");
                    }
                    else
                    {
                        xmlString.WriteAttributeString("ExtraBed", "false");
                    }
                    xmlString.WriteAttributeString("Id", rooms[j].RatePlanCode);
                    xmlString.WriteAttributeString("NumberOfCots", rooms[j].NoOfCots.ToString());
                    if (Convert.ToInt32(rooms[j].NoOfExtraBed) > 0)
                    {
                        xmlString.WriteAttributeString("NumberOfExtraBeds", rooms[j].NoOfExtraBed.ToString());
                    }
                    xmlString.WriteStartElement("PaxIds");
                    int totalPax = rooms[j].AdultCount;
                    for (int m = 0; m < rooms[j].ChildCount; m++)
                    {
                        if (Convert.ToInt32(rooms[j].ChildAge[m]) > 2)
                        {
                            totalPax++;
                        }
                    }
                    int paxDiff = 0;
                    //if (rooms[j].AdultCount == 2 && room[j].RoomTypeCode != "Q")
                    //{
                    //    if (totalPax > rooms[j].AdultCount + (int)Convert.ToInt16(rooms[j].ExtraBed))
                    //    {
                    //        totalPax = rooms[j].AdultCount + (int)Convert.ToInt16(rooms[j].ExtraBed);
                    //        paxDiff = rooms[j].AdultCount + rooms[j].ChildCount - totalPax;
                    //    }
                    //}
                    for (int m = 0; m < totalPax; m++)
                    {
                        xmlString.WriteElementString("PaxId", k.ToString());
                        k++;
                    }
                    k += paxDiff;
                    xmlString.WriteEndElement();
                    xmlString.WriteEndElement();
                }
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// ReadResponseBooking
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>Here Reading booking response object
        /// here only we need to get confirmationNo and BookingId
        /// </remarks>
        private BookingResponse ReadResponseBooking(XmlDocument xmlDoc, ref HotelItinerary itineary)
        {
            //Trace.TraceInformation("GTA.ReadResponseChargeCondition entered");
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAGenerateBookingResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { throw; }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					" Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                cancelInfo = CancelHotelBooking(itineary);
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            BookingResponse bookResponse = default(BookingResponse);
            string confNo = string.Empty;
            XmlNodeList bookList = xmlDoc.SelectNodes("/Response/ResponseDetails/BookingResponse/BookingReferences/BookingReference");
            XmlNode tempNode;
            foreach (XmlNode booking in bookList)
            {
                if (booking.Attributes[0].Value == "api")
                {
                    tempNode = booking.SelectSingleNode("text()");
                    itineary.BookingRefNo = tempNode.Value;
                }
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemStatus");
            if (tempNode != null && tempNode.Attributes[0].Value.Trim() == "C")
            {
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemConfirmationReference");
                if (tempNode != null)
                {
                    confNo = tempNode.InnerText;
                    Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
					{
						"Booking successfully created from GTA. Confirmation No:",
						confNo,
						" | ",
						DateTime.Now
					}), "");
                    bookResponse = new BookingResponse(BookingResponseStatus.Successful, "", "", ProductType.Hotel, confNo);
                    itineary.ConfirmationNo = confNo;
                    itineary.Status = HotelBookingStatus.Confirmed;
                    itineary.PaymentGuaranteedBy = "Booked and payable by Gullivers Travel Associates </br> Only Payment For Extras To Be Collected From The Client.";
                }
                else
                {
                    cancelInfo = CancelHotelBooking(itineary);
                }
                try
                {
                    //Dictionary<string, string> aotNumber = GetAOTNumber("AE");
                }
                catch { throw; }
            }
            else
            {
                cancelInfo = CancelHotelBooking(itineary);
            }
            return bookResponse;
        }

        /// <summary>
        /// GetRoomType(Based On the selected pax wise only genarating room types) 
        /// </summary>
        /// <param name="request">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>List</returns>
        public List<string> GetRoomType(HotelRequest request)
        {
            List<string> roomTypes = new List<string>();
            for (int i = 0; i < request.RoomGuest.Length; i++)
            {
                int adults = request.RoomGuest[i].noOfAdults;
                int childs = request.RoomGuest[i].noOfChild;
                if (adults == 1)
                {
                    if (childs == 0)
                    {
                        roomTypes.Add("SB|0|0");// only adults
                    }
                    if (childs == 1)
                    {
                        if (request.RoomGuest[i].childAge[0] <= 2)
                        {
                            roomTypes.Add("TS|0|0");// For infant Twin Room
                        }
                        else
                        {
                            roomTypes.Add("DB|0|0");// For child Double Bed
                        }
                    }
                    if (childs == 2)
                    {
                        if ((request.RoomGuest[i].childAge[0] <= 2 && request.RoomGuest[i].childAge[1] <= 2) || (request.RoomGuest[i].childAge[0] <= 2 && request.RoomGuest[i].childAge[1] > 2)
                            || (request.RoomGuest[i].childAge[0] > 2 && request.RoomGuest[i].childAge[1] <= 2)
                            )// mixed type
                        {
                            roomTypes.Add("DB|1|0");//2 infants 1 DB and 1 Cot
                        }
                        //else if(request.RoomGuest[i].childAge[0] <= 2 && request.RoomGuest[i].childAge[1] > 2)
                        //{
                        //    roomTypes.Add("DB|1|0");//2 infants 1 DB and 1 Cot
                        //}
                        //else if (request.RoomGuest[i].childAge[0] > 2 && request.RoomGuest[i].childAge[1] <= 2)
                        //{
                        //    roomTypes.Add("DB|1|0");//2 infants 1 DB and 1 Cot
                        //}
                        else if (request.RoomGuest[i].childAge[0] > 2 && request.RoomGuest[i].childAge[1] > 2)// only Child
                        {
                            roomTypes.Add("DB|0|1");
                        }
                    }
                }
                if (adults == 2)
                {
                    if (childs == 0)
                    {
                        roomTypes.Add("DB|0|0"); // only adults
                    }
                    if (childs == 1)
                    {
                        if (request.RoomGuest[i].childAge[0] <= 2)
                        {
                            roomTypes.Add("DB|1|0");
                        }
                        else
                        {
                            roomTypes.Add("DB|0|1");
                        }
                    }
                    if (childs == 2)
                    {
                        if (request.RoomGuest[i].childAge[0] <= 2 && request.RoomGuest[i].childAge[1] <= 2)
                        {
                            roomTypes.Add("DB|2|0");//both infants
                        }
                        else if ((request.RoomGuest[i].childAge[0] <= 2 && request.RoomGuest[i].childAge[1] > 2) || (request.RoomGuest[i].childAge[0] > 2 && request.RoomGuest[i].childAge[1] <= 2))
                        {
                            roomTypes.Add("DB|1|1");
                        }
                        //else if (request.RoomGuest[i].childAge[0] > 2 && request.RoomGuest[i].childAge[1] <= 2)
                        //{
                        //    roomTypes.Add("DB|1|1");
                        //}
                        else if (request.RoomGuest[i].childAge[0] > 2 && request.RoomGuest[i].childAge[1] > 2)
                        {
                            roomTypes.Add("DB|0|2");//both childs
                        }
                    }
                }
                if (adults == 3)
                {
                    if (childs == 0)
                    {
                        roomTypes.Add("TR|0|0");// only adults
                    }
                    if (childs == 1)
                    {
                        roomTypes.Add("Q|0|0");
                    }
                }
                if (adults == 4)
                {
                    if (childs == 0)
                    {
                        roomTypes.Add("Q|0|0"); // only adults
                    }
                }
            }
            return roomTypes;
        }

        /// <summary>
        /// This private function is used for Generates the Hotel cancel request
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateCancellationRequest(HotelItinerary itineary)
        {
            StringBuilder strBuild = new StringBuilder();
            XmlWriter newXml = XmlWriter.Create(strBuild);
            newXml.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            newXml.WriteStartElement("Request");
            newXml.WriteStartElement("Source");
            newXml.WriteStartElement("RequestorID");
            newXml.WriteAttributeString("Client", clientId);
            newXml.WriteAttributeString("EMailAddress", email);
            newXml.WriteAttributeString("Password", password);
            newXml.WriteEndElement();
            newXml.WriteStartElement("RequestorPreferences");
            newXml.WriteAttributeString("Language", language);
            newXml.WriteElementString("RequestMode", "SYNCHRONOUS");
            newXml.WriteEndElement();
            newXml.WriteEndElement();
            newXml.WriteStartElement("RequestDetails");
            newXml.WriteStartElement("CancelBookingRequest");
            newXml.WriteStartElement("BookingReference");
            if (itineary.BookingRefNo != null)
            {
                newXml.WriteAttributeString("ReferenceSource", "api");
                newXml.WriteValue(itineary.BookingRefNo);
            }
            else
            {
                newXml.WriteAttributeString("ReferenceSource", "client");
                newXml.WriteValue(clientReferenceNo);

            }
            newXml.WriteEndElement();
            newXml.WriteEndElement();
            newXml.WriteEndElement();
            newXml.WriteEndElement();
            newXml.Close();
            return strBuild.ToString();
        }
        /// <summary>
        /// ReadResponseCancelHotel
        /// </summary>
        /// <param name="xmlDoc">Cancel Response xml</param>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> ReadResponseCancelHotel(XmlDocument xmlDoc)
        {
            //Trace.TraceInformation("GTA.ReadResponseCancelHotel entered");
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTACancelHotelResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { throw; }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTACancel, Severity.High, 0, string.Concat(new object[]
				{
					" Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
					xmlDoc.OuterXml
				}), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemStatus/text()");
            if (tempNode != null)
            {
                cancelInfo.Add("Status", tempNode.Value);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemConfirmationReference");
            if (tempNode != null)
            {
                cancelInfo.Add("ID", tempNode.InnerText);
            }
            //item price
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemFee");
            if (tempNode != null)
            {
                cancelInfo.Add("Currency", tempNode.Attributes[0].Value);//currency
                cancelInfo.Add("Amount", tempNode.InnerText);
            }
            else
            {
                cancelInfo.Add("Currency", "AED");
                cancelInfo.Add("Amount", "0");
            }
            return cancelInfo;
        }
        /// <summary>
        /// This private function is used for Generates the Hotel ModifyBook request.
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        /// <remarks>This method not for use right now</remarks>
        private string GenerateModifyBookingRequest(HotelItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", itineary.Roomtype[0].Price.Currency);
            xmlString.WriteAttributeString("Country", country);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("ModifyBookingItemRequest");
            xmlString.WriteStartElement("BookingReference");
            xmlString.WriteAttributeString("ReferenceSource", "client");
            xmlString.WriteString(itineary.BookingRefNo);
            xmlString.WriteEndElement();
            HotelPassenger passInfo = new HotelPassenger();
            HotelRoom[] room = itineary.Roomtype;
            xmlString.WriteStartElement("BookingItems");
            xmlString.WriteStartElement("BookingItem");
            xmlString.WriteAttributeString("ItemType", "hotel");
            xmlString.WriteElementString("ItemReference", "1");
            xmlString.WriteStartElement("ItemRemarks");
            xmlString.WriteStartElement("ItemRemark");
            xmlString.WriteCData(itineary.SpecialRequest);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("HotelItem");
            xmlString.WriteStartElement("PeriodOfStay");
            xmlString.WriteElementString("CheckInDate", itineary.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("CheckOutDate", itineary.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("HotelRooms");
            HotelRoom[] rooms = itineary.Roomtype;
            int i = 1;
            for (int j = 0; j < rooms.Length; j++)
            {
                xmlString.WriteStartElement("HotelRoom");
                xmlString.WriteAttributeString("Code", rooms[j].RoomTypeCode);
                xmlString.WriteAttributeString("NumberOfCots", rooms[j].NoOfCots.ToString());
                if (rooms[j].ExtraBed)
                {
                    xmlString.WriteAttributeString("ExtraBed", "true");
                }
                else
                {
                    xmlString.WriteAttributeString("ExtraBed", "false");
                }
                xmlString.WriteStartElement("PaxIds");
                for (int k = 0; k < rooms[j].AdultCount; k++)
                {
                    xmlString.WriteElementString("PaxId", i.ToString());
                    i++;
                }
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadResponseModifyBooking
        /// </summary>
        /// <param name="xmlDoc">response xml object</param>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>This method not for use right now</remarks>
        private BookingResponse ReadResponseModifyBooking(XmlDocument xmlDoc, ref HotelItinerary itineary)
        {
            //Trace.TraceInformation("GTA.ReadResponseChargeCondition entered");
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					" Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            BookingResponse bookResponse = default(BookingResponse);
            string confNo = string.Empty;
            XmlNodeList bookList = xmlDoc.SelectNodes("/Response/ResponseDetails/BookingResponse/BookingReferences/BookingReference");
            XmlNode tempNode;
            foreach (XmlNode booking in bookList)
            {
                if (booking.Attributes[0].Value == "client")
                {
                    tempNode = booking.SelectSingleNode("text()");
                    itineary.BookingRefNo = tempNode.Value;
                }
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemConfirmationReference");
            if (tempNode != null)
            {
                confNo = tempNode.InnerText;
                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					"Booking successfully created from GTA. Confirmation No:",
					confNo,
					" | ",
					DateTime.Now
				}), "");
                bookResponse = new BookingResponse(BookingResponseStatus.Successful, "", "", ProductType.Hotel, confNo);
                itineary.ConfirmationNo = confNo;
                itineary.Status = HotelBookingStatus.Confirmed;
            }
            return bookResponse;
        }
        /// <summary>
        /// This private function is used for Generates the GenerateAOTRequest.
        /// </summary>
        /// <param name="countryCode">countryCode</param>
        /// <returns>string</returns>
        /// <remarks>This method not for use right now</remarks>
        private string GenerateAOTRequest(string countryCode)
        {
            //Trace.TraceInformation("GTA.GenerateAOTRequest Entered.");
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchAOTNumberRequest");
            xmlString.WriteElementString("AssistanceLanguage", language);
            xmlString.WriteElementString("Destination", countryCode);
            xmlString.WriteElementString("Nationality", country);
            xmlString.WriteElementString("ServiceType", "hotel");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            //Trace.TraceInformation("GTA.GenerateAOTRequest Exit.");
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadResponseAOTNumber
        /// </summary>
        /// <param name="xmlDoc">response xml</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method not for use right now</remarks>
        private Dictionary<string, string> ReadResponseAOTNumber(XmlDocument xmlDoc)
        {
            //Trace.TraceInformation("GTA.ReadResponseAOTNumber Entered.");
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + ConfigurationSystem.GTAConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAGenerateAOTResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { throw; }
            Dictionary<string, string> AOTNum = new Dictionary<string, string>();
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAAOTNumber, Severity.High, 0, string.Concat(new object[]
				{
					" Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            string confNo = string.Empty;
            XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchAOTNumberResponse/ContactDetails/Destination/Description/text()");
            if (tempNode != null)
            {
                AOTNum.Add("Country", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchAOTNumberResponse/ContactDetails/Destination/Offices/Office/OfficeLocation/text()");
            if (tempNode != null)
            {
                AOTNum.Add("City", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchAOTNumberResponse/ContactDetails/Destination/Offices/Office/AssistanceLanguages/AssistanceLanguage/Language/text()");
            if (tempNode != null)
            {
                AOTNum.Add("Language", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchAOTNumberResponse/ContactDetails/Destination/Offices/Office/AssistanceLanguages/AssistanceLanguage/OfficeHours");
            if (tempNode != null)
            {
                AOTNum.Add("OfficeHours", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchAOTNumberResponse/ContactDetails/Destination/Offices/Office/AssistanceLanguages/AssistanceLanguage/AOTNumbers/International");
            if (tempNode != null)
            {
                AOTNum.Add("InternationalNo", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchAOTNumberResponse/ContactDetails/Destination/Offices/Office/AssistanceLanguages/AssistanceLanguage/AOTNumbers/Local");
            if (tempNode != null)
            {
                AOTNum.Add("LocalNo", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchAOTNumberResponse/ContactDetails/Destination/Offices/Office/AssistanceLanguages/AssistanceLanguage/AOTNumbers/National");
            if (tempNode != null)
            {
                AOTNum.Add("NationalNo", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchAOTNumberResponse/ContactDetails/Destination/Offices/Office/AssistanceLanguages/AssistanceLanguage/AOTNumbers/OutOfOffice");
            if (tempNode != null)
            {
                AOTNum.Add("EmergencyNo", tempNode.InnerText);
            }
            //Trace.TraceInformation("GTA.ReadResponseAOTNumber Exit.");
            return AOTNum;
        }
        /// <summary>
        /// This private function is used for Generates the HotelAmendPaxDetails.
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        /// <remarks>This method not for use right now</remarks>
        private string GenerateHotelAmendPaxDetails(HotelItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", ConfigurationSystem.LocaleConfig["CurrencyCode"] ?? "");
            xmlString.WriteAttributeString("Country", country);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("ModifyBookingRequest");
            xmlString.WriteStartElement("BookingReference");
            xmlString.WriteAttributeString("ReferenceSource", "api");
            xmlString.WriteString(itineary.BookingRefNo);
            xmlString.WriteEndElement();
            int paxCount = 1;
            xmlString.WriteStartElement("PaxNames");
            HotelRoom[] room = itineary.Roomtype;
            string name = string.Empty;
            for (int i = 0; i < itineary.Roomtype.Length; i++)
            {
                int childCount = 0;
                for (int j = 0; j < room[i].AdultCount + room[i].ChildCount; j++)
                {
                    name = room[i].PassenegerInfo[j].Firstname + room[i].PassenegerInfo[j].Lastname;
                    if (name.Length == 0)
                    {
                        name = "TBA";
                    }
                    if (room[i].PassenegerInfo[j].PaxType == HotelPaxType.Adult)
                    {
                        xmlString.WriteStartElement("PaxName");
                        xmlString.WriteAttributeString("PaxId", paxCount.ToString());
                        xmlString.WriteCData(name);
                        xmlString.WriteEndElement();
                    }
                    else
                    {
                        if (room[i].PassenegerInfo[j].PaxType == HotelPaxType.Child)
                        {
                            xmlString.WriteStartElement("PaxName");
                            xmlString.WriteAttributeString("PaxId", paxCount.ToString());
                            xmlString.WriteAttributeString("PaxType", "child");
                            xmlString.WriteAttributeString("ChildAge", room[i].ChildAge[childCount++].ToString());
                            xmlString.WriteCData(name);
                            xmlString.WriteEndElement();
                        }
                    }
                    paxCount++;
                }
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadResponseAmendPaxDetails
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="itienary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <remarks>This method not for use right now</remarks>
        private void ReadResponseAmendPaxDetails(XmlDocument xmlDoc, ref HotelItinerary itienary)
        {
            //Trace.TraceInformation("GTA.ReadResponseAmendPaxDetails entered");
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTABooking, Severity.High, 0, string.Concat(new object[]
				{
					" Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingStatus");
            if (tempNode != null)
            {
                if (tempNode.Attributes[0].Value == "CP")
                {
                    itienary.Status = HotelBookingStatus.Pending;
                }
                else
                {
                    if (tempNode.Attributes[0].Value == "C")
                    {
                        itienary.Status = HotelBookingStatus.Confirmed;
                    }
                    else
                    {
                        itienary.Status = HotelBookingStatus.Failed;
                    }
                }
            }
            //Trace.TraceInformation("GTA.ReadResponseAmendPaxDetails Exit");
        }
        /// <summary>
        ///  This private function is used for Generates the ModifyBookingItemRequest.
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        /// <remarks>This method not for use right now</remarks>
        private string GenerateModifyBookingItemRequest(HotelItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", currency);
            xmlString.WriteAttributeString("Country", country);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("ModifyBookingItemRequest");
            xmlString.WriteStartElement("BookingReference");
            xmlString.WriteAttributeString("ReferenceSource", "api");
            xmlString.WriteString(itineary.BookingRefNo);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("BookingItems");
            xmlString.WriteStartElement("BookingItem");
            xmlString.WriteAttributeString("ItemType", "hotel");
            xmlString.WriteElementString("ItemReference", "1");
            xmlString.WriteStartElement("HotelItem");
            xmlString.WriteStartElement("PeriodOfStay");
            xmlString.WriteElementString("CheckInDate", itineary.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("CheckOutDate", itineary.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("HotelRooms");
            HotelRoom[] rooms = itineary.Roomtype;
            int i = 1;
            for (int j = 0; j < rooms.Length; j++)
            {
                xmlString.WriteStartElement("HotelRoom");
                xmlString.WriteAttributeString("Code", rooms[j].RoomTypeCode);
                xmlString.WriteAttributeString("NumberOfCots", rooms[j].NoOfCots.ToString());
                if (rooms[j].ExtraBed)
                {
                    xmlString.WriteAttributeString("ExtraBed", "true");
                }
                else
                {
                    xmlString.WriteAttributeString("ExtraBed", "false");
                }
                xmlString.WriteStartElement("PaxIds");
                for (int k = 0; k < rooms[j].PassenegerInfo.Count; k++)
                {
                    xmlString.WriteElementString("PaxId", i.ToString());
                    i++;
                }
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// This private function is used for Generates the GetBookedHotelRequest.
        /// </summary>
        /// <param name="bookingRefNo">bookingRefNo</param>
        /// <returns>string</returns>
        /// <remarks>This method not for use right now</remarks>
        private string GenerateGetBookedHotelRequest(string bookingRefNo)
        {
            //Trace.TraceInformation("GTA.GenerateGetBookedHotelRequest Entered | " + DateTime.Now);
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchBookingItemRequest");
            xmlString.WriteStartElement("BookingReference");
            xmlString.WriteAttributeString("ReferenceSource", "api");
            xmlString.WriteValue(bookingRefNo);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            //Trace.TraceInformation("GTA.GenerateGetBookedHotelRequest Exit | " + DateTime.Now);
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadResponseSearchBooking
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>bool</returns>
        /// <remarks>This method not for use right now</remarks>
        private bool ReadResponseSearchBooking(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("GTA.ReadResponseSearchBooking Entered | " + DateTime.Now);
            //responseXML = responseXML.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(responseXML);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					" GTA:ReadResponseSearchHotel,Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException(ErrorInfo.InnerText);
            }
            HotelSource sourceInfo = new HotelSource();
            sourceInfo.Load("GTA");
            decimal ourCommission = sourceInfo.OurCommission;
            int commissionType = 0;
            commissionType = (int)sourceInfo.CommissionTypeId;
            StaticData staticInfo = new StaticData();
            Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
            rateOfExList = staticInfo.CurrencyROE;
            decimal rateOfExchange = 1m;
            XmlNode itemInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingReferences");
            if (itemInfo == null || itemInfo.InnerText.Length <= 0)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					" No Hotels found ! | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                throw new BookingEngineException("No Hotels found");
            }
            XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/Item");
            if (tempNode == null)
            {
                //Trace.TraceError("Error : Hotel Code Not Found...");
                throw new BookingEngineException("Hotel Code Not Found...");
            }
            itinerary.HotelCode = tempNode.Attributes[0].Value;
            itinerary.HotelName = tempNode.InnerText;
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/ItemCity");
            if (tempNode != null)
            {
                itinerary.CityCode = tempNode.Attributes[0].Value;
                itinerary.CityRef = tempNode.InnerText;
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/ItemPrice");
                if (tempNode != null)
                {
                    for (int i = 0; i < tempNode.Attributes.Count; i++)
                    {
                        if (tempNode.Attributes[i].Name == "Nett")
                        {
                            double price = Convert.ToDouble(tempNode.Attributes[i].Value);
                        }
                    }
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/ItemConfirmationReference");
                if (tempNode != null)
                {
                    itinerary.ConfirmationNo = tempNode.InnerText;
                }
                else
                {
                    itinerary.ConfirmationNo = "Not Available";
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/ItemStatus");
                if (tempNode != null)
                {
                    string bookingStatus = tempNode.InnerText;
                    if (bookingStatus == "Cancelled")
                    {
                        itinerary.Status = HotelBookingStatus.Cancelled;
                    }
                    else
                    {
                        if (bookingStatus == "Confirmed")
                        {
                            itinerary.Status = HotelBookingStatus.Confirmed;
                        }
                        else
                        {
                            if (bookingStatus == "Failed")
                            {
                                itinerary.Status = HotelBookingStatus.Failed;
                            }
                            else
                            {
                                if (bookingStatus == "Pending Confirmation")
                                {
                                    itinerary.Status = HotelBookingStatus.Pending;
                                }
                                else
                                {
                                    if (bookingStatus == "Pending")
                                    {
                                        itinerary.Status = HotelBookingStatus.Pending;
                                    }
                                    else
                                    {
                                        if (bookingStatus == "Error")
                                        {
                                            itinerary.Status = HotelBookingStatus.Error;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/HotelItem/PeriodOfStay/CheckInDate");
                if (tempNode != null)
                {
                    itinerary.StartDate = Convert.ToDateTime(tempNode.InnerText);
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/HotelItem/PeriodOfStay/CheckOutDate");
                if (tempNode != null)
                {
                    itinerary.EndDate = Convert.ToDateTime(tempNode.InnerText);
                }
                XmlNodeList tempNodeList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchBookingItemResponse/PaxNames/PaxName");
                List<HotelPassenger> paxList = new List<HotelPassenger>();
                int iPax = 0;
                foreach (XmlNode node in tempNodeList)
                {
                    HotelPassenger pax = new HotelPassenger();
                    int id = Convert.ToInt32(node.Attributes[0].Value);
                    pax.PaxId = id;
                    tempNode = node.SelectSingleNode("text()");
                    pax.Firstname = tempNode.Value;
                    if (node.Attributes.Count == 3)
                    {
                        if (node.Attributes[1].Value == "child")
                        {
                            pax.PaxType = HotelPaxType.Child;
                            pax.Age = (int)Convert.ToInt16(node.Attributes[2].Value);
                        }
                        else
                        {
                            pax.PaxType = HotelPaxType.Adult;
                        }
                    }
                    else
                    {
                        pax.PaxType = HotelPaxType.Adult;
                    }
                    pax.Lastname = string.Empty;
                    pax.Addressline1 = string.Empty;
                    pax.LeadPassenger = (iPax == 0);
                    paxList.Add(pax);
                    iPax++;
                }
                itinerary.HotelPassenger = paxList[0];
                List<string> amenities = new List<string>();
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/HotelItem/Meals");
                IEnumerator enumerator;
                if (tempNode != null)
                {
                    XmlNodeList mealNodes = tempNode.ChildNodes;
                    enumerator = mealNodes.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            XmlNode meal = (XmlNode)enumerator.Current;
                            tempNode = meal.SelectSingleNode("text()");
                            amenities.Add(tempNode.Value);
                        }
                    }
                    finally
                    {
                        IDisposable disposable = enumerator as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                }
                XmlNodeList roomList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/HotelItem/HotelRooms/HotelRoom");
                itinerary.NoOfRooms = roomList.Count;
                HotelRoom[] hotelRooms = new HotelRoom[itinerary.NoOfRooms];
                int iRoom = 0;
                roomTypeDetails.Clear();
                enumerator = roomList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        XmlNode room = (XmlNode)enumerator.Current;
                        if (room != null)
                        {
                            hotelRooms[iRoom] = new HotelRoom();
                            Dictionary<string, string> roomData = new Dictionary<string, string>();
                            XmlAttributeCollection atbList = room.Attributes;
                            foreach (XmlAttribute atb in atbList)
                            {
                                roomData.Add(atb.Name, atb.Value);
                            }
                            string roomCode = string.Empty;
                            bool isExtraBed = false;
                            bool isSharingBedding = false;
                            int noOfExtraBed = 0;
                            int noOfCots = 0;
                            if (roomData.ContainsKey("Code"))
                            {
                                roomCode = roomData["Code"];
                            }
                            if (roomData.ContainsKey("ExtraBed"))
                            {
                                isExtraBed = Convert.ToBoolean(roomData["ExtraBed"]);
                            }
                            if (roomData.ContainsKey("NumberOfExtraBeds"))
                            {
                                noOfExtraBed = Convert.ToInt32(roomData["NumberOfExtraBeds"]);
                            }
                            if (roomData.ContainsKey("NumberOfCots"))
                            {
                                noOfCots = Convert.ToInt32(roomData["NumberOfCots"]);
                            }
                            if (roomData.ContainsKey("SharingBedding"))
                            {
                                isSharingBedding = Convert.ToBoolean(roomData["SharingBedding"]);
                            }
                            if (isExtraBed && noOfExtraBed == 0)
                            {
                                noOfExtraBed = 1;
                            }
                            roomTypeDetails.Add(string.Concat(new object[]
							{
								roomCode,
								"|",
								noOfCots,
								"|",
								noOfExtraBed
							}));
                            tempNodeList = room.SelectNodes("PaxIds/PaxId");
                            List<int> paxInRoom = new List<int>();
                            foreach (XmlNode node in tempNodeList)
                            {
                                tempNode = node.SelectSingleNode("text()");
                                paxInRoom.Add((int)Convert.ToInt16(tempNode.Value));
                            }
                            hotelRooms[iRoom].RoomTypeCode = roomCode;
                            List<HotelPassenger> roomPaxList = new List<HotelPassenger>();
                            RoomGuestData roomGuest = GenerateRoomGuest(paxList, paxInRoom, ref roomPaxList);
                            hotelRooms[iRoom].PassenegerInfo = roomPaxList;
                            hotelRooms[iRoom].AdultCount = roomGuest.noOfAdults;
                            hotelRooms[iRoom].ChildCount = roomGuest.noOfChild;
                            hotelRooms[iRoom].ChildAge = roomGuest.childAge;
                            hotelRooms[iRoom].Ameneties = amenities;
                            hotelRooms[iRoom].ExtraBed = isExtraBed;
                            hotelRooms[iRoom].NoOfExtraBed = noOfExtraBed;
                            hotelRooms[iRoom].SharingBed = isSharingBedding;
                            hotelRooms[iRoom].RatePlanCode = roomCode + itinerary.HotelCode;
                            hotelRooms[iRoom].NoOfCots = 0;
                            hotelRooms[iRoom].NoOfUnits = "1";
                            string roomTypeCode = hotelRooms[iRoom].RoomTypeCode;
                            if (roomTypeCode != null)
                            {
                                if (!(roomTypeCode == "SB"))
                                {
                                    if (!(roomTypeCode == "DB"))
                                    {
                                        if (!(roomTypeCode == "TS"))
                                        {
                                            if (!(roomTypeCode == "TR"))
                                            {
                                                if (!(roomTypeCode == "TB"))
                                                {
                                                    if (roomTypeCode == "Q")
                                                    {
                                                        hotelRooms[iRoom].RoomName = "Quad Room";
                                                    }
                                                }
                                                else
                                                {
                                                    hotelRooms[iRoom].RoomName = "Twin Room";
                                                }
                                            }
                                            else
                                            {
                                                hotelRooms[iRoom].RoomName = "Triple Room";
                                            }
                                        }
                                        else
                                        {
                                            hotelRooms[iRoom].RoomName = "Twin for Sole use";
                                        }
                                    }
                                    else
                                    {
                                        hotelRooms[iRoom].RoomName = "Double Room";
                                    }
                                }
                                else
                                {
                                    hotelRooms[iRoom].RoomName = "Single Room";
                                }
                            }
                            iRoom++;
                        }
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                HotelDetails hotelInfo = default(HotelDetails);
                hotelInfo = GetItemInformation(itinerary.CityCode, itinerary.HotelName, itinerary.HotelCode);
                itinerary.Rating = hotelInfo.hotelRating;
                if (hotelInfo.Address == null)
                {
                    itinerary.HotelAddress1 = "--NA--";
                }
                else
                {
                    itinerary.HotelAddress1 = hotelInfo.Address;
                }
                itinerary.Map = hotelInfo.Map;
                itinerary.LastModifiedOn = DateTime.UtcNow;
                itinerary.Roomtype = hotelRooms;
                if (hotelInfo.Email != null && hotelInfo.Email.Trim().Length > 0 && hotelInfo.Email != "NA" && hotelInfo.Email != "NA-NA")
                {
                    HotelItinerary expr_B82 = itinerary;
                    expr_B82.HotelAddress2 = expr_B82.HotelAddress2 + "\n E-mail :" + hotelInfo.Email;
                }
                if (hotelInfo.PhoneNumber != null && hotelInfo.PhoneNumber.Trim().Length > 0 && hotelInfo.PhoneNumber != "NA" && hotelInfo.PhoneNumber != "NA-NA")
                {
                    HotelItinerary expr_BF0 = itinerary;
                    expr_BF0.HotelAddress2 = expr_BF0.HotelAddress2 + "\n Phone No: " + hotelInfo.PhoneNumber;
                }
                if (hotelInfo.FaxNumber != null && hotelInfo.FaxNumber.Trim().Length > 0 && hotelInfo.FaxNumber != "NA" && hotelInfo.FaxNumber != "NA-NA")
                {
                    HotelItinerary expr_C5E = itinerary;
                    expr_C5E.HotelAddress2 = expr_C5E.HotelAddress2 + "\n Fax : " + hotelInfo.FaxNumber;
                }
                GTACity gtaCity = new GTACity();
                country = gtaCity.GetCountryNameforCityCode(itinerary.CityCode);
                currency = gtaCity.GetCurrencyForCountry(country);
                if (rateOfExList.ContainsKey(currency))
                {
                    rateOfExchange = rateOfExList[currency];
                }
                decimal discount = 0m;
                Dictionary<string, RoomRates[]> fareBreakUp = GetItemPriceBreakdown(itinerary.BookingRefNo, ref discount);
                HotelRoom[] rooms = itinerary.Roomtype;
                HotelRoom[] array = rooms;
                for (int j = 0; j < array.Length; j++)
                {
                    HotelRoom room2 = array[j];
                    decimal totNetFare = 0m;
                    decimal totMarkUp = 0m;
                    decimal tempdisc = discount / rooms.Length;
                    RoomRates[] roomRates = fareBreakUp[room2.RoomTypeCode];
                    HotelRoomFareBreakDown[] fareBD = new HotelRoomFareBreakDown[roomRates.Length];
                    for (int iDay = 0; iDay < roomRates.Length; iDay++)
                    {
                        fareBD[iDay] = new HotelRoomFareBreakDown();
                        fareBD[iDay].Date = roomRates[iDay].Days;
                        if (commissionType == 1)
                        {
                            fareBD[iDay].RoomPrice = roomRates[iDay].Amount + Convert.ToDecimal(ourCommission * roomRates[iDay].Amount) / 100m;
                            totMarkUp += Convert.ToDecimal(ourCommission * roomRates[iDay].Amount) / 100m;
                        }
                        if (commissionType == 2)
                        {
                            fareBD[iDay].RoomPrice = roomRates[iDay].Amount + Convert.ToDecimal(ourCommission / rateOfExchange);
                            totMarkUp += Convert.ToDecimal(ourCommission / rateOfExchange);
                        }
                        totNetFare += roomRates[iDay].Amount;
                    }
                    if (commissionType == 1)
                    {
                        tempdisc += Convert.ToDecimal(ourCommission * discount) / 100m;
                    }
                    if (commissionType == 2)
                    {
                        tempdisc += Convert.ToDecimal(ourCommission / rateOfExchange);
                    }
                    room2.RoomFareBreakDown = fareBD;
                    room2.Price = new PriceAccounts
                    {
                        Currency = currency,
                        NetFare = totNetFare,
                        Markup = totMarkUp,
                        Discount = tempdisc,
                        AccPriceType = PriceType.NetFare,
                        RateOfExchange = rateOfExchange
                    };
                }
                itinerary.Roomtype = rooms;
                bool isValidResponse = true;
                //Trace.TraceInformation("GTA.ReadResponseSearchBooking Exit | " + DateTime.Now);
                return isValidResponse;
            }
            //Trace.TraceError("Error : Hotel City Not Found...");
            throw new BookingEngineException("Hotel City Not Found...");
        }
        /// <summary>
        /// GenerateRoomGuest
        /// </summary>
        /// <param name="paxList">paxList</param>
        /// <param name="paxInRoom">paxInRoom</param>
        /// <param name="roomPaxList">roomPaxList</param>
        /// <returns>RoomGuestData</returns>
        /// <remarks>RoomGuestData Object</remarks>
        private RoomGuestData GenerateRoomGuest(List<HotelPassenger> paxList, List<int> paxInRoom, ref List<HotelPassenger> roomPaxList)
        {
            //Trace.TraceInformation("GTA.GenerateRoomGuest Entered | " + DateTime.Now);
            List<int> childAge = new List<int>();
            foreach (int id in paxInRoom)
            {
                foreach (HotelPassenger hp in paxList)
                {
                    if (id == hp.PaxId)
                    {
                        roomPaxList.Add(hp);
                        if (hp.Age > 0 && hp.Age < 18)
                        {
                            childAge.Add(hp.Age);
                        }
                    }
                }
            }
            RoomGuestData roomGuest = default(RoomGuestData);
            roomGuest.noOfChild = childAge.Count;
            roomGuest.childAge = childAge;
            roomGuest.noOfAdults = paxInRoom.Count - childAge.Count;
            //Trace.TraceInformation("GTA.GenerateRoomGuest Exit | " + DateTime.Now);
            return roomGuest;
        }
        /// <summary>
        /// GenerateItemPriceBreakdownRequest
        /// </summary>
        /// <param name="bookingRefNo">bookingRefNo</param>
        /// <returns>string</returns>
        ///<remarks>This method not for use right now</remarks>
        private string GenerateItemPriceBreakdownRequest(string bookingRefNo)
        {
            //Trace.TraceInformation("GTA.GenerateItemPriceBreakdownRequest Entered | " + DateTime.Now);
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("BookingItemPriceBreakdownRequest");
            xmlString.WriteStartElement("BookingReference");
            xmlString.WriteAttributeString("ReferenceSource", "api");
            xmlString.WriteString(bookingRefNo);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemReference");
            xmlString.WriteString("1");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            //Trace.TraceInformation("GTA.GenerateItemPriceBreakdownRequest Exit | " + DateTime.Now);
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadResponseItemPriceBreakdown
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="discount">discount</param>
        /// <returns></returns>
        ///<remarks>This method not for use right now</remarks>
        private Dictionary<string, RoomRates[]> ReadResponseItemPriceBreakdown(XmlDocument xmlDoc, ref decimal discount)
        {
            //Trace.TraceInformation("GTA.ReadResponseItemPriceBreakdown Entered | " + DateTime.UtcNow);
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingItemPriceBreakdownResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAFareBreakDown, Severity.High, 0, string.Concat(new object[]
				{
					"GTA:ReadResponseItemPriceBreakdown, Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingItemPriceBreakdownResponse/BookingItem/HotelItem/PeriodOfStay");
            DateTime startDate = Convert.ToDateTime(tempNode.SelectSingleNode("CheckInDate/text()").Value);
            TimeSpan diffResult = Convert.ToDateTime(tempNode.SelectSingleNode("CheckOutDate/text()").Value).Subtract(startDate);
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingItemPriceBreakdownResponse/BookingItem/ItemPrice");
            decimal totPrice = Convert.ToDecimal(tempNode.Attributes[1].Value);
            discount = 0m;
            for (int i = 0; i < tempNode.Attributes.Count; i++)
            {
                if (tempNode.Attributes[i].Name == "IncludedOfferDiscount")
                {
                    discount = Convert.ToDecimal(tempNode.Attributes[i].Value);
                }
            }
            XmlNodeList roomList = xmlDoc.SelectNodes("/Response/ResponseDetails/BookingItemPriceBreakdownResponse/BookingItem/HotelItem/HotelRooms/HotelRoom");
            Dictionary<string, RoomRates[]> fareBreakList = new Dictionary<string, RoomRates[]>();
            foreach (XmlNode room in roomList)
            {
                RoomRates[] rates = new RoomRates[diffResult.Days];
                int nights = 0;
                decimal gross = 0m;
                int j = 0;
                XmlNodeList priceList = room.SelectNodes("PriceRanges/PriceRange");
                foreach (XmlNode price in priceList)
                {
                    tempNode = price.SelectSingleNode("Price");
                    if (tempNode != null)
                    {
                        for (int k = 0; k < tempNode.Attributes.Count; k++)
                        {
                            string text = tempNode.Attributes[k].Name.ToString();
                            if (text != null)
                            {
                                if (!(text == "Nights"))
                                {
                                    if (text == "Gross")
                                    {
                                        gross = Convert.ToDecimal(tempNode.Attributes[k].Value);
                                    }
                                }
                                else
                                {
                                    nights = Convert.ToInt32(tempNode.Attributes[k].Value);
                                }
                            }
                        }
                        if (nights > 0 && gross >= 0m)
                        {
                            for (int i = 0; i < nights; i++)
                            {
                                RoomRates rate = default(RoomRates);
                                rate.Days = startDate.AddDays(Convert.ToDouble(j));
                                rate.Amount = gross;
                                rate.BaseFare = gross;
                                rate.Totalfare = gross;
                                rate.RateType = RateType.Published;
                                rates[j++] = rate;
                            }
                        }
                    }
                }
                if (room.Attributes[0].Value.Trim() == "CH")
                {
                    fareBreakList.Add(room.Attributes[0].Value.Trim() + "|" + room.Attributes[1].Value, rates);
                }
                else
                {
                    fareBreakList.Add(room.Attributes[0].Value.Trim(), rates);
                }
            }
            //Trace.TraceInformation("GTA.ReadResponseItemPriceBreakdown Exit | " + DateTime.UtcNow);
            return fareBreakList;
        }
        /// <summary>
        ///  GetItemPriceBreakdown for the bookingRefNo Search criteria.
        /// </summary>
        /// <param name="bookingRefNo">bookingRefNo</param>
        /// <param name="discount">discount</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method is not for use right now</remarks>
        public Dictionary<string, RoomRates[]> GetItemPriceBreakdown(string bookingRefNo, ref decimal discount)
        {
            //Hotel
            //Trace.TraceInformation("GTA.GetItemPriceBreakdown Entered | " + DateTime.UtcNow);
            string request = GenerateItemPriceBreakdownRequest(bookingRefNo);
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            Dictionary<string, RoomRates[]> fareBreakList = new Dictionary<string, RoomRates[]>();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetItemPriceBreakdown Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    fareBreakList = ReadResponseItemPriceBreakdown(xmlResp, ref discount);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					"Exception returned from GTA.GetItemPriceBreakdown Error Message:",
					ex.Message,
					" | ",
					DateTime.Now,
					"| request XML",
					request,
					"|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("GTA.GetItemPriceBreakdown Exit | " + DateTime.UtcNow);
            return fareBreakList;
        }
        /// <summary>
        /// This private function is used for Generate ItemChargeCondition Request
        /// </summary>
        /// <param name="bookingRefNo">bookingRefNo</param>
        /// <returns>string</returns>
        /// <remarks>This method is not for use right now</remarks>
        private string GenerateGetItemChargeConditionRequest(string bookingRefNo)
        {
            //Trace.TraceInformation("GTA.GenerateGetItemChargeConditionRequest Entered | " + DateTime.UtcNow);
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchChargeConditionsRequest");
            xmlString.WriteStartElement("ChargeConditionsBookingItem");
            xmlString.WriteStartElement("BookingReference");
            xmlString.WriteAttributeString("ReferenceSource", "api");
            xmlString.WriteString(bookingRefNo);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemReference");
            xmlString.WriteString("1");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            //Trace.TraceInformation("GTA.GenerateGetItemChargeConditionRequest Exit | " + DateTime.UtcNow);
            return strWriter.ToString();
        }

        /// <summary>
        /// This private function is used for SearchHotelAvailabilityRequest
        /// </summary>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        /// <remarks>This method is not for use right now</remarks>
        private string GenerateSearchHotelAvailabilityRequest(HotelItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", currency);
            xmlString.WriteAttributeString("Country", country);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchHotelAvailabilityRequest");
            xmlString.WriteStartElement("ItemDestination");
            xmlString.WriteAttributeString("DestinationType", "city");
            xmlString.WriteAttributeString("DestinationCode", itineary.CityCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ImmediateConfirmationOnly");
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemName");
            xmlString.WriteCData(itineary.HotelName);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("PeriodOfStay");
            xmlString.WriteElementString("CheckInDate", itineary.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("Duration", itineary.EndDate.Subtract(itineary.StartDate).Days.ToString());
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("Rooms");
            HotelRoom[] rooms = itineary.Roomtype;
            for (int i = 0; i < rooms.Length; i++)
            {
                xmlString.WriteStartElement("Room");
                xmlString.WriteAttributeString("Code", rooms[i].RoomTypeCode);
                xmlString.WriteAttributeString("NumberOfCots", rooms[i].NoOfCots.ToString());
                if (rooms[i].ExtraBed)
                {
                    xmlString.WriteAttributeString("ExtraBed", "true");
                }
                else
                {
                    xmlString.WriteAttributeString("ExtraBed", "false");
                }
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// ReadResponseSearchHotelAvailability
        /// </summary>
        /// <param name="xmlDoc">response xml</param>
        /// <returns>bool</returns>
        /// <remarks>This method is not for use right now</remarks>
        private bool ReadResponseSearchHotelAvailability(XmlDocument xmlDoc)
        {
            //Trace.TraceInformation("GTA.ReadResponseSearchHotelAvailability Entered | " + DateTime.Now);
            bool isValidResponse = true;
            bool isAvail = false;
            //responseXML = responseXML.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(responseXML);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchHotelAvailabilityResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					" GTA:ReadResponseSearchHotelAvailability,Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException(ErrorInfo.InnerText);
            }
            if (isValidResponse)
            {
                XmlNodeList availList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchHotelAvailabilityResponse/HotelDetails/Hotel/RoomAvailability/Availability");
                foreach (XmlNode Availiblity in availList)
                {
                    XmlNode tempNode = Availiblity.SelectSingleNode("Confirmation");
                    if (tempNode != null)
                    {
                        if (!(tempNode.Attributes[0].Value == "IM"))
                        {
                            isAvail = false;
                            break;
                        }
                        isAvail = true;
                    }
                }
            }
            //Trace.TraceInformation("GTA.ReadResponseSearchHotelAvailability Exit | " + DateTime.Now);
            return isAvail;
        }

        /// <summary>
        /// ReadResponseModifyBookingItem
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="itineary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <remarks>This method is not for use right now</remarks>
        private void ReadResponseModifyBookingItem(XmlDocument xmlDoc, ref HotelItinerary itineary)
        {
            //Trace.TraceInformation("GTA.ReadResponseModifyBookingItem Entered | " + DateTime.Now);
            bool isValidResponse = true;
            //responseXML = responseXML.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(responseXML);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					" GTA:ReadResponseModifyBookingItem,Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
					xmlDoc.OuterXml
				}), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException(ErrorInfo.InnerText);
            }
            if (isValidResponse)
            {
                XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemStatus");
                if (tempNode != null)
                {
                    string text = tempNode.Attributes[0].Value.Trim();
                    if (text != null)
                    {
                        if (text == "C")
                        {
                            itineary.Status = HotelBookingStatus.Confirmed;
                            goto IL_16E;
                        }
                        if (text == "CP")
                        {
                            itineary.Status = HotelBookingStatus.Pending;
                            goto IL_16E;
                        }
                    }
                    itineary.Status = HotelBookingStatus.Failed;
                IL_16E: ;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemConfirmationReference");
                if (tempNode != null)
                {
                    itineary.ConfirmationNo = tempNode.InnerText;
                }
                else
                {
                    itineary.ConfirmationNo = "Not Available";
                }
            }
            //Trace.TraceInformation("GTA.ReadResponseModifyBookingItem Exit | " + DateTime.Now);
        }

        /// <summary>
        /// ReadResponseUpdatedStatus
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>bool</returns>
        /// <remarks>This method is not for use right now</remarks>
        private bool ReadResponseUpdatedStatus(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {
            //Trace.TraceInformation("GTA.ReadResponseUpdatedStatus Entered | " + DateTime.Now);
            bool isValidResponse = false;
            //responseXML = responseXML.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(responseXML);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAImportHotel, Severity.High, 0, string.Concat(new object[]
				{
					" GTA:ReadResponseSearchHotel,Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException(ErrorInfo.InnerText);
            }
            XmlNode itemInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingReferences");
            if (itemInfo != null && itemInfo.InnerText.Length > 0)
            {
                isValidResponse = true;
                XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchBookingItemResponse/BookingItems/BookingItem/ItemStatus");
                if (tempNode != null)
                {
                    string bookingStatus = tempNode.InnerText;
                    if (bookingStatus == "Cancelled")
                    {
                        itinerary.Status = HotelBookingStatus.Cancelled;
                    }
                    else
                    {
                        if (bookingStatus == "Confirmed")
                        {
                            itinerary.Status = HotelBookingStatus.Confirmed;
                        }
                        else
                        {
                            if (bookingStatus == "Failed")
                            {
                                itinerary.Status = HotelBookingStatus.Failed;
                            }
                            else
                            {
                                if (bookingStatus == "Pending Confirmation")
                                {
                                    itinerary.Status = HotelBookingStatus.Pending;
                                }
                                else
                                {
                                    if (bookingStatus == "Pending")
                                    {
                                        itinerary.Status = HotelBookingStatus.Pending;
                                    }
                                    else
                                    {
                                        if (bookingStatus == "Error")
                                        {
                                            itinerary.Status = HotelBookingStatus.Error;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Trace.TraceInformation("GTA.ReadResponseUpdatedStatus Exit| " + DateTime.Now);
            return isValidResponse;
        }

        /// <summary>
        /// GenerateLocations
        /// </summary>
        /// <param name="cityCode">cityCode</param>
        /// <returns>string</returns>
        /// <remarks>This method is not for use right now</remarks>
        private string GenerateLocations(string cityCode)
        {
            //Trace.TraceInformation("GTA.GenerateLocations Request Entered | " + DateTime.UtcNow);
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchLocationRequest");
            xmlString.WriteAttributeString("CityCode", cityCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            //Trace.TraceInformation("GTA.GenerateLocations Request Exit | " + DateTime.UtcNow);
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadResponseLocations
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <returns>List object</returns>
        /// <remarks>This method is not for use right now</remarks>
        private List<string> ReadResponseLocations(XmlDocument xmlDoc)
        {
            //Trace.TraceInformation("GTA.ReadResponseLocations Entered | " + DateTime.Now);
            //responseXML = responseXML.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(responseXML);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            List<string> locList = new List<string>();
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchLocationResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
				{
					" GTA:ReadResponseLocations,Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException(ErrorInfo.InnerText);
            }
            XmlNodeList tempNodeList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchLocationResponse/LocationDetails/Location");
            foreach (XmlNode location in tempNodeList)
            {
                XmlNode tempNode = location.SelectSingleNode("text()");
                if (tempNode != null)
                {
                    locList.Add(tempNode.Value);
                }
            }
            //Trace.TraceInformation("GTA.ReadResponseLocations Exit | " + DateTime.Now);
            return locList;
        }
        /// <summary>
        /// IsValidRequest
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <returns>bool</returns>
        /// <remarks>Max 9 pax only Allowed per Booking and roomscount should be lessthen of pax count </remarks>
        private bool IsValidRequest(HotelRequest req)
        {
            bool isValidRequest = true;
            int paxCount = 0;
            for (int i = 0; i < req.NoOfRooms; i++)
            {
                paxCount += req.RoomGuest[i].noOfAdults + req.RoomGuest[i].noOfChild;
            }
            if (paxCount > 9 || GetRoomType(req).Count != req.NoOfRooms)
            {
                isValidRequest = false;
            }
            return isValidRequest;
        }

        #region SightSeeing
        /// <summary>
        ///  Get the SightSeeings for the destination Search criteria.
        /// </summary>
        /// <param name="req">req(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup</param>
        /// <param name="markupType">B2B Markup Type(like F(Fixed),P(Percentage))</param>
        /// <returns>SightseeingSearchResult[]</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public SightseeingSearchResult[] GetSightseeingAvailability(SightSeeingReguest req, decimal markup, string markupType)
        {
            Audit.Add(EventType.GTAAvailSearch, Severity.Normal, 1, "GTA.GetSightseeingAvailability entered", "0");
            GTACity cityInfo = new GTACity();
            //currency = "AED";// cityInfo.GetCurrencyForCountry(req.CountryName);
            string request = GenerateGetSightseeingAvailabilityRequest(req);
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(request);
                try
                {
                    string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASearchSightSeeingPriceRequest.xml";
                    XmlDoc.Save(filePath);
                }
                catch { }
            }
            catch { }

            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            SightseeingSearchResult[] searchRes = new SightseeingSearchResult[0];
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetSightseeingAvailability Error Message:",
                    ex.Message,
                    " | ",
                    DateTime.Now,
                    "| request XML",
                    request,
                    "|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = ReadSightseeingAvailabilityResponse(xmlResp, req, markup, markupType);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetSightseeingAvailability Error Message:",
                    ex.Message,
                    " | ",
                    DateTime.Now,
                    "| request XML",
                    request,
                    "|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
            {
                " Response from GTA for Search Request:| ",
                DateTime.Now,
                "| request XML:",
                request,
                "|response XML:",
                xmlResp.OuterXml
            }), "");
            return searchRes;
        }

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="req">req(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <returns>string</returns>
        /// <remarks>preparing XML for the request</remarks>
        private string GenerateGetSightseeingAvailabilityRequest(SightSeeingReguest req)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", req.Currency);
            xmlString.WriteAttributeString("Country", country);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            for (int i = 0; i < req.NoDays + 1; i++)
            {
                xmlString.WriteStartElement("SearchSightseeingPriceRequest");
                xmlString.WriteStartElement("ItemDestination");
                xmlString.WriteAttributeString("DestinationType", "city");
                xmlString.WriteAttributeString("DestinationCode", req.DestinationCode);
                xmlString.WriteEndElement();
                xmlString.WriteStartElement("ImmediateConfirmationOnly");
                xmlString.WriteEndElement();
                if (!string.IsNullOrEmpty(req.ItemCode))
                {
                    xmlString.WriteElementString("ItemName", req.ItemName);
                    xmlString.WriteElementString("ItemCode", req.ItemCode);
                }
                DateTime date = req.TourDate.AddDays(i);
                xmlString.WriteElementString("TourDate", date.ToString("yyyy-MM-dd"));
                xmlString.WriteElementString("NumberOfAdults", req.NoOfAdults.ToString());
                xmlString.WriteStartElement("Children");
                foreach (int childAge in req.ChildrenList)
                {
                    xmlString.WriteElementString("Age", childAge.ToString());
                }
                xmlString.WriteEndElement();
                xmlString.WriteStartElement("TypeCodes");

                foreach (string typeCode in req.TypeCodeList)
                {
                    xmlString.WriteElementString("TypeCode", typeCode);
                }
                xmlString.WriteEndElement();
                foreach (string categoryCode in req.CategoryCodeList)
                {
                    xmlString.WriteElementString("CategoryCode", categoryCode);
                }
                //xmlString.WriteStartElement("IncludeChargeConditions");
                //xmlString.WriteAttributeString("DateFormatResponse", "true");
                //xmlString.WriteEndElement();  //end IncludeChargeConditions

                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// Used in reading response XML and assigning SightSeeing Results to the response.
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="req">req(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup</param>
        /// <param name="markupType">B2B Markup Type(like F(Fixed),P(Percentage))</param>
        /// <returns>SightseeingSearchResult[]</returns>
        /// <remarks>Here only we are calucating Markup and InputVat</remarks>
        private SightseeingSearchResult[] ReadSightseeingAvailabilityResponse(XmlDocument xmlDoc, SightSeeingReguest req, decimal markup, string markupType)
        {
            //Trace.TraceInformation("GTA.ReadGetSightseeingAvailabilityResponse entered");
            SightseeingSearchResult[] searchRes = new SightseeingSearchResult[0];
            try
            {
                //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                //xmlDoc.Load("C:/Developments/DotNet/uAPI/SightSeeing/24122015_054311_GTASearchSightSeeingPriceResponse.xml");


                try
                {
                    string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASearchSightSeeingPriceResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }

                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchSightseeingPriceResponse/Errors/Error/ErrorText/text()");
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, string.Concat(new object[]
                    {
                        " GTA:ReadGetSightseeingAvailabilityResponse,Error Message:",
                        ErrorInfo.Value,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        xmlDoc.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(req.CountryCode, req.LoginCountryCode, sourceCountryCode, (int)ProductType.SightSeeing, Module.SightSeeing.ToString(), DestinationType.International.ToString());
                XmlNodeList sightseeingList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchSightseeingPriceResponse/SightseeingDetails/Sightseeing");
                searchRes = new SightseeingSearchResult[sightseeingList.Count];
                int i = 0;
                foreach (XmlNode nodeSightseeing in sightseeingList)
                {
                    SightseeingSearchResult sightseeingResult = new SightseeingSearchResult();
                    sightseeingResult.HasExtraInfo = false;
                    sightseeingResult.HasFlash = false;
                    sightseeingResult.DepaturePointRequired = false;
                    sightseeingResult.Source = SightseeingBookingSource.GTA;
                    for (int j = 0; j < nodeSightseeing.Attributes.Count; j++)
                    {
                        string name2 = nodeSightseeing.Attributes[j].Name;
                        if (name2 != null)
                        {
                            if (!(name2 == "HasExtraInfo"))
                            {
                                if (!(name2 == "HasFlash"))
                                {
                                    if (name2 == "DeparturePointRequired")
                                    {
                                        if (nodeSightseeing.Attributes[j].Value == "true")
                                        {
                                            sightseeingResult.DepaturePointRequired = true;
                                        }
                                        else
                                        {
                                            sightseeingResult.DepaturePointRequired = false;
                                        }
                                    }
                                }
                                else
                                {
                                    if (nodeSightseeing.Attributes[j].Value == "true")
                                    {
                                        sightseeingResult.HasFlash = true;
                                    }
                                    else
                                    {
                                        sightseeingResult.HasFlash = false;
                                    }
                                }
                            }
                            else
                            {
                                if (nodeSightseeing.Attributes[j].Value == "true")
                                {
                                    sightseeingResult.HasExtraInfo = true;
                                }
                                else
                                {
                                    sightseeingResult.HasExtraInfo = false;
                                }
                            }
                        }
                    }
                    XmlNode tempNode = nodeSightseeing.SelectSingleNode("City");
                    if (tempNode != null)
                    {
                        sightseeingResult.CityCode = tempNode.Attributes["Code"].Value;
                        sightseeingResult.CityName = tempNode.FirstChild.Value;
                    }
                    else
                    {
                        sightseeingResult.CityCode = req.DestinationCode;
                        sightseeingResult.CityName = "";
                    }
                    tempNode = nodeSightseeing.SelectSingleNode("Item");
                    if (tempNode != null)
                    {
                        sightseeingResult.ItemCode = tempNode.Attributes["Code"].Value;
                        sightseeingResult.ItemName = tempNode.FirstChild.Value;
                    }
                    else
                    {
                        sightseeingResult.ItemCode = "";
                        sightseeingResult.ItemName = "";
                    }
                    string pattern = req.ItemName;
                    #region To get only those satisfy the search conditions
                    if (!(pattern != null && pattern.Length > 0 ? sightseeingResult.ItemName.ToUpper().Contains(pattern.ToUpper()) : true))
                    {
                        continue;
                    }
                    #endregion
                    tempNode = nodeSightseeing.SelectSingleNode("Duration/text()");
                    if (tempNode != null)
                    {
                        sightseeingResult.Duration = tempNode.Value;
                    }
                    else
                    {
                        sightseeingResult.Duration = "";
                    }
                    XmlNodeList tempNodeList = nodeSightseeing.SelectNodes("AdditionalInformation/Information");
                    string[] AdditionalInformationList = new string[tempNodeList.Count];
                    int count = 0;
                    foreach (XmlNode tempNodeAdditionalInfo in tempNodeList)
                    {
                        AdditionalInformationList[count++] = tempNodeAdditionalInfo.FirstChild.Value;
                    }
                    sightseeingResult.AdditionalInformationList = AdditionalInformationList;
                    sightseeingResult.SightseeingTypeList = new Dictionary<string, string>();
                    tempNodeList = nodeSightseeing.SelectNodes("SightseeingTypes/SightseeingType");
                    foreach (XmlNode nodeType in tempNodeList)
                    {
                        sightseeingResult.SightseeingTypeList.Add(nodeType.Attributes["Code"].Value, nodeType.FirstChild.Value);
                    }
                    sightseeingResult.SightseeingCategoryList = new Dictionary<string, string>();
                    tempNodeList = nodeSightseeing.SelectNodes("SightseeingCategories/SightseeingCategory");
                    foreach (XmlNode nodeCategory in tempNodeList)
                    {
                        sightseeingResult.SightseeingCategoryList.Add(nodeCategory.Attributes["Code"].Value, nodeCategory.FirstChild.Value);
                    }
                    tempNodeList = nodeSightseeing.SelectNodes("TourOperations/TourOperation");
                    TourOperation[] tourList = new TourOperation[tempNodeList.Count];
                    count = 0;
                    foreach (XmlNode tempNodeTourOperation in tempNodeList)
                    {
                        TourOperation tourOperation = default(TourOperation);
                        XmlNodeList languageList = tempNodeTourOperation.SelectNodes("TourLanguages/TourLanguage");
                        List<string> langCode = new List<string>();
                        List<string> langListCode = new List<string>();
                        List<string> langName = new List<string>();
                        foreach (XmlNode tempNodeLanguage in languageList)
                        {
                            for (int j = 0; j < tempNodeLanguage.Attributes.Count; j++)
                            {
                                string name2 = tempNodeLanguage.Attributes[j].Name;
                                if (name2 != null)
                                {
                                    if (!(name2 == "Code"))
                                    {
                                        if (name2 == "LanguageListCode")
                                        {
                                            if (!langListCode.Contains(tempNodeLanguage.Attributes[j].Value))
                                            {
                                                langListCode.Add(tempNodeLanguage.Attributes[j].Value);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        langCode.Add(tempNodeLanguage.Attributes[j].Value);
                                    }
                                }
                            }
                            langName.Add(tempNodeLanguage.SelectSingleNode("text()").Value);
                        }
                        tourOperation.TourLanguageList = langListCode;
                        tourOperation.LanguageCode = langCode;
                        tourOperation.LangName = langName;
                        tourOperation.Currency = currency;
                        tempNode = tempNodeTourOperation.SelectSingleNode("ItemPrice");
                        decimal totPrice = 0m;
                        decimal ItemPrice = 0m;
                        decimal totalB2BMarkup = 0m;
                        decimal vatAmount = 0m;
                        if (tempNode != null)
                        {

                            #region price calu
                            tourOperation.Currency = tempNode.Attributes["Currency"].Value;
                            rateOfExchange = exchangeRates[tourOperation.Currency];
                            ItemPrice = decimal.Parse(tempNode.FirstChild.Value);
                            totPrice = Math.Round((ItemPrice) * rateOfExchange, decimalPoint);
                            //Calucate Input Vat Amount
                            if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                            {
                                totPrice = priceTaxDet.InputVAT.CalculateVatAmount(totPrice, ref vatAmount, decimalPoint);
                            }
                            totPrice = Math.Round(totPrice, decimalPoint);
                            tourOperation.PriceInfo = new PriceAccounts();
                            tourOperation.PriceInfo.SupplierCurrency = tourOperation.Currency;
                            tourOperation.PriceInfo.SupplierPrice = ItemPrice;
                            tourOperation.PriceInfo.RateOfExchange = rateOfExchange;
                            tourOperation.PriceInfo.Currency = agentCurrency;
                            tourOperation.PriceInfo.CurrencyCode = agentCurrency;
                            totalB2BMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(totPrice) * (markup / 100m)));
                            tourOperation.PriceInfo.Markup = Math.Round(totalB2BMarkup, decimalPoint);
                            tourOperation.PriceInfo.MarkupType = markupType;
                            tourOperation.PriceInfo.MarkupValue = markup;
                            tourOperation.ItemPrice = Math.Round(totPrice, decimalPoint);
                            tourOperation.PriceInfo.NetFare = totPrice;
                            tourOperation.PriceInfo.TaxDetails = new PriceTaxDetails();
                            tourOperation.PriceInfo.TaxDetails = priceTaxDet;
                            tourOperation.PriceInfo.InputVATAmount = vatAmount;
                            #endregion
                        }
                        tourOperation.ConfirmationCodeList = new Dictionary<string, string>();
                        XmlNodeList confirmationList = tempNodeTourOperation.SelectNodes("Confirmation");
                        foreach (XmlNode tempNodeConfirmation in confirmationList)
                        {
                            string code = tempNodeConfirmation.Attributes["Code"].Value;
                            string name = tempNodeConfirmation.FirstChild.Value;
                            tourOperation.ConfirmationCodeList.Add(code, name);
                        }
                        tourOperation.SpecialItemList = new List<string>();
                        XmlNodeList specilItemList = tempNodeTourOperation.SelectNodes("SpecialItem");
                        foreach (XmlNode splItem in specilItemList)
                        {
                            string code = splItem.Attributes["Code"].Value;
                            tourOperation.SpecialItemList.Add(code);
                            tourOperation.SpecialItemName = splItem.InnerText;
                        }
                        tourList[count++] = tourOperation;
                    }
                    sightseeingResult.TourOperationList = tourList;
                    tempNodeList = nodeSightseeing.SelectNodes("EssentialInformation/Information");
                    EssentialInformation[] essentialList = new EssentialInformation[tempNodeList.Count];
                    count = 0;
                    SightseeingStaticData staticInfo = new SightseeingStaticData();
                    sightseeingResult.EssentialInformationList = essentialList;

                    if (sightseeingResult.HasExtraInfo)
                    {
                        staticInfo = GetSightseeingItemInformation(sightseeingResult.CityCode, sightseeingResult.ItemName, sightseeingResult.ItemCode);
                        sightseeingResult.Image = staticInfo.ImageInfo;
                        sightseeingResult.Description = staticInfo.Description;
                        sightseeingResult.FlashLink = staticInfo.FlashLink;
                    }
                    searchRes[i++] = sightseeingResult;
                }
                if (searchRes.Length > i)
                {
                    Array.Resize(ref searchRes, i);
                }
            }
            catch (Exception e)
            {
                throw new BookingEngineException("sightseeing search error : " + e.ToString());
            }


            SightseeingSearchResult tempResult = new SightseeingSearchResult();
            for (int i = 0; i < searchRes.Length - 1; i++)
            {
                for (int j = i + 1; j < searchRes.Length; j++)
                {
                    if (searchRes[j].TourOperationList[0].ItemPrice < searchRes[i].TourOperationList[0].ItemPrice)
                    {
                        tempResult = searchRes[i];
                        searchRes[i] = searchRes[j];
                        searchRes[j] = tempResult;
                    }
                }
            }
            //Trace.TraceInformation("GTA.ReadGetSightseeingAvailabilityResponse completed");
            return searchRes;
        }
        /// <summary>
        /// Get the staticData for the itemCode Search criteria.
        /// </summary>
        /// <param name="cityCode">Selected CityCode</param>
        /// <param name="itemName">Selected Item</param>
        /// <param name="itemCode">Selected ItemCode</param>
        /// <returns>SightseeingStaticData</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public SightseeingStaticData GetSightseeingItemInformation(string cityCode, string itemName, string itemCode)
        {
            SightseeingStaticData sightseeingInfo = new SightseeingStaticData();
            bool isSendReq = true;
            bool isUpdateReq = false;
            sightseeingInfo.Load(itemCode, cityCode, SightseeingBookingSource.GTA);
            int timeStampDays = Convert.ToInt32(ConfigurationSystem.GTAConfig["TimeStamp"]);
            if (sightseeingInfo.Description != null && sightseeingInfo.Description.Length > 0)
            {
                isSendReq = false;
                if (DateTime.UtcNow.Subtract(sightseeingInfo.TimeStamp).Days > timeStampDays)
                {
                    isUpdateReq = true;
                }
            }
            if (isSendReq || isUpdateReq)
            {
                string request = GenerateGetSightseeingItemInformationRequest(cityCode, itemName, itemCode);
                try
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(request);
                    string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASearchItemInformationRequest.xml";
                    XmlDoc.Save(filePath);
                }
                catch { }
                //string resp = string.Empty;
                XmlDocument xmlResp = new XmlDocument();
                try
                {
                    xmlResp = SendRequest(request);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
                    {
                        "Exception returned from GTA.GetSightseeingItemInformation Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    sightseeingInfo = new SightseeingStaticData();
                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        sightseeingInfo = ReadGetSightseeingItemInformationResponse(xmlResp.OuterXml);
                    }
                    if (isUpdateReq)
                    {
                        sightseeingInfo.Update();
                    }
                    else
                    {
                        sightseeingInfo.Save();
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
                    {
                        "Exception returned from GTA.GetSightseeingItemInformation Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            return sightseeingInfo;
        }
        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="cityCode">Selected CityCode</param>
        /// <param name="itemName">Selected Item</param>
        /// <param name="itemCode">Selected ItemCode</param>
        /// <returns>string</returns>
        /// <remarks>preparing XML for the request</remarks>
        private string GenerateGetSightseeingItemInformationRequest(string cityCode, string itemName, string itemCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchItemInformationRequest");
            xmlString.WriteAttributeString("ItemType", "sightseeing");
            xmlString.WriteStartElement("ItemDestination");
            xmlString.WriteAttributeString("DestinationType", "city");
            xmlString.WriteAttributeString("DestinationCode", cityCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemName");
            xmlString.WriteString(itemName);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemCode");
            xmlString.WriteString(itemCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// Used in reading response XML and assigning SightSeeingStatic data to the response.
        /// </summary>
        /// <param name="response">xml response</param>
        /// <returns>SightseeingStaticData</returns>
        /// <remarks>Static data Loading</remarks>
        private SightseeingStaticData ReadGetSightseeingItemInformationResponse(string response)
        {
            SightseeingStaticData sightseeingInfo = new SightseeingStaticData();
            try
            {
                response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
                TextReader stringRead = new StringReader(response);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stringRead);
                try
                {
                    string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASearchItemInformationResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch
                {
                }
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchSightseeingPriceResponse/Errors/Error/ErrorText/text()");
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
                    {
                        " GTA:ReadGetSightseeingItemInformationResponse,Error Message:",
                        ErrorInfo.Value,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        response
                    }), "");
                    //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                XmlNode nodeItemDetail = xmlDoc.SelectSingleNode("Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail");
                if (nodeItemDetail == null)
                {
                    throw new Exception("No Item found from the reponse");
                }
                XmlNode tempNode = nodeItemDetail.SelectSingleNode("City");
                if (tempNode != null)
                {
                    sightseeingInfo.CityCode = tempNode.Attributes["Code"].Value;
                }
                else
                {
                    sightseeingInfo.CityCode = "";
                }
                tempNode = nodeItemDetail.SelectSingleNode("Item");
                if (tempNode != null)
                {
                    sightseeingInfo.ItemCode = tempNode.Attributes["Code"].Value;
                    sightseeingInfo.ItemName = tempNode.FirstChild.Value;
                }
                else
                {
                    sightseeingInfo.ItemCode = "";
                    sightseeingInfo.ItemName = "";
                }
                tempNode = nodeItemDetail.SelectSingleNode("SightseeingInformation/TourSummary/text()");
                if (tempNode != null)
                {
                    sightseeingInfo.Description = tempNode.Value;
                }
                else
                {
                    sightseeingInfo.Description = "";
                }
                tempNode = nodeItemDetail.SelectSingleNode("SightseeingInformation/TheTour/text()");
                if (tempNode != null)
                {
                    sightseeingInfo.TourInfo = tempNode.Value;
                }
                else
                {
                    sightseeingInfo.TourInfo = "";
                }
                tempNode = nodeItemDetail.SelectSingleNode("SightseeingInformation/Includes/text()");
                if (tempNode != null)
                {
                    sightseeingInfo.Includes = tempNode.Value;
                }
                else
                {
                    sightseeingInfo.Includes = "";
                }
                tempNode = nodeItemDetail.SelectSingleNode("SightseeingInformation/PleaseNote/text()");
                if (tempNode != null)
                {
                    sightseeingInfo.Notes = tempNode.Value;
                }
                else
                {
                    sightseeingInfo.Notes = "";
                }
                tempNode = nodeItemDetail.SelectSingleNode("SightseeingInformation/DeparturePoints/text()");
                if (tempNode != null)
                {
                    sightseeingInfo.DeparturePoint = tempNode.Value;
                }
                else
                {
                    sightseeingInfo.DeparturePoint = "";
                }
                List<TourOperations> tourOperationsList = new List<TourOperations>();
                XmlNodeList tempNodeList = nodeItemDetail.SelectNodes("SightseeingInformation/TourOperations/TourOperation");
                foreach (XmlNode nodeTourOperation in tempNodeList)
                {
                    TourOperations tourOperInfo = new TourOperations();
                    string language = string.Empty;
                    XmlNodeList languageNodeList = nodeTourOperation.SelectNodes("TourLanguages/TourLanguage");
                    foreach (XmlNode nodeLanguage in languageNodeList)
                    {
                        for (int i = 0; i < nodeLanguage.Attributes.Count; i++)
                        {
                            string name = nodeLanguage.Attributes[i].Name;
                            if (name != null)
                            {
                                if (!(name == "Code"))
                                {
                                    if (!(name == "LanguageListCode"))
                                    {
                                    }
                                }
                                else
                                {
                                    language = language + nodeLanguage.Attributes[i].Value + "|";
                                }
                            }
                        }
                    }
                    if (language.Length > 0)
                    {
                        tourOperInfo.Language = language;
                    }
                    else
                    {
                        tourOperInfo.Language = "Unescorted";
                    }
                    tempNode = nodeTourOperation.SelectSingleNode("Commentary/text()");
                    if (tempNode != null)
                    {
                        tourOperInfo.Commentary = tempNode.Value;
                    }
                    else
                    {
                        tourOperInfo.Commentary = "";
                    }
                    tempNode = nodeTourOperation.SelectSingleNode("DateRange/FromDate/text()");
                    if (tempNode != null)
                    {
                        tourOperInfo.FromDate = DateTime.Parse(tempNode.Value);
                    }
                    tempNode = nodeTourOperation.SelectSingleNode("DateRange/ToDate/text()");
                    if (tempNode != null)
                    {
                        tourOperInfo.ToDate = DateTime.Parse(tempNode.Value);
                    }
                    tempNode = nodeTourOperation.SelectSingleNode("Frequency/text()");
                    if (tempNode != null)
                    {
                        tourOperInfo.Frequency = tempNode.Value;
                    }
                    else
                    {
                        tourOperInfo.Frequency = "";
                    }
                    XmlNodeList depList = nodeTourOperation.SelectNodes("Departures/Departure");
                    foreach (XmlNode departureInfo in depList)
                    {
                        TourOperations tempTourInfo = new TourOperations();
                        tempTourInfo.CityCode = tourOperInfo.CityCode;
                        tempTourInfo.Commentary = tourOperInfo.Commentary;
                        tempTourInfo.FromDate = tourOperInfo.FromDate;
                        tempTourInfo.ToDate = tourOperInfo.ToDate;
                        tempTourInfo.ItemCode = tourOperInfo.ItemCode;
                        tempTourInfo.Frequency = tourOperInfo.Frequency;
                        tempTourInfo.Language = tourOperInfo.Language;
                        tempNode = departureInfo.SelectSingleNode("FirstService");
                        if (tempNode != null)
                        {
                            tempTourInfo.FromTime = tempNode.InnerText;
                        }
                        else
                        {
                            tempTourInfo.FromTime = "";
                        }
                        tempNode = departureInfo.SelectSingleNode("LastService");
                        if (tempNode != null)
                        {
                            tempTourInfo.ToTime = tempNode.InnerText;
                        }
                        else
                        {
                            tempTourInfo.ToTime = "";
                        }
                        tempNode = departureInfo.SelectSingleNode("Intervals");
                        if (tempNode != null)
                        {
                            tempTourInfo.Interval = tempNode.InnerText;
                        }
                        else
                        {
                            tempTourInfo.Interval = "-";
                        }
                        tempNode = departureInfo.SelectSingleNode("Time");
                        if (tempNode != null)
                        {
                            tempTourInfo.DepTime = tempNode.InnerText;
                        }
                        else
                        {
                            tempTourInfo.DepTime = "";
                        }
                        tempNode = departureInfo.SelectSingleNode("DeparturePoint");
                        if (tempNode != null)
                        {
                            tempTourInfo.DepCode = tempNode.Attributes[0].Value;
                            tempTourInfo.DepName = tempNode.SelectSingleNode("text()").Value;
                        }
                        else
                        {
                            tempTourInfo.DepCode = "";
                            tempTourInfo.DepName = "";
                        }
                        tempNode = departureInfo.SelectSingleNode("Telephone");
                        if (tempNode != null)
                        {
                            tempTourInfo.Telephone = tempNode.InnerText;
                        }
                        else
                        {
                            tempTourInfo.Telephone = "";
                        }
                        tempNode = departureInfo.SelectSingleNode("AddressLines");
                        if (tempNode != null)
                        {
                            tempTourInfo.Address = tempNode.InnerText;
                        }
                        else
                        {
                            tempTourInfo.Address = "";
                        }
                        tourOperationsList.Add(tempTourInfo);
                    }
                }
                tempNodeList = nodeItemDetail.SelectNodes("SightseeingInformation/Links/ImageLinks/ImageLink");
                string images = string.Empty;
                foreach (XmlNode nodeImageLink in tempNodeList)
                {
                    tempNode = nodeImageLink.SelectSingleNode("Image/text()");
                    images = images + tempNode.Value + "|";
                }
                sightseeingInfo.ImageInfo = images;
                //sightseeingInfo.DownloadImage(images, SightseeingBookingSource.GTA);
                tempNodeList = nodeItemDetail.SelectNodes("SightseeingInformation/ClosedDates/ClosedDate");
                string dates = string.Empty;
                foreach (XmlNode nodeInfo in tempNodeList)
                {
                    tempNode = nodeInfo.SelectSingleNode("text()");
                    dates = dates + tempNode.Value + "|";
                }
                sightseeingInfo.ClosedDates = dates;
                string flashList = string.Empty;
                tempNodeList = nodeItemDetail.SelectNodes("SightseeingInformation/Links/FlashLinks/FlashLink");
                foreach (XmlNode nodeFlashLink in tempNodeList)
                {
                    flashList = flashList + nodeFlashLink.FirstChild.Value + "|";
                }
                sightseeingInfo.FlashLink = flashList;
                sightseeingInfo.Source = SightseeingBookingSource.GTA;
                sightseeingInfo.TourOperationInfo = tourOperationsList;
            }
            catch (Exception ex)
            {
                throw new BookingEngineException("ReadSightseeingItemInformationResponse error : " + ex.ToString());
            }
            //Trace.TraceInformation("GTA.ReadSightseeingItemInformationResponse completed");
            return sightseeingInfo;
        }

        /// <summary>
        /// Get the ChargeConditions for the Selected Tour criteria.
        /// </summary>
        /// <param name="req">req(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="result">result(SightSeeing Results object)</param>
        /// <param name="chosenTour">Selected Tour</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public Dictionary<string, string> GetSightseeingChargeCondition(SightSeeingReguest req, SightseeingSearchResult result, int chosenTour)
        {
            string request = GenerateGetSightseeingChargeConditionRequest(req, result, chosenTour);
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(request);
                try
                {
                    string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAChargeConditionssightSeeingRequest.xml";
                    XmlDoc.Save(filePath);
                }
                catch { }
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            Dictionary<string, string> res;
            try
            {
                xmlResp = SendRequest(request);
                res = ReadGetSightseeingChargeConditionResponse(req, xmlResp);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingSearch, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetSightseeingChargeCondition Error Message:",
                    ex.Message,
                    " | ",
                    DateTime.Now,
                    "| request XML",
                    request,
                    "|response XML",
                    xmlResp.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            return res;
        }

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="req">req(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="result">result(SightSeeing Results object)</param>
        /// <param name="chosenTour">Selected Tour</param>
        /// <returns>string</returns>
        /// <remarks>preparing XML for the request</remarks>
        private string GenerateGetSightseeingChargeConditionRequest(SightSeeingReguest req, SightseeingSearchResult result, int chosenTour)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", "AED");
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchChargeConditionsRequest");
            xmlString.WriteStartElement("ChargeConditionsSightseeing");
            HotelCity hCity = HotelCity.Load(Convert.ToInt32(req.DestinationCode));
            string DestinationCode = hCity.GTACode;
            xmlString.WriteElementString("City", DestinationCode);
            xmlString.WriteElementString("Item", result.ItemCode);
            xmlString.WriteElementString("TourDate", req.TourDate.ToString("yyyy-MM-dd"));
           
            if (result.TourOperationList[chosenTour].LanguageCode != null && result.TourOperationList[chosenTour].LanguageCode.Count > 0)
            {
                xmlString.WriteElementString("TourLanguage", result.TourOperationList[chosenTour].LanguageCode[0]);
            }
            if (result.TourOperationList[chosenTour].TourLanguageList != null)
            {
                foreach (string langCode in result.TourOperationList[chosenTour].TourLanguageList)
                {
                    xmlString.WriteElementString("TourLanguageList", langCode);
                }
            }
            if (result.TourOperationList[chosenTour].SpecialItemList != null)
            {
                foreach (string code in result.TourOperationList[chosenTour].SpecialItemList)
                {
                    xmlString.WriteElementString("SpecialCode", code);
                }
            }
            xmlString.WriteElementString("NumberOfAdults", req.NoOfAdults.ToString());
            if (req.ChildrenList.Count > 0)
            {
                xmlString.WriteStartElement("Children");
                foreach (int age in req.ChildrenList)
                {
                    xmlString.WriteElementString("Age", age.ToString());
                }
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// Used in reading response XML and assigning Dictionary to the response.
        /// </summary>
        /// <param name="req">req(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="xmlDoc">xml response</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Loading cancellation policy</remarks>
        private Dictionary<string, string> ReadGetSightseeingChargeConditionResponse(SightSeeingReguest req, XmlDocument xmlDoc)
        {
            //    DateTime startDate = req.TourDate;
            Dictionary<string, string> polList = new Dictionary<string, string>();
            //    try
            //    {
            //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAChargeConditionsSightSeeingResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchSightseeingPriceResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            //        SightseeingSource sourceInfo = new SightseeingSource();
            //        sourceInfo.Load("GTA");
            //        StaticData roe = new StaticData();
            //        Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
            //        rateOfExList = roe.CurrencyROE;
            //        List<SightseeingPenalty> penList = new List<SightseeingPenalty>();

            #region Cancelation Policy
            //Cancelation Policy 
            XmlNodeList chargeList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchChargeConditionsResponse/ChargeConditions/ChargeCondition");
            string message = string.Empty;
            string lastCancelllation = string.Empty;
            string amendmentMessage = "Amedment Not Allowed";
            lastCancelllation = "0";
            List<SightseeingPenalty> penList = new List<SightseeingPenalty>();
            foreach (XmlNode charge in chargeList)
            {
                if (charge.Attributes[0].Value == "cancellation")
                {
                    XmlNodeList individualList = charge.SelectNodes("Condition");
                    int buffer = Convert.ToInt32(ConfigurationSystem.GTAConfig["buffer"]);
                    foreach (XmlNode chargeInfo in individualList)
                    {
                        bool isCharge = false;
                        int fromDay = 0;
                        int toDay = 0;
                        decimal chargeAmt = 0m;
                        string curr = string.Empty;
                        for (int m = 0; m < chargeInfo.Attributes.Count; m++)
                        {
                            string text = chargeInfo.Attributes[m].Name.ToString();
                            if (text != null)
                            {
                                if (!(text == "Charge"))
                                {
                                    if (!(text == "FromDay"))
                                    {
                                        if (!(text == "ToDay"))
                                        {
                                            if (!(text == "Currency"))
                                            {
                                                if (text == "ChargeAmount")
                                                {
                                                    chargeAmt = Convert.ToDecimal(chargeInfo.Attributes[m].Value);
                                                }
                                            }
                                            else
                                            {
                                                curr = chargeInfo.Attributes[m].Value;
                                            }
                                        }
                                        else
                                        {
                                            toDay = (int)Convert.ToInt16(chargeInfo.Attributes[m].Value);
                                        }
                                    }
                                    else
                                    {
                                        fromDay = (int)Convert.ToInt16(chargeInfo.Attributes[m].Value);
                                    }
                                }
                                else
                                {
                                    isCharge = Convert.ToBoolean(chargeInfo.Attributes[m].Value);
                                }
                            }

                        }
                        if (isCharge)
                        {
                            rateOfExchange = exchangeRates[curr];
                            //chargeAmt = Math.Ceiling(chargeAmt * rateOfExchange);
                            chargeAmt = Convert.ToDecimal((chargeAmt * rateOfExchange).ToString("N" + decimalPoint));
                            chargeAmt += chargeAmt * Convert.ToDecimal(ConfigurationSystem.GTAConfig["CancellationMarkup"]) / 100m;
                            string symbol = agentCurrency;
                            //string symbol = GetCurrencySymbol(curr);
                            if (fromDay == toDay)
                            {
                                //int extraDays = 0;
                                object obj = message;
                                message = string.Concat(new object[]
								{
									obj,
                                    //DateTime.Now.ToString("dd MMM yyyy"),
                                    //" onwards, ",
                                    //symbol,
                                    //" ",
                                    //chargeAmt,
									"Non-refundable and full charge will apply once booking is completed."
								});
                            }
                            if (fromDay != toDay && fromDay == 0 && toDay > 0)
                            {
                                int extraDays = 0;
                                object obj = message;
                                message = string.Concat(new object[]
								{
									obj,req.TourDate.AddDays((double)(-(double)(toDay + buffer + extraDays))).ToString("dd/MMM/yyyy")," onwards You will be charged ",
                                    symbol,
                                    " ",
                                    chargeAmt
								});
                            }
                            else
                            {
                                if (fromDay != toDay)
                                {
                                    int extraDays = 0;
                                    object obj = message;
                                    message = string.Concat(new object[]
									{
                                        obj,req.TourDate.AddDays((double)(-(double)(fromDay + buffer + extraDays))).ToString("dd/MMM/yyyy")," earlier  You will be charged ",
                                    symbol,
                                    " ",
                                    chargeAmt
									});
                                }
                            }
                            message += "|";
                        }
                    }
                }
                else
                {
                    if (charge.Attributes[1].Value == "amendment")//Amendent buffer is not being applied from GTAConfig.XML as the same is yet to be implemented
                    {
                        amendmentMessage = string.Empty;
                        XmlNodeList individualList = charge.SelectNodes("Condition");
                        int buffer = Convert.ToInt32(ConfigurationSystem.GTAConfig["amendmentBuffer"]);
                        foreach (XmlNode chargeInfo in individualList)
                        {
                            SightseeingPenalty penalityInfo = new SightseeingPenalty();
                            penalityInfo.BookingSource = SightseeingBookingSource.GTA;
                            penalityInfo.PolicyType = SightseeingChangePoicyType.Amendment;
                            bool isCharge = false;
                            bool isAllowed = true;
                            int fromDay = 0;
                            int toDay = -1;
                            decimal chargeAmt = 0m;
                            string curr = string.Empty;
                            for (int m = 0; m < chargeInfo.Attributes.Count; m++)
                            {
                                string text = chargeInfo.Attributes[m].Name.ToString();
                                if (text != null)
                                {
                                    if (!(text == "Charge"))
                                    {
                                        if (!(text == "FromDay"))
                                        {
                                            if (!(text == "ToDay"))
                                            {
                                                if (!(text == "Currency"))
                                                {
                                                    if (!(text == "ChargeAmount"))
                                                    {
                                                        if (text == "Allowable")
                                                        {
                                                            isAllowed = Convert.ToBoolean(chargeInfo.Attributes[m].Value);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        chargeAmt = Convert.ToDecimal(chargeInfo.Attributes[m].Value);
                                                    }
                                                }
                                                else
                                                {
                                                    curr = chargeInfo.Attributes[m].Value;
                                                }
                                            }
                                            else
                                            {
                                                toDay = (int)Convert.ToInt16(chargeInfo.Attributes[m].Value);
                                            }
                                        }
                                        else
                                        {
                                            fromDay = (int)Convert.ToInt16(chargeInfo.Attributes[m].Value);
                                        }
                                    }
                                    else
                                    {
                                        isCharge = Convert.ToBoolean(chargeInfo.Attributes[m].Value);
                                    }
                                }
                            }
                            if (isAllowed && isCharge)
                            {
                                rateOfExchange = exchangeRates[curr];
                                chargeAmt = Math.Ceiling(chargeAmt * rateOfExchange);
                                chargeAmt += chargeAmt * Convert.ToDecimal(ConfigurationSystem.GTAConfig["CancellationMarkup"]) / 100m;
                                chargeAmt = Math.Round(chargeAmt, Convert.ToInt32(ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                                string symbol = agentCurrency;
                                //string symbol = GetCurrencySymbol(curr);
                                if (fromDay == toDay)
                                {
                                    int extraDays = 0;
                                    if (req.TourDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        extraDays = 1;
                                    }
                                    else
                                    {
                                        if (req.TourDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            extraDays = 2;
                                        }
                                    }
                                    penalityInfo.FromDate = req.TourDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                    penalityInfo.ToDate = req.TourDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                    object obj = amendmentMessage;
                                    amendmentMessage = string.Concat(new object[]
									{
										obj,
										"Amendment upto ",
										fromDay + buffer + extraDays,
										" day(s) before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                    penalityInfo.Remarks = string.Concat(new object[]
									{
										"Amendment upto ",
										fromDay + buffer + extraDays,
										" day(s) before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                }
                                if (fromDay != toDay && fromDay == 0 && toDay > 0)
                                {
                                    int extraDays = 0;
                                    if (req.TourDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        extraDays = 1;
                                    }
                                    else
                                    {
                                        if (req.TourDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            extraDays = 2;
                                        }
                                    }
                                    penalityInfo.FromDate = req.TourDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                    penalityInfo.ToDate = req.TourDate.AddDays((double)(-(double)(toDay + buffer + extraDays)));
                                    object obj = amendmentMessage;
                                    amendmentMessage = string.Concat(new object[]
									{
										obj,
										"Amendment upto ",
										toDay + buffer + extraDays,
										" day(s) before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                    penalityInfo.Remarks = string.Concat(new object[]
									{
										"Amendment upto ",
										toDay + buffer + extraDays,
										" day(s) before the date of arrival, ",
										symbol,
										" ",
										chargeAmt,
										" will be charged."
									});
                                }
                                else
                                {
                                    if (fromDay != toDay)
                                    {
                                        int extraDays = 0;
                                        if (req.TourDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                        {
                                            extraDays = 1;
                                        }
                                        else
                                        {
                                            if (req.TourDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                            {
                                                extraDays = 2;
                                            }
                                        }
                                        if (toDay == -1)
                                        {
                                            penalityInfo.FromDate = DateTime.Now;
                                            penalityInfo.ToDate = req.TourDate;
                                        }
                                        else
                                        {
                                            penalityInfo.FromDate = req.TourDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                            penalityInfo.ToDate = req.TourDate.AddDays((double)(-(double)(toDay + buffer + extraDays)));
                                        }
                                        object obj = amendmentMessage;
                                        amendmentMessage = string.Concat(new object[]
										{
											obj,
											"Amendment from ",
											fromDay + buffer + extraDays,
											" days to ",
											toDay + buffer,
											" days before the date of arrival, ",
											symbol,
											" ",
											chargeAmt,
											" will be charged."
										});
                                        penalityInfo.Remarks = string.Concat(new object[]
										{
											"Amendment from ",
											fromDay + buffer + extraDays,
											" days to ",
											toDay + buffer,
											" days before the date of arrival, ",
											symbol,
											" ",
											chargeAmt,
											" will be charged."
										});
                                    }
                                }
                                penalityInfo.Price = chargeAmt;
                                penalityInfo.IsAllowed = isAllowed;
                                penList.Add(penalityInfo);
                                amendmentMessage += "|";
                            }
                            else
                            {
                                penalityInfo.IsAllowed = isAllowed;
                                penalityInfo.FromDate = DateTime.Now;
                                penalityInfo.ToDate = req.TourDate;
                                penalityInfo.Price = 0m;
                                if (isAllowed)
                                {
                                    penalityInfo.Remarks = "No Amendment Charges before " + (fromDay + buffer) + " days of CheckIn.";
                                }
                                else
                                {
                                    penalityInfo.Remarks = "Amendment is Not Allowed.";
                                }
                                amendmentMessage = amendmentMessage + "|" + penalityInfo.Remarks;
                                penList.Add(penalityInfo);
                            }
                        }
                    }
                }
            }
            polList.Add("CancelPolicy", message);
            polList.Add("AmendmentPolicy", amendmentMessage);
            #endregion
            return polList;
        }

        /// <summary>
        /// Get the Booking for the Selected Tour criteria.
        /// </summary>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding tour details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public BookingResponse GetSightseeingBooking(ref SightseeingItinerary itineary)
        {
            //Trace.TraceInformation("GTAApi.GetSightseeingBooking entered");
            GTACity cityInfo = new GTACity();
            //currency = cityInfo.GetCurrencyForCountry(cityInfo.GetCountryNameforCityCode(itineary.CityCode));
            string requestXml = GenerateGetSightseeingBookingRequest(itineary);

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(requestXml);
                string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAGenerateBookingRequest.xml";
                doc.Save(filePath);
            }
            catch { }

            BookingResponse bookingRes = default(BookingResponse);
            //string resp;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(requestXml);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingBooking, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetSightseeingBooking Error Message:",
                    ex.Message,
                    " | Request XML:",
                    requestXml,
                    DateTime.Now
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.SightseeingBooking, Severity.High, 0, string.Concat(new object[]
            {
                "Booking Response from GTA. Response:",
                xmlResp.OuterXml,
                " | Request XML:",
                requestXml,
                DateTime.Now
            }), "");
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    bookingRes = ReadSightseeingBookingResponse(xmlResp, ref itineary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingBooking, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetSightseeingBooking Error Message:",
                    ex.Message,
                    " | Request XML:",
                    requestXml,
                    "| Response XML :",
                    xmlResp.OuterXml,
                    DateTime.Now
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw ex; //new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("GTA.GetSightseeingBooking exiting");
            return bookingRes;
        }

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding tour details)</param>
        /// <returns>string</returns>
        /// <remarks>preparing XML for the request</remarks>
        private string GenerateGetSightseeingBookingRequest(SightseeingItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", "AED");
            xmlString.WriteAttributeString("Country", country);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("AddBookingRequest");
            xmlString.WriteElementString("BookingName", "BOOKT" + TraceIdGeneration());
            xmlString.WriteElementString("BookingReference", "BookTRef" + TraceIdGeneration());
            xmlString.WriteElementString("AgentReference", "CZT" + TraceIdGeneration());
            xmlString.WriteElementString("BookingDepartureDate", itineary.TourDate.ToString("yyyy-MM-dd"));
            xmlString.WriteStartElement("PaxNames");
            int paxId = 1;
            for (int i = 0; i < itineary.AdultCount; i++)
            {
                xmlString.WriteStartElement("PaxName");
                xmlString.WriteAttributeString("PaxId", paxId.ToString());
                xmlString.WriteCData(itineary.PaxNames[i]);
                xmlString.WriteEndElement();
                paxId++;
            }
            for (int j = 0; j < itineary.ChildCount; j++)
            {
                xmlString.WriteStartElement("PaxName");
                xmlString.WriteAttributeString("PaxId", paxId.ToString());
                xmlString.WriteAttributeString("PaxType", "child");
                xmlString.WriteAttributeString("ChildAge", itineary.ChildAge[j].ToString());
                xmlString.WriteValue(itineary.PaxNames[itineary.AdultCount + j]);
                xmlString.WriteEndElement();
                paxId++;
            }
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("BookingItems");
            xmlString.WriteStartElement("BookingItem");
            xmlString.WriteAttributeString("ItemType", "sightseeing");
            xmlString.WriteStartElement("ItemReference");
            xmlString.WriteString("1");
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemCity");
            xmlString.WriteAttributeString("Code", itineary.CityCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("Item");
            xmlString.WriteAttributeString("Code", itineary.ItemCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("SightseeingItem");
            xmlString.WriteElementString("TourDate", itineary.TourDate.ToString("yyyy-MM-dd"));
            if (itineary.Language != null && itineary.Language.Substring(0, 1) != "U")
            {
                string[] lang = itineary.Language.Split(new char[]
                {
                    '|'
                });
                xmlString.WriteStartElement("TourLanguage");
                xmlString.WriteAttributeString("Code", lang[0].Split('#')[1]);
                if (lang.Length > 1)
                {
                    xmlString.WriteAttributeString("LanguageListCode", lang[1]);
                }
                xmlString.WriteEndElement();
            }
            if (itineary.SpecialCode.Length > 0)
            {
                string[] spCodes = itineary.SpecialCode.Split(new char[]
                {
                    '|'
                });
                xmlString.WriteStartElement("SpecialItem");
                for (int i = 0; i < spCodes.Length - 1; i++)
                {
                    xmlString.WriteAttributeString("Code", spCodes[i]);
                }
                xmlString.WriteEndElement();
            }
            if (paxId > 0)
            {
                xmlString.WriteStartElement("PaxIds");
                for (int i = 1; i < paxId; i++)
                {
                    xmlString.WriteElementString("PaxId", i.ToString());
                }
                xmlString.WriteEndElement();
            }
            if (itineary.DepPointInfo != null && itineary.DepPointInfo.Length > 0)
            {
                xmlString.WriteStartElement("TourDeparture");
                xmlString.WriteElementString("Time", itineary.DepTime);
                xmlString.WriteStartElement("DeparturePoint");
                string[] depList = itineary.DepPointInfo.Split(new char[]
                {
                    '|'
                });
                xmlString.WriteAttributeString("Code", depList[0]);
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// Used in reading response XML and assigning BookingResponse to the response.
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding tour details)</param>
        /// <returns>BookingResponse</returns>
        private BookingResponse ReadSightseeingBookingResponse(XmlDocument xmlDoc, ref SightseeingItinerary itineary)
        {
            //Trace.TraceInformation("GTA.ReadSightseeingBookingResponse entered");
            BookingResponse bookingRes = default(BookingResponse);
            try
            {
                //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                try
                {
                    string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTAGenerateBookingResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }

                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
                
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.SightseeingBooking, Severity.High, 0, string.Concat(new object[]
                    {
                        " GTA:ReadGetSightseeingBookingResponse,Error Message:",
                        ErrorInfo.Value,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        xmlDoc.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                XmlNode nodeHead = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse");
                XmlNodeList tempNodeList = nodeHead.SelectNodes("BookingReferences/BookingReference");
                foreach (XmlNode nodeRef in tempNodeList)
                {
                    string text = nodeRef.Attributes["ReferenceSource"].Value;
                    if (text != null)
                    {
                        if (text == "api")
                        {
                            itineary.BookingReference = nodeRef.FirstChild.Value;
                        }
                    }
                }
                XmlNode tempNode = nodeHead.SelectSingleNode("BookingStatus");
                if (tempNode != null)
                {
                    string text = tempNode.Attributes["Code"].Value.Trim();
                    if (text != null)
                    {
                        if (text == "CP")
                        {
                            itineary.BookingStatus = SightseeingBookingStatus.Pending;
                            bookingRes.Status = BookingResponseStatus.Successful;
                            goto IL_250;
                        }
                        if (text == "C")
                        {
                            itineary.BookingStatus = SightseeingBookingStatus.Confirmed;
                            bookingRes.Status = BookingResponseStatus.Successful;
                            goto IL_250;
                        }
                        if (text == "X")
                        {
                            itineary.BookingStatus = SightseeingBookingStatus.Failed;
                            bookingRes.Status = BookingResponseStatus.Successful;
                             goto IL_250;
                        }
                    }
                    itineary.BookingStatus = SightseeingBookingStatus.Error;
                    bookingRes.Status = BookingResponseStatus.Failed;
                IL_250: ;
                }
                tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/ItemConfirmationReference");
                if (tempNode != null)
                {
                    itineary.ConfirmationNo = tempNode.InnerText;
                }
                else
                {
                    itineary.ConfirmationNo = "";
                }
                tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/AddressLines");
                if (tempNode != null)
                {
                    for (int i = 0; i < tempNode.ChildNodes.Count; i++)
                    {
                        if (tempNode.ChildNodes[i].FirstChild != null && tempNode.ChildNodes[i].FirstChild.Value != "")
                        {
                            SightseeingItinerary expr_2F5 = itineary;
                            expr_2F5.DepPointInfo = expr_2F5.DepPointInfo + tempNode.ChildNodes[i].FirstChild.Value + " ";
                        }
                    }
                }
                tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/SightseeingItem/TourSupplier");
                itineary.SupplierInfo = "";
                if (tempNode != null)
                {
                    itineary.SupplierInfo = tempNode.FirstChild.Value;
                }
                tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/Telephone/text()");
                if (tempNode != null)
                {
                    SightseeingItinerary expr_39D = itineary;
                    expr_39D.SupplierInfo = expr_39D.SupplierInfo + "Phone No:" + tempNode.Value;
                }
            }
            catch (Exception ex)
            {
                throw ex;// new BookingEngineException("ReadGetSightseeingBookingResponse error : " + ex.ToString());
            }
            return bookingRes;
        }

        /// <summary>
        /// Cancel the Tour for the Selected confirmation criteria.
        /// </summary>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding tour details)</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public Dictionary<string, string> CancelSightseeingBooking(SightseeingItinerary itineary)
        {
            //Trace.TraceInformation("GTAApi.CancelSightseeingBooking entered");
            string requestXml = GenerateCancelSightseeingBookingRequest(itineary);
            try
            {
                XmlDocument Xmldoc = new XmlDocument();
                Xmldoc.LoadXml(requestXml);
                string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASightSeeingCancellationRequest.xml";
                Xmldoc.Save(filePath);
            }
            catch { }

            Dictionary<string, string> cancelList = new Dictionary<string, string>();
            //string resp;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(requestXml);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingCancel, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.CancelSightseeingBooking Error Message:",
                    ex.Message,
                    " | Request XML:",
                    requestXml,
                    DateTime.Now
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.SightseeingCancel, Severity.High, 0, string.Concat(new object[]
            {
                "Booking Response from GTA. Response:",
                xmlResp.OuterXml,
                " | Request XML:",
                requestXml,
                DateTime.Now
            }), "");
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    cancelList = ReadCancelSightseeingBookingResponse(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingCancel, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.CancelSightseeingBooking Error Message:",
                    ex.Message,
                    " | Request XML:",
                    requestXml,
                    "| Response XML :",
                    xmlResp.OuterXml,
                    DateTime.Now
                }), "");
                //Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Trace.TraceInformation("GTA.CancelSightseeingBooking exiting");
            return cancelList;
        }
        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding tour details)</param>
        /// <returns>string</returns>
        /// <remarks>preparing XML for the request</remarks>
        private string GenerateCancelSightseeingBookingRequest(SightseeingItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            //xmlString.WriteAttributeString("Currency", itineary.Currency);
            xmlString.WriteAttributeString("Country", country);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("CancelBookingRequest");
            xmlString.WriteStartElement("BookingReference");
            xmlString.WriteAttributeString("ReferenceSource", "api");
            xmlString.WriteValue(itineary.BookingReference);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// Used in reading response XML and assigning Dictionary to the response.
        /// </summary>
        /// <param name="xmlDoc">Response xml object</param>
        /// <returns>Dictionary</returns>
        /// <remarks>cancelResponse</remarks>
        private Dictionary<string, string> ReadCancelSightseeingBookingResponse(XmlDocument xmlDoc)
        {
            //Trace.TraceInformation("GTA.ReadCancelSightseeingBookingResponse entered");
            //response = response.Replace("<xml version=\"1.0\" encoding=\"UTF-8\">", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + ConfigurationSystem.GTAConfig["SightLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTASightSeeingCancelResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.SightseeingCancel, Severity.High, 0, string.Concat(new object[]
                {
                    " Error Message:",
                    ErrorInfo.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                }), "");
                //Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemStatus/text()");
            if (tempNode != null)
            {
                cancelInfo.Add("Status", tempNode.Value);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemConfirmationReference");
            if (tempNode != null)
            {
                cancelInfo.Add("ID", tempNode.InnerText);
            }
            tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/BookingItems/BookingItem/ItemFee");
            if (tempNode != null)
            {
                cancelInfo.Add("Currency", tempNode.Attributes[0].Value);
                cancelInfo.Add("Amount", tempNode.InnerText);
            }
            else
            {
                cancelInfo.Add("Currency", "AED");
                cancelInfo.Add("Amount", "0");
            }
            //Trace.TraceInformation("GTA.ReadCancelSightseeingBookingResponse Exit");
            return cancelInfo;
        }
        #endregion

        #region Transfers
        /// <summary>
        /// Get the Transfers for the destination Search criteria.
        /// </summary>
        /// <param name="req">req(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup</param>
        /// <param name="markupType">B2B MarkupType(F:Fixed,P:Percentage)</param>
        /// <returns>TransferSearchResult[]</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public TransferSearchResult[] GetTransferAvailability(TransferRequest req,decimal markup,string markupType)
        {
      
            string request = GenerateGetTransferAvailabilityRequest(req);
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(request);
                string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTATransferAvailabilityRequest.xml";
                XmlDoc.Save(filePath);
            }
            catch { }

            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            TransferSearchResult[] searchRes = new TransferSearchResult[0];
            try
            {
                xmlResp = SendRequest(request);

                try
                {
                    //XmlDocument XmlDoc = new XmlDocument();
                    //XmlDoc.LoadXml(resp);
                    string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTATransferAvailabilityResponse.xml";
                    xmlResp.Save(filePath);
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetTransferAvailability Error Message:",
                    ex.Message,
                    " | ",
                    DateTime.Now,
                    "| request XML",
                    request,
                    "|response XML",
                    xmlResp.OuterXml
                }), "");
               
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                searchRes = ReadGetTransferAvailabilityResponse(xmlResp, req, markup,markupType);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetTransferAvailability Error Message:",
                    ex.Message,
                    " | ",
                    DateTime.Now,
                    "| request XML",
                    request,
                    "|response XML",
                    xmlResp.OuterXml
                }), "");
               
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.TransferSearch, Severity.High, 0, string.Concat(new object[]
            {
                " Response from GTA for Transfer Search Request:| ",
                DateTime.Now,
                "| request XML:",
                request,
                "|response XML:",
                xmlResp.OuterXml
            }), "");
            return searchRes;
        }

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="req">req(this object is what we serched params like(cityname,checkinData.....)</param>
        /// <returns>string</returns>
        /// <remarks>preparing XML for the request</remarks>
        private string GenerateGetTransferAvailabilityRequest(TransferRequest req)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", req.Currency);
           
            xmlString.WriteAttributeString("Country", req.NationalityCode);  //changed on 08062016
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchTransferPriceRequest");
            xmlString.WriteStartElement("ImmediateConfirmationOnly");
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("TransferPickUp");
            xmlString.WriteElementString("PickUpCityCode", req.PickUpCityCode);
            xmlString.WriteElementString("PickUpCode", req.PickUpCode);
            if (req.PickUpCityCode != "")
            {
                xmlString.WriteElementString("PickUpPointCode", req.PickUpPointCode);
            }
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("TransferDropOff");
            xmlString.WriteElementString("DropOffCityCode", req.DropOffCityCode);
            xmlString.WriteElementString("DropOffCode", req.DropOffCode);
            if (req.DropOffPointCode != "")
            {
                xmlString.WriteElementString("DropOffPointCode", req.DropOffPointCode);
            }
            xmlString.WriteEndElement();
            xmlString.WriteElementString("TransferDate", req.TrasnferDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("NumberOfPassengers", req.NumberOfPassengers.ToString());
            xmlString.WriteElementString("PreferredLanguage", req.PreferredLanguage.Substring(0, 1));
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();




            return strWriter.ToString();
        }

        /// <summary>
        /// Used in reading response XML and assigning Transfers Results to the response.
        /// </summary>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="req">req(this object is what we serched params like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup</param>
        /// <param name="markupType">B2B MarkupType(F:Fixed,P:Percentage)</param>
        /// <returns>TransferSearchResult[]</returns>
        /// <remarks>Here only we are calucating Markup</remarks>
        private TransferSearchResult[] ReadGetTransferAvailabilityResponse(XmlDocument xmlDoc, TransferRequest req, decimal markup, string markupType)
        {
            TransferSearchResult[] transferResults = new TransferSearchResult[0];
            try
            {
                //response = response.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchTransferPriceResponse/Errors/Error/ErrorText/text()");
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                XmlNodeList transferList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchTransferPriceResponse/TransferDetails/Transfer");
                transferResults = new TransferSearchResult[transferList.Count];
                int i = 0;
                string[] itemList = new string[transferList.Count];
                foreach (XmlNode transfer in transferList)
                {
                    TransferSearchResult transferResult = new TransferSearchResult();
                    bool hasExtra = false;
                    bool hasIdeas = false;
                    for (int j = 0; j < transfer.Attributes.Count; j++)
                    {
                        string text = transfer.Attributes[j].Name.ToString();
                        if (text != null)
                        {
                            if (!(text == "HasExtraInfo"))
                            {
                                if (text == "HasIdeas")
                                {
                                    if (transfer.Attributes[j].Value.ToString() == "true")
                                    {
                                        hasIdeas = true;
                                    }
                                }
                            }
                            else
                            {
                                if (transfer.Attributes[j].Value.ToString() == "true")
                                {
                                    hasExtra = true;
                                }
                            }
                        }
                    }
                    transferResult.HasExtraInfo = hasExtra;
                    transferResult.HasIdeas = hasIdeas;
                    XmlNode tempNode = transfer.SelectSingleNode("City");
                    if (tempNode != null)
                    {
                        transferResult.CityCode = tempNode.Attributes[0].Value;
                        transferResult.City = tempNode.InnerText.ToString();
                    }
                    else
                    {
                        transferResult.CityCode = "";
                        transferResult.City = "";
                    }
                    tempNode = transfer.SelectSingleNode("Item");
                    if (tempNode != null)
                    {
                        transferResult.ItemCode = tempNode.Attributes[0].Value.Trim();
                        transferResult.ItemName = tempNode.FirstChild.Value;
                    }
                    else
                    {
                        transferResult.ItemCode = "";
                        transferResult.ItemName = "";
                    }
                    tempNode = transfer.SelectSingleNode("PickUpDetails/PickUp");
                    if (tempNode != null)
                    {
                        transferResult.PickUp = default(TransferSearchResult.TransferPickUpDetails);
                        transferResult.PickUp.PickUpCode = tempNode.Attributes["Code"].Value;
                        transferResult.PickUp.PickUpName = tempNode.InnerText.ToString();
                        tempNode = tempNode.NextSibling;
                        transferResult.PickUp.PickUpDetailCode = tempNode.Attributes["Code"].Value;
                        transferResult.PickUp.PickUpDetailName = tempNode.InnerText.ToString();
                    }
                    tempNode = transfer.SelectSingleNode("DropOffDetails/DropOff");
                    if (tempNode != null)
                    {
                        transferResult.DropOff = default(TransferSearchResult.TransferDropOffDetails);
                        transferResult.DropOff.DropOffCode = tempNode.Attributes["Code"].Value;
                        transferResult.DropOff.DropOffName = tempNode.InnerText.ToString();
                        if (tempNode.Attributes["AllowForCheckInTime"] != null)
                        {
                            transferResult.DropOff.DropOffAllowForCheckInTime = float.Parse(tempNode.Attributes["AllowForCheckInTime"].Value);
                        }
                        tempNode = tempNode.NextSibling;
                        transferResult.DropOff.DropOffDetailCode = tempNode.Attributes["Code"].Value;
                        transferResult.DropOff.DropOffDetailName = tempNode.InnerText.ToString();
                        
                    }
                    XmlNodeList oohss = transfer.SelectNodes("OutOfHoursSupplements/OutOfHoursSupplement");
                    transferResult.OutOfHoursSupplements = new TransferSearchResult.TransferOutOfHoursSupplement[oohss.Count];
                    int k = 0;
                    foreach (XmlNode oohs in oohss)
                    {
                        TransferSearchResult.TransferOutOfHoursSupplement toohs = default(TransferSearchResult.TransferOutOfHoursSupplement);
                        tempNode = oohs.SelectSingleNode("FromTime");
                        if (tempNode != null)
                        {
                            toohs.FromTime = float.Parse(tempNode.InnerText.ToString());
                        }
                        tempNode = oohs.SelectSingleNode("ToTime");
                        if (tempNode != null)
                        {
                            toohs.ToTime = float.Parse(tempNode.InnerText.ToString());
                        }
                        tempNode = oohs.SelectSingleNode("Supplement");
                        if (tempNode != null)
                        {
                            toohs.Supplement = tempNode.InnerText.ToString();
                        }
                        transferResult.OutOfHoursSupplements[k++] = toohs;
                    }
                    tempNode = transfer.SelectSingleNode("ApproximateTransferTime");
                    if (tempNode != null)
                    {
                        transferResult.ApproximateTransferTime = decimal.Parse(tempNode.Attributes[0].Value);
                        transferResult.ApproximateTransferTime_Str = tempNode.InnerText.ToString();
                    }
                    else
                    {
                        transferResult.ApproximateTransferTime = 0m;
                        transferResult.ApproximateTransferTime_Str = "";
                    }
                    XmlNodeList tvs = transfer.SelectNodes("TransferVehicles/TransferVehicle");
                    int l = 0;
                    transferResult.Vehicles = new TransferSearchResult.TransferVehicleDetails[tvs.Count];
                    foreach (XmlNode tv in tvs)
                    {
                        TransferSearchResult.TransferVehicleDetails tvd = default(TransferSearchResult.TransferVehicleDetails);
                        tempNode = tv.SelectSingleNode("Vehicle");
                        if (tempNode != null)
                        {
                            tvd.Vehicle = tempNode.InnerText.ToString();
                            tvd.VehicleCode = "";
                            tvd.VehicleMaximumLuggage = 0;
                            tvd.VehicleMaximumPassengers = 0;
                            for (int m = 0; m < tempNode.Attributes.Count; m++)
                            {
                                string text = tempNode.Attributes[m].Name;
                                if (text != null)
                                {
                                    if (!(text == "Code"))
                                    {
                                        if (!(text == "MaximumLuggage"))
                                        {
                                            if (text == "MaximumPassengers")
                                            {
                                                tvd.VehicleMaximumPassengers = int.Parse(tempNode.Attributes[m].Value);
                                            }
                                        }
                                        else
                                        {
                                            tvd.VehicleMaximumLuggage = int.Parse(tempNode.Attributes[m].Value);
                                        }
                                    }
                                    else
                                    {
                                        tvd.VehicleCode = tempNode.Attributes[m].Value;
                                    }
                                }
                            }
                        }
                        tempNode = tv.SelectSingleNode("Language");
                        if (tempNode != null)
                        {
                            tvd.LanguageCode = tempNode.Attributes["Code"].Value;
                            tvd.Language = tempNode.InnerText.ToString();
                        }
                        tempNode = tv.SelectSingleNode("ItemPrice");
                       // tvd.Currency = currency;
                        decimal totPrice = 0m;
                        decimal ItemPrice = 0m;
                        decimal totalB2BMarkup = 0m;
                        if (tempNode != null)
                        {

                            #region price calu
                            currency = tempNode.Attributes["Currency"].Value;
                            rateOfExchange = exchangeRates[currency];
                            ItemPrice = decimal.Parse(tempNode.FirstChild.Value);
                            totPrice = Math.Round((ItemPrice) * rateOfExchange, decimalPoint);
                            tvd.PriceInfo = new PriceAccounts();
                            tvd.PriceInfo.SupplierCurrency = currency;
                            tvd.PriceInfo.SupplierPrice = ItemPrice;
                            tvd.PriceInfo.RateOfExchange = rateOfExchange;
                            tvd.PriceInfo.Currency = agentCurrency;
                            tvd.PriceInfo.CurrencyCode = agentCurrency;
                            totalB2BMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(totPrice) * (markup / 100m)));
                            tvd.PriceInfo.Markup = Math.Round(totalB2BMarkup, decimalPoint);
                            tvd.PriceInfo.MarkupType = markupType;
                            tvd.PriceInfo.MarkupValue = markup;
                            tvd.ItemPrice = Math.Round(totPrice, decimalPoint);
                            tvd.PriceInfo.NetFare = totPrice;
                            #endregion

                            /*Code For Updating Price as per Supplements, Added on <15042016>*/
                            if (transferResult.OutOfHoursSupplements != null && transferResult.OutOfHoursSupplements.Length > 0)
                            {
                                for (int m = 0; m < transferResult.OutOfHoursSupplements.Length; m++)
                                {
                                    if (Convert.ToDecimal(req.PickUpTime) >= Convert.ToDecimal(transferResult.OutOfHoursSupplements[m].FromTime) && Convert.ToDecimal(req.PickUpTime) <= Convert.ToDecimal(transferResult.OutOfHoursSupplements[m].ToTime))
                                    {
                                        try
                                        {
                                            decimal supplPercentage = Convert.ToDecimal(transferResult.OutOfHoursSupplements[m].Supplement.Replace("%", string.Empty));
                                            decimal supplierPrice = tvd.PriceInfo.SupplierPrice;
                                            decimal rateofExchange = tvd.PriceInfo.RateOfExchange;
                                            decimal updateItemPrice = Math.Round((supplierPrice + (supplierPrice * supplPercentage / 100)), decimalPoint);
                                            tvd.PriceInfo.NetFare = Math.Round((supplierPrice + (supplierPrice * supplPercentage / 100)) * rateofExchange, decimalPoint);
                                            tvd.PriceInfo.SupplierPrice = updateItemPrice;

                                            string b2bMarkupType = tvd.PriceInfo.MarkupType;
                                            decimal b2bMarkupValue = tvd.PriceInfo.MarkupValue;
                                            decimal finalB2BMarkup = ((b2bMarkupType == "F") ? b2bMarkupValue : (Convert.ToDecimal(tvd.PriceInfo.NetFare) * (b2bMarkupValue / 100m)));
                                            tvd.PriceInfo.Markup = Math.Round(finalB2BMarkup, decimalPoint);
                                        }
                                        catch
                                        {
                                            throw;
                                        }

                                    }
                                }
                            }

                            /*End*/
                        }
                        tvd.ItemPriceCurrency = agentCurrency;
                        
                        tempNode = tv.SelectSingleNode("Confirmation");
                        if (tempNode != null)
                        {
                            tvd.ConfirmationCode = tempNode.Attributes["Code"].Value;
                            tvd.Confirmation = tempNode.InnerText.ToString();
                        }
                        transferResult.Vehicles[l++] = tvd;
                    }
                    XmlNode transConditions = transfer.SelectSingleNode("TransferConditions");
                    if (transConditions != null)
                    {
                        transferResult.Conditions = transConditions.InnerText;
                    }
                    transferResult.Source = TransferBookingSource.GTA;
                    //Added on 10/06/2016 sorting vehicle price
                    Array.Sort(transferResult.Vehicles, delegate(TransferSearchResult.TransferVehicleDetails tsr1, TransferSearchResult.TransferVehicleDetails tsr2) { return tsr1.PriceInfo.NetFare.CompareTo(tsr2.PriceInfo.NetFare); });
                    
                    PriceAccounts prcAcc = new PriceAccounts();
                    prcAcc.NetFare=transferResult.Vehicles[0].PriceInfo.NetFare;
                    transferResult.price = prcAcc;
                    
                    transferResult.Currency = agentCurrency;
                   
                    transferResults[i++] = transferResult;
                    
                }
            }
            catch (Exception ex)
            {
                throw new BookingEngineException("transfer search error : " + ex.ToString());
            }
            //Added on 10/06/2016 sorting transfer result as per lowest vehicle price
            Array.Sort(transferResults, delegate(TransferSearchResult tsr1, TransferSearchResult tsr2) { return tsr1.price.NetFare.CompareTo(tsr2.price.NetFare); });
            return transferResults;
        }

        /// <summary>
        /// Get the TransferItemInformation for the itemCode Search criteria.
        /// </summary>
        /// <param name="cityCode">CityCode</param>
        /// <param name="itemCode">ItemCode</param>
        /// <returns>TransferStaticData</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public TransferStaticData GetTransferItemInformation(string cityCode, string itemCode)
        {
            TransferStaticData transferInfo = new TransferStaticData();
           
            
            int timeStampDays = Convert.ToInt32(ConfigurationSystem.GTAConfig["TimeStamp"]);
            transferInfo.Load(itemCode, cityCode, TransferBookingSource.GTA);
            bool isUpdateReq = false;
            bool isReqSend = false;
            if (transferInfo.ItemCode != null && transferInfo.ItemCode.Length > 0)
            {
                if (DateTime.UtcNow.Subtract(transferInfo.TimeStamp).Days > timeStampDays)
                {
                    isUpdateReq = true;
                }
            }
            else
            {
                isReqSend = true;
            }
            if (isReqSend || isUpdateReq)
            {
                string request = GenerateTransferItemRequest(cityCode, itemCode);

                try
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(request);
                    string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTATransferItemInformationRequest.xml";
                    XmlDoc.Save(filePath);
                }
                catch { }

                //string resp = string.Empty;
                XmlDocument xmlResp = new XmlDocument();
                try
                {
                    xmlResp = SendRequest(request);

                    try
                    {
                        //XmlDocument XmlDoc = new XmlDocument();
                        //XmlDoc.LoadXml(resp);
                        string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTATransferItemInformationResponse.xml";
                        xmlResp.Save(filePath);
                    }
                    catch { }

                    if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                    {
                        transferInfo = ReadResponseTransferItem(xmlResp);
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
                    {
                        "Exception returned from GTA.GetTransferAvailability Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                    }), "");
                    
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                try
                {
                    if (isUpdateReq)
                    {
                        transferInfo.Update();
                    }
                    else
                    {
                        transferInfo.Save();
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
                    {
                        "Exception returned from GTA.GetTransferAvailability Error Message:",
                        ex.Message,
                        " | ",
                        DateTime.Now,
                        "| request XML",
                        request,
                        "|response XML",
                        xmlResp.OuterXml
                    }), "");
                   
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            return transferInfo;
        }

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="cityCode">CityCode</param>
        /// <param name="itemCode">ItemCode</param>
        /// <returns>string</returns>
        /// <remarks>preparing XML for the request</remarks>
        private string GenerateTransferItemRequest(string cityCode, string itemCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchItemInformationRequest");
            xmlString.WriteAttributeString("ItemType", "transfer");
            xmlString.WriteStartElement("ItemDestination");
            xmlString.WriteAttributeString("DestinationType", "city");
            xmlString.WriteAttributeString("DestinationCode", cityCode);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("ItemCode");
            xmlString.WriteString(itemCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// Used in reading response XML and assigning ResponseTransferItem to the response.
        /// </summary>
        /// <param name="xmlDoc">Response Xml</param>
        /// <returns>TransferStaticData</returns>
        /// <remarks>Here only we are reading ItemInfo</remarks>
        private TransferStaticData ReadResponseTransferItem(XmlDocument xmlDoc)
        {
            TransferStaticData result;
            try
            {
                TransferStaticData staticInfo = new TransferStaticData();
                //response = response.Replace("<xml version=\"1.0\" encoding=\"UTF-8\">\n", "");
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/Errors/Error/ErrorText/text()");
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.TransferSearch, Severity.High, 0, string.Concat(new object[]
                    {
                        " GTA:ReadResponseTransferItem,Error Message:",
                        ErrorInfo.Value,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        xmlDoc.OuterXml
                    }), "");
                    
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                XmlNode tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/TransferInformation/Description/text()");
                if (tempNode != null)
                {
                    staticInfo.Description = tempNode.Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/TransferInformation/MeetingPoint/text()");
                if (tempNode != null)
                {
                    staticInfo.MeetingPoint = tempNode.Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/TransferInformation/TransferType");
                if (tempNode != null)
                {
                    if (tempNode.Attributes[0].Value == "PT")
                    {
                        staticInfo.Type = TransferType.pvtTransfer;
                    }
                    else
                    {
                        staticInfo.Type = TransferType.shared;
                    }
                }
                XmlNodeList condList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/TransferInformation/TransferConditions/TransferCondition");
                string condition = string.Empty;
                foreach (XmlNode cond in condList)
                {
                    tempNode = cond.SelectSingleNode("text()");
                    condition = condition + tempNode.Value + "|";
                }
                staticInfo.Conditions = condition;
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/City");
                if (tempNode != null)
                {
                    staticInfo.CityCode = tempNode.Attributes[0].Value;
                }
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/Item");
                if (tempNode != null)
                {
                    staticInfo.ItemCode = tempNode.Attributes[0].Value;
                }
                staticInfo.Source = TransferBookingSource.GTA;
                tempNode = xmlDoc.SelectSingleNode("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/TransferInformation/ApproximateTransferTime/text()");
                if (tempNode != null)
                {
                    staticInfo.TransferTime = tempNode.Value;
                }
                //Modified on 18062016 by chandan
                XmlNodeList linkList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/TransferInformation/Links/MeetingPointFlashLinks/MeetingPointFlashLink");
                string flashInfo = string.Empty;
                foreach (XmlNode Link in linkList)
                {
                    tempNode = Link.SelectSingleNode("text()");
                    if (!string.IsNullOrEmpty(flashInfo))
                    {

                        flashInfo += "|" + tempNode.Value;
                    }
                    else
                    {
                        flashInfo = tempNode.Value;
                    }
                   
                }

                XmlNodeList linkList1 = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/TransferInformation/Links/MeetingPointMapLinks/MeetingPointMapLink");
               
                foreach (XmlNode Link in linkList)
                {
                    tempNode = Link.SelectSingleNode("text()");
                    if (!string.IsNullOrEmpty(flashInfo))
                    {

                        flashInfo += "|" + tempNode.Value;
                    }
                    else
                    {
                        flashInfo = tempNode.Value;
                    }

                }

                staticInfo.FlashLinks = flashInfo;
                result = staticInfo;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTAItemSearch, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.ReadResponseTransferItem Error Message:",
                    ex.Message,
                    " | ",
                    DateTime.Now,
                    "|response XML",
                    xmlDoc.OuterXml
                }), "");
               
                throw new BookingEngineException("Error: " + ex.Message);
            }
            return result;
        }

        /// <summary>
        ///  Get the TransferChargeCondition for the itemCode Search criteria.
        /// </summary>
        /// <param name="itinearary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <param name="penalityList">penalityList</param>
        /// <param name="savePenality">savePenality</param>
        /// <returns>Dictionary</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public Dictionary<string, string> GetTransferChargeCondition(TransferItinerary itinearary, ref List<TransferPenalty> penalityList, bool savePenality)
        {
            
            string request = GenerateGetTransferChargeConditionRequest(itinearary);

            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(request);
                string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTATransferChargeConditionRequest.xml";
                XmlDoc.Save(filePath);
            }
            catch { }
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(request);
            }
            catch (Exception ex)
            {
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Dictionary<string, string> res;
            try
            {
                res = ReadGetTransferChargeConditionResponse(itinearary, xmlResp, ref penalityList, savePenality);
            }
            catch (Exception ex)
            {
                throw new BookingEngineException("Error: " + ex.Message);
            }
            return res;
        }

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="itinearary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <returns>string</returns>
        /// <remarks>preparing XML for the request</remarks>
        private string GenerateGetTransferChargeConditionRequest(TransferItinerary itinearary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", "AED");
            //xmlString.WriteAttributeString("Country", itinearary.NationalityCode);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchChargeConditionsRequest");
            xmlString.WriteStartElement("ChargeConditionsTransfer");
            xmlString.WriteElementString("City", itinearary.CityCode);
            xmlString.WriteElementString("Item", itinearary.ItemCode);
            xmlString.WriteElementString("TransferDate", itinearary.TransferDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("TransferTime", itinearary.PickUpTime);
            xmlString.WriteElementString("TransferLanguage", "E");
            xmlString.WriteStartElement("Vehicle");
            xmlString.WriteAttributeString("Code", itinearary.TransferDetails[0].VehicleCode);
            xmlString.WriteAttributeString("MaximumPassengers", itinearary.TransferDetails[0].VehicleMaximumPassengers.ToString());
            xmlString.WriteAttributeString("MaximumLuggage", itinearary.TransferDetails[0].VehicleMaximumLuggage.ToString());
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// Used in reading response XML and assigning Dictionary to the response.
        /// </summary>
        /// <param name="itinearary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <param name="xmlDoc">Response xml</param>
        /// <param name="penalityList">penalityList</param>
        /// <param name="savePenality">savePenality</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Here only we are reading CancellationPolicy</remarks>
        private Dictionary<string, string> ReadGetTransferChargeConditionResponse(TransferItinerary itinearary, XmlDocument xmlDoc, ref List<TransferPenalty> penalityList, bool savePenality)
        {
            DateTime startDate = itinearary.TransferDate;
            Dictionary<string, string> polList = new Dictionary<string, string>();
            try
            {
                //response = response.Replace("<xml version=\"1.0\" encoding=\"UTF-8\">\n", "");
                ////TransferSource sourceInfo = new TransferSource();
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
                try
                {
                    string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GTATransferChargeConditionResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/Errors/Error/ErrorText/text()");
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }

                List<TransferPenalty> penList = new List<TransferPenalty>();
                XmlNodeList chargeList = xmlDoc.SelectNodes("/Response/ResponseDetails/SearchChargeConditionsResponse/ChargeConditions/ChargeCondition");
                string message = string.Empty;
                string lastCancelllation = string.Empty;
                string amendmentMessage = "Amendment Not Allowed";
                lastCancelllation = "0";
                foreach (XmlNode charge in chargeList)
                {
                    if (charge.Attributes[0].Value == "cancellation")
                    {
                        XmlNodeList individualList = charge.SelectNodes("Condition");
                        int buffer = Convert.ToInt32(ConfigurationSystem.GTAConfig["buffer"]);
                        foreach (XmlNode chargeInfo in individualList)
                        {
                            bool isCharge = false;
                            int fromDay = 0;
                            int toDay = 0;
                            decimal chargeAmt = 0m;
                            string curr = string.Empty;
                            for (int i = 0; i < chargeInfo.Attributes.Count; i++)
                            {
                                string text = chargeInfo.Attributes[i].Name.ToString();
                                if (text != null)
                                {
                                    if (!(text == "Charge"))
                                    {
                                        if (!(text == "FromDay"))
                                        {
                                            if (!(text == "ToDay"))
                                            {
                                                if (!(text == "Currency"))
                                                {
                                                    if (text == "ChargeAmount")
                                                    {
                                                        chargeAmt = Convert.ToDecimal(chargeInfo.Attributes[i].Value);
                                                    }
                                                }
                                                else
                                                {
                                                    curr = chargeInfo.Attributes[i].Value;
                                                }
                                            }
                                            else
                                            {
                                                toDay = (int)Convert.ToInt16(chargeInfo.Attributes[i].Value);
                                            }
                                        }
                                        else
                                        {
                                            fromDay = (int)Convert.ToInt16(chargeInfo.Attributes[i].Value);
                                        }
                                    }
                                    else
                                    {
                                        isCharge = Convert.ToBoolean(chargeInfo.Attributes[i].Value);
                                    }
                                }
                            }
                            if (isCharge)
                            {
                                rateOfExchange = exchangeRates[curr];
                                chargeAmt = Convert.ToDecimal((chargeAmt * rateOfExchange).ToString("N" + decimalPoint));
                                chargeAmt = chargeAmt * itinearary.TransferDetails.Count;
                                chargeAmt += chargeAmt * Convert.ToDecimal(ConfigurationSystem.GTAConfig["CancellationMarkup"]) / 100m;
                                string symbol = agentCurrency;
                                if (fromDay == toDay)
                                {
                                    object obj = message;
                                    message = string.Concat(new object[]
								{
									obj,
									"Non-refundable and full charge will apply once booking is completed."
								});
                                }
                                if (fromDay != toDay && fromDay == 0 && toDay > 0)
                                {
                                    int extraDays = 0;
                                    object obj = message;
                                    message = string.Concat(new object[]
								{
									obj,itinearary.TransferDate.AddDays((double)(-(double)(toDay + buffer + extraDays))).ToString("dd/MMM/yyyy")," onwards You will be charged ",
                                    symbol,
                                    " ",
                                    chargeAmt
								});
                                }
                                else
                                {
                                    if (fromDay != toDay)
                                    {
                                        int extraDays = 0;
                                        object obj = message;
                                        message = string.Concat(new object[]
									{
                                        obj,itinearary.TransferDate.AddDays((double)(-(double)(fromDay + buffer + extraDays))).ToString("dd/MMM/yyyy")," earlier  You will be charged ",
                                    symbol,
                                    " ",
                                    chargeAmt
									});
                                    }
                                }
                                message += "|";
                            }
                        }
                    }
                    else
                    {
                        if (charge.Attributes[0].Value == "amendment")
                        {
                            amendmentMessage = string.Empty;
                            XmlNodeList individualList = charge.SelectNodes("Condition");
                            int buffer = Convert.ToInt32(ConfigurationSystem.GTAConfig["amendmentBuffer"]);
                            foreach (XmlNode chargeInfo in individualList)
                            {
                                TransferPenalty penalityInfo = new TransferPenalty();
                                penalityInfo.PolicyType = TransferChangePoicyType.Amendment;
                                bool isCharge = false;
                                bool isAllowed = true;
                                int fromDay = 0;
                                int toDay = -1;
                                decimal chargeAmt = 0m;
                                string curr = string.Empty;
                                for (int i = 0; i < chargeInfo.Attributes.Count; i++)
                                {
                                    string text = chargeInfo.Attributes[i].Name.ToString();
                                    if (text != null)
                                    {
                                        if (!(text == "Charge"))
                                        {
                                            if (!(text == "FromDay"))
                                            {
                                                if (!(text == "ToDay"))
                                                {
                                                    if (!(text == "Currency"))
                                                    {
                                                        if (!(text == "ChargeAmount"))
                                                        {
                                                            if (text == "Allowable")
                                                            {
                                                                isAllowed = Convert.ToBoolean(chargeInfo.Attributes[i].Value);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            chargeAmt = Convert.ToDecimal(chargeInfo.Attributes[i].Value);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        curr = chargeInfo.Attributes[i].Value;
                                                    }
                                                }
                                                else
                                                {
                                                    toDay = (int)Convert.ToInt16(chargeInfo.Attributes[i].Value);
                                                }
                                            }
                                            else
                                            {
                                                fromDay = (int)Convert.ToInt16(chargeInfo.Attributes[i].Value);
                                            }
                                        }
                                        else
                                        {
                                            isCharge = Convert.ToBoolean(chargeInfo.Attributes[i].Value);
                                        }
                                    }
                                }
                                if (isAllowed && isCharge)
                                {
                                    rateOfExchange = exchangeRates[curr];
                                    chargeAmt = Math.Ceiling(chargeAmt * rateOfExchange);
                                    chargeAmt = chargeAmt * itinearary.TransferDetails.Count;
                                    chargeAmt += chargeAmt  * Convert.ToDecimal(ConfigurationSystem.GTAConfig["CancellationMarkup"]) / 100m;
                                    chargeAmt = Math.Round(chargeAmt, Convert.ToInt32(ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                                    string symbol = agentCurrency;
                                    if (fromDay == toDay)
                                    {
                                        int extraDays = 0;
                                        if (startDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                        {
                                            extraDays = 1;
                                        }
                                        else
                                        {
                                            if (startDate.AddDays((double)(-(double)(fromDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                            {
                                                extraDays = 2;
                                            }
                                        }
                                        penalityInfo.FromDate = startDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                        penalityInfo.ToDate = startDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                        object obj = amendmentMessage;
                                        amendmentMessage = string.Concat(new object[]
                                        {
                                            obj,
                                            "Amendment upto ",
                                            fromDay + buffer + extraDays,
                                            " day(s) before the date of arrival, ",
                                            symbol,
                                            " ",
                                            chargeAmt,
                                            " will be charged."
                                        });
                                        penalityInfo.Remarks = string.Concat(new object[]
                                        {
                                            "Amendment upto ",
                                            fromDay + buffer + extraDays,
                                            " day(s) before the date of arrival, ",
                                            symbol,
                                            " ",
                                            chargeAmt,
                                            " will be charged."
                                        });
                                    }
                                    if (fromDay != toDay && fromDay == 0 && toDay > 0)
                                    {
                                        int extraDays = 0;
                                        if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                        {
                                            extraDays = 1;
                                        }
                                        else
                                        {
                                            if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                            {
                                                extraDays = 2;
                                            }
                                        }
                                        penalityInfo.FromDate = startDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                        penalityInfo.ToDate = startDate.AddDays((double)(-(double)(toDay + buffer + extraDays)));
                                        object obj = amendmentMessage;
                                        amendmentMessage = string.Concat(new object[]
                                        {
                                            obj,
                                            "Amendment upto ",
                                            toDay + buffer + extraDays,
                                            " day(s) before the date of arrival, ",
                                            symbol,
                                            " ",
                                            chargeAmt,
                                            " will be charged."
                                        });
                                        penalityInfo.Remarks = string.Concat(new object[]
                                        {
                                            "Amendment upto ",
                                            toDay + buffer + extraDays,
                                            " day(s) before the date of arrival, ",
                                            symbol,
                                            " ",
                                            chargeAmt,
                                            " will be charged."
                                        });
                                    }
                                    else
                                    {
                                        if (fromDay != toDay)
                                        {
                                            int extraDays = 0;
                                            if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Saturday)
                                            {
                                                extraDays = 1;
                                            }
                                            else
                                            {
                                                if (startDate.AddDays((double)(-(double)(toDay + buffer))).DayOfWeek == DayOfWeek.Sunday)
                                                {
                                                    extraDays = 2;
                                                }
                                            }
                                            if (toDay == -1)
                                            {
                                                penalityInfo.FromDate = DateTime.Now;
                                                penalityInfo.ToDate = startDate;
                                            }
                                            else
                                            {
                                                penalityInfo.FromDate = startDate.AddDays((double)(-(double)(fromDay + buffer + extraDays)));
                                                penalityInfo.ToDate = startDate.AddDays((double)(-(double)(toDay + buffer + extraDays)));
                                            }
                                            object obj = amendmentMessage;
                                            amendmentMessage = string.Concat(new object[]
                                            {
                                                obj,
                                                "Amendment from ",
                                                fromDay + buffer + extraDays,
                                                " days to ",
                                                toDay + buffer,
                                                " days before the date of arrival, ",
                                                symbol,
                                                " ",
                                                chargeAmt,
                                                " will be charged."
                                            });
                                            penalityInfo.Remarks = string.Concat(new object[]
                                            {
                                                "Amendment from ",
                                                fromDay + buffer + extraDays,
                                                " days to ",
                                                toDay + buffer,
                                                " days before the date of arrival, ",
                                                symbol,
                                                " ",
                                                chargeAmt,
                                                " will be charged."
                                            });
                                        }
                                    }
                                    penalityInfo.Price = chargeAmt;
                                    penalityInfo.IsAllowed = isAllowed;
                                    penList.Add(penalityInfo);
                                    amendmentMessage += "|";
                                }
                                else
                                {
                                    penalityInfo.IsAllowed = isAllowed;
                                    penalityInfo.FromDate = DateTime.Now;
                                    penalityInfo.ToDate = startDate;
                                    penalityInfo.Price = 0m;
                                    if (isAllowed)
                                    {
                                        penalityInfo.Remarks = "No Amendment Charges before " + (fromDay + buffer) + " days of Transfer Date.";
                                    }
                                    else
                                    {
                                        penalityInfo.Remarks = "Amendment is Not Allowed.";
                                    }
                                    amendmentMessage = penalityInfo.Remarks + "|";
                                    penList.Add(penalityInfo);
                                }
                            }
                        }
                    }
                }
                
                polList.Add("CancelPolicy", message);
                polList.Add("AmendmentPolicy", amendmentMessage);
               
            }
            catch (Exception ex)
            {
                throw new BookingEngineException("GTA.ReadGetTransferChargeConditionResponse error : " + ex.ToString());
            }
           
            return polList;
        }

        /// <summary>
        ///  Get the booking based on the itemCode.
        /// </summary>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public BookingResponse GetTransferBooking(ref TransferItinerary itineary)
        {

            TransferStaticData staticInfo = new TransferStaticData();
            staticInfo = GetTransferItemInformation(itineary.CityCode, itineary.ItemCode);
            string bookingReference = "BookTRef" + TraceIdGeneration();
            string requestXml = GenerateGetTransferBookingRequest(itineary, bookingReference);

            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(requestXml);
                string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss_fff") + "_GTATransferBookingRequest_1.xml";
                XmlDoc.Save(filePath);
            }
            catch { }

            //string resp = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            BookingResponse bookingRes = default(BookingResponse);
            try
            {
                xmlDoc = SendRequest(requestXml);
                //XmlDocument xmlDoc = new XmlDocument();
                try
                {

                    //xmlDoc.LoadXml(xmlResp);
                    string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss_fff") + "_GTATransferBookingResponse_1.xml";
                    xmlDoc.Save(filePath);

                }
                catch
                {

                }
                if (xmlDoc != null && xmlDoc.ChildNodes != null && xmlDoc.ChildNodes.Count > 0)
                {
                    XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
                    if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                    {
                        Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                    {
                        " GTA:ReadGetTransferBookingResponse,Error Message:",
                        ErrorInfo.Value,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        xmlDoc.OuterXml
                    }), "");

                        bookingRes.Status = BookingResponseStatus.Failed;
                        throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                    }



                    //implemented by brahmam Push mechanisam
                    if (itineary.TransferDetails.Count > 1)
                    {
                        int count = 1;
                        int itemReference = ReadBookingResponse(xmlDoc);
                        for (int i = 0; i < itineary.TransferDetails.Count; i++)
                        {
                            if (itemReference != (i + 1))
                            {
                                string bookReqXml = GenerateGetTransferBookingPushMechanismRequest((i + 1), bookingReference, itineary.ConfirmationNo, count);
                                try
                                {
                                    xmlDoc = SendRequest(bookReqXml);
                                    try
                                    {

                                        //xmlDoc.LoadXml(resp);
                                        string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss_fff") + "_GTATransferBookingResponse_" + (count + 1) + ".xml";
                                        xmlDoc.Save(filePath);
                                        count++;
                                    }
                                    catch { }
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.TransferBooking, Severity.High, 0, "Exception returned from GTA.Push MechanismGetTransferBooking Error Message:" + ex.Message, "");
                                }

                                if (xmlDoc != null && xmlDoc.ChildNodes != null && xmlDoc.ChildNodes.Count > 0)
                                {
                                    ErrorInfo = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
                                    if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                                    {
                                        Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                                      {" GTA:ReadGetTransferBookingResponse,Error Message:",ErrorInfo.Value," | ",DateTime.Now,"| Response XML",xmlDoc.OuterXml}), "");

                                        bookingRes.Status = BookingResponseStatus.Failed;
                                        throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetTransferBooking Error Message:",
                    ex.Message,
                    " | Request XML:",
                    requestXml,
                    "| Response XML:",
                    xmlDoc.OuterXml,
                    DateTime.Now
                }), "");

                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
            {
                "Transfer Booking Response from GTA. Response:",
                xmlDoc.OuterXml,
                " | Request XML:",
                requestXml,
                DateTime.Now
            }), "");
            try
            {
                if (xmlDoc != null && xmlDoc.ChildNodes != null && xmlDoc.ChildNodes.Count > 0)
                {
                    bookingRes = ReadGetTransferBookingResponse(xmlDoc, ref itineary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.GetTransferBooking Error Message:",
                    ex.Message,
                    " | Request XML:",
                    requestXml,
                    "| Response XML :",
                    xmlDoc.OuterXml,
                    DateTime.Now
                }), "");

                throw new BookingEngineException("Error: " + ex.Message);
            }
        
            
            return bookingRes;
        }

        //push mechanism purpose
        /// <summary>
        /// Used in reading response XML and assigning Dictionary to the response.
        /// </summary>
        /// <param name="xmlDoc">response xml</param>
        /// <returns>int</returns>
        /// <remarks>push mechanism purpose(first booking </remarks>
        private int ReadBookingResponse(XmlDocument xmlDoc)
        {
            //response = response.Replace("<xml version=\"1.0\" encoding=\"UTF-8\">\n", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Response/ResponseDetails/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                    {
                        " GTA:ReadGetTransferBookingResponse,Error Message:",
                        ErrorInfo.Value,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        xmlDoc.OuterXml
                    }), "");
                
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            XmlNode nodeHead = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse");
            int itemref = 0;
            if (nodeHead != null)
            {
                itemref = Convert.ToInt32(nodeHead.SelectSingleNode("BookingItems/BookingItem/ItemReference").InnerText);
            }
            return itemref;
        }

        //added by brahmam push mechanisam
        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="itemRefValue">itemRefValue</param>
        /// <param name="bookingReference">bookingReference</param>
        /// <param name="nationalityCode">nationalityCode</param>
        /// <param name="count">count</param>
        /// <returns>string</returns>
        private string GenerateGetTransferBookingPushMechanismRequest(int itemRefValue, string bookingReference, string nationalityCode, int count)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", "AED");
            xmlString.WriteAttributeString("Country", nationalityCode);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchBookingItemRequest");
            xmlString.WriteStartElement("BookingReference"); //BookingReference
            xmlString.WriteAttributeString("ReferenceSource", "client");
            xmlString.WriteValue(bookingReference);
            xmlString.WriteEndElement();//End BookingReference
            xmlString.WriteElementString("ItemReference", itemRefValue.ToString());
            xmlString.WriteElementString("ShowPaymentStatus", "false");
            xmlString.WriteEndElement();//end SearchBookingItemRequest
            xmlString.WriteEndElement();//end RequestDetails
            xmlString.WriteEndElement();//Request
            xmlString.Close();
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(strWriter.ToString());
                string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss_fff") + "_GTATransferBookingRequest_"+(count+1)+".xml";
                XmlDoc.Save(filePath);
            }
            catch { }
            return strWriter.ToString();

        }
        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <param name="bookingReference">bookingReference</param>
        /// <returns>string</returns>
        private string GenerateGetTransferBookingRequest(TransferItinerary itineary,string bookingReference)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", "AED");
            xmlString.WriteAttributeString("Country", itineary.ConfirmationNo); //added on 14042016
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("AddBookingRequest");
            xmlString.WriteAttributeString("Currency", "AED");
            xmlString.WriteElementString("BookingName", "BOOKT" + TraceIdGeneration());
            xmlString.WriteElementString("BookingReference", bookingReference);
            xmlString.WriteElementString("AgentReference",TraceIdGeneration());
            xmlString.WriteElementString("BookingDepartureDate", itineary.TransferDate.ToString("yyyy-MM-dd"));
            xmlString.WriteStartElement("PaxNames");
            xmlString.WriteStartElement("PaxName");
            xmlString.WriteAttributeString("PaxId", "1");
            xmlString.WriteValue(itineary.PassengerInfo.Split('|')[0]);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("BookingItems");
            for (int i = 1; i <= itineary.TransferDetails.Count; i++)
            {
                xmlString.WriteStartElement("BookingItem");
                xmlString.WriteAttributeString("ItemType", "transfer");
                xmlString.WriteElementString("ItemReference", i.ToString());
                xmlString.WriteStartElement("ItemCity");
                xmlString.WriteAttributeString("Code", itineary.CityCode);
                xmlString.WriteEndElement();
                xmlString.WriteStartElement("Item");
                xmlString.WriteAttributeString("Code", itineary.ItemCode);
                xmlString.WriteEndElement();
                //Geeting here pickup and drop off details
                GenerateTransferItemXML(ref xmlString, itineary, i - 1);
                //End
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="xmlString">xmlString</param>
        /// <param name="itinerary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <param name="i">ReferenceNo</param>
        private void GenerateTransferItemXML(ref XmlWriter xmlString, TransferItinerary itinerary, int i)
        {
            xmlString.WriteStartElement("TransferItem");
            xmlString.WriteStartElement("PickUpDetails");
            string[] adresInfo;
            switch (itinerary.PickUpType)
            {
                case PickDropType.ACCOMODATION:
                    xmlString.WriteStartElement("PickUpAccommodation");
                    xmlString.WriteStartElement("PickUpFrom");

                    //Adding here Hotel Code
                    xmlString.WriteStartElement("Hotel");
                    xmlString.WriteAttributeString("Code", itinerary.PickUpDescription.Split('|')[0].Trim());
                    xmlString.WriteEndElement();

                    xmlString.WriteEndElement();
                    xmlString.WriteElementString("PickUpTime", itinerary.PickUpTime);
                    xmlString.WriteEndElement();
                    goto IL_45B;
                case PickDropType.AIRPORT:
                    xmlString.WriteStartElement("PickUpAirport");
                    xmlString.WriteStartElement("ArrivingFrom");
                    xmlString.WriteStartElement("Airport");
                    xmlString.WriteAttributeString("Code", itinerary.PickUpCode);
                    xmlString.WriteEndElement();
                    xmlString.WriteEndElement();
                    xmlString.WriteElementString("FlightNumber", itinerary.PickUpDescription);
                    xmlString.WriteElementString("EstimatedArrival", itinerary.PickUpTime);
                    xmlString.WriteEndElement();
                    goto IL_45B;
                case PickDropType.STATION:
                    xmlString.WriteStartElement("PickUpStation");
                    xmlString.WriteStartElement("Station");
                    xmlString.WriteAttributeString("Code", itinerary.PickUpCode);
                    xmlString.WriteEndElement();
                    xmlString.WriteStartElement("ArrivingFrom");
                    xmlString.WriteStartElement("City");
                    xmlString.WriteAttributeString("Code", itinerary.CityCode);
                    xmlString.WriteEndElement();
                    xmlString.WriteEndElement();
                    xmlString.WriteElementString("TrainName", itinerary.PickUpDescription);
                    xmlString.WriteElementString("EstimatedArrival", itinerary.PickUpTime);
                    xmlString.WriteEndElement();
                    goto IL_45B;
                case PickDropType.PORT:
                    xmlString.WriteStartElement("PickUpPort");
                    xmlString.WriteElementString("ArrivingFrom", itinerary.PickUpCode);
                    xmlString.WriteElementString("ShipName", itinerary.PickUpDescription);
                    xmlString.WriteElementString("ShippingCompany", itinerary.PickUpRemarks);
                    xmlString.WriteElementString("EstimatedArrival", itinerary.PickUpTime);
                    xmlString.WriteEndElement();
                    goto IL_45B;
            }
            xmlString.WriteStartElement("PickUpOther");
            xmlString.WriteStartElement("PickUpAddress");
            adresInfo = itinerary.PickUpDescription.Split(new char[]
            {
                '|'
            });
            xmlString.WriteElementString("AddressLine1", adresInfo[0]);
            if (adresInfo.Length >= 2 && adresInfo[1].Length > 0)
            {
                xmlString.WriteElementString("AddressLine2", adresInfo[1]);
            }
            if (adresInfo.Length >= 3 && adresInfo[2].Length > 0)
            {
                xmlString.WriteElementString("City", adresInfo[2]);
            }
            //if (adresInfo.Length >= 4 && adresInfo[3].Length > 0)
            //{
            //    xmlString.WriteElementString("Country", adresInfo[3]);
            //}
            if (adresInfo.Length >= 5)
            {
                xmlString.WriteElementString("PostCode", adresInfo[4]);
            }
            if (adresInfo.Length >= 6 && adresInfo[5].Length > 0)
            {
                xmlString.WriteElementString("Telephone", adresInfo[5]);
            }
            xmlString.WriteEndElement();
            xmlString.WriteElementString("PickUpTime", itinerary.PickUpTime);
            xmlString.WriteEndElement();
        IL_45B:
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("DropOffDetails");
            switch (itinerary.DropOffType)
            {
                case PickDropType.ACCOMODATION:
                    xmlString.WriteStartElement("DropOffAccommodation");
                    xmlString.WriteStartElement("DropOffTo");

                    xmlString.WriteStartElement("Hotel");
                    xmlString.WriteAttributeString("Code", itinerary.DropOffDescription.Split('|')[0].Trim());
                    xmlString.WriteEndElement();


                    xmlString.WriteEndElement();
                    xmlString.WriteEndElement();
                    goto IL_88A;
                case PickDropType.AIRPORT:
                    xmlString.WriteStartElement("DropOffAirport");
                    xmlString.WriteStartElement("DepartingTo");
                    xmlString.WriteStartElement("Airport");
                    xmlString.WriteAttributeString("Code", itinerary.DropOffCode);
                    xmlString.WriteEndElement();
                    xmlString.WriteEndElement();
                    xmlString.WriteElementString("FlightNumber", itinerary.DropOffDescription);
                    xmlString.WriteElementString("DepartureTime", itinerary.DropOffTime);
                    xmlString.WriteEndElement();
                    goto IL_88A;
                case PickDropType.STATION:
                    xmlString.WriteStartElement("DropOffStation");
                    xmlString.WriteStartElement("Station");
                    xmlString.WriteAttributeString("Code", itinerary.DropOffCode);
                    xmlString.WriteEndElement();
                    xmlString.WriteStartElement("DepartingTo");
                    xmlString.WriteStartElement("City");
                    xmlString.WriteAttributeString("Code", itinerary.CityCode);
                    xmlString.WriteEndElement();
                    xmlString.WriteEndElement();
                    xmlString.WriteElementString("TrainName", itinerary.DropOffDescription);
                    xmlString.WriteElementString("DepartureTime", itinerary.DropOffTime);
                    xmlString.WriteEndElement();
                    goto IL_88A;
                case PickDropType.PORT:
                    xmlString.WriteStartElement("DropOffPort");
                    xmlString.WriteElementString("DepartingTo", itinerary.DropOffCode);
                    xmlString.WriteElementString("ShipName", itinerary.DropOffDescription);
                    xmlString.WriteElementString("ShippingCompany", itinerary.DropOffRemarks);
                    xmlString.WriteElementString("DepartureTime", itinerary.DropOffTime);
                    xmlString.WriteEndElement();
                    goto IL_88A;
            }
            xmlString.WriteStartElement("DropOffOther");
            xmlString.WriteStartElement("DropOffAddress");
            adresInfo = itinerary.DropOffDescription.Split(new char[]
            {
                '|'
            });
            xmlString.WriteElementString("AddressLine1", adresInfo[0]);
            if (adresInfo.Length >= 2 && adresInfo[1].Length > 0)
            {
                xmlString.WriteElementString("AddressLine2", adresInfo[1]);
            }
            if (adresInfo.Length >= 3 && adresInfo[2].Length > 0)
            {
                xmlString.WriteElementString("City", adresInfo[2]);
            }
            //if (adresInfo.Length >= 4 && adresInfo[3].Length > 0)
            //{
            //    xmlString.WriteElementString("Country", adresInfo[3]);
            //}
            if (adresInfo.Length >= 5)
            {
                xmlString.WriteElementString("PostCode", adresInfo[4]);
            }
            if (adresInfo.Length >= 6 && adresInfo[5].Length > 0)
            {
                xmlString.WriteElementString("Telephone", adresInfo[5]);
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
        IL_88A:
            xmlString.WriteEndElement();
            xmlString.WriteElementString("TransferDate", itinerary.TransferDate.ToString("yyyy-MM-dd"));
            string langCode = itinerary.Language;
            xmlString.WriteElementString("TransferLanguage", langCode.Substring(0, 1));
            xmlString.WriteStartElement("TransferVehicle");
            TransferVehicle vehInfo = new TransferVehicle();
            vehInfo = itinerary.TransferDetails[i];
            xmlString.WriteStartElement("Vehicle");
            if (vehInfo.VehicleCode != "")
            {
                xmlString.WriteAttributeString("Code", vehInfo.VehicleCode);
            }
            if (vehInfo.VehicleMaximumPassengers > 0)
            {
                xmlString.WriteAttributeString("MaximumPassengers", vehInfo.VehicleMaximumPassengers.ToString());
            }
            xmlString.WriteEndElement();
            xmlString.WriteElementString("Passengers", vehInfo.OccupiedPax.ToString());
            xmlString.WriteElementString("LeadPaxId", "1");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
        }

        /// <summary>
        /// Used in reading response XML and assigning BookingResponse to the response
        /// </summary>
        /// <param name="xmlDoc">Response xml Doc</param>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <returns>BookingResponse</returns>
        private BookingResponse ReadGetTransferBookingResponse(XmlDocument xmlDoc, ref TransferItinerary itineary)
        {
            
            BookingResponse bookingRes = default(BookingResponse);
            try
            {
                //response = response.Replace("<xml version=\"1.0\" encoding=\"UTF-8\">\n", "");
                //TextReader stringRead = new StringReader(response);
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(stringRead);
              //  XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Response/ResponseDetails/Errors/Error/ErrorText/text()");
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
                if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                    {
                        " GTA:ReadGetTransferBookingResponse,Error Message:",
                        ErrorInfo.Value,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        xmlDoc.OuterXml
                    }), "");
                    
                    bookingRes.Status = BookingResponseStatus.Failed;
                    throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
                }
                XmlNode nodeHead = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse");
                if (nodeHead == null)
                {
                    nodeHead = xmlDoc.SelectSingleNode("Response/ResponseDetails/SearchBookingItemResponse");
                }
                XmlNodeList tempNodeList = nodeHead.SelectNodes("BookingReferences/BookingReference");
                bookingRes.ProdType = ProductType.Transfers;
                foreach (XmlNode nodeRef in tempNodeList)
                {
                    string text = nodeRef.Attributes["ReferenceSource"].Value;
                    if (text != null)
                    {
                        if (text == "api")
                        {
                            itineary.BookingReference = nodeRef.FirstChild.Value;
                        }
                    }
                }
                XmlNode tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/ItemConfirmationReference/text()");
                itineary.ConfirmationNo = "";
                if (tempNode != null)
                {
                    itineary.ConfirmationNo = tempNode.Value;
                }
                tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/TransferItem/SupplierTelephoneNumber/text()");
                if (tempNode != null)
                {
                    itineary.TransferConditions = itineary.TransferConditions + "|" + tempNode.Value;
                }
                else
                {
                    itineary.TransferConditions = itineary.TransferConditions + "|";
                }
                tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/ItemStatus");
                if (tempNode != null)
                {
                    string text = tempNode.Attributes["Code"].Value.Trim();
                    if (text != null)
                    {
                        if (text == "CP")
                        {
                            itineary.BookingStatus = TransferBookingStatus.Error;
                            bookingRes.Status = BookingResponseStatus.Failed;
                            bookingRes.Error = tempNode.InnerText.ToString();
                           
                            goto IL_2AD;
                        }
                        if (text == "C")
                        {
                            itineary.BookingStatus = TransferBookingStatus.Confirmed;
                            bookingRes.Status = BookingResponseStatus.Successful;
                            goto IL_2AD;
                        }
                        if (text == "X")
                        {
                            itineary.BookingStatus = TransferBookingStatus.Error;
                            bookingRes.Status = BookingResponseStatus.Failed;
                            bookingRes.Error = tempNode.InnerText.ToString();
                            goto IL_2AD;
                        }
                    }
                    itineary.BookingStatus = TransferBookingStatus.Error;
                    bookingRes.Status = BookingResponseStatus.Failed;
                IL_2AD: ;
                }
            }
            catch (Exception ex)
            {
                throw new BookingEngineException("ReadGetTransferBookingResponse error : " + ex.ToString());
            }
            
            return bookingRes;
        }

        /// <summary>
        /// Get the cancel based on the ConfirmationNo
        /// </summary>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <returns>Dictionary</returns>
        public Dictionary<string, string> CancelTransferBooking(TransferItinerary itineary)
        {
            
           
            XmlDocument xmlDoc = new XmlDocument();
            
            string requestXml = GenerateCancelTransferBookingRequest(itineary);
            try
            {
               
                xmlDoc.LoadXml(requestXml);
                string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss_fff") + "_GTATransferCancelRequest_1.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            //string resp;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendRequest(requestXml);
                try
                {
                   
                    //xmlDoc.LoadXml(resp);
                    string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss_fff") + "_GTATransferCancelResponse_1.xml";
                    xmlDoc.Save(filePath);
                }


                catch { }
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    XmlNode ErrorInfo = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
                    if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
                    {
                        Audit.Add(EventType.TransferCancel, Severity.High, 0, string.Concat(new object[]
                        {
                        " GTA:ReadGetTransferCancelBookingResponse,Error Message:",
                        ErrorInfo.Value,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        xmlResp.OuterXml
                        }), "");


                        throw new Exception("<br>" + ErrorInfo.InnerText);
                    }


                    //added on 19032016 For Cancel Multiple Transfer items by use of push mechanism
                    if (itineary.TransferDetails.Count > 1)
                    {
                        int count = 1;
                        int itemReference = ReadBookingResponse(xmlResp);
                        for (int i = 0; i < itineary.TransferDetails.Count; i++)
                        {
                            if (itemReference != (i + 1))
                            {
                                string bookReqXml = GenerateGetTransferCancellingPushMechanismRequest((i + 1), itineary.BookingReference, itineary.ConfirmationNo, count);
                                try
                                {
                                    xmlResp = SendRequest(bookReqXml);
                                    try
                                    {

                                        //xmlDoc.LoadXml(resp);
                                        string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss_fff") + "_GTATransferCancellingResponse_" + (count + 1) + ".xml";
                                        xmlDoc.Save(filePath);
                                        count++;
                                    }
                                    catch { }
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.TransferBooking, Severity.High, 0, "Exception returned from GTA.Push MechanismGetTransferCancellation Error Message:" + ex.Message, "");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTACancel, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.CancelTransferBooking Error Message:",
                    ex.Message,
                    " | Request XML:",
                    requestXml,
                    DateTime.Now
                }), "");
               
                throw new BookingEngineException("Error: " + ex.Message);
            }
            Audit.Add(EventType.GTACancel, Severity.High, 0, string.Concat(new object[]
            {
                "Booking Response from GTA.CancelTransferBooking Response:",
                xmlResp.OuterXml,
                " | Request XML:",
                requestXml,
                DateTime.Now
            }), "");
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    cancelInfo = ReadCancelTransferBookingResponse(xmlResp, itineary.TransferDetails.Count);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GTACancel, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA.CancelTransferBooking Error Message:",
                    ex.Message,
                    " | Request XML:",
                    requestXml,
                    "| Response XML :",
                    xmlResp.OuterXml,
                    DateTime.Now
                }), "");
                
                throw new BookingEngineException("Error: " + ex.Message);
            }
           
            return cancelInfo;
        }

        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="itineary">Itinerary object(what we are able to book corresponding Vehical and details)</param>
        /// <returns>string</returns>
        private string GenerateCancelTransferBookingRequest(TransferItinerary itineary)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", "AED");
            xmlString.WriteAttributeString("Country", itineary.ConfirmationNo);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("CancelBookingRequest");
            xmlString.WriteStartElement("BookingReference");
            xmlString.WriteAttributeString("ReferenceSource", "api");
            xmlString.WriteValue(itineary.BookingReference);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// Used in reading response XML and assigning cancel to the response
        /// </summary>
        /// <param name="xmlDoc">response xml</param>
        /// <param name="count">count</param>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> ReadCancelTransferBookingResponse(XmlDocument xmlDoc, int count)
        {
           
            //response = response.Replace("<xml version=\"1.0\" encoding=\"UTF-8\">", "");
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/Response/ResponseDetails/BookingResponse/Errors/Error/ErrorText/text()");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.GTACancel, Severity.High, 0, string.Concat(new object[]
				{
					" Error Message:",
					ErrorInfo.Value,
					" | ",
					DateTime.Now,
					"| Response XML",
                    xmlDoc.OuterXml
                }), "");
               
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            XmlNode nodeHead = xmlDoc.SelectSingleNode("Response/ResponseDetails/BookingResponse");
            if (nodeHead == null)
            {
                nodeHead = xmlDoc.SelectSingleNode("Response/ResponseDetails/SearchBookingItemResponse");
            }

            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            XmlNode tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/ItemStatus/text()");
            if (tempNode != null)
            {
                cancelInfo.Add("Status", tempNode.Value);
            }
            tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/ItemConfirmationReference");
            if (tempNode != null)
            {
                cancelInfo.Add("ID", tempNode.InnerText);
            }
            //added on 01042016 
            tempNode = nodeHead.SelectSingleNode("BookingItems/BookingItem/ItemFee");
            decimal cancelAmount = 0;

            if (tempNode != null)
            {
                cancelInfo.Add("Currency", tempNode.Attributes[0].Value);
                cancelAmount = Convert.ToDecimal(tempNode.InnerText);
                cancelInfo.Add("Amount", (cancelAmount * count).ToString());//count for multiple vehicles
            }
            else
            {
                cancelInfo.Add("Currency", "AED");
                cancelInfo.Add("Amount", "0");
            }
           
            return cancelInfo;
        }

        /// <summary>
        /// GenerateStationRequest
        /// </summary>
        /// <param name="cityCode">cityCode</param>
        /// <returns>string</returns>
        private string GenerateStationRequest(string cityCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", "en");
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchStationRequest");
            xmlString.WriteStartElement("ItemDestination");
            xmlString.WriteAttributeString("DestinationType", "city");
            xmlString.WriteAttributeString("DestinationCode", cityCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// GetCurrencySymbol
        /// </summary>
        /// <param name="currencyCode">currencyCode</param>
        /// <returns>string</returns>
        public string GetCurrencySymbol(string currencyCode)
        {
            string symbol = string.Empty;
            if (currencyCode == "USD")
            {
                symbol = "$";
            }
            else
            {
                if (currencyCode == "GBP")
                {
                    symbol = "£";
                }
                else
                {
                    if (currencyCode == "EUR")
                    {
                        symbol = "€";
                    }
                    else
                    {
                        if (currencyCode == "AUD")
                        {
                            symbol = "A$";
                        }
                        else
                        {
                            if (currencyCode == (ConfigurationSystem.LocaleConfig["CurrencyCode"] ?? ""))
                            {
                                symbol = (ConfigurationSystem.LocaleConfig["CurrencySign"] ?? "");
                            }
                            else
                            {
                                symbol = currencyCode;
                            }
                        }
                    }
                }
            }
            return symbol;
        }


        //Push Mechanism For Cancel Transfer added on 19032016
        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="itemRefValue">itemRefValue</param>
        /// <param name="bookingReference">bookingReference</param>
        /// <param name="nationalityCode">nationalityCode</param>
        /// <param name="count">count</param>
        /// <returns>string</returns>
        private string GenerateGetTransferCancellingPushMechanismRequest(int itemRefValue, string bookingReference, string nationalityCode, int count)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("Request");
            xmlString.WriteStartElement("Source");
            xmlString.WriteStartElement("RequestorID");
            xmlString.WriteAttributeString("Client", clientId);
            xmlString.WriteAttributeString("EMailAddress", email);
            xmlString.WriteAttributeString("Password", password);
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestorPreferences");
            xmlString.WriteAttributeString("Language", language);
            xmlString.WriteAttributeString("Currency", "AED");
            xmlString.WriteAttributeString("Country", nationalityCode);
            xmlString.WriteElementString("RequestMode", "SYNCHRONOUS");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("SearchBookingItemRequest");
            xmlString.WriteStartElement("BookingReference"); //BookingReference
            xmlString.WriteAttributeString("ReferenceSource", "api");
            xmlString.WriteValue(bookingReference);
            xmlString.WriteEndElement();//End BookingReference
            xmlString.WriteElementString("ItemReference", itemRefValue.ToString());
            xmlString.WriteElementString("ShowPaymentStatus", "false");
            xmlString.WriteEndElement();//end SearchBookingItemRequest
            xmlString.WriteEndElement();//end RequestDetails
            xmlString.WriteEndElement();//Request
            xmlString.Close();
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(strWriter.ToString());
                string filePath = @"" + ConfigurationSystem.GTAConfig["TransLogPath"] + DateTime.Now.ToString("ddMMyyy_hhmmss_fff") + "_GTATransferCancellationRequest_"+(count+1)+".xml";
                XmlDoc.Save(filePath);
            }
            catch { }
            return strWriter.ToString();

        }


        #endregion

        #region IDisposable Support
        /// <summary>
        /// To detect redundant calls
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing">disposing(true/false)</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// <summary>
        /// ~GTA
        /// </summary>
        ~GTA()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary>
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
