﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CT.Configuration;
using CT.Core;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Xml;
using Newtonsoft.Json;
using System.Web;
using System.Data;
using System.Threading;
namespace CT.BookingEngine.GDS
{

    public class IllusionsApi : IDisposable
    {
        private string XmlPath = string.Empty;
        private string _token;
        private string _agentCode;
        private string _username;
        private string _password;
        private int appUserId;
        private string sessionId;
        decimal rateOfExchange = 1;
        string agentCurrency = "";
        Dictionary<string, decimal> exchangeRates;
        int decimalPoint;
        string sourceCountryCode;
        /// <summary>
        /// This variable is used,to store the Cancellation buffer,which is read from AppConfig table  
        /// </summary>
        double cnclBuffer;
        public List<HotelStaticData> StaticData { get; set; }

        private string SEARCH_HOTEL = ConfigurationSystem.IllusionsHotelConfig["SearchUrl"];

        public IllusionsApi()
        {
            assignCredential();
        }

        public IllusionsApi(string SessionId)
        {
            assignCredential();
            this.sessionId = SessionId;
        }
        private void assignCredential()
        {
            Guid guid = Guid.NewGuid();
            _token = guid.ToString();
            _username = ConfigurationSystem.IllusionsHotelConfig["username"];
            _password = ConfigurationSystem.IllusionsHotelConfig["password"];
            //Create Xml Log path folder
            XmlPath = ConfigurationSystem.IllusionsHotelConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        public string Username { get => _username; set => _username = value; }
        public string Password { get => _password; set => _password = value; }
        public double CnclBuffer { get => cnclBuffer; set => cnclBuffer = value; }

        private static Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }

        #region Get Hotel Results

        HotelRequest request = new HotelRequest();
        decimal markup = 0;
        string markupType = string.Empty;
        decimal discount = 0;
        string discounttype = string.Empty;

        List<string> hotelid = new List<string>();
        List<string> citycode = new List<string>();
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        List<HotelSearchResult> results = new List<HotelSearchResult>();
        //List<HotelStaticData> StaticData = new List<HotelStaticData>();

        public HotelSearchResult[] GetSearchResults(HotelRequest request, decimal markup, string markupType, decimal discount, string discounttype)
        {
            //GetHotelStaticData(request);
            try
            {
                this.request = request;
                this.markup = markup;
                this.markupType = markupType;
                this.discount = discount;
                this.discounttype = discounttype;
                results = new List<HotelSearchResult>();
                XmlDocument xmlDoc = new XmlDocument();
                string searchresponse = string.Empty;
                //HotelStaticData hotelStaticData = new HotelStaticData();
                //records means number of hotels passed in each request(this value is hard coded by default with 50)
                //maxrecords means number of hotels passed in each request(this value is hard coded by default with 50)
                //int records = 50, rowsCount = 0, startIndex = 0, endIndex = 0, maxrecords = 50;
                int records = 50, rowsCount = 0, startIndex = 0, endIndex = 0, maxrecords = 50;
                //int records = 999, rowsCount = 0, startIndex = 0, endIndex = 0, maxrecords = 999;// For Testing
                HotelSearchResult[] result = new HotelSearchResult[0];
                //ReadySources will hold same source orderings i.e Illusions ....
                Dictionary<string, int> readySources = new Dictionary<string, int>();
                //This dictionary will hold what fare search we want to call
                Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();

                if (StaticData != null && StaticData.Count > 0)
                {

                    //hotelid = StaticData.Select(i => i.IllusionsHotelCode).ToList();
		    hotelid = StaticData.Where(i => i.IllusionsHotelCode!=null).Select(i=>i.IllusionsHotelCode).ToList();
                    
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, "There is No Hotelid's for :" + request.CityName, "");
                    throw new Exception("No Hotels found :" + request.CityName);
                }

                rowsCount = hotelid.Count;

                int j = 0;
                while (rowsCount > 0)
                {
                    //records are <= default hotel request count in each request
                    //
                    if (records <= maxrecords)
                    {
                        startIndex = 0;
                        endIndex = (rowsCount <= records) ? rowsCount : records;
                    }
                    //records are > default hotel request count in each request
                    else
                    {
                        startIndex += maxrecords;
                        if ((records) < hotelid.Count)
                        {
                            //each request  need to pass 2999 records only
                            endIndex = maxrecords;
                        }
                        else
                        {
                            endIndex = rowsCount;
                        }
                    }
                    string key = j.ToString() + "-" + startIndex + "-" + endIndex;
                    listOfThreads.Add(key, new WaitCallback(GenarateSearchResults));
                    readySources.Add(key, 100);
                    j++;
                    rowsCount -= maxrecords;
                    if ((records + maxrecords) > hotelid.Count)
                    {
                        records += hotelid.Count - records;
                    }
                    else
                    {
                        records += maxrecords;
                    }
                }
                eventFlag = new AutoResetEvent[readySources.Count];
                int k = 0;
                //Start each fare search within a Thread which will automatically terminate after the results are received.
                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                {
                    if (readySources.ContainsKey(deThread.Key))
                    {
                        ThreadPool.QueueUserWorkItem(deThread.Value, deThread.Key);
                        eventFlag[k] = new AutoResetEvent(false);
                        k++;
                    }
                }
                if (k != 0)
                {//waiting time for all threads
                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 90), true))
                    {
                        //TODO: audit which thread is timed out                
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.IllusionsSearch, Severity.Normal, 1, "Illusions.GetHotelResults() Err :" + ex.ToString(), "0");
                throw ex;
            }
            return results.ToArray();


        }
        #endregion

        private void GenarateSearchResults(object eventNumber)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                //get the range values for request
                string[] values = eventNumber.ToString().Split('-');
                string postData = GenerateHotelSearchRequest(this.request, Convert.ToInt32(values[0]), Convert.ToInt32(values[1]), Convert.ToInt32(values[2]), hotelid);
                xmlDoc = GetHotelSearchResponse(postData, Convert.ToInt32(values[0]));
                List<HotelSearchResult> hotelresults = new List<HotelSearchResult>();
                if (xmlDoc != null)
                {
                    hotelresults = ReadHotelSearchResponse(xmlDoc, request, markup, markupType, discount, discounttype);
                    if (hotelresults != null && hotelresults.Count > 0)
                        results.AddRange(hotelresults);
                }
                eventFlag[Convert.ToInt32(eventNumber.ToString().Split('-')[0])].Set();
            }
            catch (Exception ex)
            {
                eventFlag[Convert.ToInt32(eventNumber.ToString().Split('-')[0])].Set();
                Audit.Add(EventType.IllusionsSearch, Severity.Normal, 1, "Illusions.GenerateHotelSearchResult  Err :" + ex.ToString(), "0");
                //throw ex;
            }
        }

        /// <summary>
        /// Used to send the request XML to the server and pulls the response XML.
        /// </summary>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>

        private XmlDocument GetHotelSearchResponse(string postData, int index)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.IllusionsHotelConfig["SearchUrl"]);
                request.Method = "POST"; //Using POST method       
                request.ContentType = contentType;
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();
                WebResponse response = request.GetResponse();
                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                try
                {
                    string filePath = XmlPath + "HotelSearchResponse_" + index + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xmlDoc;
        }

        private List<HotelSearchResult> ReadHotelSearchResponse(XmlDocument xmlDoc, HotelRequest request, decimal markup, string markupType, decimal discount, string discountType)
        {
            HotelSearchResult[] hotelResults = null;
            try
            {
                //xmlDoc.LoadXml(response);
                XmlNode errorNode = xmlDoc.SelectSingleNode("/HotelFindResponse/error");
                if (errorNode != null && errorNode.InnerText.Length > 0)
                {
                    Audit.Add(EventType.IllusionsSearch, Severity.High, 0, string.Concat(new object[]
                    {
                    " Illusions:HotelSearchResponse,Error Message:",
                    errorNode.Value,
                    " | ",
                    DateTime.Now,
                    "| Response XML",
                    xmlDoc.OuterXml
                    }), "");
                    //Trace.TraceError("Error: " + errorNode.InnerText);
                    throw new BookingEngineException("<br>" + errorNode.InnerText);
                }
                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                XmlNodeList hotelNodes = xmlDoc.SelectNodes("/HotelSearchResponse/Hotels/Hotel");
                hotelResults = new HotelSearchResult[hotelNodes.Count];
                int n = 0;
                string pattern = request.HotelName;
                if (hotelNodes != null)
                {
                    foreach (XmlNode node in hotelNodes)
                    {
                        HotelSearchResult hotelResult = new HotelSearchResult();
                        hotelResult.HotelCode = node.SelectSingleNode("HotelCode").InnerText;
                        try
                        {
                            //HotelStaticData hotelStaticData = StaticData.Find(h => h.HotelCode.Trim() == hotelResult.HotelCode);
			     HotelStaticData hotelStaticData = StaticData.Where(h=>h.IllusionsHotelCode!=null && h.IllusionsHotelCode==hotelResult.HotelCode).FirstOrDefault();
                            if (hotelStaticData != null)
                            {
                                hotelResult.HotelStaticID = hotelStaticData.HotelStaticID;
                                hotelResult.HotelDescription = !string.IsNullOrEmpty(hotelStaticData.HotelDescription) ? (GenericStatic.ReplaceNonASCII(hotelStaticData.HotelDescription.ToString())).Replace("\\", "") : "";
                                hotelResult.HotelAddress = !string.IsNullOrEmpty(hotelStaticData.HotelAddress) ? hotelStaticData.HotelAddress : "";
                                hotelResult.HotelMap = hotelStaticData.HotelMap;
                                hotelResult.HotelLocation = hotelStaticData.HotelLocation;
                                hotelResult.HotelContactNo = !string.IsNullOrEmpty(hotelStaticData.PhoneNumber) ? hotelStaticData.PhoneNumber : "";
                                hotelResult.HotelFacilities = !string.IsNullOrEmpty(hotelStaticData.HotelFacilities) ? GenericStatic.ReplaceNonASCII(hotelStaticData.HotelFacilities).Replace("\\", "").Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).Select(Convert.ToString).ToList() : null;
                                hotelResult.HotelImages = hotelStaticData.Images;
                                hotelResult.HotelPicture = hotelStaticData.Images[0];

                            }
                            else
                            {
                                continue;
                            }
                        }
                        catch(Exception ex) { string vg = ex.Message; continue ; }
                        //hotelResult.Currency = request.Currency;
                        hotelResult.StartDate = request.StartDate;
                        hotelResult.EndDate = request.EndDate;
                        hotelResult.HotelName = node.SelectSingleNode("HotelName").InnerText;
                        if (!string.IsNullOrEmpty(node.SelectSingleNode("StarRating").InnerText))
                        {
                            hotelResult.Rating = (HotelRating)Convert.ToInt32(Math.Floor(Convert.ToDecimal(node.SelectSingleNode("StarRating").InnerText)));
                        }
                        //hotelResult.CityCode = request.CityCode;
                        hotelResult.CityCode = node.SelectSingleNode("City").InnerText;
                        hotelResult.PropertyType = _token;
                        //Get the Hotel Static data,fill it if not exixt
                        hotelResult.RoomGuest = request.RoomGuest;

                        #region Modified by brahmam search time avoid Staticinfo downloading
                        hotelResult.BookingSource = HotelBookingSource.Illusions;
                        #endregion

                        #region To get only those satisfy the search conditions
                        if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                        {
                            if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        #endregion

                        XmlNodeList roomNodes = node.SelectNodes("RoomTypeDetails/Rooms/Room");
                        //HotelRoomsDetails[] rooms = new HotelRoomsDetails[roomNodes.Count * request.NoOfRooms];
                        //hotelResults = new HotelSearchResult[roomNodes.Count];
                        HotelRoomsDetails[] rooms = new HotelRoomsDetails[roomNodes.Count];
                        HotelRoomsDetails roomdetail = default(HotelRoomsDetails);
                        int index = 0;
                        foreach (XmlNode rnode in roomNodes)
                        {
                            string noOfRooms = Convert.ToString(request.NoOfRooms);
                            roomdetail.SupplierName = HotelBookingSource.Illusions.ToString();
                            roomdetail.SupplierId = Convert.ToString((int)HotelBookingSource.Illusions);
                            roomdetail.IsIndividualSelection = true;//Here IsIndividualSelection = true means roomselection is individual.
                            roomdetail.RoomTypeName = rnode.SelectSingleNode("RoomType").InnerText;
                            roomdetail.SequenceNo = rnode.SelectSingleNode("RoomNo").InnerText;
                            roomdetail.RatePlanId = rnode.SelectSingleNode("RoomConfigurationId").InnerText;
                            roomdetail.RatePlanCode = rnode.SelectSingleNode("RoomTypeCode").InnerText + "||" + rnode.SelectSingleNode("MealPlanCode").InnerText + "||" + rnode.SelectSingleNode("ContractTokenId").InnerText;
                            roomdetail.DynamicInventory= rnode.SelectSingleNode("DynamicYN").InnerText;
                            if (!string.IsNullOrEmpty(rnode.SelectSingleNode("MealPlan").InnerText))
                            {
                                roomdetail.mealPlanDesc = rnode.SelectSingleNode("MealPlan").InnerText;//2 rooms they giving one pipe symbol only
                            }
                            else
                            {
                                roomdetail.mealPlanDesc = "Room Only";
                            }

                            roomdetail.RoomTypeCode = rnode.SelectSingleNode("RoomTypeCode").InnerText + "-" + rnode.SelectSingleNode("MealPlanCode").InnerText + "-" + rnode.SelectSingleNode("ContractTokenId").InnerText + "-" + roomdetail.SequenceNo;
                            rateOfExchange = exchangeRates[rnode.SelectSingleNode("CurrCode").InnerText];
                            hotelResult.Currency = rnode.SelectSingleNode("CurrCode").InnerText;
                            hotelResult.SupplierType = rnode.SelectSingleNode("CurrCode").InnerText;
                            decimal totPrice = 0m;
                            totPrice = Convert.ToDecimal(rnode.SelectSingleNode("Rate").InnerText);

                            //VAT Calucation Changes Modified 14.12.2017
                            decimal hotelTotalPrice = 0m;
                            decimal vatAmount = 0m;
                            hotelTotalPrice = Math.Round((totPrice) * rateOfExchange, decimalPoint);
                            if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                            {
                                hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                            }
                            hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                            //price calculation
                            roomdetail.TotalPrice = hotelTotalPrice;
                            roomdetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomdetail.TotalPrice) * (markup / 100m)));
                            roomdetail.MarkupType = markupType;
                            roomdetail.MarkupValue = markup;
                            roomdetail.SellingFare = roomdetail.TotalPrice;
                            roomdetail.supplierPrice = totPrice;
                            roomdetail.TaxDetail = new PriceTaxDetails();
                            roomdetail.TaxDetail = priceTaxDet;
                            roomdetail.InputVATAmount = vatAmount;
                            roomdetail.TotalPrice = roomdetail.TotalPrice + roomdetail.Markup;
                            roomdetail.Discount = 0;
                            if (markup > 0 && discount > 0)
                            {
                                roomdetail.Discount = discountType == "F" ? discount : roomdetail.Markup * (discount / 100);
                            }
                            hotelResult.Price = new PriceAccounts();
                            hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                            hotelResult.Price.SupplierPrice = totPrice;
                            hotelResult.Price.RateOfExchange = rateOfExchange;

                            System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                            RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                            decimal totalprice = roomdetail.TotalPrice;

                            for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                            {
                                decimal price = roomdetail.TotalPrice / diffResult.Days;
                                if (fareIndex == diffResult.Days - 1)
                                {
                                    price = totalprice;
                                }
                                totalprice -= price;
                                hRoomRates[fareIndex].Amount = price;
                                hRoomRates[fareIndex].BaseFare = price;
                                hRoomRates[fareIndex].SellingFare = price;
                                hRoomRates[fareIndex].Totalfare = price;
                                hRoomRates[fareIndex].RateType = RateType.Negotiated;
                                hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);

                            }
                            roomdetail.Rates = hRoomRates;
                            rooms[index] = roomdetail;
                            index++;


                            hotelResult.RoomDetails = rooms.OrderBy(h => h.TotalPrice).ToArray();
                        }

                        hotelResult.Currency = agentCurrency;
                        hotelResults[n++] = hotelResult;
                    }
                    if (hotelResults.Length > n)
                    {
                        Array.Resize(ref hotelResults, n);
                    }

                    foreach (HotelSearchResult hotelResult in hotelResults)
                    {
                        //hotelResult.Price = new PriceAccounts();
                        for (int k = 0; k < request.NoOfRooms; k++)
                        {
                            for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                            {
                                if (hotelResult.RoomDetails[j].SequenceNo.Contains((k + 1).ToString()))
                                {
                                    hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice;
                                    hotelResult.Price.Discount += hotelResult.RoomDetails[j].Discount;
                                    hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                    hotelResult.Price.AccPriceType = PriceType.NetFare;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return hotelResults.ToList();
        }




        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GenerateHotelSearchRequest(HotelRequest hotelRequest, int index, int startIndex, int endIndex, List<string> hotelid)
        {
            int roomconfig = 0;
            StringBuilder request = new StringBuilder();
            string illusionhotelcode = string.Empty;
            hotelid.Sort();

            XmlWriter writer = XmlWriter.Create(request);
            writer.WriteStartElement("HotelSearchRequest");
            writer.WriteStartElement("Profile");
            writer.WriteStartElement("Password");
            writer.WriteValue(_password);
            writer.WriteEndElement();
            writer.WriteStartElement("Code");
            writer.WriteValue(_username);
            writer.WriteEndElement();
            writer.WriteStartElement("TokenNumber");
            writer.WriteValue(_token);
            writer.WriteEndElement();
            writer.WriteEndElement();//Profile

            writer.WriteStartElement("SearchCriteria");

            writer.WriteStartElement("RoomConfiguration");
            for (int j = 0; j < hotelRequest.RoomGuest.Length; j++)
            {
                RoomGuestData room = hotelRequest.RoomGuest[j];
                writer.WriteStartElement("Room");
                for (int a = 0; a < room.noOfAdults; a++)
                {
                    writer.WriteStartElement("Adult");
                    writer.WriteStartElement("Age");
                    writer.WriteValue("28");
                    writer.WriteEndElement();//Age
                    writer.WriteEndElement();//Adult
                }

                if (room.noOfChild > 0)
                {
                    foreach (int age in room.childAge)
                    {
                        writer.WriteStartElement("Child");
                        writer.WriteStartElement("Age");
                        writer.WriteValue(age.ToString());
                        writer.WriteEndElement();//Age
                        writer.WriteEndElement();//Child
                    }

                }
                writer.WriteStartElement("RoomConfigurationId");
                writer.WriteValue(roomconfig + 1);
                writer.WriteEndElement();
                writer.WriteEndElement();//Room
                roomconfig++;
            }
            writer.WriteEndElement();//RoomConfiguration
            writer.WriteStartElement("StartDate");
            writer.WriteValue(hotelRequest.StartDate.ToString("yyyyMMdd"));
            writer.WriteEndElement();
            writer.WriteStartElement("EndDate");
            writer.WriteValue(hotelRequest.EndDate.ToString("yyyyMMdd"));
            writer.WriteEndElement();
            //writer.WriteStartElement("City");
            //writer.WriteValue(hotelRequest.CityCode);
            //writer.WriteEndElement();
            writer.WriteStartElement("HotelCode");
            for (int i = startIndex; i < startIndex + endIndex; i++)
            {
                // writer.WriteElementString("Int", Convert.ToString(hotelid[i])); 
                //writer.WriteValue(string.Join(",", hotelid[i]));
                if (illusionhotelcode.Length > 0)
                {
                    illusionhotelcode += "," + hotelid[i];
                }
                else
                {
                    illusionhotelcode = hotelid[i];
                }
            }
            writer.WriteValue(illusionhotelcode);
            //writer.WriteValue(illusionhotelcode);
            writer.WriteEndElement();
            writer.WriteStartElement("IncludeOnRequest");
            writer.WriteValue("N");
            writer.WriteEndElement();
            writer.WriteStartElement("Nationality");
            writer.WriteValue(hotelRequest.PassengerNationality);
            writer.WriteEndElement();
            writer.WriteStartElement("StarRatingConfiguration");
            switch (hotelRequest.Rating)
            {
                case HotelRating.All:
                case HotelRating.FiveStar:
                    for (int i = 1; i <= 5; i++)
                    {
                        writer.WriteStartElement("StarRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
                case HotelRating.FourStar:
                    for (int i = 1; i <= 4; i++)
                    {
                        writer.WriteStartElement("StarRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
                case HotelRating.ThreeStar:
                    for (int i = 1; i <= 3; i++)
                    {
                        writer.WriteStartElement("StarRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
                case HotelRating.TwoStar:
                    for (int i = 1; i <= 2; i++)
                    {
                        writer.WriteStartElement("StarRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
                case HotelRating.OneStar:
                    for (int i = 1; i <= 1; i++)
                    {
                        writer.WriteStartElement("StarRating");
                        writer.WriteValue(i);
                        writer.WriteEndElement();
                    }
                    break;
            }
            writer.WriteEndElement();//StarRatingConfiguration
            writer.WriteEndElement();//SearchCriteria
            writer.WriteEndElement();//HotelSearchRequest
            writer.Flush();
            writer.Close();
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                string filePath = XmlPath + "HotelSearchRequest_" + index + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                //XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(request.ToString());
                foreach (XmlNode node in xmldoc)
                {
                    if (node.NodeType == XmlNodeType.XmlDeclaration)
                    {
                        xmldoc.RemoveChild(node);
                    }
                }
                xmldoc.Save(filePath);
            }
            catch { }
            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmldoc.WriteTo(xw);
            return sw.ToString();
        }
        string cancelInfo = null;
        public Dictionary<string, string> getIllusionsApiCancellationInfo(ref HotelSearchResult hResult, List<string> Rooms, HotelRequest request, decimal markup, string markupType)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
            try
            {

                string ratePlan = string.Empty;
                string roomTypeCode = string.Empty;
                cancelInfo = string.Empty;
                foreach (string pair in Rooms)
                {

                    roomTypeCode = pair;
                    //ratePlan = roomTypeCode;

                    cancellationInfo = GetCancellationInfo(ref hResult, markup, markupType, roomTypeCode);
                    for (int i = 0; i < hResult.RoomDetails.Length; i++)
                    {
                        if (hResult.RoomDetails[i].RoomTypeCode == roomTypeCode)
                        {
                            // hResult.RoomDetails[i].CancellationPolicy = cancellationInfo.ContainsKey("CancelPolicy") ? cancellationInfo["CancelPolicy"] : "";
                            //Cancel Policy Updates
                            if (cancellationInfo.ContainsKey("CancelPolicyJsonString"))
                            {
                                List<HotelCancelPolicy> deserializedCancelPolicyList = JsonConvert.DeserializeObject<List<HotelCancelPolicy>>(cancellationInfo["CancelPolicyJsonString"]);
                                hResult.RoomDetails[i].CancelPolicyList = deserializedCancelPolicyList;
                            }
                        }

                    }
                }
                //if (flag == "Book")
                    GetHotelDetailsInformation(ref hResult);
            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to get Apicancellation response ", ex);
            }
            return cancellationInfo;
        }

        public Dictionary<string, string> GetCancellationInfo(ref HotelSearchResult hResult, decimal markup, string markupType, string roomTypeCode)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
            string postData = GenerateCancellationInfoRequest(ref hResult, roomTypeCode);// GETTING XML STRING...
            //XmlDocument responseXML = GetCancellationInfoResponse(ref hResult, roomTypeCode);
            XmlDocument responseXML = GetResponse(postData, "Cancellation policy request");
            if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("CancellationPolicyResponse"))
            {
                cancellationInfo = ReadCancellationInfoResponse(responseXML, ref hResult, markup, markupType, roomTypeCode);

            }
            return cancellationInfo;
        }

        private Dictionary<string, string> ReadCancellationInfoResponse(XmlDocument xmldoc, ref HotelSearchResult hResult, decimal markup, string markupType, string roomTypeCode)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
            try
            {
                try
                {
                    string filePath = XmlPath + "CancellationPolicyResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.GetCancellationPolicy, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNodeList cancellationNodes = xmldoc.SelectNodes("/HotelCancellationPolicyResponse/CancellationDetails/Cancellation");
                //double buffer = 0;
                //if (ConfigurationSystem.IllusionsHotelConfig.ContainsKey("Buffer"))
                //{
                //    buffer = Convert.ToDouble(ConfigurationSystem.IllusionsHotelConfig["Buffer"]);
                //}
                if (cancellationNodes != null)
                {
                    string endDate = "";
                    decimal amount = 0m;
                    string ChargeType = "";
                    string msg = "";
                    int NoofRooms = 1;
                    string currency = string.Empty;
                    decimal CancelAmount = 0m;
                    //Cancel Policy Updates
                    List<HotelCancelPolicy> hotelPolicyList = new List<HotelCancelPolicy>();
                    foreach (XmlNode cancelNode in cancellationNodes)
                    {
                        //Cancel Policy Updates
                        HotelCancelPolicy hotelPolicy = new HotelCancelPolicy();

                        string startDate = cancelNode.SelectSingleNode("FromDate").InnerText;
                        DateTime stdt = DateTime.ParseExact(startDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        startDate = (stdt.AddDays(-(CnclBuffer))).ToString("dd MMM yyyy HH:mm:ss");
                        endDate = cancelNode.SelectSingleNode("ToDate").InnerText;
                        DateTime eddt = DateTime.ParseExact(endDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        endDate = (eddt.AddDays(-(CnclBuffer))).ToString("dd MMM yyyy HH:mm:ss");
                        string NightToCharge = cancelNode.SelectSingleNode("NightToCharge").InnerText;
                        if (NightToCharge == "0")
                        {
                            ChargeType = cancelNode.SelectSingleNode("PercentOrAmt").InnerText;
                            amount = Convert.ToDecimal(cancelNode.SelectSingleNode("Value").InnerText);
                        }
                        XmlNode CurrencyCode = cancelNode.SelectSingleNode("CurrencyCode");
                        if (CurrencyCode != null && CurrencyCode.InnerText.Length > 0)
                        {
                            currency = CurrencyCode.InnerText;
                        }
                        else
                        {
                            //currency = hResult.Currency;
                            currency = hResult.SupplierType;
                        }
                        XmlNode CancellationAmount = cancelNode.SelectSingleNode("CancellationAmount");
                        if (CancellationAmount != null && CancellationAmount.InnerText.Length > 0)
                        {
                            CancelAmount = Convert.ToDecimal(CancellationAmount.InnerText);
                        }
                        //string currency = "AED";
                        decimal rateOfExchange = exchangeRates[currency];
                        decimal markuprice = 0m;
                        if (markup > 0)
                        {
                            markuprice = ((markupType == "F") ? markup : ((amount) * (markup / 100m)));
                            amount = amount + markuprice;
                        }
                        //Cancel Policy Updates
                        hotelPolicy.FromDate = Convert.ToDateTime(startDate);
                        hotelPolicy.Todate = Convert.ToDateTime(endDate);
                        hotelPolicy.BufferDays = Convert.ToInt32(Math.Round(CnclBuffer));
                        hotelPolicy.ChargeType = ChargeType;
                        hotelPolicy.Currency = agentCurrency;

                        if (NightToCharge == "0")
                        {
                            if (ChargeType == "A")
                            {
                                //Cancel Policy Updates
                                hotelPolicy.Amount = Math.Round((amount / NoofRooms * rateOfExchange), decimalPoint);
                                if (msg.Length > 0)
                                {
                                    msg += "@@" + agentCurrency + " " + Math.Round((amount / NoofRooms * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate; ;
                                }
                                else
                                {
                                    msg = agentCurrency + " " + Math.Round((amount / NoofRooms * rateOfExchange), decimalPoint) + " will be charged between " + startDate + " and " + endDate;
                                }
                            }
                            else if (ChargeType == "P")
                            {
                                //Cancel Policy Updates
                                amount = Convert.ToDecimal(amount.ToString("N" + decimalPoint));
                                hotelPolicy.Amount = amount;
                                if (msg.Length > 0)
                                {
                                    msg += "@@" + amount + "% of amount will be charged between " + startDate + " and " + endDate; ;
                                }
                                else
                                {
                                    msg = amount + "% of amount will be charged between " + startDate + " and " + endDate; ;
                                }
                            }

                        }
                        else
                        {
                            if (msg.Length > 0)
                            {
                                msg += "@@" + NightToCharge + " night(s) to be charged between " + startDate + " and " + endDate; ;
                            }
                            else
                            {
                                msg = NightToCharge + " night(s) to be charged between " + startDate + " and " + endDate; ;
                            }

                        }

                        hotelPolicy.Remarks = msg;

                        //Cancel Policy Updates
                        hotelPolicyList.Add(hotelPolicy);
                    }
                    hResult.RoomDetails[hResult.RoomDetails.ToList().IndexOf(hResult.RoomDetails.ToList().Single(h => h.RoomTypeCode == roomTypeCode))].CancellationPolicy = msg;
                    if (!string.IsNullOrEmpty(msg))
                    {
                        if (cancelInfo.Length > 0)
                        {
                            cancelInfo += "|" + msg;
                        }
                        else
                        {
                            cancelInfo = msg;
                        }
                    }
                    cancellationInfo.Add("lastCancellationDate", endDate);
                    cancellationInfo.Add("CancelPolicy", cancelInfo);
                    //Cancel Policy Updates
                    if (hotelPolicyList.Count > 0)
                    {
                        string cancelPolicyJsonString = JsonConvert.SerializeObject(hotelPolicyList);
                        cancellationInfo.Add("CancelPolicyJsonString", cancelPolicyJsonString);
                    }
                }


            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to Read cancellation response ", ex);
            }
            return cancellationInfo;
        }

        public void GetHotelDetailsInformation(ref HotelSearchResult hResult)
        {
            string request = string.Empty;
            string resp = string.Empty;

            try
            {
                GetHotelDetails(ref hResult);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, 0, "Exception returned from Illusions.HotelDetailsRequest Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                //Trace.TraceError("Error: " + ex.Message);
            }


        }

        public void GetHotelDetails(ref HotelSearchResult hResult)
        {

            XmlDocument responseXML = GetHotelDetailResponse(ref hResult);

            if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("HotelDetailsResponse"))
            {

                ReadHotelDetailsResponse(responseXML, ref hResult);
            }
            else
            {
                Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, "HotelDetails Not found" + hResult.HotelCode.ToString(), "127.0.0.1");
            }

        }

        private void ReadHotelDetailsResponse(XmlDocument xmldoc, ref HotelSearchResult hResult)
        {
            try
            {

                try
                {
                    string filePath = XmlPath + "HotelDetailsResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    try
                    {
                        xmldoc.Save(filePath);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.HotelSearch, Severity.Normal, 0, "HotelDetailsIllusionsXml Not Saveing : " + ex.Message + "", "1");
                    }
                }
                catch { }

                XmlNode errorNode = xmldoc.SelectSingleNode("HotelDetailsResponse/error");
                if (errorNode != null && errorNode.InnerText.Length > 0)
                {
                    throw new BookingEngineException(errorNode.InnerText);
                }

                else
                {
                    XmlNodeList msgNodes = xmldoc.SelectNodes("/HotelDetailsResponse/Details/Messages/Message");

                    if (msgNodes != null)
                    {
                        foreach (XmlNode msgnode in msgNodes)
                        {
                            string MessageShort = msgnode.SelectSingleNode("MessageShort").InnerText;
                            string MessageFull = !string.IsNullOrEmpty(msgnode.SelectSingleNode("MessageFull").InnerText) ? (GenericStatic.ReplaceNonASCII(msgnode.SelectSingleNode("MessageFull").InnerText)).Replace("\\", "") : "";
                            string startDate = msgnode.SelectSingleNode("StartDate").InnerText;
                            DateTime stdt = DateTime.ParseExact(startDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                            startDate = stdt.ToString("dd-MM-yyy");
                            string endDate = msgnode.SelectSingleNode("EndDate").InnerText;
                            DateTime enddt = DateTime.ParseExact(endDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                            endDate = enddt.ToString("dd-MM-yyy");

                            if (!string.IsNullOrEmpty(hResult.HotelPolicyDetails))
                            {
                                hResult.HotelPolicyDetails += "|" + MessageShort + "<br/>" + MessageFull + "<br/> Start Date : " + startDate + "<br/>  End Date : " + endDate;
                            }
                            else
                            {
                                hResult.HotelPolicyDetails = MessageShort + "<br/>" + MessageFull + "<br/> Start Date : " + startDate + "<br/>  End Date : " + endDate;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to read Hotel Details response ", ex);
            }
        }

        private XmlDocument GetHotelDetailResponse(ref HotelSearchResult hResult)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.IllusionsHotelConfig["HotelDetails"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelDetailRequest(ref hResult);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;
                using (Stream dataStream = GetStreamForResponse(httpResponse))
                {
                    xmlDoc.Load(dataStream);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to get response for Hotel Detail request for HotelId : " + hResult.HotelCode, ex);
            }
            return xmlDoc;
        }

        private string GenerateHotelDetailRequest(ref HotelSearchResult hResult)
        {
            StringBuilder request = new StringBuilder();
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteStartElement("HotelDetailsRequest");
                writer.WriteStartElement("Profile");
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteStartElement("Code");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("TokenNumber");
                //writer.WriteValue(_token);
                writer.WriteValue(hResult.PropertyType);
                writer.WriteEndElement();
                writer.WriteEndElement();//Profile

                writer.WriteStartElement("SearchCriteria");
                writer.WriteStartElement("StartDate");
                writer.WriteValue(hResult.StartDate.ToString("yyyyMMdd"));
                writer.WriteEndElement();
                writer.WriteStartElement("EndDate");
                writer.WriteValue(hResult.EndDate.ToString("yyyyMMdd"));
                writer.WriteEndElement();
                writer.WriteStartElement("City");
                writer.WriteValue(hResult.CityCode);
                writer.WriteEndElement();
                writer.WriteStartElement("HotelCode");
                writer.WriteValue(hResult.HotelCode);
                writer.WriteEndElement();
                writer.WriteEndElement();//SearchCriteria
                writer.WriteEndElement();//HotelDetailsRequest

                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "HotelDetailsRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";

                    xmldoc.LoadXml(request.ToString());
                    foreach (XmlNode node in xmldoc)
                    {
                        if (node.NodeType == XmlNodeType.XmlDeclaration)
                        {
                            xmldoc.RemoveChild(node);
                        }
                    }
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to generate hotel detail request for Hotel : " + hResult.HotelCode, ex);
            }

            //return "XML=" + HttpUtility.UrlEncode(request.ToString());
            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmldoc.WriteTo(xw);
            return sw.ToString();
        }

        //private string GetResponse(string requestData, string url)
        private XmlDocument GetResponse(string postData, string requesttype)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.IllusionsHotelConfig["ServiceUrl"]);

                request.Method = "POST"; //Using POST method  

                //string postData = requestData;// GETTING JSON STRING...               

                request.ContentType = contentType;
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");

                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();
                HttpWebResponse httpResponse = response as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);

                using (XmlReader reader = XmlReader.Create(new StreamReader(dataStream, Encoding.UTF8)))
                {
                    xmlDoc.Load(reader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to get response for " + requesttype, ex);
            }

            return xmlDoc;
        }

        //private XmlDocument GetCancellationInfoResponse(ref HotelSearchResult hResult, string roomTypeCode)
        //{
        //    //string responseFromServer = string.Empty;
        //    XmlDocument xmlDoc = new XmlDocument();
        //    try
        //    {
        //        const string contentType = "application/x-www-form-urlencoded";
        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.IllusionsHotelConfig["ServiceUrl"]);
        //        request.Method = "POST"; //Using POST method       
        //        string postData = GenerateCancellationInfoRequest(ref hResult, roomTypeCode);// GETTING XML STRING...

        //        request.Method = "POST";
        //        request.ContentType = contentType;

        //        // request for compressed response. This does not seem to have any effect now.
        //        request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
        //        request.ContentLength = postData.Length;
        //        StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
        //        request.ContentType = contentType;
        //        requestWriter.Write(postData);
        //        requestWriter.Close();

        //        WebResponse response = request.GetResponse();

        //        //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
        //        HttpWebResponse httpResponse = response as HttpWebResponse;

        //        // handle if response is a compressed stream
        //        Stream dataStream = GetStreamForResponse(httpResponse);
        //        using (XmlReader reader = XmlReader.Create(new StreamReader(dataStream, Encoding.UTF8)))
        //        {
        //            xmlDoc.Load(reader);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Illusions: Failed to get response for cancellation policy request", ex);
        //    }
        //    return xmlDoc;
        //}

        private string GenerateCancellationInfoRequest(ref HotelSearchResult hResult, string roomTypeCode)
        {
            SimilarHotelSourceInfo illusionHotel = hResult.SimilarHotels.Where(f => f.Source == HotelBookingSource.Illusions).FirstOrDefault();

            StringBuilder request = new StringBuilder();
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteStartElement("HotelCancellationPolicyRequest");
                writer.WriteStartElement("Profile");
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteStartElement("Code");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("TokenNumber");
                //writer.WriteValue(_token);
                writer.WriteValue(hResult.PropertyType);
                writer.WriteEndElement();
                writer.WriteEndElement();//Profile

                writer.WriteStartElement("SearchCriteria");

                writer.WriteStartElement("HotelCode");
                writer.WriteValue(illusionHotel.HotelCode);
                //writer.WriteValue(hResult.HotelCode);
                writer.WriteEndElement();
                writer.WriteStartElement("StartDate");
                writer.WriteValue(hResult.StartDate.ToString("yyyyMMdd"));
                writer.WriteEndElement();
                writer.WriteStartElement("EndDate");
                writer.WriteValue(hResult.EndDate.ToString("yyyyMMdd"));
                writer.WriteEndElement();
                writer.WriteStartElement("RoomTypeCode");
                writer.WriteValue(roomTypeCode.Split('-')[0]);
                writer.WriteEndElement();
                writer.WriteStartElement("ContractTokenId");
                writer.WriteValue(roomTypeCode.Split('-')[2]);
                writer.WriteEndElement();

                writer.WriteEndElement();//SearchCriteria
                writer.WriteEndElement();//CancellationPolicyRequest
                writer.Flush();
                writer.Close();


                try
                {
                    string filePath = XmlPath + "CancellationPolicyRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";

                    xmldoc.LoadXml(request.ToString());
                    foreach (XmlNode node in xmldoc)
                    {
                        if (node.NodeType == XmlNodeType.XmlDeclaration)
                        {
                            xmldoc.RemoveChild(node);
                        }
                    }
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.GetCancellationPolicy, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed generate cancellation info request for Hotel : " + hResult.HotelName, ex);
            }

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmldoc.WriteTo(xw);

            return "data=" + sw.ToString();
        }

        public Dictionary<string, string> GetItemPriceCheck(HotelItinerary itinerary)
        {
            Dictionary<string, string> preBookInfo = new Dictionary<string, string>();
            string postData = GenerateItemPriceCheckRequest(itinerary);// GETTING XML STRING...
            //XmlDocument responseXML = GetItemPriceCheckResponse(itinerary);
            XmlDocument responseXML = GetResponse(postData, "item price check request");
            if (responseXML != null && responseXML.ChildNodes.Count > 0 && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("HotelSearchResponse"))
            {
                preBookInfo = ReadItemPriceCheckResponse(responseXML, itinerary);
            }
            return preBookInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="hotelRequest"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        private string GenerateItemPriceCheckRequest(HotelItinerary itinerary)
        {

            StringBuilder request = new StringBuilder();
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteStartElement("HotelSearchRequest");
                writer.WriteStartElement("Profile");
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteStartElement("Code");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("TokenNumber");
                //writer.WriteValue(_token);
                writer.WriteValue(itinerary.PropertyType);
                writer.WriteEndElement();
                writer.WriteEndElement();//Profile

                writer.WriteStartElement("SearchCriteria");

                writer.WriteStartElement("RoomConfiguration");
                for (int j = 0; j < itinerary.Roomtype.Length; j++)
                {
                    CT.BookingEngine.HotelRoom room = itinerary.Roomtype[j];
                    writer.WriteStartElement("Room");
                    for (int a = 0; a < room.AdultCount; a++)
                    {
                        writer.WriteStartElement("Adult");
                        writer.WriteStartElement("Age");
                        writer.WriteValue("28");
                        writer.WriteEndElement();//Age
                        writer.WriteEndElement();//Adult
                    }

                    if (room.ChildCount > 0)
                    {
                        foreach (int age in room.ChildAge)
                        {
                            writer.WriteStartElement("Child");
                            writer.WriteStartElement("Age");
                            writer.WriteValue(age.ToString());
                            writer.WriteEndElement();//Age
                            writer.WriteEndElement();//Child
                        }

                    }
                    writer.WriteStartElement("ContractTokenId");
                    writer.WriteValue(room.RatePlanCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[2]);
                    writer.WriteEndElement();
                    writer.WriteStartElement("MealPlanCode");
                    writer.WriteValue(room.RatePlanCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1]);
                    writer.WriteEndElement();
                    writer.WriteStartElement("RoomConfigurationId");
                    writer.WriteValue(room.RatePlanId);
                    writer.WriteEndElement();
                    writer.WriteStartElement("RoomTypeCode");
                    writer.WriteValue(room.RatePlanCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                    writer.WriteEndElement();
                    writer.WriteEndElement();//Room
                }
                writer.WriteEndElement();//RoomConfiguration
                writer.WriteStartElement("StartDate");
                writer.WriteValue(itinerary.StartDate.ToString("yyyyMMdd"));
                writer.WriteEndElement();
                writer.WriteStartElement("EndDate");
                writer.WriteValue(itinerary.EndDate.ToString("yyyyMMdd"));
                writer.WriteEndElement();
                writer.WriteStartElement("IncludeOnRequest");
                writer.WriteValue("N");
                //writer.WriteValue("Y");
                writer.WriteEndElement();
                writer.WriteStartElement("Nationality");
                writer.WriteValue(Country.GetCountryCodeFromCountryName(itinerary.PassengerNationality));
                writer.WriteEndElement();
                writer.WriteStartElement("City");
                writer.WriteValue(itinerary.CityCode);
                writer.WriteEndElement();
                writer.WriteStartElement("HotelCode");
                writer.WriteValue(itinerary.HotelCode);
                writer.WriteEndElement();
                writer.WriteStartElement("IncludeRateDetails");
                writer.WriteValue("Y");
                writer.WriteEndElement();
                writer.WriteEndElement();//SearchCriteria
                writer.WriteEndElement();//HotelSearchRequest
                writer.Flush();
                writer.Close();

                try
                {
                    string filePath = XmlPath + "ItemPriceRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmldoc.LoadXml(request.ToString());
                    foreach (XmlNode node in xmldoc)
                    {
                        if (node.NodeType == XmlNodeType.XmlDeclaration)
                        {
                            xmldoc.RemoveChild(node);
                        }
                    }
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed generate prebooking info request for Hotel : " + itinerary.HotelName, ex);
            }
            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmldoc.WriteTo(xw);
            return "data=" + sw.ToString();
        }

        /// <summary>
        /// Read preBooking Info
        /// </summary>
        /// <param name="responseXML"></param>
        /// <param name="hotelPrice"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadItemPriceCheckResponse(XmlDocument xmldoc, HotelItinerary itinerary)
        {
            Dictionary<string, string> preBookInfo = new Dictionary<string, string>();
            string status = string.Empty;
            string difference = string.Empty;

            string cancelInfo = "";
            bool RoomstatusCnf = true;
            decimal oldPrice = itinerary.Roomtype.Sum(x => x.Price.SupplierPrice);
            decimal newPrice = 0m;

            try
            {
                try
                {
                    string filePath = XmlPath + "ItemPriceResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmldoc.Save(filePath);
                    // Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNodeList HotelNodes = xmldoc.SelectNodes("/HotelSearchResponse/Hotels/Hotel");
                if (HotelNodes != null)
                {
                    HotelSearchResult hotelResult = new HotelSearchResult();
                    foreach (XmlNode priceNode in HotelNodes)
                    {
                        XmlNodeList roomNodes = priceNode.SelectNodes("RoomTypeDetails/Rooms/Room");
                        //hotelResults = new HotelSearchResult[roomNodes.Count];
                        if (roomNodes != null)
                        {

                            foreach (XmlNode rnode in roomNodes)
                            {

                                if (rnode.SelectSingleNode("RoomStatus").InnerText != "OK")
                                {
                                    RoomstatusCnf = false;
                                    break;
                                }
                                else
                                {
                                    decimal totPrice = 0m;
                                    totPrice = Convert.ToDecimal(rnode.SelectSingleNode("Rate").InnerText);
                                    newPrice += totPrice;

                                }
                            }

                        }
                    }
                }


                if ((oldPrice == newPrice) && (RoomstatusCnf = true))
                {
                    preBookInfo.Add("Status", "true");
                    preBookInfo.Add("Error", "");
                }
                else
                {
                    preBookInfo.Add("Status", "false");
                    preBookInfo.Add("Error", "PriceChanged");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to Read Item Price check response ", ex);
            }
            return preBookInfo;
        }

        public BookingResponse Book(ref HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            string postData = GenerateHotelBookingRequest(itinerary);// GETTING XML STRING...
            //XmlDocument responseXML = GetHotelBookingResponse(itinerary);
            XmlDocument responseXML = GetResponse(postData, "Hotel Bookings");
            if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("HotelBookingResponse"))
            {
                bookResponse = ReadHotelBookingResponse(responseXML, ref itinerary);
            }
            else
            {
                bookResponse.Error = "Booking Failed";
            }
            return bookResponse;
        }

        private string GenerateHotelBookingRequest(HotelItinerary itinerary)
        {
            StringBuilder request = new StringBuilder();
            int index = 0;
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                #region XML Code
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteStartElement("HotelBookingRequest");
                writer.WriteStartElement("Profile");
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteStartElement("Code");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("TokenNumber");
                //writer.WriteValue(_token);
                writer.WriteValue(itinerary.PropertyType);
                writer.WriteEndElement();
                writer.WriteEndElement();//Profile

                writer.WriteStartElement("PassengerDetails");
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    CT.BookingEngine.HotelRoom room = itinerary.Roomtype[i];
                    //int index = 0;
                    foreach (HotelPassenger pax in room.PassenegerInfo)
                    {
                        string title = pax.Title;
                        string gender = string.Empty;
                        title = title.Replace(".", "");
                        if (title.ToLower() == "mr")
                        {
                            title = "Mr";
                            gender = "M";
                        }
                        else if (title.ToLower() == "ms")
                        {
                            title = "Ms";
                            gender = "F";
                        }
                        else if (title.ToLower() == "mrs")
                        {
                            title = "Mrs";
                            gender = "F";
                        }
                        else if (title.ToLower() == "master")
                        {
                            title = "Mstr";
                            gender = "M";
                        }
                        else if (title.ToLower() == "mst")
                        {
                            title = "Mst";
                            gender = "M";
                        }
                        else if (title.ToLower() == "miss")
                        {
                            title = "Miss";
                            gender = "F";
                        }
                        writer.WriteStartElement("Passenger");
                        writer.WriteStartElement("PaxNumber");
                        writer.WriteValue(index + 1);
                        writer.WriteEndElement();
                        writer.WriteStartElement("RoomNo");
                        writer.WriteValue(i + 1);
                        writer.WriteEndElement();
                        writer.WriteStartElement("Title");
                        writer.WriteValue(title);
                        writer.WriteEndElement();
                        writer.WriteStartElement("PassengerType");
                        writer.WriteValue(pax.PaxType == HotelPaxType.Adult ? "ADT" : "CHD");
                        writer.WriteEndElement();
                        if (pax.PaxType == HotelPaxType.Adult)
                        {
                            //writer.WriteStartElement("DateOfBirth");
                            //writer.WriteValue("19890121");
                            //writer.WriteEndElement();
                            writer.WriteStartElement("Age");
                            writer.WriteValue("28");
                            writer.WriteEndElement();
                        }
                        if (pax.PaxType == HotelPaxType.Child)
                        {
                            writer.WriteStartElement("Age");
                            writer.WriteValue(pax.Age);
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("FirstName");
                        writer.WriteValue(pax.Firstname);
                        writer.WriteEndElement();
                        writer.WriteStartElement("LastName");
                        writer.WriteValue(pax.Lastname);
                        writer.WriteEndElement();
                        writer.WriteStartElement("Nationality");
                        //writer.WriteValue(itinerary.PassengerNationality);
                        //writer.WriteValue("IN");
                        writer.WriteValue(Country.GetCountryCodeFromCountryName(itinerary.PassengerNationality));
                        writer.WriteEndElement();
                        writer.WriteStartElement("Gender");
                        writer.WriteValue(gender);
                        writer.WriteEndElement();

                        writer.WriteEndElement();//Passenger
                        index++;
                    }
                    //index++;

                }

                writer.WriteEndElement();//PassengerDetails

                writer.WriteStartElement("HotelDetails");
                writer.WriteStartElement("StartDate");
                writer.WriteValue(itinerary.StartDate.ToString("yyyyMMdd"));
                writer.WriteEndElement();
                writer.WriteStartElement("EndDate");
                writer.WriteValue(itinerary.EndDate.ToString("yyyyMMdd"));
                writer.WriteEndElement();
                writer.WriteStartElement("HotelCode");
                writer.WriteValue(itinerary.HotelCode);
                writer.WriteEndElement();
                writer.WriteStartElement("CityCode");
                writer.WriteValue(itinerary.CityCode);
                //writer.WriteValue("DXB");
                writer.WriteEndElement();
                writer.WriteStartElement("AgencyRef");
                //writer.WriteValue(itinerary.AgencyReference);
                //writer.WriteValue(itinerary.AgentRef);
                writer.WriteValue("HTL-CZ-" + Convert.ToString(itinerary.BookingId));
                writer.WriteEndElement();
                writer.WriteStartElement("RoomDetails");
                for (int b = 0; b < itinerary.Roomtype.Length; b++)
                {
                    CT.BookingEngine.HotelRoom room = itinerary.Roomtype[b];
                    writer.WriteStartElement("Room");

                    writer.WriteStartElement("RoomConfigurationId");
                    writer.WriteValue(room.RatePlanId);
                    writer.WriteEndElement();
                    writer.WriteStartElement("RoomTypeCode");
                    writer.WriteValue(room.RatePlanCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ContractTokenId");
                    writer.WriteValue(room.RatePlanCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[2]);
                    writer.WriteEndElement();
                    writer.WriteStartElement("MealPlanCode");
                    writer.WriteValue(room.RatePlanCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1]);
                    writer.WriteEndElement();
                    writer.WriteStartElement("Rate");
                    writer.WriteValue(room.Price.SupplierPrice);
                    //writer.WriteValue("875");
                    //writer.WriteValue("1019");
                    writer.WriteEndElement();

                    writer.WriteEndElement();//Room
                }
                writer.WriteEndElement();//RoomDetails
                writer.WriteEndElement();//HotelDetails

                writer.WriteEndElement();//HotelBookingRequest

                writer.Flush();
                writer.Close();
                #endregion

                request = request.Replace("&lt;", "<");
                request = request.Replace("&gt;", ">");

                try
                {
                    string filePath = XmlPath + "HotelBookingRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";

                    xmldoc.LoadXml(request.ToString());

                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to generate hotel booking request for " + itinerary.HotelName, ex);
            }


            return "data=" + HttpUtility.UrlEncode(request.ToString());

        }

        private BookingResponse ReadHotelBookingResponse(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {

            HotelSearchResult[] hotelResults = null;
            BookingResponse bookRes = new BookingResponse();
            bool statusCnf = true;
            string bookingStatus = "";
            string confirmationNo = "";
            string bookingId = "";
            string error = "";
            string RoomStatus = "";
            CT.BookingEngine.HotelRoom hRoom = new CT.BookingEngine.HotelRoom();
            //HotelItinerary hitinerary = new HotelItinerary();


            try
            {

                try
                {
                    string filePath = XmlPath + "HotelBookingResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDoc.Save(filePath);
                    //Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
                XmlNode errorNode = xmlDoc.SelectSingleNode("HotelBookingResponse/error");
                if (errorNode != null && errorNode.InnerText.Length > 0)
                {
                    throw new BookingEngineException(errorNode.InnerText);
                }
                else
                {

                    XmlNodeList roomNodes = xmlDoc.SelectNodes("/HotelBookingResponse/HotelDetails/RoomDetails/Room");
                    hotelResults = new HotelSearchResult[roomNodes.Count];
                    if (roomNodes != null)
                    {
                        foreach (XmlNode rnode in roomNodes)
                        {

                            foreach (var room in itinerary.Roomtype)
                            {
                                if (room.RoomTypeCode.Split('-')[3] == rnode.SelectSingleNode("RoomNo").InnerText)
                                {
                                    room.RoomwiseConfirmNo = rnode.SelectSingleNode("SubResNo").InnerText;
                                    if (rnode.SelectSingleNode("RoomStatus").InnerText == "OK")
                                    {
                                        room.RoomwiseConfirmStatus = "Confirmed";
                                    }
                                    else if (rnode.SelectSingleNode("RoomStatus").InnerText == "RQ")
                                    {
                                        room.RoomwiseConfirmStatus = "On Request";
                                    }
                                    else if (rnode.SelectSingleNode("RoomStatus").InnerText == "XX")
                                    {
                                        room.RoomwiseConfirmStatus = "Not Available";
                                    }
                                    //else if (rnode.SelectSingleNode("RoomStatus").InnerText == "CX")
                                    //{
                                    //    RoomStatus = "Cancelled";
                                    //}
                                    string flag = "Confirm";
                                    hRoom.UpdateitineraryRoomStatus(itinerary.HotelId, room.RoomId, room.RoomwiseConfirmNo, room.RoomwiseConfirmStatus, flag);
                                    
                                    confirmationNo = xmlDoc.SelectSingleNode("HotelBookingResponse/BookingDetails/BookingNumber").InnerText;
                                    if (bookingId.Length > 0)
                                    {
                                        bookingId += "|" + rnode.SelectSingleNode("SubResNo").InnerText;
                                    }
                                    else
                                    {
                                        bookingId = rnode.SelectSingleNode("SubResNo").InnerText;
                                    }

                                }

                            }

                        }

                    }

                    foreach (var room in itinerary.Roomtype)
                    {
                        if (room.RoomwiseConfirmStatus != "Confirmed")
                        {
                            statusCnf = false;
                            if (error.Length > 0)
                            {
                                error += "|" + room.RoomwiseConfirmNo + " status is " + room.RoomwiseConfirmStatus;
                            }
                            else
                            {
                                error = room.RoomwiseConfirmNo + " status is " + room.RoomwiseConfirmStatus;
                            }
                        }
                    }


                    if (statusCnf = true && confirmationNo != "")
                    {
                        bookingStatus = "Confirmed";
                        itinerary.ConfirmationNo = confirmationNo;
                        bookRes.ConfirmationNo = confirmationNo;
                        itinerary.BookingRefNo = bookingId;
                        if (bookingStatus == "Confirmed")
                        {
                            bookRes.Status = BookingResponseStatus.Successful;
                            itinerary.Status = HotelBookingStatus.Confirmed;
                            bookRes.Error = "";

                            itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: Illusions, as per final booking form reference No: " + confirmationNo;
                        }
                        else
                        {
                            bookRes.Status = BookingResponseStatus.Failed;
                            if (bookingStatus != "Confirmed")
                            {
                                bookRes.Error = "Error from Illusions:Booking Failed";
                            }
                            else
                            {
                                bookRes.Error = bookingStatus;
                            }
                        }
                    }
                    else
                    {
                        bookRes.Status = BookingResponseStatus.PartiallyConfirmed;

                        bookRes.Error = error;
                    }
                    if (bookRes.Status == BookingResponseStatus.Failed && bookRes.Error.Length > 0)
                    {
                        throw new BookingEngineException(bookRes.Error);
                    }

                }
            }
            catch (Exception ex)
            {
                bookRes.Error = "Error from Illusions: " + ex.Message;
                bookRes.Status = BookingResponseStatus.Failed;
                throw new Exception("Booking failed reason :" + ex.Message, ex);
            }

            return bookRes;
        }


        public Dictionary<string, string> CancelHotelBooking(string source, string hotelBookingNo, string SubResNo)
        {
            Dictionary<string, string> cancelCharges = new Dictionary<string, string>();

            XmlDocument responseXML = GetHotelCancellationResponse(source, hotelBookingNo, SubResNo);

            if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("CancelHotelBookingResponse"))
            {
                cancelCharges = ReadHotelCancellationResponse(responseXML, SubResNo);
            }

            return cancelCharges;
        }

        private XmlDocument GetHotelCancellationResponse(string source, string hotelBookingNo, string SubResNo)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.IllusionsHotelConfig["ServiceUrl"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelCancellationRequest(source, hotelBookingNo, SubResNo);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;

                Stream dataStream = GetStreamForResponse(httpResponse);
                using (XmlReader reader = XmlReader.Create(new StreamReader(dataStream, Encoding.UTF8)))
                {
                    xmlDoc.Load(reader);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to get response for Hotel cancel request for HotelId " + SubResNo, ex);
            }
            return xmlDoc;
        }

        private string GenerateHotelCancellationRequest(string source, string hotelBookingNo, string SubResNo)
        {
            StringBuilder request = new StringBuilder();
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                #region XML Code
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteStartElement("CancelHotelBookingRequest");
                writer.WriteStartElement("Profile");
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteStartElement("Code");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("TokenNumber");
                writer.WriteValue(_token);
                writer.WriteEndElement();
                writer.WriteEndElement();//Profile

                writer.WriteStartElement("BookingDetails");
                writer.WriteStartElement("Source");
                writer.WriteValue(source);
                writer.WriteEndElement();
                writer.WriteStartElement("BookingNumber");
                writer.WriteValue(hotelBookingNo);
                writer.WriteEndElement();
                writer.WriteStartElement("SubResNo");
                writer.WriteValue(SubResNo);
                writer.WriteEndElement();

                writer.WriteEndElement();//BookingDetails

                writer.WriteEndElement();//CancelHotelBookingRequest

                writer.Flush();
                writer.Close();
                #endregion



                try
                {
                    string filePath = XmlPath + "BookingCancelRequest_" + sessionId + "_" + SubResNo.Split('/')[0] + "_" + SubResNo.Split('/')[1] + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";

                    xmldoc.LoadXml(request.ToString());

                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to generate hotel booking request for " + SubResNo, ex);
            }

            return "data=" + HttpUtility.UrlEncode(request.ToString());

        }

        private Dictionary<string, string> ReadHotelCancellationResponse(XmlDocument xmldoc, string SubResNo)
        {
            Dictionary<string, string> cancelData = new Dictionary<string, string>();
            try
            {

                try
                {
                    string filePath = XmlPath + "BookingCancelResponse_" + sessionId + "_" + SubResNo.Split('/')[0] + "_" + SubResNo.Split('/')[1] + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.CancellBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNode error = xmldoc.SelectSingleNode("/CancelHotelBookingResponse/ErrorMessage/Error/Errors/Msg");
                XmlNode totalAmount = xmldoc.SelectSingleNode("/CancelHotelBookingResponse/TotalAmount");
                XmlNode hotelCurrency = xmldoc.SelectSingleNode("/CancelHotelBookingResponse/Currency");
                XmlNode hotelbookingNo = xmldoc.SelectSingleNode("/CancelHotelBookingResponse/BookingDetails/BookingNumber");
                XmlNode hotelsubResNo = xmldoc.SelectSingleNode("/CancelHotelBookingResponse/BookingDetails/SubResNo");
                XmlNode hotelsource = xmldoc.SelectSingleNode("/CancelHotelBookingResponse/BookingDetails/Source");


                if (error != null && error.InnerText.Length > 0)
                {
                    cancelData.Add("Error", error.InnerText);
                }
                else
                {
                    if (hotelbookingNo != null && hotelsubResNo != null)
                    {
                        //cancelData = RetriveHotelBookingRquest(hotelsource.InnerText, hotelbookingNo.InnerText, hotelsubResNo.InnerText);
                        if (totalAmount != null)
                        {
                            cancelData.Add("Amount", totalAmount.InnerText);
                        }
                        else
                        {
                            cancelData.Add("Amount", "0");
                        }
                        //if (hotelCurrency != null)
                        if (hotelCurrency.InnerText != null && hotelCurrency.InnerText != "")
                        {
                            cancelData.Add("Currency", hotelCurrency.InnerText);
                            //cancelData.Add("Currency", "AED");
                        }
                        else
                        {
                            cancelData.Add("Currency", "AED");
                        }
                        if (hotelsubResNo != null)
                        {
                            cancelData.Add("ID", hotelsubResNo.InnerText);
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                cancelData.Add("Error", ex.Message);
                throw new Exception("Failed to read Hotel cancellation response", ex);
            }

            return cancelData;
        }
        public Dictionary<string, string> RetriveHotelBookingRquest(string[] bookingKeys)
        {
            Dictionary<string, string> cancelCharges = new Dictionary<string, string>();

            XmlDocument responseXML = GetHotelRetriveBookingResponse(bookingKeys[0].Split('|')[0], bookingKeys[0].Split('|')[1]);

            if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("RetrieveHotelBookingResponse"))
            {
                cancelCharges = ReadHotelRetrivebookingResponse(responseXML, bookingKeys);
            }

            return cancelCharges;
        }
        //public Dictionary<string, string> RetriveHotelBookingRquest(HotelItinerary itinerary)
        //{
        //    Dictionary<string, string> cancelCharges = new Dictionary<string, string>();

        //    XmlDocument responseXML = GetHotelRetriveBookingResponse(itinerary.HotelCode.Split('-')[0], itinerary.ConfirmationNo);

        //    if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("RetrieveHotelBookingResponse"))
        //    {
        //        cancelCharges = ReadHotelRetrivebookingResponse(responseXML, bookingKeys);
        //    }

        //    return cancelCharges;
        //}
        private XmlDocument GetHotelRetriveBookingResponse(string source, string hotelBookingNo)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.IllusionsHotelConfig["ServiceUrl"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelRetriveBookingRequest(source, hotelBookingNo);// GETTING XML STRING...

                request.Method = "POST";
                request.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                request.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = request.GetResponse();

                //Response.Write(((HttpWebResponse)response).StatusDescription + "" + System.Environment.NewLine);
                HttpWebResponse httpResponse = response as HttpWebResponse;

                Stream dataStream = GetStreamForResponse(httpResponse);
                using (XmlReader reader = XmlReader.Create(new StreamReader(dataStream, Encoding.UTF8)))
                {
                    xmlDoc.Load(reader);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Illusions: Failed to get response for Hotel cancel request for HotelId " + hotelBookingNo, ex);
            }
            return xmlDoc;
        }

        private string GenerateHotelRetriveBookingRequest(string source, string hotelBookingNo)
        {
            StringBuilder request = new StringBuilder();
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                #region XML Code
                XmlWriter writer = XmlWriter.Create(request);
                writer.WriteStartElement("RetrieveHotelBookingRequest");
                writer.WriteStartElement("Profile");
                writer.WriteStartElement("Password");
                writer.WriteValue(_password);
                writer.WriteEndElement();
                writer.WriteStartElement("Code");
                writer.WriteValue(_username);
                writer.WriteEndElement();
                writer.WriteStartElement("TokenNumber");
                writer.WriteValue(_token);
                writer.WriteEndElement();
                writer.WriteEndElement();//Profile

                writer.WriteStartElement("BookingDetails");
                writer.WriteStartElement("Source");
                writer.WriteValue(source);
                writer.WriteEndElement();
                writer.WriteStartElement("BookingNumber");
                writer.WriteValue(hotelBookingNo);
                writer.WriteEndElement();

                writer.WriteEndElement();//BookingDetails

                writer.WriteEndElement();//RetrieveHotelBookingRequest

                writer.Flush();
                writer.Close();
                #endregion



                try
                {
                    string filePath = XmlPath + "BookingRetriveRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";

                    xmldoc.LoadXml(request.ToString());

                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to generate hotel booking request for " + hotelBookingNo, ex);
            }


            return "data=" + HttpUtility.UrlEncode(request.ToString());

        }

        private Dictionary<string, string> ReadHotelRetrivebookingResponse(XmlDocument xmldoc, string[] bookingKeys)
        {
            Dictionary<string, string> cancelData = new Dictionary<string, string>();
            HotelSearchResult[] hotelResults = null;
            bool statusCnf = true;
            string bookingStatus = "";

            string errmsg = "";
            string RoomStatus = "";
            CT.BookingEngine.HotelRoom hRoom = new CT.BookingEngine.HotelRoom();
            HotelItinerary hitinerary = new HotelItinerary();
            try
            {

                try
                {
                    string filePath = XmlPath + "BookingRetriveResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmldoc.Save(filePath);
                    //Audit.Add(EventType.CancellBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                XmlNode error = xmldoc.SelectSingleNode("/RetrieveHotelBookingResponse/ErrorMessage/Error/Errors/Msg");
                XmlNode hotelbookingNo = xmldoc.SelectSingleNode("/RetrieveHotelBookingResponse/BookingDetails/BookingNumber");
                XmlNode hotelbookingRate = xmldoc.SelectSingleNode("/RetrieveHotelBookingResponse/BookingDetails/BookingTotalRate");
                XmlNode hotelbookingStatus = xmldoc.SelectSingleNode("/RetrieveHotelBookingResponse/BookingDetails/BookingStatus");
                XmlNode hotelsource = xmldoc.SelectSingleNode("/RetrieveHotelBookingResponse/BookingDetails/Source");
                XmlNode hotelcurrency = xmldoc.SelectSingleNode("/RetrieveHotelBookingResponse/BookingDetails/Currency");


                if (error != null && error.InnerText.Length > 0)
                {
                    cancelData.Add("Error", error.InnerText);
                }
                else
                {
                    XmlNodeList roomNodes = xmldoc.SelectNodes("/RetrieveHotelBookingResponse/HotelDetails/RoomDetails/Room");
                    hotelResults = new HotelSearchResult[roomNodes.Count];
                    if (roomNodes != null)
                    {
                        foreach (XmlNode rnode in roomNodes)
                        {
                            foreach (var room in bookingKeys[2].Split('|'))
                            {
                                if (room.Split('-')[1] == rnode.SelectSingleNode("RoomNo").InnerText)
                                {
                                    int hotelid =Convert.ToInt32(bookingKeys[3]);
                                    int roomid = Convert.ToInt32(room.Split('-')[0]);
                                    string SubResNo = rnode.SelectSingleNode("SubResNo").InnerText;
                                    if (rnode.SelectSingleNode("RoomStatus").InnerText == "OK")
                                    {
                                        RoomStatus = "Available";
                                    }
                                    else if (rnode.SelectSingleNode("RoomStatus").InnerText == "RQ")
                                    {
                                        RoomStatus = "On Request";
                                    }
                                    else if (rnode.SelectSingleNode("RoomStatus").InnerText == "XX")
                                    {
                                        RoomStatus = HotelBookingStatus.Cancelled.ToString();
                                    }

                                    string flag = "Cancel";
                                    hRoom.UpdateitineraryRoomStatus(hotelid, roomid, SubResNo, RoomStatus, flag);


                                }
                            }
                        }


                    }

                    hitinerary.Roomtype = hRoom.Load(Convert.ToInt32(bookingKeys[3]));


                    foreach (var room in hitinerary.Roomtype)
                    {
                        if (room.RoomwiseCancelStatus != HotelBookingStatus.Cancelled.ToString() && hotelbookingStatus.InnerText == HotelBookingStatus.Cancelled.ToString())
                        {
                            statusCnf = false;
                            if (errmsg.Length > 0)
                            {
                                errmsg += "|" + room.RoomwiseConfirmNo + " status is " + room.RoomwiseCancelStatus;
                            }
                            else
                            {
                                errmsg = room.RoomwiseConfirmNo + " status is " + room.RoomwiseCancelStatus;
                            }
                        }
                    }

                    if (hotelbookingNo != null)
                    {
                        if (hotelbookingRate != null)
                        {
                            cancelData.Add("Amount", hotelbookingRate.InnerText);
                        }
                        else
                        {
                            cancelData.Add("Amount", "0");
                        }

                        if (hotelcurrency.InnerText != null && hotelcurrency.InnerText != "")
                        {
                            cancelData.Add("Currency", hotelcurrency.InnerText);
                        }
                        else
                        {
                            cancelData.Add("Currency", "AED");
                            //cancelData.Add("Currency", itinerary.Currency);
                        }

                        cancelData.Add("ID", hotelbookingNo.InnerText);
                        if (statusCnf == true)
                        {
                            cancelData.Add("Status", HotelBookingStatus.Cancelled.ToString());
                        }
                        else
                        {
                            cancelData.Add("Status", HotelBookingStatus.PartiallyCancelled.ToString());
                            cancelData.Add("Message", errmsg);
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                cancelData.Add("Error", ex.Message);
                throw new Exception("Failed to read Retrive Booking Hotel response", ex);
            }

            return cancelData;
        }

        public void GetHotelStaticData(HotelRequest request)
        {


            XmlDocument responseXML = GetHotelStaticDataResponse(request);
            if (responseXML != null && responseXML.ChildNodes != null && responseXML.ChildNodes.Count > 0 && responseXML.OuterXml.Contains("MasterData"))
            {
                ReadHotelBookingResponse(responseXML);
            }

        }

        private XmlDocument GetHotelStaticDataResponse(HotelRequest request)
        {
            string cityCode = request.CityCode;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                const string contentType = "application/x-www-form-urlencoded";
                HttpWebRequest staticrequest = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.IllusionsHotelConfig["ServiceUrl"]);
                staticrequest.Method = "POST"; //Using POST method       
                //string postData = GenerateStaticDataRequest(cityCode);// GETTING XML STRING...
                string postData = GenerateHotelListRequest();// GETTING XML STRING...

                staticrequest.Method = "POST";
                staticrequest.ContentType = contentType;

                // request for compressed response. This does not seem to have any effect now.
                staticrequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                staticrequest.ContentLength = postData.Length;
                StreamWriter requestWriter = new StreamWriter(staticrequest.GetRequestStream());
                staticrequest.ContentType = contentType;
                requestWriter.Write(postData);
                requestWriter.Close();

                WebResponse response = staticrequest.GetResponse();

                HttpWebResponse httpResponse = response as HttpWebResponse;
                Stream dataStream = GetStreamForResponse(httpResponse);
                using (XmlReader reader = XmlReader.Create(new StreamReader(dataStream, Encoding.UTF8)))
                {
                    xmlDoc.Load(reader);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.IllusionsSearch, Severity.Normal, 1, "Illusions.GenerateHotelSearchResult  Err :" + ex.ToString(), "0");
                throw ex;
            }

            return xmlDoc;
        }

        private string GenerateHotelListRequest()
        {
            Guid guid = Guid.NewGuid();
            _token = guid.ToString();
            StringBuilder request = new StringBuilder();
            XmlWriter writer = XmlWriter.Create(request);
            writer.WriteStartElement("HotelListRequest");
            writer.WriteStartElement("Profile");
            writer.WriteElementString("Password", "C0zmOTr@v3Ll");
            writer.WriteElementString("Code", "C0zmo2020");
            writer.WriteElementString("TokenNumber", _token);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                string filePath = XmlPath + "HotelListRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                xmldoc.LoadXml(request.ToString());

                xmldoc.Save(filePath);

            }
            catch
            {

            }

            return "data=" + HttpUtility.UrlEncode(request.ToString());
        }
        //private string GenerateStaticDataRequest(string cityCode)
        private string GenerateStaticDataRequest(HotelRequest hrequest)
        {
            StringBuilder request = new StringBuilder();
            hotelid.Sort();

            string illusionhotelcode = string.Join(",", hotelid);

            XmlWriter writer = XmlWriter.Create(request);
            writer.WriteStartElement("RetrieveMasterData");
            writer.WriteStartElement("Profile");
            writer.WriteStartElement("Password");
            writer.WriteValue(_password);
            writer.WriteEndElement();
            writer.WriteStartElement("Code");
            writer.WriteValue(_username);
            writer.WriteEndElement();
            writer.WriteStartElement("TokenNumber");
            writer.WriteValue(_token);
            writer.WriteEndElement();
            writer.WriteEndElement();//Profile

            writer.WriteStartElement("MasterData");

            writer.WriteStartElement("Geo");
            writer.WriteStartElement("Country");
            writer.WriteValue(hrequest.CountryName);
            writer.WriteEndElement();
            writer.WriteStartElement("ISOCountryCode");
            writer.WriteValue(hrequest.CountryCode);
            writer.WriteEndElement();
            writer.WriteStartElement("City");
            writer.WriteValue(hrequest.CityCode);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("HotelDetail");
            writer.WriteValue("Y");
            writer.WriteEndElement();
            writer.WriteStartElement("HotelFacilities");
            writer.WriteValue("Y");
            writer.WriteEndElement();
            writer.WriteStartElement("HotelMessages");
            writer.WriteValue("N");
            writer.WriteEndElement();
            writer.WriteEndElement();//MasterData

            writer.WriteEndElement();//RetrieveMasterData

            writer.Flush();
            writer.Close();
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                string filePath = XmlPath + "HotelStaticdataRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                xmldoc.LoadXml(request.ToString());

                xmldoc.Save(filePath);

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to generate hotel Static data request for " + hrequest.CityCode, ex);
            }

            return "data=" + HttpUtility.UrlEncode(request.ToString());
        }

        //private BookingResponse ReadHotelBookingResponse(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        private void ReadHotelBookingResponse(XmlDocument xmlDoc)
        {


            List<HotelStaticData> StaticData;
            HotelStaticData staticInfo;

            try
            {

                try
                {
                    string filePath = XmlPath + "HotelStaticDataResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDoc.Save(filePath);
                    //Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
                XmlNode errorNode = xmlDoc.SelectSingleNode("MasterData/error");
                if (errorNode != null && errorNode.InnerText.Length > 0)
                {
                    throw new BookingEngineException(errorNode.InnerText);
                }
                else
                {
                    XmlNode hcityName = xmlDoc.SelectSingleNode("/MasterData/MasterDataDetails/City");
                    XmlNodeList sdataNodes = xmlDoc.SelectNodes("/MasterData/MasterDataDetails/Hotel");
                    StaticData = new List<HotelStaticData>();
                    if (sdataNodes != null)
                    {
                        foreach (XmlNode snode in sdataNodes)
                        {
                            staticInfo = new HotelStaticData();

                            staticInfo.CityCode = snode.SelectSingleNode("CityCode").InnerText;
                            staticInfo.HotelName = snode.SelectSingleNode("HotelName").InnerText;
                            staticInfo.HotelCode = snode.SelectSingleNode("HotelCode").InnerText;
                            staticInfo.HotelLocation = hcityName.InnerText;

                            XmlNodeList AddressNodes = snode.SelectNodes("ContactDetails");
                            foreach (XmlNode anode in AddressNodes)
                            {
                                staticInfo.HotelAddress = anode.SelectSingleNode("ContactAddress").InnerText;
                                staticInfo.URL = anode.SelectSingleNode("Website").InnerText;
                                staticInfo.PhoneNumber = anode.SelectSingleNode("ContactPhone").InnerText;
                            }
                            XmlNodeList geoNodes = snode.SelectNodes("GeoLocation");
                            foreach (XmlNode gnode in geoNodes)
                            {
                                XmlNode longitude = gnode.SelectSingleNode("Longitude");
                                XmlNode latitude = gnode.SelectSingleNode("Latitude");
                                if (longitude != null && latitude != null)
                                {
                                    staticInfo.HotelMap = latitude.InnerText + "|" + longitude.InnerText;
                                }

                            }
                            XmlNodeList htlfacilityNodes = snode.SelectNodes("HotelFacilities/Facility");
                            if (htlfacilityNodes != null)
                            {
                                foreach (XmlNode hfnode in htlfacilityNodes)
                                {
                                    staticInfo.HotelFacilities += hfnode.InnerText + "|";

                                }
                            }
                            XmlNodeList descriptionNodes = snode.SelectNodes("DescriptionList/Description");
                            if (descriptionNodes != null)
                            {
                                foreach (XmlNode descripnode in descriptionNodes)
                                {
                                    staticInfo.HotelDescription += descripnode.InnerText + "|";

                                }
                            }
                            XmlNodeList imagesNodes = snode.SelectNodes("Images/Img/ImageLocation");
                            if (imagesNodes != null)
                            {
                                foreach (XmlNode imgnode in imagesNodes)
                                {
                                    staticInfo.HotelPicture += imgnode.InnerText + "|";

                                }
                            }

                            staticInfo.IllusionsSave();
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Static data Reading  failed reason :" + ex.Message, ex);
            }

        }



        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~IllusionsApi()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

    }
}
