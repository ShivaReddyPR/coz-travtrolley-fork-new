﻿using System;
using System.Collections.Generic;
using CT.Configuration;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Xml;
using Newtonsoft.Json;
using CT.Core;
using CT.BookingEngine;

namespace TBOHotel
{
    /// <summary>
    /// This class is used to interact with TBOApi
    /// HotelBookingSource Value for TBOHotel=14
    /// Following is the steps in Booking Flow for this source
    /// STEP-1:Authentication(Each and Every Request Need to call Authenticate)
    /// STEP-2:HotelSearch(Availability)
    /// STEP-3:Every 15days(what we configuraed in App.config) Need to download StaticData
    /// STEP-4:Loading Cancellation policy
    /// STEP-5:PreBooking(RePrice)
    /// STEP-6:Booking
    /// STEP-7:Cancel
    /// STEP-8:Cancel(Get cancel Status)
    /// </summary>
    public class HotelV10:IDisposable
    {
        #region variables
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        protected string XmlPath = string.Empty;
        /// <summary>
        /// loginName will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string loginName = string.Empty;
        /// <summary>
        /// password will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string password = string.Empty;
        /// <summary>
        /// clientId will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string clientId = string.Empty;
        /// <summary>
        /// loginType will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string loginType = string.Empty;
        /// <summary>
        /// userIP will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string userIP = string.Empty;
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        decimal rateOfExchange = 1;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        string agentCurrency = "";
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        int decimalPoint;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        string sessionId;
        /// <summary>
        /// This variable is used,to store the login userid,which is read from MetaserchEngine  
        /// </summary>
        int appUserId;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        #endregion
        #region properities
        /// <summary>
        /// For storing current agent Base Currency against which conversion need to be applied.
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Exchange rates against agent base currency.
        /// </summary>
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }
        /// <summary>
        ///login UserId
        /// </summary>
        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        #endregion

        #region constructor
        /// <summary>
        /// Constrcutor for TBOHotel
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        public HotelV10()
        {
            loginName = ConfigurationSystem.TBOHotelConfig["UserName"];
            password = ConfigurationSystem.TBOHotelConfig["Password"];
            XmlPath = ConfigurationSystem.TBOHotelConfig["XmlLogPath"];
            loginType = ConfigurationSystem.TBOHotelConfig["LoginType"];
            clientId = ConfigurationSystem.TBOHotelConfig["ClientId"];
            userIP = ConfigurationSystem.TBOHotelConfig["UserIp"];
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }
        #endregion

        #region common Methods
        /// <summary>
        /// Used to send the request XML to the server and pulls the response XML.
        /// </summary>
        /// <param name="requestData">RequestData</param>
        /// <param name="url">Request Url</param>
        /// <returns>string</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private string GetResponse(string requestData, string url)
        {
            string responseFromServer = string.Empty;
            string responseXML = string.Empty;
            try
            {
                string contentType = "application/json";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST"; //Using POST method       
                string postData = requestData;// GETTING JSON STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
            }
            catch (WebException webEx)
            {
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
            }
            return responseFromServer;
        }

        //convert compressed
        /// <summary>
        /// This method is converting ContentEncoding to Decompress(i.e GZIP, Deflate, default)
        /// </summary>
        /// <param name="webResponse">Response Object</param>
        /// <returns>Stream</returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        #endregion

        #region Authentication
        /// <summary>
        ///  This private function is used for Authenticate.
        /// </summary>
        /// <returns>AuthenticationResponse</returns>
        /// <remarks>TokenId(Getting Authentication Response) Need to send EveryRequest</remarks>
        private AuthenticationResponse Authenticate()
        {
            AuthenticationRequest authRequest = new AuthenticationRequest();
            AuthenticationResponse authResponse = new AuthenticationResponse();
            try
            {
                authRequest.ClientId = clientId;
                authRequest.UserName = loginName;
                authRequest.Password = password;
                authRequest.LoginType = loginType;
                authRequest.EndUserIp = userIP;

                string request = JsonConvert.SerializeObject(authRequest);
                // To convert JSON text contained in string json into an XML node
                XmlDocument xmlDocReq = JsonConvert.DeserializeXmlNode(request, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelAuthenticateRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocReq.Save(filePath);
                        //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelAuthenticateRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(request.ToString());
                        sw.Close();

                    }
                }
                catch { }
                

                string response = GetResponse(request, ConfigurationSystem.TBOHotelConfig["Aunthenticate"]);
                try
                {
                    // To convert JSON text contained in string json into an XML node
                    XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelAuthenticateResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocRes.Save(filePath);
                        //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelAuthenticateResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(response.ToString());
                        sw.Close();


                    }
                }
                catch { }
                //To convert JSON text to object
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.ObjectCreationHandling = ObjectCreationHandling.Auto;
                settings.StringEscapeHandling = StringEscapeHandling.Default;
                settings.Formatting = Newtonsoft.Json.Formatting.Indented;

                TextReader reader = new StringReader(response);
                authResponse = (AuthenticationResponse)JsonSerializer.Create(settings).Deserialize(reader, typeof(AuthenticationResponse));

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOHotel, Severity.High, appUserId, "(TBOHotel)Failed to Authenticate. Reason : " + ex.ToString(), System.Web.HttpContext.Current.Request["REMOTE_ADDR"]);
            }
            return authResponse;
        }
        /// <summary>
        /// AuthenticationRequest
        /// </summary>
        private class AuthenticationRequest
        {
            /// <summary>
            /// ClientId
            /// </summary>
            public string ClientId;
            /// <summary>
            /// LoginType
            /// </summary>
            public string LoginType;
            /// <summary>
            /// UserName
            /// </summary>
            public string UserName;
            /// <summary>
            /// Password
            /// </summary>
            public string Password;
            /// <summary>
            /// EndUserIp
            /// </summary>
            public string EndUserIp;
        }

        /// <summary>
        /// AuthenticationResponse
        /// </summary>
        private class AuthenticationResponse
        {
            /// <summary>
            /// Status
            /// </summary>
            public string Status;
            /// <summary>
            /// TokenId
            /// </summary>
            public string TokenId;
            /// <summary>
            /// Error
            /// </summary>
            public Error Error;
            /// <summary>
            /// Member
            /// </summary>
            public Member Member;
        }
        /// <summary>
        /// Member
        /// </summary>
        private struct Member
        {
            /// <summary>
            /// FirstName
            /// </summary>
            public string FirstName;
            /// <summary>
            /// LastName
            /// </summary>
            public string LastName;
            /// <summary>
            /// Email
            /// </summary>
            public string Email;
            /// <summary>
            /// MemberId
            /// </summary>
            public int MemberId;
            /// <summary>
            /// AgencyId
            /// </summary>
            public int AgencyId;
            /// <summary>
            /// LoginName
            /// </summary>
            public string LoginName;
            /// <summary>
            /// LoginDetails
            /// </summary>
            public string LoginDetails;
            /// <summary>
            /// IsPrimaryAgent
            /// </summary>
            public bool IsPrimaryAgent;
        }
        /// <summary>
        /// Error
        /// </summary>
        private struct Error
        {
            /// <summary>
            /// ErrorCode
            /// </summary>
            public string ErrorCode;
            /// <summary>
            /// ErrorMessage
            /// </summary>
            public string ErrorMessage;
        }

        #endregion

        #region  HotelSearch
        /// <summary>
        /// Get the Hotels for the destination Search criteria.
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">B2B is MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public HotelSearchResult[] GetHotelSearch(HotelRequest req, decimal markup, string markupType)
        {
            HotelSearchResult[] searchRes = new HotelSearchResult[0];
            AuthenticationResponse autheRes = Authenticate();
            if (autheRes != null)
            {
                if (autheRes.Error.ErrorMessage != null)
                {
                    string request = GenerateHotelSerchRequest(req, autheRes);
                    //Audit.Add(EventType.TBOHotelAvailSearch, Severity.Normal, 1, "TBOHotel request message generated", "0");
                    string resp = string.Empty;
                    try
                    {
                        resp = GetResponse(request, ConfigurationSystem.TBOHotelConfig["HotelSearch"]);

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOHotelAvailSearch, Severity.High, 0, "Exception returned from TBOHotel.GetHotelSearch Error Message:" + ex.Message + " | " + DateTime.Now + "| request JSON" + request + "|response JSON" + resp, "");
                        throw new BookingEngineException("Error: " + ex.Message);
                    }
                    try
                    {
                        searchRes = ReadSearchResultResponse(resp, req, markup, markupType);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOHotelAvailSearch, Severity.High, 0, "Exception returned from TBOHotels.GetHotelSearch Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request JSON" + request + "|response JSON" + resp, "");
                        throw new BookingEngineException("Error: " + ex.ToString());
                    }

                }
            }
            return searchRes;
        }
        /// <summary>
        /// Used in preparing json for the request.
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="autheRes">AuthenticationResponse</param>
        /// <returns>string</returns>
        private string GenerateHotelSerchRequest(HotelRequest req, AuthenticationResponse autheRes)
        {
            SearchRequest seaRequest = new SearchRequest();
            seaRequest.TokenId = autheRes.TokenId;
            seaRequest.EndUserIp = userIP;
            seaRequest.BookingMode = 5;
            seaRequest.CheckInDate = req.StartDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            seaRequest.NoOfNights = req.EndDate.Subtract(req.StartDate).Days;
            seaRequest.CountryCode = req.CountryName;
            seaRequest.CityId = req.CityCode;
            seaRequest.PreferredCurrency = "INR";
            seaRequest.GuestNationality = req.PassengerNationality;
            seaRequest.NoOfRooms = req.NoOfRooms;
            RoomGuest[] roomGuest = new RoomGuest[req.NoOfRooms];
            for (int i = 0; i < req.RoomGuest.Length; i++)
            {
                roomGuest[i].NoOfAdults = req.RoomGuest[i].noOfAdults;
                roomGuest[i].NoOfChild = req.RoomGuest[i].noOfChild;
                if (req.RoomGuest[i].noOfChild > 0)
                {
                    roomGuest[i].ChildAge = req.RoomGuest[i].childAge;
                }
            }
            seaRequest.RoomGuests = roomGuest;
            seaRequest.MaxRating = 5;
            seaRequest.MinRating = 0;
            string request = JsonConvert.SerializeObject(seaRequest);
            // To convert JSON text contained in string json into an XML node
            XmlDocument xmlDocReq = JsonConvert.DeserializeXmlNode(request, "Root");
            try
            {
                if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                {
                    string filePath = XmlPath + "TBOHotelSearchRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDocReq.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                    //JsonFormat
                    string filePathJson = XmlPath + "TBOHotelSearchRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(request.ToString());
                    sw.Close();

                }
            }
            catch { }
            return request;

        }
        /// <summary>
        ///  Used in reading response Json and assigning hotel data to the response.
        /// </summary>
        /// <param name="response">Json Response Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here only we are calucating B2B markup and InputVat
        ///   Here source Currency means supplierCurrency and source amount means supplier Amount
        /// </remarks>
        private HotelSearchResult[] ReadSearchResultResponse(string response, HotelRequest request, decimal markup, string markupType)
        {
            XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
            try
            {
                if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                {
                    string filePath = XmlPath + "TBOHotelSearchResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    xmlDocRes.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                    //JsonFormat
                    string filePathJson = XmlPath + "TBOHotelSearchResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                    StreamWriter sw = new StreamWriter(filePathJson);
                    sw.Write(response.ToString());
                    sw.Close();
                }

            }
            catch { }
            XmlNode errorInfo = xmlDocRes.SelectSingleNode("/Root/HotelSearchResult/Error/ErrorMessage");
            //checking error info
            if (errorInfo != null && errorInfo.InnerText.Length > 0)
            {
                Audit.Add(EventType.TBOHotelAvailSearch, Severity.High, 0, " TBOHotels:ReadSearchResultResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response JSON" + response, "");
                throw new BookingEngineException("<br>" + errorInfo.InnerText);
            }
            PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
            XmlNodeList hotelList = xmlDocRes.SelectNodes("/Root/HotelSearchResult/HotelResults");
            HotelSearchResult[] hotelResults = new HotelSearchResult[hotelList.Count];
            string pattern = request.HotelName;
            int i = 0;
            if (hotelList != null)
            {
                foreach (XmlNode node in hotelList)
                {
                    HotelSearchResult hotelResult = new HotelSearchResult();
                    //in PropertyType saving  TBO Session id means TraceId
                    //in SequenceNumber saving TBOHotels  ResltIndex
                    hotelResult.StartDate = request.StartDate;
                    hotelResult.EndDate = request.EndDate;
                    hotelResult.PropertyType = xmlDocRes.SelectSingleNode("/Root/HotelSearchResult/TraceId").InnerText;
                    hotelResult.SequenceNumber = node.SelectSingleNode("ResultIndex").InnerText;
                    hotelResult.HotelCode = node.SelectSingleNode("HotelCode").InnerText;
                    hotelResult.HotelName = node.SelectSingleNode("HotelName").InnerText;
                    hotelResult.HotelName = hotelResult.HotelName.Replace("&apos;", "'");
                    hotelResult.Rating = (HotelRating)Convert.ToInt32(node.SelectSingleNode("StarRating").InnerText);
                    hotelResult.HotelCategory = node.SelectSingleNode("HotelCategory").InnerText;
                    hotelResult.HotelDescription = node.SelectSingleNode("HotelDescription").InnerText;
                    hotelResult.PromoMessage = node.SelectSingleNode("HotelPromotion").InnerText;
                    hotelResult.HotelPicture = node.SelectSingleNode("HotelPicture").InnerText;
                    hotelResult.HotelAddress = node.SelectSingleNode("HotelAddress").InnerText;
                    hotelResult.HotelContactNo = node.SelectSingleNode("HotelContactNo").InnerText;
                    hotelResult.HotelLocation = node.SelectSingleNode("HotelLocation").InnerText;
                    hotelResult.BookingSource = HotelBookingSource.TBOHotel;
                    hotelResult.RoomGuest = request.RoomGuest; //Added on 13072016
                    #region To get only those satisfy the search conditions
                    if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                    {
                        if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    #endregion
                    hotelResult.CityCode = request.CityCode;
                    #region Price calu
                    hotelResult.Currency = node.SelectSingleNode("Price/CurrencyCode").InnerText;
                    rateOfExchange = exchangeRates[hotelResult.Currency];
                    hotelResult.Price = new PriceAccounts();
                    
                    decimal totPrice = 0m;
                    decimal calcMarkup = 0m;
                    decimal tax = 0m;
                    decimal serviceTax = 0m;

                    totPrice = Convert.ToDecimal(node.SelectSingleNode("Price/RoomPrice").InnerText);
                    tax = Convert.ToDecimal(node.SelectSingleNode("Price/Tax").InnerText);
                    serviceTax = Convert.ToDecimal(node.SelectSingleNode("Price/ServiceTax").InnerText);

                    //in supplierPrice column saving  roomPrice,tax and servicetax
                    hotelResult.Price.SupplierPrice = totPrice + tax + serviceTax;
                    //VAT Calucation Changes Modified 14.12.2017
                    decimal hotelTotalPrice = 0m;
                    decimal vatAmount = 0m;
                    hotelTotalPrice = Math.Round(hotelResult.Price.SupplierPrice * rateOfExchange, decimalPoint);
                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                    {
                        hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                    }
                    hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                    calcMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(hotelTotalPrice) * (markup / 100m)));
                    hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                    hotelResult.Price.RateOfExchange = rateOfExchange;

                    //here added All taxes AND Markup
                    hotelResult.TotalPrice = hotelTotalPrice + calcMarkup;
                    hotelResult.Price.NetFare = hotelResult.TotalPrice;
                    hotelResult.Price.AccPriceType = PriceType.NetFare;
                    hotelResult.Currency = agentCurrency;
                    #endregion
                    HotelRoomsDetails[] rooms = new HotelRoomsDetails[0];
                    hotelResult.RoomDetails = rooms;
                    hotelResults[i++] = hotelResult;
                }
            }
            if (hotelResults.Length > i)
            {
                Array.Resize(ref hotelResults, i);
            }
            return hotelResults;
        }

        /// <summary>
        /// SearchRequest
        /// </summary>
        private class SearchRequest
        {
            /// <summary>
            /// TokenId
            /// </summary>
            public string TokenId;
            /// <summary>
            /// EndUserIp
            /// </summary>
            public string EndUserIp;
            /// <summary>
            /// BookingMode
            /// </summary>
            public int BookingMode;
            /// <summary>
            /// CheckInDate
            /// </summary>
            public string CheckInDate;
            /// <summary>
            /// NoOfNights(checkoutDate-CheckinDate)
            /// </summary>
            public int NoOfNights;
            /// <summary>
            /// CountryCode
            /// </summary>
            public string CountryCode;
            /// <summary>
            /// CityId
            /// </summary>
            public string CityId;
            /// <summary>
            /// PreferredCurrency
            /// </summary>
            public string PreferredCurrency;
            /// <summary>
            /// GuestNationality
            /// </summary>
            public string GuestNationality;
            /// <summary>
            /// NoOfRooms
            /// </summary>
            public int NoOfRooms;
            /// <summary>
            /// RoomGuests
            /// </summary>
            public RoomGuest[] RoomGuests;
            /// <summary>
            /// MaxRating
            /// </summary>
            public int MaxRating;
            /// <summary>
            /// MinRating
            /// </summary>
            public int MinRating;
        }
        /// <summary>
        /// RoomGuest
        /// </summary>
        private struct RoomGuest
        {
            /// <summary>
            /// NoOfAdults
            /// </summary>
            public int NoOfAdults;
            /// <summary>
            /// NoOfChild
            /// </summary>
            public int NoOfChild;
            /// <summary>
            /// ChildAge
            /// </summary>
            public List<int> ChildAge; // ChildAge list size should be equal to noOfChild
        }
        #endregion

        #region GetHotelDetails
        /// <summary>
        /// GetHotelDetails
        /// </summary>
        /// <param name="cityCode">Selected CityCode</param>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <param name="resutIndex">resutIndex</param>
        /// <param name="traceId">TraceId</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Every 15days (what we configuraed in App.config) Need to download StaticData
        /// here we are checking if static data is not there our DB we are sending static data request and get the response save our database
        /// </remarks>
        public HotelDetails GetHotelDetails(string cityCode, string hotelCode, int resutIndex, string traceId)
        {

            HotelDetails hotelDetails = new HotelDetails();
            try
            {
                bool dataAvailable = false, imagesAvailable = false, imagesUpdate = false;
                int timeStampDays = Convert.ToInt32(CT.Configuration.ConfigurationSystem.TBOHotelConfig["TimeStamp"]);
                HotelStaticData data = new HotelStaticData();
                //Loading static data
                data.Load(hotelCode, cityCode, HotelBookingSource.TBOHotel);
                if (data.HotelName != null && data.HotelName.Length > 0)
                {
                    //Check the Time span
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(data.TimeStamp);
                    //if it is greater than REquired time stamp days then load from DB
                    if (diffRes.Days < timeStampDays)
                    {
                        dataAvailable = true;
                    }
                }

                HotelImages images = new HotelImages();
                images.Load(hotelCode, cityCode, HotelBookingSource.TBOHotel);

                if (images.Images != null && images.Images.Length > 0)
                {
                    TimeSpan diffRes = DateTime.UtcNow.Subtract(data.TimeStamp);
                    //if it is greater than REquired time stamp days then load from DB
                    if (diffRes.Days < timeStampDays)
                    {
                        imagesAvailable = true;
                    }
                    else
                    {
                        imagesUpdate = true;
                    }
                }

                if (imagesAvailable)
                {
                    try
                    {
                        hotelDetails.Images = new List<string>();
                        string[] imageData = images.Images.Split('|');
                        hotelDetails.Image = imageData[0];
                        foreach (string img in imageData)
                        {
                            hotelDetails.Images.Add(img);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), "");
                    }
                }
                if (dataAvailable)
                {
                    //read static data from HTL_HOTEL_DATA 
                    try
                    {
                        hotelDetails.Email = data.EMail;
                        hotelDetails.FaxNumber = data.FaxNumber;
                        hotelDetails.Address = data.HotelAddress;
                        hotelDetails.HotelCode = data.HotelCode;
                        hotelDetails.Description = data.HotelDescription;
                        hotelDetails.HotelFacilities = new List<string>();
                        if (data.HotelFacilities != null && data.HotelFacilities.Length > 0)
                        {
                            foreach (string fac in data.HotelFacilities.Split('|'))
                            {
                                hotelDetails.HotelFacilities.Add(fac);
                            }
                        }

                        hotelDetails.Map = data.HotelMap;
                        hotelDetails.HotelName = data.HotelName;
                        hotelDetails.Image = data.HotelPicture;
                        hotelDetails.HotelPolicy = data.HotelPolicy;
                        hotelDetails.PhoneNumber = data.PhoneNumber;
                        hotelDetails.PinCode = data.PinCode;
                        hotelDetails.hotelRating = data.Rating;
                        hotelDetails.Attractions = new Dictionary<string, string>();

                        if (data.SpecialAttraction != null && data.SpecialAttraction.Length > 0)
                        {
                            foreach (string sa in data.SpecialAttraction.Split('|'))
                            {
                                string key = (sa.Split('#').Length > 0 ? sa.Split('#')[0] : "");
                                string value = (sa.Split('#').Length > 1 ? sa.Split('#')[1] : "");
                                hotelDetails.Attractions.Add(key, value);
                            }
                        }
                        data.CityCode = cityCode;
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), "");
                    }
                }
                if (!dataAvailable || !imagesAvailable)
                {
                    AuthenticationResponse autheRes = Authenticate();
                    string response = GenarateHotelDetailsRequest(hotelCode, resutIndex, traceId, autheRes);
                    hotelDetails = ReadHotelDetailsResponse(response);

                    if (hotelDetails.HotelName != null)
                    {
                        HotelStaticData hsd = new HotelStaticData();
                        hsd.EMail = hotelDetails.Email;
                        hsd.FaxNumber = hotelDetails.FaxNumber;
                        hsd.HotelAddress = hotelDetails.Address;
                        hsd.HotelCode = hotelCode;
                        hsd.HotelDescription = hotelDetails.Description;
                        hsd.HotelFacilities = "";
                        if (hotelDetails.HotelFacilities != null)
                        {
                            foreach (string fac in hotelDetails.HotelFacilities)
                            {
                                if (hsd.HotelFacilities.Length > 0)
                                {
                                    hsd.HotelFacilities += "|" + fac;
                                }
                                else
                                {
                                    hsd.HotelFacilities = fac;
                                }
                            }
                        }
                        hsd.HotelMap = hotelDetails.Map;
                        hsd.HotelName = hotelDetails.HotelName;
                        hsd.HotelPicture = hotelDetails.Image;
                        hsd.HotelPolicy = hotelDetails.HotelPolicy;
                        hsd.PhoneNumber = hotelDetails.PhoneNumber;
                        hsd.PinCode = hotelDetails.PinCode;
                        hsd.Rating = hotelDetails.hotelRating;
                        hsd.Source = HotelBookingSource.TBOHotel;
                        hsd.SpecialAttraction = "";

                        if (hotelDetails.Attractions != null)
                        {
                            foreach (KeyValuePair<string, string> pair in hotelDetails.Attractions)
                            {
                                if (hsd.SpecialAttraction.Length > 0)
                                {
                                    hsd.SpecialAttraction += "|" + pair.Key + "#" + pair.Value;
                                }
                                else
                                {
                                    hsd.SpecialAttraction = pair.Key + "#" + pair.Value;
                                }
                            }
                        }
                        hsd.Status = true;
                        hsd.TimeStamp = DateTime.Now;
                        hsd.URL = hotelDetails.URL;
                        hsd.CityCode = cityCode;
                        try
                        {
                            hsd.Save();
                        }
                        catch { }


                        images = new HotelImages();
                        images.CityCode = cityCode;
                        images.DownloadedImgs = "";
                        foreach (string image in hotelDetails.Images)
                        {
                            if (images.DownloadedImgs.Length > 0)
                            {
                                images.DownloadedImgs += "|" + image;
                            }
                            else
                            {
                                images.DownloadedImgs = image;
                            }
                        }
                        images.HotelCode = hotelCode;
                        images.Images = images.DownloadedImgs;
                        images.Source = HotelBookingSource.TBOHotel;
                        try
                        {
                            if (!imagesUpdate)
                            {
                                images.Save();
                            }
                            else
                            {
                                images.Update();
                            }
                        }
                        catch { }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOHoteDetailsSerch, Severity.High, 0, "Exception returned from TBOHotel.HotelDetails Error Message:" + ex.Message, "");
            }
            return hotelDetails;

        }


        /// <summary>
        /// This private function is used for Generates the Hotel Detail request.
        /// </summary>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <param name="resultIndex">resultIndex</param>
        /// <param name="traceId">traceId</param>
        /// <param name="autheRes">AuthenticationResponse</param>
        /// <returns>string</returns>
        /// <remarks>Here we are preparing HotelDetails request object and sending HotelDetail to api</remarks>
        private string GenarateHotelDetailsRequest(string hotelCode, int resultIndex, string traceId, AuthenticationResponse autheRes)
        {
            string response = string.Empty;
            try
            {
                HotelDetailsRequest detailsRequest = new HotelDetailsRequest();
                detailsRequest.TokenId = autheRes.TokenId;
                detailsRequest.TraceId = traceId;
                detailsRequest.ResultIndex = resultIndex;
                detailsRequest.HotelCode = hotelCode;
                detailsRequest.EndUserIp = userIP;

                string request = JsonConvert.SerializeObject(detailsRequest);
                // To convert JSON text contained in string json into an XML node
                XmlDocument xmlDocReq = JsonConvert.DeserializeXmlNode(request, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelDetailsRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocReq.Save(filePath);
                        //Audit.Add(EventType.TBOHoteDetailsSerch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelDetailsRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(request.ToString());
                        sw.Close();
                    }
                }
                catch { }

                response = GetResponse(request, ConfigurationSystem.TBOHotelConfig["GetHotelInfo"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, "Error:GenarateHotelDetails"+ex.ToString(), "127.0.0.1");
            }
            return response.ToString();
        }
        /// <summary>
        /// ReadHotelDetailsResponse
        /// </summary>
        /// <param name="response">StaticData Response string</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Here only we are Reading all static data</remarks>
        private HotelDetails ReadHotelDetailsResponse(string response)
        {
            HotelDetails hotelDetails = new HotelDetails();
            try
            {
                XmlNode tempNode;
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelDetailsResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocRes.Save(filePath);
                        //Audit.Add(EventType.TBOHoteDetailsSerch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelDetailsResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(response.ToString());
                        sw.Close();
                    }

                }
                catch { }
                XmlNode errorInfo = xmlDocRes.SelectSingleNode("/Root/HotelInfoResult/Error/ErrorMessage");
                //checking error info
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.TBOHoteDetailsSerch, Severity.High, 0, " TBOHotels:GenarateHotelDetails,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response JSON" + response, "");
                    throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }
                XmlNode hotelDetail = xmlDocRes.SelectSingleNode("/Root/HotelInfoResult/HotelDetails");
                if (hotelDetail != null)
                {
                    tempNode = hotelDetail.SelectSingleNode("HotelCode");
                    if(tempNode !=null)
                    {
                        hotelDetails.HotelCode=tempNode.InnerText;
                    }
                    tempNode= hotelDetail.SelectSingleNode("HotelName");
                    if (tempNode != null)
                    {
                        hotelDetails.HotelName = tempNode.InnerText;
                    }
                    tempNode=hotelDetail.SelectSingleNode("StarRating");
                    if (tempNode != null)
                    {
                        hotelDetails.hotelRating = (HotelRating)Convert.ToInt32(tempNode.InnerText);
                    }
                    tempNode = hotelDetail.SelectSingleNode("HotelURL");
                    if (tempNode != null)
                    {
                        hotelDetails.URL = tempNode.InnerText;
                    }

                    tempNode=hotelDetail.SelectSingleNode("Description");
                    if(tempNode !=null)
                    {
                        hotelDetails.Description = tempNode.InnerText;
                    }
                    
                    Dictionary<string, string> attractions = new Dictionary<string, string>();
                    XmlNodeList attractionList = hotelDetail.SelectNodes("Attractions");
                    if (attractionList != null)
                    {
                        foreach (XmlNode node in attractionList)
                        {
                            if (node.SelectSingleNode("Key") != null && node.SelectSingleNode("Value") != null)
                            {
                                attractions.Add(node.SelectSingleNode("Key").InnerText, node.SelectSingleNode("Value").InnerText);
                            }
                        }
                    }
                    hotelDetails.Attractions = attractions;
                    
                    hotelDetails.HotelFacilities = new List<string>();

                    XmlNodeList hFacilitiesList = hotelDetail.SelectNodes("HotelFacilities");
                    if (hFacilitiesList != null)
                    {
                        foreach (XmlNode facilities in hFacilitiesList)
                        {
                            hotelDetails.HotelFacilities.Add(facilities.InnerText);
                        }
                    }
                    tempNode=hotelDetail.SelectSingleNode("HotelPolicy");
                    if (tempNode != null)
                    {
                        string[] policies = tempNode.InnerText.Split('|');
                        hotelDetails.HotelPolicy = string.Empty;
                        foreach (string policie in policies)
                        {
                            if (!string.IsNullOrEmpty(policie))
                            {
                                if (hotelDetails.HotelPolicy != string.Empty)
                                {
                                    hotelDetails.HotelPolicy += "|" + policie;
                                }
                                else
                                {
                                    hotelDetails.HotelPolicy = policie;
                                }
                            }
                        }
                    }
                    tempNode = hotelDetail.SelectSingleNode("HotelPicture");
                    if (tempNode != null)
                    {
                        hotelDetails.Image = tempNode.InnerText;
                    }
                    hotelDetails.Images = new List<string>();
                    XmlNodeList images = hotelDetail.SelectNodes("Images");
                    if (images != null)
                    {
                        foreach (XmlNode img in images)
                        {
                            hotelDetails.Images.Add(img.InnerText);
                        }
                    }
                    tempNode=hotelDetail.SelectSingleNode("Address");
                    if (tempNode != null)
                    {
                        hotelDetails.Address = tempNode.InnerText;
                    }
                    tempNode=hotelDetail.SelectSingleNode("CountryName");
                    if (tempNode != null)
                    {
                        hotelDetails.CountryName = tempNode.InnerText;
                    }
                    tempNode=hotelDetail.SelectSingleNode("PinCode");
                    if (tempNode != null && !string.IsNullOrEmpty(tempNode.InnerText))
                    {
                        hotelDetails.PinCode = tempNode.InnerText;
                    }
                    tempNode =hotelDetail.SelectSingleNode("HotelContactNo");
                    if(tempNode !=null)
                    {
                        hotelDetails.PhoneNumber = tempNode.InnerText;
                    }
                    tempNode =hotelDetail.SelectSingleNode("FaxNumber");
                    if (tempNode != null)
                    {
                        hotelDetails.FaxNumber = tempNode.InnerText;
                    }
                    tempNode = hotelDetail.SelectSingleNode("Email");
                    if (tempNode != null)
                    {
                        hotelDetails.Email = tempNode.InnerText;
                    }
                    tempNode = hotelDetail.SelectSingleNode("Latitude");
                    if (tempNode != null)
                    {
                        hotelDetails.Map = tempNode.InnerText ;
                    }
                    tempNode=hotelDetail.SelectSingleNode("Longitude");
                    if(tempNode !=null)
                    {
                        hotelDetails.Map=hotelDetails.Map+ "|" + tempNode.InnerText;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("TBOHotel: Failed to read Hotel Details response ", ex);
            }

            return hotelDetails;
        }

        /// <summary>
        /// HotelDetailsRequest
        /// </summary>
        private class HotelDetailsRequest
        {
            /// <summary>
            /// ResultIndex
            /// </summary>
            public int ResultIndex;
            /// <summary>
            /// HotelCode
            /// </summary>
            public string HotelCode;
            /// <summary>
            /// EndUserIp
            /// </summary>
            public string EndUserIp;
            /// <summary>
            /// TokenId
            /// </summary>
            public string TokenId;
            /// <summary>
            /// TraceId
            /// </summary>
            public string TraceId;
        }
        #endregion

        #region GetRooms
        /// <summary>
        /// GetRooms
        /// </summary>
        /// <param name="hotelResult">HotelResults object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This B2B Markup</param>
        /// <param name="markupType">this B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <remarks>Here Only we are getting real live inventory and price</remarks>
        public void GetRooms(ref HotelSearchResult hotelResult,HotelRequest request,decimal markup, string markupType)
        {
            AuthenticationResponse autheRes = Authenticate();
            string response = GenerateGetRoomRequest(hotelResult, autheRes);
            try
            {
                ReadGetRoomsResponse(response, ref hotelResult,request, markup, markupType);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBORoomSearch, Severity.High, 0, "Exception returned from TBOHotels.ReadGetRoomsResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
        }
        /// <summary>
        /// This private function is used for Generates the room request.
        /// </summary>
        /// <param name="hotelResult">HotelResults object</param>
        /// <param name="autheRes">AuthenticationResponse</param>
        /// <returns>string</returns>
        private string GenerateGetRoomRequest(HotelSearchResult hotelResult, AuthenticationResponse autheRes)
        {
            string response = string.Empty;
            try
            {
                GetRoomsRequest roomGuestRequest = new GetRoomsRequest();
                roomGuestRequest.EndUserIp = userIP;
                roomGuestRequest.TokenId = autheRes.TokenId;
                roomGuestRequest.TraceId = hotelResult.PropertyType;
                roomGuestRequest.ResultIndex = Convert.ToInt32(hotelResult.SequenceNumber);
                roomGuestRequest.HotelCode = hotelResult.HotelCode;
                string request = JsonConvert.SerializeObject(roomGuestRequest);
                // To convert JSON text contained in string json into an XML node
                XmlDocument xmlDocReq = JsonConvert.DeserializeXmlNode(request, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelRoomsRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocReq.Save(filePath);
                        //Audit.Add(EventType.TBORoomSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelRoomsRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(request.ToString());
                        sw.Close();
                    }
                }
                catch { }
                try
                {
                    response = GetResponse(request, ConfigurationSystem.TBOHotelConfig["GetHotelRoom"]);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.TBORoomSearch, CT.Core.Severity.High, 0, "Error Returned from TBO while fetching rooms. | " + DateTime.Now + "| Request JSON" + request + "| Response JSON" + response+ex.ToString(), "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBORoomSearch, Severity.Normal, appUserId, "Error:GenarateHotelRooms" + ex.ToString(), "127.0.0.1");
            }
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response">Response String object</param>
        /// <param name="hotelResult">HotelResult Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">B2B Markup Value</param>
        /// <param name="markupType">B2B MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <remarks>Here Only We are calucating B2B Markup and InputVat</remarks>
        private void ReadGetRoomsResponse(string response, ref HotelSearchResult hotelResult,HotelRequest request, decimal markup, string markupType)
        {
            try
            {
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelRoomsResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocRes.Save(filePath);
                       // Audit.Add(EventType.TBORoomSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelRoomsResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(response.ToString());
                        sw.Close();
                    }

                }
                catch { }
                XmlNode errorInfo = xmlDocRes.SelectSingleNode("/Root/GetHotelRoomResult/Error/ErrorMessage");
                //checking error info
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.TBORoomSearch, Severity.High, 0, " TBOHotels:ReadGetRoomsResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response JSON" + response, "");
                    throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }
                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                XmlNodeList combination = xmlDocRes.SelectNodes("/Root/GetHotelRoomResult/RoomCombinations/RoomCombination");
                string infoSource = xmlDocRes.SelectSingleNode("/Root/GetHotelRoomResult/RoomCombinations/InfoSource").InnerText;
                //Loading All Combitions 
                Dictionary<int, int> combinationList = GetRoomCombination(combination, infoSource);
                XmlNodeList roomList = xmlDocRes.SelectNodes("/Root/GetHotelRoomResult/HotelRoomsDetails");
                hotelResult.SupplierType = infoSource; //Here SupplierType assigning Combition TBOHotel purpose only Added by brahmam
                int index = 0;
                if (roomList.Count >= request.RoomGuest.Length)
                {
                    hotelResult.RoomDetails = new HotelRoomsDetails[roomList.Count];
                    foreach (XmlNode roomInfo in roomList)
                    {
                        HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                        //roomIndex Specifically used for TBOHotel API,this roomIndex should be pass booking request time 
                        roomDetail.RoomIndex = Convert.ToInt32(roomInfo.SelectSingleNode("RoomIndex").InnerText);
                        roomDetail.RoomTypeName = roomInfo.SelectSingleNode("RoomTypeName").InnerText;
                        roomDetail.RoomTypeCode = roomInfo.SelectSingleNode("RoomTypeCode").InnerText + "#@#" + roomDetail.RoomIndex;
                        roomDetail.RatePlanCode = roomInfo.SelectSingleNode("RatePlanCode").InnerText;
                        XmlNode tempAmenities = roomInfo.SelectSingleNode("Amenities");
                        List<string> amenities = new List<string>();
                        if (tempAmenities != null)
                        {
                            string mealplan = string.Empty;
                            string amenitiesList = tempAmenities.InnerText;
                            if (!string.IsNullOrEmpty(amenitiesList))
                            {
                                string[] amentiesArry = amenitiesList.Split('|');
                                int i = 0;
                                foreach (string amentity in amentiesArry)
                                {
                                    if (!string.IsNullOrEmpty(amentity))
                                    {
                                        amenities.Add(amentity);
                                        if (i < 4)
                                        {
                                            if (string.IsNullOrEmpty(mealplan))
                                            {
                                                mealplan = amentity;
                                            }
                                            else
                                            {
                                                mealplan = mealplan + "," + amentity;
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                            roomDetail.Amenities = amenities;
                            if (!string.IsNullOrEmpty(mealplan))
                            {
                                roomDetail.mealPlanDesc = mealplan;
                            }
                            else
                            {
                                roomDetail.mealPlanDesc = "Room Only"; 
                            }
                        }
                        else
                        {
                            amenities.Add("No Amenitities Found");
                            roomDetail.Amenities = amenities;
                            roomDetail.mealPlanDesc = "Room Only"; 
                        }

                        XmlNode tempPromotion = roomInfo.SelectSingleNode("RoomPromotion");
                        if (tempPromotion != null)
                        {
                            string promotion = tempPromotion.InnerText;
                            if (!string.IsNullOrEmpty(promotion) && promotion.Length > 1)
                            {
                                roomDetail.PromoMessage = promotion;
                            }
                        }
                        roomDetail.SequenceNo = Convert.ToString(combinationList[roomDetail.RoomIndex]);
                        //SmokingPreference Specifically used for TBOHotel API,this SmokingPreference should be pass booking request time 
                        roomDetail.SmokingPreference = roomInfo.SelectSingleNode("SmokingPreference").InnerText;

                        #region price
                        XmlNode rateBasis = roomInfo.SelectSingleNode("Price");
                        string currency = rateBasis.SelectSingleNode("CurrencyCode").InnerText;
                        rateOfExchange = (exchangeRates.ContainsKey(currency) ? exchangeRates[currency] : 1);
                        
                        decimal totPrice = 0m;
                        decimal calcMarkup = 0m;
                        decimal tax = 0m;
                        decimal serviceTax = 0m;
                        
                        totPrice = Convert.ToDecimal(rateBasis.SelectSingleNode("RoomPrice").InnerText);
                        tax = Convert.ToDecimal(rateBasis.SelectSingleNode("Tax").InnerText);
                        serviceTax = Convert.ToDecimal(rateBasis.SelectSingleNode("ServiceTax").InnerText);
                        roomDetail.supplierPrice = totPrice + tax + serviceTax;

                        //VAT Calucation Changes Modified 14.12.2017
                        decimal hotelTotalPrice = 0m;
                        decimal vatAmount = 0m;
                        hotelTotalPrice = Math.Round((roomDetail.supplierPrice * rateOfExchange), decimalPoint);
                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                        {
                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                        }
                        hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                        roomDetail.TotalPrice = hotelTotalPrice;
                        calcMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                        roomDetail.Markup = calcMarkup;
                        roomDetail.MarkupType = markupType;
                        roomDetail.MarkupValue = markup;
                        roomDetail.SellingFare = roomDetail.TotalPrice;
                        roomDetail.TaxDetail = new PriceTaxDetails();
                        roomDetail.TaxDetail = priceTaxDet;
                        roomDetail.InputVATAmount = vatAmount;

                        //Price Details
                        System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                        RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                        decimal totalprice = roomDetail.TotalPrice;

                        for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                        {
                            decimal price = roomDetail.TotalPrice / diffResult.Days;
                            if (fareIndex == diffResult.Days - 1)
                            {
                                price = totalprice;
                            }
                            totalprice -= price;
                            hRoomRates[fareIndex].Amount = price;
                            hRoomRates[fareIndex].BaseFare = price;
                            hRoomRates[fareIndex].SellingFare = price;
                            hRoomRates[fareIndex].Totalfare = price;
                            hRoomRates[fareIndex].RateType = RateType.Negotiated;
                            hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                        }
                        roomDetail.Rates = hRoomRates;
                        #endregion

                        #region TBOPrice
                        roomDetail.TBOPrice = new PriceAccounts();
                        roomDetail.TBOPrice.SupplierCurrency = currency;
                        roomDetail.TBOPrice.SupplierPrice = Convert.ToDecimal(rateBasis.SelectSingleNode("RoomPrice").InnerText);
                        roomDetail.TBOPrice.Tax = Convert.ToDecimal(rateBasis.SelectSingleNode("Tax").InnerText);
                        roomDetail.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(rateBasis.SelectSingleNode("ExtraGuestCharge").InnerText);
                        roomDetail.TBOPrice.CessTax = Convert.ToDecimal(rateBasis.SelectSingleNode("ChildCharge").InnerText);
                        roomDetail.TBOPrice.OtherCharges = Convert.ToDecimal(rateBasis.SelectSingleNode("OtherCharges").InnerText);
                        roomDetail.TBOPrice.Discount = Convert.ToDecimal(rateBasis.SelectSingleNode("Discount").InnerText);
                        roomDetail.TBOPrice.PublishedFare = Convert.ToDecimal(rateBasis.SelectSingleNode("PublishedPrice").InnerText);
                        roomDetail.TBOPrice.ReverseHandlingCharge = Convert.ToDecimal(rateBasis.SelectSingleNode("PublishedPriceRoundedOff").InnerText);
                        roomDetail.TBOPrice.NetFare = Convert.ToDecimal(rateBasis.SelectSingleNode("OfferedPrice").InnerText);
                        roomDetail.TBOPrice.OurCommission = Convert.ToDecimal(rateBasis.SelectSingleNode("OfferedPriceRoundedOff").InnerText);
                        roomDetail.TBOPrice.AgentCommission = Convert.ToDecimal(rateBasis.SelectSingleNode("AgentCommission").InnerText);
                        roomDetail.TBOPrice.Markup = Convert.ToDecimal(rateBasis.SelectSingleNode("AgentMarkUp").InnerText);
                        roomDetail.TBOPrice.SeviceTax = Convert.ToDecimal(rateBasis.SelectSingleNode("ServiceTax").InnerText);
                        roomDetail.TBOPrice.TdsRate = Convert.ToDecimal(rateBasis.SelectSingleNode("TDS").InnerText);

                        #endregion
                       
                        #region Cancel policies
                        string lastcancelDate = roomInfo.SelectSingleNode("LastCancellationDate").InnerText;  //LastCancellationDate

                        XmlNodeList cancellationNodes = roomInfo.SelectNodes("CancellationPolicies");
                        double buffer = 0;
                        if (ConfigurationSystem.TBOHotelConfig.ContainsKey("Buffer"))
                        {
                            buffer = Convert.ToDouble(ConfigurationSystem.TBOHotelConfig["Buffer"]);
                        }
                        if (cancellationNodes != null && cancellationNodes.Count > 0) //Checking cancellationNodes count Added by brahmam 28.07.2016
                        {
                            string cancelInfo = "";
                            string endDate = "";
                            foreach (XmlNode cancelNode in cancellationNodes)
                            {
                                string startDate = cancelNode.SelectSingleNode("FromDate").InnerText;
                                startDate = (Convert.ToDateTime(startDate).AddDays(-(buffer))).ToString("dd-MMM-yyyy HH:mm:ss");
                                endDate = cancelNode.SelectSingleNode("ToDate").InnerText;
                                endDate = (Convert.ToDateTime(endDate).AddDays(-(buffer))).ToString("dd-MMM-yyyy HH:mm:ss");
                                string ChargeType = cancelNode.SelectSingleNode("ChargeType").InnerText;
                                decimal amount = Convert.ToDecimal(cancelNode.SelectSingleNode("Charge").InnerText);
                                if (cancelInfo.Length == 0)
                                {
                                    if (amount > 0)
                                    {
                                        if (ChargeType == "1")
                                        {
                                            cancelInfo = agentCurrency + " " + Math.Round((amount * rateOfExchange), decimalPoint) + " will be charged, If cancelled between " + startDate + " and " + endDate;
                                        }
                                        else if (ChargeType == "2")
                                        {

                                            cancelInfo = amount+""+ ".00% of total amount will be charged, If cancelled between " + startDate + " and " + endDate;

                                        }
                                        else if (ChargeType == "3")
                                        {
                                            cancelInfo = "Charge of " + amount + "night(s) will be charged, If cancelled between " + startDate + " and " + endDate;
                                        }
                                    }
                                    else
                                    {
                                        cancelInfo = "No cancellation charge, If cancelled between " + startDate + " and " + endDate;
                                    }
                                }
                                else
                                {
                                    if (amount > 0)
                                    {
                                        if (ChargeType == "1")
                                        {
                                            cancelInfo += "|" + agentCurrency + " " + Math.Round((amount * rateOfExchange), decimalPoint) + " will be charged, If cancelled between " + startDate + " and " + endDate;
                                        }
                                        else if (ChargeType == "2")
                                        {

                                            cancelInfo += "| " + amount + "" + ".00% of total amount will be charged, If cancelled between " + startDate + " and " + endDate;
                                        }
                                        else if (ChargeType == "3")
                                        {
                                            cancelInfo += "| Charge of " + amount +  " night(s) will be charged, If cancelled between " + startDate + " and " + endDate;
                                        }
                                    }
                                    else
                                    {
                                        cancelInfo += "| No cancellation charge, If cancelled between  " + startDate + " and " + endDate;
                                    }
                                }
                            }
                            roomDetail.CancellationPolicy = cancelInfo;
                        }
                        else
                        {
                            roomDetail.CancellationPolicy = "Non-Refundable booking";
                        }
                        roomDetail.CancellationPolicy = roomDetail.CancellationPolicy+"@#" + lastcancelDate;
                        #endregion
                     
                        hotelResult.RoomDetails[index] = roomDetail;
                        index++;
                    }
                }

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.TBORoomSearch, CT.Core.Severity.High, 0, "Error Reading from TBOHotel while fetching the rooms. Error Message:" + ex.ToString(), "");
            }
        }

        /// <summary>
        /// here Loading sequenceNumbers
        /// </summary>
        /// <param name="combination">combination</param>
        /// <param name="infoSource">infoSource(FixedCombination/OpenCombination)</param>
        /// <returns>Dictionary</returns>
        private Dictionary<int, int> GetRoomCombination(XmlNodeList combination,string infoSource)
        {
           
            Dictionary<int, int> combinationsList = new Dictionary<int, int>();
            try
            {
                
                if (combination != null)
                {
                    if (infoSource == "FixedCombination") //Same Selection
                    {
                        foreach (XmlNode combNode in combination)
                        {
                            XmlNodeList roomIndex = combNode.SelectNodes("RoomIndex");
                            if (roomIndex != null)
                            {
                                int i = 1;
                                foreach (XmlNode roomNode in roomIndex)
                                {
                                    int index = Convert.ToInt32(roomNode.InnerText);
                                    combinationsList.Add(index, i);
                                    i++;
                                }
                            }
                        }
                    }
                    else if (infoSource == "OpenCombination")//Diffrent Selection
                    {
                        int i = 1;
                        foreach (XmlNode combNode in combination)
                        {
                            XmlNodeList roomIndex = combNode.SelectNodes("RoomIndex");
                            if (roomIndex != null)
                            {
                               
                                foreach (XmlNode roomNode in roomIndex)
                                {
                                    int index = Convert.ToInt32(roomNode.InnerText);
                                    combinationsList.Add(index, i);
                                }
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return combinationsList;
        }
        /// <summary>
        /// GetRoomsRequest
        /// </summary>
        private class GetRoomsRequest
        {
            /// <summary>
            /// ResultIndex
            /// </summary>
            public int ResultIndex;
            /// <summary>
            /// EndUserIp
            /// </summary>
            public string EndUserIp;
            /// <summary>
            /// TokenId
            /// </summary>
            public string TokenId;
            /// <summary>
            /// TraceId
            /// </summary>
            public string TraceId;
            /// <summary>
            /// HotelCode
            /// </summary>
            public string HotelCode;
        }
        #endregion

        #region Booking
        /// <summary>
        /// GetBooking
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        /// <remarks>Here we are preparing Booking request object and sending book request to api </remarks>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            string requestXml = string.Empty;
            BookingResponse searchRes = new BookingResponse();
            string response = GenerateBookingRequest(itinerary);
            try
            {
                searchRes = ReadResponseBooking(response, ref itinerary);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOBooking, Severity.High, 0, "Exception returned from TBOHotels.ReadResponseBooking Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return searchRes;
        }

        /// <summary>
        ///  Reads the Hotel Booking response Json.
        /// </summary>
        /// <param name="response">Booking response Json</param>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>Here Reading booking response object
        /// here only we need to get confirmationNo and BookingId
        /// </remarks>
        private BookingResponse ReadResponseBooking(string response, ref HotelItinerary itinerary)
        {
            BookingResponse hotelBookRes = new BookingResponse();
            try
            {
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelBookingResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocRes.Save(filePath);
                        //Audit.Add(EventType.TBOBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelBookingResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(response.ToString());
                        sw.Close();
                    }

                }
                catch { }

                XmlNode errorInfo = xmlDocRes.SelectSingleNode("/Root/BookResult/Error/ErrorMessage");
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.TBORoomSearch, Severity.High, 0, " TBOHotels:ReadResponseBooking,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response JSON" + response, "");
                    hotelBookRes.Status = BookingResponseStatus.Failed;
                    hotelBookRes.Error = errorInfo.InnerText;
                    //throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }
                else
                {
                    bool voucherStatus = Convert.ToBoolean(xmlDocRes.SelectSingleNode("Root/BookResult/VoucherStatus").InnerText);
                    if (voucherStatus)
                    {
                        string status = xmlDocRes.SelectSingleNode("/Root/BookResult/Status").InnerText;
                        if (status == "1")
                        {
                            string bookingStatus = xmlDocRes.SelectSingleNode("/Root/BookResult/HotelBookingStatus").InnerText;
                            if (bookingStatus == "Confirmed")
                            {
                                string confirmationNo = xmlDocRes.SelectSingleNode("/Root/BookResult/ConfirmationNo").InnerText;
                                string bookingRefNo = xmlDocRes.SelectSingleNode("/Root/BookResult/BookingRefNo").InnerText;
                                string bokingId = xmlDocRes.SelectSingleNode("/Root/BookResult/BookingId").InnerText;
                                hotelBookRes.ConfirmationNo = confirmationNo;
                                itinerary.ConfirmationNo = confirmationNo;
                                itinerary.BookingRefNo = bookingRefNo + "/" + bokingId;
                                hotelBookRes.Status = BookingResponseStatus.Successful;
                                itinerary.Status = HotelBookingStatus.Confirmed;
                                hotelBookRes.Error = "";
                                itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: TBOHotel, as per final booking form reference No: " + confirmationNo;
                            }
                            else
                            {
                                hotelBookRes.Status = BookingResponseStatus.Failed;
                                hotelBookRes.Error = "Booking is Faild";
                            }
                        }
                        else
                        {
                            hotelBookRes.Status = BookingResponseStatus.Failed;
                            hotelBookRes.Error = "Booking is Faild";
                        }
                    }
                    else
                    {
                        hotelBookRes.Status = BookingResponseStatus.Failed;
                        hotelBookRes.Error = "Booking is not Vouchered";
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOBooking, Severity.High, 0, "Error Reading from TBOHotel while fetching the Booking. Error Message:" + ex.ToString(), "");
                throw new Exception("TBOHotel: Failed to Read Booking response ", ex);
            }
            return hotelBookRes;
        }

        /// <summary>
        /// This private function is used for Generates the Hotel Book request.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenerateBookingRequest(HotelItinerary itinerary)
        {
            string response = string.Empty;
            try
            {
                AuthenticationResponse autheRes = Authenticate();
                BookingRequest bookRequest = new BookingRequest();
                bookRequest.EndUserIp = userIP;
                bookRequest.TokenId = autheRes.TokenId;
                bookRequest.TraceId = itinerary.PropertyType;
                bookRequest.ResultIndex = Convert.ToInt32(itinerary.SequenceNumber);
                bookRequest.HotelCode = itinerary.HotelCode;
                bookRequest.HotelName = itinerary.HotelName;
                bookRequest.GuestNationality = itinerary.PassengerNationality;
                bookRequest.NoOfRooms = itinerary.NoOfRooms;
                bookRequest.IsVoucherBooking = true;
                //Loading HotelRoomDetails
                HotelBookRoomDetails[] roomDetails = new HotelBookRoomDetails[itinerary.NoOfRooms];
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    roomDetails[i].RoomIndex = itinerary.Roomtype[i].RoomIndex;
                    roomDetails[i].RatePlanCode = itinerary.Roomtype[i].RatePlanCode;
                    //roomDetails[i].RatePlanName = string.Empty;
                    string[] splitter = { "#@#" };
                    roomDetails[i].RoomTypeCode = itinerary.Roomtype[i].RoomTypeCode.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0];
                    roomDetails[i].RoomTypeName = itinerary.Roomtype[i].RoomName;

                    switch (itinerary.Roomtype[i].SmokingPreference)
                    {
                        case "NoPreference":
                            roomDetails[i].SmokingPreference = 0;
                            break;
                        case "Smoking":
                            roomDetails[i].SmokingPreference = 1;
                            break;
                        case "NonSmoking":
                            roomDetails[i].SmokingPreference = 2;
                            break;
                        case "Either":
                            roomDetails[i].SmokingPreference = 3;
                            break;
                    }
                    roomDetails[i].Supplements = itinerary.Roomtype[i].Supplements;
                    roomDetails[i].BedTypeCode = itinerary.Roomtype[i].BedTypeCode;
                    //BedTypes[] bedTypes = new BedTypes[1];
                    //bedTypes[i].BedTypeCode = itinerary.Roomtype[i].BedTypeCode;
                    //bedTypes[i].BedTypeDescription = string.Empty;
                    ////Loding Price object
                    //roomDetails[i].BedTypes = bedTypes;
                    PriceRequest tboPrice = new PriceRequest();
                    tboPrice.CurrencyCode = itinerary.Roomtype[i].TBOPrice.SupplierCurrency;
                    tboPrice.RoomPrice = itinerary.Roomtype[i].TBOPrice.SupplierPrice;
                    tboPrice.Tax = itinerary.Roomtype[i].TBOPrice.Tax;
                    tboPrice.ExtraGuestCharge = itinerary.Roomtype[i].TBOPrice.AdditionalTxnFee;
                    tboPrice.ChildCharge = itinerary.Roomtype[i].TBOPrice.CessTax;
                    tboPrice.OtherCharges = itinerary.Roomtype[i].TBOPrice.OtherCharges;
                    tboPrice.Discount = itinerary.Roomtype[i].TBOPrice.Discount;
                    tboPrice.PublishedPrice = itinerary.Roomtype[i].TBOPrice.PublishedFare;
                    tboPrice.PublishedPriceRoundedOff = itinerary.Roomtype[i].TBOPrice.ReverseHandlingCharge;
                    tboPrice.OfferedPrice = itinerary.Roomtype[i].TBOPrice.NetFare;
                    tboPrice.OfferedPriceRoundedOff = itinerary.Roomtype[i].TBOPrice.OurCommission;
                    tboPrice.AgentCommission = itinerary.Roomtype[i].TBOPrice.AgentCommission;
                    tboPrice.AgentMarkUp = itinerary.Roomtype[i].TBOPrice.Markup;
                    tboPrice.ServiceTax = itinerary.Roomtype[i].TBOPrice.SeviceTax;
                    tboPrice.TDS = itinerary.Roomtype[i].TBOPrice.TdsRate;
                    roomDetails[i].Price = tboPrice;
                    HotelPassengerinfo[] passenger = new HotelPassengerinfo[itinerary.Roomtype[i].PassenegerInfo.Count];
                    HotelRoom room = itinerary.Roomtype[i];
                    int n = 0;
                    foreach (HotelPassenger pax in room.PassenegerInfo)
                    {
                        if (pax.Title == "Mstr")
                        {
                            pax.Title = "Mr.";
                        }
                        passenger[n].Title = pax.Title;
                        passenger[n].FirstName = pax.Firstname;
                        passenger[n].MiddleName = pax.Middlename;
                        passenger[n].LastName = pax.Lastname;
                        passenger[n].LeadPassenger = pax.LeadPassenger;
                        passenger[n].Email = pax.Email;
                        passenger[n].Phoneno = pax.Phoneno;
                        if (HotelPaxType.Adult == pax.PaxType)
                        {
                           passenger[n].PaxType=1;
                        }
                        else
                        {
                            passenger[n].PaxType = 2;
                        }
                        passenger[n].Age = pax.Age;
                        passenger[n].PassportNo = null;
                        passenger[n].PassportIssueDate = null;
                        passenger[n].PassportExpDate = null;
                        n++;
                    }
                    roomDetails[i].HotelPassenger = passenger;
                }
                bookRequest.HotelRoomsDetails = roomDetails;
                string request = JsonConvert.SerializeObject(bookRequest);
                // To convert JSON text contained in string json into an XML node
                XmlDocument xmlDocReq = JsonConvert.DeserializeXmlNode(request, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelBookingequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocReq.Save(filePath);
                        //Audit.Add(EventType.TBOBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelBookingRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(request.ToString());
                        sw.Close();
                    }
                }
                catch { }

                response = GetResponse(request, ConfigurationSystem.TBOHotelConfig["Book"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOBooking, Severity.Normal, appUserId, "Error:GenerateBookingRequest" + ex.ToString(), "127.0.0.1");
            }
            return response.ToString();
        }
        /// <summary>
        /// BookingRequest
        /// </summary>
        private class BookingRequest
        {
            /// <summary>
            /// EndUserIp
            /// </summary>
            public string EndUserIp;
            /// <summary>
            /// TokenId
            /// </summary>
            public string TokenId;
            /// <summary>
            /// TraceId
            /// </summary>
            public string TraceId;
            /// <summary>
            /// ResultIndex
            /// </summary>
            public int ResultIndex;
            /// <summary>
            /// HotelCode
            /// </summary>
            public string HotelCode;
            /// <summary>
            /// HotelName
            /// </summary>
            public string HotelName;
            /// <summary>
            /// GuestNationality
            /// </summary>
            public string GuestNationality;
            /// <summary>
            /// NoOfRooms
            /// </summary>
            public int NoOfRooms;
            /// <summary>
            /// IsVoucherBooking
            /// </summary>
            public bool IsVoucherBooking;
            /// <summary>
            /// HotelRoomsDetails
            /// </summary>
            public HotelBookRoomDetails[] HotelRoomsDetails;
        }
        /// <summary>
        /// HotelBookRoomDetails
        /// </summary>
        private struct HotelBookRoomDetails
        {
            /// <summary>
            /// RoomIndex
            /// </summary>
            public int RoomIndex;
            /// <summary>
            /// RatePlanCode
            /// </summary>
            public string RatePlanCode;
            /// <summary>
            /// RoomTypeCode
            /// </summary>
            public string RoomTypeCode;
            /// <summary>
            /// RoomTypeName
            /// </summary>
            public string RoomTypeName;
            /// <summary>
            /// BedTypeCode
            /// </summary>
            public string BedTypeCode;
            /// <summary>
            /// SmokingPreference
            /// </summary>
            public int SmokingPreference;
            /// <summary>
            /// Supplements
            /// </summary>
            public string Supplements;
            /// <summary>
            /// Price
            /// </summary>
            public PriceRequest Price;
            /// <summary>
            /// HotelPassenger
            /// </summary>
            public HotelPassengerinfo[] HotelPassenger;

        }
        /// <summary>
        /// HotelPassengerinfo
        /// </summary>
        private struct HotelPassengerinfo
        {
            /// <summary>
            /// Title
            /// </summary>
            public string Title;
            /// <summary>
            /// FirstName
            /// </summary>
            public string FirstName;
            /// <summary>
            /// MiddleName
            /// </summary>
            public string MiddleName;
            /// <summary>
            /// LastName
            /// </summary>
            public string LastName;
            /// <summary>
            /// Phoneno
            /// </summary>
            public string Phoneno;
            /// <summary>
            /// Email
            /// </summary>
            public string Email;
            /// <summary>
            /// PaxType
            /// </summary>
            public int PaxType;
            /// <summary>
            /// LeadPassenger
            /// </summary>
            public bool LeadPassenger;
            /// <summary>
            /// Age
            /// </summary>
            public int Age;
            /// <summary>
            /// PassportNo
            /// </summary>
            public string PassportNo;
            /// <summary>
            /// PassportIssueDate
            /// </summary>
            public string PassportIssueDate;
            /// <summary>
            /// PassportExpDate
            /// </summary>
            public string PassportExpDate;
        }

        #endregion

        #region   BlockRooms
        /// <summary>
        ///  This method is to block the rooms before making a booking.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Before booking we are checking room is blocked or not(status Checking)
        /// If rooms is blocked(status=true) need to update update allocationId
        /// if room is unblocked(status=false) need to send error room is unblocked
        /// </remarks>
        public Dictionary<string, string> BlockRooms(HotelItinerary itinerary)
        {
            Dictionary<string, string> blockInfo = new Dictionary<string, string>();
            string response= GenerateBlockRoomRequest(itinerary);
            try
            {
                blockInfo = ReadBlockedRoomResponse(response, itinerary);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOBooking, Severity.High, 0, "Exception returned from TBOHotels.ReadBlockedRoomResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return blockInfo;
        }

        /// <summary>
        ///  Reads the Hotel Block response JSON.
        /// </summary>
        /// <param name="response">Response Json object</param>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> ReadBlockedRoomResponse(string response, HotelItinerary itinerary)
        {
            Dictionary<string, string> blockInfo = new Dictionary<string, string>();
            try
            {
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelBlockRoomsResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocRes.Save(filePath);
                        //Audit.Add(EventType.TBOBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelBlockRoomsResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(response.ToString());
                        sw.Close();
                    }

                }
                catch { }

                XmlNode errorInfo = xmlDocRes.SelectSingleNode("/Root/BlockRoomResult/Error/ErrorMessage");
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.TBORoomSearch, Severity.High, 0, " TBOHotels:ReadBlockedRoomResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response JSON" + response, "");
                    blockInfo.Add("Status", "false");
                    blockInfo.Add("Error", "No Availability Found");
                    //throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }
                else
                {
                    string traceId = xmlDocRes.SelectSingleNode("/Root/BlockRoomResult/TraceId").InnerText;
                    blockInfo.Add("traceId", traceId);
                    string availability = xmlDocRes.SelectSingleNode("/Root/BlockRoomResult/AvailabilityType").InnerText;
                    //Checking confirmed or not
                    if (availability == "Confirm")
                    {
                        bool IsPriceChanged = Convert.ToBoolean(xmlDocRes.SelectSingleNode("/Root/BlockRoomResult/IsPriceChanged").InnerText);
                        //checking price changed or not if price changed w Loading new price 
                        if (!IsPriceChanged)
                        {
                            blockInfo.Add("Status", "true");
                            bool cancelChanged = Convert.ToBoolean(xmlDocRes.SelectSingleNode("/Root/BlockRoomResult/IsCancellationPolicyChanged").InnerText);
                            if (cancelChanged)
                            {
                                
                                #region Cancel policies
                                XmlNodeList roomList = xmlDocRes.SelectNodes("/Root/BlockRoomResult/HotelRoomsDetails");
                                string lastcancelDate = string.Empty;
                                string hotelCancelInfo = "";
                                foreach (XmlNode roomInfo in roomList)
                                {
                                    lastcancelDate = roomInfo.SelectSingleNode("LastCancellationDate").InnerText;  //LastCancellationDate
                                    XmlNodeList cancellationNodes = roomInfo.SelectNodes("CancellationPolicies");
                                    double buffer = 0;
                                    if (ConfigurationSystem.TBOHotelConfig.ContainsKey("Buffer"))
                                    {
                                        buffer = Convert.ToDouble(ConfigurationSystem.TBOHotelConfig["Buffer"]);
                                    }
                                    if (cancellationNodes != null)
                                    {
                                        string cancelInfo = "";
                                        string endDate = "";
                                        foreach (XmlNode cancelNode in cancellationNodes)
                                        {
                                            string startDate = cancelNode.SelectSingleNode("FromDate").InnerText;
                                            startDate = (Convert.ToDateTime(startDate).AddDays(-(buffer))).ToString("dd-MMM-yyyy HH:mm:ss");
                                            endDate = cancelNode.SelectSingleNode("ToDate").InnerText;
                                            endDate = (Convert.ToDateTime(endDate).AddDays(-(buffer))).ToString("dd-MMM-yyyy HH:mm:ss");
                                            string ChargeType = cancelNode.SelectSingleNode("ChargeType").InnerText;
                                            decimal amount = Convert.ToDecimal(cancelNode.SelectSingleNode("Charge").InnerText);
                                            string currency = cancelNode.SelectSingleNode("Currency").InnerText;
                                            rateOfExchange = (exchangeRates.ContainsKey(currency) ? exchangeRates[currency] : 1);
                                            if (cancelInfo.Length == 0)
                                            {
                                                if (amount > 0)
                                                {
                                                    if (ChargeType == "1")
                                                    {
                                                        cancelInfo = agentCurrency + " " + Math.Round((amount * rateOfExchange), decimalPoint) + " will be charged, If cancelled between " + startDate + " and " + endDate;
                                                    }
                                                    else if (ChargeType == "2")
                                                    {

                                                        cancelInfo = amount + "" + ".00% of total amount will be charged, If cancelled between " + startDate + " and " + endDate;

                                                    }
                                                    else if (ChargeType == "3")
                                                    {
                                                        cancelInfo = agentCurrency + " " + Math.Round((amount * rateOfExchange), decimalPoint) + "will be charged per Night, If cancelled between " + startDate + " and " + endDate;
                                                    }
                                                }
                                                else
                                                {
                                                    cancelInfo = "No cancellation charge, If cancelled between " + startDate + " and " + endDate;
                                                }
                                            }
                                            else
                                            {
                                                if (amount > 0)
                                                {
                                                    if (ChargeType == "1")
                                                    {
                                                        cancelInfo += "|" + agentCurrency + " " + Math.Round((amount * rateOfExchange), decimalPoint) + " will be charged, If cancelled between " + startDate + " and " + endDate;
                                                    }
                                                    else if (ChargeType == "2")
                                                    {

                                                        cancelInfo += "| " + amount + "" + ".00% of total amount will be charged, If cancelled between " + startDate + " and " + endDate;
                                                    }
                                                    else if (ChargeType == "3")
                                                    {
                                                        cancelInfo += "|" + agentCurrency + " " + Math.Round((amount * rateOfExchange), decimalPoint) + "will be charged per Night, If cancelled between " + startDate + " and " + endDate;
                                                    }
                                                }
                                                else
                                                {
                                                    cancelInfo += "| No cancellation charge, If cancelled between  " + startDate + " and " + endDate;
                                                }
                                            }
                                        }
                                        if (hotelCancelInfo.Length > 0)
                                        {
                                            hotelCancelInfo += "|" + cancelInfo;
                                        }
                                        else
                                        {
                                            hotelCancelInfo = cancelInfo;
                                            
                                        }
                                    }
                                }
                                blockInfo.Add("lastCancellationDate", Convert.ToString(lastcancelDate));
                                blockInfo.Add("CancelPolicy", hotelCancelInfo);
                                #endregion
                            }
                            else
                            {
                                blockInfo.Add("lastCancellationDate", Convert.ToString(itinerary.LastCancellationDate));
                                blockInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                            }
                            //Added on 13072016 For checking hotelNorms avialable or not.
                             XmlNode tempNode = xmlDocRes.SelectSingleNode("/Root/BlockRoomResult/HotelNorms");
                             if (tempNode != null)
                             {
                                 string HotelPolicy = xmlDocRes.SelectSingleNode("/Root/BlockRoomResult/HotelNorms").InnerText;

                                 if (!string.IsNullOrEmpty(HotelPolicy))
                                 {
                                     blockInfo.Add("HotelPolicy", HotelPolicy);
                                 }
                             }
                             else
                             {
                                 blockInfo.Add("HotelPolicy", itinerary.HotelPolicyDetails);
                             }

                        }
                        else
                        {
                            blockInfo.Add("Status", "false");
                            blockInfo.Add("Error", "PriceChanged");
                        }
                    }

                    else
                    {
                        blockInfo.Add("Status", "false");
                        blockInfo.Add("Error", "No Availability Found");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOBooking,Severity.High, 0, "Error Reading from TBOHotel while fetching the block rooms. Error Message:" + ex.ToString(), "");
                throw new Exception("TBOHotel: Failed to Read BlockRoom response ", ex);
            }
            return blockInfo;
        }
        /// <summary>
        /// This private function is used for Generates the Hotel BlockRoom request.
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        private string GenerateBlockRoomRequest(HotelItinerary itinerary)
        {
            string response = string.Empty;
            try
            {
                AuthenticationResponse autheRes = Authenticate();
                BlockRoomRequest blockRequest = new BlockRoomRequest();
                blockRequest.EndUserIp = userIP;
                blockRequest.TokenId = autheRes.TokenId;
                blockRequest.TraceId = itinerary.PropertyType;
                blockRequest.ResultIndex = Convert.ToInt32(itinerary.SequenceNumber);
                blockRequest.HotelCode = itinerary.HotelCode;
                blockRequest.HotelName = itinerary.HotelName;
                blockRequest.GuestNationality = itinerary.PassengerNationality;
                blockRequest.NoOfRooms = itinerary.NoOfRooms;
                blockRequest.IsVoucherBooking = true;
                //Loading HotelRoomDetails
                HotelRoomDetails[] roomDetails = new HotelRoomDetails[itinerary.NoOfRooms];
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    roomDetails[i].RoomIndex = itinerary.Roomtype[i].RoomIndex;
                    roomDetails[i].RatePlanCode = itinerary.Roomtype[i].RatePlanCode;
                    //roomDetails[i].RatePlanName = string.Empty;
                    string[] splitter = { "#@#" };
                    roomDetails[i].RoomTypeCode = itinerary.Roomtype[i].RoomTypeCode.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0];
                    roomDetails[i].RoomTypeName = itinerary.Roomtype[i].RoomName;

                    switch (itinerary.Roomtype[i].SmokingPreference)
                    {
                        case "NoPreference":
                            roomDetails[i].SmokingPreference = 0;
                            break;
                        case "Smoking":
                            roomDetails[i].SmokingPreference = 1;
                            break;
                        case "NonSmoking":
                            roomDetails[i].SmokingPreference = 2;
                            break;
                        case "Either":
                            roomDetails[i].SmokingPreference =3;
                            break;
                    }
                    roomDetails[i].Supplements = itinerary.Roomtype[i].Supplements;
                    roomDetails[i].BedTypeCode = itinerary.Roomtype[i].BedTypeCode;
                    //BedTypes[] bedTypes = new BedTypes[1];
                    //bedTypes[i].BedTypeCode = itinerary.Roomtype[i].BedTypeCode;
                    //bedTypes[i].BedTypeDescription = string.Empty;
                    ////Loding Price object
                    //roomDetails[i].BedTypes = bedTypes;
                    PriceRequest tboPrice = new PriceRequest();
                    tboPrice.CurrencyCode = itinerary.Roomtype[i].TBOPrice.SupplierCurrency;
                    tboPrice.RoomPrice = itinerary.Roomtype[i].TBOPrice.SupplierPrice;
                    tboPrice.Tax = itinerary.Roomtype[i].TBOPrice.Tax;
                    tboPrice.ExtraGuestCharge = itinerary.Roomtype[i].TBOPrice.AdditionalTxnFee;
                    tboPrice.ChildCharge = itinerary.Roomtype[i].TBOPrice.CessTax;
                    tboPrice.OtherCharges = itinerary.Roomtype[i].TBOPrice.OtherCharges;
                    tboPrice.Discount = itinerary.Roomtype[i].TBOPrice.Discount;
                    tboPrice.PublishedPrice = itinerary.Roomtype[i].TBOPrice.PublishedFare;
                    tboPrice.PublishedPriceRoundedOff = itinerary.Roomtype[i].TBOPrice.ReverseHandlingCharge;
                    tboPrice.OfferedPrice = itinerary.Roomtype[i].TBOPrice.NetFare;
                    tboPrice.OfferedPriceRoundedOff = itinerary.Roomtype[i].TBOPrice.OurCommission;
                    tboPrice.AgentCommission = itinerary.Roomtype[i].TBOPrice.AgentCommission;
                    tboPrice.AgentMarkUp = itinerary.Roomtype[i].TBOPrice.Markup;
                    tboPrice.ServiceTax = itinerary.Roomtype[i].TBOPrice.SeviceTax;
                    tboPrice.TDS = itinerary.Roomtype[i].TBOPrice.TdsRate;
                    roomDetails[i].Price = tboPrice;
                }
                blockRequest.HotelRoomsDetails = roomDetails;
                string request = JsonConvert.SerializeObject(blockRequest);
                // To convert JSON text contained in string json into an XML node
                XmlDocument xmlDocReq = JsonConvert.DeserializeXmlNode(request, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelBlockRoomRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocReq.Save(filePath);
                        //Audit.Add(EventType.TBOBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelBlockRoomRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(request.ToString());
                        sw.Close();
                    }
                }
                catch { }

                response = GetResponse(request, ConfigurationSystem.TBOHotelConfig["BlockRoom"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOBooking, Severity.Normal, appUserId, "Error:GenerateBlockRoomRequest" + ex.ToString(), "127.0.0.1");
            }
            return response.ToString();
        }
        /// <summary>
        /// BlockRoomRequest
        /// </summary>
        private class BlockRoomRequest
        {
            /// <summary>
            /// EndUserIp
            /// </summary>
            public string EndUserIp;
            /// <summary>
            /// TokenId
            /// </summary>
            public string TokenId;
            /// <summary>
            /// TraceId
            /// </summary>
            public string TraceId;
            /// <summary>
            /// ResultIndex
            /// </summary>
            public int ResultIndex;
            /// <summary>
            /// HotelCode
            /// </summary>
            public string HotelCode;
            /// <summary>
            /// HotelName
            /// </summary>
            public string HotelName;
            /// <summary>
            /// GuestNationality
            /// </summary>
            public string GuestNationality;
            /// <summary>
            /// NoOfRooms
            /// </summary>
            public int NoOfRooms;
            /// <summary>
            /// IsVoucherBooking
            /// </summary>
            public bool IsVoucherBooking;
            /// <summary>
            /// HotelRoomsDetails
            /// </summary>
            public HotelRoomDetails[] HotelRoomsDetails;
        }
        /// <summary>
        /// HotelRoomDetails
        /// </summary>
        private struct HotelRoomDetails
        {
            /// <summary>
            /// RoomIndex
            /// </summary>
            public int RoomIndex;
            /// <summary>
            /// RatePlanCode
            /// </summary>
            public string RatePlanCode;
            //public string RatePlanName;
            /// <summary>
            /// RoomTypeCode
            /// </summary>
            public string RoomTypeCode;
            /// <summary>
            /// RoomTypeName
            /// </summary>
            public string RoomTypeName;
            /// <summary>
            /// BedTypeCode
            /// </summary>
            public string BedTypeCode;
            /// <summary>
            /// SmokingPreference
            /// </summary>
            public int SmokingPreference;
            /// <summary>
            /// Supplements
            /// </summary>
            public string Supplements;
            /// <summary>
            /// Price
            /// </summary>
            public PriceRequest Price;
            
        }
        //private struct BedTypes
        //{
        //    public string BedTypeCode;
        //    public string BedTypeDescription;
        //}
        /// <summary>
        /// PriceRequest
        /// </summary>
        private struct PriceRequest
        {
            /// <summary>
            /// CurrencyCode
            /// </summary>
            public string CurrencyCode;
            /// <summary>
            /// RoomPrice
            /// </summary>
            public decimal RoomPrice;
            /// <summary>
            /// Tax
            /// </summary>
            public decimal Tax;
            /// <summary>
            /// ExtraGuestCharge
            /// </summary>
            public decimal ExtraGuestCharge;
            /// <summary>
            /// ChildCharge
            /// </summary>
            public decimal ChildCharge;
            /// <summary>
            /// ChildCharge
            /// </summary>
            public decimal OtherCharges;
            /// <summary>
            /// Discount
            /// </summary>
            public decimal Discount;
            /// <summary>
            /// PublishedPrice
            /// </summary>
            public decimal PublishedPrice;
            /// <summary>
            /// PublishedPriceRoundedOff
            /// </summary>
            public decimal PublishedPriceRoundedOff;
            /// <summary>
            /// OfferedPrice
            /// </summary>
            public decimal OfferedPrice;
            /// <summary>
            /// OfferedPriceRoundedOff
            /// </summary>
            public decimal OfferedPriceRoundedOff;
            /// <summary>
            /// AgentCommission
            /// </summary>
            public decimal AgentCommission;
            /// <summary>
            /// AgentMarkUp
            /// </summary>
            public decimal AgentMarkUp;
            /// <summary>
            /// ServiceTax
            /// </summary>
            public decimal ServiceTax;
            /// <summary>
            /// TDS
            /// </summary>
            public decimal TDS;
        }
        #endregion

        #region CancelHotelBooking 
        /// <summary>
        /// CancelHotelBooking
        /// </summary>
        /// <param name="bookingRefNo">Booking confirmationNo</param>
        /// <param name="remarks">Remarks</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Here we are preparing CancelHotelBooking request object and sending CancelHotelBooking to api</remarks>
        public Dictionary<string, string> CancelHotelBooking(string bookingRefNo,string remarks)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            string response = GenerateCancellationRequest(bookingRefNo, remarks);
            try
            {
               string changeRequestId = ReadCancellationResponse(response);
               if (!string.IsNullOrEmpty(changeRequestId))
               {
                   cancellationCharges = GetCancelStatus(changeRequestId);
               }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOBooking, Severity.High, 0, "Exception returned from TBOHotels.ReadBlockedRoomResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancellationCharges;
        }

        /// <summary>
        /// ReadCancellationResponse
        /// </summary>
        /// <param name="response">PreCanceResponse xml</param>
        /// <returns>string</returns>
        /// <remarks>First here we need to take changeRequestId once get changeRequestId nee to call cancel status method</remarks>
        private string ReadCancellationResponse(string response)
        {
             string changeRequestId = string.Empty;
            try
            {
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelCanceleResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocRes.Save(filePath);
                        //Audit.Add(EventType.TBOCancel, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelCancelResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(response.ToString());
                        sw.Close();
                    }

                }
                catch { }

                XmlNode errorInfo = xmlDocRes.SelectSingleNode("/Root/HotelChangeRequestResult/Error/ErrorMessage");
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.TBOCancel, Severity.High, 0, " TBOHotels:ReadCancellationResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response JSON" + response, "");
                    throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }
                else
                {
                    string changeRequestStatus = xmlDocRes.SelectSingleNode("Root/HotelChangeRequestResult/ChangeRequestStatus").InnerText;
                    //if (changeRequestStatus == "2") //Here 2 means InPrograss
                    {
                        changeRequestId = xmlDocRes.SelectSingleNode("/Root/HotelChangeRequestResult/ChangeRequestId").InnerText;
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOCancel, Severity.High, 0, "Error Reading from TBOHotel while fetching the Cancel Hotel. Error Message:" + ex.ToString(), "");
                throw new Exception("TBOHotel: Failed to Read Cancel response ", ex);
            }
            return changeRequestId;
        }
        /// <summary>
        /// This private function is used for Generates the Hotel Cancellation request.
        /// </summary>
        /// <param name="bookingRefNo">Booking confirmationNo</param>
        /// <param name="remarks">Remarks</param>
        /// <returns>string</returns>
        private string GenerateCancellationRequest(string bookingRefNo, string remarks)
        {
            string response = string.Empty;
            try
            {
                AuthenticationResponse autheRes = Authenticate();
                CancelRequest cancelRequest = new CancelRequest();
                cancelRequest.EndUserIp = userIP;
                cancelRequest.TokenId = autheRes.TokenId;
                cancelRequest.BookingId = bookingRefNo.Split('/')[1];
                cancelRequest.RequestType = "4";//Here 4 means Cancel
                cancelRequest.Remarks = remarks;
                string request = JsonConvert.SerializeObject(cancelRequest);
                // To convert JSON text contained in string json into an XML node
                XmlDocument xmlDocReq = JsonConvert.DeserializeXmlNode(request, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOHotelCancelRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocReq.Save(filePath);
                       // Audit.Add(EventType.TBOCancel, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOHotelCancelRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(request.ToString());
                        sw.Close();
                    }
                }
                catch { }

                response = GetResponse(request, ConfigurationSystem.TBOHotelConfig["CancelBook"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOCancel, Severity.Normal, appUserId, "Error:GenerateCancellationRequest" + ex.ToString(), "127.0.0.1");
            }
            return response.ToString();
        }
        /// <summary>
        /// CancelRequest
        /// </summary>
        private class CancelRequest
        {
            /// <summary>
            /// EndUserIp
            /// </summary>
            public string EndUserIp;
            /// <summary>
            /// TokenId
            /// </summary>
            public string TokenId;
            /// <summary>
            /// BookingId
            /// </summary>
            public string BookingId;
            /// <summary>
            /// RequestType
            /// </summary>
            public string RequestType;
            /// <summary>
            /// Remarks
            /// </summary>
            public string Remarks;
        }
        #endregion

        #region GetChangeRequestStatus
        /// <summary>
        /// GetCancelStatus
        /// </summary>
        /// <param name="changeRequestId">changeRequestId</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Here we are preparing CancelStatus request object and sending CancelStatus to api</remarks>
        public Dictionary<string, string> GetCancelStatus(string changeRequestId)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            string response = GenerateCancelStatusRequest(changeRequestId);
            try
            {
                cancellationCharges = ReadCancelStatusResponse(response);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOBooking, Severity.High, 0, "Exception returned from TBOHotels.ReadBlockedRoomResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancellationCharges;
        }
        /// <summary>
        /// Reads the Hotel CancelStatusResponse JSON.
        /// </summary>
        /// <param name="response">Response JSON</param>
        /// <returns>Dictionary</returns>
        /// <remarks>Here checking status if status is true booking cancelled else booking not cancelled</remarks>
        private Dictionary<string, string> ReadCancelStatusResponse(string response)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            try
            {
                XmlDocument xmlDocRes = JsonConvert.DeserializeXmlNode(response, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOCancelStatusResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocRes.Save(filePath);
                       // Audit.Add(EventType.TBOCancel, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOCanceleStatusResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(response.ToString());
                        sw.Close();
                    }

                }
                catch { }

                XmlNode errorInfo = xmlDocRes.SelectSingleNode("/Root/HotelChangeRequestStatusResult/Error/ErrorMessage");
                if (errorInfo != null && errorInfo.InnerText.Length > 0)
                {
                    Audit.Add(EventType.TBOCancel, Severity.High, 0, " TBOHotels:ReadCancelStatusResponse,Error Message:" + errorInfo.Value + " | " + DateTime.Now + "| Response JSON" + response, "");
                    throw new BookingEngineException("<br>" + errorInfo.InnerText);
                }
                else
                {
                    XmlNode changeRequestStatus = xmlDocRes.SelectSingleNode("Root/HotelChangeRequestStatusResult/ChangeRequestStatus");
                    if (changeRequestStatus.InnerText == "3")
                    {
                        cancellationCharges.Add("Status", "Cancelled");
                        XmlNode cRequestId = xmlDocRes.SelectSingleNode("Root/HotelChangeRequestStatusResult/ChangeRequestId");
                        if (cRequestId != null)
                        {
                            cancellationCharges.Add("ID", cRequestId.InnerText);
                        }
                        XmlNode cancellationCharge = xmlDocRes.SelectSingleNode("Root/HotelChangeRequestStatusResult/CancellationCharge");
                        if (cancellationCharge != null)
                        {
                            cancellationCharges.Add("Currency", "INR"); //Currency
                            cancellationCharges.Add("Amount", cancellationCharge.InnerText);
                        }
                        else
                        {
                            cancellationCharges.Add("Currency", "INR"); //Currency
                            cancellationCharges.Add("Amount", "0");
                        }
                    }
                    else
                    {
                        cancellationCharges.Add("Status", "Failed");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cancellationCharges;
        }
        /// <summary>
        /// This private function is used for Generates the Hotel CancelStatus request.
        /// </summary>
        /// <param name="changeRequestId">changeRequestId</param>
        /// <returns>string</returns>
        private string GenerateCancelStatusRequest(string changeRequestId)
        {
            string response = string.Empty;
            try
            {
                AuthenticationResponse autheRes = Authenticate();
                CancelStatus cStatus = new CancelStatus();
                cStatus.EndUserIp = userIP;
                cStatus.TokenId = autheRes.TokenId;
                cStatus.ChangeRequestId = changeRequestId;
                string request = JsonConvert.SerializeObject(cStatus);
                // To convert JSON text contained in string json into an XML node
                XmlDocument xmlDocReq = JsonConvert.DeserializeXmlNode(request, "Root");
                try
                {
                    if (ConfigurationSystem.TBOHotelConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOCancelStatusRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        xmlDocReq.Save(filePath);
                        //Audit.Add(EventType.TBOCancel, Severity.Normal, appUserId, filePath, "127.0.0.1");

                        //JsonFormat
                        string filePathJson = XmlPath + "TBOCancelStatusRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePathJson);
                        sw.Write(request.ToString());
                        sw.Close();
                    }
                }
                catch { }

                response = GetResponse(request, ConfigurationSystem.TBOHotelConfig["CancelStatus"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOCancel, Severity.Normal, appUserId, "Error:GenerateCancelStatusRequest" + ex.ToString(), "127.0.0.1");
            }
            return response.ToString();
        }
        /// <summary>
        /// CancelStatus
        /// </summary>
        private class CancelStatus
        {
            /// <summary>
            /// EndUserIp
            /// </summary>
            public string EndUserIp;
            /// <summary>
            /// TokenId
            /// </summary>
            public string TokenId;
            /// <summary>
            /// ChangeRequestId
            /// </summary>
            public string ChangeRequestId;
        }
        #endregion

        #region IDisposable Support
        /// <summary>
        /// To detect redundant calls
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing">disposing(True/False)</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// <summary>
        /// ~HotelV10
        /// </summary>
        ~HotelV10()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary>
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
