#region Summary
///////////////////////////////////////////////////////////////////////////////
/// AUTHOR		 : Mohammed Ziyad
/// CREATE DATE	 : 
/// PURPOSE		 : 
/// SPECIAL NOTES: 
/// FILE NAME	 : $Workfile: DOTWApi.cs $	
/// VSS ARCHIVE	 : $Archive: VSS Path $
/// VERSION		 : $Revision: 2 $
///
/// ===========================================================================
/*/ $History: DOTWApi.cs $
 * 
 * *****************  Version 1  *****************
 * User:              Date:             Time: 
 * Description of change:
 * 
 * *****************  Version 2  *****************
 * User:              Date:             Time: 
 * Description of change:
 * 

/*/
///
///////////////////////////////////////////////////////////////////////////////
#endregion

#region QA Status
#region Review History
///////////////////////////////////////////////////////////////////////////////
/// CODE STATUS	: NOT REVIEWED
///
/// ===========================================================================
///	REVIEW HISTORY
/// ---------------------------------------------------------------------------
/// DATE		| NAME			| COMMENTS
/// ---------------------------------------------------------------------------
/// 
///
///////////////////////////////////////////////////////////////////////////////
#endregion

#region Known Issues and Limitations
///////////////////////////////////////////////////////////////////////////////
/// DATE		: 
///	NAME		:  
///	METHOD NAME	: 
///	DESCRIPTION	: 
///
///----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
#endregion
#endregion

#region .Net Base Class Namespace Imports
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Security.Cryptography;
#endregion

#region Custom Namespace Imports
using CT.BookingEngine;
using CT.Configuration;
//using CT.Data;
using CT.Core;
using System.Configuration;
using System.IO.Compression;
using System.Data;
#endregion

namespace CT.BookingEngine.GDS
{
    public class DOTWApi //: //Engine
    {
        
        #region Private Variable
        string reqURL = string.Empty;
        string userName = string.Empty;
        string password = string.Empty;
        string companyCode = string.Empty;
        string md5Password = string.Empty;
        string sessionId = string.Empty;
        string ourRefNo = string.Empty;
        Regex regex = new Regex("[0-9]+.{0,1}[0-9]*");
        bool testMode;
        int appUserId;
        Dictionary<string, DOTWBookingData> dotwStaticData = new Dictionary<string, DOTWBookingData>();
        #endregion

        #region Constructor
        /// <summary>
        /// Constrcutor for DOTW
        /// </summary>
        public DOTWApi()
        {
            if (ConfigurationSystem.DOTWConfig["TestMode"].Equals("true"))
            {
                testMode = true;
            }
            else
            {
                testMode = false;
            }
            if (testMode)
            {
                reqURL = ConfigurationSystem.DOTWConfig["RequestURL"];
            }
            else
            {
                reqURL = ConfigurationSystem.DOTWConfig["RequestProdURL"];
            }
            userName = ConfigurationSystem.DOTWConfig["UserName"];
            password = ConfigurationSystem.DOTWConfig["Password"];
            companyCode = ConfigurationSystem.DOTWConfig["CompanyCode"];
            md5Password = Converter(password);
            ourRefNo = "CZ" + DateTime.Now.ToString("ddMMyyyyHHmmss");
        }

        public DOTWApi(string sessionId)
            : this()
        {
            this.sessionId = sessionId;
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        #endregion

        #region Internal Class
        internal class DOTWBookingData
        {
            internal HotelStaticData staticInfo = new HotelStaticData();
            internal Dictionary<string, Dictionary<string, decimal>> cancelDetails = new Dictionary<string, Dictionary<string, decimal>>();
            internal DOTWBookingData()
            {
            }
        }
        #endregion

        #region Public Function
        /// <summary>
        /// This method will get all the available hotels from DOTW as per searched criteria
        /// </summary>
        /// <param name="req">HotelRequest Object</param>
        /// <returns>Array List of HotelSearchResult</returns>
        public HotelSearchResult[] GetHotelAvailability(HotelRequest req)
        {
            string resp = string.Empty;
            string request = string.Empty;

            //Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "DOTWApi.GetHotelAvailability Entered", "0");

            HotelSearchResult[] searchRes = new HotelSearchResult[0];

            #region Generate Request
            try
            {
                request = GenerateAvailabilityRequest(req);
                //Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "request message generated", "0");

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_searchHotelREQ.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(request);
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
            }
            catch (Exception Ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned in creation of XML request" + Ex.Message, "");
                Trace.TraceError("Error: " + Ex.Message);
                throw new BookingEngineException("Error: " + Ex.Message);
            }
            #endregion

            #region Comments
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load("C:\\Documents and Settings\\AnandG\\Desktop\\resp_dotw.xml");
            //resp = xmlDoc.InnerXml;
            #endregion

            #region Get Resposne from DOTW
            try
            {
                TimeSpan ts = new TimeSpan();
                DateTime time = DateTime.Now;
                resp = GetResponse(request);
                DateTime time1 = DateTime.Now;
                ts = time1 - time;
               // Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "DOTW search result returned in " + ts.Milliseconds + "Milliseconds", string.Empty);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetHotelAvailability Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            #endregion

            #region Generate HotelSearchResult Object
            try
            {
                searchRes = GenerateSearchResult(resp, req);
            }
            catch (Exception ex)
            {

                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetHotelAvailability Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);

            }
            #endregion

            CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, " Response from DOTW for Search Request:| " + DateTime.Now + "| request XML:" + request + "|response XML:" + resp, "");
            return searchRes;
        }

        public HotelSearchResult[] GetHotelAvailabilityV2(HotelRequest req)
        {
            string resp = string.Empty;
            string request = string.Empty;

            Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "DOTWApi.GetHotelAvailability Entered", "0");

            HotelSearchResult[] searchRes = new HotelSearchResult[0];
            List<HotelSearchResult> results = new List<HotelSearchResult>();
            int records = 50, rowsCount = 0, startIndex = 0, endIndex = 0;
            DataTable dtHotels =HotelStaticData.GetStaticHotelIds(req.CityCode);


            rowsCount = dtHotels.Rows.Count;
            TimeSpan ts = new TimeSpan();
            DateTime time = DateTime.Now;
            while (rowsCount > 0)
            {
                #region Generate Request
                try
                {
                    if (records <= 50)
                    {
                        startIndex = 0;
                        endIndex = records;
                    }
                    else
                    {
                        startIndex += 50;
                        endIndex = records;
                    }
                    request = GenerateAvailabilityRequestV2(req, startIndex, endIndex, dtHotels);
                    Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "request message generated", "0");

                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + "searchHotelREQ_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(request);
                        doc.Save(filePath);
                        Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                    }
                }
                catch (Exception Ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned in creation of XML request" + Ex.Message, "");
                    Trace.TraceError("Error: " + Ex.Message);
                    throw new BookingEngineException("Error: " + Ex.Message);
                }
                #endregion

                #region Comments
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load("C:\\Documents and Settings\\AnandG\\Desktop\\resp_dotw.xml");
                //resp = xmlDoc.InnerXml;
                #endregion

                #region Get Resposne from DOTW
                try
                {                    
                    resp = GetResponse(request);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetHotelAvailability Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                    Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                #endregion

                #region Generate HotelSearchResult Object
                try
                {
                    GenerateSearchResultV2(resp, req, ref results, ref dtHotels);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetHotelAvailability Error Message:" + ex.ToString() + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                    Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                #endregion

                rowsCount -= 50;
                records += 50;
            }
            DateTime time1 = DateTime.Now;
            ts = time1 - time;
            Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "DOTW search result returned in " + ts.Milliseconds + "Milliseconds", string.Empty);
            
            {
                searchRes = results.ToArray();
            }
            CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, " Response from DOTW for Search Request:| " + DateTime.Now + "| request XML:" + request + "|response XML:" + resp, "");
            return searchRes;
        }

        /// <summary>
        /// get the booking done for the created itinerary
        /// </summary>
        /// <param name="itinerary">Hotel Itinerary Object</param>
        /// <returns>BookingResponse Object</returns>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTWApi.GetBooking entered");
            string requestXml = string.Empty;
            BookingResponse searchRes = new BookingResponse();

            //First block the rooms before booking
            if(!BlockRooms(itinerary, false))
            {
                searchRes.Error = "DOTW could not block rooms";
                searchRes.Status = BookingResponseStatus.Failed;
                CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetBooking Error Message: could not block rooms|" + DateTime.Now + " for Hotel " + itinerary.HotelName, "1");
                Trace.TraceError("Error:could not block rooms");
                throw new BookingEngineException("DOTW Error:could not block rooms");
            }
            //If blocked then proceed to book
            requestXml = GenerateBookingRequest(itinerary);
            string resp = string.Empty;

            try
            {
                // Send request xml and get booking response

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True") // To Remove ziya
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_confirmbookingREQ.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestXml);
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                resp = SendGetRequest(requestXml);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True") // To Remove ziya
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_confirmbookingRES.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(resp);
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetBooking Error Message:" + ex.Message + " | Request XML:" + requestXml + "| Response XML :" + resp + DateTime.Now, "");
                Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Booking Response from DOTW. Response:" + resp + " | Request XML:" + requestXml + DateTime.Now, "");

            // Process the Response.
            try
            {
                searchRes = ReadResponseBooking(resp, ref itinerary);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetBooking Error Message:" + ex.Message + " | Request XML:" + requestXml + "| Response XML :" + resp + DateTime.Now, "");
                Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }           
            Trace.TraceInformation("DOTWApi.GetBooking exiting");

            return searchRes;
        }

        /// <summary>
        /// cancel a existing booking
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <returns>Dictionary Object </returns>
        public Dictionary<string, string> CancelBooking(HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTWApi.CancelBooking entered");
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            Dictionary<string, string> cancelData;
            decimal penalty = 0;




            string[] confNo;
            decimal amount = 0;
            confNo = itinerary.BookingRefNo.Split('|');
            for (int i = 0; i < itinerary.NoOfRooms; i++)
            {
                if (itinerary.PenalityInfo.Count > 0)
                {
                    penalty = itinerary.PenalityInfo[i + 1].Price;
                }
                string request = GenerateCancelBookingRequest(confNo[i], "no", penalty);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True") // To Remove ziya
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_cancelbookingREQ.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(request);
                    doc.Save(filePath);
                    Audit.Add(EventType.CancellBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                string resp = string.Empty;
                try
                {
                    resp = SendGetRequest(request);
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True") // To Remove ziya
                    {
                        string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_CancelbookingRES.xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(resp);
                        doc.Save(filePath);
                        Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                    }
                    penalty = ReadPenaltyResponse(resp);

                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWCancel, CT.Core.Severity.High, 0, "Exception returned from DOTWApi.CancelBooking (while reading penalty value) Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                    Trace.TraceError("Error: " + ex.Message);
                    throw new ArgumentException("Error: " + ex.Message);
                }
                request = GenerateCancelBookingRequest(confNo[i], "yes", penalty);
                
                cancelData = new Dictionary<string, string>();
                try
                {
                    resp = SendGetRequest(request);
                    cancelData = ReadCancelBookingResponse(resp, itinerary);

                    #region Check Cancel Status
                    if (cancelData["Status"] == "Cancelled" && i == 0)
                    {
                        cancelInfo = cancelData;
                        amount = Convert.ToDecimal(cancelInfo["Amount"]);
                        CT.Core.Audit.Add(CT.Core.EventType.DOTWCancel, CT.Core.Severity.High, 0, "Booking successfully cancelled for " + confNo + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                    }
                    else if (cancelData["Status"] == "Cancelled" && i > 0)
                    {
                        amount = amount + Convert.ToDecimal(cancelData["Amount"]);
                        cancelInfo["Amount"] = amount.ToString();
                        CT.Core.Audit.Add(CT.Core.EventType.DOTWCancel, CT.Core.Severity.High, 0, "Booking successfully cancelled for " + confNo + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                    }
                    else if (cancelData["Status"] != "Cancelled")
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.DOTWCancel, CT.Core.Severity.High, 0, "Exception returned from DOTWApi.CancelBooking Error Message: Booking Partially cancelled. Cancellation failed for " + confNo + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWCancel, CT.Core.Severity.High, 0, "Exception returned from DOTWApi.CancelBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                    Trace.TraceError("Error: " + ex.Message);
                    throw new BookingEngineException("Error: " + ex.Message);
                }
                finally
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWCancel, CT.Core.Severity.High, 0, "DOTWApi.CancelBooking : request XML" + request + "|response XML" + resp + " " + DateTime.Now, "");
                }
            }
            return cancelInfo;
        }

        /// <summary>
        /// This public methods is used for getting the cancellation policy for the rooms
        /// </summary>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <param name="penalityList">Hotel Penality List</param>
        /// <param name="savePenality">Boolean</param>
        /// <returns>Double array Dictionary Object</returns>
        public Dictionary<string, string> GetCancellationPolicy(HotelItinerary itinerary, ref List<HotelPenality> penalityList, bool savePenality)
        {
            Trace.TraceInformation("DOTW.GetHotelChargeCondition entered");
            Dictionary<string, string> polList = new Dictionary<string, string>();
            string message = string.Empty;
            string lastCancellation = string.Empty;
            string hotelPolicy = string.Empty;
            object obj = GetFromBasket("DOTWBookingData");
            if (obj != null)
            {
                Dictionary<string, DOTWBookingData> dotwCancelData = (Dictionary<string, DOTWBookingData>)obj;
                if (dotwCancelData.ContainsKey(itinerary.HotelCode))
                {
                    DOTWBookingData dotwData = dotwCancelData[itinerary.HotelCode];
                    for (int j = 0; j < itinerary.Roomtype.Length; j++)
                    {
                        if (j > 0)
                        {
                            if (!message.Contains(itinerary.Roomtype[j].RoomName))
                            {
                                //message += "#!#";
                            }
                            else
                            {
                                continue;
                            }
                        }
                        message += itinerary.Roomtype[j].RoomName;
                        Dictionary<string, decimal> cancelData = dotwData.cancelDetails[itinerary.Roomtype[j].RoomTypeCode];
                        decimal cancelMarkup = 0;
                        if (ConfigurationSystem.DOTWConfig.ContainsKey("CancellationMarkup"))
                        {
                            cancelMarkup = Convert.ToDecimal(ConfigurationSystem.DOTWConfig["CancellationMarkup"]);
                        }
                        double buffer = 0;
                        if (ConfigurationSystem.DOTWConfig.ContainsKey("Buffer"))
                        {
                            buffer = Convert.ToDouble(ConfigurationSystem.DOTWConfig["Buffer"]);
                        }
                        string symbol = string.Empty;
                        symbol = HotelCity.GetCurrencySymbol(itinerary.Roomtype[0].Price.Currency.ToUpper());
                        DateTime fromDate = DateTime.Now;
                        DateTime toDate = DateTime.Now;
                        string[] dates;
                        int i = 0;

                        #region foreach (KeyValuePair<string, decimal> pair in cancelData)
                        foreach (KeyValuePair<string, decimal> pair in cancelData)
                        {
                            dates = pair.Key.Split('|');
                            fromDate = Convert.ToDateTime(dates[0]);
                            toDate = Convert.ToDateTime(dates[1]);

                            if (fromDate == toDate) // Noshow polic case
                            {
                                message += " No Show Charge : " + pair.Value;
                            }
                            else if (fromDate == DateTime.MinValue ) // Not cancellable case
                            {
                                if (!message.Contains("Uncancellable"))
                                {
                                    
                                    {
                                        message += " Uncancellable: Full Cost will be charged from " + itinerary.StartDate.ToString("dd MMM yyyy HH:mm:ss") + "|";
                                    }
                                }
                            }
                            else
                            {
                                #region assuming the first rule will have cancellation penality 0
                                if (i == 0)
                                {
                                    if (pair.Value == 0)
                                    {
                                        lastCancellation = Convert.ToString(itinerary.StartDate.Subtract(toDate.Date).Days);
                                    }
                                    else
                                    {
                                        lastCancellation = "0";
                                    }
                                    toDate = toDate.AddDays(-buffer);
                                    if (toDate.DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        toDate = toDate.AddDays(-1);
                                    }
                                    else if (toDate.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        toDate = toDate.AddDays(-2);
                                    }
                                }
                                else if (i == 1)
                                {
                                    fromDate = fromDate.AddDays(-buffer);
                                    if (fromDate.DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        fromDate = fromDate.AddDays(-1);
                                    }
                                    else if (fromDate.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        fromDate = fromDate.AddDays(-2);
                                    }
                                }
                                #endregion
                                if (toDate <= fromDate)
                                {
                                    message += "Amount of " + symbol + " " + Math.Round((pair.Value + (pair.Value * cancelMarkup / 100)), 2) + " will be charged till " + toDate.ToString("dd MMM yyyy HH:mm:ss") + "|";
                                }
                                else
                                {
                                    message += "Amount of " + symbol + " " + Math.Round((pair.Value + (pair.Value * cancelMarkup / 100)), 2) + " will be charged from " + fromDate.ToString("dd MMM yyyy HH:mm:ss") + " to " + toDate.ToString("dd MMM yyyy HH:mm:ss") + "|";
                                }
                            }
                            HotelPenality hp = new HotelPenality();
                            hp.IsAllowed = true;
                            hp.BookingSource = HotelBookingSource.DOTW;
                            hp.Price = pair.Value;
                            if (fromDate == DateTime.MinValue)
                            {
                                hp.FromDate = DateTime.Now;
                            }
                            else
                            {
                                hp.FromDate = fromDate;
                            }
                            hp.ToDate = toDate;
                            hp.Remarks = "1|penaltiy value stored without markup.";
                            hp.PolicyType = ChangePoicyType.Other;
                            penalityList.Add(hp);
                            i++;
                        }
                        #endregion
                    }
                    for (int j = 0; j < itinerary.Roomtype.Length; j++)
                    {
                        if (message.Contains(itinerary.Roomtype[j].RoomName))
                        {
                            message = message.Replace(itinerary.Roomtype[j].RoomName, "");
                        }
                    }

                    hotelPolicy = dotwData.staticInfo.HotelPolicy;
                    polList.Add("lastCancellationDate", lastCancellation);
                    polList.Add("CancelPolicy", message);
                    polList.Add("HotelPolicy", hotelPolicy);
                }
                for (int r = 1; r < itinerary.Roomtype.Length; r++)
                {
                    DateTime fromDate = DateTime.Now;
                    DateTime toDate = DateTime.Now;
                    string[] dates;
                    if (dotwCancelData.ContainsKey(itinerary.HotelCode))
                    {
                        DOTWBookingData dotwData = dotwCancelData[itinerary.HotelCode];
                        Dictionary<string, decimal> cancelData = dotwData.cancelDetails[itinerary.Roomtype[r].RoomTypeCode];
                        foreach (KeyValuePair<string, decimal> pair in cancelData)
                        {
                            dates = pair.Key.Split('|');
                            fromDate = Convert.ToDateTime(dates[0]);
                            toDate = Convert.ToDateTime(dates[1]);
                            HotelPenality hp = new HotelPenality();
                            hp.IsAllowed = true;
                            hp.BookingSource = HotelBookingSource.DOTW;
                            hp.Price = pair.Value;
                            if (fromDate == DateTime.MinValue)
                            {
                                hp.FromDate = DateTime.Now;
                            }
                            else
                            {
                                hp.FromDate = fromDate;
                            }
                            hp.ToDate = toDate;
                            hp.Remarks = Convert.ToString(r + 1) + "|penaltiy value stored without markup.";
                            hp.PolicyType = ChangePoicyType.Other;
                            penalityList.Add(hp);
                        }
                    }
                }
            }


            Trace.TraceInformation("DOTW.GetHotelChargeCondition exiting");
            return polList;
        }

        /// <summary>
        /// This method is used to get the static information about the hotel like name ,pictures etc.
        /// </summary>
        /// <param name="cityCode">String CityCode</param>
        /// <param name="itemName">String ItemName</param>
        /// <param name="itemCode">String ItemCode</param>
        /// <returns>HotelDetails Object</returns>
        public HotelDetails GetItemInformation(string cityCode, string itemName, string itemCode)
        {
            bool imageUpdate = true;
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData hotelSData = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();
            try
            {
                hotelSData.Load(itemCode, cityCode, HotelBookingSource.DOTW);
                imageInfo.Load(itemCode, cityCode, HotelBookingSource.DOTW);
                if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                {
                    imageUpdate = false;
                }
                object obj = GetFromBasket("DOTWBookingData");

                #region Save or Update Image
                if (obj != null)
                {
                    Dictionary<string, DOTWBookingData> dotwCancelData = (Dictionary<string, DOTWBookingData>)obj;
                    if (dotwCancelData.ContainsKey(itemCode))
                    {
                        DOTWBookingData dotwData = dotwCancelData[itemCode];
                        //Need to use Hotel Map Co-ordinates stored previously from search
                        hotelSData = dotwData.staticInfo;
                        if (string.IsNullOrEmpty(hotelSData.HotelName))
                        {
                            hotelSData = dotwData.staticInfo;
                            if (imageUpdate && !string.IsNullOrEmpty(hotelSData.HotelPicture))
                            {

                                imageInfo.HotelCode = itemCode;
                                imageInfo.Source = HotelBookingSource.DOTW;
                                imageInfo.CityCode = cityCode;
                                imageInfo.Images = hotelSData.HotelPicture;
                                //download the images from the server
                                string imageData = string.Empty;

                                imageData = imageInfo.DownloadImage(imageInfo.Images, HotelBookingSource.DOTW);
                                imageInfo.DownloadedImgs = imageData;
                                try
                                {
                                    imageInfo.Update();
                                }
                                catch (Exception ex)
                                {
                                    Trace.TraceError("Error: " + ex.Message);
                                    throw new BookingEngineException("Error: " + ex.Message);
                                }
                            }

                            hotelSData.Save();
                        }
                        else if (imageUpdate && !string.IsNullOrEmpty(hotelSData.HotelPicture))
                        {
                            imageInfo.HotelCode = itemCode;
                            imageInfo.Source = HotelBookingSource.DOTW;
                            imageInfo.CityCode = cityCode;
                            imageInfo.Images = hotelSData.HotelPicture;
                            //download the images from the server
                            string imageData = imageInfo.DownloadImage(imageInfo.Images, HotelBookingSource.DOTW);
                            imageInfo.DownloadedImgs = imageData;
                            imageInfo.Save();
                        }

                    }
                }
                #endregion
                else
                {
                    hotelSData = GetHotelDetails(itemCode, itemName, cityCode);
                    hotelSData.CityCode = cityCode;
                    hotelSData.Save();
                }
                hotelInfo.HotelCode = hotelSData.HotelCode;
                hotelInfo.HotelName = hotelSData.HotelName;
                hotelInfo.Address = hotelSData.HotelAddress;
                hotelInfo.Description = hotelSData.HotelDescription;
                hotelInfo.FaxNumber = hotelSData.FaxNumber;
                hotelInfo.PhoneNumber = hotelSData.PhoneNumber;
                hotelInfo.Map = hotelSData.HotelMap;
                if (!string.IsNullOrEmpty(hotelSData.HotelFacilities))
                {
                    string[] facilities = hotelSData.HotelFacilities.Split('|');
                    hotelInfo.HotelFacilities = new List<string>(facilities);
                }
                else
                {
                    hotelInfo.HotelFacilities = new List<string>();
                }
                if (!string.IsNullOrEmpty(hotelSData.SpecialAttraction))
                {
                    hotelInfo.Attractions = new Dictionary<string, string>();
                    string[] splAttractions = hotelSData.SpecialAttraction.Split('|');
                    for (int i = 0; i < splAttractions.Length - 1; i++)
                    {
                        string[] attraction = splAttractions[i].Split('#');
                        if(!hotelInfo.Attractions.ContainsKey(attraction[0]))
                            hotelInfo.Attractions.Add(attraction[0], attraction[1]);
                    }
                }
                else
                {
                    hotelInfo.Attractions = new Dictionary<string, string>();
                }
                if (imageInfo.DownloadedImgs != null)
                {
                    string[] dwldedImages = (imageInfo.DownloadedImgs.Length > 0 ? imageInfo.DownloadedImgs.Split('|') : imageInfo.Images.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries));
                    hotelInfo.Images = new List<string>(dwldedImages);
                }
                else
                {
                    hotelInfo.Images = new List<string>();
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW GetItemInfo. exception:" + ex.Message + DateTime.Now, "");
                Trace.TraceInformation("DOTW GetItemInformation error." + ex);
            }
            return hotelInfo;

        }

        /// <summary>
        /// import the booked itinerary if exsit on server
        /// </summary>
        /// <param name="itinerary">BookingrefNo</param>
        /// <returns></returns>
        public bool GetBookedHotel(ref HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTW.GetBookedHotel entered");
            bool itemFound = false;

            string request = GenerateGetBookedHotelRequest(itinerary);
            string resp = string.Empty;
            try
            {
                resp = SendGetRequest(request);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWImport, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetBookedHotel Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
            }
            try
            {
                itemFound = ReadGetBookedHotelResponse(resp, ref itinerary);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWImport, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetBookedHotel Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }
            finally
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWImport, CT.Core.Severity.High, 0, " Response from DOTW for Import Request:| " + DateTime.Now + "| request XML:" + request + "|response XML:" + resp, "");
            }
            Trace.TraceInformation("DOTW.GetBookedHotel exited");
            return itemFound;
        }

        #endregion

        #region Private Function

        #region Availability
        /// <summary>
        /// This private function is used for generating the Request of DOTW
        /// </summary>
        /// <param name="req">HotelRequest Object</param>
        /// <returns>String XML Request</returns>
        private string GenerateAvailabilityRequest(HotelRequest req)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteElementString("product", "hotel");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "searchhotels");

            xmlString.WriteStartElement("bookingDetails");
            xmlString.WriteElementString("fromDate", req.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("toDate", req.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("currency", "366"); //AED
            xmlString.WriteStartElement("rooms");
            for (int i = 0; i < req.RoomGuest.Length; i++)
            {
                xmlString.WriteStartElement("room");
                xmlString.WriteAttributeString("runno", i.ToString());
                xmlString.WriteElementString("adultsCode", req.RoomGuest[i].noOfAdults.ToString());
                xmlString.WriteStartElement("children");
                xmlString.WriteAttributeString("no", req.RoomGuest[i].noOfChild.ToString());
                for (int c = 0; c < req.RoomGuest[i].noOfChild; c++)
                {
                    xmlString.WriteStartElement("child");
                    xmlString.WriteAttributeString("runno", c.ToString());
                    xmlString.WriteString(req.RoomGuest[i].childAge[c].ToString());
                    xmlString.WriteEndElement();
                }
                xmlString.WriteEndElement();
                xmlString.WriteElementString("extraBed", "0");
                xmlString.WriteElementString("rateBasis", "-1");
                //----------------- Changes for DOTW Version 2 ----------------------------//               
                
                string countryName = req.PassengerNationality;
                string countryCode = req.PassengerCountryOfResidence;

                xmlString.WriteElementString("passengerNationality", countryName);
                xmlString.WriteElementString("passengerCountryOfResidence", countryCode);
                //------------------------- End Changes----------------------------------//
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("return");
            xmlString.WriteElementString("getRooms", "true");

            xmlString.WriteStartElement("filters");
            xmlString.WriteElementString("city", req.CityCode);
            //----------------- Changes for DOTW Version 2 ----------------------------//
            xmlString.WriteElementString("nearbyCities", "false");//Not mandatory           
            xmlString.WriteElementString("noPrice", "false");//Not mandatory
            xmlString.WriteElementString("shortFormat", "false");//Not mandatory
            xmlString.WriteElementString("specialDeals", "false");//Not mandatory
            xmlString.WriteElementString("topDeals", "false");//Not mandatory
            //xmlString.WriteStartElement("rateTypes");//Not mandatory
            //xmlString.WriteElementString("rateType", "1"); //Possible values = 1 - DOTW Rate Type, 2 - DYNAMIC DIRECT Rate type, 3 - DYNAMIC 3RD PARTY Rate type
            //xmlString.WriteEndElement();
            //----------------- Changes for DOTW Version 2 ----------------------------//
            if (req.HotelName.Trim().Length > 0)
            {
                xmlString.WriteStartElement("c", "condition", "http://us.dotwconnect.com/xsd/complexCondition");
                xmlString.WriteStartElement("a", "condition", "http://us.dotwconnect.com/xsd/atomicCondition");
                
                xmlString.WriteElementString("fieldName", "hotelName");
                xmlString.WriteElementString("fieldTest", "like");
                xmlString.WriteStartElement("fieldValues");
                xmlString.WriteElementString("fieldValue", req.HotelName);
                xmlString.WriteEndElement();//fieldValues
                xmlString.WriteEndElement();//a:condition
                xmlString.WriteEndElement();//c:condition
            }
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("fields");
            xmlString.WriteElementString("field", "builtYear");
            xmlString.WriteElementString("field", "renovationYear");
            xmlString.WriteElementString("field", "floors");
            xmlString.WriteElementString("field", "noOfRooms");
            xmlString.WriteElementString("field", "fullAddress");
            xmlString.WriteElementString("field", "description1");
            xmlString.WriteElementString("field", "description2");
            xmlString.WriteElementString("field", "hotelName");
            xmlString.WriteElementString("field", "location");
            xmlString.WriteElementString("field", "location1");
            xmlString.WriteElementString("field", "location2");
            xmlString.WriteElementString("field", "location3");
            xmlString.WriteElementString("field", "stateName");
            xmlString.WriteElementString("field", "attraction");
            xmlString.WriteElementString("field", "amenitie");
            xmlString.WriteElementString("field", "leisure");
            xmlString.WriteElementString("field", "business");
            xmlString.WriteElementString("field", "transportation");
            xmlString.WriteElementString("field", "hotelPhone");
            xmlString.WriteElementString("field", "hotelCheckIn");
            xmlString.WriteElementString("field", "hotelCheckOut");
            xmlString.WriteElementString("field", "minAge");
            xmlString.WriteElementString("field", "rating");
            xmlString.WriteElementString("field", "images");
            xmlString.WriteElementString("field", "fireSafety");
            xmlString.WriteElementString("field", "geoPoint");
            xmlString.WriteEndElement();

            xmlString.WriteEndElement();


            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        private string GenerateAvailabilityRequestV2(HotelRequest req, int startIndex, int endIndex,DataTable dtHotels)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteElementString("product", "hotel");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "searchhotels");

            xmlString.WriteStartElement("bookingDetails");
            xmlString.WriteElementString("fromDate", req.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("toDate", req.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("currency", "366"); //AED
            xmlString.WriteStartElement("rooms");
            for (int i = 0; i < req.RoomGuest.Length; i++)
            {
                xmlString.WriteStartElement("room");
                xmlString.WriteAttributeString("runno", i.ToString());
                xmlString.WriteElementString("adultsCode", req.RoomGuest[i].noOfAdults.ToString());
                xmlString.WriteStartElement("children");
                xmlString.WriteAttributeString("no", req.RoomGuest[i].noOfChild.ToString());
                for (int c = 0; c < req.RoomGuest[i].noOfChild; c++)
                {
                    xmlString.WriteStartElement("child");
                    xmlString.WriteAttributeString("runno", c.ToString());
                    xmlString.WriteString(req.RoomGuest[i].childAge[c].ToString());
                    xmlString.WriteEndElement();
                }
                xmlString.WriteEndElement();
                xmlString.WriteElementString("extraBed", "0");
                xmlString.WriteElementString("rateBasis", "-1");
                //----------------- Changes for DOTW Version 2 ----------------------------//               

                string countryName = req.PassengerNationality;
                string countryCode = req.PassengerCountryOfResidence;

                xmlString.WriteElementString("passengerNationality", countryName);
                xmlString.WriteElementString("passengerCountryOfResidence", countryCode);
                //------------------------- End Changes----------------------------------//
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("return");
            xmlString.WriteElementString("getRooms", "true");

            xmlString.WriteStartElement("filters");

            xmlString.WriteElementString("city", req.CityCode);
            //----------------- Changes for DOTW Version 2 ----------------------------//
            xmlString.WriteElementString("nearbyCities", "false");//Not mandatory           
            xmlString.WriteElementString("noPrice", "false");//Not mandatory
            xmlString.WriteElementString("shortFormat", "false");//Not mandatory
            xmlString.WriteElementString("specialDeals", "false");//Not mandatory
            xmlString.WriteElementString("topDeals", "false");//Not mandatory
            //xmlString.WriteStartElement("rateTypes");//Not mandatory
            //xmlString.WriteElementString("rateType", "1"); //Possible values = 1 - DOTW Rate Type, 2 - DYNAMIC DIRECT Rate type, 3 - DYNAMIC 3RD PARTY Rate type
            //xmlString.WriteEndElement();
            //----------------- Changes for DOTW Version 2 ----------------------------//
            //if (req.HotelName.Trim().Length > 0)
            {
                xmlString.WriteStartElement("c", "condition", "http://us.dotwconnect.com/xsd/complexCondition");
                xmlString.WriteStartElement("a", "condition", "http://us.dotwconnect.com/xsd/atomicCondition");

                xmlString.WriteElementString("fieldName", "hotelId");
                xmlString.WriteElementString("fieldTest", "in");
                xmlString.WriteStartElement("fieldValues");
                for (int i = startIndex; i < endIndex; i++)
                {
                    if (i < dtHotels.Rows.Count && dtHotels.Rows[i]["HotelCode"] != DBNull.Value)
                    {
                        xmlString.WriteElementString("fieldValue", dtHotels.Rows[i]["HotelCode"].ToString().Trim());
                    }
                }
                xmlString.WriteEndElement();//fieldValues
                xmlString.WriteEndElement();//a:condition
                xmlString.WriteEndElement();//c:condition
            }
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("fields");
            //xmlString.WriteElementString("field", "builtYear");
            //xmlString.WriteElementString("field", "renovationYear");
            //xmlString.WriteElementString("field", "floors");
            //xmlString.WriteElementString("field", "noOfRooms");
            //xmlString.WriteElementString("field", "fullAddress");
            //xmlString.WriteElementString("field", "description1");
            //xmlString.WriteElementString("field", "description2");
            xmlString.WriteElementString("field", "hotelName");
            //xmlString.WriteElementString("field", "location");
            //xmlString.WriteElementString("field", "location1");
            //xmlString.WriteElementString("field", "location2");
            //xmlString.WriteElementString("field", "location3");
            //xmlString.WriteElementString("field", "stateName");
            //xmlString.WriteElementString("field", "attraction");
            //xmlString.WriteElementString("field", "amenitie");
            //xmlString.WriteElementString("field", "leisure");
            //xmlString.WriteElementString("field", "business");
            //xmlString.WriteElementString("field", "transportation");
            //xmlString.WriteElementString("field", "hotelPhone");
            xmlString.WriteElementString("field", "hotelCheckIn");
            xmlString.WriteElementString("field", "hotelCheckOut");
            xmlString.WriteElementString("field", "minAge");
            //xmlString.WriteElementString("field", "rating");
            xmlString.WriteElementString("field", "images");
            //xmlString.WriteElementString("field", "fireSafety");
            //xmlString.WriteElementString("field", "geoPoint");
            xmlString.WriteElementString("roomField", "name");
            xmlString.WriteElementString("roomField", "minStay");
            xmlString.WriteElementString("roomField", "roomAmenities");
            
            xmlString.WriteEndElement();

            xmlString.WriteEndElement();


            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        //private DataTable GetStaticHotelIds(string cityCode)
        //{
        //    DataTable dtHotels = new DataTable();

        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[2];
        //        paramList[0] = new SqlParameter("@P_CityCode", cityCode);
        //        paramList[1] = new SqlParameter("@P_Source", (int)HotelBookingSource.DOTW);
        //        dtHotels = CT.TicketReceipt.DataAccessLayer.DBGateway.FillDataTableSP("usp_GetHotelIds", paramList);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return dtHotels;
        //}

        private HotelSearchResult[] GenerateSearchResult(string resp, HotelRequest req)
        {
            Trace.TraceInformation("DOTWApi.GenerateSearchResult entered");
            XmlNode tempNode;
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_searchHotelsRES.xml";
                xmlDoc.Save(filePath);
                Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }

            List<HotelSearchResult> hotelResults = new List<HotelSearchResult>();
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + resp, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            else
            {
                #region Currency Check
                tempNode = xmlDoc.SelectSingleNode("result/currencyShort");
                if (tempNode != null)
                {
                    if (tempNode.InnerText != "AED")
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: Currency request did not match the response currency.| " + DateTime.Now + "| Response XML" + resp, "");
                        return hotelResults.ToArray();
                    }
                }
                else
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: Currency not mentioned in response.| " + DateTime.Now + "| Response XML" + resp, "");
                    return hotelResults.ToArray();
                }
                #endregion

                XmlNodeList hotelInfo = xmlDoc.SelectNodes("result/hotels/hotel");
                if (hotelInfo.Count == 0)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: No hotel found!| " + DateTime.Now + "| Response XML" + resp, "");
                    Trace.TraceError("No Hotel Details found. Response not coming! . Response =" + resp);
                    throw new BookingEngineException("<br> No Hotel Details found!. Response not coming ");
                }
                else
                {
                    Dictionary<string, DOTWBookingData> dotwStaticData = new Dictionary<string, DOTWBookingData>();

                    #region foreach (XmlNode hInfo in hotelInfo)
                    foreach (XmlNode hInfo in hotelInfo)
                    {
                        HotelSearchResult hotelResult = new HotelSearchResult();
                        hotelResult.BookingSource = HotelBookingSource.DOTW;
                        hotelResult.AvailType = AvailabilityType.Confirmed;
                        DOTWBookingData dotwHotel = new DOTWBookingData();
                        Dictionary<string, Dictionary<string, decimal>> cancelDetails = new Dictionary<string, Dictionary<string, decimal>>();
                        hotelResult.Currency = req.Currency;
                        hotelResult.CityCode = req.CityCode;                        
                        hotelResult.HotelCode = hInfo.Attributes["hotelid"].Value;
                        tempNode = hInfo.SelectSingleNode("availability");
                        if (tempNode != null && tempNode.InnerText != "available")
                        {
                            continue;
                        }
                        HotelStaticData hotelStaticData = new HotelStaticData();

                        #region Hotel Details
                        tempNode = hInfo.SelectSingleNode("hotelName");
                        if (tempNode != null)
                        {
                            hotelResult.HotelName = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("description1/language");
                        if (tempNode != null)
                        {
                            hotelResult.HotelDescription = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("description2/language");
                        if (tempNode != null)
                        {
                            hotelResult.HotelDescription += tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("location");
                        if (tempNode != null)
                        {
                            hotelResult.HotelLocation = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("images/thumb");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPicture = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("map");
                        if (tempNode != null)
                        {
                            hotelResult.HotelMap = tempNode.InnerText;
                        }
                        else
                        {
                            hotelResult.HotelMap = string.Empty;
                        }
                        //-----Added by shiva for Showing Hotel Map-------------
                        if (hotelResult.HotelMap == string.Empty)
                        {
                            tempNode = hInfo.SelectSingleNode("geoPoint/lat");
                            if (tempNode != null)
                            {
                                hotelResult.HotelMap = tempNode.InnerText;
                            }
                            tempNode = hInfo.SelectSingleNode("geoPoint/lng");

                            if (tempNode != null)
                            {
                                hotelResult.HotelMap += "," + tempNode.InnerText;
                            }
                        }
                        //------------------------------------------------------
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelStreetAddress");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress = tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelCity");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelState");
                        if (tempNode != null && tempNode.InnerText != "")
                        {
                            hotelResult.HotelAddress += tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelCountry");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelZipCode");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += " ZipCode:-" + tempNode.InnerText;

                            //putting pincode in static data
                            hotelStaticData.PinCode = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("rating");
                        if (tempNode != null)
                        {
                            string temp = tempNode.InnerText;
                            switch (temp)
                            {
                                case "559": hotelResult.Rating = HotelRating.OneStar; hotelResult.HotelCategory = "Economy";
                                    break;
                                case "560": hotelResult.Rating = HotelRating.TwoStar; hotelResult.HotelCategory = "Budget";
                                    break;
                                case "561": hotelResult.Rating = HotelRating.ThreeStar; hotelResult.HotelCategory = "Standard";
                                    break;
                                case "562": hotelResult.Rating = HotelRating.FourStar; hotelResult.HotelCategory = "Superior";
                                    break;
                                case "563": hotelResult.Rating = HotelRating.FiveStar; hotelResult.HotelCategory = "Luxury";
                                    break;
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("hotelPhone");
                        if (tempNode != null)
                        {
                            hotelResult.HotelContactNo = tempNode.InnerText;
                        }

                        #endregion
                        #region To get only those satisfy the search conditions
                        string pattern = req.HotelName;
                        if (Convert.ToInt16(hotelResult.Rating) >= (int)req.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)req.MaxRating)
                        {
                            if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        #endregion


                        hotelResult.StartDate = req.StartDate;
                        hotelResult.EndDate = req.EndDate;

                        //Static data.
                        //Spl Attractions.
                        hotelStaticData.SpecialAttraction = string.Empty;

                        #region Transportation Details
                        XmlNodeList atrxns = hInfo.SelectNodes("transportation/airports/airport");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/rails/rail");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/subways/subway");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/cruises/cruise");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        #endregion

                        #region Attraction Item
                        atrxns = hInfo.SelectNodes("attraction/attractionItem");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                            else
                            {
                                hotelStaticData.SpecialAttraction += "|";
                            }
                        }
                        #endregion

                        #region Hotel facilities.
                        hotelStaticData.HotelFacilities = string.Empty;
                        XmlNodeList amenitie = hInfo.SelectNodes("amenitie/language/amenitieItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelStaticData.HotelFacilities += amenity.InnerText + "|";
                        }
                        amenitie = hInfo.SelectNodes("leisure/language/leisureItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelStaticData.HotelFacilities += amenity.InnerText + "|";
                        }
                        amenitie = hInfo.SelectNodes("business/language/businessItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelStaticData.HotelFacilities += amenity.InnerText + "|";
                        }
                        tempNode = hInfo.SelectSingleNode("fireSafety");
                        if (tempNode != null && tempNode.InnerText == "yes")
                        {
                            hotelStaticData.HotelFacilities += "Fire Safety|";
                        }
                        #endregion

                        #region HotelImages
                        XmlNodeList imglist = hInfo.SelectNodes("images/hotelImages/image");
                        foreach (XmlNode img in imglist)
                        {
                            tempNode = img.SelectSingleNode("url");
                            hotelStaticData.HotelPicture += tempNode.InnerText + "|";
                        }
                        #endregion

                        #region hotelStaticData
                        //Hotel Policy.
                        hotelStaticData.HotelPolicy = string.Empty;
                        tempNode = hInfo.SelectSingleNode("hotelCheckIn");
                        if (tempNode != null)
                        {
                            hotelStaticData.HotelPolicy = "Check-In Time:- " + tempNode.InnerText + "hrs.|";
                        }
                        tempNode = hInfo.SelectSingleNode("hotelCheckOut");
                        if (tempNode != null)
                        {
                            hotelStaticData.HotelPolicy += "Check-Out Time:- " + tempNode.InnerText + "hrs.|";
                        }
                        tempNode = hInfo.SelectSingleNode("minAge");
                        if (tempNode != null)
                        {
                            hotelStaticData.HotelPolicy += "Minimum age to Check-in is " + tempNode.InnerText + "|";
                        }

                        hotelStaticData.HotelName = hotelResult.HotelName;
                        hotelStaticData.HotelLocation = hotelResult.HotelLocation;
                        hotelStaticData.HotelDescription = hotelResult.HotelDescription;
                        hotelStaticData.HotelAddress = hotelResult.HotelAddress;
                        hotelStaticData.PhoneNumber = hotelResult.HotelContactNo;
                        hotelStaticData.Rating = hotelResult.Rating;
                        hotelStaticData.Source = hotelResult.BookingSource;
                        hotelStaticData.CityCode = hotelResult.CityCode;
                        hotelStaticData.HotelCode = hotelResult.HotelCode;
                        hotelStaticData.FaxNumber = string.Empty;
                        if (hotelStaticData.PinCode == null)
                        {
                            hotelStaticData.PinCode = string.Empty;
                        }
                        if (hotelStaticData.SpecialAttraction == null)
                        {
                            hotelStaticData.SpecialAttraction = string.Empty;
                        }
                        if (hotelStaticData.HotelFacilities == null)
                        {
                            hotelStaticData.HotelFacilities = string.Empty;
                        }
                        hotelStaticData.EMail = string.Empty;
                        hotelStaticData.URL = string.Empty;
                        hotelStaticData.HotelMap = hotelResult.HotelMap;
                        
                        dotwHotel.staticInfo = hotelStaticData;
                        #endregion

                        #region Hotel Rooms Details
                        XmlNodeList rmrtsInfo = hInfo.SelectNodes("rooms/room");
                        string promotion = string.Empty;
                        if (rmrtsInfo.Count >= req.RoomGuest.Length)
                        {
                            int roomNo = 1;
                            List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();
                            foreach (XmlNode rmraInfo in rmrtsInfo)
                            {
                                XmlNodeList rooms = rmraInfo.SelectNodes("roomType");
                                foreach (XmlNode room in rooms)
                                {
                                    decimal minPrice = 0;
                                    XmlNodeList rateBasis = room.SelectNodes("rateBases/rateBasis");
                                    int ic = 0;
                                    HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                                    foreach (XmlNode temp in rateBasis)
                                    {
                                        tempNode = temp.SelectSingleNode("totalMinimumSelling/text()");
                                        if (tempNode == null || string.IsNullOrEmpty(temp.InnerText))
                                        {
                                            tempNode = temp.SelectSingleNode("total/text()");
                                        }
                                        if (tempNode != null)
                                        {
                                            MatchCollection matches = regex.Matches(tempNode.InnerText);
                                            if (ic != 0 && Convert.ToDecimal(matches[0].Value) >= minPrice)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                //roomDetail.TotalPrice = Convert.ToDecimal(matches[0].Value);
                                                // Round off to the smallest integral value that is greater than or equal to the specified double-precision floating-point number
                                                roomDetail.TotalPrice = Math.Ceiling(Convert.ToDecimal(matches[0].Value));
                                                minPrice = roomDetail.TotalPrice;
                                            }
                                        }
                                        roomDetail.mealPlanDesc = Convert.ToString(temp.Attributes["description"].Value);
                                        roomDetail.RoomTypeCode = room.Attributes["roomtypecode"].Value;
                                        roomDetail.SequenceNo = roomNo.ToString();
                                        roomDetail.RoomTypeCode += "|" + roomDetail.SequenceNo;
                                        XmlNodeList rmAmenities = room.SelectNodes("roomAmenities/amenity");
                                        roomDetail.Amenities = new List<string>();
                                        //TO DO: SHOW THE AMENITIES FOR EACH ROOM
                                        //The code for showing the room amenities in case of DOTW is commented as suggested by Archana on 12th April 2010 

                                        foreach (XmlNode rmAmenity in rmAmenities)
                                        {
                                            roomDetail.Amenities.Add(rmAmenity.InnerText);
                                        }

                                        #region Promotion
                                        if (string.IsNullOrEmpty(promotion))
                                        {
                                            XmlNodeList spls = room.SelectNodes("specials/special");
                                            foreach (XmlNode spl in spls)
                                            {
                                                tempNode = spl.SelectSingleNode("specialName");
                                                if (tempNode != null)
                                                {
                                                    promotion = tempNode.InnerText;
                                                }
                                                else
                                                {
                                                    tempNode = spl.SelectSingleNode("stay");
                                                    if (tempNode != null)
                                                    {
                                                        promotion += "stay " + tempNode.InnerText;
                                                    }
                                                    tempNode = spl.SelectSingleNode("pay");
                                                    if (tempNode != null)
                                                    {
                                                        promotion += " pay for " + tempNode.InnerText;
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        decimal counterPrice = 0;
                                        tempNode = room.SelectSingleNode("name");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeName = tempNode.InnerText;
                                        }
                                        #region rateBases Details
                                        roomDetail.Amenities.Add(temp.Attributes["description"].Value);
                                        roomDetail.RatePlanCode = temp.Attributes["id"].Value + "|";
                                        tempNode = temp.SelectSingleNode("minStay");
                                        if (tempNode.InnerText.Length > 0)
                                        {
                                            roomDetail.Occupancy = new Dictionary<string, int>();
                                            roomDetail.Occupancy.Add("minStay", Convert.ToInt32(tempNode.InnerText));
                                        }
                                        tempNode = temp.SelectSingleNode("dateApplyMinStay");
                                        if (tempNode.InnerText.Length > 0)
                                        {
                                            roomDetail.PromoMessage = "Starting from " + Convert.ToDateTime(tempNode.InnerText).ToString("dd/MMM/yyyy") + " minimum " + roomDetail.Occupancy["minStay"] + " days required to stay.";
                                        }
                                        tempNode = temp.SelectSingleNode("isBookable");                                        
                                        if (tempNode != null && tempNode.InnerText == "yes")
                                        {
                                            tempNode = temp.SelectSingleNode("paymentMode");
                                            if (tempNode != null && tempNode.InnerText.ToUpper() == "CC")
                                            {
                                                continue;
                                            }
                                            tempNode = temp.SelectSingleNode("allocationDetails");
                                            if (tempNode != null)
                                            {
                                                roomDetail.RatePlanCode += tempNode.InnerText;
                                            }
                                            #region Cancel policies.
                                            Dictionary<string, decimal> cancelDetailsRoomWise = new Dictionary<string, decimal>();
                                            DateTime canDateFrom = DateTime.Now;
                                            DateTime canDateTo = DateTime.Now;
                                            string canDate = string.Empty;
                                            decimal charge = 0;
                                            XmlNodeList canList = temp.SelectNodes("cancellationRules/rule");
                                            foreach (XmlNode rule in canList)
                                            {
                                                if (rule.SelectSingleNode("cancelRestricted")!=null && rule.SelectSingleNode("cancelRestricted").InnerText == "true")

                                                {
                                                    canDateFrom = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                                    canDateTo = DateTime.ParseExact(req.StartDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                                    canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                                    charge = roomDetail.TotalPrice; 
                                                }
                                                else if (rule.SelectSingleNode("noShowPolicy")!= null && rule.SelectSingleNode("noShowPolicy").InnerText == "true")

                                                {
                                                    tempNode = rule.SelectSingleNode("charge/text()");
                                                    if (tempNode != null)
                                                    {
                                                        MatchCollection matches = regex.Matches(tempNode.InnerText);
                                                        charge = Convert.ToDecimal(matches[0].Value);
                                                    }
                                                    canDateFrom = DateTime.ParseExact(req.StartDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                                    canDateTo = DateTime.ParseExact(req.EndDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                                    canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                                }
                                                else
                                                {
                                                    tempNode = rule.SelectSingleNode("fromDate");
                                                    if (tempNode != null)
                                                    {
                                                        canDateFrom = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                                                    }
                                                    tempNode = rule.SelectSingleNode("toDate");
                                                    if (tempNode != null)
                                                    {
                                                        canDateTo = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                                                    }
                                                    else
                                                    {
                                                        canDateTo = req.StartDate;
                                                    }
                                                    canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                                    tempNode = rule.SelectSingleNode("charge/text()");
                                                    if (tempNode != null)
                                                    {
                                                        MatchCollection matches = regex.Matches(tempNode.InnerText);
                                                        charge = Convert.ToDecimal(matches[0].Value);
                                                    }
                                                }
                                                cancelDetailsRoomWise.Add(canDate, charge);
                  
                                            }
                                            #endregion
                                            if (cancelDetails.ContainsKey(roomDetail.RoomTypeCode))
                                            {
                                                cancelDetails[roomDetail.RoomTypeCode] = cancelDetailsRoomWise;
                                            }
                                            else
                                            {
                                                cancelDetails.Add(roomDetail.RoomTypeCode, cancelDetailsRoomWise);
                                            }
                                            roomDetail.CancellationPolicy = string.Empty;
                                            #region Tariff Notes
                                            XmlNode tariffNode = temp.SelectSingleNode("tariffNotes");
                                            if (tariffNode != null && tariffNode.InnerText.Length > 0)
                                            {
                                                if (!dotwHotel.staticInfo.HotelPolicy.Contains(tariffNode.InnerText))
                                                {
                                                    dotwHotel.staticInfo.HotelPolicy += "|" + tariffNode.InnerText;
                                                }
                                            }
                                            #endregion
                                            #region Price Details
                                            XmlNodeList tempNodeList = temp.SelectNodes("/dates/date");
                                            roomDetail.Rates = new RoomRates[tempNodeList.Count];
                                            int i = 0;
                                            counterPrice = 0;
                                            foreach (XmlNode date in tempNodeList)
                                            {
                                                roomDetail.Rates[i] = new RoomRates();
                                                roomDetail.Rates[i].Days = DateTime.ParseExact(date.Attributes["datetime"].Value, "yyyy-MM-dd", null);
                                                 tempNode = temp.SelectSingleNode("priceMinimumSelling/text()");
                                              if (tempNode == null || string.IsNullOrEmpty(temp.InnerText))
                                                {
                                                    tempNode = date.SelectSingleNode("price/text()");
                                                }
                                                MatchCollection matches = regex.Matches(tempNode.InnerText);
                                                tempNode.InnerText = matches[0].Value;
                                                roomDetail.Rates[i].Amount = Convert.ToDecimal(tempNode.InnerText);
                                                roomDetail.Rates[i].BaseFare = Convert.ToDecimal(tempNode.InnerText);
                                                roomDetail.Rates[i].SellingFare = Convert.ToDecimal(tempNode.InnerText);
                                                roomDetail.Rates[i].Totalfare = Convert.ToDecimal(tempNode.InnerText);
                                                roomDetail.Rates[i].RateType = RateType.Negotiated;
                                                counterPrice += roomDetail.Rates[i].Amount;
                                                i++;
                                            }
                                            #endregion

                                            int noOfDays = req.EndDate.Subtract(req.StartDate).Days;

                                            #region if rates break up not equal to total fare.
                                            if (roomDetail.TotalPrice != counterPrice && roomDetail.Rates.Length != noOfDays)
                                            {
                                                Array.Resize(ref roomDetail.Rates, noOfDays);
                                                for (int d = 0; d < noOfDays; d++)
                                                {
                                                    roomDetail.Rates[d] = new RoomRates();
                                                    roomDetail.Rates[d].Days = req.StartDate.AddDays(Convert.ToDouble(d));
                                                    roomDetail.Rates[d].RateType = RateType.Negotiated;
                                                    roomDetail.Rates[d].Amount = roomDetail.TotalPrice / noOfDays;
                                                    roomDetail.Rates[d].BaseFare = roomDetail.TotalPrice / noOfDays;
                                                    roomDetail.Rates[d].SellingFare = roomDetail.TotalPrice / noOfDays;
                                                    //roomDetail.Rates[d].SellingFare = Math.Ceiling(roomDetail.TotalPrice / noOfDays); commented by ziya
                                                    roomDetail.Rates[d].Totalfare = roomDetail.TotalPrice / noOfDays;

                                                    // PUTIN HIEGHR 
                                                    //roomDetail.Rates[d] = new RoomRates();
                                                    //roomDetail.Rates[d].Days = req.StartDate.AddDays(Convert.ToDouble(d));
                                                    //roomDetail.Rates[d].RateType = RateType.Negotiated;
                                                    //roomDetail.Rates[d].Amount = Math.Ceiling(roomDetail.TotalPrice / noOfDays);
                                                    //roomDetail.Rates[d].BaseFare = (roomDetail.TotalPrice / noOfDays);
                                                    ////roomDetail.Rates[d].SellingFare = roomDetail.TotalPrice / noOfDays;
                                                    //roomDetail.Rates[d].SellingFare = Math.Ceiling(roomDetail.TotalPrice / noOfDays);
                                                    //roomDetail.Rates[d].Totalfare = Math.Ceiling(roomDetail.TotalPrice / noOfDays);
                                                }
                                            }
                                            #endregion
                                        }
                                        else
                                        { continue; }
                                        ic++;
                                    }
                                    #endregion
                                    roomDetails.Add(roomDetail);
                                }
                                roomNo++;
                            }

                            //Adding cancellation policy data
                            dotwHotel.cancelDetails = cancelDetails;

                            //making the basket stuff
                            dotwStaticData.Add(hotelResult.HotelCode, dotwHotel);

                            hotelResult.RoomDetails = roomDetails.ToArray();
                            Array.Sort(hotelResult.RoomDetails, delegate(HotelRoomsDetails rd1, HotelRoomsDetails rd2) { return rd1.TotalPrice.CompareTo(rd2.TotalPrice); });
                            hotelResult.PromoMessage = promotion;
                        }
                        else
                        {
                            continue;
                        }
                        #endregion

                        AddToBasket("DOTWBookingData", dotwStaticData);
                        hotelResults.Add(hotelResult);
                    }
                    #endregion
                }
            }
            return hotelResults.ToArray();
        }

        private void GenerateSearchResultV2(string resp, HotelRequest req, ref List<HotelSearchResult> hotelResults, ref DataTable dtHotels)
        {
            Trace.TraceInformation("DOTWApi.GenerateSearchResult entered");
            XmlNode tempNode;
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + "searchHotelsRES_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml";
                xmlDoc.Save(filePath);
                Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }

            
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + resp, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            else
            {
                #region Currency Check
                tempNode = xmlDoc.SelectSingleNode("result/currencyShort");
                if (tempNode != null)
                {
                    if (tempNode.InnerText != "AED")
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: Currency request did not match the response currency.| " + DateTime.Now + "| Response XML" + resp, "");
                        //return hotelResults.ToArray();
                        return;
                    }
                }
                else
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: Currency not mentioned in response.| " + DateTime.Now + "| Response XML" + resp, "");
                    //return hotelResults.ToArray();
                    return;
                }
                #endregion

                XmlNodeList hotelInfo = xmlDoc.SelectNodes("result/hotels/hotel");
                if (hotelInfo.Count == 0)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: No hotel found!| " + DateTime.Now + "| Response XML" + resp, "");
                    Trace.TraceError("No Hotel Details found. Response not coming! . Response =" + resp);
                    throw new BookingEngineException("<br> No Hotel Details found!. Response not coming ");
                }
                else
                {
                    

                    #region foreach (XmlNode hInfo in hotelInfo)
                    foreach (XmlNode hInfo in hotelInfo)
                    {
                        HotelSearchResult hotelResult = new HotelSearchResult();
                        hotelResult.BookingSource = HotelBookingSource.DOTW;
                        hotelResult.AvailType = AvailabilityType.Confirmed;
                        DOTWBookingData dotwHotel = new DOTWBookingData();
                        Dictionary<string, Dictionary<string, decimal>> cancelDetails = new Dictionary<string, Dictionary<string, decimal>>();
                        hotelResult.Currency = req.Currency;
                        hotelResult.CityCode = req.CityCode;
                        hotelResult.HotelCode = hInfo.Attributes["hotelid"].Value;
                        tempNode = hInfo.SelectSingleNode("availability");
                        if (tempNode != null && tempNode.InnerText != "available")
                        {
                            continue;
                        }
                        HotelStaticData hotelStaticData = new HotelStaticData();

                        #region Hotel Details
                        tempNode = hInfo.SelectSingleNode("hotelName");
                        if (tempNode != null)
                        {
                            hotelResult.HotelName = tempNode.InnerText;
                        }
                        else
                        {
                            DataRow[] filteredHotels = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");//ziyad to change

                            if (filteredHotels != null && filteredHotels.Length > 0)
                            {
                                hotelResult.HotelName = filteredHotels[0]["HotelName"].ToString();
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("description1/language");
                        if (tempNode != null)
                        {
                            hotelResult.HotelDescription = tempNode.InnerText;
                        }
                        else
                        {
                            DataRow[] filteredHotels = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");

                            if (filteredHotels != null && filteredHotels.Length > 0)
                            {
                                hotelResult.HotelDescription = filteredHotels[0]["description"].ToString();
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("description2/language");
                        if (tempNode != null)
                        {
                            hotelResult.HotelDescription += tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("location");
                        if (tempNode != null)
                        {
                            hotelResult.HotelLocation = tempNode.InnerText;
                        }
                        else
                        {
                            DataRow[] filteredHotels = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");

                            if (filteredHotels != null && filteredHotels.Length > 0)
                            {
                                hotelResult.HotelLocation = filteredHotels[0]["location"].ToString();
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("images/thumb");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPicture = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("map");
                        if (tempNode != null)
                        {
                            hotelResult.HotelMap = tempNode.InnerText;
                        }
                        else
                        {
                            hotelResult.HotelMap = string.Empty;
                        }
                        //-----Added by shiva for Showing Hotel Map-------------
                        if (hotelResult.HotelMap == string.Empty)
                        {
                            tempNode = hInfo.SelectSingleNode("geoPoint/lat");
                            if (tempNode != null)
                            {
                                hotelResult.HotelMap = tempNode.InnerText;
                            }
                            tempNode = hInfo.SelectSingleNode("geoPoint/lng");

                            if (tempNode != null)
                            {
                                hotelResult.HotelMap += "," + tempNode.InnerText;
                            }
                        }
                        //------------------------------------------------------
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelStreetAddress");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress = tempNode.InnerText + " ";
                        }
                        else
                        {
                            DataRow[] filteredHotels = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");

                            if (filteredHotels != null && filteredHotels.Length > 0)
                            {
                                hotelResult.HotelAddress = filteredHotels[0]["address"].ToString();
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelCity");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelState");
                        if (tempNode != null && tempNode.InnerText != "")
                        {
                            hotelResult.HotelAddress += tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelCountry");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelZipCode");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += " ZipCode:-" + tempNode.InnerText;

                            //putting pincode in static data
                            hotelStaticData.PinCode = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("rating");
                        if (tempNode != null)
                        {
                            string temp = tempNode.InnerText;
                            switch (temp)
                            {
                                case "559": hotelResult.Rating = HotelRating.OneStar; hotelResult.HotelCategory = "Economy";
                                    break;
                                case "560": hotelResult.Rating = HotelRating.TwoStar; hotelResult.HotelCategory = "Budget";
                                    break;
                                case "561": hotelResult.Rating = HotelRating.ThreeStar; hotelResult.HotelCategory = "Standard";
                                    break;
                                case "562": hotelResult.Rating = HotelRating.FourStar; hotelResult.HotelCategory = "Superior";
                                    break;
                                case "563": hotelResult.Rating = HotelRating.FiveStar; hotelResult.HotelCategory = "Luxury";
                                    break;
                            }
                        }
                        else
                        {
                            DataRow[] filteredHotels = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");

                            if (filteredHotels != null && filteredHotels.Length > 0)
                            {
                                switch (filteredHotels[0]["hotelRating"].ToString())
                                {
                                    case "1":
                                        hotelResult.Rating = HotelRating.OneStar;
                                        break;
                                    case "2":
                                        hotelResult.Rating = HotelRating.TwoStar;
                                        break;
                                    case "3":
                                        hotelResult.Rating = HotelRating.ThreeStar;
                                        break;
                                    case "4":
                                        hotelResult.Rating = HotelRating.FourStar;
                                        break;
                                    case "5":
                                        hotelResult.Rating = HotelRating.FiveStar;
                                        break;
                                }
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("hotelPhone");
                        if (tempNode != null)
                        {
                            hotelResult.HotelContactNo = tempNode.InnerText;
                        }

                        #endregion
                        #region To get only those satisfy the search conditions
                        string pattern = req.HotelName;
                        if (Convert.ToInt16(hotelResult.Rating) >= (int)req.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)req.MaxRating)
                        {
                            if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        #endregion


                        hotelResult.StartDate = req.StartDate;
                        hotelResult.EndDate = req.EndDate;

                        //Static data.
                        //Spl Attractions.
                        hotelStaticData.SpecialAttraction = string.Empty;

                        #region Transportation Details
                        XmlNodeList atrxns = hInfo.SelectNodes("transportation/airports/airport");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/rails/rail");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/subways/subway");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/cruises/cruise");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        #endregion

                        #region Attraction Item
                        atrxns = hInfo.SelectNodes("attraction/attractionItem");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelStaticData.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelStaticData.SpecialAttraction += tempNode.InnerText + "|";
                            }
                            else
                            {
                                hotelStaticData.SpecialAttraction += "|";
                            }
                        }
                        #endregion

                        if (hotelStaticData.SpecialAttraction == null || hotelStaticData.SpecialAttraction.Length <= 0)
                        {
                            DataRow[] filteredHotels = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");

                            if (filteredHotels != null && filteredHotels.Length > 0)
                            {
                                hotelStaticData.SpecialAttraction = filteredHotels[0]["specialAttraction"].ToString();
                            }
                        }

                        #region Hotel facilities.
                        hotelStaticData.HotelFacilities = string.Empty;
                        XmlNodeList amenitie = hInfo.SelectNodes("amenitie/language/amenitieItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelStaticData.HotelFacilities += amenity.InnerText + "|";
                        }
                        amenitie = hInfo.SelectNodes("leisure/language/leisureItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelStaticData.HotelFacilities += amenity.InnerText + "|";
                        }
                        amenitie = hInfo.SelectNodes("business/language/businessItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelStaticData.HotelFacilities += amenity.InnerText + "|";
                        }
                        tempNode = hInfo.SelectSingleNode("fireSafety");
                        if (tempNode != null && tempNode.InnerText == "yes")
                        {
                            hotelStaticData.HotelFacilities += "Fire Safety|";
                        }
                        #endregion

                        if (hotelStaticData.HotelFacilities == null || hotelStaticData.HotelFacilities.Length <= 0)
                        {
                            DataRow[] filteredHotels = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");

                            if (filteredHotels != null && filteredHotels.Length > 0)
                            {
                                hotelStaticData.HotelFacilities = filteredHotels[0]["hotelFacility"].ToString();
                            }
                        }

                        #region HotelImages
                        XmlNodeList imglist = hInfo.SelectNodes("images/hotelImages/image");
                        foreach (XmlNode img in imglist)
                        {
                            tempNode = img.SelectSingleNode("url");
                            hotelStaticData.HotelPicture += tempNode.InnerText + "|";
                        }
                        #endregion

                        #region hotelStaticData
                        //Hotel Policy.
                        hotelStaticData.HotelPolicy = string.Empty;
                        tempNode = hInfo.SelectSingleNode("hotelCheckIn");
                        if (tempNode != null)
                        {
                            hotelStaticData.HotelPolicy = "Check-In Time:- " + tempNode.InnerText + "hrs.|";
                        }
                        tempNode = hInfo.SelectSingleNode("hotelCheckOut");
                        if (tempNode != null)
                        {
                            hotelStaticData.HotelPolicy += "Check-Out Time:- " + tempNode.InnerText + "hrs.|";
                        }
                        else
                        {
                            if (hotelStaticData.SpecialAttraction == null || hotelStaticData.SpecialAttraction.Length <= 0)
                            {
                                DataRow[] filteredHotels = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");

                                if (filteredHotels != null && filteredHotels.Length > 0)
                                {
                                    hotelStaticData.HotelPolicy = filteredHotels[0]["hotelPolicy"].ToString();
                                }
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("minAge");
                        if (tempNode != null)
                        {
                            hotelStaticData.HotelPolicy += "Minimum age to Check-in is " + tempNode.InnerText + "|";
                        }

                        hotelStaticData.HotelName = hotelResult.HotelName;
                        hotelStaticData.HotelLocation = hotelResult.HotelLocation;
                        hotelStaticData.HotelDescription = hotelResult.HotelDescription;
                        hotelStaticData.HotelAddress = hotelResult.HotelAddress;
                        hotelStaticData.PhoneNumber = hotelResult.HotelContactNo;
                        hotelStaticData.Rating = hotelResult.Rating;
                        hotelStaticData.Source = hotelResult.BookingSource;
                        hotelStaticData.CityCode = hotelResult.CityCode;
                        hotelStaticData.HotelCode = hotelResult.HotelCode;
                        hotelStaticData.FaxNumber = string.Empty;
                        if (hotelStaticData.PinCode == null)
                        {
                            hotelStaticData.PinCode = string.Empty;
                        }
                        if (hotelStaticData.SpecialAttraction == null)
                        {
                            hotelStaticData.SpecialAttraction = string.Empty;
                        }
                        if (hotelStaticData.HotelFacilities == null)
                        {
                            hotelStaticData.HotelFacilities = string.Empty;
                        }
                        hotelStaticData.EMail = string.Empty;
                        hotelStaticData.URL = string.Empty;
                        hotelStaticData.HotelMap = hotelResult.HotelMap;

                        dotwHotel.staticInfo = hotelStaticData;
                        #endregion

                        #region Hotel Rooms Details
                        XmlNodeList rmrtsInfo = hInfo.SelectNodes("rooms/room");
                        string promotion = string.Empty;
                        if (rmrtsInfo.Count >= req.RoomGuest.Length)
                        {
                            int roomNo = 1;
                            List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();
                            foreach (XmlNode rmraInfo in rmrtsInfo)
                            {
                                XmlNodeList rooms = rmraInfo.SelectNodes("roomType");
                                foreach (XmlNode room in rooms)
                                {
                                    decimal minPrice = 0;
                                    XmlNodeList rateBasis = room.SelectNodes("rateBases/rateBasis");
                                    int ic = 0;
                                    HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                                    foreach (XmlNode temp in rateBasis)
                                    {
                                        tempNode = temp.SelectSingleNode("totalMinimumSelling/text()");
                                        if (tempNode == null || string.IsNullOrEmpty(temp.InnerText))
                                        {
                                            tempNode = temp.SelectSingleNode("total/text()");
                                        }
                                        if (tempNode != null)
                                        {
                                            MatchCollection matches = regex.Matches(tempNode.InnerText);
                                            if (ic != 0 && Convert.ToDecimal(matches[0].Value) >= minPrice)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                //roomDetail.TotalPrice = Convert.ToDecimal(matches[0].Value);
                                                // Round off to the smallest integral value that is greater than or equal to the specified double-precision floating-point number
                                                roomDetail.TotalPrice = Math.Ceiling(Convert.ToDecimal(matches[0].Value));
                                                minPrice = roomDetail.TotalPrice;
                                            }
                                        }
                                        roomDetail.mealPlanDesc = Convert.ToString(temp.Attributes["description"].Value);
                                        roomDetail.RoomTypeCode = room.Attributes["roomtypecode"].Value;
                                        roomDetail.SequenceNo = roomNo.ToString();
                                        roomDetail.RoomTypeCode += "|" + roomDetail.SequenceNo;
                                        XmlNodeList rmAmenities = room.SelectNodes("roomAmenities/amenity");
                                        roomDetail.Amenities = new List<string>();
                                        //TO DO: SHOW THE AMENITIES FOR EACH ROOM
                                        //The code for showing the room amenities in case of DOTW is commented as suggested by Archana on 12th April 2010 

                                        foreach (XmlNode rmAmenity in rmAmenities)
                                        {
                                            roomDetail.Amenities.Add(rmAmenity.InnerText);
                                        }

                                        #region Promotion
                                        if (string.IsNullOrEmpty(promotion))
                                        {
                                            XmlNodeList spls = room.SelectNodes("specials/special");
                                            foreach (XmlNode spl in spls)
                                            {
                                                tempNode = spl.SelectSingleNode("specialName");
                                                if (tempNode != null)
                                                {
                                                    promotion = tempNode.InnerText;
                                                }
                                                else
                                                {
                                                    tempNode = spl.SelectSingleNode("stay");
                                                    if (tempNode != null)
                                                    {
                                                        promotion += "stay " + tempNode.InnerText;
                                                    }
                                                    tempNode = spl.SelectSingleNode("pay");
                                                    if (tempNode != null)
                                                    {
                                                        promotion += " pay for " + tempNode.InnerText;
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        decimal counterPrice = 0;
                                        tempNode = room.SelectSingleNode("name");
                                        if (tempNode != null)
                                        {
                                            roomDetail.RoomTypeName = tempNode.InnerText;
                                        }
                                        #region rateBases Details
                                        roomDetail.Amenities.Add(temp.Attributes["description"].Value);
                                        roomDetail.RatePlanCode = temp.Attributes["id"].Value + "|";
                                        tempNode = temp.SelectSingleNode("minStay");
                                        if (tempNode != null && tempNode.InnerText.Length > 0)
                                        {
                                            roomDetail.Occupancy = new Dictionary<string, int>();
                                            roomDetail.Occupancy.Add("minStay", Convert.ToInt32(tempNode.InnerText));
                                        }
                                        else
                                        {
                                            roomDetail.Occupancy = new Dictionary<string, int>();
                                            roomDetail.Occupancy.Add("minStay", 0);
                                        }
                                        tempNode = temp.SelectSingleNode("dateApplyMinStay");
                                        if (tempNode != null && tempNode.InnerText.Length > 0)
                                        {
                                            roomDetail.PromoMessage = "Starting from " + Convert.ToDateTime(tempNode.InnerText).ToString("dd/MMM/yyyy") + " minimum " + roomDetail.Occupancy["minStay"] + " days required to stay.";
                                        }
                                        tempNode = temp.SelectSingleNode("isBookable");
                                        if (tempNode != null && tempNode.InnerText == "yes")
                                        {
                                            tempNode = temp.SelectSingleNode("paymentMode");
                                            if (tempNode != null && tempNode.InnerText.ToUpper() == "CC")
                                            {
                                                continue;
                                            }
                                            tempNode = temp.SelectSingleNode("allocationDetails");
                                            if (tempNode != null)
                                            {
                                                roomDetail.RatePlanCode += tempNode.InnerText;
                                            }
                                            #region Cancel policies.
                                            Dictionary<string, decimal> cancelDetailsRoomWise = new Dictionary<string, decimal>();
                                            DateTime canDateFrom = DateTime.Now;
                                            DateTime canDateTo = DateTime.Now;
                                            string canDate = string.Empty;
                                            decimal charge = 0;
                                            XmlNodeList canList = temp.SelectNodes("cancellationRules/rule");
                                            foreach (XmlNode rule in canList)
                                            {
                                                if (rule.SelectSingleNode("cancelRestricted") != null && rule.SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    canDateFrom = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                                    canDateTo = DateTime.ParseExact(req.StartDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                                    canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                                    charge = roomDetail.TotalPrice;
                                                }
                                                else if (rule.SelectSingleNode("noShowPolicy") != null && rule.SelectSingleNode("noShowPolicy").InnerText == "true")
                                                {
                                                    tempNode = rule.SelectSingleNode("charge/text()");
                                                    if (tempNode != null)
                                                    {
                                                        MatchCollection matches = regex.Matches(tempNode.InnerText);
                                                        charge = Convert.ToDecimal(matches[0].Value);
                                                    }
                                                    canDateFrom = DateTime.ParseExact(req.StartDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                                    canDateTo = DateTime.ParseExact(req.EndDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                                    canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                                }
                                                else
                                                {
                                                    tempNode = rule.SelectSingleNode("fromDate");
                                                    if (tempNode != null)
                                                    {
                                                        canDateFrom = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                                                    }
                                                    tempNode = rule.SelectSingleNode("toDate");
                                                    if (tempNode != null)
                                                    {
                                                        canDateTo = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                                                    }
                                                    else
                                                    {
                                                        canDateTo = req.StartDate;
                                                    }
                                                    canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                                    tempNode = rule.SelectSingleNode("charge/text()");
                                                    if (tempNode != null)
                                                    {
                                                        MatchCollection matches = regex.Matches(tempNode.InnerText);
                                                        charge = Convert.ToDecimal(matches[0].Value);
                                                    }
                                                }
                                                cancelDetailsRoomWise.Add(canDate, charge);

                                            }
                                            #endregion
                                            if (cancelDetails.ContainsKey(roomDetail.RoomTypeCode))
                                            {
                                                cancelDetails[roomDetail.RoomTypeCode] = cancelDetailsRoomWise;
                                            }
                                            else
                                            {
                                                cancelDetails.Add(roomDetail.RoomTypeCode, cancelDetailsRoomWise);
                                            }
                                            roomDetail.CancellationPolicy = string.Empty;
                                            #region Tariff Notes
                                            XmlNode tariffNode = temp.SelectSingleNode("tariffNotes");
                                            if (tariffNode != null && tariffNode.InnerText.Length > 0)
                                            {
                                                if (!dotwHotel.staticInfo.HotelPolicy.Contains(tariffNode.InnerText))
                                                {
                                                    dotwHotel.staticInfo.HotelPolicy += "|" + tariffNode.InnerText;
                                                }
                                            }
                                            #endregion
                                            #region Price Details
                                            XmlNodeList tempNodeList = temp.SelectNodes("/dates/date");
                                            roomDetail.Rates = new RoomRates[tempNodeList.Count];
                                            int i = 0;
                                            counterPrice = 0;
                                            foreach (XmlNode date in tempNodeList)
                                            {
                                                roomDetail.Rates[i] = new RoomRates();
                                                roomDetail.Rates[i].Days = DateTime.ParseExact(date.Attributes["datetime"].Value, "yyyy-MM-dd", null);
                                                tempNode = temp.SelectSingleNode("priceMinimumSelling/text()");
                                                if (tempNode == null || string.IsNullOrEmpty(temp.InnerText))
                                                {
                                                    tempNode = date.SelectSingleNode("price/text()");
                                                }
                                                MatchCollection matches = regex.Matches(tempNode.InnerText);
                                                tempNode.InnerText = matches[0].Value;
                                                roomDetail.Rates[i].Amount = Convert.ToDecimal(tempNode.InnerText);
                                                roomDetail.Rates[i].BaseFare = Convert.ToDecimal(tempNode.InnerText);
                                                roomDetail.Rates[i].SellingFare = Convert.ToDecimal(tempNode.InnerText);
                                                roomDetail.Rates[i].Totalfare = Convert.ToDecimal(tempNode.InnerText);
                                                roomDetail.Rates[i].RateType = RateType.Negotiated;
                                                counterPrice += roomDetail.Rates[i].Amount;
                                                i++;
                                            }
                                            #endregion

                                            int noOfDays = req.EndDate.Subtract(req.StartDate).Days;

                                            #region if rates break up not equal to total fare.
                                            if (roomDetail.TotalPrice != counterPrice && roomDetail.Rates.Length != noOfDays)
                                            {
                                                Array.Resize(ref roomDetail.Rates, noOfDays);
                                                for (int d = 0; d < noOfDays; d++)
                                                {
                                                    roomDetail.Rates[d] = new RoomRates();
                                                    roomDetail.Rates[d].Days = req.StartDate.AddDays(Convert.ToDouble(d));
                                                    roomDetail.Rates[d].RateType = RateType.Negotiated;
                                                    roomDetail.Rates[d].Amount = roomDetail.TotalPrice / noOfDays;
                                                    roomDetail.Rates[d].BaseFare = roomDetail.TotalPrice / noOfDays;
                                                    roomDetail.Rates[d].SellingFare = roomDetail.TotalPrice / noOfDays;
                                                    //roomDetail.Rates[d].SellingFare = Math.Ceiling(roomDetail.TotalPrice / noOfDays); commented by ziya
                                                    roomDetail.Rates[d].Totalfare = roomDetail.TotalPrice / noOfDays;

                                                    // PUTIN HIEGHR 
                                                    //roomDetail.Rates[d] = new RoomRates();
                                                    //roomDetail.Rates[d].Days = req.StartDate.AddDays(Convert.ToDouble(d));
                                                    //roomDetail.Rates[d].RateType = RateType.Negotiated;
                                                    //roomDetail.Rates[d].Amount = Math.Ceiling(roomDetail.TotalPrice / noOfDays);
                                                    //roomDetail.Rates[d].BaseFare = (roomDetail.TotalPrice / noOfDays);
                                                    ////roomDetail.Rates[d].SellingFare = roomDetail.TotalPrice / noOfDays;
                                                    //roomDetail.Rates[d].SellingFare = Math.Ceiling(roomDetail.TotalPrice / noOfDays);
                                                    //roomDetail.Rates[d].Totalfare = Math.Ceiling(roomDetail.TotalPrice / noOfDays);
                                                }
                                            }
                                            #endregion
                                        }
                                        else
                                        { continue; }
                                        ic++;
                                    }
                                        #endregion
                                    roomDetails.Add(roomDetail);
                                }
                                roomNo++;
                            }

                            //Adding cancellation policy data
                            dotwHotel.cancelDetails = cancelDetails;

                            //making the basket stuff
                            dotwStaticData.Add(hotelResult.HotelCode, dotwHotel);

                            hotelResult.RoomDetails = roomDetails.ToArray();
                            Array.Sort(hotelResult.RoomDetails, delegate(HotelRoomsDetails rd1, HotelRoomsDetails rd2) { return rd1.TotalPrice.CompareTo(rd2.TotalPrice); });
                            hotelResult.PromoMessage = promotion;
                        }
                        else
                        {
                            continue;
                        }
                        #endregion

                        AddToBasket("DOTWBookingData", dotwStaticData);
                        hotelResults.Add(hotelResult);
                    }
                    #endregion
                }
            }            
        }
        #endregion

        #region General Function
        /// <summary>
        /// add the value if not exist otherwise replace
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool AddToBasket(string key, object value)
        {
            object v;
            bool exist;
            exist = Basket.BookingSession[sessionId].TryGetValue(key, out v);

            if (exist == true)
            {
                Basket.BookingSession[sessionId].Remove(key);
            }

            Basket.BookingSession[sessionId].Add(key, value);

            return exist;
        }

        /// <summary>
        /// This private method is used to get the value from the basket if exist otherwise null
        /// </summary>
        /// <param name="key">String Key</param>
        /// <returns>object</returns>
        private object GetFromBasket(string key)
        {
            object ret = null;
            bool exist = false;
            if (Basket.BookingSession.ContainsKey(sessionId) && Basket.BookingSession[sessionId].ContainsKey(key))
            {
                exist = Basket.BookingSession[sessionId].TryGetValue(key, out ret);
            }

            if (exist == false) return null;

            return ret;
        }

        private string Converter(string password)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(password));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// To send and get xmls from DOTW
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// 


        private string SendGetRequestNEw(string request)
        {
            if (request.Contains("<?xml version=\"1.0\" encoding=\"utf-16\"?>"))
            {
                request = request.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            }
            //System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            //Byte[] byte1 = encoding.GetBytes(request);
            HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(reqURL);

            HttpWReq.ContentType = "text/xml";
            HttpWReq.ContentLength = request.Length;
            HttpWReq.Method = "POST";

            System.IO.Stream StreamData = HttpWReq.GetRequestStream();
            StreamWriter writer = new StreamWriter(StreamData);
            writer.Write(request);
            writer.Flush();

            Stream resp = Stream.Null;
            WebResponse response = null;
            HttpWebResponse objResponse;
            objResponse = (HttpWebResponse)HttpWReq.GetResponse();
            string strResult;
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                strResult = sr.ReadToEnd();
                sr.Close();
            }
            return strResult;
            // get response
            //WebResponse HttpWRes = HttpWReq.GetResponse();
            //System.IO.Stream receiveStream = HttpWRes.GetResponseStream();

            ////Reading response
            //StreamReader reader = new StreamReader(receiveStream);
            //char[] buff = new char[256];
            //int count = reader.Read(buff, 0, 256);
            //StringBuilder response = new StringBuilder();
            //while (count > 0)
            //{
            //    string str = new string(buff, 0, count);
            //    response.Append(str);
            //    count = reader.Read(buff, 0, 256);
            //}
            //return response.ToString();
        }

        private string SendGetRequest(string request)
        {
            if (request.Contains("<?xml version=\"1.0\" encoding=\"utf-16\"?>"))
            {
                request = request.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            }
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            Byte[] byte1 = encoding.GetBytes(request);
            HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(reqURL);

            HttpWReq.ContentType = "text/xml";
            HttpWReq.ContentLength = request.Length;
            HttpWReq.Method = "POST";

            System.IO.Stream StreamData = HttpWReq.GetRequestStream();
            StreamWriter writer = new StreamWriter(StreamData);
            writer.Write(request);
            writer.Flush();

            // get response
            WebResponse HttpWRes = HttpWReq.GetResponse();
            System.IO.Stream receiveStream = HttpWRes.GetResponseStream();

            //Reading response
            StreamReader reader = new StreamReader(receiveStream);
            char[] buff = new char[256];
            int count = reader.Read(buff, 0, 256);
            StringBuilder response = new StringBuilder();
            while (count > 0)
            {
                string str = new string(buff, 0, count);
                response.Append(str);
                count = reader.Read(buff, 0, 256);
            }
            return response.ToString();
        }

        #endregion

        #region Booking

        private string GenerateBookingRequest(HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTW.GenerateBookingRequest entered");

            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteElementString("product", "hotel");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "confirmbooking");


            xmlString.WriteStartElement("bookingDetails");
            //----------------------- Changes in Version 2 of DOTW ---------------------------------------//
            //If you want to merge the existing Itinerary with another then use the following elements----//
            //Set the addToBookedItn=1 and set bookedItnParent='bookingCode' to which you want to merge---//
            //For example---------------------------------------------------------------------------------//
            //xmlString.WriteElementString("addToBookedItn", "1");
            //xmlString.WriteElementString("bookedItnParent", itinerary.BookingRefNo);
            //------------------------------- End Changes ------------------------------------------------//
            xmlString.WriteElementString("fromDate", itinerary.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("toDate", itinerary.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("currency", "366");//AED
            xmlString.WriteElementString("productId", itinerary.HotelCode);
            //------------------------------------ Changes of DOTW ---------------------------------------//
            //This will send the email notification to the lead passenger of the booking itinerary alert  //
            //all modifications done to the booking whether it is cancelled or ammended or deleted etc.   //
            xmlString.WriteElementString("sendCommunicationTo", itinerary.HotelPassenger.Email);
            //--------------------------------------- End Changes ----------------------------------------//
            xmlString.WriteElementString("customerReference", ourRefNo);
            xmlString.WriteStartElement("rooms");
            for (int i = 0; i < itinerary.Roomtype.Length; i++)
            {
                xmlString.WriteStartElement("room");
                xmlString.WriteAttributeString("runno", i.ToString());
                string[] roomCode = itinerary.Roomtype[i].RoomTypeCode.Split('|');
                xmlString.WriteElementString("roomTypeCode", roomCode[0]);
                string[] rateCode = itinerary.Roomtype[i].RatePlanCode.Split('|');
                xmlString.WriteElementString("selectedRateBasis", rateCode[0]);
                xmlString.WriteElementString("allocationDetails", rateCode[1]);
                xmlString.WriteElementString("adultsCode", itinerary.Roomtype[i].AdultCount.ToString());
                xmlString.WriteStartElement("children");
                xmlString.WriteAttributeString("no", itinerary.Roomtype[i].ChildCount.ToString());
                for (int c = 0; c < itinerary.Roomtype[i].ChildCount; c++)
                {
                    xmlString.WriteStartElement("child");
                    xmlString.WriteAttributeString("runno", c.ToString());
                    xmlString.WriteString(itinerary.Roomtype[i].ChildAge[c].ToString());
                    xmlString.WriteEndElement();
                }
                xmlString.WriteEndElement();
                xmlString.WriteElementString("extraBed", "0");
                xmlString.WriteElementString("passengerNationality", itinerary.PassengerNationality);
                xmlString.WriteElementString("passengerCountryOfResidence", itinerary.PassengerCountryOfResidence);
                xmlString.WriteStartElement("passengersDetails");
                for (int n = 0; n < itinerary.Roomtype[i].PassenegerInfo.Count; n++)
                {
                    xmlString.WriteStartElement("passenger");
                    
                    if (itinerary.Roomtype[i].PassenegerInfo[n].LeadPassenger)
                    {
                        xmlString.WriteAttributeString("leading", "yes");    
                    }
                    switch (itinerary.Roomtype[i].PassenegerInfo[n].PaxType)
                    {
                        case HotelPaxType.Adult:
                            switch (itinerary.Roomtype[i].PassenegerInfo[n].Title.ToLower())
                            {
                                case "mr":
                                case "mr.":
                                    xmlString.WriteElementString("salutation", "147");
                                    break;
                                case "ms":
                                case "ms.":
                                    xmlString.WriteElementString("salutation", "148");
                                    break;
                                case "mrs":
                                case "mrs.":
                                    xmlString.WriteElementString("salutation", "149");
                                    break;
                                case "dr":
                                case "dr.":
                                    xmlString.WriteElementString("salutation", "558");
                                    break;
                                case "miss":
                                    xmlString.WriteElementString("salutation", "15134");
                                    break;
                                default:
                                    xmlString.WriteElementString("salutation", "3801");
                                    break;
                            }
                            break;
                        case HotelPaxType.Child:
                            xmlString.WriteElementString("salutation", "14632");
                            break;
                        default:
                            xmlString.WriteElementString("salutation", "3801");
                            break;
                    }
                    //xmlString.WriteElementString("salutation", "3801");
                    xmlString.WriteElementString("firstName", itinerary.Roomtype[i].PassenegerInfo[n].Firstname.Replace(" ", ""));
                    xmlString.WriteElementString("lastName", itinerary.Roomtype[i].PassenegerInfo[n].Lastname.Replace(" ", ""));
                    xmlString.WriteEndElement();
                }
                xmlString.WriteEndElement();
                xmlString.WriteStartElement("specialRequests");
                xmlString.WriteAttributeString("count", "0");
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();


            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            Trace.TraceInformation("DOTWApi.GenerateBookingRequest exiting");
            return strWriter.ToString();
        }

        private BookingResponse ReadResponseBooking(string response, ref HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTW.ReadResponseBooking entered");
            BookingResponse hotelBookRes = new BookingResponse();

            XmlNode tempNode;
            TextReader stringRead = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Error Returned from DOTW. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + response, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
                hotelBookRes = new BookingResponse(BookingResponseStatus.Failed, ErrorInfo.InnerText, string.Empty, ProductType.Hotel, "");
                //(BookingResponseStatus.Failed, ErrorInfo.InnerText, string.Empty);
            }
            else
            {
                tempNode = xmlDoc.SelectSingleNode("result/returnedCode");
                if (tempNode != null)
                {
                    itinerary.ConfirmationNo = tempNode.InnerText;
                }
                int counter = 0;
                XmlNodeList tempNodeList = xmlDoc.SelectNodes("result/bookings/booking");
                foreach (XmlNode booking in tempNodeList)
                {
                    tempNode = booking.SelectSingleNode("bookingStatus");
                    if (tempNode.InnerText == "2")
                    {
                        tempNode = booking.SelectSingleNode("bookingCode");
                        itinerary.BookingRefNo += tempNode.InnerText + "|";
                        tempNode = booking.SelectSingleNode("paymentGuaranteedBy");
                        itinerary.PaymentGuaranteedBy = tempNode.InnerText;
                        counter++;
                    }
                }
                if (counter == itinerary.NoOfRooms)
                {
                    itinerary.Status = HotelBookingStatus.Confirmed;
                    hotelBookRes = new BookingResponse(BookingResponseStatus.Successful, "", "", ProductType.Hotel, itinerary.ConfirmationNo);
                }
                else if (counter > 0)
                {
                    itinerary.Status = HotelBookingStatus.Pending;
                    hotelBookRes = new BookingResponse(BookingResponseStatus.Failed, "Partially booking done.", "", ProductType.Hotel, itinerary.ConfirmationNo);
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Partial Booking Done on DOTW Confno:" + itinerary.ConfirmationNo + "| " + DateTime.Now + "| Response XML" + response, "");
                }
                else
                {
                    itinerary.Status = HotelBookingStatus.Failed;
                    hotelBookRes = new BookingResponse(BookingResponseStatus.Failed, "Booking failed", "", ProductType.Hotel, itinerary.ConfirmationNo);
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Booking Failed| " + DateTime.Now + "| Response XML" + response, "");
                }
            }
            Trace.TraceInformation("DOTWApi.ReadResponseBookingAgreementResponse Exit");
            return hotelBookRes;
        }

        /// <summary>
        /// This method is to block the rooms before making a booking.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public bool BlockRooms(HotelItinerary itinerary, bool isForCancelPolicy)
        {
            Trace.TraceInformation("DOTWApi.GetBooking entered");
            bool blocked = true;
            //for (int i = 1; i <= itinerary.Roomtype.Length; i++)
            //{
            string requestXml = string.Empty;
            requestXml = GenerateBlockRoomRequest(itinerary, isForCancelPolicy);
            if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True") // To Remove ziya
            {
                string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_getRoomsREQ.xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(requestXml);
                doc.Save(filePath);
                Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }
            string resp = string.Empty;
            try
            {
                resp = SendGetRequest(requestXml);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True") // To Remove ziya
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_getRoomsRES.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(resp);
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelBook, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
            }
            catch
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Error Returned from DOTW while blocking rooms. | " + DateTime.Now + "| Request XML" + requestXml + "| Response XML" + resp, "");
                Trace.TraceError("Error: Returned from DOTW while blocking rooms");
            }
            try
            {
                blocked = ReadBlockedRoomResponse(resp, itinerary);
            }
            catch
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Error Returned from DOTW while blocking rooms while reading response xml. | " + DateTime.Now + "| Request XML" + requestXml + "| Response XML" + resp, "");
                Trace.TraceError("Error:Returned from DOTW while blocking rooms while reading response xml. ");
            }

           // blocked = true; // To remove-- for time being making as true for checking
            return blocked;
            //}
        }

        private string GenerateBlockRoomRequest(HotelItinerary itinerary, bool isForCancelPolicy)
        {
            Trace.TraceInformation("DOTW.GenerateBlockRoomRequest entered");

            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteElementString("product", "hotel");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "getrooms");

            xmlString.WriteStartElement("bookingDetails");
            xmlString.WriteElementString("fromDate", itinerary.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("toDate", itinerary.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("currency", "366");//AED

            xmlString.WriteStartElement("rooms");
            for (int i = 0; i < itinerary.Roomtype.Length; i++)
            {
                xmlString.WriteStartElement("room");
                xmlString.WriteAttributeString("runno", i.ToString());
                xmlString.WriteElementString("adultsCode", itinerary.Roomtype[i].AdultCount.ToString());
                xmlString.WriteStartElement("children");
                xmlString.WriteAttributeString("no", itinerary.Roomtype[i].ChildCount.ToString());
                for (int c = 0; c < itinerary.Roomtype[i].ChildCount; c++)
                {
                    xmlString.WriteStartElement("child");
                    xmlString.WriteAttributeString("runno", c.ToString());
                    xmlString.WriteString(itinerary.Roomtype[i].ChildAge[c].ToString());
                    xmlString.WriteEndElement();
                }
                xmlString.WriteEndElement();
                xmlString.WriteElementString("extraBed", "0");
                xmlString.WriteElementString("rateBasis", "-1");
                xmlString.WriteElementString("passengerNationality", itinerary.PassengerNationality);
                xmlString.WriteElementString("passengerCountryOfResidence", itinerary.PassengerCountryOfResidence);

                string[] rCode = itinerary.Roomtype[i].RoomTypeCode.Split('|');
                if (!isForCancelPolicy)
                {
                    xmlString.WriteStartElement("roomTypeSelected");
                    string[] roomCode = itinerary.Roomtype[i].RoomTypeCode.Split('|');
                    xmlString.WriteElementString("code", roomCode[0]);
                    string[] rateCode = itinerary.Roomtype[i].RatePlanCode.Split('|');
                    xmlString.WriteElementString("selectedRateBasis", rateCode[0]);
                    xmlString.WriteElementString("allocationDetails", rateCode[1]);
                    xmlString.WriteEndElement();
                }
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteElementString("productId", itinerary.HotelCode);
            //xmlString.WriteElementString("roomModified", "0");

            xmlString.WriteEndElement();
            xmlString.Close();

            Trace.TraceInformation("DOTW.GenerateBlockRoomRequest exiting");
            return strWriter.ToString();
        }

        /// <summary>
        /// This method blocks the rooms which we selected for booking.
        /// </summary>
        /// <param name="resp"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private bool ReadBlockedRoomResponse(string resp, HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTW.ReadBlockedRoomResponse entered");
            bool blocked = false;
            int roomsReq = 0;
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            XmlNode tempNode;
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Error Returned from DOTW while Blocking the rooms. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + resp, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
            }
            else
            {
                tempNode = xmlDoc.SelectSingleNode("result/hotel/allowBook");
                if (tempNode != null && tempNode.InnerText != "yes")
                {
                    return blocked;
                }
                XmlNodeList tempNodeList = xmlDoc.SelectNodes("result/hotel/rooms/room");
                if (tempNodeList.Count != itinerary.Roomtype.Length)
                {
                    return blocked;
                }
                else
                {
                    int i = 0;
                    foreach (XmlNode temp in tempNodeList)
                    {
                        i = Convert.ToInt16(temp.Attributes["runno"].Value);
                        XmlNodeList roomTypes = temp.SelectNodes("roomType");
                        foreach (XmlNode tp in roomTypes)
                        {
                            tempNode = tp.SelectSingleNode("name");
                            if (tempNode != null && tempNode.InnerText == itinerary.Roomtype[i].RoomName)
                            {
                                tempNode = tp.SelectSingleNode("rateBases/rateBasis");
                                string rpcode = tempNode.Attributes["id"].Value + "|";
                                string rpcodeId = tempNode.Attributes["id"].Value ;
                                string mealPlan = Convert.ToString(tempNode.Attributes["description"].Value);
                                //-------------------------------------Changes in Version 2--------------------------------//
                                //--------------------------------------Date 31 July 2014----------------------------------//
                                XmlNode rateType = tempNode.SelectSingleNode("rateType");
                                string currencyId = rateType.Attributes["currencyid"].Value;

                                itinerary.Roomtype[i].CurrencyId = Convert.ToInt32(currencyId);
                                itinerary.Roomtype[i].CurrencyCode = rateType.Attributes["currencyshort"].Value;

                                if (rateType.Attributes["nonrefundable"] != null && rateType.Attributes["nonrefundable"].Value == "yes")
                                {
                                    itinerary.Roomtype[i].NonRefundable = true;
                                }
                                //Do not allow to book onRequest rateType rooms.
                                if (tempNode.SelectSingleNode("onRequest").InnerText == "1")
                                {
                                    itinerary.Roomtype[i].RatePlanCode = "";
                                    roomsReq++;
                                    break;
                                }
                                //----------------------------------------------End---------------------------------------//
                                object obj = GetFromBasket("DOTWBookingData");
                                DOTWBookingData dotwData = null;
                                if (obj != null)
                                {
                                    Dictionary<string, DOTWBookingData> dotwCancelData = (Dictionary<string, DOTWBookingData>)obj;
                                    if (dotwCancelData.ContainsKey(itinerary.HotelCode))
                                    {
                                        dotwData = dotwCancelData[itinerary.HotelCode];
                                    }
                                }

                                //--------------------------------- Changes in Version 2 of DOTW --------------------------//
                                //--------------------------- changes updated by shiva dt: 30 April 2013 ------------------//
                                //------------------------------New fields added in Version 2 of DOTW----------------------//
                                XmlNode paymentMode = tempNode.SelectSingleNode("paymentMode");
                                XmlNode allowExtraMeals = tempNode.SelectSingleNode("allowsExtraMeals");
                                XmlNode allowSpecialRequests = tempNode.SelectSingleNode("allowsSpecialRequests");
                                XmlNode allowSpecials = tempNode.SelectSingleNode("allowsSpecials");
                                XmlNode passNameReqForBooking = tempNode.SelectSingleNode("passengerNamesRequiredForBooking");
                                
                                HotelRoom room = Convert.ChangeType(itinerary.Roomtype[i], typeof(HotelRoom)) as HotelRoom;
                                room.MealPlanDesc = mealPlan;
                                if (paymentMode != null)
                                {
                                    itinerary.Roomtype[i].PaymentMode = paymentMode.InnerText;
                                }
                                if (allowExtraMeals != null)
                                {
                                    itinerary.Roomtype[i].AllowExtraMeals = Convert.ToBoolean(allowExtraMeals.InnerText);
                                    room.AllowExtraMeals = Convert.ToBoolean(allowExtraMeals.InnerText);
                                }
                                if (allowSpecialRequests != null)
                                {
                                    itinerary.Roomtype[i].AllowSpecialRequests = Convert.ToBoolean(allowSpecialRequests.InnerText);
                                    room.AllowSpecialRequests = Convert.ToBoolean(allowSpecialRequests.InnerText);
                                }
                                if (allowSpecials != null)
                                {
                                    itinerary.Roomtype[i].AllowSpecials = Convert.ToBoolean(allowSpecials.InnerText);
                                    room.AllowSpecials = Convert.ToBoolean(allowSpecials.InnerText);
                                }
                                if (passNameReqForBooking != null)
                                {
                                    itinerary.Roomtype[i].PassengerNamesRequiredForBooking = Convert.ToInt32(passNameReqForBooking.InnerText);
                                    room.PassengerNamesRequiredForBooking = Convert.ToInt32(passNameReqForBooking.InnerText);
                                }
                                XmlNode tarifNotes = tp.SelectSingleNode("rateBases/rateBasis/tariffNotes");
                                if (tarifNotes != null && dotwData != null)
                                {
                                    if (!dotwData.staticInfo.HotelPolicy.Contains(tarifNotes.InnerText))
                                    {
                                        dotwData.staticInfo.HotelPolicy += tarifNotes.InnerText;
                                    }
                                }

                                //--------------------------------------- End Changes -------------------------------------//

                                //changes for blocking the room as per version 2 .
                                //--------------------------- changes updated by ziyad dt: 08 Feb 2013 ------------------//

                                tempNode = tp.SelectSingleNode("rateBases/rateBasis/allocationDetails");
                                rpcode += tempNode.InnerText;
                                string newAllocationDetails = tempNode.InnerText;
                                string[] ratePlan = itinerary.Roomtype[i].RatePlanCode.Split('|');
                                string rateBasisId = ratePlan[0];
                                //string rateBasisAllocationDetails = ratePlan[1];
                                //rateBasisId=rateBasisId.Substring(0,rateBasisId.IndexOf("|"));
                                if (!string.IsNullOrEmpty(rateBasisId))
                                {
                                    if (rpcodeId ==rateBasisId )
                                    {
                                        tempNode = tp.SelectSingleNode("rateBases/rateBasis/status");
                                        if (tempNode.InnerText == "checked")
                                        {
                                            itinerary.Roomtype[i].RatePlanCode = rpcodeId + "|" + newAllocationDetails;
                                            roomsReq++;
                                            break;
                                        }
                                    }
                                }

                                

                                Dictionary<string, decimal> cancelDetailsRoomWise = new Dictionary<string, decimal>();
                                DateTime canDateFrom = DateTime.Now;
                                DateTime canDateTo = DateTime.Now;
                                string canDate = string.Empty;
                                decimal charge = 0;
                                XmlNodeList canList = tp.SelectNodes("rateBases/rateBasis/cancellationRules/rule");
                                foreach (XmlNode rule in canList)
                                {
                                    if (rule.SelectSingleNode("cancelRestricted") != null && rule.SelectSingleNode("cancelRestricted").InnerText == "true")
                                    {
                                        canDateFrom = DateTime.ParseExact(DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                        canDateTo = DateTime.ParseExact(itinerary.StartDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);                                        
                                        canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                        charge = itinerary.TotalPrice;
                                        itinerary.Roomtype[i].CancelRestricted = true;
                                    }
                                    else if (rule.SelectSingleNode("noShowPolicy") != null && rule.SelectSingleNode("noShowPolicy").InnerText == "true")
                                    {
                                        tempNode = rule.SelectSingleNode("charge/text()");
                                        if (tempNode != null)
                                        {
                                            MatchCollection matches = regex.Matches(tempNode.InnerText);
                                            charge = Convert.ToDecimal(matches[0].Value);
                                        }
                                        canDateFrom = DateTime.ParseExact(itinerary.StartDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                        canDateTo = DateTime.ParseExact(itinerary.EndDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);
                                        canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                        itinerary.Roomtype[i].NoShowPolicy = true;
                                    }
                                    else
                                    {
                                        tempNode = rule.SelectSingleNode("fromDate");
                                        if (tempNode != null)
                                        {
                                            canDateFrom = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                                        }
                                        tempNode = rule.SelectSingleNode("toDate");
                                        if (tempNode != null)
                                        {
                                            canDateTo = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                                        }
                                        else
                                        {
                                            canDateTo = itinerary.StartDate;
                                        }
                                        canDate = canDateFrom.ToString() + "|" + canDateTo.ToString();
                                        tempNode = rule.SelectSingleNode("charge/text()");
                                        if (tempNode != null)
                                        {
                                            MatchCollection matches = regex.Matches(tempNode.InnerText);
                                            charge = Convert.ToDecimal(matches[0].Value);
                                        }
                                    }

                                    if (!cancelDetailsRoomWise.ContainsKey(canDate))
                                    {
                                        cancelDetailsRoomWise.Add(canDate, charge);
                                    }

                                }
                                if (dotwData != null && cancelDetailsRoomWise.Count > 0)
                                {
                                    dotwData.cancelDetails[itinerary.Roomtype[i].RoomTypeCode] = cancelDetailsRoomWise;
                                }

                                // ----------------End ----------------------------------
                                //tempNode = tp.SelectSingleNode("rateBases/rateBasis/allocationDetails");
                                //rpcode += tempNode.InnerText;
                                //if (rpcode == itinerary.Roomtype[i].RatePlanCode)
                                //{
                                //    tempNode = tp.SelectSingleNode("rateBases/rateBasis/status");
                                //    if (tempNode.InnerText == "checked")
                                //    {
                                //        roomsReq++;
                                //        break;
                                //    }
                                //}
                                //------------------------------------ Changes in Version 2 --------------------------------//
                                //                          Changes updated by shiva, dt: 30 April 2013                     //                                
                                //------------------------------New fields added in Version 2 of DOTW-----------------------//
                                
                                XmlNode roomCapacityInfo = tp.SelectSingleNode("roomCapacityInfo");
                                XmlNode roomPaxCapacity = roomCapacityInfo.SelectSingleNode("roomPaxCapacity");
                                XmlNode allowdAdultsWOChild = roomCapacityInfo.SelectSingleNode("allowedAdultsWithoutChildren");
                                XmlNode allowdAdultsWithChild = roomCapacityInfo.SelectSingleNode("allowedAdultsWithChildren");
                                XmlNode maxExtraBed = roomCapacityInfo.SelectSingleNode("maxExtraBed");
                                XmlNode total = tp.SelectSingleNode("total/text()");
                                
                                switch (rateType.InnerText.ToUpper())
                                {
                                    case "1":
                                        room.RateType = RoomRateType.DOTW;
                                        break;
                                    case "2":
                                        room.RateType = RoomRateType.DYNAMIC_DIRECT;
                                        break;
                                    case "3":
                                        room.RateType = RoomRateType.DYNAMIC_3rd_PARTY;
                                        break;
                                }
                                if (roomCapacityInfo != null)
                                {
                                    room.RoomCapacityInfo = Convert.ToInt32(roomCapacityInfo.InnerText);
                                    room.RoomPaxCapacity = Convert.ToInt32(roomPaxCapacity.InnerText);
                                }
                                if (allowdAdultsWithChild != null)
                                {
                                    room.AllowedAdultsWithChildren = Convert.ToInt32(allowdAdultsWithChild.InnerText);
                                }
                                if (allowdAdultsWOChild != null)
                                {
                                    room.AllowedAdultsWithoutChildren = Convert.ToInt32(allowdAdultsWOChild.InnerText);
                                }
                                if (maxExtraBed != null)
                                {
                                    room.MaxExtraBed = Convert.ToInt32(maxExtraBed.InnerText);
                                }
                                if (total != null && Convert.ToDecimal(total.InnerText) > room.Price.NetFare)
                                {
                                    room.PreviousFare = room.Price.NetFare;
                                    room.Price.NetFare = Convert.ToDecimal(total.InnerText);                                    
                                }
                                XmlNodeList services = tp.SelectNodes("rateBases/rateBasis/dates/date");

                                if (itinerary.Roomtype[i].Ameneties == null)
                                {
                                    itinerary.Roomtype[i].Ameneties = new List<string>();
                                }

                                foreach (XmlNode service in services)
                                {
                                    XmlNode incl = service.SelectSingleNode("including");

                                    if (incl != null)
                                    {
                                        if (incl.SelectSingleNode("includedMeal") != null)
                                        {
                                            XmlNodeList meals = incl.SelectNodes("includedMeal/mealName");

                                            foreach (XmlNode meal in meals)
                                            {
                                                itinerary.Roomtype[i].Ameneties.Add("Including " + meal.InnerText + " on " + service.Attributes["day"].Value);
                                            }
                                        }
                                    }
                                }

                                //--------------------------------------- End Changes --------------------------------------//
                            }
                        }
                    }
                    if (roomsReq == itinerary.Roomtype.Length)
                    {
                        blocked = true;
                    }
                }
            }
            Trace.TraceInformation("DOTW.ReadBlockedRoomResponse exiting");
            return blocked;
        }

        #endregion

        #region Cancel Booking
        /// <summary>
        /// This private method is used for Generate Cancel Booking Request
        /// </summary>
        /// <param name="confNo">String Confirmation Number</param>
        /// <param name="penality">Decimal Penality</param>
        /// <returns>String XML Request</returns>
        private string GenerateCancelBookingRequest(string confNo, string cancelText, decimal penalty)
        {
            Trace.TraceInformation("DOTWApi.GenerateCancelBookingRequest entered");

            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");

            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "cancelbooking");

            xmlString.WriteStartElement("bookingDetails");
            xmlString.WriteElementString("bookingType", "1");
            xmlString.WriteElementString("bookingCode", confNo);
            xmlString.WriteElementString("confirm", cancelText);
            xmlString.WriteStartElement("testPricesAndAllocation");
            xmlString.WriteStartElement("service");
            xmlString.WriteAttributeString("referencenumber", confNo);
            xmlString.WriteElementString("penaltyApplied", penalty.ToString());
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            Trace.TraceInformation("DOTW.GenerateCancelBookingRequest exited");
            return strWriter.ToString();
        }
        /// <summary>
        /// This private function is used for reading penalty response from source
        /// </summary>
        /// <param name="response">String Response</param>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <returns>Double array Dictionary object</returns>
        private decimal ReadPenaltyResponse(string response)
        {
            Trace.TraceInformation("DOTWApi.ReadPenaltyResponse entered");
            XmlNode tempNode;
            decimal penalty = 0;
            TextReader strWriter = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strWriter);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWCancel, CT.Core.Severity.High, 0, "Error Returned from DOTW in reading penalty. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + response, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
throw new ArgumentException(ErrorInfo.InnerText);
            }
            else
            {
                tempNode = xmlDoc.SelectSingleNode("result/successful");
                if (tempNode.InnerText == "TRUE")
                {
                    tempNode = xmlDoc.SelectSingleNode("result/services/service/cancellationPenalty/charge/text()");
                    if (tempNode != null)
                    {
                        MatchCollection matches = regex.Matches(tempNode.InnerText);
                        penalty = Convert.ToDecimal(matches[0].Value);
                    }
                }
            }
            return penalty;
        }



        /// <summary>
        /// This private function is used for reading cancel booking response from source
        /// </summary>
        /// <param name="response">String Response</param>
        /// <param name="itinerary">HotelItinerary Object</param>
        /// <returns>Double array Dictionary object</returns>
        private Dictionary<string, string> ReadCancelBookingResponse(string response, HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTWApi.ReadCancelBookingResponse entered");
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            XmlNode tempNode;
            TextReader strWriter = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strWriter);
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWCancel, CT.Core.Severity.High, 0, "Error Returned from DOTW. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + response, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
            }
            else
            {
                tempNode = xmlDoc.SelectSingleNode("result/successful");
                if (tempNode.InnerText == "TRUE")
                {
                    cancelInfo.Add("Status", "Cancelled");
                    cancelInfo.Add("ID", itinerary.BookingRefNo);
                    tempNode = xmlDoc.SelectSingleNode("result/services/service/cancellationPenalty/penaltyApplied/formatted");
                    if (tempNode != null)
                    {
                        cancelInfo.Add("Amount", tempNode.InnerText);
                    }
                    tempNode = xmlDoc.SelectSingleNode("result/services/service/cancellationPenalty/currencyShort");
                    if (tempNode != null)
                    {
                        cancelInfo.Add("Currency", tempNode.InnerText);
                    }
                }
            }
            Trace.TraceInformation("DOTWApi.ReadCancelBookingResponse exited");
            return cancelInfo;
        }
        #endregion

        #region Import Booking


        /// <summary>
        /// generate the request to import the booking
        /// </summary>
        /// <param name="itinerary">Hotel Itinerary Object</param>
        /// <returns>String XML Reuqest</returns>
        private string GenerateGetBookedHotelRequest(HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTW.GenerateGetBookedHotelRequest entered");
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);

            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "getbookingdetails");
            xmlString.WriteStartElement("bookingDetails");
            xmlString.WriteElementString("bookingType", "1");
            xmlString.WriteElementString("bookingCode", itinerary.ConfirmationNo);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            Trace.TraceInformation("DOTW.GenerateGetBookedHotelRequest exited");
            return strWriter.ToString();
        }

        /// <summary>
        /// parse the response to get the itinerary filled
        /// </summary>
        /// <param name="response">String XML Response</param>
        /// <param name="itinerary">Ref HotelItinerary Object</param>
        /// <returns>Boolean</returns>
        private bool ReadGetBookedHotelResponse(string response, ref HotelItinerary itinerary)
        {
            Trace.TraceInformation("DOTW.ReadGetBookedHotelResponse entered");
            XmlNode tempNode;
            StaticData staticInfo = new StaticData();
            TextReader stringRead = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);

            #region ErrorInfo Check
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWImport, CT.Core.Severity.High, 0, " DOTW:ReadGetBookedHotelResponse,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + response, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
                return false;
            }
            #endregion

            XmlNodeList tempNodeList = xmlDoc.SelectNodes("result/product/containing/product");
            HotelSource sourceInfo = new HotelSource();
            sourceInfo.Load("DOTW");
            decimal ourCommission = sourceInfo.OurCommission;
            int commissionType = 0;
            commissionType = (int)sourceInfo.CommissionTypeId;
            Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
            rateOfExList = staticInfo.CurrencyROE;
            decimal rateOfExchange = 1;
            string cCode = string.Empty;
            int confirmed = 0, onReq = 0, cancelled = 0;
            itinerary.Roomtype = new HotelRoom[tempNodeList.Count];
            itinerary.NoOfRooms = tempNodeList.Count;
            int i = 0;
            foreach (XmlNode temp in tempNodeList)
            {
                //status
                tempNode = temp.SelectSingleNode("status");
                if (tempNode != null)
                {
                    switch (tempNode.InnerText)
                    {
                        case "1666": confirmed++;
                            break;
                        case "1665": onReq++;
                            break;
                        case "1667": cancelled++;
                            break;
                    }
                }
                if (i == 0)
                {
                    itinerary.Source = HotelBookingSource.DOTW;
                    //HotelCode and HotelName
                    tempNode = temp.SelectSingleNode("serviceId");
                    itinerary.HotelCode = tempNode.InnerText;
                    tempNode = temp.SelectSingleNode("serviceName");
                    itinerary.HotelName = tempNode.InnerText;   

                    itinerary.CityRef = temp.SelectSingleNode("serviceLocation").InnerText.Split(',')[0];

                    HotelDetails details = GetItemInformation(itinerary.CityCode, itinerary.HotelName, itinerary.HotelCode);
                    if (string.IsNullOrEmpty(details.Address))
                    {
                        throw new ArgumentException("City Details not found");
                    }
                    itinerary.HotelAddress1 = details.Address;
                    itinerary.HotelAddress2 = string.Empty;
                    //itinerary.hotelp
                    tempNode = temp.SelectSingleNode("from");
                    itinerary.StartDate = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd", null);
                    tempNode = temp.SelectSingleNode("to");
                    itinerary.EndDate = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd", null);
                    //currency details
                    tempNode = temp.SelectSingleNode("currencyShort");
                    cCode = tempNode.InnerText;
                }
                //search for Passenger Details
                List<HotelPassenger> paxList = new List<HotelPassenger>();
                List<int> childAge = new List<int>();

                #region Passenger Details
                XmlNodeList guests = temp.SelectNodes("passengersDetails/passenger");
                foreach (XmlNode node in guests)
                {
                    HotelPassenger pax = new HotelPassenger();
                    if (node.Attributes["leading"].Value == "yes")
                    {
                        pax.LeadPassenger = true;
                    }
                    tempNode = node.SelectSingleNode("firstName");
                    if (tempNode != null)
                    {
                        pax.Firstname = tempNode.InnerText;
                    }
                    else
                    {
                        pax.Firstname = string.Empty;
                    }
                    tempNode = node.SelectSingleNode("lastName");
                    if (tempNode != null)
                    {
                        pax.Lastname = tempNode.InnerText;
                    }
                    else
                    {
                        pax.Lastname = string.Empty;
                    }
                    paxList.Add(pax);
                }
                if (i == 0)
                {
                    itinerary.HotelPassenger = paxList[0];
                }
                #endregion

                tempNode = temp.SelectSingleNode("code");
                itinerary.BookingRefNo += tempNode.InnerText + "|";
                //hotel rooms details here
                itinerary.Roomtype[i] = new HotelRoom();
                tempNode = temp.SelectSingleNode("roomTypeCode");
                itinerary.Roomtype[i].RoomTypeCode = tempNode.InnerText;
                tempNode = temp.SelectSingleNode("rateBasis");
                itinerary.Roomtype[i].RatePlanCode = tempNode.InnerText;
                tempNode = temp.SelectSingleNode("roomName");
                itinerary.Roomtype[i].RoomName = tempNode.InnerText;
                tempNode = temp.SelectSingleNode("adults");
                itinerary.Roomtype[i].AdultCount = Convert.ToInt16(tempNode.InnerText);
                XmlNodeList children = temp.SelectNodes("children/child");
                itinerary.Roomtype[i].ChildCount = children.Count;
                foreach (XmlNode child in children)
                {
                    childAge.Add(Convert.ToInt16(tempNode.InnerText));
                }
                itinerary.Roomtype[i].ChildAge = childAge;
                itinerary.Roomtype[i].PassenegerInfo = paxList;
                itinerary.Roomtype[i].NoOfUnits = "1";
                if (rateOfExList.ContainsKey(cCode))
                {
                    rateOfExchange = rateOfExList[cCode];
                }
                else
                {
                    rateOfExchange = 1;
                }

                #region Cancel policies.
                //Dictionary<string, decimal> cancelDetailsRoomWise = new Dictionary<string, decimal>();
                DateTime toDate = DateTime.Now;
                DateTime fromDate = DateTime.Now;
                decimal charge = 0;
                int j = 0;
                int lastCancellation = 0;
                decimal cancelMarkup = 0;
                string message = string.Empty;
                string symbol = string.Empty;
                symbol = HotelCity.GetCurrencySymbol(cCode);
                if (ConfigurationSystem.DOTWConfig.ContainsKey("CancellationMarkup"))
                {
                    cancelMarkup = Convert.ToDecimal(ConfigurationSystem.DOTWConfig["CancellationMarkup"]);
                }
                double buffer = 0;
                if (ConfigurationSystem.DOTWConfig.ContainsKey("Buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.DOTWConfig["Buffer"]);
                }
                XmlNodeList canList = temp.SelectNodes("cancellationRules/rule");
                foreach (XmlNode rule in canList)
                {
                    fromDate = DateTime.Now;
                    toDate = DateTime.Now;
                    tempNode = rule.SelectSingleNode("fromDate");
                    if (tempNode != null)
                    {
                        fromDate = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                    }
                    tempNode = rule.SelectSingleNode("toDate");
                    if (tempNode != null)
                    {
                        toDate = DateTime.ParseExact(tempNode.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                    }
                    else
                    {
                        toDate = itinerary.StartDate;
                    }
                    tempNode = rule.SelectSingleNode("charge/text()");
                    if (tempNode != null)
                    {
                        MatchCollection matches = regex.Matches(tempNode.InnerText);
                        charge = Convert.ToDecimal(matches[0].Value);
                    }

                    #region assuming the first rule will have cancellation penality 0
                    if (j == 0)
                    {
                        if (charge == 0)
                        {
                            lastCancellation = Convert.ToInt32(itinerary.StartDate.Subtract(toDate.Date).Days);
                        }
                        else
                        {
                            lastCancellation = 0;
                        }
                        toDate = toDate.AddDays(-buffer);
                        if (toDate.DayOfWeek == DayOfWeek.Saturday)
                        {
                            toDate = toDate.AddDays(-1);
                        }
                        else if (toDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            toDate = toDate.AddDays(-2);
                        }
                    }
                    #endregion
                    else if (j == 1)
                    {
                        fromDate = fromDate.AddDays(-buffer);
                        if (fromDate.DayOfWeek == DayOfWeek.Saturday)
                        {
                            fromDate = fromDate.AddDays(-1);
                        }
                        else if (fromDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            fromDate = fromDate.AddDays(-2);
                        }
                    }
                    if (toDate <= fromDate)
                    {
                        message += "Amount of " + symbol + " " + (charge + (charge * cancelMarkup / 100)) + " will be charged till " + toDate.ToString("dd MMM yyyy HH:mm:ss") + "|";
                    }
                    else
                    {
                        message += "Amount of " + symbol + " " + (charge + (charge * cancelMarkup / 100)) + " will be charged from " + fromDate.ToString("dd MMM yyyy HH:mm:ss") + " to " + toDate.ToString("dd MMM yyyy HH:mm:ss") + "|";
                    }
                    j++;
                }



                //DateTime fromDate = DateTime.Now;
                //DateTime toDate = DateTime.Now;
                //string[] dates;
                //int j = 0;
                //int lastCancellation = 0;
                //string message = string.Empty;

                //foreach (KeyValuePair<string, decimal> pair in cancelDetailsRoomWise)
                //{
                //}

                itinerary.LastCancellationDate = itinerary.StartDate.AddDays(-lastCancellation);
                itinerary.HotelCancelPolicy = message;
                #endregion
                XmlNodeList rmRates = temp.SelectNodes("dates/date");

                itinerary.Roomtype[i].RoomFareBreakDown = new HotelRoomFareBreakDown[rmRates.Count];
                int n = 0;
                decimal totalPrice = 0;
                decimal totalMarkUp = 0;
                decimal price = 0;
                foreach (XmlNode rmRate in rmRates)
                {
                    itinerary.Roomtype[i].RoomFareBreakDown[n] = new HotelRoomFareBreakDown();
                    itinerary.Roomtype[i].RoomFareBreakDown[n].Date = DateTime.ParseExact(rmRate.Attributes["datetime"].Value, "yyyy-MM-dd", null);
                    tempNode = rmRate.SelectSingleNode("price/formatted");
                    price = Convert.ToDecimal(tempNode.InnerText);
                    if (commissionType == (int)CommissionType.Percentage)
                    {
                        itinerary.Roomtype[i].RoomFareBreakDown[n].RoomPrice = price + price * ourCommission / 100;
                        totalMarkUp += price * ourCommission / 100;
                    }
                    else if (commissionType == (int)CommissionType.Fixed)
                    {
                        itinerary.Roomtype[i].RoomFareBreakDown[n].RoomPrice = price + ourCommission / rateOfExchange;
                        totalMarkUp += ourCommission / rateOfExchange;
                    }
                    totalPrice += Convert.ToDecimal(tempNode.InnerText);
                    n++;
                }
                itinerary.Roomtype[i].Price = new PriceAccounts();
                itinerary.Roomtype[i].Price.Currency = cCode;
                itinerary.Roomtype[i].Price.NetFare = totalPrice;
                itinerary.Roomtype[i].Price.Markup = totalMarkUp;
                itinerary.Roomtype[i].Price.AccPriceType = PriceType.NetFare;
                itinerary.Roomtype[i].Price.RateOfExchange = rateOfExchange;
                i++;
            }
            if (cancelled != 0)
            {
                itinerary.Status = HotelBookingStatus.Cancelled;
                CT.Core.Audit.Add(CT.Core.EventType.DOTWImport, CT.Core.Severity.High, 0, " DOTW:ReadGetBookedHotelResponse,Error Message: one or more rooms are cancelled in the booking| " + DateTime.Now + "| Response XML" + response, "");
            }
            else if (onReq != 0)
            {
                itinerary.Status = HotelBookingStatus.Pending;
                CT.Core.Audit.Add(CT.Core.EventType.DOTWImport, CT.Core.Severity.High, 0, " DOTW:ReadGetBookedHotelResponse,Error Message: one or more rooms are on request in the booking| " + DateTime.Now + "| Response XML" + response, "");
            }
            else
            {
                itinerary.Status = HotelBookingStatus.Confirmed;
            }

            return true;
        }

        private HotelStaticData GetHotelDetails(string hotelCode , string hotelName , string cityCode)
        {
            string resp = string.Empty;
            string request = string.Empty;
            Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "DOTWApi.GetHotelDetails Entered", "0");
            HotelStaticData dotwData = new HotelStaticData();
            #region Generate Request
            try
            {
                request = GenerateHotelDetailRequest(hotelCode, hotelName, cityCode);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss")+ "_getHotelDetailsREQ.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(request);
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "request message generated", "0");
            }
            catch (Exception Ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned in creation of XML request" + Ex.Message, "");
                Trace.TraceError("Error: " + Ex.Message);
                throw new BookingEngineException("Error: " + Ex.Message);
            }
            #endregion
            try
            {
                TimeSpan ts = new TimeSpan();
                DateTime time = DateTime.Now;
                resp = SendGetRequest(request);
                DateTime time1 = DateTime.Now;
                ts = time1 - time;
                Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "DOTW search result returned in " + ts.Milliseconds + "Milliseconds", string.Empty);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetHotelDetails Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);
            }

            #region Generate HotelDetailResult Object
            try
            {
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_getHotelDetailsRES.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(resp);
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                dotwData = ReadHotelDetailReponse(resp);
            }
            catch (Exception ex)
            {

                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetHotelDetails Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
                Trace.TraceError("Error: " + ex.Message);
                throw new BookingEngineException("Error: " + ex.Message);

            }
            #endregion

            CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, " Response from DOTW for Search Request:| " + DateTime.Now + "| request XML:" + request + "|response XML:" + resp, "");
            return dotwData;
        
        }

        private string GenerateHotelDetailRequest(string hotelCode, string hotelName, string cityCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-16\"");
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteElementString("product", "hotel");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "searchhotels");

            xmlString.WriteStartElement("bookingDetails");
            xmlString.WriteElementString("fromDate", DateTime.Now.AddMonths(2).ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("toDate", DateTime.Now.AddMonths(2).AddDays(2).ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("currency", "366"); //AED
            xmlString.WriteStartElement("rooms");
            xmlString.WriteStartElement("room");
            xmlString.WriteAttributeString("runno", "0");
            xmlString.WriteElementString("adultsCode", "1");
            xmlString.WriteStartElement("children");
            xmlString.WriteAttributeString("no", "0");
            xmlString.WriteEndElement();
            xmlString.WriteElementString("extraBed", "0");
            xmlString.WriteElementString("rateBasis", "-1");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteElementString("productCodeRequested", hotelCode);
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("return");
            xmlString.WriteElementString("getRooms", "false");
            xmlString.WriteStartElement("filters");
            xmlString.WriteElementString("city", cityCode);
            /*StringBuilder str = new StringBuilder();
            str.Append("<city>" + cityCode + "</city><c:condition><a:condition><fieldName>hotelId</fieldName><fieldTest>equals</fieldTest><fieldValues><fieldValue>" + hotelCode + "</fieldValue></fieldValues></a:condition></c:condition>");
            xmlString.WriteString(str.ToString());*/
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("fields");
            xmlString.WriteElementString("field", "builtYear");
            xmlString.WriteElementString("field", "renovationYear");
            xmlString.WriteElementString("field", "floors");
            xmlString.WriteElementString("field", "noOfRooms");
            xmlString.WriteElementString("field", "fullAddress");
            xmlString.WriteElementString("field", "description1");
            xmlString.WriteElementString("field", "description2");
            xmlString.WriteElementString("field", "hotelName");
            xmlString.WriteElementString("field", "location");
            xmlString.WriteElementString("field", "location1");
            xmlString.WriteElementString("field", "location2");
            xmlString.WriteElementString("field", "location3");
            xmlString.WriteElementString("field", "stateName");
            xmlString.WriteElementString("field", "attraction");
            xmlString.WriteElementString("field", "amenitie");
            xmlString.WriteElementString("field", "leisure");
            xmlString.WriteElementString("field", "business");
            xmlString.WriteElementString("field", "transportation");
            xmlString.WriteElementString("field", "hotelPhone");
            xmlString.WriteElementString("field", "hotelCheckIn");
            xmlString.WriteElementString("field", "hotelCheckOut");
            xmlString.WriteElementString("field", "minAge");
            xmlString.WriteElementString("field", "rating");
            xmlString.WriteElementString("field", "images");
            xmlString.WriteElementString("field", "fireSafety");
            xmlString.WriteElementString("field", "geoPoint");
            xmlString.WriteEndElement();
            xmlString.WriteElementString("resultsPerPage", "1");
            xmlString.WriteElementString("page", "0");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        private HotelStaticData ReadHotelDetailReponse(string resp)
        {
            Trace.TraceInformation("DOTWApi.ReadHotelDetailReponse entered");
            XmlNode tempNode;
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            HotelStaticData hotelResult = new HotelStaticData();
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + resp, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            else
            {
                XmlNodeList hotelInfo = xmlDoc.SelectNodes("result/hotels/hotel");
                if (hotelInfo.Count == 0)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: No hotel found!| " + DateTime.Now + "| Response XML" + resp, "");
                    Trace.TraceError("No Hotel Details found. Response not coming! . Response =" + resp);
                    throw new BookingEngineException("<br> No Hotel Details found!. Response not coming ");
                }
                else
                {
                    foreach (XmlNode hInfo in hotelInfo)
                    {
                        hotelResult.Source = HotelBookingSource.DOTW;
                        Dictionary<string, Dictionary<string, decimal>> cancelDetails = new Dictionary<string, Dictionary<string, decimal>>();
                        hotelResult.HotelCode = hInfo.Attributes["hotelid"].Value;
                        #region Hotel Details
                        tempNode = hInfo.SelectSingleNode("hotelName");
                        if (tempNode != null)
                        {
                            hotelResult.HotelName = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("description1/language");
                        if (tempNode != null)
                        {
                            hotelResult.HotelDescription = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("description2/language");
                        if (tempNode != null)
                        {
                            hotelResult.HotelDescription += tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("location");
                        if (tempNode != null)
                        {
                            hotelResult.HotelLocation = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("images/thumb");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPicture = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("map");
                        if (tempNode != null)
                        {
                            hotelResult.HotelMap = tempNode.InnerText;
                        }
                        else
                        {
                            hotelResult.HotelMap = string.Empty;
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelStreetAddress");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress = tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelCity");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelState");
                        if (tempNode != null && tempNode.InnerText != "")
                        {
                            hotelResult.HotelAddress += tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelCountry");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelZipCode");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += " ZipCode:-" + tempNode.InnerText;

                            //putting pincode in static data
                            hotelResult.PinCode = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("rating");
                        if (tempNode != null)
                        {
                            string temp = tempNode.InnerText;
                            switch (temp)
                            {
                                case "559": hotelResult.Rating = HotelRating.OneStar;
                                    break;
                                case "560": hotelResult.Rating = HotelRating.TwoStar;
                                    break;
                                case "561": hotelResult.Rating = HotelRating.ThreeStar;
                                    break;
                                case "562": hotelResult.Rating = HotelRating.FourStar;
                                    break;
                                case "563": hotelResult.Rating = HotelRating.FiveStar;
                                    break;
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("hotelPhone");
                        if (tempNode != null)
                        {
                            hotelResult.PhoneNumber = tempNode.InnerText;
                        }

                        #endregion

                        //Static data.
                        //Spl Attractions.
                        hotelResult.SpecialAttraction = string.Empty;

                        #region Transportation Details
                        XmlNodeList atrxns = hInfo.SelectNodes("transportation/airports/airport");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/rails/rail");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/subways/subway");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/cruises/cruise");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        #endregion

                        #region Attraction Item
                        atrxns = hInfo.SelectNodes("attraction/attractionItem");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                            else
                            {
                                hotelResult.SpecialAttraction += "|";
                            }
                        }
                        #endregion

                        #region Hotel facilities.
                        hotelResult.HotelFacilities = string.Empty;
                        XmlNodeList amenitie = hInfo.SelectNodes("amenitie/language/amenitieItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelResult.HotelFacilities += amenity.InnerText + "|";
                        }
                        amenitie = hInfo.SelectNodes("leisure/language/leisureItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelResult.HotelFacilities += amenity.InnerText + "|";
                        }
                        amenitie = hInfo.SelectNodes("business/language/businessItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            hotelResult.HotelFacilities += amenity.InnerText + "|";
                        }
                        tempNode = hInfo.SelectSingleNode("fireSafety");
                        if (tempNode != null && tempNode.InnerText == "yes")
                        {
                            hotelResult.HotelFacilities += "Fire Safety|";
                        }
                        #endregion

                        #region HotelImages
                        XmlNodeList imglist = hInfo.SelectNodes("images/hotelImages/image");
                        foreach (XmlNode img in imglist)
                        {
                            tempNode = img.SelectSingleNode("url");
                            hotelResult.HotelPicture += tempNode.InnerText + "|";
                        }
                        #endregion

                        #region hotelStaticData
                        //Hotel Policy.
                        hotelResult.HotelPolicy = string.Empty;
                        tempNode = hInfo.SelectSingleNode("hotelCheckIn");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPolicy = "Check-In Time:- " + tempNode.InnerText + "hrs.|";
                        }
                        tempNode = hInfo.SelectSingleNode("hotelCheckOut");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPolicy += "Check-Out Time:- " + tempNode.InnerText + "hrs.|";
                        }
                        tempNode = hInfo.SelectSingleNode("minAge");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPolicy += "Minimum age to Check-in is " + tempNode.InnerText + "|";
                        }
                        hotelResult.FaxNumber = string.Empty;
                        if (hotelResult.PinCode == null)
                        {
                            hotelResult.PinCode = string.Empty;
                        }
                        if (hotelResult.SpecialAttraction == null)
                        {
                            hotelResult.SpecialAttraction = string.Empty;
                        }
                        if (hotelResult.HotelFacilities == null)
                        {
                            hotelResult.HotelFacilities = string.Empty;
                        }
                        hotelResult.EMail = string.Empty;
                        hotelResult.URL = string.Empty;
                        hotelResult.HotelMap = hotelResult.HotelMap;
                        #endregion
                    }
                }
            }
            Trace.TraceInformation("DOTWApi.ReadHotelDetailReponse exited");
            return hotelResult; 
        }
        #endregion

        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }

        #endregion

        # region ProviderMasters
      public Dictionary<string, string> GetAllCountries()
        {
            Dictionary<string, string> Countries = new Dictionary<string, string>();
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");

            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "getallcountries");

            xmlString.WriteStartElement("return");
            xmlString.WriteStartElement("fields");
            xmlString.WriteElementString("field", "regionName");
            xmlString.WriteElementString("field", "regionCode");
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();

            string response = SendGetRequest(strWriter.ToString());

            TextReader stringRead = new StringReader(response);
            XmlDocument Doc = new XmlDocument();
            Doc.Load(stringRead);

            XmlNodeList nodes = Doc.SelectNodes("result/countries/country");

            foreach (XmlNode node in nodes)
            {
                Countries.Add(node.ChildNodes[0].InnerText, node.ChildNodes[1].InnerText);
            }

            return Countries;
        }
        # endregion

        #region DOTW Hotels Static Data

        private string GenerateHotelStaticDataRequest(HotelRequest request)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            xmlString.WriteStartElement("customer");
            xmlString.WriteElementString("username", userName);

            xmlString.WriteElementString("password", md5Password);
            xmlString.WriteElementString("id", companyCode);
            xmlString.WriteElementString("source", "1");
            xmlString.WriteElementString("product", "hotel");
            xmlString.WriteStartElement("request");
            xmlString.WriteAttributeString("command", "searchhotels");

            xmlString.WriteStartElement("bookingDetails");
            xmlString.WriteElementString("fromDate", request.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("toDate", request.EndDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("currency", request.Currency); //AED

            xmlString.WriteStartElement("rooms");
            xmlString.WriteAttributeString("no", "1");
            xmlString.WriteStartElement("room");
            xmlString.WriteAttributeString("runno", "1");
            xmlString.WriteElementString("adultsCode", "1");
            xmlString.WriteStartElement("children");
            xmlString.WriteAttributeString("no", "0");

            xmlString.WriteEndElement();
            xmlString.WriteElementString("extraBed", "0");
            xmlString.WriteElementString("rateBasis", "-1");
            //----------------- Changes for DOTW Version 2 ----------------------------//               

            //string countryName = req.PassengerNationality;
            //string countryCode = req.PassengerCountryOfResidence;

            //xmlString.WriteElementString("passengerNationality", countryName);
            //xmlString.WriteElementString("passengerCountryOfResidence", countryCode);
            //------------------------- End Changes----------------------------------//
            xmlString.WriteEndElement();

            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("return");
            //xmlString.WriteElementString("getRooms", "true");

            xmlString.WriteStartElement("filters");

            xmlString.WriteElementString("city", request.CityCode);
            xmlString.WriteElementString("noPrice", "false");
            //----------------- Changes for DOTW Version 2 ----------------------------//
            //xmlString.WriteElementString("nearbyCities", "false");//Not mandatory           
            //xmlString.WriteElementString("noPrice", "false");//Not mandatory
            //xmlString.WriteElementString("shortFormat", "false");//Not mandatory
            //xmlString.WriteElementString("specialDeals", "false");//Not mandatory
            //xmlString.WriteElementString("topDeals", "false");//Not mandatory
            //xmlString.WriteStartElement("rateTypes");//Not mandatory
            //xmlString.WriteElementString("rateType", "1"); //Possible values = 1 - DOTW Rate Type, 2 - DYNAMIC DIRECT Rate type, 3 - DYNAMIC 3RD PARTY Rate type
            //xmlString.WriteEndElement();
            //----------------- Changes for DOTW Version 2 ----------------------------//
            //if (req.HotelName.Trim().Length > 0)
            //{
            //    xmlString.WriteStartElement("c", "condition", "http://us.dotwconnect.com/xsd/complexCondition");
            //    xmlString.WriteStartElement("a", "condition", "http://us.dotwconnect.com/xsd/atomicCondition");

            //    xmlString.WriteElementString("fieldName", "hotelName");
            //    xmlString.WriteElementString("fieldTest", "like");
            //    xmlString.WriteStartElement("fieldValues");
            //    xmlString.WriteElementString("fieldValue", req.HotelName);
            //    xmlString.WriteEndElement();//fieldValues
            //    xmlString.WriteEndElement();//a:condition
            //    xmlString.WriteEndElement();//c:condition
            //}
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("fields");
            xmlString.WriteElementString("field", "hotelName");
            xmlString.WriteElementString("field", "description1");
            //xmlString.WriteElementString("field", "builtYear");
            //xmlString.WriteElementString("field", "renovationYear");
            xmlString.WriteElementString("field", "floors");
            xmlString.WriteElementString("field", "noOfRooms");
            xmlString.WriteElementString("field", "fullAddress");

            //xmlString.WriteElementString("field", "description2");

            xmlString.WriteElementString("field", "location");
            xmlString.WriteElementString("field", "location1");
            xmlString.WriteElementString("field", "location2");
            xmlString.WriteElementString("field", "location3");
            xmlString.WriteElementString("field", "stateName");
            xmlString.WriteElementString("field", "attraction");
            xmlString.WriteElementString("field", "amenitie");
            xmlString.WriteElementString("field", "leisure");
            xmlString.WriteElementString("field", "business");
            xmlString.WriteElementString("field", "transportation");
            xmlString.WriteElementString("field", "hotelPhone");
            xmlString.WriteElementString("field", "hotelCheckIn");
            xmlString.WriteElementString("field", "hotelCheckOut");
            //xmlString.WriteElementString("field", "minAge");
            xmlString.WriteElementString("field", "rating");
            xmlString.WriteElementString("field", "lastUpdated");
            //xmlString.WriteElementString("field", "images");
            //xmlString.WriteElementString("field", "fireSafety");
            //xmlString.WriteElementString("field", "geoPoint");
           
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            xmlString.Flush();

          

            return strWriter.ToString();
        }

        private string GetResponse(string request)
        {
            string response = "";
            try
            {
                if (request.Contains("<?xml version=\"1.0\" encoding=\"utf-16\"?>"))
                {
                    request = request.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                }
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                byte[] byte1 = encoding.GetBytes(request);
                HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(reqURL);

                HttpWReq.ContentType = "text/xml";
                HttpWReq.ContentLength = byte1.Length;
                HttpWReq.Method = "POST";               
                HttpWReq.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                
                System.IO.Stream StreamData = HttpWReq.GetRequestStream();
                StreamData.Write(byte1, 0, byte1.Length);
                StreamData.Close();

                // get response
                HttpWebResponse HttpWRes = HttpWReq.GetResponse() as HttpWebResponse;

                //Reading response
                Stream dataStream = GetStreamForResponse(HttpWRes);
                StreamReader reader = new StreamReader(dataStream);

                response = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response.ToString();
        }

        private List<HotelStaticData> ReadHotelStaticDataReponse(string resp, string cityCode)
        {
            Trace.TraceInformation("DOTWApi.ReadHotelDetailReponse entered");
            XmlNode tempNode;
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            List<HotelStaticData> hotelResults = new List<HotelStaticData>();
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
            if (ErrorInfo != null)
            {
                //CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + resp, "");
                Trace.TraceError("Error: " + ErrorInfo.InnerText);
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }
            else
            {
                XmlNodeList hotelInfo = xmlDoc.SelectNodes("result/hotels/hotel");
                if (hotelInfo.Count == 0)
                {
                    //CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Error returned from DOTW. Error Message: No hotel found!| " + DateTime.Now + "| Response XML" + resp, "");
                    Trace.TraceError("No Hotel Details found. Response not coming! . Response =" + resp);
                    throw new BookingEngineException("<br> No Hotel Details found!. Response not coming ");
                }
                else
                {
                    foreach (XmlNode hInfo in hotelInfo)
                    {
                        HotelStaticData hotelResult = new HotelStaticData();
                        hotelResult.Source = HotelBookingSource.DOTW;
                        Dictionary<string, Dictionary<string, decimal>> cancelDetails = new Dictionary<string, Dictionary<string, decimal>>();
                        hotelResult.HotelCode = hInfo.Attributes["hotelid"].Value;
                        #region Hotel Details
                        tempNode = hInfo.SelectSingleNode("hotelName");
                        if (tempNode != null)
                        {
                            hotelResult.HotelName = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("description1/language");
                        if (tempNode != null)
                        {
                            hotelResult.HotelDescription = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("description2/language");
                        if (tempNode != null)
                        {
                            hotelResult.HotelDescription += tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("location");
                        if (tempNode != null)
                        {
                            hotelResult.HotelLocation = tempNode.InnerText;
                        }
                        if(hotelResult.HotelLocation == null)
                        {
                            hotelResult.HotelLocation = string.Empty;
                        }

                        tempNode = hInfo.SelectSingleNode("images/thumb");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPicture = tempNode.InnerText;
                        }
                        if (hotelResult.HotelPicture == null)
                        {
                            hotelResult.HotelPicture = string.Empty;
                        }
                        tempNode = hInfo.SelectSingleNode("map");
                        if (tempNode != null)
                        {
                            hotelResult.HotelMap = tempNode.InnerText;
                        }
                        else
                        {
                            hotelResult.HotelMap = string.Empty;
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelStreetAddress");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress = tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelCity");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelState");
                        if (tempNode != null && tempNode.InnerText != "")
                        {
                            hotelResult.HotelAddress += tempNode.InnerText + " ";
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelCountry");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("fullAddress/hotelZipCode");
                        if (tempNode != null)
                        {
                            hotelResult.HotelAddress += " ZipCode:-" + tempNode.InnerText;

                            //putting pincode in static data
                            hotelResult.PinCode = tempNode.InnerText;
                        }
                        tempNode = hInfo.SelectSingleNode("rating");
                        if (tempNode != null)
                        {
                            string temp = tempNode.InnerText;
                            switch (temp)
                            {
                                case "559": hotelResult.Rating = HotelRating.OneStar;
                                    break;
                                case "560": hotelResult.Rating = HotelRating.TwoStar;
                                    break;
                                case "561": hotelResult.Rating = HotelRating.ThreeStar;
                                    break;
                                case "562": hotelResult.Rating = HotelRating.FourStar;
                                    break;
                                case "563": hotelResult.Rating = HotelRating.FiveStar;
                                    break;
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("hotelPhone");
                        if (tempNode != null)
                        {
                            hotelResult.PhoneNumber = tempNode.InnerText;
                        }
                        if (hotelResult.PhoneNumber == null)
                        {
                            hotelResult.PhoneNumber = string.Empty;
                        }
                        #endregion

                        //Static data.
                        //Spl Attractions.
                        hotelResult.SpecialAttraction = string.Empty;

                        #region Transportation Details
                        XmlNodeList atrxns = hInfo.SelectNodes("transportation/airports/airport");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/rails/rail");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/subways/subway");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        atrxns = hInfo.SelectNodes("transportation/cruises/cruise");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                        }
                        #endregion

                        #region Attraction Item
                        atrxns = hInfo.SelectNodes("attraction/attractionItem");
                        foreach (XmlNode atrxn in atrxns)
                        {
                            tempNode = atrxn.SelectSingleNode("name");
                            hotelResult.SpecialAttraction += tempNode.InnerText + "#";
                            tempNode = atrxn.SelectSingleNode("dist");
                            if (tempNode != null)
                            {
                                hotelResult.SpecialAttraction += tempNode.InnerText + "|";
                            }
                            else
                            {
                                hotelResult.SpecialAttraction += "|";
                            }
                        }

                        if (hotelResult.SpecialAttraction == null)
                        {
                            hotelResult.SpecialAttraction = string.Empty;
                        }
                        #endregion

                        #region Hotel facilities.
                        hotelResult.HotelFacilities = string.Empty;
                        XmlNodeList amenitie = hInfo.SelectNodes("amenitie/language/amenitieItem");
                        foreach (XmlNode amenity in amenitie)
                        {                            
                            hotelResult.HotelFacilities += amenity.InnerText + "|";
                        }
                        amenitie = hInfo.SelectNodes("leisure/language/leisureItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            if (amenity.InnerText != null && amenity.InnerText.Length > 0)
                            {
                                hotelResult.HotelFacilities += amenity.InnerText + "|";
                            }
                            else
                            {
                                CatalogueData cd = new CatalogueData();
                                hotelResult.HotelFacilities += cd.GetStaticData(amenity.Attributes["value"].Value, "Leisure", HotelBookingSource.DOTW) + "|";
                            }
                        }
                        amenitie = hInfo.SelectNodes("business/language/businessItem");
                        foreach (XmlNode amenity in amenitie)
                        {
                            if (amenity.InnerText != null && amenity.InnerText.Length > 0)
                            {
                                hotelResult.HotelFacilities += amenity.InnerText + "|";
                            }
                            else
                            {
                                CatalogueData cd = new CatalogueData();
                                hotelResult.HotelFacilities += cd.GetStaticData(amenity.Attributes["value"].Value, "Business", HotelBookingSource.DOTW) + "|";
                            }
                        }
                        tempNode = hInfo.SelectSingleNode("fireSafety");
                        if (tempNode != null && tempNode.InnerText == "yes")
                        {
                            hotelResult.HotelFacilities += "Fire Safety|";
                        }
                        #endregion

                        #region HotelImages
                        XmlNodeList imglist = hInfo.SelectNodes("images/hotelImages/image");
                        foreach (XmlNode img in imglist)
                        {
                            tempNode = img.SelectSingleNode("url");
                            hotelResult.HotelPicture += tempNode.InnerText + "|";
                        }

                        if (hotelResult.HotelPicture == null)
                        {
                            hotelResult.HotelPicture = string.Empty;
                        }
                        #endregion

                        #region hotelStaticData
                        //Hotel Policy.
                        hotelResult.HotelPolicy = string.Empty;
                        tempNode = hInfo.SelectSingleNode("hotelCheckIn");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPolicy = "Check-In Time:- " + tempNode.InnerText + "hrs.|";
                        }
                        tempNode = hInfo.SelectSingleNode("hotelCheckOut");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPolicy += "Check-Out Time:- " + tempNode.InnerText + "hrs.|";
                        }
                        tempNode = hInfo.SelectSingleNode("minAge");
                        if (tempNode != null)
                        {
                            hotelResult.HotelPolicy += "Minimum age to Check-in is " + tempNode.InnerText + "|";
                        }
                        hotelResult.FaxNumber = string.Empty;
                        if (hotelResult.PinCode == null)
                        {
                            hotelResult.PinCode = string.Empty;
                        }
                        if (hotelResult.SpecialAttraction == null)
                        {
                            hotelResult.SpecialAttraction = string.Empty;
                        }
                        if (hotelResult.HotelFacilities == null)
                        {
                            hotelResult.HotelFacilities = string.Empty;
                        }
                        hotelResult.EMail = string.Empty;
                        hotelResult.URL = string.Empty;
                        hotelResult.HotelMap = "";
                        tempNode = hInfo.SelectSingleNode("from");
                        if (tempNode != null)
                        {
                            hotelResult.FromPrice = Convert.ToDecimal(tempNode.InnerXml.Substring(tempNode.InnerXml.IndexOf(">") + 1, tempNode.InnerXml.IndexOf("<") - 2));
                        }
                        tempNode = hInfo.SelectSingleNode("availability");
                        if (tempNode != null)
                        {
                            switch (tempNode.InnerText)
                            {
                                case "available":
                                    hotelResult.Status = true;
                                    break;
                                default:
                                    hotelResult.Status = false;
                                    break;
                            }
                        }
                        hotelResult.CityCode = cityCode;
                        #endregion

                        try
                        {                            
                            hotelResult.Save();
                        }
                        catch (Exception ex) { throw new Exception("Failed to Save DOTW HotelStatic details" + ex.Message, ex); }
                        hotelResults.Add(hotelResult);
                    }
                }
            }
            Trace.TraceInformation("DOTWApi.ReadHotelDetailReponse exited");
            return hotelResults;
        }

        public List<HotelStaticData> GetHotelStaticData(HotelRequest request)
        {
            List<HotelStaticData> staticDataList = new List<HotelStaticData>();

            try
            {
                string requestXML = GenerateHotelStaticDataRequest(request);

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True") // To Remove ziya
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + "HotelStaticDataREQ_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestXML);
                    doc.Save(filePath);
                    //Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                string responseXML = GetResponse(requestXML);

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True") // To Remove ziya
                {
                    string filePath = @"" + ConfigurationSystem.DOTWConfig["XmlLogPath"] + "HotelStaticDataRES_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(responseXML);
                    doc.Save(filePath);
                    //Audit.Add(EventType.GetHotelDetails, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                staticDataList = ReadHotelStaticDataReponse(responseXML, request.CityCode);                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return staticDataList;
        }

       

       
        #endregion
    }
}

