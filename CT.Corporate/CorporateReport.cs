﻿using System;
using System.Collections.Generic;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using System.Data;
using System.Data.SqlClient;

namespace CT.Corporate
{
    public class CorporateReport
    {

        public static DataTable GetReport(DateTime fromDate, DateTime toDate,int agentId)
        {
            DataTable dtReport = new DataTable();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_FromDate", fromDate));
                parameters.Add(new SqlParameter("@P_ToDate", toDate));
                if (agentId > 0)
                    parameters.Add(new SqlParameter("@P_AgentId", agentId));

                dtReport = DBGateway.FillDataTableSP("usp_GetCorpTripReport", parameters.ToArray());
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get the corp trip report. Reason : "+ex.ToString(), "");
            }

            return dtReport;
        }
    }
}
