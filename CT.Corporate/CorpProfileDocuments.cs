﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CT.Corporate
{
    public class CorpProfileDocuments
    {
        #region Variables
        long docId;
        long profileId;
        string docTypeName;
        string docFileName;
        string docFilePath;
        int createdBy;
        int retDocId;
        string passportNo;
        #endregion
        #region Properities
        public long ProfileId
        {
            get
            {
                return profileId;
            }

            set
            {
                profileId = value;
            }
        }

        public string DocTypeName
        {
            get
            {
                return docTypeName;
            }

            set
            {
                docTypeName = value;
            }
        }

        public string DocFileName
        {
            get
            {
                return docFileName;
            }

            set
            {
                docFileName = value;
            }
        }

        public string DocFilePath
        {
            get
            {
                return docFilePath;
            }

            set
            {
                docFilePath = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }

            set
            {
                createdBy = value;
            }
        }

        public int RetDocId
        {
            get
            {
                return retDocId;
            }

            set
            {
                retDocId = value;
            }
        }
        public long DocId
        {
            get
            {
                return docId;
            }

            set
            {
                docId = value;
            }
        }

        public string PassportNo
        {
            get
            {
                return passportNo;
            }
            set
            {
                passportNo = value;
            }
        }
        #endregion

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[12];

                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                paramList[1] = new SqlParameter("@P_DocTypeName", DocTypeName);
                paramList[2] = new SqlParameter("@P_DocFileName", docFileName);
                paramList[3] = new SqlParameter("@P_DocFilePath", docFilePath);
                paramList[4] = new SqlParameter("@P_CreatedBy", createdBy);
                paramList[5] = new SqlParameter("@P_DOC_ID_RET", SqlDbType.Int, 200);
                paramList[5].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("usp_Corp_Profile_Doc_Add_Update", paramList);
                retDocId = Convert.ToInt32(paramList[5].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CorpProfileDocuments> Load(long profileId)
        {
           //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<CorpProfileDocuments> detailsList = new List<CorpProfileDocuments>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_PROFILE_ID", profileId);
                //SqlDataReader data = DBGateway.ExecuteReaderSP("USP_CORP_PROFILE_GET_DOC_DETAILS", paramList, connection);
                using (DataTable dtCorp = DBGateway.FillDataTableSP("USP_CORP_PROFILE_GET_DOC_DETAILS", paramList))
                {
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    if (dtCorp !=null && dtCorp.Rows.Count > 0)
                    {
                        foreach(DataRow data in dtCorp.Rows)
                        {
                            CorpProfileDocuments visaDetails = new CorpProfileDocuments();
                            if (data["DOCID"] != DBNull.Value)
                            {
                                visaDetails.docId = Convert.ToInt32(data["DOCID"]);
                            }
                            if (data["PROFILEID"] != DBNull.Value)
                            {
                                visaDetails.profileId = Convert.ToInt32(data["PROFILEID"]);
                            }
                            if (data["DOCTYPENAME"] != DBNull.Value)
                            {
                                visaDetails.docTypeName = Convert.ToString(data["DOCTYPENAME"]);
                            }
                            if (data["DOCFILENAME"] != DBNull.Value)
                            {
                                visaDetails.docFileName = Convert.ToString(data["DOCFILENAME"]);
                            }
                            if (data["DOCFILEPATH"] != DBNull.Value)
                            {
                                visaDetails.docFilePath = Convert.ToString(data["DOCFILEPATH"]);
                            }

                            if (data["CREATEDBY"] != DBNull.Value)
                            {
                                visaDetails.createdBy = Convert.ToInt32(data["CREATEDBY"]);
                            }
                            detailsList.Add(visaDetails);
                        }
                    }
                }
                //data.Close();
               //connection.Close();
                return detailsList;
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

        public List<CorpProfileDocuments> GetDomenttailsByPax(string passportno,int agentid)
        {            
            try
            {
                List<CorpProfileDocuments> documentssList = new List<CorpProfileDocuments>();
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@PASSPORTNO", passportno);
                paramList[1] = new SqlParameter("@AGENTID", agentid);
                using (DataTable dtCorp = DBGateway.FillDataTableSP("USP_CORP_DOC_DETAILS_BY_PAX", paramList))
                {
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    if (dtCorp != null && dtCorp.Rows.Count > 0)
                    {
                        foreach (DataRow data in dtCorp.Rows)
                        {
                            CorpProfileDocuments visaDetails = new CorpProfileDocuments();
                            if (data["DOCID"] != DBNull.Value)
                            {
                                visaDetails.docId = Convert.ToInt32(data["DOCID"]);
                            }
                            if (data["PROFILEID"] != DBNull.Value)
                            {
                                visaDetails.profileId = Convert.ToInt32(data["PROFILEID"]);
                            }
                            if (data["DOCTYPENAME"] != DBNull.Value)
                            {
                                visaDetails.docTypeName = Convert.ToString(data["DOCTYPENAME"]);
                            }
                            if (data["DOCFILENAME"] != DBNull.Value)
                            {
                                visaDetails.docFileName = Convert.ToString(data["DOCFILENAME"]);
                            }
                            if (data["DOCFILEPATH"] != DBNull.Value)
                            {
                                visaDetails.docFilePath = Convert.ToString(data["DOCFILEPATH"]);
                            }
                            if (data["CREATEDBY"] != DBNull.Value)
                            {
                                visaDetails.createdBy = Convert.ToInt32(data["CREATEDBY"]);
                            }
                           visaDetails.PassportNo = passportno;

                            documentssList.Add(visaDetails);
                        }
                    }
                }                
                return documentssList;
            }
            catch (Exception ex)
            {               
                throw ex;
            }
        }
    }
}
