﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.Corporate
{
    public class CorporateProfileTripDetails
    {

        int _flightId;
        string _empId;
        string _empName;
        string _empEmail;
        List<CorpProfileApproval> _profileApproversList = new List<CorpProfileApproval>();
        int profileId;

        public int FlightId
        {
            get { return _flightId; }
            set { _flightId = value;}
        }

        public List<CorpProfileApproval> ProfileApproversList
        {
            get { return _profileApproversList; }
            set { _profileApproversList = value; }
        }

        public string EmpId
        {
            get { return _empId; }
            set { _empId = value; }
        }

        public string EmpName
        {
            get { return _empName; }
            set { _empName = value; }
        }

        public string EmpEmail {
            get { return _empEmail; }
            set { _empEmail = value; }
        }

        public int ProfileId { get => profileId; set => profileId = value; }

        public CorporateProfileTripDetails()
        {

        }
        public CorporateProfileTripDetails(int flightId)
        {
           Load(flightId);
        }

        public void Load(int flightId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_FlightId", flightId);
                DataSet ds = new DataSet();
                ds = DBGateway.ExecuteQuery("usp_Get_Corp_Trip_Details_By_FlightId", paramList);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                    }    
                    if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow data in ds.Tables[1].Rows)
                        {
                            CorpProfileApproval approver = new CorpProfileApproval();
                            if (data["ApproverId"] != DBNull.Value)
                            {
                                approver.ApproverId = Convert.ToInt32(data["ApproverId"]);
                            }
                            if (data["ApproverHierarchy"] != DBNull.Value)
                            {
                                approver.Hierarchy = Convert.ToInt32(data["ApproverHierarchy"]);
                            }
                            if (data["APPNAME"] != DBNull.Value)
                            {
                                approver.ApproverName = Convert.ToString(data["APPNAME"]);
                            }
                            if (data["APPEMAIL"] != DBNull.Value)
                            {
                                approver.ApproverEmail = Convert.ToString(data["APPEMAIL"]);
                            }
                            if (data["ApprovalStatus"] != DBNull.Value)
                            {
                                approver.ApprovalStatus = Convert.ToString(data["ApprovalStatus"]) == "A" ? "Approved" : Convert.ToString(data["ApprovalStatus"]) == "R" ? "Rejected" : "Awaiting Approval";
                            }
                            _profileApproversList.Add(approver);
                        }
                    }
                    if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow data in ds.Tables[2].Rows)
                        {
                            if (data["Name"] != DBNull.Value)
                            {
                                _empName = Convert.ToString(data["Name"]);
                            }
                            if (data["Email"] != DBNull.Value)
                            {
                                _empEmail = Convert.ToString(data["Email"]);
                            }
                            if (data["EmployeeId"] != DBNull.Value)
                            {
                                _empId = Convert.ToString(data["EmployeeId"]);
                            }
                            if (data["profileId"] != DBNull.Value)
                            {
                                profileId = Convert.ToInt32(data["profileId"]);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static void UpdateSelectedTrip(int flightId)
        //{
        //    try
        //    {

        //        SqlParameter[] paramArr = new SqlParameter[1];
        //        paramArr[0] = new SqlParameter("@P_Flight_Id", flightId);
        //        DBGateway.ExecuteNonQuery("usp_Update_Profile_Selected_Trip", paramArr);
        //    }
        //    catch { throw; }
        //}

        public static void UpdateTripApprovalStatus(int expDetailId, string approvalStatus, int modifiedBy, string remarks, int selTripId)
        {
            try
            {

                SqlParameter[] paramArr = new SqlParameter[5];
                paramArr[0] = new SqlParameter("@P_ExpDetail_ID", expDetailId);
                paramArr[1] = new SqlParameter("@P_APPROVAL_STATUS", approvalStatus);
                paramArr[2] = new SqlParameter("@P_CORPORATE_PROFILE_ID", modifiedBy);
                if (!string.IsNullOrEmpty(remarks))
                {
                    paramArr[3] = new SqlParameter("@P_REMARKs", remarks);
                }
                else
                {
                    paramArr[3] = new SqlParameter("@P_REMARKs", DBNull.Value);
                }
                paramArr[4] = new SqlParameter("@P_SELECTED_TRIP", selTripId);
                DBGateway.ExecuteNonQuery("usp_Update_Profile_Trip_Transc_Approval_Status", paramArr);
            }
            catch { throw; }
        }

        public static DataTable GetLocalTripDetailsQueueList(int profileId, DateTime fromDate, DateTime toDate, string approvalStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                if (profileId > 0)
                {
                    paramList[0] = new SqlParameter("@P_UserId", profileId);
                }
                else
                {
                    paramList[0] = new SqlParameter("@P_UserId", DBNull.Value);
                }
                paramList[1] = new SqlParameter("@P_FromDate", fromDate);
                paramList[2] = new SqlParameter("@P_ToDate", toDate);
                paramList[3] = new SqlParameter("@P_ApprovalStatus", approvalStatus);
            return DBGateway.FillDataTableSP("usp_Get_Corp_Local_Trip_Details_Queue_List", paramList);
            }
            catch
            {
                throw;
            }
        }
     
    }
}
