﻿using System;
using System.Collections.Generic;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace CT.Corporate
{
    public enum Category
    {
        NEGOTIATEDFARES = 1,
        SECURITYRULES = 2,
        PREFERREDAIRLINES = 3,
        AIRLINESTOAVOID = 4,
        AIRFARESHOPPING = 5,
        //CABINTYPERULES=6,
        DESTINATIONTOAVOID = 7,
        DATESTOAVOID = 8,
        DEFAULTCABIN = 9,
        EXCEPTIONCABIN = 10,

        //Lokesh: 31May2017 -- CorporatePolicies.aspx -- Approval Tab --Categories
        PRETRIPAPPROVAL =11,
        RELAUNCHAPPROVAL = 12 ,
        PREVISAAPPROVAL =13,        
        AVOIDTRANSITTIME=14,
        AUTOTICKETING = 15,
        COMPLETEBOOKING=16,
        RESTRICTBOOKING=17,
        HOTELBOOKINGTHRESHOLD=18,
        TOTALBOOKINGAMOUNT=19,
        BOOKINGTHRESHOLD=20,
        OVERNIGHTBOOKING=21


    }
    public class PolicyMaster
    {
        #region Variables
        int _policyId;
        string _policyCode;
        string _policyName;
        List<DateTime> _datesToAvoidList = new List<DateTime>();
        int _agentId;
        string _status;
        int _createdBy;
        int _modifiedBy;
        List<PolicyDetails> _policyDetailsList = new List<PolicyDetails>();
        List<PolicyExpenseDetails> _policyExpenseDetailsList = new List<PolicyExpenseDetails>();
        #endregion

        #region Properities

        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }


        public int PolicyId
        {
            get { return _policyId; }
            set { _policyId = value; }
        }
        public string PolicyCode
        {
            get { return _policyCode; }
            set { _policyCode = value; }
        }
        public string PolicyName
        {
            get { return _policyName; }
            set { _policyName = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public List<DateTime> DatesToAvoidList
        {
            get { return _datesToAvoidList; }
            set { _datesToAvoidList = value; }
        }
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public List<PolicyDetails> PolicyDetailsList
        {
            get { return _policyDetailsList; }
            set { _policyDetailsList = value; }
        }
        public List<PolicyExpenseDetails> PolicyExpenseDetailsList
        {
            get { return _policyExpenseDetailsList; }
            set { _policyExpenseDetailsList = value; }
        }

        #endregion

        #region Constructors
        public PolicyMaster()
        {
            _policyId = -1;
        }
        public PolicyMaster(long id)
        {
            _policyId = Convert.ToInt32(id);
            Load(_policyId);

        }
        #endregion



        public static DataTable GetAllCorporatePolicies(char recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                return DBGateway.ExecuteQuery("usp_GetAllCorporatePolicies", paramList).Tables[0];

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        //Load PolicyData
        public void Load(int policyId)
        {
            IFormatProvider mFomatter = new System.Globalization.CultureInfo("en-US");
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_PolicyId", policyId);
            try
            {
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_PolicyGetData", paramList, connection);
                using (DataTable dtPolicy = DBGateway.FillDataTableSP("usp_PolicyGetData", paramList))
                {
                    if (dtPolicy !=null && dtPolicy.Rows.Count > 0)
                    {
                        DataRow data = dtPolicy.Rows[0];
                        if (data["policyId"] != DBNull.Value)
                        {
                            _policyId = Convert.ToInt32(data["policyId"]);
                        }
                        if (data["policyCode"] != DBNull.Value)
                        {
                            _policyCode = Convert.ToString(data["policyCode"]);
                        }
                        if (data["policyName"] != DBNull.Value)
                        {
                            _policyName = Convert.ToString(data["policyName"]);
                        }
                        if (data["datesToAvoid"] != DBNull.Value)
                        {
                            string datesToAvoid = Convert.ToString(data["datesToAvoid"]);
                            if (!string.IsNullOrEmpty(datesToAvoid))
                            {
                                _datesToAvoidList = new List<DateTime>();
                                string[] datesArry = datesToAvoid.Split('|');
                                foreach (string date in datesArry)
                                {
                                    try
                                    {
                                        DateTime edate = DateTime.Parse(date, mFomatter);
                                        _datesToAvoidList.Add(edate);
                                    }
                                    catch { }
                                }
                            }
                        }
                        if (data["agentId"] != DBNull.Value)
                        {
                            _agentId = Convert.ToInt32(data["agentId"]);
                        }
                        if (data["status"] != DBNull.Value)
                        {
                            _status = Convert.ToString(data["status"]);
                        }
                        if (data["createdBy"] != DBNull.Value)
                        {
                            _createdBy = Convert.ToInt32(data["createdBy"]);
                        }

                        PolicyDetails policyDetail = new PolicyDetails();
                        _policyDetailsList = policyDetail.Load(_policyId);
                        PolicyExpenseDetails policyExpenseDetail = new PolicyExpenseDetails();
                        _policyExpenseDetailsList = policyExpenseDetail.Load(_policyId);
                    }
                }
                //data.Close();
                //connection.Close();
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

        public List<PolicyMaster> GetPolicyByProfileId(int profileId, int productId,int travelReasonId)
        {
            SqlConnection connection = DBGateway.GetConnection();
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_PROFILEID", profileId);
                paramList[1] = new SqlParameter("@P_PRODUCTID", productId);
                if(travelReasonId>0) paramList[2] = new SqlParameter("@P_TRAVEL_REASON", travelReasonId);

                DataTable dt = DBGateway.ExecuteQuery("Corp_Policy_Getlist_ByProfileId", paramList).Tables[0];

                DataTable dtTemp = dt.Copy();
                int policyTempId = 0;
                List<PolicyMaster> policyList = new List<PolicyMaster>();
                foreach (DataRow drPolicyHdr in dt.Rows)
                {
                    if (policyTempId != Convert.ToInt32(drPolicyHdr["PolicyId"]))
                    {
                        policyTempId = Convert.ToInt32(drPolicyHdr["PolicyId"]);
                        PolicyMaster policy = new PolicyMaster();
                        policy.PolicyId = Convert.ToInt32(drPolicyHdr["PolicyId"]);
                        policy.PolicyCode = Convert.ToString(drPolicyHdr["PolicyCode"]);
                        policy.PolicyName = Convert.ToString(drPolicyHdr["PolicyName"]);

                        string datesToAvoid = Convert.ToString(drPolicyHdr["DatesToAvoid"]);
                        if (!string.IsNullOrEmpty(datesToAvoid))
                        {
                            policy.DatesToAvoidList = new List<DateTime>();
                            string[] datesArry = datesToAvoid.Split('|');
                            foreach (string date in datesArry)
                            {
                                try
                                {
                                    policy.DatesToAvoidList.Add(Convert.ToDateTime(date));
                                }
                                catch { }
                            }
                        }

                        policy.AgentId = Convert.ToInt32(drPolicyHdr["AgentId"]);
                        policy.Status = Convert.ToString(drPolicyHdr["Status"]);
                        policy.CreatedBy = Convert.ToInt32(drPolicyHdr["CreatedBy"]);

                        List<PolicyDetails> policyDtls = new List<PolicyDetails>();

                        //DataView dv = dtTemp.DefaultView;
                        //dv.RowFilter = "PolicyId=" + policy.PolicyId;
                        //foreach (DataRow drPolicyDetails in dv.ToTable().Rows)
                        foreach (DataRow drPolicyDetails in dtTemp.Select("PolicyId=" + policy.PolicyId))
                        {
                            //if (policy.PolicyId == Convert.ToInt32(drPolicyDetails["PolicyId"]))
                            // {
                            PolicyDetails objPolicy = new PolicyDetails();
                            objPolicy.Id = Convert.ToInt32(drPolicyDetails["DetailId"]);
                            objPolicy.PolicyId = Convert.ToInt32(drPolicyDetails["PolicyId"]);
                            objPolicy.ProductId = Convert.ToInt32(drPolicyDetails["ProductId"]);
                            //objPolicy.Category = (Category)(drPolicyDetails["category"]);                                
                            objPolicy.Category = (Category)Enum.Parse(typeof(Category), drPolicyDetails["category"].ToString());
                            objPolicy.FilterType = Convert.ToString(drPolicyDetails["filtertype"]);
                            objPolicy.FilterValue1 = Convert.ToString(drPolicyDetails["filterValue1"]);
                            objPolicy.FilterValue2 = Convert.ToString(drPolicyDetails["filterValue2"]);
                            objPolicy.FilterValue3 = Convert.ToString(drPolicyDetails["filterValue3"]);
                            objPolicy.FilterValue4 = Convert.ToString(drPolicyDetails["filterValue4"]);
                            objPolicy.Status = Convert.ToString(drPolicyDetails["detailsStatus"]);
                            objPolicy.CreatedBy = Convert.ToInt32(drPolicyDetails["DetailsCreatedBy"]);
                            policyDtls.Add(objPolicy);

                            //} 
                        }
                        policy.PolicyDetailsList = policyDtls;

                        policyList.Add(policy);
                    }
                }
                connection.Close();
                return policyList;

            }

            catch (Exception ex)
            {
                connection.Close();
                throw ex;
            }
        }


        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                string datesTo = string.Empty;
                if (_datesToAvoidList != null && _datesToAvoidList.Count > 0)
                {
                    foreach (DateTime dt in _datesToAvoidList)
                    {
                        if (string.IsNullOrEmpty(datesTo))
                        {
                            //  datesTo = Convert.ToString(dt.ToString("dd/MM/yyyy"));
                            datesTo = Convert.ToString(dt.ToString("yyyy/MM/dd"));
                        }
                        else
                        {
                            datesTo = datesTo + "|" + Convert.ToString(dt.ToString("yyyy/MM/dd"));
                        }
                    }
                }

                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@P_PolicyCode", _policyCode);
                paramList[1] = new SqlParameter("@P_DatesToAvoid", datesTo);
                paramList[2] = new SqlParameter("@P_AgentId", _agentId);
                paramList[3] = new SqlParameter("@P_Status", _status);
                if (_policyId == -1)
                {
                    paramList[4] = new SqlParameter("@P_CreatedBy", _createdBy);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_CreatedBy", _modifiedBy);
                }
                paramList[5] = new SqlParameter("@P_PolicyId", _policyId);

                paramList[6] = new SqlParameter("@P_PolicyName", _policyName);
                paramList[7] = new SqlParameter("@P_Policy_ID_RET", SqlDbType.Int, 200);
                paramList[7].Direction = ParameterDirection.Output;


                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdateCorporatePolicyHeader", paramList);
                if (count > 0 && paramList[7].Value != DBNull.Value)
                {
                    _policyId = Convert.ToInt32(paramList[7].Value);

                    try
                    {
                        foreach (PolicyDetails pd in _policyDetailsList)
                        {
                            pd.PolicyId = _policyId;
                            pd.Save(cmd);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    try
                    {
                        foreach (PolicyExpenseDetails ped in _policyExpenseDetailsList)
                        {
                            ped.PolicyId = _policyId;
                            ped.Save(cmd);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }


                }
                trans.Commit();
            }
            catch (Exception ex)
            {

                trans.Rollback();
                throw ex;
            }
        }


        public static DataTable GetCorpPolicyLinkList(int profileId)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                return DBGateway.ExecuteQuery("usp_Get_Corp_Policy_Link_List", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        

    }

    public class PolicyDetails
    {
        #region variables
        int _id;
        int _policyId;
        int _productId;
        Category _category;
        string _filterType;
        string _filterValue1;
        string _filterValue2;
        string _filterValue3;
        string _filterValue4;
        string _status;
        int _createdBy;
        int _modifiedBy;


        #endregion

        #region Properities
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int PolicyId
        {
            get { return _policyId; }
            set { _policyId = value; }
        }
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public Category Category
        {
            get { return _category; }
            set { _category = value; }
        }
        public string FilterType
        {
            get { return _filterType; }
            set { _filterType = value; }
        }
        public string FilterValue1
        {
            get { return _filterValue1; }
            set { _filterValue1 = value; }
        }
        public string FilterValue2
        {
            get { return _filterValue2; }
            set { _filterValue2 = value; }
        }
        public string FilterValue3
        {
            get { return _filterValue3; }
            set { _filterValue3 = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        public string FilterValue4 { get => _filterValue4; set => _filterValue4 = value; }

        #endregion
        public List<PolicyDetails> Load(int policyId)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<PolicyDetails> policyList = new List<PolicyDetails>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_PolicyId", policyId);
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetPolicyDetails", paramList, connection);
                using (DataTable dtPolicy = DBGateway.FillDataTableSP("usp_GetPolicyDetails", paramList))
                {
                    if (dtPolicy !=null && dtPolicy.Rows.Count > 0)
                    {
                        foreach(DataRow data in dtPolicy.Rows)
                        {
                            PolicyDetails objPolicy = new PolicyDetails();
                            if (data["DetailId"] != DBNull.Value)
                            {
                                objPolicy._id = Convert.ToInt32(data["DetailId"]);
                            }
                            if (data["policyId"] != DBNull.Value)
                            {
                                objPolicy._policyId = Convert.ToInt32(data["policyId"]);
                            }
                            if (data["productId"] != DBNull.Value)
                            {
                                objPolicy._productId = Convert.ToInt32(data["productId"]);
                            }
                            if (data["category"] != DBNull.Value)
                            {
                                objPolicy._category = (Category)(Enum.Parse(typeof(Category), (data["category"].ToString())));
                            }
                            if (data["filtertype"] != DBNull.Value)
                            {
                                objPolicy._filterType = Convert.ToString(data["filtertype"]);
                            }
                            if (data["filterValue1"] != DBNull.Value)
                            {
                                objPolicy._filterValue1 = Convert.ToString(data["filterValue1"]);
                            }
                            if (data["filterValue2"] != DBNull.Value)
                            {
                                objPolicy._filterValue2 = Convert.ToString(data["filterValue2"]);
                            }
                            if (data["filterValue3"] != DBNull.Value)
                            {
                                objPolicy._filterValue3 = Convert.ToString(data["filterValue3"]);
                            }
                            if (data["filterValue4"] != DBNull.Value)
                            {
                                objPolicy._filterValue4 = Convert.ToString(data["filterValue4"]);
                            }
                            else
                                objPolicy._filterValue4 = string.Empty;
                            if (data["status"] != DBNull.Value)
                            {
                                objPolicy._status = Convert.ToString(data["status"]);
                            }
                            if (data["createdBy"] != DBNull.Value)
                            {
                                objPolicy._createdBy = Convert.ToInt32(data["createdBy"]);
                            }
                            policyList.Add(objPolicy);
                        }
                    }
                }
                //data.Close();
                //connection.Close();
                return policyList;
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

        public void Save(SqlCommand cmd)
        {

           
            try
            {
               
                SqlParameter[] paramList = new SqlParameter[10];
                paramList[0] = new SqlParameter("@P_PolicyId", _policyId);
                paramList[1] = new SqlParameter("@P_productId", _productId);
                paramList[2] = new SqlParameter("@P_Category", _category.ToString());
                paramList[3] = new SqlParameter("@P_FilterType", _filterType);
                paramList[4] = new SqlParameter("@P_FilterValue1", _filterValue1);
                paramList[5] = new SqlParameter("@P_Status", _status);
                if (_id == -1)
                {
                    paramList[6] = new SqlParameter("@P_CreatedBy", _createdBy);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_CreatedBy", _modifiedBy);
                }
                paramList[7] = new SqlParameter("@P_detailId", _id);
                paramList[8] = new SqlParameter("@P_FilterValue2", _filterValue2);
                paramList[9] = new SqlParameter("@P_FilterValue3", _filterValue3);
                List<SqlParameter> parameters = new List<SqlParameter>(paramList);
                if (!string.IsNullOrEmpty(_filterValue4))
                    parameters.Add(new SqlParameter("@P_FilterValue4", _filterValue4));

                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdateCorporatePolicyDetails", parameters.ToArray());
            }
            catch (Exception ex)
            {


                throw ex;
            }
        }

    }

    /// <summary>
    /// This class holds the All ExpenseDetails for a Particular Policy
    /// </summary>
    public class PolicyExpenseDetails
    {
        #region Variables
        private int _expId;
        private int _policyId;
        private string _countryCode;
        private string _cityCode;
        private string _currency;
        private decimal _amount;
        private int _setupId;
        private string _type;
        private string _status;
        private int _createdBy;
        private int _modifiedBy;
        private bool _incudeTravelDays;
        private string _dailyActuals;
        
        #endregion

        #region Properties
        public bool IncudeTravelDays
        {
            get { return _incudeTravelDays; }
            set { _incudeTravelDays = value; }
        }

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }

        }

        public int ExpId
        {
            get { return _expId; }
            set { _expId = value; }
        }


        public int PolicyId
        {
            get { return _policyId; }
            set { _policyId = value; }
        }


        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }


        public string CityCode
        {
            get { return _cityCode; }
            set { _cityCode = value; }
        }


        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }

        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }



        public int SetupId
        {
            get { return _setupId; }
            set { _setupId = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string DailyActuals
        {
            get { return _dailyActuals; }
            set { _dailyActuals = value; }
        }

        #endregion

        /// <summary>
        /// Default Constructor
        /// </summary>
        public PolicyExpenseDetails()
        {

        }


        /// <summary>
        /// This method will all the return all the expense types from the table Corp_Profile_Setup
        /// </summary>
        /// <param name="recordStatus"></param>
        /// <param name="expenseTypeCode"></param>
        /// <returns></returns>
        public static DataTable GetAllExpenseTypes(Char recordStatus, string expenseTypeCode)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramList[1] = new SqlParameter("@P_EXPENSE_TYPE_CODE", expenseTypeCode);
                paramList[2] = new SqlParameter("@P_agent_id", CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                return DBGateway.ExecuteQuery("usp_GetExpenseTypeList", paramList).Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public static DataTable GetAllExpenseTypes(Char recordStatus, string expenseTypeCode,int agentid)
        //{
        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[3];
        //        paramList[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
        //        paramList[1] = new SqlParameter("@P_EXPENSE_TYPE_CODE", expenseTypeCode);
        //        if(agentid>0)paramList[2] = new SqlParameter("@P_agent_id", agentid);
        //        return DBGateway.ExecuteQuery("usp_GetExpenseTypeList", paramList).Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        /// <summary>
        /// Gets all the Policy Expense Details from the table Corp_Policy_Expense_Master for the supplied policy Id.
        /// </summary>
        /// <param name="policyId"></param>
        /// <returns></returns>
        public List<PolicyExpenseDetails> Load(int policyId)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<PolicyExpenseDetails> policyExpList = new List<PolicyExpenseDetails>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_PolicyId", policyId);
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetPolicyExpenseDetails", paramList, connection);
                using (DataTable dtPolicy = DBGateway.FillDataTableSP("usp_GetPolicyExpenseDetails", paramList))
                {
                    if (dtPolicy !=null && dtPolicy.Rows.Count > 0)
                    {
                        foreach(DataRow data in dtPolicy.Rows)
                        {
                            PolicyExpenseDetails objPolicyExpDetail = new PolicyExpenseDetails();
                            if (data["ExpId"] != DBNull.Value)
                            {
                                objPolicyExpDetail._expId = Convert.ToInt32(data["ExpId"]);
                            }
                            if (data["policyId"] != DBNull.Value)
                            {
                                objPolicyExpDetail._policyId = Convert.ToInt32(data["PolicyId"]);
                            }
                            if (data["CountryCode"] != DBNull.Value)
                            {
                                objPolicyExpDetail._countryCode = Convert.ToString(data["CountryCode"]);
                            }
                            if (data["CityCode"] != DBNull.Value)
                            {
                                objPolicyExpDetail._cityCode = Convert.ToString(data["CityCode"]);
                            }
                            if (data["Currency"] != DBNull.Value)
                            {
                                objPolicyExpDetail._currency = Convert.ToString(data["Currency"]);
                            }
                            if (data["Amount"] != DBNull.Value)
                            {
                                objPolicyExpDetail._amount = Convert.ToDecimal(data["Amount"]);
                            }
                            if (data["SetupId"] != DBNull.Value)
                            {
                                objPolicyExpDetail._setupId = Convert.ToInt32(data["SetupId"]);
                            }
                            if (data["Type"] != DBNull.Value)
                            {
                                objPolicyExpDetail._type = Convert.ToString(data["Type"]);
                            }
                            if (data["Status"] != DBNull.Value)
                            {
                                objPolicyExpDetail._status = Convert.ToString(data["Status"]);
                            }
                            if (data["CreatedBy"] != DBNull.Value)
                            {
                                objPolicyExpDetail._createdBy = Convert.ToInt32(data["CreatedBy"]);
                            }
                            if (data["IncludeTravelDays"] != DBNull.Value)
                            {
                                objPolicyExpDetail._incudeTravelDays = Convert.ToBoolean(data["IncludeTravelDays"]);
                            }
                            if (data["Daily"] != DBNull.Value)
                            {
                                objPolicyExpDetail._dailyActuals = Convert.ToString(data["Daily"]);
                            }


                            policyExpList.Add(objPolicyExpDetail);
                        }
                    }
                }
                //data.Close();
                //connection.Close();
                return policyExpList;
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

        /// <summary>
        /// Saves the Expense Details in the table Corp_Policy_Expense_Master  against the policy Id
        /// </summary>
        public void Save(SqlCommand cmd)
        {

           
            try
            {
              
                SqlParameter[] paramList = new SqlParameter[12];
                paramList[0] = new SqlParameter("@P_PolicyId", _policyId);
                paramList[1] = new SqlParameter("@P_CountryCode", _countryCode);
                paramList[2] = new SqlParameter("@P_Currency", _currency);
                paramList[3] = new SqlParameter("@P_Amount", _amount);
                if (!string.IsNullOrEmpty(Convert.ToString(_setupId)))
                {
                    paramList[4] = new SqlParameter("@P_SetupId", _setupId);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_SetupId", DBNull.Value);
                }
                paramList[5] = new SqlParameter("@P_Status", _status);
                if (_expId == -1)
                {
                    paramList[6] = new SqlParameter("@P_CreatedBy", _createdBy);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_CreatedBy", _modifiedBy);
                }
                paramList[7] = new SqlParameter("@P_Type", _type);
                paramList[8] = new SqlParameter("@P_ExpId", _expId);
                paramList[9] = new SqlParameter("@P_CityCode", _cityCode);
                paramList[10] = new SqlParameter("@P_IncludeTraveldays", _incudeTravelDays);
                paramList[11] = new SqlParameter("@P_DailyActuals", _dailyActuals);
                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdateCorporatePolicyExpenseDetails", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }

}
