﻿using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CorporateTravelReason
/// </summary>
namespace CT.Corporate
{
    public class CorporateTravelReason
{
        #region Variables
        long _reasonId;
        int _agentId;
        string _code;
        string _description;
        string _type;
        string _actionStatus;
        string _status;
        int _createdBy;
        #endregion
        #region Properties
        public long ReasonId
        {
            get { return _reasonId; }
            set { _reasonId = value; }
        }
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string ActionStatus
        {
            get { return _actionStatus; }
            set { _actionStatus = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion
        #region Constructors
        public CorporateTravelReason()
        {
            
        }
        public CorporateTravelReason(long reasonid)
        {
            _reasonId = reasonid;
            GetDetails(_reasonId);
        }
        #endregion
        #region Private Methods
        private void GetDetails(long reasonid)
        {
            try
            {
                DataTable dt = GetData(reasonid);
                UpdataBusinessData(dt);
            }
            catch
            {
                throw;
            }
        }
        private DataTable GetData(long id)
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@P_REASONID", id);
                return DBGateway.FillDataTableSP("USP_CORP_TRAVEL_REASON_GET_DATA", paramlist);
            }
            catch
            {
                throw;
            }
        }
        private void UpdataBusinessData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["REASONID"] != null)
                    {
                        _reasonId = Convert.ToInt32(dr["REASONID"]);
                    }
                    if (dr["AGENTID"] != null)
                    {
                        _agentId = Convert.ToInt32(dr["AGENTID"]);
                    }
                    if (dr["CODE"] != null)
                    {
                        _code = Convert.ToString(dr["CODE"]);
                    }
                    if (dr["DESCRIPTION"] != null)
                    {
                        _description = Convert.ToString(dr["DESCRIPTION"]);
                    }
                    if (dr["TYPE"] != null)
                    {
                        _type = Convert.ToString(dr["TYPE"]);
                    }
                    if (dr["ACTIONSTATUS"] != null)
                    {
                        _actionStatus = Convert.ToString(dr["ACTIONSTATUS"]);
                    }
                    if (dr["STATUS"] != null)
                    {
                        _status = Convert.ToString(dr["STATUS"]);
                    }
                }
            }
        }
        #endregion
        #region Public Metohd
        public void Save()
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[10];
                paramlist[0] = new SqlParameter("@P_REASONID", _reasonId);
                paramlist[1] = new SqlParameter("@P_AGENTID", _agentId);
                paramlist[2] = new SqlParameter("@P_CODE", _code);
                paramlist[3] = new SqlParameter("@P_DESCRIPTION", _description);
                paramlist[4] = new SqlParameter("@P_TYPE", _type);
                paramlist[5] = new SqlParameter("@P_ACTIONSTATUS", _actionStatus);
                paramlist[6] = new SqlParameter("@P_STATUS", _status);
                paramlist[7] = new SqlParameter("@P_CREATEDBY", _createdBy);               
                paramlist[8] = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar, 10);
                paramlist[8].Direction = ParameterDirection.Output;
                paramlist[9] = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar, 100);
                paramlist[9].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("USP_CORP_TRAVEL_REASON_ADD_UPDATE", paramlist);
                string messagetype = Convert.ToString(paramlist[8].Value);
                if (messagetype == "E")
                {
                    string message = Convert.ToString(paramlist[9].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Static Methods
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                return DBGateway.FillDataTableSP("USP_CORP_TRAVEL_REASON_GET_LIST", paramList);
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }
}