﻿using System;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for TravelUtility
/// </summary>

namespace CT.Corporate
{
    public class TravelUtility
    {
	    public TravelUtility()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }


        public static DataTable GetTravelReasonList(string type,long agentId,ListStatus status)
        {
            return GetTravelReasonList(type, agentId, status, 1);
        }

        public static DataTable GetTravelReasonList(string type, long agentId, ListStatus status, MemberType memberType)
        {
            return GetTravelReasonList(type, agentId, status, 1, memberType);
        }

        /// <summary>
        /// Overload method to get the travel reason list based on product id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="agentId"></param>
        /// <param name="status"></param>
        /// <param name="ProductID"></param>
        /// <returns></returns>
        public static DataTable GetTravelReasonList(string type, long agentId, ListStatus status, int ProductID)
        {            
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if (!string.IsNullOrEmpty(type)) paramList[0] = new SqlParameter("@P_Type", type);
                if (agentId > 0) paramList[1] = new SqlParameter("@P_AgentId", agentId);
                paramList[2] = new SqlParameter("@P_LISTSTATUS", (int)status);                
                if (Settings.LoginInfo != null && Settings.LoginInfo.MemberType != MemberType.TRAVELCORDINATOR) paramList[3] = new SqlParameter("@P_user_Type", Settings.LoginInfo.MemberType.ToString());                
                paramList[4] = new SqlParameter("@P_ProductID", (int)ProductID);
                DataTable dt = DBGateway.FillDataTableSP("Corp_Travel_Reason_Getlist", paramList);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetTravelReasonList(string type, long agentId, ListStatus status, int ProductID, MemberType memberType)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if (!string.IsNullOrEmpty(type)) paramList[0] = new SqlParameter("@P_Type", type);
                if (agentId > 0) paramList[1] = new SqlParameter("@P_AgentId", agentId);
                paramList[2] = new SqlParameter("@P_LISTSTATUS", (int)status);
                if (memberType != MemberType.TRAVELCORDINATOR) paramList[3] = new SqlParameter("@P_user_Type", memberType.ToString());
                paramList[4] = new SqlParameter("@P_ProductID", (int)ProductID);
                DataTable dt = DBGateway.FillDataTableSP("Corp_Travel_Reason_Getlist", paramList);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetProfileList(long profileId, long agentId, ListStatus status)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId );
                if (agentId > 0) paramList[1] = new SqlParameter("@P_AgentId", agentId);
                paramList[2] = new SqlParameter("@P_LIST_STATUS", (int)status);
                DataTable dt = DBGateway.FillDataTableSP("Corp_Profile_Getlist", paramList);
                //connection.Close();
                return dt;

            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }


        public static DataTable GetPolicyBreakingRules(long profileId, int productId, string category)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                paramList[1] = new SqlParameter("@P_ProductId", productId);
                paramList[2] = new SqlParameter("@P_Category", category);

                dt = DBGateway.FillDataTableSP("usp_GetPolicyBreakingRules", paramList);
            }
            catch (Exception ex)
            {                
                throw ex;
            }

            return dt;
        }
		
		/// <summary>
        /// To get Excel download data
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="agentId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static DataTable GetExcelDatabySPName(DateTime FromDate, DateTime ToDate, string AgentIds, string SPName)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_FromDate", FromDate);
                paramList[1] = new SqlParameter("@P_ToDate", ToDate);
                paramList[2] = new SqlParameter("@P_AgentIds", AgentIds);
                DataTable dt = DBGateway.FillDataTableSP(SPName, paramList);
                //connection.Close();
                return dt;

            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }
    }
}
