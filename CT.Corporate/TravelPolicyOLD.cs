﻿using System;
using System.Collections.Generic;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.Corporate
{
    public class PolicyMasterTEMP
    {
        #region Variables
        int _policyId;
        string _policyCode;
        string _policyName;
        List<DateTime> _datesToAvoidList;
        int _agentId;
        string _status;
        int _createdBy;
        List<PlocyDetailsTEMP> _policyDetailsList;
        #endregion

        #region Properities
        public int PolicyId
        {
            get { return _policyId; }
            set { _policyId = value; }
        }
        public string PolicyCode
        {
            get { return _policyCode; }
            set { _policyCode = value; }
        }
        public string PolicyName
        {
            get { return _policyName; }
            set { _policyName = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public List<DateTime> DatesToAvoidList
        {
            get { return _datesToAvoidList; }
            set { _datesToAvoidList = value; }
        }
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public List<PlocyDetailsTEMP> PolicyDetailsList
        {
            get { return _policyDetailsList; }
            set { _policyDetailsList = value; }
        }
        #endregion

        #region Constructors
        public PolicyMasterTEMP()
        {

        }
        #endregion

        //Load PolicyData
        public void Load(int policyId)
        {
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_PolicyId", policyId);
            try
            {
                SqlDataReader data = DBGateway.ExecuteReaderSP("usp_PolicyGetData", paramList, connection);
                if (data.Read())
                {
                    if (data["policyId"] != DBNull.Value)
                    {
                        _policyId = Convert.ToInt32(data["policyId"]);
                    }
                    if (data["policyCode"] != DBNull.Value)
                    {
                        _policyCode = Convert.ToString(data["policyCode"]);
                    }
                    if (data["policyName"] != DBNull.Value)
                    {
                        _policyName = Convert.ToString(data["policyName"]);
                    }
                    if (data["datesToAvoid"] != DBNull.Value)
                    {
                        string datesToAvoid = Convert.ToString(data["datesToAvoid"]);
                        if (!string.IsNullOrEmpty(datesToAvoid))
                        {
                            _datesToAvoidList = new List<DateTime>();
                            string[] datesArry = datesToAvoid.Split('|');
                            foreach (string date in datesArry)
                            {
                                try
                                {
                                    _datesToAvoidList.Add(Convert.ToDateTime(date));
                                }
                                catch { }
                            }
                        }
                    }
                    if (data["agentId"] != DBNull.Value)
                    {
                        _agentId = Convert.ToInt32(data["agentId"]);
                    }
                    if (data["status"] != DBNull.Value)
                    {
                        _status = Convert.ToString(data["status"]);
                    }
                    if (data["createdBy"] != DBNull.Value)
                    {
                        _createdBy = Convert.ToInt32(data["createdBy"]);
                    }
                    PlocyDetailsTEMP policyDetail = new PlocyDetailsTEMP();
                    _policyDetailsList = policyDetail.Load(_policyId);
                }
                data.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw ex;
            }
        }
    }

    public class PlocyDetailsTEMP
    {
        #region variables
        int _id;
        int _policyId;
        int _productId;
        string _category;
        string _filterType;
        string _filterValue1;
        string _filterValue2;
        string _filterValue3;
        string _status;
        int _createdBy;
        #endregion

        #region Properities
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int PolicyId
        {
            get { return _policyId; }
            set { _policyId = value; }
        }
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        public string FilterType
        {
            get { return _filterType; }
            set { _filterType = value; }
        }
        public string FilterValue1
        {
            get { return _filterValue1; }
            set { _filterValue1 = value; }
        }
        public string FilterValue2
        {
            get { return _filterValue2; }
            set { _filterValue2 = value; }
        }
        public string FilterValue3
        {
            get { return _filterValue3; }
            set { _filterValue3 = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion
        public List<PlocyDetailsTEMP> Load(int policyId)
        {
            SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<PlocyDetailsTEMP> policyList = new List<PlocyDetailsTEMP>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_PolicyId", policyId);
                SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetPolicyDetails", paramList, connection);
                if (data.HasRows)
                {
                    while (data.Read())
                    {
                        PlocyDetailsTEMP objPolicy = new PlocyDetailsTEMP();
                        if (data["id"] != DBNull.Value)
                        {
                            objPolicy._id = Convert.ToInt32(data["id"]);
                        }
                        if (data["policyId"] != DBNull.Value)
                        {
                            objPolicy._policyId = Convert.ToInt32(data["policyId"]);
                        }
                        if (data["productId"] != DBNull.Value)
                        {
                            objPolicy._productId = Convert.ToInt32(data["productId"]);
                        }
                        if (data["category"] != DBNull.Value)
                        {
                            objPolicy._category = Convert.ToString(data["category"]);
                        }
                        if (data["filtertype"] != DBNull.Value)
                        {
                            objPolicy._filterType = Convert.ToString(data["filtertype"]);
                        }
                        if (data["filterValue1"] != DBNull.Value)
                        {
                            objPolicy._filterValue1 = Convert.ToString(data["filterValue1"]);
                        }
                        if (data["filterValue2"] != DBNull.Value)
                        {
                            objPolicy._filterValue2 = Convert.ToString(data["filterValue2"]);
                        }
                        if (data["filterValue3"] != DBNull.Value)
                        {
                            objPolicy._filterValue3 = Convert.ToString(data["filterValue3"]);
                        }
                        if (data["status"] != DBNull.Value)
                        {
                            objPolicy._status = Convert.ToString(data["status"]);
                        }
                        if (data["createdBy"] != DBNull.Value)
                        {
                            objPolicy._createdBy = Convert.ToInt32(data["createdBy"]);
                        }
                        policyList.Add(objPolicy);
                    }
                }
                data.Close();
                connection.Close();
                return policyList;
            }
            catch (Exception ex)
            {
                connection.Close();
                throw ex;
            }
        }
    }
}
