﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;

namespace CT.Corporate
{
    public class ExpensesUtility
    {        
        //To get categories list
        public static DataTable GetCategory(int agentId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AgentId", agentId);
                return DBGateway.FillDataTableSP("usp_GetExpCategoryList", paramList);
            }
            catch
            {
                throw;
            }
        }

        //To get types list
        public static DataTable GetExpType(int agentId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AgentId", agentId);
                return DBGateway.FillDataTableSP("usp_GetExpTypesList", paramList);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To get expense profiles list
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="agentId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static DataTable GetExpProfileList(long profileId, long agentId, ListStatus status)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                if (agentId > 0) paramList[1] = new SqlParameter("@P_AgentId", agentId);
                paramList[2] = new SqlParameter("@P_LIST_STATUS", (int)status);
                DataTable dt = DBGateway.FillDataTableSP("Corp_Profile_ExpGetlist", paramList);
                //connection.Close();
                return dt;

            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

        /// <summary>
        /// To get cost centres associated to the profile for expense
        /// </summary>
        /// <param name="profileid"></param>
        /// <returns></returns>
        public static DataTable GetExpCostCenters(long profileid)
        {
            DataTable dt = new DataTable();
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@ProfileId", profileid));
                dt = DBGateway.FillDataTableSP("usp_GetExpCostCenters", paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
