﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
/// <summary>
/// Summary description for Class1
/// </summary>
namespace CT.Corporate
{
    public class CorporateProfileSetup
    {
        //private long _id;
        private long _setupId;
        private int _agentId;
        private string _code;
        private string _name;
        private string _type;
        private string _flexV1;
        private string _flexV2;
        private string _flexV3;
        private string _flexV4;
        private string _status;
        private int _createdBy;

        public long SetupId
        {
            get
            { return _setupId; }
            set
            { _setupId = value; }
        }
        public int AgentId
        {
            get
            { return _agentId; }
            set
            { _agentId = value; }
        }
        public string Code
        {
            get
            { return _code; }
            set
            { _code = value; }
        }

        public string Name
        {
            get
            { return _name; }
            set
            { _name = value; }
        }

        public string Type
        {
            get
            { return _type; }
            set
            { _type = value; }
        }

        public string FlexV1
        {
            get
            { return _flexV1; }
            set
            { _flexV1 = value; }
        }

        public string FlexV2
        {
            get
            { return _flexV2; }
            set
            { _flexV2 = value; }
        }
        public string FlexV3
        {
            get
            { return _flexV3; }
            set
            { _flexV3 = value; }
        }
        public string FlexV4
        {
            get
            { return _flexV4; }
            set
            { _flexV4 = value; }
        }

        public string Status
        {
            get
            { return _status; }
            set
            { _status = value; }
        }


        public int CreatedBy
        {
            get
            { return _createdBy; }
            set
            { _createdBy = value; }
        }


        #region Constructors
        public CorporateProfileSetup()
        {

            _setupId = -1;

            DataSet ds = GetData(_setupId);
            //if (dt != null && dt.Rows.Count > 0)
            UpdateBusinessData(ds);
        }
        public CorporateProfileSetup(long id)
        {


            _setupId = id;
            GetDetails(id);
        }
        #endregion

        private void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
            //if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            //{
            //    UpdateBusinessData(ds.Tables[0].Rows[0]);
            //}

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_SetupId", id);
                DataSet dsResult = DBGateway.ExecuteQuery("Corp_Profile_Setup_GetData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _setupId = Utility.ToLong(dr["SetupId"]);
                _agentId = Utility.ToInteger(dr["AgentId"]); ;
                _code = Utility.ToString(dr["Code"]);
                _name = Utility.ToString(dr["Name"]);
                _type = Utility.ToString(dr["Type"]);
                _flexV1 = Utility.ToString(dr["FlexV1"]);
                _flexV2 = Utility.ToString(dr["FlexV2"]);
                _flexV3 = Utility.ToString(dr["FlexV3"]);
                _flexV4 = Utility.ToString(dr["FlexV4"]);
                _status = Utility.ToString(dr["Status"]);
                _createdBy = Utility.ToInteger(dr["CreatedBy"]);

            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                            UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }



            }
            catch { throw; }

        }
        #region Public methods
        public void Save()
        {
            //SqlTransaction trans = null;
            //SqlCommand cmd = null;
            try
            {
                //cmd = new SqlCommand();
                //cmd.Connection = DBGateway.CreateConnection();
                //cmd.Connection.Open();
                //trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                //cmd.Transaction = trans;

                SqlParameter[] paramList = new SqlParameter[13];
                paramList[0] = new SqlParameter("@P_SetupId", _setupId);
                paramList[1] = new SqlParameter("@P_AgentId", _agentId);
                paramList[2] = new SqlParameter("@P_Code", _code);
                paramList[3] = new SqlParameter("@P_Name", _name);
                paramList[4] = new SqlParameter("@P_Type", _type);
                if (!string.IsNullOrEmpty(_flexV1)) paramList[5] = new SqlParameter("@P_FlexV1", _flexV1);
                if (!string.IsNullOrEmpty(_flexV2)) paramList[6] = new SqlParameter("@P_FlexV2", _flexV2);
                if (!string.IsNullOrEmpty(_flexV3)) paramList[7] = new SqlParameter("@P_FlexV3", _flexV3);
                if (!string.IsNullOrEmpty(_flexV4)) paramList[8] = new SqlParameter("@P_FlexV4", _flexV4);

                paramList[9] = new SqlParameter("@P_Status", _status);
                paramList[10] = new SqlParameter("@P_CreatedBy", _createdBy);
                paramList[11] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[11].Direction = ParameterDirection.Output;
                paramList[12] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[12].Direction = ParameterDirection.Output;


                DBGateway.ExecuteNonQuery("Corp_Profile_SetUp_Add_Update", paramList);
                string messageType = Utility.ToString(paramList[11].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[12].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

            }
            catch
            {
                //trans.Rollback();
                throw;
            }

        }


        #endregion

        #region StaticMethods
        public static DataTable GetCorpProfileSetpDetails(int agentId, string type, ListStatus status)
        {
            SqlConnection con = DBGateway.CreateConnection();
            try
            {
                SqlParameter[] paramArray = new SqlParameter[3];
                paramArray[0] = new SqlParameter("@P_AGENTID", agentId);
                if (!string.IsNullOrEmpty(type)) paramArray[1] = new SqlParameter("@P_TYPE", type);
                paramArray[2] = new SqlParameter("@P_LIST_STATUS", (int)status);
                DataTable dt = DBGateway.ExecuteQuery("CORP_PROFILE_SETPUP_GETLIST", paramArray).Tables[0];
                con.Close();
                return dt;
            }

            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        public static DataTable GetList(int agentId, string type, ListStatus status)
        {
            SqlConnection con = DBGateway.CreateConnection();
            try
            {
                SqlParameter[] paramArray = new SqlParameter[3];
                paramArray[0] = new SqlParameter("@P_AGENTID", agentId);
                if (!string.IsNullOrEmpty(type)) paramArray[1] = new SqlParameter("@P_TYPE", type);
                paramArray[2] = new SqlParameter("@P_LIST_STATUS", (int)status);
                DataTable dt = DBGateway.ExecuteQuery("CORP_PROFILE_SETPUP_GETLIST", paramArray).Tables[0];
                con.Close();
                return dt;
            }

            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }



        #endregion
    }
}