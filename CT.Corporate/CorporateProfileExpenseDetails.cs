﻿using System;
using System.Data;
using System.Web;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Summary description for CorporateProfileExpenseDetails
/// </summary>
/// 
namespace CT.Corporate
{

    public class Corp_Profile_Expense_Reimbursement_Detail
    {
        int _expDetailId;
        decimal _claimAmount;
        string _currency;
        decimal _reimbursementAmount;
        decimal _totalReimbursementAmount;
        string _reimStatus;
        decimal _balance;
        string _status;
        int _createdBy;
        public int ExpDetailId
        {
            get { return _expDetailId; }
            set { _expDetailId = value; }
        }
        public decimal ClaimAmount
        {
            get { return _claimAmount; }
            set { _claimAmount = value; }
        }
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        public decimal ReimbursementAmount
        {
            get { return _reimbursementAmount; }
            set { _reimbursementAmount = value; }
        }

        public decimal TotalReimbursementAmount
        {
            get { return _totalReimbursementAmount; }
            set { _totalReimbursementAmount = value; }
        }
        public string ReimStatus
        {
            get { return _reimStatus; }
            set { _reimStatus = value; }
        }
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_ExpDetailId", _expDetailId);
                paramList[1] = new SqlParameter("@P_ClaimAmount", _claimAmount);
                paramList[2] = new SqlParameter("@P_Currency", _currency);
                paramList[3] = new SqlParameter("@P_ReimbursementAmount", _reimbursementAmount);
                // paramList[4] = new SqlParameter("@P_TotalReimbursementAmount", _totalReimbursementAmount);
                // paramList[5] = new SqlParameter("@P_ReimStatus", _reimStatus);
                //  paramList[6] = new SqlParameter("@P_Balance", _balance);
                paramList[4] = new SqlParameter("@P_Status", _status);
                paramList[5] = new SqlParameter("@P_CreatedBy", _createdBy);
                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_Add_Update_Corp_Exp_Reimbursement", paramList);
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }


    }

    public class Corp_Profile_Transc_Approval
    {
        #region Variables
        int _id;
        int _expDetailId;
        int _approverId;
        string _tranxType;
        int _approverHierarchy;
        string _isLastApproval;
        string _approvalStatus;
        string _status;
        int _createdBy;
        string _remarks;

        #endregion

        #region properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int ExpDetailId
        {
            get { return _expDetailId; }
            set { _expDetailId = value; }
        }
        public int ApproverId
        {
            get { return _approverId; }
            set { _approverId = value; }
        }
        public string TranxType
        {
            get { return _tranxType; }
            set { _tranxType = value; }
        }
        public int ApproverHierarchy
        {
            get { return _approverHierarchy; }
            set { _approverHierarchy = value; }
        }

        public string IsLastApproval
        {
            get { return _isLastApproval; }
            set { _isLastApproval = value; }
        }

        public string ApprovalStatus
        {
            get { return _approvalStatus; }
            set { _approvalStatus = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string Remarks { get => _remarks; set => _remarks = value; }



        #endregion

        public int SaveCorpProfileTransApproval(SqlCommand cmd)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[10];
                paramList[0] = new SqlParameter("@P_Id", _id);
                paramList[1] = new SqlParameter("@P_ExpDetailId", _expDetailId);
                paramList[2] = new SqlParameter("@P_TranxType", _tranxType);
                paramList[3] = new SqlParameter("@P_ApproverHierarchy", _approverHierarchy);
                if (string.IsNullOrEmpty(_isLastApproval))
                {
                    paramList[4] = new SqlParameter("@P_IsLastApproval", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_IsLastApproval", DBNull.Value);
                }
                paramList[5] = new SqlParameter("@P_ApprovalStatus", _approvalStatus);
                paramList[6] = new SqlParameter("@P_Status", _status);
                if (_id == -1)
                {
                    paramList[7] = new SqlParameter("@P_createdBy", _createdBy);
                }
                else
                {

                    paramList[7] = new SqlParameter("@P_createdBy", _createdBy);
                }
                paramList[8] = new SqlParameter("@P_ApproverId", _approverId);
                paramList[9] = new SqlParameter("@P_Transc_Approval_Id", SqlDbType.BigInt);
                paramList[9].Direction = ParameterDirection.Output;
                

                int count = DBGateway.ExecuteNonQueryDetails(cmd, "Corp_Profile_Transc_Approval_Add_Update", paramList);
                if (count > 0 && paramList[9].Value != DBNull.Value)
                {
                    _id = Convert.ToInt32(paramList[9].Value);
                }

            }
            catch { throw; }
            return _id;
        }

        public int SaveCorpProfileTripApprovals()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@P_Id", _id);
                paramList[1] = new SqlParameter("@P_ExpDetailId", _expDetailId);
                paramList[2] = new SqlParameter("@P_TranxType", _tranxType);
                paramList[3] = new SqlParameter("@P_ApproverHierarchy", _approverHierarchy);
                if (string.IsNullOrEmpty(_isLastApproval))
                {
                    paramList[4] = new SqlParameter("@P_IsLastApproval", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_IsLastApproval", DBNull.Value);
                }
                paramList[5] = new SqlParameter("@P_ApprovalStatus", _approvalStatus);
                paramList[6] = new SqlParameter("@P_Status", _status);
                if (_id == -1)
                {
                    paramList[7] = new SqlParameter("@P_createdBy", _createdBy);
                }
                else
                {

                    paramList[7] = new SqlParameter("@P_createdBy", _createdBy);
                }
                paramList[8] = new SqlParameter("@P_ApproverId", _approverId);
                paramList[9] = new SqlParameter("@P_Transc_Approval_Id", SqlDbType.BigInt);
                paramList[9].Direction = ParameterDirection.Output;
                paramList[10] = new SqlParameter("@P_Remarks", _remarks);
                int count = DBGateway.ExecuteNonQuerySP("Corp_Profile_Transc_Approval_Add_Update", paramList);
                if (count > 0 && paramList[9].Value != DBNull.Value)
                {
                    _id = Convert.ToInt32(paramList[9].Value);
                }

            }
            catch { throw; }
            return _id;
        }
    }

    public class CorporateProfileExpenseDocDetails
    {
        #region Variables
        int _docId;
        int _expDetailId;
        string _docDescription;
        string _fileType;
        string _filePath;
        string _status;
        int _createdBy;
        int _modifiedBy;
        #endregion

        #region Properties
        public int DocId
        {
            get { return _docId; }
            set { _docId = value; }
        }
        public int ExpDetailId
        {
            get { return _expDetailId; }
            set { _expDetailId = value; }
        }
        public string DocDescription
        {
            get { return _docDescription; }
            set { _docDescription = value; }
        }
        public string FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }
        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }



        #endregion

        #region Public methods
        public void SaveExpenseDocDetails(SqlCommand cmd)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@P_docId", _docId);
                paramList[1] = new SqlParameter("@P_expDetailId", _expDetailId);
                paramList[2] = new SqlParameter("@P_docDescription", _docDescription);
                paramList[3] = new SqlParameter("@P_fileType", _fileType);
                paramList[4] = new SqlParameter("@P_filePath", _filePath);
                paramList[5] = new SqlParameter("@P_status", _status);
                if (_docId == -1)
                {
                    paramList[6] = new SqlParameter("@P_createdBy", _createdBy);
                }
                else
                {

                    paramList[6] = new SqlParameter("@P_createdBy", _modifiedBy);
                }
                int count = DBGateway.ExecuteNonQueryDetails(cmd, "CORP_PROFILE_EXPENSE_DOC_DETAIL_ADD_UPDATE", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

    public class CorporateProfileExpenseDetails
    {
        #region Variables.
        int _expDetailId;
        DateTime _date;

        int _agentId;
        int _profileId;
        string _countryCode;
        string _cityCode;
        int _costCenterId;
        int _exptypeid;
        string _referenceCode;
        string _description;
        string _currency;
        decimal _amount;
        string _comment;
        string _docNo;
        string _type;
        string _refId;
        string _approvalHierarachy;
        string _approvalStatus;
        string _status;
        string _expmode;
        int _createdBy;
        int _modifiedBy;



        List<HttpPostedFile> _profileExpenseUploadedFilesList = new List<HttpPostedFile>();
        List<CorpProfileApproval> _profileApproversList = new List<CorpProfileApproval>();

        int _empId;
        string _empEmail;
        string _empName;

        string _expCategoryText;
        string _expTypeText;
        string _agencyName;
        string _reimAmount;
        string _reimCurrency;
        string _claimAmount;
        string _balanceAmount;






        #endregion

        #region Properties

        public int ExpDetailId
        {
            get { return _expDetailId; }
            set { _expDetailId = value; }
        }
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public int ProfileId
        {
            get { return _profileId; }
            set { _profileId = value; }
        }
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }
        public string CityCode
        {
            get { return _cityCode; }
            set { _cityCode = value; }
        }
        public int CostCenterId
        {
            get { return _costCenterId; }
            set { _costCenterId = value; }
        }
        public int Exptypeid
        {
            get { return _exptypeid; }
            set { _exptypeid = value; }
        }
        public string ReferenceCode
        {
            get { return _referenceCode; }
            set { _referenceCode = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
        public string DocNo
        {
            get { return _docNo; }
            set { _docNo = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string RefId
        {
            get { return _refId; }
            set { _refId = value; }
        }
        public string ApprovalHierarachy
        {
            get { return _approvalHierarachy; }
            set { _approvalHierarachy = value; }
        }
        public string ApprovalStatus
        {
            get { return _approvalStatus; }
            set { _approvalStatus = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public string Expmode
        {
            get { return _expmode; }
            set { _expmode = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        public List<CorpProfileApproval> ProfileApproversList
        {
            get { return _profileApproversList; }
            set { _profileApproversList = value; }
        }

        public List<HttpPostedFile> ProfileExpenseUploadedFilesList
        {
            get { return _profileExpenseUploadedFilesList; }
            set { _profileExpenseUploadedFilesList = value; }
        }

        public int EmpId
        {
            get { return _empId; }
            set { _empId = value; }
        }

        public string EmpEmail
        {
            get { return _empEmail; }
            set { _empEmail = value; }
        }

        public string EmpName
        {
            get { return _empName; }
            set { _empName = value; }
        }

        public string ExpCategoryText
        {
            get { return _expCategoryText; }
            set { _expCategoryText = value; }

        }

        public string ExpTypeText
        {
            get { return _expTypeText; }
            set { _expTypeText = value; }
        }
        public string AgencyName
        {
            get { return _agencyName; }
            set { _agencyName = value; }
        }
        public string ReimAmount
        {
            get { return _reimAmount; }
            set { _reimAmount = value; }
        }

        public string ReimCurrency
        {
            get { return _reimCurrency; }
            set { _reimCurrency = value; }
        }

        public string ClaimAmount
        {
            get { return _claimAmount; }
            set { _claimAmount = value; }
        }

        public string BalanceAmount
        {
            get { return _balanceAmount; }
            set { _balanceAmount = value; }
        }


        protected const string filePath = "Upload/Corporate";

        #endregion

        public CorporateProfileExpenseDetails()
        {

        }


        public CorporateProfileExpenseDetails(int expDetailId)
        {
            Load(expDetailId);
        }

        #region Public methods
        public void Load(int expDetailId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ExpDetailId", expDetailId);
                DataSet ds = new DataSet();
                ds = DBGateway.ExecuteQuery("usp_Get_Corp_Expense_Details_By_ExpenseId", paramList);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        CorporateProfileExpenseDetails detail = new CorporateProfileExpenseDetails();
                        foreach (DataRow data in ds.Tables[0].Rows)
                        {
                            if (data["Date"] != DBNull.Value)
                            {
                                _date = Convert.ToDateTime(data["Date"]);
                            }
                            if (data["DocNo"] != DBNull.Value)
                            {
                                _docNo = Convert.ToString(data["DocNo"]);
                            }
                            if (data["EXP_CATEGORY"] != DBNull.Value)
                            {
                                _expCategoryText = Convert.ToString(data["EXP_CATEGORY"]);
                            }
                            if (data["EXP_TYPE"] != DBNull.Value)
                            {
                                _expTypeText = Convert.ToString(data["EXP_TYPE"]);
                            }
                            if (data["Amount"] != DBNull.Value)
                            {
                                _amount = Convert.ToDecimal(data["Amount"]);
                            }
                            if (data["EMPID"] != DBNull.Value)
                            {
                                _empId = Convert.ToInt32(data["EMPID"]);
                            }
                            if (data["EMPNAME"] != DBNull.Value)
                            {
                                _empName = Convert.ToString(data["EMPNAME"]);
                            }
                            if (data["EMPEMAIL"] != DBNull.Value)
                            {
                                _empEmail = Convert.ToString(data["EMPEMAIL"]);
                            }
                            if (data["REIMAMOUNT"] != DBNull.Value)
                            {
                                _reimAmount = Convert.ToString(data["REIMAMOUNT"]);
                            }
                            if (data["REIMCURRENCY"] != DBNull.Value)
                            {
                                _reimCurrency = Convert.ToString(data["REIMCURRENCY"]);
                            }
                            if (data["AgencyName"] != DBNull.Value)
                            {
                                _agencyName = Convert.ToString(data["AgencyName"]);
                            }
                            if (data["AgencyName"] != DBNull.Value)
                            {
                                _agencyName = Convert.ToString(data["AgencyName"]);
                            }
                            if (data["APPHIERARCHY"] != DBNull.Value)
                            {
                                _approvalHierarachy = Convert.ToString(data["APPHIERARCHY"]);
                            }
                            if (data["CLAIMAMOUNT"] != DBNull.Value)
                            {
                                _claimAmount = Convert.ToString(data["CLAIMAMOUNT"]);
                            }
                            if (data["BALANCEAMOUNT"] != DBNull.Value)
                            {
                                _balanceAmount = Convert.ToString(data["BALANCEAMOUNT"]);
                            }
                            if (data["CURRENCY"] != DBNull.Value)
                            {
                                _currency = Convert.ToString(data["CURRENCY"]);
                            }

                        }
                    }

                    if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow data in ds.Tables[1].Rows)
                        {
                            CorpProfileApproval approver = new CorpProfileApproval();
                            if (data["ApproverId"] != DBNull.Value)
                            {
                                approver.ApproverId = Convert.ToInt32(data["ApproverId"]);
                            }
                            if (data["ApproverHierarchy"] != DBNull.Value)
                            {
                                approver.Hierarchy = Convert.ToInt32(data["ApproverHierarchy"]);
                            }
                            if (data["ApprovalStatus"] != DBNull.Value)
                            {
                                approver.ApprovalStatus = Convert.ToString(data["ApprovalStatus"]);
                            }
                            if (data["APPNAME"] != DBNull.Value)
                            {
                                approver.ApproverName = Convert.ToString(data["APPNAME"]);
                            }
                            if (data["APPEMAIL"] != DBNull.Value)
                            {
                                approver.ApproverEmail = Convert.ToString(data["APPEMAIL"]);
                            }
                            _profileApproversList.Add(approver);

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string Save(List<CorporateProfileExpenseDetails> _expDetailsList)
        {
            string docNo = string.Empty;
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                if (_expDetailsList != null && _expDetailsList.Count > 0)
                {
                    for (int i = 0; i < _expDetailsList.Count; i++)
                    {
                        if (i == 0) //For first record while insertion we will get the doc no.
                        {
                            docNo = _expDetailsList[i].Save(_expDetailsList[i], cmd);

                        }
                        else//The same doc number is updated for the next subsequent expense details.
                        {
                            _expDetailsList[i].DocNo = docNo;
                            _expDetailsList[i].Save(_expDetailsList[i], cmd);
                        }
                    }
                    trans.Commit();
                }
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return docNo;
        }

        public string Save(CorporateProfileExpenseDetails objExpDetail, SqlCommand cmd)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[30];
                paramList[0] = new SqlParameter("@P_expDetailId", objExpDetail.ExpDetailId);
                paramList[1] = new SqlParameter("@P_date", objExpDetail.Date);
                paramList[2] = new SqlParameter("@P_agentId", objExpDetail.AgentId);
                paramList[3] = new SqlParameter("@P_profileId", objExpDetail.ProfileId);
                paramList[4] = new SqlParameter("@P_countryCode", objExpDetail.CountryCode);
                paramList[5] = new SqlParameter("@P_cityCode", objExpDetail.CityCode);
                paramList[6] = new SqlParameter("@P_costCenterId", objExpDetail.CostCenterId);
                paramList[7] = new SqlParameter("@P_exptypeid", objExpDetail.Exptypeid);
                paramList[8] = new SqlParameter("@P_referenceCode", objExpDetail.ReferenceCode);
                paramList[9] = new SqlParameter("@P_description", objExpDetail.Description);
                paramList[10] = new SqlParameter("@P_currency", objExpDetail.Currency);
                paramList[11] = new SqlParameter("@P_amount", objExpDetail.Amount);
                paramList[12] = new SqlParameter("@P_comment", objExpDetail.Comment);
                if (!string.IsNullOrEmpty(objExpDetail.DocNo))
                {
                    paramList[13] = new SqlParameter("@P_docNo", objExpDetail.DocNo);
                }
                else
                {
                    paramList[13] = new SqlParameter("@P_docNo", DBNull.Value);
                }
                paramList[14] = new SqlParameter("@P_type", objExpDetail.Type);
                paramList[15] = new SqlParameter("@P_refId", objExpDetail.RefId);
                paramList[16] = new SqlParameter("@P_approvalHierarachy", objExpDetail.ApprovalHierarachy);
                paramList[17] = new SqlParameter("@P_approvalStatus", objExpDetail.ApprovalStatus);
                paramList[18] = new SqlParameter("@P_status", objExpDetail.Status);
                paramList[19] = new SqlParameter("@P_expmode", objExpDetail.Expmode);
                if (_expDetailId == -1)
                {
                    paramList[20] = new SqlParameter("@P_createdBy", objExpDetail.CreatedBy);
                }
                else
                {
                    paramList[20] = new SqlParameter("@P_createdBy", objExpDetail.ModifiedBy);
                }
                paramList[21] = new SqlParameter("@P_Exp_Detail_Id_Ret", SqlDbType.BigInt);
                paramList[21].Direction = ParameterDirection.Output;

                paramList[22] = new SqlParameter("@P_Corp_Profile_Id_Ret", SqlDbType.BigInt);
                paramList[22].Direction = ParameterDirection.Output;

                paramList[23] = new SqlParameter("@P_Corp_Profile_Doc_No", SqlDbType.NVarChar, 20);
                paramList[23].Direction = ParameterDirection.Output;

                paramList[24] = new SqlParameter("@P_Corp_Profile_Emp_Id", SqlDbType.BigInt);
                paramList[24].Direction = ParameterDirection.Output;

                paramList[25] = new SqlParameter("@P_Corp_Profile_Emp_Email", SqlDbType.NVarChar, 30);
                paramList[25].Direction = ParameterDirection.Output;

                paramList[26] = new SqlParameter("@P_Corp_Profile_Emp_Name", SqlDbType.NVarChar, 30);
                paramList[26].Direction = ParameterDirection.Output;


                paramList[27] = new SqlParameter("@P_ExpCategory_Text", SqlDbType.NVarChar, 30);
                paramList[27].Direction = ParameterDirection.Output;

                paramList[28] = new SqlParameter("@P_ExpType_Text", SqlDbType.NVarChar, 30);
                paramList[28].Direction = ParameterDirection.Output;

                paramList[29] = new SqlParameter("@P_AgencyName", SqlDbType.NVarChar, 30);
                paramList[29].Direction = ParameterDirection.Output;

                int count = DBGateway.ExecuteNonQueryDetails(cmd, "CORP_PROFILE_EXPENSE_DETAIL_ADD_UPDATE", paramList);
                if (count > 0 && paramList[21].Value != DBNull.Value)
                {
                    objExpDetail.ExpDetailId = Convert.ToInt32(paramList[21].Value);
                    objExpDetail.DocNo = Convert.ToString(paramList[23].Value);

                    objExpDetail.EmpId = Convert.ToInt32(paramList[24].Value);
                    objExpDetail.EmpEmail = Convert.ToString(paramList[25].Value);
                    objExpDetail.EmpName = Convert.ToString(paramList[26].Value);

                    objExpDetail.ExpCategoryText = Convert.ToString(paramList[27].Value);
                    objExpDetail.ExpTypeText = Convert.ToString(paramList[28].Value);
                    objExpDetail.AgencyName = Convert.ToString(paramList[29].Value);


                    if (objExpDetail.ProfileExpenseUploadedFilesList != null && objExpDetail.ProfileExpenseUploadedFilesList.Count > 0)
                    {

                        #region Documents Upload Section
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + filePath) + @"\";
                        string clientFolder = dirFullPath + objExpDetail.ExpDetailId + @"\";

                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        int numFiles = 0;
                        foreach (HttpPostedFile file in objExpDetail.ProfileExpenseUploadedFilesList)
                        {
                            string str_image = string.Empty;
                            string pathToSave = string.Empty;
                            numFiles = numFiles + 1;
                            string fileName = file.FileName;
                            string fileExtension = file.ContentType;
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                fileExtension = Path.GetExtension(fileName);
                                str_image = numFiles.ToString() + "_" + "Doc_" + fileName;
                                pathToSave = clientFolder + str_image;
                                file.SaveAs(pathToSave); //Files saving new folder
                            }

                            CorporateProfileExpenseDocDetails docDetails = new CorporateProfileExpenseDocDetails();
                            docDetails.DocId = -1;
                            docDetails.ExpDetailId = objExpDetail.ExpDetailId;
                            docDetails.FilePath = pathToSave;
                            docDetails.FileType = fileExtension;
                            docDetails.CreatedBy = objExpDetail.CreatedBy;
                            docDetails.DocDescription = string.Empty;
                            docDetails.Status = "A";
                            docDetails.SaveExpenseDocDetails(cmd);
                        }

                        #endregion
                    }


                    objExpDetail.ProfileApproversList = Load(Convert.ToInt32(paramList[22].Value), objExpDetail.Expmode);

                    //get the particular profile approvers list for particular type.

                    if (objExpDetail.ProfileApproversList != null && objExpDetail.ProfileApproversList.Count > 0)
                    {
                        foreach (CorpProfileApproval approver in objExpDetail.ProfileApproversList)
                        {
                            Corp_Profile_Transc_Approval transcApproval = new Corp_Profile_Transc_Approval();
                            transcApproval.ApprovalStatus = "P";//Initial Approval Status 'Pending'
                            transcApproval.ApproverHierarchy = approver.Hierarchy;
                            transcApproval.ApproverId = approver.ApproverId;
                            transcApproval.CreatedBy = objExpDetail.CreatedBy;
                            transcApproval.ExpDetailId = Convert.ToInt32(paramList[21].Value);
                            transcApproval.Status = "A";
                            transcApproval.Id = -1;
                            if (objExpDetail.Expmode == "T" || objExpDetail.Expmode == "N")
                            {
                                //Travel mode or non travel mode 
                                //Transaction type is expense(E) 
                                //else tranasction type is Trip(T)
                                transcApproval.TranxType = "E"; //Expense Category
                            }
                            else
                            {
                                transcApproval.TranxType = "T"; //Trip Category
                            }

                            int rowId = transcApproval.SaveCorpProfileTransApproval(cmd);
                            approver.TranxApprovalId = rowId;

                        }
                    }


                }

            }
            catch
            {

                throw;
            }

            return _docNo;
        }

        /// <summary>
        /// Gets all the Approvers List from the table corp_profile_approval for the supplied profile Id.
        /// </summary>
        /// <param name="policyId"></param>
        /// <returns></returns>
        public List<CorpProfileApproval> Load(int profileId, string type)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<CorpProfileApproval> approversList = new List<CorpProfileApproval>();
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                paramList[1] = new SqlParameter("@P_Type", type);
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetProfileApproversListByType", paramList, connection);
                using (DataTable dtProfile = DBGateway.FillDataTableSP("usp_GetProfileApproversListByType", paramList))
                {
                    if (dtProfile !=null && dtProfile.Rows.Count > 0)
                    {
                        foreach(DataRow data in dtProfile.Rows)
                        {
                            CorpProfileApproval approverDetails = new CorpProfileApproval();
                            if (data["Id"] != DBNull.Value)
                            {
                                approverDetails.Id = Convert.ToInt32(data["Id"]);
                            }
                            if (data["ProfileId"] != DBNull.Value)
                            {
                                approverDetails.ProfileId = Convert.ToInt32(data["ProfileId"]);
                            }
                            if (data["ApproverId"] != DBNull.Value)
                            {
                                approverDetails.ApproverId = Convert.ToInt32(data["ApproverId"]);
                            }
                            if (data["Hierarchy"] != DBNull.Value)
                            {
                                approverDetails.Hierarchy = Convert.ToInt32(data["Hierarchy"]);
                            }
                            if (data["Type"] != DBNull.Value)
                            {
                                approverDetails.Type = Convert.ToString(data["Type"]);
                            }
                            if (data["Status"] != DBNull.Value)
                            {
                                approverDetails.Status = Convert.ToString(data["Status"]);
                            }
                            if (data["CreatedBy"] != DBNull.Value)
                            {
                                approverDetails.CreatedBy = Convert.ToInt32(data["CreatedBy"]);
                            }
                            if (data["APPROVERNAME"] != DBNull.Value)
                            {
                                approverDetails.ApproverName = Convert.ToString(data["APPROVERNAME"]);
                            }
                            if (data["APPROVEREMAIL"] != DBNull.Value)
                            {
                                approverDetails.ApproverEmail = Convert.ToString(data["APPROVEREMAIL"]);
                            }
                            if (data["JourneyType"] != DBNull.Value)
                            {
                                approverDetails.JourneyType = Convert.ToString(data["JourneyType"]);
                            }
                            if (data["Amount"] != DBNull.Value)
                            {
                                approverDetails.Amount = Convert.ToDecimal(data["Amount"]);
                            }
                            if (data["ApprovalType"] != DBNull.Value)
                            {
                                approverDetails.ApprovalType = Convert.ToString(data["ApprovalType"]);
                            }
                            approversList.Add(approverDetails);
                        }
                    }
                }
                //data.Close();
                //connection.Close();
                return approversList;
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

        public static DataTable GetExpenseBookings(int profileId, DateTime fromDate, DateTime toDate)
        {
            DataTable dtBookings = null;
            SqlParameter[] paramList = new SqlParameter[3];

            paramList[0] = new SqlParameter("@P_ProfileId", profileId);
            paramList[1] = new SqlParameter("@P_FromDate", fromDate);
            paramList[2] = new SqlParameter("@P_ToDate", toDate);
            return dtBookings = DBGateway.FillDataTableSP("corp_profile_get_expense_summary_bookings", paramList);
        }

        public static decimal GetAmountSpend(long userId)
        {
            decimal balance = 0;

            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_UserId", userId);
                paramList[1] = new SqlParameter("@P_Corp_Profile_AmountSpend_Ret", SqlDbType.BigInt);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQueryDetails(cmd, "usp_GetCorp_Profile_Amount_Spend", paramList);
                if (paramList[1].Value != DBNull.Value)
                {
                    balance = Convert.ToDecimal(paramList[1].Value);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return balance;
        }


        #endregion


        public static DataTable GetCorpExpenseApprovalsList(int profileId, DateTime fromDate, DateTime toDate)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                if (profileId > 0) paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                paramList[1] = new SqlParameter("@P_FromDate", fromDate);
                paramList[2] = new SqlParameter("@P_ToDate", toDate);
                return DBGateway.FillDataTableSP("usp_Get_Corp_Expense_Approvals_List", paramList);
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetCorpReimbursementList(int profileId, DateTime fromDate, DateTime toDate, string expCat, string expType)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if (profileId > 0)
                {
                    paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                }
                else
                {
                    paramList[0] = new SqlParameter("@P_ProfileId", DBNull.Value);
                }
                paramList[1] = new SqlParameter("@P_FromDate", fromDate);
                paramList[2] = new SqlParameter("@P_ToDate", toDate);
                if (string.IsNullOrEmpty(expCat))
                {
                    paramList[3] = new SqlParameter("@P_ExpCat", DBNull.Value);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_ExpCat", expCat);
                }
                if (string.IsNullOrEmpty(expType))
                {
                    paramList[4] = new SqlParameter("@P_ExpType", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_ExpType", expType);
                }
                return DBGateway.FillDataTableSP("usp_Get_Corp_Reimbursement_List", paramList);
            }
            catch
            {
                throw;
            }
        }


        public static DataTable GetExpenseDetailsQueueList(int profileId, DateTime fromDate, DateTime toDate, string expCat, string expType, string approvalStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                if (profileId > 0)
                {
                    paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                }
                else
                {
                    paramList[0] = new SqlParameter("@P_ProfileId", DBNull.Value);
                }
                paramList[1] = new SqlParameter("@P_FromDate", fromDate);
                paramList[2] = new SqlParameter("@P_ToDate", toDate);
                if (string.IsNullOrEmpty(expCat))
                {
                    paramList[3] = new SqlParameter("@P_ExpCat", DBNull.Value);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_ExpCat", expCat);
                }
                if (string.IsNullOrEmpty(expType))
                {
                    paramList[4] = new SqlParameter("@P_ExpType", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_ExpType", expType);
                }
                paramList[5] = new SqlParameter("@P_ApprovalStatus", approvalStatus);

                return DBGateway.FillDataTableSP("usp_Get_Corp_Expense_Details_Queue_List", paramList);
            }
            catch
            {
                throw;
            }
        }



        public static DataTable GetExpenseDetails(int expDetailId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ExpDetailId", expDetailId);

                return DBGateway.FillDataTableSP("usp_GetCorpExpenseDetails", paramList);
            }
            catch
            {
                throw;
            }
        }
        public static void UpdateRefundStatus(int expDetailId, string approvalStatus, int modifiedBy)
        {
            try
            {

                SqlParameter[] paramArr = new SqlParameter[3];
                paramArr[0] = new SqlParameter("@P_ExpDetail_ID", expDetailId);
                paramArr[1] = new SqlParameter("@P_APPROVAL_STATUS", approvalStatus);
                paramArr[2] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                DBGateway.ExecuteNonQuery("usp_UpdateProfileTranscApprovalStatus", paramArr);
            }
            catch { throw; }
        }
        public static void UpdateRefundStatusByMail(int expDetailId, int approverId, string approvalStatus, string remarks)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[4];
                paramArr[0] = new SqlParameter("@P_ExpDetail_ID", expDetailId);
                paramArr[1] = new SqlParameter("@P_APPROVAL_STATUS", approvalStatus);
                paramArr[2] = new SqlParameter("@P_Approver_Id", approverId);
                if (string.IsNullOrEmpty(remarks))
                {
                    paramArr[3] = new SqlParameter("@P_Remarks", DBNull.Value);
                }
                else
                {
                    paramArr[3] = new SqlParameter("@P_Remarks", remarks);
                }

                DBGateway.ExecuteNonQuery("usp_UpdateProfileTranscApprovalStatusByMail", paramArr);
            }
            catch { throw; }
        }


        public static int GetAmountForExpType(int userId, string type, string countryCode, string cityCode, string expTypeId)
        {
            int balance = 0;

            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_USERID", userId);
                paramList[1] = new SqlParameter("@P_TYPE", type);
                paramList[2] = new SqlParameter("@P_COUNTRYCODE", countryCode);
                paramList[3] = new SqlParameter("@P_CITYCODE", cityCode);
                paramList[4] = new SqlParameter("@P_EXPTYPEID", expTypeId);
                paramList[5] = new SqlParameter("@P_AMOUNT", SqlDbType.Int);
                paramList[5].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQueryDetails(cmd, "CORP_PROFILE_POLICY_GET_EXPENSES_LIST", paramList);
                if (paramList[5].Value != DBNull.Value)
                {
                    balance = Convert.ToInt32(paramList[5].Value);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return balance;


        }

        public static bool GetTravelDaysInclusion(int userId, string type, string countryCode, string cityCode, string expTypeId)
        {
            bool includeTravelDays = false;

            SqlCommand cmd = null;
            try
            {


                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_USERID", userId);
                paramList[1] = new SqlParameter("@P_TYPE", type);
                paramList[2] = new SqlParameter("@P_COUNTRYCODE", countryCode);
                paramList[3] = new SqlParameter("@P_CITYCODE", cityCode);
                paramList[4] = new SqlParameter("@P_EXPTYPEID", expTypeId);
                paramList[5] = new SqlParameter("@P_INC_TRAVEL_DAYS", SqlDbType.Bit);
                paramList[5].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQueryDetails(cmd, "GET_CORP_PROFILE_POLICY_EXPENSE_TRAVEL_DAYS", paramList);
                if (paramList[5].Value != DBNull.Value)
                {
                    includeTravelDays = Convert.ToBoolean(paramList[5].Value);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return includeTravelDays;
        }

        public static DataTable GetExpenseDocDetails(int expDetailId)
        {
            DataTable dtDocs = null;
            SqlParameter[] paramList = new SqlParameter[1];

            paramList[0] = new SqlParameter("@P_ExpDetailId", expDetailId);
            return dtDocs = DBGateway.FillDataTableSP("corp_profile_get_expense_doc_details", paramList);
        }

        public static DataTable GetPolicyExpenseCountryList()
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", 'A');
                return DBGateway.ExecuteQuery("corp_profile_get_expense_country_list", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetPolicyExpenseCityList(string countryCode)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_COUNTRY_CODE", countryCode);
                return DBGateway.ExecuteQuery("corp_profile_get_expense_city_list", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetTripDetailsQueueList(int profileId, DateTime fromDate, DateTime toDate, string approvalStatus,int agentId, int productId)
        {
           try
            {
                SqlParameter[] paramList = new SqlParameter[7];
                if (profileId > 0)
                {
                    paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                }
                else
                {
                    paramList[0] = new SqlParameter("@P_ProfileId", DBNull.Value);
                }
                paramList[1] = new SqlParameter("@P_FromDate", fromDate);
                paramList[2] = new SqlParameter("@P_ToDate", toDate);
                paramList[3] = new SqlParameter("@P_ApprovalStatus", approvalStatus);
                paramList[4] = new SqlParameter("@P_AgentId", agentId);
                paramList[5] = new SqlParameter("@P_ProductId", productId); //Added for CorporateTipDetails queue based on product id

                //paramList[4] = string.IsNullOrEmpty(bookingType) ? new SqlParameter("@P_BookingType", DBNull.Value) : new SqlParameter("@P_BookingType", bookingType);
                return DBGateway.FillDataTableSP("usp_Get_Corp_Trip_Details_Queue_List", paramList);
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetCorpTicketApprovalsList(int profileId, DateTime fromDate, DateTime toDate,int empId, int productId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if (profileId > 0) paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                paramList[1] = new SqlParameter("@P_FromDate", fromDate);
                paramList[2] = new SqlParameter("@P_ToDate", toDate);
                if (empId > 0)
                {
                    paramList[3] = new SqlParameter("@P_SelProfileId", empId);
                }
                paramList[4] = new SqlParameter("@P_ProductId", productId); //Added for CorporateTipDetails queue based on product id
                List<SqlParameter> parameters = new List<SqlParameter>(paramList);
                //if (string.IsNullOrEmpty(bookingType))
                //    parameters.Add(new SqlParameter("@P_BookingType", DBNull.Value));
                //else
                //    parameters.Add(new SqlParameter("@P_BookingType", bookingType));

                return DBGateway.FillDataTableSP("usp_Get_Corp_Trip_Approvals_List", parameters.ToArray());
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetCorporateBookings(string tripId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_TripId", tripId);                

                return DBGateway.FillDataTableSP("usp_Get_Corp_Bookings_By_Trip_Id", paramList);
            }
            catch
            {
                throw;
            }
        }

        

    }
}

