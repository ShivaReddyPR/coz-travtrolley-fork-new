﻿using System;
using System.Data;
using System.Web;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;


/// <summary>
/// Summary description for Expenses Reimbursement details
/// </summary>
///

namespace CT.Corporate
{
    public class ExpReimbursement
    {
        /// <summary>
        ///  unique table Id
        /// </summary>
        public int ERM_ID { get; set; }
        /// <summary>
        ///  Reportdata table Id
        /// </summary>
        public int ERM_ED_ID { get; set; }
        /// <summary>
        ///  Claim Amount
        /// </summary>
        public decimal ERM_ClaimAmount { get; set; }
        /// <summary>
        ///  Currency
        /// </summary>
        public string ERM_Currency { get; set; }
        /// <summary>
        ///  Reimbursement Amount
        /// </summary>
        public decimal ERM_ReimbursementAmount { get; set; }
        /// <summary>
        ///  Total Reimburse Amount
        /// </summary>
        public decimal ERM_TotalReimbursementAmount { get; set; }
        /// <summary>
        ///  Reimbursement Status
        /// </summary>
        public string ERM_ReimStatus { get; set; }
        /// <summary>
        ///  Balance Amount
        /// </summary>
        public decimal ERM_Balance { get; set; }
        /// <summary>
        ///  Active/Inactive
        /// </summary>
        public bool ERM_Status { get; set; }
        /// <summary>
        ///  Created by
        /// </summary>
        public int ERM_CreatedBy { get; set; }
        /// <summary>
        ///  Modified By
        /// </summary>
        public int ERM_ModifiedBy { get; set; }
        /// <summary>
        ///  Mode to mention save action
        /// </summary>
        public string Mode { get; set; }
 /// <summary>
        ///  ERM ReimRemarks
        /// </summary>
        public string ERM_ReimRemarks { get; set; }
        //To Save data in expenses reimbursement table
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                SqlParameter[] paramList = new SqlParameter[10];
                paramList[0] = new SqlParameter("@ed_Id", ERM_ED_ID);
                paramList[1] = new SqlParameter("@erm_ClaimAmount", ERM_ClaimAmount);
                paramList[2] = new SqlParameter("@erm_Currency", ERM_Currency);
                paramList[3] = new SqlParameter("@erm_ReimbursementAmount", ERM_ReimbursementAmount);
                paramList[4] = new SqlParameter("@erm_Status", ERM_Status);
                paramList[5] = new SqlParameter("@erm_CreatedBy", ERM_CreatedBy);
                paramList[6] = new SqlParameter("@erm_ModifiedBy", ERM_ModifiedBy);
                paramList[7] = new SqlParameter("@Mode", Mode);
                paramList[8] = new SqlParameter("@erm_ReimRemarks", ERM_ReimRemarks);
                paramList[9] = new SqlParameter("@erm_ReimStatus", ERM_ReimStatus);
                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_Add_Update_Exp_Reimbursement", paramList);
                trans.Commit();
            }
            catch(Exception ex)
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        //To get reimbursement queue data
        public static DataTable GetReimburseQueue(DateTime FromDate, DateTime ToDate, int ProfileId, int EC_ID, int ET_ID)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@p_FromDate", FromDate);
                paramList[1] = new SqlParameter("@p_ToDate", ToDate);
                if (ProfileId > 0)
                {
                    paramList[2] = new SqlParameter("@p_ProfileId", ProfileId);
                }
                else
                {
                    paramList[2] = new SqlParameter("@p_ProfileId", DBNull.Value);
                }
                if (ET_ID > 0)
                {
                    paramList[3] = new SqlParameter("@p_ExpType", ET_ID);
                }
                else
                {
                    paramList[3] = new SqlParameter("@p_ExpType", DBNull.Value);
                }
                if (EC_ID > 0)
                {
                    paramList[4] = new SqlParameter("@p_ExpCat", EC_ID);
                }
                else
                {
                    paramList[4] = new SqlParameter("@p_ExpCat", DBNull.Value);
                }
                return DBGateway.FillDataTableSP("usp_Get_EXP_Reimbursement_List", paramList);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To get reimburse count
        /// </summary>
        /// <param name="Ed_ID"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static DataTable GetReimbursedCount(int Ed_ID, int UserId)
        {
            try
            {
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@ED_ID", Ed_ID));
                liParams.Add(new SqlParameter("@USER", UserId));
                return DBGateway.FillDataTableSP("usp_GetExpReimCount", liParams.ToArray());
            }
            catch
            {
                return null;
            }
        }
    }

    public class ExpReport
    {
        /// <summary>
        ///  unique table Id
        /// </summary>
        public int ER_ID { get; set; }
        /// <summary>
        ///  Ref No
        /// </summary>
        public string ER_RefNo { get; set; }
        /// <summary>
        ///  Report Date
        /// </summary>
        public DateTime ER_Date { get; set; }
        /// <summary>
        ///  Title
        /// </summary>
        public string ER_Title { get; set; }
        /// <summary>
        ///  Purpose
        /// </summary>
        public string ER_Purpose { get; set; }
        /// <summary>
        ///  Comp Visited
        /// </summary>
        public string ER_CompVisited { get; set; }
        /// <summary>
        ///  Comments
        /// </summary>
        public string ER_Comments { get; set; }
        /// <summary>
        ///  Approved by
        /// </summary>
        public string ER_Aprroved { get; set; }
        /// <summary>
        ///  Active/Inactive
        /// </summary>
        public bool ER_Status { get; set; }
        /// <summary>
        ///  Created By
        /// </summary>
        public int ER_CreatedBy { get; set; }
        /// <summary>
        ///  MOdified By
        /// </summary>
        public int ER_ModifiedBy { get; set; }
    }

    public class ExpReportData
    {
        public DateTime Date { get; set; }

        /// <summary>
        ///  unique table Id
        /// </summary>
        public int ED_Id { get; set; }
        /// <summary>
        ///  Transaction Ddate
        /// </summary>
        public DateTime ED_TransDate { get; set; }
        /// <summary>
        ///  Pay Type
        /// </summary>
        public int ED_PayType { get; set; }
        /// <summary>
        ///  Card Master id
        /// </summary>
        public int ED_CM_Id { get; set; }
        /// <summary>
        ///  Receipt Status
        /// </summary>
        public string ED_ReceiptStatus { get; set; }
        /// <summary>
        ///  City
        /// </summary>
        public string ED_City { get; set; }
        /// <summary>
        ///  UOM
        /// </summary>
        public string ED_UOM { get; set; }
        /// <summary>
        ///  Units
        /// </summary>
        public float ED_Units { get; set; }
        /// <summary>
        ///  Price per Unit
        /// </summary>
        public decimal ED_UnitPrice { get; set; }
        /// <summary>
        ///  Currency
        /// </summary>
        public string ED_Currency { get; set; }
        /// <summary>
        ///  Exc Rate
        /// </summary>
        public decimal ED_ExcRate { get; set; }
        /// <summary>
        ///  Exc Amount
        /// </summary>
        public decimal ED_ExcAmount { get; set; }
        /// <summary>
        ///  Tax
        /// </summary>
        public decimal ED_Tax { get; set; }
        /// <summary>
        ///  Total Amount
        /// </summary>
        public decimal ED_TotalAmount { get; set; }
        /// <summary>
        ///  Billing
        /// </summary>
        public bool ED_IsBillable { get; set; }
        /// <summary>
        ///  Claim Expenses
        /// </summary>
        public bool ED_ClaimExpense { get; set; }
        /// <summary>
        ///  Comment
        /// </summary>
        public string ED_Comment { get; set; }
        /// <summary>
        ///  Reimburse Status
        /// </summary>
        public string ED_REIMBURSESTATUS { get; set; }
        /// <summary>
        ///  Approval Status
        /// </summary>
        public string ED_APPROVALSTATUS { get; set; }
        /// <summary>
        ///  Acitve/InActive
        /// </summary>
        public bool ED_Status { get; set; }
        /// <summary>
        ///  Created By
        /// </summary>
        public int ED_CreatedBy { get; set; }
        /// <summary>
        ///  Modified by
        /// </summary>
        public int ED_ModifiedBy { get; set; }

        /// <summary>
        ///  Employee Id
        /// </summary>
        public string Emp_Id { get; set; }
        /// <summary>
        ///  Employee Name
        /// </summary>
        public string EMP_Name { get; set; }
        /// <summary>
        ///  Employee Email
        /// </summary>
        public string EMP_Email { get; set; }

        /// <summary>
        ///  Expenses Category text
        /// </summary>
        public string Exp_CategoryText { get; set; }
        /// <summary>
        /// Expenses Type text
        /// </summary>
        public string Exp_TypeText { get; set; }
        /// <summary>
        ///  Agency name
        /// </summary>
        public string AgencyName { get; set; }
        /// <summary>
        ///  Reimburse Amount
        /// </summary>
        public string ReimAmount { get; set; }
        /// <summary>
        ///  Reim Currency
        /// </summary>
        public string ReimCurrency { get; set; }
        /// <summary>
        ///  CLaim Amount
        /// </summary>
        public string ClaimAmount { get; set; }
        /// <summary>
        ///  Balance Amount
        /// </summary>
        public string BalanceAmount { get; set; }
        /// <summary>
        ///  Doc No.
        /// </summary>
        public string DocNo { get; set; }


        List<CorpProfileApproval> _profileApproversList = new List<CorpProfileApproval>();

        public List<CorpProfileApproval> ProfileApproversList
        {
            get { return _profileApproversList; }
            set { _profileApproversList = value; }
        }

        public List<ExpReport> ExpReportList { get; set; }

        public ExpReportData()
        {

        }
		
		//To get data for send email
        public ExpReportData(int ED_Id)
        {
            Load(ED_Id);
        }
		
		//To load data for send email
        public void Load(int ED_Id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@eD_Id", ED_Id);
                DataSet ds = new DataSet();
                ds = DBGateway.ExecuteQuery("usp_Get_ReportData_By_ED_Id", paramList);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        ExpReportData detail = new ExpReportData();
                        foreach (DataRow data in ds.Tables[0].Rows)
                        {
                            if (data["Date"] != DBNull.Value)
                            {
                                Date = Convert.ToDateTime(data["Date"]);
                            }
                            if (data["DocNo"] != DBNull.Value)
                            {
                                DocNo = Convert.ToString(data["DocNo"]);
                            }
                            if (data["EXP_CATEGORY"] != DBNull.Value)
                            {
                                Exp_CategoryText = Convert.ToString(data["EXP_CATEGORY"]);
                            }
                            if (data["EXP_TYPE"] != DBNull.Value)
                            {
                                Exp_TypeText = Convert.ToString(data["EXP_TYPE"]);
                            }
                            if (data["ED_TotalAmount"] != DBNull.Value)
                            {
                                ED_TotalAmount = Convert.ToDecimal(data["ED_TotalAmount"]);
                            }
                            if (data["EMPID"] != DBNull.Value)
                            {
                                Emp_Id = Convert.ToString(data["EMPID"]);
                            }
                            if (data["EMPNAME"] != DBNull.Value)
                            {
                                EMP_Name = Convert.ToString(data["EMPNAME"]);
                            }
                            if (data["EMPEMAIL"] != DBNull.Value)
                            {
                                EMP_Email = Convert.ToString(data["EMPEMAIL"]);
                            }
                            if (data["REIMAMOUNT"] != DBNull.Value)
                            {
                                ReimAmount = Convert.ToString(data["REIMAMOUNT"]);
                            }
                            if (data["REIMCURRENCY"] != DBNull.Value)
                            {
                                ReimCurrency = Convert.ToString(data["REIMCURRENCY"]);
                            }
                            if (data["AgencyName"] != DBNull.Value)
                            {
                                AgencyName = Convert.ToString(data["AgencyName"]);
                            }
                            //if (data["APPHIERARCHY"] != DBNull.Value)
                            //{
                            //    _approvalHierarachy = Convert.ToString(data["APPHIERARCHY"]);
                            //}
                            if (data["CLAIMAMOUNT"] != DBNull.Value)
                            {
                                ClaimAmount = Convert.ToString(data["CLAIMAMOUNT"]);
                            }
                            if (data["BALANCEAMOUNT"] != DBNull.Value)
                            {
                                BalanceAmount = Convert.ToString(data["BALANCEAMOUNT"]);
                            }
                            if (data["ED_Currency"] != DBNull.Value)
                            {
                                ED_Currency = Convert.ToString(data["ED_Currency"]);
                            }

                        }
                    }

                    if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow data in ds.Tables[1].Rows)
                        {
                            CorpProfileApproval approver = new CorpProfileApproval();
                            if (data["ApproverId"] != DBNull.Value)
                            {
                                approver.ApproverId = Convert.ToInt32(data["ApproverId"]);
                            }
                            if (data["ApproverHierarchy"] != DBNull.Value)
                            {
                                approver.Hierarchy = Convert.ToInt32(data["ApproverHierarchy"]);
                            }
                            if (data["ApprovalStatus"] != DBNull.Value)
                            {
                                approver.ApprovalStatus = Convert.ToString(data["ApprovalStatus"]);
                            }
                            if (data["APPNAME"] != DBNull.Value)
                            {
                                approver.ApproverName = Convert.ToString(data["APPNAME"]);
                            }
                            if (data["APPEMAIL"] != DBNull.Value)
                            {
                                approver.ApproverEmail = Convert.ToString(data["APPEMAIL"]);
                            }
                            _profileApproversList.Add(approver);

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
		//To get Expenses details data
        public static DataTable GetReportData(int ED_Id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ed_Id", ED_Id);

                return DBGateway.FillDataTableSP("usp_GetExpenseReportData", paramList);
            }
            catch
            {
                throw;
            }
        }
		
		//To get document details data
        public static DataTable GetDocDetails(int ED_Id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@RC_ED_Id", ED_Id);

                return DBGateway.FillDataTableSP("usp_GetExpReceipts", paramList);
            }
            catch
            {
                throw;
            }
        }

    }
}
