﻿using System;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using CT.Core;
using System.Collections.Generic;

namespace CT.Corporate
{
    public class FlexMaster
    {
        #region variable
        int _agentId;
        int _productId;
        DataTable _dtFlexMaster;
        #endregion
        #region Properities
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public DataTable DtFlexMaster
        {
            get { return _dtFlexMaster; }
            set { _dtFlexMaster = value; }
        }

        public int ProductId { get => _productId; set => _productId = value; }
        #endregion

        public FlexMaster()
        {
        }
      public FlexMaster(int agentId,int ProductId)
        {
            _dtFlexMaster = GetData(agentId, ProductId);
        }
        #region Methods
        private DataTable GetData(long id, int ProductId)
        {
            DataTable dtResult = null;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_AGENT_ID", id);
                paramList[1] = new SqlParameter("@P_PRODUCT_ID", ProductId);
                dtResult = DBGateway.ExecuteQuery("CORP_PROFILE_FLEX_GETDATA", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Low, 1, ex.ToString() + "Failed to get Flex Data", "");
            }
            return dtResult;
        }
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                if (_dtFlexMaster != null)
                {
                    DataTable dt = _dtFlexMaster.GetChanges();

                    int recordStatus = 0;
                    if (dt != null && dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dt.Rows)
                        {
                            switch (dr.RowState)
                            {
                                case DataRowState.Added: recordStatus = 1;
                                    break;
                                case DataRowState.Modified: recordStatus = 2;
                                    break;
                                case DataRowState.Deleted: recordStatus = -1;
                                    dr.RejectChanges();
                                    break;
                                default: break;

                            }
                            if (Convert.ToInt32(dr["flexid"]) > 0)
                            {
                                int flexid = SaveFlexDeatils(dr, recordStatus);
                                if (flexid > 0)
                                {
                                    dr["flexid"] = flexid;
                                }                                                             
                                //SaveFlexDeatils(dt,cmd,_agentId, Convert.ToInt32(dr["flexOrder"]), Convert.ToString(dr["flexLabel"]), Convert.ToString(dr["flexControl"]), Convert.ToString(dr["flexSqlQuery"]), Convert.ToString(dr["flexDataType"]), Convert.ToString(dr["flexMandatoryStatus"]), Convert.ToInt32(dr["flexid"]), Convert.ToInt32(dr["flexCreatedBy"]), recordStatus, Convert.ToString(dr["flexGDSprefix"]), Convert.ToString(dr["flexplacehold"]), _productId, Convert.ToBoolean(dr["flexDisable"]), dr["flexValId"].Equals(DBNull.Value) ? 0 : Convert.ToInt32(dr["flexValId"]),flexTravelReason);
                            }
                        }
                    }
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
        }
        private int SaveFlexDeatils(DataRow dr,int recordStatus)
        {
            try
            {
                int Id = 0;
                List<SqlParameter> paramList = new List<SqlParameter>(); 
                paramList.Add(new SqlParameter("@Id", SqlDbType.Int));
                paramList[0].Direction = ParameterDirection.Output;
                paramList.Add(new SqlParameter("@P_flexAgentId", _agentId));
                paramList.Add(new SqlParameter("@P_flexOrder", string.IsNullOrEmpty(dr["flexOrder"].ToString()) ? 0 : Convert.ToInt32(dr["flexOrder"])));
                paramList.Add(new SqlParameter("@P_flexLabel", Convert.ToString(dr["flexLabel"])));
                paramList.Add(new SqlParameter("@P_flexControl", Convert.ToString(dr["flexControl"])));
                if (!string.IsNullOrEmpty(Convert.ToString(dr["flexSqlQuery"])))
                    paramList.Add(new SqlParameter("@P_flexSqlQuery", Convert.ToString(dr["flexSqlQuery"])));
                paramList.Add(new SqlParameter("@P_flexDataType", Convert.ToString(dr["flexDataType"])));
                paramList.Add(new SqlParameter("@P_flexMandatoryStatus", Convert.ToString(dr["flexMandatoryStatus"])));
                paramList.Add(new SqlParameter("@P_flexCreatedBy", string.IsNullOrEmpty(dr["flexCreatedBy"].ToString())? 0: Convert.ToInt32(dr["flexCreatedBy"]))); 
                paramList.Add(new SqlParameter("@P_flexId", Convert.ToInt32(dr["flexid"])));
                paramList.Add(new SqlParameter("@P_RecordStatus", recordStatus));
                if (!string.IsNullOrEmpty(Convert.ToString(dr["flexGDSprefix"])))
                    paramList.Add(new SqlParameter("@P_flexGDSprefix", Convert.ToString(dr["flexGDSprefix"])));
                paramList.Add(new SqlParameter("@P_flexPlaceHolder", Convert.ToString(dr["flexplacehold"])));
                if ( dr["flexValId"]  != DBNull.Value)
                paramList.Add(new SqlParameter("@P_flexValId", Convert.ToInt32(dr["flexValId"])));
                paramList.Add(new SqlParameter("@P_flexProductId", ProductId));
                paramList.Add(new SqlParameter("@P_flexDisable", Convert.ToBoolean(dr["flexDisable"])));
                paramList.Add(new SqlParameter("@P_flexTravelReason", string.IsNullOrEmpty(dr["flexTravelReason"].ToString()) ? 0 : Convert.ToInt32(dr["flexTravelReason"].ToString().Split('~')[0])));               
                paramList.Add(new SqlParameter("@P_flexStatus", dr["flexStatus"].ToString()));               

                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramList.Add(paramMsgType);

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramList.Add(paramMsgText); 
               
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_Add_Corp_Profile_Flex_Master", paramList.ToArray());
                if (paramList[0].Value != DBNull.Value)
                    Id = (int)paramList[0].Value; 
                if (Convert.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Convert.ToString(paramMsgText.Value));
                return Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SaveFlexDeatils(DataTable dt, SqlCommand cmd, long agentid, int flexorder, string flexlabel, string flexcontrol, string flexquery, string flexdatatype, string flexmandatorystatus, long flexId, int createdBy, int recordStatus,string GDSprefix,string PlaceHolder,int ProductId,bool flexDisable,int Valid,int flexTravelReason)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[19];
                paramList[0] = new SqlParameter("@P_flexAgentId", agentid);
                paramList[1] = new SqlParameter("@P_flexOrder", flexorder);
                paramList[2] = new SqlParameter("@P_flexLabel", flexlabel);
                paramList[3] = new SqlParameter("@P_flexControl", flexcontrol);
                if (!string.IsNullOrEmpty(flexquery
                    )) paramList[4] = new SqlParameter("@P_flexSqlQuery", flexquery);
                paramList[5] = new SqlParameter("@P_flexDataType", flexdatatype);
                paramList[6] = new SqlParameter("@P_flexMandatoryStatus", flexmandatorystatus);
                paramList[7] = new SqlParameter("@P_flexCreatedBy", createdBy);
                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramList[8] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramList[9] = paramMsgText;
                paramList[10] = new SqlParameter("@P_flexId", flexId);
                paramList[11] = new SqlParameter("@P_RecordStatus", recordStatus);
             if(!string.IsNullOrEmpty(GDSprefix))   paramList[12] = new SqlParameter("@P_flexGDSprefix", GDSprefix);
                paramList[13] = new SqlParameter("@P_flexPlaceHolder", PlaceHolder);
             if(Valid!=0)  paramList[14] = new SqlParameter("@P_flexValId", Valid);
                paramList[15] = new SqlParameter("@P_flexProductId", ProductId);
                paramList[16] = new SqlParameter("@P_flexDisable", flexDisable);
                paramList[17] = new SqlParameter("@Id", SqlDbType.Int);
                paramList[17].Direction = ParameterDirection.Output;
                paramList[18] = new SqlParameter("@P_flexTravelReason", flexTravelReason);
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_Add_Corp_Profile_Flex_Master", paramList);
                if (paramList[17].Value != DBNull.Value)
                {
                    int Id = (int)paramList[17].Value;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["flexValId"] != DBNull.Value)
                        {
                            if (Convert.ToInt32(dr["flexValId"]) == flexId)
                            {
                                dr["flexValId"] = Id;
                            }
                        }

                    }
                }
                
                    if (Convert.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Convert.ToString(paramMsgText.Value));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
