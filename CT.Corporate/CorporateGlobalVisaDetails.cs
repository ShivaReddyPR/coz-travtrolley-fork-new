﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.Corporate
{
    public class CorporateGlobalVisaDetails
    {
        public CorporateGlobalVisaDetails()
        {
        }

        public static DataTable GetGVDetailsList(DateTime fromDate, DateTime toDate, int profileId)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[3];
                paramArr[0] = new SqlParameter("@p_from_date", fromDate);
                paramArr[1] = new SqlParameter("@p_to_date", toDate);
                if (profileId > 0)
                {
                    paramArr[2] = new SqlParameter("@P_UserId", profileId);
                }
                else
                {
                    paramArr[2] = new SqlParameter("@P_UserId", DBNull.Value);
                }
                return DBGateway.ExecuteQuery("GV_p_visa_sales_getApprove_list", paramArr).Tables[0]; ;
            }
            catch { throw; }
        }

        public static DataTable GetGVApprovalDetailsList(int profileId, DateTime fromDate, DateTime toDate, int selEmpProfileId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                if (profileId > 0)
                {
                    paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                }
                else
                {
                    paramList[0] = new SqlParameter("@P_ProfileId", DBNull.Value);
                }
                paramList[1] = new SqlParameter("@P_FromDate", fromDate);
                paramList[2] = new SqlParameter("@P_ToDate", toDate);
                if (selEmpProfileId > 0)
                {
                    paramList[3] = new SqlParameter("@P_SelProfileId", selEmpProfileId);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_SelProfileId", DBNull.Value);
                }
                return DBGateway.FillDataTableSP("usp_Get_Corp_GV_Approvals_List", paramList);
            }
            catch
            {
                throw;
            }
        }
        public static void UpdateApproverStatus(int expDetailId, string approvalStatus, int modifiedBy)
        {
            try
            {

                SqlParameter[] paramArr = new SqlParameter[3];
                paramArr[0] = new SqlParameter("@P_ExpDetail_ID", expDetailId);
                paramArr[1] = new SqlParameter("@P_APPROVAL_STATUS", approvalStatus);
                paramArr[2] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                DBGateway.ExecuteNonQuery("usp_UpdateGVApprovalStatus", paramArr);
            }
            catch { throw; }
        }

    }
}
