﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
/// <summary>
/// Summary description for Class1
/// </summary>
/// 
namespace CT.Corporate
{
    public class CorporateProfile
    {
        #region private Variables
        //private long _id;
        private long _profileId;
        private int _agentId;
        private string _profileType;
        private string _title;
        private string _surName;
        private string _name;
        private string _designation;
        private string _designationName;
        private string _imagePath;
        private string _imageType;
        private string _employeeId;
        private int _division;
        private int _costCentre;
        private string _grade;
        private string _telephone;
        private string _mobilephone;
        private string _fax;
        private string _email;
        private string _address1;
        private string _address2;
        private string _nationalityCode;
        private string _passportNo;
        private DateTime _dateOfIssue;
        private DateTime _dateOfExpiry;
        private string _placeOfIssue;
        private DateTime _dateOfBirth;
        private string _placeOfBirth;
        private string _gender;

        private string _seatPreference;
        private string _mealRequest;
        private string _hotelRemarks;
        private string _otherRemarks;
        private string _status;
        private int _createdBy;
        private DataTable _dtProfileDetails;
        private DataTable _dtProfileFlex;
        private DataTable _dtProfilePolicy; //If there is no data

        private string _loginName;
        private string _password;
        private MemberType _memberType;
        private int _locationId;

        private long _profileIdRet;
        private string _deletedDetails;

        private string _policyIds;
        private string _policyStatus;
        //private DataTable _dtUserRoles;
        private long _profileUserId;
        //Added by lokesh on 27-Nov-2017.
        private DateTime _dateOfJoining;

        // Add By Somasekhar on 29/03/2018
        private string _martialStatus;
        // Add By Phani on 29/03/2018
        private string _middleName;
        private DateTime _dateOfTermination;
        private string _gstNumber;
        private string _gstEmail;
        private string _gstName;
        private string _gstPhone;
        private string _gstAddress;
        private string _residency;
        private string _execAssistance;
        private long _deligateSupervisor;
        private string _gdsProfilePNR;
        private bool _domesticEligibility;
        private bool _intlEligibility;
        private string _PassportCOI;
        private string _batch;
        private bool _isActive;
        private bool _isCardAllowedTravel;  
        private bool _isCardAllowedExpense;
        private int _TravelCardID;
        private int _ExpenseCardID;
        private string _approverType;
        private bool _affidivit;
        private List<CorpCostCenter> _corpCostCenters;
        #endregion

        List<CorpProfileApproval> _profileApproversList = new List<CorpProfileApproval>();
        List<CorpProfileVisaDetails> _profileVisaDetailsList = new List<CorpProfileVisaDetails>();
        List<CorpProfileDocuments> _profileDocsList = new List<CorpProfileDocuments>();
        List<CropProfileContactDetails> _profileContactList = new List<CropProfileContactDetails>();
        List<CropProfileGDSSettings> _profileGDSSettings = new List<CropProfileGDSSettings>();
        #region public Properties
        public long ProfileId
        {
            get
            { return _profileId; }
            set
            { _profileId = value; }
        }
        public int AgentId
        {
            get
            { return _agentId; }
            set
            { _agentId = value; }
        }
        public string ProfileType
        {
            get
            { return _profileType; }
            set
            { _profileType = value; }
        }


        public string Title
        {
            get
            { return _title; }
            set
            { _title = value; }
        }

        public string SurName
        {
            get
            { return _surName; }
            set
            { _surName = value; }
        }
        public string Name
        {
            get
            { return _name; }
            set
            { _name = value; }
        }

        public string Designation
        {
            get
            { return _designation; }
            set
            { _designation = value; }
        }

        public string ImagePath
        {
            get
            { return _imagePath; }
            set
            { _imagePath = value; }
        }

        public string ImageType
        {
            get
            { return _imageType; }
            set
            { _imageType = value; }
        }

        public string EmployeeId
        {
            get
            { return _employeeId; }
            set
            { _employeeId = value; }
        }

        public int Division
        {
            get
            { return _division; }
            set
            { _division = value; }
        }

        public int CostCentre
        {
            get
            { return _costCentre; }
            set
            { _costCentre = value; }
        }

        public string Grade
        {
            get
            { return _grade; }
            set
            { _grade = value; }
        }

        public string Telephone
        {
            get
            { return _telephone; }
            set
            { _telephone = value; }
        }

        public string Mobilephone
        {
            get
            { return _mobilephone; }
            set
            { _mobilephone = value; }
        }

        public string Fax
        {
            get
            { return _fax; }
            set
            { _fax = value; }
        }

        public string Email
        {
            get
            { return _email; }
            set
            { _email = value; }
        }

        public string Address1
        {
            get
            { return _address1; }
            set
            { _address1 = value; }
        }

        public string Address2
        {
            get
            { return _address2; }
            set
            { _address2 = value; }
        }

        public string NationalityCode
        {
            get
            { return _nationalityCode; }
            set
            { _nationalityCode = value; }
        }
        public string PassportNo
        {
            get
            { return _passportNo; }
            set
            { _passportNo = value; }
        }

        public DateTime DateOfIssue
        {
            get
            { return _dateOfIssue; }
            set
            { _dateOfIssue = value; }
        }

        public DateTime DateOfExpiry
        {
            get
            { return _dateOfExpiry; }
            set
            { _dateOfExpiry = value; }
        }

        public string PlaceOfIssue
        {
            get
            { return _placeOfIssue; }
            set
            { _placeOfIssue = value; }
        }

        public DateTime DateOfBirth
        {
            get
            { return _dateOfBirth; }
            set
            { _dateOfBirth = value; }
        }

        public string PlaceOfBirth
        {
            get
            { return _placeOfBirth; }
            set
            { _placeOfBirth = value; }
        }

        public string Gender
        {
            get
            { return _gender; }
            set
            { _gender = value; }
        }

        public string SeatPreference
        {
            get
            { return _seatPreference; }
            set
            { _seatPreference = value; }
        }

        public string MealRequest
        {
            get
            { return _mealRequest; }
            set
            { _mealRequest = value; }
        }

        public string HotelRemarks
        {
            get
            { return _hotelRemarks; }
            set
            { _hotelRemarks = value; }
        }

        public string OtherRemarks
        {
            get
            { return _otherRemarks; }
            set
            { _otherRemarks = value; }
        }

        public string Status
        {
            get
            { return _status; }
            set
            { _status = value; }
        }


        public int CreatedBy
        {
            get
            { return _createdBy; }
            set
            { _createdBy = value; }
        }

        public DataTable DtProfileDetails
        {
            get
            { return _dtProfileDetails; }
            set
            { _dtProfileDetails = value; }
        }

        public long ProfileIdRet
        {
            get
            { return _profileIdRet; }
            set
            { _profileIdRet = value; }
        }

        public DataTable DtProfileFlex
        {
            get
            { return _dtProfileFlex; }
            set
            { _dtProfileFlex = value; }
        }

        public DataTable DtProfilePolicy
        {
            get
            { return _dtProfilePolicy; }
            set
            { _dtProfilePolicy = value; }
        }


        public string LoginName
        {
            get { return _loginName; }
            set { _loginName = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public MemberType MemberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }

        public int LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        public string DeletedDetails
        {
            get { return _deletedDetails; }
            set { _deletedDetails = value; }
        }


        public string PolicyIds
        {
            get { return _policyIds; }
            set { _policyIds = value; }
        }

        public string PolicyStatus
        {
            get { return _policyStatus; }
            set { _policyStatus = value; }
        }

        //public DataTable DtUserRoles
        //{
        //    get
        //    { return _dtUserRoles; }
        //    set
        //    { _dtUserRoles = value; }
        //}

        //Add By Somasekhar on 29/03/2018       
        public string MartialStatus
        {
            get
            { return _martialStatus; }
            set
            { _martialStatus = value; }
        }
        public string MiddleName
        {
            get
            { return _middleName; }
            set
            { _middleName = value; }
        }
        public string GstNumber
        {
            get
            { return _gstNumber; }
            set
            { _gstNumber = value; }
        }
        public string GstEmail
        {
            get
            { return _gstEmail; }
            set
            { _gstEmail = value; }
        }
        public string GstName
        {
            get
            { return _gstName; }
            set
            { _gstName = value; }
        }
        public string GstPhone
        {
            get
            { return _gstPhone; }
            set
            { _gstPhone = value; }
        }
        public string GstAddress
        {
            get
            { return _gstAddress; }
            set
            { _gstAddress = value; }
        }
        public string Residency
        {
            get
            { return _residency; }
            set
            { _residency = value; }
        }
        public long DeligateSupervisor
        {
            get
            { return _deligateSupervisor; }
            set
            { _deligateSupervisor = value; }
        }
        public string ExecAssistance
        {
            get
            { return _execAssistance; }
            set
            { _execAssistance = value; }
        }
        public string GdsProfilePNR
        {
            get
            { return _gdsProfilePNR; }
            set
            { _gdsProfilePNR = value; }
        }
        public bool DomesticEligibility
        {
            get
            { return _domesticEligibility; }
            set
            { _domesticEligibility = value; }
        }
        public bool IntlEligibility
        {
            get
            { return _intlEligibility; }
            set
            { _intlEligibility = value; }
        }
        public DateTime DateOfJoining
        {
            get { return _dateOfJoining; }
            set { _dateOfJoining = value; }
        }
        public List<CorpProfileApproval> ProfileApproversList
        {
            get { return _profileApproversList; }
            set { _profileApproversList = value; }
        }
        //Added by lokesh on 27-Nov-2017.
        public DateTime DateOfTermination
        {
            get { return _dateOfTermination; }
            set { _dateOfTermination = value; }
        }

        public List<CorpProfileVisaDetails> ProfileVisaDetailsList
        {
            get { return _profileVisaDetailsList; }
            set { _profileVisaDetailsList = value; }
        }
        public List<CorpProfileDocuments> ProfileDocsList
        {
            get { return _profileDocsList; }
            set { ProfileDocsList = value; }
        }
        public string DesignationName
        {
            get { return _designationName; }
            set { _designationName = value; }
        }
        public List<CropProfileContactDetails> ProfileContactList
        {
            get { return _profileContactList; }
            set { _profileContactList = value; }
        }
        public List<CropProfileGDSSettings> ProfileGDSSettings
        {
            get { return _profileGDSSettings; }
            set { _profileGDSSettings = value; }
        }
        public string PassportCOI
        {
            get { return _PassportCOI; }
            set { _PassportCOI = value; }
        }

        public string Batch { get => _batch; set => _batch = value; }
        public bool IsActive { get => _isActive; set => _isActive = value; }
        public string ApproverType { get => _approverType; set => _approverType = value; }
        public bool Affidivit { get => _affidivit; set => _affidivit = value; }
        public bool IsCardAllowedTravel { get => _isCardAllowedTravel; set => _isCardAllowedTravel = value; }
        public bool IsCardAllowedExpense { get => _isCardAllowedExpense; set => _isCardAllowedExpense = value; }
        public int TravelCardID { get => _TravelCardID; set => _TravelCardID = value; }
        public int ExpenseCardID { get => _ExpenseCardID; set => _ExpenseCardID = value; }
        public List<CorpCostCenter> CorpCostCenters { get => _corpCostCenters; set => _corpCostCenters = value; }
        #endregion

        #region Constructors
        public CorporateProfile()
        {

            _profileId = -1;

            DataSet ds = GetData(_profileId, Settings.LoginInfo.AgentId);
            //if (dt != null && dt.Rows.Count > 0)
            UpdateBusinessData(ds);
        }
        public CorporateProfile(long id, int agentId)
        {


            _profileId = id;

            GetDetails(id, agentId);
            CorpProfileApproval approvals = new CorpProfileApproval();
            _profileApproversList = approvals.Load(_profileId);

            CorpProfileVisaDetails details = new CorpProfileVisaDetails();
            _profileVisaDetailsList = details.Load(_profileId);

            CorpProfileDocuments docs = new CorpProfileDocuments();
            _profileDocsList = docs.Load(_profileId);
            CropProfileContactDetails Contact = new CropProfileContactDetails();
            _profileContactList = Contact.Load(_profileId);
            CropProfileGDSSettings GDSStttings = new CropProfileGDSSettings();
            _profileGDSSettings = GDSStttings.Load(_profileId);

        }
        #endregion

        private void GetDetails(long id, int agentId)
        {

            DataSet ds = GetData(id, agentId);
            UpdateBusinessData(ds);
            //if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            //{
            //    UpdateBusinessData(ds.Tables[0].Rows[0]);
            //}

        }
        private DataSet GetData(long id, int agentId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_ProfileId", id);
                if (agentId > 0) paramList[1] = new SqlParameter("@P_AgentId", agentId);
                try { paramList[2] = new SqlParameter("@P_UserID", Settings.LoginInfo.UserID); } catch { paramList[2] = new SqlParameter("@P_UserID", 0); }
                //if (Settings.LoginInfo != null && Settings.LoginInfo.UserID > 0) paramList[2] = new SqlParameter("@P_UserID", Settings.LoginInfo.UserID);
                DataSet dsResult = DBGateway.ExecuteQuery("CORP_PROFILE_GETDATA", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetCorpProfilesList(int agentId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AgentId", agentId);
                return DBGateway.FillDataTableSP("usp_GetCorpProfilesList", paramList);
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _profileId = Utility.ToLong(dr["ProfileId"]);
                _agentId = Utility.ToInteger(dr["AgentId"]); ;
                _profileType = Utility.ToString(dr["ProfileType"]);
                _title = Utility.ToString(dr["Title"]);
                _surName = Utility.ToString(dr["SurName"]);
                _name = Utility.ToString(dr["Name"]);
                _designation = Utility.ToString(dr["Designation"]);
                _designationName = Utility.ToString(dr["DesignationName"]);
                _imagePath = Utility.ToString(dr["ImagePath"]);
                _imageType = Utility.ToString(dr["ImageType"]);
                _employeeId = Utility.ToString(dr["EmployeeId"]);
                _division = Utility.ToInteger(dr["Division"]);
                _costCentre = Utility.ToInteger(dr["CostCentre"]);
                _grade = Utility.ToString(dr["Grade"]);
                _telephone = Utility.ToString(dr["Telephone"]);
                _mobilephone = Utility.ToString(dr["Mobilephone"]);
                _fax = Utility.ToString(dr["Fax"]);
                _email = Utility.ToString(dr["Email"]);
                _address1 = Utility.ToString(dr["Address1"]);
                _address2 = Utility.ToString(dr["Address2"]);
                _nationalityCode = Utility.ToString(dr["NationalityCode"]);
                _passportNo = Utility.ToString(dr["PassportNo"]);
                _dateOfIssue = Utility.ToDate(dr["DateOfIssue"]);
                _dateOfExpiry = Utility.ToDate(dr["DateOfExpiry"]);
                _placeOfIssue = Utility.ToString(dr["PlaceOfIssue"]);
                _dateOfBirth = Utility.ToDate(dr["DateOfBirth"]);
                _placeOfBirth = Utility.ToString(dr["PlaceOfBirth"]);
                _gender = Utility.ToString(dr["Gender"]);
                _seatPreference = Utility.ToString(dr["SeatPreference"]);
                _mealRequest = Utility.ToString(dr["MealRequest"]);
                _hotelRemarks = Utility.ToString(dr["HotelRemarks"]);
                _otherRemarks = Utility.ToString(dr["OtherRemarks"]);
                _status = Utility.ToString(dr["Status"]);
                _createdBy = Utility.ToInteger(dr["CreatedBy"]);
                _loginName = Utility.ToString(dr["user_login_name"]);
                _locationId = Utility.ToInteger(dr["user_location_id"]);
                _dateOfJoining = Utility.ToDate(dr["DateOfJoining"]);
                string type = Utility.ToString(dr["USER_MEMBER_TYPE"]);
                if (type == MemberType.ADMIN.ToString())
                {
                    _memberType = MemberType.ADMIN;
                }
                else if (type == MemberType.CASHIER.ToString())
                {
                    _memberType = MemberType.CASHIER;
                }
                else if (type == MemberType.OPERATIONS.ToString())
                {
                    _memberType = MemberType.OPERATIONS;
                }
                else if (type == MemberType.BACKOFFICE.ToString())
                {
                    _memberType = MemberType.BACKOFFICE;
                }
                else if (type == MemberType.SUPERVISOR.ToString())
                {
                    _memberType = MemberType.SUPERVISOR;
                }

                //Added by somasekhar on 31/03/2018
                _martialStatus = Utility.ToString(dr["fpax_marital_status"]);
                //phani
                _middleName = Utility.ToString(dr["MiddleName"]);
                _gstNumber = Utility.ToString(dr["GSTNumber"]);
                _gstName = Utility.ToString(dr["GSTName"]);
                _gstAddress = Utility.ToString(dr["GSTAddress"]);
                _gstPhone = Utility.ToString(dr["GSTPhone"]);
                _gstEmail = Utility.ToString(dr["GSTEmail"]);
                _gdsProfilePNR = Utility.ToString(dr["GDSProfilePNR"]);
                _dateOfTermination = Utility.ToDate(dr["DateOfTermination"]);
                _residency = Utility.ToString(dr["Residency"]);
                _execAssistance = Utility.ToString(dr["ExecAssistance"]);
                _deligateSupervisor = Convert.ToInt64(dr["DeligateSupervisor"]);
                _domesticEligibility = Utility.ToBoolean(dr["DomesticEligibility"]);
                _intlEligibility = Utility.ToBoolean(dr["IntlEligibility"]);
                _PassportCOI = Utility.ToString(dr["PassportCOI"]);
                _batch = Utility.ToString(dr["Batch"]);
                _approverType = dr["ApproverType"] != DBNull.Value ? Utility.ToString(dr["ApproverType"]) : "N";
               _affidivit = Utility.ToBoolean(dr["Affidivit"]);
               _isCardAllowedExpense = Utility.ToBoolean(dr["IsCardAllowedExpense"]);
               _isCardAllowedTravel = Utility.ToBoolean(dr["IsCardAllowedTravel"]);
               _TravelCardID = Utility.ToInteger(dr["TravelCardID"]);
               _ExpenseCardID = Utility.ToInteger(dr["ExpenseCardID"]);
               // Loading the Corp Cost center details.
                _corpCostCenters = CorpCostCenter.Load(_profileId);
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds != null)
                    {
                        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                                UpdateBusinessData(ds.Tables[0].Rows[0]);
                        }
                    }
                    if (ds.Tables[1] != null)
                        _dtProfileDetails = ds.Tables[1];
                    if (ds.Tables[2] != null)
                        _dtProfileFlex = ds.Tables[2];
                    if (ds.Tables[3] != null)
                        _dtProfilePolicy = ds.Tables[3];
                    //if (ds.Tables[4] != null)
                    //    _dtUserRoles = ds.Tables[4];
                    // if (ds.Tables[3] != null)
                    //  _dtProfileFlexMaster = ds.Tables[3];


                }

            }
            catch { throw; }

        }

        #region Public methods
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                SqlParameter[] paramList = new SqlParameter[64];
                paramList[0] = new SqlParameter("@P_ProfileId", _profileId);
                paramList[1] = new SqlParameter("@P_AgentId", _agentId);
                paramList[2] = new SqlParameter("@P_ProfileType", _profileType);
                paramList[3] = new SqlParameter("@P_SurName", _surName);
                paramList[4] = new SqlParameter("@P_Name", _name);
                paramList[5] = new SqlParameter("@P_Designation", _designation);
                if (!string.IsNullOrEmpty(_imagePath)) paramList[6] = new SqlParameter("@P_ImagePath", _imagePath);
                if (!string.IsNullOrEmpty(_imageType)) paramList[7] = new SqlParameter("@P_ImageType", _imageType);
                paramList[8] = new SqlParameter("@P_EmployeeId", _employeeId);

                paramList[9] = new SqlParameter("@P_Division", _division);
                paramList[10] = new SqlParameter("@P_CostCentre", _costCentre);
                paramList[11] = new SqlParameter("@P_Grade", _grade);
                paramList[12] = new SqlParameter("@P_Telephone", _telephone);
                paramList[13] = new SqlParameter("@P_Mobilephone", _mobilephone);
                if (!string.IsNullOrEmpty(_fax)) paramList[14] = new SqlParameter("@P_Fax", _fax);
                paramList[15] = new SqlParameter("@P_Email", _email);
                paramList[16] = new SqlParameter("@P_Address1", _address1);
                if (!string.IsNullOrEmpty(_address2)) paramList[17] = new SqlParameter("@P_Address2", _address2);


                paramList[18] = new SqlParameter("@P_NationalityCode", _nationalityCode);
                paramList[19] = new SqlParameter("@P_PassportNo", _passportNo);
                paramList[20] = new SqlParameter("@P_DateOfIssue", _dateOfIssue);
                paramList[21] = new SqlParameter("@P_DateOfExpiry", _dateOfExpiry);
                paramList[22] = new SqlParameter("@P_PlaceOfIssue", _placeOfIssue);

                paramList[23] = new SqlParameter("@P_DateOfBirth", _dateOfBirth);
                paramList[24] = new SqlParameter("@P_PlaceOfBirth", _placeOfBirth);
                paramList[25] = new SqlParameter("@P_Gender", _gender);


                paramList[26] = new SqlParameter("@P_SeatPreference", _seatPreference);

                paramList[27] = new SqlParameter("@P_MealRequest", _mealRequest);

                paramList[28] = new SqlParameter("@P_HotelRemarks", _hotelRemarks);
                paramList[29] = new SqlParameter("@P_OtherRemarks", _otherRemarks);
                paramList[30] = new SqlParameter("@P_Status", _status);
                paramList[31] = new SqlParameter("@P_CreatedBy", _createdBy);
                paramList[32] = new SqlParameter("@P_ProfileId_Ret", SqlDbType.BigInt);
                paramList[32].Direction = ParameterDirection.Output;
                paramList[33] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[33].Direction = ParameterDirection.Output;
                paramList[34] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[34].Direction = ParameterDirection.Output;

                paramList[35] = new SqlParameter("@P_Login_name", _loginName);
                if (!string.IsNullOrEmpty(_password)) paramList[36] = new SqlParameter("@P_PASSWORD", GenericStatic.EncryptData(_password));
                paramList[37] = new SqlParameter("@P_MEMBER_TYPE", _memberType.ToString());

                paramList[38] = new SqlParameter("@P_LOCATION_ID", _locationId);
                paramList[39] = new SqlParameter("@P_Title", _title);
                paramList[40] = new SqlParameter("@P_ProfileUserId", SqlDbType.BigInt);
                paramList[40].Direction = ParameterDirection.Output;

                paramList[41] = new SqlParameter("@P_DATE_OF_JOINING", _dateOfJoining);
                paramList[42] = new SqlParameter("@P_MartialStatus", _martialStatus);
                //phani
                paramList[43] = new SqlParameter("@P_MiddleName", _middleName);
                paramList[44] = _dateOfTermination == DateTime.MinValue ? new SqlParameter("@P_DateOfTermination", DBNull.Value) :
                    new SqlParameter("@P_DateOfTermination", _dateOfTermination);
                paramList[45] = new SqlParameter("@P_GstNumber", _gstNumber);
                paramList[46] = new SqlParameter("@P_GstName", _gstName);
                paramList[47] = new SqlParameter("@P_GstPhone", _gstPhone);
                paramList[48] = new SqlParameter("@P_GstAddress", _gstAddress);
                paramList[49] = new SqlParameter("@P_GstEmail", _gstEmail);
                paramList[50] = new SqlParameter("@P_Residency", _residency);
                paramList[51] = new SqlParameter("@P_ExecAssistance", _execAssistance);
                paramList[52] = new SqlParameter("@P_DeligateSupervisor", _deligateSupervisor);
                paramList[53] = new SqlParameter("@P_GdsProfilePNR", _gdsProfilePNR);
                paramList[54] = new SqlParameter("@P_DomesticEligibility", _domesticEligibility);
                paramList[55] = new SqlParameter("@P_IntlEligibility", _intlEligibility);
                paramList[56] = new SqlParameter("@PassportCOI", _PassportCOI);
                paramList[57] = new SqlParameter("@P_batch", _batch);
                paramList[58] = new SqlParameter("@P_approverType", _approverType);
                paramList[59] = new SqlParameter("@P_affidivit", _affidivit);
                paramList[60] = new SqlParameter("@P_ExpenseCardID", _ExpenseCardID);
                paramList[61] = new SqlParameter("@P_TravelCardID", _TravelCardID);
                paramList[62] = new SqlParameter("@P_IsCardAllowedExpense", _isCardAllowedExpense);
                paramList[63] = new SqlParameter("@P_IsCardAllowedTravel", _isCardAllowedTravel);
                DBGateway.ExecuteNonQueryDetails(cmd, "CORP_PROFILE_ADD_UPDATE", paramList);
                string messageType = Utility.ToString(paramList[33].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[34].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _profileIdRet = Utility.ToLong(paramList[32].Value);
                _profileUserId = Utility.ToLong(paramList[40].Value);
                if (_corpCostCenters != null && _corpCostCenters.Count > 0)
                {
                    foreach (CorpCostCenter costCenter in _corpCostCenters)
                    {
                        costCenter.profileid = (int)ProfileIdRet;
                        costCenter.Save();
                    }
                }
                if (_dtProfileDetails != null)
                {

                    DataTable dt = _dtProfileDetails.GetChanges();
                    int recordStatus = 0;
                    if (dt != null && dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dt.Rows)
                        {

                            switch (dr.RowState)
                            {
                                case DataRowState.Added:
                                    recordStatus = 1;
                                    break;
                                case DataRowState.Modified:
                                    recordStatus = 2;
                                    break;
                                case DataRowState.Deleted:
                                    recordStatus = -1;
                                    dr.RejectChanges();
                                    break;
                                default: break;
                            }
                            SaveCorpProfileDetails(cmd, recordStatus, Utility.ToLong(dr["DetailId"]), _profileIdRet, Utility.ToString(dr["DetailType"]), Utility.ToString(dr["DisplayValue"]),
                                Utility.ToString(dr["DisplayText"]), Utility.ToString(dr["Value"]), Utility.ToString(dr["DisplayCardType"]), Utility.ToString(dr["Status"]), _createdBy);
                        }
                    }
                }
                //TO DELETE THE REMOVED ITEMS 
                if (!string.IsNullOrEmpty(_deletedDetails))
                {
                    string[] deletedIds = _deletedDetails.Split(',');
                    for (int x = 0; x < deletedIds.Length - 1; x++)
                    {

                        DeleteProfileDetails(cmd, Utility.ToLong(deletedIds[x]), _profileId, Settings.LoginInfo.UserID);
                    }

                }

                DataTable dtFlex = _dtProfileFlex;
                if (dtFlex != null && dtFlex.Rows.Count > 0)
                {
                    foreach (DataRow drFlex in dtFlex.Rows)
                    {

                        SaveCorpProfileFlexDetails(cmd, Utility.ToLong(drFlex["DetailId"]), Utility.ToLong(drFlex["flexId"]), _profileIdRet, Utility.ToString(drFlex["flexLabel"]), Utility.ToString(drFlex["Detail_FlexData"]), Settings.LoginInfo.UserID);

                    }

                }

                if (!string.IsNullOrEmpty(_policyIds))
                {
                    string[] policyIds = _policyIds.Split(',');
                    string[] policyStatus = _policyStatus.Split(',');
                    for (int x = 0; x < policyIds.Length - 1; x++)
                    {

                        SaveCorpProfilePolicyLink(cmd, Utility.ToLong(policyIds[x]), _profileIdRet, policyStatus[x], Settings.LoginInfo.UserID);
                    }

                }
                //Commented by lokesh now the corporate users are not inserted into CT_T_user_master table
                //So no roles for the user.

                //DataTable dtRoles = _dtUserRoles;
                //if (dtRoles != null && dtRoles.Rows.Count > 0)
                //{
                //    int recordStatus = -1;
                //    foreach (DataRow drRoles in dtRoles.Rows)
                //    {
                //        if (Utility.ToLong(drRoles["urd_id"]) > 0) recordStatus = 2;
                //        else recordStatus = 1;

                //        SaveUserRoles(cmd, recordStatus, Utility.ToLong(drRoles["urd_id"]), _profileUserId, Utility.ToLong(drRoles["Role_id"]), Utility.ToString(drRoles["urd_status"]), Settings.LoginInfo.UserID);

                //    }

                //}
                if (_profileApproversList != null && _profileApproversList.Count > 0)
                {
                    foreach (CorpProfileApproval pa in _profileApproversList)
                    {
                        pa.ProfileId = (int)ProfileIdRet;
                        pa.SaveCorpProfileApprovals(cmd);
                    }
                }

                if (_profileVisaDetailsList != null && _profileVisaDetailsList.Count > 0)
                {
                    foreach (CorpProfileVisaDetails pa in _profileVisaDetailsList)
                    {
                        pa.ProfileId = (int)ProfileIdRet;
                        pa.SaveCorpProfileVisaDetails(cmd);
                    }
                }
                if (_profileContactList != null && _profileContactList.Count > 0)
                {
                    foreach (CropProfileContactDetails pa in _profileContactList)
                    {
                        pa.ProfileId = (int)ProfileIdRet;
                        pa.SaveCorpProfileContactDetails(cmd);
                    }
                }
                if (_profileGDSSettings != null && _profileGDSSettings.Count > 0)
                {
                    foreach (CropProfileGDSSettings pa in _profileGDSSettings)
                    {
                        pa.ProfileId = (int)ProfileIdRet;
                        pa.SaveCorpProfileGDSSettings(cmd);
                    }
                }
               
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {

                cmd.Connection.Close();
            }


        }

        public void SaveCorpProfileDetails(SqlCommand cmd, int recordStatus, long detailId, long profileId, string detailsType, string displayValue, string displayText, string value,
                 string cardType, string status, int createdBy)
        {

            try
            {

                SqlParameter[] paramArr = new SqlParameter[12];
                paramArr[0] = new SqlParameter("@P_DetailId", detailId);
                paramArr[1] = new SqlParameter("@P_ProfileId", profileId);
                paramArr[2] = new SqlParameter("@P_DetailType", detailsType);
                paramArr[3] = new SqlParameter("@P_DisplayValue", displayValue);
                paramArr[4] = new SqlParameter("@P_DisplayText", displayText);
                paramArr[5] = new SqlParameter("@P_Value", value);
                paramArr[6] = new SqlParameter("@P_Status", status);
                paramArr[7] = new SqlParameter("@P_CreatedBy", createdBy);
                paramArr[8] = new SqlParameter("@P_record_status", recordStatus);

                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[9] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[10] = paramMsgText;
                //This is added Anji by 10-oct-2019 regarding cardtype
                if (!string.IsNullOrEmpty(cardType)) paramArr[11] = new SqlParameter("@P_cardType", cardType);
                //SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
                //paramMsgText1.Size = 20;
                //paramMsgText1.Direction = ParameterDirection.Output;
                //paramArr[31] = paramMsgText1;



                DBGateway.ExecuteNonQueryDetails(cmd, "CORP_PROFILE_DETAILS_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

                //_docNumber = Utility.ToString(paramDocNo.Value);
                //_transactionId = Utility.ToLong(paramMsgText1.Value);

            }
            catch
            {
                throw;
            }
        }

        public void SaveCorpProfileFlexDetails(SqlCommand cmd, long flexDetailId, long flexId, long profileId, string flexLabel, string flexData, long createdBy)
        {

            try
            {

                SqlParameter[] paramArr = new SqlParameter[8];
                paramArr[0] = new SqlParameter("@P_DetailId", flexDetailId);
                paramArr[1] = new SqlParameter("@P_FlexId", flexId);

                paramArr[2] = new SqlParameter("@P_ProfileId", profileId);
                paramArr[3] = new SqlParameter("@P_FlexLabel", flexLabel);
                paramArr[4] = new SqlParameter("@P_FlexData", flexData);
                paramArr[5] = new SqlParameter("@P_CreatedBy", createdBy);

                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[6] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[7] = paramMsgText;

                //SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
                //paramMsgText1.Size = 20;
                //paramMsgText1.Direction = ParameterDirection.Output;
                //paramArr[31] = paramMsgText1;



                DBGateway.ExecuteNonQueryDetails(cmd, "CORP_PROFILE_FLEX_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

                //_docNumber = Utility.ToString(paramDocNo.Value);
                //_transactionId = Utility.ToLong(paramMsgText1.Value);

            }
            catch
            {
                throw;
            }
        }


        public void SaveCorpProfilePolicyLink(SqlCommand cmd, long policyId, long profileId, string status, long createdBy)
        {

            try
            {

                SqlParameter[] paramArr = new SqlParameter[6];
                paramArr[0] = new SqlParameter("@P_PolicyId", policyId);
                paramArr[1] = new SqlParameter("@P_ProfileId", profileId);
                paramArr[2] = new SqlParameter("@P_Status", status);
                paramArr[3] = new SqlParameter("@P_CreatedBy", createdBy);

                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[4] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[5] = paramMsgText;

                //SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
                //paramMsgText1.Size = 20;
                //paramMsgText1.Direction = ParameterDirection.Output;
                //paramArr[31] = paramMsgText1;



                DBGateway.ExecuteNonQueryDetails(cmd, "CORP_PROFILE_POLICYLINK_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

                //_docNumber = Utility.ToString(paramDocNo.Value);
                //_transactionId = Utility.ToLong(paramMsgText1.Value);

            }
            catch
            {
                throw;
            }
        }


        public void SaveUserRoles(SqlCommand cmd, int recordStatus, long urdId, long userId, long roleId, string status, long createdBy)
        {

            try
            {
                SqlParameter[] paramArr = new SqlParameter[8];
                paramArr[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramArr[1] = new SqlParameter("@P_URD_ID", urdId);

                paramArr[2] = new SqlParameter("@P_URD_USER_ID", userId);
                paramArr[3] = new SqlParameter("@P_URD_ROLE_ID", roleId);

                paramArr[4] = new SqlParameter("@P_URD_STATUS", status);
                paramArr[5] = new SqlParameter("@P_URD_CREATED_BY", createdBy);

                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[6] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 200;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[7] = paramMsgText;


                DBGateway.ExecuteNonQueryDetails(cmd, "ct_p_user_role_add_update", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));


            }
            catch { throw; }

        }

        public void DeleteProfileDetails(SqlCommand cmd, long DetailId, long profileId, long createdBy)
        {

            try
            {

                SqlParameter[] paramArr = new SqlParameter[3];
                paramArr[0] = new SqlParameter("@P_PROFILE_ID", profileId);
                paramArr[1] = new SqlParameter("@P_DETAILID", DetailId);
                paramArr[2] = new SqlParameter("@P_DELETEDBY", createdBy);

                //SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
                //paramMsgText1.Size = 20;
                //paramMsgText1.Direction = ParameterDirection.Output;
                //paramArr[31] = paramMsgText1;



                DBGateway.ExecuteNonQueryDetails(cmd, "CORP_PROFILE_DETAILS_DELETE", paramArr);


                //_docNumber = Utility.ToString(paramDocNo.Value);
                //_transactionId = Utility.ToLong(paramMsgText1.Value);

            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region StaticMethods
        public static DataTable GetCorpProfileSetpDetails(int agentId, string type, ListStatus status)
        {
            SqlConnection con = DBGateway.CreateConnection();
            try
            {
                SqlParameter[] paramArray = new SqlParameter[3];
                paramArray[0] = new SqlParameter("@P_AGENTID", agentId);
                if (!string.IsNullOrEmpty(type)) paramArray[1] = new SqlParameter("@P_TYPE", type);
                paramArray[2] = new SqlParameter("@P_LIST_STATUS", (int)status);
                DataTable dt = DBGateway.ExecuteQuery("CORP_PROFILE_SETPUP_GETLIST", paramArray).Tables[0];
                con.Close();
                return dt;
            }

            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        public static DataTable GetList(int agentId, long profileId, ListStatus status)
        {

            try
            {
                return GetList(agentId, profileId, status, string.Empty, string.Empty, string.Empty, string.Empty);
            }
            catch
            {
                throw;
            }
        }


        public static DataTable GetList(int agentId, long profileId, ListStatus status, string surName, string name, string employeeId, string email)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[8];
                if (profileId > 0) paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                if (agentId > 0) paramList[1] = new SqlParameter("@P_AgentId", agentId);
                paramList[2] = new SqlParameter("@P_LIST_STATUS", (int)status);
                if (!string.IsNullOrEmpty(surName)) paramList[3] = new SqlParameter("@P_SurName", surName);
                if (!string.IsNullOrEmpty(name)) paramList[4] = new SqlParameter("@P_Name", name);
                if (!string.IsNullOrEmpty(employeeId)) paramList[5] = new SqlParameter("@P_EmployeeId", employeeId);
                if (!string.IsNullOrEmpty(email)) paramList[6] = new SqlParameter("@P_Email", email);
                //paramList[7] = new SqlParameter("@user_member_type", Settings.LoginInfo.MemberType.ToString());
                paramList[7] = new SqlParameter("@P_login_id", Settings.LoginInfo.UserID);
                return DBGateway.ExecuteQuery("Corp_Profile_Getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable FillDropDown(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                dt = DBGateway.ExecuteQuery(query, paramList, CommandType.Text).Tables[0];
            }
            catch { throw; }
            return dt;
        }


        //Lokesh-1June2017 --Added regarding CorporateProfile.aspx -- Approvals Tab
        /// <summary>
        /// This method will return all the approvers from the table Corp_Profile with Grade 'M'
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="profileId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static DataTable GetProfilesListByGrade(string status, string grade, int agentId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", status);
                paramList[1] = new SqlParameter("@P_GRADE", grade);
                paramList[2] = new SqlParameter("@P_AGENTID", agentId);
                return DBGateway.ExecuteQuery("corp_profile_list_bygrade", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static int GetCorpProfileId(int userId)
        {
            int cProfileId = 0;

            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_UserId", userId);
                paramList[1] = new SqlParameter("@P_ProfileId", SqlDbType.Int);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQueryDetails(cmd, "usp_GetCorp_ProfileID", paramList);
                if (paramList[1].Value != DBNull.Value)
                {
                    cProfileId = Convert.ToInt32(paramList[1].Value);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return cProfileId;
        }


        public static DataTable GetCorpProfileDetailsByUserId(int userId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_userId", userId);
                return DBGateway.ExecuteQuery("usp_Get_Corp_Profile_Details_By_UserId", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetCorpEmployeesListByGrade(int profileId)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_Profile_Id", profileId);
                return DBGateway.FillDataTableSP("usp_Get_Corp_Employees_List_By_Grade", paramList);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static DataTable GetCorpProfilesListByApproverType(int profileId, string approverType)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_Profile_Id", profileId);
                paramList[1] = new SqlParameter("@P_ApproverType", approverType);
                return DBGateway.FillDataTableSP("usp_Get_Corp_Profiles_List_By_Approver", paramList);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static DataTable GetProfileDocDetails(int profileId)
        {
            DataTable dtDocs = null;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_PROFILE_ID", profileId);
            return dtDocs = DBGateway.FillDataTableSP("USP_CORP_PROFILE_GET_DOC_DETAILS", paramList);
        }

        /// <summary>
        /// Retrieves Corporate Flex Fields based on Agent and Product
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="productId"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetAgentFlexDetailsByProduct(int agentId, int productId)
        {
            DataTable dtFlexFields = new DataTable();
            try
            {
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@P_AgentId", agentId);
                parameters[1] = new SqlParameter("@P_ProductId", productId);
                if (Settings.LoginInfo != null && Settings.LoginInfo.UserID > 0) parameters[2] = new SqlParameter("@P_UserID", Settings.LoginInfo.UserID);
                dtFlexFields = DBGateway.FillDataTableSP("usp_GetCorpFlexFieldsByProduct", parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(AgentFlexDetails)Failed to retrieve Agent (" + agentId + ") Flex fields by Product (" + productId + ")" + ex.ToString(), "");
            }

            return dtFlexFields;
        }
        #endregion
        //To Retrive the Default Flex Fields for Import PNR
        public static DataTable GetDefaultFlexFields(int agentId, int productId)
        {
            DataTable dtDefaultFlexFields = new DataTable();
            try
            {
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@AgentId", agentId);
                parameters[1] = new SqlParameter("@ProductId", productId);
                dtDefaultFlexFields = DBGateway.FillDataTableSP("usp_GetDefaultFlex", parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(GetDefaultFlexFields)Failed to retrieve Agent (" + agentId + ") Default Flex fields by Product (" + productId + ")" + ex.ToString(), "");
            }
            return dtDefaultFlexFields;

        }

        #region for Getting ProfileId -- Somasekhar on 31/03/2018
        public static DataTable GetProfileIdbasedOnEmpID(string empID)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_EMPID", empID);

            DataTable dt = DBGateway.FillDataTableSP("USP_CORP_PROFILE_GETPROFILEIDBYEMPID", paramList);

            return dt;
        }

        #endregion
        public static DataTable GetCorpCardDetailsByAgent(int agentId)
        {
            DataTable dt = null;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@AgentID", agentId));
                dt = DBGateway.FillDataTableSP("USP_GetCardDetailsByAgent", paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        /// <summary>
        /// Returns the controls to be displayed for Petrofac profile master. Added by Anji
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static DataTable LoadControls(int agentId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AgentId", agentId);
                return DBGateway.FillDataTableSP("usp_GetCorpProfiles_Controls", paramList);
            }
            catch
            {
                throw;
            }
        }
    }

    /// <summary>
    /// This will class will hold the Approvers List based on Ticket or Expense Type.
    /// </summary>
    public class CorpProfileApproval
    {
        #region membersList
        int _id;
        int _profileId;
        int _approverId;
        int _hierarchy;
        string _type;
        string _status;
        int _createdBy;
        int _modifiedBy;
        string _approverName;
        string _approverEmail;
        int _tranxApprovalId;
        string _profileEmail;
        string _approvalStatus;
        string journeyType;
        decimal amount;
        string approvalType;
        #endregion

        #region Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int ProfileId
        {
            get { return _profileId; }
            set { _profileId = value; }
        }

        public int ApproverId
        {
            get { return _approverId; }
            set { _approverId = value; }
        }

        public int Hierarchy
        {
            get { return _hierarchy; }
            set { _hierarchy = value; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        public string ApproverEmail
        {
            get { return _approverEmail; }
            set { _approverEmail = value; }
        }

        public string ApproverName
        {
            get { return _approverName; }
            set { _approverName = value; }
        }

        public string ApprovalStatus
        {
            get { return _approvalStatus; }
            set { _approvalStatus = value; }
        }

        public int TranxApprovalId
        {
            get { return _tranxApprovalId; }
            set { _tranxApprovalId = value; }
        }

        public string ProfileEmail
        {
            get { return _profileEmail; }
            set { _profileEmail = value; }
        }

        public string JourneyType { get => journeyType; set => journeyType = value; }
        public decimal Amount { get => amount; set => amount = value; }
        public string ApprovalType { get => approvalType; set => approvalType = value; }

        #endregion


        public CorpProfileApproval()
        {

        }


        /// <summary>
        /// Saves the Approvers in the List in the table Corp_Profile_Approval  against the Profile Id
        /// </summary>
        public void SaveCorpProfileApprovals(SqlCommand cmd)
        {


            try
            {

                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@P_ProfileId", _profileId);
                paramList[1] = new SqlParameter("@P_ApproverId", _approverId);
                paramList[2] = new SqlParameter("@P_Hierarchy", _hierarchy);
                paramList[3] = new SqlParameter("@P_Status", _status);
                if (_id == -1)
                {
                    paramList[4] = new SqlParameter("@P_CreatedBy", _createdBy);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_CreatedBy", _modifiedBy);
                }
                paramList[5] = new SqlParameter("@P_Type", _type);
                paramList[6] = new SqlParameter("@P_ID", _id);

                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdate_Corporate_Profile_Approval", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets all the Approvers List from the table corp_profile_approval for the supplied profile Id.
        /// </summary>
        /// <param name="policyId"></param>
        /// <returns></returns>
        public List<CorpProfileApproval> Load(long profileId)
        {
            // SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<CorpProfileApproval> approversList = new List<CorpProfileApproval>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetProfileApproversList", paramList, connection);
                using (DataTable dtProfile = DBGateway.FillDataTableSP("usp_GetProfileApproversList", paramList))
                {
                    if (dtProfile != null && dtProfile.Rows.Count > 0)
                    {
                        foreach (DataRow data in dtProfile.Rows)
                        {
                            CorpProfileApproval approverDetails = new CorpProfileApproval();
                            if (data["Id"] != DBNull.Value)
                            {
                                approverDetails.Id = Convert.ToInt32(data["Id"]);
                            }
                            if (data["ProfileId"] != DBNull.Value)
                            {
                                approverDetails._profileId = Convert.ToInt32(data["ProfileId"]);
                            }
                            if (data["ApproverId"] != DBNull.Value)
                            {
                                approverDetails._approverId = Convert.ToInt32(data["ApproverId"]);
                            }
                            if (data["Hierarchy"] != DBNull.Value)
                            {
                                approverDetails._hierarchy = Convert.ToInt32(data["Hierarchy"]);
                            }
                            if (data["Type"] != DBNull.Value)
                            {
                                approverDetails._type = Convert.ToString(data["Type"]);
                            }
                            if (data["Status"] != DBNull.Value)
                            {
                                approverDetails._status = Convert.ToString(data["Status"]);
                            }
                            if (data["CreatedBy"] != DBNull.Value)
                            {
                                approverDetails._createdBy = Convert.ToInt32(data["CreatedBy"]);
                            }
                            if (data["email"] != DBNull.Value)
                            {
                                approverDetails._profileEmail = Convert.ToString(data["email"]);
                            }
                            if (data["JourneyType"] != DBNull.Value)
                            {
                                approverDetails.journeyType = Convert.ToString(data["JourneyType"]);
                            }
                            if (data["Amount"] != DBNull.Value)
                            {
                                approverDetails.amount = Convert.ToDecimal(data["Amount"]);
                            }
                            approversList.Add(approverDetails);
                        }
                    }
                }
                //data.Close();
                //connection.Close();
                return approversList;
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public static string GetRejectionReason(int flightId)
        {
            string reason = string.Empty;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_FlightId", flightId));
                DataTable dtReason = DBGateway.FillDataTableSP("Corp_Get_Rejection_Reason", parameters.ToArray());

                if (dtReason.Rows.Count > 0)
                {
                    reason = dtReason.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(CorpApproval)Failed to get Rejection reason for flightid: " + flightId + ". Reason : " + ex.ToString(), "");
            }

            return reason;
        }

        public static List<CorpProfileApproval> RemoveDuplicatesIterative(List<CorpProfileApproval> items)
        {
            List<CorpProfileApproval> result = new List<CorpProfileApproval>();
            for (int i = 0; i < items.Count; i++)
            {
                // Assume not duplicate.
                bool duplicate = false;
                for (int z = 0; z < i; z++)
                {
                    if (items[z].Hierarchy == items[i].Hierarchy)
                    {
                        // This is a duplicate.
                        duplicate = true;
                        break;
                    }
                }
                // If not duplicate, add to result.
                if (!duplicate)
                {
                    result.Add(items[i]);
                }
            }
            return result;
        }
    }


    /// <summary>
    /// This will class will hold the Visa Details of Individual Profile
    /// </summary>
    public class CorpProfileVisaDetails
    {
        #region membersList
        int _visaId;
        int _profileId;
        string _visaCountry;
        string _visaNumber;
        DateTime _visaIssueDate;
        DateTime _visaExpDate;
        string _status;
        int _createdBy;
        //Add by somasekhar on 28/03/2018 For "V- Visa"/"E- Emitates ID "
        string _type;
        //phani added
        string _visaplaceofissue;
        #endregion

        #region Properties

        public int VisaId
        {
            get { return _visaId; }
            set { _visaId = value; }
        }
        public int ProfileId
        {
            get { return _profileId; }
            set { _profileId = value; }
        }

        public string VisaCountry
        {
            get { return _visaCountry; }
            set { _visaCountry = value; }
        }
        public string VisaNumber
        {
            get { return _visaNumber; }
            set { _visaNumber = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public DateTime VisaIssueDate
        {

            get { return _visaIssueDate; }
            set { _visaIssueDate = value; }
        }
        public DateTime VisaExpDate
        {

            get { return _visaExpDate; }
            set { _visaExpDate = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string Visaplaceofissue
        {
            get { return _visaplaceofissue; }
            set { _visaplaceofissue = value; }
        }

        #endregion
        public CorpProfileVisaDetails()
        {

        }

        /// <summary>
        /// Saves the Visa Details in the table Corp_Profile_Visa_Details against the Profile Id
        /// </summary>
        public void SaveCorpProfileVisaDetails(SqlCommand cmd)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[10];
                paramList[0] = new SqlParameter("@P_PROFILE_ID", _profileId);
                paramList[1] = new SqlParameter("@P_VISACOUNTRY", _visaCountry);
                paramList[2] = new SqlParameter("@P_VISA_NO", _visaNumber);
                if (_type == "V")
                {
                    paramList[3] = new SqlParameter("@P_VISA_ISSUE_DATE", _visaIssueDate);
                }
                paramList[4] = new SqlParameter("@P_VISA_EXP_DATE", _visaExpDate);
                paramList[5] = new SqlParameter("@P_STATUS", _status);
                paramList[6] = new SqlParameter("@P_TYPE", _type);
                paramList[7] = new SqlParameter("@P_CREATED_BY", _createdBy);
                paramList[8] = new SqlParameter("@P_VISA_ID", _visaId);
                paramList[9] = new SqlParameter("@P_visaplaceof_issue", _visaplaceofissue);
                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdate_Corp_Profile_VisaDetails", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets all the Visa Details List from the table Corp_Profile_Visa_Details for the supplied profile Id.
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public List<CorpProfileVisaDetails> Load(long profileId)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<CorpProfileVisaDetails> detailsList = new List<CorpProfileVisaDetails>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetCorpProfileVisaDetails", paramList, connection);
                using (DataTable dtProfile = DBGateway.FillDataTableSP("usp_GetCorpProfileVisaDetails", paramList))
                {
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    if (dtProfile != null && dtProfile.Rows.Count > 0)
                    {
                        foreach (DataRow data in dtProfile.Rows)
                        {
                            CorpProfileVisaDetails visaDetails = new CorpProfileVisaDetails();
                            if (data["VisaId"] != DBNull.Value)
                            {
                                visaDetails._visaId = Convert.ToInt32(data["VisaId"]);
                            }
                            if (data["ProfileId"] != DBNull.Value)
                            {
                                visaDetails._profileId = Convert.ToInt32(data["ProfileId"]);
                            }
                            if (data["VisaCountry"] != DBNull.Value)
                            {
                                visaDetails._visaCountry = Convert.ToString(data["VisaCountry"]);
                            }
                            if (data["VisaNo"] != DBNull.Value)
                            {
                                visaDetails._visaNumber = Convert.ToString(data["VisaNo"]);
                            }
                            if (data["VisaIssueDate"] != DBNull.Value)
                            {
                                visaDetails._visaIssueDate = Convert.ToDateTime(data["VisaIssueDate"], dateFormat);
                            }
                            if (data["VisaExpDate"] != DBNull.Value)
                            {
                                visaDetails._visaExpDate = Convert.ToDateTime(data["VisaExpDate"], dateFormat);
                            }
                            if (data["type"] != DBNull.Value)
                            {
                                visaDetails._type = Convert.ToString(data["type"]);
                            }
                            if (data["CreatedBy"] != DBNull.Value)
                            {
                                visaDetails._createdBy = Convert.ToInt32(data["CreatedBy"]);
                            }
                            if (data["PlaceOfIssue"] != DBNull.Value)
                            {
                                visaDetails._visaplaceofissue = Convert.ToString(data["PlaceOfIssue"]);
                            }
                            detailsList.Add(visaDetails);
                        }
                    }
                }
                //data.Close();
                //connection.Close();
                return detailsList;
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }



    }
    public class CropProfileContactDetails
    {
        #region membersList
        int _contId;
        int _profileId;
        string _street;
        string _city;
        string _state;
        string _country;
        string _pincode;
        string _phone;
        string _ccmail;
        bool _emailnotification;
        string _type;
        int _createdBy;
        #endregion
        #region Properties

        public int ContId
        {
            get { return _contId; }
            set { _contId = value; }
        }
        public int ProfileId
        {
            get { return _profileId; }
            set { _profileId = value; }
        }
        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string Pincode
        {
            get { return _pincode; }
            set { _pincode = value; }
        }
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        public string Ccmail
        {
            get { return _ccmail; }
            set { _ccmail = value; }
        }
        public bool Emailnotification
        {
            get { return _emailnotification; }
            set { _emailnotification = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion
        public CropProfileContactDetails()
        {

        }
        public void SaveCorpProfileContactDetails(SqlCommand cmd)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@P_PROFILE_ID", _profileId);
                paramList[1] = new SqlParameter("@P_STREET", _street);
                paramList[2] = new SqlParameter("@P_CITY", _city);
                paramList[3] = new SqlParameter("@P_COUNTRY", _country);
                paramList[4] = new SqlParameter("@P_STATE", _state);
                paramList[5] = new SqlParameter("@P_PINCODE", _pincode);
                paramList[6] = new SqlParameter("@P_TYPE", _type);
                paramList[7] = new SqlParameter("@P_PHONE", _phone);
                paramList[8] = new SqlParameter("@P_EMAIL_NOTIFICATION", _emailnotification);
                paramList[9] = new SqlParameter("@P_CC_EMAIL", _ccmail);
                paramList[10] = new SqlParameter("@P_CREATED_BY", _createdBy);
                paramList[10] = new SqlParameter("@P_CONT_ID", _contId);
                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdate_Corp_Profile_ContactDetails", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CropProfileContactDetails> Load(long profileId)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<CropProfileContactDetails> detailsList = new List<CropProfileContactDetails>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetCorpProfileVisaDetails", paramList, connection);
                using (DataTable dtProfile = DBGateway.FillDataTableSP("usp_GetCorpProfileContantDetails", paramList))
                {
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    if (dtProfile != null && dtProfile.Rows.Count > 0)
                    {
                        foreach (DataRow data in dtProfile.Rows)
                        {
                            CropProfileContactDetails ContDetails = new CropProfileContactDetails();
                            if (data["contId"] != DBNull.Value)
                            {
                                ContDetails._contId = Convert.ToInt32(data["contId"]);
                            }
                            if (data["ProfileId"] != DBNull.Value)
                            {
                                ContDetails._profileId = Convert.ToInt32(data["ProfileId"]);
                            }
                            if (data["Country"] != DBNull.Value)
                            {
                                ContDetails._country = Convert.ToString(data["Country"]);
                            }
                            if (data["street"] != DBNull.Value)
                            {
                                ContDetails._street = Convert.ToString(data["street"]);
                            }
                            if (data["City"] != DBNull.Value)
                            {
                                ContDetails._city = Convert.ToString(data["City"]);
                            }
                            if (data["State"] != DBNull.Value)
                            {
                                ContDetails._state = Convert.ToString(data["State"]);
                            }
                            if (data["Type"] != DBNull.Value)
                            {
                                ContDetails._type = Convert.ToString(data["Type"]);
                            }
                            if (data["CreatedBy"] != DBNull.Value)
                            {
                                ContDetails._createdBy = Convert.ToInt32(data["CreatedBy"]);
                            }
                            if (data["Phone"] != DBNull.Value)
                            {
                                ContDetails._phone = Convert.ToString(data["Phone"]);
                            }
                            if (data["CCEmails"] != DBNull.Value)
                            {
                                ContDetails._ccmail = Convert.ToString(data["CCEmails"]);
                            }
                            if (data["Pincode"] != DBNull.Value)
                            {
                                ContDetails._pincode = Convert.ToString(data["Pincode"]);
                            }
                            if (data["EmailNotification"] != DBNull.Value)
                            {
                                ContDetails._emailnotification = Convert.ToBoolean(data["EmailNotification"]);
                            }
                            detailsList.Add(ContDetails);
                        }
                    }
                }
                //data.Close();
                //connection.Close();
                return detailsList;
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }
    }
    public class CropProfileGDSSettings
    {
        #region membersList
        int _settingsId;
        int _profileId;
        string _gds;
        string _description;
        string _ownerPCC;
        int _queueNo;
        string _extraCommand;
        string _corporateSSR;
        string _osi;
        string _remraks;
        string _type;
        int _createdBy;
        #endregion
        #region Properties

        public int SettingsId
        {
            get { return _settingsId; }
            set { _settingsId = value; }
        }
        public int ProfileId
        {
            get { return _profileId; }
            set { _profileId = value; }
        }
        public string GDS
        {
            get { return _gds; }
            set { _gds = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string OwnerPCC
        {
            get { return _ownerPCC; }
            set { _ownerPCC = value; }
        }
        public int QueueNo
        {
            get { return _queueNo; }
            set { _queueNo = value; }
        }
        public string ExtraCommand
        {
            get { return _extraCommand; }
            set { _extraCommand = value; }
        }
        public string CorporateSSR
        {
            get { return _corporateSSR; }
            set { _corporateSSR = value; }
        }
        public string OSI
        {
            get { return _osi; }
            set { _osi = value; }
        }
        public string Remraks
        {
            get { return _remraks; }
            set { _remraks = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion
        public CropProfileGDSSettings()
        {

        }
        public void SaveCorpProfileGDSSettings(SqlCommand cmd)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[12];
                paramList[0] = new SqlParameter("@P_PROFILE_ID", _profileId);
                paramList[1] = new SqlParameter("@P_GDS", _gds);
                paramList[2] = new SqlParameter("@P_Description", _description);
                paramList[3] = new SqlParameter("@P_OwnerPCC", _ownerPCC);
                paramList[4] = new SqlParameter("@P_QueueNo", _queueNo);
                paramList[5] = new SqlParameter("@P_extraCommand", _extraCommand);
                paramList[6] = new SqlParameter("@P_corporateSSR", _corporateSSR);
                paramList[7] = new SqlParameter("@P_OSI", _osi);
                paramList[8] = new SqlParameter("@P_Remraks", _remraks);
                paramList[9] = new SqlParameter("@P_SettingsId", _settingsId);
                paramList[10] = new SqlParameter("@P_CREATED_BY", _createdBy);
                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddUpdate_Corp_Profile_GDS_Settings", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CropProfileGDSSettings> Load(long profileId)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            try
            {
                List<CropProfileGDSSettings> detailsList = new List<CropProfileGDSSettings>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ProfileId", profileId);
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetCorpProfileVisaDetails", paramList, connection);
                using (DataTable dtProfile = DBGateway.FillDataTableSP("usp_GetCorpProfileGDSSettings", paramList))
                {
                    if (dtProfile != null && dtProfile.Rows.Count > 0)
                    {
                        foreach (DataRow data in dtProfile.Rows)
                        {
                            CropProfileGDSSettings GDSSettings = new CropProfileGDSSettings();
                            if (data["settingsId"] != DBNull.Value)
                            {
                                GDSSettings._settingsId = Convert.ToInt32(data["settingsId"]);
                            }
                            if (data["ProfileId"] != DBNull.Value)
                            {
                                GDSSettings._profileId = Convert.ToInt32(data["ProfileId"]);
                            }
                            if (data["GDS"] != DBNull.Value)
                            {
                                GDSSettings._gds = Convert.ToString(data["GDS"]);
                            }
                            if (data["Description"] != DBNull.Value)
                            {
                                GDSSettings._description = Convert.ToString(data["Description"]);
                            }
                            if (data["OwnerPCC"] != DBNull.Value)
                            {
                                GDSSettings._ownerPCC = Convert.ToString(data["OwnerPCC"]);
                            }
                            if (data["QueueNo"] != DBNull.Value)
                            {
                                GDSSettings._queueNo = Convert.ToInt32(data["QueueNo"]);
                            }
                            if (data["extraCommand"] != DBNull.Value)
                            {
                                GDSSettings._extraCommand = Convert.ToString(data["extraCommand"]);
                            }
                            if (data["corporateSSR"] != DBNull.Value)
                            {
                                GDSSettings._corporateSSR = Convert.ToString(data["corporateSSR"]);
                            }
                            if (data["OSI"] != DBNull.Value)
                            {
                                GDSSettings._osi = Convert.ToString(data["OSI"]);
                            }
                            if (data["Remraks"] != DBNull.Value)
                            {
                                GDSSettings._remraks = Convert.ToString(data["Remraks"]);
                            }
                            detailsList.Add(GDSSettings);
                        }
                    }
                }
                //data.Close();
                //connection.Close();
                return detailsList;
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

       
    }
    public class CorpCostCenter
    {
        public long map_id { get; set; }
        public long profileid { get; set; }
        public int costcenterid { get; set; }
        public int productid { get; set; }
        public bool status { get; set; }
        public int createdby { get; set; }
        public DateTime createdon { get; set; }
        public int modifiedby { get; set; }
        public DateTime modifiedon { get; set; }

       
        public static List<CorpCostCenter> Load(long profileid)
        {
            List<CorpCostCenter>  corpCostCenters = new List<CorpCostCenter>();
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@profileid", profileid));
                DataTable dt = DBGateway.FillDataTableSP("USP_GetCorpCostCenter", paramList.ToArray());
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CorpCostCenter corpCostCenter = new CorpCostCenter();
                        corpCostCenter.map_id = Convert.ToInt64(dr["map_id"]);
                        corpCostCenter.profileid = Convert.ToInt64(dr["profileid"]);
                        corpCostCenter.costcenterid = Convert.ToInt32(dr["costcenterid"]);
                        corpCostCenter.productid = Convert.ToInt32(dr["productid"]);
                        corpCostCenter.status = Convert.ToBoolean(dr["status"]);
                        corpCostCenters.Add(corpCostCenter);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Corp Cost Centers failed to . Error:" + ex.ToString(), "");
                throw ex;
            }
            return corpCostCenters;
        }
        public long Save()
        {
            int res = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@profileid", profileid));
                paramList.Add(new SqlParameter("@costcenterid", costcenterid));
                paramList.Add(new SqlParameter("@productid", productid));
                paramList.Add(new SqlParameter("@status", status));
                paramList.Add(new SqlParameter("@createdby", createdby));
                paramList.Add(new SqlParameter("@createdon", DateTime.Now));
                res = DBGateway.ExecuteNonQuery("USP_ADDCorpCostCenter", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Corp Cost Centers failed to . Error:" + ex.ToString(), "");
                throw ex;
            }
           return res;
        }
    }
}
