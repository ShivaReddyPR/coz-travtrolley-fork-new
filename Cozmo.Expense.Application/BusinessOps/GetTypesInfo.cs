﻿using Cozmo.Api.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cozmo.Expense.Application.Models;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class GetTypesInfo
    {
        /// <summary>
        /// Query request to save the type master data
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplRicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo AgentInfo { get; set; }
            public ExpTypes TypesInfo { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="SaveTypes"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    List<ExpTypes> tInfo = new List<ExpTypes>();
                    tInfo = ExpTypes.GetTypesInfo(clsQuery.TypesInfo.ET_AgentId);

                    var newdata = tInfo;
                    var viewModel = new ViewModel(newdata);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsQuery.AgentInfo.LoginUserId,
                        "Expense Api: Failed to GetTypesInfo. Error: " + ex.ToString(), clsQuery.AgentInfo.IPAddress);
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>        
            public ViewModel(List<ExpTypes> data)
            {
                Data = data;
            }
            public List<ExpTypes> Data { get; }
        }
    }
}
