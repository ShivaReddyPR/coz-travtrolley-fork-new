﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class RemoveExpDtlData
    {
        /// <summary>
        /// Query request to delete expense details
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo AgentInfo { get; set; }

            public List<int> ExpDtlIds { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="RemoveExpDtlData"/>
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    if (clsQuery.ExpDtlIds != null && clsQuery.ExpDtlIds.Count > 0)
                        ExpReportData.Delete(clsQuery.ExpDtlIds, clsQuery.AgentInfo.LoginUserId);

                    var viewModel = new ViewModel("Success");
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsQuery.AgentInfo.LoginUserId,
                        "Expense Api: Failed to RemoveExpDtlData. Error: " + ex.ToString(), clsQuery.AgentInfo.IPAddress);
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(string data)
            {
                Data = data;
            }
            public string Data { get; }
        }
    }
}
