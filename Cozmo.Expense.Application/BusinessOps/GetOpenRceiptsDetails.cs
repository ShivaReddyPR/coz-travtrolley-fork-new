﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class GetOpenRceiptsDetails
    {
        /// <summary>
        /// Query request to get available flight/hotel trips info to link expense report
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo AgentInfo { get; set; }

            public int ProfileId { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="GetOpenRceiptsDetails"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    var liReceipts = ExpReceipts.GetOpenReceipts(clsQuery.ProfileId, string.Empty);
                    var liReceiptsData = ExpReceipts.GetOpenReceipts(clsQuery.ProfileId, "ALL");
                    
                    var viewModel = new ViewModel(liReceipts, liReceiptsData);
                    return await Task.FromResult(new ApplicationResponse(viewModel));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsQuery.AgentInfo.LoginUserId,
                        "Expense Api: Failed to GetOpenRceiptsDetails. Error: " + ex.ToString(), clsQuery.AgentInfo.IPAddress);
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(List<ExpReceipts> liReceipts, List<ExpReceipts> liReceiptsData)
            {
                dtOpenReceipts = liReceipts;
                dtOpenReceiptsDtls = liReceiptsData;
            }

            public List<ExpReceipts> dtOpenReceipts { get; }

            public List<ExpReceipts> dtOpenReceiptsDtls { get; }
        }
    }
}

