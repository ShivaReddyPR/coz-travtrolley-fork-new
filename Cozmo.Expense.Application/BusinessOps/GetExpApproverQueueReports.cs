﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using CT.TicketReceipt.BusinessLayer;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class GetExpApproverQueueReports
    {
        /// <summary>
        /// Query request to get available flight/hotel trips info to link expense report
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo AgentInfo { get; set; }
            public ExpApproverQueue approverQueue { get; set; }
            public List<ExpApprovalReports> approverReports { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="GetExpenseReceiptdto"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    if (clsQuery.approverReports != null && clsQuery.approverReports.Count > 0)
                    {
                        foreach (var report in clsQuery.approverReports)
                        {
                            ExpApproverQueue.ApprovalReports(report, string.Empty);
                        }
                    }
                    var viewModel = new ViewModel(clsQuery.AgentInfo, clsQuery.approverQueue);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(ApiAgentInfo apiAgentInfo, ExpApproverQueue queue)
            {               
                //Data = queue.LoadExpenseApprovalReports();
            }
            public System.Data.DataTable Data { get; }
        }
    }
}


