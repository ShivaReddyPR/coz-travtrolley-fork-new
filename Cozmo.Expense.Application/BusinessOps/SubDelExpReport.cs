﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class SubDelExpReport
    {
        /// <summary>
        /// Query request to get expense create screen load data
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo AgentInfo { get; set; }

            public int ExpRepId { get; set; }

            public int ProfileId { get; set; }

            public string Action { get; set; }

            public string Host { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="SubDelExpReport"/>
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    TransactionOptions options = new TransactionOptions();
                    options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                    options.Timeout = new TimeSpan(0, 1, 0);//1 minute
                    using (TransactionScope expenseSubmit = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                    {
                        if (clsQuery.Action == "S")
                            ExpApproverQueue.ApprovalReports(new ExpApprovalReports { ApprovalStatus = "P", CreatedBy = clsQuery.AgentInfo.LoginUserId.ToString(), expApproverId = "0", ExpDetailId = "0", ExpRepId = clsQuery.ExpRepId, ProfileID = clsQuery.ProfileId.ToString() }, clsQuery.Host);
                        else
                        {
                            var expdetails = ExpReportData.GetExpRepData(clsQuery.ExpRepId);
                            var expIds = expdetails.Where(x => x != null).Select(x => x.ED_Id).ToList();
                            ExpReportData.Delete(expIds, clsQuery.AgentInfo.LoginUserId);
                        }

                        expenseSubmit.Complete();
                    }

                    var viewModel = new ViewModel("Success");
                    return await Task.FromResult(new ApplicationResponse(viewModel));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsQuery.AgentInfo.LoginUserId,
                        "Expense Api: Failed to SubDelExpReport. Error: " + ex.ToString(), clsQuery.AgentInfo.IPAddress);
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(string data)
            {
                Data = data;
            }

            public string Data { get; set; }
        }
    }
}
