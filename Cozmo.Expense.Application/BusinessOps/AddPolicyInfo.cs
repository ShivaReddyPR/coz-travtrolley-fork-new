﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CT.TicketReceipt.BusinessLayer;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class AddPolicyInfo
    {
        /// <summary>
        /// Query request to Add Attendee Master info
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {

            public ApiAgentInfo AgentInfo { get; set; }
            public ExpPolicyMaster PolicyInfo { get; set; }
            public ExpPolicyMaster[] DelPolicyInfo { get; set; }
            public string Status { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="AddPolicyInfo"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    if (clsQuery.Status == "D")
                    {
                        foreach (ExpPolicyMaster delpolicyinfo in clsQuery.DelPolicyInfo)
                        {
                            delpolicyinfo.Save();
                        }
                    }
                    else
                    {
                        ExpPolicyMaster policyinfo = (ExpPolicyMaster)clsQuery.PolicyInfo;
                        policyinfo.Save();
                    }
                    var newdata = new ExpPolicyMaster();
                    var viewModel = new ViewModel(newdata);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));

                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, "Save,Err:" + ex.Message, "");
                    return new ApplicationResponse().AddError("PolicySaveException", ex.ToString());
                }
            }
        }

            /// <summary>
            /// A viewmodel to return to the application
            /// </summary>
            public class ViewModel
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="ViewModel"/> class.
                /// </summary>
                public ViewModel(ExpPolicyMaster PMInfo)
                {
                    Data = PMInfo;
                }
                public ExpPolicyMaster Data { get; }
            }
    }
}
