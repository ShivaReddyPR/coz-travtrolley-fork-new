﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.BusinessLayer;
using CT.Corporate;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class GetTypesScreenLoadData
    {
        /// <summary>
        /// Query request to get screen data
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {

            public ApiAgentInfo AgentInfo { get; set; }

        }

        /// <summary>
        /// Validation for the <see cref="GetTypesScreenLoadData"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    DataTable dtAgents = AgentMaster.GetList(1, string.Empty, clsQuery.AgentInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                    DataTable dtCostCenter = CorporateProfileSetup.GetList(clsQuery.AgentInfo.AgentId, "CS", ListStatus.Short);
                    DataTable dtCategories = ExpCategories.GetList(clsQuery.AgentInfo.AgentId);
                    DataTable dtUOM = CorporateProfileSetup.GetList(clsQuery.AgentInfo.AgentId, "UM", ListStatus.Short);
                    var viewModel = new ViewModel(dtAgents, dtCostCenter, dtCategories, dtUOM);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsQuery.AgentInfo.LoginUserId,
                        "Expense Api: Failed to GetTypesScreenDataException. Error: " + ex.ToString(), clsQuery.AgentInfo.IPAddress);
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(DataTable dtAgents,DataTable dtCostCenter, DataTable dtCategories, DataTable dtUOM )
            {

                Data = new GetTypesScreenLoadDataDto();
                Data.dtAgents = dtAgents;
                Data.dtCostCenter = dtCostCenter;
                Data.dtCategories = dtCategories;
                Data.dtUOM = dtUOM;
            }
            public GetTypesScreenLoadDataDto Data { get; }
        }
    }
}
