﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class SaveExpDtlData
    {
        /// <summary>
        /// Query request to get expense create screen load data
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo AgentInfo { get; set; }

            public ExpReportData expReportData { get; set; }

            public List<ExpAttendeeMaster> expAttendeeMasters { get; set; }            

            public List<ExpCompanyVendor> expNewCompanies { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="SaveExpDtlData"/>
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    TransactionOptions options = new TransactionOptions();
                    options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                    options.Timeout = new TimeSpan(0, 1, 0);//1 minute
                    using (TransactionScope expenseSave = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                    {
                        if (clsQuery.expNewCompanies != null && clsQuery.expNewCompanies.Count > 0 && clsQuery.expNewCompanies != null && clsQuery.expNewCompanies.Count > 0)
                        {
                            clsQuery.expNewCompanies.ForEach(x => {

                                var iCmpId = x.CV_ID;
                                x.CV_ID = 0;
                                x.Save("INSERT");

                                clsQuery.expAttendeeMasters.Where(c => c.EAM_CV_Id == iCmpId).ToList().ForEach(y => {

                                    y.EAM_CV_Id = x.CV_ID;
                                });
                            });
                        }

                        if (clsQuery.expAttendeeMasters != null && clsQuery.expAttendeeMasters.Count > 0)
                        {
                            clsQuery.expAttendeeMasters.ForEach(x => {

                                var pair = clsQuery.expReportData.expAttendees.Select((Value, Index) => new { Value, Index }).Single(p => p.Value.EA_EAM_Id == x.EAM_ID);
                                x.EAM_ID = x.EAM_ID <= 0 ? 0 : x.EAM_ID;
                                x.Save();
                                clsQuery.expReportData.expAttendees[pair.Index].EA_EAM_Id = x.EAM_ID;
                            });
                        }

                        clsQuery.expReportData.Save();
                        expenseSave.Complete();
                    }

                    var viewModel = new ViewModel("Success", clsQuery.expReportData.expReceipts);
                    return await Task.FromResult(new ApplicationResponse(viewModel));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsQuery.AgentInfo.LoginUserId,
                        "Expense Api: Failed to SaveExpDtlData. Error: " + ex.ToString(), clsQuery.AgentInfo.IPAddress);
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(string data, List<ExpReceipts> liReceipts)
            {
                Data = data;
                expReceipts = liReceipts != null ? liReceipts.Where(x => x.RC_Mode == "S").ToList() : liReceipts;
            }

            public string Data { get; set; }

            public List<ExpReceipts> expReceipts { get; set; }
        }
    }
}
