﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.BusinessOps
{
    public class GetExpApproverQueue
    {
        /// <summary>
        /// Query request to get available Expense Approval Reports
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo AgentInfo { get; set; }
            public ExpApproverQueue approverQueue { get; set; }
            public List<int> SelExpenses { get; set; }
            public string type { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="GetExpApproverQueue"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    if (clsQuery.type == "QD" && clsQuery.SelExpenses != null && clsQuery.SelExpenses.Count > 0)
                    {
                        clsQuery.SelExpenses.ForEach(x => {

                            ExpReimbursement clsRM = new ExpReimbursement();
                            clsRM.ERM_ED_ID = x;
                            clsRM.ERM_ClaimAmount = x;
                            clsRM.Mode = "P";
                            clsRM.Save();
                        });
                    }
                    var viewModel = new ViewModel(clsQuery.AgentInfo, clsQuery.approverQueue, clsQuery.type);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(ApiAgentInfo apiAgentInfo, ExpApproverQueue queue, string type)
            {
                Data = new ExpApprovalQueuedto();                
                Data.dtProfiles = type == "AQ" ? CorporateProfile.GetCorpProfilesListByApproverType(apiAgentInfo.LoginUserCorpId, "E") :
                    ExpensesUtility.GetExpProfileList(apiAgentInfo.LoginUserCorpId, apiAgentInfo.AgentId, ListStatus.Short);
                Data.dtcostCenters = ExpensesUtility.GetExpCostCenters(apiAgentInfo.LoginUserCorpId);
                Data.dtPaymentType = ExpApproverQueue.LoadPaymentTypes(apiAgentInfo.AgentId);
                Data.dtApprovalReceipts = queue.LoadExpenseApprovalReports(type, apiAgentInfo.LoginUserCorpId);
            }
            public ExpApprovalQueuedto Data { get; }
        }
    }
}


