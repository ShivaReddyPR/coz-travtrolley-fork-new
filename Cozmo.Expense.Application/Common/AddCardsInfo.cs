﻿using Cozmo.Api.Application.Models;
using Cozmo.Expense.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CT.TicketReceipt.BusinessLayer;

namespace Cozmo.Expense.Application.Common
{
    public class AddCardsInfo
    {
        /// <summary>
        /// Query request to Add credit cards info
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {

            public ApiAgentInfo AgentInfo { get; set; }
            public ApiCardInfo CardInfo { get; set; }
            public ApiCardInfo[] DelCardInfo { get; set; }
            public string  flag { get; set; }

        }

        /// <summary>
        /// Validation for the <see cref="AddCardsInfo"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    
                    if (clsQuery.flag == "ADD") 
                        clsQuery.CardInfo.Save(clsQuery.flag);                        
                    else
                    {                        
                        foreach (ApiCardInfo delCinfo in clsQuery.DelCardInfo)
                        {
                            ApiCardInfo.Delete(delCinfo.CM_ID, delCinfo.CM_Status, delCinfo.CM_AgentId, delCinfo.CM_CreatedBy, clsQuery.flag);
                        }
                    }
                    var newdata = new ApiCardInfo();
                    var viewModel = new ViewModel(newdata);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));

                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsQuery.AgentInfo.LoginUserId,
                        "Expense Api: Failed to AddCardsInfo. Error: " + ex.ToString(), clsQuery.AgentInfo.IPAddress);
                    return new ApplicationResponse().AddError("ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            //public ViewModel(string status)
            //{
            //    Status = status;
            //}
            //public string Status { get; }

            public ViewModel(ApiCardInfo CMInfo)
            {
                Data = CMInfo;
            }
            public ApiCardInfo Data { get; }
        }
    }
}
