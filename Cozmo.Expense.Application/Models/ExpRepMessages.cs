﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Cozmo.Expense.Application.Models
{
    public class ExpRepMessages
    {
        public int ERM_Id { get; set; }
        public int ERM_ED_Id { get; set; }
        public string ERM_Message { get; set; }
        public bool ERM_Status { get; set; }
        public int ERM_CreatedBy { get; set; }
        public DateTime ERM_CreatedDate { get; set; }
        public int ERM_ModifiedBy { get; set; }
        public DateTime ERM_ModifiedDate { get; set; }

        /// <summary>
        /// To save expoense warning messages
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ERM_Id", ERM_Id));
                if (ERM_Id == 0)
                    liParamsList[0].Direction = ParameterDirection.InputOutput;                
                liParamsList.Add(new SqlParameter("@ERM_ED_Id", ERM_ED_Id));
                liParamsList.Add(new SqlParameter("@ERM_Message", ERM_Message));
                liParamsList.Add(new SqlParameter("@ERM_Status", ERM_Status));
                liParamsList.Add(new SqlParameter("@ERM_CreatedBy", ERM_CreatedBy));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.SaveExpRepMessage, liParamsList.ToArray());
                ERM_Id = ERM_Id == 0 ? (int)liParamsList[0].Value : ERM_Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense messages data
        /// </summary>
        /// <param name="iExpDtlId"></param>
        /// <returns></returns>
        public static List<ExpRepMessages> GetExpMessages(int iExpDtlId)
        {
            List<ExpRepMessages> liExpRepMessages = new List<ExpRepMessages>();
            try
            {
                if (iExpDtlId == 0)
                    return liExpRepMessages;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ERM_ED_Id", iExpDtlId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpRepMessages, liParamsList.ToArray());
                liExpRepMessages = GenericStatic.ConvertToList<ExpRepMessages>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liExpRepMessages;
        }
    }
}
