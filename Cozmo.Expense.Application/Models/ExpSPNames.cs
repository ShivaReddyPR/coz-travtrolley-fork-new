﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.Models
{
    internal static class ExpSPNames
    {
        public const string GetAvailableTrips = "usp_GetAvailableTrips";
        public const string SaveExpenseReport = "usp_SaveExpenseReport";
        public const string GetExpenseReport = "usp_GetExpenseReport";
        public const string SaveExpenseReportTrips = "usp_SaveExpenseReportTrips";
        public const string GetExpenseReportTrips = "usp_GetExpenseReportTrips";
        public const string UpdateTripExpStatus = "usp_UpdateTripExpStatus";

        public const string SaveExpReportData = "usp_SaveExpReportData";
        public const string SaveExpReportFlex = "usp_SaveExpReportFlex";
        public const string SaveExpAttendees = "usp_SaveExpAttendees";
        public const string DelExpReportData = "usp_DelExpReportData";
        public const string GetExpCreateData = "usp_GetExpCreateData";
        public const string GetExpTypeFieldsData = "usp_GetExpTypeFieldsData";
        public const string GetExpReportFlex = "usp_GetExpReportFlex";
        public const string GetExpAttendees = "usp_GetExpAttendees";
        public const string GetExpRepData = "usp_GetExpRepData";
        public const string GetExpAffidavit = "usp_GetExpAffidavit";

        public const string SaveAttendeeMaster = "usp_SaveAttendeeMaster";
        public const string GetAttendeeMaster = "usp_GetAttendeeMaster";
        public const string GetCmpAttendees = "usp_GetCmpAttendees";
        public const string GetAttendeesList = "usp_GetAttendeesList";

        public const string GetOpenReceipts = "usp_GetOpenReceipts";
        public const string SaveExpReceipts = "usp_SaveExpReceipts";
        public const string GetOpenExpenses = "usp_GetOpenExpenses"; 
        public const string GetExpReceipts = "usp_GetExpReceipts";

        public const string SaveExpRepMessage = "usp_SaveExpRepMessage";
        public const string GetExpRepMessages = "usp_GetExpRepMessages";

        public const string GetExpApprovalReports = "USP_GetExpApprovalReports"; 
        public const string GetExpApprovalDetails = "USP_GetExpApprovalDetails"; 
        public const string SaveApproverDetails = "USP_SaveApproverDetails"; 
        public const string GetPaymentTypes = "USP_GETEXPPAYMENTTYPES"; 
        public const string GetExpReportDetails = "USP_GetExpReportDetails";
        public const string GetExpApprovalData = "usp_GetExpApprovalData";
        public const string GetReimburseReport = "usp_GetReimburseReport";

        public const string GetExpenseCityMaster = "GetExpenseCityMaster";
    }
}
