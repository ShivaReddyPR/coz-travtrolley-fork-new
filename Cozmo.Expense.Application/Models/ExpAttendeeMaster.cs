﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cozmo.Expense.Application.Models
{
    public class ExpAttendeeMaster
    {
        public int EAM_ID { get; set; }
        public int EAM_AgentId { get; set; }
        public string EAM_Title { get; set; }
        public string EAM_Name { get; set; }
        public string EAM_Type { get; set; }
        public int EAM_CV_Id { get; set; }
        public bool EAM_Status { get; set; }
        public int EAM_CreatedBy { get; set; }
        public DateTime EAM_CreatedDate { get; set; }
        public int EAM_ModifiedBy { get; set; }
        public DateTime EAM_ModifiedDate { get; set; }
        public string EAM_LastName { get; set; }
        public string EAM_ComanyName { get; set; }

        /// <summary>
        /// To save attendee master data
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@EAM_ID", EAM_ID));
                if (EAM_ID == 0)
                    liParamsList[0].Direction = ParameterDirection.InputOutput;
                liParamsList.Add(new SqlParameter("@EAM_AgentId", EAM_AgentId));
                liParamsList.Add(new SqlParameter("@EAM_Title", EAM_Title));
                liParamsList.Add(new SqlParameter("@EAM_Name", EAM_Name));
                liParamsList.Add(DBGateway.GetParameter("EAM_Type", EAM_Type));
                liParamsList.Add(new SqlParameter("@EAM_CV_Id", EAM_CV_Id));
                liParamsList.Add(new SqlParameter("@EAM_Status", EAM_Status));
                liParamsList.Add(new SqlParameter("@EAM_CreatedBy", EAM_CreatedBy));
                liParamsList.Add(DBGateway.GetParameter("EAM_LastName", EAM_LastName));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.SaveAttendeeMaster, liParamsList.ToArray());
                EAM_ID = EAM_ID == 0 ? (int)liParamsList[0].Value : EAM_ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get attendee master data
        /// </summary>
        /// <param name="iExpDtlId"></param>
        /// <returns></returns>
        public static ExpAttendeeMaster GetAttendeeMaster(int ExpAttendeeId)
        {
            ExpAttendeeMaster clsExpAttendeeMaster = new ExpAttendeeMaster();
            try
            {
                if (ExpAttendeeId == 0)
                    return clsExpAttendeeMaster;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@EAM_ID", ExpAttendeeId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetAttendeeMaster, liParamsList.ToArray());
                clsExpAttendeeMaster = GenericStatic.ConvertToObj<ExpAttendeeMaster>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clsExpAttendeeMaster;
        }

        /// <summary>
        /// To get expense attendees data
        /// </summary>
        /// <param name="iExpDtlId"></param>
        /// <returns></returns>
        public static List<ExpAttendeeMaster> GetCmpAttendees(int iCmpId)
        {
            List<ExpAttendeeMaster> liExpAttendees = new List<ExpAttendeeMaster>();
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@EAM_CV_Id", iCmpId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetCmpAttendees, liParamsList.ToArray());
                liExpAttendees = GenericStatic.ConvertToList<ExpAttendeeMaster>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liExpAttendees;
        }

        /// <summary>
        /// To get expense attendees data
        /// </summary>
        /// <param name="AgentId"></param>
        /// <returns></returns>
        public static List<ExpAttendeeMaster> GetAttendees(int AgentId)
        {
            List<ExpAttendeeMaster> liExpAttendees = new List<ExpAttendeeMaster>();
            try
            {
                if (AgentId == 0)
                    return liExpAttendees;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@EAM_AgentId", AgentId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetAttendeesList, liParamsList.ToArray());
                liExpAttendees = GenericStatic.ConvertToList<ExpAttendeeMaster>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liExpAttendees;
        }

        /// <summary>
        /// To get company list
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static DataTable GetcompanyList(int agentId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@eAM_AgentId", agentId);
                return DBGateway.FillDataTableSP("usp_GetCompanyList", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
