﻿using Cozmo.Api.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;



namespace Cozmo.Expense.Application.Models
{
    public class ApiCardInfo
    {
        /// <summary>
        ///  AgentId
        /// </summary>
        public int CM_AgentId { get; set; }
        /// <summary>
        ///  Card Master name
        /// </summary>
        public string CM_Name { get; set; }
        /// <summary>
        ///  Cost Center
        /// </summary>
        public int CM_CostCenter { get; set; }
        /// <summary>
        ///  Card Type
        /// </summary>
        public string CM_Type { get; set; }
        /// <summary>
        ///  Card Number
        /// </summary>
        public string CM_Number { get; set; }
        /// <summary>
        ///  Card Expiry
        /// </summary>
        public string CM_Expiry { get; set; }
        /// <summary>
        ///  Card CVV
        /// </summary>
        public string CM_CVV { get; set; }
        /// <summary>
        ///  Card Status
        /// </summary>
        public bool CM_Status { get; set; }
        /// <summary>
        ///  Created By
        /// </summary>
        public int CM_CreatedBy { get; set; }
        /// <summary>
        ///  Created date
        /// </summary>
        public DateTime CM_CreatedDate { get; set; }
        /// <summary>
        ///  Modified By
        /// </summary>
        public int CM_ModifiedBy { get; set; }
        /// <summary>
        ///  Modified Date
        /// </summary>
        public DateTime CM_ModifiedDate { get; set; }
        /// <summary>
        ///  Table Unique Id
        /// </summary>
        public int CM_ID { get; set; }
        /// <summary>
        ///  Cost Center Text
        /// </summary>
        public string CM_CostCenterName { get; set; }
        /// <summary>
        ///  Agent Name
        /// </summary>
        public string CM_AgentName { get; set; }

		//To Add/update the data in cardmaster table
        public int Save(string flag)
        {
            int rowsAffected = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[12];
                paramList[0] = new SqlParameter("@Flag", flag);
                paramList[1] = new SqlParameter("@cm_AgentId", CM_AgentId);
                paramList[2] = new SqlParameter("@cm_Name", CM_Name);
                paramList[3] = new SqlParameter("@cm_CostCenter", CM_CostCenter);
                paramList[4] = new SqlParameter("@cm_Type", CM_Type);
                paramList[5] = new SqlParameter("@cm_Number", CM_Number);
                if (!string.IsNullOrEmpty(CM_Expiry))
                {
                    paramList[6] = new SqlParameter("@cm_Expiry", CM_Expiry);
                }
                else
                {
                    paramList[6] = new SqlParameter("@cm_Expiry", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(CM_CVV))
                {
                    paramList[7] = new SqlParameter("@cm_CVV", CM_CVV);
                }
                else
                {
                    paramList[7] = new SqlParameter("@cm_CVV", DBNull.Value);
                }
                paramList[8] = new SqlParameter("@cm_Status", CM_Status);
                paramList[9] = new SqlParameter("@cm_Createdby", CM_CreatedBy);
                paramList[10] = new SqlParameter("@cm_Modifiedby", CM_ModifiedBy);
                paramList[11] = new SqlParameter("@cm_ID", CM_ID);
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_CT_M_EXP_CARDMASTER", paramList);
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.ExpenseApi, Severity.High, (int)Settings.LoginInfo.UserID, "Save,Err:" + ex.Message, "");
                throw ex;
            }
            return rowsAffected;
        }

		//To update the card master table status to inactive
        public static int Delete(int CM_ID, bool CM_Status, int CM_AgentId, int CM_CreatedBy, string flag)
        {
            int rowsAffected = 0;
            try
            {

                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@Flag", flag);
                paramList[1] = new SqlParameter("@cm_ID", CM_ID);
                paramList[2] = new SqlParameter("@cm_Status", CM_Status);
                paramList[3] = new SqlParameter("@cm_Modifiedby", CM_CreatedBy);
                paramList[4] = new SqlParameter("@cm_AgentId", CM_AgentId);
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_CT_M_EXP_CARDMASTER", paramList);
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.ExpenseApi, Severity.High, (int)Settings.LoginInfo.UserID, "Delete,Err:" + ex.Message, "");
                throw ex;
            }
            return rowsAffected;
        }

		//To get card master data
        public static List<ApiCardInfo> GetBookingData(int AgentId)
        {
            List<ApiCardInfo> cinfoList = new List<ApiCardInfo>();

            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@cm_AgentId", AgentId);

            DataTable dtcinfo = DBGateway.ExecuteQuery("usp_GET_CT_M_EXP_CARDMASTER", paramList).Tables[0];
            cinfoList = GenericStatic.ConvertToList<ApiCardInfo>(dtcinfo);

            return cinfoList;
        }

    }
}
