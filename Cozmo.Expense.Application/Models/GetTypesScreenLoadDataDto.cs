﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Cozmo.Expense.Application.Models
{
   public class GetTypesScreenLoadDataDto
    {
        public DataTable dtAgents { get; set; }
        public DataTable dtCostCenter { get; set; }
        public DataTable dtCategories { get; set; }        
        public DataTable dtUOM { get; set; }
    }
}
