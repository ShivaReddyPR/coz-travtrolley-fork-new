﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.Models
{
    public class ExpUploadReceiptdto
    {
        public DataTable dtProfiles { get; set; }
        public List<ExpReceipts> receipts { get; set; }
        public DataTable dtExpenses { get; set; }
    }
   
}
