﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;

namespace Cozmo.Expense.Application.Models
{
    public class PaymentType
    {
        public int PT_ID { get; set; }
        public int PT_AgentId { get; set; }
        public string PT_Code { get; set; }
        public string PT_Desc { get; set; }        
        public int PT_CostCenter { get; set; }
        public string PT_GLAccount { get; set; }
        public string PT_GLType { get; set; }
        public bool PT_Status { get; set; }
        public int PT_CreatedBy { get; set; }
        public DateTime PT_CreatedDate { get; set; }
        public int PT_ModifiedBy { get; set; }
        public DateTime PT_ModifiedDate { get; set; }
        //Screen
        public int PTS_ID { get; set; }
        public int PTS_PT_Id { get; set; }
        public string PTS_Screen { get; set; }              
        //ExpenseType
        public int PTE_ID { get; set; }
        public int PTE_PT_Id { get; set; }
        public string PTE_ET_Id { get; set; }

        public int UserId { get; set; }

        /// <summary>
        /// To save the paymenttypes info 
        /// </summary>
        /// <param name="ptInfo"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public int Save(string flag)
        {
            int rows = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@Flag", flag));
                paramList.Add(new SqlParameter("@PT_ID", PT_ID));
                paramList.Add(new SqlParameter("@PT_AgentId", PT_AgentId));
                paramList.Add(new SqlParameter("@PT_CostCenter", PT_CostCenter));
                paramList.Add(new SqlParameter("@PT_Code", PT_Code));
                paramList.Add(new SqlParameter("@PT_Desc", PT_Desc));
                paramList.Add(new SqlParameter("@PT_GLAccount", PT_GLAccount));
                paramList.Add(new SqlParameter("@PT_GLType", PT_GLType));
                paramList.Add(new SqlParameter("@PT_Status", PT_Status));
                paramList.Add(new SqlParameter("@PT_CreatedBy", PT_CreatedBy));
                paramList.Add(new SqlParameter("@PT_ModifiedBy", PT_ModifiedBy));
                paramList.Add(new SqlParameter("@PTS_Screen", PTS_Screen));
                paramList.Add(new SqlParameter("@PTE_ET_Id", PTE_ET_Id));
                rows = DBGateway.ExecuteNonQuerySP("usp_Add_Update_CT_M_PAYMENTTYPES", paramList.ToArray());               
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Insert/Update PaymentTypes Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return rows;
        }

        /// <summary>
        /// To update the status as 0 in data base  
        /// </summary>
        /// <param name="PT_ID"></param>
        /// <param name="PT_Status"></param>
        /// <param name="PT_AgentId"></param>
        /// <param name="PT_ModifiedBy"></param>
        /// <param name="flag"></param>
        /// <returns></returns>    
        public int Delete(int PT_ID, bool PT_Status, int PT_AgentId, int PT_ModifiedBy, string flag)
        {
            int result = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@Flag", flag));
                paramList.Add(new SqlParameter("@PT_ID", PT_ID));
                paramList.Add(new SqlParameter("@PT_AgentId", PT_AgentId));
                paramList.Add(new SqlParameter("@PT_Status", PT_Status));
                paramList.Add(new SqlParameter("@PT_Modifiedby", PT_ModifiedBy));
                result = DBGateway.ExecuteNonQuerySP("usp_Add_Update_CT_M_PAYMENTTYPES", paramList.ToArray());
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to update status PaymentTypes Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// To get screenList information selected User
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static DataTable GetScreenList(PaymentType s)
        {            
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_USER_ID", s.UserId);
                return DBGateway.FillDataTableSP("usp_GET_ScreenList", paramList);
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Get ScreenNames Info. Reason: " + ex.ToString(), "");
                throw ex;
            }          
        }

        /// <summary>
        /// To get payment type information for selected agent
        /// </summary>
        /// <param name="ptInfo"></param>
        /// <returns></returns>
        public static List<PaymentType> GetPaymentTypesInfo(int iAgentId)
        {
            List<PaymentType> ptList = new List<PaymentType>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@PT_AgentId", iAgentId);
                DataTable dtptinfo = DBGateway.ExecuteQuery("usp_GET_CT_M_PAYMENTTYPES", paramList).Tables[0];
                ptList = GenericStatic.ConvertToList<PaymentType>(dtptinfo);
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to GetPaymentTypes Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return ptList;
        }

    }
}
