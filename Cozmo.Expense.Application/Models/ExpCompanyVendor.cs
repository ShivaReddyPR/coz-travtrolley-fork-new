﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.BookingEngine;

namespace Cozmo.Expense.Application.Models
{
    public class ExpCompanyVendor
    {
        public int CV_ID { get; set; }
        public int CV_AgentId { get; set; }
        public string CV_Code { get; set; }
        public string CV_Name { get; set; }
        public string CV_Type { get; set; }
        public bool CV_Status { get; set; }
        public int CV_CreatedBy { get; set; }
        public DateTime CV_CreatedDate { get; set; }
        public int CV_ModifiedBy { get; set; }
        public DateTime CV_ModifiedDate { get; set; }        
        public string CV_AgentName { get; set; }

        /// <summary>
        /// To save the company vendor info 
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        public int Save(string flag)
        {
            int rows = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@CV_ID", CV_ID));
                if (CV_ID <= 0)
                    paramList[0].Direction = ParameterDirection.InputOutput;
                paramList.Add(new SqlParameter("@flag", flag));
                paramList.Add(new SqlParameter("@CV_AgentId", CV_AgentId));
                paramList.Add(new SqlParameter("@CV_Code", CV_Code));
                paramList.Add(new SqlParameter("@CV_Type", CV_Type));
                paramList.Add(new SqlParameter("@CV_Name", CV_Name));
                paramList.Add(new SqlParameter("@CV_Status", CV_Status));
                paramList.Add(new SqlParameter("@CV_CreatedBy", CV_CreatedBy));
                paramList.Add(new SqlParameter("@CV_ModifiedBy", CV_ModifiedBy));               
                rows = DBGateway.ExecuteNonQuerySP("Usp_CT_T_EXP_COMPANYVENDOR", paramList.ToArray());
                CV_ID = CV_ID <= 0 ? (int)paramList[0].Value : CV_ID;
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Insert/Update CompanyVendor Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return rows;
        }

        /// <summary>
        /// To update the status as 0 in data base
        /// </summary>
        /// <param name="CV_ID"></param>
        /// <param name="CV_Status"></param>
        /// <param name="CV_AgentId"></param>
        /// <param name="CV_ModifiedBy"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public int Delete(int CV_ID, bool CV_Status, int CV_AgentId, int CV_ModifiedBy, string flag)
        {
            int result = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@Flag", flag));
                paramList.Add(new SqlParameter("@CV_ID", CV_ID));
                paramList.Add(new SqlParameter("@CV_AgentId", CV_AgentId));
                paramList.Add(new SqlParameter("@CV_Status", CV_Status));
                paramList.Add(new SqlParameter("@CV_Modifiedby", CV_ModifiedBy));
                result = DBGateway.ExecuteNonQuerySP("Usp_CT_T_EXP_COMPANYVENDOR", paramList.ToArray());                
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Delete CVInfo. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// To get comapny vendor information selected agent
        /// </summary>
        /// <param name="vendorInfo"></param>
        /// <returns></returns>
        public static List<ExpCompanyVendor> GetVendorInfo(int iAgentId)
        {
            List<ExpCompanyVendor> cvList = new List<ExpCompanyVendor>();
            try
            {                
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@CV_AgentId", iAgentId);
                DataTable dtcinfo = DBGateway.ExecuteQuery("usp_GET_CT_T_EXP_COMPANYVENDOR", paramList).Tables[0];
                cvList = GenericStatic.ConvertToList<ExpCompanyVendor>(dtcinfo);                
            }
            catch(Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to GetCompanyVendorInfo. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return cvList;
        }
    }
}
