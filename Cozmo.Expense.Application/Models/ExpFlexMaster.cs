﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;

namespace Cozmo.Expense.Application.Models
{
    public class ExpFlexMaster
    {
        public int EF_ID { get; set; }
        public int EF_ET_Id { get; set; }
        public string EF_Label { get; set; }
        public string EF_Control { get; set; }
        public string EF_DataType { get; set; }
        public string EF_SQLQuery { get; set; }
        public string EF_Mandatory { get; set; }
        public int EF_Order { get; set; }
        public int EF_OrderDependId { get; set; }
        public int EF_DisplayDependId { get; set; }
        public string EF_IsDisable { get; set; }
    	public int EF_FieldWidth { get; set; }
	    public string EF_IsNewRow { get; set; }
	    public string EF_CssClass { get; set; }
        public string EF_Type { get; set; }
        public string EF_GroupFlag { get; set; }
        public string EF_HideField { get; set; }
        public bool EF_Status { get; set; }
        public int EF_CreatedBy { get; set; }
        public DateTime EF_CreatedDate { get; set; }
        public int EF_ModifiedBy { get; set; }
        public DateTime EF_ModifiedDate { get; set; }
        public string EF_OrderLabel { get; set; }
        public string EF_DisplayLabel { get; set; }

        /// <summary>
        /// To save the flex master info 
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        public int Save(string flag)
        {
            int rows = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@flag", flag));
                paramList.Add(new SqlParameter("@EF_ET_Id", EF_ET_Id));
                paramList.Add(new SqlParameter("@EF_Label", EF_Label));
                paramList.Add(new SqlParameter("@EF_Control", EF_Control));
                paramList.Add(new SqlParameter("@EF_DataType", EF_DataType));
                paramList.Add(new SqlParameter("@EF_SQLQuery", EF_SQLQuery));
                paramList.Add(new SqlParameter("@EF_Mandatory", EF_Mandatory));
                paramList.Add(new SqlParameter("@EF_Order", EF_Order));               
                paramList.Add(new SqlParameter("@EF_DisplayDependId", EF_DisplayDependId));
                paramList.Add(new SqlParameter("@EF_OrderDependId", EF_OrderDependId));
                paramList.Add(new SqlParameter("@EF_IsDisable", EF_IsDisable));
                paramList.Add(new SqlParameter("@EF_FieldWidth", EF_FieldWidth));
                paramList.Add(new SqlParameter("@EF_IsNewRow", EF_IsNewRow));
                paramList.Add(new SqlParameter("@EF_CssClass", EF_CssClass));
                paramList.Add(new SqlParameter("@EF_Type", EF_Type));
                paramList.Add(new SqlParameter("@EF_GroupFlag", EF_GroupFlag));
                paramList.Add(new SqlParameter("@EF_HideField", EF_HideField));
                paramList.Add(new SqlParameter("@EF_Status", EF_Status));
                paramList.Add(new SqlParameter("@EF_CreatedBy", EF_CreatedBy));
                paramList.Add(new SqlParameter("@EF_ModifiedBy", EF_ModifiedBy));                
                paramList.Add(new SqlParameter("@EF_ID", EF_ID));
                rows = DBGateway.ExecuteNonQuerySP("usp_Add_Update_CT_M_EXP_FLEXMASTER", paramList.ToArray());
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Insert/Update Flex Master Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return rows;
        }

        /// <summary>
        /// To update the status as 0 in data base  
        /// </summary>
        /// <param name="EF_ID"></param>
        /// <param name="EF_Status"></param>        
        /// <param name="EF_ModifiedBy"></param>
        /// <param name="flag"></param>
        /// <returns></returns>    
        public int Delete(int EF_ID, bool EF_Status, int EF_ModifiedBy, string flag)
        {
            int result = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@Flag", flag));
                paramList.Add(new SqlParameter("@EF_ID", EF_ID));               
                paramList.Add(new SqlParameter("@EF_Status", EF_Status));
                paramList.Add(new SqlParameter("@EF_Modifiedby", EF_ModifiedBy));
                result = DBGateway.ExecuteNonQuerySP("usp_Add_Update_CT_M_EXP_FLEXMASTER", paramList.ToArray());
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Delete Flex Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// To get flex information selected type
        /// </summary>
        /// <param name="fInfo"></param>
        /// <returns></returns>
        public static List<ExpFlexMaster> GetFlexInfo(int iExpType)
        {
            List<ExpFlexMaster> tList = new List<ExpFlexMaster>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@EF_ET_Id", iExpType);
                DataTable dttinfo = DBGateway.ExecuteQuery("usp_GET_CT_M_EXP_FLEXMASTER", paramList).Tables[0];
                tList = GenericStatic.ConvertToList<ExpFlexMaster>(dttinfo);
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to GetFlex Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return tList;
        }

        /// <summary>
        /// To get OrderDepend field list
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static DataTable GetOrderDependId(int agentId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@AGENTID", agentId);
                return DBGateway.ExecuteQuery("Usp_GetOrderDependId", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get OrderDepend field. Reason: " + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// To get DisplayDependField list
        /// </summary>
        /// <param name="iExpType"></param>
        /// <param name="flexType"></param>
        /// <returns></returns>
        public static DataTable GetDisplayDepend(int iExpType, string flexType)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@EF_Type", flexType);
                paramList[1] = new SqlParameter("@EF_ET_Id", iExpType);
                return DBGateway.ExecuteQuery("Usp_GetDisplayDependField", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get DisplayDepend Field. Reason: " + ex.ToString(), "");
                throw ex;
            }
        }

    }
}
