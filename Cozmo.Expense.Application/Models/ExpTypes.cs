﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;

namespace Cozmo.Expense.Application.Models
{
  public class ExpTypes
    {
        public int ET_ID { get; set; }
        public int ET_AgentId { get; set; }
        public string ET_Code { get; set; }
        public string ET_Desc { get; set; }
        public int ET_EC_ID { get; set; }
        public string ET_UOM { get; set; }
        public int ET_CostCenter { get; set; }
        public string ET_GLAccount { get; set; }
        public string ET_EnableItemize { get; set; }
        public string ET_EnableAttendee { get; set; }
        public string ET_EnableTaxCalc { get; set; }
        public bool ET_Status { get; set; }
        public int ET_CreatedBy { get; set; }
        public DateTime ET_CreatedDate { get; set; }
        public int ET_ModifiedBy { get; set; }
        public DateTime ET_ModifiedDate { get; set; }
        public string ET_AgentName { get; set; }
        public string ET_ReceiptNotReq { get; set; }

        /// <summary>
        /// To save the types info 
        /// </summary>
        /// <param name="tInfo"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public int Save(string flag)
        {
            int rows = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@flag", flag));
                paramList.Add(new SqlParameter("@ET_AgentId", ET_AgentId));
                paramList.Add(new SqlParameter("@ET_Code", ET_Code));
                paramList.Add(new SqlParameter("@ET_Desc", ET_Desc));
                paramList.Add(new SqlParameter("@ET_UOM", ET_UOM));
                paramList.Add(new SqlParameter("@ET_CostCenter", ET_CostCenter));
                paramList.Add(new SqlParameter("@ET_GLAccount", ET_GLAccount));
                paramList.Add(new SqlParameter("@ET_EnableItemize", ET_EnableItemize));
                paramList.Add(new SqlParameter("@ET_EnableTaxCalc", ET_EnableTaxCalc));
                paramList.Add(new SqlParameter("@ET_EnableAttendee", ET_EnableAttendee));
                paramList.Add(new SqlParameter("@ET_Status", ET_Status));
                paramList.Add(new SqlParameter("@ET_CreatedBy", ET_CreatedBy));
                paramList.Add(new SqlParameter("@ET_ModifiedBy", ET_ModifiedBy));
                paramList.Add(new SqlParameter("@ET_EC_ID", ET_EC_ID));
                paramList.Add(new SqlParameter("@ET_ID", ET_ID));
                paramList.Add(new SqlParameter("@ET_ReceiptNotReq", ET_ReceiptNotReq));
                rows = DBGateway.ExecuteNonQuerySP("usp_Add_Update_CT_M_EXP_TYPES", paramList.ToArray());
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Insert/Update expense Types Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return rows;
        }

        /// <summary>
        /// To update the status as 0 in data base  
        /// </summary>
        /// <param name="ET_ID"></param>
        /// <param name="ET_Status"></param>
        /// <param name="ET_AgentId"></param>
        /// <param name="ET_ModifiedBy"></param>
        /// <param name="flag"></param>
        /// <returns></returns>    
        public int Delete(int ET_ID, bool ET_Status, int ET_AgentId, int ET_ModifiedBy, string flag)
        {
            int result = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@Flag", flag));
                paramList.Add(new SqlParameter("@ET_ID", ET_ID));
                paramList.Add(new SqlParameter("@ET_AgentId", ET_AgentId));
                paramList.Add(new SqlParameter("@ET_Status", ET_Status));
                paramList.Add(new SqlParameter("@ET_Modifiedby", ET_ModifiedBy));                
                result = DBGateway.ExecuteNonQuerySP("usp_Add_Update_CT_M_EXP_TYPES", paramList.ToArray());
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Delete expense Types Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// To get types information selected agent
        /// </summary>
        /// <param name="tInfo"></param>
        /// <returns></returns>
        public static List<ExpTypes> GetTypesInfo(int iAgentId)
        {
            List<ExpTypes> tList = new List<ExpTypes>();
            try
            {                
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ET_AgentId", iAgentId);
                DataTable dttinfo = DBGateway.ExecuteQuery("usp_GET_CT_M_EXP_TYPES", paramList).Tables[0];
                tList = GenericStatic.ConvertToList<ExpTypes>(dttinfo);                
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Get expenseTypes Info. Reason: " + ex.ToString(), "");               
            }
            return tList;
        }

        /// <summary>
        /// To get types dropdown list
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static DataTable GetTypesList(int agentId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ET_AgentId", agentId);
                return DBGateway.ExecuteQuery("usp_GET_CT_M_EXP_TYPES", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get expenseTypes dropdown. Reason: " + ex.ToString(), "");
                throw ex;
            }
        }
    }
}
