﻿using System.Data;

namespace Cozmo.Expense.Application.Models
{
    public class GetExpReportScreenDataDto
    {
        public ExpenseReport expenseReport { get; set; }

        public DataTable dtProfiles { get; set; }
    }
}
