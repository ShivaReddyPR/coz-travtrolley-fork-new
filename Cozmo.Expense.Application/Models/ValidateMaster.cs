﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.Models
{
    public class ValidateMaster
    {
        public int VL_ID { get; set; }
        public string VL_KEY { get; set; }
        public string VL_QUERY { get; set; }
        public bool VL_Status { get; set; }
        public int VL_CreatedBy { get; set; }
        public DateTime VL_CreatedDate { get; set; }
        public int VL_ModifiedBy { get; set; }
        public DateTime VL_ModifiedDate { get; set; }

        /// <summary>
        /// To validate record based on given pk values
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sPKValues"></param>
        /// <param name="iUser"></param>
        /// <returns></returns>
        public static DataTable GetDuplicate(string sKey, List<string> liPKValues, int iUser)
        {
            DataTable dt = new DataTable();
            try
            {
                if (string.IsNullOrEmpty(sKey))
                    return dt;

                string sPKValues = liPKValues == null || liPKValues.Count == 0 ? string.Empty : 
                    string.Join("|", liPKValues.Where(x => !string.IsNullOrEmpty(x)).Select(x => x.Replace("|", string.Empty)).ToArray()).Trim('|');

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@VALKEY", sKey));
                liParamsList.Add(new SqlParameter("@PKVALUES", sPKValues));
                liParamsList.Add(new SqlParameter("@User", iUser));
                dt = DBGateway.FillDataTableSP("usp_CheckExpDuplicates", liParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
