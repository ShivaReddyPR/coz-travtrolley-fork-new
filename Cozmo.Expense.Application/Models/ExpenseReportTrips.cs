﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cozmo.Expense.Application.Models
{
    [Serializable]
    public class ExpenseReportTrips
    {
        public int ERT_Id { get; set; }
        public int ERT_ER_Id { get; set; }
        public int ERT_ProductId { get; set; }
        public int ERT_REF_Id { get; set; }
        public bool ERT_Status { get; set; }
        public int ERT_CreatedBy { get; set; }
        public DateTime ERT_CreatedDate { get; set; }
        public int ERT_ModifiedBy { get; set; }
        public DateTime ERT_ModifiedDate { get; set; }

        /// <summary>
        /// To save expoense report trips data
        /// </summary>
        /// <returns></returns>
        public static void Save(List<ExpenseReportTrips> liExpRepTrips)
        {
            try
            {
                if (liExpRepTrips == null || liExpRepTrips.Count == 0 || liExpRepTrips[0].ERT_ER_Id == 0)
                    return;

                string sFlightIds = !liExpRepTrips[0].ERT_Status ? string.Empty : string.Join("|", liExpRepTrips.Where(x => x != null && x.ERT_ProductId == 1).Select(x => x.ERT_REF_Id.ToString()).ToArray()).Trim('|');
                string sHotelIds = !liExpRepTrips[0].ERT_Status ? string.Empty : string.Join("|", liExpRepTrips.Where(x => x != null && x.ERT_ProductId == 2).Select(x => x.ERT_REF_Id.ToString()).ToArray()).Trim('|');

                List<SqlParameter> liParamsList = new List<SqlParameter>();                
                liParamsList.Add(DBGateway.GetParameter("FlightIds", sFlightIds));
                liParamsList.Add(DBGateway.GetParameter("HotelIds", sHotelIds));
                liParamsList.Add(new SqlParameter("@ERT_ER_ID", liExpRepTrips[0].ERT_ER_Id));
                liParamsList.Add(new SqlParameter("@ERT_CreatedBy", liExpRepTrips[0].ERT_CreatedBy));
                liParamsList.Add(new SqlParameter("@ERT_Status", liExpRepTrips[0].ERT_Status));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.SaveExpenseReportTrips, liParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense report trips data
        /// </summary>
        /// <param name="iExpRepId"></param>
        /// <returns></returns>
        public static List<ExpenseReportTrips> GetExpReportTrips(int iExpRepId)
        {
            List<ExpenseReportTrips> liRepTrips = new List<ExpenseReportTrips>();
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ERT_ER_Id", iExpRepId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpenseReportTrips, liParamsList.ToArray());
                liRepTrips = GenericStatic.ConvertToList<ExpenseReportTrips>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liRepTrips;
        }

        /// <summary>
        /// To get available flight/hotel trips to include in expense report
        /// </summary>
        /// <param name="iAgentId"></param>
        /// <param name="iProfileId"></param>
        /// <param name="iExpRepId"></param>
        /// <param name="Mode"></param>
        /// <returns></returns>
        public static DataTable GetAvailableTrips(int iAgentId, int iProfileId, int iExpRepId, string Mode)
        {
            DataTable dt = new DataTable();
            try
            {
                if (iAgentId == 0 || iProfileId == 0)
                    return dt;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@AgentId", iAgentId));
                liParamsList.Add(new SqlParameter("@CorpProfile", iProfileId));
                liParamsList.Add(new SqlParameter("@ERT_ER_Id", iExpRepId));
                liParamsList.Add(new SqlParameter("@MODE", Mode));
                dt = DBGateway.FillDataTableSP(ExpSPNames.GetAvailableTrips, liParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        /// <summary>
        /// To update flight/hotel trip expense status
        /// </summary>
        /// <returns></returns>
        public static void UpdateTripExpStatus(List<ExpenseReportTrips> liExpRepTrips, string sStatus, int iUser)
        {
            try
            {
                if (liExpRepTrips == null || liExpRepTrips.Count == 0)
                    return;

                string sFlightIds = string.Join("|", liExpRepTrips.Where(x => x != null && x.ERT_ProductId == 1).Select(x => x.ERT_REF_Id.ToString()).ToArray()).Trim('|');
                string sHotelIds = string.Join("|", liExpRepTrips.Where(x => x != null && x.ERT_ProductId == 2).Select(x => x.ERT_REF_Id.ToString()).ToArray()).Trim('|');

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(DBGateway.GetParameter("HotelIds", sHotelIds));
                liParamsList.Add(DBGateway.GetParameter("FlightIds", sFlightIds));
                liParamsList.Add(new SqlParameter("@Status", sStatus));
                liParamsList.Add(new SqlParameter("@User", iUser));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.UpdateTripExpStatus, liParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
