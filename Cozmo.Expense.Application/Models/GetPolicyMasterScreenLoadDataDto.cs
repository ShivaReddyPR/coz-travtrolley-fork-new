﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Cozmo.Expense.Application.Models
{
    public class GetPolicyMasterScreenLoadDataDto
    {
        public DataTable dtAgents { get; set; }
        public DataTable dtExptypes { get; set; }
        public DataTable dtCity { get; set; }
        public DataTable dtPayType { get; set; }
        public DataTable dtCurrency { get; set; }
        public string ETUOM { get; set; }
        public DataTable dtCostCenter { get; set; }
    }
}
