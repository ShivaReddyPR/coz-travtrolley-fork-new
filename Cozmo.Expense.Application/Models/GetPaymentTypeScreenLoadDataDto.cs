﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Cozmo.Expense.Application.Models
{
   public class GetPaymentTypeScreenLoadDataDto
    {
        public DataTable dtAgents { get; set; }
        public DataTable dtCostCenter { get; set; }
        public DataTable dtTypes { get; set; }        
    }
}
