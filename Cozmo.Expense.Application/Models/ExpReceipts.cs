﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cozmo.Expense.Application.Models
{
    public class ExpReceipts
    {
        /// <summary>
        ///   Receipt ID
        /// </summary>
        public long RC_Id { get; set; }
        /// <summary>
        ///   Receipt Trip ID
        /// </summary>
        public long RC_ED_Id { get; set; }
        /// <summary>
        /// Reciept corp profile id
        /// </summary>
        public int RC_ProfileId { get; set; }
        /// <summary>
        ///   Receipt Name
        /// </summary>
        public string RC_Name { get; set; }
        /// <summary>
        /// Receipt File Name
        /// </summary>
        public string RC_FileName { get; set; }
        /// <summary>
        ///  Upload Receipt File path
        /// </summary>
        public string RC_Path { get; set; }       
        /// <summary>
        ///   Receipt Status
        /// </summary>
        public bool RC_Status { get; set; }
        /// <summary>
        ///   Receipt created user
        /// </summary>
        public int RC_CreatedBy { get; set; }
        /// <summary>
        ///   Receipt File created date
        /// </summary>
        public string RC_CreatedDate { get; set; }
        /// <summary>
        ///   Receipt  Modified User
        /// </summary>
        public int RC_ModifiedBy { get; set; }
        /// <summary>
        ///   Receipt File  Modified Date
        /// </summary>
        public string RC_ModifiedDate { get; set; }
        /// <summary>
        ///   To mention receipt action save/update/delete
        /// </summary>
        public string RC_Mode { get; set; }
        /// <summary>
        /// To get absolute file path updated in the DB
        /// </summary>
        private string absolutefilepath = string.Empty;

        public void Save()
        {
            try
            {
                List<SqlParameter> liParamslist = new List<SqlParameter>();
                liParamslist.Add(new SqlParameter("@RC_Id", RC_Id));
                if (RC_Id == 0)
                    liParamslist[0].Direction = ParameterDirection.Output;
                liParamslist.Add(DBGateway.GetParameter("ActualPath", absolutefilepath));
                liParamslist[1].Direction = ParameterDirection.Output;
                liParamslist[1].Size = 250;
                liParamslist.Add(new SqlParameter("@RC_ED_Id", RC_ED_Id));
                liParamslist.Add(new SqlParameter("@RC_ProfileId", RC_ProfileId));
                liParamslist.Add(new SqlParameter("@RC_Name", RC_Name));
                liParamslist.Add(new SqlParameter("@RC_FileName", RC_FileName));
                liParamslist.Add(new SqlParameter("@RC_Path", RC_Path));
                liParamslist.Add(new SqlParameter("@RC_Status", RC_Status));
                liParamslist.Add(new SqlParameter("@RC_CreatedBy", RC_CreatedBy));
                liParamslist.Add(new SqlParameter("@RC_Mode", RC_Mode));
                int res = DBGateway.ExecuteNonQuery(ExpSPNames.SaveExpReceipts, liParamslist.ToArray());
                RC_Id = RC_Mode == "S" ? Convert.ToInt32(liParamslist[0].Value) : RC_Id;
                RC_Path = RC_Mode == "S" ? Convert.ToString(liParamslist[1].Value) : RC_Mode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get open receipts for a profile
        /// </summary>
        /// <param name="iCorpProfile"></param>
        /// <returns></returns>
        public static  List<ExpReceipts> GetOpenReceipts(int iCorpProfile, string Mode)
        {
            List<ExpReceipts> liReceipts = new List<ExpReceipts>();

            if (iCorpProfile == 0)
                return liReceipts;
            
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@CorpProfile", iCorpProfile));
                liParamsList.Add(new SqlParameter("@Mode", Mode));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetOpenReceipts, liParamsList.ToArray());
                liReceipts = dt != null && dt.Rows.Count > 0 ? GenericStatic.ConvertToList<ExpReceipts>(dt) : liReceipts;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liReceipts;
        }

        /// <summary>
        /// To get expense related receipts
        /// </summary>
        /// <param name="iCorpProfile"></param>
        /// <returns></returns>
        public static List<ExpReceipts> GetExpReceipts(int iExpDtlId)
        {
            List<ExpReceipts> liReceipts = new List<ExpReceipts>();

            if (iExpDtlId == 0)
                return liReceipts;

            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@RC_ED_Id", iExpDtlId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpReceipts, liParamsList.ToArray());
                liReceipts = dt != null && dt.Rows.Count > 0 ? GenericStatic.ConvertToList<ExpReceipts>(dt) : liReceipts;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liReceipts;
        }

        /// <summary>
        /// To get open expenses list for receipt linking
        /// </summary>
        /// <param name="iCorpProfile"></param>
        /// <returns></returns>
        public static DataTable GetOpenExpenses(int iCorpProfile)
        {
            DataTable dtExpenses = new DataTable();

            if (iCorpProfile == 0)
                return dtExpenses;

            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@CorpProfile", iCorpProfile));
                dtExpenses = DBGateway.FillDataTableSP(ExpSPNames.GetOpenExpenses, liParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtExpenses;
        }
    }
}
