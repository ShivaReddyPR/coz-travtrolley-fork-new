﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;

namespace Cozmo.Expense.Application.Models
{
    public class ExpCategories
    {
        public int EC_ID { get; set; }
        public int EC_AgentId { get; set; }
        public string EC_Code { get; set; }
        public string EC_Desc { get; set; }
        public int EC_Order { get; set; }
        public int EC_CostCenter { get; set; }
        public string EC_GLAccount { get; set; }
        public bool EC_Status { get; set; }
        public int EC_CreatedBy { get; set; }
        public DateTime EC_CreatedDate { get; set; }
        public int EC_ModifiedBy { get; set; }
        public DateTime EC_ModifiedDate { get; set; }
        public string EC_AgentName { get; set; }

        /// <summary>
        /// To save the categories info 
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns> 
        public int Save(string flag)
        {
            int rows = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@flag", flag));
                paramList.Add(new SqlParameter("@EC_AgentId", EC_AgentId));
                paramList.Add(new SqlParameter("@EC_Code", EC_Code));
                paramList.Add(new SqlParameter("@EC_Desc", EC_Desc));
                paramList.Add(new SqlParameter("@EC_Order", EC_Order));
                paramList.Add(new SqlParameter("@EC_CostCenter", EC_CostCenter));
                paramList.Add(new SqlParameter("@EC_GLAccount", EC_GLAccount));
                paramList.Add(new SqlParameter("@EC_Status", EC_Status));
                paramList.Add(new SqlParameter("@EC_CreatedBy", EC_CreatedBy));
                paramList.Add(new SqlParameter("@EC_ModifiedBy", EC_ModifiedBy));
                paramList.Add(new SqlParameter("@EC_ID", EC_ID));
                rows = DBGateway.ExecuteNonQuerySP("usp_CT_M_EXP_CATEGORIES", paramList.ToArray());
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Insert/Update Categories Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return rows;
        }

        /// <summary>
        /// To update the status as 0 in data base
        /// </summary>
        /// <param name="EC_ID"></param>
        /// <param name="EC_Status"></param>
        /// <param name="EC_AgentId"></param>
        /// <param name="EC_Modifiedby"></param>
        /// <param name="flag"></param>
        /// <returns></returns>      
        public int Delete(int EC_ID, bool EC_Status,int EC_AgentId, int EC_Modifiedby, string flag)
        {            
            int result = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@Flag", flag));
                paramList.Add(new SqlParameter("@EC_ID", EC_ID));
                paramList.Add(new SqlParameter("@EC_AgentId", EC_AgentId));
                paramList.Add(new SqlParameter("@EC_Status", EC_Status));
                paramList.Add(new SqlParameter("@EC_Modifiedby", EC_Modifiedby));            
                result = DBGateway.ExecuteNonQuerySP("usp_CT_M_EXP_CATEGORIES", paramList.ToArray());
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Delete Categories Info. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// To get categories information for selected agent
        /// </summary>
        /// <param name="cInfo"></param>
        /// <returns></returns>
        public static List<ExpCategories> GetCategoriesInfo(int iAgentId)
        {
            List<ExpCategories> cList = new List<ExpCategories>();
            try {               
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@EC_AgentId", iAgentId);
                DataTable dtcinfo = DBGateway.ExecuteQuery("usp_GET_CT_M_EXP_CATEGORIES", paramList).Tables[0];
                cList = GenericStatic.ConvertToList<ExpCategories>(dtcinfo);               
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to GetCategories Info. Reason: " + ex.ToString(), "");
                throw ex;   
            }
            return cList;
        }

        /// <summary>
        /// To get categories dropdown list
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static DataTable GetList(int agentId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@EC_AgentId", agentId);
                return DBGateway.ExecuteQuery("usp_GET_CT_M_EXP_CATEGORIES", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get Categories dropdown. Reason: " + ex.ToString(), "");
                throw ex;
            }
        }
    }
}
