﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Cozmo.Expense.Application.Models
{
    public class GetCompanyVendorScreenLoadDataDto
    {
        public DataTable dtAgents { get; set; }
    }
}
