﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using System.Globalization;

namespace Cozmo.Expense.Application.Models
{
    public class ExpExchangeMaster
    {
        public int EM_ID { get; set; }
        public int EM_AgentId { get; set; }
        public string EM_BaseCurrency { get; set; }
        public string EM_Currency { get; set; }
        public string EM_Rate { get; set; }
        public string EM_Remarks { get; set; }
        public string EM_Date { get; set; }
        public bool EM_Status { get; set; }
        public int EM_CreatedBy { get; set; }
        public DateTime EM_CreatedDate { get; set; }
        public int EM_ModifiedBy { get; set; }
        public DateTime EM_ModifiedDate { get; set; }

        /// <summary>
        /// To save expense Exchange master data
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@EM_ID", EM_ID));
                if (EM_ID == 0)
                    paramList[0].Direction = ParameterDirection.InputOutput;
                paramList.Add(new SqlParameter("@EM_AgentId", EM_AgentId));
                paramList.Add(new SqlParameter("@EM_BaseCurrency", EM_BaseCurrency));
                paramList.Add(new SqlParameter("@EM_Currency", EM_Currency));
                paramList.Add(new SqlParameter("@EM_Rate", EM_Rate));
                if (string.IsNullOrWhiteSpace(EM_Date))
                {
                    EM_Date = string.Empty;
                }
                else
                {
                    paramList.Add(new SqlParameter("@EM_Date", DateTime.ParseExact(EM_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture)));
                }
                paramList.Add(new SqlParameter("@EM_Remarks", EM_Remarks));
                paramList.Add(new SqlParameter("@EM_Status", EM_Status));
                paramList.Add(new SqlParameter("@EM_CreatedBy", EM_CreatedBy));
                int rows = DBGateway.ExecuteNonQuerySP("usp_SaveExpExchangeData", paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense exchange data
        /// </summary>
        /// <param name="AgentId"></param>
        /// <returns></returns>
        public static List<ExpExchangeMaster> GetExpExchange(int AgentId)
        {
            List<ExpExchangeMaster> cList = new List<ExpExchangeMaster>();
            try
            {
                if (AgentId == 0)
                    return cList;

                List<SqlParameter> paramsList = new List<SqlParameter>();
                paramsList.Add(new SqlParameter("@EM_AgentId", AgentId));
                DataTable dt = DBGateway.FillDataTableSP("usp_GetExpExchangeData", paramsList.ToArray());
                cList = GenericStatic.ConvertToList<ExpExchangeMaster>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cList;
        }
    }
}
