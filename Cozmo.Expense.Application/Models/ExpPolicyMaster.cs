﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;


namespace Cozmo.Expense.Application.Models
{
    public class ExpPolicyMaster
    {
        public int EPM_Id { get; set; }
        public int EPM_ET_Id { get; set; }
        public string EPM_Name { get; set; }
        public int EPM_ECT_Id { get; set; }
        public decimal EPM_Units { get; set; }
        public string EPM_UOM { get; set; }
        public string EPM_Currency { get; set; }
        public int EPM_PT_Id { get; set; }
        public string EPM_AllowFutureDate { get; set; }
        public int EPM_NoOfAttendees { get; set; }
        public int EPM_CostCentre { get; set; }
        public decimal EPM_FixedAmount { get; set; }
        public decimal EPM_WarningAmount { get; set; }
        public decimal EPM_BlockAmount { get; set; }
        public string EPM_WarningMsg { get; set; }
        public bool EPM_Status { get; set; }
        public int EPM_CreatedBy { get; set; }
        public DateTime EPM_CreatedDate { get; set; }
        public int EPM_ModifiedBy { get; set; }
        public DateTime EPM_ModifiedDate { get; set; }
        public string EPM_TypeName { get; set; }
        public string EPM_CityName { get; set; }
        public string EPM_PayType { get; set; }
        public string EPM_CostCenterName { get; set; }


        /// <summary>
        /// To save policy master data
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ePM_ID", EPM_Id));
                liParamsList[0].Direction = ParameterDirection.InputOutput;
                liParamsList.Add(new SqlParameter("@ePM_ET_ID", EPM_ET_Id));
                liParamsList.Add(new SqlParameter("@ePM_Name", EPM_Name));
                liParamsList.Add(new SqlParameter("@ePM_ECT_ID", EPM_ECT_Id));
                liParamsList.Add(new SqlParameter("@ePM_Units", EPM_Units));
                liParamsList.Add(new SqlParameter("@ePM_UOM", EPM_UOM));
                liParamsList.Add(new SqlParameter("@ePM_CURRENCY", EPM_Currency));
                liParamsList.Add(new SqlParameter("@ePM_PT_Id", EPM_PT_Id));
                liParamsList.Add(new SqlParameter("@ePM_AllowFutureDate", EPM_AllowFutureDate));
                liParamsList.Add(new SqlParameter("@ePM_NoOfAttendees", EPM_NoOfAttendees));
                liParamsList.Add(new SqlParameter("@ePM_CostCentre", EPM_CostCentre));
                liParamsList.Add(new SqlParameter("@ePM_FixedAmount", EPM_FixedAmount));
                liParamsList.Add(new SqlParameter("@ePM_WarningAmount", EPM_WarningAmount));
                liParamsList.Add(new SqlParameter("@ePM_BlockAmount", EPM_BlockAmount));
                liParamsList.Add(new SqlParameter("@ePM_WarningMsg", EPM_WarningMsg));
                liParamsList.Add(new SqlParameter("@ePM_Status", EPM_Status));
                liParamsList.Add(new SqlParameter("@ePM_CreatedBy", EPM_CreatedBy));
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_SavePolicyMaster", liParamsList.ToArray());
                EPM_Id = EPM_Id == 0 ? (int)liParamsList[0].Value : EPM_Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense policy master data
        /// </summary>
        /// <param name="TypeId"></param>
        /// <returns></returns>
        public static List<ExpPolicyMaster> GetPolicyInfo(int TypeId)
        {
            List<ExpPolicyMaster> liExpPolicy = new List<ExpPolicyMaster>();
            try
            {
                if (TypeId == 0)
                    return liExpPolicy;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ePM_ET_Id", TypeId));
                DataTable dt = DBGateway.FillDataTableSP("usp_GetPolicyMaster", liParamsList.ToArray());
                liExpPolicy = GenericStatic.ConvertToList<ExpPolicyMaster>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liExpPolicy;
        }

        /// <summary>
        /// To get city list
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static DataTable GetExpCityList(int agentId, int iUserId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@AGENTID", agentId);
                paramList[1] = new SqlParameter("@User", iUserId);
                return DBGateway.FillDataTableSP(ExpSPNames.GetExpenseCityMaster, paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get screenwise pay type list
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static DataTable GetScreenwisePayTypeList(string screen)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@screen", screen);
                return DBGateway.FillDataTableSP("usp_GetExpScreenwisePayTypeList", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get exp type pay type list
        /// </summary>
        /// <param name="ET_ID"></param>
        /// <returns></returns>
        public static DataTable GetExptypePayTypeList(int ET_ID)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@eT_ID", ET_ID);
                return DBGateway.FillDataTableSP("usp_GetExpTypePayTypeList", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get cost center list
        /// </summary>
        /// <param name="ET_ID"></param>
        /// <returns></returns>
        public static DataTable GetCostCenterList(int ET_ID)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@eT_ID", ET_ID);
                return DBGateway.FillDataTableSP("usp_GetCostCenterbyExpType", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get UOM Data
        /// </summary>
        /// <param name="ET_ID"></param>
        /// <returns></returns>
        public static string GetUOMData(int ET_ID)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@eT_ID", ET_ID);
                paramList[1] = new SqlParameter("@eT_UOM", SqlDbType.VarChar, 10);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.FillDataTableSP("usp_GetUOMbyExpType", paramList);
                return Convert.ToString(paramList[1].Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// To get CURRENCY Data
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public static DataTable GetCurrencyList()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                return DBGateway.FillDataTableSP("usp_GetExpCurrencyList", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// To get city wise CURRENCY Data
        /// </summary>
        /// <param name="ECT_ID"></param>
        /// <returns></returns>
        public static string GetCitywiseCurrencyList(int ECT_ID)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@ecT_ID", ECT_ID);
                paramList[1] = new SqlParameter("@ecT_CURRENCY", SqlDbType.VarChar, 5);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.FillDataTableSP("usp_GetExpCitywiseCurrencyList", paramList);
                return Convert.ToString(paramList[1].Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
