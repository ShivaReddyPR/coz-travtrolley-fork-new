﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.Models
{
    public class ExpApprovalReports
    {
        public string ApprovalStatus { get; set; }
        public string ExpDetailId { get; set; }
        public string expApproverId { get; set; }
        public string CreatedBy { get; set; }
        public string ProfileID { get; set; }
        public string Remarks { get; set; }
        public int ExpRepId { get; set; }
    }
}
