﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Cozmo.Expense.Application.Models
{
    public class ExpFlexData
    {
        public int EFD_Id { get; set; }
        public int EFD_ED_Id { get; set; }
        public int EFD_EF_Id { get; set; }
        public string EFD_Label { get; set; }
        public string EFD_Data { get; set; }
        public string EFD_Type { get; set; }
        public bool EFD_Status { get; set; }
        public int EFD_CreatedBy { get; set; }
        public DateTime EFD_CreatedDate { get; set; }
        public int EFD_ModifiedBy { get; set; }
        public DateTime EFD_ModifiedDate { get; set; }

        /// <summary>
        /// To save expoense report expenses data
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@EFD_Id", EFD_Id));
                if (EFD_Id == 0)
                    liParamsList[0].Direction = ParameterDirection.InputOutput;                
                liParamsList.Add(new SqlParameter("@EFD_ED_Id", EFD_ED_Id));
                liParamsList.Add(new SqlParameter("@EFD_EF_Id", EFD_EF_Id));
                liParamsList.Add(new SqlParameter("@EFD_Label", EFD_Label));
                liParamsList.Add(new SqlParameter("@EFD_Data", EFD_Data));
                liParamsList.Add(new SqlParameter("@EFD_Type", EFD_Type));
                liParamsList.Add(new SqlParameter("@EFD_Status", EFD_Status));
                liParamsList.Add(new SqlParameter("@EFD_CreatedBy", EFD_CreatedBy));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.SaveExpReportFlex, liParamsList.ToArray());
                EFD_Id = EFD_Id == 0 ? (int)liParamsList[0].Value : EFD_Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense report flex data
        /// </summary>
        /// <param name="iExpDtlId"></param>
        /// <returns></returns>
        public static List<ExpFlexData> GetExpFlexData(int iExpDtlId)
        {
            List<ExpFlexData> liExpFlexData = new List<ExpFlexData>();
            try
            {
                if (iExpDtlId == 0)
                    return liExpFlexData;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@EFD_ED_Id", iExpDtlId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpReportFlex, liParamsList.ToArray());
                liExpFlexData = GenericStatic.ConvertToList<ExpFlexData>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liExpFlexData;
        }
    }
}
