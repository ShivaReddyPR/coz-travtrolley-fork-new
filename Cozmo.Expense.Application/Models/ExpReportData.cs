﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.Models
{
    public class ExpReportData
    {
        public int ED_Id { get; set; }
        public int ED_ER_Id { get; set; }
        public int ED_ET_Id { get; set; }
        public DateTime ED_TransDate { get; set; }
        public int ED_PT_Id { get; set; }
        public int ED_CM_Id { get; set; }
        public string ED_ReceiptStatus { get; set; }
        public int ED_ECT_Id { get; set; }
        public string ED_UOM { get; set; }
        public decimal ED_Units { get; set; }
        public decimal ED_UnitPrice { get; set; }
        public string ED_Currency { get; set; }
        public decimal ED_ExcRate { get; set; }
        public decimal ED_ExcAmount { get; set; }
        public decimal ED_Tax { get; set; }
        public decimal ED_TotalAmount { get; set; }
        public bool ED_IsBillable { get; set; }
        public int ED_CV_Id { get; set; }
        public bool ED_ClaimExpense { get; set; }
        public string ED_Comment { get; set; }
        public string ED_REIMBURSESTATUS { get; set; }
        public string ED_APPROVALSTATUS { get; set; }
        public bool ED_Status { get; set; }
        public int ED_CreatedBy { get; set; }
        public DateTime ED_CreatedDate { get; set; }
        public int ED_ModifiedBy { get; set; }
        public DateTime ED_ModifiedDate { get; set; }

        public List<ExpFlexData> expFlexDatas { get; set; }
        public List<ExpAttendees> expAttendees { get; set; }
        public List<ExpReceipts> expReceipts { get; set; }
        public List<ExpRepMessages> expMessages { get; set; }

        /// <summary>
        /// To save expoense report expenses data
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ED_Id", ED_Id));
                if (ED_Id == 0)
                    liParamsList[0].Direction = ParameterDirection.InputOutput;
                liParamsList.Add(new SqlParameter("@ED_ER_Id", ED_ER_Id));
                liParamsList.Add(new SqlParameter("@ED_ET_Id", ED_ET_Id));
                liParamsList.Add(new SqlParameter("@ED_TransDate", ED_TransDate));
                liParamsList.Add(new SqlParameter("@ED_PT_Id", ED_PT_Id));
                liParamsList.Add(new SqlParameter("@ED_CM_Id", ED_CM_Id));
                liParamsList.Add(DBGateway.GetParameter("ED_ReceiptStatus", ED_ReceiptStatus));
                liParamsList.Add(new SqlParameter("@ED_ECT_Id", ED_ECT_Id));
                liParamsList.Add(DBGateway.GetParameter("ED_UOM", ED_UOM));
                liParamsList.Add(new SqlParameter("@ED_Units", ED_Units));
                liParamsList.Add(new SqlParameter("@ED_UnitPrice", ED_UnitPrice));
                liParamsList.Add(DBGateway.GetParameter("ED_Currency", ED_Currency));
                liParamsList.Add(new SqlParameter("@ED_ExcRate", ED_ExcRate));
                liParamsList.Add(new SqlParameter("@ED_ExcAmount", ED_ExcAmount));
                liParamsList.Add(new SqlParameter("@ED_Tax", ED_Tax));
                liParamsList.Add(new SqlParameter("@ED_TotalAmount", ED_TotalAmount));
                liParamsList.Add(new SqlParameter("@ED_IsBillable", ED_IsBillable));
                liParamsList.Add(new SqlParameter("@ED_CV_Id", ED_CV_Id));
                liParamsList.Add(new SqlParameter("@ED_ClaimExpense", ED_ClaimExpense));
                liParamsList.Add(DBGateway.GetParameter("ED_Comment", ED_Comment));
                liParamsList.Add(DBGateway.GetParameter("ED_REIMBURSESTATUS", ED_REIMBURSESTATUS));
                liParamsList.Add(DBGateway.GetParameter("ED_APPROVALSTATUS", ED_APPROVALSTATUS));
                liParamsList.Add(new SqlParameter("@ED_Status", ED_Status));
                liParamsList.Add(new SqlParameter("@ED_CreatedBy", ED_CreatedBy));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.SaveExpReportData, liParamsList.ToArray());
                ED_Id = ED_Id == 0 ? (int)liParamsList[0].Value : ED_Id;

                if (expFlexDatas != null && expFlexDatas.Count > 0 && ED_Id > 0)
                {
                    expFlexDatas.Where(x => x != null).ToList().ForEach(x => {

                        x.EFD_ED_Id = ED_Id;
                        x.EFD_CreatedBy = ED_CreatedBy;
                        x.Save();
                    });                    
                }

                if (expAttendees != null && expAttendees.Count > 0 && ED_Id > 0)
                {                    
                    expAttendees.Where(x => x != null).ToList().ForEach(x => {

                        x.EA_ED_Id = ED_Id;
                        x.EA_CreatedBy = ED_CreatedBy;
                        x.Save();
                    });
                }

                if (expReceipts != null && expReceipts.Count > 0 && ED_Id > 0)
                {
                    expReceipts.Where(x => x != null).ToList().ForEach(x => {

                        x.RC_ED_Id = ED_Id;
                        x.RC_CreatedBy = ED_CreatedBy;
                        x.Save();
                    });
                }

                if (expMessages != null && expMessages.Count > 0 && ED_Id > 0)
                {
                    expMessages.Where(x => x != null).ToList().ForEach(x => {

                        x.ERM_ED_Id = ED_Id;
                        x.ERM_CreatedBy = ED_CreatedBy;
                        x.Save();
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To delete multiple expoense details 
        /// </summary>
        /// <returns></returns>
        public static void Delete(List<int> liEDIs, int iUser)
        {
            try
            {
                if (liEDIs == null || liEDIs.Count == 0)
                    return;

                string sEDIds = string.Join("|", liEDIs.Where(x => x > 0).Select(x => x.ToString()).ToArray()).Trim('|');

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ED_Ids", sEDIds));
                liParamsList.Add(new SqlParameter("@User", iUser));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.DelExpReportData, liParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense report data list
        /// </summary>
        /// <param name="iExpRepId"></param>
        /// <returns></returns>
        public static List<ExpReportData> GetExpRepData(int iExpRepId)
        {
            List<ExpReportData> liExpRepData = new List<ExpReportData>();
            try
            {
                if (iExpRepId == 0)
                    return liExpRepData;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ED_ER_Id", iExpRepId));
                liParamsList.Add(new SqlParameter("@ED_Id", 0));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpRepData, liParamsList.ToArray());
                liExpRepData = dt != null && dt.Rows.Count > 0 ? GenericStatic.ConvertToList<ExpReportData>(dt) : liExpRepData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liExpRepData;
        }

        /// <summary>
        /// To get expense details
        /// </summary>
        /// <param name="iExpRepDtlId"></param>
        /// <returns></returns>
        public static ExpReportData GetExpRepDtls(int iExpRepDtlId)
        {
            ExpReportData clsExpRepData = new ExpReportData();
            try
            {
                if (iExpRepDtlId == 0)
                    return clsExpRepData;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ED_ER_Id", 0));
                liParamsList.Add(new SqlParameter("@ED_Id", iExpRepDtlId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpRepData, liParamsList.ToArray());
                clsExpRepData = dt != null && dt.Rows.Count > 0 ? GenericStatic.ConvertToObj<ExpReportData>(dt) : clsExpRepData;

                if (clsExpRepData != null && clsExpRepData.ED_Id > 0)
                {
                    clsExpRepData.expFlexDatas = ExpFlexData.GetExpFlexData(clsExpRepData.ED_Id);
                    clsExpRepData.expAttendees = ExpAttendees.GetExpAttendees(clsExpRepData.ED_Id);
                    clsExpRepData.expReceipts = ExpReceipts.GetExpReceipts(clsExpRepData.ED_Id);
                    clsExpRepData.expMessages = ExpRepMessages.GetExpMessages(clsExpRepData.ED_Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clsExpRepData;
        }

        /// <summary>
        /// To get expense create screen load data 
        /// </summary>
        /// <param name="iAgentId"></param>
        /// <param name="sRefNo"></param>
        /// <param name="iUserId"></param>
        /// <returns></returns>
        public static DataSet GetExpCreateData(int iAgentId, int iExpRepId, int iUserId)
        {
            DataSet ds = new DataSet();
            try
            {
                if (iAgentId == 0 || iExpRepId == 0)
                    return ds;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@AGENTID", iAgentId));
                liParamsList.Add(new SqlParameter("@ED_ER_Id", iExpRepId));
                liParamsList.Add(new SqlParameter("@User", iUserId));
                ds = DBGateway.ExecuteQuery(ExpSPNames.GetExpCreateData, liParamsList.ToArray());

                if (ds != null && ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "CATEGORIES";
                    ds.Tables[1].TableName = "EXPTYPES";
                    ds.Tables[2].TableName = "EXPREPORT";
                    ds.Tables[3].TableName = "EXPDETAILS";
                    ds.Tables[4].TableName = "EXPMESSAGES";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        /// <summary>
        /// To get expense type related data
        /// </summary>
        /// <param name="iAgentId"></param>
        /// <param name="iExpType"></param>
        /// <param name="iUserId"></param>
        /// <returns></returns>
        public static DataSet GetExpCreateFields(int iAgentId, int iExpType, int iUserId)
        {
            DataSet ds = new DataSet();
            try
            {
                if (iAgentId == 0 || iExpType == 0)
                    return ds;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ET_ID", iExpType));
                liParamsList.Add(new SqlParameter("@AGENTID", iAgentId));
                liParamsList.Add(new SqlParameter("@User", iUserId));
                ds = DBGateway.ExecuteQuery(ExpSPNames.GetExpTypeFieldsData, liParamsList.ToArray());

                if (ds != null && ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "DEFAULTFLEX";
                    ds.Tables[1].TableName = "NORMALFLEX";
                    ds.Tables[2].TableName = "ITEMFLEX";
                    ds.Tables[3].TableName = "POLICYMASTER";
                    ds.Tables[4].TableName = "CUSTOMERS";
                    ds.Tables[5].TableName = "TAXINFO";
                    ds.Tables[6].TableName = "FPCURRENCY";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        /// <summary>
        /// To get expense affidavit data
        /// </summary>
        /// <param name="iExpDtlId"></param>
        /// <param name="iUserId"></param>
        /// <returns></returns>
        public static DataTable GetExpAffidavit(int iExpDtlId, int iUserId)
        {
            DataTable dt = new DataTable();
            try
            {
                if (iExpDtlId == 0)
                    return dt;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ED_Id", iExpDtlId));
                liParamsList.Add(new SqlParameter("@User", iUserId));
                dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpAffidavit, liParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }    
}
