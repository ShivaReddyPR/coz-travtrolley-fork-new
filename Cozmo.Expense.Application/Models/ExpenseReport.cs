﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.Models
{
    public class ExpenseReport
    {
        public int ER_ID { get; set; }
        public string ER_RefNo { get; set; }
        public DateTime ER_Date { get; set; }
        public string ER_Title { get; set; }
        public string ER_Purpose { get; set; }
        public string ER_CompVisited { get; set; }
        public string ER_Comments { get; set; }
        public int ER_ProfileId { get; set; }
        public int ER_ProfileCC { get; set; }
        public string ER_Aprroved { get; set; }
        public bool ER_Status { get; set; }
        public int ER_CreatedBy { get; set; }
        public DateTime ER_CreatedDate { get; set; }
        public int ER_ModifiedBy { get; set; }
        public DateTime ER_ModifiedDate { get; set; }

        public List<ExpenseReportTrips> expenseReportTrips { get; set; }

        /// <summary>
        /// To save expoense report data
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ER_ID", ER_ID));
                if (ER_ID == 0)
                    liParamsList[0].Direction = ParameterDirection.InputOutput;
                liParamsList.Add(DBGateway.GetParameter("ER_RefNo", ER_RefNo));
                if (string.IsNullOrEmpty(ER_RefNo))
                {
                    liParamsList[1].Direction = ParameterDirection.InputOutput;
                    liParamsList[1].Size = 50;
                }
                liParamsList.Add(new SqlParameter("@ER_Date", ER_Date));
                liParamsList.Add(new SqlParameter("@ER_Title", ER_Title));
                liParamsList.Add(DBGateway.GetParameter("ER_Purpose", ER_Purpose));
                liParamsList.Add(DBGateway.GetParameter("ER_CompVisited", ER_CompVisited));
                liParamsList.Add(DBGateway.GetParameter("ER_Comments", ER_Comments));
                liParamsList.Add(new SqlParameter("@ER_ProfileId", ER_ProfileId));
                liParamsList.Add(new SqlParameter("@ER_ProfileCC", ER_ProfileCC));
                liParamsList.Add(new SqlParameter("@ER_Status", ER_Status));
                liParamsList.Add(new SqlParameter("@ER_CreatedBy", ER_CreatedBy));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.SaveExpenseReport, liParamsList.ToArray());
                ER_ID = ER_ID == 0 ? (int)liParamsList[0].Value : ER_ID;
                ER_RefNo = string.IsNullOrEmpty(ER_RefNo) ? (string)liParamsList[1].Value : ER_RefNo;

                if (expenseReportTrips != null && expenseReportTrips.Count > 0)
                {
                    expenseReportTrips[0].ERT_ER_Id = ER_ID;
                    expenseReportTrips[0].ERT_CreatedBy = ER_CreatedBy;
                    ExpenseReportTrips.Save(expenseReportTrips);
                }
                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense report details
        /// </summary>
        /// <param name="iExpRepId"></param>
        /// <returns></returns>
        public static ExpenseReport GetExpReport(int iExpRepId)
        {
            ExpenseReport clsExpenseReport = new ExpenseReport();
            try
            {
                if (iExpRepId == 0)
                    return clsExpenseReport;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@ER_ID", iExpRepId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpenseReport, liParamsList.ToArray());
                clsExpenseReport = GenericStatic.ConvertToObj<ExpenseReport>(dt);
                clsExpenseReport.expenseReportTrips = ExpenseReportTrips.GetExpReportTrips(iExpRepId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clsExpenseReport;
        }
    }
}
