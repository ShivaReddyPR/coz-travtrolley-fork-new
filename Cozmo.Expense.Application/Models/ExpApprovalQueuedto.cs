﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.Models
{
    public class ExpApprovalQueuedto
    {
        public DataTable dtProfiles { get; set; }
        public DataTable dtReceiptStatus { get; set; }
        public DataTable dtPaymentType { get; set; }
        public DataTable dtApprovalReceipts { get; set; }
        public DataTable dtcostCenters { get; set; }
    }    
 }
