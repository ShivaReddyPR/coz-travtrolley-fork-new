﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Cozmo.Expense.Application.Models
{
   public class GetExchangeMasterScreenLoadDataDto
    {

        public DataTable dtAgents { get; set; }
        public DataTable dtCurrency { get; set; } 
        
    }
}
