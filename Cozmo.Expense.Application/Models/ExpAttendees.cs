﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cozmo.Expense.Application.Models
{
    public class ExpAttendees
    {
        public int EA_Id { get; set; }
        public int EA_ED_Id { get; set; }
        public int EA_EAM_Id { get; set; }
        public decimal EA_Amount { get; set; }
        public bool EA_Status { get; set; }
        public int EA_CreatedBy { get; set; }
        public DateTime EA_CreatedDate { get; set; }
        public int EA_ModifiedBy { get; set; }
        public DateTime EA_ModifiedDate { get; set; }

        /// <summary>
        /// To save expoense report expenses data
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@EA_Id", EA_Id));
                if (EA_Id == 0)
                    liParamsList[0].Direction = ParameterDirection.InputOutput;
                liParamsList.Add(new SqlParameter("@EA_ED_Id", EA_ED_Id));
                liParamsList.Add(new SqlParameter("@EA_EAM_Id", EA_EAM_Id));
                liParamsList.Add(new SqlParameter("@EA_Amount", EA_Amount));
                liParamsList.Add(new SqlParameter("@EA_Status", EA_Status));
                liParamsList.Add(new SqlParameter("@EA_CreatedBy", EA_CreatedBy));
                int rowsAffected = DBGateway.ExecuteNonQuerySP(ExpSPNames.SaveExpAttendees, liParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense attendees data
        /// </summary>
        /// <param name="iExpDtlId"></param>
        /// <returns></returns>
        public static List<ExpAttendees> GetExpAttendees(int iExpDtlId)
        {
            List<ExpAttendees> liExpAttendees = new List<ExpAttendees>();
            try
            {
                if (iExpDtlId == 0)
                    return liExpAttendees;

                List<SqlParameter> liParamsList = new List<SqlParameter>();
                liParamsList.Add(new SqlParameter("@EA_ED_Id", iExpDtlId));
                DataTable dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpAttendees, liParamsList.ToArray());
                liExpAttendees = GenericStatic.ConvertToList<ExpAttendees>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return liExpAttendees;
        }

    }
}
