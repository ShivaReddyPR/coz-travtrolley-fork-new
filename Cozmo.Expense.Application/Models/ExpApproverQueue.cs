﻿
using CT.Configuration;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Expense.Application.Models
{
    public class ExpApproverQueue
    {
        /// <summary>
        /// From date
        /// </summary>
        public string FromDate { get; set; }

        /// <summary>
        /// To date
        /// </summary>
        public string ToDate { get; set; }

        /// <summary>
        ///  Agent Id
        /// </summary>
        public int AgentId { get; set; } 
        /// <summary>
        ///  Profile Id
        /// </summary>
        public string ProfileId { get; set; }
        
        /// <summary>
        ///  Cost Center
        /// </summary>
        public string CostCenter { get; set; }

        /// <summary>
        /// Receipt Status
        /// </summary>
        public string ReceiptStatus { get; set; }

        /// <summary>
        /// Payment Type
        /// </summary>
        public string PaymentType { get; set; }

        /// <summary>
        /// Reference No
        /// </summary>
        public string ReportReferenceNo { get; set; } 
        /// <summary>
        ///Title
        /// </summary>
        public string ReportTitle { get; set; }

        /// <summary>
        ///  Approval Status
        /// </summary>
        public string ApproverStatus { get; set; }

        /// <summary>
        /// Loading Expense Approval reports.
        /// </summary>
        /// <returns></returns>
        public DataTable LoadExpenseApprovalReports(string type, int iLoginProfile)
        {
            DataTable dt = null;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@AgentId", AgentId));
                paramList.Add(new SqlParameter("@FromDate", FromDate));
                paramList.Add(new SqlParameter("@ToDate", ToDate));
                paramList.Add(new SqlParameter("@ProfileId", ProfileId));
                paramList.Add(new SqlParameter("@CostCenter", CostCenter));
                paramList.Add(new SqlParameter("@ReceiptStatus", ReceiptStatus));
                paramList.Add(new SqlParameter("@PaymentType", PaymentType));
                paramList.Add(new SqlParameter("@ReportTitle", ReportTitle));
                paramList.Add(new SqlParameter("@ReportReferenceNo", ReportReferenceNo));
                paramList.Add(new SqlParameter("@ApproverStatus", ApproverStatus));
                paramList.Add(new SqlParameter("@LoginProfile", iLoginProfile));

                if (type == "AQ")
                    dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpApprovalReports, paramList.ToArray());
                else if (type == "RR")
                    dt = DBGateway.FillDataTableSP(ExpSPNames.GetReimburseReport, paramList.ToArray());
                else
                    dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpApprovalDetails, paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        /// <summary>
        /// getting  the Expense Reports data.
        /// </summary>
        /// <param name="Reports"></param>
        /// <returns></returns>
        public static DataSet ApprovalReports(ExpApprovalReports Reports, string sHost)
        {
            DataSet ds = new DataSet();
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@ApprovalStatus", Reports.ApprovalStatus));
                paramList.Add(new SqlParameter("@ExpDetailId", Reports.ExpDetailId));
                paramList.Add(new SqlParameter("@expApproverId", Reports.expApproverId));
                paramList.Add(new SqlParameter("@ProfileID", Reports.ProfileID));
                paramList.Add(new SqlParameter("@CreatedBy", Reports.CreatedBy));
                paramList.Add(new SqlParameter("@ExpRepId", Reports.ExpRepId));
                paramList.Add(new SqlParameter("Remarks", Reports.Remarks));
                ds = DBGateway.ExecuteQuery(ExpSPNames.SaveApproverDetails, paramList.ToArray());
                               
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string sUserName = CT.Core.GenericStatic.EncryptData(Convert.ToString(dt.Rows[0]["UserName"]));
                            CT.Core.EmailParams clsParams = new CT.Core.EmailParams();
                            clsParams.FromEmail = ConfigurationSystem.Email["fromEmail"];
                            clsParams.ToEmail = Convert.ToString(dt.Rows[0]["Email"]);
                            //clsParams.CCEmail = Convert.ToString(dt.Rows[0]["CCEmail"]);
                            clsParams.Subject = Convert.ToString(dt.Rows[0]["RepSubject"]);
                            clsParams.EmailBody = Convert.ToString(dt.Rows[0]["RepHeader"]).Replace("@Expenses", Convert.ToString(dt.Rows[0]["RepBody"]))
                                .Replace("@Host", sHost).Replace("@USERNAME", sUserName);

                            CT.Core.Email.Send(clsParams);
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
            return ds;
        }
        /// <summary>
        /// Loading the Expense payment Types here.
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static DataTable LoadPaymentTypes(int agentId)
        {
            DataTable dt = null;
            try {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@agentId", agentId)); 
                dt = DBGateway.FillDataTableSP(ExpSPNames.GetPaymentTypes, paramList.ToArray());
            }
            catch (Exception ex) { throw ex; }
            return dt;
        }

        /// <summary>
        /// Getting the expense report details by expense report id
        /// </summary>
        /// <param name="@ExpRepId"></param>
        /// <returns></returns>
        public static DataSet GetExpReportDetails(long ExpRepId)
        {
            DataSet ds = null;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@ExpRepId", ExpRepId));
                ds = DBGateway.FillSP(ExpSPNames.GetExpReportDetails, paramList.ToArray());
                ds.Tables[0].TableName = "ReportHeader";
                ds.Tables[1].TableName = "ReportBody";
                ds.Tables[2].TableName = "ReportReceipts";
            }
            catch (Exception ex) { throw ex; }
            return ds;
        }

        /// <summary>
        /// To get expense approval info
        /// </summary>
        /// <param name="Approver"></param>
        /// <param name="ExpRepId"></param>
        /// <param name="ExpDtlId"></param>
        /// <returns></returns>
        public static DataTable GetApprovalData(int Approver, int ExpRepId, int ExpDtlId)
        {
            DataTable dt = null;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@APPROVER", Approver));
                paramList.Add(new SqlParameter("@EXPREPID", ExpRepId));
                paramList.Add(new SqlParameter("@EXPDTLID", ExpDtlId));
                dt = DBGateway.FillDataTableSP(ExpSPNames.GetExpApprovalData, paramList.ToArray());
            }
            catch (Exception ex) { throw ex; }
            return dt;
        }
    }
   
}
