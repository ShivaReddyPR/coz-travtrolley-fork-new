﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace Cozmo.Expense.Application.Models
{
    public class ExpCityMaster
    {
        public int ECT_ID { get; set; }
        public int ECT_AgentId { get; set; }
        public string ECT_Code { get; set; }
        public string ECT_Name { get; set; }
        public string ECT_Country { get; set; }
        public string ECT_Currency { get; set; }
        public bool ECT_Status { get; set; }
        public int ECT_CreatedBy { get; set; }
        public DateTime ECT_CreatedDate { get; set; }
        public int ECT_ModifiedBy { get; set; }
        public DateTime ECT_ModifiedDate { get; set; }
        public string COUNTRY_NAME { get; set; }

        /// <summary>
        /// To save expense city master data
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@ECT_ID", ECT_ID));
                if (ECT_ID == 0)
                    paramList[0].Direction = ParameterDirection.InputOutput;
                paramList.Add(new SqlParameter("@ECT_AgentId", ECT_AgentId));
                paramList.Add(new SqlParameter("@ECT_Code", ECT_Code));
                paramList.Add(new SqlParameter("@ECT_Name", ECT_Name));
                paramList.Add(new SqlParameter("@ECT_Country", ECT_Country));
                paramList.Add(new SqlParameter("@ECT_Currency", ECT_Currency));
                paramList.Add(new SqlParameter("@ECT_Status", ECT_Status));
                paramList.Add(new SqlParameter("@ECT_CreatedBy", ECT_CreatedBy));
                int rows = DBGateway.ExecuteNonQuerySP("usp_SaveExpCityData", paramList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get expense cities data
        /// </summary>
        /// <param name="AgentId"></param>
        /// <returns></returns>
        public static List<ExpCityMaster> GetExpCity(int AgentId)
        {
            List<ExpCityMaster> cList = new List<ExpCityMaster>();
            try
            {
                if (AgentId == 0)
                    return cList;

                List<SqlParameter> paramsList = new List<SqlParameter>();
                paramsList.Add(new SqlParameter("@ECT_AgentId", AgentId));
                DataTable dt = DBGateway.FillDataTableSP("usp_GetExpCityData", paramsList.ToArray());
                cList = GenericStatic.ConvertToList<ExpCityMaster>(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cList;
        }
    }
}
