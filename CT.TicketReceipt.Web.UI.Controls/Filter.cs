using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CT.TicketReceipt.Web.UI.Controls
{
    public enum FilterType
    {
        Text = 0,
        DropDownList = 1,
        DateControl = 3
    }    

    [DefaultProperty("ID")]
    [ToolboxData("<{0}:Filter runat=server></{0}:Filter>")]
    public class Filter : WebControl, INamingContainer
    {        
        public string HeaderText
        {
            get
            {
                string s = (string)ViewState["Text"];
                return ((s == null) ? string.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }        
        public FilterType Type
        {
            get
            {
                if (ViewState["FilterType"] == null) return FilterType.Text;
                else return (FilterType)ViewState["FilterType"];                
            }

            set
            {
                ViewState["FilterType"] = value;
            }
        }        
        public string Value
        {
            get
            {
                string val = string.Empty;
                switch (Type)
                {
                    case FilterType.Text:
                        val = ((TextBox)this.FindControl("txt")).Text;
                        break;
                    case FilterType.DropDownList:
                        val = ((DropDownList)this.FindControl("ddl")).Text;
                        break;
                }
                return val;
            }

            set
            {
                switch (Type)
                {
                    case FilterType.Text:
                        ((TextBox)this.FindControl("txt")).Text = value;
                        break;
                    case FilterType.DropDownList:
                        ((DropDownList)this.FindControl("ddl")).Text = value;
                        break;
                }
            }            
        }        
        public override Unit Width
        {
            get
            {
                if (ViewState["Width"] == null) return Unit.Pixel(100);
                else return (Unit)ViewState["Width"];                
            }

            set
            {
                ViewState["Width"] = value;
            }
        }
        public override string CssClass
        {
            get
            {
                if (ViewState["CssClass"] == null) return base.CssClass;
                else return (string)ViewState["CssClass"]; 
            }
            set
            {
                ViewState["CssClass"] = value;
            }
        }
        public delegate void ClickHandler(object sender, EventArgs e);
        public event ClickHandler Click;

        Label lbl = new Label();        
        Control ctrl = new Control();
        ImageButton img = new ImageButton();
                        
                
        
        protected override void CreateChildControls()
        {
            // Create the controls dynamicaaly based on the FilterType
            EnsureChildControls();
            
            switch (Type)
            {
                case FilterType.Text:
                    TextBox txt = new TextBox();
                    txt.Width = Width;
                    //txt.CssClass = base.CssClass;
                    txt.CssClass = CssClass;
                    
                    txt.ID = "txt";                    
                    ctrl = txt;
                    break;
                case FilterType.DropDownList:
                    DropDownList ddl = new DropDownList();
                    ddl.Width = Width;
                    ddl.CssClass = base.CssClass;
                    ddl.ID = "ddl";
                    ctrl = ddl;
                    break;
            }
            

            lbl.Text = HeaderText;
            lbl.Width = Width;
            lbl.CssClass = "filterHeaderText";
            
            img.ID = "img";
            img.ImageUrl = "~/Images/filter.jpg";
            img.Width = new Unit(15);
            img.Height= new Unit(15);
            img.Click += new ImageClickEventHandler(img_Click);

            this.Controls.Add(new LiteralControl("<table><tr><td>"));           
            this.Controls.Add(ctrl);
            this.Controls.Add(new LiteralControl("</td><td valign='middle'>"));
            this.Controls.Add(img);
            this.Controls.Add(new LiteralControl("</td></tr><tr><td>"));
            this.Controls.Add(lbl);
            this.Controls.Add(new LiteralControl("</td></tr></table>"));

            base.CreateChildControls();
        }
       
        protected void img_Click(object sender, ImageClickEventArgs e)
        {            
            if (Click != null) Click(sender, (EventArgs)e);
        }
        
        public Control GetControl()
        {
            // Returns the Filter Control as basic Control
            EnsureChildControls();
            switch (Type)
            {
                case FilterType.Text:
                    return this.FindControl("txt");
               
                case FilterType.DropDownList:
                    return this.FindControl("ddl");
              
                default:
                    return this.FindControl("txt");
            }
        }
                      
    }
}
