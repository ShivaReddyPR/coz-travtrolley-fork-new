using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
namespace CT.TicketReceipt.Web.UI.Controls
{
    public class CommonGrid : Control
    {
        public void BindGrid(GridView gv , DataSet ds)
        {            
            // Bind the gridview and display the header row even if the DataSource is null
            if (ds != null && ds.Tables[0] != null)
            {
                bindGrid(gv, ds.Tables[0]);
            }
            else
            {
                gv.DataSource = null;
                gv.DataBind();
            }
        }
        public void BindGrid(GridView gv, DataTable dt)
        {
            // Bind the gridview and display the header row even if the DataSource is null
            if (dt != null)
            {
                bindGrid(gv, dt);
            }
            else
            {
                gv.DataSource = dt;
                gv.DataBind();
            }
        }
        private void bindGrid(GridView gv, DataTable dt)
        {
            // Bind the gridview and display the header row even if the DataSource is null
            if (dt.Rows.Count == 0)
            {
                DataTable dtTemp = dt.Clone();
                DataRow dr = dtTemp.NewRow();
                foreach (DataColumn dc in dtTemp.Columns)
                {
                    if (!dc.AllowDBNull)
                    {
                        switch (dc.DataType.Name)
                        {
                            case "DateTime":
                                dr[dc] = default(DateTime);
                                break;
                            default:
                                dr[dc] = 0;
                                break;
                        }                      
                    }
                }
                dtTemp.Rows.Add(dr);
                gv.DataSource = dtTemp;
                gv.DataBind();
                gv.Rows[0].Visible = false;
            }
            else
            {
                gv.DataSource = dt;
                gv.DataBind();
            }
        }
        public void FilterGridView(GridView gv, DataTable dt, string[,] controls)
        {
            // Filter the datasource according to the ControlID - Value pairs passed as 2D array
            // and Bind the GridView with the filtered DataSource

            string condition = string.Empty;
            string[] code = new string[controls.Length / controls.Rank];
            if (gv.HeaderRow != null)
            {
                Control con;
                GridViewRow gvHeader = gv.HeaderRow;
                for (int i = 0; i < controls.Length / controls.Rank; i++)
                {
                    con = gvHeader.FindControl(controls[i, 0]);
                    switch (con.GetType().Name)
                    {
                        case "DropDownList":
                            code[i] = ((DropDownList)con).SelectedItem.Text;                            
                            break;  
                        case "datecontrol_ascx":
                            code[i] = ((TextBox)con.FindControl("Date")).Text;
                            break;
                        case "TextBox":
                            code[i] = ((TextBox)con).Text;
                            break;
                        case "Filter":
                            code[i] = ((Filter)con).Value;
                            break;
                        default:
                            code[i] = ((TextBox)con).Text;//TODO:
                            break;
                    }
                    code[i] = code[i].Trim();
                    if (condition != string.Empty && !string.IsNullOrEmpty(code[i]))
                    {
                        condition += " AND ";
                    }
                    if (!string.IsNullOrEmpty(code[i]))
                    {                        
                        switch (dt.Columns[controls[i, 1].ToString()].DataType.Name)
                        {
                            case "String":
                                condition += controls[i, 1] + " like '" + code[i].Replace("'", "''") + "%'";
                                break;
                            case "DateTime":
                                condition += controls[i, 1] + " >= '" + code[i] + " 00:00' and " + controls[i, 1] + " <= '" + code[i] + " 23:59'";
                                break;
                            case "Decimal":
                                condition += controls[i, 1] + " = " + Utility.ToLong(code[i]);
                                break;
                            default:
                                condition += controls[i, 1] + " = " + code[i];
                                break;
                        } 
                    }                   

                }
                if (string.IsNullOrEmpty(condition))
                {
                    bindGrid(gv, dt);
                }
                else
                {
                    DataView dv = dt.DefaultView;
                    dv.RowFilter = condition;
                    bindGrid(gv, dv.ToTable());
                }

                gvHeader = gv.HeaderRow;
                for (int i = 0; i < controls.Length / controls.Rank; i++)
                {
                    con = gvHeader.FindControl(controls[i, 0]);
                    switch (con.GetType().Name)
                    {
                        case "DropDownList":
                            ((DropDownList)con).Text = code[i];
                            break;
                        case "datecontrol_ascx":
                            ((TextBox)con.FindControl("Date")).Text = code[i];
                            break;
                        case "TextBox":
                            ((TextBox)con).Text = code[i];
                            break;
                        case "Filter":
                            ((Filter)con).Value = code[i];
                            break;
                        default:
                            ((TextBox)con).Text = code[i];//TODO:
                            break;
                    }
                }                

            }
        }
    }
    
}
