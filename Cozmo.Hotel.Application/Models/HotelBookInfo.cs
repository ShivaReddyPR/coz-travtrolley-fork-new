﻿using CT.BookingEngine;
using System;
using System.Collections.Generic;

namespace Cozmo.Hotel.Application.Models
{
    [Serializable]
    public class HotelBookInfo
    {
        /// <summary>
        /// promotion details
        /// </summary>
        public PromoDetails PromoInfo { get; set; }
        /// <summary>
        /// payment details
        /// </summary>
        public PaymentInformation PaymentInfo { get; set; }
        /// <summary>
        /// TotalPrice
        /// </summary>
        public decimal TotalPrice { get; set; }
        /// <summary>
        /// BookingId
        /// </summary>
        public int BookingId { get; set; }
        /// <summary>
        /// Pending queue id
        /// </summary>
        public int PendingQueueId { get; set; }
        /// <summary>
        /// Payment Id
        /// </summary>
        public string PaymentId { get; set; }
        /// <summary>
        /// order Id
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// PassenegerInfo
        /// </summary>
        public List<HotelPassenger> PassenegerInfo { get; set; }
        /// <summary>
        /// UserId
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// TotalRooms
        /// </summary>
        public int TotalRooms { get; set; }
    }
}
