﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.Models
{
    public enum HotelBookApiCall
    {
        SearchFlightsReq = 10,
        SearchFlightsResp = 11,
        GetHotelRoomsReq = 20,
        GetHotelRoomsResp = 21,
        GetRoomPolicyReq = 30,
        GetRoomPolicyResp = 31,
        BookHotelReq = 40,
        BookHotelResp = 41,
        HotelCancelReq = 50,
        HotelCancelResp = 51
    }
}
