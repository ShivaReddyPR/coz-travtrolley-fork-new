﻿using Cozmo.Api.Application.Models;
using CT.BookingEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cozmo.Hotel.Application.Models
{
    public static class ValidateErrors
    {
        /// <summary>
        /// To validate hotel session
        /// </summary>
        /// <param name="hotelReq"></param>
        public static void HotelSession(int errorCode, object hotelReq)
        {
            if (hotelReq == null)
                throw new ApiReqException(errorCode, "Api session expired, please relogin and start booking.");
        }

        /// <summary>
        /// To validate api req info params
        /// </summary>
        /// <param name="apiReqInfo"></param>
        public static void ApiReq(ApiReqInfo apiReqInfo)
        {
            if (apiReqInfo == null)
                throw new ApiReqException(800, "Api request info missing.");

            if (string.IsNullOrEmpty(apiReqInfo.AgentRefKey))
                throw new ApiReqException(801, "Agent reference key missing in Api request info.");
        }

        /// <summary>
        /// To validate hotel req info params
        /// </summary>
        /// <param name="hotelReq"></param>
        public static void HotelReq(HotelRequest hotelReq)
        {
            if (hotelReq == null)
                throw new ApiReqException(900, "Invalid hotel request.");
            

            if (hotelReq.CityId == 0 && string.IsNullOrEmpty(hotelReq.CityName) && hotelReq.Latitude == 0 && hotelReq.Longtitude == 0 && 
                (hotelReq.HotelStaticId == null || hotelReq.HotelStaticId.Count == 0))
                throw new ApiReqException(901, "Hotel request city code/name/location info missing.");

            if (hotelReq.StartDate == DateTime.MinValue || hotelReq.EndDate == DateTime.MinValue)
                throw new ApiReqException(902, "Hotel request checkin date/chekcout date missing.");

            var currDate = DateTime.Now.ToShortDateString();
            DateTime todayDate = Convert.ToDateTime(currDate);
            if (hotelReq.StartDate < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
                throw new ApiReqException(902, "Hotel check in date should be greater than or equal to current date.");

            if (hotelReq.StartDate >= hotelReq.EndDate)
                throw new ApiReqException(902, "Hotel check in date should be less than check out date.");

            if (string.IsNullOrEmpty(hotelReq.PassengerNationality))
                throw new ApiReqException(903, "Hotel request passenger nationality missing.");

            if (hotelReq.RoomGuest == null || hotelReq.RoomGuest.Length == 0)
                throw new ApiReqException(904, "Hotel request room guest info missing.");

            if (hotelReq.RoomGuest.Where(x => x.noOfAdults == 0 && x.noOfChild == 0).Count() > 0)
                throw new ApiReqException(905, "Hotel request room guest adult/child info missing.");
        }

        /// <summary>
        /// To validate hotel rooms info
        /// </summary>
        /// <param name="hotelRoomsDetails"></param>
        public static void HotelRooms(HotelRoomsDetails[] hotelRoomsDetails)
        {
            if (hotelRoomsDetails == null || hotelRoomsDetails.Length == 0)
                throw new ApiReqException(921, "Rooms not available for selected hotel, please select different hotel.");
        }

        /// <summary>
        /// To validate cancel policy api request
        /// </summary>
        /// <param name="roomKeys"></param>
        public static void CancelReq(List<string> roomKeys)
        {
            if (roomKeys == null || roomKeys.Count == 0 || roomKeys.Where(x => !string.IsNullOrEmpty(x)).ToList().Count == 0)
                throw new ApiReqException(922, "Room reference keys missing.");
        }

        /// <summary>
        /// To validate cancel policy response
        /// </summary>
        /// <param name="cancelPolicy"></param>
        public static void CancelResp(string cancelPolicy)
        {
            if (string.IsNullOrEmpty(cancelPolicy))
                throw new ApiReqException(923, "Cancellation policy not available for selected rooms, please select different rooms.");
        }

        /// <summary>
        /// To validate hotel book api request
        /// </summary>
        /// <param name="roomKeys"></param>
        /// <param name="hotelPassengers"></param>
        public static void HotelBookReq(List<string> roomKeys, List<HotelPassenger> hotelPassengers)
        {
            CancelReq(roomKeys);

            if (hotelPassengers == null || hotelPassengers.Count == 0 || hotelPassengers.Where(x => x != null).ToList().Count == 0)
                throw new ApiReqException(930, "Room(s) guest info missing.");

            if (hotelPassengers.Where(x => string.IsNullOrEmpty(x.Firstname) || string.IsNullOrEmpty(x.Lastname)).ToList().Count > 0)
                throw new ApiReqException(931, "First/Last name required for all guests.");

            if (hotelPassengers.Where(x => x.LeadPassenger && (string.IsNullOrEmpty(x.Email) || string.IsNullOrEmpty(x.Phoneno))).ToList().Count > 0)
                throw new ApiReqException(932, "Email/Phone no required for lead guest.");

            if (hotelPassengers.Where(x => !x.PaxType.Equals(default(HotelPaxType))).ToList().Count > 0)
                throw new ApiReqException(933, "Pax type required for all guests.");
        }

        /// <summary>
        /// To validate cancel policy response
        /// </summary>
        /// <param name="cancelPolicy"></param>
        public static void RoomInfo(HotelRoomsDetails selRoom)
        {
            if (string.IsNullOrEmpty(selRoom.roomRefKey))
                throw new ApiReqException(934, "Room reference key invalid.");
        }

        /// <summary>
        /// To validate hotel cancel confirm no
        /// </summary>
        /// <param name="confirmNo"></param>
        public static void CancelConfirmNo(string confirmNo)
        {
            if (string.IsNullOrEmpty(confirmNo))
                throw new ApiReqException(935, "Invalid hotel confirm no.");
        }

        /// <summary>
        /// To validate hotel cancel itinerary
        /// </summary>
        /// <param name="hotelItinerary"></param>
        public static void CancelItinerary(HotelItinerary hotelItinerary)
        {
            if (hotelItinerary == null || string.IsNullOrEmpty(hotelItinerary.ConfirmationNo))
                throw new ApiReqException(936, "Invalid hotel confirm no.");
        }
    }
}
