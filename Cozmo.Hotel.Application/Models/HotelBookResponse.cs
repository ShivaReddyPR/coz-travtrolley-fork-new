﻿using System;
using System.Collections.Generic;

namespace Cozmo.Hotel.Application.Models
{
    /// <summary>
    /// Hotel booking response
    /// </summary>
    [Serializable]
    public class HotelBookResponse
    {
        /// <summary>
        /// Booking RefNo
        /// </summary>
        public string BookingRefNo { get; set; }
        /// <summary>
        /// RoomReferenceNo
        /// </summary>
        public List<HotelRoomStatus> RoomsStatus { get; set; }
        /// <summary>
        /// Confirmation No
        /// </summary>
        public string ConfirmationNo { get; set; }
        /// <summary>
        /// Booking Id
        /// </summary>
        public int BookingId { get; set; }
        /// <summary>
        /// B2CRef Id
        /// </summary>
        public int B2CRefId { get; set; }
    }

    /// <summary>
    /// Hotel room wise booking status
    /// </summary>
    [Serializable]
    public class HotelRoomStatus
    {
        /// <summary>
        /// Room number
        /// </summary>
        public int roomNo { get; set; }
        /// <summary>
        /// Confirmation Number
        /// </summary>
        public string confirmNumber { get; set; }
        /// <summary>
        /// Confirmation Status
        /// </summary>
        public string confirmStatus { get; set; }
    }
}
