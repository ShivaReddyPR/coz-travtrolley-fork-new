﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.Models
{
    public class HotelCancelResponse
    {
        /// <summary>
        /// Cancel Status
        /// </summary>
        public string CancelStatus { get; set; }
        /// <summary>
        /// Cancel RefNo
        /// </summary>
        public string CancelRefNo { get; set; }
        /// <summary>
        /// Refund Amount
        /// </summary>
        public decimal RefundAmount { get; set; }
        /// <summary>
        /// Confirmation No
        /// </summary>
        public string HotelConfirmNo { get; set; }
    }
}
