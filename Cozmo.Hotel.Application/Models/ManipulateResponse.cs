﻿using Cozmo.Api.Application.Models;
using CT.BookingEngine;
using Newtonsoft.Json;
using System;

namespace Cozmo.Hotel.Application.Models
{
    public static class ManipulateResponse
    {
        public static object HotelApiResponse<T>(object responseData, int AgentId)
        {
            object newObj = responseData;

            try
            {                
                var serializerSettings = ApiCommon.GetJsonResolver<T>((int)ProductType.Hotel, ApiObjects.HotelSearchResult, AgentId);
                var json = JsonConvert.SerializeObject(responseData, serializerSettings);
                newObj = JsonConvert.DeserializeObject(json);
            }
            catch (Exception ex)
            {

            }

            return newObj;
        }        
    }
}
