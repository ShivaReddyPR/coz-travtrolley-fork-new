﻿using Cozmo.Api.Application.Models;
using CT.BookingEngine;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Cozmo.Hotel.Application.Models
{
    public static class APIGenericStatic
    {
        /// <summary>
        /// To get login info
        /// </summary>
        /// <param name="ApiKey"></param>
        /// <param name="iOnBehalfLoc"></param>
        /// <returns></returns>
        public static LoginInfo GetLoginInfo(string ApiKey, int iOnBehalfLoc)
        {
            LoginInfo clsLInfo = null;
            try
            {
                clsLInfo = LoginInfo.GetLoginInfo(ApiKey, (int)ProductType.Hotel);
                clsLInfo.IsOnBehalfOfAgent = iOnBehalfLoc > 0;
                
                if (clsLInfo.IsOnBehalfOfAgent)
                {
                    LocationMaster clsLM = new LocationMaster(iOnBehalfLoc);
                    AgentMaster clsAM = new AgentMaster(clsLM.AgentId);

                    clsLInfo.IsOnBehalfOfAgent = true;
                    clsLInfo.OnBehalfAgentID = clsLM.AgentId;
                    clsLInfo.OnBehalfAgentLocation = iOnBehalfLoc;
                    clsLInfo.OnBehalfAgentCurrency = clsAM.AgentCurrency;
                    clsLInfo.OnBehalfAgentDecimalValue = clsAM.DecimalValue;
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = clsAM.AgentCurrency;
                    clsLInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                    clsLInfo.OnBehalfAgentSourceCredentials = AgentMaster.GetAirlineCredentials(clsLM.AgentId);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to get Login info from Hotel web API:" + ex.Message, "");
            }
            return clsLInfo;
        }

        /// <summary>
        /// To get login user location country code
        /// </summary>
        /// <param name="iLocId"></param>
        /// <returns></returns>
        public static string GetLoginCountry(long iLocId)
        {
            string sLoginCntryCode = string.Empty;
            try
            {
                LocationMaster clsLM = new LocationMaster(iLocId);
                sLoginCntryCode = clsLM.CountryCode;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to get Login country code in Hotel web API:" + ex.Message, "");
            }
            return sLoginCntryCode;
        }

        /// <summary>
        /// To get B2C user preferences
        /// </summary>
        /// <param name="clsCredential"></param>
        /// <returns></returns>
        public static APIUserPreference GetPreferences(AuthenticationData clsCredential)
        {
            LoginInfo clsLoginInfo = new LoginInfo();
            UserPreference clsUserPref = new UserPreference();
            APIUserPreference clsAPIPref = new APIUserPreference();

            try
            {
                //int iUserId = clsUserPref.GetMemberIdBySiteName(clsCredential.SiteName);
                int iUserId = clsUserPref.GetMemberIdBySiteNameForHotel(clsCredential.SiteName);
                LoginInfo AgentLoginInfo = UserMaster.GetB2CUser(iUserId);
                clsAPIPref.Load(iUserId, ItemType.Hotel);
                clsAPIPref.AgentCurrency = AgentLoginInfo.Currency;
                clsAPIPref.AgentDecimalPoint = AgentLoginInfo.DecimalValue;
                clsAPIPref.AgentEmail = AgentLoginInfo.AgentEmail;
                clsAPIPref.AgentID = AgentLoginInfo.AgentId;
                AgentMaster agent = new AgentMaster(AgentLoginInfo.AgentId);
                clsAPIPref.AgentAddress = agent.Address;
                clsAPIPref.AgentPhoneNo = agent.Phone1;
                clsAPIPref.MemberId = Convert.ToInt32(AgentLoginInfo.UserID);
                clsAPIPref.LocationId = Convert.ToInt32(AgentLoginInfo.LocationID);
                clsAPIPref.LocationCountryCode = AgentLoginInfo.LocationCountryCode;
                clsAPIPref.LoginUserName = AgentLoginInfo.LoginName;
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Site Name is not valid(" + clsCredential.SiteName + "): " + ex.ToString(), "");
                throw new ArgumentException("Site Name is not valid");
            }
            return clsAPIPref;
        }

        /// <summary>
        /// To save hotel cache memeory data
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="dataObject"></param>
        /// <param name="ResultType"></param>
        public static void SaveHotelCache(string sessionId, object dataObject, string ResultType)
        {
            if (sessionId == null || dataObject == null)
                return;

            try
            {
                byte[] data = GenericStatic.GetByteArrayWithObject(dataObject);
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@sessionId", sessionId));
                liParams.Add(new SqlParameter("@resultData", data));
                liParams.Add(DBGateway.GetParameter("ResulType", ResultType));

                DBGateway.ExecuteNonQuerySP("usp_AddHotelSearchResultCache", liParams.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To Get hotel cache memeory data
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="ResultType"></param>
        public static byte[] GetHotelCache(string sessionId, string ResultType)
        {
            if (sessionId == null)
                throw new Exception("Session id missing to get hotel cache data.");

            byte[] data = new byte[0];
            try
            {
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@sessionId", sessionId));
                liParams.Add(DBGateway.GetParameter("ResulType", ResultType));

                var sqlReader = DBGateway.ExecuteReaderSP("usp_GetHotelSearchResultCache", liParams.ToArray(), DBGateway.GetConnection());

                ValidateErrors.HotelSession(10006, sqlReader);

                if (sqlReader.Read())
                    data = (byte[])sqlReader["resultData"];

                ValidateErrors.HotelSession(10007, data);

                sqlReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }
    }

    /// <summary>
    /// To get B2C authentication data
    /// </summary>
    public class AuthenticationData
    {
        public string SiteName { get; set; }
        public string AccountCode { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
