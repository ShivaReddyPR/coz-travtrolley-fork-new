﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class GetHotelItinerary
    {
        /// <summary>
        /// Query request to get payment preferences
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public HotelBookInfo HotelBookInfo { get; set; }

            public string ConfirmNo { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            IMediator _mediator;

            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler(IMediator mediator)
            {
                _mediator = mediator;
            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine();

                try
                {
                    if (!string.IsNullOrEmpty(clsQuery.ConfirmNo))
                    {
                        var itineraryData = MetaSearchEngine.GetHotelItineraryByConfNo(clsQuery.ConfirmNo);
                        return await Task.FromResult(new ApplicationResponse(itineraryData));
                    }

                    ValidateErrors.ApiReq(clsQuery.ApiReqInfo);
                                        
                    var roomRefKeys = clsQuery.HotelBookInfo.PassenegerInfo.Select(x => x.roomRefKey).Distinct().ToList();

                    clsMS.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey);
                    clsMS.SessionId = clsQuery.ApiReqInfo.SSRefKey;

                    var cacheData = APIGenericStatic.GetHotelCache(clsMS.SessionId, "HotelSearchReq");
                    var hotelReq = cacheData != null && cacheData.Length > 0 ? (HotelRequest)CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    cacheData = APIGenericStatic.GetHotelCache(clsMS.SessionId, "RoomDetails");
                    var selResult = cacheData != null && cacheData.Length > 0 ? (HotelSearchResult)CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    ValidateErrors.HotelSession(10003, selResult);

                    ValidateErrors.HotelSession(10002, hotelReq);

                    /* For UAH we are giving no of rooms selection at room type selection, so need to update the same to main request */
                    if (clsQuery.HotelBookInfo.TotalRooms > 1)
                    {

                        hotelReq.NoOfRooms = clsQuery.HotelBookInfo.TotalRooms;
                        int TotalPax = hotelReq.RoomGuest[0].noOfAdults;
                        hotelReq.RoomGuest[0].noOfAdults = 1;
                        hotelReq.RoomGuest = Enumerable.Repeat(hotelReq.RoomGuest[0], clsQuery.HotelBookInfo.TotalRooms).ToArray();
                        hotelReq.RoomGuest[clsQuery.HotelBookInfo.TotalRooms - 1].noOfAdults = hotelReq.RoomGuest[clsQuery.HotelBookInfo.TotalRooms - 1].noOfAdults + (TotalPax - clsQuery.HotelBookInfo.TotalRooms);
                        selResult.RoomGuest = hotelReq.RoomGuest;
                    }                    

                    var roomTypeCodes = new List<string>();

                    roomRefKeys.ForEach(x => {

                        roomTypeCodes.Add(selResult.RoomDetails.Where(t => t.roomRefKey == x).Select(t => t.RoomTypeCode).FirstOrDefault());
                    });

                    var roomDtls = selResult.RoomDetails.Where(x => x.roomRefKey == roomRefKeys[0]).Select(x => x).FirstOrDefault();
                    var sourceName = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), roomDtls.SupplierName);

                    var roomTypes = string.Join("^", roomTypeCodes.ToArray()).Trim('^');
                    var cancelPolicy = clsMS.GetApiRoomsCancelPolicy(selResult, roomTypes, hotelReq, roomDtls.SequenceNo, sourceName);

                    var selRooms = selResult.RoomDetails.Where(x => roomRefKeys.Contains(x.roomRefKey)).Select(x => x).ToArray();
                    var itinerary = clsMS.CreateHotelItinerary(selResult, hotelReq, ref selRooms);

                    for (int r = 0; r < itinerary.Roomtype.Length; r++)
                    {
                        itinerary.Roomtype[r].PassenegerInfo = clsQuery.HotelBookInfo.PassenegerInfo.Where(p => p.RoomId == r + 1).ToList();

                        for (int i = 0; i < itinerary.Roomtype[r].PassenegerInfo.Count; i++)
                        {
                            itinerary.Roomtype[r].PassenegerInfo[i].Country = itinerary.PassengerCountryOfResidence;
                            itinerary.Roomtype[r].PassenegerInfo[i].Nationality = itinerary.PassengerNationality;
                        }
                    };

                    itinerary.BookUserIP = clsQuery.ApiReqInfo.SourceIP;
                    itinerary.TotalPrice = clsQuery.HotelBookInfo.TotalPrice > 0 ? clsQuery.HotelBookInfo.TotalPrice : itinerary.TotalPrice;
                    itinerary.TransType = hotelReq.Transtype;
                    itinerary.ConfirmationNo = string.Empty;
                    itinerary.HotelPolicyDetails = itinerary.HotelPolicyDetails.Replace("^", "|<strong>Disclaimer: </strong> ");
                    itinerary.HotelCancelPolicy = itinerary.HotelCancelPolicy.Replace('^', '|');
                    itinerary.CreatedBy = clsQuery.HotelBookInfo.UserId > 0 ? clsQuery.HotelBookInfo.UserId : (int)clsMS.SettingsLoginInfo.UserID;
                    itinerary.PaymentInfo = clsQuery.HotelBookInfo.PaymentInfo;
                    itinerary.PaymentId = clsQuery.HotelBookInfo.PaymentId;
                    itinerary.OrderId = clsQuery.HotelBookInfo.OrderId;
                    itinerary.BookingId = clsQuery.HotelBookInfo.BookingId;
                    itinerary.PGSoruce = clsQuery.HotelBookInfo.PaymentInfo != null ? clsQuery.HotelBookInfo.PaymentInfo.PaymentGateway : itinerary.PGSoruce;
                    itinerary.PromoInfo = clsQuery.HotelBookInfo.PromoInfo;

                    string BookingRemarks = "Bookings including children will be based on sharing parents bedding and no separate bed for children is provided unless otherwise stated:";
                    itinerary.HotelPolicyDetails = itinerary.HotelPolicyDetails.Replace(BookingRemarks, "").Replace("^", "|") + "|<strong>Disclaimer: </strong> " + 
                        (itinerary.Roomtype.Any(x => x.ChildCount > 0) ? BookingRemarks + "|<strong> Bed type availabilities depends @ Check - In time </strong>" : 
                        "|<strong> Bed type availabilities depends @ Check - In time </strong>");

                    Product prod = itinerary;
                    prod.ProductId = itinerary.ProductId;
                    prod.ProductType = itinerary.ProductType;
                    prod.ProductTypeId = itinerary.ProductTypeId;

                    return await Task.FromResult(new ApplicationResponse(prod));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelApi, CT.Core.Severity.High, -2, "Hotel Api: GetPaymentPreference handler class. Error: " + ex.ToString(), string.Empty);

                    return new ApplicationResponse().AddError("Hotel ApiException", ex.ToString());
                }
            }
        }
    }
}