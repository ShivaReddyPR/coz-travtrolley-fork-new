﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class GetRoomsOutPutVAT
    {
        /// <summary>
        /// Query request to get hotel room cancellation policy
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class RoomsOutPutVATReq : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public List<string> RoomRefKeys { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<RoomsOutPutVATReq, ApplicationResponse>
        {
            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(RoomsOutPutVATReq clsQuery, CancellationToken cancellationToken)
            {
                LoginInfo loginInfo = null;
                List<object> liRoomsOutPutVAT = new List<object>();

                try
                {
                    ValidateErrors.ApiReq(clsQuery.ApiReqInfo);

                    ValidateErrors.CancelReq(clsQuery.RoomRefKeys);

                    loginInfo = APIGenericStatic.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey);

                    var cacheData = APIGenericStatic.GetHotelCache(clsQuery.ApiReqInfo.SSRefKey, "RoomDetails");
                    var selResult = cacheData != null && cacheData.Length > 0 ? (HotelSearchResult)CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    ValidateErrors.HotelSession(10003, selResult);

                    var roomDtls = selResult.RoomDetails.Where(x => clsQuery.RoomRefKeys.Contains(x.roomRefKey)).Select(x => x).ToArray();

                    if (roomDtls == null || roomDtls.Length == 0)
                        return new ApplicationResponse(liRoomsOutPutVAT);

                    int iAgencyId = !loginInfo.IsOnBehalfOfAgent ? loginInfo.AgentId : loginInfo.OnBehalfAgentID;
                    int iLocationId = !loginInfo.IsOnBehalfOfAgent ? (int)loginInfo.LocationID : loginInfo.OnBehalfAgentLocation;

                    LocationMaster locationMaster = new LocationMaster(iLocationId);
                    AgentMaster clsAgentMaster = new AgentMaster(iAgencyId);

                    decimal outPutVat = 0m;

                    if (locationMaster.CountryCode != "IN" && roomDtls[0].TaxDetail != null && roomDtls[0].TaxDetail.OutputVAT != null)
                    {
                        decimal totalHtAmount = 0, discount = 0;

                        decimal total = Math.Round(roomDtls.Sum(x => x.SellingFare + x.SellExtraGuestCharges + x.ChildCharges + x.Markup), 2);
                        var totMarkup = Math.Round(roomDtls.Sum(x => x.Markup), clsAgentMaster.DecimalValue);
                        discount = roomDtls.Sum(x => x.Discount);

                        totalHtAmount = clsAgentMaster.AgentType == (int)AgentType.Agent ? Math.Ceiling(total) : Math.Round(Math.Ceiling(total), clsAgentMaster.DecimalValue);
                        totMarkup = Math.Round(totMarkup, clsAgentMaster.DecimalValue);
                        outPutVat = roomDtls[0].TaxDetail.OutputVAT.CalculateVatAmount(totalHtAmount - discount, totMarkup, clsAgentMaster.DecimalValue);
                    }

                    foreach (HotelRoomsDetails room in roomDtls)
                    {
                        decimal roomOutPutVAT = 0m;
                        if (locationMaster.CountryCode == "IN")
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            roomOutPutVAT = GSTTaxDetail.LoadGSTValues(ref gstTaxList, room.Markup, iLocationId, loginInfo.UserID);
                        }
                        else if (outPutVat > 0)
                            roomOutPutVAT = Math.Round(outPutVat / roomDtls.Length, clsAgentMaster.DecimalValue);
                        
                        liRoomsOutPutVAT.Add(new { roomKey = room.roomRefKey, OutPutVAT = Math.Round(roomOutPutVAT, clsAgentMaster.DecimalValue) });
                    }

                    return await Task.FromResult(new ApplicationResponse(liRoomsOutPutVAT));
                }
                catch (ApiReqException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, loginInfo != null ? (int)loginInfo.UserID : -2,
                       "Hotel Api: GetRoomsOutPutVAT handler class. Error: " + ex.ErrorCode.ToString() + "-" + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    return new ApplicationResponse(liRoomsOutPutVAT);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, loginInfo != null ? (int)loginInfo.UserID : -2,
                       "Hotel Api: GetRoomsOutPutVAT handler class. Error: " + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    return new ApplicationResponse(liRoomsOutPutVAT);
                }
            }
        }
    }
}
