﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class GetRoomPolicy
    {
        /// <summary>
        /// Query request to get hotel room cancellation policy
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class RoomPolicyReq : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public List<string> RoomRefKeys { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<RoomPolicyReq, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(RoomPolicyReq clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine();

                try
                {
                    ValidateErrors.ApiReq(clsQuery.ApiReqInfo);

                    ValidateErrors.CancelReq(clsQuery.RoomRefKeys);

                    clsMS.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey);
                    clsMS.SessionId = clsQuery.ApiReqInfo.SSRefKey;

                    var cacheData = APIGenericStatic.GetHotelCache(clsMS.SessionId, "HotelSearchReq");
                    var hotelReq = cacheData != null && cacheData.Length > 0 ? (HotelRequest)CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    ValidateErrors.HotelSession(10002, hotelReq);

                    cacheData = APIGenericStatic.GetHotelCache(clsMS.SessionId, "RoomDetails");
                    var selResult = cacheData != null && cacheData.Length > 0 ? (HotelSearchResult)CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    ValidateErrors.HotelSession(10003, selResult);

                    var roomTypeCodes = new List<string>();

                    clsQuery.RoomRefKeys.ForEach(x => {

                        roomTypeCodes.Add(selResult.RoomDetails.Where(t => t.roomRefKey == x).Select(t => t.RoomTypeCode).FirstOrDefault());
                    });

                    var roomDtls = selResult.RoomDetails.Where(x => x.roomRefKey == clsQuery.RoomRefKeys[0]).Select(x => x).FirstOrDefault();
                    var sourceName = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), roomDtls.SupplierName);

                    var roomTypes = string.Join("^", roomTypeCodes.Distinct().ToArray()).Trim('^');
                    var cancelPolicy = clsMS.GetApiRoomsCancelPolicy(selResult, roomTypes, hotelReq, roomDtls.SequenceNo, sourceName);

                    List<HotelRoomPolicies> liPolicies = new List<HotelRoomPolicies>();
                    roomTypeCodes.ForEach(x => {

                        var roomInfo = selResult.RoomDetails.Where(y => y.RoomTypeCode == x).Select(p => p).FirstOrDefault();

                        if (!string.IsNullOrEmpty(roomInfo.RoomTypeCode))
                        {
                            ValidateErrors.CancelResp(roomInfo.CancellationPolicy);

                            var roomPolicy = new HotelRoomPolicies();
                            roomPolicy.RoomRefKey = roomInfo.roomRefKey;
                            roomPolicy.CancelPolicyDetails = roomInfo.CancelPolicyList;
                            roomPolicy.PolicyInfo = new List<RoomPolicyDetails>();

                            if (sourceName == HotelBookingSource.GIMMONIX)
                            {
                                if (!string.IsNullOrEmpty(roomInfo.EssentialInformation))
                                {
                                    roomInfo.EssentialInformation.Split('^').ToList().ForEach(m =>
                                    {
                                        roomPolicy.PolicyInfo.Add(new RoomPolicyDetails { Name = "Important Information", Message = m });
                                    });
                                }

                                var cancelMessages = roomInfo.CancellationPolicy.Split('^').ToList();
                                roomPolicy.CancelPolicyMessages = cancelMessages[0].Split('|').ToList();

                                if (cancelMessages.Count > 1)
                                    roomPolicy.CancelPolicyMessages.Add(cancelMessages[1]);
                            }
                            else if (sourceName == HotelBookingSource.UAH) {

                                if (!string.IsNullOrEmpty(roomInfo.EssentialInformation))
                                {
                                    roomInfo.EssentialInformation.Split(',').ToList().ForEach(m =>
                                    {
                                        var plInfo = m.Split('|');
                                        roomPolicy.PolicyInfo.Add(new RoomPolicyDetails { Name = plInfo[0], Message = plInfo[1] });
                                    });
                                }

                                roomPolicy.CancelPolicyMessages = roomInfo.CancellationPolicy.Split('^').ToList();
                            }
                            else if (sourceName == HotelBookingSource.HotelConnect)
                            {
                                if (!string.IsNullOrEmpty(roomInfo.EssentialInformation))
                                    roomPolicy.PolicyInfo.Add(new RoomPolicyDetails { Name = "Important Information", Message = roomInfo.EssentialInformation });

                                roomInfo.CancellationPolicy.Split('|').ToList().ForEach(c => {
                                    roomPolicy.CancelPolicyMessages = new List<string>();
                                    if (!string.IsNullOrEmpty(c))
                                        roomPolicy.CancelPolicyMessages.AddRange(c.Split(new string[] { "@@" }, StringSplitOptions.RemoveEmptyEntries));
                                });
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(roomInfo.EssentialInformation))
                                {
                                    roomInfo.EssentialInformation.Split('|').ToList().ForEach(m =>
                                    {
                                        roomPolicy.PolicyInfo.Add(new RoomPolicyDetails { Name = "Important Information", Message = m });
                                    });
                                }

                                roomPolicy.CancelPolicyMessages = roomInfo.CancellationPolicy.Split('|').ToList();
                            }

                            roomPolicy.PolicyInfo.Add(new RoomPolicyDetails { Name = "Disclaimer", Message = "Bed type availabilities depends @ Check-In time." });

                            liPolicies.Add(roomPolicy);
                        }
                    });

                    //APIGenericStatic.SaveHotelCache(clsQuery.ApiReqInfo.SSRefKey, selResult, "RoomPolicy");

                    return await Task.FromResult(new ApplicationResponse(liPolicies));
                }
                catch (ApiReqException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: GetRoomPolicy handler class. Error: " + ex.ErrorCode.ToString() + "-" + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError(ex.ErrorCode.ToString(), ex.Message);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: GetRoomPolicy handler class. Error: " + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError("Hotel ApiException", "Failed to get cancellation policy, please contact admin.");
                }
            }
        }
    }
}
