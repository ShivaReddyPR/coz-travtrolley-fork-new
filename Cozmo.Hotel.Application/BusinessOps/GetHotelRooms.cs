﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class GetHotelRooms
    {
        /// <summary>
        /// Query request to get hotel rooms data
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class HotelRoomsReq : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public string HotelRefKey { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<HotelRoomsReq, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(HotelRoomsReq clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine();

                try
                {
                    ValidateErrors.ApiReq(clsQuery.ApiReqInfo);

                    if (string.IsNullOrEmpty(clsQuery.HotelRefKey))
                        throw new ApiReqException("Invalid hotel reference key.");

                    clsMS.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey);
                    clsMS.SessionId = clsQuery.ApiReqInfo.SSRefKey;

                    var cacheData = APIGenericStatic.GetHotelCache(clsMS.SessionId, "HotelSearchReq");
                    var hotelReq = cacheData != null && cacheData.Length > 0 ? (HotelRequest)CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    ValidateErrors.HotelSession(1000, hotelReq);

                    cacheData = APIGenericStatic.GetHotelCache(clsMS.SessionId, "HotelResults");
                    var searchResults = cacheData != null && cacheData.Length > 0 ? (HotelSearchResult[])CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    ValidateErrors.HotelSession(1001, hotelReq);

                    var selResult = searchResults.Where(x => x != null && x.HotelRefKey == clsQuery.HotelRefKey).Select(x => x).FirstOrDefault();

                    var resultObj = clsMS.GetApiHotelRoomDetails(hotelReq, clsMS.SettingsLoginInfo.AgentId, clsQuery.ApiReqInfo.LocRefKey, selResult.HotelCode,
                        clsQuery.ApiReqInfo.SSRefKey, clsMS.SettingsLoginInfo, Convert.ToString((int)selResult.BookingSource));

                    ValidateErrors.HotelRooms(resultObj.RoomDetails);

                    resultObj.RoomDetails = resultObj.RoomDetails.Where(x => !string.IsNullOrEmpty(x.RoomTypeCode)).ToArray();

                    var supplierGroups = new Dictionary<string, string>();

                    for (int i = 0; i < resultObj.RoomDetails.Length; i++)
                    {
                        var typeInfo = resultObj.RoomDetails[i].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                        resultObj.RoomDetails[i].roomRefKey = typeInfo.Length > 1 ? typeInfo[1].Split('^')[0] : typeInfo[0];
                        resultObj.RoomDetails[i].gxRoomId = typeInfo.Length > 2 ? typeInfo[2] : string.Empty;
                        resultObj.RoomDetails[i].gxPkgName = typeInfo.Length > 3 ? typeInfo[3] : string.Empty;

                        if (resultObj.RoomDetails[i].SupplierName != "GIMMONIX" && !supplierGroups.ContainsKey(resultObj.RoomDetails[i].SupplierId))
                            supplierGroups.Add(resultObj.RoomDetails[i].SupplierId, Guid.NewGuid().ToString());

                        resultObj.RoomDetails[i].roomGroupRefKey = resultObj.RoomDetails[i].SupplierId.ToString() + "-" + 
                            (resultObj.RoomDetails[i].SupplierName == "GIMMONIX" ? resultObj.RoomDetails[i].roomRefKey : supplierGroups[resultObj.RoomDetails[i].SupplierId]);
                    }

                    if (resultObj != null && resultObj.RoomDetails != null && resultObj.RoomDetails.Length > 0)
                        APIGenericStatic.SaveHotelCache(clsQuery.ApiReqInfo.SSRefKey, resultObj, "RoomDetails");

                    return await Task.FromResult(
                        new ApplicationResponse(ApiCommon.IgnoreProps<HotelRoomsDetails>(resultObj.RoomDetails, (int)ProductType.Hotel, ApiObjects.RoomResult, clsMS.SettingsLoginInfo.AgentId))
                    );
                }
                catch (ApiReqException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: GetHotelRooms handler class. Error: " + ex.ErrorCode.ToString() + "-" + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError(ex.ErrorCode.ToString(), ex.Message);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: GetHotelRooms handler class. Error: " + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError("Hotel ApiException", "Failed to get hotel rooms details, please contact admin.");
                }
            }
        }
    }
}
