﻿using Cozmo.Api.Application.Models;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class GetPaymentPreference
    {
        /// <summary>
        /// Query request to get payment preferences
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo ApiAgentInfo { get; set; }

            public string PaymentPrefParams { get; set; }

            public string SessionId { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {                
                try
                {
                    if (string.IsNullOrEmpty(clsQuery.SessionId))
                        throw new Exception("Session Id missing.");

                    if (string.IsNullOrEmpty(clsQuery.PaymentPrefParams))
                        throw new Exception("Payment preference params missing.");

                    MetaSearchEngine clsMS = new MetaSearchEngine(clsQuery.SessionId);                    
                    string paymentPreferences = clsMS.GetPaymentPreference(clsQuery.PaymentPrefParams);

                    return await Task.FromResult(new ApplicationResponse(paymentPreferences));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelApi, CT.Core.Severity.High, -2, "Hotel Api: GetPaymentPreference handler class. Error: " + ex.ToString(), string.Empty);

                    return new ApplicationResponse().AddError("Hotel ApiException", ex.ToString());
                }
            }
        }
    }
}