﻿using Cozmo.Api.Application.Models;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class GetCorpTravelPolicy
    {
        /// <summary>
        /// Query request to update corporate travel policy to result room object
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiAgentInfo ApiAgentInfo { get; set; }

            public HotelRequest request { get; set; }

            public HotelSearchResult resultObj { get; set; }

            public string roomTypeCode { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {                
                try
                {
                    if (clsQuery.request == null)
                        throw new Exception("Hotel request object missing.");

                    if (clsQuery.resultObj == null)
                        throw new Exception("Hotel result object missing.");

                    MetaSearchEngine clsMS = new MetaSearchEngine();
                    clsMS.SessionId = clsQuery.resultObj.ApiSessionId;
                    int iRoomIndx = clsQuery.resultObj.RoomDetails.ToList().FindIndex(x => x.RoomTypeCode == clsQuery.roomTypeCode);
                    clsQuery.resultObj.RoomDetails[iRoomIndx].TravelPolicyResult = clsMS.GetHotelPolicyResult(clsQuery.request, clsQuery.resultObj, clsQuery.roomTypeCode);

                    return await Task.FromResult(new ApplicationResponse(new ViewModel(clsQuery.resultObj)));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelApi, CT.Core.Severity.High, -2, "Hotel Api: GetCorpTravelPolicy handler class. Error: " + ex.ToString(), string.Empty);

                    return new ApplicationResponse().AddError("Hotel ApiException", ex.ToString());
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(HotelSearchResult hotelSearchResult)
            {
                ResultObj = hotelSearchResult;
                CSessionId = hotelSearchResult.ApiSessionId;
                decimalValue = 2;
            }

            public HotelSearchResult ResultObj { get; set; }

            public string CSessionId { get; set; }

            public int decimalValue { get; set; }
        }
    }
}