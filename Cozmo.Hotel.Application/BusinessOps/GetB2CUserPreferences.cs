﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class GetB2CUserPreferences
    {
        /// <summary>
        /// Query request to get hotel search results
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public AuthenticationData AuthenticationInfo { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {                
                try
                {
                    if (clsQuery.AuthenticationInfo == null)
                        throw new Exception("Authentication info missing");

                    var preferences = APIGenericStatic.GetPreferences(clsQuery.AuthenticationInfo);

                    return await Task.FromResult(new ApplicationResponse(preferences));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelApi, CT.Core.Severity.High, -2,
                       "Hotel Api: GetB2CUserPreferences handler class. Error: " + ex.ToString(), clsQuery.AuthenticationInfo != null ? clsQuery.AuthenticationInfo.SiteName : string.Empty);

                    return new ApplicationResponse().AddError("Hotel ApiException", ex.ToString());
                }
            }
        }
    }
}