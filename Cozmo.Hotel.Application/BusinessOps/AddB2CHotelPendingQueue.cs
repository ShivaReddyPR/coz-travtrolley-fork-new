﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class AddB2CHotelPendingQueue
    {
        /// <summary>
        /// Query request to add hotel ititnerary to B2C Pending queue
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public HotelBookInfo HotelBookInfo { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            IMediator _mediator;

            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler(IMediator mediator)
            {
                _mediator = mediator;
            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine(clsQuery.ApiReqInfo.SSRefKey);

                try
                {
                    var hotelApiReq = new GetHotelItinerary.Query();
                    hotelApiReq.ApiReqInfo = clsQuery.ApiReqInfo;
                    hotelApiReq.HotelBookInfo = clsQuery.HotelBookInfo;

                    var result = await _mediator.Send(hotelApiReq);

                    if (result == null || (result.Errors != null && result.Errors.Count() > 0))
                        throw new Exception(result.Errors.Select(x => x.Value).FirstOrDefault());

                    var product = (Product)result.Result;
                    
                    clsMS.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey);
                    var penQueueId = clsMS.SaveB2CHotelPendingQueue((Product)result.Result, clsMS.SettingsLoginInfo.AgentId, clsMS.SettingsLoginInfo.UserID, clsQuery.ApiReqInfo.SSRefKey);

                    return await Task.FromResult(new ApplicationResponse(penQueueId));
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelApi, CT.Core.Severity.High, -2, "Hotel Api: AddB2CHotelPendingQueue handler class. Error: " + ex.ToString(), string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError("Hotel ApiException", ex.ToString());
                }
            }
        }
    }
}
