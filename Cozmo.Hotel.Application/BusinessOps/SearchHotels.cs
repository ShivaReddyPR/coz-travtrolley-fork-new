﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using MediatR;
using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class SearchHotels
    {
        /// <summary>
        /// Query request to get hotel search results
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class HotelSearchReq : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public HotelRequest ReqHotelSearch { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<HotelSearchReq, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(HotelSearchReq clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine();

                try
                {
                    ValidateErrors.ApiReq(clsQuery.ApiReqInfo);
                    if (!string.IsNullOrEmpty(clsQuery.ReqHotelSearch.CityCode) && int.TryParse(clsQuery.ReqHotelSearch.CityCode, out int f)) clsQuery.ReqHotelSearch.CityId = Convert.ToInt32(clsQuery.ReqHotelSearch.CityCode);
                    if (clsQuery.ReqHotelSearch.CityId > 0)
                    {
                        var cityInfo = HotelCity.GetCityInfoById(clsQuery.ReqHotelSearch.CityId);

                        if (cityInfo == null || string.IsNullOrEmpty(cityInfo.CityName))
                        {
                            clsQuery.ReqHotelSearch.CityId = 0;
                            clsQuery.ReqHotelSearch.CityName = string.Empty;
                        }
                        else
                        {
                            clsQuery.ReqHotelSearch.CityName = cityInfo.CityName;
                            clsQuery.ReqHotelSearch.CountryCode = cityInfo.CountryCode;
                        }
                    }

                    ValidateErrors.HotelReq(clsQuery.ReqHotelSearch);

                    var getFromCache = clsQuery.ApiReqInfo.SSRefKey.Split('^').Length > 1;
                    clsQuery.ApiReqInfo.SSRefKey = clsQuery.ApiReqInfo.SSRefKey.Split('^')[0];

                    clsMS.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey);
                    clsMS.SessionId = clsQuery.ApiReqInfo.SSRefKey;

                    clsQuery.ReqHotelSearch.LoginCountryCode = clsQuery.ApiReqInfo.LocRefKey > 0 ? APIGenericStatic.GetLoginCountry(clsQuery.ApiReqInfo.LocRefKey) :
                        clsMS.SettingsLoginInfo.LocationCountryCode;

                    try
                    {
                        clsQuery.ReqHotelSearch.CountryCode = string.IsNullOrEmpty(clsQuery.ReqHotelSearch.CountryCode) ?
                            CT.Core.Country.GetCountryCodeFromCountryName(clsQuery.ReqHotelSearch.CountryName) : clsQuery.ReqHotelSearch.CountryCode;

                        var liConfigs = ApiConfigs.GetApiConfigs(clsMS.SettingsLoginInfo.AgentId, 2, string.Empty, string.Empty);

                        if (liConfigs != null && liConfigs.Count > 0)
                        {
                            clsQuery.ReqHotelSearch.Transtype = liConfigs.Where(x => x.AC_AppKey.ToLower() == "transtype").Select(x => x.AC_AppValue).FirstOrDefault();

                            liConfigs = liConfigs.Where(x => x.AC_AppKey.ToLower() == "searchmaxtime" && int.TryParse(x.AC_AppValue, out int n)).ToList();
                            clsMS.searchMaxTime = liConfigs != null && liConfigs.Count > 0 ? liConfigs.Max(x => Convert.ToInt32(x.AC_AppValue)) : 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, clsMS.SettingsLoginInfo.AgentId,
                            "Hotel Api: SearchHotels handler class. Error:" + ex.ToString(), string.Empty);
                    }

                    if (clsQuery.ReqHotelSearch.Transtype != "B2B")
                    {
                        clsQuery.ReqHotelSearch.Corptraveler = "0";
                        clsQuery.ReqHotelSearch.Corptravelreason = string.Empty;
                    }

                    /* To get hotel sources for agent if not assigned to request on UI side */
                    if (clsQuery.ReqHotelSearch.Sources == null || clsQuery.ReqHotelSearch.Sources.Count == 0)
                    {
                        DataTable dtSources = AgentMaster.GetAgentSources(clsMS.SettingsLoginInfo.AgentId, 2, true);
                        clsQuery.ReqHotelSearch.Sources = dtSources != null && dtSources.Rows.Count > 0 ? dtSources.AsEnumerable().Select(r => r.Field<string>("Name")).ToList() :
                            clsQuery.ReqHotelSearch.Sources;
                    }

                    APIGenericStatic.SaveHotelCache(clsQuery.ApiReqInfo.SSRefKey, clsQuery.ReqHotelSearch, "HotelSearchReq");

                    HotelSearchResult[] results = null;

                    if (getFromCache)
                    {
                        var cacheData = APIGenericStatic.GetHotelCache(clsMS.SessionId, "HotelResults");
                        results = cacheData != null && cacheData.Length > 0 ? (HotelSearchResult[])CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;
                    }

                    if (results == null)
                    {
                        results = clsMS.GetHotelResults(clsQuery.ReqHotelSearch, clsMS.SettingsLoginInfo.AgentId);

                        if (results != null && results.Length > 0)
                            APIGenericStatic.SaveHotelCache(clsQuery.ApiReqInfo.SSRefKey, results, "HotelResults");
                    }

                    return await Task.FromResult(
                        new ApplicationResponse(ApiCommon.IgnoreProps<HotelSearchResult>(results, (int)ProductType.Hotel, ApiObjects.HotelSearchResult, clsMS.SettingsLoginInfo.AgentId))
                    );
                }
                catch (ApiReqException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: SearchHotels handler class. Error: " + ex.ErrorCode.ToString() + "-" + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError(ex.ErrorCode.ToString(), ex.Message);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: SearchHotels handler class. Error: " + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError("Hotel ApiException", "Failed to get hotel search results, please contact admin.");
                }
            }
        }
    }
}
