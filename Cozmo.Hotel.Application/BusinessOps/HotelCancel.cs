﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using CT.AccountingEngine;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using MediatR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class HotelCancel
    {
        /// <summary>
        /// Query request to cancel hotel
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class HotelCancelReq : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public string HotelConfirmNo { get; set; }

            public List<string> RoomConfirmNos { get; set; }

            public string TransType { get; set; }

            public decimal AFee { get; set; }

            public decimal SFee { get; set; }

            public int SRRequestId { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<HotelCancelReq, ApplicationResponse>
        {
            IMediator _mediator;

            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler(IMediator mediator)
            {
                _mediator = mediator;
            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(HotelCancelReq clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine();

                try
                {
                    ValidateErrors.ApiReq(clsQuery.ApiReqInfo);

                    ValidateErrors.CancelConfirmNo(clsQuery.HotelConfirmNo);

                    clsMS.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey);
                    clsMS.SessionId = clsQuery.ApiReqInfo.SSRefKey;

                    var itinerary = MetaSearchEngine.GetHotelItineraryByConfNo(clsQuery.HotelConfirmNo);

                    ValidateErrors.CancelItinerary(itinerary);

                    string cancelRefNo = string.Empty;
                    string cancelSource = string.Empty;

                    if (itinerary.Source == HotelBookingSource.GIMMONIX)
                    {
                        cancelRefNo = itinerary.BookingRefNo;
                        cancelSource = "GIMMONIX";
                    }

                    if (itinerary.Source == HotelBookingSource.UAH)
                    {
                        cancelSource = "UAH";
                        cancelRefNo = itinerary.ConfirmationNo + "|" + itinerary.BookingRefNo;  //"DHB190430162048432|3900673|3871174" //
                    }

                    if (itinerary.Source == HotelBookingSource.RezLive)
                    {
                        cancelSource = "RezLive";
                        cancelRefNo = itinerary.ConfirmationNo + "|" + itinerary.BookingRefNo;  //"DHB190430162048432|3900673|3871174" //
                    }

                    if (itinerary.Source == HotelBookingSource.Illusions)
                    {
                        cancelSource = "Illusions";
                        string roomKey = string.Join("|", itinerary.Roomtype.Select(x => x.RoomId + "-" + x.RoomTypeCode.Split('-')[3]).ToArray());
                        cancelRefNo = itinerary.HotelCode.Split('-')[0] + "|" + itinerary.ConfirmationNo + "^" + itinerary.BookingRefNo + "^" + roomKey + "^" + itinerary.HotelId;  //"32|7729^7729/1|7729/2^3077-1|3078-2^2477" //
                    }

                    var cancelInfo = clsMS.GetBookingCancelInfo(cancelSource, cancelRefNo);

                    HotelCancelResponse clsRsp = new HotelCancelResponse();

                    clsRsp.CancelStatus = cancelInfo.ContainsKey("Status") && cancelInfo["Status"].ToUpper() != "FAILED" ? cancelInfo["Status"] :
                        cancelInfo.ContainsKey("status") && cancelInfo["status"].ToUpper() != "FAILED" ? cancelInfo["status"] : "Cancellation Failed";

                    if (clsRsp.CancelStatus.ToUpper() != "CANCELLED")
                        return await Task.FromResult(new ApplicationResponse(clsRsp));

                    clsRsp.HotelConfirmNo = clsQuery.HotelConfirmNo;

                    clsRsp.CancelRefNo = cancelInfo.ContainsKey("ID") ? cancelInfo["ID"] : cancelInfo.ContainsKey("id") ? cancelInfo["id"] : string.Empty;
                    var cancelAmount = cancelInfo.ContainsKey("Amount") ? Convert.ToDecimal(cancelInfo["Amount"]) : cancelInfo.ContainsKey("amount") ? Convert.ToDecimal(cancelInfo["amount"]) : 0;                    
                    var cancelCurrency = cancelInfo.ContainsKey("Currency") ? cancelInfo["Currency"] : cancelInfo.ContainsKey("currency") ? cancelInfo["currency"] : string.Empty;

                    itinerary.Status = HotelBookingStatus.Cancelled;
                    itinerary.CancelId = itinerary.Source == HotelBookingSource.RezLive ? itinerary.ConfirmationNo : clsRsp.CancelRefNo;
                    itinerary.Update();

                    ServiceRequest sr = new ServiceRequest();                    
                    CancellationCharges cancellationCharge = new CancellationCharges();
                    cancellationCharge.AdminFee = Convert.ToDecimal(clsQuery.AFee);
                    cancellationCharge.SupplierFee = Convert.ToDecimal(clsQuery.SFee);
                    cancellationCharge.PaymentDetailId = 0;
                    cancellationCharge.ReferenceId = itinerary.Roomtype[0].RoomId;
                    cancellationCharge.CancelPenalty = cancelAmount;

                    decimal exchangeRate = 1;
                    var bookingAmt = itinerary.Roomtype.Where(x => x.Price != null && x.Price.NetFare > 0).Sum(x => x.Price.NetFare + x.Price.Markup + x.Price.B2CMarkup);

                    if (!string.IsNullOrEmpty(cancelCurrency) && cancelCurrency != clsMS.SettingsLoginInfo.Currency)
                    {
                        CT.Core.StaticData staticInfo = new CT.Core.StaticData();
                        staticInfo.BaseCurrency = clsMS.SettingsLoginInfo.Currency;

                        Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                        exchangeRate = rateOfExList[cancelCurrency];
                        cancellationCharge.CancelPenalty = cancelAmount * exchangeRate;
                    }

                    cancellationCharge.CreatedBy = (int)clsMS.SettingsLoginInfo.UserID;
                    cancellationCharge.ProductType = ProductType.Hotel;
                    cancellationCharge.Save();

                    if (cancelAmount > 0)
                    {
                        if (exchangeRate == 1)
                            bookingAmt -= Math.Ceiling(cancelAmount);
                        else if (itinerary.Source != HotelBookingSource.HotelConnect)
                            bookingAmt -= Math.Ceiling(cancelAmount * exchangeRate);
                    }
                                        
                    bookingAmt = Math.Ceiling(bookingAmt);

                    // Admin & Supplier Fee save in Leadger
                    int invoiceNumber = 0;
                    decimal adminFee = Math.Ceiling(clsQuery.SFee + clsQuery.AFee) < bookingAmt ? Math.Ceiling(clsQuery.SFee + clsQuery.AFee) : 0;

                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                    NarrationBuilder objNarration = new NarrationBuilder();
                    invoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                    
                    ledgerTxn.LedgerId = itinerary.AgencyId;
                    ledgerTxn.Amount = -adminFee;
                    objNarration.HotelConfirmationNo = itinerary.ConfirmationNo.Replace('|', '@');
                    objNarration.TravelDate = itinerary.StartDate.ToShortDateString();

                    if (itinerary.PaymentMode == ModeOfPayment.CreditCard) //Checking card or credit
                    {
                        ledgerTxn.ReferenceType = ReferenceType.CardHotelCancellationCharge;
                        objNarration.Remarks = "Card Hotel Cancellation Charges";
                    }
                    else
                    {
                        ledgerTxn.ReferenceType = ReferenceType.HotelCancellationCharge;
                        objNarration.Remarks = "Hotel Cancellation Charges";
                    }

                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.IsLCC = true;
                    ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;

                    ledgerTxn.Notes = string.Empty;
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = (int)clsMS.SettingsLoginInfo.UserID;
                    ledgerTxn.TransType = itinerary.TransType;
                    ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                    ledgerTxn.Save();
                    LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                    //save Refund amount
                    ledgerTxn = new LedgerTransaction();
                    ledgerTxn.LedgerId = itinerary.AgencyId;
                    ledgerTxn.Amount = bookingAmt;
                    objNarration.PaxName = string.Format("{0} {1}", itinerary.Roomtype[0].PassenegerInfo[0].Firstname, itinerary.Roomtype[0].PassenegerInfo[0].Lastname);

                    if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
                    {
                        ledgerTxn.ReferenceType = ReferenceType.CardHotelRefund;
                        ledgerTxn.Notes = "Card Hotel Voucher Refunded";
                        objNarration.Remarks = "Card Refunded for Voucher No -" + itinerary.ConfirmationNo.Replace('|', '@');///modified by brahmam MIKI purpose
                    }
                    else
                    {
                        ledgerTxn.ReferenceType = ReferenceType.HotelRefund;
                        ledgerTxn.Notes = "Hotel Voucher Refunded";
                        objNarration.Remarks = "Refunded for Voucher No -" + itinerary.ConfirmationNo.Replace('|', '@');///modified by brahmam MIKI purpose
                    }

                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.IsLCC = true;
                    ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;

                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = (int)clsMS.SettingsLoginInfo.UserID;
                    ledgerTxn.TransType = itinerary.TransType;
                    ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                    ledgerTxn.Save();
                    LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);
                    
                    bookingAmt -= adminFee <= bookingAmt ? adminFee : 0;

                    AgentMaster clsAM = new AgentMaster(itinerary.AgencyId);
                    if (itinerary.PaymentMode != ModeOfPayment.CreditCard)
                    {
                        if (itinerary.TransType != "B2C")
                        {
                            clsAM.CreatedBy = (int)clsMS.SettingsLoginInfo.UserID;
                            clsAM.UpdateBalance(bookingAmt);
                        }
                    }

                    //Update Queue Status
                    if (clsQuery.SRRequestId > 0)
                    {
                        CT.Core.Queue.SetStatus(CT.Core.QueueType.Request, clsQuery.SRRequestId, CT.Core.QueueStatus.Completed, (int)clsMS.SettingsLoginInfo.UserID, 0, "Completed");
                        sr.UpdateServiceRequestAssignment(clsQuery.SRRequestId, (int)ServiceRequestStatus.Completed, (int)clsMS.SettingsLoginInfo.UserID, (int)ServiceRequestStatus.Completed, null);
                    }

                    //Sending Email.
                    try
                    {
                        Hashtable table = new Hashtable();
                        table.Add("agentName", clsAM.Name);
                        table.Add("hotelName", itinerary.HotelName);
                        table.Add("confirmationNo", itinerary.ConfirmationNo);

                        List<string> toArray = new List<string>();
                        UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itinerary.AgencyId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);

                        var cancelMails = Convert.ToString(ConfigurationManager.AppSettings["HOTEL_CANCEL_MAIL"]);

                        if (!string.IsNullOrEmpty(cancelMails))
                            toArray.AddRange(cancelMails.Split(';'));

                        string message = ConfigurationManager.AppSettings["HOTEL_REFUND"];

                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray.Where(x => !string.IsNullOrEmpty(x)).ToList(),
                            "(Hotel)Response for cancellation. Confirmation No:(" + itinerary.ConfirmationNo + ")", message, table);
                    }
                    catch (Exception ex)
                    {

                    }

                    clsRsp.RefundAmount = bookingAmt;

                    return await Task.FromResult(new ApplicationResponse(clsRsp));
                }
                catch (ApiReqException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: HotelCancel handler class. Error: " + ex.ErrorCode.ToString() + "-" + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError(ex.ErrorCode.ToString(), ex.Message);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: HotelCancel handler class. Error: " + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError("Hotel ApiException", "Failed to cancel hotel, please contact admin.");
                }
            }
        }
    }
}
