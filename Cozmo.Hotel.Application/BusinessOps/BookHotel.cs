﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.Models;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Hotel.Application.BusinessOps
{
    public class BookHotel
    {
        /// <summary>
        /// Query request to book hotel
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class BookHotelReq : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public HotelBookInfo HotelBookInfo { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<BookHotelReq, ApplicationResponse>
        {
            IMediator _mediator;

            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler(IMediator mediator)
            {
                _mediator = mediator;
            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(BookHotelReq clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine();

                try
                {
                    ValidateErrors.ApiReq(clsQuery.ApiReqInfo);
                    
                    clsMS.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey);
                    clsMS.SessionId = clsQuery.ApiReqInfo.SSRefKey;

                    var hotelApiReq = new GetHotelItinerary.Query();
                    hotelApiReq.ApiReqInfo = clsQuery.ApiReqInfo;
                    hotelApiReq.HotelBookInfo = clsQuery.HotelBookInfo;

                    var result = await _mediator.Send(hotelApiReq);

                    if (result == null || (result.Errors != null && result.Errors.Count() > 0))
                        throw new Exception(result.Errors.Select(x => x.Value).FirstOrDefault());

                    var product = (Product)result.Result;
                    var bookRes = clsMS.Book(ref product, clsMS.SettingsLoginInfo.AgentId, BookingStatus.Ready, clsMS.SettingsLoginInfo.UserID);

                    var hotelBookResp = new HotelBookResponse();

                    if (bookRes.Status == BookingResponseStatus.Successful)
                    {
                        hotelBookResp.ConfirmationNo = bookRes.ConfirmationNo.Split('|')[0];
                        var itinerary = MetaSearchEngine.GetHotelItineraryByConfNo(hotelBookResp.ConfirmationNo);
                        hotelBookResp.BookingId = bookRes.BookingId;
                        hotelBookResp.BookingRefNo = itinerary.AgentRef;
                        hotelBookResp.B2CRefId = bookRes.B2CRefId;

                        if (itinerary.Source == HotelBookingSource.Illusions && itinerary.Roomtype != null)
                        {
                            hotelBookResp.RoomsStatus = new List<HotelRoomStatus>();

                            for (int i = 0; i < itinerary.Roomtype.Length; i++)
                            {
                                hotelBookResp.RoomsStatus.Add(new HotelRoomStatus
                                {
                                    roomNo = i + 1,
                                    confirmNumber = string.IsNullOrEmpty(itinerary.Roomtype[i].RoomwiseConfirmNo) ? hotelBookResp.ConfirmationNo + "/" + (i + 1).ToString() :
                                        itinerary.Roomtype[i].RoomwiseConfirmNo,
                                    confirmStatus = string.IsNullOrEmpty(itinerary.Roomtype[i].RoomwiseConfirmStatus) ? "Confirmed" : itinerary.Roomtype[i].RoomwiseConfirmStatus
                                });
                            }
                        }
                    }
                    else
                        return new ApplicationResponse().AddError(string.Empty, "BookError: " + bookRes.Error);

                    return await Task.FromResult(
                        new ApplicationResponse(ApiCommon.IgnoreProps<HotelBookResponse>(hotelBookResp, (int)ProductType.Hotel, ApiObjects.BookResponse, clsMS.SettingsLoginInfo.AgentId))
                    );
                }
                catch (ApiReqException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: BookHotel handler class. Error: " + ex.ErrorCode.ToString() + "-" + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError(ex.ErrorCode.ToString(), ex.Message);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.ExpenseApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -2,
                       "Hotel Api: BookHotel handler class. Error: " + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail(ProductType.Hotel.ToString(), ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError("Hotel ApiException", "Failed to book hotel, please contact admin.");
                }
            }
        }
    }
}
