﻿using Cozmo.Api.Application.Models;
using Cozmo.Hotel.Application.BusinessOps;
using Cozmo.Hotel.Application.Models;
using MediatR;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Cozmo.Hotel.Api.Controllers
{
    /// <summary>
    /// Hotel controller to get/book hotels from various api sources
    /// </summary>
    [Authorize]
    [RoutePrefix("api/hotel")]
    public class HotelController : ApiController
    {
        private readonly IMediator _mediator;

        private readonly string _traceId;

        private readonly string _reqIP;

        /// <summary>
        /// Hotel constructor
        /// </summary>
        /// <param name="mediator"></param>
        public HotelController(IMediator mediator)
        {
            _mediator = mediator;
            var KeyStartPos = System.Web.HttpContext.Current.Request.Headers.ToString().IndexOf("&Authorization=");
            _traceId = System.Web.HttpContext.Current.Request.Headers.ToString().Substring(KeyStartPos + 15, 43).Replace("Bearer+", string.Empty).Trim();
            _reqIP = System.Web.HttpContext.Current.Request.UserHostAddress;
        }

        /// <summary>
        /// Gets the hotel results using the search request
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("searchHotels")]
        [ResponseType(typeof(SearchHotels))]
        public async Task<IHttpActionResult> SearchFlights([FromBody]SearchHotels.HotelSearchReq hotelApiReq)
        {
            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.SearchFlightsReq, JsonConvert.SerializeObject(hotelApiReq), _reqIP);

            hotelApiReq.ApiReqInfo.SSRefKey = _traceId + (string.IsNullOrEmpty(hotelApiReq.ApiReqInfo.SSRefKey) ? string.Empty : "^" + hotelApiReq.ApiReqInfo.SSRefKey);
            hotelApiReq.ApiReqInfo.SourceIP = _reqIP;

            var result = await _mediator.Send(hotelApiReq);

            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.SearchFlightsResp, 
                result.Errors.Count() > 0 ? "Error : " + JsonConvert.SerializeObject(result.Errors) : "Results sent", _reqIP);

            return Ok(result);
        }

        /// <summary>
        /// Gets the hotel rooms info
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("getHotelRooms")]
        [ResponseType(typeof(GetHotelRooms))]
        public async Task<IHttpActionResult> GetHotelRooms([FromBody]GetHotelRooms.HotelRoomsReq hotelApiReq)
        {
            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.GetHotelRoomsReq, JsonConvert.SerializeObject(hotelApiReq), _reqIP);

            hotelApiReq.ApiReqInfo.SSRefKey = _traceId;
            hotelApiReq.ApiReqInfo.SourceIP = _reqIP;

            var result = await _mediator.Send(hotelApiReq);

            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.GetHotelRoomsResp, JsonConvert.SerializeObject(result), _reqIP);

            return Ok(result);
        }

        /// <summary>
        /// Gets the hotel rooms info
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("getRoomPolicy")]
        public async Task<IHttpActionResult> GetRoomPolicy([FromBody]GetRoomPolicy.RoomPolicyReq hotelApiReq)
        {
            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.GetRoomPolicyReq, JsonConvert.SerializeObject(hotelApiReq), _reqIP);

            hotelApiReq.ApiReqInfo.SSRefKey = _traceId;
            hotelApiReq.ApiReqInfo.SourceIP = _reqIP;

            var result = await _mediator.Send(hotelApiReq);

            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.GetRoomPolicyResp, JsonConvert.SerializeObject(result), _reqIP);

            return Ok(result);
        }

        /// <summary>
        /// Books hotel at supplier based on selected hotel and guest info
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("bookHotel")]
        public async Task<IHttpActionResult> BookHotel([FromBody]BookHotel.BookHotelReq hotelApiReq)
        {
            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.BookHotelReq, JsonConvert.SerializeObject(hotelApiReq), _reqIP);

            hotelApiReq.ApiReqInfo.SSRefKey = _traceId;
            hotelApiReq.ApiReqInfo.SourceIP = _reqIP;

            var result = await _mediator.Send(hotelApiReq);

            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.BookHotelResp, JsonConvert.SerializeObject(result), _reqIP);

            return Ok(result);
        }

        /// <summary>
        /// Cancel hotel at supplier based on hotel confirmation no
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("hotelCancel")]
        public async Task<IHttpActionResult> HotelCancel([FromBody]HotelCancel.HotelCancelReq hotelApiReq)
        {
            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.HotelCancelReq, JsonConvert.SerializeObject(hotelApiReq), _reqIP);

            hotelApiReq.ApiReqInfo.SSRefKey = _traceId;
            hotelApiReq.ApiReqInfo.SourceIP = _reqIP;

            var result = await _mediator.Send(hotelApiReq);

            ApiLogs.Save(hotelApiReq.ApiReqInfo.AgentRefKey, _traceId, 2, (int)HotelBookApiCall.HotelCancelResp, JsonConvert.SerializeObject(result), _reqIP);

            return Ok(result);
        }
    }
}
