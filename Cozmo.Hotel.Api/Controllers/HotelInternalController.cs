﻿using Cozmo.Hotel.Application.BusinessOps;
using MediatR;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cozmo.Hotel.Api.Controllers
{
    /// <summary>
    /// Hotel internal controller to get hotel internal info for cozmo UI apps
    /// </summary>
    [RoutePrefix("api/hotelInternal")]
    public class HotelInternalController : ApiController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Hotel Internal constructor
        /// </summary>
        /// <param name="mediator"></param>
        public HotelInternalController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Gets the user preferences for B2C hotel UI app
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("getB2CUserPreferences")]
        public async Task<IHttpActionResult> GetB2CUserPreferences([FromBody]GetB2CUserPreferences.Query hotelApiReq)
        {
            var result = await _mediator.Send(hotelApiReq);

            return Ok(result);
        }

        /// <summary>
        /// To update corporate travel policy for given hotel result room object
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("getCorpTravelPolicy")]
        public async Task<IHttpActionResult> GetCorpTravelPolicy([FromBody]GetCorpTravelPolicy.Query hotelApiReq)
        {
            var result = await _mediator.Send(hotelApiReq);

            return Ok(result);
        }

        /// <summary>
        /// Gets the payment preferences
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("getPaymentPreference")]
        public async Task<IHttpActionResult> GetPaymentPreference([FromBody]GetPaymentPreference.Query hotelApiReq)
        {
            var result = await _mediator.Send(hotelApiReq);

            return Ok(result);
        }

        /// <summary>
        /// To get out put vat for rooms
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("getRoomsOutPutVAT")]
        public async Task<IHttpActionResult> GetRoomsOutPutVAT([FromBody]GetRoomsOutPutVAT.RoomsOutPutVATReq hotelApiReq)
        {
            var result = await _mediator.Send(hotelApiReq);

            return Ok(result);
        }

        /// <summary>
        /// To add itinerary object to B2C pending queue
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("addB2CHotelPendingQueue")]
        public async Task<IHttpActionResult> AddB2CHotelPendingQueue([FromBody]AddB2CHotelPendingQueue.Query hotelApiReq)
        {
            var result = await _mediator.Send(hotelApiReq);

            return Ok(result);
        }

        /// <summary>
        /// Gets itinerary object using request and result objects
        /// </summary>
        /// <param name="hotelApiReq"></param>
        [HttpPost, Route("getHotelItinerary")]
        public async Task<IHttpActionResult> GetHotelItinerary([FromBody]GetHotelItinerary.Query hotelApiReq)
        {
            var result = await _mediator.Send(hotelApiReq);

            return Ok(result);
        }
    }
}
