﻿using System;
using System.IO;
using System.Xml;
using com.ni.dp.util;
using CT.Configuration;
using CT.Core;

namespace NetworkInternationalPay
{
    [Serializable]
    public class NetworkInternationalPaymentService
    {
        /// <summary>
        /// Transaction Request Fields
        /// </summary>
        string xmlPath;//Logs Path.
        long merchantID;
        string merchantEncryptionKey;
        string merchantOrderNumber;
        string currency;
        decimal amount;
        string successURL;
        string failureURL;
        string transactionType;
        string transactionMode;
        string payModeType;
        int creditCardNumber;
        string cardVerificationValue;
        string expiryMonth;
        string expiryYear;
        string cardType;
        string billToFirstName;
        string billToLastName;
        string billToStreet1;
        string billToStreet2;
        string billToCity;
        string billToState;
        string billToPostalCode;
        string billToCountry;
        string billToEmail;
        string billToPhoneNumber1;
        string billToPhoneNumber2;
        string billToPhoneNumber3;
        string billToMobileNumber;
        string gatewayID;
        string customerID;
        string shipToFirstName;
        string shipToLastName;
        string shipToStreet1;
        string shipToStreet2;
        string shipToCity;
        string shipToState;
        string shipToPostalCode;
        string shipToCountry;
        string shipToPhoneNumber1;
        string shipToPhoneNumber2;
        string shipToPhoneNumber3;
        string shipToMobileNumber;
        string transactionSource;
        string productInfo;
        string isUserLoggedIn;
        string itemTotal;
        string itemCategory;
        string ignoreValidationResult;
        string udf1;
        string udf2;
        string udf3;
        string udf4;
        string udf5;

        #region Properties
        /// <summary>
        /// Unique merchant ID created at NI Online for every merchant at the time of registration. This is mandatory for every transaction. Any character other than the Merchant ID will not be accepted. Ensure that there is no space in between, before or after the field.
        /// </summary>
        public long MerchantID
        {
            get { return merchantID; }
            set { merchantID = value; }
        }

        public string MerchantEncryptionKey
        {
            get { return merchantEncryptionKey; }
            set { merchantEncryptionKey = value; }
        }

        /// <summary>
        /// Your order number for each transaction. Special characters that are defined in the document are allowed.For example, asdf123 OR 1234 OR ABCD-1234.
        /// </summary>
        public string MerchantOrderNumber
        {
            get { return merchantOrderNumber; }
            set { merchantOrderNumber = value; }
        }
        /// <summary>
        /// Use to specify currency for the transaction. Currently only AED is accepted for all transactions (As per ISO Currency Code).
        /// </summary>
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        /// <summary>
        /// Price : Values up to 2 decimal places are allowed. For example, 1000.02.
        /// </summary>
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        /// <summary>
        /// Merchant success URL, where the customer will be redirected if the transaction is successful.
        /// </summary>
        public string SuccessURL
        {
            get { return successURL; }
            set { successURL = value; }
        }

        /// <summary>
        /// Merchant failure URL, where the customer will be redirected if the transaction is failure.
        /// </summary>
        public string FailureURL
        {
            get { return failureURL; }
            set { failureURL = value; }
        }
        /// <summary>
        /// 01 for SALE 02 for AUTHROIZATION -- We use only Sale Request.
        /// </summary>
        public string TransactionType
        {
            get { return transactionType; }
            set { transactionType = value; }
        }

        /// <summary>
        /// Type of Transaction (Internet, Moto, and Recurring). Default Value: INTERNET If the facility of Skip Authentication is offered via browser re-direct then MOTO needs to be sent in the request.
        /// </summary>
        public string TransactionMode
        {
            get { return transactionMode; }
            set { transactionMode = value; }
        }

        /// <summary>
        /// (CC)-Credit Card, (DC)-Debit Card, (DD)-Direct Debit -- We have to pass "CC"
        /// </summary>
        public string PayModeType
        {
            get { return payModeType; }
            set { payModeType = value; }
        }

        /// <summary>
        /// Customer Credit or Debit card number. Maxlength – 16 digits. Only merchants whose integration type is “Merchant Hosted” will send this information.
        /// </summary>
        public int CreditCardNumber
        {
            get { return creditCardNumber; }
            set { creditCardNumber = value; }
        }

        /// <summary>
        /// Card Verification Value (CVV) 3 digits - All Visa and MasterCard cards 4 digit - for AMEX cardsOnly merchants whose integration type is “Merchant Hosted” will send this information.
        /// </summary>
        public string CardVerificationValue
        {
            get { return cardVerificationValue; }
            set { cardVerificationValue = value; }
        }
        /// <summary>
        /// Two-digit month when card expires. Format: MM (For example, 01 for Jan & 03 for March) Only merchants whose integration type is “Merchant Hosted” will send this information.
        /// </summary>
        public string ExpiryMonth
        {
            get { return expiryMonth; }
            set { expiryMonth = value; }
        }

        /// <summary>
        /// Four-digit year when card expires. Format: YYYY (For example, 2014 OR 2016). should be >= current yearOnly merchants whose integration type is “Merchant Hosted” will send this information.
        /// </summary>
        public string ExpiryYear
        {
            get { return expiryYear; }
            set { expiryYear = value; }
        }
        /// <summary>
        /// Type of Card Possible values: MASTERCARD, VISA, DINERS, AMEX, JCB Only merchants whose integration type is “Merchant Hosted” will send this information.
        /// </summary>
        public string CardType
        {
            get { return cardType; }
            set { cardType = value; }
        }
        /// <summary>
        /// Card Holder’s first name as printed on the card. Special characters that are defined in the document are allowed.Important: This string will accept either alphabetic or a combination of alphabetic and special characters as defined in the document.
        /// </summary>
        public string BillToFirstName
        {
            get { return billToFirstName; }
            set { billToFirstName = value; }
        }

        /// <summary>
        /// Card Holder’s last name as printed on the card. Special characters that are defined in the document are allowed. Important: This string will accept either alphabetic or a combination of alphabetic and special characters as defined in the document.
        /// </summary>
        public string BillToLastName
        {
            get { return billToLastName; }
            set { billToLastName = value; }
        }

        /// <summary>
        /// Customer address. Special characters that are defined in the document are allowed.
        /// </summary>
        public string BillToStreet1
        {
            get { return billToStreet1; }
            set { billToStreet1 = value; }
        }

        /// <summary>
        /// Additional address information. Special characters that are defined in the document are allowed.
        /// </summary>
        public string BillToStreet2
        {
            get { return billToStreet2; }
            set { billToStreet2 = value; }
        }

        /// <summary>
        /// City of the billing address. This is NOT a numeric field and special characters are NOT allowed EXCEPT Space.
        /// </summary>
        public string BillToCity
        {
            get { return billToCity; }
            set { billToCity = value; }
        }

        /// <summary>
        /// State or province of the billing address. Use the State, Province, and Territory Codes for the United States and Canada. This is NOT a numeric field and special characters are NOT allowed EXCEPT (Space).
        /// </summary>
        public string BillToState
        {
            get { return billToState; }
            set { billToState = value; }
        }
        /// <summary>
        /// Postal code for the billing address. The field is an Alphanumeric field, special characters NOT allowed EXCEPT space. For example, abc123 OR abc 12 OR 1234
        /// </summary>
        public string BillToPostalCode
        {
            get { return billToPostalCode; }
            set { billToPostalCode = value; }
        }
        /// <summary>
        /// Country of the billing address. Use the two-character ISO Standard Country Codes.
        /// </summary>
        public string BillToCountry
        {
            get { return billToCountry; }
            set { billToCountry = value; }
        }
        /// <summary>
        /// Customer email address, including the full domain name. For example, abc@gmail.com
        /// </summary>
        public string BillToEmail
        {
            get { return billToEmail; }
            set { billToEmail = value; }
        }

        /// <summary>
        /// Country code of the customer landline phone. If the customer does not enter the country code then set default value: 99 or send this field as blank as it is optional.
        /// </summary>
        public string BillToPhoneNumber1
        {
            get { return billToPhoneNumber1; }
            set { billToPhoneNumber1 = value; }
        }
        /// <summary>
        /// Area code of the customer landline phone. If the customer does not enter the area code then set default value: 99 or send this field as blank as it is optional.
        /// </summary>
        public string BillToPhoneNumber2
        {
            get { return billToPhoneNumber2; }
            set { billToPhoneNumber2 = value; }
        }
        /// <summary>
        /// Landline number of the customer. If the customer does not enter the landline number then set default value: 999999 or send this field as blank as it is optional.
        /// </summary>
        public string BillToPhoneNumber3
        {
            get { return billToPhoneNumber3; }
            set { billToPhoneNumber3 = value; }
        }
        /// <summary>
        /// Mobile number of the customer. If the customer enters the mobile number at your end please send the value to this particular field. Possible accepted values: +971-99999-9999 or 9999999999 Mobile number must NOT have a SPACE.
        /// </summary>
        public string BillToMobileNumber
        {
            get { return billToMobileNumber; }
            set { billToMobileNumber = value; }
        }
        /// <summary>
        /// When multiple PGs are assigned by Network Online to the merchant, the merchant will be given the values for each GatewayID by Network Online. This is applicable only for merchants opting for “Merchant Hosted” Integration. The assigned gateway ID needs to be passed in the request.Default Value: Blank
        /// </summary>
        public string GatewayID
        {
            get { return gatewayID; }
            set { gatewayID = value; }
        }

        /// <summary>
        /// Your identifier assigned to your customer when their profile was created. This field has to be passed by you only if you have opted for the Tokenization/Stored Card feature. The field is Alphanumeric, special characters are NOT allowed.
        /// </summary>
        public string CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        /// <summary>
        /// First Name of the receiver to whom the product will be shipped to. Special characters that are defined in the document are allowed.Important: This string will accept either alphabetic or a combination of alphabetic and special characters as defined in the document.For merchants opting for “Merchant Hosted” Integration, this field is not validated..
        /// </summary>
        public string ShipToFirstName
        {
            get { return shipToFirstName; }
            set { shipToFirstName = value; }
        }
        /// <summary>
        /// Last Name of the receiver to whom the product will be shipped to. Special characters that are defined in the document are allowed. Important: This string will accept either alphabetic or a combination of alphabetic and special characters as defined in the document. For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToLastName
        {
            get { return shipToLastName; }
            set { shipToLastName = value; }
        }
        /// <summary>
        /// First line of the street address where the product needs to be delivered to. Special characters that are defined in the document are allowed.For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToStreet1
        {
            get { return shipToStreet1; }
            set { shipToStreet1 = value; }
        }
        /// <summary>
        /// Additional address information for shipping the product. Special characters that are defined in the document are allowed. For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToStreet2
        {
            get { return shipToStreet2; }
            set { shipToStreet2 = value; }
        }

        /// <summary>
        /// City of the Shipping address. This is NOT a numeric field and special characters are NOT allowed EXCEPT space.For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToCity
        {
            get { return shipToCity; }
            set { shipToCity = value; }
        }

        /// <summary>
        /// State, Province, and Territory Codes for the Shipping Address. This is NOT a numeric field and Special characters are NOT allowed EXCEPT space. For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToState
        {
            get { return shipToState; }
            set { shipToState = value; }
        }

        /// <summary>
        /// Postal code for the shipping address. The field is an Alphanumeric, special characters are NOT allowed EXCEPT space. For example, abc123 OR abc 12 OR 1234For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToPostalCode
        {
            get { return shipToPostalCode; }
            set { shipToPostalCode = value; }
        }
        /// <summary>
        /// Country of the shipping address. Use the two-character ISO Standard Country Codes. For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToCountry
        {
            get { return shipToCountry; }
            set { shipToCountry = value; }
        }

        /// <summary>
        /// Country code of the receivers’ landline phone. If the customer does not enter the country code then set default value: 99 or send this field as blank as it is optional.For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToPhoneNumber1
        {
            get { return shipToPhoneNumber1; }
            set { shipToPhoneNumber1 = value; }
        }

        /// <summary>
        /// Area code of the receivers’ landline phone. If the customer does not enter the area code then set default value: 99 or send this field as blank as it is optional. For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToPhoneNumber2
        {
            get { return shipToPhoneNumber2; }
            set { shipToPhoneNumber2 = value; }
        }
        /// <summary>
        /// Phone number of the receivers’ landline phone. If the customer does not enter the landline number then set default value: 999999 or send this field as blank as it is optional.For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToPhoneNumber3
        {
            get { return shipToPhoneNumber3; }
            set { shipToPhoneNumber3 = value; }
        }
        /// <summary>
        /// Mobile number of the receiver. If the customer enters the mobile number at your end please send the value in this particular field. Possible accepted values: +971-99999-9999 or 9999999999 Mobile number must NOT be sent with a SPACE. For merchants opting for “Merchant Hosted” Integration, this field is not validated.
        /// </summary>
        public string ShipToMobileNumber
        {
            get { return shipToMobileNumber; }
            set { shipToMobileNumber = value; }
        }

        /// <summary>
        /// If you are using multiple channels, use this to identify channel. Only possible values are:Web, IVR, or Mobile.
        /// </summary>
        public string TransactionSource
        {
            get { return transactionSource; }
            set { transactionSource = value; }
        }

        /// <summary>
        /// You can send details about the product and related information in this field.
        /// </summary>
        public string ProductInfo
        {
            get { return productInfo; }
            set { productInfo = value; }
        }
        /// <summary>
        /// If the customer has signed in to your website for this transaction then “Y”. If your customer has done a guest checkout then “N”.
        /// </summary>
        public string IsUserLoggedIn
        {
            get { return isUserLoggedIn; }
            set { isUserLoggedIn = value; }
        }

        /// <summary>
        /// This is a comma separated list of the sale value of all the items in the order. Example, if the order had two items of value 500 and 1000, send the value as “500.00, 1000.00”. Amount up to 2 decimal places.
        /// </summary>
        public string ItemTotal
        {
            get { return itemTotal; }
            set { itemTotal = value; }
        }
        /// <summary>
        /// This is a comma separated list of the categories of all the items in the order. For example, if the order is two books send “Book, Book”. This field is used to determine product category: electronic, handling, physical, service, or shipping.
        /// </summary>
        public string ItemCategory
        {
            get { return itemCategory; }
            set { itemCategory = value; }
        }
        /// <summary>
        /// Set this value to TRUE if you want to proceed for Authorization irrespective of the 3DSecure/Authentication result. Default value: FALSE
        /// </summary>
        public string IgnoreValidationResult
        {
            get { return ignoreValidationResult; }
            set { ignoreValidationResult = value; }
        }
        /// <summary>
        /// This is a user-defined field to send additional information about the transaction.
        /// </summary>
        public string UDF1
        {
            get { return udf1; }
            set { udf1 = value; }
        }
        /// <summary>
        /// This is a user-defined field to send additional information about the transaction.
        /// </summary>
        public string UDF2
        {
            get { return udf2; }
            set { udf2 = value; }
        }

        /// <summary>
        /// This is a user-defined field to send additional information about the transaction.
        /// </summary>
        public string UDF3
        {
            get { return udf3; }
            set { udf3 = value; }
        }
        /// <summary>
        /// This is a user-defined field to send additional information about the transaction.
        /// </summary>
        public string UDF4
        {
            get { return udf4; }
            set { udf4 = value; }
        }

        /// <summary>
        /// This is a user-defined field to send additional information about the transaction.
        /// </summary>
        public string UDF5
        {
            get { return udf5; }
            set { udf5 = value; }
        }
        #endregion

        /// <summary>
        /// Default Constructor
        /// </summary>
        public NetworkInternationalPaymentService()
        {
            xmlPath = ConfigurationSystem.NetworkInternationalPayConfig["XmlPath"];
            if (!Directory.Exists(xmlPath))
            {
                Directory.CreateDirectory(xmlPath);
            }


            merchantID = Convert.ToInt64(ConfigurationSystem.NetworkInternationalPayConfig["MerchantID"]);
            merchantEncryptionKey = ConfigurationSystem.NetworkInternationalPayConfig["EncryptionKey"];
            currency = ConfigurationSystem.NetworkInternationalPayConfig["Currency"];
            transactionType = ConfigurationSystem.NetworkInternationalPayConfig["TransactionType"];
            transactionMode = ConfigurationSystem.NetworkInternationalPayConfig["TransactionMode"];
            payModeType = ConfigurationSystem.NetworkInternationalPayConfig["PayModeType"];
        }


        /// <summary>
        /// Gets the encrypted message which will be passed to NI transaction page for payment processing
        /// </summary>
        /// <returns></returns>
        public string GetEncryptedTransactionRequest()
        {

            ///Transaction request format:
            ///requestParameter = <MerchantID>|<Encrypted Message>
            ///Here encrypted message will be a pipe separated string of all fields in the same sequence
            ///Note :MerchantId will not be encrypted in any request.
            ///This message will be encrypted using AES 256 bit encryption and appended to the MerchantID and then sent to NI transaction page.

            string encryptedRequest = string.Empty;
            try
            {
                //The below transaction request fields are mandatory.

                /**********Start: Mandatory Fields*************************************************/
                if (string.IsNullOrEmpty(Convert.ToString(merchantID)))
                {
                    throw new Exception("NI payment gateway:Need to specify merchant id for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(merchantOrderNumber)))
                {
                    throw new Exception("NI payment gateway:Need to specify merchant order number for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(currency)))
                {
                    throw new Exception("NI payment gateway:Need to specify currency for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(amount)))
                {
                    throw new Exception("NI payment gateway:Need to specify amount for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(successURL)))
                {
                    throw new Exception("NI payment gateway:Need to specify successURL for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(failureURL)))
                {
                    throw new Exception("NI payment gateway:Need to specify failureURL for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(transactionType)))
                {
                    throw new Exception("NI payment gateway:Need to specify transactionType for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(transactionMode)))
                {
                    throw new Exception("NI payment gateway:Need to specify transactionMode for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(payModeType)))
                {
                    throw new Exception("NI payment gateway:Need to specify payModeType for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(billToFirstName)))
                {
                    throw new Exception("NI payment gateway:Need to specify customer First Name for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(billToLastName)))
                {
                    throw new Exception("NI payment gateway:Need to specify customer Last Name for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(billToStreet1)))
                {
                    throw new Exception("NI payment gateway:Need to specify cutomer street address for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(billToCity)))
                {
                    throw new Exception("NI payment gateway:Need to specify cutomer city  for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(billToState)))
                {
                    throw new Exception("NI payment gateway:Need to specify cutomer state  for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(billToPostalCode)))
                {
                    throw new Exception("NI payment gateway:Need to specify cutomer postal code  for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(billToCountry)))
                {
                    throw new Exception("NI payment gateway:Need to specify cutomer country code  for the transaction");
                }
                else if (string.IsNullOrEmpty(Convert.ToString(billToEmail)))
                {
                    throw new Exception("NI payment gateway:Need to specify cutomer email  for the transaction");
                }

                /********************End:Mandatory Fields************************************************************************/


                /*******************Start:Non-Mandatory Fileds or Optional Fields*********************************************************************/

                if (string.IsNullOrEmpty(cardVerificationValue))
                {
                    cardVerificationValue = string.Empty;
                }
                if (string.IsNullOrEmpty(expiryMonth))
                {
                    expiryMonth = string.Empty;
                }
                if (string.IsNullOrEmpty(expiryYear))
                {
                    expiryYear = string.Empty;
                }
                if (string.IsNullOrEmpty(cardType))
                {
                    cardType = string.Empty;
                }
                if (string.IsNullOrEmpty(billToPhoneNumber1))
                {
                    billToPhoneNumber1 = string.Empty;
                }
                if (string.IsNullOrEmpty(billToPhoneNumber2))
                {
                    billToPhoneNumber2 = string.Empty;
                }
                if (string.IsNullOrEmpty(billToPhoneNumber3))
                {
                    billToPhoneNumber3 = string.Empty;
                }
                if (string.IsNullOrEmpty(billToMobileNumber))
                {
                    billToMobileNumber = string.Empty;
                }
                if (string.IsNullOrEmpty(gatewayID))
                {
                    gatewayID = string.Empty;
                }
                if (string.IsNullOrEmpty(customerID))
                {
                    customerID = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToFirstName))
                {
                    shipToFirstName = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToLastName))
                {
                    shipToLastName = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToStreet1))
                {
                    shipToStreet1 = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToStreet2))
                {
                    shipToStreet2 = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToCity))
                {
                    shipToCity = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToState))
                {
                    shipToState = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToPostalCode))
                {
                    shipToPostalCode = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToCountry))
                {
                    shipToCountry = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToPhoneNumber1))
                {
                    shipToPhoneNumber1 = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToPhoneNumber2))
                {
                    shipToPhoneNumber2 = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToPhoneNumber3))
                {
                    shipToPhoneNumber3 = string.Empty;
                }
                if (string.IsNullOrEmpty(shipToMobileNumber))
                {
                    shipToMobileNumber = string.Empty;
                }
                if (string.IsNullOrEmpty(transactionSource))
                {
                    transactionSource = string.Empty;
                }
                if (string.IsNullOrEmpty(productInfo))
                {
                    productInfo = string.Empty;
                }
                if (string.IsNullOrEmpty(isUserLoggedIn))
                {
                    isUserLoggedIn = string.Empty;
                }
                if (string.IsNullOrEmpty(itemTotal))
                {
                    itemTotal = string.Empty;
                }
                if (string.IsNullOrEmpty(itemCategory))
                {
                    itemCategory = string.Empty;
                }
                if (string.IsNullOrEmpty(ignoreValidationResult))
                {
                    ignoreValidationResult = string.Empty;
                }
                if (string.IsNullOrEmpty(udf1))
                {
                    udf1 = string.Empty;
                }
                if (string.IsNullOrEmpty(udf2))
                {
                    udf2 = string.Empty;
                }
                if (string.IsNullOrEmpty(udf3))
                {
                    udf3 = string.Empty;
                }
                if (string.IsNullOrEmpty(udf4))
                {
                    udf4 = string.Empty;
                }
                if (string.IsNullOrEmpty(udf5))
                {
                    udf5 = string.Empty;
                }
                /*******************End:Non-Mandatory Fileds or Optional Fields*********************************************************************/


                string strMessage = string.Empty;
                strMessage += merchantOrderNumber + "|" +
                currency + "|" +
                Math.Round(amount,2) + "|" +
                successURL + "|" +
                failureURL + "|" +
                transactionType + "|" +
                transactionMode + "|" +
                payModeType + "|" +
                creditCardNumber + "|" +
                cardVerificationValue + "|" +
                expiryMonth + "|" +
                expiryYear + "|" +
                cardType + "|" +
                billToFirstName + "|" +
                billToLastName + "|" +
                billToStreet1 + "|" +
                billToStreet2 + "|" +
                billToCity + "|" +
                billToState + "|" +
                billToPostalCode + "|" +
                billToCountry + "|" +
                billToEmail + "|" +
                billToPhoneNumber1 + "|" +
                billToPhoneNumber2 + "|" +
                billToPhoneNumber3 + "|" +
                billToMobileNumber + "|" +
                gatewayID + "|" +
                customerID + "|" +
                shipToFirstName + "|" +
                shipToLastName + "|" +
                shipToStreet1 + "|" +
                shipToStreet2 + "|" +
                shipToCity + "|" +
                shipToState + "|" +
                shipToPostalCode + "|" +
                shipToCountry + "|" +
                shipToPhoneNumber1 + "|" +
                shipToPhoneNumber2 + "|" +
                shipToPhoneNumber3 + "|" +
                shipToMobileNumber + "|" +
                transactionSource + "|" +
                productInfo + "|" +
                isUserLoggedIn + "|" +
                itemTotal + "|" +
                itemCategory + "|" +
                ignoreValidationResult + "|" +
                udf1 + "|" +
                udf2 + "|" +
                udf3 + "|" +
                udf4 + "|" +
                udf5 + "|";

                XmlDocument doc = new XmlDocument();
                string decryptedRequest = merchantID + "|" + strMessage;
                string xmlDecryptedRequest = @"<root>" + decryptedRequest + "</root>";
                doc.LoadXml(xmlDecryptedRequest);
                doc.Save(xmlPath + "NI_Decrypted_Transaction_Request_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml");

                //Encrypt the message using AES 256 bit encryption

                EncDec aesEncrypt = new EncDec();
                /***Merchant ID has to be concatenated with encrypted requestparameter****/
                encryptedRequest = merchantID + "|" + aesEncrypt.Encrypt(merchantEncryptionKey, strMessage);       
                string xmlEncryptedRequest = @"<root>" + encryptedRequest + "</root>";
                doc.LoadXml(xmlEncryptedRequest);
                doc.Save(xmlPath + "NI_Encrypted_Transaction_Request_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml");

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error in NI payment gateway GetEncryptedTransactionRequest method. Reason : " + ex.ToString(), "");
                encryptedRequest = string.Empty;
            }
            //Send this requestParameter to NI Transaction URL using POST method.
            return encryptedRequest;
        }


        /// <summary>
        /// Returns the decrypted message from the encryptedTransactionResponse to save the transaction status i.e payment success or failed.
        /// </summary>
        /// <param name="encryptedTransactionResponse"></param>
        /// <returns></returns>
        public string DecryptTransactionResponse(string encryptedTransactionResponse)
        {
            //Here responseParameter = <Encrypted Message>
            // Here encrypted message will be a pipe separated string.
            //Here we need to decrypt the message and  save the transaction status in the database.
            //Decrypted message = OrderNumber + "|" + NIOnlineRefID + "|" + Currency + "|" + Amount + "|" + Status + "|" + BankReferenceNumber + "|" + PayModeType + "|" + CardType + "|" + ErrorCode + "|" + ErrorResponseMessage + "|" + TransactionType + "|" + CardEnrollmentResponse + "|" + ECI_Values + "|" + PGAuthCode + "|" + FraudDecision + "|" + FraudReason + "|" + DCC_Converted + "|" + DCC_ConvertedAmount + "|" + DCC_ConvertedCurrency + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "|";
            string transactionResponse = string.Empty;
            try
            {
                XmlDocument doc = new XmlDocument();
                string xmlEncryptedTransactionResponse = @"<root>" + encryptedTransactionResponse + "</root>";
                doc.LoadXml(xmlEncryptedTransactionResponse);
                doc.Save(xmlPath + "NI_Encrypted_Transaction_Response_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml");

                EncDec aesEncrypt = new EncDec();
                transactionResponse = aesEncrypt.Decrypt(merchantEncryptionKey, encryptedTransactionResponse);       
                string xmltransactionResponse = @"<root>" + transactionResponse + "</root>";
                doc.LoadXml(xmltransactionResponse);
                doc.Save(xmlPath + "NI_Decrypted_Transaction_Response" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml");

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error in NI payment gateway DecryptTransactionResponse method. Reason : " + ex.ToString(), "");
                transactionResponse = string.Empty;
            }
            return transactionResponse;
        }


    }
}
