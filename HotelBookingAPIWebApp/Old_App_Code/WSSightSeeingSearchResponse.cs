﻿//using System.Linq;
//using System.Xml.Linq;

/// <summary>
/// Summary description for WSSightSeeingSearchResponse
/// </summary>
public class WSSightSeeingSearchResponse
{
    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    private WSSightSeeingResult[] result;
    public WSSightSeeingResult[] Result
    {
        get { return result; }
        set { result = value; }
    }

    private string sessionId;
    public string SessionId
    {
        get { return sessionId; }
        set { sessionId = value; }
    }
    
    public WSSightSeeingSearchResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
