using System;
using System.Data;
using System.Collections.Generic;
//using Technology.BookingEngine;
//using CoreLogic;
//using CoreLogic;
//using Technology.Configuration;
//using Technology.MetaSearchEngine;
using CT.BookingEngine;
using CT.Core;

public struct WSRoomGuestData
{
    public int NoOfAdults;
    public int NoOfChild;
    public List<int> ChildAge; // ChildAge list size should be equal to noOfChild
}

public enum WSHotelRating
{
    All = 0,
    OneStar = 1,
    TwoStar = 2,
    ThreeStar = 3,
    FourStar = 4,
    FiveStar = 5
}

public enum WSHotelRatingInput
{
    All = 0,
    OneStarOrLess = 1,
    TwoStarOrLess = 2,
    ThreeStarOrLess = 3,
    FourStarOrLess = 4,
    FiveStarOrLess = 5
}


public struct WSRoomInfo
{
    public string roomTypeCode;
    public string roomTypeName;
    public int noOfRooms;
    public int noOfCots;
    public List<int> childAge;
}


/// <summary>
/// Summary description for WSHotelSearchRequest
/// </summary>
public class WSHotelSearchRequest
{
    /// <summary>
    /// Changes in Version 2 of DOTW
    /// updated by ziyad dt: 15 May 2013
    /// </summary>
    
    
    
    private string passengerNationality;
    public string PassengerNationality
    {
        get { return passengerNationality; }
        set { passengerNationality = value; }
    }

    /// <summary>
    /// Specifies which type of rates (or suppliers - 
    /// if the request is sent with noPrice set to true) 
    /// are taken into consideration by the system. Possible values:
    /// 1 - DOTW rate type
    /// 2 - DYNAMIC DIRECT rate type
    /// 3 - DYNAMIC 3rd PARTY rate type
    /// </summary>
    private RoomRateType roomRateType;
    public RoomRateType RoomRateType
    {
        get { return roomRateType; }
        set { roomRateType = value; }
    }

    /// <summary>
    /// Specifies passenger country of residence and it is mandatory to be sent in the request. 
    /// This is DOTW country internal code that can be obtained using the getallcountries request.
    /// </summary>
    /// 
    private string passengerCountryOfResidence;
    public string PassengerCountryOfResidence
    {
        get { return passengerCountryOfResidence; }
        set { passengerCountryOfResidence = value; }
    }
    /// <summary>
    /// End Changes of Version 2
    private DateTime checkInDate;
    public DateTime CheckInDate
    {
        get { return checkInDate; }
        set { checkInDate = value; }
    }

    private DateTime checkOutDate;
    public DateTime CheckOutDate
    {
        get { return checkOutDate; }
        set { checkOutDate = value; }
    }

    private string countryName;
    public string CountryName
    {
        get { return countryName; }
        set { countryName = value; }
    }

    private bool isDomestic;
    public bool IsDomestic
    {
        get { return isDomestic; }
        set { isDomestic = value; }
    }

    private string cityReference;
    public string CityReference
    {
        get { return cityReference; }
        set { cityReference = value; }
    }

    private int cityId;
    /// <summary>
    /// City Id as provided in the list : mandatory for international
    /// </summary>
    public int CityId
    {
        get { return cityId; }
        set { cityId = value; }
    }

    private int noOfRooms; // total no of rooms
    public int NoOfRooms
    {
        get
        {
            return noOfRooms;
        }
        set
        {
            noOfRooms = value;
        }
    }

    private WSRoomGuestData[] roomGuest;
    public WSRoomGuestData[] RoomGuest
    {
        get
        {
            return roomGuest;
        }
        set
        {
            roomGuest = value;
        }
    }

    private string hotelName; // - Optional
    public string HotelName
    {
        get
        {
            return hotelName;
        }
        set
        {
            hotelName = value;
        }
    }

    private WSHotelRatingInput rating; // By Default - All
    public WSHotelRatingInput Rating
    {
        get
        {
            return rating;
        }
        set
        {
            rating = value;
        }
    }

    static StaticData staticInfo = new StaticData();
   
    public static Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>(); //set INR
    static WSHotelSearchRequest()
    {
        staticInfo.BaseCurrency="AED"; //TODO ZIYAD
        rateOfExList = staticInfo.CurrencyROE;
    }

    public static void UpdatePrice(ref HotelSearchResult[] mseResult, HotelRequest req, int agencyId)
    {
        PriceAccounts price = new PriceAccounts();
        DataTable dtAllAgentsMarkup = new DataTable();
        if (agencyId > 0)
        {
            //Getting all sources markup
            dtAllAgentsMarkup = UpdateMarkup.GetAllMarkup(agencyId, (int)ProductType.Hotel);
        }
        for (int i = 0; i < mseResult.Length; i++)
        {
            for (int count = 0; count < mseResult[i].RoomDetails.Length; count++)
            {
                #region B2CAgentMarkup calculations
                //price = mseResult[i].Price;
                DataView dv = dtAllAgentsMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2C') AND SourceId='" + mseResult[i].BookingSource.ToString() + "'";
                DataTable dtAgentMarkup = dv.ToTable();
                if (dtAgentMarkup != null && dtAgentMarkup.Rows.Count > 0)
                {
                    decimal agtB2CMarkUp = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Markup"]);
                    mseResult[i].RoomDetails[count].B2CMarkupType = dtAgentMarkup.Rows[0]["MarkupType"].ToString();
                    mseResult[i].RoomDetails[count].B2CMarkupValue = agtB2CMarkUp;

                    if (Convert.ToString(dtAgentMarkup.Rows[0]["MarkupType"]) == "P")
                    {
                        mseResult[i].RoomDetails[count].B2CMarkup = (Convert.ToDecimal(mseResult[i].RoomDetails[count].Markup + mseResult[i].RoomDetails[count].SellingFare) * (agtB2CMarkUp / 100m));
                    }
                    else
                    {
                        mseResult[i].RoomDetails[count].B2CMarkup = agtB2CMarkUp;
                    }
                }

                #endregion
            }

        }
    }

    public static WSHotelResult[] GetHotelSearchResults(HotelSearchResult[] mseResult, HotelRequest req, int agencyId)
    {
        decimal rateofExchange = 1;
        WSHotelResult[] results = new WSHotelResult[mseResult.Length];
        for (int i = 0; i < mseResult.Length; i++)
        {
            rateofExchange = 1;
            results[i] = new WSHotelResult();
            //commented by brahmam code optimization
            //PriceType priceType;
            //priceType = WSHotelSearchRequest.GetPriceTypeFromSource(mseResult[i].BookingSource);
            results[i].Index = (i + 1);
            results[i].CheckInDate = mseResult[i].StartDate;
            results[i].CheckOutDate = mseResult[i].EndDate;
            results[i].CityCode = mseResult[i].CityCode;
            results[i].Currency = mseResult[i].Currency;
            results[i].LastInActivieTime = mseResult[i].LastInActivieTime;
            //Added by brahmam
            results[i].BookingSource =(WSHotelBookingSource) mseResult[i].BookingSource;
            results[i].PromoMessage = mseResult[i].PromoMessage;
            results[i].Price=mseResult[i].Price;
            if (!string.IsNullOrEmpty(req.PassengerNationality))
                results[i].PassengerNationality = req.PassengerNationality;
            if (!string.IsNullOrEmpty(req.PassengerCountryOfResidence))
                results[i].PassengerCountryOfResidence = req.PassengerCountryOfResidence;
            if (rateOfExList.ContainsKey(mseResult[i].Currency))
            {
                rateofExchange = rateOfExList[mseResult[i].Currency];
            }
            results[i].RateOfExchange = rateofExchange;
            //commented by brahmam code optimization
            //string imagePath = string.Empty;
            //if (mseResult[i].BookingSource == HotelBookingSource.TBOConnect)
            //{
            //    imagePath = ConfigurationSystem.HotelConnectConfig["imgPathForServer"] + "/";
            //}
            ////else if (mseResult[i].BookingSource == HotelBookingSource.GTA && Convert.ToBoolean(ConfigurationSystem.GTAConfig["NewImageStruct"]))
            ////{
            ////    imagePath = ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.GTAConfig["imgPathForServer"] + "/";
            ////}
            //else if (mseResult[i].BookingSource == HotelBookingSource.HotelBeds)
            //{
            //    //imagePath = ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.HotelBedsConfig["imgPathForServer"] + "/";
            //}
            //else if (mseResult[i].BookingSource == HotelBookingSource.Tourico)
            //{
            //    //Commented Due to the Tourico image are coming from Tourico.
            //    imagePath = ""; // ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.TouricoConfig["imgPathForServer"] + "/";
            //}
            WSHotelInfo hotelInfo = new WSHotelInfo();
            hotelInfo.HotelAddress = mseResult[i].HotelAddress;
            hotelInfo.HotelCode = mseResult[i].HotelCode;
            hotelInfo.HotelContactNo = mseResult[i].HotelContactNo;
            hotelInfo.HotelDescription = mseResult[i].HotelDescription;
            hotelInfo.HotelLocation = mseResult[i].HotelLocation;
            hotelInfo.HotelMap = mseResult[i].HotelMap;
            hotelInfo.HotelName = mseResult[i].HotelName;
            if (mseResult[i].HotelPicture != null && mseResult[i].HotelPicture.Trim() != "")
            {
                hotelInfo.HotelPicture = mseResult[i].HotelPicture;
            }
            hotelInfo.Rating = (WSHotelRating)Enum.Parse(typeof(WSHotelRating), mseResult[i].Rating.ToString());
            hotelInfo.TotalPrice = mseResult[i].TotalPrice;
            results[i].HotelInfo = hotelInfo;
            results[i].RoomGuestDetails = new WSRoomGuestData[req.RoomGuest.Length];
            for (int j = 0; j < req.RoomGuest.Length; j++)
            {
                results[i].RoomGuestDetails[j] = new WSRoomGuestData();
                results[i].RoomGuestDetails[j].NoOfAdults = req.RoomGuest[j].noOfAdults;
                results[i].RoomGuestDetails[j].NoOfChild = req.RoomGuest[j].noOfChild;
                results[i].RoomGuestDetails[j].ChildAge = req.RoomGuest[j].childAge;
            }
            if (mseResult[i].RoomDetails != null)
            {
                results[i].RoomDetails = new WSHotelRoomsDetails[mseResult[i].RoomDetails.Length];
                for (int k = 0; k < mseResult[i].RoomDetails.Length; k++)
                {
                    results[i].RoomDetails[k] = new WSHotelRoomsDetails();
                    results[i].RoomDetails[k].Amenities = new List<string>();
                    results[i].RoomDetails[k].Amenities = mseResult[i].RoomDetails[k].Amenities;
                    results[i].RoomDetails[k].RoomTypeName = mseResult[i].RoomDetails[k].RoomTypeName;
                    results[i].RoomDetails[k].RoomTypeCode = mseResult[i].RoomDetails[k].RoomTypeCode;
                    results[i].RoomDetails[k].RatePlanCode = mseResult[i].RoomDetails[k].RatePlanCode;
                    results[i].RoomDetails[k].SequenceNo = mseResult[i].RoomDetails[k].SequenceNo;
                    //Added by brahmam
                    results[i].RoomDetails[k].SellingFare = mseResult[i].RoomDetails[k].SellingFare;
                    results[i].RoomDetails[k].Markup = mseResult[i].RoomDetails[k].Markup;
                    results[i].RoomDetails[k].MarkupType = mseResult[i].RoomDetails[k].MarkupType;
                    results[i].RoomDetails[k].Discount = mseResult[i].RoomDetails[k].Discount;
                    results[i].RoomDetails[k].DiscountType = mseResult[i].RoomDetails[k].DiscountType;
                    results[i].RoomDetails[k].SellExtraGuestCharges = mseResult[i].RoomDetails[k].SellExtraGuestCharges;
                    results[i].RoomDetails[k].TotalTax = mseResult[i].RoomDetails[k].TotalTax;
                    results[i].RoomDetails[k].Rates = mseResult[i].RoomDetails[k].Rates;
                    results[i].RoomDetails[k].mealPlanDesc = mseResult[i].RoomDetails[k].mealPlanDesc;
                    results[i].RoomDetails[k].PromoMessage = mseResult[i].RoomDetails[k].PromoMessage;
                    results[i].RoomDetails[k].ChildCharges = mseResult[i].RoomDetails[k].ChildCharges;
                    results[i].RoomDetails[k].MaxExtraBeds = mseResult[i].RoomDetails[k].MaxExtraBeds;
                    results[i].RoomDetails[k].EssentialInformation = mseResult[i].RoomDetails[k].EssentialInformation;
                    results[i].RoomDetails[k].SharingBedding = mseResult[i].RoomDetails[k].SharingBedding;
                    results[i].RoomDetails[k].NumberOfCots = mseResult[i].RoomDetails[k].NumberOfCots;
                    results[i].RoomDetails[k].IsExtraBed = mseResult[i].RoomDetails[k].IsExtraBed;
                    results[i].RoomDetails[k].CancellationPolicy = mseResult[i].RoomDetails[k].CancellationPolicy;
                    results[i].RoomDetails[k].B2CMarkup = mseResult[i].RoomDetails[k].B2CMarkup;
                    results[i].RoomDetails[k].B2CMarkupType = mseResult[i].RoomDetails[k].B2CMarkupType;
                    results[i].RoomDetails[k].B2CMarkupValue = mseResult[i].RoomDetails[k].B2CMarkupValue;
                    if (mseResult[i].RoomDetails[k].TaxDetail != null && mseResult[i].RoomDetails[k].TaxDetail.OutputVAT != null)
                    {
                        results[i].RoomDetails[k].IsVatApplied = mseResult[i].RoomDetails[k].TaxDetail.OutputVAT.Applied;
                        results[i].RoomDetails[k].VatAppliedType = mseResult[i].RoomDetails[k].TaxDetail.OutputVAT.AppliedOn;
                        results[i].RoomDetails[k].VatCost = mseResult[i].RoomDetails[k].TaxDetail.OutputVAT.Charge;
                    }
                    else
                    {
                        results[i].RoomDetails[k].VatAppliedType = VATAppliedOn.TC;
                    }
                    if (results[i].RoomDetails[k].SequenceNo != null)
                    {
                        results[i].RoomDetails[k].NoOfUnits = 1;
                    }
                    else
                    {
                        results[i].RoomDetails[k].NoOfUnits = req.NoOfRooms;
                    }
                    if (mseResult[i].RoomDetails[k].Occupancy != null)
                    {
                        if (mseResult[i].RoomDetails[k].Occupancy.ContainsKey("Max_Adult_Occupancy"))
                        {
                            results[i].RoomDetails[k].Occupancy.MaxAdult = mseResult[i].RoomDetails[k].Occupancy["Max_Adult_Occupancy"];
                        }
                        if (mseResult[i].RoomDetails[k].Occupancy.ContainsKey("Max_Child_Occupancy"))
                        {
                            results[i].RoomDetails[k].Occupancy.MaxChild = mseResult[i].RoomDetails[k].Occupancy["Max_Child_Occupancy"];
                        }
                        if (mseResult[i].RoomDetails[k].Occupancy.ContainsKey("Max_Infant_Occupancy"))
                        {
                            results[i].RoomDetails[k].Occupancy.MaxInfant = mseResult[i].RoomDetails[k].Occupancy["Max_Infant_Occupancy"];
                        }
                        if (mseResult[i].RoomDetails[k].Occupancy.ContainsKey("Max_Guest_Occupancy"))
                        {
                            results[i].RoomDetails[k].Occupancy.MaxGuest = mseResult[i].RoomDetails[k].Occupancy["Max_Guest_Occupancy"];
                        }
                        if (mseResult[i].RoomDetails[k].Occupancy.ContainsKey("Base_Adult_Occupancy"))
                        {
                            results[i].RoomDetails[k].Occupancy.BaseAdult = mseResult[i].RoomDetails[k].Occupancy["Base_Adult_Occupancy"];
                        }
                        if (mseResult[i].RoomDetails[k].Occupancy.ContainsKey("Base_Child_Occupancy"))
                        {
                            results[i].RoomDetails[k].Occupancy.BaseChild = mseResult[i].RoomDetails[k].Occupancy["Base_Child_Occupancy"];
                        }
                        if (mseResult[i].RoomDetails[k].Occupancy.ContainsKey("minStay"))
                        {
                            results[i].RoomDetails[k].Occupancy.minStay = mseResult[i].RoomDetails[k].Occupancy["minStay"];
                        }

                    }
                    
                    results[i].RoomDetails[k].Rate = new WSRate();
                    results[i].RoomDetails[k].Rate.Currency = mseResult[i].Currency;
                    TimeSpan ts = mseResult[i].EndDate.Subtract(mseResult[i].StartDate);
                    int daysCount = ts.Days;
                    PriceAccounts price = new PriceAccounts();
                    //#region Calculate Price
                    //if (mseResult[i].BookingSource == HotelBookingSource.Desiya)
                    //{
                    //    price = CT.AccountingEngine.AccountingEngine.GetPrice(mseResult[i], mseResult[i].RoomDetails[k].RoomTypeCode, agencyId, 0, results[i].RoomDetails[k].NoOfUnits, daysCount, priceType, true);
                    //}
                    //else if (mseResult[i].BookingSource == HotelBookingSource.TBOConnect) //check for TBO
                    //{
                    //    decimal grossFare = 0, TotalTax = 0;
                    //    for (int m = 0; m < mseResult[i].RoomDetails[k].Rates.Length; m++)
                    //    {
                    //        grossFare += mseResult[i].RoomDetails[k].Rates[m].Amount + mseResult[i].RoomDetails[k].SellExtraGuestCharges + mseResult[i].RoomDetails[k].ChildCharges;
                    //        TotalTax = mseResult[i].RoomDetails[k].TotalTax;
                    //    }
                    //    grossFare = grossFare / req.NoOfRooms;
                    //    TotalTax = TotalTax / req.NoOfRooms;
                    //    price.PublishedFare = grossFare;
                    //    price.Tax = TotalTax;
                    //    price.SeviceTax = CT.AccountingEngine.AccountingEngine.CalculateProductServiceTax(grossFare + TotalTax, true, ProductType.Hotel);
                    //    Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, 1, "Gross Fare: " + grossFare + "Total Tax: " + TotalTax + "Service Tax: " + price.SeviceTax, "");
                    //}
                    //else if (mseResult[i].BookingSource != HotelBookingSource.IAN)
                    //{
                    //    price = CT.AccountingEngine.AccountingEngine.GetPrice(mseResult[i], mseResult[i].RoomDetails[k].RoomTypeCode, agencyId, 0, 1, daysCount, priceType, true);
                    //}

                    //#endregion
                    decimal totalRate = 0;
                    decimal totalTax = 0;
                    results[i].RoomDetails[k].RoomRate = new WSRoomRate();
                    results[i].RoomDetails[k].RoomRate.ExtraGuestCharges = (mseResult[i].RoomDetails[k].SellExtraGuestCharges * daysCount * rateofExchange) / results[i].RoomDetails[k].NoOfUnits;
                    decimal totalRoomRate = 0;
                    decimal totalRoomTax = 0;
                    results[i].RoomDetails[k].RoomRate.DayRates = new WSDayRates[daysCount];
                    for (int m = 0; m < daysCount; m++)
                    {
                        results[i].RoomDetails[k].RoomRate.DayRates[m] = new WSDayRates();
                        results[i].RoomDetails[k].RoomRate.DayRates[m].BaseFare = mseResult[i].RoomDetails[k].Rates[m].SellingFare * rateofExchange / results[i].RoomDetails[k].NoOfUnits;
                        results[i].RoomDetails[k].RoomRate.DayRates[m].Days = mseResult[i].RoomDetails[k].Rates[m].Days;
                        totalRoomRate = totalRoomRate + results[i].RoomDetails[k].RoomRate.DayRates[m].BaseFare;
                        //totalRoomRate = results[i].RoomDetails[k].RoomRate.DayRates[m].BaseFare;
                        //break;
                    }
                    
                    results[i].RoomDetails[k].RoomRate.DisCount = price.WhiteLabelDiscount * rateofExchange;
                    //commented by brahmam code optimization
                    //if (mseResult[i].BookingSource == HotelBookingSource.Travco)
                    //{
                    //    HotelSource hotelS = new HotelSource();
                    //    hotelS.Load("Travco", agencyId);
                    //    decimal ourCommission = hotelS.OurCommission;
                    //    int commissionType = 0;
                    //    commissionType = (int)hotelS.CommissionTypeId;
                    //    if (hotelS.FareTypeId == FareType.Net)
                    //    {
                    //        if (commissionType == (int)CommissionType.Percentage)
                    //        {
                    //            mseResult[i].RoomDetails[k].Discount += Convert.ToDecimal(ourCommission) * mseResult[i].RoomDetails[k].Discount / 100;
                    //        }
                    //        if (commissionType == (int)CommissionType.Fixed)
                    //        {
                    //            if (mseResult[i].RoomDetails[k].Discount > 0)
                    //            {
                    //                mseResult[i].RoomDetails[k].Discount += Convert.ToDecimal(ourCommission / rateofExchange);
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (commissionType == (int)CommissionType.Percentage)
                    //        {
                    //            mseResult[i].RoomDetails[k].Discount += Convert.ToDecimal(ourCommission) * mseResult[i].RoomDetails[k].Discount / 100;
                    //        }
                    //        if (commissionType == (int)CommissionType.Fixed)
                    //        {
                    //            if (mseResult[i].RoomDetails[k].Discount > 0)
                    //            {
                    //                mseResult[i].RoomDetails[k].Discount += Convert.ToDecimal(ourCommission / rateofExchange);
                    //            }
                    //        }
                    //    }
                    //    results[i].RoomDetails[k].RoomRate.DisCount += mseResult[i].RoomDetails[k].Discount * rateofExchange;
                    //}
                    results[i].RoomDetails[k].RoomRate.OtherCharges = price.WLCharge * rateofExchange;
                    results[i].RoomDetails[k].RoomRate.ServiceTax = (price.SeviceTax + price.TdsCommission) * rateofExchange;
                    totalRoomRate = totalRoomRate + results[i].RoomDetails[k].RoomRate.ExtraGuestCharges;
                    totalRoomTax = mseResult[i].RoomDetails[k].TotalTax * rateofExchange / results[i].RoomDetails[k].NoOfUnits;
                    results[i].RoomDetails[k].RoomRate.TotalRoomRate = totalRoomRate;
                    results[i].RoomDetails[k].RoomRate.TotalRoomTax = totalRoomTax;
                    totalRate = (results[i].RoomDetails[k].RoomRate.TotalRoomRate+mseResult[i].RoomDetails[k].Markup+ results[i].RoomDetails[k].RoomRate.OtherCharges + results[i].RoomDetails[k].RoomRate.ServiceTax - results[i].RoomDetails[k].RoomRate.DisCount) * results[i].RoomDetails[k].NoOfUnits;//Markup added by brahmam
                    totalTax = results[i].RoomDetails[k].RoomRate.TotalRoomTax * results[i].RoomDetails[k].NoOfUnits;
                    results[i].RoomDetails[k].Rate.TotalRate = totalRate;
                    results[i].RoomDetails[k].Rate.TotalTax = totalTax;
                }
            }
            results[i].NoOfRooms = req.NoOfRooms;
            if (mseResult[i].BookingSource != HotelBookingSource.Desiya)
            {
                results[i].SendAllRoomLeadGuestInfo = true;
            }
        }
        return results;
    }

    public static HotelRating GetWSHotelRating(WSHotelRatingInput rating)
    {
        HotelRating mseHotelRating = new HotelRating();
        if (rating == WSHotelRatingInput.All)
        {
            mseHotelRating = HotelRating.All;
        }
        if (rating == WSHotelRatingInput.OneStarOrLess)
        {
            mseHotelRating = HotelRating.OneStar;
        }
        if (rating == WSHotelRatingInput.TwoStarOrLess)
        {
            mseHotelRating = HotelRating.TwoStar;
        }
        if (rating == WSHotelRatingInput.ThreeStarOrLess)
        {
            mseHotelRating = HotelRating.ThreeStar;
        }
        if (rating == WSHotelRatingInput.FourStarOrLess)
        {
            mseHotelRating = HotelRating.FourStar;
        }
        if (rating == WSHotelRatingInput.FiveStarOrLess)
        {
            mseHotelRating = HotelRating.FiveStar;
        }
        return mseHotelRating;
    }

    public static PriceType GetPriceTypeFromSource(HotelBookingSource source)
    {
        PriceType priceType;
        //Get Hotel source commission type
        HotelSource hSource = new HotelSource();
        hSource.Load(source.ToString());

        //since there are two enums of similar type where one is in BookingEngine(PriceAccounts) & other in CoreLogic(Enumerator).
        if (hSource.FareTypeId == FareType.Net)
        {
            priceType = PriceType.NetFare;
        }
        else
        {
            priceType = PriceType.PublishedFare;
        }
        return priceType;
    }
    private static HotelSearchResult GetFilteredRoomType(HotelSearchResult result, HotelRequest request)
    {
        if (request.NoOfRooms > 1 && !request.IsDomestic)
        {
            List<HotelRoomsDetails> selectedRooms = new List<HotelRoomsDetails>();
            for (int i = 1; i <= request.NoOfRooms; i++)
            {
                foreach (HotelRoomsDetails hrd in result.RoomDetails)
                {
                    if (hrd.SequenceNo.Contains(i.ToString()))
                    {
                        selectedRooms.Add(hrd);
                        break;
                    }
                }
            }
            result.RoomDetails = selectedRooms.ToArray();
        }
        return result;
    }
}
