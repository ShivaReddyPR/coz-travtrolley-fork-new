/// <summary>
/// Summary description for WSStatus
/// </summary>
public class WSStatus
{
    private string statusCode;
    public string StatusCode
    {
        get { return statusCode; }
        set { statusCode = value; }
    }

    private string description;
    public string Description
    {
        get { return description; }
        set { description = value; }
    }

    private string category;
    public string Category
    {
        get { return category; }
        set { category = value; }
    }

    public WSStatus()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
