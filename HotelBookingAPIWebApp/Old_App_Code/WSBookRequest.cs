using System;
using System.Collections.Generic;
//using Technology.BookingEngine;
//using Technology.BookingEngine.GDS;
//using CoreLogic;
//using Technology.MetaSearchEngine;
//using CoreLogic;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;


/// <summary>
/// Summary description for WSBookRequest
/// </summary>
public class WSBookRequest
{
    private List<string> roomCodes;
    public List<string> RoomCodes
    {
        get
        {
            return roomCodes;
        }
        set
        {
            roomCodes = value;
        }
    }
    /* private WSCreditCardInfo ccInfo;
     public WSCreditCardInfo CCInfo
     {
         get
         {
             return ccInfo;
         }
         set
         {
             ccInfo = value;
         }
     }*/

    private WSGuest[] guest;
    public WSGuest[] Guest
    {
        get
        {
            return guest;
        }
        set
        {
            guest = value;
        }
    }

    private string sessionId;
    public string SessionId
    {
        get
        {
            return sessionId;
        }
        set
        {
            sessionId = value;
        }
    }

    private string flightInfo;
    public string FlightInfo
    {
        get
        {
            return flightInfo;
        }
        set
        {
            flightInfo = value;
        }
    }

    private string specialRequest;
    public string SpecialRequest
    {
        get
        {
            return specialRequest;
        }
        set
        {
            specialRequest = value;
        }
    }

    private WSPaymentInformation paymentInfo;
    public WSPaymentInformation PaymentInfo
    {
        get
        {
            return paymentInfo;
        }
        set
        {
            paymentInfo = value;
        }
    }

    private int noOfRooms;
    public int NoOfRooms
    {
        get
        {
            return noOfRooms;
        }
        set
        {
            noOfRooms = value;
        }
    }

    private int index;
    public int Index
    {
        get { return index; }
        set { index = value; }
    }

    private string hotelCode;
    public string HotelCode
    {
        get { return hotelCode; }
        set { hotelCode = value; }
    }

    private string hotelName;
    public string HotelName
    {
        get { return hotelName; }
        set { hotelName = value; }
    }

    private WSHotelRoomsDetails[] roomDetails;
    public WSHotelRoomsDetails[] RoomDetails
    {
        get { return roomDetails; }
        set { roomDetails = value; }
    }
    // For DOTW new Param
    private string passengerNationality;
    public string PassengerNationality
    {
        get { return passengerNationality; }
        set { passengerNationality = value; }
    }
    
    private string passengerCountryOfResidence;
    public string PassengerCountryOfResidence
    {
        get { return passengerCountryOfResidence; }
        set { passengerCountryOfResidence = value; }
    }

    //Stores promo information
    private WSPromoDetail promoDetail;
    public WSPromoDetail PromoDetail
    {
        get { return promoDetail; }
        set { promoDetail = value; }
    }
    //VAT Implementation
    decimal outPutVatAmount;
    
    public decimal OutPutVatAmount
    {
        get
        {
            return outPutVatAmount;
        }

        set
        {
            outPutVatAmount = value;
        }
    }


    public WSBookRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static HotelSearchResult GetSelectedSearchResult(WSBookRequest request, HotelSearchResult[] mseResults, int memberId)
    {
        HotelSearchResult mseResult = new HotelSearchResult();
        int count = request.index - 1;
        if (request.hotelCode == mseResults[count].HotelCode && request.hotelName.Trim() == mseResults[count].HotelName)
        {
            decimal mseRate = 0;
            decimal apiRate = 0;
            decimal serviceTax = 0;
            PriceType priceType = WSHotelSearchRequest.GetPriceTypeFromSource(mseResults[count].BookingSource);
            if (mseResults[count].BookingSource == HotelBookingSource.Desiya || mseResults[count].BookingSource == HotelBookingSource.IAN)
            {
                # region currency conversion to AED
                decimal rateofExchange = 1;
                Dictionary<string, decimal> rateofExchangeList = WSHotelSearchRequest.rateOfExList;
                rateofExchange = rateofExchangeList[mseResults[count].Currency];
                #endregion
                string[] stringArray = new string[] { "###" };
                string[] roomCodes = request.RoomCodes[0].Split(stringArray, StringSplitOptions.None);
                if (mseResults[count].BookingSource == HotelBookingSource.Desiya)
                {
                    for (int j = 0; j < mseResults[count].RoomDetails.Length; j++)
                    {

                        if (mseResults[count].RoomDetails[j].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && mseResults[count].RoomDetails[j].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                        {
                            //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, agencyId, "Request RoomTypeCode:" + mseResults[count].RoomDetails[j].RoomTypeCode.Trim() + "=" + roomCodes[0].ToString() + " || " + request.RoomDetails[j].RatePlanCode.ToString() + "=" + roomCodes[1].ToString(), "");

                            if (priceType == PriceType.PublishedFare)
                            {
                                //mseRate += mseResults[count].RoomDetails[j].TotalPrice;
                                mseRate += (mseResults[count].RoomDetails[j].SellingFare + (mseResults[count].RoomDetails[j].SellExtraGuestCharges * mseResults[count].RoomDetails[j].Rates.Length) + mseResults[count].RoomDetails[j].TotalTax) * rateofExchange;
                                serviceTax = CT.AccountingEngine.AccountingEngine.CalculateProductServiceTax(mseResults[count].RoomDetails[j].TotalPrice, true, ProductType.Hotel);
                                mseRate += serviceTax * rateofExchange;
                            }
                            else
                            {
                                mseRate += (mseResults[count].RoomDetails[j].SellingFare + (mseResults[count].RoomDetails[j].SellExtraGuestCharges * mseResults[count].RoomDetails[j].Rates.Length) + mseResults[count].RoomDetails[j].TotalTax) * rateofExchange;
                                serviceTax =CT.AccountingEngine.AccountingEngine.CalculateProductServiceTax(mseResults[count].RoomDetails[j].TotalPrice, true, ProductType.Hotel);
                                mseRate += serviceTax * rateofExchange;
                            }
                        }
                    }

                }
                else
                {
                    decimal grossFare = 0, totalTax = 0;
                    for (int m = 0; m < mseResults[count].RoomDetails.Length; m++)
                    {
                        if (mseResults[count].RoomDetails[m].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && mseResults[count].RoomDetails[m].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                        {
                            for (int k = 0; k < mseResults[count].RoomDetails[m].Rates.Length; k++)
                            {
                                grossFare += mseResults[count].RoomDetails[m].Rates[k].Amount ;
                            }
                            totalTax = mseResults[count].RoomDetails[m].TotalTax;
                            break;
                        }
                    }
                    mseRate = (grossFare + totalTax) * rateofExchange;
                }
                for (int k = 0; k < request.RoomDetails.Length; k++)
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, memberId, "Request RoomTypeCode:"
                    + request.RoomDetails[k].RoomTypeCode.ToString() + "=" + roomCodes[0].ToString()
                    + " || " + request.RoomDetails[k].RatePlanCode.ToString() + "=" + roomCodes[1].ToString(), "");
                    if (request.RoomDetails[k].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && request.RoomDetails[k].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                    {
                        apiRate += (request.RoomDetails[k].RoomRate.TotalRoomRate + request.RoomDetails[k].RoomRate.TotalRoomTax) * request.RoomDetails[k].NoOfUnits;
                        apiRate += request.RoomDetails[k].RoomRate.ServiceTax;
                    }
                }
            }
            else
            {
                # region currency conversion 
                decimal rateofExchange = 1;
                if (mseResults[count].BookingSource != HotelBookingSource.TBOConnect && mseResults[count].BookingSource != HotelBookingSource.DOTW)
                {
                    Dictionary<string, decimal> rateofExchangeList = WSHotelSearchRequest.rateOfExList;
                    rateofExchange = rateofExchangeList[mseResults[count].Currency];
                }
                #endregion
                for (int iR = 0; iR < request.noOfRooms; iR++)
                {
                    string[] stringArray = new string[] { "###" };
                    string[] roomCodes = request.RoomCodes[iR].Split(stringArray, StringSplitOptions.None);

                    Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, memberId, "RoomTypeCode:" + request.RoomCodes[iR].ToString() + "      After split :" + roomCodes[0].ToString() + "  &" + roomCodes[1].ToString() + " mseresult.roomtypecode :" + mseResults[count].RoomDetails[0].RoomTypeCode.Trim() + " mseresult.rateplancode :" + mseResults[count].RoomDetails[0].RatePlanCode.Trim(), "");
                    //Cache results
                    for (int j = 0; j < mseResults[count].RoomDetails.Length; j++)
                    {
                        if (mseResults[count].RoomDetails[j].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && mseResults[count].RoomDetails[j].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                        {
                            mseRate += mseResults[count].RoomDetails[j].SellingFare * rateofExchange + mseResults[count].RoomDetails[j].TotalTax * rateofExchange;
                            if (mseResults[count].BookingSource == HotelBookingSource.TBOConnect)
                            {
                                serviceTax = (CT.AccountingEngine.AccountingEngine.CalculateProductServiceTax(mseResults[count].RoomDetails[j].TotalPrice, true, ProductType.Hotel)) * rateofExchange;
                            }
                            else
                            {
                                serviceTax = (CT.AccountingEngine.AccountingEngine.CalculateProductServiceTax(mseResults[count].RoomDetails[j].TotalPrice, false, ProductType.Hotel)) * rateofExchange;
                            }
                            mseRate += serviceTax;
                        }
                    }
                }
                for (int jR = 0; jR < request.noOfRooms; jR++)
                {
                    string[] stringArray = new string[] { "###" };
                    string[] roomCodes = request.RoomCodes[jR].Split(stringArray, StringSplitOptions.None);
                    //API results
                    for (int k = 0; k < request.RoomDetails.Length; k++)
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, memberId, "Request RoomTypeCode:"
                            + request.RoomDetails[k].RoomTypeCode.ToString() + "=" + roomCodes[0].ToString()
                            + " || " + request.RoomDetails[k].RatePlanCode.ToString() + "=" + roomCodes[1].ToString(), "");
                        if (request.RoomDetails[k].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && request.RoomDetails[k].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                        {
                            apiRate += (request.RoomDetails[k].RoomRate.TotalRoomRate + request.RoomDetails[k].RoomRate.TotalRoomTax) * request.RoomDetails[k].NoOfUnits;
                            apiRate += request.RoomDetails[k].RoomRate.ServiceTax;
                        }
                    }
                }
            }
            Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, memberId, "MSE Rate:" + mseRate.ToString() + " || API Rate:" + apiRate.ToString(), "");

            //if mseRate is equal to apiRate or difference between mseRate and apiRate is less then 2 or greater then -2 and mseRate is greater then zero
            if (((mseRate == apiRate) || ((mseRate - apiRate) >= -1 && (mseRate - apiRate) <= 1)) && mseRate > 0)
            {
                mseResult = mseResults[count];
                return mseResult;
            }
            else
            {
                throw new ArgumentException("Booking Request price(" + apiRate.ToString() + ") was changed. Actual price is " + mseRate.ToString());
            }

        }
        else
        {
            throw new ArgumentException("Either HotelCode or HotelName or index entered by user is incorrect");
        }
        if(mseResult == null)
        {
            throw new ArgumentException("Rate of RoomDetails not matched with the RoomDetails user entered as input perameters.");
        }
        return mseResult;
    }

    public static HotelItinerary BuildHotelItinerary(WSBookRequest request, UserMaster member, HotelSearchResult hotelResult, LoginInfo AgentLoginInfo)
    {
        HotelItinerary itineary = new HotelItinerary();        
        PriceType priceType;
        Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
        StaticData staticInfo = new StaticData();
        staticInfo.BaseCurrency = AgentLoginInfo.Currency;  //Added by brahmam
        rateOfExList = staticInfo.CurrencyROE;
        decimal rateofExchange = 1;
        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
        {
            itineary.BookingMode = BookingMode.WhiteLabel;
        }
        else
        {
            itineary.BookingMode = BookingMode.BookingAPI;
        }
        itineary.EndDate = hotelResult.EndDate;
        itineary.StartDate = hotelResult.StartDate;
        itineary.HotelCode = hotelResult.HotelCode;
        itineary.HotelAddress1 = hotelResult.HotelAddress;
        itineary.HotelAddress2 = hotelResult.HotelContactNo;
        itineary.HotelName = hotelResult.HotelName;
        itineary.NoOfRooms = request.NoOfRooms;
        itineary.Rating = hotelResult.Rating;
        itineary.CityRef = hotelResult.CityCode;
        itineary.Map = hotelResult.HotelMap;
        itineary.CityCode = hotelResult.CityCode;
        itineary.Source = hotelResult.BookingSource;
        itineary.CreatedBy = (int)member.ID; //Added by brahmam
        itineary.AgencyId = member.AgentId;//Added by brahmam
        itineary.LocationId =(int) member.LocationID;
        if (itineary.Source == HotelBookingSource.Desiya || itineary.Source == HotelBookingSource.TBOConnect)
        {
            itineary.IsDomestic = true;
        }
        else
        {
            itineary.IsDomestic = false;
        }
        // for DOTW assiging Nationality and Country of Residence -- modified by ziya 2013-may-15
        //if (itineary.Source == HotelBookingSource.DOTW) commented by brahmam GTA Also required
        {
            itineary.PassengerCountryOfResidence = request.passengerCountryOfResidence;
            itineary.PassengerNationality= request.passengerNationality;
        }
        //Get Hotel source commission type
        HotelSource hSource = new HotelSource();

        hSource.Load(hotelResult.BookingSource.ToString());
        //since there are two enums of similar type where one is in BookingEngine(PriceAccounts) & other in CoreLogic(Enumerator).
        if (hSource.FareTypeId == FareType.Net)
        {
            priceType = PriceType.NetFare;
        }
        else
        {
            priceType = PriceType.PublishedFare;
        }

        bool isMultiRoom = false;
        if (!itineary.IsDomestic && itineary.NoOfRooms > 1)
        {
            isMultiRoom = true;
        }
        itineary.TransType = "B2C";
        //#region For GTA
        ////This Code is taken from HotelPaxDetails page for getting FareBreakDown List
        //if (itineary.Source == HotelBookingSource.GTA)
        //{
        //    HotelRequest req = new HotelRequest();
        //    req.StartDate = hotelResult.StartDate;
        //    req.EndDate = hotelResult.EndDate;
        //    req.RoomGuest = hotelResult.RoomGuest;
        //    req.NoOfRooms = request.NoOfRooms;
        //    GTACity gtaCity = new GTACity();
        //    string country = gtaCity.GetCountryNameforCityCode(hotelResult.CityCode);
        //    req.CountryName = country;
        //    GTA gtaApi = new GTA();
        //    List<string> roomTypeDetails = new List<string>();
        //    HotelSource hotelS = new HotelSource();
        //    System.TimeSpan diffResult = req.EndDate.Subtract(req.StartDate);
        //    roomTypeDetails = gtaApi.GetRoomType(req);
        //    decimal discount = 0;
        //    Dictionary<string, RoomRates[]> fareBreakList = gtaApi.GetFareBreakDown(req, itineary.HotelCode, itineary.CityCode, ref discount);
        //    for (int k = 0; k < roomTypeDetails.Count; k++)
        //    {
        //        HotelRoomsDetails roomdetail = new HotelRoomsDetails();
        //        RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
        //        roomdetail.SequenceNo = (k + 1).ToString();
        //        List<int> childAgeList = new List<int>();
        //        string childAgeString = string.Empty;
        //        if (req.RoomGuest[k].childAge != null)
        //        {
        //            childAgeList = req.RoomGuest[k].childAge;
        //        }
        //        for (int x = 0; x < childAgeList.Count; x++)
        //        {
        //            if (x == 0)
        //            {
        //                childAgeString = childAgeList[x].ToString();
        //            }
        //            else
        //            {
        //                childAgeString += "," + childAgeList[x].ToString();
        //            }
        //        }
        //        string name = string.Empty;
        //        string[] tempList = roomTypeDetails[k].Split('|');
        //        switch (tempList[0])
        //        {
        //            case "SB": name = "Single Room";
        //                break;
        //            case "DB": name = "Double Room";
        //                break;
        //            case "TS": name = "Twin for Sole use";
        //                break;
        //            case "TR": name = "Triple Room";
        //                break;
        //            case "TB": name = "Twin Room";
        //                break;
        //            case "Q": name = "Quad Room";
        //                break;
        //        }
        //        if (Convert.ToInt16(tempList[1]) != 0)
        //        {
        //            name += " with " + tempList[1] + " Cot(s)";
        //        }
        //        if (Convert.ToBoolean(Convert.ToInt16(tempList[2])))
        //        {
        //            if (isMultiRoom)
        //            {
        //                name += " with Child (" + childAgeString + ")";
        //            }
        //            else
        //            {
        //                name += " with Extra Bed.";
        //            }
        //        }
        //        roomdetail.RoomTypeName = name;
        //        roomdetail.RoomTypeCode = tempList[0] + "|" + tempList[1] + "|" + tempList[2] + "|" + childAgeString + "|" + itineary.HotelCode;


        //        foreach (string rateType in fareBreakList.Keys)
        //        {
        //            if (rateType == tempList[0])
        //            {
        //                int r = 0;
        //                foreach (RoomRates rateInfo in fareBreakList[tempList[0]])
        //                {
        //                    hRoomRates[r].Amount = rateInfo.Amount;
        //                    hRoomRates[r].BaseFare = rateInfo.BaseFare;
        //                    hRoomRates[r].SellingFare = rateInfo.SellingFare;
        //                    hRoomRates[r].Days = rateInfo.Days;
        //                    hRoomRates[r].RateType = rateInfo.RateType;
        //                    hRoomRates[r].Totalfare = rateInfo.Totalfare;
        //                    r++;
        //                }

        //            }
        //            else
        //            {
        //                string[] childDetail = rateType.Split('|');
        //                if (childDetail.Length > 1 && childDetail[1] == "CH")
        //                {
        //                    int countAge = 0;

        //                    foreach (int age in childAgeList)
        //                    {
        //                        if (Convert.ToInt16(childDetail[0]) == age)
        //                        {
        //                            countAge++;
        //                        }
        //                    }
        //                    RoomRates[] childRate = fareBreakList[rateType];
        //                    for (int d = 0; d < diffResult.Days; d++)
        //                    {
        //                        hRoomRates[d].Amount += childRate[d].Amount * countAge;
        //                        hRoomRates[d].BaseFare += childRate[d].BaseFare * countAge;
        //                        hRoomRates[d].SellingFare += childRate[d].SellingFare * countAge;
        //                        hRoomRates[d].Totalfare += childRate[d].Totalfare * countAge;
        //                    }
        //                }
        //            }
        //        }
        //        hotelResult.RoomDetails[k].Rates = hRoomRates;
        //    }
        //}
        //#endregion


        //creating room info
        HotelRoom[] roomInfo = new HotelRoom[itineary.NoOfRooms];

        if (isMultiRoom)
        {
            #region block for multiple room booking

            int z = 0;//index for each hotel room
            for (int iR = 0; iR < itineary.NoOfRooms; iR++)
            {
                string[] stringArray = new string[] { "###" };
                string[] roomCodes = request.RoomCodes[iR].Split(stringArray, StringSplitOptions.None);
                //if (hotelResult.BookingSource == HotelBookingSource.DOTW || hotelResult.BookingSource == HotelBookingSource.HotelBeds)
                //{
                for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                {
                    if (hotelResult.BookingSource == HotelBookingSource.DOTW || hotelResult.BookingSource == HotelBookingSource.HotelBeds)
                    {
                        if (hotelResult.RoomDetails[j].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && hotelResult.RoomDetails[j].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                        {
                            HotelRoom roomdata = new HotelRoom();
                            if (itineary.Source == HotelBookingSource.GTA)
                            {
                                string[] roomList = hotelResult.RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomTypeCode = roomList[0];
                                roomdata.NoOfCots = Convert.ToInt16(roomList[1]);
                                roomdata.ExtraBed = Convert.ToBoolean(Convert.ToInt16(roomList[2]));
                            }
                            else if (itineary.Source == HotelBookingSource.TBOConnect)
                            {
                                string[] roomList = hotelResult.RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomId = Convert.ToInt32(roomList[0]);
                                roomdata.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;
                            }
                            else
                            {
                                roomdata.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = hotelResult.RoomDetails[j].RoomTypeName;
                            roomdata.MealPlanDesc = hotelResult.RoomDetails[j].mealPlanDesc;
                            roomdata.RatePlanCode = hotelResult.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = hotelResult.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = hotelResult.RoomDetails[j].mealPlanDesc;
                            roomdata.ChildCharge = hotelResult.RoomDetails[j].ChildCharges;
                            roomdata.ExtraGuestCharge = hotelResult.RoomDetails[j].SellExtraGuestCharges;
                            if (itineary.Source == HotelBookingSource.DOTW)
                            {
                                roomdata.EssentialInformation = hotelResult.RoomDetails[j].EssentialInformation;
                                roomdata.CancelRestricted = hotelResult.RoomDetails[j].CancelRestricted;
                            }
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[hotelResult.RoomDetails[j].Rates.Length];
                            for (int k = 0; k < hotelResult.RoomDetails[j].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = hotelResult.RoomDetails[j].Rates[k].Days;
                                fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare;
                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            //room guest for each room
                            roomdata.AdultCount = hotelResult.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = hotelResult.RoomGuest[iR].noOfChild;
                            List<int> childAge = new List<int>();
                            if (hotelResult.RoomGuest[iR].childAge != null && hotelResult.RoomGuest[iR].childAge.Count > 0)
                            {
                                foreach (int age in hotelResult.RoomGuest[iR].childAge)
                                {
                                    childAge.Add(age);
                                }
                            }
                            roomdata.ChildAge = childAge;
                            roomdata.NoOfUnits = "1";
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                            int nights = DiffResult.Days;

                            //roomdata.Price = CT.AccountingEngine.AccountingEngine.GetPrice(hotelResult, roomCodes[0], member.AgentId, 0, 1, nights, priceType);

                            {
                                roomdata.Price = new PriceAccounts();
                                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                                for (int m = j; m < hotelResult.RoomDetails.Length; m++)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[hotelResult.RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < hotelResult.RoomDetails[m].Rates.Length; k++)
                                    {
                                        grossFare += hotelResult.RoomDetails[m].Rates[k].SellingFare;
                                        totalExtraGuestCharge += hotelResult.RoomDetails[m].SellExtraGuestCharges;
                                        roomdata.Price.Discount = hotelResult.RoomDetails[m].Discount;
                                        roomdata.Price.Markup = hotelResult.RoomDetails[m].Markup;
                                        roomdata.Price.MarkupValue = hotelResult.RoomDetails[m].MarkupValue;
                                        roomdata.Price.MarkupType = hotelResult.RoomDetails[m].MarkupType;
                                        roomdata.Price.DiscountValue = hotelResult.RoomDetails[m].DiscountValue;
                                        roomdata.Price.DiscountType = hotelResult.RoomDetails[m].DiscountType;
                                        roomdata.Price.B2CMarkup = hotelResult.RoomDetails[m].B2CMarkup;
                                        roomdata.Price.B2CMarkupType = hotelResult.RoomDetails[m].B2CMarkupType;
                                        roomdata.Price.B2CMarkupValue = hotelResult.RoomDetails[m].B2CMarkupValue;
                                        roomdata.Price.InputVATAmount = hotelResult.RoomDetails[m].InputVATAmount;
                                        roomdata.Price.TaxDetails = hotelResult.RoomDetails[m].TaxDetail;
                                        roomdata.Price.OutputVATAmount = Math.Round(request.outPutVatAmount / request.noOfRooms, roomdata.Price.DecimalPoint);
                                        if (hotelResult.RoomDetails[m].supplierPrice > 0)
                                        {
                                            roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].supplierPrice;
                                        }
                                        else
                                        {
                                            roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].TotalPrice / hotelResult.Price.RateOfExchange;
                                        }
                                        roomdata.Price.SupplierCurrency = hotelResult.Price.SupplierCurrency;
                                        roomdata.Price.RateOfExchange = hotelResult.Price.RateOfExchange;
                                    }

                                    totalChildCharge = hotelResult.RoomDetails[m].ChildCharges;
                                    totalTax = hotelResult.RoomDetails[m].TotalTax;
                                    break;

                                }
                                {
                                    roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                                }
                                roomdata.Price.Tax = totalTax;
                                //roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelResult.Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;
                            }
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = hotelResult.Currency;
                            //if (rateOfExList.ContainsKey(hotelResult.Currency))
                            //{
                            //    roomdata.Price.RateOfExchange = rateOfExList[hotelResult.Currency];
                            //}
                            //else
                            //{
                            //    roomdata.Price.RateOfExchange = 1;
                            //}
                            //roomdata.Price.Markup = roomdata.Price.Markup;
                            roomdata.Price.Currency = hotelResult.Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }
                    else if (itineary.Source == HotelBookingSource.GTA)
                    {
                        if (hotelResult.RoomDetails[j].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && hotelResult.RoomDetails[j].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                        {
                            //j = Convert.ToInt32(rCode);
                            decimal price = 0;
                            {
                                price = hotelResult.RoomDetails[j].SellingFare;
                            }
                            if (hotelResult.RoomDetails[j].RoomTypeName.Contains("|"))
                            {
                                string[] names = hotelResult.RoomDetails[j].RoomTypeName.Split('|');
                                price = hotelResult.RoomDetails[j].TotalPrice / names.Length;
                            }
                            price = Math.Round(price, 2);
                           // rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                            if (hotelResult.RoomDetails[j].RoomTypeName != null)
                            {
                                HotelRoom roomdata = new HotelRoom();
                                roomdata.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;
                                {
                                    roomdata.RoomName = hotelResult.RoomDetails[j].RoomTypeName;
                                }
                                roomdata.RatePlanCode = hotelResult.RoomDetails[j].RatePlanCode;
                                roomdata.Ameneties = hotelResult.RoomDetails[j].Amenities;
                                roomdata.MealPlanDesc = hotelResult.RoomDetails[j].mealPlanDesc;
                                roomdata.EssentialInformation = hotelResult.RoomDetails[j].EssentialInformation;
                                roomdata.NoOfCots = hotelResult.RoomDetails[j].NumberOfCots;
                                roomdata.NoOfExtraBed = hotelResult.RoomDetails[j].MaxExtraBeds;
                                roomdata.ExtraBed = hotelResult.RoomDetails[j].IsExtraBed;
                                roomdata.SharingBed = hotelResult.RoomDetails[j].SharingBedding;
                                itineary.HotelCancelPolicy = hotelResult.RoomDetails[j].CancellationPolicy;
                                roomdata.ExtraGuestCharge = hotelResult.RoomDetails[j].SellExtraGuestCharges;
                                roomdata.ChildCharge = hotelResult.RoomDetails[j].ChildCharges;
                                HotelRoomFareBreakDown[] fareInfo;
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    if (k == 0)
                                    {
                                        HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                        fare.Date = hotelResult.RoomDetails[j].Rates[k].Days;
                                        if (hotelResult.BookingSource == HotelBookingSource.RezLive)
                                        {
                                            if (hotelResult.RoomDetails[j].RoomTypeName.Contains("|"))
                                            {
                                                string[] names = hotelResult.RoomDetails[j].RoomTypeName.Split('|');
                                                fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare / names.Length;
                                            }
                                            else
                                            {
                                                fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                            }
                                        }
                                        {
                                            fare.RoomPrice = fare.RoomPrice;
                                        }
                                        fareInfo[k] = fare;
                                    }
                                }
                                roomdata.RoomFareBreakDown = fareInfo;

                                //room guest for each room
                                roomdata.AdultCount = hotelResult.RoomGuest[iR].noOfAdults;
                                roomdata.ChildCount = hotelResult.RoomGuest[iR].noOfChild;
                                List<int> childAge = new List<int>();
                                if (hotelResult.RoomGuest[iR].childAge != null && hotelResult.RoomGuest[iR].childAge.Count > 0)
                                {
                                    foreach (int age in hotelResult.RoomGuest[iR].childAge)
                                    {
                                        childAge.Add(age);
                                    }
                                }
                                roomdata.ChildAge = childAge;

                                roomdata.NoOfUnits = "1";//for each room there will be only one unit
                                roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                                System.TimeSpan DiffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                int nights = DiffResult.Days;
                                //No need to call Get Price as it is already set for HotelResult
                                roomdata.Price = new PriceAccounts();
                                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                                for (int m = j; m < hotelResult.RoomDetails.Length; m++)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[1];
                                    for (int k = 0; k < 1; k++)
                                    {
                                        grossFare += hotelResult.RoomDetails[m].SellingFare;
                                        roomdata.Price.Markup = hotelResult.RoomDetails[m].Markup;
                                        roomdata.Price.Discount = hotelResult.RoomDetails[m].Discount;
                                        roomdata.Price.MarkupValue = hotelResult.RoomDetails[m].MarkupValue;
                                        roomdata.Price.MarkupType = hotelResult.RoomDetails[m].MarkupType;
                                        roomdata.Price.DiscountValue = hotelResult.RoomDetails[m].DiscountValue;
                                        roomdata.Price.DiscountType = hotelResult.RoomDetails[m].DiscountType;
                                        roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].supplierPrice;
                                        roomdata.Price.SupplierCurrency = hotelResult.Price.SupplierCurrency;
                                        roomdata.Price.RateOfExchange = hotelResult.Price.RateOfExchange;
                                        roomdata.Price.B2CMarkup = hotelResult.RoomDetails[m].B2CMarkup;
                                        roomdata.Price.B2CMarkupType = hotelResult.RoomDetails[m].B2CMarkupType;
                                        roomdata.Price.B2CMarkupValue = hotelResult.RoomDetails[m].B2CMarkupValue;
                                        roomdata.Price.InputVATAmount = hotelResult.RoomDetails[m].InputVATAmount;
                                        roomdata.Price.TaxDetails = hotelResult.RoomDetails[m].TaxDetail;
                                        roomdata.Price.OutputVATAmount = Math.Round(request.outPutVatAmount / request.noOfRooms, roomdata.Price.DecimalPoint);
                                    }
                                    totalExtraGuestCharge = hotelResult.RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = hotelResult.RoomDetails[m].ChildCharges;
                                    totalTax = hotelResult.RoomDetails[m].TotalTax;
                                    break;
                                }
                                {
                                    roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                                }
                                roomdata.Price.Tax = totalTax;
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelResult.Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;

                                roomdata.Price.AccPriceType = priceType;

                                roomdata.Price.CurrencyCode = hotelResult.Currency;
                                roomdata.Price.Currency = hotelResult.Currency;
                                roomInfo[z++] = roomdata;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (hotelResult.RoomDetails[j].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && hotelResult.RoomDetails[j].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                        {
                            //if (itineary.Source != HotelBookingSource.HotelConnect)
                            //{
                            //    j = Convert.ToInt32(roomCodes[0]);
                            //}
                            decimal price = 0;
                            //CT.Core1.StaticData staticInfo = new CT.Core1.StaticData();
                            //staticInfo.BaseCurrency = agency.AgentCurrency;
                            //Dictionary<string, decimal> rateOfExchange = staticInfo.CurrencyROE;
                            //if (result.Currency == "USD")
                            // {
                            //     price = result.RoomDetails[j].SellingFare * (decimal)Settings.LoginInfo.AgentExchangeRates[result.Currency];
                            // }
                            // else
                            {
                                price = hotelResult.RoomDetails[j].SellingFare;
                            }
                            if (hotelResult.RoomDetails[j].RoomTypeName.Contains("|"))
                            {
                                string[] names = hotelResult.RoomDetails[j].RoomTypeName.Split('|');
                                price = hotelResult.RoomDetails[j].TotalPrice / names.Length;
                            }
                            price = Math.Round(price, 2);
                            //rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                            //For RezLive update based on room index


                            if (hotelResult.RoomDetails[j].RoomTypeName != null)
                            {
                                HotelRoom roomdata = new HotelRoom();


                                roomdata.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;


                                //if (result.RoomDetails[j].RoomTypeName.Split('|').Length > 0)
                                //{
                                //    roomdata.RoomName = result.RoomDetails[j].RoomTypeName.Split('|')[iR];
                                //}
                                //else
                                {
                                    roomdata.RoomName = hotelResult.RoomDetails[j].RoomTypeName;
                                }


                                roomdata.RatePlanCode = hotelResult.RoomDetails[j].RatePlanCode;
                                roomdata.Ameneties = hotelResult.RoomDetails[j].Amenities;
                                roomdata.MealPlanDesc = hotelResult.RoomDetails[j].mealPlanDesc;
                                roomdata.ExtraGuestCharge = hotelResult.RoomDetails[j].SellExtraGuestCharges;
                                roomdata.ChildCharge = hotelResult.RoomDetails[j].ChildCharges;
                                HotelRoomFareBreakDown[] fareInfo;


                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    if (k == 0)
                                    {
                                        HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                        fare.Date = hotelResult.RoomDetails[j].Rates[k].Days;
                                        if (hotelResult.BookingSource == HotelBookingSource.RezLive)
                                        {
                                            if (hotelResult.RoomDetails[j].RoomTypeName.Contains("|"))
                                            {
                                                string[] names = hotelResult.RoomDetails[j].RoomTypeName.Split('|');
                                                fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare / names.Length;
                                            }
                                            else
                                            {
                                                fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                            }
                                        }
                                        else if (hotelResult.BookingSource == HotelBookingSource.LOH)
                                        {
                                            fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare;
                                        }
                                        else
                                        {
                                            fare.RoomPrice = hotelResult.RoomDetails[j].TotalPrice;
                                        }
                                        //if (result.Currency != "AED")
                                        //{
                                        //    fare.RoomPrice = fare.RoomPrice * rateOfExList[result.Currency];
                                        //}
                                        //else
                                        {
                                            fare.RoomPrice = fare.RoomPrice;
                                        }
                                        fareInfo[k] = fare;
                                    }
                                }
                                roomdata.RoomFareBreakDown = fareInfo;

                                //room guest for each room
                                roomdata.AdultCount = hotelResult.RoomGuest[iR].noOfAdults;
                                roomdata.ChildCount = hotelResult.RoomGuest[iR].noOfChild;
                                List<int> childAge = new List<int>();
                                if (hotelResult.RoomGuest[iR].childAge != null && hotelResult.RoomGuest[iR].childAge.Count > 0)
                                {
                                    foreach (int age in hotelResult.RoomGuest[iR].childAge)
                                    {
                                        childAge.Add(age);
                                    }
                                }
                                roomdata.ChildAge = childAge;

                                roomdata.NoOfUnits = "1";//for each room there will be only one unit
                                roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                                System.TimeSpan DiffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                                int nights = DiffResult.Days;
                                //No need to call Get Price as it is already set for HotelResult
                                roomdata.Price = new PriceAccounts();
                                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                                for (int m = j; m < hotelResult.RoomDetails.Length; m++)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[1];
                                    for (int k = 0; k < 1; k++)
                                    {
                                        if (hotelResult.BookingSource == HotelBookingSource.RezLive)
                                        {
                                            if (hotelResult.RoomDetails[j].RoomTypeName.Contains("|"))
                                            {
                                                string[] names = hotelResult.RoomDetails[j].RoomTypeName.Split('|');
                                                grossFare += hotelResult.RoomDetails[m].Rates[k].SellingFare / names.Length;
                                            }
                                            else
                                            {
                                                grossFare += hotelResult.RoomDetails[m].Rates[k].SellingFare / request.NoOfRooms;
                                            }
                                        }
                                        else if (hotelResult.BookingSource == HotelBookingSource.LOH)
                                        {
                                            grossFare += hotelResult.RoomDetails[m].SellingFare;
                                            roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].supplierPrice;
                                            roomdata.Price.PublishedFare = hotelResult.RoomDetails[m].supplierPrice * rateOfExList["USD"];
                                        }
                                        roomdata.Price.Markup = hotelResult.RoomDetails[m].Markup;
                                        roomdata.Price.Discount = hotelResult.RoomDetails[m].Discount;
                                        roomdata.Price.MarkupValue = hotelResult.RoomDetails[m].MarkupValue;
                                        roomdata.Price.MarkupType = hotelResult.RoomDetails[m].MarkupType;
                                        roomdata.Price.DiscountValue = hotelResult.RoomDetails[m].DiscountValue;
                                        roomdata.Price.DiscountType = hotelResult.RoomDetails[m].DiscountType;
                                        roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].supplierPrice;
                                        roomdata.Price.SupplierCurrency = (hotelResult.Price != null && hotelResult.Price.SupplierCurrency != null ? hotelResult.Price.SupplierCurrency : Settings.LoginInfo.Currency);
                                        roomdata.Price.RateOfExchange = (hotelResult.Price != null ? hotelResult.Price.RateOfExchange : 1);
                                        roomdata.Price.B2CMarkup = hotelResult.RoomDetails[m].B2CMarkup;
                                        roomdata.Price.B2CMarkupType = hotelResult.RoomDetails[m].B2CMarkupType;
                                        roomdata.Price.B2CMarkupValue = hotelResult.RoomDetails[m].B2CMarkupValue;
                                        roomdata.Price.InputVATAmount = hotelResult.RoomDetails[m].InputVATAmount;
                                        roomdata.Price.TaxDetails = hotelResult.RoomDetails[m].TaxDetail;
                                        roomdata.Price.OutputVATAmount = Math.Round(request.outPutVatAmount / request.noOfRooms, roomdata.Price.DecimalPoint);
                                    }
                                    totalExtraGuestCharge = hotelResult.RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = hotelResult.RoomDetails[m].ChildCharges;
                                    totalTax = hotelResult.RoomDetails[m].TotalTax;
                                    break;
                                }
                                //if (result.Currency != "AED")
                                //{
                                //    roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * rateOfExList[result.Currency];
                                //}
                                //else
                                {
                                    roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                                    //roomdata.Price.Markup = result.Price.Markup;
                                }
                                roomdata.Price.Tax = totalTax;
                                //roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelResult.Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;

                                roomdata.Price.AccPriceType = priceType;

                                roomdata.Price.CurrencyCode = hotelResult.Currency;
                                //if (rateOfExList.ContainsKey(result.Currency))
                                //{
                                //    roomdata.Price.RateOfExchange = rateOfExList[result.Currency];
                                //}
                                //else
                                //{
                                //    roomdata.Price.RateOfExchange = 1;
                                //}
                                //roomdata.Price.Markup = result.Price.Markup;
                                roomdata.Price.Currency = hotelResult.Currency;
                                roomInfo[z++] = roomdata;
                                break;
                            }


                        }
                    }
                }
            }
            #endregion
        }
        else
        {
            #region block for single room booking

            string[] stringArray = new string[] { "###" };
            string[] roomCodes = request.RoomCodes[0].Split(stringArray, StringSplitOptions.None);

            for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
            {
                if (hotelResult.BookingSource == HotelBookingSource.DOTW || hotelResult.BookingSource == HotelBookingSource.HotelBeds)
                {
                    if (hotelResult.RoomDetails[j].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && hotelResult.RoomDetails[j].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                    {
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            if (itineary.Source == HotelBookingSource.GTA)
                            {
                                string[] roomList = hotelResult.RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomTypeCode = roomList[0];
                                roomdata.NoOfCots = Convert.ToInt16(roomList[1]);
                                roomdata.ExtraBed = Convert.ToBoolean(Convert.ToInt16(roomList[2]));
                            }
                            else if (itineary.Source == HotelBookingSource.TBOConnect)
                            {
                                string[] roomList = hotelResult.RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomId = Convert.ToInt32(roomList[0]);
                                roomdata.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;
                            }
                            else
                            {
                                roomdata.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = hotelResult.RoomDetails[j].RoomTypeName;
                            roomdata.MealPlanDesc = hotelResult.RoomDetails[j].mealPlanDesc;
                            roomdata.RatePlanCode = hotelResult.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = hotelResult.RoomDetails[j].Amenities;
                            roomdata.ExtraGuestCharge = hotelResult.RoomDetails[j].SellExtraGuestCharges / request.NoOfRooms;
                            if (hotelResult.BookingSource == HotelBookingSource.DOTW) { 
                             roomdata.EssentialInformation = hotelResult.RoomDetails[j].EssentialInformation;
                            }

                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[hotelResult.RoomDetails[j].Rates.Length];
                            for (int k = 0; k < hotelResult.RoomDetails[j].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = hotelResult.RoomDetails[j].Rates[k].Days;

                                //Added by brahmam changed based on b2b code 
                                if (request.NoOfRooms == hotelResult.RoomDetails.Length)
                                {
                                    if (itineary.Source == HotelBookingSource.IAN)
                                    {
                                        fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].Totalfare;
                                    }
                                    else
                                    {
                                        fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare * request.NoOfRooms;
                                    }
                                }
                                else
                                {
                                    if (itineary.Source == HotelBookingSource.IAN)
                                    {
                                        fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].Totalfare / request.NoOfRooms;
                                    }
                                    else
                                    {
                                        if (itineary.Source != HotelBookingSource.RezLive)
                                        {
                                            fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                        }
                                        else
                                        {
                                            fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].Totalfare / request.NoOfRooms;
                                        }
                                    }
                                }
                                //fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            roomdata.AdultCount = hotelResult.RoomGuest[i].noOfAdults;
                            List<int> childAge = new List<int>();
                            if (hotelResult.RoomGuest[i].childAge != null && hotelResult.RoomGuest[i].childAge.Count > 0)
                            {
                                foreach (int age in hotelResult.RoomGuest[i].childAge)
                                {
                                    childAge.Add(age);
                                }
                            }
                            roomdata.ChildAge = childAge;
                            roomdata.ChildCount = hotelResult.RoomGuest[i].noOfChild;
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            roomdata.NoOfUnits = "1";
                            System.TimeSpan DiffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                            int nights = DiffResult.Days;
                            //if (itineary.Source != HotelBookingSource.IAN && itineary.Source != HotelBookingSource.TBOConnect) // check for TBO
                            //{
                            //    roomdata.Price = CT.AccountingEngine.AccountingEngine.GetPrice(hotelResult, roomCodes[0], member.AgentId, 0, request.NoOfRooms, nights, priceType);
                            //}
                            //else
                            {
                                roomdata.Price = new PriceAccounts();
                                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                                for (int m = 0; m < hotelResult.RoomDetails.Length; m++)
                                {
                                    if (hotelResult.RoomDetails[m].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && hotelResult.RoomDetails[m].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                                    {
                                        fareInfo = new HotelRoomFareBreakDown[hotelResult.RoomDetails[m].Rates.Length];
                                        for (int k = 0; k < hotelResult.RoomDetails[m].Rates.Length; k++)
                                        {
                                            //grossFare += hotelResult.RoomDetails[m].Rates[k].Amount;//commented by brahmam based on new b2b
                                            grossFare += hotelResult.RoomDetails[m].Rates[k].SellingFare;
                                            roomdata.Price.Discount = hotelResult.RoomDetails[m].Discount;
                                            totalExtraGuestCharge = hotelResult.RoomDetails[m].SellExtraGuestCharges;
                                            roomdata.Price.Markup = hotelResult.RoomDetails[m].Markup;
                                            roomdata.Price.MarkupValue = hotelResult.RoomDetails[m].MarkupValue;
                                            roomdata.Price.MarkupType = hotelResult.RoomDetails[m].MarkupType;
                                            roomdata.Price.DiscountValue = hotelResult.RoomDetails[m].DiscountValue;
                                            roomdata.Price.DiscountType = hotelResult.RoomDetails[m].DiscountType;
                                            roomdata.Price.B2CMarkup = hotelResult.RoomDetails[m].B2CMarkup;
                                            roomdata.Price.B2CMarkupType = hotelResult.RoomDetails[m].B2CMarkupType;
                                            roomdata.Price.B2CMarkupValue = hotelResult.RoomDetails[m].B2CMarkupValue;
                                            roomdata.Price.InputVATAmount = hotelResult.RoomDetails[m].InputVATAmount;
                                            roomdata.Price.TaxDetails = hotelResult.RoomDetails[m].TaxDetail;
                                            roomdata.Price.OutputVATAmount = Math.Round(request.outPutVatAmount / request.noOfRooms, roomdata.Price.DecimalPoint);
                                            if (hotelResult.RoomDetails[m].supplierPrice > 0)
                                            {
                                                roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].supplierPrice;
                                            }
                                            else
                                            {
                                                if (hotelResult.Price.RateOfExchange == 0)
                                                {
                                                    hotelResult.Price.RateOfExchange = 1;
                                                }
                                                roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].TotalPrice / hotelResult.Price.RateOfExchange;
                                            }
                                            roomdata.Price.RateOfExchange = hotelResult.Price.RateOfExchange;
                                            roomdata.Price.SupplierCurrency = hotelResult.Price.SupplierCurrency;
                                        }
                                        totalTax = hotelResult.RoomDetails[m].TotalTax;
                                        totalChildCharge = hotelResult.RoomDetails[m].ChildCharges;
                                        break;
                                    }
                                }
                                roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                                //roomdata.Price.PublishedFare = grossFare / request.NoOfRooms;
                                roomdata.Price.Tax = totalTax / request.NoOfRooms;
                                # region for TBO agent Commission & Service Tax
                                if (itineary.Source == HotelBookingSource.TBOConnect)
                                {
                                    decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelResult.Price.AgentCommission / 100;
                                    roomdata.Price.PublishedFare += agentComm;
                                    roomdata.Price.AgentCommission = agentComm;
                                    roomdata.Price.SeviceTax = CT.AccountingEngine.AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                                }
                                # endregion
                            }
                            if (hSource.CommissionTypeId == CommissionType.Fixed)
                            {
                                //roomdata.Price.Markup = roomdata.Price.Markup / request.NoOfRooms;
                            }
                            roomdata.Price.AccPriceType = priceType;
                            //if (itineary.Source == HotelBookingSource.Desiya)
                            //{

                            //    roomdata.Price.RateOfExchange = 1;
                            //    roomdata.Price.CurrencyCode = "INR";
                            //}
                            //else
                            //{
                            //    roomdata.Price.CurrencyCode = hotelResult.Currency;
                            //    if (rateOfExList.ContainsKey(hotelResult.Currency))
                            //    {
                            //        roomdata.Price.RateOfExchange = rateOfExList[hotelResult.Currency];
                            //    }
                            //    else
                            //    {
                            //        roomdata.Price.RateOfExchange = 1;
                            //    }
                            //}
                            roomdata.Price.CurrencyCode = hotelResult.Currency;
                            roomdata.Price.Currency = hotelResult.Currency;
                            roomInfo[i] = roomdata;
                        }
                        break;
                    }
                }
                else if (hotelResult.BookingSource == HotelBookingSource.LOH)
                {
                    if (hotelResult.RoomDetails[j].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && hotelResult.RoomDetails[j].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                    {

                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();

                            {
                                roomdata.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = hotelResult.RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = hotelResult.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = hotelResult.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = hotelResult.RoomDetails[j].mealPlanDesc;
                            roomdata.ExtraGuestCharge = hotelResult.RoomDetails[j].SellExtraGuestCharges / request.NoOfRooms;
                            roomdata.ChildCharge = hotelResult.RoomDetails[j].ChildCharges / request.NoOfRooms;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[hotelResult.RoomDetails[j].Rates.Length];
                            for (int k = 0; k < hotelResult.RoomDetails[j].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = hotelResult.RoomDetails[j].Rates[k].Days;
                                if (request.NoOfRooms == hotelResult.RoomDetails.Length)
                                {
                                    fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare * request.NoOfRooms;
                                    //if (result.Currency != "AED")
                                    //{
                                    //    fare.RoomPrice = fare.RoomPrice * rateOfExList[result.Currency];
                                    //}
                                }
                                else
                                {
                                    fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                    //if (result.Currency != "AED")
                                    //{
                                    //    fare.RoomPrice = fare.RoomPrice * rateOfExList[result.Currency];
                                    //}
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = hotelResult.RoomGuest[i].noOfAdults;
                            List<int> childAge = new List<int>();
                            if (hotelResult.RoomGuest[i].childAge != null && hotelResult.RoomGuest[i].childAge.Count > 0)
                            {
                                foreach (int age in hotelResult.RoomGuest[i].childAge)
                                {
                                    childAge.Add(age);
                                }
                            }
                            roomdata.ChildAge = childAge;
                            roomdata.ChildCount = hotelResult.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = 0; m < hotelResult.RoomDetails.Length; m++)
                            {
                                if (m == j)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[hotelResult.RoomDetails[m].Rates.Length];
                                    grossFare = hotelResult.RoomDetails[m].SellingFare;
                                    totalExtraGuestCharge = hotelResult.RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = hotelResult.RoomDetails[m].ChildCharges;
                                    totalTax = hotelResult.RoomDetails[m].TotalTax;
                                    roomdata.Price.Discount = hotelResult.RoomDetails[m].Discount;
                                    roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].supplierPrice;
                                    roomdata.Price.PublishedFare = hotelResult.RoomDetails[m].supplierPrice * rateOfExList["USD"];
                                    roomdata.Price.Markup = hotelResult.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = hotelResult.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = hotelResult.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = hotelResult.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = hotelResult.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = hotelResult.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = hotelResult.Price.RateOfExchange;
                                    roomdata.Price.B2CMarkup = hotelResult.RoomDetails[m].B2CMarkup;
                                    roomdata.Price.B2CMarkupType = hotelResult.RoomDetails[m].B2CMarkupType;
                                    roomdata.Price.B2CMarkupValue = hotelResult.RoomDetails[m].B2CMarkupValue;
                                    roomdata.Price.InputVATAmount = hotelResult.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = hotelResult.RoomDetails[m].TaxDetail;
                                    roomdata.Price.OutputVATAmount = Math.Round(request.outPutVatAmount / request.noOfRooms, roomdata.Price.DecimalPoint);
                                    break;
                                }
                            }
                            {
                                roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                            }


                            roomdata.Price.Tax = totalTax * request.NoOfRooms;
                            if (itineary.Source == HotelBookingSource.TBOConnect)
                            {
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelResult.Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;
                                roomdata.Price.SeviceTax = CT.AccountingEngine.AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                            }

                            //incase of fixed type commission it is giving mark up for all room at once in accounting engine to do change.
                            if (hSource.CommissionTypeId == CT.Core.CommissionType.Fixed)
                            {
                                //roomdata.Price.Markup = roomdata.Price.Markup * request.NoOfRooms;// commented by shiva
                            }
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = hotelResult.Currency;
                            roomdata.Price.Currency = hotelResult.Currency;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
                else if (itineary.Source == HotelBookingSource.GTA)
                {
                    if (hotelResult.RoomDetails[j].RoomTypeCode.Trim().Equals(roomCodes[0].Trim()) && hotelResult.RoomDetails[j].RatePlanCode.Trim().Equals(roomCodes[1].Trim()))
                    {
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            {
                                roomdata.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = hotelResult.RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = hotelResult.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = hotelResult.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = hotelResult.RoomDetails[j].mealPlanDesc;
                            roomdata.EssentialInformation = hotelResult.RoomDetails[j].EssentialInformation;
                            roomdata.NoOfCots = hotelResult.RoomDetails[j].NumberOfCots;
                            roomdata.NoOfExtraBed = hotelResult.RoomDetails[j].MaxExtraBeds;
                            roomdata.ExtraBed = hotelResult.RoomDetails[j].IsExtraBed;
                            roomdata.SharingBed = hotelResult.RoomDetails[j].SharingBedding;
                            itineary.HotelCancelPolicy = hotelResult.RoomDetails[j].CancellationPolicy;
                            roomdata.ExtraGuestCharge = hotelResult.RoomDetails[j].SellExtraGuestCharges / request.NoOfRooms;
                            roomdata.ChildCharge = hotelResult.RoomDetails[j].ChildCharges / request.NoOfRooms;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[hotelResult.RoomDetails[j].Rates.Length];
                            for (int k = 0; k < hotelResult.RoomDetails[j].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = hotelResult.RoomDetails[j].Rates[k].Days;
                                if (request.NoOfRooms == hotelResult.RoomDetails.Length)
                                {
                                    fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare * request.NoOfRooms;
                                }
                                else
                                {
                                    fare.RoomPrice = hotelResult.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = hotelResult.RoomGuest[i].noOfAdults;
                            List<int> childAge = new List<int>();
                            if (hotelResult.RoomGuest[i].childAge != null && hotelResult.RoomGuest[i].childAge.Count > 0)
                            {
                                foreach (int age in hotelResult.RoomGuest[i].childAge)
                                {
                                    childAge.Add(age);
                                }
                            }
                            roomdata.ChildAge = childAge;
                            roomdata.ChildCount = hotelResult.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = 0; m < hotelResult.RoomDetails.Length; m++)
                            {
                                if (m == j)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[hotelResult.RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < hotelResult.RoomDetails[m].Rates.Length; k++)
                                    {
                                        grossFare += hotelResult.RoomDetails[m].Rates[k].SellingFare;
                                    }
                                    totalExtraGuestCharge = hotelResult.RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = hotelResult.RoomDetails[m].ChildCharges;
                                    totalTax = hotelResult.RoomDetails[m].TotalTax;
                                    roomdata.Price.Discount = hotelResult.RoomDetails[m].Discount;
                                    roomdata.Price.Markup = hotelResult.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = hotelResult.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = hotelResult.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = hotelResult.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = hotelResult.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = hotelResult.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = hotelResult.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = hotelResult.Price.RateOfExchange;
                                    roomdata.Price.B2CMarkup = hotelResult.RoomDetails[m].B2CMarkup;
                                    roomdata.Price.B2CMarkupType = hotelResult.RoomDetails[m].B2CMarkupType;
                                    roomdata.Price.B2CMarkupValue = hotelResult.RoomDetails[m].B2CMarkupValue;
                                    roomdata.Price.InputVATAmount = hotelResult.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = hotelResult.RoomDetails[m].TaxDetail;
                                    roomdata.Price.OutputVATAmount = Math.Round(request.outPutVatAmount / request.noOfRooms, roomdata.Price.DecimalPoint);
                                    break;
                                }
                            }
                            roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                            roomdata.Price.Tax = totalTax * request.NoOfRooms;
                            if (itineary.Source == HotelBookingSource.TBOConnect)
                            {
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelResult.Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;
                                roomdata.Price.SeviceTax = CT.AccountingEngine.AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                            }

                            //incase of fixed type commission it is giving mark up for all room at once in accounting engine to do change.
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = hotelResult.Currency;
                            roomdata.Price.Currency = hotelResult.Currency;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
            }
            #endregion
        }
        itineary.Roomtype = roomInfo;
        //currency
        if (hotelResult.BookingSource == HotelBookingSource.LOH)
        {
            itineary.Roomtype[0].Price.SupplierCurrency = hotelResult.Price.SupplierCurrency;
            //if (hotelInfo[index].Price.SupplierPrice == 0)
            {
                foreach (HotelRoom room in itineary.Roomtype)
                {
                    itineary.TotalPrice += room.Price.SupplierPrice;
                }
            }
            //else
            //{
            //    itinerary.TotalPrice = result.Price.SupplierPrice;
            //}
        }
        else
        {
            itineary.TotalPrice = Math.Round(hotelResult.SellingFare, 2, MidpointRounding.AwayFromZero);
        }
        foreach (HotelRoom room in itineary.Roomtype)
        {
            room.Price.DecimalPoint = 2;
        }
        itineary.Currency = hotelResult.Currency;
        if (rateOfExList.ContainsKey(roomInfo[0].Price.Currency))
        {
            rateofExchange = rateOfExList[roomInfo[0].Price.Currency];
        }
        else
        {
            rateofExchange = 1;
        }


        MetaSearchEngine mse = new MetaSearchEngine(request.SessionId);
        LoginInfo loginInfo = AgentLoginInfo;
        mse.SettingsLoginInfo = loginInfo;
        Dictionary<string, string> polList = new Dictionary<string, string>();
        try
        {
            #region Hotel Agreement Details

            List<HotelPenality> penalityList = new List<HotelPenality>();
            if (itineary.Source == HotelBookingSource.LOH)
            {
                LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine(request.SessionId);


                jxe.AgentCurrency = loginInfo.Currency;
                jxe.ExchangeRates = loginInfo.AgentExchangeRates;
                jxe.DecimalPoint = loginInfo.DecimalValue;

                decimal supplierPrice = 0;

                foreach (HotelRoom room in itineary.Roomtype)
                {
                    supplierPrice += room.Price.SupplierPrice;
                }
                //cancelation policy is coming based on the nationality
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                itineary.PassengerNationality = Country.GetCountryCodeFromCountryName(request.passengerNationality);
                polList = jxe.GetCancellationDetails(itineary.HotelCode, itineary.Roomtype[0].RatePlanCode.Replace(" ", "+"), itineary.StartDate, itineary.EndDate, hotelResult.SequenceNumber, supplierPrice, itineary.PassengerNationality);
                if (polList.ContainsKey("RatePlanCode"))
                {
                    foreach (HotelRoom room in itineary.Roomtype)
                    {
                        room.RatePlanCode = polList["RatePlanCode"];
                    }
                }
            }
            else if (itineary.Source == HotelBookingSource.GTA)
            {
                string[] splitter = { "@#" };
                polList.Add("lastCancellationDate", itineary.StartDate.ToString());
                polList.Add("CancelPolicy", itineary.HotelCancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0]);
                polList.Add("AmendmentPolicy", itineary.HotelCancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1]);
                polList.Add("HotelPolicy", "");
            }
            //Loading Cancellation policy
            else if (itineary.Source == HotelBookingSource.HotelBeds)
            {
                if (request != null && request.roomDetails != null && request.roomDetails.Length > 0)
                {
                    if (!string.IsNullOrEmpty(request.roomDetails[0].CancellationPolicy))
                    {
                        string[] splitter = { "@#" };
                        polList.Add("lastCancellationDate", itineary.StartDate.ToString());
                        polList.Add("CancelPolicy", request.roomDetails[0].CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0]);
                        polList.Add("HotelPolicy", request.roomDetails[0].CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1]);
                        polList.Add("AmendmentPolicy", "");
                    }
                    else
                    {
                        polList = mse.GetCancellationDetails(itineary, ref penalityList, true, itineary.Source);
                    }
                }
                else
                {
                    polList = mse.GetCancellationDetails(itineary, ref penalityList, true, itineary.Source);
                }
            }
            else
            {

                polList = mse.GetCancellationDetails(itineary, ref penalityList, true, itineary.Source);
            }

            //if (itineary.IsDomestic)
            //{
            //    itineary.VoucherStatus = true;
            //    if (itineary.Source == HotelBookingSource.TBOConnect)
            //    {
            //        //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, member.AgencyId, "Source : " + itineary.Source.ToString(), "");
            //        itineary.PenalityInfo = penalityList;
            //    }
            //}
            //else
            //{
            //    itineary.VoucherStatus = false;
            //    itineary.PenalityInfo = penalityList;
            //}
            itineary.VoucherStatus = true;    //Added bty brahmam 
            itineary.PenalityInfo = penalityList;
            if (polList.ContainsKey("HotelPolicy"))
            {
                itineary.HotelPolicyDetails = polList["HotelPolicy"];
            }
            else
            {
                itineary.HotelPolicyDetails = "";
            }
            if (polList.ContainsKey("CancelPolicy"))
            {
                //if (polList["CancelPolicy"] == null || polList["CancelPolicy"] == "")
                //    itineary.HotelCancelPolicy = "";
                //else
                itineary.HotelCancelPolicy = polList["CancelPolicy"];
            }
            else
            {
                itineary.HotelCancelPolicy = "";
            }
            if (polList.ContainsKey("lastCancellationDate"))
            {
                DateTime dtLastCancellationDate = new DateTime();
                if (itineary.Source == HotelBookingSource.TBOConnect)
                {
                    TimeSpan diffResult = Convert.ToDateTime(polList["lastCancellationDate"]).Subtract(DateTime.Now);
                    dtLastCancellationDate = mse.GetLastCancellationDateforHotel(
                        diffResult.Days
                        , itineary.StartDate, itineary.Source);
                }
                else if (itineary.Source == HotelBookingSource.LOH || itineary.Source == HotelBookingSource.GTA || itineary.Source == HotelBookingSource.HotelBeds)
                {
                    dtLastCancellationDate = Convert.ToDateTime(polList["lastCancellationDate"]);
                }
                else
                {
                    dtLastCancellationDate = mse.GetLastCancellationDateforHotel(Convert.ToInt32(polList["lastCancellationDate"]), itineary.StartDate, itineary.Source);
                }
                //// done temporarily
                //if (dtLastCancellationDate == null || dtLastCancellationDate == DateTime.MinValue)
                //    itineary.LastCancellationDate = DateTime.Now;
                //else
                itineary.LastCancellationDate = dtLastCancellationDate;
            }
            else
            {
                //incase of tourico last cancellation date is null since the payment is done as booked.
                itineary.LastCancellationDate = DateTime.Now;
            }

            if (itineary.HotelPolicyDetails == null)
            {
                itineary.HotelPolicyDetails = string.Empty;
            }
            if (itineary.HotelCancelPolicy == null)
            {
                itineary.HotelCancelPolicy = string.Empty;
            }
            #endregion
        }
        catch(Exception ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " |  Booking Source: " + hotelResult.BookingSource + " | Stack Trace: " + ex.StackTrace, "");
            throw new ArgumentException("Hotel Agreement Details Not found");
        }
        try
        {
            #region Hotel Details
            //Commented by brahmam     in hotelResult class we need add two more parameters PassengerNationality and PassengerCountryOfResidence based on new HotelAppi
            //HotelDetails hotelDetail = mse.GetHotelDetails(itineary.CityCode, itineary.HotelName, itineary.HotelCode, itineary.Source); 
            HotelDetails hotelDetail = mse.GetHotelDetails(hotelResult.CityCode, hotelResult.HotelCode, hotelResult.StartDate, hotelResult.EndDate,itineary.PassengerNationality,itineary.PassengerCountryOfResidence, hotelResult.BookingSource);
            if (itineary.Source == HotelBookingSource.Desiya)
            {
                itineary.HotelPolicyDetails += hotelDetail.HotelPolicy + "| ";
            }
            if (itineary.Source == HotelBookingSource.DOTW)
            {
                itineary.HotelPolicyDetails = hotelDetail.HotelPolicy;
                foreach (HotelRoom hotelRoom in itineary.Roomtype)
                {
                    itineary.HotelPolicyDetails += !string.IsNullOrEmpty(hotelRoom.EssentialInformation)?"| " + hotelRoom.EssentialInformation :"";
                }
                
            }

            if (hotelDetail.Email != null && hotelDetail.Email.Length > 0 && hotelDetail.Email != "NA" && hotelDetail.Email != "NA-NA")
            {
                itineary.HotelAddress2 += "\n E-mail :" + hotelDetail.Email;
            }
            if (hotelDetail.PhoneNumber != null && hotelDetail.PhoneNumber.Length > 0 && hotelDetail.PhoneNumber != "NA" && hotelDetail.PhoneNumber != "NA-NA")
            {
                itineary.HotelAddress2 += "\n Phone No: " + hotelDetail.PhoneNumber;
            }
            if (hotelDetail.FaxNumber != null && hotelDetail.FaxNumber.Length > 0 && hotelDetail.FaxNumber != "NA" && hotelDetail.FaxNumber != "NA-NA")
            {
                itineary.HotelAddress2 += "\n Fax : " + hotelDetail.FaxNumber;
            }

            #endregion
        }
        catch (Exception ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "Error : " + ex.Message + " |    Booking Source: " + hotelResult.BookingSource + " |  Stack Trace: " + ex.StackTrace, "");
            throw new ArgumentException("Hotel Details Not found");
        }

        HotelPassenger leadpassInfo = new HotelPassenger();
        for (int i = 0; i < request.Guest.Length; i++)
        {
            if (request.Guest[i].LeadGuest)
            {
                leadpassInfo.PaxType = (HotelPaxType)Enum.Parse(typeof(HotelPaxType), request.Guest[i].GuestType.ToString());
                leadpassInfo.Title = request.Guest[i].Title;
                leadpassInfo.Firstname = request.Guest[i].FirstName;
                leadpassInfo.Lastname = request.Guest[i].LastName;
                leadpassInfo.Addressline1 = request.Guest[i].Addressline1;
                leadpassInfo.Addressline2 = request.Guest[i].Addressline2;
                leadpassInfo.Email = request.Guest[i].Email;
                leadpassInfo.Areacode = request.Guest[i].Areacode;
                leadpassInfo.Countrycode = request.Guest[i].Countrycode;
                leadpassInfo.Phoneno = request.Guest[i].Phoneno;
                leadpassInfo.City = request.Guest[i].City;
                leadpassInfo.Country = request.Guest[i].Country;
                leadpassInfo.LeadPassenger = true;
                leadpassInfo.Zipcode = request.Guest[i].Zipcode;
                leadpassInfo.State = request.Guest[i].State;
                leadpassInfo.Nationality = request.Guest[i].Nationality;
                leadpassInfo.NationalityCode=Country.GetCountryCodeFromCountryName(request.Guest[i].Nationality.Trim()); 
                break;
            }
        }

        //for (int i = 0; i < itineary.Roomtype.Length; i++)
        //{
        //    List<HotelPassenger> hotelPaxList = new List<HotelPassenger>();
        //    if (i == 0)
        //    {
        //        hotelPaxList.Add(leadpassInfo);
        //    }
        //    else if (itineary.Source != HotelBookingSource.Desiya)
        //    {
        //        HotelPassenger hPax = new HotelPassenger();
        //        hPax.PaxType = (HotelPaxType)Enum.Parse(typeof(HotelPaxType), request.Guest[i].GuestType.ToString());
        //        hPax.Title = request.Guest[i].Title;
        //        hPax.Firstname = request.Guest[i].FirstName;
        //        hPax.Lastname = request.Guest[i].LastName;
        //        hotelPaxList.Add(hPax);
        //    }
        //    itineary.Roomtype[i].PassenegerInfo = hotelPaxList;
        //}

        #region add passenger info
        int tmp = 0;
        int l = 0;
        for (int i = 0; i < itineary.Roomtype.Length; i++)
        {
            List<HotelPassenger> hotelPaxList = new List<HotelPassenger>();
            //for (int iR = 1; iR <= Convert.ToInt16(itineary.Roomtype[i].NoOfUnits); iR++)
            {
                if (i == 0 & l == 0)
                {
                    tmp = 1;
                    hotelPaxList.Add(leadpassInfo);
                    l++;
                }
                else
                {
                    tmp = 0;
                }
                for (; tmp < itineary.Roomtype[i].AdultCount; tmp++)
                {
                    HotelPassenger adultpassInfo = new HotelPassenger();
                    adultpassInfo.Title = request.guest[l].Title;
                    string fName = request.guest[l].FirstName;
                    adultpassInfo.Firstname = fName;
                    string lName = request.guest[l].LastName;
                    adultpassInfo.Lastname = lName;
                    if (tmp == 0)
                    {
                        //for each room first adult will be the lead pax
                        adultpassInfo.LeadPassenger = true;
                    }
                    else
                    {
                        adultpassInfo.LeadPassenger = false;
                    }
                    adultpassInfo.PaxType = HotelPaxType.Adult;
                    hotelPaxList.Add(adultpassInfo);
                    l++;
                }
                for (int k = 0; k < itineary.Roomtype[i].ChildCount; k++)
                {
                    HotelPassenger childpassInfo = new HotelPassenger();
                    childpassInfo.Title = request.guest[l].Title;
                    string fName = request.guest[l].FirstName;
                    childpassInfo.Firstname = fName;
                    string lName = request.guest[l].LastName;
                    childpassInfo.Lastname = lName;
                    childpassInfo.LeadPassenger = false;
                    childpassInfo.PaxType = HotelPaxType.Child;
                    childpassInfo.Age = itineary.Roomtype[i].ChildAge[k];
                    hotelPaxList.Add(childpassInfo);
                    l++;
                }
            }
            itineary.Roomtype[i].PassenegerInfo = hotelPaxList;
        }
        itineary.HotelPassenger = leadpassInfo;
        #endregion



        if (itineary.Source == HotelBookingSource.IAN)
        {
            itineary.PropertyType = hotelResult.PropertyType;
            itineary.SupplierType = hotelResult.SupplierType;
        }
        if (itineary.Source == HotelBookingSource.GTA)
        {
            itineary.SpecialRequest = itineary.Roomtype[0].EssentialInformation;
        }
        else
        {
            itineary.SpecialRequest = request.SpecialRequest;
        }
        itineary.FlightInfo = request.FlightInfo;
        return itineary;
    }
}
