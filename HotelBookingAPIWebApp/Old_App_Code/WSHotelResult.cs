using System;
//using Technology.BookingEngine;
using System.Collections.Generic;
using CT.BookingEngine;

public enum WSHotelBookingSource
{
    Desiya = 1,
    GTA = 2,
    HotelBeds = 3,
    Tourico = 4,
    TBOConnect = 5,  // change for tbo
    Miki = 7,
    DOTW = 9,
    WST = 10,
    HotelConnect = 11,// Cozmo Hotel inventory
    RezLive = 12,
    LOH = 13
}

public struct WSHotelInfo
{
    public string HotelCode;
    public string HotelName;
    public string HotelPicture;
    public string HotelDescription;
    public string HotelMap;
    public string HotelLocation;
    public string HotelAddress;
    public string HotelContactNo;
    public WSHotelRating Rating;
    public decimal TotalPrice;
}

public struct WSHotelRoomsDetails
{
    public string RoomTypeCode;
    public string RoomTypeName;
    public string RatePlanCode;
    public WSRate Rate;
    public WSRoomRate RoomRate;
    public List<string> Amenities;
    public string SequenceNo;//for which room it is associated 1|2|3 means this room type is for 1st ,2nd & 3rd room.
    public int NoOfUnits;
    //Aeede by brahmam
    public decimal SellExtraGuestCharges;
    public decimal Discount;
    public string DiscountType;
    public decimal SellingFare;
    public decimal Markup;
    public string MarkupType;
    public decimal TotalTax;
    public RoomRates[] Rates;
    public WSOccupancy Occupancy;
    public string mealPlanDesc;
    public decimal ChildCharges;
    public string PromoMessage;
    public int MaxExtraBeds;
    public string EssentialInformation;
    public bool SharingBedding;
    public int NumberOfCots;   
    public bool IsExtraBed;
    public string CancellationPolicy;
    //B2C purpose
    public decimal B2CMarkup;
    public string B2CMarkupType;
    public decimal B2CMarkupValue;
    public VATAppliedOn VatAppliedType;
    public decimal VatCost;
    public bool IsVatApplied;
}

public struct WSCreditCardInfo
{

    public string Email;
    public string Address1;
    public string Address2;
    public string FirstName;
    public string LastName;
    public string HomePhone;
    public string WorkPhone;
    //Ask piyus what to send
    public string CreditCardType;
    public string CreditCardNumber;
    public string CreditCardIdentifier;
    public string CreditCardExpirationMonth;
    public string CreditCardExpirationYear;
    public string[] EmailAddress;
    public string City;
    public string StateCode;
    public string CountryCode;
    public string PostalCode;
    public decimal Amount;
}

public struct WSOccupancy
{
    public int MaxAdult;
    public int MaxChild;
    public int MaxInfant;
    public int MaxGuest;
    public int BaseAdult;
    public int BaseChild;
    public int minStay;

}

/// <summary>
/// Summary description for WSHotelResult
/// </summary>
public class WSHotelResult
{
    private int index;
    private string cityCode;
    private DateTime checkInDate;
    private DateTime checkOutDate;
    private string passengerCountryOfResidence;
    public string PassengerCountryOfResidence
    {
        get { return passengerCountryOfResidence; }
        set { passengerCountryOfResidence = value; }
    }
    private string passengerNationality;
    public string PassengerNationality
    {
        get { return passengerNationality; }
        set { passengerNationality = value; }
    }
    private WSHotelInfo hotelInfo;
    private WSRoomGuestData[] roomGuestDetails;
    private WSHotelRoomsDetails[] roomDetails;
    private string currency;
    private decimal rateOfExchange;
    private int noOfRooms;
    //private bool isSameRoomTypeRequired;
    private bool sendAllRoomLeadGuestInfo;
    private WSHotelBookingSource bookingSource;  //Added by brahmam
    private string promoMessage;//Added by brahmam
    private PriceAccounts price;
    private DateTime lastInActivieTime; //time validation Booking requesttime
                                        //private bool sendCreditCardDetails;

    //public bool SendCreditCardDetails
    //{
    //    get
    //    {
    //        return sendCreditCardDetails;
    //    }
    //    set
    //    {
    //        sendCreditCardDetails = value;
    //    }
    //}
    public bool SendAllRoomLeadGuestInfo
    {
        get
        {
            return sendAllRoomLeadGuestInfo;
        }
        set
        {
            sendAllRoomLeadGuestInfo = value;
        }
    }
    public int Index
    {
        get { return index; }
        set { index = value; }
    }
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }
    public DateTime CheckInDate
    {
        get { return checkInDate; }
        set { checkInDate = value; }
    }
    public DateTime CheckOutDate
    {
        get { return checkOutDate; }
        set { checkOutDate = value; }
    }
    public WSHotelInfo HotelInfo
    {
        get { return hotelInfo; }
        set { hotelInfo = value; }
    }
    public WSRoomGuestData[] RoomGuestDetails
    {
        get { return roomGuestDetails; }
        set { roomGuestDetails = value; }
    }
    public WSHotelRoomsDetails[] RoomDetails
    {
        get { return roomDetails; }
        set { roomDetails = value; }
    }
    public string Currency
    {
        get { return currency; }
        set { currency = value; }
    }
    public decimal RateOfExchange
    {
        get { return rateOfExchange; }
        set { rateOfExchange = value; }
    }
    public int NoOfRooms
    {
        get { return noOfRooms; }
        set { noOfRooms = value; }
    }
    public WSHotelBookingSource BookingSource
    {
        get { return bookingSource; }
        set { bookingSource = value; }
    }
    public string PromoMessage
    {
        get { return promoMessage; }
        set {promoMessage  = value; }
    }
    public PriceAccounts Price
    {
        get { return price; }
        set { price = value; }
    }
    public DateTime LastInActivieTime
    {
        get { return lastInActivieTime; }
        set { lastInActivieTime = value; }
    }

    //public bool IsSameRoomTypeRequired
    //{
    //    get { return isSameRoomTypeRequired; }
    //    set { isSameRoomTypeRequired = value; }
    //}


    public WSHotelResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}


public class WSDayRates
{
    //public WeekDays Days;
    private DateTime days;
    public DateTime Days // for 0 it will be first day, for 1 it will be second day and so on
    {
        get { return days; }
        set { days = value; }
    }

    private decimal baseFare;
    public decimal BaseFare
    {
        get { return baseFare; }
        set { baseFare = value; }
    }
}

public class WSRate
{
    private decimal totalRate;
    public decimal TotalRate // sum of roomBaseFare + ExtraGuestCharges
    {
        get { return totalRate; }
        set { totalRate = value; }
    }

    private decimal totalTax;
    public decimal TotalTax
    {
        get { return totalTax; }
        set { totalTax = value; }
    }

    private string currency;
    public string Currency
    {
        get { return currency; }
        set { currency = value; }
    }
}

public class WSRoomRate
{

    /// <summary>
    /// 
    /// </summary>
    private WSDayRates[] dayRates;
    /// <summary>
    /// 
    /// </summary>
    public WSDayRates[] DayRates
    {
        get { return dayRates; }
        set { dayRates = value; }
    }

    private decimal extraGuestCharges;
    public decimal ExtraGuestCharges
    {
        get { return extraGuestCharges; }
        set { extraGuestCharges = value; }
    }

    private decimal disCount;
    public decimal DisCount
    {
        get { return disCount; }
        set { disCount = value; }
    }

    private decimal otherCharges;
    public decimal OtherCharges
    {
        get { return otherCharges; }
        set { otherCharges = value; }
    }

    private decimal serviceTax;
    public decimal ServiceTax
    {
        get { return serviceTax; }
        set { serviceTax = value; }
    }

    private decimal totalRoomRate;
    public decimal TotalRoomRate
    {
        get { return totalRoomRate; }
        set { totalRoomRate = value; }
    }

    private decimal totalRoomTax;
    public decimal TotalRoomTax
    {
        get { return totalRoomTax; }
        set { totalRoomTax = value; }
    }
}


