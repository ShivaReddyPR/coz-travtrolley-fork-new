using System.Collections.Generic;

/// <summary>
/// Summary description for WSHotelDetail
/// </summary>
public class WSHotelDetail
{
    private string address;
    public string Address
    {
        get
        {
            return address;
        }
        set
        {
            address = value;
        }
    }

    private string[][] attractions;
    public string[][] Attractions
    {
        get
        {
            return attractions;
        }
        set
        {
            attractions = value;
        }
    }

    private string countryName;
    public string CountryName
    {
        get
        {
            return countryName;
        }
        set
        {
            countryName = value;
        }
    }

    private string description;
    public string Description
    {
        get
        {
            return description;
        }
        set
        {
            description = value;
        }
    }

    private string email;
    public string Email
    {
        get
        {
            return email;
        }
        set
        {
            email = value;
        }
    }

    private string faxNumber;
    public string FaxNumber
    {
        get
        {
            return faxNumber;
        }
        set
        {
            faxNumber = value;
        }
    }

    private string hotelCode;
    public string HotelCode
    {
        get
        {
            return hotelCode;
        }
        set
        {
            hotelCode = value;
        }
    }

    private List<string> hotelFacilities;
    public List<string> HotelFacilities
    {
        get
        {
            return hotelFacilities;
        }
        set
        {
            hotelFacilities = value;
        }
    }

    private string hotelName;
    public string HotelName
    {
        get
        {
            return hotelName;
        }
        set
        {
            hotelName = value;
        }
    }

    private string hotelPolicy;
    public string HotelPolicy
    {
        get
        {
            return hotelPolicy;
        }
        set
        {
            hotelPolicy = value;
        }
    }

    private WSHotelRating hotelRating;
    public WSHotelRating HotelRating
    {
        get
        {
            return hotelRating;
        }
        set
        {
            hotelRating = value;
        }
    }

    private string image;
    public string Image
    {
        get
        {
            return image;
        }
        set
        {
            image = value;
        }
    }

    private List<string> imageUrls;
    public List<string> ImageUrls
    {
        get
        {
            return imageUrls;
        }
        set
        {
            imageUrls = value;
        }
    }

    private string map;
    public string Map
    {
        get
        {
            return map;
        }
        set
        {
            map = value;
        }
    }

    private string phoneNumber;
    public string PhoneNumber
    {
        get
        {
            return phoneNumber;
        }
        set
        {
            phoneNumber = value;
        }
    }

    private int pinCode;
    public int PinCode
    {
        get
        {
            return pinCode;
        }
        set
        {
            pinCode = value;
        }
    }

    private WSRoom[] roomData;
    public WSRoom[] RoomData
    {
        get
        {
            return roomData;
        }
        set
        {
            roomData = value;
        }
    }

    private List<string> roomFacilities;
    public List<string> RoomFacilities
    {
        get
        {
            return roomFacilities;
        }
        set
        {
            roomFacilities = value;
        }
    }

    private string services;
    public string Services
    {
        get
        {
            return services;
        }
        set
        {
            services = value;
        }
    }

    private string hotelWebsiteUrl;
    public string HotelWebsiteUrl
    {
        get
        {
            return hotelWebsiteUrl;
        }
        set
        {
            hotelWebsiteUrl = value;
        }
    }

    public WSHotelDetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
