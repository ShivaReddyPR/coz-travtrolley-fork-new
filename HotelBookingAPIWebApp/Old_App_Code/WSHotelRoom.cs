using System.Collections.Generic;

/// <summary>
/// Summary description for WSHotelRoom
/// </summary>
public class WSHotelRoom
{
    private int adultCount;
    public int AdultCount
    {
        get { return adultCount; }
        set { adultCount = value; }
    }

    private int childCount; // - Optional
    public int ChildCount
    {
        get { return childCount; }
        set { childCount = value; }
    }

    private List<int> childAge; // It will contain Child Age 
    public List<int> ChildAge
    {
        get { return childAge; }
        set { childAge = value; }
    }

    private string roomName;
    public string RoomName
    {
        get { return roomName; }
        set { roomName = value; }
    }

    private List<string> ameneties;
    public List<string> Ameneties
    {
        get { return ameneties; }
        set { ameneties = value; }
    }

    private bool extraBed;
    public bool ExtraBed
    {
        get { return extraBed; }
        set { extraBed = value; }
    }

    private int noOfExtraBed;
    public int NoOfExtraBed
    {
        get { return noOfExtraBed; }
        set { noOfExtraBed = value; }
    }

    private int noOfCots;
    public int NoOfCots
    {
        get { return noOfCots; }
        set { noOfCots = value; }
    }

    private bool sharingBed;
    public bool SharingBed
    {
        get { return sharingBed; }
        set { sharingBed = value; }
    }

    private List<WSGuest> guestInfo;
    public List<WSGuest> GuestInfo
    {
        get { return guestInfo; }
        set { guestInfo = value; }
    }

    private WSHotelRoomRate roomRate;
    public WSHotelRoomRate RoomRate
    {
        get { return roomRate; }
        set { roomRate = value; }
    }
    private string mealPlanDesc = string.Empty;
    public string MealPlanDesc
    {
        get { return mealPlanDesc; }
        set { mealPlanDesc = value; }
    }

    public WSHotelRoom()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public class WSHotelRoomRate
{
    private decimal totalRoomPrice;
    public decimal TotalRoomPrice
    {
        get { return totalRoomPrice; }
        set { totalRoomPrice = value; }
    }

    private decimal totalRoomTax;
    public decimal TotalRoomTax
    {
        get { return totalRoomTax; }
        set { totalRoomTax = value; }
    }

    private decimal discount;
    public decimal Discount
    {
        get { return discount; }
        set { discount = value; }
    }

    private decimal otherCharges;
    public decimal OtherCharges
    {
        get { return otherCharges; }
        set { otherCharges = value; }
    }

    private decimal serviceTax;
    public decimal ServiceTax
    {
        get { return serviceTax; }
        set { serviceTax = value; }
    }
    private decimal vatAmount;
    public decimal VatAmount
    {
        get { return vatAmount; }
        set { vatAmount = value; }
    }
}
