using System;
using System.Collections.Generic;
using CT.BookingEngine;

[Serializable]
public class WSTransferSearchResult
{
    [Serializable]
    public struct WSTransferPickUpPointDetails
    {
        public string PickUpPointName;
        public string PickUpPointCode;
        public string PickUpPointCityCode;
    }
    [Serializable]
    public struct WLTransferPickUpDetails
    {
        public string PickUpCode;
        public string PickUpName;
        public string PickUpDetailCode;
        public string PickUpDetailName;
        public List<WSTransferPickUpPointDetails> PickUpPointDetails;
    }
    [Serializable]
    public struct WLTransferDropOffDetails
    {
        public string DropOffCode;
        public string DropOffName;
        public float DropOffAllowForCheckInTime;
        public string DropOffDetailCode;
        public string DropOffDetailName;
    }
    [Serializable]
    public struct WLTransferOutOfHoursSupplement
    {
        public float FromTime;
        public float ToTime;
        public string Supplement;
        public string Details;
    }
    [Serializable]
    public struct WLTransferVehicleDetails
    {
        public string Vehicle;
        public string VehicleCode;
        public int VehicleMaximumPassengers;
        public int VehicleMaximumLuggage;
        public string LanguageCode;
        public string Language;
        public decimal ItemPrice;
        public PriceAccounts PriceInfo;
        public string ItemPriceCurrency;
        public string Confirmation;
        public string ConfirmationCode;
    }
    [Serializable]
    public struct WLTransferLocationDetails
    {
        public string LocationName;
        public string LocationCode;
    }

    public bool HasExtraInfo;
    public bool HasIdeas;
    public string City;
    public string CityCode;
    public string ItemName;
    public string ItemCode;
    public WLTransferPickUpDetails PickUp;
    public WLTransferDropOffDetails DropOff;
    public WLTransferOutOfHoursSupplement[] OutOfHoursSupplements;
    public decimal ApproximateTransferTime;
    public string ApproximateTransferTime_Str;
    public WLTransferVehicleDetails[] Vehicles;
    public WSTransferBookingSource Source;
    public PriceAccounts price;
    public string Currency;
    public string Conditions;
}

