using System;
using System.Configuration;
//using Technology.Indiatimes;
//using Technology.BookingEngine;
//using CoreLogic;
//using Technology.Configuration;
using System.Collections;
using System.Xml;
using CT.BookingEngine;
using CT.Configuration;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.WhiteLabel;
/// <summary>
/// Summary description for WSAddHotelBookingRequest
/// </summary>
public class WSAddHotelBookingRequest
{
    #region privateFields
    string sessionId;
    int hotelIndex; 
    int referenceId;
    string confirmationNo;
    WLHotelRating starRating;
    PaymentGatewaySource paySource;
    string paymentId;
    decimal paymentAmount;
    int bookingId;
    int agencyId;
    string passengerInfo;
    string cityRef;
    string hotelName;
    string hotelCode;
    string address1;
    string address2;
    DateTime checkInDate;
    DateTime checkOutDate;
    int noOfRooms;
    string roomName;
    WLHotelBookingStatus bookingStatus;
    WLHotelBookingSource source;
    bool isDomestic;
    string cityCode;
    string bookingRefNo;
    bool voucherStatus;
    string remarks;
    string ipAddress;
    string email;
    string phone;
    int adultCount;
    int childCount;
    string country;
    DateTime createdOn;
    int createdBy;
    DateTime lastModifiedOn;
    int lastModifiedBy;
    #endregion

    #region publicProperties
    public string SessionId
    {
        get
        {
            return sessionId;
        }
        set
        {
            sessionId = value;
        }
    }

    public int HotelIndex
    {
        get
        {
            return hotelIndex;
        }
        set
        {
            hotelIndex = value;
        }
    }

    public int ReferenceId
    {
        get
        {
            return referenceId;
        }
        set
        {
            referenceId = value;
        }
    }

    public string ConfirmationNo
    {
        get
        {
            return confirmationNo;
        }
        set
        {
            confirmationNo = value;
        }
    }

    public WLHotelRating StarRating
    {
        get
        {
            return starRating;
        }
        set
        {
            starRating = value;
        }
    }

    public PaymentGatewaySource PaySource
    {
        get
        {
            return paySource;
        }
        set
        {
            paySource = value;
        }
    }

    public string PaymentId
    {
        get
        {
            return paymentId;
        }
        set
        {
            paymentId = value;
        }
    }

    public decimal PaymentAmount
    {
        get
        {
            return paymentAmount;
        }
        set
        {
            paymentAmount = value;
        }
    }

    public int BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }

    public int AgencyId
    {
        get
        {
            return agencyId;
        }
        set
        {
            agencyId = value;
        }
    }

    public string PassengerInfo
    {
        get
        {
            return passengerInfo;
        }
        set
        {
            passengerInfo = value;
        }
    }

    public string CityRef
    {
        get
        {
            return cityRef;
        }
        set
        {
            cityRef = value;
        }
    }

    public string HotelName
    {
        get
        {
            return hotelName;
        }
        set
        {
            hotelName = value;
        }
    }

    public string HotelCode
    {
        get
        {
            return hotelCode;
        }
        set
        {
            hotelCode = value;
        }
    }

    public string Address1
    {
        get
        {
            return address1;
        }
        set
        {
            address1 = value;
        }
    }

    public string Address2
    {
        get
        {
            return address2;
        }
        set
        {
            address2 = value;
        }
    }

    public DateTime CheckInDate
    {
        get
        {
            return checkInDate;
        }
        set
        {
            checkInDate = value;
        }
    }

    public DateTime CheckOutDate
    {
        get
        {
            return checkOutDate;
        }
        set
        {
            checkOutDate = value;
        }
    }

    public int NoOfRooms
    {
        get
        {
            return noOfRooms;
        }
        set
        {
            noOfRooms = value;
        }
    }

    public string RoomName
    {
        get
        {
            return roomName;
        }
        set
        {
            roomName = value;
        }
    }

    public WLHotelBookingStatus BookingStatus
    {
        get
        {
            return bookingStatus;
        }
        set
        {
            bookingStatus = value;
        }
    }

    public WLHotelBookingSource Source
    {
        get
        {
            return source;
        }
        set
        {
            source = value;
        }
    }

    public bool IsDomestic
    {
        get
        {
            return isDomestic;
        }
        set
        {
            isDomestic = value;
        }
    }

    public string CityCode
    {
        get
        {
            return cityCode;
        }
        set
        {
            cityCode = value;
        }
    }

    public string BookingRefNo
    {
        get
        {
            return bookingRefNo;
        }
        set
        {
            bookingRefNo = value;
        }
    }

    public bool VoucherStatus
    {
        get
        {
            return voucherStatus;
        }
        set
        {
            voucherStatus = value;
        }
    }

    public string Remarks
    {
        get
        {
            return remarks;
        }
        set
        {
            remarks = value;
        }
    }

    public string IPAddress
    {
        get
        {
            return ipAddress;
        }
        set
        {
            ipAddress = value;
        }
    }

    public string Email
    {
        get
        {
            return email;
        }
        set
        {
            email = value;
        }
    }

    public string Phone
    {
        get
        {
            return phone;
        }
        set
        {
            phone = value;
        }
    }

    public int AdultCount
    {
        get
        {
            return adultCount;
        }
        set
        {
            adultCount = value;
        }
    }

    public int ChildCount
    {
        get
        {
            return childCount;
        }
        set
        {
            childCount = value;
        }
    }

    public string Country
    {
        get
        {
            return country;
        }
        set
        {
            country = value;
        }
    }

    public DateTime CreatedOn
    {
        get
        {
            return createdOn;
        }
        set
        {
            createdOn = value;
        }
    }

    public int CreatedBy
    {
        get
        {
            return createdBy;
        }
        set
        {
            createdBy = value;
        }
    }

    public DateTime LastModifiedOn
    {
        get
        {
            return lastModifiedOn;
        }
        set
        {
            lastModifiedOn = value;
        }
    }

    public int LastModifiedBy
    {
        get
        {
            return lastModifiedBy;
        }
        set
        {
            lastModifiedBy = value;
        }
    }

    #endregion

    public WSAddHotelBookingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    # region Methods
    public WSAddHotelBookingResponse Add(string authenticationValue, ApiCustomerType apiCustomerType, int memberId)
    {
        WSAddHotelBookingResponse saveResponse = new WSAddHotelBookingResponse();
        try
        {
            UserMaster member = new UserMaster(memberId);
            //commented by brahmam
            //member.Load(memberId);
            int agencyId;
            if (member.AgentId > 0)
            {
                agencyId = member.AgentId;
            }
            else
            {
                ConfigurationSystem config = new ConfigurationSystem();
                agencyId = Convert.ToInt32(config.GetSelfAgencyId()["selfAgencyId"]);
            }
            AgentMaster agency = new AgentMaster(agencyId);

            WLHotelBookingDetails bDetail = new WLHotelBookingDetails();
            bDetail.AgencyId = agencyId;
            bDetail.Address1 = address1;
            bDetail.Address2 = address2;
            bDetail.AdultCount = adultCount;
            bDetail.BookingId = bookingId;
            bDetail.BookingRefNo = bookingRefNo;
            bDetail.BookingStatus = bookingStatus;
            bDetail.CheckInDate = checkInDate;
            bDetail.CheckOutDate = checkOutDate;
            bDetail.ChildCount = childCount;
            bDetail.CityCode = cityCode;
            bDetail.CityRef = cityRef;
            bDetail.ConfirmationNo = confirmationNo;
            bDetail.Country = country;
            bDetail.Email = email;
            bDetail.CreatedBy = memberId;
            bDetail.HotelCode = hotelCode;
            bDetail.HotelName = hotelName;
            bDetail.IPAddress = ipAddress;
            bDetail.IsDomestic = isDomestic;
            bDetail.LastModifiedBy = memberId;
            bDetail.NoOfRooms = noOfRooms;
            bDetail.PassengerInfo = passengerInfo;
            bDetail.PaymentAmount = paymentAmount;
            bDetail.PaySource = (CT.BookingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.BookingEngine.PaymentGatewaySource), Convert.ToString(paySource));
            bDetail.PaymentId = paymentId;
            bDetail.Phone = phone;
            bDetail.Remarks = remarks;
            bDetail.RoomName = roomName;
            bDetail.Source = source;
            bDetail.StarRating = starRating;
            bDetail.VoucherStatus = voucherStatus;
            int referenceId = bDetail.Save();

            //WSAddHotelBookingResponse saveResponse = new WSAddHotelBookingResponse();
            saveResponse.Status = new WSStatus();
            saveResponse.Status.Category = "SHD";
            if (referenceId > 0)
            {
                saveResponse.Status.Description = "Details Saved";
                saveResponse.Status.StatusCode = "1";

                ApiCustomer apiCustomer = new ApiCustomer();
                apiCustomer.Load(authenticationValue, apiCustomerType);
                saveResponse.ReferenceId = apiCustomer.ApiCustomerCode + referenceId.ToString();

                # region sending Email
                string memberEmailId = string.Empty;

                UserPreference userPref = new UserPreference();
                userPref = userPref.GetPreference(memberId, "EMAILIDFORHOTEL", "HOTEL SETTINGS");
                if (userPref != null && userPref.Value != null && userPref.Value.Trim() != string.Empty)
                {
                    memberEmailId = userPref.Value;
                }
                else
                {
                    memberEmailId = member.Email;
                }
                if (bookingStatus == WLHotelBookingStatus.Failed)
                {
                    Hashtable table = new Hashtable(3);
                    table.Add("paymentId", "<b>" + paymentId + "</b>");
                    table.Add("tripId", saveResponse.ReferenceId);
                    table.Add("city", cityRef);
                    table.Add("paymentAmount", paymentAmount);
                    table.Add("memberName", member.FirstName + " " + member.LastName);
                    table.Add("agencyName", agency.Name);
                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    toArray.Add(memberEmailId);
                    //toArray.Add(ConfigurationManager.AppSettings["fromEmail"]);
                    try
                    {
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Hotel Booking Failed", ConfigurationManager.AppSettings["HotelBookingFailedMessage"] + "\n", table);
                    }
                    catch (System.Net.Mail.SmtpException ex)
                    {
                        Audit.Add(EventType.Email, Severity.High, 0, "B2C-Hotel-Smtp is unable to send the reference/trip Id message" + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Email, Severity.High, 0, "B2C-Hotel-Smtp is unable to send the message " + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                    }
                }

                # endregion

                # region email tripId
                //code for email ReferenceId starts here

                string leadPassengerName = string.Empty;
                XmlDocument xmlDoc = new XmlDocument();
                passengerInfo = passengerInfo.Replace("xmlns=\"http://Technology/HotelBookingApi\"", string.Empty);
                passengerInfo = passengerInfo.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", string.Empty);
                passengerInfo = passengerInfo.Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", string.Empty);
                xmlDoc.LoadXml(passengerInfo);
                XmlNodeList passenger = xmlDoc.SelectNodes("/Guest/WSGuest");
                string paxEmail = string.Empty;
                string title = string.Empty;
                string fName = string.Empty;
                string mName = string.Empty;
                string lName = string.Empty;
                foreach (XmlNode node in passenger)
                {
                    if (node.HasChildNodes)
                    {
                        if (node.SelectSingleNode("Title") != null)
                        {
                            title = node.SelectSingleNode("Title").InnerText.Trim();
                        }
                        if (node.SelectSingleNode("FirstName") != null)
                        {
                            fName = node.SelectSingleNode("FirstName").InnerText.Trim();
                        }
                        if (node.SelectSingleNode("MiddleName") != null)
                        {
                            mName = node.SelectSingleNode("MiddleName").InnerText.Trim();
                        }
                        if (node.SelectSingleNode("LastName") != null)
                        {
                            lName = node.SelectSingleNode("LastName").InnerText.Trim();
                        }
                        if (node.SelectSingleNode("Email") != null)
                        {
                            paxEmail = node.SelectSingleNode("Email").InnerText.Trim();
                        }
                        if (fName != string.Empty && lName != string.Empty && paxEmail != string.Empty)
                        {
                            break;
                        }
                    }
                }
                leadPassengerName = title + " " + fName + " " + mName + " " + lName;
                Hashtable hTable = new Hashtable(2);
                hTable.Add("tripId", "<b>" + apiCustomer.ApiCustomerCode + referenceId + "</b>");
                hTable.Add("passengerName", leadPassengerName);
                hTable.Add("city", cityRef);
                hTable.Add("memberName", member.FirstName + " " + member.LastName);
                hTable.Add("agencyName", agency.Name);
                System.Collections.Generic.List<string> toArrayBookingMsg = new System.Collections.Generic.List<string>();
                toArrayBookingMsg.Add(paxEmail);
                try
                {
                    // TODO ziya  
                    // Email.Send(ConfigurationManager.AppSettings["fromEmail"], memberEmailId, toArrayBookingMsg, "Booking Acknowledgment", ConfigurationManager.AppSettings["HotelBookingMessage"] + "\n", hTable);
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    Audit.Add(EventType.Email, Severity.High, 0, "B2C-Hotel-Smtp is unable to send the hotel reference/trip Id message" + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Email, Severity.High, 0, "B2C-Hotel-Smtp is unable to send the message " + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                }
                //code for email ReferenceId ends here
                # endregion
            }
            else
            {
                saveResponse.Status.Description = "Detail Saving Failed";
                saveResponse.Status.StatusCode = "3";
            }
        }
        catch (Exception ex1)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "WSAddHotelBookingResponse Error message " + ex1.ToString(), "");
        }
        return saveResponse;
    }
    # endregion

}
