using System.Collections.Generic;

/// <summary>
/// Summary description for WSGetDestinationCityListResponse
/// </summary>
public class WSGetDestinationCityListResponse
{
    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    private List<WSCity> cityList;
    public List<WSCity> CityList
    {
        get
        {
            return cityList;
        }
        set
        {
            cityList = value;
        }
    }


    public WSGetDestinationCityListResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
