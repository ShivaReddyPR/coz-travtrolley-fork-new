﻿////using System.Linq;
//using System.Xml.Linq;

/// <summary>
/// Summary description for WLTransferCancellationData
/// </summary>
public class WSTransferCancellationData 
{
    string cancelKey;
    string cancelValue;

    public string CancelKey
    {
        get { return cancelKey;}
        set { cancelKey =value;}
    }

    public string CancelValue
    {
        get { return cancelValue; }
        set { cancelValue = value; }
    }

    public WSTransferCancellationData()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
