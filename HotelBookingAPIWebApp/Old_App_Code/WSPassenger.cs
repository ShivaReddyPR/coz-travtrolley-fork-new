/// <summary>
/// Summary description for WSPassenger
/// </summary>
public class WSPassenger
{
    private string title;
    public string Title
    {
        get
        {
            return title;
        }
        set
        {
            title = value;
        }
    }

    private string firstName;
    public string FirstName
    {
        get
        {
            return firstName;
        }
        set
        {
            firstName = value;
        }
    }

    private string middleName;
    public string MiddleName
    {
        get
        {
            return middleName;
        }
        set
        {
            middleName = value;
        }
    }

    private string lastName;
    public string LastName
    {
        get
        {
            return lastName;
        }
        set
        {
            lastName = value;
        }
    }

    private bool leadPassenger;
    public bool LeadPassenger
    {
        get
        {
            return leadPassenger;
        }
        set
        {
            leadPassenger = value;
        }
    }

    private int age;
    public int Age
    {
        get
        {
            return age;
        }
        set
        {
            age = value;
        }
    }

    private string addressline1;
    public string Addressline1
    {
        get
        {
            return addressline1;
        }
        set
        {
            addressline1 = value;
        }
    }

    private string addressline2;
    public string Addressline2
    {
        get
        {
            return addressline2;
        }
        set
        {
            addressline2 = value;
        }
    }

    private string countrycode;
    public string Countrycode
    {
        get
        {
            return countrycode;
        }
        set
        {
            countrycode = value;
        }
    }

    private string areacode;
    public string Areacode
    {
        get
        {
            return areacode;
        }
        set
        {
            areacode = value;
        }
    }

    private string phoneno;
    public string Phoneno
    {
        get
        {
            return phoneno;
        }
        set
        {
            phoneno = value;
        }
    }

    private string email;
    public string Email
    {
        get
        {
            return email;
        }
        set
        {
            email = value;
        }
    }

    private string city;
    public string City
    {
        get
        {
            return city;
        }
        set
        {
            city = value;
        }
    }

    private string state;
    public string State
    {
        get
        {
            return state;
        }
        set
        {
            state = value;
        }
    }

    private string country;
    public string Country
    {
        get
        {
            return country;
        }
        set
        {
            country = value;
        }
    }

    private string zipcode;
    public string Zipcode
    {
        get
        {
            return zipcode;
        }
        set
        {
            zipcode = value;
        }
    }

    private WSHotelPaxType paxType;
    public WSHotelPaxType PaxType
    {
        get
        {
            return paxType;
        }
        set
        {
            paxType = value;
        }
    }

    private int roomIndex;
    public int RoomIndex
    {
        get
        {
            return roomIndex;
        }
        set
        {
            roomIndex = value;
        }
    }

    public WSPassenger()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public enum WSHotelPaxType
{
    Adult = 1,
    Child = 2
}
