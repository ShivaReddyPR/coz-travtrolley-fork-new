/// <summary>
/// Summary description for WSAgency
/// </summary>
public class WSAgency
{
    private string name;
    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    private string addressLine1;
    public string AddressLine1
    {
        get { return addressLine1; }
        set { addressLine1 = value; }
    }

    private string addressLine2;
    public string AddressLine2
    {
        get { return addressLine2; }
        set { addressLine2 = value; }
    }

    private string email;
    public string Email
    {
        get { return email; }
        set { email = value; }
    }

    private string phone;
    public string Phone
    {
        get { return phone; }
        set { phone = value; }
    }

    private string fax;
    public string Fax
    {
        get { return fax; }
        set { fax = value; }
    }

    private string city;
    public string City
    {
        get { return city; }
        set { city = value; }
    }

    private string pin;
    public string PIN
    {
        get { return pin; }
        set { pin = value; }
    }

    public WSAgency()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
