﻿using System;
//using System.Linq;
//using System.Xml.Linq;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for WSSightSeeingBookingRequest
/// </summary>
[Serializable]
public class WSSightSeeingBookingRequest
{
    private int sightseeingId;
    private string cityCode;     //City Code
    private string cityName;
    private string itemCode;     //Item Code
    private string itemName;
    private string currency;             //Price Information
    private PriceAccounts priceInfo;
    private WSSightseeingBookingStatus status;        //Item Status
    private DateTime tourDate;   //Tour Date
    private string tourLanguage;
    private List<string> paxNames; //Pax Id list according to the PaxType List
    private List<int> childAge;
    private int adultCount;
    private int childCount;
    private DateTime createdOn;
    private int createdBy;
    private DateTime lastModifiedOn;
    private int lastModifiedBy;
    private DateTime lastCancellationDate;
    private string cancelPolicy;
    private WSSightseeingBookingSource source;
    private string duration;
    private string depTime;
    private string depPointInfo;
    private string specialCode;
    private string supplierInfo;
    private string email;
    private string phNo;
    private string image;
    private WSPaymentInformation paymentInfo;
    private string sessionId;
    private string address;
    private int agentId;
    private int locationId;
    private string note;
    private string specialName;
    private string telePhno;
    private string countryName;
    #region properities
    public int SightseeingId
    {
        get { return sightseeingId; }
        set { sightseeingId = value; }
    }
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }
    public string CityName
    {
        get { return cityName; }
        set { cityName = value; }
    }
    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
    public string Currency
    {
        get { return currency; }
        set { currency = value; }
    }
    public List<string> PaxNames
    {
        get { return paxNames; }
        set { paxNames = value; }
    }
    public int AdultCount
    {
        get { return adultCount; }
        set { adultCount = value; }
    }
    public int ChildCount
    {
        get { return childCount; }
        set { childCount = value; }
    }
    public PriceAccounts Price
    {
        get { return priceInfo; }
        set { priceInfo = value; }
    }
    public WSSightseeingBookingStatus BookingStatus
    {
        get { return status; }
        set { status = value; }
    }
    public DateTime TourDate
    {
        get { return tourDate; }
        set { tourDate = value; }
    }
    public string Language
    {
        get { return tourLanguage; }
        set { tourLanguage = value; }
    }
    /// <summary>
    /// Gets or sets createdBy
    /// </summary>
    public int CreatedBy
    {
        get
        {
            return createdBy;
        }
        set
        {
            createdBy = value;
        }
    }

    /// <summary>
    /// Gets or sets createdOn Date
    /// </summary>
    public DateTime CreatedOn
    {
        get
        {
            return createdOn;
        }
        set
        {
            createdOn = value;
        }
    }

    /// <summary>
    /// Gets or sets lastModifiedBy
    /// </summary>
    public int LastModifiedBy
    {
        get
        {
            return lastModifiedBy;
        }
        set
        {
            lastModifiedBy = value;
        }
    }

    /// <summary>
    /// Gets or sets lastModifiedOn Date
    /// </summary>
    public DateTime LastModifiedOn
    {
        get
        {
            return lastModifiedOn;
        }
        set
        {
            lastModifiedOn = value;
        }
    }

    
    public string CancellationPolicy
    {
        get { return cancelPolicy; }
        set { cancelPolicy = value; }
    }
    public DateTime LastCancellationDate
    {
        get { return lastCancellationDate; }
        set { lastCancellationDate = value; }
    }
    public string ItemName
    {
        get { return itemName; }
        set { itemName = value; }
    }
    public List<int> ChildAge
    {
        get { return childAge; }
        set { childAge = value; }
    }
    public WSSightseeingBookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    public string Duration
    {
        get { return duration; }
        set { duration = value; }
    }
    public string DepTime
    {
        get { return depTime; }
        set { depTime = value; }
    }
    public string DepPointInfo
    {
        get { return depPointInfo; }
        set { depPointInfo = value; }
    }
    public string SpecialCode
    {
        get { return specialCode; }
        set { specialCode = value; }
    }
    public string SupplierInfo
    {
        get { return supplierInfo; }
        set { supplierInfo = value; }
    }
    public string Email
    {
        get { return email; }
        set { email = value; }
    }
    public string PhNo
    {
        get { return phNo; }
        set { phNo = value; }
    }
    public string Image
    {
        get { return image; }
        set { image = value; }
    }
    public WSPaymentInformation PaymentInfo
    {
        get
        {
            return paymentInfo;
        }
        set
        {
            paymentInfo = value;
        }
    }
    public string SessionId
    {
        get { return sessionId; }
        set { sessionId = value; }
    }
    public string Address
    {
        get { return address; }
        set { address = value; }
    }
    public int AgentId
    {
        get { return agentId; }
        set { agentId = value; }
    }
    public int LocationId
    {
        get { return locationId; }
        set { locationId = value; }
    }
    public string Note
    {
        get { return note; }
        set { note = value; }
    }
    public string SpecialName
    {
        get { return specialName; }
        set { specialName = value; }
    }
    public string TelePhno
    {
        get { return telePhno; }
        set { telePhno = value; }
    }
    public string CountryName
    {
        get { return countryName; }
        set { countryName = value; }
    }
    
    
    #endregion
    public WSSightSeeingBookingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static SightseeingItinerary BuildSightSeeingItinerary(WSSightSeeingBookingRequest request,LoginInfo AgentLoginInfo)
    {
        SightseeingItinerary itinerary = new SightseeingItinerary();
        itinerary.AdultCount = request.adultCount;
        itinerary.CancellationPolicy = request.cancelPolicy;
        itinerary.ChildAge = request.childAge;
        itinerary.ChildCount = request.childCount;
        itinerary.CityCode = request.cityCode;
        itinerary.CityName = request.cityName;
        itinerary.Currency = AgentLoginInfo.Currency;
        itinerary.DepPointInfo = request.depPointInfo;
        itinerary.DepTime = request.depTime;
        itinerary.Duration = request.duration;
        itinerary.Email = request.email;
        itinerary.ItemCode = request.itemCode;
        itinerary.VoucherStatus = true;
        itinerary.IsDomestic = false;
        itinerary.ItemName = request.itemName;
        itinerary.Language = request.tourLanguage;
        itinerary.LastCancellationDate = request.lastCancellationDate;
        itinerary.PaxNames = request.paxNames;
        itinerary.PhNo = request.phNo;
        itinerary.Price = request.Price;
        itinerary.Price.AccPriceType = PriceType.NetFare;
        if (request.source == WSSightseeingBookingSource.GTA)
        {
            itinerary.Source = SightseeingBookingSource.GTA;
        }
        itinerary.SpecialCode = request.specialCode;
        itinerary.SupplierInfo = request.supplierInfo;
        itinerary.TourDate = request.tourDate;
        itinerary.CreatedBy = (int)AgentLoginInfo.UserID;
        itinerary.TransType = "B2C";
        itinerary.LastCancellationDate = request.tourDate; //Here Later we need to implement Buffer also 
        itinerary.CancelId = "";
        itinerary.Address = request.address;
        itinerary.AgentId = AgentLoginInfo.AgentId;
        itinerary.LocationId =(int)AgentLoginInfo.LocationID;
        itinerary.Note = request.note;
        itinerary.TelePhno = request.telePhno;
        itinerary.SpecialName = request.specialName;
        itinerary.CountryName = request.countryName;
        return itinerary;
    }
}


public enum WSSightseeingBookingSource
{
    GTA = 1
}
public enum WSSightseeingBookingStatus
{
    Confirmed = 1,
    Cancelled = 2,
    Failed = 0,
    Pending = 3,
    Error = 4
}