using System;
using System.Collections.Generic;
//using Technology.BookingEngine;
//using CoreLogic;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for WSGetHotelBookingRequest
/// </summary>
public class WSGetHotelBookingRequest
{
    private string bookingId;
    public string BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }

    public WSGetHotelBookingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static WSHotelBookingDetail BuildGetBookingResponse(HotelItinerary itineary, BookingDetail booking)
    {
        WSHotelBookingDetail hotelBookingDetail = new WSHotelBookingDetail();
        hotelBookingDetail.BookingId = booking.BookingId.ToString();
        hotelBookingDetail.BookingRefNo = itineary.BookingRefNo;
        hotelBookingDetail.BookingStatus = (WSHotelBookingStatus)Enum.Parse(typeof(WSHotelBookingStatus),itineary.Status.ToString());
        hotelBookingDetail.CheckInDate = itineary.StartDate;
        hotelBookingDetail.CheckOutDate = itineary.EndDate;
        hotelBookingDetail.CityCode = itineary.CityCode;
        hotelBookingDetail.CityRef = itineary.CityRef;
        hotelBookingDetail.ConfirmationNo = itineary.ConfirmationNo;
        hotelBookingDetail.FlightInfo = itineary.FlightInfo;
        hotelBookingDetail.HotelAddress1 = itineary.HotelAddress1;
        hotelBookingDetail.HotelAddress2 = itineary.HotelAddress2;
        hotelBookingDetail.HotelCancelPolicy = itineary.HotelCancelPolicy;
        hotelBookingDetail.HotelName = itineary.HotelName;
        hotelBookingDetail.HotelPolicyDetails = itineary.HotelPolicyDetails;
        hotelBookingDetail.IsDomestic = itineary.IsDomestic;
        hotelBookingDetail.BookingDate = itineary.CreatedOn;
        hotelBookingDetail.Currency = itineary.Roomtype[0].Price.Currency;
        StaticData staticInfo = new StaticData();
        staticInfo.BaseCurrency = "AED";  //TODO Ziyad     
        Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
        rateOfExList = staticInfo.CurrencyROE;
        if (rateOfExList.ContainsKey(itineary.Roomtype[0].Price.Currency))
        {
            hotelBookingDetail.RateofExchange = rateOfExList[itineary.Roomtype[0].Price.Currency];
        }
        else
        {
            hotelBookingDetail.RateofExchange = 1;
        }
        hotelBookingDetail.LastCancellationDate = itineary.LastCancellationDate;
        hotelBookingDetail.Map = itineary.Map;
        hotelBookingDetail.NoOfRooms = itineary.NoOfRooms;
        hotelBookingDetail.PaneltyInfo = new List<HotelPenality>();
        hotelBookingDetail.PaneltyInfo = itineary.PenalityInfo;
        hotelBookingDetail.Source = itineary.Source;
        hotelBookingDetail.PaymentGuaranteedBy = itineary.PaymentGuaranteedBy;
        foreach (CT.BookingEngine.HotelPenality hotelPanelty in hotelBookingDetail.PaneltyInfo)
        {
            if (Convert.ToInt32(hotelPanelty.PolicyType) == 0)
            {
                hotelPanelty.PolicyType = ChangePoicyType.Amendment;
            }
            if (Convert.ToInt32(hotelPanelty.BookingSource) == 0)
            {
                hotelPanelty.BookingSource = HotelBookingSource.Desiya;
            }
        }
        if (itineary.HotelPassenger != null)
        {
            hotelBookingDetail.Guest = new WSGuest();
            hotelBookingDetail.Guest.Addressline1 = itineary.HotelPassenger.Addressline1;
            hotelBookingDetail.Guest.Addressline2 = itineary.HotelPassenger.Addressline2;
            hotelBookingDetail.Guest.Age = itineary.HotelPassenger.Age;
            hotelBookingDetail.Guest.Areacode = itineary.HotelPassenger.Areacode;
            hotelBookingDetail.Guest.City = itineary.HotelPassenger.City;
            hotelBookingDetail.Guest.Country = itineary.HotelPassenger.Country;
            hotelBookingDetail.Guest.Countrycode = itineary.HotelPassenger.Countrycode;
            hotelBookingDetail.Guest.Email = itineary.HotelPassenger.Email;
            hotelBookingDetail.Guest.FirstName = itineary.HotelPassenger.Firstname;
            hotelBookingDetail.Guest.LastName = itineary.HotelPassenger.Lastname;
            hotelBookingDetail.Guest.LeadGuest = itineary.HotelPassenger.LeadPassenger;
            hotelBookingDetail.Guest.MiddleName = itineary.HotelPassenger.Middlename;
            hotelBookingDetail.Guest.GuestType = (WSHotelGuestType)Enum.Parse(typeof(WSHotelGuestType), itineary.HotelPassenger.PaxType.ToString());
            hotelBookingDetail.Guest.Phoneno = itineary.HotelPassenger.Phoneno;
            hotelBookingDetail.Guest.RoomIndex = 0;
            hotelBookingDetail.Guest.State = itineary.HotelPassenger.State;
            hotelBookingDetail.Guest.Title = itineary.HotelPassenger.Title;
            hotelBookingDetail.Guest.Zipcode = itineary.HotelPassenger.Zipcode;
            hotelBookingDetail.Guest.Nationality = itineary.HotelPassenger.Nationality;
        }
        hotelBookingDetail.Rating = (WSHotelRating)Enum.Parse(typeof(WSHotelRating),itineary.Rating.ToString());
        if (itineary.Roomtype != null)
        {
            hotelBookingDetail.Roomtype = new WSHotelRoom[itineary.Roomtype.Length];
            for (int i = 0; i < itineary.Roomtype.Length; i++)
            {
                hotelBookingDetail.Roomtype[i] = new WSHotelRoom();
                hotelBookingDetail.Roomtype[i].AdultCount = itineary.Roomtype[i].AdultCount;
                hotelBookingDetail.Roomtype[i].Ameneties = itineary.Roomtype[i].Ameneties;
                hotelBookingDetail.Roomtype[i].ChildAge = itineary.Roomtype[i].ChildAge;
                hotelBookingDetail.Roomtype[i].ChildCount = itineary.Roomtype[i].ChildCount;
                hotelBookingDetail.Roomtype[i].ExtraBed = itineary.Roomtype[i].ExtraBed;
                hotelBookingDetail.Roomtype[i].NoOfCots = itineary.Roomtype[i].NoOfCots;
                hotelBookingDetail.Roomtype[i].NoOfExtraBed = itineary.Roomtype[i].NoOfExtraBed;
                hotelBookingDetail.Roomtype[i].RoomName = itineary.Roomtype[i].RoomName;
                hotelBookingDetail.Roomtype[i].MealPlanDesc = itineary.Roomtype[i].MealPlanDesc; //Added by brahmam
                hotelBookingDetail.Roomtype[i].SharingBed = itineary.Roomtype[i].SharingBed;
                if (itineary.Roomtype[i].PassenegerInfo != null && itineary.Roomtype[i].PassenegerInfo.Count > 0)
                {
                    hotelBookingDetail.Roomtype[i].GuestInfo = new List<WSGuest>();
                    foreach (HotelPassenger hotelPassenger in itineary.Roomtype[i].PassenegerInfo)
                    {
                        WSGuest guest = new WSGuest();
                        guest.Addressline1 = hotelPassenger.Addressline1;
                        guest.Addressline2 = hotelPassenger.Addressline2;
                        guest.Age = hotelPassenger.Age;
                        guest.Areacode = hotelPassenger.Areacode;
                        guest.City = hotelPassenger.City;
                        guest.Country = hotelPassenger.Country;
                        guest.Countrycode = hotelPassenger.Countrycode;
                        guest.Email = hotelPassenger.Email;
                        guest.FirstName = hotelPassenger.Firstname;
                        guest.LastName = hotelPassenger.Lastname;
                        guest.MiddleName = hotelPassenger.Middlename;
                        guest.GuestType = (WSHotelGuestType)Enum.Parse(typeof(WSHotelGuestType), hotelPassenger.PaxType.ToString());
                        guest.Phoneno = hotelPassenger.Phoneno;
                        guest.RoomIndex = i;
                        guest.State = hotelPassenger.State;
                        guest.Title = hotelPassenger.Title;
                        guest.Zipcode = hotelPassenger.Zipcode;
                        guest.LeadGuest = hotelPassenger.LeadPassenger;
                        hotelBookingDetail.Roomtype[i].GuestInfo.Add(guest);
                    }
                }
                hotelBookingDetail.Roomtype[i].RoomRate = new WSHotelRoomRate();
                PriceType priceType = WSHotelSearchRequest.GetPriceTypeFromSource(itineary.Source);
                if (priceType == PriceType.PublishedFare)
                {
                    hotelBookingDetail.Roomtype[i].RoomRate.TotalRoomPrice = itineary.Roomtype[i].Price.PublishedFare - itineary.Roomtype[i].Price.Discount - itineary.Roomtype[i].Price.AgentCommission;
                }
                else if (priceType == PriceType.NetFare)
                {
                    hotelBookingDetail.Roomtype[i].RoomRate.TotalRoomPrice = itineary.Roomtype[i].Price.NetFare + itineary.Roomtype[i].Price.Markup + itineary.Roomtype[i].Price.B2CMarkup - itineary.Roomtype[i].Price.Discount - itineary.Roomtype[i].Price.AgentCommission;
                }
                hotelBookingDetail.Roomtype[i].RoomRate.VatAmount = itineary.Roomtype[i].Price.OutputVATAmount;
                hotelBookingDetail.Roomtype[i].RoomRate.TotalRoomTax = itineary.Roomtype[i].Price.Tax;
                hotelBookingDetail.Roomtype[i].RoomRate.Discount = itineary.Roomtype[i].Price.WhiteLabelDiscount;
                hotelBookingDetail.Roomtype[i].RoomRate.OtherCharges = itineary.Roomtype[i].Price.WLCharge;
                hotelBookingDetail.Roomtype[i].RoomRate.ServiceTax = itineary.Roomtype[i].Price.SeviceTax + itineary.Roomtype[i].Price.TdsCommission;
            }
        }
        hotelBookingDetail.SpecialRequest = itineary.SpecialRequest;
        hotelBookingDetail.VoucherStatus = itineary.VoucherStatus;

        //commented by brahmam
        //Agency agency = new Agency(booking.AgencyId);
        AgentMaster agency = new AgentMaster(booking.AgencyId);
        hotelBookingDetail.Agency = new WSAgency();

        //hotelBookingDetail.Agency.AddressLine1 = agency.Address.Line1;
        //hotelBookingDetail.Agency.AddressLine2 = agency.Address.Line2;
        //hotelBookingDetail.Agency.City = agency.Address.CityName;
        //hotelBookingDetail.Agency.Email = agency.Address.Email;
        //hotelBookingDetail.Agency.Fax = agency.Address.Fax;
        //hotelBookingDetail.Agency.Name = agency.AgencyName;
        //hotelBookingDetail.Agency.Phone = agency.Address.Phone;
        //hotelBookingDetail.Agency.PIN = agency.Address.Pin;

        hotelBookingDetail.Agency.AddressLine1 = agency.Address;
        //hotelBookingDetail.Agency.AddressLine2 = agency.Address.Line2;
        hotelBookingDetail.Agency.City = agency.City;
        hotelBookingDetail.Agency.Email = agency.Email1;
        hotelBookingDetail.Agency.Fax = agency.Fax;
        hotelBookingDetail.Agency.Name = agency.Name;
        hotelBookingDetail.Agency.Phone = agency.Phone1;
        hotelBookingDetail.Agency.PIN = agency.POBox;
        hotelBookingDetail.AgencyReference = itineary.AgencyReference;
        if (itineary.Source == HotelBookingSource.HotelBeds)
        {
            hotelBookingDetail.ShowAgencyReference = true;
        }
        else
        {
            hotelBookingDetail.ShowAgencyReference = false;
        }
        if (itineary.Source == HotelBookingSource.IAN)
        {
            hotelBookingDetail.BookingReferenceType = WSBookingReferenceType.ConfirmationNo;
        }
        else
        {
            hotelBookingDetail.BookingReferenceType = WSBookingReferenceType.ConfirmationNo;
        }
        hotelBookingDetail.VatDescription = itineary.VatDescription;
        if (itineary.Source == HotelBookingSource.GTA)
        {

            //commented by brahmam

            //GTACity cityInfo = new GTACity();
            //cityInfo.Load(itineary.CityCode);
            //Dictionary<string, string> aotList = new Dictionary<string, string>();
            //aotList = cityInfo.LoadGTAAOTNumber(cityInfo.CountryCode, cityInfo.CountryName);
            //WSAOTNumbers hotelAotNumbers = new WSAOTNumbers();
            //hotelAotNumbers.Country = aotList["Country"];
            //hotelAotNumbers.Contact = aotList["City"];
            //hotelAotNumbers.IntlCall = aotList["InternationalNo"];
            //hotelAotNumbers.DomesticCall = aotList["NationalNo"];
            //hotelAotNumbers.LocalCall = aotList["LocalNo"];
            //hotelAotNumbers.OfficeHours = aotList["OfficeHours"];
            //hotelAotNumbers.Emergency = aotList["EmergencyNo"];
            //hotelAotNumbers.Language = aotList["Language"];
            //hotelBookingDetail.AOTNumbers = hotelAotNumbers;
        }
        return hotelBookingDetail;
    }
}
