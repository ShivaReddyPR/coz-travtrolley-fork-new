/// <summary>
/// Summary description for WSGetHotelBookingResponse
/// </summary>
public class WSGetHotelBookingResponse
{
    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    private WSHotelBookingDetail bookingDetail;
    public WSHotelBookingDetail BookingDetail
    {
        get
        {
            return bookingDetail;
        }
        set
        {
            bookingDetail = value;
        }
    }

    //Stores promo information
    private WSPromoDetail promoDetail;
    public WSPromoDetail PromoDetail
    {
        get { return promoDetail; }
        set { promoDetail = value; }
    }

    public WSGetHotelBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
