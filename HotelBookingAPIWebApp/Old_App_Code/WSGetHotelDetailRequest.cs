using System;
using System.Collections.Generic;
using System.Configuration;
//using Technology.BookingEngine;
//using Technology.Configuration;
using CT.BookingEngine;
using CT.Configuration;

/// <summary>
/// Summary description for WSGetHotelDetailRequest
/// </summary>
public class WSGetHotelDetailRequest
{
    private string sessionId;
    public string SessionId
    {
        get
        {
            return sessionId;
        }
        set
        {
            sessionId = value;
        }
    }

    private int index;
    public int Index
    {
        get { return index; }
        set { index = value; }
    }    

    public static WSHotelDetail FillHotelDetails(HotelDetails mseHotelDetail, HotelSearchResult result)
    {
        WSHotelDetail hotelDetail = new WSHotelDetail();
        string imagePath = string.Empty;
        if (result.BookingSource == HotelBookingSource.Desiya)
        {
            //Not to do
        }
        
        else if (result.BookingSource == HotelBookingSource.GTA)
        {
            //imagePath = ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.GTAConfig["imgPathForServer"] + "/";
        }
        else if(result.BookingSource == HotelBookingSource.HotelBeds)
        {
            //imagePath = ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.HotelBedsConfig["imgPathForServer"] + "/";
        }
        else if(result.BookingSource == HotelBookingSource.Tourico)
        {
            imagePath = ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.TouricoConfig["imgPathForServer"] + "/";
        }
        else if (result.BookingSource == HotelBookingSource.DOTW)
        {
            //imagePath = ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.DOTWConfig["imgPathForServer"] + "/";
        }
        else if (result.BookingSource == HotelBookingSource.Miki)
        {
            imagePath = ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.MikiConfig["imgPathForServer"] + "/";
        }
        else if (result.BookingSource == HotelBookingSource.Travco)
        {
            imagePath = ConfigurationManager.AppSettings["WebSiteRootUrl"] + ConfigurationSystem.TravcoConfig["imgPathForServer"] + "/";
        }
        hotelDetail.Address = mseHotelDetail.Address;
        // Attractions Checking null
        if (mseHotelDetail.Attractions != null)
        {
            hotelDetail.Attractions = new string[2][];
            hotelDetail.Attractions[0] = new string[mseHotelDetail.Attractions.Count];
            hotelDetail.Attractions[1] = new string[mseHotelDetail.Attractions.Count];
            int count = 0;
            foreach (KeyValuePair<string, string> de in mseHotelDetail.Attractions)
            {
                hotelDetail.Attractions[0][count] = de.Key;
                hotelDetail.Attractions[1][count] = de.Value;
                count++;
            }
        }
        hotelDetail.CountryName = mseHotelDetail.CountryName;
        hotelDetail.Description = mseHotelDetail.Description;
        hotelDetail.Email = mseHotelDetail.Email;
        hotelDetail.FaxNumber = mseHotelDetail.FaxNumber;
        hotelDetail.HotelCode = mseHotelDetail.HotelCode;
        hotelDetail.HotelFacilities = mseHotelDetail.HotelFacilities;
        hotelDetail.HotelName = mseHotelDetail.HotelName;
        hotelDetail.HotelPolicy = mseHotelDetail.HotelPolicy;
        hotelDetail.HotelRating = (WSHotelRating)Enum.Parse(typeof(WSHotelRating),mseHotelDetail.hotelRating.ToString());
        if (mseHotelDetail.Image != null && mseHotelDetail.Image.Trim() != "")
        {
            hotelDetail.Image = imagePath + mseHotelDetail.Image;
        }
        if (mseHotelDetail.Images != null && mseHotelDetail.Images.Count > 0)
        {
            hotelDetail.ImageUrls = new List<string>();
            foreach (string image in mseHotelDetail.Images)
            {
                if (image != null && image.Trim() != "")
                {
                    string hotelImage = imagePath + image;
                    hotelDetail.ImageUrls.Add(hotelImage);
                }
            }
        }
        hotelDetail.Map = mseHotelDetail.Map;
        hotelDetail.PhoneNumber = mseHotelDetail.PhoneNumber;
        hotelDetail.PinCode = Convert.ToInt32(mseHotelDetail.PinCode);
        hotelDetail.RoomFacilities = mseHotelDetail.RoomFacilities;
        hotelDetail.Services = mseHotelDetail.Services;
        hotelDetail.HotelWebsiteUrl = mseHotelDetail.URL;
        if (mseHotelDetail.RoomData != null)
        {
            hotelDetail.RoomData = new WSRoom[mseHotelDetail.RoomData.Length];
            for (int i = 0; i < mseHotelDetail.RoomData.Length; i++)
            {
                hotelDetail.RoomData[i] = new WSRoom();
                hotelDetail.RoomData[i].Description = mseHotelDetail.RoomData[i].Description;
                hotelDetail.RoomData[i].Images = new List<string>();
                if (mseHotelDetail.RoomData[i].Images != null && mseHotelDetail.RoomData[i].Images.Count > 0)
                {
                    foreach (string image in mseHotelDetail.RoomData[i].Images)
                    {
                        if (image != null && image.Trim() != "")
                        {
                            string roomImage = imagePath + image;
                            hotelDetail.RoomData[i].Images.Add(roomImage);
                        }
                    }
                }
                hotelDetail.RoomData[i].RoomName = mseHotelDetail.RoomData[i].RoomName;
                hotelDetail.RoomData[i].RoomTypeId = mseHotelDetail.RoomData[i].RoomTypeId;
            }
        }
        return hotelDetail;
    }

    public WSGetHotelDetailRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
