using System.Collections.Generic;

/// <summary>
/// Summary description for WSRoom
/// </summary>
public class WSRoom
{
    private string description;
    public string Description
    {
        get
        {
            return description;
        }
        set
        {
            description = value;
        }
    }

    private List<string> images;
    public List<string> Images
    {
        get
        {
            return images;
        }
        set
        {
            images = value;
        }
    }

    private string roomName;
    public string RoomName
    {
        get
        {
            return roomName;
        }
        set
        {
            roomName = value;
        }
    }

    private string roomTypeId;
    public string RoomTypeId
    {
        get
        {
            return roomTypeId;
        }
        set
        {
            roomTypeId = value;
        }
    }

    public WSRoom()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
