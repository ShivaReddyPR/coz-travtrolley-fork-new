﻿//using System.Linq;
//using System.Xml.Linq;
using CT.BookingEngine;

/// <summary>
/// Summary description for WLTransferVehicle
/// </summary>
public class WSTransferVehicle
{
    private string vehicle;
    private int transferId;
    private string vehicleCode;
    private int vehicleMaximumPassengers;
    private int vehicleMaximumLuggage;
    private PriceAccounts itemPrice;
    private string currency;
    private int occPax;

    public string Vehicle
    {
        get { return vehicle; }
        set { vehicle = value; }
    }
    public int TransferId
    {
        get { return transferId; }
        set { transferId = value; }

    }
    public string VehicleCode
    {
        get
        {
            return vehicleCode;
        }
        set { vehicleCode = value; }
    }
    public int VehicleMaximumPassengers
    {
        get { return vehicleMaximumPassengers; }
        set { vehicleMaximumPassengers = value; }
    }

    public int VehicleMaximumLuggage
    {
        get { return vehicleMaximumLuggage; }
        set { vehicleMaximumLuggage = value; }
    }
    public PriceAccounts ItemPrice
    {
        get { return itemPrice; }
        set { itemPrice = value; }
    }
    public string Currency
    {
        get { return currency; }
        set { currency = value; }
    }
    public int OccupiedPax
    {
        get { return occPax; }
        set { occPax = value; }
    }
    


}
