﻿using System;
//using System.Linq;
//using System.Xml.Linq;
using CT.BookingEngine;
using System.Collections.Generic;

/// <summary>
/// Summary description for WSSightSeeingBookingDetail
/// </summary>
public class WSSightSeeingBookingDetail
{
    private int sightseeingId;
    private string cityCode;     //City Code
    private string cityName;
    private string itemCode;     //Item Code
    private string itemName;
    private string currency;             //Price Information
    private PriceAccounts priceInfo;
    private string confirmationNo;    //Item Reference in Booking
    private DateTime tourDate;   //Tour Date
    private string tourLanguage;
    private List<string> paxNames; //Pax Id list according to the PaxType List
    private List<int> childAge;
    private int adultCount;
    private int childCount;
    private string bookingRef; //Random-generated Unique Identifier
    private DateTime createdOn;
    private int createdBy;
    private bool isDomestic;
    private bool voucherStatus;
    private DateTime lastCancellationDate;
    private string cancelPolicy;
    private string cancelId;
    private WSSightseeingBookingSource source;
    private string duration;
    private string depTime;
    private string depPointInfo;
    private string specialCode;
    private string supplierInfo;
    private string email;
    private string phNo;
    private string transType;
    private string address;
    private int agentId;
    private int locationId;
    private string note;
    private string specialName;
    private string telePhno;
    private string countryName;
    private int bookingId;
    #region Properities
    public int SightseeingId
    {
        get { return sightseeingId; }
        set { sightseeingId = value; }
    }
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }
    public string CityName
    {
        get { return cityName; }
        set { cityName = value; }
    }
    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
    public string Currency
    {
        get { return currency; }
        set { currency = value; }
    }
    public List<string> PaxNames
    {
        get { return paxNames; }
        set { paxNames = value; }
    }
    public int AdultCount
    {
        get { return adultCount; }
        set { adultCount = value; }
    }
    public int ChildCount
    {
        get { return childCount; }
        set { childCount = value; }
    }
    public string CancelId
    {
        get { return cancelId; }
        set { cancelId = value; }
    }
    public string ConfirmationNo
    {
        get { return confirmationNo; }
        set { confirmationNo = value; }
    }
    public PriceAccounts Price
    {
        get { return priceInfo; }
        set { priceInfo = value; }
    }
    public DateTime TourDate
    {
        get { return tourDate; }
        set { tourDate = value; }
    }
    public string Language
    {
        get { return tourLanguage; }
        set { tourLanguage = value; }
    }
    /// <summary>
    /// Gets or sets createdBy
    /// </summary>
    public int CreatedBy
    {
        get
        {
            return createdBy;
        }
        set
        {
            createdBy = value;
        }
    }

    /// <summary>
    /// Gets or sets createdOn Date
    /// </summary>
    public DateTime CreatedOn
    {
        get
        {
            return createdOn;
        }
        set
        {
            createdOn = value;
        }
    }
    public string CancellationPolicy
    {
        get { return cancelPolicy; }
        set { cancelPolicy = value; }
    }
    public DateTime LastCancellationDate
    {
        get { return lastCancellationDate; }
        set { lastCancellationDate = value; }
    }
    public string BookingReference
    {
        get { return bookingRef; }
        set { bookingRef = value; }
    }
    public bool IsDomestic
    {
        get { return isDomestic; }
        set { isDomestic = value; }
    }
    public bool VoucherStatus
    {
        get { return voucherStatus; }
        set { voucherStatus = value; }
    }
    public string ItemName
    {
        get { return itemName; }
        set { itemName = value; }
    }
    public List<int> ChildAge
    {
        get { return childAge; }
        set { childAge = value; }
    }
    public WSSightseeingBookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    public string Duration
    {
        get { return duration; }
        set { duration = value; }
    }
    public string DepTime
    {
        get { return depTime; }
        set { depTime = value; }
    }
    public string DepPointInfo
    {
        get { return depPointInfo; }
        set { depPointInfo = value; }
    }
    public string SpecialCode
    {
        get { return specialCode; }
        set { specialCode = value; }
    }
    public string SupplierInfo
    {
        get { return supplierInfo; }
        set { supplierInfo = value; }
    }
    public string Email
    {
        get { return email; }
        set { email = value; }
    }
    public string PhNo
    {
        get { return phNo; }
        set { phNo = value; }
    }
    public string TransType
    {
        get { return transType; }
        set { transType = value; }
    }
    public int AgentId
    {
        get { return agentId; }
        set { agentId = value; }
    }
    public int LocationId
    {
        get { return locationId; }
        set { locationId = value; }
    }
    public string Note
    {
        get { return note; }
        set { note = value; }
    }
    public string SpecialName
    {
        get { return specialName; }
        set { specialName = value; }
    }
    public string TelePhno
    {
        get { return telePhno; }
        set { telePhno = value; }
    }
    public string CountryName
    {
        get { return countryName; }
        set { countryName = value; }
    }
    public string Address
    {
        get { return address; }
        set { address = value; }
    }
    public int BookingId
    {
        get { return bookingId; }
        set { bookingId = value; }
    }
    #endregion
    public WSSightSeeingBookingDetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    //LoadingBooking Details
    public static WSSightSeeingBookingDetail BuildGetBookingResponse(SightseeingItinerary itineary)
    {
        WSSightSeeingBookingDetail bookingDetail = new WSSightSeeingBookingDetail();
        bookingDetail.adultCount = itineary.AdultCount;
        bookingDetail.bookingRef = itineary.BookingReference;
        bookingDetail.cancelPolicy = itineary.CancellationPolicy;
        bookingDetail.childAge = itineary.ChildAge;
        bookingDetail.childCount = itineary.ChildCount;
        bookingDetail.cityCode = itineary.CityCode;
        bookingDetail.cityName = itineary.CityName;
        bookingDetail.confirmationNo = itineary.ConfirmationNo;
        bookingDetail.createdBy = itineary.CreatedBy;
        bookingDetail.createdOn = itineary.CreatedOn;
        bookingDetail.currency = itineary.Currency;
        bookingDetail.depPointInfo = itineary.DepPointInfo;
        bookingDetail.depTime = itineary.DepTime;
        bookingDetail.duration = itineary.Duration;
        bookingDetail.email = itineary.Email;
        bookingDetail.itemCode = itineary.ItemCode;
        bookingDetail.itemName = itineary.ItemName;
        bookingDetail.tourLanguage = itineary.Language;
        bookingDetail.lastCancellationDate = itineary.LastCancellationDate;
        bookingDetail.paxNames = itineary.PaxNames;
        bookingDetail.phNo = itineary.PhNo;
        bookingDetail.Price = itineary.Price;
        bookingDetail.sightseeingId = itineary.SightseeingId;
        if (itineary.Source == SightseeingBookingSource.GTA)
        {
            bookingDetail.source = WSSightseeingBookingSource.GTA;
        }
        bookingDetail.specialCode = itineary.SpecialCode;
        bookingDetail.supplierInfo = itineary.SupplierInfo;
        bookingDetail.tourDate = itineary.TourDate;
        bookingDetail.voucherStatus = itineary.VoucherStatus;
        bookingDetail.agentId = itineary.AgentId;
        bookingDetail.locationId = itineary.LocationId;
        bookingDetail.note = itineary.Note;
        bookingDetail.address = itineary.Address;
        bookingDetail.countryName = itineary.CountryName;
        bookingDetail.SpecialName = itineary.SpecialName;
        bookingDetail.TelePhno = itineary.TelePhno;
        bookingDetail.bookingId = BookingDetail.GetBookingIdByProductId(itineary.SightseeingId, ProductType.SightSeeing);
        return bookingDetail;
    }
}

