using System;
using System.Collections.Generic;
//using Technology.BookingEngine;
using CT.BookingEngine;

/// <summary>
/// Summary description for WSHotelBookingDetail
/// </summary>
public class WSHotelBookingDetail
{
    private string bookingId;
    public string BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }

    private string bookingRefNo;
    public string BookingRefNo
    {
        get
        {
            return bookingRefNo;
        }
        set
        {
            bookingRefNo = value;
        }
    }

    private string confirmationNo;
    public string ConfirmationNo
    {
        get
        {
            return confirmationNo;
        }
        set
        {
            confirmationNo = value;
        }
    }

    private string hotelName;
    public string HotelName
    {
        get
        {
            return hotelName;
        }
        set
        {
            hotelName = value;
        }
    }

    private WSHotelRating rating;
    public WSHotelRating Rating
    {
        get
        {
            return rating;
        }
        set
        {
            rating = value;
        }
    }

    private DateTime checkInDate;
    public DateTime CheckInDate
    {
        get
        {
            return checkInDate;
        }
        set
        {
            checkInDate = value;
        }
    }

    private DateTime checkOutDate;
    public DateTime CheckOutDate
    {
        get
        {
            return checkOutDate;
        }
        set
        {
            checkOutDate = value;
        }
    }

    private string hotelAddress1;
    public string HotelAddress1
    {
        get
        {
            return hotelAddress1;
        }
        set
        {
            hotelAddress1 = value;
        }
    }

    private string hotelAddress2;
    public string HotelAddress2
    {
        get
        {
            return hotelAddress2;
        }
        set
        {
            hotelAddress2 = value;
        }
    }

    private string cityCode;
    public string CityCode
    {
        get
        {
            return cityCode;
        }
        set
        {
            cityCode = value;
        }
    }

    private string cityRef;
    public string CityRef
    {
        get
        {
            return cityRef;
        }
        set
        {
            cityRef = value;
        }
    }

    private string flightInfo;
    public string FlightInfo
    {
        get
        {
            return flightInfo;
        }
        set
        {
            flightInfo = value;
        }
    }

    private string specialRequest;
    public string SpecialRequest
    {
        get
        {
            return specialRequest;
        }
        set
        {
            specialRequest = value;
        }
    }

    private int noOfRooms;
    public int NoOfRooms
    {
        get
        {
            return noOfRooms;
        }
        set
        {
            noOfRooms = value;
        }
    }

    private List<HotelPenality> paneltyInfo;
    public List<HotelPenality> PaneltyInfo
    {
        get
        {
            return paneltyInfo;
        }
        set
        {
            paneltyInfo = value;
        }
    }

    private string hotelCancelPolicy;
    public string HotelCancelPolicy
    {
        get
        {
            return hotelCancelPolicy;
        }
        set
        {
            hotelCancelPolicy = value;
        }
    }

    private string hotelPolicyDetails;
    public string HotelPolicyDetails
    {
        get
        {
            return hotelPolicyDetails;
        }
        set
        {
            hotelPolicyDetails = value;
        }
    }

    private WSGuest guest;
    public WSGuest Guest
    {
        get
        {
            return guest;
        }
        set
        {
            guest = value;
        }
    }

    private bool isDomestic;
    public bool IsDomestic
    {
        get
        {
            return isDomestic;
        }
        set
        {
            isDomestic = value;
        }
    }

    private DateTime bookingDate;
    public DateTime BookingDate
    {
        get
        {
            return bookingDate;
        }
        set
        {
            bookingDate = value;
        }
    }

    private DateTime lastCancellationDate;
    public DateTime LastCancellationDate
    {
        get
        {
            return lastCancellationDate;
        }
        set
        {
            lastCancellationDate = value;
        }
    }

    private string map;
    public string Map
    {
        get
        {
            return map;
        }
        set
        {
            map = value;
        }
    }

    private WSHotelRoom[] roomtype;
    public WSHotelRoom[] Roomtype
    {
        get
        {
            return roomtype;
        }
        set
        {
            roomtype = value;
        }
    }

    private WSHotelBookingStatus bookingStatus;
    public WSHotelBookingStatus BookingStatus
    {
        get
        {
            return bookingStatus;
        }
        set
        {
            bookingStatus = value;
        }
    }

    private bool voucherStatus;
    public bool VoucherStatus
    {
        get
        {
            return voucherStatus;
        }
        set
        {
            voucherStatus = value;
        }
    }

    private decimal rateofExchange;
    public decimal RateofExchange
    {
        get
        {
            return rateofExchange;
        }
        set
        {
            rateofExchange = value;
        }
    }

    private string currency;
    public string Currency
    {
        get
        {
            return currency;
        }
        set
        {
            currency = value;
        }
    }

    private string vatDescription;
    public string VatDescription
    {
        get
        {
            return vatDescription;
        }
        set
        {
            vatDescription = value;
        }
    }

    private WSAOTNumbers aotNumbers;
    public WSAOTNumbers AOTNumbers
    {
        get
        {
            return aotNumbers;
        }
        set
        {
            aotNumbers = value;
        }
    }

    private string agencyReference;
    public string AgencyReference
    {
        get
        {
            return agencyReference;
        }
        set
        {
            agencyReference = value;
        }
    }

    private WSBookingReferenceType bookingReferenceType;
    public WSBookingReferenceType BookingReferenceType
    {
        get
        {
            return bookingReferenceType;
        }
        set
        {
            bookingReferenceType = value;
        }
    }

    private bool showAgencyReference;
    public bool ShowAgencyReference
    {
        get
        {
            return showAgencyReference;
        }
        set
        {
            showAgencyReference = value;
        }
    }

    private WSAgency agency;
    public WSAgency Agency
    {
        get { return agency; }
        set { agency = value; }
    }
    private HotelBookingSource source;
    public HotelBookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    string paymentGuaranteedBy = string.Empty;
    public string PaymentGuaranteedBy
    {
        get { return paymentGuaranteedBy; }
        set { paymentGuaranteedBy = value; }
    }
    public WSHotelBookingDetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public enum WSHotelBookingStatus
{
    Failed = 0,
    Confirmed = 1,
    Cancelled = 2,
    Rejected = 5,
    Vouchered = 6
}

public enum WSBookingReferenceType
{
    BookingRefNo = 1,
    ConfirmationNo = 2,
}

public struct WSAOTNumbers
{
    public string Country;
    public string Contact;
    public string IntlCall;
    public string DomesticCall;
    public string LocalCall;
    public string OfficeHours;
    public string Emergency;
    public string Language;
}
