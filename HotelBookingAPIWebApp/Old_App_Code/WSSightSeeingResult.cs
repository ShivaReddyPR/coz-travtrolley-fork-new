﻿using System;
//using System.Linq;
//using System.Xml.Linq;
using System.Collections.Generic;
using CT.BookingEngine;

/// <summary>
/// Summary description for WSSightSeeingResult
/// </summary>
/// 
[Serializable]
public class WSSightSeeingResult
{
    private bool depaturePointRequired;              //if set to true, depature point is required ,when booking
    private string cityCode;                         //City Code & Name
    private string cityName;
    private string itemCode;                         //Sightseeing Code & Name
    private string itemName;
    private string duration;                         //Sighseeing Duration
    private WSSightseeingTypeList[] sightseeingTypeList;          //Sightseeing Type Code & Name
    private WSSightseeingCategoryList[] sightseeingCategoryList;      //Sightseeing Category Code & Name
    private bool hasExtraInfo;
    private bool hasFlash;
    private WSTourOperation[] tourOperationList;
    private string[] additionalInformationList;
    private EssentialInformation[] essentialInformationList;
    private string description;
    private string image;
    private WSSightseeingBookingSource source;
    private string flashLink;
    private string sessionId;
    private int index;
    #region Properity
    public bool DepaturePointRequired
    {
        get { return depaturePointRequired; }
        set { depaturePointRequired = value; }
    }
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }
    public string CityName
    {
        get { return cityName; }
        set { cityName = value; }
    }

    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
    public string ItemName
    {
        get { return itemName; }
        set { itemName = value; }
    }
    public string Duration
    {
        get { return duration; }
        set { duration = value; }
    }
    public WSSightseeingTypeList[] SightseeingTypeList
    {
        get { return sightseeingTypeList; }
        set { sightseeingTypeList = value; }
    }
    public WSSightseeingCategoryList[] SightseeingCategoryList
    {
        get { return sightseeingCategoryList; }
        set { sightseeingCategoryList = value; }
    }
    public bool HasExtraInfo
    {
        get { return hasExtraInfo; }
        set { hasExtraInfo = value; }
    }
    public bool HasFlash
    {
        get { return hasFlash; }
        set { hasFlash = value; }
    }
    public WSTourOperation[] TourOperationList
    {
        get { return tourOperationList; }
        set { tourOperationList = value; }
    }
    public string[] AdditionalInformationList
    {
        get { return additionalInformationList; }
        set { additionalInformationList = value; }
    }
    public EssentialInformation[] EssentialInformationList
    {
        get { return essentialInformationList; }
        set { essentialInformationList = value; }
    }
    public string Description
    {
        get { return description; }
        set { description = value; }
    }
    public string Image
    {
        get { return image; }
        set { image = value; }
    }
    public WSSightseeingBookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    public string FlashLink
    {
        get { return flashLink; }
        set { flashLink = value; }
    }
    public string SessionId
    {
        get { return sessionId; }
        set { sessionId = value; }
    }
    public int Index
    {
        get { return index; }
        set { index = value; }
    }
    #endregion

    public WSSightSeeingResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public struct WSTourOperation
{
    public List<string> TourLanguageList;         //Tour Available Language
    public List<string> LanguageCode;
    public decimal ItemPrice;                          //Item Price in decimal
    public string Currency;
    public WSConfirmationCodeList[] ConfirmationCodeList; //
    public List<string> SpecialItemList;      //
    public string SpecialItemName;
    public List<string> LangName;
    public PriceAccounts PriceInfo;
}
public struct WSSightseeingTypeList
{
    public string Code;
    public string Value;
}

public struct WSSightseeingCategoryList
{
    public string Code;
    public string Value;
}
public struct WSConfirmationCodeList
{
    public string Code;
    public string Value;
}