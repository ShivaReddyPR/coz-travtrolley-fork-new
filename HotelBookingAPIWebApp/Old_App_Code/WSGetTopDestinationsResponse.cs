using System.Collections.Generic;

/// <summary>
/// Summary description for WSGetTopDestinationsResponse
/// </summary>
public class WSGetTopDestinationsResponse
{
    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    private List<WSCity> cityList;
    public List<WSCity> CityList
    {
        get
        {
            return cityList;
        }
        set
        {
            cityList = value;
        }
    }

    public WSGetTopDestinationsResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
