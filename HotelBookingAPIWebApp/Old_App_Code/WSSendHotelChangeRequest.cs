using System;
//using Technology.BookingEngine;
//using CoreLogic;
//using CoreLogic;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for SendHotelChangeRequest
/// </summary>
public class WSSendHotelChangeRequest
{
    private string bookingId;
    public string BookingId
    {
        get { return bookingId; }
        set { bookingId = value; }
    }

    private WSRequestType requestType;
    public WSRequestType RequestType
    {
        get { return requestType; }
        set { requestType = value; }
    }

    private string remarks;
    public string Remarks
    {
        get { return remarks; }
        set { remarks = value; }
    }

    public WSSendHotelChangeRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static WSSendHotelChangeRequestResponse SendChangeRequest(WSSendHotelChangeRequest request, UserMaster member)
    {
        WSSendHotelChangeRequestResponse response = new WSSendHotelChangeRequestResponse();
        WSStatus status = new WSStatus();
        ServiceRequest serviceRequest = new ServiceRequest();
        BookingDetail booking = new BookingDetail();
        HotelItinerary itineary = new HotelItinerary();
        HotelRoom room = new HotelRoom();        
        try
        {
            booking = new BookingDetail(Convert.ToInt32(request.BookingId.Trim()));
            Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].ProductTypeId == (int)ProductType.Hotel)
                {
                    itineary.Load(products[i].ProductId);
                    itineary.Roomtype = room.Load(products[i].ProductId);
                    break;
                }
            }
            booking.SetBookingStatus(BookingStatus.InProgress, member.AgentId);
        }
        catch (BookingEngineException ex)
        {
            status.Category = "CR";
            status.StatusCode = "06";
            status.Description = "SendHotelChangeRequest method Failed, Technical Fault 01";
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, (int)member.ID, "B2C-Hotel- Error : " + ex.Message + " Stack Trace " + ex.StackTrace, "");
            return response;
        }
        catch (ArgumentException ex)
        {
            status.Category = "CR";
            status.StatusCode = "07";
            status.Description = "SendHotelChangeRequest method Failed. : " + ex.Message;
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, (int)member.ID, "B2C-Hotel- Error : " + ex.Message + " Stack Trace " + ex.StackTrace, "");
            return response;
        }
        catch (Exception ex)
        {
            status.Category = "CR";
            status.StatusCode = "08";
            status.Description = "Technical Fault 02";
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, (int)member.ID, "B2C-Hotel- Error : " + ex.Message + " Stack Trace " + ex.StackTrace, "");
            return response;
        }
        serviceRequest.BookingId = booking.BookingId;
        serviceRequest.ReferenceId = itineary.Roomtype[0].RoomId;
        serviceRequest.ProductType = ProductType.Hotel;
        serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), request.RequestType.ToString());
        serviceRequest.Data = request.Remarks;
        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
        serviceRequest.CreatedBy = member.AgentId;
        BookingHistory bh = new BookingHistory();
        bh.BookingId = booking.BookingId;
        bh.Remarks = "Request sent to cancel the Hotel booking of " + itineary.ConfirmationNo;
        bh.EventCategory = EventCategory.HotelCancel;
        bh.CreatedBy = member.AgentId;
        try
        {
            serviceRequest.Save();
            bh.Save();
        }
        catch (BookingEngineException ex)
        {
            status.Category = "CR";
            status.StatusCode = "09";
            status.Description = "Technical Fault 03";
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, (int)member.ID, "B2C-Hotel- Error : " + ex.Message + " Stack Trace " + ex.StackTrace, "");
            return response;
        }
        catch (ArgumentException ex)
        {
            status.Category = "CR";
            status.StatusCode = "10";
            status.Description = "ServiceRequest save Failed. : " + ex.Message;
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, (int)member.ID, "B2C-Hotel- Error : " + ex.Message + " Stack Trace " + ex.StackTrace, "");
            return response;
        }
        catch (Exception ex)
        {
            status.Category = "CR";
            status.StatusCode = "11";
            status.Description = "Technical Fault 04";
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, (int)member.ID, " B2C-Hotel-Error : " + ex.Message + " Stack Trace " + ex.StackTrace, "");
            return response;
        }
        if (serviceRequest.RequestId > 0)
        {
            status.Category = "CR";
            status.StatusCode = "01";
            status.Description = "Successful";
            response.Status = status;
            response.ChangeRequestId = serviceRequest.RequestId;
            Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, (int)member.ID, "B2C-Hotel-SendHotelChangeRequest Successful", "");
        }
        return response;
    }
}

public enum WSRequestType
{
    HotelCancel = 1
}

