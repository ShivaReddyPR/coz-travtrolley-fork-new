﻿//using System.Linq;
//using System.Xml.Linq;
using CT.BookingEngine;
using System.Collections.Generic;

/// <summary>
/// Summary description for WSTransferBookRequest
/// </summary>
public class WSTransferBookRequest
{
    WSPaymentInformation paymentInformation;
    WSTransferItinerary transferItinerary;


    public WSPaymentInformation PaymentInformation
    {
        get { return paymentInformation; }
        set { paymentInformation = value; }
    }

    public WSTransferItinerary TransferItinerary
    {
        get { return transferItinerary; }
        set { transferItinerary = value; }
    }

    public WSTransferBookRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public TransferItinerary BuildItinerary()
    {
        TransferItinerary itinerary = new TransferItinerary();
        itinerary.BookingReference = transferItinerary.BookingReference;
        itinerary.BookingStatus = (TransferBookingStatus)transferItinerary.BookingStatus;
        itinerary.CancelId = transferItinerary.CancelId;
        itinerary.CancellationPolicy = transferItinerary.CancellationPolicy;
        itinerary.CityCode = transferItinerary.CityCode;
        itinerary.ConfirmationNo = transferItinerary.ConfirmationNo;
        itinerary.CreatedBy = transferItinerary.CreatedBy;
        itinerary.CreatedOn = transferItinerary.CreatedOn;
        itinerary.DropOffCode = transferItinerary.DropOffCode;
        itinerary.DropOffDescription = transferItinerary.DropOffDescription;
        itinerary.DropOffRemarks = transferItinerary.DropOffRemarks;
        itinerary.DropOffTime = transferItinerary.DropOffTime;
        itinerary.DropOffType = (PickDropType)transferItinerary.DropOffType;
        itinerary.IsDomestic = transferItinerary.IsDomestic;
        itinerary.ItemCode = transferItinerary.ItemCode;
        itinerary.ItemName = transferItinerary.ItemName;
        itinerary.Language = transferItinerary.Language;
        itinerary.LastCancellationDate = transferItinerary.LastCancellationDate;
        itinerary.LastModifiedBy = transferItinerary.LastModifiedBy;
        itinerary.LastModifiedOn = transferItinerary.LastModifiedOn;
        itinerary.NumOfPax = transferItinerary.NumOfPax;
        itinerary.PassengerInfo = transferItinerary.PassengerInfo;
        itinerary.PenalityInfo = new List<TransferPenalty>();
        itinerary.PickUpCode = transferItinerary.PickUpCode;
        itinerary.PickUpDescription = transferItinerary.PickUpDescription;
        itinerary.PickUpRemarks = transferItinerary.PickUpRemarks;
        itinerary.PickUpTime = transferItinerary.PickUpTime;
        itinerary.PickUpType = (PickDropType)transferItinerary.PickUpType;
        itinerary.Price = transferItinerary.Price;
        itinerary.ProductType = ProductType.Transfers;
        itinerary.Source = (TransferBookingSource)transferItinerary.Source;
        itinerary.TransferDate = transferItinerary.TransferDate;
        itinerary.TransferConditions = transferItinerary.TransferConditions;
        itinerary.TransferDetails = new List<TransferVehicle>();
        foreach (WSTransferVehicle vehicle in transferItinerary.TransferDetails)
        {
            TransferVehicle transVehicle = new TransferVehicle();
            transVehicle.Currency = vehicle.Currency;
            transVehicle.ItemPrice = vehicle.ItemPrice;
            transVehicle.OccupiedPax = vehicle.OccupiedPax;
            transVehicle.TransferId = vehicle.TransferId;
            transVehicle.Vehicle = vehicle.Vehicle;
            transVehicle.VehicleCode = vehicle.VehicleCode;
            transVehicle.VehicleMaximumLuggage = vehicle.VehicleMaximumLuggage;
            transVehicle.VehicleMaximumPassengers = vehicle.VehicleMaximumPassengers;

            itinerary.TransferDetails.Add(transVehicle);
        }
        itinerary.TransferTime = transferItinerary.TransferTime;
        itinerary.VoucherStatus = transferItinerary.VoucherStatus;
        itinerary.TransactionType = "B2C";
        return itinerary;
    }
}
