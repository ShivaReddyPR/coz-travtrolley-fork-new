﻿using CZHotelInventory.Application.BusinessOps;
using MediatR;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Xml.Linq;

namespace CZHotelInventory.Controllers
{
    /// <summary>
    /// Controller to perform Hotel request related actions
    /// </summary>
    [Authorize]
    [RoutePrefix("api/hotel")]
    public class HotelController : ApiController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// CommonMasters constructor
        /// </summary>
        /// <param name="mediator"></param>
        public HotelController(IMediator mediator)
        {
            _mediator = mediator;
        }


        /// <summary>
        /// Gets the hotel search result
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("hotel")]
        [ResponseType(typeof(Hotel.ViewModel))]
        public async Task<IHttpActionResult> Hotel([FromBody]Hotel.Query clsQuery){
            var result = await _mediator.Send(clsQuery);
              
            return Ok(result);
        }

        /// <summary>
        /// Gets the hotelRoom result
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("hotelRoom")]
        [ResponseType(typeof(HotelRoom.ViewModel))]
        public async Task<IHttpActionResult> HotelRoom([FromBody]HotelRoom.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the Cancellation Policy
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("cancellationPolicy")]
        [ResponseType(typeof(CancelPolicy.ViewModel))]
        public async Task<IHttpActionResult> CancellationPolicy([FromBody]CancelPolicy.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Checking Item price
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("priceRequest")]
        [ResponseType(typeof(HotelPriceRequest.ViewModel))]
        public async Task<IHttpActionResult> PriceRequest([FromBody]HotelPriceRequest.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Booking Hotel
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("bookHotel")]
        [ResponseType(typeof(HotelBooking.ViewModel))]
        public async Task<IHttpActionResult> BookHotel([FromBody]HotelBooking.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Booking Cancel
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("bookingCancel")]
        [ResponseType(typeof(CZHotelInventory.Application.BusinessOps.CancelBooking.ViewModel))]
        public async Task<IHttpActionResult> BookingCancel([FromBody]CancelBooking.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Email Request for Booking Cancel
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("bookingCancelEmailRequest")]
        [ResponseType(typeof(CZHotelInventory.Application.BusinessOps.CancelEmailRequest.ViewModel))]
        public async Task<IHttpActionResult> BookingCancelEmailRequest([FromBody]CancelEmailRequest.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }
    }
}
