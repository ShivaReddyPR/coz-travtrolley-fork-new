﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;


namespace CZHotelInventory.Api.Models
{
    /// <summary>
    /// To pass agent info in api request 
    /// </summary>
    public class ApiAgentInfo
    {
        public int AgentId { get; set; }

        public int LoginUserId { get; set; }

        public int OnBelahfAgentLoc { get; set; }

        public string IPAddress { get; set; }

        public int LoginUserCorpId { get; set; }


        public string AgentToken(string sUserName, string sPassword)
        {
            string sToken = string.Empty;

            string sAPIURL = "http://localhost:60983/";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(sAPIURL);

            HttpResponseMessage response =
              client.PostAsync("token",
                new StringContent(string.Format("grant_type=password&username={0}&password={1}",
                 HttpUtility.UrlEncode(sUserName),
                  HttpUtility.UrlEncode(sPassword)), Encoding.UTF8,
                  "application/x-www-form-urlencoded")).Result;

            string resultJSON = response.Content.ReadAsStringAsync().Result;
            LoginTokenResult result = JsonConvert.DeserializeObject<LoginTokenResult>(resultJSON);

            if (result != null && !string.IsNullOrEmpty(result.ErrorDescription))
                throw new Exception(result.Error + ": " + result.ErrorDescription);

            sToken = result.AccessToken;
            //HttpContext.Current.Session[sSessionKey] = sToken;


            return sToken;
        }

        /// <summary>
        /// To read api token result
        /// </summary>
        public class LoginTokenResult
        {
            public override string ToString()
            {
                return AccessToken;
            }

            [JsonProperty(PropertyName = "access_token")]
            public string AccessToken { get; set; }

            [JsonProperty(PropertyName = "error")]
            public string Error { get; set; }

            [JsonProperty(PropertyName = "error_description")]
            public string ErrorDescription { get; set; }

        }
    }
}
