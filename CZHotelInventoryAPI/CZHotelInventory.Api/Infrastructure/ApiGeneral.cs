﻿//using CZHotelInventory.Application.BusinessOps;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using System.Xml;

namespace CZHotelInventory.Api.Infrastructure
{
    /// <summary>
    /// To impelement common static classes
    /// </summary>
    public static class ApiGeneral
    {
        public static string _logsPath = ConfigurationManager.AppSettings["ApiLogsPath"];

        /// <summary>
        /// To generate Json and XMl logs based on web response Json string
        /// </summary>
        /// <param name="IsJson"></param>
        /// <param name="IsXML"></param>
        /// <param name="sData"></param>
        /// <param name="sPath"></param>
        /// <returns></returns>
        public static string GenerateLogs(bool IsJson, bool IsXML, string sData, string sTraceId)
        {
            string sFNames = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(_logsPath))
                    throw new Exception("Invalid logs path");

                string sPath = _logsPath + @"\" + DateTime.Now.ToString("dd-MM-yyy") + @"\";
                if (!Directory.Exists(sPath))
                    Directory.CreateDirectory(sPath);

                sPath += sTraceId;

                if (IsJson)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    js.MaxJsonLength = Int32.MaxValue;
                    dynamic searchResponse = js.Deserialize<dynamic>(sData);
                    sData = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    sFNames = sPath + "_Json_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".json";
                    StreamWriter sw = new StreamWriter(sFNames);
                    sw.Write(sData);
                    sw.Close();
                }

                if (IsXML)
                {
                    TextReader txtReader = new StringReader(sData);
                    XmlDocument doc = JsonConvert.DeserializeXmlNode(sData, "Root");
                    string sFile = sPath + "_xml_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    sFNames = string.IsNullOrEmpty(sFNames) ? sFile : sPath + "|" + sFile;
                    doc.Save(sFile);
                }
            }
            catch (Exception ex)
            {
              //Audit.Add(EventType.Book, Severity.High, 1, "Failed to Generate Logs from GenerateLogs Method. Reason : " + ex.ToString(), "");
            }

            return sFNames;
        }

        /// <summary>
        /// To store Owin token in the text file with the file name as GUID
        /// </summary>
        /// <param name="sGUID"></param>
        /// <param name="SOwinToken"></param>
        /// <returns></returns>
        public static string SetOwinToken(string sGUID, string SOwinToken)
        {
            string sResponse = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(_logsPath))
                    throw new Exception("Invalid logs path");

                string sPath = _logsPath + @"\" + DateTime.Now.ToString("dd-MM-yyy") + @"\";
                if (!Directory.Exists(sPath))
                    Directory.CreateDirectory(sPath);

                sPath = sPath + "ApiResponse_OwinAuth_" + sGUID + ".txt";

                File.AppendAllText(sPath, SOwinToken);
                sResponse = "Success";
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Book, Severity.High, 1, "Failed  from SetOwinToken Method. Reason : " + ex.ToString(), "");
            }

            return sResponse;
        }

        /// <summary>
        /// To get Owin token from the text file with the file name as GUID
        /// </summary>
        /// <param name="sGUID"></param>
        /// <returns></returns>
        public static string GetOwinToken(string sGUID)
        {
            string sResponse = string.Empty;
            try
            {
                string sPath = _logsPath + @"\" + DateTime.Now.ToString("dd-MM-yyy") + @"\ApiResponse_OwinAuth_" + sGUID + ".txt";
                if (File.Exists(sPath))
                    sResponse = File.ReadAllText(sPath);
            }
            catch (Exception ex)
            {
               // Audit.Add(EventType.Exception, Severity.High, 1, "Failed  from GetOwinToken Method. Reason : " + ex.ToString(), "");
            }

            return sResponse;
        }
    }
}