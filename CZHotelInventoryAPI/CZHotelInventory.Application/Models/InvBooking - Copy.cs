﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using System.Linq;
using CZHotelInventory.Application.BusinessOps;
using System.Globalization;

namespace CZHotelInventory.Application.Models
{
    public class InvBooking
    {
        //public InvBooking()
        //{
        //    Connection();
        //}

        //private void Connection()
        //{
        //    if (ConfigurationSystem.HotelConnectConfig["ConnectionString"] != null)
        //    {
        //        DBGateway.ConnectionString = (ConfigurationSystem.HotelConnectConfig["ConnectionString"]);
        //    }
        //}

        /// <summary>
        /// This method returns the search results for the Hotel Search Request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DataTable GetInventoryHotels(HotelRequest request)
        {
            DataTable dtHotelResults = new DataTable();
            string roomPax = "";
            string childAge = "";
            string HotelIds = "";
            //int child = -1;
            int order = 1;
            int countryId = 0;
            Dictionary<int, string> Guests = new Dictionary<int, string>();
            try
            {
                foreach (RoomGuest guest in request.RoomGuestData)
                {
                    if (roomPax.Trim().Length > 0)
                    {
                        roomPax += "|" + guest.NoOfAdults + "," + guest.NoOfChild;
                        Guests.Add(order, guest.NoOfAdults + "," + guest.NoOfChild);
                    }
                    else
                    {
                        roomPax = guest.NoOfAdults + "," + guest.NoOfChild;
                        Guests.Add(order, guest.NoOfAdults + "," + guest.NoOfChild);
                    }

                    order++;
                    if (guest.NoOfChild > 0)
                    {
                        for (int i = 0; i < guest.ChildAge.Length; i++)
                        {
                            int age = guest.ChildAge[i];
                            if (childAge.Trim().Length > 0 && !childAge.EndsWith("|"))
                            {
                                childAge += "," + age;
                            }
                            else
                            {
                                childAge += age.ToString();
                            }
                        }

                        {
                            childAge += "|";
                        }
                    }
                    else
                    {
                        if (childAge.Trim().Length > 0 && childAge.EndsWith("|"))
                        {
                            childAge += "0|";
                        }
                        else
                        {
                            childAge += "0|";
                        }
                    }
                }

                if (childAge.EndsWith("|"))
                {
                    childAge = childAge.Remove(childAge.Length - 1, 1);
                }

                if (request.HotelId != null)
                {
                    HotelIds = string.Join(",", request.HotelId);
                }



                SqlParameter[] paramList = new SqlParameter[19];
                if (countryId > 0)
                {
                    paramList[0] = new SqlParameter("@P_Country_Id", countryId);
                }
                else
                {
                    paramList[0] = new SqlParameter("@P_Country_Id", DBNull.Value);
                }
                if (request.CityId > 0) paramList[1] = new SqlParameter("@P_City_Id", request.CityId);
                else paramList[1] = new SqlParameter("@P_City_Id", DBNull.Value);
                paramList[2] = new SqlParameter("@P_CheckIn_Date", DateTime.ParseExact(request.StartDate, "dd/MM/yyyy", null));
                paramList[3] = new SqlParameter("@P_CheckOut_Date", DateTime.ParseExact(request.EndDate, "dd/MM/yyyy", null));
                switch (request.Rating)
                {
                    case HotelRating.All:
                        paramList[4] = new SqlParameter("@P_Star_Level", "");
                        break;
                    case HotelRating.OneStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "1S");
                        break;
                    case HotelRating.TwoStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "2S");
                        break;
                    case HotelRating.ThreeStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "3S");
                        break;
                    case HotelRating.FourStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "4S");
                        break;
                    case HotelRating.FiveStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "5S");
                        break;
                }
                paramList[5] = new SqlParameter("@P_Hotel_Name", request.HotelName);
                paramList[6] = new SqlParameter("@P_No_Of_Rooms", request.NoOfRooms);
                paramList[7] = new SqlParameter("@P_Room_Pax", roomPax);
                paramList[8] = new SqlParameter("@P_Child_Ages", childAge);
                paramList[9] = new SqlParameter("@P_Property_Type_Id", DBNull.Value);
                paramList[10] = new SqlParameter("@P_Show_Map", DBNull.Value);
                paramList[11] = new SqlParameter("@P_Currency", request.Currency);
                paramList[12] = new SqlParameter("@P_MSG_TEXT", "");
                paramList[12].Direction = ParameterDirection.Output;
                paramList[13] = new SqlParameter("@P_MSG_TYPE", "");
                paramList[13].Direction = ParameterDirection.Output;


                if (request.PassengerNationality == null || request.PassengerNationality.Length <= 0)
                {
                    paramList[14] = new SqlParameter("@P_Nationality", DBNull.Value);
                }
                else
                {

                    //string countryCode = BusinessOps.Country.GetCountryCodeFromCountryName(request.PassengerNationality);
                    //   string countryCode = country. .GetCountryCodeFromCountryName(request.PassengerNationality);

                    //request.PassengerNationality = countryCode;

                    paramList[14] = new SqlParameter("@P_Nationality", (request.PassengerNationality));
                }
                if (request.Longtitude != 0.0)
                {
                    paramList[15] = new SqlParameter("@P_Longitude", request.Longtitude);
                }
                else
                {
                    paramList[15] = new SqlParameter("@P_Longitude", DBNull.Value);
                }
                if (request.Latitude != 0.0)
                {
                    paramList[16] = new SqlParameter("@P_Latitude", request.Latitude);
                }
                else
                {
                    paramList[16] = new SqlParameter("@P_Latitude", DBNull.Value);
                }
                if (request.Radius > 0) paramList[17] = new SqlParameter("@P_Radius", request.Radius / 1000);
                else paramList[17] = new SqlParameter("@P_Radius", DBNull.Value);
                if (HotelIds == "")
                {
                    paramList[18] = new SqlParameter("@P_HotelIds", DBNull.Value);
                }
                else
                {
                    paramList[18] = new SqlParameter("@P_HotelIds", HotelIds);
                }
                InvBooking invBooking = new InvBooking();
                dtHotelResults = DBGateway.FillDataTableSP("USP_GETHOTELRESULTSNEW", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "(CZInventory)Failed to get results. Error: " + ex.ToString(), "");
            }

            return dtHotelResults;
        }

        /// <summary>
        /// This method returns the search results for the Hotel Search Request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DataTable GetInventoryHotelRooms(HotelRoomRequest roomrequest)
        {

            DataTable dtHotelRoomResults = new DataTable();
            string roomPax = "";
            string childAge = "";
            int order = 1;

            Dictionary<int, string> Guests = new Dictionary<int, string>();
            try
            {
                foreach (RoomGuest guest in roomrequest.RoomGuestData)
            {
                if (roomPax.Trim().Length > 0)
                {
                    roomPax += "|" + guest.NoOfAdults + "," + guest.NoOfChild;
                    Guests.Add(order, guest.NoOfAdults + "," + guest.NoOfChild);
                }
                else
                {
                    roomPax = guest.NoOfAdults + "," + guest.NoOfChild;
                    Guests.Add(order, guest.NoOfAdults + "," + guest.NoOfChild);
                }

                order++;
                if (guest.NoOfChild > 0)
                {
                    for (int i = 0; i < guest.ChildAge.Length; i++)
                    {
                        int age = guest.ChildAge[i];
                        if (childAge.Trim().Length > 0 && !childAge.EndsWith("|"))
                        {
                            childAge += "," + age;
                        }
                        else
                        {
                            childAge += age.ToString();
                        }
                    }

                    //if (!childAge.Contains("|"))
                    {
                        childAge += "|";
                    }
                }
                else
                {
                    if (childAge.Trim().Length > 0 && childAge.EndsWith("|"))
                    {
                        childAge += "0|";
                    }
                    else
                    {
                        childAge += "0|";
                    }
                }
            }

            if (childAge.EndsWith("|"))
            {
                childAge = childAge.Remove(childAge.Length - 1, 1);
            }


            
                SqlParameter[] paramList = new SqlParameter[7];

                paramList[0] = new SqlParameter("@P_HOTEL_ID", roomrequest.HotelCode);
                paramList[1] = new SqlParameter("@P_CheckIn_Date", DateTime.ParseExact(roomrequest.StartDate, "dd/MM/yyyy", null));
                paramList[2] = new SqlParameter("@P_CheckOut_Date", DateTime.ParseExact(roomrequest.EndDate, "dd/MM/yyyy", null));
                paramList[3] = new SqlParameter("@P_ROOM_PAX", roomPax);
                paramList[4] = new SqlParameter("@P_Child_Ages", childAge);
                //paramList[4] = new SqlParameter("@P_No_Of_Rooms", roomrequest.NoOfRooms);
                paramList[5] = new SqlParameter("@P_No_Of_Rooms", roomrequest.NoOfRooms);
		        paramList[6] = new SqlParameter("@P_Nationality", roomrequest.PassengerNationality);

                dtHotelRoomResults = DBGateway.FillDataTableSP("USP_GETROOMDETAILS_NEW", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "(CZInventory)Failed to get results. Error: " + ex.ToString(), "");
            }
            return dtHotelRoomResults;
        }

        public DataTable GetCancelPolicy(HotelCancelPolicyRequest cancelpolicyrequest)
        {
            DataTable dtCancelPolicy = new DataTable();
            try
            {
                IFormatProvider format = new CultureInfo("en-GB", true);
                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_Hotel_Id", cancelpolicyrequest.HotelCode);
                paramList[1] = new SqlParameter("@P_Room_Id", cancelpolicyrequest.RoomId);
                paramList[2] = new SqlParameter("@P_Period_Id", cancelpolicyrequest.PeriodId);
                paramList[3] = new SqlParameter("@P_Allocation_Type", cancelpolicyrequest.AllocationType);
                paramList[4] = new SqlParameter("@P_Sell_Type", cancelpolicyrequest.SellType);
                paramList[5] = new SqlParameter("@P_Market_Id", cancelpolicyrequest.MarketId);
                paramList[6] = new SqlParameter("@P_CHECK_IN", DateTime.Parse(cancelpolicyrequest.StartDate, format) );
                paramList[7] = new SqlParameter("@P_CHECK_OUT", DateTime.Parse(cancelpolicyrequest.EndDate, format) );
                paramList[8] = new SqlParameter("@P_Accom_Id", cancelpolicyrequest.AccomId);
                dtCancelPolicy = DBGateway.FillDataTableSP("P_HTL_CANCELLATION_POLICY_GET_NEW", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(CZInventory)Failed to get Cancel Policy for HotelCode. Error : " + ex.ToString(), "");
            }
            return dtCancelPolicy;
        }

        /// <summary>
        /// This method returns the search results for the Item Price Request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DataTable GetInventoryPriceRequest(PriceRequest pricerequest)
        {

            DataTable dtPriceCheck = new DataTable();
            string roomPax = "";
            string childAge = "";
            int order = 1;
            string roomId = "";

            Dictionary<int, string> Guests = new Dictionary<int, string>();
            try
            {
                foreach (RoomGuest guest in pricerequest.RoomDetail)
            {
                if (roomPax.Trim().Length > 0)
                {
                    roomPax += "|" + guest.NoOfAdults + "," + guest.NoOfChild;
                    Guests.Add(order, guest.NoOfAdults + "," + guest.NoOfChild);
                }
                else
                {
                    roomPax = guest.NoOfAdults + "," + guest.NoOfChild;
                    Guests.Add(order, guest.NoOfAdults + "," + guest.NoOfChild);
                }

                order++;
                if (guest.NoOfChild > 0)
                {
                    for (int i = 0; i < guest.ChildAge.Length; i++)
                    {
                        int age = guest.ChildAge[i];
                        if (childAge.Trim().Length > 0 && !childAge.EndsWith("|"))
                        {
                            childAge += "," + age;
                        }
                        else
                        {
                            childAge += age.ToString();
                        }
                    }
                    {
                        childAge += "|";
                    }
                }
                else
                {
                    if (childAge.Trim().Length > 0 && childAge.EndsWith("|"))
                    {
                        childAge += "0|";
                    }
                    else
                    {
                        childAge += "0|";
                    }
                }
                if (roomId.Trim().Length > 0)
                {
                    //roomId += "|" + guest.RoomId + "," + guest.RoomSeqNo;
					roomId += "|" + guest.RoomId + "," + guest.RoomSeqNo + "," + guest.AllocType + "," + guest.SellType + "," + guest.MarketId + "," + guest.AccomId;
                    }
                else
                {
                        //roomId = guest.RoomId + "," + guest.RoomSeqNo;
                        roomId = guest.RoomId + "," + guest.RoomSeqNo + "," + guest.AllocType + "," + guest.SellType + "," + guest.MarketId + "," + guest.AccomId; 
                }

            }

            if (childAge.EndsWith("|"))
            {
                childAge = childAge.Remove(childAge.Length - 1, 1);
            }


            
                SqlParameter[] paramList = new SqlParameter[7];

                paramList[0] = new SqlParameter("@P_HOTEL_ID", pricerequest.HotelCode);
                paramList[1] = new SqlParameter("@P_CheckIn_Date", DateTime.ParseExact(pricerequest.StartDate, "dd/MM/yyyy", null));
                paramList[2] = new SqlParameter("@P_CheckOut_Date", DateTime.ParseExact(pricerequest.EndDate, "dd/MM/yyyy", null));
                paramList[3] = new SqlParameter("@P_ROOM_PAX", roomPax);
                //paramList[4] = new SqlParameter("@P_No_Of_Rooms", pricerequest.NoOfRooms);
                paramList[4] = new SqlParameter("@P_Child_Ages", childAge);
                paramList[5] = new SqlParameter("@P_Room_Id", roomId);
		        paramList[6] = new SqlParameter("@P_Nationality", pricerequest.PassengerNationality);

                dtPriceCheck = DBGateway.FillDataTableSP("USP_ITEMPRICECHECK_NEW", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "(CZInventory)Failed to get results. Error: " + ex.ToString(), "");
            }
            return dtPriceCheck;
        }

        public string ConfirmBooking(BookingRequest hotelBooking)
        {
            SqlTransaction bookingTrans = null;
            string confirmationRef = string.Empty;
            int bookingId = 0;
            try
            {
                SqlConnection con = DBGateway.GetConnection();
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                bookingTrans = con.BeginTransaction();

                //*****************************************************************************************//
                //                  Generate and Save Confirmation number for the booking
                //*****************************************************************************************//

                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@P_ConfirmationRef", confirmationRef);
                paramList[0].Direction = System.Data.ParameterDirection.Output;
                paramList[0].Size = 50;
                paramList[1] = new SqlParameter("@P_RoomId", Convert.ToInt32(hotelBooking.RoomDetail[0].RoomReferenceKey.Split('-')[1]));
                paramList[2] = new SqlParameter("@P_PropertyId", Convert.ToInt32(hotelBooking.HotelCode));
                paramList[3] = new SqlParameter("@P_CreatedOn", DateTime.Now);
                paramList[4] = new SqlParameter("@P_CreatedBy", hotelBooking.CreatedBy);
                try
                {
                    int count = DBGateway.ExecuteNonQuerySP("P_HTL_CONFIRMATION_REFERENCE_ADD", paramList);

                    if (count > 0 && paramList[0].Value != null)
                    {
                        confirmationRef = paramList[0].Value.ToString();
                        hotelBooking.ConfirmationNo = confirmationRef;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed to Save Confirmation", ex);
                }

                //*****************************************************************************************//
                //                          Save the Booking detail header
                //*****************************************************************************************//

                paramList = new SqlParameter[28];
                paramList[0] = new SqlParameter("@P_BookingId", -1);
                paramList[1] = new SqlParameter("@P_ClientId", hotelBooking.HotelCode);
                paramList[2] = new SqlParameter("@P_AgentReference", hotelBooking.AgencyReference);
                paramList[3] = new SqlParameter("@P_ConfirmationNo", hotelBooking.ConfirmationNo);
                paramList[4] = new SqlParameter("@P_StartDate", DateTime.ParseExact(hotelBooking.StartDate, "dd/MM/yyyy", null));
                paramList[5] = new SqlParameter("@P_EndDate", DateTime.ParseExact(hotelBooking.EndDate, "dd/MM/yyyy", null));
                paramList[6] = new SqlParameter("@P_BookingStatus", hotelBooking.HotelBookingStatus);
                paramList[7] = new SqlParameter("@P_CityCode", (hotelBooking.CityCode != null ? hotelBooking.CityCode : ""));
                paramList[8] = new SqlParameter("@P_CountryCode", (hotelBooking.HotelPassenger.Countrycode != null ? hotelBooking.HotelPassenger.Countrycode : ""));
                paramList[9] = new SqlParameter("@P_PropertyId", hotelBooking.HotelCode);
                paramList[10] = new SqlParameter("@P_SpecialRequest", (hotelBooking.SpecialRequest != null ? hotelBooking.SpecialRequest : ""));
                paramList[11] = new SqlParameter("@P_FlightInformation", (hotelBooking.FlightInfo != null ? hotelBooking.FlightInfo : ""));
                paramList[12] = new SqlParameter("@P_PromoId", -1);
                paramList[13] = new SqlParameter("@P_CreatedOn", hotelBooking.CreatedOn);
                paramList[14] = new SqlParameter("@P_CreatedBy", hotelBooking.CreatedBy);
                if (hotelBooking.LastModifiedBy > 0)
                {
                    paramList[15] = new SqlParameter("@P_LastModifiedOn", hotelBooking.LastModifiedOn);
                    paramList[16] = new SqlParameter("@P_LastModifiedBy", hotelBooking.LastModifiedBy);
                }
                else
                {
                    paramList[15] = new SqlParameter("@P_LastModifiedOn", DBNull.Value);
                    paramList[16] = new SqlParameter("@P_LastModifiedBy", DBNull.Value);
                }

                paramList[17] = new SqlParameter("@P_FirstName", hotelBooking.HotelPassenger.Firstname);
                paramList[18] = new SqlParameter("@P_LastName", hotelBooking.HotelPassenger.Lastname);
                paramList[19] = new SqlParameter("@P_AddressLine1", hotelBooking.HotelPassenger.Addressline1);
                paramList[20] = new SqlParameter("@P_AddressLine2", hotelBooking.HotelPassenger.Addressline2);
                paramList[21] = new SqlParameter("@P_City", hotelBooking.HotelPassenger.City);
                paramList[22] = new SqlParameter("@P_State", (hotelBooking.HotelPassenger.State != null ? hotelBooking.HotelPassenger.State : ""));
                paramList[23] = new SqlParameter("@P_Country", hotelBooking.HotelPassenger.Country);
                paramList[24] = new SqlParameter("@P_PhoneNumber", hotelBooking.HotelPassenger.Phoneno);
                paramList[25] = new SqlParameter("@P_Email", hotelBooking.HotelPassenger.Email);
                paramList[26] = new SqlParameter("@P_Currency", hotelBooking.Currency);
                paramList[27] = new SqlParameter("@P_NewId", 0);
                paramList[27].Direction = System.Data.ParameterDirection.Output;
                int recCount = 0;
                try
                {
                    recCount = DBGateway.ExecuteNonQuerySP("P_HTL_BOOKING_CONFIRMATION_ADDUPDATE", paramList);
                }
                catch (Exception ex)
                {
                    throw new Exception("(CZInventory)Failed to Save Booking Header. Error : " + ex.Message, ex);
                }

                if (recCount > 0 && paramList[27].Value != null)
                {
                    bookingId = Convert.ToInt32(paramList[27].Value);

                    bool isSameRoom = false;
                    int firstRoom = Convert.ToInt32(hotelBooking.RoomDetail[0].RoomReferenceKey.Split('-')[1]);
                    try
                    {

                        //*************************************************************************
                        //                      Save Room Booking Information
                        //*************************************************************************

                        foreach (RoomGuest room in hotelBooking.RoomDetail)
                        {
                            //if (room.RoomId == firstRoom)
                            if (Convert.ToInt32(room.RoomReferenceKey.Split('-')[1]) == firstRoom)
                            {
                                isSameRoom = true;
                            }
                            else
                            {
                                isSameRoom = false;
                            }

                            string ChildAge = string.Empty;
                            if (room.ChildAge != null && room.ChildAge.Length > 0)
                            {
                                ChildAge = "";
                                foreach (int age in room.ChildAge)
                                {
                                    if (ChildAge.Length > 0)
                                    {
                                        ChildAge += "," + age.ToString();
                                    }
                                    else
                                    {
                                        ChildAge = age.ToString();
                                    }
                                }
                            }
                            else
                            {
                                ChildAge = "";
                            }
                            paramList = new SqlParameter[19];
                            paramList[0] = new SqlParameter("@P_BookingId", bookingId);
                            paramList[1] = new SqlParameter("@P_RoomConfirmationNo", confirmationRef);
                            paramList[2] = new SqlParameter("@P_RoomStatus", 0);
                            paramList[3] = new SqlParameter("@P_RoomId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[1]));
                            paramList[4] = new SqlParameter("@P_RoomCode", room.RoomReferenceKey);
                            string roomNameWithMealPlan = room.RoomName + "-" + room.MealPlan;
                            paramList[5] = new SqlParameter("@P_RoomName", roomNameWithMealPlan);
                            paramList[6] = new SqlParameter("@P_NoOfAdults", room.NoOfAdults);
                            paramList[7] = new SqlParameter("@P_NoOfChild", room.NoOfChild);
                            paramList[8] = new SqlParameter("@P_FirstName", room.PassenegerInfo[0].Firstname);
                            paramList[9] = new SqlParameter("@P_LastName", room.PassenegerInfo[0].Lastname);
                            paramList[10] = new SqlParameter("@P_CancellationDetails", hotelBooking.HotelCancelPolicy);
                            paramList[11] = new SqlParameter("@P_Price", room.SupplierPrice);
                            paramList[12] = new SqlParameter("@P_Tax", room.Tax);
                            paramList[13] = new SqlParameter("@P_RoomReferenceNo", Convert.ToInt32(room.RoomReferenceKey.Split('-')[1]));
                            paramList[14] = new SqlParameter("@P_ChildAge", ChildAge);
                            paramList[15] = new SqlParameter("@P_Alloc_Type", room.AllocType);
                            paramList[16] = new SqlParameter("@P_Sell_Type", room.SellType);
                            paramList[17] = new SqlParameter("@P_MarketId", room.MarketId);
                            paramList[18] = new SqlParameter("@P_RqId", room.RqId);
                            int count = DBGateway.ExecuteNonQuerySP("P_HTL_ROOM_BOOKING_INFORMATION_ADD_New", paramList);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Failed to Save Room Booking information", ex);
                    }

                    //****************************************************************
                    //                  Save Room Quota information
                    //****************************************************************

                    List<string> roomIds = new List<string>();
                    foreach (RoomGuest room in hotelBooking.RoomDetail)
                    {
						IFormatProvider format = new CultureInfo("en-GB", true);
                        string quota = string.Empty;
                        //If Same room booked then update Quota = No of rooms
                        if (isSameRoom)
                        {
                            string[] vals = room.RoomReferenceKey.Split('-');
                            //quota = vals[0] + "," + vals[1] + "," + vals[2] + "," + DateTime.ParseExact(hotelBooking.StartDate, "dd/MM/yyyy", null) + "," + DateTime.ParseExact(hotelBooking.EndDate, "dd/MM/yyyy", null) + "," + vals[3] + ",1";
							quota = vals[0] + "," + vals[1] + "," + vals[2] + "," + DateTime.Parse(hotelBooking.StartDate, format) + "," + DateTime.Parse(hotelBooking.EndDate, format) + "," + vals[3] + ",1"+ ","+room.AllocType+","+room.SellType+","+room.MarketId;
                            int sameRoomIndex = roomIds.FindIndex(delegate (string rq) { return Convert.ToInt32(rq.Split(',')[1]) == Convert.ToInt32(room.RoomReferenceKey.Split('-')[1]) && Convert.ToInt32(rq.Split(',')[0]) == Convert.ToInt32(room.RoomReferenceKey.Split('-')[0]) && Convert.ToInt32(rq.Split(',')[2]) == Convert.ToInt32(room.RoomReferenceKey.Split('-')[2]) && rq.Split(',')[7]== room.RoomReferenceKey.Split('-')[7]&& rq.Split(',')[8] == room.RoomReferenceKey.Split('-')[8] && Convert.ToInt32(rq.Split(',')[9]) == Convert.ToInt32(room.RoomReferenceKey.Split('-')[9]); });

                            if (sameRoomIndex >= 0)
                            {
                                int Quota = Convert.ToInt32(roomIds[sameRoomIndex].Split(',')[6]);
                                Quota++;
                                string[] ids = roomIds[sameRoomIndex].Split(',');
                                roomIds[sameRoomIndex] = ids[0] + "," + ids[1] + "," + ids[2] + "," + ids[3] + "," + ids[4] + "," + ids[5] + "," + Quota+","+ids[7]+","+ids[8]+","+ids[9];
                            }

                            //Add only unique rooms
                            if (!roomIds.Exists(delegate (string rq) { return Convert.ToInt32(rq.Split(',')[1]) == Convert.ToInt32(quota.Split(',')[1]) && Convert.ToInt32(rq.Split(',')[0]) == Convert.ToInt32(quota.Split(',')[0]) && Convert.ToInt32(rq.Split(',')[2]) == Convert.ToInt32(quota.Split(',')[2]) && Convert.ToInt32(rq.Split(',')[5]) == Convert.ToInt32(quota.Split(',')[5]) && Convert.ToInt32(rq.Split(',')[9]) == Convert.ToInt32(quota.Split(',')[9]); }))
                            {
                                roomIds.Add(quota);
                            }
                        }
                        else //Otherwise quota will be 1
                        {
                            string[] vals = room.RoomReferenceKey.Split('-');
                            //quota = vals[0] + "," + vals[1] + "," + vals[2] + "," + DateTime.ParseExact(hotelBooking.StartDate, "dd/MM/yyyy", null) + "," + DateTime.ParseExact(hotelBooking.EndDate, "dd/MM/yyyy", null) + "," + vals[3] + ",1";
							quota = vals[0] + "," + vals[1] + "," + vals[2] + "," + DateTime.Parse(hotelBooking.StartDate, format) + "," + DateTime.Parse(hotelBooking.EndDate, format) + "," + vals[3] + ",1" + "," + room.AllocType + "," + room.SellType+","+room.MarketId;

                            //Add only unique rooms
                            if (!roomIds.Exists(delegate (string rq) { return Convert.ToInt32(rq.Split(',')[1]) == Convert.ToInt32(quota.Split(',')[1]) && Convert.ToInt32(rq.Split(',')[0]) == Convert.ToInt32(quota.Split(',')[0]) && Convert.ToInt32(rq.Split(',')[2]) == Convert.ToInt32(quota.Split(',')[2]) && Convert.ToInt32(rq.Split(',')[5]) == Convert.ToInt32(quota.Split(',')[5]); }))
                            {
                                roomIds.Add(quota);
                            }
                        }
                    }
                    foreach (string rq in roomIds)
                    {
                        try
                        {
                            paramList = new SqlParameter[10];

                            paramList[0] = new SqlParameter("@P_RoomId", Convert.ToInt32(rq.Split(',')[1]));
                            paramList[1] = new SqlParameter("@P_HotelId", Convert.ToInt32(rq.Split(',')[0]));
                            paramList[2] = new SqlParameter("@P_PeriodId", Convert.ToInt32(rq.Split(',')[2]));
                            paramList[3] = new SqlParameter("@P_Start_Date",Convert.ToDateTime( rq.Split(',')[3]));
                            paramList[4] = new SqlParameter("@P_End_Date", Convert.ToDateTime(rq.Split(',')[4]));
                            paramList[5] = new SqlParameter("@P_AccomId", Convert.ToInt32(rq.Split(',')[5]));
                            paramList[6] = new SqlParameter("@P_QuotaUsed", Convert.ToInt32(rq.Split(',')[6]));
                            paramList[7] = new SqlParameter("@P_Alloc_Type", Convert.ToChar(rq.Split(',')[7]));
                            paramList[8] = new SqlParameter("@P_Sell_Type", Convert.ToString(rq.Split(',')[8]));
                            paramList[9] = new SqlParameter("@P_Market_id", Convert.ToInt32(rq.Split(',')[9]));

                            int rec = DBGateway.ExecuteNonQuerySP("P_HTL_UPDATE_ROOM_QUOTA_NEW", paramList);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Failed to Update Room Quota", ex);
                        }
                    }

                    //*******************************************************************
                    //                     Save Room Fare Breakdown
                    //*******************************************************************

                    try
                    {
                        decimal charge = 0; Int16 rateType = (Int16)FareType.Net;
                        foreach (RoomGuest room in hotelBooking.RoomDetail)
                        {
                            int days = DateTime.ParseExact(hotelBooking.EndDate, "dd/MM/yyyy", null).Subtract(DateTime.ParseExact(hotelBooking.StartDate, "dd/MM/yyyy", null)).Days;

                            //*******************************************************************************************
                            // If more than 2 days booked and more than 1 room booked then save room fare breakdown
                            // day wise by dividing the price with total days. In this scenario RoomFareBreakDown will be
                            // only one for each room
                            //*******************************************************************************************
                            if (days > 1 && hotelBooking.RoomDetail.Length > 1)
                            {
                                //for (int i = 0; i < days; i++)
                                //{
                                    foreach (HotelRoomFareBreakDown fare in room.RoomFareBreakDown)
                                    {
                                        paramList = new SqlParameter[14];
                                        paramList[0] = new SqlParameter("@P_BookingId", bookingId);
                                        paramList[1] = new SqlParameter("@P_RoomId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[1]));
                                        paramList[2] = new SqlParameter("@P_Date", DateTime.ParseExact(fare.Date, "dd/MM/yyyy", null));//  DateTime.ParseExact(hotelBooking.StartDate, "dd/MM/yyyy", null).AddDays(i));
                                        paramList[3] = new SqlParameter("@P_Price", (room.SupplierPrice) / days);
                                        paramList[4] = new SqlParameter("@P_Tax", (room.Tax / days));
                                        paramList[5] = new SqlParameter("@P_ExtraGuestCharge", charge);
                                        paramList[6] = new SqlParameter("@P_RateType", rateType);
                                        paramList[7] = new SqlParameter("@P_NetPercentage", charge);
                                        paramList[8] = new SqlParameter("@P_IncomingComm", charge);
                                        paramList[9] = new SqlParameter("@P_OutgoingComm", charge);
                                        paramList[10] = new SqlParameter("@P_ChildCharge", charge);
                                        paramList[11] = new SqlParameter("@P_AccomId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[3]));
                                        paramList[12] = new SqlParameter("@P_PeriodId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[2]));
                                        paramList[13] = new SqlParameter("@P_HotelId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[0]));

                                        int count = DBGateway.ExecuteNonQuerySP("P_HTL_BOOKING_ROOM_FARE_BREAK_DOWN_ADD", paramList);
                                    }
                                //}

                            }
                            else
                            {
                                //************************************************************************************
                                // If only one room booked for multiple days then RoomFareBreakDown will be there for
                                // each day i.e. 5th to 9th fare break down will be for 5th, 6th, 7th and 8th
                                //************************************************************************************
                                foreach (HotelRoomFareBreakDown fare in room.RoomFareBreakDown)
                                {
                                    paramList = new SqlParameter[14];
                                    paramList[0] = new SqlParameter("@P_BookingId", bookingId);
                                    paramList[1] = new SqlParameter("@P_RoomId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[1]));
                                    paramList[2] = new SqlParameter("@P_Date", DateTime.ParseExact(fare.Date, "dd/MM/yyyy", null));
                                    paramList[3] = new SqlParameter("@P_Price", (room.SupplierPrice) / days);  //(room.NetFare + room.Markup) / room.RoomFareBreakDown.Length);
                                    paramList[4] = new SqlParameter("@P_Tax", (room.Tax / room.RoomFareBreakDown.Length));
                                    paramList[5] = new SqlParameter("@P_ExtraGuestCharge", charge);
                                    paramList[6] = new SqlParameter("@P_RateType", rateType);
                                    paramList[7] = new SqlParameter("@P_NetPercentage", charge);
                                    paramList[8] = new SqlParameter("@P_IncomingComm", charge);
                                    paramList[9] = new SqlParameter("@P_OutgoingComm", charge);
                                    paramList[10] = new SqlParameter("@P_ChildCharge", charge);
                                    paramList[11] = new SqlParameter("@P_AccomId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[3]));
                                    paramList[12] = new SqlParameter("@P_PeriodId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[2]));
                                    paramList[13] = new SqlParameter("@P_HotelId", Convert.ToInt32(room.RoomReferenceKey.Split('-')[0]));

                                    int count = DBGateway.ExecuteNonQuerySP("P_HTL_BOOKING_ROOM_FARE_BREAK_DOWN_ADD", paramList);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Failed to Save Room Fare Breakdown", ex);
                    }
                }

                bookingTrans.Commit();


                #region Send Hotel Booking Status Mail
                try
                {

                    string from = string.Empty, mailTo = string.Empty, subject = string.Empty, message = string.Empty, eMailsTo = string.Empty;
                    int paxCount = 0;
                    List<string> toList = new List<string>();
                    Hashtable tempTable = new Hashtable();
                    AgentAppConfig clsAppCnf = new AgentAppConfig();
                    clsAppCnf.Source = "HIS";
					
					//string sLogoPath = "https://travtrolley.com/Uploads/AgentLogos/" + Convert.ToString(hotelBooking.CreatedBy).ImgFileName;
                    string sLogoPath = "https://ibyta.com/images/ibyta-logo.jpg";
                    //"https://travtrolley.com/Uploads/AgentLogos/" + Convert.ToString(hotelBooking.CreatedBy) + ".jpg";
					
                    List<AgentAppConfig> appConfigs = clsAppCnf.GetConfigDataBySource();

                    EmailParams clsParams = new EmailParams();
                    
                    from = ConfigurationManager.AppSettings["fromEmail"];
                    clsParams.FromEmail = from;
                    //DataTable dtEmail = Email.GetHotelVoucherEmail(hotelBooking.ConfirmationNo);
					DataTable dtEmail = Email.GetHotelVoucherEmail(hotelBooking.ConfirmationNo,"Inventory");
                    /*-----------------------------------------------
                              Sending Mail To Inventory Email
                     -----------------------------------------------*/
                    try
                    {
                        mailTo = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_CONFIRM_EMAIL").Select(y => y.AppValue).FirstOrDefault();
                        subject = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_CONFIRM_SUBJECT").Select(y => y.AppValue).FirstOrDefault();
                        message = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_CONFIRM_MESSAGE").Select(y => y.AppValue).FirstOrDefault();

                        toList.Add(mailTo);
                        clsParams.ToEmail = mailTo;
                        //if (ConfigurationManager.AppSettings["Bcc"] != null)
                        //{
                        //    clsParams.CCEmail = ConfigurationManager.AppSettings["Bcc"];
                        //}
                        clsParams.Subject = subject;
                        for (int i = 0; i < hotelBooking.RoomDetail.Length; i++)
                        {
                            paxCount += hotelBooking.RoomDetail[i].NoOfAdults + hotelBooking.RoomDetail[i].NoOfChild;
                        }

                        /*--------------------------------------------------------------------------------------------------------
                         *                                      Body content for mail
                         *                                    ------------------------- 
                         * \n Dear Guest, \n\n New Hotel Booking Confirmation No. %confNo% Hotel Name : %hotelName% Check In Date:  
                        %checkInDate%. Check Out Date:  %checkOutDate%. No. Od Rooms:  %rooms% No. Of Pax:  %pax% 
                         ---------------------------------------------------------------------------------------------------------*/




                    
                        if (dtEmail != null && dtEmail.Rows.Count > 0)
                        {
                            clsParams.EmailBody = Convert.ToString(dtEmail.Rows[0]["EmailBody"]).Replace("Supplier Amount:@TotalPrice", "").Replace("@NoOfGuests", Convert.ToString(paxCount)).Replace("@NoOfRooms", Convert.ToString(hotelBooking.NoOfRooms)).Replace("@ROOMDTLS", Convert.ToString(dtEmail.Rows[0]["RoomInfo"]));

                            Email.Send(clsParams);
                        }
                        //    tempTable.Add("confNo", "<b>" + hotelBooking.ConfirmationNo + "</b>");
                        //tempTable.Add("hotelName", "<b>" + hotelBooking.HotelName + "</b>");
                        //tempTable.Add("checkInDate", hotelBooking.StartDate.ToString());
                        //tempTable.Add("checkOutDate", hotelBooking.EndDate.ToString());
                        //tempTable.Add("rooms", hotelBooking.NoOfRooms);
                        //tempTable.Add("pax", paxCount);

                        //Email.Send(from, from, toList, subject, message, tempTable);
                    }
                    catch (Exception exInv)
                    {

                        Audit.Add(EventType.Email, Severity.Normal, 0, "Sending Failed To Inv Email. Error : " + exInv.Message, "");
                    }


                    /*-----------------------------------------------
                              Sending Mail To Supplier Email
                     ------------------------------------------------*/
					 
					 dtEmail = Email.GetHotelVoucherEmail(hotelBooking.ConfirmationNo, "Supplier");
					 
                    try
                    {
                       // toList.Clear();
                        subject = string.Empty;
                        message = string.Empty;
                        subject = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_SUPP_CONFIRM_SUBJECT").Select(y => y.AppValue).FirstOrDefault();
                        message = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_SUPP_CONFIRM_MESSAGE").Select(y => y.AppValue).FirstOrDefault();
                        try
                        {
                            //Connection();
                            hotelBooking.ChainCode = GetHotelSupplierEmails(Convert.ToInt32(hotelBooking.HotelCode));
                        }
                        catch { }

                        if (!string.IsNullOrEmpty(hotelBooking.ChainCode))
                        {
                            toList.AddRange(hotelBooking.ChainCode.Split(','));
                        }
                        tempTable.Clear();
                        clsParams = new EmailParams();
                        clsParams.FromEmail = from;
                        clsParams.ToEmail =hotelBooking.ChainCode;
                        clsParams.Subject = subject;
                        //tempTable.Add("confNo", "<b>" + hotelBooking.ConfirmationNo + "</b>");
                        //tempTable.Add("hotelName", "<b>" + hotelBooking.HotelName + "</b>");
                        //tempTable.Add("checkInDate", hotelBooking.StartDate.ToString());
                        //tempTable.Add("checkOutDate", hotelBooking.EndDate.ToString());
                        //tempTable.Add("rooms", hotelBooking.NoOfRooms);
                        //tempTable.Add("pax", paxCount);
                        decimal supplierprice = 0;
                        string childAges = "";
                        int childCount = hotelBooking.RoomDetail.Sum(x => x.NoOfChild);
                        int adultCount = hotelBooking.RoomDetail.Sum(x => x.NoOfAdults);
                        for (int i = 0; i < hotelBooking.RoomDetail.Length; i++)
                        {
                            supplierprice += hotelBooking.RoomDetail[i].SupplierPrice;
                            childAges += hotelBooking.RoomDetail[i].ChildAge != null && hotelBooking.RoomDetail[i].ChildAge.Length > 0 ?string.Join(",",hotelBooking.RoomDetail[i].ChildAge.Select(p=>p.ToString()).ToArray())+",": "";
                        }
                        childAges = !string.IsNullOrEmpty(childAges)&& childAges.Length>0?childAges.Replace(",", " years,").TrimEnd(','):"";
                        string TotalPax = Convert.ToString(paxCount) + "( " + Convert.ToString(adultCount) + " Adult(s)" + (childCount>0 ?","+Convert.ToString(childCount) + " children " + childAges:"")+")";
                        // tempTable.Add("supplierprice", supplierprice);
                        if (dtEmail != null && dtEmail.Rows.Count > 0)
                        {
                            //clsParams.EmailBody = Convert.ToString(dtEmail.Rows[0]["EmailBody"]).Replace("@TotalPrice", Convert.ToString(supplierprice)).Replace("@NoOfGuests", Convert.ToString(paxCount)).Replace("@NoOfRooms", Convert.ToString(hotelBooking.NoOfRooms)).Replace("@ROOMDTLS", Convert.ToString(dtEmail.Rows[0]["RoomInfo"]));
							
							clsParams.EmailBody = Convert.ToString(dtEmail.Rows[0]["EmailBody"]).Replace("@NoOfGuests", TotalPax).Replace("@NoOfRooms", Convert.ToString(hotelBooking.NoOfRooms)).Replace("@ROOMDTLS", Convert.ToString(dtEmail.Rows[0]["RoomInfo"])).Replace("@AgentLogo", sLogoPath);

                            Email.Send(clsParams);
                        }

                        //Email.Send(from, from, toList, subject, message, tempTable);
                    }
                    catch (Exception exSupp)
                    {
                        Audit.Add(EventType.Email, Severity.Normal, 0, "Sending Failed To Supplier Email. Error : " + exSupp.Message, "");

                    }

                    /*-----------------------------------------------
                            Saving Booking Status After Mailed
                     ------------------------------------------------*/
                    eMailsTo = mailTo + "," + hotelBooking.ChainCode;
                    AddBookingStatusMail(bookingId, hotelBooking.ConfirmationNo, BookingStatus.Booked, eMailsTo);

                }
                catch (Exception exMail)
                {
                    Audit.Add(EventType.Email, Severity.Normal, 0, "Error : " + exMail.Message, "");

                }
                #endregion


            }
            catch (Exception ex)
            {
                bookingTrans.Rollback();
                Audit.Add(EventType.Email, Severity.Normal, 0, "Error : " + ex.ToString(), "");
                throw ex;
            }
            return confirmationRef;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="confirmationNo"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        public DataTable CancelBooking(BookingCancelRequest bookcancel)
        {
            DataTable dtCancelPolicy = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_ConfNo", bookcancel.ConfirmationNo);

                dtCancelPolicy = DBGateway.FillDataTableSP("P_HTL_Cancel_Booking_New", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(CZInventory)Failed to cancel booking. Error : " + ex.ToString(), "");
            }

            return dtCancelPolicy;
        }

        //Getting Supplier Emails <06/05/2016>
        public static string GetHotelSupplierEmails(int hotelId)
        {
            string emailsTo = string.Empty;
            try
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@hotelId", hotelId);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP("P_HTL_GetHotelSupplierEmails", paramList, connection);
                if (dataReader.Read())
                {
                    emailsTo = dataReader["eMail1"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed To Retrieve Hotel Supplier Email. Error : " + ex.Message);
            }
            return emailsTo;
        }

        //Save and Update Booking Status After Mailed <05052016>
        public static void AddBookingStatusMail(int hotelId, string confNo, BookingStatus status, string eMailTo)
        {
            // DBGateway.ConnectionString = (ConfigurationSystem.HotelConnectConfig["ConnectionString"]);
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@hotelId", hotelId);
                paramList[1] = new SqlParameter("@confirmationNo", confNo);
                paramList[2] = new SqlParameter("@status", Convert.ToString(status));
                paramList[3] = new SqlParameter("@email", eMailTo);

                DBGateway.ExecuteNonQuerySP("P_HTL_AddUpdateBookingStatusEmails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Low, 0, "Failed To save Booking Status Mail. Error : " + ex.Message, "");
                throw new Exception("Failed To save Booking Status Mail. Error : " + ex.Message);
            }
        }

    }
}
