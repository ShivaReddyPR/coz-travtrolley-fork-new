﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.Models
{
    
    public struct Rooms
    {
        public int RoomId;
        public string RoomName;
        public int AccomId;
        public string Accomodation;
        public decimal Rate;
        public int PeriodId;
        public string ChildRateType;
        public decimal ExtraBedRate;
        public decimal ChildExtraBedRate;
        public string RoomChild1AgeRange;
        public string RoomChild2AgeRange;
        public string RoomChild3AgeRange;
        public int RoomNumber;
        public string[] Images;
        public int Adults;
        public int Childs;
        public decimal ChildRate1;
        public decimal ChildRate2;
        public decimal ChildRate3;
        public decimal ChildRate4;
        public decimal ChildRateApplied;
        public decimal TotalRate;
        public decimal[] RateDaywise;
        public decimal TaxRate;
        public string TaxType;
        public int NoOfAdult;
        public int NoOfChild;
       // public string WeekDay;
        public int QuotaUsed;
        public string[] Hotelfacilities;
        public string AllocationType;
        public string SellType;
        public string ExtraBed;
        public int Quota;
        public int MarketId;
        public int RqId;
    }

    public struct RoomTypeDetails
    {
        public int HotelId;
        public Rooms[] Rooms;
    }

    public class HotelRoomDetailsDto
    {
        public List<RoomTypeDetails> GetRoomResponse { get; set; }
    }

}
