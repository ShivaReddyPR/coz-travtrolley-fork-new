﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.Models
{
    public class CancelEmailResponse
    {
        public string Status { get; set; }
    }
    public class CancelEmailResponseDto
    {
        public List<CancelEmailResponse> HotelBookingCancelEmailResponse { get; set; }
    }

}
