﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.Models
{
    public class HotelSearchResult
    {

        public int HotelCode { get; set; }
        public string HotelName { get; set; }
        public decimal TotalRate { get; set; }
        public string Address { get; set; }
        public string StarLevel { get; set; }
        public string HotelMainImage { get; set; }
        public string PoliciesAndDisclaimer { get; set; }
        public string HotelDescriptions { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Currency { get; set; }
    }

    public class HotelSearchResultDto
    {
        public List<HotelSearchResult> HotelSearchResponse { get; set; }
    }

     
}
