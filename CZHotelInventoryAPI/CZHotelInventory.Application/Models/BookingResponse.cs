﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.Models
{
    public class BookingResponse
    {
        public string Status { get; set; }
        public string ConfirmationNo { get; set; }
    }

    public class BookingResponseDto
    {
        public List<BookingResponse> HotelBookingResponse { get; set; }
    }
}
