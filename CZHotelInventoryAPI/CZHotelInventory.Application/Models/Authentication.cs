﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.Models
{
    public class Authentication
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
