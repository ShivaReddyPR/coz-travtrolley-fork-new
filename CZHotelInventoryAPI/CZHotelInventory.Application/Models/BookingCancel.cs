﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.Models
{
    public class BookingCancel
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Status { get; set; }
    }

    public class BookingCancelDto
    {
        public List<BookingCancel> HotelBookingCancelResponse { get; set; }
    }
}
