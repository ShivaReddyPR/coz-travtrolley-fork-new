﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Net.Mail;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CZHotelInventory.Application.BusinessOps;
using System.Data;
using System.Data.SqlClient;
using System.Net;

namespace CZHotelInventory.Application.Models
{
    public class EmailParams
    {
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string CCEmail { get; set; }
        public string BCCEmail { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }
        public string[] Attachments { get; set; }
        public Hashtable varMapping { get; set; }
    }

    public class Email
    {
        static System.Net.Mail.SmtpClient smtp;
        public Email()
        {
        }

        /// <summary>
        /// Static method sens email to the recipient
        /// </summary>
        /// <param name="from">Who sands the email</param>
        /// <param name="replyTo">On which id he wants reply</param>
        /// <param name="toArray">Recipients List</param>
        /// <param name="subject">subject</param>
        /// <param name="message">Message in hml format</param>
        /// <param name="varMapping">Hash table contains variable which canbe replaced in message</param>
        public static void Send(string from, string replyTo, List<string> toArray, string subject, string message, Hashtable varMapping)
        {
            try
            {
                string commaSeperatedValues = string.Empty;
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                smtp = new System.Net.Mail.SmtpClient();
                mail.Subject = subject + (System.Configuration.ConfigurationManager.AppSettings["TestMode"].Equals("True") ? "(Test)" : string.Empty);

                foreach (string s in toArray)
                {
                    mail.To.Add(s);
                }

                mail.ReplyToList.Add(replyTo);
                mail.From = new MailAddress(from, "ibyta");
                mail.IsBodyHtml = true;
                //These values are taken from a new config file HostPort.config
                //ConfigurationSystem con = new ConfigurationSystem();
                //Hashtable hostPort = con.GetHostPort();
                //smtp.Host = "smtp.gmail.com";
                //smtp.Port = 587;
                //smtp.EnableSsl = true;// for gmail
                //smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["gmailUser"], ConfigurationManager.AppSettings["gmailPwd"]);
                //if (hostPort["Bcc"] != null && hostPort["Bcc"].ToString().Trim() != string.Empty)
                //{
                //    mail.Bcc.Add(hostPort["Bcc"].ToString());
                //}
                if (ConfigurationManager.AppSettings["Bcc"] != null)
                {
                    mail.Bcc.Add(ConfigurationManager.AppSettings["Bcc"]);
                }

                if (varMapping != null)
                {
                    foreach (DictionaryEntry de in varMapping)
                    {
                        string keyString = "%" + de.Key.ToString() + "%";
                        if (de.Value != null)
                        {
                            message = message.Replace(keyString, de.Value.ToString());
                        }
                        else
                        {
                            message = message.Replace(keyString, " ");
                        }
                    }
                }
                message = message.Replace("\\n", "<br/>");
                mail.Body = message;
                try
                {
                    //Audit.Add(EventType.Email, Severity.Normal, 0, "smptpHost: " + smtp.Host, "");
                    smtp.Send(mail);
                }
                catch (SmtpException exSmtp)
                {
                    //Audit.Add(EventType.Email, Severity.Normal, 0, "smptp Error: " + exSmtp.ToString(), "");
                    throw new SmtpException("Smtp is unable to send the message");
                }
                catch (Exception ex)
                {
                    //Audit.Add(EventType.Email, Severity.Normal, 0, "Execption " + ex.ToString(), "");
                }
            }
            catch { throw; }
        }
        public static DataTable GetHotelVoucherEmail(string sConfirmation,string EmailType)
        {   
            try
            {               
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@BookRefNo", sConfirmation);
                paramList[1] = new SqlParameter("@Emailtype", EmailType);
                return DBGateway.FillDataTableSP("usp_GetHotelVoucherEmail", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To send email based on EmailParams object data
        /// </summary>
        /// <param name="clsParams"></param>
        public static void Send(EmailParams clsParams)
        {
            string commaSeperatedValues = string.Empty;
            MailMessage mail = new MailMessage();
            smtp = new SmtpClient();

            mail.Subject = clsParams.Subject + (System.Configuration.ConfigurationManager.AppSettings["TestMode"].Equals("True") ? "(Test)" : string.Empty);

            if (!string.IsNullOrEmpty(clsParams.BCCEmail))
                mail.Bcc.Add(clsParams.BCCEmail);

            mail.From = new MailAddress(clsParams.FromEmail, "ibyta");
            mail.To.Add(!string.IsNullOrEmpty(clsParams.ToEmail) ? clsParams.ToEmail : string.Empty);
            //if (!string.IsNullOrEmpty(clsParams.CCEmail))
            //    mail.CC.Add(clsParams.CCEmail);

            //ConfigurationSystem con = new ConfigurationSystem();
            //smtp.Port = 587;
            //smtp.EnableSsl = true;// for gmail
            //smtp.Host = "smtp.gmail.com";
            mail.Bcc.Add(!string.IsNullOrEmpty(clsParams.BCCEmail) ? clsParams.BCCEmail : !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Bcc"]) ?
                ConfigurationManager.AppSettings["Bcc"] : string.Empty);

            mail.IsBodyHtml = true;

            if (clsParams.varMapping != null)
            {
                foreach (DictionaryEntry de in clsParams.varMapping)
                {
                    string keyString = "%" + de.Key.ToString() + "%";
                    clsParams.EmailBody = clsParams.EmailBody.Replace(keyString, de.Value.ToString());
                }
            }

            mail.Body = clsParams.EmailBody.Replace("\\n", "<br/>");

            if (clsParams.Attachments != null && clsParams.Attachments.Length > 0)
                clsParams.Attachments.ToList().ForEach(x => { try { mail.Attachments.Add(new Attachment(x)); } catch { } });

            try
            {
              // smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["gmailUser"], ConfigurationManager.AppSettings["gmailPwd"]);
                smtp.Send(mail);
            }
            catch (SmtpException ex)
            {
                throw new SmtpException("Smtp is unable to send the message:" + ex.Message);
            }
        }
        public static DataTable GetBookingCancelEmail(string sConfirmation,string flag)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@BookRefNo", sConfirmation);
                paramList[1] = new SqlParameter("@FLAG", flag);
                return DBGateway.FillDataTableSP("USP_GETHTLCNLRQSTMAIL", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
