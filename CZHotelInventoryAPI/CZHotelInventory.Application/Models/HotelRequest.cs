﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.Models
{

    public class HotelRequest
    {
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
        public int Radius { get; set; }
        public int CityId { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CityCode { get; set; }
        public int NoOfRooms { get; set; }
        public RoomGuest[] RoomGuestData { get; set; }
        public string HotelName { get; set; }
        public HotelRating Rating { get; set; }
        public int ExtraCots { get; set; }
        public string Currency { get; set; }
        public AvailabilityType AvailabilityType { get; set; }
        public string[] Sources;
        public bool IsDomestic { get; set; }
        public bool SearchByArea { get; set; }
        public bool IsMultiRoom { get; set; }
        public int MinRating { get; set; }
        public int MaxRating { get; set; }
        public HotelRequestMode RequestMode { get; set; }
        public string PassengerNationality { get; set; }
        public RoomRateType RoomRateType { get; set; }
        //public List<string> HotelId { get; set; }
        public int[] HotelId;
    }

    public enum HotelRating
    {
        /// <summary>
        /// All
        /// </summary>
        All = 0,
        /// <summary>
        /// OneStar
        /// </summary>
        OneStar = 1,
        /// <summary>
        /// TwoStar
        /// </summary>
        TwoStar = 2,
        /// <summary>
        /// ThreeStar
        /// </summary>
        ThreeStar = 3,
        /// <summary>
        /// FourStar
        /// </summary>
        FourStar = 4,
        /// <summary>
        /// FiveStar
        /// </summary>
        FiveStar = 5
    }
    
    public enum AvailabilityType
    {
        /// <summary>
        /// OnRequest
        /// </summary>
        OnRequest = 1,
        /// <summary>
        /// Confirmed
        /// </summary>
        Confirmed = 2
    }

    public enum HotelRequestMode
    {
        B2B = 1,
        BookingAPI = 2,
        WhiteLabel = 3
    }

    /// <summary>
    /// Specifies which type of rates 
    /// (or suppliers - if the request is sent with noPrice set to true) 
    /// are taken into consideration by the system
    /// </summary>
    public enum RoomRateType
    {
        /// <summary>
        /// DOTW
        /// </summary>
        DOTW = 1,
        /// <summary>
        /// DYNAMIC_DIRECT
        /// </summary>
        DYNAMIC_DIRECT = 2,
        /// <summary>
        /// DYNAMIC_3rd_PARTY
        /// </summary>
        DYNAMIC_3rd_PARTY = 3
    }

        //testing for hotel booking
    public class RoomGuest
    {
        public int NoOfAdults;
        public int NoOfChild;
        public int[] ChildAge;
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public string MealPlan { get; set; }
        public bool ExtraBed { get; set; }
        public int MaxExtraBeds { get; set; }
        public int NoOfCots { get; set; }
        public int PeriodId { get; set; }
        public int AccomId { get; set; }
        public decimal SellingFare { get; set; }
        public bool SharingBed { get; set; }
        public decimal SupplierPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Tax { get; set; }
        public decimal NetFare { get; set; }
        public decimal Markup { get; set; }
        public List<HotelPassenger> PassenegerInfo { get; set; }
        public HotelRoomFareBreakDown[] RoomFareBreakDown { get; set; }
        public string AllocType { get; set; }
        public string SellType { get; set; }
        public string RoomReferenceKey { get; set; }
        public int RoomSeqNo { get; set; }
        public int MarketId { get; set; }
        public int RqId { get; set; }
    }

    public class HotelRoomFareBreakDown
    {
        public int RoomId { get; set; }
        public string Date { get; set; }
        public decimal RoomPrice { get; set; }
    }

    public class HotelPassenger
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Addressline1 { get; set; }
        public string Addressline2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Phoneno { get; set; }
        public string Email { get; set; }
        public string Countrycode { get; set; }
    }

    public class HotelRoomRequest
    {
        public int HotelCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public RoomGuest[] RoomGuestData { get; set; }
        public int NoOfRooms { get; set; }
        public string PassengerNationality;
    }

    public class HotelCancelPolicyRequest
    {
        public int HotelCode { get; set; }
        public int RoomId { get; set; }
        public int PeriodId { get; set; }
        public string AllocationType { get; set; }
        public string SellType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string MarketId { get; set; }
        public int AccomId { get; set; }
    }

    public class PriceRequest
    {
        public int HotelCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public RoomGuest[] RoomDetail { get; set; }
        public int NoOfRooms { get; set; }
        public string PassengerNationality;
    }

    public class BookingRequest
    {
        public string HotelName { get; set; }
        public int BookingSource { get; set; }
        public int HotelCode { get; set; }
        public string CityCode { get; set; }
        public string AgencyReference { get; set; }
        public int HotelBookingStatus { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal Price { get; set; }
        public string ConfirmationNo { get; set; }
        public string SpecialRequest { get; set; }
        public string FlightInfo { get; set; }
        public int CreatedBy { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public string Currency { get; set; }
        public RoomGuest[] RoomDetail { get; set; }
        public HotelPassenger HotelPassenger { get; set; }
        public string HotelCancelPolicy { get; set; }
        public int NoOfRooms { get; set; }
        public string ChainCode { get; set; }
    }

    public class BookingCancelRequest
    {
        public string ConfirmationNo { get; set; }
    }

}
