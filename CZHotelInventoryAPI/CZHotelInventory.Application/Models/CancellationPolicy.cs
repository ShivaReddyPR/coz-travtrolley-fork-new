﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.Models
{
    public struct CancelPolicies
    {
        public bool Refundable { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ChargeType { get; set; }
        public int ChargeValue { get; set; }
        public int BufferDays { get; set; }//newly added on 04/12/2020
        public string policy { get; set; }
        public string DeadlineDate { get; set; }
        public int RoomNo { get; set; }
        public decimal Price { get; set; }
    }

    public struct CancellationPolicies
    {
        public string HotelNorms { get; set; }
        public List<CancelPolicies> CancellationPolicy;
    }
    public class HotelCancellationPolicyDto
    {
        public List<CancellationPolicies> CancellationPolicyResponse { get; set; }
    }
}
