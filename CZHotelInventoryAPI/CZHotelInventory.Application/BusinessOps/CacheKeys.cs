using System;
using System.Collections.Generic;
using System.Text;

namespace CZHotelInventory.Application.BusinessOps
{
    public static class MemCacheKeys
    {
        public const string AuditConfig = "mck_AuditConfig";       
        public const string HotelConnectConfig = "mck_HotelConnectConfig";        
    }
}
