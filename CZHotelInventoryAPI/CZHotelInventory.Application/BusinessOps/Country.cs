﻿using CZHotelInventory.Application.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CZHotelInventory.Application.BusinessOps
{
    class Country
    {
        public static string GetCountryCodeFromCountryName(string countryName)
        {
            string country = string.Empty;
            ////Trace.TraceInformation("Country.GetCountryCodeFromCountryName(string countryName) entered: countryName=" + countryName);
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@countryName", countryName);
                paramList[1] = new SqlParameter("@countryCode", System.Data.SqlDbType.VarChar, 5);
                paramList[1].Direction = System.Data.ParameterDirection.Output;
                DBGateway.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["dbSetting"];
                DBGateway.ExecuteNonQuerySP("usp_GetCountryCodeFromCountryName", paramList);
                ////Trace.TraceInformation("Country.GetCountryCodeFromCountryName(string countryName) exited: countryName=" + countryName);
                if (paramList[1].Value != DBNull.Value)
                {
                    country = Convert.ToString(paramList[1].Value);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "Country.GetCountryCodeFromCountryName() Err :" + ex.ToString(), "0");
            }
            return country;
        }
    }
}
