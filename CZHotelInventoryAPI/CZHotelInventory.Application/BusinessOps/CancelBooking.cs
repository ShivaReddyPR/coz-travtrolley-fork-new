﻿using CZHotelInventory.Api.Models;
using CZHotelInventory.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using System.Collections;
using System.Configuration;

namespace CZHotelInventory.Application.BusinessOps
{
    public class CancelBooking
    {
        /// <summary>
        /// Query request to get Hotel Search Result
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public BookingCancelRequest HotelBookingCancelRequest { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="CancelBooking"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {

                    List<BookingCancel> hotelbookingcancel = new List<BookingCancel>();

                    InvBooking booking = new InvBooking();
                    IFormatProvider format = new System.Globalization.CultureInfo("en-GB");
                    int durationHours = 0;
                    string hotelNorms = "";
                    Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
                  //  SentCancelEmail(clsQuery.HotelBookingCancelRequest.ConfirmationNo, "REQUEST");
                    DataTable dtCancelPolicy = booking.CancelBooking(clsQuery.HotelBookingCancelRequest);

                    List<CancelPolicies> cancelPolicyList = new List<CancelPolicies>();
                    if (dtCancelPolicy != null && dtCancelPolicy.Rows.Count > 0)
                    {
                        CancellationPolicies cancellationpolicies = new CancellationPolicies();
                        int minnights = Convert.ToInt32(dtCancelPolicy.Rows[0]["minnights"]);
                        
                        for (int i = 0; i < dtCancelPolicy.Rows.Count; i++)
                        {
                            DataRow row = dtCancelPolicy.Rows[i];
                            string[] hours = row["cnx_before_hours"].ToString().Split(',');
                            string[] chargeTypes = row["cnx_charge_type"].ToString().Split(',');
                            string[] chargeValues = row["cnx_charge_value"].ToString().Split(',');
                            bool refundable = true;
                            if (row["policyStatus"].ToString().Equals("N"))
                            {
                                refundable = false;
                            }
                            CancelPolicies cancelpolicies;
                            if (refundable)
                            {
                                int j = 0;
                                for (j = 0; j < hours.Length; j++)
                                {
                                    //i = j;
                                    cancelpolicies = new CancelPolicies();
                                    //string format = "dd-MM-yyyy";                                   
                                    durationHours = Convert.ToInt32(hours[j]);                                   
                                    cancelpolicies.Refundable = true;
                                    cancelpolicies.StartDate = DateTime.Parse(Convert.ToString(row["fromdate"]), format).ToString("dd-MM-yyyy");//Convert.ToDateTime( row["fromdate"]).ToString("dd-MM-yyyy");  //startdate.ToString("dd-MM-yyyy");
                                    cancelpolicies.ChargeValue = Convert.ToInt32(chargeValues[j]);
                                    cancelpolicies.BufferDays = Convert.ToInt32(Math.Round(Convert.ToDecimal(durationHours) / 24));                       
                                    cancelpolicies.DeadlineDate = DateTime.Parse(Convert.ToString(row["fromdate"]), format).AddDays(-cancelpolicies.BufferDays).ToString("dd-MM-yyyy");
                                    cancelpolicies.ChargeType = chargeTypes[j];
                                    cancelpolicies.RoomNo= Convert.ToInt32(row["roomno"]);
                                    cancelpolicies.Price = Convert.ToDecimal(row["price"]);
                                    if (cancelPolicyList.Any(x => x.DeadlineDate == cancelpolicies.DeadlineDate && x.ChargeValue <= cancelpolicies.ChargeValue))
                                    {
                                        cancelPolicyList.RemoveAt(cancelPolicyList.FindIndex(c => c.DeadlineDate == cancelpolicies.DeadlineDate));
                                    }
                                    if (cancelPolicyList.Any(x => x.DeadlineDate == cancelpolicies.DeadlineDate && x.ChargeValue >= cancelpolicies.ChargeValue) || DateTime.Parse(cancelpolicies.DeadlineDate, format) == DateTime.Parse(dtCancelPolicy.Rows[0]["enddate"].ToString(), format).AddDays(-1) || cancelPolicyList.Any(x => x.ChargeType == cancelpolicies.ChargeType && x.ChargeValue == cancelpolicies.ChargeValue) || cancelPolicyList.Any(x => x.ChargeValue == 100))
                                    {
                                        continue;
                                    }
                                    // DateTime enddate = DateTime.ParseExact(@clsQuery.CancelPolicyRequest.StartDate, @"d/M/yyyy", CultureInfo.InvariantCulture).AddHours(durationHours);
                                    //DateTime enddate = DateTime.Parse(clsQuery.CancelPolicyRequest.StartDate).AddHours(-durationHours);
                                    cancelpolicies.EndDate = DateTime.Parse(Convert.ToString(row["todate"]), format).ToString("dd-MM-yyyy"); // Convert.ToDateTime(row["todate"]).ToString("dd-MM-yyyy");// enddate.ToString("dd-MM-yyyy");                                    
                                    cancelPolicyList.Add(cancelpolicies);
                                }
                                string leastdate = cancelPolicyList.Find(x => x.ChargeValue == 100).DeadlineDate;
                                if (!string.IsNullOrEmpty(leastdate) && cancelPolicyList.Any(x => DateTime.Parse(x.DeadlineDate, format) > DateTime.Parse(leastdate, format)))
                                {
                                    cancelPolicyList.RemoveAll(x => DateTime.Parse(x.DeadlineDate, format) > DateTime.Parse(leastdate, format));
                                }
                            }
                            else
                            {
                                cancelpolicies = new CancelPolicies();
                                cancelpolicies.Refundable = false;
                                cancelpolicies.RoomNo = Convert.ToInt32(row["roomno"]);
                                cancelpolicies.Price = Convert.ToDecimal(row["price"]);
                                cancelpolicies.policy = "Non-refundable and full charge will apply once booking is completed.";
                                cancelPolicyList.Add(cancelpolicies);
                            }
                        }
                    //    cancelPolicyList = cancelPolicyList.OrderBy( x =>DateTime.Parse(x.DeadlineDate,format)).ToList();
                        cancellationpolicies.CancellationPolicy = cancelPolicyList; 
                        var lstCancel=  cancelPolicyList.GroupBy(x => x.RoomNo);
                        decimal cnclAmount = 0;
                        foreach (var cancllst in lstCancel )
                        {
                          if( cancllst.Any(x=>!string.IsNullOrEmpty(x.policy) && x.policy.Length>0))
                            {
                                cnclAmount += cancllst.FirstOrDefault().Price;
                            }
                            else { 
                            var query = from d in cancllst
                                        let val = Math.Abs(Convert.ToDouble((DateTime.Parse(d.DeadlineDate, format) - DateTime.Today).TotalMinutes.ToString())) 
                                        select d;
                                CancelPolicies canceledPolicy = DateTime.Parse(query.FirstOrDefault().DeadlineDate, format) <= DateTime.Today ? query.FirstOrDefault() : new CancelPolicies();
                                if(canceledPolicy.Price!=0)
                                { 
                                decimal onenightAmount = canceledPolicy.Price / minnights;
                                cnclAmount += canceledPolicy.ChargeType == "N" ? onenightAmount *canceledPolicy.ChargeValue : (canceledPolicy.Price * canceledPolicy.ChargeValue) / 100;
                                }
                                else
                                {
                                    cnclAmount += 0;
                                }
                            }
                        }                       
                        Audit.Add(EventType.Exception, Severity.High, 0, "After  Non refund loop" + Convert.ToString(cancellationpolicies.CancellationPolicy.Count()), "");
                        cancellationCharges["Amount"] = cnclAmount.ToString();
                        cancellationCharges.Add("Status", "Cancelled");
                        cancellationCharges.Add("Currency", dtCancelPolicy.Rows[0]["currency"].ToString());
                    }
                    //if (dtBookingCancel != null && dtBookingCancel.Rows.Count > 0)
                    //{
                    //    int rows = dtBookingCancel.Rows.Count; int durationHours = 0;
                    //    cancellationCharges.Add("Currency", dtBookingCancel.Rows[0]["currency"].ToString());
                    //    for (int j = 0; j < dtBookingCancel.Rows.Count; j++)
                    //    {
                    //        DataRow row = dtBookingCancel.Rows[j];
                    //        string[] hours = row["cnx_before_hours"].ToString().Split(',');
                    //        string[] chargeTypes = row["cnx_charge_type"].ToString().Split(',');
                    //        string[] chargeValues = row["cnx_charge_value"].ToString().Split(',');
                    //        decimal dayRate = Math.Round(Convert.ToDecimal(row["Price"]), 2);
                    //        DateTime checkInDate = Convert.ToDateTime(row["StartDate"]);
                    //        decimal fare = Math.Round(Convert.ToDecimal(row["TotalPrice"]), 2);
                    //        int rooms = Convert.ToInt32(row["Rooms"]);
                    //        string cnxStatus = row["RPM_CNX_Status"].ToString();
                    //        DateTime cnx_from_date = Convert.ToDateTime(row["cnx_from_date"]);
                    //        DateTime cnx_to_date = Convert.ToDateTime(row["cnx_to_date"]);
                    //        for (int i = 0; i < hours.Length; i++)
                    //        {
                    //        if (cnxStatus != "N")
                    //        {
                    //                durationHours = Convert.ToInt32(hours[i]);

                    //            if (checkInDate.Subtract(DateTime.Now).TotalDays * 24 <= Convert.ToInt32(hours[i]))
                    //            {
                    //                if (chargeTypes[i] == "N")
                    //                {
                    //                    if (rooms > rows)//If Same room and Same Price for all rooms, one row will come
                    //                    {
                    //                        if (cancellationCharges.ContainsKey("Amount"))
                    //                        {
                    //                            decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                    //                            decimal roomRate = dayRate * Convert.ToInt32(chargeValues[i]);
                    //                            roomRate = roomRate * rooms;
                    //                            amount += roomRate;
                    //                            cancellationCharges["Amount"] = amount.ToString();
                    //                        }
                    //                        else
                    //                        {
                    //                            decimal roomRate = dayRate * Convert.ToInt32(chargeValues[i]);
                    //                            roomRate = roomRate * rooms;
                    //                            cancellationCharges.Add("Amount", (roomRate).ToString());
                    //                        }
                    //                    }
                    //                    else //if(rooms == rows)//If Same room and different price, three rows will return
                    //                    {
                    //                        if (cancellationCharges.ContainsKey("Amount"))
                    //                        {
                    //                            decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                    //                            amount += (dayRate * Convert.ToInt32(chargeValues[i]));
                    //                            cancellationCharges["Amount"] = amount.ToString();
                    //                        }
                    //                        else
                    //                        {
                    //                            cancellationCharges.Add("Amount", (dayRate * Convert.ToInt32(chargeValues[i])).ToString());
                    //                        }
                    //                    }
                    //                }
                    //                    //else if (chargeValues[i] == "100" && chargeTypes[i] == "P" && DateTime.Now == checkInDate)
                    //                    else if (chargeValues[i] == "100" && chargeTypes[i] == "P" && hours[i] == "0")
                    //                    {
                    //                    if (cancellationCharges.ContainsKey("Amount"))
                    //                    {
                    //                        decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                    //                        amount += fare;
                    //                        cancellationCharges["Amount"] = amount.ToString();
                    //                    }
                    //                    else
                    //                    {
                    //                        cancellationCharges.Add("Amount", fare.ToString());
                    //                    }
                    //                }
                    //                else if (chargeTypes[i] == "P")
                    //                {
                    //                    if (cancellationCharges.ContainsKey("Amount"))
                    //                    {
                    //                        decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);

                    //                        decimal charge = ((fare / rooms) * Convert.ToInt32(chargeValues[i]) / 100);
                    //                        charge = charge * rooms;
                    //                        amount += charge;
                    //                        cancellationCharges["Amount"] = amount.ToString();
                    //                    }
                    //                    else
                    //                    {
                    //                        decimal charge = ((fare / rooms) * Convert.ToInt32(chargeValues[i]) / 100);
                    //                        charge = charge * rooms;
                    //                        cancellationCharges.Add("Amount", (charge).ToString());
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    if (cancellationCharges.ContainsKey("Amount"))
                    //                    {
                    //                        decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                    //                        amount += fare;
                    //                        cancellationCharges["Amount"] = amount.ToString();
                    //                    }
                    //                    else
                    //                    {
                    //                        cancellationCharges.Add("Amount", fare.ToString());
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                    if (chargeValues[i] == "100" && chargeTypes[i] == "P" && hours[i] == "0")
                    //                    {
                    //                        if (cancellationCharges.ContainsKey("Amount"))
                    //                        {
                    //                            decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                    //                            amount += fare;
                    //                            cancellationCharges["Amount"] = amount.ToString();
                    //                        }
                    //                        else
                    //                        {
                    //                            cancellationCharges.Add("Amount", fare.ToString());
                    //                        }
                    //                    }
                    //                    {
                    //                        {
                    //                             if (cancellationCharges.ContainsKey("Amount"))
                    //                        {
                    //                            decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                    //                            amount += fare;
                    //                            cancellationCharges["Amount"] = amount.ToString();
                    //                        }
                    //                        else
                    //                        {
                    //                            cancellationCharges.Add("Amount", "0");
                    //                        }
                    //                        }
                    //                    }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (cancellationCharges.ContainsKey("Amount"))
                    //            {
                    //                decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                    //                amount += fare;
                    //                cancellationCharges["Amount"] = amount.ToString();
                    //            }
                    //            else
                    //            {
                    //                cancellationCharges.Add("Amount", fare.ToString());
                    //            }
                    //        }
                    //    }

                    //    }
                    //    cancellationCharges.Add("Status", "Cancelled");
                    //}

                    BookingCancel bookingCancel = new BookingCancel();

                    bookingCancel.Amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                    bookingCancel.Status = cancellationCharges["Status"];
                    if(bookingCancel.Status=="Cancelled")
                        SentCancelEmail(clsQuery.HotelBookingCancelRequest.ConfirmationNo, "REFUND");
                    bookingCancel.Currency = cancellationCharges["Currency"];      
                    hotelbookingcancel.Add(bookingCancel);

                    var newdata = hotelbookingcancel;
                    var viewModel = new ViewModel(newdata);
                    return await System.Threading.Tasks.Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Get,Err:" + ex.Message, "");
                    return new ApplicationResponse().AddError("BookingCencelResponseException", ex.ToString());
                }

            }
            private void SentCancelEmail(string confirmationNo,string flag)
            {
                try
                {
                    string from = string.Empty, mailTo = string.Empty, subject = string.Empty, message = string.Empty, eMailsTo = string.Empty;
                    List<string> toList = new List<string>();
                    Hashtable tempTable = new Hashtable();
                    AgentAppConfig clsAppCnf = new AgentAppConfig();
                    clsAppCnf.Source = "HIS";
					
					//string sLogoPath = "https://travtrolley.com/Uploads/AgentLogos/" + Convert.ToString(hotelBooking.CreatedBy).ImgFileName;
                    string sLogoPath = "https://ibyta.com/images/ibyta-logo.jpg";
					
                    List<AgentAppConfig> appConfigs = clsAppCnf.GetConfigDataBySource();

                    EmailParams clsParams = new EmailParams();

                    from = ConfigurationManager.AppSettings["fromEmail"];
                    clsParams.FromEmail = from;
                    DataTable dtEmail = Email.GetBookingCancelEmail(confirmationNo, flag);

                    mailTo = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_CANCEL_EMAIL").Select(y => y.AppValue).FirstOrDefault();
					subject = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_SUPP_CANCEL_SUBJECT").Select(y => y.AppValue).FirstOrDefault();
                    //subject = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_Supp_Cancel_Subject").Select(y => y.AppValue).FirstOrDefault();

                    toList.Add(mailTo);
                    clsParams.ToEmail = mailTo;//toList[0];
                    clsParams.CCEmail = mailTo;
                    clsParams.Subject = subject;

                    if (dtEmail != null && dtEmail.Rows.Count > 0)
                    {
                        //clsParams.EmailBody = Convert.ToString(dtEmail.Rows[0]["EmailBody"]);
						clsParams.EmailBody = flag == "REFUND" ? Convert.ToString(dtEmail.Rows[0]["EmailBody"]) : Convert.ToString(dtEmail.Rows[0]["EmailBody"]).Replace("@ROOMDTLS", Convert.ToString(dtEmail.Rows[0]["RoomInfo"])).Replace("@AgentLogo", sLogoPath);

                        Email.Send(clsParams);
                    }
                }
                catch (Exception ex)
                {

                    Audit.Add(EventType.Exception, Severity.Low, 0, "Error at SentCancelEmail() for cancel" + flag + "reason is:" + ex.ToString(),"");
                }
            }

        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(List<BookingCancel> HotelBookingCancelResponse)
            {
                Data = new BookingCancelDto();
                Data.HotelBookingCancelResponse = HotelBookingCancelResponse;
            }
            public BookingCancelDto Data { get; }


        }


    }
}
