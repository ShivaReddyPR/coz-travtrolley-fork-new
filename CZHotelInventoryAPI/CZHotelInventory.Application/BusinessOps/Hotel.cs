﻿using CZHotelInventory.Api.Models;
using CZHotelInventory.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;


namespace CZHotelInventory.Application.BusinessOps
{
    public class Hotel
    {
        /// <summary>
        /// Query request to get Hotel Search Result
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public HotelRequest HotelSearchRequest { get; set; }

        }

        /// <summary>
        /// Validation for the <see cref="Hotel"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {

                try
                {
                    List<HotelSearchResult> hotelSearchResult = new List<HotelSearchResult>();

                    InvBooking booking = new InvBooking();
                    
                    DataTable dtHotelResults = booking.GetInventoryHotels(clsQuery.HotelSearchRequest);
                    DataView dv = dtHotelResults.DefaultView;
                    dv.RowFilter = "NOT(TotalRate IS NULL and TotalRate='')";
                    dtHotelResults = dv.ToTable();
                    if (dtHotelResults != null)
                    {
                        foreach (DataRow row in dtHotelResults.Rows)
                        {
                            //row["HotelMainImage"] = row["HotelMainImage"] != DBNull.Value ? ConfigurationSystem.HotelConnectConfig["imgPathForServer"] + row["HotelMainImage"] : "";
                            row["HotelMainImage"] = row["HotelMainImage"] != DBNull.Value ? ConfigurationManager.AppSettings["imgPathForServer"] + row["HotelMainImage"] : "";
                        }
                        hotelSearchResult = GenericStatic.ConvertToList<HotelSearchResult>(dtHotelResults);
                    }
                        var newdata = hotelSearchResult;
                        var viewModel = new ViewModel(newdata);
                        return await System.Threading.Tasks.Task.FromResult(new ApplicationResponse(viewModel.Data));
                    
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelConnectAvailSearch,Severity.High, 1, "Get,Err:" + ex.Message, "");
                    //CT.Core.Audit.Add(EventType.Exception, CT.Core.Severity.Normal, 1, "CZ.CZHotelInventoryAPIAvailSearch Err :" + ex.ToString(), "0");
                    return new ApplicationResponse().AddError("GetHotelSearchResultException", ex.ToString());
                }

            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(List<HotelSearchResult> HotelSearchResponse)
            {
                Data = new HotelSearchResultDto();
                Data.HotelSearchResponse = HotelSearchResponse;
            }
            public HotelSearchResultDto Data { get; }


        }
    }
}
