﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CZHotelInventory.Application.Models;

namespace CZHotelInventory.Application.BusinessOps
{
    public class AgentAppConfig
    {
        public int ConfigId { get; set; }
        public int ProductID { get; set; }
        public int AgentID { get; set; }
        public string Source { get; set; }
        public string AppKey { get; set; }
        public string AppValue { get; set; }
        public string Description { get; set; }
        public string BookingStatus { get; set; }

        public List<AgentAppConfig> GetConfigData()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@AgentID", AgentID);
                DataTable dtAgentConfig = DBGateway.FillDataTableSP("usp_GetAgentAppConfig", paramList);

                return (dtAgentConfig != null && dtAgentConfig.Rows.Count > 0) ? GenericStatic.ConvertToList<AgentAppConfig>(dtAgentConfig) :
                    new List<AgentAppConfig>();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(GetConfigData)Failed to get Agent app config data. Reason : " + ex.ToString(), "");
                return new List<AgentAppConfig>();
            }
        }
        public List<AgentAppConfig> GetConfigDataBySource()
        {
            try
            {
                DBGateway.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["dbSetting"];
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@P_Source", Source));
                DataTable dtAgentConfig = DBGateway.FillDataTableSP("usp_GetAppConfigBySource", paramList.ToArray());

                return (dtAgentConfig != null && dtAgentConfig.Rows.Count > 0) ? GenericStatic.ConvertToList<AgentAppConfig>(dtAgentConfig) :
                    new List<AgentAppConfig>();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(GetConfigDataBySource)Failed to get Source app config data. Reason : " + ex.ToString(), "");
                return new List<AgentAppConfig>();
            }
        }
    }
}
