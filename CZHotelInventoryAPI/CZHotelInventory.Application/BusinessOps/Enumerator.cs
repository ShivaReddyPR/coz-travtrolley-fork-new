namespace CZHotelInventory.Application.BusinessOps
{
    /// <summary>
    /// Hotel and Insurance FareType
    /// </summary>
    public enum FareType
    {
        Published = 1,
        Net = 2
    }
}
