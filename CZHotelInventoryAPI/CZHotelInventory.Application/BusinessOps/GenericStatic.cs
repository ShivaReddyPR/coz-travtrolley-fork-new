﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace CZHotelInventory.Application.BusinessOps
{
    public static class GenericStatic
    {
        /// <summary>
        /// Converts XML to given entity object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="XML"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string XML)
        {
            if (XML == null || XML == string.Empty)
                return default(T);
            XmlSerializer xs = null;
            StringReader sr = null;
            try
            {
                xs = new XmlSerializer(typeof(T));
                sr = new StringReader(XML);
                return (T)xs.Deserialize(sr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    sr.Dispose();
                }
            }
        }

        /// <summary>
        /// Converts source xml string to desired xml string using xslt file
        /// </summary>
        /// <param name="sXmlResponse"></param>
        /// <param name="sXSLTPath"></param>
        /// <returns></returns>
        public static StringBuilder TransformXML(string sXmlResponse, string sXSLTPath)
        {
            StringBuilder output = new StringBuilder();
            Dictionary<string, XslCompiledTransform> xsltCompilers = new Dictionary<string, XslCompiledTransform>();
            sXmlResponse = sXmlResponse.Replace("xmlns=", "SupTagURL=");
            sXmlResponse = sXmlResponse.Replace("xmlns:", "").Replace("ns1:", "").Replace("soap:", "");

            XslCompiledTransform compiler = new XslCompiledTransform(true);
            compiler.Load(sXSLTPath);
            xsltCompilers.Add(sXSLTPath, compiler);
            using (XmlWriter xWriter = XmlTextWriter.Create(output))
            {
                xsltCompilers[sXSLTPath].Transform(XmlReader.Create(new StringReader(sXmlResponse)), null, xWriter);
            }

            return output;
        }

        /// <summary>
        /// Writes XML log file of the given entity into provided path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="response"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string WriteLogFile(Type type, object response, string fileName)
        {
            string filePath = string.Empty;
            try
            {
                XmlSerializer ser = new XmlSerializer(type);
                filePath = fileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, response);
                sw.Close();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, 1, "Failed to Execute WriteLogFile Method. Reason : " + ex.ToString(), "");
            }
            return filePath;
        }

        /// <summary>
        /// Writes XML log file of the given string data into provided path
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string WriteLogFileXML(string data, string fileName)
        {
            string filePath = string.Empty;
            try
            {               
                XmlDocument doc = new XmlDocument();
                filePath = fileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                doc.LoadXml(data);                
                doc.Save(filePath);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, 1, "Failed to Execute WriteLogFileXML Method. Reason : " + ex.ToString(), "");
            }
            return filePath;
        }

        /// <summary>
        /// Converts source xml to desired xml and the same will be converted to entity
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="genClass"></param>
        /// <param name="sXmlResponse"></param>
        /// <param name="sXSLTPath"></param>
        /// <returns></returns>
        public static T TransformXMLAndDeserialize<T>(T genClass, string sXmlResponse, string sXSLTPath)
        {
            StringBuilder output = TransformXML(sXmlResponse, sXSLTPath);
            genClass = Deserialize<T>(output.ToString());
            return genClass;
        }

        /// <summary>
        /// To convert data to list entity which has both table column names and entity property names are same
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<T> ConvertToList<T>(this DataTable table) where T : new()
        {
            Type t = typeof(T);
            List<T> returnObject = new List<T>();

            foreach (DataRow dr in table.Rows)
            {
                T newRow = new T();
                foreach (DataColumn col in dr.Table.Columns)
                {
                    string colName = col.ColumnName;

                    PropertyInfo pInfo = t.GetProperty(colName.ToUpper(),
                        BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                    if (pInfo != null)
                    {
                        try
                        {
                            object val = dr[colName];
                            val = !(Nullable.GetUnderlyingType(pInfo.PropertyType) != null) ? Convert.ChangeType(val, pInfo.PropertyType) :
                                val is System.DBNull ? null : Convert.ChangeType(val, Nullable.GetUnderlyingType(pInfo.PropertyType));
                            pInfo.SetValue(newRow, val, null);
                        }
                        catch { }
                    }
                }
                returnObject.Add(newRow);
            }
            return returnObject;
        }

        public static string EncryptData(string textData)
        {
            try
            {
                string Encryptionkey = "<@>^@!@#";
                RijndaelManaged objrij = new RijndaelManaged();
                //set the mode for operation of the algorithm   
                objrij.Mode = CipherMode.CBC;
                //set the padding mode used in the algorithm.   
                objrij.Padding = PaddingMode.PKCS7;
                //set the size, in bits, for the secret key.   
                objrij.KeySize = 0x80;
                //set the block size in bits for the cryptographic operation.    
                objrij.BlockSize = 0x80;
                //set the symmetric key that is used for encryption & decryption.    
                byte[] passBytes = Encoding.UTF8.GetBytes(Encryptionkey);
                //set the initialization vector (IV) for the symmetric algorithm    
                byte[] EncryptionkeyBytes = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

                int len = passBytes.Length;
                if (len > EncryptionkeyBytes.Length)
                {
                    len = EncryptionkeyBytes.Length;
                }
                Array.Copy(passBytes, EncryptionkeyBytes, len);

                objrij.Key = EncryptionkeyBytes;
                objrij.IV = EncryptionkeyBytes;

                //Creates symmetric AES object with the current key and initialization vector IV.    
                ICryptoTransform objtransform = objrij.CreateEncryptor();
                byte[] textDataByte = Encoding.UTF8.GetBytes(textData);
                //Final transform the test string.  
                return Convert.ToBase64String(objtransform.TransformFinalBlock(textDataByte, 0, textDataByte.Length));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "Failed to Encrypt the string. Reason : " + ex.ToString(), "");
                return textData;
            }
        }

        public static string DecryptData(string EncryptedText)
        {
            try
            {
                string Encryptionkey = "<@>^@!@#";
                RijndaelManaged objrij = new RijndaelManaged();
                objrij.Mode = CipherMode.CBC;
                objrij.Padding = PaddingMode.PKCS7;

                objrij.KeySize = 0x80;
                objrij.BlockSize = 0x80;
                byte[] encryptedTextByte = Convert.FromBase64String(EncryptedText);
                byte[] passBytes = Encoding.UTF8.GetBytes(Encryptionkey);
                byte[] EncryptionkeyBytes = new byte[0x10];
                int len = passBytes.Length;
                if (len > EncryptionkeyBytes.Length)
                {
                    len = EncryptionkeyBytes.Length;
                }
                Array.Copy(passBytes, EncryptionkeyBytes, len);
                objrij.Key = EncryptionkeyBytes;
                objrij.IV = EncryptionkeyBytes;
                byte[] TextByte = objrij.CreateDecryptor().TransformFinalBlock(encryptedTextByte, 0, encryptedTextByte.Length);
                return Encoding.UTF8.GetString(TextByte);  //it will return readable string  
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "Failed to Decrypt the string. Reason : " + ex.ToString(), "");
                return EncryptedText;
            }
        }

        /// <summary>
        /// To generate Json and XMl logs based on web response Json string
        /// </summary>
        /// <param name="IsJson"></param>
        /// <param name="IsXML"></param>
        /// <param name="sData"></param>
        /// <param name="sPath"></param>
        /// <returns></returns>
        public static string GenerateLogs(bool IsJson, bool IsXML, ref string sData, string sPath)
        {
            string sFNames = string.Empty; 
            try
            {
                if (IsJson)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic searchResponse = js.Deserialize<dynamic>(sData);
                    sData = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                    sFNames = sPath + "Json_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".json";
                    StreamWriter sw = new StreamWriter(sFNames);
                    sw.Write(sData);
                    sw.Close();
                }

                if (IsXML)
                {
                    TextReader txtReader = new StringReader(sData);
                    XmlDocument doc = JsonConvert.DeserializeXmlNode(sData, "Root");
                    string sFile = sPath + "xml_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    sFNames = string.IsNullOrEmpty(sFNames) ? sFile : sPath + "|" + sFile;
                    doc.Save(sFile);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, 1, "Failed to Generate Logs from GenerateLogs Method. Reason : " + ex.ToString(), "");
            }

            return sFNames;
        }

        /// <summary>
        /// To replace NonASCII characters from the input string
        /// </summary>
        /// <param name="sInput"></param>
        /// <returns></returns>
        public static string ReplaceNonASCII(string sInput)
        {
            try
            {
                return Encoding.ASCII.GetString(
                    Encoding.Convert(
                        Encoding.UTF8,
                        Encoding.GetEncoding(
                            Encoding.ASCII.EncodingName,
                            new EncoderReplacementFallback(string.Empty),
                            new DecoderExceptionFallback()
                            ),
                        Encoding.UTF8.GetBytes(sInput)
                    )
                );
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, 1, "Error while replacing the ASCII characters. Reason : " + ex.ToString(), "");
                return sInput;
            }
        }

        /// <summary>
        /// To replace non alpha numeric characters from the input string
        /// </summary>
        /// <param name="sInput"></param>
        /// <returns></returns>
        public static string FilterAlphaNumeric(string sInput)
        {
            try
            {
                return Regex.Replace(sInput, "[^0-9A-Za-z]+", ""); ;
            }
            catch (Exception ex)
            {
              //  Audit.Add(EventType.Book, Severity.High, 1, "Error while filtering alpha numeric characters. Reason : " + ex.ToString(), "");
                return sInput;
            }
        }

        /// <summary>
        /// To serialize given object into Json string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeJson<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

        /// <summary>
        /// To log the error message to Audit table and send the as email
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        //public static void LogAuditSendMail(string sMessage, string sSubject, long sUserId)
        //{
        //    try
        //    {                
        //        Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(sUserId), "exception reason is" + sMessage, "");

        //        sSubject = string.IsNullOrEmpty(sSubject) ? "Web Application Exception details" : sSubject;
        //        Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["ErrorNotificationMailingId"],
        //            sSubject, "Dear Support Team:<br /><br />Please see below exception details <br /><br />" + sMessage);
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.Exception, Severity.High, 0, "Exception at LogAuditSendMail, reason is:" + ex.ToString(), "");
        //    }
        //}
    }

    //public static class DataTableToList
    //{
    //    /// <summary>
    //    ///  Convert a database data table to a runtime dynamic definied type collection (dynamic class' name as table name).
    //    /// </summary>
    //    /// <param name="dt"></param>
    //    /// <param name="className"></param>
    //    /// <returns></returns>
    //    public static List<dynamic> ToDynamicList(DataTable dt, string className)
    //    {
    //        return ToDynamicList(ToDictionary(dt), getNewObject(dt.Columns, className));
    //    }

    //    private static List<Dictionary<string, object>> ToDictionary(DataTable dt)
    //    {
    //        var columns = dt.Columns.Cast<DataColumn>();
    //        var Temp = dt.AsEnumerable().Select(dataRow => columns.Select(column =>
    //                             new { Column = column.ColumnName, Value = dataRow[column] })
    //                         .ToDictionary(data => data.Column, data => data.Value)).ToList();
    //        return Temp.ToList();
    //    }

    //    private static List<dynamic> ToDynamicList(List<Dictionary<string, object>> list, Type TypeObj)
    //    {
    //        dynamic temp = new List<dynamic>();
    //        foreach (Dictionary<string, object> step in list)
    //        {
    //            object Obj = Activator.CreateInstance(TypeObj);

    //            PropertyInfo[] properties = Obj.GetType().GetProperties();

    //            Dictionary<string, object> DictList = (Dictionary<string, object>)step;

    //            foreach (KeyValuePair<string, object> keyValuePair in DictList)
    //            {
    //                foreach (PropertyInfo property in properties)
    //                {
    //                    if (property.Name == keyValuePair.Key)
    //                    {
    //                        if (keyValuePair.Value != null && keyValuePair.Value.GetType() != typeof(System.DBNull))
    //                        {
    //                            if (keyValuePair.Value.GetType() == typeof(System.Guid))
    //                            {
    //                                property.SetValue(Obj, keyValuePair.Value, null);
    //                            }
    //                            else
    //                            {
    //                                property.SetValue(Obj, keyValuePair.Value, null);
    //                            }
    //                        }
    //                        break;
    //                    }
    //                }
    //            }
    //            //temp.Add(Obj);
    //        }
    //        return temp;
    //    }

    //    private static Type getNewObject(DataColumnCollection columns, string className)
    //    {
    //        AssemblyName assemblyName = new AssemblyName();
    //        assemblyName.Name = "YourAssembly";
    //        AssemblyBuilder assemblyBuilder = Thread.GetDomain().DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
    //        ModuleBuilder module = assemblyBuilder.DefineDynamicModule("YourDynamicModule");
    //        TypeBuilder typeBuilder = module.DefineType(className, TypeAttributes.Public);

    //        foreach (DataColumn column in columns)
    //        {
    //            string propertyName = column.ColumnName;
    //            FieldBuilder field = typeBuilder.DefineField(propertyName, column.DataType, FieldAttributes.Public);
    //            PropertyBuilder property = typeBuilder.DefineProperty(propertyName, System.Reflection.PropertyAttributes.None, column.DataType, new Type[] { column.DataType });
    //            MethodAttributes GetSetAttr = MethodAttributes.Public | MethodAttributes.HideBySig;
    //            MethodBuilder currGetPropMthdBldr = typeBuilder.DefineMethod("get_value", GetSetAttr, column.DataType, new Type[] { column.DataType }); // Type.EmptyTypes);
    //            ILGenerator currGetIL = currGetPropMthdBldr.GetILGenerator();
    //            currGetIL.Emit(OpCodes.Ldarg_0);
    //            currGetIL.Emit(OpCodes.Ldfld, field);
    //            currGetIL.Emit(OpCodes.Ret);
    //            MethodBuilder currSetPropMthdBldr = typeBuilder.DefineMethod("set_value", GetSetAttr, null, new Type[] { column.DataType });
    //            ILGenerator currSetIL = currSetPropMthdBldr.GetILGenerator();
    //            currSetIL.Emit(OpCodes.Ldarg_0);
    //            currSetIL.Emit(OpCodes.Ldarg_1);
    //            currSetIL.Emit(OpCodes.Stfld, field);
    //            currSetIL.Emit(OpCodes.Ret);
    //            property.SetGetMethod(currGetPropMthdBldr);
    //            property.SetSetMethod(currSetPropMthdBldr);
    //        }
    //        Type obj = typeBuilder.CreateType();
    //        return obj;
    //    }
    //}
}
