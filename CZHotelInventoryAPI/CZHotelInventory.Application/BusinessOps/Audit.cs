using CZHotelInventory.Application.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace CZHotelInventory.Application.BusinessOps
{
    public enum EventType
    {
        Login = 1,
        Logout = 2,
        Search = 3,
        Book = 4,
        Issue = 5,
        Exception = 6,
        DisplayPNR = 7,
        Price = 8,
        CancellBooking = 9,
        Ticketing = 10,
        PNRRefresh = 11,
        UpdateSSR = 12,
        MetaSearchEngine = 13,
        SpiceJet = 14,
        SpiceJetSearch = 15,
        SpiceJetFareQuote = 16,
        SpiceJetBooking = 17,
        SpiceJetDisplayPNR = 18,
        Navitaire = 19,
        NavitaireSearch = 20,
        NavitaireFareQuote = 21,
        NavitaireBooking = 22,
        NavitaireDisplayPNR = 23,
        UpdateConfigXML = 24,
        Paramount = 25,
        ParamountLogin = 26,
        ParamountSearch = 27,
        ParamountFareQuote = 28,
        ParamountBooking = 29,
        AirDeccan = 30,
        AirDeccanSearch = 31,
        AirDeccanFareQuote = 32,
        AirDeccanBooking = 33,
        Email = 34,
        CalendarFare = 35,
        AirDeccanDisplayPNR = 36,
        HotelDesiya = 37,
        DesiyaAvailSearch = 38,
        DesiyaSpecificDetail = 39,
        DesiyaBookingAgreement = 40,
        GetFareQuote = 41,
        GetFareRule = 42,
        GetBooking = 43,
        Mdlr = 44,
        MdlrLogin = 45,
        MdlrSearch = 46,
        MdlrFareQuote = 47,
        MdlrBooking = 48,
        DesiyaCancel = 49,
        GoAir = 50,
        GoAirSearch = 51,
        GoAirFareQuote = 52,
        GoAirBooking = 53,
        GoAirDisplayPNR = 54,
        GTAAvailSearch = 55,
        GTAItemSearch = 56,
        GTAFareBreakDown = 57,
        GTAImageSearch = 58,
        GTAChagreCondition = 59,
        GTABooking = 60,
        GTACancel = 61,
        AddAPIBookingDetail = 62,
        GTARoomsAvailability = 63,
        GTAAOTNumber = 64,
        HotelDeal = 65,
        GTAImportHotel = 66,
        InsuranceBooking = 67,
        IsAgentCapable = 68,
        InvoiceEdition = 69,
        HotelBedsAvailSearch = 70,
        HotelBedsBooking = 71,
        HotelBedsCancel = 72,
        HotelBedsImport = 73,
        AirlineDealSheetSave = 74,
        AirlineDealSheetModify = 75,
        AirlineDealSheetDelete = 76,
        HotelSearch = 77,
        TBOBanks = 78,
        TouricoAvailSearch = 79,
        TouricoBooking = 80,
        TouricoCancel = 81,
        TouricoImport = 82,
        HDFCSessionExpire = 83,
        TransferSearch = 84,
        TransferBooking = 85,
        TransferCancel = 86,
        CCAvenue = 87,
        GetHotelDetails = 88,
        GetCancellationPolicy = 89,
        HotelBook = 90,
        GetHotelBooking = 91,
        SendHotelChangeRequest = 92,
        GetHotelChangeRequestStatus = 93,
        SightseeingSearch = 94,
        SightseeingBooking = 95,
        SightseeingCancel = 96,
        Sama = 97,
        SamaLogin = 98,
        SamaSearch = 99,
        SamaFareQuote = 100,
        SamaBooking = 101,
        GetCountryList = 102,
        GetDestinationCityList = 103,
        GetTopDestinations = 104,
        IRCTCXMLLogs = 105,
        IRCTCSearch = 106,
        IRCTCFareAndAvailability = 107,
        IANAvailSearch = 108,
        IANRoomAvailability = 109,
        IANBooking = 110,
        IANCancel = 111,
        IANItemInformation = 112,
        IRCTCBook = 113,
        IRCTCItineraryDetails = 114,
        IRCTCBookingSearch = 115,
        IRCTCCancelTicket = 116,
        IndiaTimesBookingDetailsTripSearch = 117,
        IndiaTimesBookingDetailsUpdateIPStatus = 118,
        IndiaTimesBookingDetailsUpdateCustomerStatus = 119,
        IndiaTimesBookingDetailsUpdateHistory = 120,
        IndiaTimesBookingDetailsRejectTicket = 121,
        IndiaTimesBookingDetailsTicketing = 122,
        GetIPAddressStatus = 123,
        IRCTCGetPnrStatus = 124,
        PLBRuleMaster = 125,
        AgencyCategory = 126,
        PLBBlackouts = 127,
        PLBSectorException = 128,
        Airline = 129,
        Account = 130,
        IRCTCGetTrainRoute = 131,
        IRCTCGetRDSBalance = 132,
        IndiaTimesTicketSave = 133,
        IndiaTimesEmailTicket = 134,
        IndiaTimesPendingQueueTripSearch = 135,
        IndiaTimesPendingQueueReleasePNR = 136,
        IndiaTimesPendingQueueMoveSelection = 137,
        AddPendingBookingDetail = 138,
        ServiceRequest = 139,
        HermesAirSearch = 140,
        HermesAirFareQuote = 141,
        HermesAirBooking = 142,
        IRCTCImportPnr = 143,
        HermesAccountDetail = 144,
        HermesDisplayPNR = 145,
        IRCTCTrainRoutesUpdate = 146,
        IRCTCTatkalBookingRequest = 147,
        IRCTCRefundDetail = 148,
        IRCTCMobile = 149,
        AmexPaymentGateway = 150,
        AgencyCustomer = 151,
        GetStateListForIAN = 152,
        AddHotelAPIBookingDetail = 153,
        BApiHotelBookingPage = 154,
        BApiHotelTripSearch = 155,
        SendSMS = 156,
        ChangeRequest = 157,
        ManualInvoice = 158,
        CreditCardPaymentRequest = 159,
        ICICIPaymentGateWay = 160,
        GetActiveHotelDeals = 161,
        HotelConnectAvailSearch = 162,
        HotelConnectBooking = 163,
        HotelConnectItemInformation = 164,
        HotelConnectCancel = 165,
        MikiAvailSearch = 166,
        MikiBooking = 167,
        MikiCancel = 168,
        HotelMiki = 169,
        TravcoAvailSearch = 170,
        TravcoBooking = 171,
        TravcoCancel = 172,
        HotelTravco = 173,
        FlyDubai = 174,
        FlyDubaiFareQuote = 175,
        FlyDubaiBooking = 176,
        IAN3DVerified = 177,
        DOTWAvailSearch = 178,
        DOTWBooking = 179,
        DOTWCancel = 180,
        DOTWImport = 182,
        WSTAvailSearch = 183,
        WSTBooking = 184,
        WSTCancel = 185,
        WSTImport = 186,
        HotelCMS = 187,
        HolidayPackageQuery = 188,
        AirlineCategoryProperties = 189,
        AgencyCommission = 190,
        AgencyCancellationCharge = 191,
        AutoPayment = 192,
        IRCTCAgentCommission = 193,
        AirArabiaSearch = 194,
        AirArabiaPriceRetrieve = 195,
        AirArabiaBooking = 196,
        AirArabiaRetrieveByPNR = 197,
        MobileRecharge = 198,
        IndigoCancellation = 199,
        SpiceJetCancellation = 200,
        ReturnSearch = 201,
        AmadeusLogin = 202,
        FlyDubaiDisplayPNR = 203,
        AirArabiaCreditbalance = 204,
        IndiaTimesBookingDetailsHotel = 205,
        IndiaTimesPendingQueueHotel = 206,
        AddPendingHotelBookingDetail = 207,
        UpdatePendingHotelBookingDetail = 208,
        IndiaTimesHotelBookingDetailsSave = 209,
        Visa = 210,
        HolidayPackageQueue = 211,
        FixedDeparture = 212,
        RezLiveAvailSearch = 213,
        RezLiveBooking = 214,
        RezLiveCancel = 215,
        TBOHotelAvailSearch = 216,
        TBOHotel = 217,
        TBOHoteDetailsSerch = 218,
        TBORoomSearch = 219,
        TBOBooking = 210,
        TBOCancel = 211,
        TBOAir = 212,
        JACAvailSearch = 213,
        JacChargeCondition = 214,
        JACDetailsSearch = 215,
        JACBooking = 216,
        JACCancel = 217,
        EETAvailSearch = 218,
        EETChargeCondition = 219,
        EETDetailsSearch = 220,
        EETBooking = 221,
        EETCancel = 222,
        FleetSearch = 223,
        FleetResults = 224,
        FleetBooking = 225,
        SayaraAvailSearch = 226,
        SayaraCitySearch = 227,
        SayaraBooking = 228,
        FleetBookingQueue = 229,
        FleetCancel = 230,
        SayaraBookingQueue = 231,
        SayaraCancel = 232,
        SayaraChangeRequest = 233,
        AgodaAvailSearch = 234,
        AgodaBooking = 235,
        AgodaCancel = 236,
        AgodaStatic = 237,
        SafexPay = 238,//Added by Somasekhar on 28/06/2018 
        YatraAvailSearch = 239,//Added by Somasekhar on 24/08/2018 
        YatraStatic = 240,//Added by Somasekhar on 30/08/2018 
        YatraBooking = 241,//Added by Somasekhar on 04/09/2018 
        YatraCancel = 242,//Added by Somasekhar on 04/09/2018 
        #region GRN  Added BY Harish
        GrnSearch = 243,
        GrnRefetch = 244,
        GrnRecheck = 245,
        GrnBooking = 246,
        GrnCancel = 247,
        GrnStatic = 248,
        #endregion
        #region OYO --- Added by Somaskehar on 11/12/2018
        OYOAvailSearch = 249,
        OYOStatic = 250,
        OYOBooking = 251,
        OYOCancel = 252,
        OYOGSTIN = 253,
        #endregion
        #region Gimmonix --- Added by Harish on May-04-2018
        GimmonixSearch = 254,
        GimmonixRoomDetails = 255,
        GimmonicCancelltionPolicy = 256,
        GimmonixBooking = 257,
        GimmonixBookCancel = 258,
        GimmonixViewBooking = 259,
        GimmonixPaymentPreferences = 260,
        #endregion
        #region Illusions --- Added by Krishna Phani on 14/04/2020
        IllusionsSearch = 261,
        IllusionsRoomDetails = 262,
        IllusionsCancelltionPolicy = 263,
        IllusionsBooking = 264,
        IllusionsBookCancel = 265,
        IllusionsViewBooking = 266,
        IllusionsPaymentPreferences = 267,
        #endregion
        ROSauthentication = 1000,
        ROSRegistration = 1001,
        ROSCrewRosterList = 1002,
        ROSCrewRosterStatus = 1003,
        RosChauffeurJobList = 1004,
        ROSChauffeurJobStatus = 1005,
        RosLocationList = 1006,
        RosFeedBack = 1007,
        RosUpdateCrewRoster = 1008,
        RosPersonalHire = 1009,
        RosEmpGetData = 1010,
        RosUpdateEmpData = 1011,
        RosContactInfo = 1012,
        RosForgotPassword = 1013,
        AirindiaExpress = 1014,
        RePrice = 1015

    }

    public enum Severity
    {
        High = 1,
        Low = 2,
        Normal = 3
    }

    public class Audit
    {
        int userId;
        DateTime evenTime;
        string detail;
        EventType evType;
        Severity sev;
        string ipAddress;

        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }


        public int UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
            }
        }

        public DateTime EvenTime
        {
            get
            {
                return evenTime;
            }
            set
            {
                evenTime = value;
            }
        }

        public string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }

        public EventType EvType
        {
            get
            {
                return evType;
            }
            set
            {
                evType = value;
            }
        }

        public Severity Sev
        {
            get
            {
                return sev;
            }
            set
            {
                sev = value;
            }
        }



        /// <summary>
        /// Method to log each and every important transaction into database
        /// </summary>
        /// <param name="EvType"></param>
        /// <param name="Sev"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        public static void Add(EventType EvType, Severity Sev, int UserId, string Detail, string IPAddress)
        {

            // TO DO: also add Time
            ////Trace.TraceInformation("Audit.Add entered : ");
            try
            {
                //bool audit = Convert.ToBoolean(ConfigurationSystem.AuditConfig[Sev.ToString().ToLower()]);
                bool audit = true;
                if (audit)
                {
                    if (Detail == null || Detail == "")
                    {
                        throw new ArgumentException("Detail should have a value", "Detail");
                    }

                    // adding data
                    SqlParameter[] paramList = new SqlParameter[6];
                    paramList[0] = new SqlParameter("@eventType", EvType);
                    paramList[1] = new SqlParameter("@severity", Sev);
                    paramList[2] = new SqlParameter("@userId", UserId);
                    paramList[3] = new SqlParameter("@detail", Detail);
                    paramList[4] = new SqlParameter("@eventTime", DateTime.Now);
                    paramList[5] = new SqlParameter("@ipAddress", IPAddress);

                    int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddAudit", paramList);
                }
            }

            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Audit.Add())Failed to get Source app config data. Reason : " + ex.ToString(), "");
            }          
        }
    }


}

