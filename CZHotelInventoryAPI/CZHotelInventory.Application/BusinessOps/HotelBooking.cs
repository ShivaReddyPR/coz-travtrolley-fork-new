﻿using CZHotelInventory.Api.Models;
using CZHotelInventory.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;


namespace CZHotelInventory.Application.BusinessOps
{
    public class HotelBooking
    {
        /// <summary>
        /// Query request to get Hotel Search Result
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public BookingRequest HotelBookingRequest { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="HotelBooking"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {

                    List<BookingResponse> hotelbookingresponse = new List<BookingResponse>();

                    InvBooking booking = new InvBooking();

                    string ConfirmationNo = booking.ConfirmBooking(clsQuery.HotelBookingRequest);

                    BookingResponse bookingResponse = new BookingResponse();
                    if(ConfirmationNo != null)
                    {
                        bookingResponse.ConfirmationNo = ConfirmationNo;
                        bookingResponse.Status = "Successful";
                    }

                    hotelbookingresponse.Add(bookingResponse);

                    var newdata = hotelbookingresponse;
                    var viewModel = new ViewModel(newdata);
                    return await System.Threading.Tasks.Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Get,Err:" + ex.Message, "");
                   // CT.Core.Audit.Add(EventType.Exception, CT.Core.Severity.Normal, 1, "CZ.CZHotelInventoryAPIHotelBooking Err :" + ex.ToString(), "0");
                    return new ApplicationResponse().AddError("BookingResponseException", ex.ToString());
                }

            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(List<BookingResponse> HotelBookingResponse)
            {
                Data = new BookingResponseDto();
                Data.HotelBookingResponse = HotelBookingResponse;
            }
            public BookingResponseDto Data { get; }


        }
    }
}
