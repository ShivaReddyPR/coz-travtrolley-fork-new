using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CZHotelInventory.Application.BusinessOps
{
    public enum ProductType
    {
        //Null = 0,
        Flight = 1,
        Hotel = 2,
        Packages = 3,
        Activity = 4,
        Insurance = 5,
        SightSeeing = 6,
        Train = 7,
        Transfers = 9,
        SMSPack = 8,
        MobileRecharge = 10,
        FixedDeparture = 11,
        Visa=12,
        Car=13,
        ItineraryAddService = 14,
        BaggageInsurance=15

    }
    public enum BookingStatus
    {
        Hold = 1,
        Ready = 2,
        Cancelled = 3,
        InProgress = 4,
        Ticketed = 5,
        VoidInProgress = 6,
        Void = 7,
        Delivered = 8,
        Inactive = 9,
        AmendmentInProgress = 10,
        AmendmentDone = 11,
        Released = 12,
        OutsidePurchase = 13,
        Booked = 14,
        CancellationInProgress = 15,
        Refunded = 16,
        ModificationInProgress=17,
        Modified = 18,
        RefundInProgress = 19,
        //TODO: In case we add one more status there can be an affect in GetBooking method of BookingAPI(ref: Please see TODO list there)
        
        CancellationDone = 20      //Newly added for Flight change request
    }

}
