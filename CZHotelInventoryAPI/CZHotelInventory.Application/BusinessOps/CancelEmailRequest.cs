﻿using CZHotelInventory.Api.Models;
using CZHotelInventory.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using System.Collections;
using System.Configuration;


namespace CZHotelInventory.Application.BusinessOps
{
    public class CancelEmailRequest
    {
        /// <summary>
        /// Query request to get Hotel Search Result
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public BookingCancelRequest HotelBookingCancelRequest { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="CancelBooking"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {

                    //InvBooking booking = new InvBooking();

                    List<CancelEmailResponse> hotelcancelEmailResponse = new List<CancelEmailResponse>();

                    //SentCancelEmail(clsQuery.HotelBookingCancelRequest.ConfirmationNo, "REQUEST");

                    string from = string.Empty, mailTo = string.Empty, subject = string.Empty, message = string.Empty, eMailsTo = string.Empty;
                    List<string> toList = new List<string>();
                    Hashtable tempTable = new Hashtable();
                    AgentAppConfig clsAppCnf = new AgentAppConfig();
                    clsAppCnf.Source = "HIS";

                    //string sLogoPath = "https://travtrolley.com/Uploads/AgentLogos/" + Convert.ToString(hotelBooking.CreatedBy).ImgFileName;
                    string sLogoPath = "https://ibyta.com/images/ibyta-logo.jpg";

                    List<AgentAppConfig> appConfigs = clsAppCnf.GetConfigDataBySource();

                    EmailParams clsParams = new EmailParams();

                    from = ConfigurationManager.AppSettings["fromEmail"];
                    clsParams.FromEmail = from;
                    DataTable dtEmail = Email.GetBookingCancelEmail(clsQuery.HotelBookingCancelRequest.ConfirmationNo, "REQUEST");

                    mailTo = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_CANCEL_EMAIL").Select(y => y.AppValue).FirstOrDefault();
                    subject = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_SUPP_CANCEL_SUBJECT").Select(y => y.AppValue).FirstOrDefault();

                    toList.Add(mailTo);
                    clsParams.ToEmail = toList[0];
                    clsParams.CCEmail = mailTo;
                    clsParams.Subject = subject;

                    if (dtEmail != null && dtEmail.Rows.Count > 0)
                    {
                        //clsParams.EmailBody = Convert.ToString(dtEmail.Rows[0]["EmailBody"]);
                        clsParams.EmailBody =  Convert.ToString(dtEmail.Rows[0]["EmailBody"]).Replace("@ROOMDTLS", Convert.ToString(dtEmail.Rows[0]["RoomInfo"])).Replace("@AgentLogo", sLogoPath);

                        Email.Send(clsParams);
                    }


                    CancelEmailResponse cancelEmail = new CancelEmailResponse();
                    cancelEmail.Status = "Mail sent Successfully";

                    hotelcancelEmailResponse.Add(cancelEmail);

                    var newdata = hotelcancelEmailResponse;
                    var viewModel = new ViewModel(newdata);
                    return await System.Threading.Tasks.Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Get,Err:" + ex.Message, "");
                    return new ApplicationResponse().AddError("BookingCancelEmailResponseException", ex.ToString());
                }

            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(List<CancelEmailResponse> HotelBookingCancelEmailResponse)
            {
                Data = new CancelEmailResponseDto();
                Data.HotelBookingCancelEmailResponse = HotelBookingCancelEmailResponse;
            }

            public CancelEmailResponseDto Data { get; }


        }
    }
}
