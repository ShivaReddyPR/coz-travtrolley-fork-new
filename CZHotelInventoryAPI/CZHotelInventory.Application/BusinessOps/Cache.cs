using System;
using System.Collections.Generic;
using System.Text;
using BeIT.MemCached;



namespace CZHotelInventory.Application.BusinessOps
{
    public class Cache
    {
        static MemcachedClient cache;
        static Cache()
        {
            string[] serverList = System.Configuration.ConfigurationManager.AppSettings["MemCacheServerList"].Split(';');         
            MemcachedClient.Setup("cache", serverList);
            cache = MemcachedClient.GetInstance("cache");
        }

        public static void Add(string key, object value)
        {
            try
            {
                cache.Add(key, value);
            }
            catch (Exception) { }
        }

        public static object Get(string key)
        {
            try
            {
                return cache.Get(key);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void Delete(string key)
        {
            try
            {
                cache.Delete(key);
            }
            catch (Exception) { }

        }
        //Use this replace if key already exists in mem cache server
        public static void Replace(string key, object value)
        {
            try
            {
                cache.Replace(key, value);

            }
            catch (Exception) { }

        }
        public static void FlushAll()
        {
            try
            {
                cache.FlushAll();

            }
            catch (Exception) { }

        }
        public static bool ContainsKey(string key)
        {
            bool contains = false;
            try
            {
                if (cache.Get(key) != null)
                {
                    contains = true;
                }
            }
            catch (Exception) { }
            return contains;
        }
    }
}
