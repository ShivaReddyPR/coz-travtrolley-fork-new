using System;
using System.Collections.Generic;
using System.Xml;
using System.Collections;

namespace CZHotelInventory.Application.BusinessOps
{
    //TODO: Make a static hash table.
    public class ConfigurationSystem
    {
        // path of config files
        static string configPath = System.Configuration.ConfigurationManager.AppSettings["configFiles"];
        static bool isMemCacheEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["MemCacheEnabled"]);
        public Hashtable GetHostPort()
        {
            XmlDocument xmlDoc = new XmlDocument();
            Hashtable myHash = new Hashtable();
            xmlDoc.Load(configPath + "HostPort.config.xml");
            XmlNode node = xmlDoc.ChildNodes[1];
            if (node.HasChildNodes)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    myHash.Add(node.ChildNodes[i].Name, node.ChildNodes[i].InnerText);
                }
            }
            return myHash;
        }
        /// <summary>
        /// Config for Hotel Connect Integration
        /// </summary>
        private static Dictionary<string, string> hotelConnectConfig = null;
        public static Dictionary<string, string> HotelConnectConfig
        {
            get
            {
                if (!ShouldLoadFromFile(hotelConnectConfig, MemCacheKeys.HotelConnectConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(hotelConnectConfig, MemCacheKeys.HotelConnectConfig);
                }
                else
                {
                    hotelConnectConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    //xmlDoc.Load(configPath + "HIS.config.xml");
                    xmlDoc.Load(configPath + "HISApi.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList hotelConnectData = config.ChildNodes;
                    for (int i = 0; i < hotelConnectData.Count; i++)
                    {
                        hotelConnectConfig.Add(hotelConnectData[i].Name, hotelConnectData[i].InnerXml);
                    }
                    AddInMemCache(hotelConnectConfig, MemCacheKeys.HotelConnectConfig);
                    return hotelConnectConfig;
                }
            }
        }
        /// This method checks whether data should be loaded from config files or is it already there in memcache or memory
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static bool ShouldLoadFromFile(object dataObject, string key)
        {
            bool response = false;
            if (isMemCacheEnabled)
            {
                response = !Cache.ContainsKey(key);
            }
            else
            {
                response = (dataObject == null);
            }
            return response;
        }
        /// <summary>
        /// This method gets the actual data either from memcache or from memory
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static object GetStaticData(object dataObject, string key)
        {
            object staticData;
            if (isMemCacheEnabled)
            {
                staticData = Cache.Get(key);
            }
            else
            {
                staticData = dataObject;
            }
            return staticData;
        }
        /// <summary>
        /// This method is used to add data in memcache
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="key"></param>
        private static void AddInMemCache(object dataObject, string key)
        {
            if (isMemCacheEnabled)
            {
                Cache.Add(key, dataObject);
            }
        }
        /// <summary>
        ///Configuration entries for switching on/off logging.
        /// </summary>
        private static Dictionary<string, string> auditConfig = null;

        /// <summary>
        /// Gets the dictionary containing Audit configuration.
        /// </summary>
        public static Dictionary<string, string> AuditConfig
        {
            get
            {
                if (!ShouldLoadFromFile(auditConfig, MemCacheKeys.AuditConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(auditConfig, MemCacheKeys.AuditConfig);
                }
                else
                {
                    auditConfig = new Dictionary<string, string>();
                    lock (auditConfig)
                    {

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(configPath + "Audit.config.xml");
                        XmlNode config = xmlDoc.SelectSingleNode("config");
                        XmlNodeList data = config.ChildNodes;
                        for (int i = 0; i < data.Count; i++)
                        {
                            auditConfig.Add(data[i].Name, data[i].InnerXml);
                        }
                        AddInMemCache(auditConfig, MemCacheKeys.AuditConfig);
                    }
                    return auditConfig;
                }
            }
        }
    }
}

