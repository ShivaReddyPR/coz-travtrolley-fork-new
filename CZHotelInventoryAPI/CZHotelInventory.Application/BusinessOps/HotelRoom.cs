﻿using CZHotelInventory.Api.Models;
using CZHotelInventory.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;


namespace CZHotelInventory.Application.BusinessOps
{
    public class HotelRoom
    {
        /// <summary>
        /// Query request to get Hotel Search Result
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public HotelRoomRequest RoomRequest { get; set;}
        }

        /// <summary>
        /// Validation for the <see cref="HotelRoom"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    List<RoomTypeDetails> CZhotelroomdetails = new List<RoomTypeDetails>();

                    InvBooking booking = new InvBooking();

                    DataTable dtHotelRoomResults = booking.GetInventoryHotelRooms(clsQuery.RoomRequest);
                    if(dtHotelRoomResults != null)
                    { 
                        RoomTypeDetails roomTypeDetails = new RoomTypeDetails();
                        roomTypeDetails.HotelId = clsQuery.RoomRequest.HotelCode;
                        Rooms[] rooms = new Rooms[dtHotelRoomResults.Rows.Count];
                        for (int i = 0; i < dtHotelRoomResults.Rows.Count; i++)
                        {
                            rooms[i].RoomId = Convert.ToInt32(dtHotelRoomResults.Rows[i]["RoomId"].ToString());
                            rooms[i].RoomNumber = Convert.ToInt32(dtHotelRoomResults.Rows[i]["room_number"].ToString());
                            rooms[i].RoomName = dtHotelRoomResults.Rows[i]["RoomName"].ToString();
                            rooms[i].AccomId = Convert.ToInt32(dtHotelRoomResults.Rows[i]["accomId"].ToString());
                            rooms[i].Accomodation = dtHotelRoomResults.Rows[i]["accomodation"].ToString();
                            rooms[i].Rate = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["Rate"].ToString());
                            rooms[i].PeriodId = Convert.ToInt32(dtHotelRoomResults.Rows[i]["periodid"].ToString());
                            //rooms[i].ChildRateType = dtHotelRoomResults.Rows[i]["Child_Rate_Type"].ToString();
                            rooms[i].ExtraBedRate = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["ExtraBedRate"].ToString());
                            rooms[i].ChildExtraBedRate = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["ChildExtraBedRate"].ToString());
                            //rooms[i].RoomChild1AgeRange = dtHotelRoomResults.Rows[i]["room_child1_age_range"].ToString();
                            //rooms[i].RoomChild2AgeRange = dtHotelRoomResults.Rows[i]["room_child2_age_range"].ToString();
                            //rooms[i].RoomChild3AgeRange = dtHotelRoomResults.Rows[i]["room_child3_age_range"].ToString();
                            List<string> images = new List<string>();
                            //string imagePath = ConfigurationSystem.HotelConnectConfig["imgPathForServer"];
                            string imagePath = ConfigurationManager.AppSettings["imgPathForServer"];
                            if (dtHotelRoomResults.Rows[i]["room_img1"].ToString()!="")
                            {
                                images.Add(imagePath + "RoomImages/" + dtHotelRoomResults.Rows[i]["room_img1"].ToString());
                            }
                            if (dtHotelRoomResults.Rows[i]["room_img2"].ToString() != "")
                            {
                                images.Add(imagePath + "RoomImages/" + dtHotelRoomResults.Rows[i]["room_img2"].ToString());
                            }
                            if (dtHotelRoomResults.Rows[i]["room_img3"].ToString() != "")
                            {
                                images.Add(imagePath + "RoomImages/" + dtHotelRoomResults.Rows[i]["room_img3"].ToString());
                            }
                            if (dtHotelRoomResults.Rows[i]["room_img4"].ToString() != "")
                            {
                                images.Add(imagePath + "RoomImages/" + dtHotelRoomResults.Rows[i]["room_img4"].ToString());
                            }
                            rooms[i].Images = images.ToArray();
                            rooms[i].Adults = Convert.ToInt32(dtHotelRoomResults.Rows[i]["Adults"].ToString());
                            rooms[i].Childs = Convert.ToInt32(dtHotelRoomResults.Rows[i]["Childs"].ToString());
                           // rooms[i].WeekDay = dtHotelRoomResults.Rows[i]["Week_Day"].ToString();
                            rooms[i].ChildRate1 = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["ChildRate1"].ToString());
                            rooms[i].ChildRate2 = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["ChildRate2"].ToString());
                            rooms[i].ChildRate3 = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["ChildRate3"].ToString());
                            rooms[i].ChildRate4 = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["ChildRate4"].ToString());
                            rooms[i].ChildRateApplied = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["ChildrateApplied"].ToString());
                            rooms[i].TotalRate = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["TotalRate"].ToString());
                            int minStay = Convert.ToInt32(dtHotelRoomResults.Rows[i]["MinStay"].ToString());
                            List<decimal> Daywise = new List<decimal>();
                            for (int k = 0; k < minStay; k++)
                            {
                                decimal roomrate = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["TotalRate"].ToString());

                                Daywise.Add(roomrate/minStay);
                            }
                            rooms[i].RateDaywise = Daywise.ToArray();
                            rooms[i].TaxRate = Convert.ToDecimal(dtHotelRoomResults.Rows[i]["TaxRate"].ToString());
                            rooms[i].TaxType = dtHotelRoomResults.Rows[i]["TaxType"].ToString();
                            //rooms[i].QuotaUsed = Convert.ToInt32(dtHotelRoomResults.Rows[i]["quota_used"].ToString());
                            //rooms[i].NoOfAdult = Convert.ToInt32(dtHotelRoomResults.Rows[i]["room_adult"].ToString());
                            //rooms[i].NoOfChild = Convert.ToInt32(dtHotelRoomResults.Rows[i]["room_child"].ToString());
                            rooms[i].ExtraBed = dtHotelRoomResults.Rows[i]["ExtraBed"].ToString();
                            List<string> facilities = new List<string>();
                            if (dtHotelRoomResults.Rows[i]["facility_name"].ToString() != "")
                            {
                                rooms[i].Hotelfacilities = dtHotelRoomResults.Rows[i]["facility_name"].ToString().Split(',');
                            }
                            rooms[i].AllocationType = dtHotelRoomResults.Rows[i]["AllocType"].ToString();
                            rooms[i].SellType = dtHotelRoomResults.Rows[i]["SellType"].ToString();
                            rooms[i].MarketId = Convert.ToInt32(dtHotelRoomResults.Rows[i]["MarketId"]);
                            rooms[i].RqId = Convert.ToInt32(dtHotelRoomResults.Rows[i]["Rqid"]);
                        }
                        roomTypeDetails.Rooms = rooms;
                        CZhotelroomdetails.Add(roomTypeDetails);
                    }

                    var newdata = CZhotelroomdetails;
                    var viewModel = new ViewModel(newdata);
                    return await System.Threading.Tasks.Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Get,Err:" + ex.Message, "");
                   // CT.Core.Audit.Add(EventType.Exception, CT.Core.Severity.Normal, 1, "CZ.CZHotelInventoryAPIRoomDetails Err :" + ex.ToString(), "0");
                    return new ApplicationResponse().AddError("GetRoomDetailsException", ex.ToString());
                }

            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(List<RoomTypeDetails> GetRoomResponse)
            {
                Data = new HotelRoomDetailsDto();
                Data.GetRoomResponse = GetRoomResponse;
            }
            public HotelRoomDetailsDto Data { get; }
            
        }
    }
}
