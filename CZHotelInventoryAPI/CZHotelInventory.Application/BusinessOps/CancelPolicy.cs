﻿using CZHotelInventory.Api.Models;
using CZHotelInventory.Application.Models;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using System.Globalization;

namespace CZHotelInventory.Application.BusinessOps
{
    public class CancelPolicy
    {
        /// <summary>
        /// Query request to get Hotel Search Result
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public HotelCancelPolicyRequest CancelPolicyRequest { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="CancelPolicy"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {

                //string cancelPolicy = "";
                //string terms = "";
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                int durationHours = 0;
                string hotelNorms = "";

                try
                {
                    HotelCancellationPolicyDto CZHotelCancellationPolicyDto = new HotelCancellationPolicyDto();
                    List<CancellationPolicies> hotelCancelPolicylist = new List<CancellationPolicies>();

                    InvBooking booking = new InvBooking();

                    DataTable dtCancelPolicy = booking.GetCancelPolicy(clsQuery.CancelPolicyRequest);
                    List<CancelPolicies> cancelPolicyList = new List<CancelPolicies>();
                    if (dtCancelPolicy != null && dtCancelPolicy.Rows.Count > 0)
                    {
                        CancellationPolicies cancellationpolicies = new CancellationPolicies();
                        if (dtCancelPolicy.Rows[0]["cnx_terms_conditions"] != DBNull.Value)
                        {
                            hotelNorms = dtCancelPolicy.Rows[0]["cnx_terms_conditions"].ToString() + "#";
                        }
                        hotelNorms += "CheckIn Time: " + Convert.ToDateTime(dtCancelPolicy.Rows[0]["check_in_hrs"]).ToString("hh:mm tt");
                        hotelNorms += "# CheckOut Time: " + Convert.ToDateTime(dtCancelPolicy.Rows[0]["check_out_hrs"]).ToString("hh:mm tt");
                        if (dtCancelPolicy.Rows[0]["is_early_check_in"].ToString() == "1")
                        {
                            hotelNorms += "# Early CheckIn Allowed prior " + dtCancelPolicy.Rows[0]["early_checkin_hrs"].ToString() + " hours";
                        }
                        if (dtCancelPolicy.Rows[0]["policies_and_disclaimer"].ToString().Length > 0)
                        {
                            hotelNorms += "#" + dtCancelPolicy.Rows[0]["policies_and_disclaimer"].ToString();
                        }
                        else
                        {
                            hotelNorms += "#All timings are based on local times only.";
                        }
                        if (dtCancelPolicy.Rows[0]["CityName"].ToString().ToLower().Contains("dubai"))
                        {
                            hotelNorms += "#Compulsory Tourism Dirham to be paid by guest directly to the hotel per room per night valid from 31st March 2014 Onward.#Normally it would be AED 20 - 40 per night.";
                        }
                        cancellationpolicies.HotelNorms = hotelNorms;
                        for (int i = 0; i < dtCancelPolicy.Rows.Count; i++)
                        {
                            DataRow row = dtCancelPolicy.Rows[i];
                            string[] hours = row["cnx_before_hours"].ToString().Split(',');
                            string[] chargeTypes = row["cnx_charge_type"].ToString().Split(',');
                            string[] chargeValues = row["cnx_charge_value"].ToString().Split(',');
                            bool refundable = true;
                            if (row["policyStatus"].ToString().Equals("N"))
                            {
                                refundable = false;
                            }
                            CancelPolicies cancelpolicies;
                            IFormatProvider format = new CultureInfo("en-GB", true);
                            if (refundable)
                            {
                                int j = 0;
                                for (j = 0; j < hours.Length; j++)
                                {
                                    //i = j;
                                    cancelpolicies = new CancelPolicies();
                                    //string format = "dd-MM-yyyy";                                   
                                    durationHours = Convert.ToInt32(hours[j]);
                                 
                                    DateTime startdate = DateTime.Parse(clsQuery.CancelPolicyRequest.StartDate,format).AddHours(-durationHours);

                                    // DateTime startdate = DateTime.ParseExact(@clsQuery.CancelPolicyRequest.StartDate, @"d/M/yyyy",CultureInfo.InvariantCulture).AddHours(-durationHours);
                                    // DateTime startdate = DateTime.ParseExact(clsQuery.CancelPolicyRequest.StartDate, "dd-MM-yyyy", null).AddHours(-durationHours);
                                    // DateTime startdate = DateTime.Parse(clsQuery.CancelPolicyRequest.StartDate).AddHours(-durationHours);
                                    cancelpolicies.Refundable = true;
                                    //cancelpolicies.StartDate = DateTime.Parse(Convert.ToString(row["fromdate"]), format).ToString("dd-MM-yyyy");//Convert.ToDateTime( row["fromdate"]).ToString("dd-MM-yyyy");  //startdate.ToString("dd-MM-yyyy");
                                    cancelpolicies.StartDate = DateTime.Parse(Convert.ToString(row["fromdate"])).ToString("dd-MM-yyyy");//Convert.ToDateTime( row["fromdate"]).ToString("dd-MM-yyyy");  //startdate.ToString("dd-MM-yyyy");
                                    cancelpolicies.ChargeValue = Convert.ToInt32(chargeValues[j]);
                                    cancelpolicies.BufferDays = Convert.ToInt32(Math.Round(Convert.ToDecimal(durationHours) / 24));
                                    DateTime enddate = DateTime.Parse(clsQuery.CancelPolicyRequest.StartDate,format);
                                    cancelpolicies.DeadlineDate = DateTime.Parse(Convert.ToString(row["fromdate"])).AddDays(-cancelpolicies.BufferDays).ToString("dd-MM-yyyy");
                                    cancelpolicies.ChargeType = chargeTypes[j];
                                    if (cancelPolicyList.Any(x => x.DeadlineDate == cancelpolicies.DeadlineDate && x.ChargeValue <= cancelpolicies.ChargeValue))
                                    {
                                        cancelPolicyList.RemoveAt(cancelPolicyList.FindIndex(c => c.DeadlineDate == cancelpolicies.DeadlineDate));
                                    }
                                    if (cancelPolicyList.Any(x => x.DeadlineDate == cancelpolicies.DeadlineDate && x.ChargeValue >= cancelpolicies.ChargeValue)|| DateTime.Parse(cancelpolicies.DeadlineDate,format)== DateTime.Parse(clsQuery.CancelPolicyRequest.EndDate,format).AddDays(-1)|| cancelPolicyList.Any(x => x.ChargeType == cancelpolicies.ChargeType && x.ChargeValue == cancelpolicies.ChargeValue)||cancelPolicyList.Any(x=>x.ChargeValue==100))
                                    {
                                        continue;
                                    }
                                    // DateTime enddate = DateTime.ParseExact(@clsQuery.CancelPolicyRequest.StartDate, @"d/M/yyyy", CultureInfo.InvariantCulture).AddHours(durationHours);
                                    //DateTime enddate = DateTime.Parse(clsQuery.CancelPolicyRequest.StartDate).AddHours(-durationHours);
                                    cancelpolicies.EndDate = DateTime.Parse(Convert.ToString(row["todate"])).ToString("dd-MM-yyyy"); // Convert.ToDateTime(row["todate"]).ToString("dd-MM-yyyy");// enddate.ToString("dd-MM-yyyy");                                    
                                    cancelPolicyList.Add(cancelpolicies);
                                }
                                string  leastdate =cancelPolicyList.Find(x => x.ChargeValue == 100).DeadlineDate;
                                if(!string.IsNullOrEmpty(leastdate)&& cancelPolicyList.Any(x=> DateTime.Parse(x.DeadlineDate,format)> DateTime.Parse(leastdate,format)))
                                {
                                    cancelPolicyList.RemoveAll(x => DateTime.Parse(x.DeadlineDate,format) > DateTime.Parse(leastdate,format));
                                }
                            }
                            else
                            {
                                cancelpolicies = new CancelPolicies();
                                cancelpolicies.Refundable = false;
                                //if (cancelPolicyList.Any(x => x.policy == "Non-refundable and full charge will apply once booking is completed."))
                                //{
                                //    Audit.Add(EventType.Exception, Severity.High, 0, "before  Non refund break loop" + Convert.ToString(cancelPolicyList.Count()), "");
                                //    break;
                                //}
                                cancelPolicyList.Clear();
                                cancelpolicies.policy = "Non-refundable and full charge will apply once booking is completed.";
                                cancelPolicyList.Add(cancelpolicies);
                                break;
                            }
                        }
                        Audit.Add(EventType.Exception, Severity.High, 0, "before  Non refund loop"+Convert.ToString(cancelPolicyList.Count()), "");
                        //if (cancelPolicyList.Any(x => !string.IsNullOrEmpty(x.policy) && x.policy.Length > 0))
                        //{
                        //    List<CancelPolicies> policies = new List<CancelPolicies>();
                        //    Audit.Add(EventType.Exception, Severity.High, 0, "enterd in Non refund loop" + Convert.ToString(cancelPolicyList.Count()), "");
                        //    cancellationpolicies.CancellationPolicy = policies.Add(cancelPolicyList.Where(x => !string.IsNullOrEmpty(x.policy) && x.policy.Length > 0).FirstOrDefault())
                        //        //cancelPolicyList.GetRange(0, 1);

                        //}
                        //else
                        //{
                            Audit.Add(EventType.Exception, Severity.High, 0, "enterd in refund loop", "");
                            cancellationpolicies.CancellationPolicy = cancelPolicyList.OrderBy(x => x.DeadlineDate).ToList(); ;
                        //}
                        Audit.Add(EventType.Exception, Severity.High, 0, "After  Non refund loop" + Convert.ToString(cancellationpolicies.CancellationPolicy.Count()), "");
                        hotelCancelPolicylist.Add(cancellationpolicies);
                    }

                    var newdata = hotelCancelPolicylist;
                    var viewModel = new ViewModel(newdata);
                    return await System.Threading.Tasks.Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.CZHotelInventoryAPICancellationPolicyInfo Err :" + ex.ToString(), "0");
                    return new ApplicationResponse().AddError("GetHotelCancelResponseException", ex.ToString());
                }

            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(List<CancellationPolicies> GetCancelResponse)
            {
                Data = new HotelCancellationPolicyDto();
                Data.CancellationPolicyResponse = GetCancelResponse;
            }
            public HotelCancellationPolicyDto Data { get; }


        }
    }
}
