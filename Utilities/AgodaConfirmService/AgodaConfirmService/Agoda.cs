﻿using CT.Configuration;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using System.Configuration;

namespace CT.BookingEngine.GDS_Service
{
    public class Agoda
    {

        /***************COMPLETE BOOKING FLOW OF Agoda---- Develop by Brahmam****************************************
        * 
        * STEP-1:HotelSearch(Availability)
        * STEP-2:Booking (Booking)
        */

        private string XmlPath = string.Empty;
        private string siteId = string.Empty;
        private string apiKey = string.Empty;
        private string language = string.Empty;
        private string clientReferenceNo = string.Empty;
        string currency = string.Empty;
        private StaticData staticInfo = new StaticData();


        private decimal rateOfExchange = 1;
        private Dictionary<string, decimal> exchangeRates;
        private int decimalPoint;
        private string agentCurrency;
        string sourceCountryCode;
        int appUserId;
        string sessionId;
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int AgentDecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }

        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }

        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        public string SessionId
        {
            get
            {
                return sessionId;
            }

            set
            {
                sessionId = value;
            }
        }
        #region Constructor
        public Agoda()
        {
            LoadCredentials();
        }
        private void LoadCredentials()
        {
            //Create Hotel Xml Log path folder per day wise
            XmlPath = ConfigurationSystem.AgodaConfig["XmlLogPath"];
            XmlPath +=DateTime.Now.ToString("dd-MM-yyy") + "\\";
            siteId = ConfigurationSystem.AgodaConfig["SiteID"];
            apiKey = ConfigurationSystem.AgodaConfig["APIKey"];
            language = ConfigurationSystem.AgodaConfig["lang"];
            currency = ConfigurationSystem.AgodaConfig["currency"];
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
        }
        #endregion
        #region commonMethods
        private string SendRequest(string requeststring, string url)
        {
            string responseFromServer = "";
            try
            {
                string contentType = "application/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST"; //Using POST method       
                string postData = requeststring;// GETTING XML STRING...
                request.ContentType = contentType;
                //ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //ServicePointManager.DefaultConnectionLimit = 9999;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                //Basic Authentication required to process the request.               
                string _auth = string.Format("{0}:{1}", siteId, apiKey);
                request.Headers.Add(HttpRequestHeader.Authorization, _auth);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;


                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Agoda Failed to get response from Hotel request : " + ex.Message, ex);
            }
            return responseFromServer;
        }
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        #endregion
        


        #region Booking Detatils
        public void GetBookingHotels()
        {
            DataTable dtHotelBookings = new DataTable();

            SqlParameter[] paramList = new SqlParameter[0];

            dtHotelBookings = DBGateway.FillDataTableSP("usp_GetAgodaBookings", paramList);
            WriteToFile("get bookingslist");
            foreach(DataRow row in dtHotelBookings.Rows)
            {
                try
                {
                    string confirmationNo = row["confirmationNo"].ToString();
                    int hotelId = Convert.ToInt32(row["hotelId"].ToString());
                    int createdBy = Convert.ToInt32(row["createdBy"]);
                    WriteToFile("Genarte Confirm Status for " + confirmationNo);
                    GetBookingDetails(confirmationNo, hotelId, createdBy);
                    

                }
                catch (Exception ex)
                {
                    WriteErrorFile("Error" + ex);
                }
            }
        }
        public void GetBookingDetails(string confirmationNo, int hotelId, int createdBy)
        {
            string request = GenerateBookingDetailsRequest(confirmationNo);
            WriteToFile("Agoda  GenerateBookingDetailsRequest message generated ConfiramtionNo: " + confirmationNo);
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(request);
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmssfff") + "_GetBookingDetailsRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            string resp = string.Empty;
            try
            {
                resp = SendRequest(request, ConfigurationSystem.AgodaConfig["BookingDetails"]);
            }
            catch (Exception ex)
            {
                WriteToFile("Error ConfiramtionNo: " + confirmationNo + " " + ex.Message);
            }
            try
            {
                ReadBookingDetailsResponse(resp,hotelId,createdBy, confirmationNo);
            }
            catch (Exception ex)
            {
                WriteToFile("Error ConfiramtionNo: " + confirmationNo + " " + ex.Message);
            }
        }
        private void ReadBookingDetailsResponse(string response, int hotelId, int createdBy, string conNo)
        {
            TextReader stringRead = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmssfff") + "_BookingDetailsResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("Agoda", "http://xml.agoda.com");
            XmlNode tempNodeErr = xmlDoc.SelectSingleNode("//Agoda:BookingDetailsResponseV2", nsmgr).Attributes["status"];
            if (tempNodeErr != null && tempNodeErr.InnerText == "200") //Here Status 200 Means Response Success
            {
                XmlNodeList bookingResponse = xmlDoc.SelectNodes("//Agoda:BookingDetailsResponseV2/Agoda:Bookings/Agoda:Booking", nsmgr);
                if (bookingResponse != null)
                {
                    foreach (XmlNode nodeRes in bookingResponse)
                    {
                        if (nodeRes != null)
                        {
                            string confirmationNo = string.Empty;
                            string bookingStatus = string.Empty;
                            string vatDescription = string.Empty;
                            string supplierReference = string.Empty;
                            string supplierName = string.Empty;
                            string hotelName = string.Empty;
                            string source = string.Empty;
                            string supplierMessage = string.Empty;
                            XmlNode tempNode = nodeRes.SelectSingleNode("//Agoda:BookingID", nsmgr);
                            if (tempNode != null)
                            {
                                confirmationNo = tempNode.InnerText;
                            }
                            tempNode = nodeRes.SelectSingleNode("//Agoda:Hotel", nsmgr);
                            if (tempNode != null)
                            {
                                hotelName = tempNode.InnerText;
                            }
                            tempNode = nodeRes.SelectSingleNode("//Agoda:Status", nsmgr);
                            if (tempNode != null)
                            {
                                bookingStatus = tempNode.InnerText;
                            }
                            tempNode = nodeRes.SelectSingleNode("//Agoda:SupplierReference", nsmgr);
                            if (tempNode != null)
                            {
                                if (tempNode.InnerText != "Awaiting")
                                {
                                    supplierReference = tempNode.InnerText;
                                    tempNode = nodeRes.SelectSingleNode("//Agoda:Source", nsmgr);
                                    if (tempNode != null)
                                    {
                                        source = tempNode.InnerText;
                                    }
                                    tempNode = nodeRes.SelectSingleNode("//Agoda:SupplierName", nsmgr);
                                    if (tempNode != null)
                                    {
                                        supplierName = tempNode.InnerText;
                                    }
                                    if (source == "Hotel" && supplierName == "YCS")
                                    {
                                        supplierMessage = "Agoda YCS";
                                    }
                                    else if(source == "Hotel" && supplierName != "YCS")
                                    {
                                        supplierMessage = supplierName;
                                    }
                                    else if (source == "Supplier")
                                    {
                                        supplierMessage = supplierName;
                                    }
                                    vatDescription = "Please don't forget to mention the reference " + supplierMessage + "," + supplierReference + " when contacting at " + hotelName;
                                    int bookingId = BookingDetail.GetBookingIdByProductId(hotelId, ProductType.Hotel);
                                    WriteToFile("Booking Id is-" + bookingId);
                                    UpdateHotelServiceDetails(bookingStatus, vatDescription, hotelId, bookingId, createdBy);
                                    WriteToFile("Saving finished at " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                                    WriteToFile("Update Confirm Status for " + confirmationNo);
                                }
                                //else
                                //{
                                break;
                                //}
                            }
                        }
                    }

                }
            }
            else
            {
                XmlNode errorInfo = xmlDoc.SelectSingleNode("BookingDetailsResponseV2/Agoda:ErrorMessages/Agoda:ErrorMessage", nsmgr);
                WriteToFile("Error ConfiramtionNo: " + conNo + " " + errorInfo.InnerText);
            }
        }
        private void UpdateHotelServiceDetails(string remarks, string vatDescription, int hotelId, int bookingId, int createdBy)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if(!string.IsNullOrEmpty(vatDescription))paramList[0] = new SqlParameter("@vatDescription", vatDescription);
                paramList[1] = new SqlParameter("@Remarks", remarks);
                paramList[2] = new SqlParameter("@hotelId", hotelId);
                paramList[3] = new SqlParameter("@BookingId", bookingId);
                paramList[4] = new SqlParameter("@createdBy", createdBy);
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_Agoda_ServiceStatus_Update", paramList);
            }
            catch (Exception ex)
            {
                WriteErrorFile("Remarks Saving Error" + ex);
            }
        }
        private string GenerateBookingDetailsRequest(string bookingId)
        {
            StringBuilder strWriter = new StringBuilder();
            try
            {
                XmlWriter xmlString = XmlWriter.Create(strWriter);
                xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                xmlString.WriteStartElement("BookingDetailsRequestV2", "http://xml.agoda.com");//CancellationRequestV2 started
                xmlString.WriteAttributeString("siteid", siteId);
                xmlString.WriteAttributeString("apikey", apiKey);
                xmlString.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                xmlString.WriteElementString("BookingID", bookingId);
                xmlString.WriteEndElement();//CancellationRequestV2
                xmlString.Close();
            }
            catch (Exception ex)
            {
                WriteToFile("Agoda: Failed generate GenerateBookingDetailsRequest info request for ConfirmationNo : " + bookingId);
            }
            return strWriter.ToString();
        }
        private void WriteToFile(string p)
        {
            string path = ConfigurationManager.AppSettings["Service_Log"];
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(p, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }
        private void WriteErrorFile(string p)
        {
            string path = ConfigurationManager.AppSettings["Error_Log"];
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(p, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }
        #endregion
    }
}

