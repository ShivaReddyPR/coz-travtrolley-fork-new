﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AgodaConfirmService
{
    public partial class Service1 : ServiceBase
    {
        int intervalMinutesInvoke;
        public Service1()
        {
            InitializeComponent();
        }
        protected override void OnStart(string[] args)
        {
            WriteToFile("Agoda Confirmation Service started {0}");
            ScheduleService();
        }

        private void WriteToFile(string p)
        {
            string path = ConfigurationManager.AppSettings["Service_Log"];
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(p, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }
        private Timer Schedular;
        private void ScheduleService()
        {
            try
            {
                Schedular = new Timer(new TimerCallback(SchedularCallback));
                string mode = ConfigurationManager.AppSettings["Mode"].ToUpper();
                WriteToFile("Agoda Confirmation Service Mode: " + mode + " {0}");

                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;
                if (mode.ToUpper() == "INTERVAL")
                {
                    //Get the Interval in Minutes from AppSettings.
                    int intervalMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutes"]);
                    intervalMinutesInvoke = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutesInvoke"]);

                    //Set the Scheduled Time by adding the Interval to Current Time.
                    WriteToFile("Begin Get schedular");
                    CT.BookingEngine.GDS_Service.Agoda agodaApi = new CT.BookingEngine.GDS_Service.Agoda();
                    agodaApi.GetBookingHotels();
                    WriteToFile("End Get schedular");
                    scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next Interval.
                        scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                    }
                }
                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

                WriteToFile("Agoda Confirmation Service scheduled to run after: " + schedule + " {0}");

                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                Schedular.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                WriteErrorFile("Agoda Confirmation Service Error on: {0} " + ex.Message + ex.StackTrace);

                //Stop the Windows Service.
                using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }
         
        private void SchedularCallback(object e)
        {
            WriteToFile("Agoda Confirmation Service Log: {0}");
            ScheduleService();
        }
        protected override void OnStop()
        {
            WriteToFile("Agoda Confirmation Service stopped {0}");
            Schedular.Dispose();
        }
        private void WriteErrorFile(string p)
        {
            string path = ConfigurationManager.AppSettings["Error_Log"];
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(p, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }
    }
}
