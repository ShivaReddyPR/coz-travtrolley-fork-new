using CT.BookingEngine;

/// <summary>
/// Summary description for WSTicketRequest
/// </summary>
public class WSTicketRequest
{
    private string bookingId;
    public string BookingID
    {
        get { return bookingId; }
        set { bookingId = value; }
    }
    private string origin;
    public string Origin
    {
        get { return origin; }
        set { origin = value; }
    }

    private string destination;
    public string Destination
    {
        get { return destination; }
        set { destination = value; }
    }

    private WSSegment[] segment;
    public WSSegment[] Segment
    {
        get { return segment; }
        set { segment = value; }
    }

    private string fareType;
    public string FareType
    {
        get { return fareType; }
        set { fareType = value; }
    }

    private string[] fareBasis;
    public string[] FareBasis
    {
        get { return fareBasis; }
        set { fareBasis = value; }
    }
    private WSFare fare;
    public WSFare Fare
    {
        get { return fare; }
        set { fare = value; }
    }

    private WSPassenger[] passenger;
    public WSPassenger[] Passenger
    {
        get { return passenger; }
        set { passenger = value; }
    }
    private string remarks;
    public string Remarks
    {
        get { return remarks; }
        set { remarks = value; }
    }

    private bool instantTicket;
    public bool InstantTicket
    {
        get { return instantTicket; }
        set { instantTicket = value; }
    }
    private WSPaymentInformation paymentInformation;
    public WSPaymentInformation PaymentInformation
    {
        get { return paymentInformation; }
        set { paymentInformation = value; }
    }
    private BookingSource source;
    public BookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    private string sessionId;
    public string SessionId
    {
        get { return sessionId; }
        set { sessionId = value; }
    }
    private bool isOneWayBooking;
    public bool IsOneWayBooking
    {
        get { return isOneWayBooking; }
        set { isOneWayBooking = value; }
    }

    // Whether Insurance is added
    private bool isInsured = false;
    public bool IsInsured
    {
        get { return isInsured; }
        set { isInsured = value; }
    }
    private string guid;
    public string GUID
    {
        get { return guid; }
        set { guid = value; }
    }

    //Stores promo information
    private WSPromoDetail promoDetail;
    public WSPromoDetail PromoDetail
    {
        get { return promoDetail; }
        set { promoDetail = value; }
    }

    private bool isGSTMandatory;

    public bool IsGSTMandatory
    {
        get
        {
            return isGSTMandatory;
        }

        set
        {
            this.isGSTMandatory = value;
        }
    }

    private string gstCompanyAddress;
    public string GstCompanyAddress
    {
        get
        {
            return gstCompanyAddress;
        }

        set
        {
            this.gstCompanyAddress = value;
        }
    }

    private string gstCompanyContactNumber;
    public string GstCompanyContactNumber
    {
        get
        {
            return gstCompanyContactNumber;
        }

        set
        {
            this.gstCompanyContactNumber = value;
        }
    }

    private string gstCompanyName;
    public string GstCompanyName
    {
        get
        {
            return gstCompanyName;
        }

        set
        {
            this.gstCompanyName = value;
        }
    }

    private string gstNumber;
    public string GstNumber
    {
        get
        {
            return gstNumber;
        }

        set
        {
            this.gstNumber = value;
        }
    }

    private string gstCompanyEmail;
    public string GstCompanyEmail
    {
        get
        {
            return gstCompanyEmail;
        }

        set
        {
            this.gstCompanyEmail = value;
        }
    }


    private bool isLCC;

    public bool IsLCC
    {
        get
        {
            return isLCC;
        }

        set
        {
            this.isLCC = value;
        }
    }

    private int resultId;
    public int ResultId
    {
        get { return resultId; }
        set { resultId = value; }
    }

    public WSTicketRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
