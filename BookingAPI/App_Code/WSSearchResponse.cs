using System.Collections.Generic;

/// <summary>
/// Summary description for WSSearchResponse
/// </summary>
public class WSSearchResponse
{
    private bool isDomestic;
    public bool IsDomestic
    {
        get { return isDomestic; }
        set { isDomestic = value; }
    }
    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    private bool roundTrip;
    public bool RoundTrip
    {
        get { return roundTrip; }
        set { roundTrip = value; }
    }

    private WSResult[] result;
    public WSResult[] Result
    {
        get { return result; }
        set { result = value; }
    }
    private string sessionId;
    public string SessionId
    {
        get { return sessionId; }
        set { sessionId = value; }
    }
    private string errorMsg;
    public string ErrorMsg
    {
        get { return errorMsg; }
        set { errorMsg = value; }
    }
    private CityAirport[][] cityInfo;
    public CityAirport[][] CityInfo
    {
        get { return cityInfo; }
        set { cityInfo = value; }
    }
    public WSSearchResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public class CityAirport
    {
        private string cityName;
        private string cityCode;
        private string countryName;
        private string countryCode;
        private List<AirportInfo> airport;

        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
        public List<AirportInfo> Airport
        {
            get { return airport; }
            set { airport = value; }
        }
    }
    public struct AirportInfo
    {
        public string airportCode;
        public string airportName;
    }
}
