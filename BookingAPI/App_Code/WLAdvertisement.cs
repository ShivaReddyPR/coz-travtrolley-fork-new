using System.Collections.Generic;
using CT.CMS;

// <summary>
/// Summary description for Advertisment
/// </summary>
public class WLAdvertisement
{
    #region private members
    /// <summary>
    /// Unique Id of the Advertisement
    /// </summary>
    private int advertisementId;
    /// <summary>
    /// Title of Ad
    /// </summary>
    private string title;
    /// <summary>
    /// Image Name
    /// </summary>
    private string imageFile;
    /// <summary>
    /// Desciption of Ad
    /// </summary>
    private string adDescription;
    /// <summary>
    /// Unique Id of a agency
    /// </summary>
    private int agencyId;
    /// <summary>
    /// Type of Ads
    /// </summary>
    private AdType adType;
    /// <summary>
    /// List of Ad Blocks
    /// </summary>    
    private List<CT.CMS.AdBlock> adBlocks;

    #endregion

    #region Public  Property
    /// <summary>
    /// Gets or sets Id of the Advertisement
    /// </summary>
    public int AdvertisementId
    {
        get
        {
            return advertisementId;
        }
        set
        {
            advertisementId = value;
        }
    }

    /// <summary>
    /// Title of the Advertisement
    /// </summary>
    public string Title
    {
        get
        {
            return title;
        }
        set
        {
            title = value;
        }
    }

    /// <summary>
    ///FileName of the Image 
    /// </summary>
    public string ImageFile
    {
        get
        {
            return imageFile;
        }
        set
        {
            imageFile = value;
        }
    }

    /// <summary>
    ///Ad desciption
    /// </summary>
    public string AdDescription
    {
        get
        {
            return adDescription;
        }
        set
        {
            adDescription = value;
        }
    }
    /// <summary>
    /// Gets and Sets the AdType 
    /// </summary>
    public AdType AdType
    {
        get
        {
            return adType;
        }
        set
        {
            adType = value;
        }
    }

    /// <summary>
    /// Gets and Sets the Agency 
    /// </summary>
    public int AgencyId
    {
        get
        {
            return agencyId;
        }
        set
        {
            agencyId = value;
        }
    }


    /// <summary>
    /// List of AdBlocks
    /// </summary>
    public List<CT.CMS.AdBlock> AdBlocks
    {
        get
        {
            return adBlocks;
        }
        set
        {
            adBlocks = value;
        }
    }
    string imagePath = CT.Configuration.ConfigurationSystem.WLAdConfig["imagePath"];//"http://localhost:3712/TekTravelWeb/Images/CMS/rahul";

    #endregion

    public WLAdvertisement()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Setting the properties of the WLAdvertisement class
    /// </summary>
    /// <param name="ad">object of Technology.CMS.Advertisement class so as to Populate the properties of WLAdvertisement</param>
    /// <returns></returns>
    public static WLAdvertisement ReadAdvertisement(Advertisement ad, string memberName)
    {
        WLAdvertisement adv = new WLAdvertisement();
        adv.AdDescription = ad.AdDescription;
        adv.AdvertisementId = ad.AdvertisementId;
        adv.Title = ad.Title;
        adv.ImageFile = adv.imagePath + "/" + memberName + "/" + ad.ImageFile;
        adv.AdType = (AdType)ad.AdType;
        adv.AdBlocks = (List<AdBlock>)ad.AdBlocks;
        return adv;
    }
}
/// <summary>
/// Diffrent Type of Ad
/// </summary>
public enum AdType
{
    /// <summary>
    /// Image type of Ad
    /// </summary>
    Image = 1,
    /// <summary>
    /// Text Type of Ad
    /// </summary>
    Text = 2
}

