/// <summary>
/// Summary description for WSFareRuleResponse
/// </summary>
public class WSFareRuleResponse
{
    /// <summary>
    /// private property of fareRule 
    /// </summary>
    private WSFareRule[] fareRules;
    /// <summary>        
    /// public property of fareRule 
    /// </summary>       
    public WSFareRule[] FareRules
    {
        get
        {
            return fareRules;
        }
        set
        {
            fareRules = value;
        }
    }
    /// <summary>
    /// Status of FareRule Response
    /// </summary>
    private WSStatus status;
    /// <summary>
    /// Get or set status of FareRule Response
    /// </summary>
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }
}
