using System;
using CT.BookingEngine;
using System.Collections.Generic;

/// <summary>
/// Summary description for Passenger
/// </summary>
public class WSPassenger
{
    private string title;
    public string Title
    {
        get { return title; }
        set { title = value; }
    }

    private string firstName;
    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }

    private string lastName;
    public string LastName
    {
        get { return lastName; }
        set { lastName = value; }
    }

    private PassengerType type;
    public PassengerType Type
    {
        get { return type; }
        set { type = value; }
    }

    private DateTime dateOfBirth;
    /// <summary>
    /// DOB is mandatory for Hermes Booking
    /// </summary>
    public DateTime DateOfBirth
    {
        get { return dateOfBirth; }
        set { dateOfBirth = value; }
    }

    private WSFare fare;
    public WSFare Fare
    {
        get { return fare; }
        set { fare = value; }
    }

    private Gender gender;

    /// <summary>
    /// Gender Mandatory for Hermes
    /// </summary>
    public Gender Gender
    {
        get { return gender; }
        set { gender = value; }
    }

    private string passportNumber;

    /// <summary>
    /// PassportNumber Mandatory for Hermes in International Booking
    /// </summary>
    public string PassportNumber
    {
        get { return passportNumber; }
        set { passportNumber = value; }
    }

    private DateTime passportExpiry;

    /// <summary>
    /// PassportExpiry Date is mandatory for Hermes in International Booking
    /// </summary>
    public DateTime PassportExpiry
    {
        get { return passportExpiry; }
        set { passportExpiry = value; }
    }

    private string pinCode;

    /// <summary>
    /// First Pax PinCode is mandatory for Hermes
    /// </summary>
    public string PinCode
    {
        get { return pinCode; }
        set { pinCode = value; }
    }

    private string country;
    public string Country
    {
        get { return country; }
        set { country = value; }
    }

    //Lokesh:18May2017,Added reagrding APIS Mandate Fields For B2C Bookings purpose.
    private string nationality;
    public string Nationality
    {
        get { return nationality; }
        set { nationality = value; }
    }

    private string phone;
    public string Phone
    {
        get { return phone; }
        set { phone = value; }
    }

    private string addressLine1;
    public string AddressLine1
    {
        get { return addressLine1; }
        set { addressLine1 = value; }
    }

    private string addressLine2;
    public string AddressLine2
    {
        get { return addressLine2; }
        set { addressLine2 = value; }
    }

    private string email;
    public string Email
    {
        get { return email; }
        set { email = value; }
    }

    private Meal meal;
    public Meal Meal
    {
        get { return meal; }
        set { meal = value; }
    }
    private Seat seat;
    public Seat Seat
    {
        get { return seat; }
        set { seat = value; }
    }
    private string ffAirline;
    public string FFAirline
    {
        get { return ffAirline; }
        set { ffAirline = value; }
    }

    private string ffNumber;
    public string FFNumber
    {
        get { return ffNumber; }
        set { ffNumber = value; }
    }

    /// Baggage Code for G9 (AirArabia)
    /// </summary>
    private string baggageCode;
    public string BaggageCode
    {
        get { return baggageCode; }
        set { baggageCode = value; }
    }
    /// <summary>
    /// BaggageCode Type for FlyDubai (BAGB,BAGL etc)
    /// </summary>
    string baggageType;
    /// <summary>
    /// Category/SSRCategory Id for FlyDubai (99 for Baggage)
    /// </summary>
    string categoryId;
    /// <summary>
    /// Baggage Charge for Flydubai.
    /// </summary>
    List<decimal> baggageCharge;
    /// <summary>
    /// BaggageCode Type for FlyDubai (BAGB,BAGL etc)
    /// </summary>
    public string BaggageType
    {
        get { return baggageType; }
        set { baggageType = value; }
    }
    /// <summary>
    /// Category/SSRCategory Id for FlyDubai (99 for Baggage)
    /// </summary>
    public string CategoryId
    {
        get { return categoryId; }
        set { categoryId = value; }
    }

    public List<decimal> FlyDubaiBaggageCharge
    {
        get { return baggageCharge; }
        set { baggageCharge = value; }
    }

    private List<int> fareInformationID;
    public List<int> FareInformationID
    {
        get { return fareInformationID; }
        set { fareInformationID = value; }
    }
    private WSSegAdditionalInfo[] segmentPTCDetail;
    public WSSegAdditionalInfo[] SegmentPTCDetails
    {
        get { return segmentPTCDetail; }
        set { segmentPTCDetail = value; }
    }

    private bool isLeadPax;
    public bool IsLeadPax
    {
        get { return isLeadPax; }
        set { isLeadPax = value; }
    }

    //Added by Lokesh on 4/04/2018 
    //For G9 Source only for lead Pax 
    //if the customer is travelling from India and provides any GST info (StateCode and Tax RegNo)
    private string gstStateCode;
    public string GSTStateCode
    {
        get { return gstStateCode; }
        set { gstStateCode = value; }
    }
    private string gstTaxRegNo;
    public string GSTTaxRegNo
    {
        get { return gstTaxRegNo; }
        set { gstTaxRegNo = value; }
    }
    /// <summary>
    /// For FraudLabs
    /// </summary>
    private string stateCode;
    public string StateCode
    {
        get { return stateCode; }
        set { stateCode = value; }
    }

    public WSPassenger()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
