﻿using System;
using System.Configuration;
using System.Collections;
using CT.Configuration;
using CT.Core;
using CT.BookingEngine.WhiteLabel;
using System.Xml;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;

/// <summary>
/// Summary description for WSSaveDetailsRequest
/// </summary>
public class WSAddBookingRequest
{
    # region Private Fields 
    PaymentGatewaySource paySource;
    string paymentId;
    decimal paymentAmount;
    bool isDomesticReturn;
    string bookingIdOut;
    string bookingIdIn;
    string outPNR;
    string inPNR;
    bool outTicketed;
    bool inTicketed;
    WLBookingStatus bookingStatus;
    int userMasterId;
    string airlineCodeOut;
    string airlineCodeIn;     
    string flightNumberOut;
    string flightNumberIn;
    string passengerInfo;
    string remarks;
    DateTime depDate;
    DateTime returnDate;
    string sector;
    string email;
    string phone;
    string ipAddress;
    bool isDomestic;
    string orderId;
    //Saving Promo detail in case Ticketing not allowed
    WSPromoDetail promoDetail;
    //Used in saving PromoDetail and will be assigned from BookingResponse in FlightBooking page
    int itineraryId;
    # endregion

    # region Public Properties
    public PaymentGatewaySource PaySource
    {
        set
        {
            paySource = value;
        }
        get
        {
            return paySource;
        }
    }

    public string PaymentId
    {
        set
        {
            paymentId = value;
        }
        get
        {
            return paymentId;
        }
    }

    public decimal PaymentAmount
    {
        set
        {
            paymentAmount = value;
        }
        get
        {
            return paymentAmount;
        }
    }

    public bool IsDomesticReturn
    {
        set
        {
            isDomesticReturn = value;
        }
        get
        {
            return isDomesticReturn;
        }
    }  

    public string BookingIdOut
    {
        set
        {
            bookingIdOut = value;
        }
        get
        {
            return bookingIdOut;
        }
    }
   
    public string BookingIdIn
    {
        set
        {
            bookingIdIn = value;
        }
        get
        {
            return bookingIdIn;
        }
    }

    public string InPNR
    {
        set
        {
            inPNR = value;
        }
        get
        {
            return inPNR;
        }
    }

    public string OutPNR
    {
        set
        {
            outPNR = value;
        }
        get
        {
            return outPNR;
        }
    }

    public bool OutTicketed
    {
        set
        {
            outTicketed = value;
        }
        get
        {
            return outTicketed;
        }
    }

    public bool InTicketed 
    {
        set
        {
            inTicketed = value;
        }
        get
        {
            return inTicketed;
        }
    }

    public WLBookingStatus BookingStatus
    {
        set
        {
            bookingStatus = value;
        }
        get
        {
            return bookingStatus;
        }
    }

    public int UserMasterId
    {
        set
        {
            userMasterId = value;
        }
        get
        {
            return userMasterId;
        }
    }

    public string AirlineCodeOut
    {
        set
        {
            airlineCodeOut = value;
        }
        get
        {
            return airlineCodeOut;
        }
    }

    public string AirlineCodeIn
    {
        set
        {
            airlineCodeIn = value;
        }
        get
        {
            return airlineCodeIn;
        }
    }
         
    public string FlightNumberOut
    {
        set
        {
            flightNumberOut = value;
        }
        get
        {
            return flightNumberOut;
        }
    }

    public string FlightNumberIn
    {
        set
        {
            flightNumberIn = value;
        }
        get
        {
            return flightNumberIn;
        }
    }

    public string PassengerInfo
    {
        set
        {
            passengerInfo = value;
        }
        get
        {
            return passengerInfo.Replace("xmlns=\"http://192.168.0.170/TT/BookingAPI\"","");
        }
    }

    public string Remarks
    {
        set
        {
            remarks = value;
        }
        get
        {
            return remarks;
        }
    }
    public DateTime DepDate
    {
        set
        {
            depDate = value;
        }
        get
        {
            return depDate;
        }
    }

    public DateTime ReturnDate
    {
        set
        {
            returnDate = value;
        }
        get
        {
            return returnDate;
        }
    }

    public string Sector
    {
        get
        {
            return sector;
        }
        set
        {
            sector = value;
        }
    }

    public string Email
    {
        get
        {
            return email;
        }
        set
        {
            email = value;
        }
    }
    public string Phone
    {
        get
        {
            return phone;
        }
        set
        {
            phone = value;
        }
    }
    public string IPAddress
    {
        get
        {
            return ipAddress;
        }
        set
        {
            ipAddress = value;
        }
    }
    public bool IsDomestic
    {
        set
        {
            isDomestic = value;
        }
        get
        {
            return isDomestic;
        }
    }
    public string OrderId
    {
        set
        {
            orderId = value;
        }
        get
        {
            return orderId;
        }
    }
    /// <summary>
    /// Saving Promo detail in case Ticketing not allowed
    /// </summary>
    public WSPromoDetail PromoDetail
    {
        get { return promoDetail; }
        set { promoDetail = value; }
    }

    /// <summary>
    /// Used in saving PromoDetail and will be assigned from BookingResponse in FlightBooking page
    /// </summary>
    public int ItineraryId
    {
        get { return itineraryId; }
        set { itineraryId = value; }
    }
    # endregion

    # region Methods
    
    public WSAddBookingResponse Add(string authenticationValue, ApiCustomerType apiCustomerType, int agentId)
    {
        WSAddBookingResponse saveResponse = new WSAddBookingResponse();
        try
        {
            UserMaster userMaster = new UserMaster(userMasterId);
            LoginInfo loginInfo = UserMaster.GetB2CUser(userMasterId);
            int AgentMasterId = loginInfo.AgentId;
            if (userMaster.AgentId > 0)
            {
                //AgentMasterId = userMaster.AgentId;
            }
            else
            {
                ConfigurationSystem config = new ConfigurationSystem();
                //AgentMasterId = Convert.ToInt32(config.GetSelfAgencyId()["selfAgentMasterId"]);
            }
            AgentMaster AgentMaster = new AgentMaster(loginInfo.AgentId);
            # region sending Email
            string UserMasterEmailId = string.Empty;

            UserPreference userPref = new UserPreference();
            userPref = userPref.GetPreference(UserMasterId, "EMAILID", "SETTINGS");
            if (userPref != null && userPref.Value != null && userPref.Value.Trim() != string.Empty)
            {
                UserMasterEmailId = userPref.Value;
            }
            else
            {
                UserMasterEmailId = userMaster.Email;
            }

            string leadPassengerName = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "1", "");
            if (passengerInfo.Contains("BookingAPI"))
            {
                passengerInfo = passengerInfo.Replace("﻿<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                passengerInfo = passengerInfo.Replace("xmlns=\"http://192.168.0.170/TT/BookingAPI\"", string.Empty);
                passengerInfo = passengerInfo.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                passengerInfo = passengerInfo.Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
            }
            //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "2", "");
            xmlDoc.LoadXml(passengerInfo);
            //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "3", "");
            XmlNode passenger = xmlDoc.SelectSingleNode("Passenger");
            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "4", "");
            string cust_email = string.Empty;
            string fName = string.Empty;
            string lName = string.Empty;
            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "5", "");
            foreach (XmlNode node in passenger)
            {
                if (node.SelectSingleNode("FirstName") != null && node.SelectSingleNode("FirstName").FirstChild != null)
                {
                    fName = node.SelectSingleNode("FirstName").FirstChild.Value;
                }
                if (node.SelectSingleNode("LastName") != null && node.SelectSingleNode("LastName").FirstChild != null)
                {
                    lName = node.SelectSingleNode("LastName").FirstChild.Value;
                }
                if (node.SelectSingleNode("Email") != null && node.SelectSingleNode("Email").FirstChild != null)
                {
                    cust_email = node.SelectSingleNode("Email").FirstChild.Value;
                }
                if (fName != string.Empty && email != string.Empty)
                {
                    break;
                }

            }
            leadPassengerName = fName + " " + lName;

            if (bookingStatus == WLBookingStatus.Failed)
            {

                Hashtable table = new Hashtable(3);
                table.Add("paymentId", "<b>" + paymentId + "</b>");
                table.Add("origin", sector);
                table.Add("paymentAmount", paymentAmount);
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                toArray.Add(UserMasterEmailId);
                //toArray.Add(ConfigurationManager.AppSettings["fromEmail"]);
                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Booking Failed", ConfigurationManager.AppSettings["BookingFailedMessage"] + "\n" + AgentMaster.Name, table);
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "B2C-Smtp is unable to send the reference/trip Id message" + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "B2C-Smtp is unable to send the message " + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                }
            }
            else if(bookingStatus == WLBookingStatus.Duplicate)
            {
                
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                toArray.Add(UserMasterEmailId);

                string message = string.Format("Dear Agent, Your booking for {0} on {2} with passenger {1} has been failed because of Duplicate booking with same Itinerary.", sector, leadPassengerName, depDate);


                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Booking Failed", ConfigurationManager.AppSettings["BookingFailedMessage"] + "\n" + AgentMaster.Name,new Hashtable());
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "B2C-Smtp is unable to send the reference/trip Id message" + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "B2C-Smtp is unable to send the message " + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                }
            }

            # endregion

            WLBookingDetail bDetail = new WLBookingDetail();
            bDetail.AgencyId = AgentMasterId;
            bDetail.AirlineCodeIn = airlineCodeIn;
            bDetail.AirlineCodeOut = airlineCodeOut;
            bDetail.BookingIdIn = Convert.ToInt32(bookingIdIn);
            bDetail.BookingIdOut = Convert.ToInt32(bookingIdOut);
            bDetail.DepDate = depDate;
            bDetail.FlightNumberIn = flightNumberIn;
            bDetail.FlightNumberOut = flightNumberOut;
            bDetail.InPNR = inPNR;
            bDetail.InTicketed = inTicketed;
            bDetail.IsDomesticReturn = isDomesticReturn;
            bDetail.BookingStatus = bookingStatus;
            bDetail.OutPNR = outPNR;
            bDetail.OutTicketed = outTicketed;
            bDetail.PassengerInfo = passengerInfo;
            bDetail.PaymentAmount = paymentAmount;
            bDetail.PaymentId = paymentId;
            bDetail.CreatedBy = (int)(int)userMaster.ID;
            bDetail.LastModifiedBy = (int)(int)userMaster.ID;
            bDetail.Remarks = remarks;
            bDetail.PaySource = (CT.BookingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.BookingEngine.PaymentGatewaySource), Convert.ToString(paySource));

            //To Resolve DateTime Issue
            if (bDetail.ReturnDate <= DateTime.MinValue || bDetail.ReturnDate >= DateTime.MaxValue)
            {
                bDetail.ReturnDate = DateTime.MinValue;
            }
            else
            {
                bDetail.ReturnDate = returnDate;
            }
            bDetail.Email = email;
            bDetail.Phone = phone;
            bDetail.Sector = sector;
            bDetail.IPAddress = ipAddress;
            bDetail.IsDomestic = isDomestic;
            bDetail.OrderId = orderId;
            int referenceId = bDetail.Save();
            //WSAddBookingResponse saveResponse = new WSAddBookingResponse();
            saveResponse.Status = new WSStatus();
            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "reference id : " + referenceId.ToString(), "");
            saveResponse.Status.Category = "SD";

            if (referenceId > 0)
            {
                saveResponse.Status.Description = "Details Saved";
                saveResponse.Status.StatusCode = "0";
                ///
                //ApiCustomer apiCustomer = new ApiCustomer();
                //apiCustomer.Load(authenticationValue, apiCustomerType);
                saveResponse.ReferenceId = referenceId.ToString();
                try
                {

                    #region email tripId
                    //code for email ReferenceId starts here
                    //Code moved to above for failure emails
                    //string leadPassengerName = string.Empty;
                    //XmlDocument xmlDoc = new XmlDocument();
                    ////CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "1", "");
                    //if (passengerInfo.Contains("BookingAPI"))
                    //{
                    //    passengerInfo = passengerInfo.Replace("﻿<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                    //    passengerInfo = passengerInfo.Replace("xmlns=\"http://192.168.0.170/TT/BookingAPI\"", string.Empty);
                    //    passengerInfo = passengerInfo.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                    //    passengerInfo = passengerInfo.Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
                    //}
                    ////CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "2", "");
                    //xmlDoc.LoadXml(passengerInfo);
                    ////CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "3", "");
                    //XmlNode passenger = xmlDoc.SelectSingleNode("Passenger");
                    //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "4", "");
                    //string cust_email = string.Empty;
                    //string fName = string.Empty;
                    //string lName = string.Empty;
                    //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "5", "");
                    //foreach (XmlNode node in passenger)
                    //{
                    //    if (node.SelectSingleNode("FirstName") != null && node.SelectSingleNode("FirstName").FirstChild != null)
                    //    {
                    //        fName = node.SelectSingleNode("FirstName").FirstChild.Value;
                    //    }
                    //    if (node.SelectSingleNode("LastName") != null && node.SelectSingleNode("LastName").FirstChild != null)
                    //    {
                    //        lName = node.SelectSingleNode("LastName").FirstChild.Value;
                    //    }
                    //    if (node.SelectSingleNode("Email") != null && node.SelectSingleNode("Email").FirstChild != null)
                    //    {
                    //        cust_email = node.SelectSingleNode("Email").FirstChild.Value;
                    //    }
                    //    if (fName != string.Empty && email != string.Empty)
                    //    {
                    //        break;
                    //    }

                    //}
                    //leadPassengerName = fName + " " + lName;
                    Hashtable table = new Hashtable(2);
                    table.Add("tripId", "<b>" + referenceId + "</b>");
                    table.Add("passengerName", leadPassengerName);
                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    //if (apiCustomerType == ApiCustomerType.B2B2B)
                    //{
                    //    toArray.Add(UserMasterEmailId);
                    //}
                    //else
                    {
                        toArray.Add(cust_email);
                    }
                    try
                    {
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], UserMasterEmailId, toArray, "Flight Booking", ConfigurationManager.AppSettings["FlightBookingMessage"] + "\n" + AgentMaster.Name, table);
                    }
                    catch (System.Net.Mail.SmtpException ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "B2C-FlightBookingDetail-Smtp is unable to send the reference/trip Id message" + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                    }
                    catch (Exception ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "B2C-FlightBookingDetail-Smtp is unable to send the message " + ex.Message + " Stack Trace = " + ex.StackTrace, "");
                    }
                    //code for email ReferenceId ends here
                    #endregion
                    // CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "completed zi", "");
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Book, Severity.Normal, (int)loginInfo.UserID, ex.ToString(), "");
                }
            }
            else
            {
                saveResponse.Status.Description = "Detail Saving Failed";
                saveResponse.Status.StatusCode = "01";
            }

        }
        catch (Exception ex1)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.High, 0, "WSAddBookingResponse.Add Error message " + ex1.ToString(), "");
        }
        finally
        {
            #region Added Promotion
            try
            {
                if (promoDetail != null)
                {
                    PromoDetails promoDet = new PromoDetails();
                    promoDet.PromoId = promoDetail.PromoId;
                    promoDet.PromoCode = promoDetail.PromoCode;
                    promoDet.ProductId = (int)ProductType.Flight;
                    promoDet.DiscountType = promoDetail.PromoDiscountType;
                    promoDet.DiscountValue = promoDetail.PromoDiscountValue;
                    promoDet.DiscountAmount = promoDetail.PromoDiscountAmount;
                    promoDet.ReferenceId = itineraryId;
                    promoDet.CreatedBy = (int)userMasterId;
                    promoDet.Save();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Ticketing, Severity.High, (int)UserMasterId, "B2C-Flight API :Exception while saving PromoDetails.. Message:" + ex.ToString(), "");
            }

            #endregion
        }
        return saveResponse;
    }
    # endregion


    public WSAddBookingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
