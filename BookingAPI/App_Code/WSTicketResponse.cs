using CT.BookingEngine;

/// <summary>
/// Summary description for WSTicketResponse
/// </summary>
public class WSTicketResponse
{
    private string pnr;
    public string PNR
    {
        get
        {
            return pnr;
        }
        set
        {
            pnr = value;
        }
    }
    private string bookingId;
    public string BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }
    private bool ssrDenied;
    public bool SSRDenied
    {
        get
        {
            return ssrDenied;
        }
        set
        {
            ssrDenied = value;
        }
    }
    private string ssrMessage;
    public string SSRMessage
    {
        get
        {
            return ssrMessage;
        }
        set
        {
            ssrMessage = value;
        }
    }
    private ProductType prodType;
    public ProductType ProdType
    {
        get
        {
            return prodType;
        }
        set
        {
            prodType = value;
        }
    }
    private string confirmationNo;
    public string ConfirmationNo
    {
        get
        {
            return confirmationNo;
        }
        set
        {
            confirmationNo = value;
        }
    }
    private string paymentReferenceNumber;
    public string PaymentReferenceNumber
    {
        get
        {
            return paymentReferenceNumber;
        }
        set
        {
            paymentReferenceNumber = value;
        }
    }
    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public static WSTicketResponse ReadTicketResponse(TicketingResponse ticketingResponse, string ssrMessage, bool ssrDenied, WSStatus status, ProductType prodType, string paymentReferenceNumber)
    {
        WSTicketResponse wsTicketResponse = new WSTicketResponse();
        wsTicketResponse.PNR = ticketingResponse.PNR;
        wsTicketResponse.ssrDenied = ssrDenied;
        wsTicketResponse.ssrMessage = ssrMessage;
        wsTicketResponse.status = status;
        wsTicketResponse.ConfirmationNo = "";
        wsTicketResponse.ProdType = prodType;
        wsTicketResponse.PaymentReferenceNumber = paymentReferenceNumber;
        return wsTicketResponse;
    }


    public WSTicketResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
