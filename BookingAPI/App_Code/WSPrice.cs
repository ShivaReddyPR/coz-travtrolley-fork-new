/// <summary>
/// Summary description for WSPrice
/// </summary>
public class WSPrice
{
    private string currency;
    public string Currency
    {
        get { return currency; }
        set { currency = value; }
    }

    private decimal publishedFare;
    public decimal PublishedFare
    {
        get { return publishedFare; }
        set { publishedFare = value; }
    }

    private decimal tax;
    public decimal Tax
    {
        get { return tax; }
        set { tax = value; }
    }

    private decimal serviceTax;
    public decimal ServiceTax
    {
        get { return serviceTax; }
        set { serviceTax = value; }
    }

    private decimal offerPrice;
    public decimal OfferPrice
    {
        get { return offerPrice; }
        set { offerPrice = value; }
    }

    private decimal discount;
    public decimal Discount
    {
        get { return discount; }
        set { discount = value; }
    }


    //baggage fare for G9 (AirArabia)
    private decimal baggageCharge;
    public decimal BaggageCharge
    {
        get { return baggageCharge; }
        set { baggageCharge = value; }
    }

    public WSPrice()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
