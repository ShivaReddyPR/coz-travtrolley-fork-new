using System;
using System.Collections;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;

/// <summary>
/// Summary description for Segment
/// </summary>
public class WSSegment
{
    const string LINK_AVAILABILITY = "LinkAvailability";
    const string AVAILABILITY_SOURCE = "AvailabilitySource";
    const string PROVIDER_CODE = "ProviderCode";
    const string SEGMENT_REF = "SegmentRef";
    const string POLLED_AVAILABILITY_OPTION = "PolledAvailabilityOption";

    private int segmentIndicator;
    public int SegmentIndicator
    {
        get { return segmentIndicator; }
        set { segmentIndicator = value; }
    }

    private WSAirline airline;
    public WSAirline Airline
    {
        get { return airline; }
        set { airline = value; }
    }

    private string flightNumber;
    public string FlightNumber
    {
        get { return flightNumber; }
        set { flightNumber = value; }
    }

    private string fareClass;
    public string FareClass
    {
        get { return fareClass; }
        set { fareClass = value; }
    }

    private string fareCabinClass;
    public string FareCabinClass
    {
        get { return fareCabinClass; }
        set { fareCabinClass = value; }
    }

    private string airlinePnr;
    public string AirlinePNR
    {
        get { return airlinePnr; }
        set { airlinePnr = value; }
    }

    private WSAirport origin;
    public WSAirport Origin
    {
        get { return origin; }
        set { origin = value; }
    }

    private WSAirport destination;
    public WSAirport Destination
    {
        get { return destination; }
        set { destination = value; }
    }

    private DateTime depTime;
    public DateTime DepTIme
    {
        get { return depTime; }
        set { depTime = value; }
    }

    private DateTime arrTime;
    public DateTime ArrTime
    {
        get { return arrTime; }
        set { arrTime = value; }
    }

    private bool eTicketEligible;
    public bool ETicketEligible
    {
        get { return eTicketEligible; }
        set { eTicketEligible = value; }
    }

    private string duration;
    public string Duration
    {
        get { return duration; }
        set { duration = value; }
    }

    private int stop;
    public int Stop
    {
        get { return stop; }
        set { stop = value; }
    }

    private string craft;
    public string Craft
    {
        get { return craft; }
        set { craft = value; }
    }

    private string status;
    public string Status
    {
        get { return status; }
        set { status = value; }
    }



    /// <summary>
    /// Fare Info Key for a segment (UAPI)
    /// </summary>
    string fareInfoKey;
    public string FareInfoKey
    {
        get
        {
            return fareInfoKey;
        }
        set
        {
            fareInfoKey = value;
        }
    }
    /// <summary>
    /// to store UAPI Departure Date Format
    /// </summary>
    string uapiDepartureTime;
    public string UapiDepartureTime
    {
        get { return uapiDepartureTime; }
        set { uapiDepartureTime = value; }
    }

    /// <summary>
    /// to store UAPI Arrival Date Format
    /// </summary>
    string uapiArrivalTime;
    public string UapiArrivalTime
    {
        get { return uapiArrivalTime; }
        set { uapiArrivalTime = value; }
    }

    /// <summary>
    /// Group - 0 outbount,1 inbound(UAPI)
    /// </summary>
    int group;
    public int Group
    {
        get { return group; }
        set { group = value; }
    }
    /// <summary>
    /// Unique identity number for UAPIS Segment Key
    /// </summary>
    string uapiSegmentRefKey;
    public string UapiSegmentRefKey
    {
        get
        {
            return uapiSegmentRefKey;
        }
        set
        {
            uapiSegmentRefKey = value;
        }
    }
    bool stopOver;
    public bool StopOver
    {
        get
        {
            return stopOver;
        }
        set
        {
            stopOver = value;
        }
    }

    SerializableDictionary<string,object> uapiReservationValues;
    public SerializableDictionary<string, object> UAPIReservationValues
    {
        get { return uapiReservationValues; }
        set { uapiReservationValues = value; }
    }

    string segmentFareType;
    public string SegmentFareType
    {
        get { return segmentFareType; }
        set { segmentFareType = value; }
    }

    public WSSegment()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public FlightInfo GetSegment()
    {
        FlightInfo segment = new FlightInfo();
        segment.Airline = airline.AirlineCode;
        segment.AirlinePNR = airlinePnr;
        segment.ArrivalTime = arrTime;
        segment.ArrTerminal = destination.Terminal;
        segment.BookingClass = fareClass;
        segment.CabinClass = fareCabinClass;
        segment.Craft = craft;
        segment.DepartureTime = depTime;
        segment.DepTerminal = origin.Terminal;
        segment.Destination = destination.GetAirport();
        segment.Duration = TimeSpan.Parse(duration);
        segment.ETicketEligible = eTicketEligible;
        segment.FlightNumber = flightNumber;
        //segment.FlightStatus = status;
        segment.Origin = origin.GetAirport();
        segment.Stops = stop;
        segment.FareInfoKey = fareInfoKey;
        segment.UapiDepartureTime = UapiDepartureTime;// TO strore RPH for G9
        segment.UAPIReservationValues = Convert.ChangeType(segment.UAPIReservationValues, typeof(Hashtable)) as Hashtable;
        return segment;
    }

    public static WSSegment ReadSegment(FlightInfo segment)
    {
        WSSegment wsSegment = new WSSegment();
        try
        {
            WSAirline wsAirline = new WSAirline();
            Airline airline = new Airline();
            try
            {
                airline.Load(segment.Airline);
                wsAirline.AirlineCode = airline.AirlineCode;
                wsAirline.AirlineName = airline.AirlineName;
                wsSegment.airline = wsAirline;
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(EventType.Book, Severity.High, 0, "B2C-Error while loading airline airlineCode " + segment.Airline + ex.Message + " StackTrace : " + ex.StackTrace, "");
            }
            wsSegment.segmentIndicator = segment.Group + 1;
            wsSegment.flightNumber = segment.FlightNumber;
            wsSegment.airlinePnr = segment.AirlinePNR;
            wsSegment.fareClass = segment.BookingClass;
            wsSegment.fareCabinClass = segment.CabinClass;
            wsSegment.origin = WSAirport.ReadAirport(segment.Origin);
            wsSegment.origin.Terminal = segment.DepTerminal;
            wsSegment.depTime = segment.DepartureTime;
            wsSegment.destination = WSAirport.ReadAirport(segment.Destination);
            wsSegment.destination.Terminal = segment.ArrTerminal;
            wsSegment.arrTime = segment.ArrivalTime;
            wsSegment.eTicketEligible = segment.ETicketEligible;
            TimeSpan t = segment.Duration;
            wsSegment.duration = (t.Days * 24 + t.Hours).ToString("00:") + t.Minutes.ToString("00");
            wsSegment.craft = segment.Craft;
            if (segment.FlightStatus == FlightStatus.Confirmed)
            {
                wsSegment.status = "Confirmed";
            }
            else
            {
                wsSegment.status = "Not Confirmed";
            }
            wsSegment.stop = segment.Stops;            
            
            if (segment.FareInfoKey != null) wsSegment.fareInfoKey = segment.FareInfoKey;
            wsSegment.Group = segment.Group;
            if (!string.IsNullOrEmpty(segment.UapiDepartureTime)) wsSegment.UapiDepartureTime = segment.UapiDepartureTime;
            if (!string.IsNullOrEmpty(segment.UapiArrivalTime)) wsSegment.uapiArrivalTime = segment.UapiArrivalTime;
            if (segment.UapiSegmentRefKey != null) wsSegment.UapiSegmentRefKey = segment.UapiSegmentRefKey;
            wsSegment.stopOver = segment.StopOver;
            wsSegment.segmentFareType = segment.SegmentFareType;
            wsSegment.uapiReservationValues = new SerializableDictionary<string, object>();
            if (segment.UAPIReservationValues != null)
            {
                wsSegment.uapiReservationValues[LINK_AVAILABILITY] = segment.UAPIReservationValues[LINK_AVAILABILITY];
                wsSegment.uapiReservationValues[POLLED_AVAILABILITY_OPTION] = segment.UAPIReservationValues[POLLED_AVAILABILITY_OPTION];
                wsSegment.uapiReservationValues[AVAILABILITY_SOURCE] = segment.UAPIReservationValues[AVAILABILITY_SOURCE];
                wsSegment.uapiReservationValues[PROVIDER_CODE] = segment.UAPIReservationValues[PROVIDER_CODE];
                wsSegment.uapiReservationValues[SEGMENT_REF] = segment.UAPIReservationValues[SEGMENT_REF];
            }
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.Exception, Severity.High, 1, ex.ToString(), "");
        }
        return wsSegment;
    }
    /// <summary>
    /// Method to send error mail
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="UserMaster"></param>
    private void SendErrorMail(string subject, Exception ex)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        Hashtable hostPort = con.GetHostPort();
        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
        string fromEmailId = hostPort["fromEmail"].ToString();
        if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
        {
            try
            {
                string mailMessage = string.Empty;              
                mailMessage = "\n\nException: " + ex.Message;
                mailMessage += "\n\nStack trace: " + ex.StackTrace;
                Email.Send(fromEmailId, errorNotificationMailingId, "Error in Booking API " + subject, mailMessage);
            }
            catch (Exception excep)
            {
                CT.Core.Audit.Add(EventType.Book, Severity.High, 0, "B2C-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
            }
        }
    }
}
