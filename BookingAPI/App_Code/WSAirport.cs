using CT.BookingEngine;

/// <summary>
/// Summary description for WSAirport
/// </summary>
public class WSAirport
{
    private string airportCode;
    public string AirportCode
    {
        get { return airportCode; }
        set { airportCode = value; }
    }

    private string airportName;
    public string AirportName
    {
        get { return airportName; }
        set { airportName = value; }
    }

    private string terminal;
    public string Terminal
    {
        get { return terminal; }
        set { terminal = value; }
    }

    private string cityCode;
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }

    private string cityName;
    public string CityName
    {
        get { return cityName; }
        set { cityName = value; }
    }

    private string countryCode;
    public string CountryCode
    {
        get { return countryCode; }
        set { countryCode = value; }
    }

    private string countryName;
    public string CountryName
    {
        get { return countryName; }
        set { countryName = value; }
    }

    public WSAirport()
    {
        
    }

    public Airport GetAirport()
    {
        Airport airport = new Airport();
        airport.AirportCode = airportCode;
        airport.AirportName = airportName;
        airport.CityCode = cityCode;
        airport.CityName = cityName;
        airport.CountryCode = countryCode;
        airport.CountryName = countryName;
        return airport;
    }

    public static WSAirport ReadAirport(Airport airport)
    {
        WSAirport wsAirport = new WSAirport();
        wsAirport.AirportCode = airport.AirportCode;
        wsAirport.AirportName = airport.AirportName;
        wsAirport.CityCode = airport.CityCode;
        wsAirport.CityName = airport.CityName;
        wsAirport.CountryCode = airport.CountryCode;
        wsAirport.CountryName = airport.CountryName;
        return wsAirport;
    }
}
