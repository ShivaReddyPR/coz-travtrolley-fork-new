using CT.BookingEngine.WhiteLabel;

/// <summary>
/// Summary description for WSSaveIPAddressResponse
/// </summary>
public class WSSaveIPAddressResponse
{
    private IPAddressStatus ipAddressStatus;
    public IPAddressStatus IPAddressStatus
    {
        get { return ipAddressStatus; }
        set { ipAddressStatus = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public WSSaveIPAddressResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
