/// <summary>
/// Summary description for WSGetFareQuoteResponse
/// </summary>
public class WSGetFareQuoteResponse
{
    private WSPTCFare[] fare;
    public WSPTCFare[] Fare
    {
        get { return fare; }
        set { fare = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }
    //added by Shiva 10 Apr 2018
    private WSResult result;
    public WSResult Result
    {
        get { return result; }
        set { result = value; }
    }
    /// <summary>
    /// Added for Verifying & Updating Baggage fares returned in BaggagePriceQuote in G9
    /// </summary>
    private System.Collections.Generic.List<decimal> paxBaggageFares;
    /// <summary>
    /// Added for Verifying & Updating Baggage fares returned in BaggagePriceQuote in G9
    /// </summary>
    public System.Collections.Generic.List<decimal> PaxBaggageFares
    {
        get
        {
            return paxBaggageFares;
        }
        set
        {
            paxBaggageFares = value;
        }
    }

    public WSGetFareQuoteResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
