using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Diagnostics;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;


/// <summary>
/// Summary description for WSGetBookingResponse
/// </summary>
public class WSGetBookingResponse
{
    private string bookingId;
    public string BookingId
    {
        get { return bookingId; }
        set { bookingId = value; }
    }

    private WSAgentMaster agentMaster;
    public WSAgentMaster AgentMaster
    {
        get { return agentMaster; }
        set { agentMaster = value; }
    }

    private string pnr;
    public string PNR
    {
        get { return pnr; }
        set { pnr = value; }
    }
    private string airlinePnr;
    public string AirlinePNR
    {
        get { return airlinePnr; }
        set { airlinePnr = value; }
    }
    private string errorMsg;
    public string ErrorMsg
    {
        get { return errorMsg; }
        set { errorMsg = value; }
    }

    private string remarks;
    public string Remarks
    {
        get { return remarks; }
        set { remarks = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    private WSFare fare;
    public WSFare Fare
    {
        get { return fare; }
        set { fare = value; }
    }

    private WSPassenger[] passenger;
    public WSPassenger[] Passenger
    {
        get { return passenger; }
        set { passenger = value; }
    }

    private string origin;
    public string Origin
    {
        get { return origin; }
        set { origin = value; }
    }

    private string destination;
    public string Destination
    {
        get { return destination; }
        set { destination = value; }
    }

    private WSSegment[] segment;
    public WSSegment[] Segment
    {
        get { return segment; }
        set { segment = value; }
    }

    private string fareType;
    public string FareType
    {
        get { return fareType; }
        set { fareType = value; }
    }

    private string[] fareBasis;
    public string[] FareBasis
    {
        get { return fareBasis; }
        set { fareBasis = value; }
    }

    private WSTicket[] ticket;
    public WSTicket[] Ticket
    {
        get { return ticket; }
        set { ticket = value; }
    }

    private BookingSource source;
    public BookingSource Source
    {
        get { return source; }
        set { source = value; }
    }

    //Stores promo information
    private WSPromoDetail promoDetail;
    public WSPromoDetail PromoDetail
    {
        get { return promoDetail; }
        set { promoDetail = value; }
    }

    private DateTime issueDate;
    public DateTime IssueDate
    {
        get{ return issueDate;}
        set { issueDate = value; }
    }

    public WSGetBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Whether Insurance is added or not 
    /// </summary>
    private bool isInsured = true;
    public bool IsInsured
    {
        get
        {
            return isInsured;
        }
        set
        {
            isInsured = value;
        }
    }   


    private bool isLCC;

    public bool IsLCC
    {
        get
        {
            return isLCC;
        }

        set
        {
            isLCC = value;
        }
    }


    public void Load(string bookId, int AgentMasterId, bool isWhiteLabel)
    {
        Trace.TraceInformation("WSGetBookingResponse.Load entered. BookingId = " + bookId);
        //
        BookingDetail bookingDetail;
        FlightItinerary itinerary;
        DataTable dtPromoDetails;
        string bookingIdRegex = "^[0-9]+$";
        bool isInteger = false;
        int flightId;
        if (Regex.IsMatch(bookId.Trim(), bookingIdRegex))
        {
            isInteger = true;
        }
        if (!isInteger)
        {
            try
            {
                flightId = FlightItinerary.GetFlightId(bookId);
                bookId = BookingDetail.GetBookingIdByProductId(flightId, ProductType.Flight).ToString();
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Invalid BookingId");
            }
            catch (SqlException sqlExec)
            {
                throw new Exception("Query error occured", sqlExec);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }        
        try
        {
            bookingDetail = new BookingDetail(Convert.ToInt32(bookId));
            itinerary = new FlightItinerary(bookingDetail.ProductsList[0].ProductId);
            dtPromoDetails = PromoDetails.GetPromoDetails(itinerary.BookingId);

        }
        catch (ArgumentException ex)
        {
            throw new ArgumentException(ex.Message);
        }
        catch (SqlException sqlExec)
        {
            throw new Exception("Query error occured",sqlExec);
        }
        if (AgentMasterId > 0 && bookingDetail.AgencyId != AgentMasterId)
        {
            throw new ArgumentException("Invalid BookingId");
        }
        pnr = itinerary.PNR;
        issueDate = itinerary.CreatedOn;
        if (itinerary.AirLocatorCode != null)
        {
            airlinePnr = itinerary.AirLocatorCode;//Airline PNR for UAPI
        }
        else
        {
            airlinePnr = "";
        }

        try
        {
            if (dtPromoDetails != null && dtPromoDetails.Rows.Count > 0)
            {
                promoDetail = new WSPromoDetail();
                promoDetail.PromoCode = dtPromoDetails.Rows[0]["Promo_Code"].ToString();
                promoDetail.PromoDiscountAmount = Convert.ToDecimal(dtPromoDetails.Rows[0]["Promo_Discount_Amount"]);
            }
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.Exception, Severity.High, 1, "(B2C-Flight)Failed to retrieve promo information for Flight Booking. Reason : " + ex.ToString(), "");
        }

        IsLCC = itinerary.IsLCC;
        bookingId = bookId;
        WSStatus wsStatus = new WSStatus();
        wsStatus.Category = "RT";
        wsStatus.Description = bookingDetail.Status.ToString();
        //0 in this line is being added to make it a two digit number like 01,02,03 etc.
        if (Convert.ToInt32(bookingDetail.Status) < 10)
        {
            wsStatus.StatusCode = "0" + Convert.ToString((int)bookingDetail.Status);
        }
        else
        {
            wsStatus.StatusCode = Convert.ToString((int)bookingDetail.Status);
        }
        status = wsStatus;
        //TODO: In case there is an additon of more status in BookingStatus please check that we don't need to add this 0 for that here as then it will cross 9 and become a two digit number by itself.
        origin = itinerary.Origin;
        destination = itinerary.Destination;
        fareBasis = new string[itinerary.FareRules.Count];

        isInsured = itinerary.IsInsured;
        for (int i = 0; i < itinerary.FareRules.Count; i++)
        {
            fareBasis[i] = itinerary.FareRules[i].FareBasisCode;
        }
        passenger = new WSPassenger[itinerary.Passenger.Length];
        WSFare fare = new WSFare();
        fare.Currency = itinerary.Passenger[0].Price.Currency;
        bool isLCC = true;
        if (itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.WorldSpan || itinerary.FlightBookingSource == BookingSource.Galileo)
        {
            isLCC = false;
        }
        List<Ticket> ticketList = CT.BookingEngine.Ticket.GetTicketList(itinerary.FlightId);
        ticket = new WSTicket[ticketList.Count];
        
        for (int i = 0; i < itinerary.Passenger.Length; i++)
        {
            passenger[i] = new WSPassenger();
            fare.BaseFare += itinerary.Passenger[i].Price.PublishedFare;
            fare.Tax += itinerary.Passenger[i].Price.Tax;
            fare.AdditionalTxnFee += itinerary.Passenger[i].Price.AdditionalTxnFee;
            fare.AgentCommission += itinerary.Passenger[i].Price.AgentCommission;
            fare.Discount += itinerary.Passenger[i].Price.WhiteLabelDiscount;
            fare.PLB += itinerary.Passenger[i].Price.AgentPLB;
            if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.IsLCC)
            {
                fare.BaggageCharge += itinerary.Passenger[i].Price.BaggageCharge;
                passenger[i].BaggageCode = itinerary.Passenger[i].BaggageCode;
            }

            if (itinerary.IsInsured)
            {
                fare.InsuranceAmount += itinerary.Passenger[i].Price.InsuranceAmount;
                //passenger[i].Fare.InsuranceAmount = itinerary.Passenger[i].Price.InsuranceAmount;
            }
            if (isWhiteLabel)
            {
                if (ticketList.Count - 1 >= i)
                {
                    fare.OtherCharges += itinerary.Passenger[i].Price.OtherCharges + itinerary.Passenger[i].Price.WLCharge + ticketList[i].ServiceFee;
                }
                else
                {
                    fare.OtherCharges += itinerary.Passenger[i].Price.OtherCharges + itinerary.Passenger[i].Price.WLCharge;
                }
            }
            else
            {
                fare.OtherCharges += itinerary.Passenger[i].Price.OtherCharges + itinerary.Passenger[i].Price.WLCharge;
            }
            fare.PublishedPrice += itinerary.Passenger[i].Price.PublishedFare;
            fare.ServiceTax += itinerary.Passenger[i].Price.SeviceTax;
            fare.OutputVAT += itinerary.Passenger[i].Price.OutputVATAmount;
            passenger[i].AddressLine1 = itinerary.Passenger[i].AddressLine1;
            passenger[i].AddressLine2 = itinerary.Passenger[i].AddressLine2;
            passenger[i].Country = itinerary.Passenger[i].Country.CountryCode;
            passenger[i].DateOfBirth = itinerary.Passenger[i].DateOfBirth;
            passenger[i].Email = itinerary.Passenger[i].Email;
            passenger[i].IsLeadPax = itinerary.Passenger[i].IsLeadPax;
            WSFare paxFare = new WSFare();
            paxFare.BaseFare = itinerary.Passenger[i].Price.PublishedFare;
            if (isWhiteLabel)
            {
                if (ticketList.Count - 1 >= i)
                {
                    paxFare.OtherCharges = itinerary.Passenger[i].Price.OtherCharges + itinerary.Passenger[i].Price.WLCharge + ticketList[i].ServiceFee;
                }
                else
                {
                    paxFare.OtherCharges = itinerary.Passenger[i].Price.OtherCharges + itinerary.Passenger[i].Price.WLCharge;
                }
            }
            else
            {
                paxFare.OtherCharges = itinerary.Passenger[i].Price.OtherCharges;
            }
            paxFare.ServiceTax = itinerary.Passenger[i].Price.SeviceTax;
            paxFare.Tax = itinerary.Passenger[i].Price.Tax;
            paxFare.PLB = itinerary.Passenger[i].Price.AgentPLB;
            paxFare.Discount = itinerary.Passenger[i].Price.WhiteLabelDiscount;
            paxFare.AirTransFee = itinerary.Passenger[i].Price.AirlineTransFee;
            paxFare.ChargeBU = itinerary.Passenger[i].Price.ChargeBU.ToArray();
            paxFare.Currency = itinerary.Passenger[i].Price.Currency;
            paxFare.AdditionalTxnFee = itinerary.Passenger[i].Price.AdditionalTxnFee;
            paxFare.Markup = itinerary.Passenger[i].Price.Markup;
            paxFare.B2CMarkup = itinerary.Passenger[i].Price.B2CMarkup;
            paxFare.B2CMarkupType = itinerary.Passenger[i].Price.B2CMarkupType;
            paxFare.B2CMarkupValue = itinerary.Passenger[i].Price.B2CMarkupValue;
            paxFare.OutputVAT = itinerary.Passenger[i].Price.OutputVATAmount;
            if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.IsLCC)
            {
                paxFare.BaggageCharge = itinerary.Passenger[i].Price.BaggageCharge;
            }
            if (itinerary.IsInsured)
            {
                paxFare.InsuranceAmount = itinerary.Passenger[i].Price.InsuranceAmount;
            }
            //if (isLCC)
            //{
            //    paxFare.FuelSurcharge = Util.GetFuelSurcharge(itinerary.AirlineCode, itinerary.Origin, itinerary.Destination);
            //}
            //else
            {
                paxFare.FuelSurcharge = 0;
            }
            passenger[i].Fare = paxFare;
            passenger[i].FFAirline = itinerary.Passenger[i].FFAirline;
            passenger[i].FFNumber = itinerary.Passenger[i].FFNumber;
            passenger[i].FirstName = itinerary.Passenger[i].FirstName;
            passenger[i].LastName = itinerary.Passenger[i].LastName;
            passenger[i].Title = itinerary.Passenger[i].Title;
            passenger[i].Meal = itinerary.Passenger[i].Meal;
            passenger[i].Seat = itinerary.Passenger[i].Seat;
            passenger[i].PassportNumber = itinerary.Passenger[i].PassportNo;
            passenger[i].Phone = itinerary.Passenger[i].CellPhone;
            passenger[i].Type = itinerary.Passenger[i].Type;

            /**********************************************************************************
             *  For showing GDS Baggage details in case of HOLD bookings by shiva 25 Oct 2017
             * ********************************************************************************/
            string paxType = string.Empty;
            if (itinerary.Passenger[i].Type == PassengerType.Adult)
                paxType = "ADT";
            else if (itinerary.Passenger[i].Type == PassengerType.Child)
                paxType = "CNN";
            if (itinerary.Passenger[i].Type == PassengerType.Infant)
                paxType = "INF";
            if (itinerary.Passenger[i].Type == PassengerType.Senior)
                paxType = "SNR";
            List<SegmentPTCDetail> ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(itinerary.FlightId, paxType);
            passenger[i].SegmentPTCDetails = new WSSegAdditionalInfo[ptcDetails.Count];
            for (int j = 0; j < ptcDetails.Count; j++)
            {
                passenger[i].SegmentPTCDetails[j] = new WSSegAdditionalInfo();
                passenger[i].SegmentPTCDetails[j].Baggage = ptcDetails[j].Baggage;
                passenger[i].SegmentPTCDetails[j].FareBasis = ptcDetails[j].FareBasis;
                passenger[i].SegmentPTCDetails[j].FlightKey = ptcDetails[j].FlightKey;
                passenger[i].SegmentPTCDetails[j].NVA = ptcDetails[j].NVA;
                passenger[i].SegmentPTCDetails[j].NVB = ptcDetails[j].NVB;
            }
            /************************************* END **************************************/
        }
        this.fare = fare;

        segment = new WSSegment[itinerary.Segments.Length];
        for (int i = 0; i < itinerary.Segments.Length; i++)
        {
            segment[i] = WSSegment.ReadSegment(itinerary.Segments[i]);
            if (segment[i].AirlinePNR == null || segment[i].AirlinePNR.Trim() == "")
            {
                segment[i].AirlinePNR = itinerary.PNR;
            }
        }
            
        for (int i = 0; i < ticketList.Count; i++)
        {
            ticket[i] = new WSTicket();
            
            //TODO Airline PNR to be moved to segment.
            //ticket[i].AirlinePNR = itinerary.Segments[i].air
            ticket[i].FirstName = ticketList[i].PaxFirstName;
            ticket[i].LastName = ticketList[i].PaxLastName;
            ticket[i].Title = ticketList[i].Title;
            ticket[i].IssueDate = ticketList[i].IssueDate;
            ticket[i].PaxType = ticketList[i].PaxType;
            string paxType=string.Empty;
            if (ticket[i].PaxType == PassengerType.Adult)
                paxType = "ADT";
            else if (ticket[i].PaxType == PassengerType.Child)
                paxType = "CNN";
            if (ticket[i].PaxType == PassengerType.Infant)
                paxType = "INF";
            if (ticket[i].PaxType == PassengerType.Senior)
                paxType = "SNR";
            ticketList[i].PtcDetail = SegmentPTCDetail.GetSegmentPTCDetail(itinerary.FlightId, paxType );
            if (ticketList[i].PtcDetail != null)
            {
                ticket[i].SegmentAdditionalInfo = new WSSegAdditionalInfo[ticketList[i].PtcDetail.Count];
                for (int j = 0; j < ticketList[i].PtcDetail.Count; j++)
                {
                    ticket[i].SegmentAdditionalInfo[j] = new WSSegAdditionalInfo();
                    ticket[i].SegmentAdditionalInfo[j].Baggage = ticketList[i].PtcDetail[j].Baggage;
                    ticket[i].SegmentAdditionalInfo[j].FareBasis = ticketList[i].PtcDetail[j].FareBasis;
                    ticket[i].SegmentAdditionalInfo[j].FlightKey = ticketList[i].PtcDetail[j].FlightKey;
                    ticket[i].SegmentAdditionalInfo[j].NVA = ticketList[i].PtcDetail[j].NVA;
                    ticket[i].SegmentAdditionalInfo[j].NVB = ticketList[i].PtcDetail[j].NVB;
                }
            }
            ticket[i].TicketId = ticketList[i].TicketId;
            ticket[i].TicketNumber = ticketList[i].TicketNumber;
            ticket[i].ValidatingAirline = ticketList[i].ValidatingAriline;
            
            WSFare wsFare = new WSFare();
            wsFare.BaseFare = ticketList[i].Price.PublishedFare;
            wsFare.Tax = ticketList[i].Price.Tax;
            wsFare.Discount = ticketList[i].Price.WhiteLabelDiscount;
            if (isWhiteLabel)
            {
                wsFare.OtherCharges = ticketList[i].Price.OtherCharges + ticketList[i].Price.WLCharge + ticketList[i].ServiceFee;
            }
            else
            {
                wsFare.OtherCharges = ticketList[i].Price.OtherCharges;
            }
            wsFare.AdditionalTxnFee = ticketList[i].Price.AdditionalTxnFee;
            wsFare.ChargeBU = ticketList[i].Price.ChargeBU.ToArray();
            wsFare.Currency = ticketList[i].Price.Currency;
            wsFare.ServiceTax = ticketList[i].Price.SeviceTax;
            wsFare.PLB = ticketList[i].Price.AgentPLB;
            wsFare.OutputVAT = ticketList[i].Price.OutputVATAmount;
            ticket[i].Fare = wsFare;
        }

        AgentMaster crAgentMaster = new AgentMaster();
        
        AgentMaster = new WSAgentMaster();
        AgentMaster.AddressLine1 = crAgentMaster.Address;
        AgentMaster.AddressLine2 = "";
        AgentMaster.Email = crAgentMaster.Email1;
        AgentMaster.Name = crAgentMaster.Name;
        AgentMaster.Phone = crAgentMaster.Phone1;
        AgentMaster.Fax = crAgentMaster.Fax;
        AgentMaster.City = crAgentMaster.City;
        AgentMaster.PIN = crAgentMaster.POBox;

        fareType = itinerary.FareType;
        Source = itinerary.FlightBookingSource;

        Trace.TraceInformation("WSGetBookingResponse.Load exiting");
    }

}
