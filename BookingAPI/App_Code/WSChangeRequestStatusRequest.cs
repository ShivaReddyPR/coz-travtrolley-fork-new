using System;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;


/// <summary>
/// Summary description for WSChangeRequestStatusRequest
/// </summary>
public class WSChangeRequestStatusRequest
{
    private string requestId;
    public string RequestId
    {
        get { return requestId; }
        set { requestId = value; }
    }

    public static WSChangeRequestStatusResponse GetCRStatus(WSChangeRequestStatusRequest request, AgentMaster AgentMaster)
    {
        int value;
        WSChangeRequestStatusResponse response = new WSChangeRequestStatusResponse();
        WSStatus status = new WSStatus();
        if (!int.TryParse(request.requestId, out value))
        {
            status.Category = "GC";
            status.Description = "Invalid ServiceRequest Id";
            status.StatusCode = "01";
            response.RequestId = request.requestId;
            response.ChangeRequestStatus = ChangeRequestStatus.Pending;
            response.Status = status;
            return response;
        }
        ServiceRequest serviceRequest;        
        try
        {
            serviceRequest = new ServiceRequest(value);
        }
        catch(Exception ex)
        {
            status.Category = "GC";
            status.Description = "Invalid ServiceRequest Id";
            status.StatusCode = "01";
            response.RequestId = request.requestId;
            response.ChangeRequestStatus = ChangeRequestStatus.Pending;
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, 0, "Problem in retrieving change request " + ex.Message +ex.StackTrace, "");
            return response;
        }
        if (serviceRequest != null)
        {
            if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Unassigned)
            {
                response.ChangeRequestStatus = ChangeRequestStatus.Pending;
            }
            else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Assigned)
            {
                response.ChangeRequestStatus = ChangeRequestStatus.InProgress;
            }
            else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Acknowledged)
            {
                response.ChangeRequestStatus = ChangeRequestStatus.InProgress;
            }
            else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Completed)
            {
                response.ChangeRequestStatus = ChangeRequestStatus.Processed;
            }
            else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Closed)
            {
                response.ChangeRequestStatus = ChangeRequestStatus.Processed;
            }
            else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Rejected)
            {
                response.ChangeRequestStatus = ChangeRequestStatus.Rejected;
            }
            else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Pending)
            {
                response.ChangeRequestStatus = ChangeRequestStatus.Pending;
            }
            else
            {
                response.ChangeRequestStatus = ChangeRequestStatus.Pending;
            }
        }
        Ticket ticket = new Ticket();
        ticket.Load(Convert.ToInt32(serviceRequest.ReferenceId));
        if (ticket != null)
        {
            decimal ticketPrice = (AgentMaster.AgentType == (int)Agencytype.Service) ? ticket.Price.GetServiceAgentPrice() : ticket.Price.GetAgentPrice();
            CancellationCharges cancelCharge = new CancellationCharges();
            cancelCharge.Load(serviceRequest.ReferenceId, ProductType.Flight);
            decimal amount = ticketPrice - cancelCharge.AdminFee - cancelCharge.SupplierFee - ticket.Price.OtherCharges - ticket.Price.Discount;
            status.Category = "GC";
            status.Description = "Sucessfull";
            status.StatusCode = "02";
            response.CancellationCharge = cancelCharge.AdminFee + cancelCharge.SupplierFee;
            if (response.ChangeRequestStatus == ChangeRequestStatus.Processed && (ticket.Status.Trim().ToLower() == "voided" || ticket.Status.Trim().ToLower() == "refunded") || ticket.Status.Trim().ToLower() == "cancelled")
            {
                response.RefundedAmount = amount;
            }
            else
            {
                response.RefundedAmount = 0;
            }
            response.RequestId = request.requestId;
            response.Status = status;            
        }
        else
        {
            status.Category = "GC";
            response.RequestId = request.requestId;
            status.Description = "Invalid ServiceRequestID / TicketID";
            status.StatusCode = "03";
            response.Status = status;            
        }
        return response;

    }
}
