/// <summary>
/// Summary description for WSGetScoreResponse1
/// </summary>
public class WSGetScoreResponse
{
    private ScoreStaus scoreStatus;
    public ScoreStaus ScoreStatus
    {
        get
        {
            return scoreStatus;
        }
        set
        {
            scoreStatus = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }
}
