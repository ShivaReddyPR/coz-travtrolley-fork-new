/// <summary>
/// Summary description for WSSendCahngeRequestResponse
/// </summary>
public class WSSendChangeRequestResponse
{
    private string changeRequestId;
    public string ChangeRequestId
    {
        get { return changeRequestId; }
        set { changeRequestId = value; }
    }
    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }
    
}
