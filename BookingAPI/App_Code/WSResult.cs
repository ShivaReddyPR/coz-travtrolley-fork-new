using System;
using System.Collections.Generic;
using CT.BookingEngine;


/// <summary>
/// Summary description for WSFlightResult
/// </summary>
public class WSResult
{
    private int tripIndicator;
    public int TripIndicator
    {
        get { return tripIndicator; }
        set { tripIndicator = value; }
    }

    private WSFare fare;
    public WSFare Fare
    {
        get { return fare; }
        set { fare = value; }
    }

    private WSPTCFare[] fareBreakdown;
    public WSPTCFare[] FareBreakdown
    {
        get { return fareBreakdown; }
        set { fareBreakdown = value; }
    }

    private string origin;
    public string Origin
    {
        get { return origin; }
        set { origin = value; }
    }

    private string destination;
    public string Destination
    {
        get { return destination; }
        set { destination = value; }
    }

    private WSSegment[] segment;
    public WSSegment[] Segment
    {
        get { return segment; }
        set { segment = value; }
    }

    private string ibDuration;
    public string IbDuration
    {
        get { return ibDuration; }
        set { ibDuration = value; }
    }

    private string obDuration;
    public string ObDuration
    {
        get { return obDuration; }
        set { obDuration = value; }
    }

    private BookingSource source;
    public BookingSource Source
    {
        get { return source; }
        set { source = value; }
    }

    private string[] fareBasis;
    public string[] FareBasis
    {
        get { return fareBasis; }
        set { fareBasis = value; }
    }
    private bool isLcc;
    public bool IsLcc
    {
        get { return isLcc; }
        set { isLcc = value; }
    }
    private int ibSegCount;
    public int IbSegCount
    {
        get { return ibSegCount; }
        set { ibSegCount = value; }
    }
    private int obSegCount;
    public int ObSegCount
    {
        get { return obSegCount; }
        set { obSegCount = value; }
    }
    private bool isRefundable;
    public bool IsRefundable
    {
        get { return isRefundable; }
        set { isRefundable = value; }
    }

    private UAPIdll.Air40.AirPricingSolution uapiPricingSolution;
    // For UAPI to get price in Create Reservation Res
    public UAPIdll.Air40.AirPricingSolution UapiPricingSolution
    {
        get{return uapiPricingSolution;}
        set{uapiPricingSolution = value;}
    }
    private string guid;
    /// <summary>
    /// For FlyDubai
    /// </summary>
    public string GUID
    {
        get { return guid; }
        set { guid = value; }
    }

    private bool isBaggageIncluded;
    public bool IsBaggageIncluded
    {
        get { return isBaggageIncluded; }
        set { isBaggageIncluded = true; }
    }
    //private SerializableDictionary<long, bool> baggageIncluded;
    //public SerializableDictionary<long, bool> BaggageIncluded
    //{
    //    get { return baggageIncluded; }
    //    set { baggageIncluded = value; }
    //}

    private string baggageIncludedInFare;
    /// <summary>
    /// Set Baggage Code included in Total Fare for FlyDubai. Example:- 20kg.
    /// </summary>
    public string BaggageIncludedInFare
    {
        get { return baggageIncludedInFare; }
        set { baggageIncludedInFare = value; }
    }

    private SerializableDictionary<string, List<int>> fareInformationId;
    /// <summary>
    /// Stores FareInfo ID from xml result for FlyDubai
    /// </summary>
    public SerializableDictionary<string, List<int>> FareInformationId
    {
        get { return fareInformationId; }
        set { fareInformationId = value; }
    }

    private string fareType;
    public string FareType
    {
        get
        {
            return fareType;
        }
        set
        {
            fareType = value;
        }
    }

    private int resultId;
    public int ResultId
    {
        get { return resultId; }
        set { resultId = value; }
    }

  

    private bool isGSTMandatory;

    public bool IsGSTMandatory
    {
        get
        {
            return isGSTMandatory;
        }

        set
        {
            this.isGSTMandatory = value;
        }
    }

    public WSResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public SearchResult getResult()
    {
        SearchResult result = new SearchResult();
        result.BaseFare = Convert.ToDouble(fare.BaseFare);
        result.Currency = fare.Currency;
        result.Tax = Convert.ToDouble(fare.Tax);
        result.TotalFare = Convert.ToDouble(fare.Tax + fare.BaseFare);
        result.FareBreakdown = new Fare[fareBreakdown.Length];
        for (int i = 0; i < result.FareBreakdown.Length; i++)
        {
            result.FareBreakdown[i] = fareBreakdown[i].GetPTCFare();
        }
        result.FareRules = WSUtility.GetFareRule(fareBasis);
        List<FlightInfo> flightListOB = new List<FlightInfo>();
        List<FlightInfo> flightListIB = new List<FlightInfo>();
        for (int i = 0; i < segment.Length; i++)
        {
            if (i == 0 && (source == BookingSource.HermesAirLine || source == BookingSource.AirArabia))
            {
                result.Airline = segment[i].Airline.AirlineCode;
            }
            if (segment[i].SegmentIndicator == 1)
            {
                flightListOB.Add(segment[i].GetSegment());
            }
            else if (segment[i].SegmentIndicator == 2)
            {
                flightListIB.Add(segment[i].GetSegment());
            }
        }
        if (flightListIB.Count == 0)
        {
            result.Flights = new FlightInfo[1][];
            result.Flights[0] = flightListOB.ToArray();
        }
        else
        {
            result.Flights = new FlightInfo[2][];
            result.Flights[0] = flightListOB.ToArray();
            result.Flights[1] = flightListIB.ToArray();
        }
        result.ResultBookingSource = source;
        return result;
    }
}
