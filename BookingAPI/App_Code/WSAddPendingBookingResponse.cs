/// <summary>
/// Summary description for WSAddPendingBookingResponse
/// </summary>
public class WSAddPendingBookingResponse
{
    private string queueId;
    public string QueueId
    {
        get { return queueId; }
        set { queueId = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public WSAddPendingBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
