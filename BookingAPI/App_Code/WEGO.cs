﻿//using System.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;

/// <summary>
/// Summary description for WEGO
/// </summary>
[XmlRoot(ElementName = "list")]
public class WEGOSearchResponse
{
    List<APIFlightItinerary> flightItineraries;
    string errorMessage; //Notification to the user if there are any errors
    

    [XmlElement(ElementName = "flightItinerary")]
    public List<APIFlightItinerary> FlightItineraries
    {
        get { return flightItineraries; }
        set { flightItineraries = value; }
    }

    public string ErrorMessage
    {
        get { return errorMessage; }
        set { errorMessage = value; }
    }

}

public class WEGOSearchRequest
{
    AuthenticationData credential;
    WSSearchRequest searchRequest;
    string ipAddress;

    public AuthenticationData Credential
    {
        get { return credential; }
        set { credential = value; }
    }

    public WSSearchRequest SearchRequest
    {
        get { return searchRequest; }
        set { searchRequest = value; }
    }

    public string IPAddress
    {
        get { return ipAddress; }
        set { ipAddress = value; }
    }
    
}

public class APIFlightItinerary
{
    ItineraryPrice price;
    Leg outboundLeg;
    Leg inboundLeg;
    string deepLinkUrl;
    int adults;
    int childs;
    int infants;

    [XmlElement(ElementName = "adults")]
    public int Adults
    {
        get { return adults; }
        set { adults = value; }
    }

    [XmlElement(ElementName = "childs")]
    public int Childs
    {
        get { return childs; }
        set { childs = value; }
    }

    [XmlElement(ElementName = "infants")]
    public int Infants
    {
        get { return infants; }
        set { infants = value; }
    }

    [XmlElement(ElementName = "price")]
    public ItineraryPrice Price
    {
        get { return price; }
        set { price = value; }
    }

    [XmlElement(ElementName = "outboundLeg")]
    public Leg OutboundLeg
    {
        get { return outboundLeg; }
        set { outboundLeg = value; }
    }

    [XmlElement(ElementName = "inboundLeg")]
    public Leg InboundLeg
    {
        get { return inboundLeg; }
        set { inboundLeg = value; }
    }

    [XmlElement(ElementName = "deeplinkURL")]
    public string DeepLinkURL
    {
        get { return deepLinkUrl; }
        set { deepLinkUrl = value; }
    }
}

public class ItineraryPrice
{

    string currencyCode;
    decimal pricePerAdult;
    decimal pricePerChild;
    decimal pricePerInfant;
    decimal totalAmount;


    public string CurrencyCode
    {
        get { return currencyCode; }
        set { currencyCode = value; }
    }

    public decimal PricePerAdult
    {
        get { return pricePerAdult; }
        set { pricePerAdult = value; }
    }
    public decimal PricePerChild
    {
        get { return pricePerChild; }
        set { pricePerChild = value; }
    }
    public decimal PricePerInfant
    {
        get { return pricePerInfant; }
        set { pricePerInfant = value; }
    }
    public decimal TotalAmount
    {
        get { return totalAmount; }
        set { totalAmount = value; }
    }


}

public class Leg
{
    Segment[] segments;

    [XmlElement(ElementName = "segments")]
    public Segment[] Segments
    {
        get { return segments; }
        set { segments = value; }
    }
}

public class Segment
{
    string flightClass;
    string flightNumber;
    string departureTime;
    string arrivalTime;
    string arrivalStation;
    string departureStation;
    string cabinClass;
    string operatingCarrier;
    string marketingCarrier;
    BaggageAllowance[] freeBaggageAllowance;


    [XmlElement(ElementName = "flightClass")]
    public string FlightClass
    {
        get { return flightClass; }
        set { flightClass = value; }
    }

    [XmlElement(ElementName = "flightNumber")]
    public string FlightNumber
    {
        get { return flightNumber; }
        set { flightNumber = value; }
    }

    [XmlElement(ElementName = "departureTime")]
    public string DepartureTime
    {
        get { return departureTime; }
        set { departureTime = value; }
    }

    [XmlElement(ElementName = "arrivalTime")]
    public string ArrivalTime
    {
        get { return arrivalTime; }
        set { arrivalTime = value; }
    }
    [XmlElement(ElementName = "destination")]
    public string Destination
    {
        get { return departureStation; }
        set { departureStation = value; }
    }

    [XmlElement(ElementName = "origin")]
    public string Origin
    {
        get { return arrivalStation; }
        set { arrivalStation = value; }
    }

    [XmlElement(ElementName = "operatingCarrier")]
    public string OperatingCarrier
    {
        get { return operatingCarrier; }
        set { operatingCarrier = value; }
    }

    [XmlElement(ElementName = "marketingCarrier")]
    public string MarketingCarrier
    {
        get { return marketingCarrier; }
        set { marketingCarrier = value; }
    }

    public BaggageAllowance[] FreeBaggageAllowance
    {
        get { return freeBaggageAllowance; }
        set { freeBaggageAllowance = value; }
    }
}

public class BaggageAllowance
{
    string paxType;
    string weight;
    string unit;

    [XmlElement(ElementName = "paxType")]
    public string PaxType
    {
        get { return paxType; }
        set { paxType = value; }
    }

    [XmlElement(ElementName = "weight")]
    public string Weight
    {
        get { return weight; }
        set { weight = value; }
    }

    [XmlElement(ElementName = "unit")]
    public string Unit
    {
        get { return unit; }
        set { unit = value; }
    }
}
