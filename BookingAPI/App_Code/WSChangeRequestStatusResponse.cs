/// <summary>
/// Summary description for WSChangeRequestStatusResponse
/// </summary>
public class WSChangeRequestStatusResponse
{
    private string requestId;
    public string RequestId
    {
        get { return requestId; }
        set { requestId = value; }
    }
    private decimal refundedAmount;
    public decimal RefundedAmount
    {
        get { return refundedAmount; }
        set { refundedAmount = value; }
    }
    private decimal cancellationCharge;
    public decimal CancellationCharge
    {
        get { return cancellationCharge; }
        set { cancellationCharge = value; }
    }
    private ChangeRequestStatus changeRequestStatus;
    public ChangeRequestStatus ChangeRequestStatus
    {
        get {return changeRequestStatus;}
        set { changeRequestStatus = value;}
    }
    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }
    
}
