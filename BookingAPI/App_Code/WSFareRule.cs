using System;
using CT.BookingEngine;

/// <summary>
/// Summary description for WSFareRule
/// </summary>
public class WSFareRule
{
    /// <summary>
    /// three character code of the origin city
    /// </summary>
    private string origin;
    /// <summary>
    /// gets or sets the origin city code
    /// </summary>
    public string Origin
    {
        get { return origin; }
        set { origin = value; }
    }
    /// <summary>
    /// three character code of the destination city
    /// </summary>
    private string destination;
    /// <summary>
    /// gets or sets the destination city code
    /// </summary>
    public string Destination
    {
        get { return destination; }
        set { destination = value; }
    }
    /// <summary>
    /// two character airline code
    /// </summary>
    private string airline;
    /// <summary>
    /// gets or sets the airline code
    /// </summary>
    public string Airline
    {
        get { return airline; }
        set { airline = value; }
    }
    /// <summary>
    /// fare restrictions
    /// </summary>
    private string fareRestriction;
    /// <summary>
    /// gets or sets the fare restrictions
    /// </summary>
    public string FareRestriction
    {
        get { return fareRestriction; }
        set { fareRestriction = value; }
    }
    /// <summary>
    /// farebasis code
    /// </summary>
    private string fareBasisCode;
    /// <summary>
    /// gets or sets the fare basis code
    /// </summary>
    public string FareBasisCode
    {
        get { return fareBasisCode; }
        set { fareBasisCode = value; }
    }
    /// <summary>
    /// detailed fare rule.
    /// </summary>
    private string fareRuleDetail;
    /// <summary>
    /// gets or sets the fare rule detail
    /// </summary>
    public string FareRuleDetail
    {
        get { return fareRuleDetail; }
        set { fareRuleDetail = value; }
    }
    /// <summary>
    /// Departure Time if Search Type is OneWay otherwise null or blank
    /// </summary>
    private DateTime departureDate;
    public DateTime DepartureDate
    {
        get { return departureDate; }
        set { departureDate = value; }
    }
    /// <summary>
    /// Return Date if Search Type is Return otherwise null or blank
    /// </summary>
    private DateTime returnDate;
    public DateTime ReturnDate
    {
        get { return returnDate; }
        set { returnDate = value; }
    }
    /// <summary>
    /// source
    /// </summary>
    private BookingSource source;
    /// <summary>
    /// gets or sets the source
    /// </summary>
    public BookingSource Source
    {
        get { return source; }
        set { source = value; }
    }


    /// <summary>
    /// gets or sets the Fare Rule Key Value for UAPI
    /// </summary>
    private string fareRuleKeyValue;
    /// <summary>
    /// gets or sets the Fare Rule Key Value for UAPI
    /// </summary>
    public string FareRuleKeyValue //For UAPI
    {
        get { return fareRuleKeyValue; }
        set { fareRuleKeyValue = value; }
    }

    /// <summary>
    /// three character code of the destination city
    /// </summary>
    private string fareInfoRef;
    /// <summary>
    /// gets or sets the Fare Info Refernce Key for UAPI
    /// </summary>
    public string FareInfoRef // For UAPI
    {
        get { return fareInfoRef; }
        set { fareInfoRef = value; }
    }
    public WSFareRule()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static WSFareRule ReadFareRuleRequest(FareRule fareRule)
    {
        WSFareRule wsFareRule = new WSFareRule();
        wsFareRule.Airline = fareRule.Airline;
        wsFareRule.Origin = fareRule.Origin;
        wsFareRule.Destination = fareRule.Destination;
        wsFareRule.FareRestriction = fareRule.FareRestriction;
        wsFareRule.FareBasisCode = fareRule.FareBasisCode;
        wsFareRule.FareRuleDetail = fareRule.FareRuleDetail;
        wsFareRule.DepartureDate = fareRule.DepartureTime;
        wsFareRule.ReturnDate = fareRule.ReturnDate;
        if(fareRule.FareInfoRef!=null)wsFareRule.FareInfoRef = fareRule.FareInfoRef;
        if (!string.IsNullOrEmpty(fareRule.FareRuleKeyValue )) wsFareRule.FareRuleKeyValue = fareRule.FareRuleKeyValue;
        //wsFareRule.Source = fareRule.Source;
        return wsFareRule;
    }
}
