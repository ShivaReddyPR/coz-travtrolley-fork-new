﻿using System;
//using System.Linq;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for WSAgentBalanceRequest
/// </summary>
/// 
public class WSB2BAgentBalanceRequest
{

    int _agentId;
    public int AgentId
    {
        get { return _agentId; }
        set { _agentId = value; }
    }
    public WSB2BAgentBalanceRequest()
    {
    }
}





public class WSB2BAgentBalanceResponse
{


    decimal _availableCredit;
    public decimal AvailableCredit
    {
        get { return _availableCredit; }
        set { _availableCredit = value; }
    }

    string _errorCode;
    string _errorMessage;

    public string ErrorCode
    {
        get { return _errorCode; }
        set { _errorCode = value; }
    }

    public string ErrorMessage
    {
        get { return _errorMessage; }
        set { _errorMessage = value; }
    }

    public WSB2BAgentBalanceResponse()
    {
    }

    public WSB2BAgentBalanceResponse GetAgentBalance(WSB2BAgentBalanceRequest request)
    {
        WSB2BAgentBalanceResponse res = new WSB2BAgentBalanceResponse();
        try
        {

            decimal balance = AgentMaster.UpdateAgentBalance(request.AgentId,0,0);
            res._availableCredit = balance; //Available balance for the respective agent
            res._errorCode = "000";
            res._errorMessage = "Success";
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return res;
    }
}



