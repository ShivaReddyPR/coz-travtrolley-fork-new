﻿using System;
//using System.Linq;
using CT.AccountingEngine;
using CT.TicketReceipt.BusinessLayer;

public class WSSaveLedgerTransactionRequest
{


    LedgerTransaction _ledgerTransaction;
    public LedgerTransaction LedgerTransaction
    {
        get { return _ledgerTransaction; }
        set { _ledgerTransaction = value; }
    }


    public WSSaveLedgerTransactionRequest()
    {

    }
}

public class WSLedgerTransaction
{
    int _ledgerId;
    public int LedgerId
    {
        get { return _ledgerId; }
        set { _ledgerId = value; }
    }
    decimal _agentBalance;
    public decimal AgentBalance
    {
        get { return _agentBalance; }
        set { _agentBalance = value; }
    }
    public WSLedgerTransaction()
    {
    }
}


public class WSSaveLedgerTransactionResponse
{

    string _errorCode;
    string _errorMessage;
    WSLedgerTransaction _ledgerTransactionInfo;

    public WSLedgerTransaction LedgerTransactionInfo
    {
        get { return _ledgerTransactionInfo; }
        set { _ledgerTransactionInfo = value; }
    }

    public string ErrorCode
    {
        get { return _errorCode; }
        set { _errorCode = value; }
    }

    public string ErrorMessage
    {
        get { return _errorMessage; }
        set { _errorMessage = value; }
    }
    public WSSaveLedgerTransactionResponse()
    {
    }

    public WSSaveLedgerTransactionResponse SaveLedgerDetails(WSSaveLedgerTransactionRequest request)
    {
        WSSaveLedgerTransactionResponse res = new WSSaveLedgerTransactionResponse();
        try
        {

            LedgerTransaction ledgerTrans = new LedgerTransaction();
            ledgerTrans = request.LedgerTransaction;
            ledgerTrans.Date = DateTime.Now;
            ledgerTrans.Amount = -request.LedgerTransaction.Amount;
            ledgerTrans.TransType = "B2B";
            decimal balance = AgentMaster.UpdateAgentBalance(ledgerTrans.AgencyId, ledgerTrans.Amount, ledgerTrans.CreatedBy);
            ledgerTrans.Save();
            if (ledgerTrans.TxnId > 0)
            {

                WSLedgerTransaction transactionInfo = new WSLedgerTransaction();
                transactionInfo.LedgerId = ledgerTrans.TxnId;
                transactionInfo.AgentBalance = balance;
                res._errorCode = "000";
                res._errorMessage = "Success";
                res._ledgerTransactionInfo = transactionInfo;

            }
            else
            {
                res._ledgerTransactionInfo = null;
                res._errorCode = "001";
                res._errorMessage = "Failed to Save ledger Details";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return res;
    }





}


