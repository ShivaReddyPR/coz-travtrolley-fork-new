﻿using System;
//using System.Linq;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Configuration;
using System.IO;
using System.Xml;
using CT.Core;

/// <summary>
/// API  Service Authentication
/// </summary>
public class ServiceAuthentication : SoapHeader
{
    /// <summary>
    /// User Name
    /// </summary>
    public string UserName;
    /// <summary>
    /// Password
    /// </summary>
    public string Password;
}

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class UtilityApiService : System.Web.Services.WebService
{
    /* ==============List of error codes and their descriptions =================
     *  000 -- Success.
     *  001 -- All types of  Exceptions.
     *  002 -- Required parameters validation to serve the request.  
     */
    public ServiceAuthentication Credential;
    bool userAuth = false;
    string xmlPath = ConfigurationManager.AppSettings["Path"];
    string userName = string.Empty;
    string password = string.Empty;
    public UtilityApiService()
    {
        //creating Folder added by brahmam
        if (!System.IO.Directory.Exists(xmlPath))
        {
            System.IO.Directory.CreateDirectory(xmlPath);
        }
        userName = ConfigurationManager.AppSettings["UserName"];
        password = ConfigurationManager.AppSettings["Password"];
    }

    /// <summary>
    /// Gets the VMS B2B Agent Information for the respective B2BAgent.
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    
    [WebMethod]
    [SoapHeader("Credential")]
    public WSB2BAgentBalanceResponse GetAgentBalance(WSB2BAgentBalanceRequest request)
    {
        WSB2BAgentBalanceResponse response = new WSB2BAgentBalanceResponse();
        try
        {
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSB2BAgentBalanceRequest));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GetAgentBalanceRequest.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, request);
                fs.Close();
            }
            catch { }
            //checking authentication brahmam
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (Credential != null) 
                {
                    if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                    {
                        userAuth = false;
                    }
                    if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                    {
                        userAuth = false;
                    }
                    else
                    {
                        if (Credential.UserName.Contains(userName) && Credential.Password.Contains(password))
                        {
                            userAuth = true;
                        }
                    }
                }
                else
                {
                    userAuth = false;
                }
            }
            if (userAuth) //checking authentication brahmam
            {
                if (request.AgentId <= 0)
                {
                    response.ErrorMessage = "Invalid Agent Id";
                    response.ErrorCode = "002";
                    response.AvailableCredit = 0;
                }
                else
                {
                    response = response.GetAgentBalance(request);
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSB2BAgentBalanceResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_GetAgentBalanceResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }

        }
        catch (Exception ex)
        {
            response.ErrorMessage = "Failed to get Agent Balance";
            response.ErrorCode = "001";
            response.AvailableCredit = 0;
            Audit.Add(EventType.Exception, Severity.High, 1, "Error in Utility Api Service :GetAgentBalance method " + ex.Message, "0");
           
        }
        return response;
    }

    /// <summary>
    /// Saves the ledger transaction details
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>

    [WebMethod]
    public WSSaveLedgerTransactionResponse SaveLedgerDetails(WSSaveLedgerTransactionRequest request)
    {
        WSSaveLedgerTransactionResponse response = new WSSaveLedgerTransactionResponse();
        try
        {
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSSaveLedgerTransactionRequest));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_SaveLedgerDetailsRequest.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, request);
                fs.Close();
            }
            catch { }
            //checking authentication brahmam
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (Credential != null)
                {
                    if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                    {
                        userAuth = false;
                    }
                    if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                    {
                        userAuth = false;
                    }
                    else
                    {
                        if (Credential.UserName.Contains(userName) && Credential.Password.Contains(password))
                        {
                            userAuth = true;
                        }
                    }
                }
                else
                {
                    userAuth = false;
                }
            }
            if (userAuth) //checking authentication brahmam
            {
                if (request.LedgerTransaction.TxnId < 0)
                {
                    response.ErrorMessage = "Invalid Transaction Id";
                    response.ErrorCode = "001";
                }
                else if (request.LedgerTransaction.LedgerId < 0)
                {
                    response.ErrorMessage = "Invalid Ledger Id";
                    response.ErrorCode = "001";
                }
                else if (request.LedgerTransaction.Amount <= 0)
                {
                    response.ErrorMessage = "Invalid amount";
                    response.ErrorCode = "001";
                }
                else if (request.LedgerTransaction.Narration == null)
                {
                    response.ErrorMessage = "Invalid narration";
                    response.ErrorCode = "001";
                }
                else if (request.LedgerTransaction.ReferenceType == null)
                {
                    response.ErrorMessage = "Invalid ReferenceType";
                    response.ErrorCode = "001";
                }
                else if (request.LedgerTransaction.AgencyId < 0)
                {
                    response.ErrorMessage = "Invalid Agent Id";
                    response.ErrorCode = "001";
                }
                else if (request.LedgerTransaction.CreatedBy < 0)
                {
                    response.ErrorMessage = "Invalid User Id";
                    response.ErrorCode = "001";
                }
                else if (string.IsNullOrEmpty(Convert.ToString(request.LedgerTransaction.ReferenceId)))
                {
                    response.ErrorMessage = "Invalid Reference Id";
                    response.ErrorCode = "001";
                }
                else
                {
                    response = response.SaveLedgerDetails(request);
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSSaveLedgerTransactionResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_SaveLedgerDetailsResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorMessage = "Failed to Save LedgerDetails";
            response.ErrorCode = "001";
            response.LedgerTransactionInfo = null;
            Audit.Add(EventType.Exception, Severity.High, 1, "Error in Utility Api Service :SaveLedgerDetails method " + ex.Message, "0");
        }
        return response;
    }

}
