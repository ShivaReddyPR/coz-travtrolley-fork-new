﻿namespace Visa
{
    /// <summary>
    /// This Enum contains the different types of charges in other charges
    /// </summary>
    public enum VisaStatus
    {
        Submitted = 1,
        Eligible = 2,
        PartiallyEligible = 3,
        NotEligible = 4,
        InProcess = 5,
        Approved = 6,
        PartiallyApproved = 7,
        Rejected = 8,
        RequestChange = 9,
        Cancelled = 10,
        Duplicate = 11,
        InProcess_M = 12,
        InProcessPartial_M = 13
    }
    public enum PassportType
    {
        Ordinary = 1,
        Diplomatic = 2,
        Official = 3
    }
    public enum VisaPassengerStatus
    {
        Submitted = 1,
        Eligible = 2,
        NotEligible = 3,
        InProcess = 4,
        Approved = 5,
        Rejected = 6,
        Cancelled = 7,
        Duplicate=8,
        InProcess_M = 9    //Added By Hari View booking Visa Purpose.
    }
    public enum VisaPassengerPaymentStatus
    {
        Payed = 1,
        NotPayed = 2


    }

    public enum VisaPaymentStatus
    {

        Accepted = 1,
        InProcess = 2,
        Rejected = 3
    }
}
