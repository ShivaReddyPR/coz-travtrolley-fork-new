﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using CT.TicketReceipt.DataAccessLayer;

namespace Visa
{
    public class VisaUtility
    {
        public static int GetMemberIdBySiteName(string siteName)
        {

            if (siteName == null || siteName.Length == 0)
            {
                throw new ArgumentException("Site name cannot be blank");
            }
            int id = 0;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@siteName", siteName);
            paramList[1] = new SqlParameter("@userId", SqlDbType.Int);
            paramList[1].Direction = ParameterDirection.Output;

            int retVal = DBGateway.ExecuteNonQuerySP("usp_GetMemberIdBySiteName", paramList);
            id = (int)paramList[1].Value;

           
            return id;

        }
        public static int GetAgencyIdByMemberId(int memberId)
        {


            if (memberId == 0)
            {
                throw new ArgumentException("memberId cannot be blank");
            }
            int id = 0;
            
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@memberId", memberId);
            paramList[1] = new SqlParameter("@agencyId", SqlDbType.Int);
            paramList[1].Direction = ParameterDirection.Output;

            int retVal = DBGateway.ExecuteNonQuerySP("usp_GetAgencyIdByMemberId", paramList);
            id = (int)paramList[1].Value;


            return id;

        }
        public static string GetCityByCountryCode(string countryCode )
        {

            string CityString = string.Empty; 
            if (countryCode != null && countryCode != "")
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@countryCode", countryCode);
                SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetCityByCountryCode", paramList, connection);
                
                while (data.Read())
                {
                    CityString+= (String)data["cityName"]+ ","+(String)data["cityCode"]+"/";
                }
                data.Close();
                connection.Close();
                
            }
            return CityString;

        }
        public static SortedList GetAllCity()
        {

            SortedList CityList = new SortedList();
           
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[0];

                SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetCityListForVisa", paramList, connection);

                while (data.Read())
                {
                    CityList.Add((String)data["cityCode"], (String)data["cityName"]);
                }
                data.Close();
                connection.Close();


                return CityList;

        }
        public static string GetRegCityByCountryCode(string countryCode)
        {

            string CityString = string.Empty;
            if (countryCode != null && countryCode != "")
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@countryCode", countryCode);
                SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetRegCityByCountryCode", paramList, connection);

                while (data.Read())
                {
                    CityString += (String)data["cityName"] + "," + (int)data["cityId"] + "/";
                }
                data.Close();
                connection.Close();

            }
            return CityString;

        }

        public static string GetVisaCityByVisaType(int visaTypeId)
        {
            string CityString = string.Empty;
            if ( visaTypeId != 0)
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@visaTypeId", visaTypeId);
                SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaCityByVisaType", paramList, connection);

                while (data.Read())
                {
                    CityString += (string)data["cityName"] + "," + (string)data["cityCode"] + "/";
                }
                data.Close();
                connection.Close();

            }
            return CityString;
        }

        public static SortedList GetCountryList()
        {
           
            SortedList countryList = new SortedList();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetCountryList", paramList, connection);
            while (data.Read())
            {
                countryList.Add( (String)data["countryCode"],(String)data["countryName"]);
            }
            data.Close();
            connection.Close();
            countryList.TrimToSize();
            return countryList;
        }
        public static int AddVisaQueueHistory( int visaId,int visaStatusId,decimal depositFee,decimal refundableFee,string remarks,int updatedBy)
        {
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@visaStatusId", visaStatusId);
            paramList[2] = new SqlParameter("@depositFee", depositFee);
            paramList[3] = new SqlParameter("@refundableFee", refundableFee);

            paramList[4] = new SqlParameter("@remarks", remarks);
            paramList[5] = new SqlParameter("@updatedBy", updatedBy);
            
            int status= DBGateway.ExecuteNonQuerySP("usp_AddVisaQueueHistory", paramList);
            return status;
        }


        public static DataTable GetOtherCity(string countryCode)
        {

           // SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0]=new SqlParameter("@countryCode",countryCode);
            DataTable dataTable = DBGateway.FillDataTableSP("usp_GetOtherCityReport", paramList);
           
           
            
           // connection.Close();


            return dataTable;
           

        }


    }
}
