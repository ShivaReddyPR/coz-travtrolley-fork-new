using System;
using System.Collections.Generic;
using System.Diagnostics;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
namespace Visa
{
    public class Visafaq
    {
        #region Variable
        int faqId;
        string countryCode;
        string faqQuestion;
        string faqAnswer;
        bool isActive;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        private string agency;
        private int agentId;
        #endregion

        #region Properties
        public int FaqId
        {
            get
            {
                return faqId;
            }
            set
            {
                faqId = value;
            }
        }
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }
        public string FaqQuestion
        {
            get
            {
                return faqQuestion;
            }
            set
            {
                faqQuestion = value;
            }
        }
        public string FaqAnswer
        {
            get
            {
                return faqAnswer;
            }
            set
            {
                faqAnswer = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        public string Agency
        {
            get
            {
                return agency;
            }
            set
            {
                agency = value;
            }
        }

        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }
        #endregion


        //private void Validate()
        //{
        //    //Trace.TraceInformation("Visafaq.BuildValidateMethod entered");
        //    if (countryCode == null || countryCode.Trim().Length == 0 || countryCode.Trim().Length > 2)
        //    {
        //        throw new ArgumentException(" countryCode cannot be null,empty and should be less than 3 characters.");
        //    }
        //    if (faqQuestion == null || faqQuestion.Trim().Length == 0 || faqQuestion.Trim().Length > 2000)
        //    {
        //        throw new ArgumentException(" faqQuestion cannot be null,empty and should be less than 2001 characters.");
        //    }
        //    if (isActive == null)
        //    {
        //        throw new ArgumentException(" isActive cannot be null");
        //    }
        //    if (createdOn == null)
        //    {
        //        throw new ArgumentException(" createdOn cannot be null");
        //    }
        //    //Trace.TraceInformation("Visafaq.BuildValidateMethod exited");
        //}

        #region Methods
        /// <summary>
        /// Default Cunstuctor
        /// </summary>
        public Visafaq()
        {

        }

        /// <summary>
        /// Saving and updating visa faq
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
            //Trace.TraceInformation("Visafaq.save entered");
            //Validate();
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@faqId", faqId);
            if (countryCode == null)
            {
                paramList[1] = new SqlParameter("@countryCode", DBNull.Value);
            }
            else
            {
                paramList[1] = new SqlParameter("@countryCode", countryCode);
            }
            paramList[2] = new SqlParameter("@faqQuestion", faqQuestion);
            if (faqAnswer == null)
            {
                paramList[3] = new SqlParameter("@faqAnswer", DBNull.Value);
            }
            else
            {
                paramList[3] = new SqlParameter("@faqAnswer", faqAnswer);
            }
            paramList[4] = new SqlParameter("@isActive", isActive);
            paramList[6] = new SqlParameter("@agencyId", agentId);
            if (faqId > 0)
            {
                paramList[5] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateVisafaq, paramList);
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@createdBy", createdBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddVisafaq, paramList);
                faqId = Convert.ToInt32(paramList[0].Value);
            }
            ////Trace.TraceInformation("Visafaq.save exited");
            return rowsAffected;
        }


        private void ReadDataReader(DataRow reader)
        {
            //if (reader.Read())
            {
                faqId = Convert.ToInt32(reader["faqId"]);
                countryCode = Convert.ToString(reader["countryCode"]);
                faqQuestion = Convert.ToString(reader["faqQuestion"]);
                if (reader["faqAnswer"] != DBNull.Value)
                {
                    faqAnswer = Convert.ToString(reader["faqAnswer"]);
                }
                isActive = Convert.ToBoolean(reader["isActive"]);
                createdBy = Convert.ToInt32(reader["createdBy"]);
                createdOn = Convert.ToDateTime(reader["createdOn"]);
                lastModifiedBy = Convert.ToInt32(reader["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(reader["lastModifiedOn"]);
                agentId = Convert.ToInt32(reader["agencyId"]);
            }
        }

        /// <summary>
        /// Faq list
        /// </summary>
        /// <returns></returns>
        public static List<Visafaq> GetFaqList(int agentId)
        {
            List<Visafaq> faqList = new List<Visafaq>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@faqId", DBNull.Value);
            if (agentId > 0) paramList[1] = new SqlParameter("@agencyId", agentId);
            else paramList[1] = new SqlParameter("@agencyId", DBNull.Value);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetVisaFaqList, paramList, connection);
            while (data.Read())
            {
                Visafaq faq = new Visafaq();
                faq.faqId = (int)data["faqId"];
                faq.faqQuestion = (String)data["faqQuestion"];
                faq.agency = data["agent_Name"] != DBNull.Value ? (String)data["agent_Name"] : string.Empty; 
                if (data["isActive"] == DBNull.Value)
                {
                    faq.isActive = true;
                }
                else
                {
                    faq.isActive = (bool)data["isActive"];
                }

                faqList.Add(faq);

            }
            data.Close();
            connection.Close();
            return faqList;
        }


        /// <summary>
        /// Get faq data based on faq id
        /// </summary>
        public void Load()
        {
            //Trace.TraceInformation("Visafaq.load entered");
            SqlParameter[] paramList = new SqlParameter[1]; paramList[0] = new SqlParameter("@faqId", faqId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_GetVisafaq", paramList, connection);
                using (DataTable dtFaq = DBGateway.FillDataTableSP("usp_GetVisafaq", paramList))
                {
                    if (dtFaq != null && dtFaq.Rows.Count > 0)
                    {
                        ReadDataReader(dtFaq.Rows[0]);
                        //reader.Close();
                    }
                }
            }
            //Trace.TraceInformation("Visafaq.Load exited");
        }

        /// <summary>
        /// Constructor : creates an instance of VisaFaq 
        /// </summary>
        /// <param name="VisaFaqId">Visa Faq id of the VisaFaq to be instanciated</param>
        public Visafaq(int faqId)
        {
            Load(faqId);
        }


        /// <summary>
        /// Load faq question answer by faq Id
        /// </summary>
        /// <param name="faqId"></param>
        public void Load(int faqId)
        {
            //Trace.TraceInformation("VisaFaq.load entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@faqId", faqId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetVisaFaqById, paramList, connection);
                using (DataTable dtFaq = DBGateway.FillDataTableSP(SPNames.GetVisaFaqById, paramList))
                {
                    if (dtFaq != null && dtFaq.Rows.Count > 0)
                    {
                        ReadDataReader(dtFaq.Rows[0]);
                    }
                    //dataReader.Close();
                }
            }

        }



        /// <summary>
        /// Change Visa Status
        /// </summary>
        /// <param name="nationalityId"></param>
        /// <param name="isActive"></param>
        public static void ChangeStatus(int faqId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@faqId", faqId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP(SPNames.ChangeVisaFaqStatus, paramList);
        }
        #endregion

    }
}
