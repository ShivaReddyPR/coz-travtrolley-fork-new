﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;


namespace Visa
{
    public class VisaCity
    {
        #region Variable

        /// <summary>
        /// Name of city
        /// </summary>
        private int cityId;
        /// <summary>
        /// Name of city
        /// </summary>
        private string cityName;
        /// <summary>
        /// two character code of city
        /// </summary>
        private string cityCode;

        /// <summary>
        /// Name of countryCode
        /// </summary>
        private string countryCode;
        /// <summary>
        /// Shows either the member is active or not. true for isActive member.
        /// </summary>

        private bool isActive;
        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        private string cityNameAR;




        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets city Id
        /// </summary>
        public int CityId
        {
            get
            {
                return cityId;
            }
            set
            {
                cityId = value;
            }
        }

        /// <summary>
        /// Gets or sets city name
        /// </summary>
        public string CityName
        {
            get
            {
                return cityName;
            }
            set
            {
                cityName = value;
            }
        }



        /// <summary>
        /// Gets or sets city code
        /// </summary>
        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }

        /// <summary>
        /// Gets or sets city code
        /// </summary>
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }


        /// <summary>
        /// Gets or sets the status of member ( true for active member )
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }


        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public string CityNameAR
        {
            get
            {
                return cityNameAR;
            }
            set
            {
                cityNameAR = value;
            }
        }

        #endregion


        #region Method

        /// <summary>
        /// Constructor : creates an empty instance of VisaCity
        /// </summary>
        public VisaCity()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaCity with City detail corresponding to CityId given CityId
        /// </summary>
        /// <param name="VisaCityId">Visa Type id of the VisaCity to be instanciated</param>
        public VisaCity(int cityId)
        {
            Load(cityId);
        }


        /// <summary>
        /// Loads the information about City identified by CityId
        /// </summary>
        /// <param name="CityId">City id of the City to be loaded</param>
        public void Load(int cityId)
        {

            if (cityId <= 0)
            {
                throw new ArgumentException("cityId must have a positive integer value", "cityId");
            }
           // SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityId", cityId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisaCityById", paramList, connection);
            using (DataTable dtVisa = DBGateway.FillDataTableSP("usp_ReadVisaCityById", paramList))
            {
                if (dtVisa != null && dtVisa.Rows.Count > 0)
                {
                    //if (dataReader.Read())
                    {
                        Read(dtVisa.Rows[0]);
                        //dataReader.Close();
                        //connection.Close();

                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("city does not exist : cityId = " + cityId, "cityId");
                }
            }
        }

        private void Read(DataRow dataReader)
        {
            cityId =Convert.ToInt32(dataReader["cityId"]);
            cityName =Convert.ToString(dataReader["cityName"]);
            cityCode = Convert.ToString(dataReader["cityCode"]);
            countryCode = Convert.ToString(dataReader["countryCode"]);
            isActive = (bool)dataReader["isActive"];
            createdOn = Convert.ToDateTime(dataReader["createdOn"]);
            createdBy = Convert.ToInt32(dataReader["createdBy"]);
            lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
            lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
          
            if (dataReader["cityNameAR"] != DBNull.Value)
            {
                cityNameAR = dataReader["cityNameAR"].ToString();
            }
            else
            {
                cityNameAR = string.Empty;
            }
        }

        /// <summary>
        /// Gets list of countries with city code
        /// </summary>
        /// <returns>Sorted list of all countries. city code as key and name as value</returns>
        public static List<VisaCity> GetCityList()
        {

            List<VisaCity> cityList = new List<VisaCity>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaCity", paramList, connection);
            while (data.Read())
            {
                VisaCity city = new VisaCity();
                city.cityId = (int)data["cityId"];
                city.cityName = (String)data["cityName"];
                city.cityCode = (String)data["cityCode"];
               
                if (data["isActive"] == DBNull.Value)
                {
                    city.isActive = true;
                }
                else
                {
                    city.isActive = (bool)data["isActive"];
                }

                cityList.Add(city);

            }
            data.Close();
            connection.Close();
            return cityList;
        }





        /// <summary>
        /// Gets list of city   with country code
        /// </summary>
        /// <returns>Sorted list of all cities. city code as key and name as value</returns>
        public static List<VisaCity> GetCityList(string countryCode)
        {

            List<VisaCity> cityList = new List<VisaCity>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@countryCode", countryCode);
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaCityList", paramList, connection);
            while (data.Read())
            {
                VisaCity city = new VisaCity();
                city.cityId = (int)data["cityId"];
                city.cityName = (String)data["cityName"];
                city.cityCode = (String)data["cityCode"];
                city.countryCode = data["countryCode"].ToString(); 

                if (data["isActive"] == DBNull.Value)
                {
                    city.isActive = true;
                }
                else
                {
                    city.isActive = (bool)data["isActive"];
                }

                cityList.Add(city);
            }
            data.Close();
            connection.Close();
            return cityList;
        }

        public int Save()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@cityName", cityName);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@countryCode", countryCode);
            paramList[3] = new SqlParameter("@createdBy", createdBy);
            paramList[4] = new SqlParameter("@isActive", isActive);
            paramList[5] = new SqlParameter("@ErrMessage", SqlDbType.Int);
            paramList[5].Direction = ParameterDirection.Output;
            paramList[6] = new SqlParameter("@cityNameAR", cityNameAR);
            DBGateway.ExecuteNonQuerySP("usp_AddVisaCity", paramList);
            status = (int)paramList[5].Value;
            return status;

        }

        public int UpdateCity()
        {
            int status = 0;
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@cityName", cityName);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@countryCode", countryCode);
            paramList[3] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[4] = new SqlParameter("@cityId", cityId);
            paramList[5] = new SqlParameter("@ErrMessage", SqlDbType.Int);
            paramList[5].Direction = ParameterDirection.Output;
            paramList[6] = new SqlParameter("@cityNameAR", cityNameAR);
            DBGateway.ExecuteNonQuerySP("usp_UpdateVisaCity", paramList);
            status = (int)paramList[5].Value;
            return status;
        }






        public static void ChangeStatus(int cityId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@cityId", cityId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaCityStatus", paramList);


        }


        #endregion


    }
}
