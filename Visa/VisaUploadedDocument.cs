﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace Visa
{
    public class VisaUploadedDocument
    {
        #region variable
        int paxId;
        int documentId;
        string documentPath;
        #endregion


        #region
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }
        public int DocumentId
        {
            get
            {
                return documentId;
            }
            set
            {
                documentId = value;
            }
        }
        public string DocumentPath
        {
            get
            {
                return documentPath;
            }
            set
            {
                documentPath = value;
            }
        }
        #endregion

        #region

        private void Validate()
        {
            //Trace.TraceInformation("VisaUploadedDocument.BuildValidateMethod entered");
            //Trace.TraceInformation("VisaUploadedDocument.BuildValidateMethod exited");
        }

        public int Save()
        {
            //Trace.TraceInformation("VisaUploadedDocument.save entered");
            Validate();
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@paxId", paxId);
            if (documentId == 0)
            {
                paramList[1] = new SqlParameter("@documentId", DBNull.Value);
            }
            else
            {
                paramList[1] = new SqlParameter("@documentId", documentId);
            }
            if (documentPath == null)
            {
                paramList[2] = new SqlParameter("@documentPath", DBNull.Value);
            }
            else
            {
                paramList[2] = new SqlParameter("@documentPath", documentPath);
            }
            if (paxId > 0)
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaUploadedDocument", paramList);
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddVisaUploadedDocument", paramList);
                paxId = Convert.ToInt32(paramList[0].Value);
            }
            //Trace.TraceInformation("VisaUploadedDocument.save exited");
            return rowsAffected;
        }

        private void Read(DataRow dataReader)
        {
            //if (dataReader.Read())
            {
                paxId = Convert.ToInt32(dataReader["paxId"]);
                if (dataReader["documentId"] != DBNull.Value)
                {
                    documentId = Convert.ToInt32(dataReader["documentId"]);

                }
                if (dataReader["documentPath"] != DBNull.Value)
                {
                    documentPath = Convert.ToString(dataReader["documentPath"]);

                }
            }
        }

        public void Load(int paxId)
        {
            ////Trace.TraceInformation("VisaUploadedDocument.load entered");
            SqlParameter[] paramList = new SqlParameter[1]; paramList[0] = new SqlParameter("@paxId", paxId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetVisaUploadedDocument", paramList, connection);
                using (DataTable dtUpload = DBGateway.FillDataTableSP("usp_GetVisaUploadedDocument", paramList))
                {
                    if (dtUpload != null && dtUpload.Rows.Count > 0)
                    {
                        Read(dtUpload.Rows[0]);
                        //dataReader.Close();
                    }
                }
            }
            ////Trace.TraceInformation("VisaUploadedDocument.Load exited");
        }

        public static List<VisaUploadedDocument> GetUploadedDocumentList(int paxId)
        {

            List<VisaUploadedDocument> documentList = new List<VisaUploadedDocument>();
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaUploadedDocument", paramList, connection);
            using (DataTable dtUpload = DBGateway.FillDataTableSP("usp_GetVisaUploadedDocument", paramList))
            {
                if (dtUpload != null && dtUpload.Rows.Count > 0)
                {
                    foreach(DataRow data in dtUpload.Rows)
                    {
                        VisaUploadedDocument document = new VisaUploadedDocument();
                        document.documentId =Convert.ToInt32(data["documentId"]);
                        if (data["documentPath"] == DBNull.Value)
                        {
                            document.documentPath = "";
                        }
                        else
                        {
                            document.documentPath = Convert.ToString(data["documentPath"]);
                        }
                        documentList.Add(document);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            return documentList;
        }


        #endregion
    }
}