﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;

namespace Visa
{
    public class VisaRequirement
    {
        #region   Variable
        /// <summary>
        /// Unique identity number of a criteria
        /// </summary>
        private int requirementId;
        /// <summary>
        /// requirementName
        /// </summary>

        private string requirementName;
        /// <summary>
        /// two character code of country
        /// </summary>
        private string countryCode;
        /// <summary>
        /// Description of requirement
        /// </summary>
        private string description;


        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// Shows either the member is active or not. true for isActive member.
        /// </summary>
        private bool isActive;
        private string agency;
        private int agentId;
        #endregion

        #region Property
        /// <summary>
        /// Gets or sets the requirementId
        /// </summary>
        public int RequirementId
        {
            get
            {
                return requirementId;
            }
            set
            {
                requirementId = value;
            }
        }


        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string RequirementName
        {
            get
            {
                return requirementName;
            }
            set
            {
                requirementName = value;
            }
        }

        /// <summary>
        /// Gets or sets country code
        /// </summary>
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }



        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }


        /// <summary>
        /// Gets or sets the status of member ( true for active member )
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        public string Agency
        {
            get
            {
                return agency;
            }
            set
            {
                agency = value;
            }
        }

        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }
        #endregion

        #region Method

        /// <summary>
        /// Constructor : creates an empty instance of VisaRequirement
        /// </summary>
        public VisaRequirement()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaRequirement with Criteria detail corresponding to CriteriaId given CriteriaId
        /// </summary>
        /// <param name="criteriaId">criteriaId of the VisaRequirement to be instanciated</param>
        public VisaRequirement(int requirementId)
        {
            Load(requirementId);
        }


        /// <summary>
        /// Loads the information about member identified by memberId
        /// </summary>
        /// <param name="memberId">Member id of the member to be loaded</param>
        public void Load(int requirementId)
        {

            if (requirementId <= 0)
            {
                throw new ArgumentException("requirementId must have a positive integer value", "VisaTypeId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@requirementId", requirementId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisaRequirementById", paramList, connection);
            using (DataTable dtRequirement = DBGateway.FillDataTableSP("usp_ReadVisaRequirementById", paramList))
            {
                if (dtRequirement != null && dtRequirement.Rows.Count > 0)
                {
                    //if (dataReader.Read())
                    {
                        Read(dtRequirement.Rows[0]);
                        //dataReader.Close();
                        //connection.Close();

                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("Visa Requirement  does not exist : visaTypeId = " + requirementId, "RequirementId");
                }
            }
        }



        private void Read(DataRow dataReader)
        {
            requirementId = Convert.ToInt32(dataReader["requirementId"]);
            requirementName = Convert.ToString(dataReader["requirementName"]);
            countryCode = Convert.ToString(dataReader["countryCode"]);
            description = Convert.ToString(dataReader["description"]);
            isActive = Convert.ToBoolean(dataReader["isActive"]);
            createdOn = Convert.ToDateTime(dataReader["createdOn"]);
            createdBy = Convert.ToInt32(dataReader["createdBy"]);
            lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
            lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
            agentId = Convert.ToInt32(dataReader["agencyId"]);

        }

        public void save()
        {
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@requirementName", requirementName);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[2] = new SqlParameter("@description", description);
            paramList[5] = new SqlParameter("@agencyId", agentId);
            if (requirementId > 0)
            {
                paramList[3] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramList[4] = new SqlParameter("@requirementId", requirementId);
                DBGateway.ExecuteNonQuerySP("usp_UpdateVisaRequirement", paramList);
            }
            else
            {
                paramList[3] = new SqlParameter("@createdBy", createdBy);
                paramList[4] = new SqlParameter("@isActive", isActive);
                DBGateway.ExecuteNonQuerySP("usp_AddVisaRequirement", paramList);
            }

        }


        public static List<VisaRequirement> GetRequirementList(string countryCode,int agentId)
        {

            List<VisaRequirement> requirementList = new List<VisaRequirement>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
          
            paramList[0] = new SqlParameter("@countryCode", countryCode);
            if (agentId > 0) paramList[1] = new SqlParameter("@agencyId", agentId);
            else paramList[1] = new SqlParameter("@agencyId", DBNull.Value);
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaRequirement", paramList, connection);
            while (data.Read())
            {
                VisaRequirement requirement = new VisaRequirement();
                requirement.requirementId = (int)data["requirementId"];
                requirement.requirementName = (string)data["requirementName"];
                requirement.countryCode = (string)data["countryCode"];
                requirement.description = (string)data["description"];
                requirement.agency = data["agent_Name"] != DBNull.Value ? (String)data["agent_Name"] : string.Empty; 
                if (data["isActive"] == DBNull.Value)
                {
                    requirement.isActive = true;
                }
                else
                {
                    requirement.isActive = (bool)data["isActive"];
                }

                requirementList.Add(requirement);

            }
            data.Close();
            connection.Close();

           
            return requirementList;
        }

        public static void ChangeStatus(int requirementId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@requirementId", requirementId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaRequirementStatus", paramList);


        }
        #endregion

    }
}
