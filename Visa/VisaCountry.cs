﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;


namespace Visa
{
    public class VisaCountry
    {

        #region Variable

        /// <summary>
        /// Name of country
        /// </summary>
        private int countryId;
        /// <summary>
        /// Name of country
        /// </summary>
        private string countryName;
        /// <summary>
        /// two character code of country
        /// </summary>
        private string countryCode;

        /// <summary>
        /// regionId Of country.
        /// </summary>
        private int regionId;
        /// <summary>
        /// Shows either the member is active or not. true for isActive member.
        /// </summary>

        private bool isActive;
        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        private string countryNameAR;




        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets country Id
        /// </summary>
        public int CountryId
        {
            get
            {
                return countryId;
            }
            set
            {
                countryId = value;
            }
        }

        /// <summary>
        /// Gets or sets country name
        /// </summary>
        public string CountryName
        {
            get
            {
                return countryName;
            }
            set
            {
                countryName = value;
            }
        }



        /// <summary>
        /// Gets or sets country code
        /// </summary>
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }

        /// <summary>
        /// Gets or sets Region Id
        /// </summary>
        public int RegionId
        {
            get
            {
                return regionId;
            }
            set
            {
                regionId = value;
            }
        }

        /// <summary>
        /// Gets or sets the status of member ( true for active member )
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }


        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public string CountryNameAR
        {
            get
            {
                return countryNameAR;
            }
            set
            {
                countryNameAR = value;
            }
        }

        #endregion


        #region Method

        /// <summary>
        /// Constructor : creates an empty instance of visaType
        /// </summary>
        public VisaCountry()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaType with member detail corresponding to memberId given memberId
        /// </summary>
        /// <param name="VisaTypeId">Visa Type id of the VisaType to be instanciated</param>
        public VisaCountry(int countryId)
        {
            Load(countryId);
        }


        /// <summary>
        /// Loads the information about member identified by memberId
        /// </summary>
        /// <param name="memberId">Member id of the member to be loaded</param>
        public void Load(int countryId)
        {

            if (countryId <= 0)
            {
                throw new ArgumentException("countryId must have a positive integer value", "VisaTypeId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@countryId", countryId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisacountryById", paramList, connection);
            using (DataTable dtVisa = DBGateway.FillDataTableSP("usp_ReadVisacountryById", paramList))
            {
                if (dtVisa != null && dtVisa.Rows.Count > 0)
                {
                    //if (dataReader.Read())
                    {
                        Read(dtVisa.Rows[0]);
                        //dataReader.Close();
                        //connection.Close();

                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("country does not exist : countryId = " + countryId, "countryId");
                }
            }
        }

        private void Read(DataRow dataReader)
        {

            countryId = Convert.ToInt32(dataReader["countryId"]);
            countryName = Convert.ToString(dataReader["countryName"]);
            countryCode = Convert.ToString(dataReader["countryCode"]);
            regionId = Convert.ToInt32(dataReader["regionId"]);
            isActive = Convert.ToBoolean(dataReader["isActive"]);
            createdOn = Convert.ToDateTime(dataReader["createdOn"]);
            createdBy = Convert.ToInt32(dataReader["createdBy"]);
            lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
            lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
            countryNameAR = Convert.ToString(dataReader["countryNameAR"]);
        }

        ///// <summary>
        ///// Gets a country identified by country code
        ///// </summary>
        ///// <param name="countryCode">country code</param>
        ///// <returns>country object</returns>
        //public static VisaCountry GetCountry(String countryCode)
        //{
        //    //Trace.TraceInformation("Country.GetCountry Entered : countryCode = " + countryCode);
        //    VisaCountry country = new VisaCountry();

        //    SqlConnection connection = DBGateway.GetConnection();
        //    SqlParameter[] paramList = new SqlParameter[1];
        //    paramList[0] = new SqlParameter("@countryCode", countryCode);
        //    SqlDataReader data = DBGateway.ExecuteReaderSP("SPNames.GetCountry", paramList, connection);
        //    if (data.Read())
        //    {
        //        country.countryCode = countryCode;
        //        country.countryName = (String)data["countryName"];

        //        //Trace.TraceInformation("Country.GetCountry Exited : countryName = " + country.countryName);
        //    }
        //    else
        //    {
        //        //country doesnt exist in database
                
        //        country = null;
        //    }
        //    data.Close();
        //    connection.Close();
        //    return country;
        //}

        /// <summary>
        /// Gets list of countries with country code
        /// </summary>
        /// <returns>Sorted list of all countries. Country code as key and name as value</returns>
        public static List<VisaCountry> GetCountryList()
        {

            List<VisaCountry> countryList = new List<VisaCountry>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaCountryList", paramList, connection);
            while (data.Read())
            {
                VisaCountry country = new VisaCountry();
                country.countryId = (int)data["countryId"];
                country.countryName = (String)data["countryName"];
                country.countryCode = (String)data["countryCode"];
                country.regionId = (int)data["regionId"];
                if (data["isActive"] == DBNull.Value)
                {
                    country.isActive = true;
                }
                else
                {
                    country.isActive = (bool)data["isActive"];
                }

                countryList.Add(country);

            }
            data.Close();
            connection.Close();
            return countryList;
        }

        public static List<VisaCountry> GetCountryList(int regionId)
        {

            List<VisaCountry> countryList = new List<VisaCountry>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@regionId",regionId);
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaCountryListByRegion", paramList, connection);
            while (data.Read())
            {
                VisaCountry country = new VisaCountry();
                country.countryId = (int)data["countryId"];
                country.countryName = (String)data["countryName"];
                country.countryCode = (String)data["countryCode"];
                country.regionId = (int)data["regionId"];
                if (data["isActive"] == DBNull.Value)
                {
                    country.isActive = true;
                }
                else
                {
                    country.isActive = (bool)data["isActive"];
                }

                countryList.Add(country);

            }
            data.Close();
            connection.Close();
            return countryList;
        }

        /// <summary>
        /// Get active country list
        /// </summary>
        /// <returns></returns>
        public static List<VisaCountry> GetActiveVisaCountryList()
        {

            List<VisaCountry> countryList = new List<VisaCountry>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetActiveVisaCountryList", paramList, connection);
            while (data.Read())
            {
                VisaCountry country = new VisaCountry();
                country.countryId = (int)data["countryId"];
                country.countryName = (String)data["countryName"];
                country.countryCode = (String)data["countryCode"];
                country.regionId = (int)data["regionId"];
                if (data["isActive"] == DBNull.Value)
                {
                    country.isActive = true;
                }
                else
                {
                    country.isActive = (bool)data["isActive"];
                }

                countryList.Add(country);

            }
            data.Close();
            connection.Close();
            return countryList;
        }


        public int Save()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@countryName", countryName);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[2] = new SqlParameter("@regionId", regionId);
            paramList[3] = new SqlParameter("@createdBy", createdBy);
            paramList[4] = new SqlParameter("@isActive", isActive);
            paramList[5] = new SqlParameter("@ErrMessage", SqlDbType.Int);
            paramList[5].Direction = ParameterDirection.Output;
            paramList[6] = new SqlParameter("@countryNameAR", countryNameAR);
            DBGateway.ExecuteNonQuerySP("usp_AddVisaCountry", paramList);
            status = (int)paramList[5].Value;
            return status;
        }


        public int UpdateCountry()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@countryName", countryName);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[2] = new SqlParameter("@regionId", regionId);
            paramList[3] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[4] = new SqlParameter("@countryId", countryId);
            paramList[5] = new SqlParameter("@ErrMessage", SqlDbType.Int);
            paramList[5].Direction = ParameterDirection.Output;
            paramList[6] = new SqlParameter("@countryNameAR", countryNameAR);
            DBGateway.ExecuteNonQuerySP("usp_UpdateVisaCountry", paramList);
            status = (int)paramList[5].Value;
            return status;
        }


        public static void ChangeStatus(int countryId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@countryId", countryId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaCountryStatus", paramList);


        }


        #endregion


    }
}
