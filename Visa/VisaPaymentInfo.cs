using System;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.BookingEngine;
namespace Visa
{


    public class VisaPaymentInfo
    {

        #region Variable
        int paymentInfoId;
        int visaId;
        string orderId;
        string paymentId;
        VisaPaymentStatus paymentStatus;
        decimal amount;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        PGSource pgSourceId;
        string remarks;
        int visaStatus;
        #endregion


        #region Properties

        public int PaymentInfoId
        {
            get
            {
                return paymentInfoId;
            }
            set
            {
                paymentInfoId = value;
            }
        }
        public int VisaId
        {
            get
            {
                return visaId;
            }
            set
            {
                visaId = value;
            }
        }
        public string OrderId
        {
            get
            {
                return orderId;
            }
            set
            {
                orderId = value;
            }
        }
        public string PaymentId
        {
            get
            {
                return paymentId;
            }
            set
            {
                paymentId = value;
            }
        }
        public VisaPaymentStatus PaymentStatus
        {
            get
            {
                return paymentStatus;
            }
            set
            {
                paymentStatus = value;
            }
        }
        public decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        public PGSource PGSourceId
        {
            get { return pgSourceId; }
            set { pgSourceId = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public int VisaStatus
        {
            get { return visaStatus; }
            set { visaStatus = value; }
        }
        #endregion

        #region VisaPaymentInfo class constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public VisaPaymentInfo()
        {

        }

        /// <summary>
        /// Constructor which initialise the fields according to paymentInfoId
        /// </summary>
        //public VisaPaymentInfo(int paymentInfoId)
        //{
        //    Load(paymentInfoId);

        //}
        /// <summary>
        /// Constructor which initialise the fields according to visaId
        /// </summary>
        public VisaPaymentInfo(int visaId)
        {
            Load(visaId);

        }
        #endregion

        #region Method 

        private void Validate()
        {
            ////Trace.TraceInformation("VisaPaymentInfo.BuildValidateMethod entered");
            if (createdOn == null)
            {
                throw new ArgumentException(" createdOn cannot be null");
            }
            ////Trace.TraceInformation("VisaPaymentInfo.BuildValidateMethod exited");
        }


        public int Save()
        {
            ////Trace.TraceInformation("VisaPaymentInfo.save entered");
            Validate();
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@paymentInfoId", paymentInfoId);
            paramList[1] = new SqlParameter("@visaId", visaId);
            if (orderId == null)
            {
                paramList[2] = new SqlParameter("@orderId", DBNull.Value);
            }
            else
            {
                paramList[2] = new SqlParameter("@orderId", orderId);
            }
            if (paymentId == null)
            {
                paramList[3] = new SqlParameter("@paymentId", DBNull.Value);
            }
            else
            {
                paramList[3] = new SqlParameter("@paymentId", paymentId);
            }
            if ((int)paymentStatus == 0)
            {
                paramList[4] = new SqlParameter("@paymentStatus", DBNull.Value);
            }
            else
            {
                paramList[4] = new SqlParameter("@paymentStatus", paymentStatus);
            }
            if (amount == 0)
            {
                paramList[5] = new SqlParameter("@amount", DBNull.Value);
            }
            else
            {
                paramList[5] = new SqlParameter("@amount", amount);
            }
            if (paymentInfoId > 0)
            {
               
                paramList[6] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaPaymentInfo", paramList);
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
               
                paramList[6] = new SqlParameter("@createdBy", createdBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddVisaPaymentInfo", paramList);
                paymentInfoId = Convert.ToInt32(paramList[0].Value);
            }
            ////Trace.TraceInformation("VisaPaymentInfo.save exited");
            return paymentInfoId;
        }

        private void ReadDataReader(DataRow reader)
        {
            //if (reader.Read())
            {
                paymentInfoId = Convert.ToInt32(reader["paymentInfoId"]);
                visaId = Convert.ToInt32(reader["visaId"]);
                if (reader["orderId"] != DBNull.Value)
                {
                    orderId = Convert.ToString(reader["orderId"]);

                }
                if (reader["paymentId"] != DBNull.Value)
                {
                    paymentId = Convert.ToString(reader["paymentId"]);

                }
                if (reader["paymentStatus"] != DBNull.Value)
                {
                    paymentStatus = (VisaPaymentStatus)Convert.ToInt32(reader["paymentStatus"]);

                }
                if (reader["amount"] != DBNull.Value)
                {
                    amount = Convert.ToDecimal(reader["amount"]);
                }
                if (reader["pgSourceId"] != DBNull.Value)
                {
                    pgSourceId = (PGSource)Convert.ToInt32(reader["pgSourceId"]);
                }
                if (reader["remarks"] != DBNull.Value)
                {
                    remarks = Convert.ToString(reader["remarks"]);
                }
                createdBy = Convert.ToInt32(reader["createdBy"]);
                createdOn = Convert.ToDateTime(reader["createdOn"]);
                lastModifiedBy = Convert.ToInt32(reader["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(reader["lastModifiedOn"]);
            }
        }
        public void GetPaymentInfoByPaymentId(int paymentInfoId)
        {
            //Trace.TraceInformation("VisaPaymentInfo.load entered");
            SqlParameter[] paramList = new SqlParameter[1]; paramList[0] = new SqlParameter("@paymentInfoId", paymentInfoId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_GetVisaPaymentInfo", paramList, connection);
                using (DataTable dtPayment = DBGateway.FillDataTableSP("usp_GetVisaPaymentInfo", paramList))
                {
                    if (dtPayment != null && dtPayment.Rows.Count > 0)
                    {
                        ReadDataReader(dtPayment.Rows[0]);
                        //reader.Close();
                    }
                }
            }
            //Trace.TraceInformation("VisaPaymentInfo.Load exited");
        }
        public void Load(int visaId)
        {
            //Trace.TraceInformation("VisaPaymentInfo.load entered");
            SqlParameter[] paramList = new SqlParameter[1]; paramList[0] = new SqlParameter("@visaId", visaId);
           // using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_GetVisaPaymentInfoByVisaId", paramList, connection);
                using (DataTable dtPayment = DBGateway.FillDataTableSP("usp_GetVisaPaymentInfoByVisaId", paramList))
                {
                    if (dtPayment != null && dtPayment.Rows.Count > 0)
                    {
                        ReadDataReader(dtPayment.Rows[0]);
                        //reader.Close();
                    }
                }
            }
            //Trace.TraceInformation("VisaPaymentInfo.Load exited");
        }
        #region Added By Hari update/Insert the Payment Details.
        public int UpdatePaymentInfo()
        {
            int result = 0;
            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@visaId", VisaId);
            paramList[1] = new SqlParameter("@orderId", OrderId);
            paramList[2] = new SqlParameter("@paymentId", PaymentId);
            paramList[3] = new SqlParameter("@paymentStatus", (int)VisaPassengerPaymentStatus.Payed);
            paramList[4] = new SqlParameter("@pgSourceId", pgSourceId);
            paramList[5] = new SqlParameter("@remarks", Remarks);
            paramList[6] = new SqlParameter("@createdBy", CreatedBy);
            paramList[7] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[8] = new SqlParameter("@amount", amount);
            paramList[9] = new SqlParameter("@visastatus", VisaStatus);
            result = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaPaymentDetails", paramList);
            return result;
        }
        #endregion
        #endregion
    }
}
