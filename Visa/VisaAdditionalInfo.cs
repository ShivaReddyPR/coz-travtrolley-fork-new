﻿using System;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace Visa
{
    public enum Gender
    {
        Male = 1,
        Female = 2
    }

    public enum Insurence
    {

    }
    public enum Sponsor
    {

    }

    public enum MaritalStatus
    {
        Single = 1,
        Married = 2

    }
    public class VisaAdditionalInfo
    {

        #region Variable
        int additionalFieldId;
        string fileNo;
        string trackingNo;
        string guranterName;
        string guranterPassport;
        string guranterNationality;
        string guranterNationalityOther;
        string guranterPreNationality;
        PassportType guranterPassportType;
        DateTime guranterPassportDate;
        DateTime guranterPassportExpiryDate;
        string guranterPassportCity;
        string guranterPassportCountry;
        string guranterPassportCityOther;

        string realationShipWithGuranter;
        string guranterVisaNo;
        DateTime guranterVisaExpiryDate;
        string guranterOfficeNo;
        string guranterMobile;
        string guranterEmailId;
        string receiptNumber;
        string acName;
        Gender acSex;
        DateTime acDob;
        DateTime acPob;
        string airline;
        string flightNumber;
        DateTime arrival;
        DateTime depart;
        Insurence insurance;
        Sponsor sponsor;
        string qualification;
        string spokenLanguage;
        string religion;
        string occupation;
        MaritalStatus maritalStatus;
        #endregion


        #region properties
        public int AdditionalFieldId
        {
            get
            {
                return additionalFieldId;
            }
            set
            {
                additionalFieldId = value;
            }
        }
        public string FileNo
        {
            get
            {
                return fileNo;
            }
            set
            {
                fileNo = value;
            }
        }
        public string TrackingNo
        {
            get
            {
                return trackingNo;
            }
            set
            {
                trackingNo = value;
            }
        }
        public string GuranterName
        {
            get
            {
                return guranterName;
            }
            set
            {
                guranterName = value;
            }
        }
        public string GuranterPassport
        {
            get
            {
                return guranterPassport;
            }
            set
            {
                guranterPassport = value;
            }
        }
        public string GuranterNationality
        {
            get
            {
                return guranterNationality;
            }
            set
            {
                guranterNationality = value;
            }
        }
        public string GuranterNationalityOther
        {
            get
            {
                return guranterNationalityOther;
            }
            set
            {
                guranterNationalityOther = value;
            }
        }
        public string GuranterPreNationality
        {
            get
            {
                return guranterPreNationality;
            }
            set
            {
                guranterPreNationality = value;
            }
        }
        public PassportType GuranterPassportType
        {
            get
            {
                return guranterPassportType;
            }
            set
            {
                guranterPassportType = value;
            }
        }
        public DateTime GuranterPassportDate
        {
            get
            {
                return guranterPassportDate;
            }
            set
            {
                guranterPassportDate = value;
            }
        }
        public DateTime GuranterPassportExpiryDate
        {
            get
            {
                return guranterPassportExpiryDate;
            }
            set
            {
                guranterPassportExpiryDate = value;
            }
        }
        public string GuranterPassportCity
        {
            get
            {
                return guranterPassportCity;
            }
            set
            {
                guranterPassportCity = value;
            }
        }
        public string GuranterPassportCountry
        {
            get
            {
                return guranterPassportCountry;
            }
            set
            {
                guranterPassportCountry = value;
            }
        }

        public string GuranterPassportCityOther
        {
            get
            {
                return guranterPassportCityOther;
            }
            set
            {
                guranterPassportCityOther = value;
            }
        }

        public string RealationShipWithGuranter
        {
            get
            {
                return realationShipWithGuranter;
            }
            set
            {
                realationShipWithGuranter = value;
            }
        }
        public string GuranterVisaNo
        {
            get
            {
                return guranterVisaNo;
            }
            set
            {
                guranterVisaNo = value;
            }
        }
        public DateTime GuranterVisaExpiryDate
        {
            get
            {
                return guranterVisaExpiryDate;
            }
            set
            {
                guranterVisaExpiryDate = value;
            }
        }
        public string GuranterOfficeNo
        {
            get
            {
                return guranterOfficeNo;
            }
            set
            {
                guranterOfficeNo = value;
            }
        }
        public string GuranterMobile
        {
            get
            {
                return guranterMobile;
            }
            set
            {
                guranterMobile = value;
            }
        }
        public string GuranterEmailId
        {
            get
            {
                return guranterEmailId;
            }
            set
            {
                guranterEmailId = value;
            }
        }
        public string ReceiptNumber
        {
            get
            {
                return receiptNumber;
            }
            set
            {
                receiptNumber = value;
            }
        }
        public string AcName
        {
            get
            {
                return acName;
            }
            set
            {
                acName = value;
            }
        }
        public Gender AcSex
        {
            get
            {
                return acSex;
            }
            set
            {
                acSex = value;
            }
        }
        public DateTime AcDob
        {
            get
            {
                return acDob;
            }
            set
            {
                acDob = value;
            }
        }
        public DateTime AcPob
        {
            get
            {
                return acPob;
            }
            set
            {
                acPob = value;
            }
        }
        public string Airline
        {
            get
            {
                return airline;
            }
            set
            {
                airline = value;
            }
        }
        public string FlightNumber
        {
            get
            {
                return flightNumber;
            }
            set
            {
                flightNumber = value;
            }
        }
        public DateTime Arrival
        {
            get
            {
                return arrival;
            }
            set
            {
                arrival = value;
            }
        }
        public DateTime Depart
        {
            get
            {
                return depart;
            }
            set
            {
                depart = value;
            }
        }
        public Insurence Insurance
        {
            get
            {
                return insurance;
            }
            set
            {
                insurance = value;
            }
        }
        public Sponsor Sponsor
        {
            get
            {
                return sponsor;
            }
            set
            {
                sponsor = value;
            }
        }
        public string Qualification
        {
            get
            {
                return qualification;
            }
            set
            {
                qualification = value;
            }
        }
        public string SpokenLanguage
        {
            get
            {
                return spokenLanguage;
            }
            set
            {
                spokenLanguage = value;
            }
        }
        public string Religion
        {
            get
            {
                return religion;
            }
            set
            {
                religion = value;
            }
        }
        public string Occupation
        {
            get
            {
                return occupation;
            }
            set
            {
                occupation = value;
            }
        }

        public MaritalStatus MaritalStatus
        {
            get
            {
                return maritalStatus;
            }
            set
            {
                maritalStatus = value;
            }

        }

        #endregion


        #region VisaAdditionalInfo class constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public VisaAdditionalInfo()
        {

        }

        /// <summary>
        /// Constructor which initialise the fields according to paxId
        /// </summary>
        public VisaAdditionalInfo(int additionalFieldId)
        {
            Load(additionalFieldId);

        }

        #endregion


        #region  Method

        public int Save()
        {

            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[36];

            paramList[0] = new SqlParameter("@additionalFieldId", additionalFieldId);

            if (fileNo == null)
            {
                paramList[1] = new SqlParameter("@fileNo", DBNull.Value);
            }
            else
            {
                paramList[1] = new SqlParameter("@fileNo", fileNo);
            }
            if (trackingNo == null)
            {
                paramList[2] = new SqlParameter("@trackingNo", DBNull.Value);
            }
            else
            {
                paramList[2] = new SqlParameter("@trackingNo", trackingNo);
            }
            if (guranterName == null)
            {
                paramList[3] = new SqlParameter("@guranterName", DBNull.Value);
            }
            else
            {
                paramList[3] = new SqlParameter("@guranterName", guranterName);
            }
            if (guranterPassport == null)
            {
                paramList[4] = new SqlParameter("@guranterPassport", DBNull.Value);
            }
            else
            {
                paramList[4] = new SqlParameter("@guranterPassport", guranterPassport);
            }
            if (guranterNationality == null)
            {
                paramList[5] = new SqlParameter("@guranterNationality", DBNull.Value);
            }
            else
            {
                paramList[5] = new SqlParameter("@guranterNationality", guranterNationality);
            }
            if (guranterPreNationality == null)
            {
                paramList[6] = new SqlParameter("@guranterPreNationality", DBNull.Value);
            }
            else
            {
                paramList[6] = new SqlParameter("@guranterPreNationality", guranterPreNationality);
            }
            if (guranterPassportType == 0)
            {
                paramList[7] = new SqlParameter("@guranterPassportType", DBNull.Value);
            }
            else
            {
                paramList[7] = new SqlParameter("@guranterPassportType", guranterPassportType);
            }
            if (guranterPassportDate == new DateTime(1, 1, 1))
            {
                paramList[8] = new SqlParameter("@guranterPassportDate", DBNull.Value);

            }
            else
            {
                paramList[8] = new SqlParameter("@guranterPassportDate", guranterPassportDate);
            }
            if (guranterPassportExpiryDate == new DateTime(1, 1, 1))
            {
                paramList[9] = new SqlParameter("@guranterPassportExpiryDate", DBNull.Value);
            }
            else
            {
                paramList[9] = new SqlParameter("@guranterPassportExpiryDate", guranterPassportExpiryDate);
            }
            if (guranterPassportCity == null)
            {
                paramList[10] = new SqlParameter("@guranterPassportCity", DBNull.Value);
            }
            else
            {
                paramList[10] = new SqlParameter("@guranterPassportCity", guranterPassportCity);
            }
            if (guranterPassportCountry == null)
            {
                paramList[11] = new SqlParameter("@guranterPassportCountry", DBNull.Value);
            }
            else
            {
                paramList[11] = new SqlParameter("@guranterPassportCountry", guranterPassportCountry);
            }
            if (realationShipWithGuranter == null)
            {
                paramList[12] = new SqlParameter("@realationShipWithGuranter", DBNull.Value);
            }
            else
            {
                paramList[12] = new SqlParameter("@realationShipWithGuranter", realationShipWithGuranter);
            }
            if (guranterVisaNo == null)
            {
                paramList[13] = new SqlParameter("@guranterVisaNo", DBNull.Value);
            }
            else
            {
                paramList[13] = new SqlParameter("@guranterVisaNo", guranterVisaNo);
            }
            if (guranterVisaExpiryDate == new DateTime(1, 1, 1))
            {
                paramList[14] = new SqlParameter("@guranterVisaExpiryDate", DBNull.Value);
            }
            else
            {
                paramList[14] = new SqlParameter("@guranterVisaExpiryDate", guranterVisaExpiryDate);
            }
            if (guranterOfficeNo == null)
            {
                paramList[15] = new SqlParameter("@guranterOfficeNo", DBNull.Value);
            }
            else
            {
                paramList[15] = new SqlParameter("@guranterOfficeNo", guranterOfficeNo);
            }
            if (guranterMobile == null)
            {
                paramList[16] = new SqlParameter("@guranterMobile", DBNull.Value);
            }
            else
            {
                paramList[16] = new SqlParameter("@guranterMobile", guranterMobile);
            }
            if (guranterEmailId == null)
            {
                paramList[17] = new SqlParameter("@guranterEmailId", DBNull.Value);
            }
            else
            {
                paramList[17] = new SqlParameter("@guranterEmailId", guranterEmailId);
            }
            if (receiptNumber == null)
            {
                paramList[18] = new SqlParameter("@receiptNumber", DBNull.Value);
            }
            else
            {
                paramList[18] = new SqlParameter("@receiptNumber", receiptNumber);
            }
            if (acName == null)
            {
                paramList[19] = new SqlParameter("@acName", DBNull.Value);
            }
            else
            {
                paramList[19] = new SqlParameter("@acName", acName);
            }
            if ((int)acSex == 0)
            {
                paramList[20] = new SqlParameter("@acSex", DBNull.Value);
            }
            else
            {
                paramList[20] = new SqlParameter("@acSex", acSex);
            }
            if (acDob == new DateTime(1, 1, 1))
            {
                paramList[21] = new SqlParameter("@acDob", DBNull.Value);
            }
            else
            {
                paramList[21] = new SqlParameter("@acDob", acDob);
            }
            if (acPob == new DateTime(1, 1, 1))
            {
                paramList[22] = new SqlParameter("@acPob", DBNull.Value);
            }
            else
            {
                paramList[22] = new SqlParameter("@acPob", acPob);
            }
            if (airline == null)
            {
                paramList[23] = new SqlParameter("@airline", DBNull.Value);
            }
            else
            {
                paramList[23] = new SqlParameter("@airline", airline);
            }
            if (flightNumber == null)
            {
                paramList[24] = new SqlParameter("@flightNumber", DBNull.Value);
            }
            else
            {
                paramList[24] = new SqlParameter("@flightNumber", flightNumber);
            }
            if (arrival == new DateTime(1, 1, 1))
            {
                paramList[25] = new SqlParameter("@arrival", DBNull.Value);
            }
            else
            {
                paramList[25] = new SqlParameter("@arrival", arrival);
            }
            if (depart == new DateTime(1, 1, 1))
            {
                paramList[26] = new SqlParameter("@depart", DBNull.Value);
            }
            else
            {
                paramList[26] = new SqlParameter("@depart", depart);
            }
            if ((int)insurance == 0)
            {
                paramList[27] = new SqlParameter("@insurance", DBNull.Value);
            }
            else
            {
                paramList[27] = new SqlParameter("@insurance", insurance);
            }
            if ((int)sponsor == 0)
            {
                paramList[28] = new SqlParameter("@sponsor", DBNull.Value);
            }
            else
            {
                paramList[28] = new SqlParameter("@sponsor", sponsor);
            }
            if (qualification == null)
            {
                paramList[29] = new SqlParameter("@qualification", DBNull.Value);
            }
            else
            {
                paramList[29] = new SqlParameter("@qualification", qualification);
            }
            if (spokenLanguage == null)
            {
                paramList[30] = new SqlParameter("@spokenLanguage", DBNull.Value);
            }
            else
            {
                paramList[30] = new SqlParameter("@spokenLanguage", spokenLanguage);
            }
            if (religion == null)
            {
                paramList[31] = new SqlParameter("@religion", DBNull.Value);
            }
            else
            {
                paramList[31] = new SqlParameter("@religion", religion);
            }
            if (occupation == null)
            {
                paramList[32] = new SqlParameter("@occupation", DBNull.Value);
            }
            else
            {
                paramList[32] = new SqlParameter("@occupation", occupation);
            }
            if ((int)maritalStatus == 0)
            {
                paramList[33] = new SqlParameter("@maritalStatus", DBNull.Value);
            }
            else
            {
                paramList[33] = new SqlParameter("@maritalStatus", maritalStatus);
            }

            if (guranterPassportCityOther == null)
            {
                paramList[34] = new SqlParameter("@guranterPassportCityOther", DBNull.Value);
            }
            else
            {
                paramList[34] = new SqlParameter("@guranterPassportCityOther", guranterPassportCityOther);
            }



            if (guranterNationalityOther == null)
            {
                paramList[35] = new SqlParameter("@guranterNationalityOther", DBNull.Value);
            }
            else
            {
                paramList[35] = new SqlParameter("@guranterNationalityOther", guranterNationalityOther);
            }

            if (additionalFieldId > 0)
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaAdditionalInfo", paramList);
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddVisaAdditionalInfo", paramList);
                additionalFieldId = Convert.ToInt32(paramList[0].Value);
            }

            return rowsAffected;
        }

        private void ReadDataReader(DataRow reader)
        {
            //if (reader.Read())
            {
                additionalFieldId = Convert.ToInt32(reader["additionalFieldId"]);
                if (reader["fileNo"] != DBNull.Value)
                {
                    fileNo = Convert.ToString(reader["fileNo"]);

                }
                if (reader["trackingNo"] != DBNull.Value)
                {
                    trackingNo = Convert.ToString(reader["trackingNo"]);

                }
                if (reader["guranterName"] != DBNull.Value)
                {
                    guranterName = Convert.ToString(reader["guranterName"]);

                }
                if (reader["guranterPassport"] != DBNull.Value)
                {
                    guranterPassport = Convert.ToString(reader["guranterPassport"]);

                }
                if (reader["guranterNationality"] != DBNull.Value)
                {
                    guranterNationality = Convert.ToString(reader["guranterNationality"]);

                }
                if (reader["guranterPreNationality"] != DBNull.Value)
                {
                    guranterPreNationality = Convert.ToString(reader["guranterPreNationality"]);

                }
                if (reader["guranterPassportType"] != DBNull.Value)
                {
                    guranterPassportType = (PassportType)Convert.ToInt16(reader["guranterPassportType"]);

                }
                if (reader["guranterPassportDate"] != DBNull.Value)
                {
                    guranterPassportDate = Convert.ToDateTime(reader["guranterPassportDate"]);

                }
                if (reader["guranterPassportExpiryDate"] != DBNull.Value)
                {
                    guranterPassportExpiryDate = Convert.ToDateTime(reader["guranterPassportExpiryDate"]);

                }
                if (reader["guranterPassportCity"] != DBNull.Value)
                {
                    guranterPassportCity = Convert.ToString(reader["guranterPassportCity"]);

                }
                if (reader["guranterPassportCountry"] != DBNull.Value)
                {
                    guranterPassportCountry = Convert.ToString(reader["guranterPassportCountry"]);

                }
                if (reader["guranterPassportCityOther"] != DBNull.Value)
                {
                    guranterPassportCityOther = Convert.ToString(reader["guranterPassportCityOther"]);

                }

                if (reader["realationShipWithGuranter"] != DBNull.Value)
                {
                    realationShipWithGuranter = Convert.ToString(reader["realationShipWithGuranter"]);

                }
                if (reader["guranterVisaNo"] != DBNull.Value)
                {
                    guranterVisaNo = Convert.ToString(reader["guranterVisaNo"]);

                }
                if (reader["guranterVisaExpiryDate"] != DBNull.Value)
                {
                    guranterVisaExpiryDate = Convert.ToDateTime(reader["guranterVisaExpiryDate"]);

                }
                if (reader["guranterOfficeNo"] != DBNull.Value)
                {
                    guranterOfficeNo = Convert.ToString(reader["guranterOfficeNo"]);

                }
                if (reader["guranterMobile"] != DBNull.Value)
                {
                    guranterMobile = Convert.ToString(reader["guranterMobile"]);

                }
                if (reader["guranterEmailId"] != DBNull.Value)
                {
                    guranterEmailId = Convert.ToString(reader["guranterEmailId"]);

                }
                if (reader["receiptNumber"] != DBNull.Value)
                {
                    receiptNumber = Convert.ToString(reader["receiptNumber"]);

                }
                if (reader["acName"] != DBNull.Value)
                {
                    acName = Convert.ToString(reader["acName"]);

                }
                if (reader["acSex"] != DBNull.Value)
                {
                    acSex = (Gender)Convert.ToInt32(reader["acSex"]);

                }
                if (reader["acDob"] != DBNull.Value)
                {
                    acDob = Convert.ToDateTime(reader["acDob"]);

                }
                if (reader["acPob"] != DBNull.Value)
                {
                    acPob = Convert.ToDateTime(reader["acPob"]);

                }
                if (reader["airline"] != DBNull.Value)
                {
                    airline = Convert.ToString(reader["airline"]);

                }
                if (reader["flightNumber"] != DBNull.Value)
                {
                    flightNumber = Convert.ToString(reader["flightNumber"]);

                }
                if (reader["arrival"] != DBNull.Value)
                {
                    arrival = Convert.ToDateTime(reader["arrival"]);

                }
                if (reader["depart"] != DBNull.Value)
                {
                    depart = Convert.ToDateTime(reader["depart"]);

                }
                if (reader["insurance"] != DBNull.Value)
                {
                    insurance = (Insurence)Convert.ToInt32(reader["insurance"]);

                }
                if (reader["sponsor"] != DBNull.Value)
                {
                    sponsor = (Sponsor)Convert.ToInt32(reader["sponsor"]);

                }
                if (reader["qualification"] != DBNull.Value)
                {
                    qualification = Convert.ToString(reader["qualification"]);

                }
                if (reader["spokenLanguage"] != DBNull.Value)
                {
                    spokenLanguage = Convert.ToString(reader["spokenLanguage"]);

                }
                if (reader["religion"] != DBNull.Value)
                {
                    religion = Convert.ToString(reader["religion"]);

                }
                if (reader["occupation"] != DBNull.Value)
                {
                    occupation = Convert.ToString(reader["occupation"]);

                }
                if (reader["maritalStatus"] != DBNull.Value)
                {
                    maritalStatus = (MaritalStatus)Convert.ToInt32(reader["maritalStatus"]);

                }
            }
        }

        public void Load(int additionalFieldId)
        {

            SqlParameter[] paramList = new SqlParameter[1]; paramList[0] = new SqlParameter("@additionalFieldId", additionalFieldId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_GetVisaAdditionalInfo", paramList, connection);
                using (DataTable dtVisa = DBGateway.FillDataTableSP("usp_GetVisaAdditionalInfo", paramList))
                {
                    if (dtVisa != null && dtVisa.Rows.Count > 0)
                    {
                        ReadDataReader(dtVisa.Rows[0]);
                        //reader.Close();
                    }
                }
            }

        }

        #endregion
    }
}