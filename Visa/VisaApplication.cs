using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;

using System.Transactions;
using CT.TicketReceipt.DataAccessLayer;
namespace Visa
{



    public class VisaApplication
    {

        #region variable

        int visaId;
        int agencyId;
        int visaTypeId;
        //VisaType visaType;
        string countryCode;
        string destinationCityCode;
        VisaStatus status;
        string remarks;
        string nationality;
        string residence;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        List<VisaPassenger> passengerList;

        #endregion


        #region Properties

        public int VisaId
        {
            get
            {
                return visaId;
            }
            set
            {
                visaId = value;
            }
        }
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        public int VisaTypeId
        {
            get
            {
                return visaTypeId;
            }
            set
            {
                visaTypeId = value;
            }
        }
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }
        public string DestinationCityCode
        {
            get
            {
                return destinationCityCode;
            }
            set
            {
                destinationCityCode = value;
            }
        }
        public VisaStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }
        public string Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                nationality = value;
            }
        }
        public string Residence
        {
            get
            {
                return residence;
            }
            set
            {
                residence = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        public List<VisaPassenger> PassengerList
        {
            get
            {
                if (passengerList == null)
                {
                    passengerList = new List<VisaPassenger>();
                }
                return passengerList;
            }
            set
            {
                passengerList = value;
            }
        }
        #endregion


        #region VisaApplication class constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public VisaApplication()
        {

        }

        /// <summary>
        /// Constructor which initialise the fields according to paxId
        /// </summary>
        public VisaApplication(int visaId)
        {
            Load(visaId);

        }

        #endregion

        #region Method

        private void Validate()
        {
            //Trace.TraceInformation("Visa.BuildValidateMethod entered");
            if (countryCode == null || countryCode.Trim().Length == 0 || countryCode.Trim().Length > 2)
            {
                throw new ArgumentException(" countryCode cannot be null,empty and should be less than 3 characters.");
            }
            if (destinationCityCode == null || destinationCityCode.Trim().Length == 0 || destinationCityCode.Trim().Length > 3)
            {
                throw new ArgumentException(" destinationCityCode cannot be null,empty and should be less than 4 characters.");
            }
            if ((int)status == 0)
            {
                throw new ArgumentException(" status cannot be empty ");
            }
           
            //Trace.TraceInformation("Visa.BuildValidateMethod exited");
        }

        public int Save(int customerId)
        {
            //Trace.TraceInformation("Visa.save entered");
            Validate();
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@visaTypeId", visaTypeId);
            paramList[2] = new SqlParameter("@countryCode", countryCode);
            paramList[3] = new SqlParameter("@destinationCityCode", destinationCityCode);
            paramList[4] = new SqlParameter("@status", (int)status);
            if (remarks == null)
            {
                paramList[5] = new SqlParameter("@remarks", DBNull.Value);
            }
            else
            {
                paramList[5] = new SqlParameter("@remarks", remarks);
            }
            if (nationality == null)
            {
                paramList[6] = new SqlParameter("@nationality", DBNull.Value);
            }
            else
            {
                paramList[6] = new SqlParameter("@nationality", nationality);
            }
            if (residence == null)
            {
                paramList[7] = new SqlParameter("@residence", DBNull.Value);
            }
            else
            {
                paramList[7] = new SqlParameter("@residence", residence);
            }
            paramList[9] = new SqlParameter("@agencyId",agencyId);
            using (TransactionScope updateTransaction = new TransactionScope())
            {
                if (visaId > 0)
                {

                    paramList[8] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                    rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisa", paramList);

                }
                else
                {
                    paramList[0].Direction = ParameterDirection.Output;

                    paramList[8] = new SqlParameter("@createdBy", createdBy);
                    rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddVisa", paramList);
                    visaId = Convert.ToInt32(paramList[0].Value);
                    //VisaApplicationDetails applied = new VisaApplicationDetails();
                    //applied.VisaId = visaId;
                    //applied.CustomerId = customerId;
                    //applied.Save();
                    
                }

                foreach (VisaPassenger passenger in passengerList)
                {
                    passenger.VisaId = visaId;
                    passenger.Address.AddressId = passengerList[0].Address.AddressId;
                    passenger.Price.PriceId = passengerList[0].Price.PriceId;
                    passenger.Save();
                }
                updateTransaction.Complete();
               
            }
            //Trace.TraceInformation("Visa.save exited");
            return rowsAffected;


        }

        private void Read(DataRow dataReader)
        {
            //if (dataReader.Read())
            {
                visaId = Convert.ToInt32(dataReader["visaId"]);
                agencyId= Convert.ToInt32(dataReader["agencyId"]);
                visaTypeId = Convert.ToInt32(dataReader["visaTypeId"]);
                countryCode = Convert.ToString(dataReader["countryCode"]);
                if (dataReader["destinationCityCode"] != DBNull.Value)
                {
                    destinationCityCode = Convert.ToString(dataReader["destinationCityCode"]);
                }
                status = (VisaStatus)Convert.ToInt32(dataReader["status"]);
                
                if (dataReader["remarks"] != DBNull.Value)
                {
                    remarks = Convert.ToString(dataReader["remarks"]);

                }
                if (dataReader["nationality"] != DBNull.Value)
                {
                    nationality = Convert.ToString(dataReader["nationality"]);

                }
                if (dataReader["residence"] != DBNull.Value)
                {
                    residence = Convert.ToString(dataReader["residence"]);

                }
                createdBy = Convert.ToInt32(dataReader["createdBy"]);
                createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                VisaPassenger passenger = new VisaPassenger();
                passenger.VisaId = visaId;
                passengerList = passenger.GetPassengerList();

            }
        }

        public int UpdateVisaStatus()
        {
            int result = 0;

            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@VisaStatusId", (int)status);
            paramList[2] = new SqlParameter("@remarks", Remarks);
            result = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaStatus", paramList);
            return result;

        }

        public void Load(int visaId)
        {
            ////Trace.TraceInformation("Visa.load entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@visaId", visaId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetVisa", paramList, connection);
                using (DataTable dtVisa = DBGateway.FillDataTableSP("usp_GetVisa", paramList))
                {
                    if (dtVisa != null && dtVisa.Rows.Count > 0)
                    {
                        Read(dtVisa.Rows[0]);
                    }
                    //dataReader.Close();
                }
            }
            ////Trace.TraceInformation("Visa.Load exited");
        }

        
        #endregion
    }
}