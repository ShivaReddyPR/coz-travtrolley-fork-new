﻿namespace Visa
{
    internal static class SPNames
    {
        // Visa Queues
        //=====================
        public const string GetResultForAdminTicketQueue = "usp_GetResultForAdminVisaQueue";
        public const string GetResultForAdminVisaQueueByVisaNumber = "usp_GetResultForAdminVisaQueueByVisaId";
        public const string GetResultForAdminVisaQueueByAgencyId = "usp_GetResultForAdminVisaQueueByAgencyId";
        public const string GetResultForAdminVisaQueueByPaxName = "usp_GetResultForAdminVisaQueueByPaxName";
        public const string GetVisaUploadedDocument = "usp_GetVisaUploadedDocument";
        public const string GetVisaUploadedDocumentById = "usp_GetVisaUploadedDocumentById";
        public const string GetVisaQueueSearch = "usp_GetVisaQueueSearch";
        public const string UpdateVisaStatusAndPath = "usp_UpdateVisaStatusAndPath";
        public const string UpdateVisaQueueHistory = "usp_UpdateVisaQueueHistory";

        public const string GetListVisaQueueHistory = "usp_GetListVisaQueueHistory";


        // Visa Faq
        public const string GetVisaFaqList = "usp_GetVisafaq";
        public const string GetVisaFaqById= "usp_GetVisafaqById";
        public const string ChangeVisaFaqStatus = "usp_ChangeVisaFaqStatus";
        public const string AddVisafaq = "usp_AddVisafaq";
        public const string UpdateVisafaq = "usp_UpdateVisafaq";


    }
}
