using System;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;

using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace Visa
{
    public class VisaNationality
    {
        #region Variable
        int nationalityId;
        string nationalityCode;
        string nationalityName;
        bool isActive;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        string nationalityNameAR;
        #endregion


        #region Properties
        public int NationalityId
        {
            get
            {
                return nationalityId;
            }
            set
            {
                nationalityId = value;
            }
        }
        public string NationalityCode
        {
            get
            {
                return nationalityCode;
            }
            set
            {
                nationalityCode = value;
            }
        }
        public string NationalityName
        {
            get
            {
                return nationalityName;
            }
            set
            {
                nationalityName = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        public string NationalityNameAR
        {
            get
            {
                return nationalityNameAR;
            }
            set
            {
                nationalityNameAR = value;
            }
        }


        #endregion


        public VisaNationality()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaType with member detail corresponding to memberId given memberId
        /// </summary>
        /// <param name="VisaTypeId">Visa Type id of the VisaType to be instanciated</param>
        public VisaNationality(int nationalityId)
        {
            Load(nationalityId);
        }

        public VisaNationality(string nationalityCode)
        {
            Load(nationalityCode);
        }


        private void Validate()
        {
            ////Trace.TraceInformation("VisaNationality.BuildValidateMethod entered");
            //if (isActive == null)
            //{
            //    throw new ArgumentException(" isActive cannot be null");
            //}
            if (createdOn == null)
            {
                throw new ArgumentException(" createdOn cannot be null");
            }
            ////Trace.TraceInformation("VisaNationality.BuildValidateMethod exited");
        }


        public int Save()
        {
           
           
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@nationalityId", nationalityId);
            paramList[1] = new SqlParameter("@nationalityCode", nationalityCode);
            paramList[2] = new SqlParameter("@nationalityName", nationalityName);
            paramList[3] = new SqlParameter("@isActive", isActive);
            paramList[5] = new SqlParameter("@nationalityNameAR", nationalityNameAR);
            if (nationalityId > 0)
            {
              
                paramList[4] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaNationality", paramList);
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
               
                paramList[4] = new SqlParameter("@createdBy", createdBy);
               
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddVisaNationality", paramList);
                nationalityId = Convert.ToInt32(paramList[0].Value);
            }
          

            return rowsAffected;
        }

        private void Read(DataRow dataReader)
        {
            //if (dataReader.Read())
            {
                nationalityId = Convert.ToInt32(dataReader["nationalityId"]);
                nationalityCode = Convert.ToString(dataReader["nationalityCode"]);
                nationalityName = Convert.ToString(dataReader["nationalityName"]);
                isActive = Convert.ToBoolean(dataReader["isActive"]);
                createdBy = Convert.ToInt32(dataReader["createdBy"]);
                createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                if (dataReader["nationalityNameAR"] != DBNull.Value)
                {
                    nationalityNameAR = dataReader["nationalityNameAR"].ToString();
                }
                else
                {
                    nationalityNameAR = string.Empty;
                }
                
            }
        }
        public void Load(int nationalityId)
        {
            //Trace.TraceInformation("VisaNationality.load entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@nationalityId", nationalityId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetVisaNationality", paramList, connection);
                using (DataTable dtNationality = DBGateway.FillDataTableSP("usp_GetVisaNationality", paramList))
                {
                    if (dtNationality != null && dtNationality.Rows.Count > 0)
                    {
                        Read(dtNationality.Rows[0]);
                        //dataReader.Close();
                    }
                }
            }
           
        }
        public void Load(string nationalityCode)
        {
            //Trace.TraceInformation("VisaNationality.load entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@nationalityCode", nationalityCode);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetVisaNationalityByCode", paramList, connection);
                using (DataTable dtNationality = DBGateway.FillDataTableSP("usp_GetVisaNationalityByCode", paramList))
                {
                    Read(dtNationality.Rows[0]);
                    //dataReader.Close();
                }
            }

        }

        public static List<VisaNationality> GetNationalityList()
        {

            List<VisaNationality> nationalityList = new List<VisaNationality>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@nationalityId", DBNull.Value);
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaNationality", paramList, connection);
            while (data.Read())
            {
                VisaNationality nationality = new VisaNationality();
                nationality.nationalityId = (int)data["nationalityId"];
                nationality.nationalityName = (String)data["nationalityName"];
                nationality.nationalityCode = (String)data["nationalityCode"];

                if (data["isActive"] == DBNull.Value)
                {
                    nationality.isActive = true;
                }
                else
                {
                    nationality.isActive = (bool)data["isActive"];
                }

                nationalityList.Add(nationality);

            }
            data.Close();
            connection.Close();

           
            return nationalityList;
        }
        public static SortedList GetNationSortredList()
        {

            SortedList NationalityList = new SortedList();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaNationalityByStatus", paramList, connection);
            while (data.Read())
            {
                NationalityList.Add((String)data["nationalityCode"], (String)data["nationalityName"]);
            }
            data.Close();
            connection.Close();
            NationalityList.TrimToSize();
            return NationalityList;
        }



        public static void ChangeStatus(int nationalityId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@nationalityId", nationalityId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaNationalityStatus", paramList);


        }


    }
}
