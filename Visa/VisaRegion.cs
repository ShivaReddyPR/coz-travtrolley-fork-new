﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;


namespace Visa
{
    public class VisaRegion
    {
        #region Variable

        /// <summary>
        /// Name of region
        /// </summary>
        private int regionId;
        /// <summary>
        /// Name of region
        /// </summary>
        private string regionName;
        /// <summary>
        /// two character code of region
        /// </summary>
        private string regionCode;
      
        /// <summary>
        /// Shows either the member is active or not. true for isActive member.
        /// </summary>

        private bool isActive;
        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;




        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets country Id
        /// </summary>
        public int RegionId
        {
            get
            {
                return regionId;
            }
            set
            {
                regionId = value;
            }
        }

        /// <summary>
        /// Gets or sets country name
        /// </summary>
        public string RegionName
        {
            get
            {
                return regionName;
            }
            set
            {
                regionName = value;
            }
        }



        /// <summary>
        /// Gets or sets country code
        /// </summary>
        public string RegionCode
        {
            get
            {
                return regionCode;
            }
            set
            {
                regionCode = value;
            }
        }

        

        /// <summary>
        /// Gets or sets the status of member ( true for active member )
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }


        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        #endregion



        #region Method
         /// <summary>
        /// Constructor : creates an empty instance of visaType
        /// </summary>
        public VisaRegion()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaType with member detail corresponding to memberId given memberId
        /// </summary>
        /// <param name="VisaTypeId">Visa Type id of the VisaType to be instanciated</param>
        public VisaRegion(int regionId)
        {
            Load(regionId);
        }


        /// <summary>
        /// Loads the information about member identified by memberId
        /// </summary>
        /// <param name="memberId">Member id of the member to be loaded</param>
        public void Load(int regionId)
        {

            if (regionId <= 0)
            {
                throw new ArgumentException("regionId must have a positive integer value", "VisaTypeId");
            }
           // SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@regionId", regionId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisaregionById", paramList, connection);
            using (DataTable dtRegion = DBGateway.FillDataTableSP("usp_ReadVisaregionById", paramList))
            {
                if (dtRegion != null && dtRegion.Rows.Count > 0)
                {
                   // if (dataReader.Read())
                    {
                        Read(dtRegion.Rows[0]);
                        //dataReader.Close();
                        //connection.Close();

                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("region does not exist : regionId = " + regionId, "regionId");
                }
            }
        }

        private void Read(DataRow dataReader)
        {
            regionId = Convert.ToInt32(dataReader["regionId"]);
            regionName = Convert.ToString(dataReader["regionName"]);
            regionCode = Convert.ToString(dataReader["regionCode"]);
            isActive = Convert.ToBoolean(dataReader["isActive"]);
            createdOn = Convert.ToDateTime(dataReader["createdOn"]);
            createdBy = Convert.ToInt32(dataReader["createdBy"]);
            lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
            lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
        }



        public int Save()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@regionName", regionName);
            paramList[1] = new SqlParameter("@regionCode", regionCode);
            paramList[2] = new SqlParameter("@createdBy", createdBy);
            paramList[3] = new SqlParameter("@isActive", isActive);
            paramList[4] = new SqlParameter("@ErrMessage", SqlDbType.Int);
            paramList[4].Direction = ParameterDirection.Output;
            DBGateway.ExecuteNonQuerySP("usp_AddVisaRegion", paramList);
            status = (int)paramList[4].Value;
            return status;
        }

        public int UpdateRegion()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@regionName", regionName);
            paramList[1] = new SqlParameter("@regionCode", regionCode);
            paramList[2] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[3] = new SqlParameter("@regionId", regionId);
            paramList[4] = new SqlParameter("@ErrMessage", SqlDbType.Int);
            paramList[4].Direction = ParameterDirection.Output;
            DBGateway.ExecuteNonQuerySP("usp_UpdateVisaRegion", paramList);
            status = (int)paramList[4].Value;
            return status;
        }



        /// <summary>
        /// Gets list of countries with region code
        /// </summary>
        /// <returns>Sorted list of all countries. Region code as key and name as value</returns>
        public static List<VisaRegion> GetRegionList()
        {

            List<VisaRegion> regionList = new List<VisaRegion>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@regionId", DBNull.Value);
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaRegion", paramList, connection);
            while (data.Read())
            {
                VisaRegion region = new VisaRegion();
                region.regionId = (int)data["regionId"];
                region.regionName = (String)data["regionName"];
                region.regionCode = (String)data["regionCode"];
               if (data["isActive"] == DBNull.Value)
                {
                    region.isActive = true;
                }
                else
                {
                    region.isActive = (bool)data["isActive"];
                }

                regionList.Add(region);

            }
            data.Close();
            connection.Close();
          
            return regionList;
        }


        /// <summary>
        /// Gets list of countries with region code
        /// </summary>
        /// <returns>Sorted list of all countries. Region code as key and name as value</returns>
        public static List<VisaRegion> GetActiveRegionList()
        {

            List<VisaRegion> regionList = new List<VisaRegion>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetActiveVisaRegion", paramList, connection);
            while (data.Read())
            {
                VisaRegion region = new VisaRegion();
                region.regionId = (int)data["regionId"];
                region.regionName = (String)data["regionName"];
                region.regionCode = (String)data["regionCode"];
                if (data["isActive"] == DBNull.Value)
                {
                    region.isActive = true;
                }
                else
                {
                    region.isActive = (bool)data["isActive"];
                }
                regionList.Add(region);
            }
            data.Close();
            connection.Close();
            return regionList;
        }





        public static void ChangeStatus(int regionId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@regionId", regionId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaRegionStatus", paramList);


        }



        #endregion


    }
}
