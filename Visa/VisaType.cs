﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace Visa
{
    public class VisaType
    {
        #region   Variable
        /// <summary>
        /// Unique identity number of a visaType
        /// </summary>
        private int visaTypeId;
        /// <summary>
        /// Type / Name of Visa
        /// </summary>
        private string visaTypeName;
        /// <summary>
        /// two character code of country
        /// </summary>
        private string countryCode;
        /// <summary>
        /// Description of visaType
        /// </summary>
        private string description;
        /// <summary>
        /// No of days to process any particular type of visa
        /// </summary>
        private int processingDays;
        /// <summary>
        /// Validity for Number of Days. 
        /// </summary>
        private int validityDays;
        /// <summary>
        /// Validity for Number of Days in other Country. 
        /// </summary>
        private int livingValidity;
        /// <summary>
        /// Shows either the  is active or not. true for isActive member.
        /// </summary>
        private bool isActive;
        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;

        private string visaIconPath;
        private string visaTypeNameAR;
        private string descriptionAR;


        //Written by venkatesh 06-04-2018
        private int agentId;
        private string agentName;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the visaTypeId
        /// </summary>
        public int VisaTypeId
        {
            get
            {
                return visaTypeId;
            }
            set
            {
                visaTypeId = value;
            }
        }


        /// <summary>
        /// Gets or sets the visaTypeName
        /// </summary>
        public string VisaTypeName
        {
            get
            {
                return visaTypeName;
            }
            set
            {
                visaTypeName = value;
            }
        }

        /// <summary>
        /// Gets or sets country code
        /// </summary>
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }


        /// <summary>
        /// Gets or sets the processingDays
        /// </summary>
        public int ProcessingDays
        {
            get
            {
                return processingDays;
            }
            set
            {
                processingDays = value;
            }
        }
        /// <summary>
        /// Gets or sets the validityDays
        /// </summary>
        public int ValidityDays
        {
            get
            {
                return validityDays;
            }
            set
            {
                validityDays = value;
            }
        }

        /// <summary>
        /// Gets or sets the Living Validity in other country
        /// </summary> 
        public int LivingValidity
        {
            get
            {
                return livingValidity;
            }
            set
            {
                livingValidity = value;
            }
        }

        /// <summary>
        /// Gets or sets the status of member ( true for active member )
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }


        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
         /// <summary>
        /// Gets the icon Path 
        /// </summary>
        public string VisaIconPath
        {
            get
            {
                return visaIconPath;
            }
            set
            {
                visaIconPath = value;
            }
        }

        public string VisaTypeNameAR
        {
            get
            {
                return visaTypeNameAR;
            }

            set
            {
                visaTypeNameAR = value;
            }
        }

        public string DescriptionAR
        {
            get
            {
                return descriptionAR;
            }

            set
            {
                descriptionAR = value;
            }
        }
        public int AgentID
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public string AgentName
        {
            get { return agentName; }
            set { agentName = value; }
        }


        #endregion


        #region Method



        /// <summary>
        /// Constructor : creates an empty instance of visaType
        /// </summary>
        public VisaType()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaType with member detail corresponding to memberId given memberId
        /// </summary>
        /// <param name="VisaTypeId">Visa Type id of the VisaType to be instanciated</param>
        public VisaType(int visaTypeId)
        {
            Load(visaTypeId);
        }


        /// <summary>
        /// Loads the information about member identified by memberId
        /// </summary>
        /// <param name="memberId">Member id of the member to be loaded</param>
        public void Load(int visaTypeId)
        {

            if (visaTypeId <= 0)
            {
                throw new ArgumentException("VisaTypeId must have a positive integer value", "VisaTypeId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@visaTypeId", visaTypeId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisaTypeById", paramList, connection);
            using (DataTable dtVisaType = DBGateway.FillDataTableSP("usp_ReadVisaTypeById", paramList))
            {
                if (dtVisaType != null && dtVisaType.Rows.Count > 0)
                {
                    //if (dataReader.Read())
                    {
                        Read(dtVisaType.Rows[0]);
                        //dataReader.Close();
                        //connection.Close();

                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("VisaType does not exist : visaTypeId = " + visaTypeId, "visaTypeId");
                }
            }
        }

        public int Save()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[13];
            paramList[0] = new SqlParameter("@visaType", visaTypeName);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[2] = new SqlParameter("@description", description);
            paramList[3] = new SqlParameter("@processingDays", processingDays);
            paramList[4] = new SqlParameter("@validityDays", validityDays);
            paramList[5] = new SqlParameter("@livingValidity", livingValidity);
            if (visaIconPath != "")
            {
                paramList[8] = new SqlParameter("@visaIconPath", visaIconPath);
            }
            else
            {
                paramList[8] = new SqlParameter("@visaIconPath", DBNull.Value);
            }
            paramList[9] = new SqlParameter("@visaTypeId", visaTypeId);
            paramList[10] = new SqlParameter("@visaTypeNameAR", visaTypeNameAR);
            paramList[11] = new SqlParameter("@vdescriptionAR", descriptionAR);
            paramList[12] = new SqlParameter("@agentId", agentId);
            // if true call update visa type procedure else create new Visa type.......
            if (visaTypeId > 0)
            {
                paramList[6] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramList[7] = new SqlParameter("@isActive", isActive);
                status = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaType", paramList);

            }
            else
            {
                paramList[9].Direction = ParameterDirection.Output;
                paramList[6] = new SqlParameter("@createdBy", createdBy);
                paramList[7] = new SqlParameter("@isActive", isActive);
                status = DBGateway.ExecuteNonQuerySP("usp_AddVisaType", paramList);


            }
            visaTypeId = Convert.ToInt32(paramList[9].Value);

            return status;

        }

        private void Read(DataRow dataReader)
        {
            if (dataReader["visaTypeId"] != DBNull.Value)
            {
                visaTypeId = Convert.ToInt32(dataReader["visaTypeId"]);
            }
            if (dataReader["visaType"] != DBNull.Value)
            {
                visaTypeName = Convert.ToString(dataReader["visaType"]);
            }
            if (dataReader["countryCode"] != DBNull.Value)
            {
                countryCode = Convert.ToString(dataReader["countryCode"]);
            }
            if (dataReader["description"] != DBNull.Value)
            {
                description = Convert.ToString(dataReader["description"]);
            }
            if (dataReader["processingDays"] != DBNull.Value)
            {
                processingDays = Convert.ToInt32(dataReader["processingDays"]);
            }
            if (dataReader["validityDays"] != DBNull.Value)
            {
                validityDays = Convert.ToInt32(dataReader["validityDays"]);
            }
            if (dataReader["livingValidity"] != DBNull.Value)
            {
                livingValidity = Convert.ToInt32(dataReader["livingValidity"]);
            }
            if (dataReader["isActive"] != DBNull.Value)
            {
                isActive = Convert.ToBoolean(dataReader["isActive"]);
            }
            if (dataReader["createdOn"] != DBNull.Value)
            {
                createdOn = Convert.ToDateTime(dataReader["createdOn"]);
            }
            if (dataReader["createdBy"] != DBNull.Value)
            {
                createdBy = Convert.ToInt32(dataReader["createdBy"]);
            }
            if (dataReader["lastModifiedOn"] != DBNull.Value)
            {
                lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
            }
            if (dataReader["lastModifiedBy"] != DBNull.Value)
            {
                lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
            }
            if (dataReader["visaIconPath"] != DBNull.Value)
            {
                visaIconPath = Convert.ToString(dataReader["visaIconPath"]);
            }
            if (dataReader["visaTypeAR"] != DBNull.Value)
            {
                visaTypeNameAR = Convert.ToString(dataReader["visaTypeAR"]);
            }
            else
            {
                visaTypeNameAR = string.Empty;
            }
            if(dataReader["descriptionAR"] != DBNull.Value)
            {
                descriptionAR = Convert.ToString(dataReader["descriptionAR"]);
            }
            else
            {
                descriptionAR = string.Empty;
            }
            if (dataReader["agent_id"] != DBNull.Value)
            {
                agentId= Convert.ToInt32(dataReader["agent_id"]);
            }
            if (dataReader["agent_name"] != DBNull.Value) {
                agentName = Convert.ToString(dataReader["agent_name"]);
            }
        }

      
        public static List<VisaType> GetVisaTypeList(string countryCode, int agentId)
        {

            List<VisaType> visaTypeList = new List<VisaType>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];

            if (!string.IsNullOrEmpty(countryCode)) { paramList[0] = new SqlParameter("@countryCode", countryCode); }
            else { paramList[0] = new SqlParameter("@countryCode", DBNull.Value); }
            if (agentId > 0) { paramList[1] = new SqlParameter("@agentId", agentId); }
            else { paramList[1] = new SqlParameter("@agentId", DBNull.Value); }

            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaTypeList", paramList, connection);
            while (data.Read())
            {
                VisaType visaType = new VisaType();
                if (data["visaTypeId"] != DBNull.Value)
                {
                    visaType.visaTypeId = Convert.ToInt32(data["visaTypeId"]);
                }
                if (data["countryCode"] != DBNull.Value)
                {
                    visaType.countryCode = Convert.ToString(data["countryCode"]);
                }
                if (data["visaType"] != DBNull.Value)
                {
                    visaType.visaTypeName = Convert.ToString(data["visaType"]);
                }
                if (data["description"] != DBNull.Value) {
                    visaType.description = Convert.ToString(data["description"]);
                }
                if (data["validityDays"] != DBNull.Value)
                {
                    visaType.validityDays = Convert.ToInt32(data["validityDays"]);
                }
                if (data["livingValidity"] != DBNull.Value)
                {
                    visaType.livingValidity = Convert.ToInt32(data["livingValidity"]);
                }
                if (data["processingDays"] != DBNull.Value)
                {
                    visaType.processingDays = Convert.ToInt32(data["processingDays"]);
                }
                if (data["visaIconPath"] != DBNull.Value)
                {
                    visaType.visaIconPath = Convert.ToString(data["visaIconPath"]);
                }

                if (data["isActive"] == DBNull.Value)
                {
                    visaType.isActive = true;
                }
                else
                {
                    visaType.isActive = Convert.ToBoolean(data["isActive"]);
                }
                if(data["visaTypeAR"] != DBNull.Value)
                {
                    visaType.visaTypeNameAR = Convert.ToString(data["visaTypeAR"]);
                }
                else
                {
                    visaType.visaTypeNameAR = string.Empty;
                }
                if(data["descriptionAR"] != DBNull.Value)
                {
                    visaType.descriptionAR = Convert.ToString(data["descriptionAR"]);
                }
                else
                {
                    visaType.descriptionAR = string.Empty;
                }
                if(data["agent_name"] != DBNull.Value)
                {
                    visaType.AgentName = Convert.ToString(data["agent_name"]);
                }
                
                visaTypeList.Add(visaType);

            }
            data.Close();
            connection.Close();
           
            return visaTypeList;
        }


        public static List<VisaType> GetAllVisaTypeList()
        {

            List<VisaType> visaTypeList = new List<VisaType>();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetAllVisaType", paramList,connection);
            while (data.Read())
            {
                VisaType visaType = new VisaType();
                visaType.visaTypeId = (int)data["visaTypeId"];
                visaType.visaTypeName = (string)data["visaType"];
                //visaType.visaIconPath = (string)data["visaIconPath"];
                visaTypeList.Add(visaType);
            }
            data.Close();
            connection.Close();

            return visaTypeList;
        }


        public static void ChangeStatus(int visaTypeId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@visaTypeId", visaTypeId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaTypeStatus", paramList);


        }
        #endregion

    }
}
