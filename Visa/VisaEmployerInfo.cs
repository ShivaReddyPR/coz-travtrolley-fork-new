﻿using System;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace Visa
{
    public class VisaEmployerInfo
    {

        #region Variable
        int employerInfoId;
        string employerName;
        string address;
        string designation;
        string visitingCard;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        #endregion



        #region Properties
        public int EmployerInfoId
        {
            get
            {
                return employerInfoId;
            }
            set
            {
                employerInfoId = value;
            }
        }
        public string EmployerName
        {
            get
            {
                return employerName;
            }
            set
            {
                employerName = value;
            }
        }
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        public string Designation
        {
            get
            {
                return designation;
            }
            set
            {
                designation = value;
            }
        }
        public string VisitingCard
        {
            get
            {
                return visitingCard;
            }
            set
            {
                visitingCard = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        #endregion

        #region Visa passenger class constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public VisaEmployerInfo()
        {

        }

        /// <summary>
        /// Constructor which initialise the fields according to paxId
        /// </summary>
        public VisaEmployerInfo(int employerInfoId)
        {
            Load(employerInfoId);

        }



        #endregion

        #region Method

        private void Validate()
        {
            //Trace.TraceInformation("VisaEmployerInfo.BuildValidateMethod entered");
            if (createdOn == null)
            {
                throw new ArgumentException(" createdOn cannot be null");
            }
            //Trace.TraceInformation("VisaEmployerInfo.BuildValidateMethod exited");
        }


        public int Save()
        {
            //Trace.TraceInformation("VisaEmployerInfo.save entered");
            Validate();
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@employerInfoId", employerInfoId);
            if (employerName == null)
            {
                paramList[1] = new SqlParameter("@employerName", DBNull.Value);
            }
            else
            {
                paramList[1] = new SqlParameter("@employerName", employerName);
            }
            if (address == null)
            {
                paramList[2] = new SqlParameter("@address", DBNull.Value);
            }
            else
            {
                paramList[2] = new SqlParameter("@address", address);
            }
            if (designation == null)
            {
                paramList[3] = new SqlParameter("@designation", DBNull.Value);
            }
            else
            {
                paramList[3] = new SqlParameter("@designation", designation);
            }
            if (visitingCard == null)
            {
                paramList[4] = new SqlParameter("@visitingCard", DBNull.Value);
            }
            else
            {
                paramList[4] = new SqlParameter("@visitingCard", visitingCard);
            }
            if (employerInfoId > 0)
            {

                paramList[5] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaEmployerInfo", paramList);
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;

                paramList[5] = new SqlParameter("@createdBy", createdBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddVisaEmployerInfo", paramList);
                employerInfoId = Convert.ToInt32(paramList[0].Value);
            }
            //Trace.TraceInformation("VisaEmployerInfo.save exited");
            return rowsAffected;
        }

        private void ReadDataReader(DataRow reader)
        {
            //if (reader.Read())
            {
                employerInfoId = Convert.ToInt32(reader["employerInfoId"]);
                if (reader["employerName"] != DBNull.Value)
                {
                    employerName = Convert.ToString(reader["employerName"]);

                }
                if (reader["address"] != DBNull.Value)
                {
                    address = Convert.ToString(reader["address"]);

                }
                if (reader["designation"] != DBNull.Value)
                {
                    designation = Convert.ToString(reader["designation"]);

                }
                if (reader["visitingCard"] != DBNull.Value)
                {
                    visitingCard = Convert.ToString(reader["visitingCard"]);

                }
                createdBy = Convert.ToInt32(reader["createdBy"]);
                createdOn = Convert.ToDateTime(reader["createdOn"]);
                lastModifiedBy = Convert.ToInt32(reader["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(reader["lastModifiedOn"]);
            }
        }
        public void Load(int employerInfoId)
        {
            ////Trace.TraceInformation("VisaEmployerInfo.load entered");
            SqlParameter[] paramList = new SqlParameter[1]; paramList[0] = new SqlParameter("@employerInfoId", employerInfoId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_GetVisaEmployerInfo", paramList, connection);
                using (DataTable dtEmployee = DBGateway.FillDataTableSP("usp_GetVisaEmployerInfo", paramList))
                {
                    if (dtEmployee != null && dtEmployee.Rows.Count > 0)
                    {
                        ReadDataReader(dtEmployee.Rows[0]);
                        //reader.Close();
                    }
                }
            }
            //Trace.TraceInformation("VisaEmployerInfo.Load exited");
        }
        #endregion
    }
}