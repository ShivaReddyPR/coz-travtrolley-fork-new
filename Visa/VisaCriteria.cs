﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;

namespace Visa
{
    public class VisaCriteria
    {

        #region   Variable
        /// <summary>
        /// Unique identity number of a criteria
        /// </summary>
        private int criteriaId;
        /// <summary>
        /// CriteriaName
        /// </summary>

        private string criteriaName;
        /// <summary>
        /// two character code of country
        /// </summary>
        private string countryCode;
        /// <summary>
        /// Description of criteria
        /// </summary>
        private string description;


        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// Shows either the criteria is active or not. true for isActive criteria.
        /// </summary>
        private bool isActive;
        private string agency;
        private int agentId;

        #endregion

        #region public properties
        /// <summary>
        /// Gets or sets the criteriaId
        /// </summary>
        public int CriteriaId
        {
            get
            {
                return criteriaId;
            }
            set
            {
                criteriaId = value;
            }
        }


        /// <summary>
        /// Gets or sets the criteriaName
        /// </summary>
        public string CriteriaName
        {
            get
            {
                return criteriaName;
            }
            set
            {
                criteriaName = value;
            }
        }

        /// <summary>
        /// Gets or sets country code
        /// </summary>
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }



        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public string Agency
        {
            get
            {
                return agency;
            }
            set
            {
                agency = value;
            }
        }

        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }

        /// <summary>
        /// Gets or sets the status of criteria ( true for active criteria )
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        #endregion

        #region Method



        /// <summary>
        /// Constructor : creates an empty instance of VisaCriteria
        /// </summary>
        public VisaCriteria()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaCriteria with Criteria detail corresponding to CriteriaId given CriteriaId
        /// </summary>
        /// <param name="criteriaId">criteriaId of the VisaCriteriaId to be instanciated</param>
        public VisaCriteria(int criteriaId)
        {
            Load(criteriaId);
        }

        /// <summary>
        /// Loads the information about member identified by memberId
        /// </summary>
        /// <param name="memberId">Member id of the member to be loaded</param>
        public void Load(int criteriaId)
        {

            if (criteriaId <= 0)
            {
                throw new ArgumentException("VisaTypeId must have a positive integer value", "VisaTypeId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@criteriaId", criteriaId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisaCriteriaById", paramList, connection);
            using (DataTable dtCriteria = DBGateway.FillDataTableSP("usp_ReadVisaCriteriaById", paramList))
            {
                if (dtCriteria != null && dtCriteria.Rows.Count > 0)
                {
                    //if (dataReader.Read())
                    {
                        Read(dtCriteria.Rows[0]);
                        //dataReader.Close();
                        //connection.Close();

                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("VisaType does not exist : visaTypeId = " + criteriaId, "visaTypeId");
                }
            }
        }

      public void Save()
        {
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@criteriaName", criteriaName);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[2] = new SqlParameter("@description", description);
            paramList[5] = new SqlParameter("@agencyId", agentId);
            if (criteriaId > 0)
            {
                paramList[3] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramList[4] = new SqlParameter("@criteriaId", criteriaId);
                DBGateway.ExecuteNonQuerySP("usp_UpdateVisaCriteria", paramList);
            }
            else
            {
                paramList[3] = new SqlParameter("@createdBy", createdBy);
                paramList[4] = new SqlParameter("@isActive", isActive);
                DBGateway.ExecuteNonQuerySP("usp_AddVisaCriteria", paramList);
            }

        }


        public void Update()
        {
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@criteriaName", criteriaName);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[2] = new SqlParameter("@description", description);
            


        }

        private void Read(DataRow dataReader)
        {
            criteriaId = Convert.ToInt32(dataReader["criteriaId"]);
            criteriaName = Convert.ToString(dataReader["criteriaName"]);
            countryCode = Convert.ToString(dataReader["countryCode"]);
            description = Convert.ToString(dataReader["description"]);
            isActive = Convert.ToBoolean(dataReader["isActive"]);
            createdOn = Convert.ToDateTime(dataReader["createdOn"]);
            createdBy = Convert.ToInt32(dataReader["createdBy"]);
            lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
            lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
            agentId = Convert.ToInt32(dataReader["agencyId"]);
        }

       public static List<VisaCriteria> GetCriteriaList(string countryCode,int agentId)
        {

            List<VisaCriteria> criteriaList = new List<VisaCriteria>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@countryCode", countryCode);
            if (agentId > 0) paramList[1] = new SqlParameter("@agencyId", agentId);
            else paramList[1] = new SqlParameter("@agencyId", DBNull.Value);
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaCriteria", paramList, connection);
            while (data.Read())
            {
                VisaCriteria criteria = new VisaCriteria();
                criteria.criteriaId = (int)data["criteriaId"];
                criteria.criteriaName = (String)data["criteriaName"];
                criteria.countryCode = (String)data["countryCode"];
                criteria.description = (String)data["description"];
                criteria.agency = data["agent_Name"] != DBNull.Value ? (String)data["agent_Name"] : string.Empty; 
                if (data["isActive"] == DBNull.Value)
                {
                    criteria.isActive = true;
                }
                else
                {
                    criteria.isActive = (bool)data["isActive"];
                }

                criteriaList.Add(criteria);

            }
            data.Close();
            connection.Close();
            return criteriaList;
        }

        public static void ChangeStatus(int criteriaId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@criteriaId", criteriaId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaCriteriaStatus", paramList);


        }
        #endregion
    }
}
