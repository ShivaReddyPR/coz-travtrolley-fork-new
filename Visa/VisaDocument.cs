﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Collections;


namespace Visa
{
    public class VisaDocument
    {

        #region Variable

        /// <summary>
        /// Name of country
        /// </summary>
        private int documentId;
        /// <summary>
        /// document Type
        /// </summary>
        private string documentType;
        /// <summary>
        /// Name of country
        /// </summary>
        private string documentDescription;
        /// <summary>
        /// two character code of country
        /// </summary>
        private string countryCode;
        /// <summary>
        /// Shows file Type Gif Or JPEG.
        /// </summary>
        private string fileType;
        /// <summary>
        /// Shows file Size In KB.
        /// </summary>
        private int fileSize;
        /// <summary>
        /// Shows height of Image.
        /// </summary>
        private int height;
        /// <summary>
        /// Shows width Of image.
        /// </summary>
        private int width;
        /// <summary>
        /// Shows either the member is active or not. true for isActive member.
        /// </summary>
        private bool isActive;
        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;

        private string agency;
        private int agentId;

        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets country name
        /// </summary>
        public int DocumentId
        {
            get
            {
                return documentId;
            }
            set
            {
                documentId = value;
            }
        }
        /// <summary>
        /// Gets or sets country name
        /// </summary>
        public string DocumentType
        {
            get
            {
                return documentType;
            }
            set
            {
                documentType = value;
            }
        }


        /// <summary>
        /// Gets or sets country name
        /// </summary>
        public string DocumentDescription
        {
            get
            {
                return documentDescription;
            }
            set
            {
                documentDescription = value;
            }
        }



        /// <summary>
        /// Gets or sets country code
        /// </summary>
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }
        public string FileType
        {
            get
            {
                return fileType;
            }
            set
            {
                fileType = value;
            }
        }
        public int FileSize
        {
            get
            {
                return fileSize;
            }
            set
            {
                fileSize = value;
            }
        }
        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        /// <summary>
        /// Gets or sets the status of member ( true for active member )
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }


        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public string Agency
        {
            get
            {
                return agency;
            }
            set
            {
                agency = value;
            }
        }

        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }

        #endregion


        #region Method


        /// <summary>
        /// Constructor : creates an empty instance of visaType
        /// </summary>
        public VisaDocument()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaType with member detail corresponding to memberId given memberId
        /// </summary>
        /// <param name="VisaTypeId">Visa Type id of the VisaType to be instanciated</param>
        public VisaDocument(int documentId)
        {
            Load(documentId);
        }


        /// <summary>
        /// Loads the information about member identified by memberId
        /// </summary>
        /// <param name="memberId">Member id of the member to be loaded</param>
        public void Load(int documentId)
        {

            if (documentId <= 0)
            {
                throw new ArgumentException("documentId must have a positive integer value", "DocumentId");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@documentId", documentId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisaDocumentById", paramList, connection);
            using (DataTable dtDocument = DBGateway.FillDataTableSP("usp_ReadVisaDocumentById", paramList))
            {
                if (dtDocument != null && dtDocument.Rows.Count > 0)
                {
                    //if (dataReader.Read())
                    {
                        Read(dtDocument.Rows[0]);
                        //dataReader.Close();
                        //connection.Close();

                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("country does not exist : countryId = " + documentId, "countryId");
                }
            }
        }




        private void Read(DataRow dataReader)
        {
            documentId =Convert.ToInt32(dataReader["documentId"]);
            documentType = Convert.ToString(dataReader["documentType"]);
            documentDescription = Convert.ToString(dataReader["documentDescription"]);
            countryCode = Convert.ToString(dataReader["countryCode"]);
            fileSize = Convert.ToInt32(dataReader["fileSize"]);
            height = Convert.ToInt32(dataReader["height"]);
            width = Convert.ToInt32(dataReader["width"]);
            fileType = Convert.ToString(dataReader["fileType"]);
            isActive = Convert.ToBoolean(dataReader["isActive"]);
            createdOn = Convert.ToDateTime(dataReader["createdOn"]);
            createdBy = Convert.ToInt32(dataReader["createdBy"]);
            lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
            lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
            agentId  = Convert.ToInt32(dataReader["agencyId"]);


        }

        public int Save()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@documentDescription", documentDescription);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[2] = new SqlParameter("@documentType", documentType);
            paramList[5] = new SqlParameter("@fileType", fileType);
            paramList[6] = new SqlParameter("@fileSize", fileSize);
            paramList[7] = new SqlParameter("@height", height);
            paramList[8] = new SqlParameter("@width", width);
            paramList[9] = new SqlParameter("@agencyId", agentId );



            // if true call update visa Country procedure else create new Visa Country.
            if (documentId > 0)
            {
                paramList[3] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramList[4] = new SqlParameter("@documentId", documentId);
                status = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaDocument", paramList);

            }
            else
            {
                paramList[3] = new SqlParameter("@createdBy", createdBy);
                paramList[4] = new SqlParameter("@isActive", isActive);
                status = DBGateway.ExecuteNonQuerySP("usp_AddVisaDocument", paramList);


            }

            return status;

        }

        public static SortedList GetDocumentTypeList(string countryCode,int agentId)
        {

            SortedList documentList = new SortedList();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@countryCode", countryCode);
            paramList[1] = new SqlParameter("@agencyId", agentId );
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaDocument", paramList, connection);
            while (data.Read())
            {
                documentList.Add(Convert.ToString(data["documentId"]), (String)data["documentType"]);

            }

            data.Close();
            connection.Close();
            documentList.TrimToSize();
            return documentList;
        }
       

        /// <summary>
        /// Gets list of Document with country Code 
        /// </summary>
        /// <returns>Sorted list of all countries. Country code as key and name as value</returns>
        public static List<VisaDocument> GetDocumentList(string countryCode, int agentId)
        {

            List<VisaDocument> documentList = new List<VisaDocument>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
          
            paramList[0] = new SqlParameter("@countryCode", countryCode);
            if (agentId > 0) paramList[1] = new SqlParameter("@agencyId", agentId);
            else paramList[1] = new SqlParameter("@agencyId", DBNull.Value);

            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaDocument", paramList, connection);
            while (data.Read())
            {
                VisaDocument document = new VisaDocument();
                document.documentId = (int)data["documentId"];
                document.documentType = (string)data["documentType"];
                document.countryCode = (string)data["countryCode"];
                document.documentDescription = (string)data["documentDescription"];
                document.fileType = (string)data["fileType"];
                document.fileSize = (int)data["fileSize"];
                document.height = (int)data["height"];
                document.width = (int)data["width"];
                document.agency = data["agent_Name"] != DBNull.Value ? (String)data["agent_Name"] : string.Empty; 
                if (data["isActive"] == DBNull.Value)
                {
                    document.isActive = true;
                }
                else
                {
                    document.isActive = (bool)data["isActive"];
                }

                documentList.Add(document);

            }
            data.Close();
            connection.Close();
            return documentList;
        }


        public static void ChangeStatus(int documentId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@documentId", documentId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaDocumentStatus", paramList);


        }




        #endregion


    }
}
