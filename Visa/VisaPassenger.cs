﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Transactions;
using CT.Core;
using CT.BookingEngine;

namespace Visa
{


    public class VisaPassenger
    {
        #region Variable

        private int paxId;
        private int visaId;
        private string title;
        private string firstname;
        private string lastName;
        private string spouseName;
        private string nationality;
        private string nationalityOther;
        private string preNationality;
        private string fatherName;
        private string motherName;
        private string contactNoCountryCode;
        private string contactNo;
        private string emailId;
        private DateTime dob;
        private string birthPlace;
        private string birthPlaceOther;
        private string birthCountry;
        private Gender sex;
        private string homePhone;
        private int addressId;
        private Address address;
        private int priceId;
        private PriceAccounts price;
        private int additionalFieldId;
        private VisaAdditionalInfo additionalInfo;
        private int employerInfoId;
        private VisaEmployerInfo employerInfo;
        private string passportNo;
        private PassportType passportType;
        private string passprotIssueCity;
        private string passprotIssueCityOther;
        private string passportIssueCountry;
        private DateTime passportIssueDate;
        private string passportIssuingAuthority;
        private DateTime passportExpiryDate;
        private DateTime travelDate;
        private DateTime exitDate;
        private int createdBy;
        private string visaNumber;
        private DateTime visaIssue;
        private DateTime visaExpiry;
        private DateTime visaProcessingLastDate;
        private DateTime createdOn;
        private int lastModifiedBy;
        private DateTime lastModifiedOn;
        private List<VisaUploadedDocument> uploadedDocument;
        private string visaDocumentPath;
        private VisaPassengerStatus status;
        private VisaPassengerPaymentStatus paymentStatus;
        private string remarks;
      

        #endregion


        #region properties

        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }
        public int VisaId
        {
            get
            {
                return visaId;
            }
            set
            {
                visaId = value;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }
        public string Firstname
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
        public string SpouseName
        {
            get
            {
                return spouseName;
            }
            set
            {
                spouseName = value;
            }
        }

        public string Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                nationality = value;
            }
        }
        public string NationalityOther
        {
            get
            {
                return nationalityOther;
            }
            set
            {
                nationalityOther = value;
            }
        }
        public string PreNationality
        {
            get
            {
                return preNationality;
            }
            set
            {
                preNationality = value;
            }
        }
        public string FatherName
        {
            get
            {
                return fatherName;
            }
            set
            {
                fatherName = value;
            }
        }
        public string MotherName
        {
            get
            {
                return motherName;
            }
            set
            {
                motherName = value;
            }
        }

        public string ContactNoCountryCode
        {
            get
            {
                return contactNoCountryCode;
            }
            set
            {
                contactNoCountryCode = value;
            }
        }
        public string ContactNo
        {
            get
            {
                return contactNo;
            }
            set
            {
                contactNo = value;
            }
        }
        public string EmailId
        {
            get
            {
                return emailId;
            }
            set
            {
                emailId = value;
            }
        }
        public DateTime Dob
        {
            get
            {
                return dob;
            }
            set
            {
                dob = value;
            }
        }

        public string BirthPlace
        {
            get
            {
                return birthPlace;
            }
            set
            {
                birthPlace = value;
            }
        }
        public string BirthPlaceOther
        {
            get
            {
                return birthPlaceOther;
            }
            set
            {
                birthPlaceOther = value;
            }
        }
        public string BirthCountry
        {
            get
            {
                return birthCountry;
            }
            set
            {
                birthCountry = value;
            }
        }
        public Gender Sex
        {
            get
            {
                return sex;
            }
            set
            {
                sex = value;
            }
        }
        public string HomePhone
        {
            get
            {
                return homePhone;
            }
            set
            {
                homePhone = value;
            }
        }
        public int AddressId
        {
            get
            {
                return addressId;
            }
            set
            {
                addressId = value;
            }
        }


        public Address Address
        {
            get
            {
                if (this.address == null)
                {
                    address = new Address();
                }
                return address;
            }
            set
            {
                address = value;
            }
        }
        public int PriceId
        {
            get
            {
                return priceId;
            }
            set
            {
                priceId = value;
            }
        }
        public PriceAccounts Price
        {
            get
            {
                if (price == null)
                {
                    price = new PriceAccounts();
                }
                return price;
            }
            set
            {
                price = value;
            }
        }

        public int AdditionalFieldId
        {
            get
            {
                return additionalFieldId;
            }
            set
            {
                additionalFieldId = value;
            }
        }


        public VisaAdditionalInfo AdditionalInfo
        {
            get
            {
                if (this.additionalInfo == null)
                {
                    additionalInfo = new VisaAdditionalInfo();
                }
                return additionalInfo;
            }
            set
            {
                additionalInfo = value;
            }
        }

        /* private int employerInfoId;
        private VisaEmployerInfo employerInfo;*/

        public int EmployerInfoId
        {
            get
            {
                return employerInfoId;
            }
            set
            {
                employerInfoId = value;
            }
        }


        public VisaEmployerInfo EmployerInfo
        {
            get
            {
                if (this.employerInfo == null)
                {
                    employerInfo = new VisaEmployerInfo();
                }
                return employerInfo;
            }
            set
            {
                employerInfo = value;
            }
        }

        public string PassportNo
        {
            get
            {
                return passportNo;
            }
            set
            {
                passportNo = value;
            }
        }
        public PassportType PassportType
        {
            get
            {
                return passportType;
            }
            set
            {
                passportType = value;
            }
        }
        public string PassprotIssueCity
        {
            get
            {
                return passprotIssueCity;
            }
            set
            {
                passprotIssueCity = value;
            }
        }
        public string PassprotIssueCityOther
        {
            get
            {
                return passprotIssueCityOther;
            }
            set
            {
                passprotIssueCityOther = value;
            }
        }
       
        public string PassportIssueCountry
        {
            get
            {
                return passportIssueCountry;
            }
            set
            {
                passportIssueCountry = value;
            }
        }

        public DateTime PassportIssueDate
        {
            get
            {
                return passportIssueDate;
            }
            set
            {
                passportIssueDate = value;
            }
        }
        public string PassportIssuingAuthority
        {
            get
            {
                return passportIssuingAuthority;
            }
            set
            {
                passportIssuingAuthority = value;
            }
        }
        public DateTime PassportExpiryDate
        {
            get
            {
                return passportExpiryDate;
            }
            set
            {
                passportExpiryDate = value;
            }
        }
        public DateTime TravelDate
        {
            get
            {
                return travelDate;
            }
            set
            {
                travelDate = value;
            }
        }
        public DateTime ExitDate
        {
            get
            {
                return exitDate;
            }
            set
            {
                exitDate = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public string VisaNumber
        {
            get
            {
                return visaNumber;
            }
            set
            {
                visaNumber = value;
            }
        }
        public DateTime VisaIssue
        {
            get
            {
                return visaIssue;
            }
            set
            {
                visaIssue = value;
            }
        }
        public DateTime VisaExpiry
        {
            get
            {
                return visaExpiry;
            }
            set
            {
                visaExpiry = value;
            }
        }
        public DateTime VisaProcessingLastDate
        {
            get
            {
                return visaProcessingLastDate;
            }
            set
            {
                visaProcessingLastDate = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }


        public List<VisaUploadedDocument> UploadedDocument
        {
            get
            {
                if (uploadedDocument == null)
                {
                    uploadedDocument = new List<VisaUploadedDocument>();

                }
                return uploadedDocument;
            }
            set
            {
                uploadedDocument = value;
            }
        }
        public string VisaDocumentPath
        {
            get
            {
                return visaDocumentPath;
            }
            set
            {
                visaDocumentPath = value;
            }
        }
        public VisaPassengerStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public VisaPassengerPaymentStatus PaymentStatus
        {
            get
            {
                return paymentStatus;
            }
            set
            {
                paymentStatus = value;
            }
        }
        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }


        #endregion

        

        #region Visa passenger class constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public VisaPassenger()
        {

        }

        /// <summary>
        /// Constructor which initialise the fields according to paxId
        /// </summary>
        public VisaPassenger(int paxId)
        {
            Load(paxId);

        }

        #endregion

        #region Method

        private void Validate()
        {
            ////Trace.TraceInformation("Visapassenger.BuildValidateMethod entered");
            if (firstname == null || firstname.Trim().Length == 0 || firstname.Trim().Length > 50)
            {
                throw new ArgumentException(" firstname cannot be null,empty and should be less than 51 characters.");
            }
            if (emailId == null || emailId.Trim().Length == 0 || emailId.Trim().Length > 50)
            {
                throw new ArgumentException(" emailId cannot be null,empty and should be less than 51 characters.");
            }
            if ((int)sex == 0)
            {
                throw new ArgumentException(" sex cannot be empty ");
            }
            if (passportNo == null || passportNo.Trim().Length == 0 || passportNo.Trim().Length > 20)
            {
                throw new ArgumentException(" passportNo cannot be null,empty and should be less than 21 characters.");
            }
            if (passportIssueDate == null)
            {
                throw new ArgumentException(" passportIssueDate cannot be null");
            }
            if (passportIssuingAuthority == null || passportIssuingAuthority.Trim().Length == 0 || passportIssuingAuthority.Trim().Length > 50)
            {
                throw new ArgumentException(" passportIssuingAuthority cannot be null,empty and should be less than 51 characters.");
            }
            if (passportExpiryDate == null)
            {
                throw new ArgumentException(" passportExpiryDate cannot be null");
            }
            if (createdOn == null)
            {
                throw new ArgumentException(" createdOn cannot be null");
            }
            ////Trace.TraceInformation("Visapassenger.BuildValidateMethod exited");
        }

        public int Save()
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[35];
            paramList[0] = new SqlParameter("@paxId", paxId);
            paramList[1] = new SqlParameter("@visaId", visaId);
            if (title == null)
            {
                paramList[2] = new SqlParameter("@title", DBNull.Value);
            }
            else
            {
                paramList[2] = new SqlParameter("@title", title);
            }
            paramList[3] = new SqlParameter("@firstname", firstname);
            if (lastName == null)
            {
                paramList[4] = new SqlParameter("@lastName", DBNull.Value);
            }
            else
            {
                paramList[4] = new SqlParameter("@lastName", lastName);
            }
           

            if (nationality == null)
            {
                paramList[5] = new SqlParameter("@nationality", DBNull.Value);
            }
            else
            {
                paramList[5] = new SqlParameter("@nationality", nationality);
            }
            if (preNationality == null)
            {
                paramList[6] = new SqlParameter("@preNationality", DBNull.Value);
            }
            else
            {
                paramList[6] = new SqlParameter("@preNationality", preNationality);
            }
            if (fatherName == null)
            {
                paramList[7] = new SqlParameter("@fatherName", DBNull.Value);
            }
            else
            {
                paramList[7] = new SqlParameter("@fatherName", fatherName);
            }
            if (motherName == null)
            {
                paramList[8] = new SqlParameter("@motherName", DBNull.Value);
            }
            else
            {
                paramList[8] = new SqlParameter("@motherName", motherName);
            }
            if (contactNo == null)
            {
                paramList[9] = new SqlParameter("@contactNo", DBNull.Value);
            }
            else
            {
                paramList[9] = new SqlParameter("@contactNo", contactNo);
            }
            if (string.IsNullOrEmpty(emailId))
            {
                paramList[10] = new SqlParameter("@emailId", DBNull.Value);
            }
            else
            {
                paramList[10] = new SqlParameter("@emailId", emailId);
            }
            if (dob == null)
            {
                paramList[11] = new SqlParameter("@dob", DBNull.Value);
            }
            else
            {
                paramList[11] = new SqlParameter("@dob", dob);
            }

            if (birthPlace == null)
            {
                paramList[12] = new SqlParameter("@birthPlace", DBNull.Value);
            }
            else
            {
                paramList[12] = new SqlParameter("@birthPlace", birthPlace);
            }
            if (birthCountry == null)
            {
                paramList[13] = new SqlParameter("@birthCountry", DBNull.Value);
            }
            else
            {
                paramList[13] = new SqlParameter("@birthCountry", birthCountry);
            }
            paramList[14] = new SqlParameter("@sex", sex);
            if (homePhone == null)
            {
                paramList[15] = new SqlParameter("@homePhone", DBNull.Value);
            }
            else
            {
                paramList[15] = new SqlParameter("@homePhone", homePhone);
            }
            //paramList[16] = new SqlParameter("@addressId", addressId);
            //paramList[17] = new SqlParameter("@priceId", priceId);
            //paramList[18] = new SqlParameter("@additionalFieldId", additionalFieldId);
           
            paramList[19] = new SqlParameter("@passportNo", passportNo);
            if ((int)passportType == 0)
            {
                paramList[20] = new SqlParameter("@passportType", DBNull.Value);
            }
            else
            {
                paramList[20] = new SqlParameter("@passportType", passportType);
            }
            if (passprotIssueCity == null)
            {
                paramList[21] = new SqlParameter("@passprotIssueCity", DBNull.Value);
            }
            else
            {
                paramList[21] = new SqlParameter("@passprotIssueCity", passprotIssueCity);
            }
            if (passportIssueCountry == null)
            {
                paramList[22] = new SqlParameter("@passportIssueCountry", DBNull.Value);
            }
            else
            {
                paramList[22] = new SqlParameter("@passportIssueCountry", passportIssueCountry);
            }
            paramList[23] = new SqlParameter("@passportIssueDate", passportIssueDate);
            paramList[24] = new SqlParameter("@passportIssuingAuthority", passportIssuingAuthority);
            paramList[25] = new SqlParameter("@passportExpiryDate", passportExpiryDate);
            if (travelDate == null)
            {
                paramList[26] = new SqlParameter("@travelDate", DBNull.Value);
            }
            else
            {
                paramList[26] = new SqlParameter("@travelDate", travelDate);
            }
            if (exitDate == null)
            {
                paramList[27] = new SqlParameter("@exitDate", DBNull.Value);
            }
            else
            {
                paramList[27] = new SqlParameter("@exitDate", exitDate);
            }
            if (visaNumber == null)
            {
                paramList[28] = new SqlParameter("@visaNumber", DBNull.Value);
            }
            else
            {
                paramList[28] = new SqlParameter("@visaNumber", visaNumber);
            }
            if (visaIssue == null)
            {
                paramList[29] = new SqlParameter("@visaIssue", DBNull.Value);
            }
            else
            {
                paramList[29] = new SqlParameter("@visaIssue", visaIssue);
            }
            if (visaExpiry == null)
            {
                paramList[30] = new SqlParameter("@visaExpiry", DBNull.Value);
            }
            else
            {
                paramList[30] = new SqlParameter("@visaExpiry", visaExpiry);
            }
            if (visaProcessingLastDate == null)
            {
                paramList[31] = new SqlParameter("@visaProcessingLastDate", DBNull.Value);
            }
            else
            {
                paramList[31] = new SqlParameter("@visaProcessingLastDate", visaProcessingLastDate);
            }

            if (contactNoCountryCode == null)
            {
                paramList[34] = new SqlParameter("@ContactNoCountryCode", DBNull.Value);
            }
            else
            {
                paramList[34] = new SqlParameter("@ContactNoCountryCode", contactNoCountryCode);
            }

            
            if (paxId > 0)
            {
               
                paramList[32] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                using (TransactionScope tr = new TransactionScope())
                {
                    address.Save();
                    price.Save();
                    additionalInfo.Save();
                    paramList[16] = new SqlParameter("@addressId", address.AddressId);
                    paramList[17] = new SqlParameter("@priceId", price.PriceId);
                    paramList[18] = new SqlParameter("@additionalFieldId", additionalInfo.AdditionalFieldId);
                    rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisapassenger", paramList);

                }
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
               
                paramList[32] = new SqlParameter("@createdBy", createdBy);
                using (TransactionScope tr = new TransactionScope())
                {
                    if (address.AddressId <= 0)
                    {

                        address.Save();
                        employerInfo.Save();
                        paramList[33] = new SqlParameter("@employerInfoId",employerInfo.EmployerInfoId);
                    }
                    else 
                    {
                        paramList[33] = new SqlParameter("@employerInfoId", DBNull.Value);
                    }

                  
                    price.Save();
                    additionalInfo.Save();
                    paramList[16] = new SqlParameter("@addressId", address.AddressId);
                    paramList[17] = new SqlParameter("@priceId", price.PriceId);
                    paramList[18] = new SqlParameter("@additionalFieldId", additionalInfo.AdditionalFieldId);
                    rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddVisapassenger", paramList);
                    tr.Complete();
                }
                paxId = Convert.ToInt32(paramList[0].Value);
            }


            return rowsAffected;
        }

        private void Read(DataRow dataReader)
        {
            //if (dataReader.Read())
            {
                paxId = Convert.ToInt32(dataReader["paxId"]);
                visaId = Convert.ToInt32(dataReader["visaId"]);
                if (dataReader["title"] != DBNull.Value)
                {
                    title = Convert.ToString(dataReader["title"]);
                }
                firstname = Convert.ToString(dataReader["firstname"]);
                if (dataReader["lastName"] != DBNull.Value)
                {
                    lastName = Convert.ToString(dataReader["lastName"]);

                }
                if (dataReader["nationality"] != DBNull.Value)
                {
                    nationality = Convert.ToString(dataReader["nationality"]);

                }
                if (dataReader["nationalityOther"] != DBNull.Value)
                {
                    nationalityOther = Convert.ToString(dataReader["nationalityOther"]);

                }
                if (dataReader["preNationality"] != DBNull.Value)
                {
                    preNationality = Convert.ToString(dataReader["preNationality"]);

                }
                if (dataReader["fatherName"] != DBNull.Value)
                {
                    fatherName = Convert.ToString(dataReader["fatherName"]);

                }
                if (dataReader["motherName"] != DBNull.Value)
                {
                    motherName = Convert.ToString(dataReader["motherName"]);
                }
                if (dataReader["ContactNoCountryCode"] != DBNull.Value)
                {
                    contactNoCountryCode = Convert.ToString(dataReader["ContactNoCountryCode"]);
                }
                if (dataReader["contactNo"] != DBNull.Value)
                {
                    contactNo = Convert.ToString(dataReader["contactNo"]);

                }
                emailId = Convert.ToString(dataReader["emailId"]);
                if (dataReader["dob"] != DBNull.Value)
                {
                    dob = Convert.ToDateTime(dataReader["dob"]);

                }
                if (dataReader["birthPlace"] != DBNull.Value)
                {
                    birthPlace = Convert.ToString(dataReader["birthPlace"]);

                }
                if (dataReader["birthCountry"] != DBNull.Value)
                {
                    birthCountry = Convert.ToString(dataReader["birthCountry"]);

                }
                if (dataReader["sex"] != DBNull.Value)
                {
                    sex = (Gender)Convert.ToInt32(dataReader["sex"]);
                }
                if (dataReader["homePhone"] != DBNull.Value)
                {
                    homePhone = Convert.ToString(dataReader["homePhone"]);

                }
                if (dataReader["addressId"] != DBNull.Value)
                {
                    addressId = Convert.ToInt32(dataReader["addressId"]);
                    address = new Address(addressId);
                }
                priceId = Convert.ToInt32(dataReader["priceId"]);
                price = new PriceAccounts();
                price.Load(priceId);
                if (dataReader["additionalFieldId"] != DBNull.Value)
                {
                    additionalFieldId = Convert.ToInt32(dataReader["additionalFieldId"]);
                    additionalInfo = new VisaAdditionalInfo(additionalFieldId);
                }
                if (dataReader["passportNo"] != DBNull.Value)
                {
                    passportNo = Convert.ToString(dataReader["passportNo"]);
                }
                if (dataReader["passportType"] != DBNull.Value)
                {
                    passportType = (PassportType)Convert.ToInt32(dataReader["passportType"]);

                }
                if (dataReader["passprotIssueCity"] != DBNull.Value)
                {
                    passprotIssueCity = Convert.ToString(dataReader["passprotIssueCity"]);

                }
                if (dataReader["passportIssueCountry"] != DBNull.Value)
                {
                    passportIssueCountry = Convert.ToString(dataReader["passportIssueCountry"]);

                }
                if (dataReader["passportIssueDate"] != DBNull.Value)
                {
                    passportIssueDate = Convert.ToDateTime(dataReader["passportIssueDate"]);

                }
                if (dataReader["passportIssuingAuthority"] != DBNull.Value)
                {
                    passportIssuingAuthority = Convert.ToString(dataReader["passportIssuingAuthority"]);
                }
                if (dataReader["passportExpiryDate"] != DBNull.Value)
                {
                    passportExpiryDate = Convert.ToDateTime(dataReader["passportExpiryDate"]);
                }
                
                if (dataReader["travelDate"] != DBNull.Value)
                {
                    travelDate = Convert.ToDateTime(dataReader["travelDate"]);

                }
                if (dataReader["exitDate"] != DBNull.Value)
                {
                    exitDate = Convert.ToDateTime(dataReader["exitDate"]);

                }
                createdBy = Convert.ToInt32(dataReader["createdBy"]);
                if (dataReader["visaNumber"] != DBNull.Value)
                {
                    visaNumber = Convert.ToString(dataReader["visaNumber"]);

                }
                if (dataReader["visaIssue"] != DBNull.Value)
                {
                    visaIssue = Convert.ToDateTime(dataReader["visaIssue"]);

                }
                if (dataReader["visaExpiry"] != DBNull.Value)
                {
                    visaExpiry = Convert.ToDateTime(dataReader["visaExpiry"]);

                }
                if (dataReader["visaProcessingLastDate"] != DBNull.Value)
                {
                    visaProcessingLastDate = Convert.ToDateTime(dataReader["visaProcessingLastDate"]);

                }
                if (dataReader["birthPlaceOther"] != DBNull.Value)
                {
                    birthPlaceOther =Convert.ToString(dataReader["birthPlaceOther"]);
                }

                if (dataReader["passprotIssueCityOther"] != DBNull.Value)
                {
                    passprotIssueCityOther = Convert.ToString(dataReader["passprotIssueCityOther"]);
                }
                if (dataReader["spouseName"] != DBNull.Value)
                {
                    spouseName = Convert.ToString(dataReader["spouseName"]);

                }

                if (dataReader["remarks"] != DBNull.Value)
                {
                    remarks = Convert.ToString(dataReader["remarks"]);
                }
                if (dataReader["status"] != DBNull.Value)
                {
                    status = (VisaPassengerStatus)Convert.ToInt32(dataReader["status"]);
                }
                if (dataReader["paymentStatus"] != DBNull.Value)
                {
                    paymentStatus = (VisaPassengerPaymentStatus)Convert.ToInt32(dataReader["paymentStatus"]);
                }

                
                createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                uploadedDocument = VisaUploadedDocument.GetUploadedDocumentList(paxId);
            }
        }

        public void Load(int paxId)
        {

            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
           // using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetVisapassenger", paramList, connection);
                using (DataTable dtPassenger = DBGateway.FillDataTableSP("usp_GetVisapassenger", paramList))
                {
                    if (dtPassenger != null && dtPassenger.Rows.Count > 0)
                    {
                        Read(dtPassenger.Rows[0]);
                     //   dataReader.Close();
                    }
                }
            }
        }


        public int UpdatePassengerStatus()
        {
            int result = 0;

            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@paxId", paxId);
            paramList[2] = new SqlParameter("@paxStatus",status);
            paramList[3] = new SqlParameter("@paxRemarks",remarks);
            result = DBGateway.ExecuteNonQuerySP("usp_UpdatePassengerStatus", paramList);
            return result;

        }

        public int UpdatePaxStatusAndPath()
        {
            int result = 0;

            SqlParameter[] paramList = new SqlParameter[8];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@paxId", paxId);
            paramList[2] = new SqlParameter("@paxStatus", status);
            paramList[3] = new SqlParameter("@visDocumentPath", visaDocumentPath);
            paramList[4] = new SqlParameter("@visaNumber", visaNumber);
            paramList[5] = new SqlParameter("@visaIssue", visaIssue);
            paramList[6] = new SqlParameter("@visaExpiry", visaExpiry);
            paramList[7] = new SqlParameter("@paxRemarks", remarks);
            result = DBGateway.ExecuteNonQuerySP(SPNames.UpdateVisaStatusAndPath, paramList);
            return result;

        }
        public List<VisaPassenger> GetPassengerList()
        {
            List<VisaPassenger> passengerList = new List<VisaPassenger>();
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@visaId", visaId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaPassengerList", paramList, connection);
            using (DataTable dtPass = DBGateway.FillDataTableSP("usp_GetVisaPassengerList", paramList))
            {
                if (dtPass != null && dtPass.Rows.Count > 0)
                {
                    foreach(DataRow data in dtPass.Rows)
                    {
                        VisaPassenger passenger = new VisaPassenger();
                        passenger.paxId =Convert.ToInt32(data["paxId"]);
                        if (data["title"] != DBNull.Value)
                        {
                            passenger.title = Convert.ToString(data["title"]);
                        }

                        passenger.firstname = Convert.ToString(data["firstname"]);
                        if (data["lastName"] != DBNull.Value)
                        {
                            passenger.lastName = Convert.ToString(data["lastName"]);
                        }
                        if (data["nationality"] != DBNull.Value)
                        {
                            passenger.nationality = Convert.ToString(data["nationality"]);

                        }
                        if (data["nationalityOther"] != DBNull.Value)
                        {
                            passenger.nationalityOther = Convert.ToString(data["nationalityOther"]);

                        }
                        if (data["preNationality"] != DBNull.Value)
                        {
                            passenger.preNationality = Convert.ToString(data["preNationality"]);

                        }
                        if (data["fatherName"] != DBNull.Value)
                        {
                            passenger.fatherName = Convert.ToString(data["fatherName"]);

                        }
                        if (data["motherName"] != DBNull.Value)
                        {
                            passenger.motherName = Convert.ToString(data["motherName"]);
                        }
                        if (data["homePhone"] != DBNull.Value)
                        {
                            passenger.homePhone = Convert.ToString(data["homePhone"]);
                        }
                        if (data["passportType"] != DBNull.Value)
                        {
                            passenger.passportType = (PassportType)Convert.ToInt32(data["passportType"]);

                        }
                        if (data["sex"] != DBNull.Value)
                        {
                            passenger.sex = (Gender)Convert.ToInt32(data["sex"]);
                        }
                        if (data["contactNo"] != DBNull.Value)
                        {
                            passenger.contactNo = Convert.ToString(data["contactNo"]);
                        }
                        if (data["travelDate"] != DBNull.Value)
                        {
                            passenger.travelDate =Convert.ToDateTime(data["travelDate"]);
                        }
                        if (data["birthCountry"] != DBNull.Value)
                        {
                            passenger.birthCountry = Convert.ToString(data["birthCountry"]);
                        }
                        if (data["birthPlace"] != DBNull.Value)
                        {
                            passenger.birthPlace = Convert.ToString(data["birthPlace"]);
                        }
                        if (data["passportIssueCountry"] != DBNull.Value)
                        {
                            passenger.passportIssueCountry = Convert.ToString(data["passportIssueCountry"]);
                        }
                        if (data["passprotIssueCity"] != DBNull.Value)
                        {
                            passenger.passprotIssueCity = Convert.ToString(data["passprotIssueCity"]);
                        }

                        if (data["birthPlaceOther"] != DBNull.Value)
                        {
                            passenger.birthPlaceOther = Convert.ToString(data["birthPlaceOther"]);
                        }

                        if (data["passprotIssueCityOther"] != DBNull.Value)
                        {
                            passenger.passprotIssueCityOther = Convert.ToString(data["passprotIssueCityOther"]);
                        }
                        if (data["passportNo"] != DBNull.Value)
                        {
                            passenger.passportNo = Convert.ToString(data["passportNo"]);
                        }
                        if (data["addressId"] != DBNull.Value)
                        {
                            passenger.addressId = Convert.ToInt32(data["addressId"]);
                            passenger.address = new Address(passenger.addressId);
                        }
                        if (data["additionalFieldId"] != DBNull.Value)
                        {
                            passenger.additionalFieldId = Convert.ToInt32(data["additionalFieldId"]);
                            passenger.additionalInfo = new VisaAdditionalInfo(passenger.additionalFieldId);
                        }
                        passenger.priceId = Convert.ToInt32(data["priceId"]);
                        passenger.price = new PriceAccounts();
                        passenger.price.Load(passenger.priceId);


                        if (data["spouseName"] != DBNull.Value)
                        {
                            passenger.spouseName = Convert.ToString(data["spouseName"]);
                        }

                        if (data["remarks"] != DBNull.Value)
                        {
                            passenger.remarks = Convert.ToString(data["remarks"]);
                        }
                        if (data["status"] != DBNull.Value)
                        {
                            passenger.status = (VisaPassengerStatus)Convert.ToInt32(data["status"]);
                        }
                        if (data["paymentStatus"] != DBNull.Value)
                        {
                            passenger.paymentStatus = (VisaPassengerPaymentStatus)Convert.ToInt32(data["paymentStatus"]);

                        }
                        if (data["passportNo"] != DBNull.Value)
                        {
                            passenger.passportNo = Convert.ToString(data["passportNo"]);
                        }
                        if (data["passportIssuingAuthority"] != DBNull.Value)
                        {
                            passenger.passportIssuingAuthority = Convert.ToString(data["passportIssuingAuthority"]);
                        }
                        if (data["passportIssueDate"] != DBNull.Value)
                        {
                            passenger.passportIssueDate = Convert.ToDateTime(data["passportIssueDate"]);

                        }
                        if (data["employerInfoId"] != DBNull.Value)
                        {
                            passenger.employerInfoId = Convert.ToInt32(data["employerInfoId"]);
                            passenger.employerInfo = new VisaEmployerInfo(passenger.employerInfoId);
                        }
                        else
                        {
                            passenger.employerInfo = new VisaEmployerInfo();
                            passenger.employerInfo.EmployerInfoId = 0;
                        }
                        if (data["passportExpiryDate"] != DBNull.Value)
                        {
                            passenger.passportExpiryDate = Convert.ToDateTime(data["passportExpiryDate"]);

                        }

                        if (data["visaProcessingLastDate"] != DBNull.Value)
                        {
                            passenger.visaProcessingLastDate = Convert.ToDateTime(data["visaProcessingLastDate"]);
                        }
                        if (data["dob"] != DBNull.Value)
                        {
                            passenger.dob = Convert.ToDateTime(data["dob"]);
                        }

                        passenger.emailId = "";
                        if (data["visaDocumentPath"] != DBNull.Value)
                        {
                            passenger.visaDocumentPath = Convert.ToString(data["visaDocumentPath"]);
                        }
                        passenger.createdOn = Convert.ToDateTime(data["createdOn"]);
                        passengerList.Add(passenger);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            return passengerList;
        }
        #endregion

    }
}
