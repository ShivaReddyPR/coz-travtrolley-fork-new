﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;

namespace Visa
{
    public class VisaQueue
    {
        /// <summary>
        /// pnr of the booking
        /// </summary>
        private string pnr = string.Empty;
        /// <summary>
        /// Ticket number to be searched
        /// </summary>
        private string visaNumber = string.Empty;
        /// <summary>
        /// Agency for which bookings have to be searched
        /// </summary>
        private string agencyId = string.Empty;
        /// <summary>
        /// Passenger name to be searched
        /// </summary>
        private string paxName = string.Empty;
        /// <summary>
        /// Page Number to be shown
        /// </summary>
        private int pageNumber;
        /// <summary>
        /// Maximum nuber of data on each page
        /// </summary>
        private int numberOfDataOnEachPage;

        /// <summary>
        /// Unique identity number of a visaType
        /// </summary>
        private int visaTypeId;


        /// <summary>
        /// Unique identity number of a visaType
        /// </summary>
        private int visaId;

        /// <summary>
        /// Unique identity number of a passenger
        /// </summary>
        private int paxId;

        /// <summary>
        /// Unique identity number of a visaType
        /// </summary>
        private int visaStatusId;

        private string visaDocumentPath;

        private DateTime visaIssue;
        private DateTime visaExpiry;

        private string visaRemarks;
        //private int visaQueueHistoryId;
        decimal visaDepositFee;
        decimal visaRefundableFee;
        int updatedBy;
        DateTime updatedOn;

        private string userEmail;
        private string countryCode;

        // added by rao 26.12.2016
        decimal visaMarkup;
        decimal visaInsuranceFee;
        decimal visaFee;

        DateTime fromDate;
        DateTime toDate;
        string bookedDate;
        int userId;

        /// <summary>
        /// Field to store invoice date
        /// </summary>
        private DateTime invoiceDate;
        public DateTime InvoiceDate
        {
            set { invoiceDate = value; }
            get { return invoiceDate; }
        }

        /// <summary>
        /// Field to store Invoice Number
        /// </summary>
        private int invoiceNumber;
        public int InvoiceNumber
        {
            set { invoiceNumber = value; }
            get { return invoiceNumber; }
        }


        /// <summary>
        /// total number of rows found for a given search request
        /// </summary>
        private int queueCount;
        /// <summary>
        /// to find if agent or admin
        /// </summary>
        private bool isAgent = false;
        public int QueueCount
        {
            get
            {
                return queueCount;
            }
        }
        public string Pnr
        {
            set
            {
                pnr = value;
            }
        }
        public string VisaNumber
        {
            set
            {
                visaNumber = value;
            }
        }
        public string AgencyId
        {
            set
            {
                agencyId = value;
            }
        }
        public string PaxName
        {
            set
            {
                paxName = value;
            }
        }
        public int PageNumber
        {
            get
            {
                return pageNumber;
            }
            set
            {
                pageNumber = value;
            }
        }
        public int NumberOfDataOnEachPage
        {
            set
            {
                numberOfDataOnEachPage = value;
            }
        }
        public bool IsAgent
        {
            set
            {
                isAgent = value;
            }
        }

        /// <summary>
        /// Gets or sets the visaTypeId
        /// </summary>
        public int VisaTypeId
        {
            get
            {
                return visaTypeId;
            }
            set
            {
                visaTypeId = value;
            }
        }


        /// <summary>
        /// Gets or sets the visaTypeId
        /// </summary>
        public int VisaId
        {
            get
            {
                return visaId;
            }
            set
            {
                visaId = value;
            }
        }


        /// <summary>
        /// Gets or sets the paxId
        /// </summary>
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }



        /// <summary>
        /// Gets or sets the visaStatusId
        /// </summary>
        public int VisaStatusId
        {
            get
            {
                return visaStatusId;
            }
            set
            {
                visaStatusId = value;
            }
        }
        public string VisaDocumentPath
        {
            get
            {
                return visaDocumentPath;
            }
            set
            {
                visaDocumentPath = value;
            }
        }

        public DateTime VisaIssue
        {
            get
            {
                return visaIssue;
            }
            set
            {
                visaIssue = value;
            }
        }
        public DateTime VisaExpiry
        {
            get
            {
                return visaExpiry;
            }
            set
            {
                visaExpiry = value;
            }
        }

        public String VisaRemarks
        {
            get
            {
                return visaRemarks;
            }
            set
            {
                visaRemarks = value;
            }
        }

        public String UserEmail
        {
            get
            {
                return userEmail;
            }
            set
            {
                userEmail = value;
            }
        }

        public decimal VisaDepositFee
        {
            get { return visaDepositFee; }
            set { visaDepositFee = value; }
        }

        public decimal VisaRefundableFee
        {
            get { return visaRefundableFee; }
            set { visaRefundableFee = value; }
        }

        public Int32 UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public DateTime UpdatedOn
        {
            get { return updatedOn; }
            set { updatedOn = value; }
        }
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }

        // added by rao
        public decimal VisaMarkup
        {
            get { return visaMarkup; }
            set { visaMarkup = value; }
        }
        public decimal VisaInsuranceFee
        {
            get { return visaInsuranceFee; }
            set { visaInsuranceFee = value; }
        }

        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        public string BookedDate
        {
            get { return bookedDate; }
            set { bookedDate = value; }
        }

        public decimal VisaFee
        {
            get { return visaFee; }
            set { visaFee = value; }
        }

        public int UserId
        {
            get
            {
                return userId;
            }

            set
            {
                userId = value;
            }
        }

        DataTable dt;
        /// <summary>
        /// Gets the data according to the filter criteria
        /// </summary>
        /// <returns></returns>
        public List<DataRow[]> GetData(int FilterDuplicateRecords)
        {
            dt = new DataTable();
            List<DataRow[]> listOfVisaBookings = new List<DataRow[]>();

            //bool dbHit = false;

            //if (visaNumber.Trim().Length > 0)
            //{
            //    dt = SearchByVisaNumber(); 
            //}
            //else if (visaTypeId > 0)
            //{
            //    //dt = SearchByPnr();
            //}
            //else
            //{
            //    if (paxName.Trim().Length > 0)
            //    {
            //        dt = SearchByPaxName(); 
            //        dbHit = true;
            //    }
            //    if (agencyId.Trim().Length > 0)
            //    {
            //        if (!dbHit)
            //        {
            //            dt = SearchByAgencyId();
            //            dbHit = true;
            //        }
            //        else
            //        {
            //            string[] splittedAgencyId = agencyId.Split(',');
            //            DataRow[] tempDataRow = new DataRow[dt.Rows.Count];
            //            int count = 0;
            //            foreach (string singleAgencyId in splittedAgencyId)
            //            {
            //                if (dt.Select("agencyId = '" + singleAgencyId + "'") != null && dt.Select("agencyId = '" + singleAgencyId + "'").Length > 0)
            //                {
            //                    dt.Select("agencyId = '" + singleAgencyId + "'").CopyTo(tempDataRow, count);
            //                    count += dt.Select("agencyId = '" + singleAgencyId + "'").Length;
            //                }
            //            }
            //            GetDataTableFromDataRow(tempDataRow); 
            //        }
            //    }
            //    //If no criteria is provided then search to get latest 100 tickets
            //    if (!dbHit)
            //    {
            //        dt = SearchWithoutFilters();
            //        dbHit = true;
            //    }
            //}

            dt = VisaQueueSearch(FilterDuplicateRecords);

            //If data table contains result then convert the result into list of data row array
            if (dt.Columns.Count > 1 && dt.Rows.Count > 0)
            {
                listOfVisaBookings = GetFinalResult(dt);
            }
            return listOfVisaBookings;
        }




        /// <summary>
        /// This function searches when no filter criteria is provided
        /// </summary>
        /// <returns></returns>
        /// 
        string whereString = string.Empty;
        private DataTable VisaQueueSearch(int FilterDuplicateRecords)
        {

            SqlParameter[] paramList = new SqlParameter[9];



            if (visaNumber.Length > 0)
            {
                paramList[0] = new SqlParameter("@visaNumber", visaNumber);
            }
            else
            {
                paramList[0] = new SqlParameter("@visaNumber", DBNull.Value);

            }
            if (agencyId.Length > 0)
            {
                paramList[1] = new SqlParameter("@agencyId", agencyId);
            }
            else
            {
                paramList[1] = new SqlParameter("@agencyId", DBNull.Value);

            }
            if (visaStatusId > 0)
            {
                paramList[2] = new SqlParameter("@visaStatus", visaStatusId);
            }
            else
            {
                paramList[2] = new SqlParameter("@visaStatus", DBNull.Value);

            }
            if (visaTypeId > 0)
            {
                paramList[3] = new SqlParameter("@visaType", visaTypeId);
            }
            else
            {
                paramList[3] = new SqlParameter("@visaType", DBNull.Value);

            }
            if (paxName.Length > 0)
            {
                paramList[4] = new SqlParameter("@paxName", paxName);
            }
            else
            {
                paramList[4] = new SqlParameter("@paxName", DBNull.Value);

            }

            if (countryCode == null || CountryCode == "")
            {
                paramList[5] = new SqlParameter("@countryCode", DBNull.Value);
            }
            else
            {
                paramList[5] = new SqlParameter("@countryCode", countryCode);
            }

            //paramList[6] = new SqlParameter("@fromDate", fromDate);
            //paramList[7] = new SqlParameter("@toDate", toDate);

            //Parameter which determines whether to load the duplicate records during the AdminPageLoad Event for the first time.
            paramList[6] = new SqlParameter("@filterDuplicateRecords", FilterDuplicateRecords);





            if (!fromDate.Equals(DateTime.MinValue) && !ToDate.Equals(DateTime.MaxValue))
            {
                whereString += " and v.createdOn >= '" + fromDate + "'" + " AND v.createdOn <= '" + ToDate + "'";
            }
            paramList[7] = new SqlParameter("@bookedDate", whereString);

            if(userId>0)
            {
                paramList[8] = new SqlParameter("@userId", userId);
            }
            else
            {
                paramList[8] = new SqlParameter("@userId", DBNull.Value);
            }
            
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetVisaQueueSearch, paramList);

            return tempDataTable;
        }


        /// <summary>
        /// This function searches when no filter criteria is provided
        /// </summary>
        /// <returns></returns>
        private DataTable SearchWithoutFilters()
        {

            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminTicketQueue, new SqlParameter[0]);

            return tempDataTable;
        }


        /// <summary>
        /// This function searches the booking by visa number 
        /// </summary>
        /// <returns></returns>
        public DataTable SearchByVisaNumber()
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@visaId", visaId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminVisaQueueByVisaNumber, paramList);
            return tempDataTable;
        }


        /// <summary>
        /// This function searches the booking by visa number 
        /// </summary>
        /// <returns></returns>
        public DataTable getVisaUploadedDocuement()
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetVisaUploadedDocument, paramList);
            return tempDataTable;
        }
        public static DataTable GetUploadedDocuement(int paxId, int documentId)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@paxId", paxId);
            paramList[1] = new SqlParameter("@documentId", documentId);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetVisaUploadedDocumentById, paramList);
            return tempDataTable;
        }



        /// <summary>
        /// This function searches the booking by agencyid
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByAgencyId()
        {
            DataTable tempDataTable = new DataTable();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminVisaQueueByAgencyId, paramList);
            return tempDataTable;
        }



        public int UpdateVisaStatus()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@VisaStatusId", visaStatusId);
            paramList[2] = new SqlParameter("@remarks", visaRemarks);
            status = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaStatus", paramList);
            return status;

        }

        public int UpdateVisaStatusAndPath()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[8];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@paxId", paxId);
            paramList[2] = new SqlParameter("@VisaStatusId", visaStatusId);
            paramList[3] = new SqlParameter("@visDocumentPath", visaDocumentPath);
            paramList[4] = new SqlParameter("@visaNumber", visaNumber);
            paramList[5] = new SqlParameter("@visaIssue", visaIssue);
            paramList[6] = new SqlParameter("@visaExpiry", visaExpiry);
            paramList[7] = new SqlParameter("@remarks", visaRemarks);
            status = DBGateway.ExecuteNonQuerySP(SPNames.UpdateVisaStatusAndPath, paramList);
            return status;

        }

        /// <summary>
        /// Update Visa Queue History
        /// </summary>
        /// <returns></returns>
        public int UpdateVisaQueueHistory()
        {
            int status = 0;
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@VisaStatusId", visaStatusId);
            paramList[2] = new SqlParameter("@visaDepositFee", visaDepositFee);
            paramList[3] = new SqlParameter("@visaRefundableFee", visaRefundableFee);
            paramList[4] = new SqlParameter("@updatedBy", updatedBy);
            paramList[5] = new SqlParameter("@remarks", visaRemarks);
            status = DBGateway.ExecuteNonQuerySP(SPNames.UpdateVisaQueueHistory, paramList);
            return status;
        }

        /// <summary>
        /// This function searches the booking by pax name
        /// </summary>
        /// <returns></returns>
        private DataTable SearchByPaxName()
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxname", paxName);
            DataTable tempDataTable = DBGateway.FillDataTableSP(SPNames.GetResultForAdminVisaQueueByPaxName, paramList);
            return tempDataTable;
        }

        /// <summary>
        /// This function converts data row array to data table
        /// </summary>
        /// <param name="dataRowArray"></param>
        private void GetDataTableFromDataRow(DataRow[] dataRowArray)
        {
            DataTable tempDataTable = new DataTable();
            tempDataTable = dt.Clone();
            foreach (DataRow dataRow in dataRowArray)
            {
                tempDataTable.ImportRow(dataRow);
            }
            dt.Clear();
            dt = tempDataTable;
        }



        /// <summary>
        ///This function formats the result into list of data row array
        /// </summary>
        /// <param name="tempDataTable"></param>
        /// <returns></returns>
        private List<DataRow[]> GetFinalResult(DataTable tempDataTable)
        {
            int startIndex = (pageNumber - 1) * numberOfDataOnEachPage;
            List<DataRow[]> listOfVisaBookings = new List<DataRow[]>();
            List<string> distinctId = SelectDistinctId(tempDataTable);
            queueCount = distinctId.Count;
            if (queueCount < (pageNumber - 1) * numberOfDataOnEachPage)
            {
                pageNumber = 1;
                startIndex = 0;
            }
            if ((startIndex + (numberOfDataOnEachPage - 1)) >= queueCount)
            {
                numberOfDataOnEachPage = queueCount - (startIndex);
            }
            List<string> distinctIdInRange = distinctId.GetRange(startIndex, numberOfDataOnEachPage);
            foreach (string id in distinctIdInRange)
            {
                DataRow[] arrayOfDataRowForId = null;
                string[] idFilter = id.Split('-');
                if (idFilter[0] == "2")
                {
                    arrayOfDataRowForId = tempDataTable.Select("visaId ='" + idFilter[1] + "'");
                }
                else if (idFilter[0] == "1")
                {
                    arrayOfDataRowForId = tempDataTable.Select("pendingId ='" + idFilter[1] + "'");
                }
                listOfVisaBookings.Add(arrayOfDataRowForId);
            }
            return listOfVisaBookings;
        }



        /// <summary>
        /// Gets distinct pnr's in a given table
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private List<string> SelectDistinctId(DataTable tempDataTable)
        {
            List<string> distinctId = new List<string>();
            foreach (DataRow dr in tempDataTable.Rows)
            {
                if (isAgent)
                {
                    if (dr["pendingId"] != DBNull.Value)
                    {
                        if (!distinctId.Contains("1-" + Convert.ToString(dr["pendingId"])))
                        {
                            distinctId.Add("1-" + Convert.ToString(dr["pendingId"]));
                        }
                    }
                    else
                    {
                        if (!distinctId.Contains("2-" + Convert.ToString(dr["visaId"])))
                        {
                            distinctId.Add("2-" + Convert.ToString(dr["visaId"]));
                        }
                    }
                }
                else
                {
                    if (!distinctId.Contains("2-" + Convert.ToString(dr["visaId"])))
                    {
                        distinctId.Add("2-" + Convert.ToString(dr["visaId"]));
                    }
                }
            }
            return distinctId;
        }



        /// <summary>
        /// Retrieve History of a VisaQueue
        /// </summary>
        /// <param name="bookingId">Visa Id</param>
        /// <returns>Liat of VisaQueueHistory Object</returns>
        public static List<VisaQueue> GetVisaQueueHistory(int visaID)
        {
            if (visaID <= 0)
            {
                throw new ArgumentException("Booking Id should be positive integer", "BookingId");
            }
            List<VisaQueue> listBookingHistory = new List<VisaQueue>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@visaId", visaID);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetListVisaQueueHistory, paramList, connection);
            while (data.Read())
            {
                VisaQueue objVisaQueue = new VisaQueue();
                objVisaQueue.visaId = Convert.ToInt32(data["visaId"]);
                if (data["remarks"] != DBNull.Value)
                {
                    objVisaQueue.visaRemarks = data["remarks"].ToString();
                }
                else
                {
                    objVisaQueue.visaRemarks = string.Empty;
                }
                objVisaQueue.visaStatusId = Convert.ToInt32(data["statusId"].ToString());
                // objVisaQueue.visaDepositFee = Convert.ToDecimal(data["visaDepositFee"]);
                //   objVisaQueue.visaRefundableFee = Convert.ToDecimal(data["visaRefundableFee"]);
                objVisaQueue.updatedBy = Convert.ToInt32(data["updatedBy"]);
                objVisaQueue.updatedOn = Convert.ToDateTime(data["updatedOn"]);
                listBookingHistory.Add(objVisaQueue);
            }
            data.Close();
            connection.Close();
            return listBookingHistory;
        }
        //
        public static DataTable GetVisaReportItems(DateTime fromDate, DateTime toDate, int agentId, int visatypeId, int VisaStatusId, int UserId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (agentId > 0)
                {
                    paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                }
                else
                {
                    paramList[2] = new SqlParameter("@P_AGENT_ID", DBNull.Value);
                }
                if (visatypeId > 0)
                {
                    paramList[3] = new SqlParameter("@P_VISATYPE_ID", visatypeId);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_VISATYPE_ID", DBNull.Value);
                }
                if (VisaStatusId >0)
                    paramList[4] = new SqlParameter("@P_VISASTATUS_ID", VisaStatusId);
                else
                    paramList[4] = new SqlParameter("@P_VISASTATUS_ID", DBNull.Value);
                if (UserId > 0)
                    paramList[5] = new SqlParameter("@P_USER_ID", UserId);
                else
                    paramList[5] = new SqlParameter("@P_USER_ID", DBNull.Value);

                DataTable dtResult = DBGateway.FillDataTableSP("USP_GET_VISA_REPORT_LIST_ITEMS", paramList);
                return dtResult;
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetVisaTypeItems(string status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_STATUS", status);

                DataTable dtResult = DBGateway.FillDataTableSP("USP_GET_VISA_TYPE_LIST_ITEMS", paramList);
                return dtResult;
            }
            catch
            {
                throw;
            }
        }
    }
}
