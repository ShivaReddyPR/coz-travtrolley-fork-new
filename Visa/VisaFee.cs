using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;


namespace Visa
{
    public class VisaFee
    {
        #region


        int visaFeeId;
        int visaTypeId;
        string destinationCityCode;
        decimal cost;
        decimal markup;
        decimal visaDepositCharge;
        decimal insuranceCharge;
        decimal refundable;
        bool isActive;
        string message;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        int _agentId;
        string visaTypeName;
        string agency;
        string nationalityCode;
        #endregion

        #region
        public int VisaFeeId
        {
            get
            {
                return visaFeeId;
            }
            set
            {
                visaFeeId = value;
            }
        }
        public int VisaTypeId
        {
            get
            {
                return visaTypeId;
            }
            set
            {
                visaTypeId = value;
            }
        }
        public string DestinationCityCode
        {
            get
            {
                return destinationCityCode;
            }
            set
            {
                destinationCityCode = value;
            }
        }
        public decimal Cost
        {
            get
            {
                return cost;
            }
            set
            {
                cost = value;
            }
        }
        public decimal Markup
        {
            get
            {
                return markup;
            }
            set
            {
                markup = value;
            }
        }
        public decimal VisaDepositCharge
        {
            get
            {
                return visaDepositCharge;
            }
            set
            {
                visaDepositCharge = value;
            }
        }
        public decimal InsuranceCharge
        {
            get
            {
                return insuranceCharge;
            }
            set
            {
                insuranceCharge = value;
            }
        }
        public decimal Refundable
        {
            get
            {
                return refundable;
            }
            set
            {
                refundable = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public string VisaTypeName
        {
            get
            {
                return visaTypeName;
            }
            set
            {
                visaTypeName = value;
            }
        }
        public string Agency
        {
            get
            {
                return agency;
            }
            set
            {
                agency = value;
            }
        }

        public string NationalityCode
        {
            get { return nationalityCode; }
            set { nationalityCode = value; }
        }
        #endregion

        #region  Method

        public VisaFee()
        {
        }
        public VisaFee(int visaFeeId)
        {
            Load(visaFeeId);
        }
        public VisaFee(int visaTypeId, string cityCode)
        {
            Load(visaTypeId, cityCode);
        }
        private void Validate()
        {

        }


        public int Save()
        {


            int rowsAffected = 0;
            int errMessage = 0;
            SqlParameter[] paramList = new SqlParameter[14];
            paramList[0] = new SqlParameter("@visaFeeId", visaFeeId);
            paramList[1] = new SqlParameter("@visaTypeId", visaTypeId);
            if (!string.IsNullOrEmpty(destinationCityCode))
            {
                paramList[2] = new SqlParameter("@destinationCityCode", destinationCityCode);
            }
            else
            {
                paramList[2] = new SqlParameter("@destinationCityCode", DBNull.Value);
            }
            paramList[3] = new SqlParameter("@cost", cost);
            paramList[4] = new SqlParameter("@markup", markup);
            paramList[5] = new SqlParameter("@visaDepositCharge", visaDepositCharge);
            paramList[6] = new SqlParameter("@insuranceCharge", insuranceCharge);
            paramList[7] = new SqlParameter("@refundable", refundable);
            paramList[8] = new SqlParameter("@isActive", isActive);
            paramList[9] = new SqlParameter("@message", message);
            paramList[10] = new SqlParameter("@createdBy", createdBy);
            paramList[11] = new SqlParameter("@errMessage", errMessage);

            paramList[0].Direction = ParameterDirection.Output;
            paramList[11].Direction = ParameterDirection.Output;
            paramList[12] = new SqlParameter("@agencyId", _agentId);

            if (!string.IsNullOrEmpty(nationalityCode))
            {
                paramList[13] = new SqlParameter("@nationalityCode", nationalityCode);
            }
            else
            {
                paramList[13] = new SqlParameter("@nationalityCode", DBNull.Value);
            }

            rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddVisaFee", paramList);
            visaFeeId = Convert.ToInt32(paramList[0].Value);
            errMessage = Convert.ToInt32(paramList[11].Value);
            return errMessage;
        }

        public int Update()
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[12];
            paramList[0] = new SqlParameter("@visaFeeId", visaFeeId);
            paramList[1] = new SqlParameter("@visaTypeId", visaTypeId);
            if (!string.IsNullOrEmpty(destinationCityCode))
            {
                paramList[2] = new SqlParameter("@destinationCityCode", destinationCityCode);
            }
            else
            {
                paramList[2] = new SqlParameter("@destinationCityCode", DBNull.Value);
            }
            paramList[3] = new SqlParameter("@cost", cost);
            paramList[4] = new SqlParameter("@markup", markup);
            paramList[5] = new SqlParameter("@visaDepositCharge", visaDepositCharge);
            paramList[6] = new SqlParameter("@insuranceCharge", insuranceCharge);
            paramList[7] = new SqlParameter("@refundable", refundable);
            paramList[8] = new SqlParameter("@isActive", isActive);
            paramList[9] = new SqlParameter("@message", message);
            paramList[10] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            if (!string.IsNullOrEmpty(nationalityCode))
            {
                paramList[11] = new SqlParameter("@nationalityCode", nationalityCode);
            }
            else
            {
                paramList[11] = new SqlParameter("@nationalityCode", DBNull.Value);
            }
            rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaFee", paramList);
            return rowsAffected;
        }
        private void Read(DataRow dataReader)
        {
            //if (dataReader.Read())
            {
                if (dataReader["agencyId"] != DBNull.Value)
                {
                    _agentId = Convert.ToInt32(dataReader["agencyId"]);
                }
                if (dataReader["visaFeeId"] != DBNull.Value)
                {
                    visaFeeId = Convert.ToInt32(dataReader["visaFeeId"]);
                }
                if (dataReader["visaTypeId"] != DBNull.Value)
                {
                    visaTypeId = Convert.ToInt32(dataReader["visaTypeId"]);
                }
                if (dataReader["destinationCityCode"] != DBNull.Value)
                {
                    destinationCityCode = Convert.ToString(dataReader["destinationCityCode"]);
                }
                if (dataReader["cost"] != DBNull.Value)
                {
                    cost = Convert.ToDecimal(dataReader["cost"]);
                }
                if (dataReader["markup"] != DBNull.Value)
                {
                    markup = Convert.ToDecimal(dataReader["markup"]);
                }
                if (dataReader["visaDepositCharge"] != DBNull.Value)
                {
                    visaDepositCharge = Convert.ToDecimal(dataReader["visaDepositCharge"]);
                }
                if (dataReader["insuranceCharge"] != DBNull.Value)
                {
                    insuranceCharge = Convert.ToDecimal(dataReader["insuranceCharge"]);
                }
                if (dataReader["refundable"] != DBNull.Value)
                {
                    refundable = Convert.ToDecimal(dataReader["refundable"]);
                }
                if (dataReader["isActive"] != DBNull.Value)
                {
                    isActive = Convert.ToBoolean(dataReader["isActive"]);
                }
                if (dataReader["createdBy"] != DBNull.Value)
                {
                    createdBy = Convert.ToInt32(dataReader["createdBy"]);
                }
                if (dataReader["createdOn"] != DBNull.Value)
                {
                    createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                }
                if (dataReader["lastModifiedBy"] != DBNull.Value)
                {
                    lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                }
                if (dataReader["lastModifiedOn"] != DBNull.Value)
                {
                    lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                }
                if (dataReader["nationalityCode"] != DBNull.Value)
                {
                    nationalityCode = Convert.ToString(dataReader["nationalityCode"]);
                }
                if (dataReader["message"] != DBNull.Value)
                {
                    message = Convert.ToString(dataReader["message"]);
                }
            }
        }


        public void Load(int visaFeeId)
        {

            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@visaFeeId", visaFeeId);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisaFeeById", paramList, connection);
                using (DataTable dtFee = DBGateway.FillDataTableSP("usp_ReadVisaFeeById", paramList))
                {
                    if (dtFee != null && dtFee.Rows.Count > 0)
                    {
                        Read(dtFee.Rows[0]);
                        //dataReader.Close();
                    }
                }
            }

        }
        public void Load(int visaTypeId, string cityCode)
        {

            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@visaTypeId", visaTypeId);
            paramList[1] = new SqlParameter("@destinationCityCode", cityCode);
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetVisaFeeByCityCodeAndVisaType", paramList, connection);
                using (DataTable dtFee = DBGateway.FillDataTableSP("usp_GetVisaFeeByCityCodeAndVisaType", paramList))
                {
                    if (dtFee != null && dtFee.Rows.Count > 0)
                    {
                        Read(dtFee.Rows[0]);
                        //dataReader.Close();
                    }
                }
            }
        }


        public static List<VisaFee> GetVisaFeeList(string countryCode, int agentId, int visaType, string nationality)
        {

            List<VisaFee> visaFeeList = new List<VisaFee>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@countryCode", countryCode);
            if (agentId > 0) paramList[1] = new SqlParameter("@agentId", agentId);
            else paramList[1] = new SqlParameter("@agentId", DBNull.Value);
            paramList[2] = new SqlParameter("@nationality", nationality);
            if (visaType > 0) paramList[3] = new SqlParameter("@visaType", visaType);
            else paramList[3] = new SqlParameter("@visaType", DBNull.Value);

            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaFee", paramList, connection);
            while (data.Read())
            {
                VisaFee visaFee = new VisaFee();
                if (data["visaFeeId"] != DBNull.Value)
                {
                    visaFee.visaFeeId = Convert.ToInt32(data["visaFeeId"]);
                }
                if (data["visaTypeId"] != DBNull.Value)
                {
                    visaFee.visaTypeId = Convert.ToInt32(data["visaTypeId"]);
                }
                if (data["visaType"] != DBNull.Value)
                {
                    visaFee.visaTypeName = Convert.ToString(data["visaType"]);
                }
                if (data["agent_Name"] != DBNull.Value)
                {
                    visaFee.agency = data["agent_Name"] != DBNull.Value ? Convert.ToString(data["agent_Name"]) : string.Empty;
                }
                if (data["destinationCityCode"] != DBNull.Value)
                {
                    visaFee.destinationCityCode = Convert.ToString(data["destinationCityCode"]);
                }
                if (data["visaDepositCharge"] != DBNull.Value)
                {
                    visaFee.visaDepositCharge = Convert.ToDecimal(data["visaDepositCharge"]);
                }
                if (data["markup"] != DBNull.Value)
                {
                    visaFee.markup = Convert.ToDecimal(data["markup"]);
                }
                if (data["insuranceCharge"] != DBNull.Value)
                {
                    visaFee.insuranceCharge = Convert.ToDecimal(data["insuranceCharge"]);
                }
                if (data["refundable"] != DBNull.Value)
                {
                    visaFee.refundable = Convert.ToDecimal(data["refundable"]);
                }
                if (data["cost"] != DBNull.Value)
                {
                    visaFee.cost = Convert.ToDecimal(data["cost"]);
                }
                if (data["message"] != DBNull.Value)
                {
                    visaFee.message = Convert.ToString(data["message"]);
                }
                if (data["isActive"] == DBNull.Value)
                {
                    visaFee.isActive = true;
                }
                else
                {
                    visaFee.isActive = Convert.ToBoolean(data["isActive"]);
                }
                if (data["nationality"] != DBNull.Value)
                {
                    visaFee.nationalityCode = Convert.ToString(data["nationality"]);
                }

                visaFeeList.Add(visaFee);

            }
            data.Close();
            connection.Close();


            return visaFeeList;
        }

        /// <summary>
        /// This function updates the visa price  details in Price Table
        /// </summary>
        /// <returns></returns>
        public static bool UpdateVisaFee(int visaId, decimal refundable, decimal visaDepositCharge, decimal visaFee, decimal markup, decimal insuranceFee, decimal netFee, decimal inputVat, decimal outputVat)
        {
            SqlParameter[] paramList = new SqlParameter[9];
            paramList[0] = new SqlParameter("@visaId", visaId);
            paramList[1] = new SqlParameter("@otherCharges", refundable);
            paramList[2] = new SqlParameter("@transactionFee", visaDepositCharge);
            paramList[3] = new SqlParameter("@visaFee", visaFee);
            paramList[4] = new SqlParameter("@markupFee", markup);
            paramList[5] = new SqlParameter("@insuranceFee", insuranceFee);
            paramList[6] = new SqlParameter("@netFee", netFee);
            paramList[7] = new SqlParameter("@inputVat", inputVat);
            paramList[8] = new SqlParameter("@outputVat", outputVat);
            int rows_affected = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaPrice", paramList);
            if (rows_affected > 0)
                return true;
            return false;
        }

        public static void ChangeStatus(int visaFeeId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@visaFeeId", visaFeeId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaFeeStatus", paramList);


        }

        #endregion
    }
}
