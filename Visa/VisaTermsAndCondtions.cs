﻿using System;
using System.Collections.Generic;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace Visa
{
    public class VisaTermsAndCondition
    {
        #region Variable

        /// <summary>
        /// Name of terms
        /// </summary>
        private int termsId;
        /// <summary>
        /// Name of terms
        /// </summary>
        private string description;
        /// <summary>
        /// description
        /// </summary>
        /// 

        private string countryCode;


        private bool isActive;
        /// <summary>
        /// Date when the record was created on
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the member record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;

        private string agency;
        private int agentId;



        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets terms Id
        /// </summary>
        public int TermsId
        {
            get
            {
                return termsId;
            }
            set
            {
                termsId = value;
            }
        }

        /// <summary>
        /// Gets or sets terms name
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        /// <summary>
        /// Gets or sets terms code
        /// </summary>
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }


        /// <summary>
        /// Gets or sets the status of member ( true for active member )
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }


        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public string Agency
        {
            get
            {
                return agency;
            }
            set
            {
                agency = value;
            }
        }

        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }
        #endregion


        #region Method

        /// <summary>
        /// Constructor : creates an empty instance of visaType
        /// </summary>
        public VisaTermsAndCondition()
        {

        }

        /// <summary>
        /// Constructor : creates an instance of VisaType with member detail corresponding to memberId given memberId
        /// </summary>
        /// <param name="VisaTypeId">Visa Type id of the VisaType to be instanciated</param>
        public VisaTermsAndCondition(int termsId)
        {
            Load(termsId);
        }


        /// <summary>
        /// Loads the information about member identified by memberId
        /// </summary>
        /// <param name="memberId">Member id of the member to be loaded</param>
        public void Load(int termsId)
        {

            if (termsId <= 0)
            {
                throw new ArgumentException("termsId must have a positive integer value", "VisaTypeId");
            }
           // SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@termsId", termsId);
            //SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_ReadVisaTermsAndConditionById", paramList, connection);
            using (DataTable dtTerms = DBGateway.FillDataTableSP("usp_ReadVisaTermsAndConditionById", paramList))
            {
                if (dtTerms != null && dtTerms.Rows.Count > 0)
                {
                    //if (dataReader.Read())
                    {
                        Read(dtTerms.Rows[0]);
                        //dataReader.Close();
                        //connection.Close();
                    }
                }
                else
                {
                    //dataReader.Close();
                    //connection.Close();
                    throw new ArgumentException("terms does not exist : termsId = " + termsId, "termsId");
                }
            }
        }

        private void Read(DataRow dataReader)
        {
            termsId = Convert.ToInt32(dataReader["termsId"]);
            description = Convert.ToString(dataReader["description"]);
            countryCode = Convert.ToString(dataReader["countryCode"]);
            isActive = Convert.ToBoolean(dataReader["isActive"]);
            createdOn = Convert.ToDateTime(dataReader["createdOn"]);
            createdBy = Convert.ToInt32(dataReader["createdBy"]);
            lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
            lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
            agentId = Convert.ToInt32(dataReader["agencyId"]);
        }

       public static List<VisaTermsAndCondition> GetTermsAndConditionList(string countryCode,int agentId)
        {

            List<VisaTermsAndCondition> visaTermsList = new List<VisaTermsAndCondition>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];

            paramList[0] = new SqlParameter("@countryCode", countryCode);
            if (agentId > 0) paramList[1] = new SqlParameter("@agencyId", agentId);
            else paramList[1] = new SqlParameter("@agencyId", DBNull.Value);
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetVisaTermsAndCondition", paramList, connection);
            while (data.Read())
            {
                VisaTermsAndCondition terms = new VisaTermsAndCondition();
                terms.termsId = (int)data["termsId"];
                terms.countryCode = (string)data["countryCode"];
                terms.description = (string)data["description"];
                terms.agency = data["agent_Name"] != DBNull.Value ? (String)data["agent_Name"] : string.Empty; 
                if (data["isActive"] == DBNull.Value)
                {
                    terms.isActive = true;
                }
                else
                {
                    terms.isActive = (bool)data["isActive"];
                }

                visaTermsList.Add(terms);

            }
            data.Close();
            connection.Close();


            return visaTermsList;
        }


        public int Save()
        {
            int status = 0;

            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@description", description);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[4] = new SqlParameter("@agencyId", agentId);

            // if true call update visa terms procedure else create new Visa terms.
            if (termsId > 0)
            {
                paramList[2] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramList[3] = new SqlParameter("@termsId", termsId);
                status = DBGateway.ExecuteNonQuerySP("usp_UpdateVisaTermsAndCondition", paramList);

            }
            else
            {
                paramList[2] = new SqlParameter("@createdBy", createdBy);
                paramList[3] = new SqlParameter("@isActive", isActive);
                status = DBGateway.ExecuteNonQuerySP("usp_AddVisaTermsAndCondition", paramList);


            }

            return status;

        }


        public static void ChangeStatus(int termsId, bool isActive)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@termsId", termsId);
            paramList[1] = new SqlParameter("@isActive", isActive);
            DBGateway.ExecuteNonQuerySP("usp_ChangeVisaTermsAndConditionStatus", paramList);


        }


        #endregion


    }
}
