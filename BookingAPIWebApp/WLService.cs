using System.Web.Services;


/// <summary>
/// Summary description for WLService
/// </summary>

[WebService(Namespace = "http://192.168.0.170/TT/BookingAPI",
            Description = "Search and Book API",
            Name = "WLService")]



[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WLService : System.Web.Services.WebService
{
    public AuthenticationData Credential;

    public string error;

    public WLService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /// <summary>
    /// Gets the Ads for a particular module
    /// </summary>
    /// <param name="adModule">Module for which to get the Advertisement</param>
    /// <returns>A list of object of WLAdvertisement</returns>
    //[WebMethod]
    //[SoapHeader("Credential")]
    //public List<WLAdvertisement> GetAdsOfModule(string adModule)
    //{
    //    UserPreference preference = new UserPreference();
    //    string siteName = Credential.SiteName;
    //    int UserMasterId = 0;
    //    try
    //    {
    //        UserMasterId = preference.GetUserMasterIdBySiteName(siteName);
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.AddAuditBookingAPI(EventType.Login, Severity.Normal, 0, "GetAdsOfModule Failed: " + ex.Message, "");
    //    }
    //    UserMaster UserMaster = new UserMaster(UserMasterId);
    //    AgentMaster AgentMaster = new AgentMaster(UserMaster.AgentId);
    //    List<WLAdvertisement> WLadList = new List<WLAdvertisement>();
    //    List<Advertisement> adList = new List<Advertisement>();
    //    Advertisement ad = new Advertisement();
    //    AdModule adMod;
    //    try
    //    {
    //        adMod = (AdModule)Enum.Parse(typeof(AdModule), adModule);
    //    }
    //    catch (ArgumentException)
    //    {
    //        return WLadList;
    //    }
    //    //adList = ad.GetAdsOfModule(AgentMaster.ID, adMod);
    //    foreach (Advertisement advert in adList)
    //    {
    //        WLAdvertisement adv = new WLAdvertisement();
    //        adv = WLAdvertisement.ReadAdvertisement(advert, UserMaster.LoginName);
    //        WLadList.Add(adv);
    //    }
    //    return WLadList;
    //}
    //[WebMethod]
    //[SoapHeader("Credential")]
    //public WLAdvertisement Load(int advertisementId)
    //{
    //    UserPreference preference = new UserPreference();
    //    string siteName = Credential.SiteName;
    //    int UserMasterId = preference.GetUserMasterIdBySiteName(siteName);
    //    UserMaster UserMaster = new UserMaster(UserMasterId);
    //    Advertisement ad = new Advertisement();
    //    ad.Load(advertisementId);
    //    WLAdvertisement WLad = new WLAdvertisement();
    //    WLad = WLAdvertisement.ReadAdvertisement(ad, UserMaster.LoginName);
    //    return WLad;
    //}
    //[WebMethod]
    //[SoapHeader("Credential")]
    //public List<WSFaqs> GetFAQs()
    //{
    //    UserPreference preference = new UserPreference();
    //    string siteName = Credential.SiteName;
    //    int UserMasterId = preference.GetUserMasterIdBySiteName(siteName);
    //    AgentMaster AgentMaster = new AgentMaster(new UserMaster(UserMasterId).AgentId);
    //    List<WhiteLabelFaqs> faqList = new List<WhiteLabelFaqs>();
    //    //faqList = WhiteLabelFaqs.LoadRecentFAQs(AgentMaster.ID,ItemType.Flight);
    //    List<WSFaqs> wsFaqList = new List<WSFaqs>();
    //    foreach (WhiteLabelFaqs faq in faqList)
    //    {
    //        WSFaqs wsFaq = new WSFaqs();
    //        wsFaq = WSFaqs.ReadFaqs(faq);
    //        wsFaqList.Add(wsFaq);
    //    }
    //    return wsFaqList;
    //}
    //[WebMethod]
    //[SoapHeader("Credential")]
    //public List<WSFaqs> GetAllFAQs()
    //{
    //    UserPreference preference = new UserPreference();
    //    string siteName = Credential.SiteName;
    //    int UserMasterId = 0;
    //    try
    //    {
    //        UserMasterId = preference.GetUserMasterIdBySiteName(siteName);
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.AddAuditBookingAPI(EventType.Login, Severity.Normal, 0, "GetAllFAQs Failed: " + ex.Message, "");
    //    }
    //    AgentMaster AgentMaster = new AgentMaster(new UserMaster(UserMasterId).AgentId);
    //    List<WhiteLabelFaqs> faqList = new List<WhiteLabelFaqs>();
    //    //faqList = WhiteLabelFaqs.LoadAllFAQs(AgentMaster.AgentMasterId,ItemType.Flight);
    //    List<WSFaqs> wsFaqList = new List<WSFaqs>();
    //    foreach (WhiteLabelFaqs faq in faqList)
    //    {
    //        WSFaqs wsFaq = new WSFaqs();
    //        wsFaq = WSFaqs.ReadFaqs(faq);
    //        wsFaqList.Add(wsFaq);
    //    }
    //    return wsFaqList;
    //}
}

