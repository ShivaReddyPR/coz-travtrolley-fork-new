
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Diagnostics;
using CT.AccountingEngine;
using CT.BookingEngine;
using CT.Core;
using CT.BookingEngine.GDS;
using CT.TicketReceipt.DataAccessLayer;
using CT.MetaSearchEngine;
using CT.Configuration;
using System.Threading;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using CT.BookingEngine.WhiteLabel;
using System.Net;
using System.Configuration;
using CT.TicketReceipt.BusinessLayer;
using System.Security.Cryptography;
using System.Linq;

public class Texp : SoapException
{
    public Texp(string message) : base(message, SoapException.ServerFaultCode) { }
}

public class AuthenticationData : SoapHeader
{
    public string SiteName;
    public string AccountCode;
    public string UserName;
    public string Password;
}


[WebService(Namespace = "http://192.168.0.170/TT/BookingAPI",
            Description = "Search and Book API",
            Name = "BookingAPI")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class BookingAPI : System.Web.Services.WebService
{
    public AuthenticationData Credential;
    private static string statusDescription;
    private static string statusCode;
    public string error;
    const string LINK_AVAILABILITY = "LinkAvailability";
    const string AVAILABILITY_SOURCE = "AvailabilitySource";
    const string PROVIDER_CODE = "ProviderCode";
    const string SEGMENT_REF = "SegmentRef";
    const string POLLED_AVAILABILITY_OPTION = "PolledAvailabilityOption";
    private LoginInfo AgentLoginInfo;


    // for sabre authenticate token

    private  string sabreAuthenticateToken;

    public string SabreAuthenticateToken
    {
        get
        {
            return sabreAuthenticateToken;
        }

        set
        {
            sabreAuthenticateToken = value;
        }
    }

    public BookingAPI()
    {
        
    }
   

    private static UserMaster WSValidate(AuthenticationData credential)
    {
        UserMaster UserMaster = new UserMaster();
        BookingAPI bApi = new BookingAPI();        
        if (credential.SiteName != null && credential.SiteName != "")
        {            
            //string ipAddr = this.Context.Request.UserHostAddress;           
            try
            {                
                UserMaster = bApi.GetUserMasterBySiteName(credential.SiteName);
                //Audit.AddAuditBookingAPI(EventType.Login, Severity.Low, (int)(int)UserMaster.ID, "B2C-Credential Succeed for site " + credential.SiteName, "");
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Credential Failed " + credential.SiteName + " Site is not configure for whitelabel " + ex.ToString(), "");
                return null;
            }
        }
        else if (credential.AccountCode != null && credential.AccountCode != "")
        {
            try
            {
                UserMaster = bApi.GetUserMasterByAccountCode(credential.AccountCode);
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Credential Failed AccountCode " + credential.AccountCode + " is not found " + ex.ToString(), "");
                return null;
            }

        }
        else
        {
            try
            {
                UserMaster.IsAuthenticatedUser(credential.UserName, credential.Password, 1);
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-UserMaster.Authenticate Failed for User :" + credential.UserName + "  " + ex.ToString(), "0");
            }
            if (UserMaster == null)
            {
                Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Credential Failed for username " + credential.UserName, "0");
                return null;
            }
            else
            {
                Audit.AddAuditBookingAPI(EventType.Login, Severity.Low, (int)(int)UserMaster.ID, "B2C-Credential Succeed for username " + credential.UserName, "0");
            }
        }
        return UserMaster;
    }
   
    
    private class WSSearch
    {
        private AutoResetEvent[] arEvent;
        private List<WSResult> resultList;
        private SearchResult[][] result;
        public string[] sessionId;
        public LoginInfo AgentLoginInfo;

        /// <summary>
        /// for Sabre Authenticate Token
        /// </summary>
        public string SabreAuthenticateToken;

        private class RequestParam
        {
            public SearchRequest Request;
            public int EventIndex;
            public RequestParam(SearchRequest request, int eventIndex)
            {
                Request = request;
                EventIndex = eventIndex;
            }
        }

        /// <summary>
        /// Method to send error mail
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="UserMaster"></param>
        /// <param name="ex"></param>
        /// <param name="request"></param>
        private void SendErrorMail(string subject, UserMaster UserMaster, Exception ex, WSSearchRequest request)
        {
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
            {
                try
                {
                    string mailMessage = string.Empty;
                    mailMessage = "UserMaster Name: " + UserMaster.FirstName + " " + UserMaster.LastName;
                    mailMessage += "\nAgentMasterId: " + UserMaster.AgentId.ToString();
                    mailMessage += "\nOrigin: " + request.Origin;
                    mailMessage += "\nDestination: " + request.Destination;
                    mailMessage += "\nSearchType" + request.Type.ToString();
                    mailMessage += "\nDeparture Date: " + request.DepartureDate.Date.ToString();
                    if (request.Type == SearchType.Return)
                    {
                        mailMessage += "\nReturn Date: " + request.ReturnDate.Date.ToString();
                    }
                    mailMessage += "\nAdult Count: " + request.AdultCount.ToString();
                    mailMessage += "\nChild Count: " + request.ChildCount.ToString();
                    mailMessage += "\nInfant Count: " + request.ChildCount.ToString();
                    if (request.PreferredCarrier != null && request.PreferredCarrier.Trim() != string.Empty)
                    {
                        mailMessage += "\nPreferred Carrier: " + request.PreferredCarrier;
                    }
                    mailMessage += "\nClass: " + request.CabinClass.ToString();
                    mailMessage += "\nException: " + ex.ToString();
                    mailMessage += "\nStack trace: " + ex.StackTrace;
                    Email.Send(fromEmailId, errorNotificationMailingId, "Error in Booking API - " + subject, mailMessage);
                }
                catch (Exception excep)
                {
                    CT.Core.Audit.Add(EventType.Book, Severity.High, (int)(int)UserMaster.ID, "B2C-ErrorEmail-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
                }
            }
        }

        private bool WSSearchInput(WSSearchRequest request)
        {
            Regex regex = new Regex(@"[aA-zZ]{3}");
            if (request.AdultCount == 0 && request.ChildCount == 0 && request.InfantCount == 0)
            {
                statusDescription = "Please select atleast one passenger";
                statusCode = "03";
                return false;
            }
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
            if (request.DepartureDate < currentDate)
            {
                statusDescription = "Invalid departure date " + request.DepartureDate.ToString() + " currentDate is " + currentDate.ToString();
                statusCode = "05";
                return false;
            }

            if (request.Destination == null || request.Origin == null)
            {
                statusDescription = "Origin or Destination not available";
                statusCode = "06";
                return false;
            }
            else
            {
                if (!(regex.IsMatch(request.Destination) && regex.IsMatch(request.Origin)))
                {
                    statusDescription = "Invalid destination or origin";
                    statusCode = "07";
                    return false;
                }
            }
            //Enum.IsDefined(typeof(SearchType),request.Type) || 
            if ((request.Type != SearchType.OneWay && request.Type != SearchType.Return))
            {
                statusDescription = "Invalid search type";
                statusCode = "08";
                return false;
            }

            if (request.Type == SearchType.Return && request.ReturnDate < request.DepartureDate)
            {
                statusDescription = "Invalid return date ";
                statusCode = "10";
                return false;
            }
            return true;

        }

        public WSSearchResponse Search(WSSearchRequest request, AuthenticationData credential, string ipAddr)
        {
            UserMaster UserMaster = new UserMaster();
            //load UserMaster from credential
            SabreAuthenticateToken = request.TokenVariable;
            UserMaster = WSValidate(credential);
            if (UserMaster != null)
            {
                BookingAPI bApi = new BookingAPI();

                #region Check for validating input parameter

                //validating Request
                if (!(WSSearchInput(request)))
                {
                    WSSearchResponse errorResponse = new WSSearchResponse();
                    WSStatus status = new WSStatus();
                    status.Category = "SR";
                    status.Description = statusDescription;
                    status.StatusCode = statusCode;
                    errorResponse.Status = status;
                    Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)(int)UserMaster.ID, "B2C-Input parameter is incorrect: " + statusDescription, ipAddr);
                    return errorResponse;
                }
                #endregion

                bool error = false;
                WSSearchResponse response = new WSSearchResponse();
                response.CityInfo = new WSSearchResponse.CityAirport[2][];
                response.ErrorMsg = string.Empty;
                List<WSSearchResponse.CityAirport> cityInfoList = new List<WSSearchResponse.CityAirport>();
                WSSearchResponse.CityAirport cityInfo;
                WSSearchResponse.AirportInfo airportInfo;
                resultList = new List<WSResult>();

                #region if city or airport name is passed

                //If city Name is passed
                if (request.Origin.Length != 3)
                {
                    List<Airport> airportList = new List<Airport>();
                    try
                    {
                        airportList = Airport.GetInfoForCityOrAirport(request.Origin);
                    }
                    catch (Exception ex)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "B2C-Airport.GetInfoForCityOrAirport(1st call) failed: Origin: " + request.Origin + " Eror Message :" + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                        SendErrorMail("in Search while loading list of airport of origin city", UserMaster, ex, request);
                        WSStatus status = new WSStatus();
                        status.Category = "SR";
                        status.Description = "Unable to load airportlist of origin city";
                        status.StatusCode = "11";
                        response.Status = status;
                        return response;
                    }

                    //If No airport in city
                    if (airportList.Count == 0)
                    {
                        response.ErrorMsg = "Origin not found";
                        response.Result = new WSResult[0];
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.Normal, (int)UserMaster.ID, "Origin not found", "");
                        error = true;
                    }
                    //Loading City and AirportInfo
                    for (int i = 0; i < airportList.Count; i++)
                    {
                        cityInfo = new WSSearchResponse.CityAirport();
                        cityInfo.CityCode = airportList[i].CityCode;
                        cityInfo.CityName = airportList[i].CityName;
                        cityInfo.CountryCode = airportList[i].CountryCode;
                        cityInfo.CountryName = airportList[i].CountryName;
                        cityInfo.Airport = new List<WSSearchResponse.AirportInfo>();
                        airportInfo = new WSSearchResponse.AirportInfo();
                        airportInfo.airportCode = airportList[i].AirportCode;
                        airportInfo.airportName = airportList[i].AirportName;
                        cityInfo.Airport.Add(airportInfo);

                        while ((i + 1 < airportList.Count) && (airportList[i].CityCode == airportList[i + 1].CityCode))
                        {
                            airportInfo = new WSSearchResponse.AirportInfo();
                            airportInfo.airportCode = airportList[i + 1].AirportCode;
                            airportInfo.airportName = airportList[i + 1].AirportName;
                            cityInfo.Airport.Add(airportInfo);
                            i++;
                        }
                        cityInfoList.Add(cityInfo);
                    }
                    WSSearchResponse.CityAirport[] cityInfoArray = cityInfoList.ToArray();
                    response.CityInfo[0] = new WSSearchResponse.CityAirport[0];
                    //If only airport found in that city
                    if (cityInfoList.Count == 1)
                    {
                        request.Origin = cityInfoList[0].CityCode;
                    }
                    //If more then one aiport in given city then we return error
                    else if (cityInfoList.Count > 1)
                    {
                        response.CityInfo[0] = cityInfoArray;
                        response.ErrorMsg = "Multiple origin found";
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.Normal, (int)UserMaster.ID, "Multiple origin found", ipAddr);
                        response.Result = new WSResult[0];
                        error = true;
                    }
                }
                else
                {
                    response.CityInfo[0] = new WSSearchResponse.CityAirport[0];
                }
                cityInfoList = new List<WSSearchResponse.CityAirport>();
                //If in destination city name is passed
                if (request.Destination.Length != 3)
                {
                    List<Airport> airportList = new List<Airport>();
                    try
                    {
                        airportList = Airport.GetInfoForCityOrAirport(request.Destination);
                    }
                    catch (Exception ex)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "Airport.GetInfoForCityOrAirport(2nd call) failed: Destination " + request.Destination + " Error Message" + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                        SendErrorMail("in Search while loading list of airport of destination city", UserMaster, ex, request);
                        WSStatus status = new WSStatus();
                        status.Category = "SR";
                        status.Description = "Unable to load airportlist of destination city";
                        status.StatusCode = "12";
                        response.Status = status;
                        return response;
                    }
                    if (airportList.Count == 0)
                    {
                        if (response.ErrorMsg.Length == 0)
                        {
                            response.ErrorMsg = "Destination not found";
                        }
                        else
                        {
                            response.ErrorMsg += "Destination not found";
                        }
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.Normal, (int)UserMaster.ID, "Destination not found", ipAddr);
                        response.Result = new WSResult[0];
                        error = true;
                    }
                    for (int i = 0; i < airportList.Count; i++)
                    {
                        cityInfo = new WSSearchResponse.CityAirport();
                        cityInfo.CityCode = airportList[i].CityCode;
                        cityInfo.CityName = airportList[i].CityName;
                        cityInfo.CountryCode = airportList[i].CountryCode;
                        cityInfo.CountryName = airportList[i].CountryName;
                        cityInfo.Airport = new List<WSSearchResponse.AirportInfo>();
                        airportInfo = new WSSearchResponse.AirportInfo();
                        airportInfo.airportCode = airportList[i].AirportCode;
                        airportInfo.airportName = airportList[i].AirportName;
                        cityInfo.Airport.Add(airportInfo);

                        while ((i + 1 < airportList.Count) && (airportList[i].CityCode == airportList[i + 1].CityCode))
                        {
                            airportInfo = new WSSearchResponse.AirportInfo();
                            airportInfo.airportCode = airportList[i + 1].AirportCode;
                            airportInfo.airportName = airportList[i + 1].AirportName;
                            cityInfo.Airport.Add(airportInfo);
                            i++;
                        }
                        cityInfoList.Add(cityInfo);
                    }
                    WSSearchResponse.CityAirport[] cityInfoArray = cityInfoList.ToArray();
                    response.CityInfo[1] = new WSSearchResponse.CityAirport[0];
                    //If only one airport is found
                    if (cityInfoList.Count == 1)
                    {
                        request.Destination = cityInfoList[0].CityCode;
                    }
                    //If multpel airport in the city is found
                    else if (cityInfoList.Count > 1)
                    {
                        response.CityInfo[1] = cityInfoArray;
                        if (response.ErrorMsg.Length == 0)
                        {
                            response.ErrorMsg = "Multiple destination found";
                        }
                        else
                        {
                            response.ErrorMsg += "Multiple destination found";
                        }
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.Normal, (int)UserMaster.ID, "Multiple destination found", ipAddr);
                        response.Result = new WSResult[0];
                        error = true;
                    }
                }
                else
                {
                    response.CityInfo[1] = new WSSearchResponse.CityAirport[0];
                }

                #endregion

                //If there is only one airport in given origin and destination
                if (!error)
                {
                    //response.IsDomestic = Util.IsDomestic(request.Origin, request.Destination, "AE"); not required as not handling domestic

                    response.RoundTrip = request.Type == SearchType.Return;
                    bool domesticReturn = response.IsDomestic && response.RoundTrip;

                    #region Building Request
                    SearchRequest mRequest = new SearchRequest();
                    mRequest.AdultCount = request.AdultCount;
                    mRequest.ChildCount = request.ChildCount;
                    mRequest.InfantCount = request.InfantCount;
                    mRequest.SeniorCount = request.SeniorCount;
                    // Advanced Search
                    mRequest.RefundableFares = request.RefundableFares;
                    mRequest.MaxStops = request.MaxStops == "-1" ? string.Empty : request.MaxStops;

                    // Assigning XML credentials
                    //mRequest.XMLId = UserMaster.AirXMLId;
                    //mRequest.XMLpwd = UserMaster.AirXMLpwd;
                    //mRequest.XMLhap = UserMaster.AirXMLhap;
                    //mRequest.XMLPcc = UserMaster.AirXmlPcc;

                    #region Preferred Carrier

                    mRequest.Sources = new List<string>();
                    List<string> prefCarrierGDS = new List<string>();
                    // in domestic search if preferred carrier is blank or null then search all carrier
                    if (response.IsDomestic && (request.PreferredCarrier == null || request.PreferredCarrier.Trim() == ""))
                    {
                        //request.PreferredCarrier = "IT,S2,AI,IC,9W,6E,SG,9H,I7,G8";
                        request.PreferredCarrier = "";
                    }
                    string[] prefCarrier;
                    if (request.PreferredCarrier != null && request.PreferredCarrier.Trim() != "")
                    {
                        prefCarrier = request.PreferredCarrier.Split(',');
                        for (int i = 0; i < prefCarrier.Length; i++)
                        {
                            prefCarrier[i] = prefCarrier[i].Trim().ToUpper();
                        }
                    }
                    else
                    {
                        prefCarrier = new string[0];
                    }
                    if (!response.IsDomestic)
                    {
                        DataTable dtFlightSources = AgentMaster.GetAgentSources(AgentLoginInfo.AgentId, 1);

                        if (prefCarrier.Length > 0)
                        {
                            mRequest.RestrictAirline = true;
                        }

                        mRequest.Sources = new List<string>();
                        
                        foreach (DataRow row in dtFlightSources.Rows)
                        {
                            mRequest.Sources.Add(row["Name"].ToString());

                            //Added reagrding wego audit purpose.
                            if (!string.IsNullOrEmpty(request.Sources))
                            {
                                request.Sources += "," + Convert.ToString(row["Name"]);
                            }
                            else
                            {
                                request.Sources = Convert.ToString(row["Name"]);
                            }
                        }

                        if (mRequest.RestrictAirline)
                        {
                            //Restrict airline will not be applicable for LCC so remove from search
                            
                            
                            string carrier = string.Join(",", prefCarrier);
                            if (carrier.Contains("G9") && mRequest.Sources.Contains("G9"))//If restricted to AirArabia
                            {
                                mRequest.Sources.Clear();//Remove UAPI and other sources
                                mRequest.Sources.Add("G9");
                            }
                            else
                            {
                                mRequest.Sources.Remove("G9");//Remove AirArabia
                            }
                            if (carrier.Contains("FZ") && mRequest.Sources.Contains("FZ"))//if restricted to FlyDubai
                            {
                                mRequest.Sources.Clear();//Remove UAPI and other sources
                                mRequest.Sources.Add("FZ");
                            }
                            else
                            {
                                mRequest.Sources.Remove("FZ");//Remove FlyDubai
                            }
                            if (carrier.Contains("IX") && mRequest.Sources.Contains("IX"))
                            {
                                mRequest.Sources.Clear();
                                mRequest.Sources.Add("IX");
                            }
                            else
                            {
                                mRequest.Sources.Remove("IX");
                            }
                            if (carrier.Contains("J9") && mRequest.Sources.Contains("J9"))
                            {
                                mRequest.Sources.Clear();
                                mRequest.Sources.Add("J9");
                            }
                            else
                            {
                                mRequest.Sources.Remove("J9");
			    }
			    if (carrier.Contains("6E") && mRequest.Sources.Contains("6E"))
                            {
                                mRequest.Sources.Clear();
                                mRequest.Sources.Add("6E");
                            }
                            else
                            {
                                mRequest.Sources.Remove("6E");
                            }
                            if (carrier.Contains("SG") && mRequest.Sources.Contains("SG"))
                            {
                                mRequest.Sources.Clear();
                                mRequest.Sources.Add("SG");
                            }
                            else
                            {
                                mRequest.Sources.Remove("SG");
                            }
                            if (carrier.Contains("G8") && mRequest.Sources.Contains("G8"))
                            {
                                mRequest.Sources.Clear();
                                mRequest.Sources.Add("G8");
                            }
                            else
                            {
                                mRequest.Sources.Remove("G8");
                            }
                            //else
                            {
                                
                                //mRequest.Sources.Add("GDS");
                                //mRequest.Sources.Add("G9");
                                //mRequest.Sources.Add("FZ");
                            }
                        }
                        else
                        {
                            //mRequest.Sources.Add("GDS");
                            //mRequest.Sources.Add("G9");
                            //mRequest.Sources.Add("FZ");
                        }
                    }
                    #endregion
                    if (!domesticReturn)
                    {
                        mRequest.Type = request.Type;
                        result = new SearchResult[1][];
                        arEvent = new AutoResetEvent[1];
                        sessionId = new string[1];
                    }
                    else
                    {
                        // for this case two one way hits will be made. 
                        // the second one way hit is made below.
                        mRequest.Type = SearchType.OneWay;
                        result = new SearchResult[2][];
                        arEvent = new AutoResetEvent[2];
                        sessionId = new string[2];
                    }
                    if (mRequest.Type == SearchType.OneWay)
                    {
                        mRequest.Segments = new FlightSegment[1];
                    }
                    else if (mRequest.Type == SearchType.Return)
                    {
                        mRequest.Segments = new FlightSegment[2];
                    }
                    mRequest.Segments[0] = new FlightSegment();
                    mRequest.Segments[0].Origin = request.Origin;
                    mRequest.Segments[0].Destination = request.Destination;
                    mRequest.Segments[0].flightCabinClass = request.CabinClass;
                    mRequest.Segments[0].PreferredDepartureTime = request.DepartureDate;
                    mRequest.Segments[0].PreferredArrivalTime = request.DepartureDate;
                    mRequest.Segments[0].PreferredAirlines = prefCarrier;
                    mRequest.Segments[0].NearByOriginPort = true;

                    if (mRequest.Type == SearchType.Return)
                    {
                        mRequest.Segments[1] = new FlightSegment();
                        mRequest.Segments[1].Origin = request.Destination;
                        mRequest.Segments[1].Destination = request.Origin;
                        mRequest.Segments[1].flightCabinClass = request.CabinClass;
                        mRequest.Segments[1].PreferredDepartureTime = request.ReturnDate;
                        mRequest.Segments[1].PreferredArrivalTime = request.ReturnDate;
                        mRequest.Segments[1].PreferredAirlines = prefCarrier;
                        mRequest.Segments[1].NearByOriginPort = true;
                    }
                    mRequest.TimeIntervalSpecified = false;
                    #endregion
                    try
                    {
                        RequestParam reqParam = new RequestParam(mRequest, 0);
                        WaitCallback obSearch = new WaitCallback(Search);
                        arEvent[0] = new AutoResetEvent(false);
                        ThreadPool.QueueUserWorkItem(obSearch, ((object)reqParam));
                        if (domesticReturn)
                        {
                            #region Building domestic return request.
                            SearchRequest retRequest = new SearchRequest();
                            retRequest.AdultCount = mRequest.AdultCount;
                            retRequest.ChildCount = mRequest.ChildCount;
                            retRequest.SeniorCount = mRequest.SeniorCount;
                            retRequest.InfantCount = mRequest.InfantCount;
                            retRequest.Sources = mRequest.Sources;
                            retRequest.Type = mRequest.Type;
                            retRequest.Segments = new FlightSegment[1];
                            retRequest.Segments[0] = new FlightSegment();
                            retRequest.Segments[0].Origin = request.Destination;
                            retRequest.Segments[0].Destination = request.Origin;
                            retRequest.Segments[0].flightCabinClass = request.CabinClass;
                            retRequest.Segments[0].PreferredAirlines = mRequest.Segments[0].PreferredAirlines;
                            retRequest.Segments[0].PreferredDepartureTime = request.ReturnDate;
                            #endregion

                            RequestParam retParam = new RequestParam(retRequest, 1);
                            WaitCallback ibSearch = new WaitCallback(Search);
                            arEvent[1] = new AutoResetEvent(false);
                            ThreadPool.QueueUserWorkItem(ibSearch, ((object)retParam));
                        }
                        //Add MaxTimeOut based on the selected Sources from ActiveSource config, same as in MSE
                        int maxTimeOut = 0;
                        foreach (string src in mRequest.Sources)
                        {
                            int timeOut = 0;
                            if (ConfigurationSystem.ActiveSources.ContainsKey(src))
                            {
                                timeOut = Convert.ToInt32(ConfigurationSystem.ActiveSources[src].Split(',')[1]);
                            }
                            maxTimeOut = timeOut > maxTimeOut ? timeOut : maxTimeOut;
                        }
                        maxTimeOut += 30;//Additional 30 seconds to wait for MSE CombineResult(...)  if in case MSE executes for maxTimeOut allow 30 seconds to combine results
                        Audit.Add(EventType.Search, Severity.Low, (int)UserMaster.ID, "B2C Search MaxTimeOut: " + maxTimeOut + " Seconds", "");
                        //Change timeout to 2 minutes
                        //TODO: find max timeout from config.                        
                        WaitHandle.WaitAll(arEvent, new TimeSpan(0, 0, maxTimeOut), true);                        
                    }
                    catch (System.Threading.ThreadAbortException threadEx)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "Search fail - Search Type :" + request.Type.ToString() + " Origin :" + request.Origin + " Destination :" + request.Destination + "Departure Date: " + request.DepartureDate.ToString() + " Adult: " + request.AdultCount.ToString() + "Child Count " + request.ChildCount + " Error Message :" + threadEx.ToString() + " Stack Trace = " + threadEx.StackTrace, ipAddr);
                        WSSearchResponse searchRes = new WSSearchResponse();
                        WSStatus resStatus = new WSStatus();
                        resStatus.Category = "SR";
                        resStatus.Description = threadEx.Message;
                        resStatus.StatusCode = "11";
                        searchRes.Status = resStatus;
                        return searchRes;
                    }
                    catch (Exception ex)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "Search fail - Search Type :" + request.Type.ToString() + " Origin :" + request.Origin + " Destination :" + request.Destination + "Departure Date: " + request.DepartureDate.ToString() + " Adult: " + request.AdultCount.ToString() + "Child Count " + request.ChildCount + " Error Message :" + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                        SendErrorMail("in Search while getting result from MSE", UserMaster, ex, request);
                        WSSearchResponse searchRes = new WSSearchResponse();
                        WSStatus resStatus = new WSStatus();
                        resStatus.Category = "SR";
                        resStatus.Description = ex.Message;
                        resStatus.StatusCode = "11";
                        searchRes.Status = resStatus;
                        return searchRes;
                    }

                    #region Building API WSResult

                    //Trip Indicator 1 for oneway search
                    try
                    {
                        if (result[0] != null && result[0].Length > 0)
                        {
                            BuildResultList(ref resultList, result[0], request.Origin, request.Destination, 1, AgentLoginInfo.AgentId, (int)UserMaster.ID, credential);
                            response.Result = resultList.ToArray();
                           
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "Search fail - Search Type :" + request.Type.ToString() + " Origin :" + request.Origin + " Destination :" + request.Destination + "Departure Date: " + request.DepartureDate.ToString() + " Adult: " + request.AdultCount.ToString() + "Child Count " + request.ChildCount + " Error Message :" + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                        SendErrorMail("in Search while building WSResult", UserMaster, ex, request);
                        WSSearchResponse searchRes = new WSSearchResponse();
                        WSStatus resStatus = new WSStatus();
                        resStatus.Category = "SR";
                        resStatus.Description = ex.Message;
                        resStatus.StatusCode = "11";
                        searchRes.Status = resStatus;
                        return searchRes;
                    }
                    response.SessionId = sessionId[0];
                    //Trip Indicator 2 for return search
                    if (domesticReturn)
                    {
                        try
                        {
                            if (result[1] != null && result[1].Length > 0)
                            {
                                BuildResultList(ref resultList, result[1], request.Destination, request.Origin, 2, AgentLoginInfo.AgentId, (int)UserMaster.ID, credential);
                                response.Result = resultList.ToArray();

                            }
                        }
                        catch (Exception ex)
                        {
                            WSSearchResponse searchResp = new WSSearchResponse();
                            WSStatus resStat = new WSStatus();
                            resStat.Category = "SR";
                            resStat.Description = ex.Message;
                            resStat.StatusCode = "11";
                            searchResp.Status = resStat;
                            SendErrorMail("in Search while building WSResult of Domestic return", UserMaster, ex, request);
                            Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "Search fail - Search Type :" + request.Type.ToString() + " Origin :" + request.Origin + " Destination :" + request.Destination + "Departure Date: " + request.DepartureDate.ToString() + " Adult: " + request.AdultCount.ToString() + "Child Count " + request.ChildCount + " Error Message :" + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                            return searchResp;
                        }
                        response.SessionId += "," + sessionId[1];
                    }

                    #endregion
                }
                else
                {
                    WSStatus status = new WSStatus();
                    status.Category = "SR";
                    status.Description = "Fail";
                    status.StatusCode = "13";
                    response.Status = status;
                    return response;
                }
                WSStatus wsStatus = new WSStatus();
                wsStatus.Category = "SR";
                wsStatus.Description = "Successfull";
                wsStatus.StatusCode = "02";
                response.Status = wsStatus;
                if (response.Result != null)
                {
                    Audit.AddAuditBookingAPI(EventType.Search, Severity.Low, (int)UserMaster.ID, "Search succeed of: " + request.Origin + " - " + request.Destination + " for departure date : " + request.DepartureDate.ToString() + " Number of result found :" + response.Result.Length.ToString(), ipAddr);
                }
                else
                {
                    Audit.AddAuditBookingAPI(EventType.Search, Severity.Normal, (int)UserMaster.ID, "Search succeed but no result found. Origin : " + request.Origin + " Destination - " + request.Destination + " for departure date : " + request.DepartureDate.ToString() + " Preferred carrier " + request.PreferredCarrier + " - Adult Count:" + request.AdultCount + " Child count:" + request.ChildCount + " Class: " + request.CabinClass.ToString()+ " Isdomestic "+response.IsDomestic.ToString(), ipAddr);
                }
                return response;
            }
            //If credential Fail
            else
            {
                WSSearchResponse response = new WSSearchResponse();
                WSStatus wsStatus = new WSStatus();
                wsStatus.Category = "SR";
                wsStatus.Description = "login failed";
                wsStatus.StatusCode = "01";
                response.Status = wsStatus;
                return response;
            }
        }

        private void Search(object req)
        {
            RequestParam reqParam = (RequestParam)req;
            try
            {
                
                MetaSearchEngine mse = new MetaSearchEngine();
                mse.SettingsLoginInfo = AgentLoginInfo;
                mse.TransactionType = "B2C";
                mse.SabreAuthenicateToken = SabreAuthenticateToken ;  //added by bangar
                mse.ListSectors = SectorList.GetSectorList(false);
                result[reqParam.EventIndex] = mse.GetResults(reqParam.Request);
                SabreAuthenticateToken =  mse.SabreAuthenicateToken;  //added by bangar
                sessionId[reqParam.EventIndex] = mse.SessionId;
                Audit.Add(EventType.Search, Severity.Low, (int)AgentLoginInfo.UserID, "(BookingAPI)MSE Search Results found " + result[reqParam.EventIndex].Length, "");
            }
            catch (Exception ex)
            {
                result[reqParam.EventIndex] = new SearchResult[0];
                Audit.AddAuditBookingAPI(EventType.Search, Severity.Normal, 0, "Search failed: " + ex.ToString() + "Stack Trace :" + ex.StackTrace, "");
            }
            finally
            {
                arEvent[reqParam.EventIndex].Set();
            }
        }

        public void BuildResultList(ref List<WSResult> resultList, SearchResult[] mResult, string origin, string destination, int tripIndicator, int AgentMasterId, int UserMasterId, AuthenticationData authDta)
        {
            try
            {
                Audit.Add(EventType.Search, Severity.Low, 1, "BookingAPI Buidling results Entered", "");
                int counter = 0;
                //AgentMasterId = AgentLoginInfo.AgentId;//temperory
                //if (AgentMasterId == 0)
                //{
                //    ConfigurationSystem config = new ConfigurationSystem();
                //    AgentMasterId = Convert.ToInt32(config.GetSelfAgencyId()["selfAgentMasterId"]);
                //}
                //List<AirlineCommission> commissionList = new List<AirlineCommission>();
                //commissionList = AirlineCommission.GetActiveAirlineCommissionOfAgency(AgentMasterId);
                //DataTable dtAgentMarkup = UpdateMarkup.GetAllMarkup((int)AgentMasterId, 1);

                AgentMaster agency = new AgentMaster(AgentMasterId);
                int paxCount = 0;
                foreach (Fare fare in mResult[0].FareBreakdown)
                {
                    paxCount += fare.PassengerCount;
                }
                for (int i = 0; i < mResult.Length; i++)
                {
                    WSResult result = new WSResult();
                    result.TripIndicator = tripIndicator;
                    WSFare fare = new WSFare();
                    result.ResultId = mResult[i].ResultId-1;//Passing the original result index assigned in MSE //i;//For comparing unique result while grouping similar results                    
                    decimal totalMarkup = 0m;
                    SearchResult searchResult = mResult[i];
                    //List<FlightInfo> flightInfos = new List<FlightInfo>();
                    //flightInfos.AddRange(searchResult.Flights[0]);
                    //if (searchResult.Flights.Length > 1)
                    //{
                    //    flightInfos.AddRange(searchResult.Flights[1]);
                    //}
                    //bool isAirlineLCC = Airline.IsAirlineLCC(searchResult.Flights[0][0].Airline);
                    //bool isInternational = flightInfos.Exists(delegate (FlightInfo fi) { return fi.Origin.CountryCode != fi.Destination.CountryCode; });
                    result.FareBreakdown = new WSPTCFare[mResult[i].FareBreakdown.Length];
                    //string source = SearchResult.GetFlightBookingSource(searchResult.ResultBookingSource);
                    try
                    {
                        fare.TaxDetails = mResult[i].Price.TaxDetails;
                        for (int j = 0; j < mResult[i].FareBreakdown.Length; j++)
                        {
                            //Keep B2B filter criteria for HandlingFee because HandlingFee will be applied only for B2B and since we do filter B2C after this DefaultView will be changed
                            string b2bRowFilter = string.Empty;
                            double b2bDiscount = 0;
                            #region Old Markup code
                            //if (dtAgentMarkup != null && dtAgentMarkup.Rows.Count > 0)
                            //{
                            //    dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND FlightType='" + (isInternational ? "INT" : "DOM") + "' AND JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' AND CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "'";
                            //    b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND FlightType='" + (isInternational ? "INT" : "DOM") + "' AND JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' AND CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "'";
                            //    if (dtAgentMarkup.DefaultView.Count == 0)//FlightType not found FlightType=All, JourneyType=ONW,RET, CarrierType=GDS,LCC
                            //    {
                            //        dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                            //        b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                            //    }

                            //    if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, JourneyType not found FlightType=All, JourneyType=All, CarrierType=GDS,LCC
                            //    {
                            //        dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                            //        b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                            //    }

                            //    if (dtAgentMarkup.DefaultView.Count == 0)//CarrierType, JourneyType not found FlightType=INT,DOM, JourneyType=All, CarrierType=All
                            //    {
                            //        dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                            //        b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                            //    }

                            //    if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, CarrierType not found FlightType=All, JourneyType=ONW,RET, CarrierType=All
                            //    {
                            //        dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                            //        b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                            //    }

                            //    if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, JourneyType, CarrierType not found FlightType=All, JourneyType=All, CarrierType=All
                            //    {
                            //        dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                            //        b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                            //    }
                            //    //Assign price details from FareBreakDown
                            //    decimal grossFare = Convert.ToDecimal(searchResult.FareBreakdown[j].BaseFare / searchResult.FareBreakdown[j].PassengerCount);
                            //    searchResult.Price.PublishedFare = grossFare;
                            //    searchResult.Price.Tax = searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount;

                            //    if (AgentMasterId > 1)
                            //    {                                    
                            //        if (dtAgentMarkup.DefaultView != null && dtAgentMarkup.DefaultView.Count > 0 && dtAgentMarkup.DefaultView[0]["MarkupType"] != DBNull.Value)
                            //        {
                            //            decimal agtMarkUp = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Markup"]);
                            //            decimal discount = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Discount"]);


                            //            searchResult.Price.MarkupType = dtAgentMarkup.DefaultView[0]["MarkupType"].ToString();
                            //            searchResult.Price.MarkupValue = agtMarkUp;

                            //            if (Convert.ToString(dtAgentMarkup.DefaultView[0]["MarkupType"]) == "P")
                            //            {
                            //                if (agency.AgentAirMarkupType == "BF")
                            //                {
                            //                    searchResult.Price.Markup = (Convert.ToDecimal(grossFare) * agtMarkUp) / 100;// TODO- should be configurable Basefare/totalFare
                            //                }
                            //                else if (agency.AgentAirMarkupType == "TX")
                            //                {
                            //                    if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            //                    {
                            //                        searchResult.Price.Markup = (Convert.ToDecimal((searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / paxCount)) * agtMarkUp) / 100;
                            //                    }
                            //                    else
                            //                    {
                            //                        searchResult.Price.Markup = (Convert.ToDecimal(searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) * agtMarkUp) / 100;
                            //                    }
                            //                }
                            //                else if (agency.AgentAirMarkupType == "TF")
                            //                {
                            //                    if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            //                    {
                            //                        searchResult.Price.Markup = (Convert.ToDecimal(((decimal)searchResult.FareBreakdown[j].TotalFare / searchResult.FareBreakdown[j].PassengerCount) + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / paxCount)) * agtMarkUp) / 100;
                            //                    }
                            //                    else
                            //                    {
                            //                        searchResult.Price.Markup = (Convert.ToDecimal(searchResult.FareBreakdown[j].TotalFare / searchResult.FareBreakdown[j].PassengerCount) * agtMarkUp) / 100;
                            //                    }
                            //                }
                            //            }
                            //            else
                            //            {
                            //                searchResult.Price.Markup = agtMarkUp;
                            //            }
                            //            if (Convert.ToString(dtAgentMarkup.DefaultView[0]["DiscountType"]) == "P")
                            //            {
                            //                if (agency.AgentAirMarkupType == "BF") // Base Fare
                            //                {
                            //                    b2bDiscount = Convert.ToDouble((Convert.ToDecimal(grossFare) * discount) / 100);
                            //                }
                            //                else if (agency.AgentAirMarkupType == "TX")  // Tax
                            //                {
                            //                    if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            //                    {
                            //                        b2bDiscount = (double)(Convert.ToDecimal((searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / paxCount)) * discount) / 100;
                            //                    }
                            //                    else
                            //                    {
                            //                        b2bDiscount = (double)(Convert.ToDecimal(searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) * discount) / 100;
                            //                    }
                            //                }
                            //                else if (agency.AgentAirMarkupType == "TF") // Total Fare = BaseFare + Tax
                            //                {
                            //                    if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            //                    {
                            //                        b2bDiscount = (double)(Convert.ToDecimal(((decimal)searchResult.FareBreakdown[j].TotalFare / searchResult.FareBreakdown[j].PassengerCount) + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / paxCount)) * discount) / 100;
                            //                    }
                            //                    else
                            //                    {
                            //                        b2bDiscount = (double)(Convert.ToDecimal(searchResult.FareBreakdown[j].TotalFare / searchResult.FareBreakdown[j].PassengerCount) * discount) / 100;
                            //                    }
                            //                }
                            //            }
                            //            else
                            //            {
                            //                b2bDiscount = Convert.ToDouble(discount);
                            //            }      
                            //            //Discount making zero.
                            //            searchResult.Price.Discount = 0;
                            //        }

                            //        //if (transType == "B2C")
                            //        {
                            //            //Get B2C Markup
                            //            dtAgentMarkup.DefaultView.RowFilter = "TransType='B2C' AND SourceId=" + source + " AND ProductId=1 AND FlightType='" + (isInternational ? "INT" : "DOM") + "' AND JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' AND CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "'";
                            //            if (dtAgentMarkup.DefaultView.Count == 0)
                            //            {
                            //                dtAgentMarkup.DefaultView.RowFilter = "TransType='B2C' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (searchResult.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                            //            }
                            //            if (dtAgentMarkup.DefaultView != null && dtAgentMarkup.DefaultView.Count > 0 && dtAgentMarkup.DefaultView[0]["MarkupType"] != DBNull.Value)
                            //            {
                            //                decimal agtB2CMarkUp = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Markup"]);
                            //                decimal discount = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Discount"]);
                            //                searchResult.Price.B2CMarkupType = dtAgentMarkup.DefaultView[0]["MarkupType"].ToString();
                            //                searchResult.Price.B2CMarkupValue = agtB2CMarkUp;

                            //                if (Convert.ToString(dtAgentMarkup.DefaultView[0]["MarkupType"]) == "P")
                            //                {
                            //                    if (agency.AgentAirMarkupType == "BF")
                            //                    {
                            //                        searchResult.Price.B2CMarkup = ((Convert.ToDecimal(searchResult.FareBreakdown[j].BaseFare) + searchResult.Price.Markup - searchResult.Price.Discount) * agtB2CMarkUp) / 100;// TODO- should be configurable Basefare/totalFare
                            //                    }
                            //                    else if (agency.AgentAirMarkupType == "TX")
                            //                    {
                            //                        if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            //                        {
                            //                            searchResult.Price.B2CMarkup = (Convert.ToDecimal((searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount)+searchResult.Price.Markup + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / paxCount)) * agtB2CMarkUp) / 100;
                            //                        }
                            //                        else
                            //                        {
                            //                            searchResult.Price.B2CMarkup = ((searchResult.Price.Markup )+ Convert.ToDecimal(searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) * agtB2CMarkUp) / 100;
                            //                        }
                            //                     }
                            //                    else if (agency.AgentAirMarkupType == "TF")
                            //                    {
                            //                        if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            //                        {
                            //                            searchResult.Price.B2CMarkup = (Convert.ToDecimal((searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) + searchResult.Price.Markup + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / paxCount)) * agtB2CMarkUp) / 100;
                            //                        }
                            //                        else
                            //                        {
                            //                            searchResult.Price.B2CMarkup = ((searchResult.Price.Markup)+Convert.ToDecimal(searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) * agtB2CMarkUp) / 100;
                            //                        }
                            //                     }
                            //                }
                            //                else
                            //                {
                            //                    searchResult.Price.B2CMarkup = agtB2CMarkUp;
                            //                }
                            //                if (Convert.ToString(dtAgentMarkup.DefaultView[0]["DiscountType"]) == "P")
                            //                {
                            //                    if (agency.AgentAirMarkupType == "BF") // Base Fare
                            //                    {
                            //                        b2bDiscount = Convert.ToDouble((Convert.ToDecimal(grossFare) * discount) / 100);
                            //                    }
                            //                    else if (agency.AgentAirMarkupType == "TX")  // Tax
                            //                    {
                            //                        if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            //                        {
                            //                            searchResult.Price.Discount = (Convert.ToDecimal((searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / paxCount)) * discount) / 100;
                            //                        }
                            //                        else
                            //                        {
                            //                            searchResult.Price.Discount =(Convert.ToDecimal(searchResult.FareBreakdown[j].Tax / searchResult.FareBreakdown[j].PassengerCount) * discount) / 100;
                            //                        }
                            //                    }
                            //                    else if (agency.AgentAirMarkupType == "TF") // Total Fare = BaseFare + Tax
                            //                    {
                            //                        if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            //                        {
                            //                            searchResult.Price.Discount =(Convert.ToDecimal(((decimal)searchResult.FareBreakdown[j].TotalFare / searchResult.FareBreakdown[j].PassengerCount) + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / paxCount)) * discount) / 100;
                            //                        }
                            //                        else
                            //                        {
                            //                            searchResult.Price.Discount = (Convert.ToDecimal(searchResult.FareBreakdown[j].TotalFare / searchResult.FareBreakdown[j].PassengerCount) * discount) / 100;
                            //                        }
                            //                    }
                            //                }
                            //                else
                            //                {
                            //                    searchResult.Price.Discount = discount;
                            //                }
                            //            }
                            //        }
                            //    }
                            //    dtAgentMarkup.DefaultView.RowFilter = b2bRowFilter;

                            //    //Calculate Handling Fee only if Login user is from India
                            //    if (searchResult.LoginCountryCode == "IN" && dtAgentMarkup.DefaultView.Count > 0 && (dtAgentMarkup.DefaultView[0]["HandlingFeeType"]) != DBNull.Value)
                            //    {
                            //        decimal handlingFee = 0m, handlingFeeAmount = 0m;
                            //        if (dtAgentMarkup.DefaultView[0]["HandlingFeeValue"] != DBNull.Value)
                            //        {
                            //            handlingFee = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["HandlingFeeValue"]);
                            //        }

                            //        searchResult.Price.HandlingFeeType = Convert.ToString(dtAgentMarkup.DefaultView[0]["HandlingFeeType"]);
                            //        searchResult.Price.HandlingFeeValue = handlingFee;

                            //        decimal totalFare = (decimal)(searchResult.FareBreakdown[j].TotalFare / searchResult.FareBreakdown[j].PassengerCount) + (searchResult.Price.Markup) + (searchResult.Price.B2CMarkup);
                            //        if (searchResult.Price.HandlingFeeType == "P")
                            //        {
                            //            if (searchResult.FareBreakdown[j].PassengerType == PassengerType.Adult && searchResult.ResultBookingSource == BookingSource.TBOAir)//Add OtherCharges for Adult only
                            //            {
                            //                handlingFeeAmount = (totalFare + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / searchResult.FareBreakdown[j].PassengerCount)) * handlingFee / 100;
                            //            }
                            //            else
                            //            {
                            //                handlingFeeAmount = (totalFare * handlingFee / 100);
                            //            }
                            //            handlingFeeAmount = handlingFeeAmount * searchResult.FareBreakdown[j].PassengerCount;
                            //        }
                            //        else
                            //        {
                            //            handlingFeeAmount = handlingFee * searchResult.FareBreakdown[j].PassengerCount;
                            //        }
                            //        searchResult.Price.HandlingFeeAmount += handlingFeeAmount;
                            //        searchResult.BaseFare += (double)handlingFeeAmount;
                            //        searchResult.FareBreakdown[j].HandlingFee = handlingFeeAmount;
                            //        searchResult.TotalFare += (double)handlingFeeAmount;
                            //    }
                            //}

                            #endregion
                            ////// TEst Ziyad remove

                            //foreach (FlightInfo[] inf in mResult[i].Flights)
                            //{
                            //    foreach (FlightInfo infStand in inf)
                            //    {
                            //        infStand.FlightStatus = FlightStatus.Waitlisted;
                            //    }
                            //}

                            //System.Xml.Serialization.XmlSerializer ser1 = new System.Xml.Serialization.XmlSerializer(typeof(SearchResult));
                            //System.Text.StringBuilder sb1 = new System.Text.StringBuilder();
                            //System.IO.StringWriter writer1 = new System.IO.StringWriter(sb1);
                            //ser1.Serialize(writer1, mResult[i]);  // Here Classes are converted to XML String. 
                            //                                      // This can be viewed in SB or writer.
                            //                                      // Above XML in SB can be loaded in XmlDocument object                
                            //System.Xml.XmlDocument doc1 = new System.Xml.XmlDocument();
                            //doc1.LoadXml(sb1.ToString());
                            ////doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchReq.xml");
                            //string filePath1 = @"D:\XMLLogs\Test\ObjectRead\firsttest" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            //doc1.Save(filePath1);

                            //// end test
                            ///
                            //if (j == 0)
                            //{
                            //    mResult[i].FareBreakdown[j].TotalFare += (double)mResult[i].Price.InputVATAmount;
                            //}
                            //mResult[i].Price = CT.AccountingEngine.AccountingEngine.GetPrice(mResult[i], j, AgentMasterId, 0, 1, "B2C");
                            fare.BaseFare += (decimal)(mResult[i].FareBreakdown[j].BaseFare) + (mResult[i].FareBreakdown[j].HandlingFee);
                            fare.PLB = mResult[i].Price.AgentPLB;
                            fare.Tax += mResult[i].FareBreakdown[j].Tax;
                            fare.InputVAT = mResult[i].Price.InputVATAmount;
                            if (mResult[i].Price.TaxDetails != null && mResult[i].Price.TaxDetails.InputVAT != null)
                            {
                                fare.InputVATApplied = mResult[i].Price.TaxDetails.InputVAT.CostIncluded;
                                if (j == 0 && !mResult[i].Price.TaxDetails.InputVAT.CostIncluded)
                                {
                                    fare.Tax += mResult[i].Price.InputVATAmount;
                                }
                            }
                            // commented by hari malla.calculated markup,discount for total pax in MSE.
                            fare.Discount += (mResult[i].FareBreakdown[j].AgentDiscount);// * mResult[i].FareBreakdown[j].PassengerCount;
                            fare.Currency = mResult[i].Currency;
                            fare.AgentCommission += mResult[i].Price.AgentCommission;
                            fare.ServiceTax = mResult[i].Price.SeviceTax;
                            fare.AirTransFee = mResult[i].Price.AirlineTransFee;
                            fare.Markup += mResult[i].FareBreakdown[j].AgentMarkup;// * mResult[i].FareBreakdown[j].PassengerCount;
                            fare.MarkupType = mResult[i].Price.MarkupType;
                            fare.MarkupValue = mResult[i].Price.MarkupValue;
                            fare.B2CMarkup += mResult[i].FareBreakdown[j].B2CMarkup;// * mResult[i].FareBreakdown[j].PassengerCount;
                            fare.B2CMarkupType = mResult[i].Price.B2CMarkupType;
                            fare.B2CMarkupValue = mResult[i].Price.B2CMarkupValue;
                            fare.AdditionalTxnFee = mResult[i].Price.AdditionalTxnFee;
                            fare.OtherCharges = mResult[i].Price.OtherCharges;                            
                            fare.SServiceFee = mResult[i].Price.SServiceFee;
                            fare.TransactionFee = mResult[i].Price.TransactionFee;
                            //mResult[i].FareBreakdown[j].AgentMarkup = mResult[i].Price.Markup;
                            //mResult[i].FareBreakdown[j].B2CMarkup = mResult[i].Price.B2CMarkup;
                            //mResult[i].FareBreakdown[j].AgentDiscount = (Convert.ToDecimal(b2bDiscount) + mResult[i].Price.Discount);
                            //totalMarkup += mResult[i].Price.Markup + mResult[i].Price.B2CMarkup;
                            result.FareBreakdown[j] = new WSPTCFare();
                            result.FareBreakdown[j].PassengerType = mResult[i].FareBreakdown[j].PassengerType;
                            result.FareBreakdown[j].PassengerCount = mResult[i].FareBreakdown[j].PassengerCount;
                            result.FareBreakdown[j].B2CMarkup = mResult[i].FareBreakdown[j].B2CMarkup / mResult[i].FareBreakdown[j].PassengerCount;
                            result.FareBreakdown[j].Markup = mResult[i].FareBreakdown[j].AgentMarkup / mResult[i].FareBreakdown[j].PassengerCount;
                            result.FareBreakdown[j].SupplierCurrency = mResult[i].Price.SupplierCurrency;

                            result.FareBreakdown[j].SupplierPrice = (decimal)mResult[i].FareBreakdown[j].SupplierFare;
                            result.FareBreakdown[j].Tax = (decimal)(mResult[i].FareBreakdown[j].TotalFare - mResult[i].FareBreakdown[j].BaseFare);
                            result.FareBreakdown[j].BaseFare = (decimal)mResult[i].FareBreakdown[j].BaseFare + (mResult[i].FareBreakdown[j].HandlingFee);


                            //Agent commission not there in BookingEngine-Fare.
                            //result.FareBreakdown[j].AgentCommission = (decimal)mResult[i].FareBreakdown[j];
                            result.FareBreakdown[j].AdditionalTxnFee = mResult[i].FareBreakdown[j].AdditionalTxnFee;
                            result.FareBreakdown[j].AirlineTransFee = mResult[i].FareBreakdown[j].AirlineTransFee;

                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, UserMasterId, "Airline Code " + mResult[i].Airline + " Error Message " + ex.ToString() + ex.StackTrace, "");
                    }

                    //Caculate Output VAT Amount
                    if (mResult[i].Price.TaxDetails != null && mResult[i].Price.TaxDetails.OutputVAT != null)
                    {
                        fare.OutputVATApplied = mResult[i].Price.TaxDetails.OutputVAT.Applied;
                        fare.OutputVATCharge = mResult[i].Price.TaxDetails.OutputVAT.Charge;
                        fare.OutputVATOn = mResult[i].Price.TaxDetails.OutputVAT.AppliedOn.ToString();
                    }


                    //WSUserPreference userPref = new WSUserPreference();
                    
                    //userPref.Load(UserMasterId);                    
                    
                    result.Origin = origin;
                    result.Destination = destination;
                    result.BaggageIncludedInFare = mResult[i].BaggageIncludedInFare;//Assign UAPI Baggage info from search result.
                    result.FareType = mResult[i].FareType;
                    result.GUID = mResult[i].GUID;
                    if (mResult[i].ResultBookingSource == BookingSource.AirIndiaExpressIntl)
                    {
                        result.FareInformationId = new List<int>();
                        foreach (KeyValuePair<string, List<int>> pair in mResult[i].FareInformationId)
                        {
                            result.FareInformationId.AddRange(pair.Value);
                        }
                    }
                    if (mResult[i].ResultBookingSource == BookingSource.FlyDubai)
                    {
                        //result.BaggageIncluded = new SerializableDictionary<long, bool>();
                        //if (mResult[i].BaggageIncluded != null)
                        //{
                        //    foreach (KeyValuePair<long, bool> pair in mResult[i].BaggageIncluded)
                        //    {
                        //        result.BaggageIncluded.Add(pair.Key, pair.Value);
                        //    }
                        //}
                        //result.BaggageIncludedInFare = mResult[i].BaggageIncludedInFare;
                        //result.FareInformationId = new List<int>();
                        //foreach (KeyValuePair<string, List<int>> pair in mResult[i].FareInformationId)
                        //{ 
                        //    result.FareInformationId.Add(pair.Value);
                        //}
                        
                        result.IsBaggageIncluded = mResult[i].IsBaggageIncluded;
                    }
                    
                    if (mResult[i].ResultBookingSource == BookingSource.UAPI)// TO pass AirPricing Details to get Farequote while creating reservation
                    {
                        result.UapiPricingSolution = new UAPIdll.Air46.AirPricingSolution(); //mResult[i].UapiPricingSolution;
                        PropertiesCopier.CopyProperties(mResult[i].UapiPricingSolution, result.UapiPricingSolution);
                        PropertiesCopier.CheckRecursiveDifferentTypes(mResult[i].UapiPricingSolution, result.UapiPricingSolution);
                        
                    }
                    if (mResult[i].NonRefundable)
                    {
                        result.IsRefundable = false;
                    }
                    else
                    {
                        result.IsRefundable = true;
                    }

                    result.IsGSTMandatory = mResult[i].IsGSTMandatory;

                    #region Old unwanted code
                    //decimal totalAirlineTransFee = 0;
                    //decimal totalAdditionalTransactionFee = 0;
                    //paxCount = 0;
                    //int totalPaxCount = 0;
                    //int paxCountForServiceFee = 0;
                    //for (int j = 0; j < mResult[i].FareBreakdown.Length; j++)
                    //{
                    //    if (mResult[i].FareBreakdown[j].PassengerType != PassengerType.Infant)
                    //    {
                    //        paxCount += mResult[i].FareBreakdown[j].PassengerCount;
                    //    }
                    //    totalPaxCount += mResult[i].FareBreakdown[j].PassengerCount;
                    //    paxCountForServiceFee += mResult[i].FareBreakdown[j].PassengerCount;
                    //    if (mResult[i].FareBreakdown[j].PassengerType == PassengerType.Infant)
                    //    {
                    //        if (mResult[i].ResultBookingSource != BookingSource.WorldSpan && mResult[i].ResultBookingSource != BookingSource.Amadeus && mResult[i].ResultBookingSource != BookingSource.Galileo && mResult[i].ResultBookingSource != BookingSource.UAPI)
                    //        {
                    //            paxCountForServiceFee -= mResult[i].FareBreakdown[j].PassengerCount;
                    //        }
                    //    }
                    //}
                    //for (int j = 0; j < mResult[i].FareBreakdown.Length; j++)
                    //{
                    //    result.FareBreakdown[j] = new WSPTCFare();
                    //    result.FareBreakdown[j].PassengerType = mResult[i].FareBreakdown[j].PassengerType;
                    //    result.FareBreakdown[j].PassengerCount = mResult[i].FareBreakdown[j].PassengerCount;
                    //    result.FareBreakdown[j].B2CMarkup = mResult[i].FareBreakdown[j].B2CMarkup;
                    //    result.FareBreakdown[j].Markup = mResult[i].FareBreakdown[j].AgentMarkup;
                    //    result.FareBreakdown[j].SupplierCurrency = mResult[i].Price.SupplierCurrency;
                    //    result.FareBreakdown[j].SupplierPrice = (decimal)mResult[i].FareBreakdown[j].SupplierFare / mResult[i].FareBreakdown[j].PassengerCount;
                    //    //Agent commission not there in BookingEngine-Fare.
                    //    //result.FareBreakdown[j].AgentCommission = (decimal)mResult[i].FareBreakdown[j];
                    //    result.FareBreakdown[j].AdditionalTxnFee = mResult[i].FareBreakdown[j].AdditionalTxnFee;
                    //    result.FareBreakdown[j].BaseFare = (decimal)mResult[i].FareBreakdown[j].BaseFare + (mResult[i].FareBreakdown[j].HandlingFee / mResult[i].FareBreakdown[j].PassengerCount);
                    //    result.FareBreakdown[j].AirlineTransFee = mResult[i].FareBreakdown[j].AirlineTransFee;
                    //    result.FareBreakdown[j].Tax = (decimal)(mResult[i].FareBreakdown[j].TotalFare - mResult[i].FareBreakdown[j].BaseFare);
                    //    if (mResult[i].Price.YQTax > 0 && mResult[i].FareBreakdown[j].PassengerType != PassengerType.Infant)
                    //    {
                    //        result.FareBreakdown[j].FuelSurcharge = mResult[i].Price.YQTax * mResult[i].FareBreakdown[j].PassengerCount / paxCount;
                    //    }
                    //    else
                    //    {
                    //        result.FareBreakdown[j].FuelSurcharge = 0;
                    //    }
                    //    totalAirlineTransFee += mResult[i].FareBreakdown[j].AirlineTransFee;
                    //    # region Calculation of service fee for WL customer on the basis of preferences
                    //    //if (authDta.SiteName == null || authDta.SiteName.Trim() == string.Empty)
                    //    //{
                    //    //    result.FareBreakdown[j].AdditionalTxnFee = mResult[i].FareBreakdown[j].AdditionalTxnFee;
                    //    //    totalAdditionalTransactionFee += mResult[i].FareBreakdown[j].AdditionalTxnFee;
                    //    //}
                    //    //else if (userPref.AdditionalTxnFee != null && userPref.AdditionalTxnFee.ToUpper() == "TRUE")
                    //    //{
                    //    //    result.FareBreakdown[j].AdditionalTxnFee = mResult[i].FareBreakdown[j].AdditionalTxnFee;
                    //    //    totalAdditionalTransactionFee += mResult[i].FareBreakdown[j].AdditionalTxnFee;
                    //    //}
                    //    //else
                    //    //{
                    //    //    result.FareBreakdown[j].AdditionalTxnFee = 0;
                    //    //    totalAdditionalTransactionFee = 0;
                    //    //}
                    //    # endregion
                    //    //paxCount += result.FareBreakdown[j].PassengerCount;
                    //}
                    //decimal otherCharges = 0;
                    //decimal serviceCharge = 0;
                    //if (authDta.SiteName != null && authDta.SiteName.Trim() != "")
                    //{
                    //    if (mResult[i].Price.ChargeBU != null && mResult[i].Price.ChargeBU.Count > 0)
                    //    {
                    //        fare.ChargeBU = new ChargeBreakUp[mResult[i].Price.ChargeBU.Count];
                    //        for (int k = 0; k < mResult[i].Price.ChargeBU.Count; k++)
                    //        {
                    //            fare.ChargeBU[k] = mResult[i].Price.ChargeBU[k];
                    //            if (mResult[i].Price.ChargeBU[k].ChargeType == ChargeType.CreditCardCharge || mResult[i].Price.ChargeBU[k].ChargeType == ChargeType.OtherCharges)
                    //            {
                    //                otherCharges += mResult[i].Price.ChargeBU[k].Amount;
                    //            }
                    //        }
                    //        otherCharges += (serviceCharge * totalPaxCount);
                    //    }

                    //    UserPreference usPref = new UserPreference();
                    //    if (mResult[i].Flights[0][0].Origin.CountryCode == "IN" && mResult[i].Flights[0][mResult[i].Flights[0].Length - 1].Destination.CountryCode == "IN")
                    //    {
                    //        //AirlineServiceFee asf = AirlineServiceFee.Load(mResult[i].Flights[0][0].Airline, AgentMasterId);
                    //        //int chargeType = 1; // 1 is for fixed type.
                    //        //decimal chargeValue = 0;
                    //        //if (asf.AirlineCode != null && asf.AirlineCode != "" && asf.IsActive)
                    //        //{
                    //        //    chargeType = Convert.ToInt32(asf.ServiceFeeType);
                    //        //    chargeValue = asf.ServiceFee;
                    //        //}
                    //        //else
                    //        {
                    //            //usPref = usPref.GetPreference(UserMasterId, WLPreferenceKeys.ServiceChargeType, "Booking & Confirmation");
                    //            //if (usPref != null && usPref.Value != null && usPref.Value.Trim() != string.Empty)
                    //            //{
                    //            //    chargeType = Convert.ToInt32(usPref.Value);
                    //            //}
                    //            //usPref = new UserPreference();
                    //            //usPref = usPref.GetPreference(UserMasterId, WLPreferenceKeys.ServiceCharge, "Booking & Confirmation");
                    //            //if (usPref != null && usPref.Value != null && usPref.Value.Trim() != string.Empty)
                    //            //{
                    //            //    chargeValue = Convert.ToDecimal(usPref.Value);
                    //            //}
                    //        }
                    //        //if (chargeType == 1)
                    //        //{
                    //        //    fare.OtherCharges += chargeValue * paxCountForServiceFee;
                    //        //}
                    //        //else
                    //        //{
                    //        //    fare.OtherCharges += (fare.BaseFare + fare.Tax) * (chargeValue / 100);
                    //        //}

                    //    }
                    //    else
                    //    {
                    //        //usPref = usPref.GetPreference(UserMasterId, WLPreferenceKeys.IntlServiceChargeType, "Booking & Confirmation");
                    //        //int chargeType = 1; // 1 is for fixed type.
                    //        //if (usPref != null && usPref.Value != null && usPref.Value.Trim() != string.Empty)
                    //        //{
                    //        //    chargeType = Convert.ToInt32(usPref.Value);
                    //        //}
                    //        //usPref = new UserPreference();
                    //        //usPref = usPref.GetPreference(UserMasterId, WLPreferenceKeys.IntlServiceCharge, "Booking & Confirmation");
                    //        //decimal chargeValue = 0;
                    //        //if (usPref != null && usPref.Value != null && usPref.Value.Trim() != string.Empty)
                    //        //{
                    //        //    chargeValue = Convert.ToDecimal(usPref.Value);
                    //        //}
                    //        //if (chargeType == 1)
                    //        //{
                    //        //    fare.OtherCharges += chargeValue * paxCountForServiceFee;
                    //        //}
                    //        //else
                    //        //{
                    //        //    fare.OtherCharges += (fare.BaseFare + fare.Tax) * (chargeValue / 100);
                    //        //}
                    //    }
                    //    //if (commissionList.FindIndex( )
                    //    //AirlineCommission com = commissionList.Find(delegate(AirlineCommission commission)
                    //    //{
                    //    //    return commission.AirlineCode == mResult[i].Airline;
                    //    //}
                    //    //);
                    //    //if (com != null && com.AirlineCode != null && com.Commission > 0)
                    //    //{
                    //    //    fare.Discount = Math.Round(fare.AgentCommission * com.Commission / 100);
                    //    //}
                    //}
                    //else
                    //{
                    //    if (mResult[i].Price.ChargeBU != null && mResult[i].Price.ChargeBU.Count > 0)
                    //    {
                    //        fare.ChargeBU = new ChargeBreakUp[mResult[i].Price.ChargeBU.Count];
                    //        for (int k = 0; k < mResult[i].Price.ChargeBU.Count; k++)
                    //        {
                    //            fare.ChargeBU[k] = mResult[i].Price.ChargeBU[k];
                    //            if (mResult[i].Price.ChargeBU[k].ChargeType == ChargeType.OtherCharges)
                    //            {
                    //                otherCharges += mResult[i].Price.ChargeBU[k].Amount;
                    //            }
                    //        }
                    //    }
                    //}
                    ////fare.OtherCharges += otherCharges;
                    ////fare.AirTransFee = totalAirlineTransFee;
                    //if (mResult[i].Price.YQTax > 0)
                    //{
                    //    fare.FuelSurcharge = mResult[i].Price.YQTax;
                    //}
                    //else
                    //{
                    //    fare.FuelSurcharge = 0;
                    //}
                    //fare.AdditionalTxnFee = totalAdditionalTransactionFee; 
                    #endregion
                    result.Fare = fare;

                    #region Building segments of the result

                    FlightInfo[] mSegment = SearchResult.GetSegments(mResult[i]);
                    result.Segment = new WSSegment[mSegment.Length];
                    for (int j = 0; j < mSegment.Length; j++)
                    {
                        result.Segment[j] = WSSegment.ReadSegment(mSegment[j]);
                    }
                    TimeSpan Duration = new TimeSpan();
                    TimeSpan ibDuration = new TimeSpan();
                    for (int j = 0, l = 0; j < mResult[i].Flights.Length; j++)
                    {
                        result.ObSegCount = mResult[i].Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0 ? 0 : mResult[i].Flights[0].Select(p => p.FlightNumber).Distinct().Count();
                        if(mResult[i].Flights.Length>1)
                           result.IbSegCount = mResult[i].Flights[1].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0 ? 0 : mResult[i].Flights[1].Select(p => p.FlightNumber).Distinct().Count();
                        for (int k = 0; k < mResult[i].Flights[j].Length; k++, l++)
                        {
                            result.Segment[l] = WSSegment.ReadSegment(mResult[i].Flights[j][k]);
                            result.Segment[l].SegmentIndicator = j + 1;
                            if ((mResult[i].ResultBookingSource == BookingSource.Indigo || mResult[i].ResultBookingSource == BookingSource.SpiceJet || mResult[i].ResultBookingSource == BookingSource.GoAir) && !string.IsNullOrEmpty(mResult[i].BaggageIncludedInFare))
                            {
                                if (mResult[i].Flights[j].Length > 1)
                                {
                                    result.Segment[l].Defaultbaggage = (!string.IsNullOrEmpty(mResult[i].BaggageIncludedInFare.Split('|')[j].Split(',')[k]) && mResult[i].BaggageIncludedInFare.Split('|')[j].Split(',')[k] != "0" ? mResult[i].BaggageIncludedInFare.Split('|')[j].Split(',')[k] : "0");
                                }
                                else
                                    result.Segment[l].Defaultbaggage = ((mResult[i].BaggageIncludedInFare.Split('|').Length > k && !string.IsNullOrEmpty(mResult[i].BaggageIncludedInFare.Split('|')[j]) && mResult[i].BaggageIncludedInFare.Split('|')[j] != "0") ? mResult[i].BaggageIncludedInFare.Split('|')[j] : "0");
                            }

                            if (mResult[i].ResultBookingSource == BookingSource.Sabre) //added by bangar 07-04-2021
                                result.Segment[l].Defaultbaggage = !string.IsNullOrEmpty(mResult[i].Flights[j][k].DefaultBaggage) ? mResult[i].Flights[j][k].DefaultBaggage : "As PerAirline Norms";


                            //Correcting Duration calculation as per local time zones. Done by Shiva 12 June 2018
                            //Adding durations to get total duration since we are already calculating according to local time zones in MSE
                            if (j == 0)
                            {
                                
                                //searchResult.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0
                                //result.ObSegCount++;
                                Duration = Duration.Add(mResult[i].Flights[j][k].Duration);
                                result.ObDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                                //result.ObDuration = (Duration.Days * 24 + Duration.Hours).ToString("00:") + Duration.Minutes.ToString("00");                               
                                if (!string.IsNullOrEmpty(mResult[i].FareType))
                                    result.Segment[l].SegmentFareType = mResult[i].FareType.Split(',')[0];
                            }
                            else if (j == 1)
                            {
                                
                                //result.IbSegCount++;
                                ibDuration = ibDuration.Add(mResult[i].Flights[j][k].Duration);
                                result.IbDuration = (ibDuration.Days >= 1 ? Math.Floor(ibDuration.TotalHours).ToString() : (ibDuration.Hours.ToString().Length > 1 ? ibDuration.Hours.ToString() : "0" + ibDuration.Hours)) + ":" + (ibDuration.Minutes.ToString().Length > 1 ? ibDuration.Minutes.ToString() : "0" + ibDuration.Minutes);
                                //result.IbDuration = (ibDuration.Days * 24 + ibDuration.Hours).ToString("00:") + ibDuration.Minutes.ToString("00");
                                if(mResult[i].ResultBookingSource == BookingSource.TBOAir)
                                    result.Segment[l].SegmentFareType = mResult[i].FareType;
                                else if (!string.IsNullOrEmpty(mResult[i].FareType) && mResult[i].FareType.Split(',').Length > 1)
                                    result.Segment[l].SegmentFareType = mResult[i].FareType.Split(',')[1];
                            }
                        }
                    }
                    //if (result.ObSegCount > 0)
                    //{
                    //    TimeSpan tOut = mSegment[result.ObSegCount - 1].AccumulatedDuration;
                    //    result.ObDuration = (tOut.Days * 24 + tOut.Hours).ToString("00:") + tOut.Minutes.ToString("00");
                        
                    //    //TODO: 
                    //}
                    //if (result.IbSegCount > 0)
                    //{
                    //    TimeSpan tIn = mSegment[result.IbSegCount + result.ObSegCount - 1].AccumulatedDuration;
                    //    result.IbDuration = (tIn.Days * 24 + tIn.Hours).ToString("00:") + tIn.Minutes.ToString("00");
                    //}
                    #endregion

                    result.Source = mResult[i].ResultBookingSource;
                    List<FareRule> fareRule = new List<FareRule>();
                    result.IsLcc = mResult[i].IsLCC;//Set IsLCC from the result
                    if (mResult[i].ResultBookingSource == BookingSource.WorldSpan || mResult[i].ResultBookingSource == BookingSource.Amadeus || mResult[i].ResultBookingSource == BookingSource.Galileo || mResult[i].ResultBookingSource == BookingSource.UAPI)
                    {
                        result.IsLcc = false;
                    }                    
                    if (mResult[i].FareRules != null)
                    {
                        fareRule = mResult[i].FareRules;
                        result.FareBasis = new string[fareRule.Count];
                        counter = 0;
                        if (result.IsLcc)
                        {
                            for (int j = 0; j < fareRule.Count; j++)
                            {
                                result.FareBasis[j] = mResult[i].Flights[0][0].Airline + mResult[i].Flights[0][0].Origin.CityCode + destination + "-" + fareRule[j].FareBasisCode;
                            }
                        }
                        else
                        {
                            for (int j = 0; j < mResult[i].Flights.Length; j++)
                            {
                                for (int k = 0; k < mResult[i].Flights[j].Length; k++)
                                {
                                    if (counter < fareRule.Count)
                                    {
                                        if (fareRule[counter].FareInfoRef != null && fareRule[counter].FareInfoRef.Length > 0)
                                        {
                                            //result.FareBasis[counter] = mResult[i].Flights[j][k].Airline + mResult[i].Flights[j][k].Origin.CityCode + mResult[i].Flights[j][k].Destination.CityCode + "-" + fareRule[counter].FareBasisCode + "-" + Convert.ToString(fareRule[counter].FareInfoRef) + "-" + fareRule[counter].FareRuleKeyValue;
                                            result.FareBasis[counter] = mResult[i].Flights[j][k].Airline + mResult[i].Flights[j][k].Origin.CityCode + mResult[i].Flights[j][k].Destination.CityCode + "~" + fareRule[counter].FareBasisCode + "~" + Convert.ToString(fareRule[counter].FareInfoRef) + "~" + fareRule[counter].FareRuleKeyValue;
                                        }
                                        else
                                        {
                                            //result.FareBasis[counter] = mResult[i].Flights[j][k].Airline + mResult[i].Flights[j][k].Origin.CityCode + mResult[i].Flights[j][k].Destination.CityCode + "-" + fareRule[counter].FareBasisCode;
                                            result.FareBasis[counter] = mResult[i].Flights[j][k].Airline + mResult[i].Flights[j][k].Origin.CityCode + mResult[i].Flights[j][k].Destination.CityCode + "~" + fareRule[counter].FareBasisCode;
                                        }
                                        counter = counter + 1;
                                    }
                                }
                            }
                        }
                    }
                    //else if (mResult[i].ResultBookingSource == BookingSource.Paramount)
                    //{
                    //    ParamountSearchResult pmRes = new ParamountSearchResult();
                    //    pmRes = (ParamountSearchResult)mResult[i];
                    //    result.FareBasis = new string[1];
                    //    result.FareBasis[0] = "I7" + mResult[i].Flights[0][0].Origin.CityCode + mResult[i].Flights[0][0].Destination.CityCode + "-" + pmRes.FareCode;
                    //}
                    //else if (mResult[i].ResultBookingSource == BookingSource.Mdlr)
                    //{
                    //    MdlrSearchResult pmRes = new MdlrSearchResult();
                    //    pmRes = (MdlrSearchResult)mResult[i];
                    //    result.FareBasis = new string[1];
                    //    result.FareBasis[0] = "9H" + mResult[i].Flights[0][0].Origin.CityCode + mResult[i].Flights[0][0].Destination.CityCode + "-" + pmRes.FareCode;

                    //}
                    //else if (mResult[i].ResultBookingSource == BookingSource.SpiceJet)
                    //{
                    //    SpiceJetSearchResult sjRes = new SpiceJetSearchResult();
                    //    sjRes = (SpiceJetSearchResult)mResult[i];
                    //    fareRule = SpiceJetAPI.GetFareRule(mResult[i].Flights[0][0].Origin.CityCode, mResult[i].Flights[0][0].Destination.CityCode, sjRes.SelectedFareBasisCode[0]);
                    //    result.FareBasis = new string[1];
                    //    result.FareBasis[0] = fareRule[0].Airline + origin + destination + "-" + fareRule[0].FareBasisCode;
                    //}
                    //else if (mResult[i].ResultBookingSource == BookingSource.Indigo)
                    //{
                    //    NavitaireSearchResult sjRes = new NavitaireSearchResult();
                    //    sjRes = (NavitaireSearchResult)mResult[i];
                    //    fareRule = Navitaire.GetFareRule(mResult[i].Flights[0][0].Origin.CityCode, mResult[i].Flights[0][0].Destination.CityCode, sjRes.SelectedFareBasisCode[0], "6E");
                    //    result.FareBasis = new string[1];
                    //    result.FareBasis[0] = fareRule[0].Airline + origin + destination + "-" + fareRule[0].FareBasisCode;
                    //}
                    //else if(mResult[i].ResultBookingSource == BookingSource.GoAir)
                    //{
                    //    GoAirSearchResult adRes = new GoAirSearchResult();
                    //    adRes = (GoAirSearchResult)mResult[i];
                    //    fareRule = GoAirApi.GetFareRule(mResult[i].Flights[0][0].Origin.CityCode, mResult[i].Flights[0][0].Destination.CityCode, adRes.FareRules[0].FareBasisCode);
                    //    result.FareBasis = new string[1];
                    //    result.FareBasis[0] = fareRule[0].Airline + origin + destination + "-" + fareRule[0].FareBasisCode;
                    //}
                    else if (mResult[i].ResultBookingSource == BookingSource.AirArabia)
                    {

                        fareRule = AirArabia.GetFareRule(mResult[i].Flights[0][0].Origin.CityCode, mResult[i].Flights[0][0].Destination.CityCode, "P");
                        result.FareBasis = new string[1];
                        result.FareBasis[0] = fareRule[0].Airline + origin + destination + "-P";
                    }

                    resultList.Add(result);


                    ////// TEst Ziyad remove

                    //System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSResult));
                    //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    //System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    //ser.Serialize(writer, result);  // Here Classes are converted to XML String. 
                    //                                // This can be viewed in SB or writer.
                    //                                // Above XML in SB can be loaded in XmlDocument object                
                    //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    //doc.LoadXml(sb.ToString());
                    ////doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchReq.xml");
                    //string filePath = @"D:\XMLLogs\Test\ObjectRead\Finaltest" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    //doc.Save(filePath);

                    //// end test
                }
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.Exception, Severity.High, UserMasterId, ex.ToString(), "");
            }
            finally
            {
                Audit.Add(EventType.Search, Severity.Low, 1, "BookingAPI Build results Exited", "");
            }

        }
        
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]    
    public WSSearchResponse Search(WSSearchRequest request)
    {
        WSSearch wSearch = new WSSearch();
        UserPreference pref = new UserPreference();
        int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
        wSearch.AgentLoginInfo = UserMaster.GetB2CUser(UserId);
        request.MaxStops = string.Empty;
        WSSearchResponse response = wSearch.Search(request, Credential, this.Context.Request.UserHostAddress);
        response.SabreAuthenticateToken = wSearch.SabreAuthenticateToken;
        
        SabreAuthenticateToken = wSearch.SabreAuthenticateToken;
       
        //Audit.Add(EventType.Search, Severity.Low, UserId, "(BookingAPI)Flight Search Results found "+response.Result.Length, this.Context.Request.UserHostAddress);
        try
        {
            //For viewing the xml result response. 
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(response.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, response);  // Here Classes are converted to XML String. 
            // This can be viewed in SB or writer.
            // Above XML in SB can be loaded in XmlDocument object
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
            string filePath = @"c:\XMLlogs\B2C\";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            doc.Save(Path.Combine(filePath, "FlightSearchResultResponse" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml"));
        }
        catch { }
        response.ErrorMsg = "";
        
      

        return response;
    }

    private bool WSBookInput(WSBookRequest bookRequest)
    {
        if (bookRequest.SessionId == null)
        {
            statusDescription = "Session Id cannot be blank or null";
            statusCode = "08";
            return false;
        }
        if (bookRequest.SessionId != null && bookRequest.SessionId.Contains(","))
        {
            statusDescription = "separte sessionId from comma and pass only one seesionId in request";
            statusCode = "08";
            return false;
        }
        if (bookRequest.Destination == null || bookRequest.Destination == "")
        {
            statusDescription = "Destination is null";
            statusCode = "09";
            return false;
        }
        if (bookRequest.Fare == null)
        {
            statusDescription = "Fare was null";
            statusCode = "10";
            return false;
        }
        if (bookRequest.Origin == null || bookRequest.Origin == "")
        {
            statusDescription = "Origin was blank or null";
            statusCode = "11";
            return false;
        }
        if (bookRequest.Passenger == null)
        {
            statusDescription = "Passengers was null";
            statusCode = "12";
            return false;
        }
        if (bookRequest.Passenger.Length <= 0)
        {
            statusDescription = "Passenger length is zero";
            statusCode = "12";
            return false;
        }
        foreach (WSPassenger passenger in bookRequest.Passenger)
        {
            if (passenger.Fare == null)
            {
                statusDescription = "Fare of passenger can't be null";
                statusCode = "10";
                return false;
            }
        }
        if (bookRequest.Passenger.Length >= 15)
        {
            statusDescription = "Passenger length can't be more then 15";
            statusCode = "12";
            return false;
        }
        if (bookRequest.Segment == null)
        {
            statusDescription = "Segments was null";
            statusCode = "13";
            return false;
        }
        return true;
    }

    [WebMethod(EnableSession=true)]
    [SoapHeader("Credential")]
    public WSBookResponse Book(WSBookRequest bookRequest)
    {
        CreditCardPaymentInformation cardInfo = new CreditCardPaymentInformation();
        UserPreference pref = new UserPreference();
        int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
        AgentLoginInfo = UserMaster.GetB2CUser(UserId);


        /**************************** Start :Book Method Audit records for Wego Portal ***********/


        Audit.Add(EventType.Search, Severity.High, UserId, "(" + Credential.SiteName + ")  Booking in progress.Agent Id :" + AgentLoginInfo.AgentId + "Session Id :" + bookRequest.SessionId, "", bookRequest.Source.ToString());

        /**************************** End :Book Method Audit records for Wego Portal ***********/

        #region Assign credit card Information if passed
        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
        {
            cardInfo.Amount = bookRequest.PaymentInformation.Amount;
            cardInfo.PaymentGateway = (CT.AccountingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.AccountingEngine.PaymentGatewaySource), bookRequest.PaymentInformation.PaymentGateway.ToString());
            cardInfo.PaymentId = bookRequest.PaymentInformation.PaymentId;
            cardInfo.TrackId = bookRequest.PaymentInformation.TrackId;
            cardInfo.IPAddress = bookRequest.PaymentInformation.IPAddress;
            cardInfo.Charges = bookRequest.PaymentInformation.Charges;
        }
        #endregion

        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);

        //Check for authentication
        if (userMaster != null)
        {
            string ipAddr = this.Context.Request.UserHostAddress;

            # region Validating input parameter
            // Check for Input parameter validation
            if (!(WSBookInput(bookRequest)))
            {
                WSBookResponse errorResponse = new WSBookResponse();
                WSStatus status = new WSStatus();
                status.Category = "BK";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                errorResponse.ProdType = ProductType.Flight;
                Audit.AddAuditBookingAPI(EventType.Book, Severity.High, (int)userMaster.ID, "Input parameter is incorrect: " + statusDescription, ipAddr);
                //Save Credit Card Info
                if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
                {
                    cardInfo.Remarks = "Payment Successful";
                    cardInfo.ReferenceId = 0;
                    cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                    cardInfo.TransType = "B2C";
                    cardInfo.Save();
                    
                }
                return errorResponse;
            }
            # endregion

            # region Check the booking rights for user
            
            //if (!Role.IsAllowedTask(UserMaster.RoleId, (int)Task.MakeFlightBookings))
            //{
            //    WSBookResponse errorResponse = new WSBookResponse();
            //    WSStatus status = new WSStatus();
            //    status.Category = "BK";
            //    status.Description = "User is not Authorized for booking";
            //    status.StatusCode = "30";
            //    errorResponse.Status = status;
            //    errorResponse.ProdType = ProductType.Flight;
            //    Audit.AddAuditBookingAPI(EventType.Book, Severity.High, (int)UserMaster.ID, "UserMaster is not Authorized for booking. Name :" + UserMaster.FirstName + " "+UserMaster.LastName + " Login Name :" + UserMaster.LoginName, ipAddr);
            //    return errorResponse;
            //}
            # endregion

            #region Check for LCC airline
            bool isLcc = false;
            if (bookRequest.Source != BookingSource.Amadeus && bookRequest.Source != BookingSource.Sabre && bookRequest.Source != BookingSource.Galileo && bookRequest.Source != BookingSource.WorldSpan && bookRequest.Source != BookingSource.UAPI)
            {
                isLcc = true;
            }
            isLcc = bookRequest.IsLCC;//Check whether the itinerary is LCC irrespective of Source
            if (isLcc)
            {
                WSBookResponse bookResponse = new WSBookResponse();
                WSStatus status = new WSStatus();
                status.Category = "BK";
                status.Description = "LCC airline cannot be booked";
                status.StatusCode = "07";
                bookResponse.Status = status;
                bookResponse.ProdType = ProductType.Flight;
                Audit.AddAuditBookingAPI(EventType.Book, Severity.High, (int)userMaster.ID, "LCC airline cannot be booked: airlinecode = " + bookRequest.Segment[0].Airline.AirlineCode, ipAddr);
                //if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
                //{
                //    cardInfo.ReferenceId = 0;
                //    cardInfo.ReferenceType = ReferenceIdType.Invoice;
                //    cardInfo.Save();
                //}
                return bookResponse;
            }
            #endregion

            //Booking of Non LCC airline starts
            else
            {
                WSBookResponse bookResponse = new WSBookResponse();
                WSStatus status = new WSStatus();

                //assigning AgentMaster id of agents and staff(on behalf of TBO)
                int AgentMasterId;
                if (userMaster.AgentId > 0)
                {
                    AgentMasterId = userMaster.AgentId;
                }
                else
                {
                    ConfigurationSystem config = new ConfigurationSystem();
                    AgentMasterId = AgentLoginInfo.AgentId; //Convert.ToInt32(config.GetSelfAgencyId()["selfAgentMasterId"]);
                }

                # region Building itinerary for MSE

                FlightItinerary itinerary = new FlightItinerary();
                try
                {
                    itinerary = BuildItinerary(bookRequest, (int)userMaster.ID, AgentMasterId);
                }
                catch (ArgumentException ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "12";
                    status.Description = ex.Message;
                    bookResponse.Status = status;
                    bookResponse.ProdType = ProductType.Flight;
                    Audit.Add(EventType.Book, Severity.High, UserId, "(" + Credential.SiteName + ")Book Method(Failed to build the itinerary for MSE) Error:" + ex.ToString(), ipAddr, Convert.ToString(bookRequest.Source));
                    return bookResponse;
                }
                catch (Exception ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "15";
                    status.Description = "Technical Problem 2";
                    bookResponse.Status = status;
                    bookResponse.ProdType = ProductType.Flight;
                    SendErrorMail(" while building itinerary for MSE", userMaster, ex, bookRequest);


                    Audit.Add(EventType.Book, Severity.High, UserId, "(" + Credential.SiteName + ")Book Method(while building itinerary for MSE) Error:" + ex.ToString(), ipAddr, Convert.ToString(bookRequest.Source));


                    //Save Credit Card Info
                    if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
                    {
                        cardInfo.Remarks = "Payment Successful";
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                        cardInfo.TransType = "B2C";
                        cardInfo.Save();
                        bookResponse.ConfirmationNo = cardInfo.PaymentInformationId.ToString();//Update paymentinfoid
                    }
                    return bookResponse;
                }

                #endregion

                #region Call MSE book method

                if (!MetaSearchEngine.IsActiveSession(bookRequest.SessionId))
                {
                    MetaSearchEngine mseNew = new MetaSearchEngine();
                    string sessionId = mseNew.StartSession();
                    bookRequest.SessionId = sessionId;
                    Audit.Add(EventType.Search, Severity.High, UserId, "(" + Credential.SiteName + ") New sessionId generated:" + sessionId, ipAddr, Convert.ToString(bookRequest.Source));


                }

                MetaSearchEngine mse = new MetaSearchEngine(bookRequest.SessionId);
                BookingResponse mseBookingResponse;
                try
                {   
                    mse.SettingsLoginInfo = AgentLoginInfo;
                    mse.SabreAuthenicateToken = SabreAuthenticateToken;
                    mseBookingResponse = mse.Book(ref itinerary, AgentMasterId, BookingStatus.Hold, userMaster, true, "");
                    if (mseBookingResponse.Error != null && mseBookingResponse.Error != "")
                    {
                        Audit.AddAuditBookingAPI(EventType.Book, Severity.High, (int)userMaster.ID, "Booking Response error: " + mseBookingResponse.Error, ipAddr);
                    }
                    bookResponse.ConfirmationNo = mseBookingResponse.ConfirmationNo;
                    bookResponse.PNR = mseBookingResponse.PNR;
                    bookResponse.ProdType = mseBookingResponse.ProdType;
                    bookResponse.SSRDenied = mseBookingResponse.SSRDenied;
                    bookResponse.SSRMessage = mseBookingResponse.SSRMessage;
                    bookResponse.ItineraryID = itinerary.FlightId;//For saving PromoDetail we need FlightId as a reference, this will be used in FlightBooking.aspx.cs WSAddBookingRequest.Add()
                    
                }               
                catch (Exception ex)
                {
                    mseBookingResponse = new BookingResponse(BookingResponseStatus.Failed, ex.ToString(), "");
                    CT.Core.Audit.Add(EventType.Book, Severity.High, UserId, "(" + Credential.SiteName + ") B2C-FlightBooking-Fail to book requested itinerary" + ex.ToString(), ipAddr, Convert.ToString(bookRequest.Source));
                    status.Category = "BK";
                    if (mseBookingResponse.Error.ToLower().Contains("timeout"))
                    {
                        mseBookingResponse.Error = "Time Out";
                    }
                    else if (mseBookingResponse.Error.ToLower().Contains("bookingId not generated"))
                    {
                        mseBookingResponse.Error = "Transaction Failure";
                    }
                    else if (mseBookingResponse.Error != null && mseBookingResponse.Error.Trim() != string.Empty)
                    {
                        mseBookingResponse.Error = mseBookingResponse.Error;
                    }
                    status.Description = mseBookingResponse.Error;
                    status.StatusCode = "02";
                    bookResponse.Status = status;
                    bookResponse.ProdType = ProductType.Flight;
                    SendErrorMail("while calling book method of MSE", userMaster, ex, bookRequest);
                    //Save Credit Card Info
                    if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
                    {
                        cardInfo.Remarks = "Payment Successful";
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                        cardInfo.TransType = "B2C";
                        cardInfo.Save();
                        bookResponse.ConfirmationNo = cardInfo.PaymentInformationId.ToString();//Update paymentinfoid
                    }
                    return bookResponse;
                }
                #endregion

                //Audit.Add(EventType.Search, Severity.High, 1, "Start:Update the Look To Book Ratio", "");


                //If the site name contains "wego" then we need to update the LookToBook Ratio Table
                if (!string.IsNullOrEmpty(Credential.SiteName) && Credential.SiteName.ToLower().Contains("wego"))
                {
                    try
                    {
                        bool isBooked = false;
                        string mseWegoBookingErrorMessage = string.Empty;
                        if (mseBookingResponse.Error != null && mseBookingResponse.Error != "")
                        {
                            mseWegoBookingErrorMessage = mseBookingResponse.Error;
                        }


                        if (mseBookingResponse.Status != BookingResponseStatus.Failed)
                        {
                            isBooked = true;
                        }
                        //Need to update against the same session id ,user id and agency id as isBooked true(Booking Hits)
                        mse.SaveBookingRatio("B2C", ProductType.Flight, string.Empty, bookRequest.SessionId, 0, UserId, AgentLoginInfo.AgentId, mseWegoBookingErrorMessage, Credential.SiteName, isBooked, mseBookingResponse.BookingId, Convert.ToString(bookRequest.Source));

                    }
                    catch (Exception ex)
                    {

                        Audit.Add(EventType.Book, Severity.High, UserId, "(" + Credential.SiteName + ") Fail to Update the Look To Book Ratio" + ex.ToString(), ipAddr, Convert.ToString(bookRequest.Source));

                    }
                }
                //Audit.Add(EventType.Search, Severity.High, 1, "End:Update the Look To Book Ratio", "");



                if (mseBookingResponse.Status != BookingResponseStatus.Failed)
                {
                    bookResponse.BookingId = mseBookingResponse.BookingId.ToString();
                }
                mseBookingResponse.ProdType = ProductType.Flight;
                bookResponse.ConfirmationNo = mseBookingResponse.ConfirmationNo;
                bookResponse.PNR = mseBookingResponse.PNR;
                bookResponse.ProdType = mseBookingResponse.ProdType;
                bookResponse.SSRDenied = mseBookingResponse.SSRDenied;
                bookResponse.SSRMessage = mseBookingResponse.SSRMessage;

                #region Assigning status of WSBookResponse

                bookResponse.Status = new WSStatus();
                bookResponse.Status.Description = mseBookingResponse.Status.ToString();
                if (mseBookingResponse.Error != null && mseBookingResponse.Error.Trim() != string.Empty)
                {
                    bookResponse.Status.Description += " Error:" + mseBookingResponse.Error;
                }
                bookResponse.Status.Category = "BK";
                if (bookResponse.Status.Description == BookingResponseStatus.BookedOther.ToString())
                {
                    bookResponse.Status.StatusCode = "03";
                    bookResponse.Status.Description = "Fare is not available at the time of booking";
                }
                else if (bookResponse.Status.Description == BookingResponseStatus.OtherClass.ToString())
                {
                    bookResponse.Status.StatusCode = "04";
                    bookResponse.Status.Description = "Fare Class is not available at the time of booking";
                }
                else if (bookResponse.Status.Description == BookingResponseStatus.OtherFare.ToString())
                {
                    bookResponse.Status.StatusCode = "05";
                    bookResponse.Status.Description = "Fare is not available at the time of booking";
                }
                else if (bookResponse.Status.Description == BookingResponseStatus.Successful.ToString())
                {
                    bookResponse.Status.StatusCode = "06";// Succesfully booked

                    //Save Credit Card Info
                    if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = Convert.ToInt32(bookResponse.BookingId);
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                        cardInfo.TransType = "B2C";
                        cardInfo.Save();
                        bookResponse.ConfirmationNo = cardInfo.PaymentInformationId.ToString();//Update paymentinfoid
                    }
                }
                //else if (bookResponse.Status.Description == BookingResponseStatus.ScheduleChanged.ToString())
                //{
                //    bookResponse.Status.StatusCode = "10";// Booked but Schedule changed for UAPI

                //}
                else if (bookResponse.Status.Description == BookingResponseStatus.Failed.ToString())
                {
                    bookResponse.Status.StatusCode = "02";
                    bookResponse.Status.Description = "Failed " + mseBookingResponse.Error;
                }

                if (mseBookingResponse.Error != null && mseBookingResponse.Error.Trim() != string.Empty)
                {
                    if (mseBookingResponse.Error.ToLower().Contains("sell failure"))
                    {
                        bookResponse.Status.Description = "Seat is not available at the time of booking";
                        bookResponse.Status.StatusCode = "25";
                    }
                    if (mseBookingResponse.Error.ToUpper().Contains("UNA 2 BK ALT- SEGMENTS"))
                    {
                        bookResponse.Status.Description = "Seat is not available at the time of booking";
                        bookResponse.Status.StatusCode = "25";
                    }
                    if (mseBookingResponse.Error.ToUpper().Contains("SEAT NOT AVAILABLE"))
                    {
                        bookResponse.Status.Description = "Seat is not available at the time of booking";
                        bookResponse.Status.StatusCode = "25";
                    }
                }
                if (bookResponse.Status.StatusCode != "06")
                {
                    SendErrorMail("Booking failed for GDS in B2C (" + Credential.SiteName + ")", userMaster, new Exception(bookResponse.Status.Description), bookRequest);
                }
                Audit.Add(EventType.Book, Severity.High, UserId, "(" + Credential.SiteName + ") B2C-Booking status:" + bookResponse.Status.Description + " " + mseBookingResponse.Error, ipAddr, Convert.ToString(bookRequest.Source));
                #endregion

                return bookResponse;
            }
        }
        else
        {
            WSBookResponse bookResponse = new WSBookResponse();
            bookResponse.Status.StatusCode = "01";
            bookResponse.Status.Description = "Authentication Failed";
            //Save Credit Card Info
            if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
            {
                cardInfo.Remarks = "Payment Successful";
                cardInfo.ReferenceId = 0;
                cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                cardInfo.TransType = "B2C";
                cardInfo.Save();
                bookResponse.ConfirmationNo = cardInfo.PaymentInformationId.ToString();//Update paymentinfoid
            }
            return bookResponse;
        }
    }

    private bool WSCancelItineraryInput(WSCancelItineraryRequest cancelRequest)
    {
        if (cancelRequest.BookingId == null)
        {
            statusDescription = "bookingId must be provided";
            statusCode = "01";
            return false;
        }
        else if (cancelRequest.Source != BookingSource.Amadeus && cancelRequest.Source != BookingSource.Sabre && cancelRequest.Source != BookingSource.Galileo && cancelRequest.Source != BookingSource.WorldSpan && cancelRequest.Source != BookingSource.UAPI)
        {
            statusDescription = "Only Non LCC booking can be cancelled";
            statusCode = "05";
            return false;
        }
        else
        {
            return true;
        }
    }

    [WebMethod(EnableSession=true)]
    [SoapHeader("Credential")]
    public WSCancelItineraryResponse CancelItinerary(WSCancelItineraryRequest cancelRequest)
    {
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        if (UserMaster != null)
        {
            string ipAddr = this.Context.Request.UserHostAddress;
            WSCancelItineraryResponse cancelResponse = new WSCancelItineraryResponse();
            WSStatus status = new WSStatus();
      
            if (!(WSCancelItineraryInput(cancelRequest)))
            {
                status.Category = "CN";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                cancelResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, (int)UserMaster.ID, "B2C-Invalid input : " + statusDescription, ipAddr);
                return cancelResponse;
            }
            string pnr = "";
            ///Getting PNR from Booking ID
            try
            {               
                
                pnr = FlightItinerary.GetPNRByFlightId(Product.GetProductIdByBookingId(Convert.ToInt32(cancelRequest.BookingId), ProductType.Flight)).Trim();
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, (int)UserMaster.ID, "B2C-BookingDetail.GetBookingIdByFlightId failed: " + ex.ToString(), ipAddr);
                status.Category = "CN";
                status.Description = "booking cannot be cancelled";
                status.StatusCode = "04";
                cancelResponse.Status = status;
                SendErrorMail("while CancelItinerary at the time of fetching PNR For BookingId: " + cancelRequest.BookingId, UserMaster, ex);
                return cancelResponse;
            }
            BookingDetail bookingDetails = new BookingDetail(Convert.ToInt32(cancelRequest.BookingId));
            if (bookingDetails.Status == BookingStatus.Ticketed)
            {
                status.Category = "CN";
                status.Description = "booking cannot be cancelled";
                status.StatusCode = "04";
                cancelResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, (int)UserMaster.ID, "B2C-Booking can't be cancelled because it is ticketed. BookingId " + cancelRequest.BookingId, ipAddr);
                return cancelResponse;
            }
            string cancelledPNR = "";
            BookingSource bookingSource;
            if (cancelRequest.Source == BookingSource.Amadeus)
            {
                bookingSource = BookingSource.Amadeus;
            }
            else if (cancelRequest.Source == BookingSource.Galileo)
            {
                bookingSource = BookingSource.Galileo;
            }
            else if (cancelRequest.Source == BookingSource.UAPI)
            {
                bookingSource = BookingSource.UAPI;// TO DO UAPI
            }
            else if (cancelRequest.Source == BookingSource.Sabre)
            {
                bookingSource = BookingSource.Sabre;// sabre
            }

            else
            {
                bookingSource = BookingSource.WorldSpan;
            }
            try
            {
                MetaSearchEngine mse = new MetaSearchEngine();
                cancelledPNR = mse.CancelItinerary(pnr, bookingSource);
            }
            catch (Exception ex)
            {
                status.Category = "CN";
                status.Description = ex.Message;
                status.StatusCode = "02";
                cancelResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, (int)UserMaster.ID, "B2C-Cancel Itinerary  failed : " + ex.ToString() + " Stack Trace : " + ex.ToString(), ipAddr);
                SendErrorMail("while CancelItinerary at the time of calling MSE method For PNR: " + pnr + " BookingId " + cancelRequest.BookingId, UserMaster, ex);
                return cancelResponse;
            }
            if (cancelledPNR == pnr)
            {
                status.Category = "CN";
                status.Description = "booking cancelled sucessfully";
                status.StatusCode = "03";
                cancelResponse.Status = status;
                cancelResponse.BookingId = cancelRequest.BookingId;
                // TODO : check the staus
                BookingDetail.SetBookingStatus(Convert.ToInt32(cancelRequest.BookingId), BookingStatus.Cancelled, (int)UserMaster.ID);
                CT.Core.Queue.SetStatus(QueueType.Booking, Convert.ToInt32(cancelRequest.BookingId), QueueStatus.Assigned, (int)UserMaster.ID, status.Description);
                BookingHistory bh = new BookingHistory();
                bh.BookingId = Convert.ToInt32(cancelRequest.BookingId);
                bh.EventCategory = EventCategory.Booking;
                bh.Remarks = "Booked seat are released pnr: " + pnr;
                bh.CreatedBy = (int)UserMaster.ID;
                bh.Save();
                Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.Normal, (int)UserMaster.ID, "B2C-Cancel Itinerary of PNR :" + cancelledPNR + " sucessfull : ", ipAddr);
            }
            else
            {
                status.Category = "CN";
                status.Description = "booking cannot be cancelled";
                status.StatusCode = "04";
                cancelResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, (int)UserMaster.ID, "B2C-Cancel Itinerary for Amadeus failed : " + cancelledPNR, ipAddr);
            }
            return cancelResponse;
        }
        else
        {
            WSCancelItineraryResponse cancelResponse = new WSCancelItineraryResponse();
            cancelResponse.Status = new WSStatus();
            cancelResponse.Status.StatusCode = "01";
            cancelResponse.Status.Description = "Authentication Failed";
            return cancelResponse;
        }
    }



    [WebMethod]
    [SoapHeader("Credential")]
    public WSUserPreference GetPreferences()
    {
        WSUserPreference userPref = new WSUserPreference();
        UserPreference pref = new UserPreference();
        int UserMasterId;
        try
        {
            
            UserMasterId = pref.GetMemberIdBySiteName(Credential.SiteName);
            AgentLoginInfo = UserMaster.GetB2CUser(UserMasterId);
            userPref.Load(UserMasterId);
            userPref.AgentCurrency = AgentLoginInfo.Currency;
            userPref.MemberId = UserMasterId;
            userPref.AgentDecimalPoint = AgentLoginInfo.DecimalValue;
            userPref.AgentEmail = AgentLoginInfo.AgentEmail;
            userPref.AgentID = AgentLoginInfo.AgentId;
            userPref.AgentPhoneNo = AgentLoginInfo.AgentPhone;
            userPref.AgentCountryCode = AgentLoginInfo.LocationCountryCode;
        }
        catch (Exception ex)
        {
            //Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Site Name is not valid: " + ex.Message, "");
            Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Site Name is not valid: " + ex.ToString(), "");
            throw new ArgumentException("Site Name is not valid");
        }
        //UserMaster UserMaster = new UserMaster(UserMasterId);
        
        return userPref;
    }

    //[WebMethod]
    //[SoapHeader("Credential")]
    //public WSUserPreference GetPreferencesbyProduct(ProductType prodType)
    //{
    //    WSUserPreference userPref = new WSUserPreference();
    //    UserPreference pref = new UserPreference();
    //    int UserMasterId;
    //    try
    //    {

    //        UserMasterId = pref.GetMemberIdBySiteName(Credential.SiteName);
    //        AgentLoginInfo = UserMaster.GetB2CUser(UserMasterId);
    //        switch (prodType)
    //        {
    //            case ProductType.Visa:
    //                userPref.Load(UserMasterId, ItemType.Visa);
    //                break;
    //        }
    //        userPref.AgentCurrency = AgentLoginInfo.Currency;
    //        userPref.AgentDecimalPoint = AgentLoginInfo.DecimalValue;
    //        userPref.AgentEmail = AgentLoginInfo.AgentEmail;
    //        userPref.AgentID = AgentLoginInfo.AgentId;
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Site Name is not valid: " + ex.Message, "");
    //        throw new ArgumentException("Site Name is not valid");
    //    }
    //    //UserMaster UserMaster = new UserMaster(UserMasterId);

    //    return userPref;
    //}

    private bool WSFareQuoteInput(WSGetFareQuoteRequest fareQuoteRequest)
    {
        if (fareQuoteRequest.Result == null)
        {
            statusDescription = "Result cannot be null";
            statusCode = "04";
            return false;
        }
        bool lccCheck = false;
        try
        {
       
            lccCheck = fareQuoteRequest.Result.IsLcc;
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.GetFareQuote, Severity.High, 0, "B2C-Airline.IsAirlineLCC(call 2nd) failed: " + ex.ToString() + ex.StackTrace, "");
        }
        if (!lccCheck)
        {
            statusDescription = "GetFareQuote handles only LCC airline";
            statusCode = "05";
            return false;
        }
        if (fareQuoteRequest.SessionId == null || fareQuoteRequest.SessionId == "")
        {
            statusDescription = "sessionId cannot be null or blank";
            statusCode = "06";
            return false;
        }
        if (fareQuoteRequest.SessionId != null && fareQuoteRequest.SessionId.Contains(","))
        {
            statusDescription = "separte sessionId from comma and pass only one seesionId in request";
            statusCode = "06";
            return false;
        }
        if (!MetaSearchEngine.IsActiveSession(fareQuoteRequest.SessionId))
        {
            statusDescription = "Your booking session is expired please start a new search.";
            statusCode = "06";
            return false;
        }
       
        return true;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetFareQuoteResponse GetFareQuote(WSGetFareQuoteRequest fareQuoteRequest)
    {
        UserPreference pref = new UserPreference();
        int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
        AgentLoginInfo = UserMaster.GetB2CUser(UserId);
        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);
        if (userMaster != null)
        {
            if (!(WSFareQuoteInput(fareQuoteRequest)))
            {
                WSGetFareQuoteResponse errorResponse = new WSGetFareQuoteResponse();
                WSStatus status = new WSStatus();
                status.Category = "FQ";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.GetFareQuote, Severity.High, (int)userMaster.ID, "B2C-Input parameter is incorrect: " + statusDescription + " Is LCC flight " + fareQuoteRequest.Result.IsLcc, "");
                return errorResponse;
            }
            string ipAddr = this.Context.Request.UserHostAddress;
            MetaSearchEngine mse = new MetaSearchEngine(fareQuoteRequest.SessionId);
            SearchResult res = new SearchResult();
            //res = fareQuoteRequest.Result.getResult();
            WSUserPreference userPref = new WSUserPreference();
            userPref.Load((int)userMaster.ID);
            Fare[] fare;
            WSGetFareQuoteResponse fareQuoteResponse = new WSGetFareQuoteResponse();
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            string sessionId = string.Empty;
            if (fareQuoteRequest.SessionId != null)
            {
                sessionId = fareQuoteRequest.SessionId;
            }
            try
            {
                fare = mse.GetFareQuote(res);
                res.FareBreakdown = fare;
                int AgentMasterId;
                if (userMaster.AgentId > 0)
                {
                    AgentMasterId = userMaster.AgentId;
                }
                else
                {
                    ConfigurationSystem config = new ConfigurationSystem();
                    AgentMasterId = AgentLoginInfo.AgentId;// Convert.ToInt32(config.GetSelfAgencyId()["selfAgentMasterId"]);
                }
                res.Price = CT.AccountingEngine.AccountingEngine.GetPrice(res, -1, AgentMasterId, 0);
            }
            catch (Exception ex)
            {
                WSStatus status = new WSStatus();
                status.Description = ex.Message;
                status.Category = "FQ";
                status.StatusCode = "02";
                fareQuoteResponse.Status = status;
                Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)userMaster.ID, "B2C-GetFareQuotes Failed. Error : " + ex.ToString() + " Stack trace " + ex.StackTrace, ipAddr);
                SendErrorMail("while GetFare Quote from MSE", userMaster, ex, sessionId);
                return fareQuoteResponse;
            }
            int paxCount = 0;
            for (int i = 0; i < res.FareBreakdown.Length; i++)
            {
                paxCount += res.FareBreakdown[i].PassengerCount; 
            }

            WSPTCFare[] ptcFare = new WSPTCFare[fare.Length];
            for (int i = 0; i < fare.Length; i++)
            {
                ptcFare[i].BaseFare = Convert.ToDecimal(fare[i].BaseFare);
                ptcFare[i].PassengerCount = fare[i].PassengerCount;
                ptcFare[i].PassengerType = fare[i].PassengerType;
                ptcFare[i].Tax = fare[i].Tax;
                if (res.Price.YQTax > 0 && fare[i].PassengerType != PassengerType.Infant)
                {
                    ptcFare[i].FuelSurcharge = Math.Round(res.Price.YQTax * fare[i].PassengerCount / paxCount);
                }
                else
                {
                    ptcFare[i].FuelSurcharge = 0;
                }
                # region Assigning Service Fee on the basis of customer preferrence

                // For a Non Whitelabel customer 
                if (Credential.SiteName == null || Credential.SiteName.Trim() == string.Empty)
                {
                    ptcFare[i].AdditionalTxnFee = fare[i].AdditionalTxnFee;
                }
                //for Whitelabel customer if Service fee check box is check in WL setting
                else if (userPref.AdditionalTxnFee != null && userPref.AdditionalTxnFee.ToUpper() == "TRUE")
                {
                    ptcFare[i].AdditionalTxnFee = fare[i].AdditionalTxnFee;
                }
                else
                {
                    ptcFare[i].AdditionalTxnFee = 0;
                }
                #endregion

                ptcFare[i].AirlineTransFee = fare[i].AirlineTransFee;                
            }
            fareQuoteResponse.Fare = ptcFare;
            WSStatus wsStatus = new WSStatus();
            wsStatus.Description = "Successful";
            wsStatus.Category = "FQ";
            wsStatus.StatusCode = "03";
            fareQuoteResponse.Status = wsStatus;
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Normal, (int)userMaster.ID, "B2C-GetFareQuotes Succeed : ", "");
            return fareQuoteResponse;
        }
        else
        {
            WSGetFareQuoteResponse fareQuoteResponse = new WSGetFareQuoteResponse();
            WSStatus status = new WSStatus();
            status.Description = "GetFareQuote Authentication Failed.";
            status.Category = "FQ";
            status.StatusCode = "01";
            fareQuoteResponse.Status = status;
            return fareQuoteResponse;
        }
    }

    private bool WSFareRuleInput(WSFareRuleRequest fareRuleRequest)
    {
        if (fareRuleRequest.FareRules.Length == 0)
        {
            statusDescription = "fare rule array cannot be of zero length";
            statusCode = "03";
            return false;
        }
        else
        {
            for (int i = 0; i < fareRuleRequest.FareRules.Length; i++)
            {
                if (fareRuleRequest.FareRules[i].Airline == null || fareRuleRequest.FareRules[i].Airline == "")
                {
                    statusDescription = "fare rule array cannot be of zero length";
                    statusCode = "04";
                    return false;
                }
                if (fareRuleRequest.FareRules[i].Airline == null || fareRuleRequest.FareRules[i].Airline == "")
                {
                    statusDescription = "Airline cannot be null or left blank";
                    statusCode = "05";
                    return false;
                }
                if (fareRuleRequest.FareRules[i].Destination == null || fareRuleRequest.FareRules[i].Destination == "")
                {
                    statusDescription = "Destination cannot be null or left blank";
                    statusCode = "06";
                    return false;
                }
                if (fareRuleRequest.FareRules[i].FareBasisCode == null || fareRuleRequest.FareRules[i].FareBasisCode == "")
                {
                    statusDescription = "FareBasisCode cannot be null or left blank";
                    statusCode = "07";
                    return false;
                }
                if (fareRuleRequest.FareRules[i].Origin == null || fareRuleRequest.FareRules[i].Origin == "")
                {
                    statusDescription = "Origin cannot be null or left blank";
                    statusCode = "10";
                    return false;
                }
            }
        }
        return true;
    }

    [WebMethod(EnableSession=true)]
    [SoapHeader("Credential")]
    public WSFareRuleResponse GetFareRule(WSFareRuleRequest fareRuleRequest,WSSearchRequest wsSearchRequest)
    {
        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)userMaster.ID);
        SearchResult result = Basket.FlightBookingSession[fareRuleRequest.SessionId].Result[fareRuleRequest.ResultId];
        if (userMaster != null)
        {
            if (!(WSFareRuleInput(fareRuleRequest)))
            {
                WSFareRuleResponse errorResponse = new WSFareRuleResponse();
                WSStatus status = new WSStatus();
                status.Category = "FK";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.GetFareRule, Severity.Normal, (int)userMaster.ID, "Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }
            List<FareRule> fareRule = new List<FareRule>();
            string airline = fareRuleRequest.FareRules[0].Airline;
            // BADELLON-4MC3RP --> airline code + origin + destination + "-" + farebasiscode.
            string origin = string.Empty;
            string destination = string.Empty;
            string farebasisCode = string.Empty;

            try
            {
                //if (fareRuleRequest.FareRules[0].Source == BookingSource.TBOAir || airline.ToUpper() == "SG" || airline.ToUpper() == "6E" || airline.ToUpper() == "DN" || airline.ToUpper() == "I7" || airline.ToUpper() == "G8" || airline.ToUpper() == "9H")
                //{
                //    origin = fareRuleRequest.FareRules[0].Origin;
                //    destination = fareRuleRequest.FareRules[0].Destination;
                //    farebasisCode = fareRuleRequest.FareRules[0].FareBasisCode;
                //}
                if (fareRuleRequest.FareRules[0].Source == BookingSource.AirArabia)//(airline.ToUpper() == "G9")
                {
                    List<FareRule> fRuleParam = new List<FareRule>();
                    try
                    {
                        fareRule = AirArabia.GetFareRule(fareRuleRequest.FareRules[0].Origin, fareRuleRequest.FareRules[fareRuleRequest.FareRules.Length - 1].Destination, fareRuleRequest.FareRules[0].FareBasisCode);
                    }
                    catch (Exception ex)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)userMaster.ID, "Air Arabia GetFareRule failed: " + ex.ToString(), "");
                    }
                }
                else if (fareRuleRequest.FareRules[0].Source == BookingSource.FlyDubai) //(airline.ToUpper() == "FZ")
                {
                    fareRule = CT.BookingEngine.GDS.FlyDubaiApi.GetFareRule(fareRuleRequest.FareRules[0].Origin, fareRuleRequest.FareRules[0].Destination, fareRuleRequest.FareRules[0].FareBasisCode);
                }
                else if ((fareRuleRequest.FareRules[0].Source == BookingSource.TBOAir || fareRuleRequest.FareRules[0].Source == BookingSource.Indigo || fareRuleRequest.FareRules[0].Source == BookingSource.SpiceJet || fareRuleRequest.FareRules[0].Source == BookingSource.GoAir) && result != null)
                {
                    MetaSearchEngine mse = new MetaSearchEngine(fareRuleRequest.SessionId);
                    mse.SettingsLoginInfo = AgentLoginInfo;
                    if (fareRuleRequest.FareRules[0].Source == BookingSource.SpiceJet)
                    {
                        SearchRequest searchRequest = new SearchRequest();
                        searchRequest.AdultCount = wsSearchRequest.AdultCount;
                        searchRequest.ChildCount = wsSearchRequest.ChildCount;
                        searchRequest.InfantCount = wsSearchRequest.InfantCount;
                        if (result.Flights.Length == 1)
                        {
                            searchRequest.Type = SearchType.OneWay;
                            searchRequest.Segments = new FlightSegment[1];
                            searchRequest.Segments[0] = new FlightSegment();
                            searchRequest.Segments[0].Origin = wsSearchRequest.Origin;
                            searchRequest.Segments[0].Destination = wsSearchRequest.Destination;

                        }
                        else if (result.Flights.Length == 2)
                        {
                            searchRequest.Type = SearchType.Return;
                            searchRequest.Segments = new FlightSegment[2];
                            searchRequest.Segments[0] = new FlightSegment();
                            searchRequest.Segments[0].Origin = wsSearchRequest.Origin;
                            searchRequest.Segments[0].Destination = wsSearchRequest.Destination;
                            searchRequest.Segments[1] = new FlightSegment();
                            searchRequest.Segments[1].Origin = wsSearchRequest.Destination;
                            searchRequest.Segments[1].Destination = wsSearchRequest.Origin;
                        }
                        fareRule = mse.GetFareRule(searchRequest, result);
                    }
                    else
                        fareRule = mse.GetFareRule(result);
                }
               
                

                //if (airline.ToUpper() == "SG")
                //{
                //    try
                //    {
                //        fareRule = SpiceJetAPI.GetFareRule(origin, destination, farebasisCode);
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "SpiceJetAPI.GetFareRule failed: " + ex.Message, "");
                //    }
                //}
                //else if (airline.ToUpper() == "6E")
                //{
                //    try
                //    {
                //        fareRule = Navitaire.GetFareRule(origin, destination, farebasisCode, airline);
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "Navitaire.GetFareRule failed: " + ex.Message, "");
                //    }
                //}                
                //else if (airline.ToUpper() == "I7")
                //{
                //    try
                //    {
                //        ParamountApi paramount = new ParamountApi();
                //        fareRule.Add(paramount.GetFareRule(origin, destination, farebasisCode));
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "Paramount.GetFareRule failed: " + ex.Message, "");
                //    }
                //}
                //else if (airline.ToUpper() == "G8")
                //{
                //    try
                //    {
                //        fareRule = GoAirApi.GetFareRule(origin, destination, farebasisCode);
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "GoAirAPI.GetFareRule failed: " + ex.Message, "");
                //    }
                //}
                //else if (airline.ToUpper() == "9H")
                //{
                //    try
                //    {
                //        string[] code = farebasisCode.Split('-');
                //        fareRule = Mdlr.GetFareRule(origin, destination, code[code.Length - 1]);
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)UserMaster.ID, "Mdlr.GetFareRule failed: " + ex.Message, "");
                //    }
                //}
                else              //Non-LCC
                {
                    for (int i = 0; i < fareRuleRequest.FareRules.Length; i++)
                    {
                        string tempFareBasisCode = string.Empty;
                        FareRule fRule = new FareRule();
                        fRule.Airline = fareRuleRequest.FareRules[i].Airline;
                        fRule.Origin = fareRuleRequest.FareRules[i].Origin;
                        fRule.Destination = fareRuleRequest.FareRules[i].Destination;
                        tempFareBasisCode = (fareRuleRequest.FareRules[i].FareBasisCode.Length > 9 ? fareRuleRequest.FareRules[i].FareBasisCode.Remove(0, 9) : fareRuleRequest.FareRules[i].FareBasisCode);
                        if (tempFareBasisCode.IndexOf('~') != -1) // From UAPI - FareInfo Key & FareInfo Key Value
                        {
                            string[] fareInfo = tempFareBasisCode.Split('~');
                            if (fareInfo.Length > 1)
                            {
                                fRule.FareBasisCode = fareInfo[0];
                                fRule.FareInfoRef = (fareInfo[1]);
                                if (fareInfo.Length > 2)
                                {
                                    fRule.FareRuleKeyValue = fareInfo[2];
                                }
                            }
                        }
                        else
                        {
                            fRule.FareBasisCode = tempFareBasisCode;
                        }

                        //fRule.FareBasisCode = fareRuleRequest.FareRules[i].FareBasisCode.Remove(0, 9);
                        fRule.DepartureTime = fareRuleRequest.FareRules[i].DepartureDate;
                        fRule.ReturnDate = fareRuleRequest.FareRules[i].ReturnDate;
                        //fRule.Source = fareRuleRequest.FareRules[i].Source;
                        fareRule.Add(fRule);
                    }
                    try
                    {
                        //if (fareRuleRequest.FareRules[0].Source.Equals(BookingSource.Galileo))
                        //    fareRule = GalileoApi.GetFareRuleList(fareRule);
                        if (fareRuleRequest.FareRules[0].Source.Equals(BookingSource.UAPI))
                        {
                            SourceDetails agentDetails = new SourceDetails();
                            UAPI uapi = new UAPI();
                            agentDetails = AgentLoginInfo.AgentSourceCredentials["UA"];
                            uapi.agentBaseCurrency = AgentLoginInfo.Currency;
                            uapi.ExchangeRates = AgentLoginInfo.AgentExchangeRates;

                            uapi.UserName = agentDetails.UserID;
                            uapi.Password = agentDetails.Password;
                            uapi.TargetBranch = agentDetails.HAP;
                            uapi.AppUserId = AgentLoginInfo.UserID.ToString();
                            uapi.EndpointURL = agentDetails.EndPoint;
                            uapi.SessionId = Guid.NewGuid().ToString();
                            fareRule = uapi.GetFareRuleList(fareRule);
                        }
                        //Added by Lokesh on 15-April-2018
                        //This is for Amadeus Air Source
                        //The below method is used to get the fare rules
                        else if (fareRuleRequest.FareRules[0].Source.Equals(BookingSource.Amadeus))
                        {
                            SourceDetails agentDetails = new SourceDetails();
                            AmadeusAPI amadeusAPI = new AmadeusAPI();
                            agentDetails = AgentLoginInfo.AgentSourceCredentials["1A"];
                            amadeusAPI.AgentBaseCurrency = AgentLoginInfo.Currency;
                            amadeusAPI.ExchangeRates = AgentLoginInfo.AgentExchangeRates;
                            amadeusAPI.AgentDecimalValue = AgentLoginInfo.DecimalValue;
                            amadeusAPI.UserName = AgentLoginInfo.AgentSourceCredentials["1A"].UserID;
                            amadeusAPI.Password = AgentLoginInfo.AgentSourceCredentials["1A"].Password;
                            amadeusAPI.OfficeId = AgentLoginInfo.AgentSourceCredentials["1A"].PCC;
                            amadeusAPI.MarketIataCode = AgentLoginInfo.AgentSourceCredentials["1A"].HAP;

                            amadeusAPI.AppUserID = Convert.ToInt32(AgentLoginInfo.UserID.ToString());
                            amadeusAPI.SessionID = fareRuleRequest.SessionId;

                            SearchResult searchResult = Basket.FlightBookingSession[fareRuleRequest.SessionId].Result[fareRuleRequest.ResultId];
                            fareRule = amadeusAPI.GetFareRules(searchResult);
                        }

                        // added by bangar for sabre get farerules
                        else if (fareRuleRequest.FareRules[0].Source.Equals(BookingSource.Sabre))
                        {
                            SourceDetails agentDetails = new SourceDetails();
                            SabreAPI sabreAPI = new SabreAPI();
                          
                                agentDetails = AgentLoginInfo.AgentSourceCredentials["SABRE"];
                                sabreAPI.AgentBaseCurrency = AgentLoginInfo.Currency;
                                sabreAPI.ExchangeRates = AgentLoginInfo.AgentExchangeRates;

                                sabreAPI.ClientId = AgentLoginInfo.AgentSourceCredentials["SABRE"].UserID;
                                sabreAPI.ClientSecret = AgentLoginInfo.AgentSourceCredentials["SABRE"].Password;
                                sabreAPI.PCC = AgentLoginInfo.AgentSourceCredentials["SABRE"].PCC;

                                sabreAPI.AppUserID = Convert.ToInt32(AgentLoginInfo.UserID.ToString());
                                sabreAPI.SessionID = fareRuleRequest.SessionId;

                                SearchResult searchResult = Basket.FlightBookingSession[fareRuleRequest.SessionId].Result[fareRuleRequest.ResultId];
                            sabreAPI.SessionToken = Basket.FlightBookingSession[fareRuleRequest.SessionId].Log[0].ToString();
                            fareRule = sabreAPI.GenrateFareRules(searchResult);

                        }



                    }
                    catch (Exception ex)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, (int)userMaster.ID, "GetFareRule failed: " + ex.ToString(), "");
                    }
                }
                Audit.AddAuditBookingAPI(EventType.GetFareRule, Severity.Low, (int)userMaster.ID, "GetFareRule Succeed", "");
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.GetFareRule, Severity.High, (int)userMaster.ID, "Error in GetFareRule: " + ex.ToString(), "");
            }
            WSFareRuleResponse wsFareRuleResponse = new WSFareRuleResponse();
            wsFareRuleResponse.FareRules = new WSFareRule[fareRule.Count];
            int counter = 0;
            foreach (FareRule fRule in fareRule)
            {
                wsFareRuleResponse.FareRules[counter] = new WSFareRule();
                wsFareRuleResponse.FareRules[counter].Airline = fRule.Airline;
                wsFareRuleResponse.FareRules[counter].Destination = fRule.Destination;
                wsFareRuleResponse.FareRules[counter].Origin = fRule.Origin;
                wsFareRuleResponse.FareRules[counter].FareBasisCode = fRule.FareBasisCode;
                wsFareRuleResponse.FareRules[counter].FareRestriction = fRule.FareRestriction;
                wsFareRuleResponse.FareRules[counter].FareRuleDetail = fRule.FareRuleDetail;
                wsFareRuleResponse.FareRules[counter].Source = fareRuleRequest.FareRules[counter].Source;
                wsFareRuleResponse.FareRules[counter].DepartureDate = fRule.DepartureTime;
                wsFareRuleResponse.FareRules[counter].ReturnDate = fRule.ReturnDate;
                counter++;
            }
            WSStatus wsStatus = new WSStatus();
            wsStatus.Description = "Successful";
            wsStatus.Category = "FR";
            wsStatus.StatusCode = "02";
            wsFareRuleResponse.Status = wsStatus;
            return wsFareRuleResponse;
        }
        else
        {
            WSFareRuleResponse wsFareRuleResponse = new WSFareRuleResponse();
            WSStatus status = new WSStatus();
            status.Description = "GetFareRule Authentication Failed.";
            status.Category = "FR";
            status.StatusCode = "01";
            wsFareRuleResponse.Status = status;
            return wsFareRuleResponse;
        }
    }

    private bool WSGetBookingInput(WSGetBookingRequest bookingRequest)
    {
        if (bookingRequest.BookingId == null || bookingRequest.BookingId == "")
        {
            statusDescription = "BookingID canot be null or blank";
            statusCode = "20";
            return false;
        }
        return true;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetBookingResponse GetBooking(WSGetBookingRequest bookingRequest)
    {        
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        WSGetBookingResponse errorResponse = new WSGetBookingResponse();
        WSStatus status = new WSStatus();
        if (UserMaster != null)
        {
            if (!(WSGetBookingInput(bookingRequest)))
            {
                status.Category = "RT";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                errorResponse.Source = BookingSource.Amadeus;
                Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.Normal, (int)UserMaster.ID, "Input parameter is incorrect: " + statusDescription, ipAddr);
                return errorResponse;
            }
            WSGetBookingResponse booking = new WSGetBookingResponse();
            bool isWhiteLabel = false;
            if (Credential.SiteName != null && Credential.SiteName.Trim() != string.Empty)
            {
                isWhiteLabel = true;
            }
            try
            {
                booking.Load(bookingRequest.BookingId, UserMaster.AgentId, isWhiteLabel);
            }
            catch (ArgumentException ex)
            {
                status.Category = "RT";
                status.Description = ex.Message;
                status.StatusCode = "22";
                errorResponse.Status = status;
                errorResponse.Source = BookingSource.Amadeus;
                Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.Normal, (int)UserMaster.ID, "Error in Get Booking for bookingID " + bookingRequest.BookingId + ex.ToString(), ipAddr);
                return errorResponse;
            }
            catch (SqlException ex)
            {
                status.Category = "RT";
                status.Description = ex.Message;
                status.StatusCode = "23";
                errorResponse.Status = status;
                errorResponse.Source = BookingSource.Amadeus;
                Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.Normal, (int)UserMaster.ID, "Error in Get Booking for bookingID " + bookingRequest.BookingId + ex.ToString(), ipAddr);
                return errorResponse;
            }
            catch (Exception ex)
            {
                status.Category = "RT";
                status.Description = "Technical Fault";
                status.StatusCode = "24";
                errorResponse.Status = status;
                errorResponse.Source = BookingSource.Amadeus;
                Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.Normal, (int)UserMaster.ID, "Error in Get Booking for bookingID " + bookingRequest.BookingId + " " + ex.ToString() + " Stack Trace " + ex.StackTrace, ipAddr);
                SendErrorMail("while GetBooking For BookingId " + bookingRequest.BookingId, UserMaster, ex);
                return errorResponse;
                
            }
            return booking;
        }
        else
        {
            status.Category = "RT";
            status.Description = "GetBooking Authentication Failed";
            status.StatusCode = "25";
            errorResponse.Status = status;
            return errorResponse;
        }

    }

    private bool WSGetTripDetailInput(WSGetTripDetailRequest tripRequest)
    {
        if (tripRequest.TripId == null || tripRequest.TripId == "")
        {
            statusDescription = "TripID can not be null or blank";
            statusCode = "02";
            return false;
        }
        return true;
    }


    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetTripDetailResponse GetTripDetail(WSGetTripDetailRequest tripRequest)
    {
        Trace.TraceInformation("BookingAPI.GetTripDetail entered. RID = " + tripRequest.TripId);
        UserMaster UserMaster = new UserMaster();
        WSGetTripDetailResponse tripResponse = new WSGetTripDetailResponse();
        UserMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (UserMaster != null)
        {
            if (!(WSGetTripDetailInput(tripRequest)))
            {
                WSGetTripDetailResponse errorResponse = new WSGetTripDetailResponse();
                WSStatus status = new WSStatus();
                status.Category = "RT";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.High, (int)UserMaster.ID, "Input parameter is incorrect TripId" + tripRequest.TripId + " " + statusDescription, ipAddr);
                return errorResponse;
            }

            BApiBookingDetail bDetail = new BApiBookingDetail();
            try
            {
                int tripId = 0;
                int value;
                if (int.TryParse(tripRequest.TripId, out value))
                {
                    tripId = value;
                }
                else
                {
                    tripId = Convert.ToInt32(tripRequest.TripId.Remove(0, 3));
                }

                bDetail.Load(tripId, UserMaster.AgentId);
            }
            catch (Exception ex)
            {
                WSGetTripDetailResponse errorResponse = new WSGetTripDetailResponse();
                WSStatus status = new WSStatus();
                status.Category = "RT";
                status.Description = "Retrieve Trip Detail failed as BAPIBookingDetails could not be loaded for tripid=" + tripRequest.TripId;
                status.StatusCode = "04";
                errorResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.Normal, (int)UserMaster.ID, "Input parameter is incorrect TripId" + tripRequest.TripId + " " + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                return errorResponse;
            }
            if (bDetail.IsDomesticReturn)
            {
                if (bDetail.BookingIdOut != null && bDetail.BookingIdOut != "" && bDetail.BookingIdIn != null && bDetail.BookingIdIn != "")
                {
                    try
                    {
                        tripResponse.BookingDetail = new WSBookingDetail[2];
                        tripResponse.BookingDetail[0] = new WSBookingDetail();
                        tripResponse.BookingDetail[0].Load(bDetail.BookingIdOut);
                        tripResponse.BookingDetail[1] = new WSBookingDetail();
                        tripResponse.BookingDetail[1].Load(bDetail.BookingIdIn);
                        tripResponse.Status = new WSStatus();
                        tripResponse.Status.Category = "RT";
                        tripResponse.Status.Description = "Successful";
                        tripResponse.Status.StatusCode = "05";
                    }
                    catch (Exception ex)
                    {
                        WSStatus status = new WSStatus();
                        status.Category = "RT";
                        status.Description = "Retriving Trip Details Failed";
                        status.StatusCode = "06";
                        tripResponse.Status = status;
                        Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.High, (int)UserMaster.ID, "Retrieve Trip Booking Failed TripId" + tripRequest.TripId + " " + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                        return tripResponse;
                    }
                }
                else
                {
                    WSStatus status = new WSStatus();
                    status.Category = "RT";
                    status.Description = "Retriving Trip Details Failed as atleast one BookingId was null";
                    status.StatusCode = "07";
                    tripResponse.Status = status;
                    return tripResponse;
                }
            }
            else
            {
                if (bDetail.BookingIdOut != null && bDetail.BookingIdOut != "")
                {
                    try
                    {

                        tripResponse.BookingDetail = new WSBookingDetail[1];
                        tripResponse.BookingDetail[0] = new WSBookingDetail();
                        tripResponse.BookingDetail[0].Load(bDetail.BookingIdOut);
                        tripResponse.Status = new WSStatus();
                        tripResponse.Status.Category = "RT";
                        tripResponse.Status.Description = "Successful";
                        tripResponse.Status.StatusCode = "08";
                    }
                    catch (Exception ex)
                    {
                        WSStatus status = new WSStatus();
                        status.Category = "RT";
                        status.Description = "Retriving Trip Details Failed";
                        status.StatusCode = "09";
                        tripResponse.Status = status;
                        Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.High, (int)UserMaster.ID, "Retrieve Trip Booking For Outbound Failed for TripId" + tripRequest.TripId + " " + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                        return tripResponse;
                    }
                }
                else
                {
                    WSStatus status = new WSStatus();
                    status.Category = "RT";
                    status.Description = "Retriving Trip Details Failed as BookingId was null";
                    status.StatusCode = "10";
                    tripResponse.Status = status;
                    return tripResponse;
                }
            }
        }
        else
        {
            WSGetTripDetailResponse errorResponse = new WSGetTripDetailResponse();
            WSStatus status = new WSStatus();
            status.Category = "RT";
            status.Description = "Retriving Trip Details Authentication Failed";
            status.StatusCode = "01";
            errorResponse.Status = status;
            return errorResponse;
        }
        Audit.AddAuditBookingAPI(EventType.GetBooking, Severity.High, (int)UserMaster.ID, "Retrieve Trip Booking  Passed for TripId: " + tripRequest.TripId, ipAddr);
        Trace.TraceInformation("BookingAPI.GetTripDetail exiting.");
        return tripResponse;
    }

    private bool WSTicketInput(WSTicketRequest ticketRequest)
    {
        if (ticketRequest.BookingID == null || ticketRequest.BookingID == "")
        {
            if (ticketRequest.Fare == null)
            {
                statusDescription = "BookingID and Fare both cannot be null or blank simultaniously";
                statusCode = "23";
                return false;
            }
            if (ticketRequest.FareBasis == null)
            {
                statusDescription = "BookingID and FareBasis both cannot be null or blank simultaniously";
                statusCode = "16";
                return false;
            }
            if (ticketRequest.FareType == null)
            {
                statusDescription = "BookingID and FareType both cannot be null or blank simultaniously";
                statusCode = "17";
                return false;
            }
            if (ticketRequest.Origin == null)
            {
                statusDescription = "BookingID and Origin both cannot be null or blank simultaniously";
                statusCode = "18";
                return false;
            }
            if (ticketRequest.Passenger == null)
            {
                statusDescription = "BookingID and Passenger both cannot be null or blank simultaniously";
                statusCode = "19";
                return false;
            }
            if (ticketRequest.Passenger.Length <= 0)
            {
                statusDescription = "BookingID and length Passenger array both cannot be zero";
                statusCode = "19";
                return false;
            }
            foreach (WSPassenger passenger in ticketRequest.Passenger)
            {
                if (passenger.Fare == null)
                {
                    statusDescription = "BookingId and fare of passenger can't be null";
                    statusCode = "19";
                    return false;
                }
            }
            if (ticketRequest.Passenger.Length <= 0)
            {
                statusDescription = "BookingID and length Passenger array both cannot be zero";
                statusCode = "19";
                return false;
            }
            if (ticketRequest.Segment == null || ticketRequest.Segment.Length <= 0)
            {
                statusDescription = "BookingID and Segment both cannot be null or blank simultaniously";
                statusCode = "20";
                return false;
            }
            if (ticketRequest.SessionId == null)
            {
                statusDescription = "BookingID and SessionID both cannot be null or blank simultaniously";
                statusCode = "21";
                return false;
            }
            if (!MetaSearchEngine.IsActiveSession(ticketRequest.SessionId))
            {
                statusDescription = "Your booking session is expired please start a new search.";
                statusCode = "21";
                return false;
            }
        }
        return true;
    }

    private string GetTicketXml(object obj)
    {
        MemoryStream stream = null;
        TextWriter writer = null;
        try
        {
            stream = new MemoryStream(); // read xml in memory

            writer = new StreamWriter(stream, Encoding.Unicode);
            // get serialise object
            if (obj.GetType().Name.Equals("WSTicketRequest"))
            {
                WSTicketRequest objTR = (WSTicketRequest)obj;
                XmlSerializer serializer = new XmlSerializer(typeof(WSTicketRequest));
                serializer.Serialize(writer, objTR); // read object
            }
            else if (obj.GetType().Name.Equals("WSTicketResponse"))
            {
                WSTicketResponse objTR = (WSTicketResponse)obj;
                XmlSerializer serializer = new XmlSerializer(typeof(WSTicketResponse));
                serializer.Serialize(writer, objTR); // read object
            }

            int count = (int)stream.Length; // saves object in memory stream

            byte[] arr = new byte[count];
            stream.Seek(0, SeekOrigin.Begin);
            // copy stream contents in byte array

            stream.Read(arr, 0, count);
            UnicodeEncoding utf = new UnicodeEncoding(); // convert byte array to string

            return utf.GetString(arr).Trim();
        }
        catch
        {
            return string.Empty;
        }
        finally
        {
            if (stream != null) stream.Close();
            if (writer != null) writer.Close();
        }
    }

    [WebMethod(EnableSession=true)]
    [SoapHeader("Credential")]
    public WSTicketResponse Ticket(WSTicketRequest wsTicketRequest)
    {
        WSTicketResponse wsTicketResponse = new WSTicketResponse();
        try
        {
            WSUserPreference preference = new WSUserPreference();
            //Retrieve preferences to check Ticketing allowed for GDS or not
            preference = GetPreferences();

            UserPreference pref = new UserPreference();           
            int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
            AgentLoginInfo = UserMaster.GetB2CUser(UserId);

            /**************************** Start :Ticket Method Audit records for Wego Portal ***********/

            Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")Ticketing in progress.Agent Id :" + AgentLoginInfo.AgentId + "Session Id:" + wsTicketRequest.SessionId, "", Convert.ToString(wsTicketRequest.Source));
            /**************************** End :Ticket Method Audit records for Wego Portal ***********/



            #region Credit card information
            CreditCardPaymentInformation cardInfo = new CreditCardPaymentInformation();
            FlightItinerary itinerary = new FlightItinerary();
            bool isPGOwnedBYTBO = false;
            string paymentId = "";
            if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
            {
                cardInfo.Amount = wsTicketRequest.PaymentInformation.Amount;
                cardInfo.PaymentGateway = (CT.AccountingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.AccountingEngine.PaymentGatewaySource), wsTicketRequest.PaymentInformation.PaymentGateway.ToString());
                cardInfo.PaymentId = wsTicketRequest.PaymentInformation.PaymentId;
                cardInfo.TrackId = wsTicketRequest.PaymentInformation.TrackId;
                cardInfo.IPAddress = wsTicketRequest.PaymentInformation.IPAddress;
                cardInfo.Charges = wsTicketRequest.PaymentInformation.Charges;
                cardInfo.PaymentInformationId = wsTicketRequest.PaymentInformation.PaymentInformationId;//Set PaymentInformationID already saved
            }
            #endregion


            UserMaster userMaster = new UserMaster();
            userMaster = WSValidate(Credential);
            string ipAddr = this.Context.Request.UserHostAddress;
            decimal AgentMastertLccLimit = 0;
            string strTicketRequestXml = string.Empty;
            string strTicketResponseXml = string.Empty;
            string strCredentials = string.Empty;
            bool bSaveStatus = false;
            string oldSessionId = string.Empty;
            if (userMaster != null)
            {
                bool isLCC = false;
                if (wsTicketRequest.BookingID != null && wsTicketRequest.BookingID.Trim() != string.Empty)
                {
                    isLCC = false;
                }
                else if (wsTicketRequest.Source != BookingSource.Amadeus && wsTicketRequest.Source != BookingSource.Sabre && wsTicketRequest.Source != BookingSource.WorldSpan && wsTicketRequest.Source != BookingSource.Galileo && wsTicketRequest.Source != BookingSource.UAPI)
                {
                    isLCC = true;
                }
                isLCC = wsTicketRequest.IsLCC;//Check whether the itinerary is LCC irrespective of Source
                pref = pref.GetPreference((int)userMaster.ID, "ISPGOWNEDBYTBO", "Booking & Confirmation");
                if (pref != null && pref.Value != null)
                {
                    isPGOwnedBYTBO = Convert.ToBoolean(pref.Value);
                }
                if (wsTicketRequest.IsOneWayBooking && isPGOwnedBYTBO && Credential.SiteName != null)
                {
                    //paymentId = AcceptPayment(wsTicketRequest.PaymentInformation.PaymentGateway, wsTicketRequest.PaymentInformation.TrackId, wsTicketRequest.PaymentInformation.Amount, userMaster.AgentId, (int)userMaster.ID, isLCC);
                    wsTicketResponse.PaymentReferenceNumber = paymentId;
                }
                else if (!wsTicketRequest.IsOneWayBooking && isPGOwnedBYTBO && Credential.SiteName != null)
                {
                    paymentId = (wsTicketRequest.PaymentInformation.PaymentId);

                    Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")Ticket Method;paymentId =" + paymentId.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));


                }

                #region validate input parameter

                if (!(WSTicketInput(wsTicketRequest)))
                {
                    WSTicketResponse errorResponse = new WSTicketResponse();
                    errorResponse.ProdType = ProductType.Flight;
                    WSStatus wsStatus = new WSStatus();
                    wsStatus.Category = "TK";
                    wsStatus.Description = statusDescription;
                    wsStatus.StatusCode = statusCode;
                    errorResponse.Status = wsStatus;
                    errorResponse.ProdType = ProductType.Flight;

                    Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")Input parameter is incorrect:" + statusDescription, ipAddr, Convert.ToString(wsTicketRequest.Source));


                    if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return errorResponse;
                }
                #endregion

                #region Log the ticket request in DB


                if (wsTicketRequest.SessionId != null && wsTicketRequest.SessionId.Trim() != string.Empty)
                {
                    oldSessionId = wsTicketRequest.SessionId;
                }
                strTicketRequestXml = GetTicketXml(wsTicketRequest);
                strCredentials = string.Empty;
                if (Credential.SiteName != null && Credential.SiteName != "")
                {
                    strCredentials = Credential.SiteName;
                }
                else if (Credential.AccountCode != null && Credential.AccountCode != "")
                {
                    strCredentials = Credential.AccountCode;
                }
                else
                {
                    strCredentials = Credential.UserName;
                }
                if (strTicketRequestXml != string.Empty)
                {
                    bSaveStatus = SaveTicketXML(userMaster, wsTicketRequest.SessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);
                }
                else
                {


                    Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")Error in GetTicketXml method", ipAddr, Convert.ToString(wsTicketRequest.Source));


                }
                #endregion

                WSStatus status = new WSStatus();
                if (isLCC && wsTicketRequest.SessionId != null && !MetaSearchEngine.IsActiveSession(wsTicketRequest.SessionId))
                {


                    Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Booking session is expired for session id:" + wsTicketRequest.SessionId, ipAddr, Convert.ToString(wsTicketRequest.Source));


                    status.Category = "TK";
                    status.StatusCode = "21";
                    status.Description = "Booking session is expire.please search again.";
                    wsTicketResponse.Status = status;
                    wsTicketResponse.ProdType = ProductType.Flight;
                    if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    wsTicketResponse.PaymentReferenceNumber = paymentId;
                    return wsTicketResponse;
                }
                MetaSearchEngine mse = new MetaSearchEngine(wsTicketRequest.SessionId);
                string remarks = string.Empty;
                #region Check for rights of user

                //if ((!Role.IsAllowedTask(UserMaster.RoleId, (int)Task.DomesticAutoTicketing)) || (!Role.IsAllowedTask(UserMaster.RoleId, (int)Task.DomesticAutoTicketing)))
                //{
                //    WSTicketResponse errorResponse = new WSTicketResponse();
                //    errorResponse.PaymentReferenceNumber = paymentId;
                //    status.Category = "TK";
                //    status.Description = "User is not Authorized to generate the ticket";
                //    status.StatusCode = "30";
                //    errorResponse.Status = status;
                //    errorResponse.ProdType = ProductType.Flight;
                //    Audit.AddAuditBookingAPI(EventType.Book, Severity.High, (int)UserMaster.ID, "UserMaster is not Authorized for Ticketing. Name :" + UserMaster.FirstName + " "+UserMaster.LastName + " Login Name :" + UserMaster.LoginName, ipAddr);
                //    return errorResponse;
                //}
                #endregion

                #region Check whether the Non LCC flight is already booked or not

                if ((wsTicketRequest.Source == BookingSource.WorldSpan || wsTicketRequest.Source == BookingSource.Amadeus || wsTicketRequest.Source == BookingSource.Sabre || wsTicketRequest.Source == BookingSource.Galileo || wsTicketRequest.Source == BookingSource.UAPI) && (wsTicketRequest.BookingID == null || wsTicketRequest.BookingID.Trim() == string.Empty))
                {
                    status.Category = "TK";
                    status.Description = "Please First book the requested Itinerary then ticket";
                    status.StatusCode = "22";
                    wsTicketResponse.Status = status;
                    wsTicketResponse.ProdType = ProductType.Flight;


                    Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")Selected itinerary is not booked", "", Convert.ToString(wsTicketRequest.Source));


                    if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    wsTicketResponse.PaymentReferenceNumber = paymentId;

                    //Log the ticket response in DB
                    strTicketResponseXml = GetTicketXml(wsTicketResponse);
                    bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                    return wsTicketResponse;
                }
                #endregion

                #region assigning AgentMaster

                int AgentMasterId;
                if (userMaster.AgentId > 0)
                {
                    AgentMasterId = userMaster.AgentId;
                }
                else
                {
                    ConfigurationSystem config = new ConfigurationSystem();
                    AgentMasterId = AgentLoginInfo.AgentId; //Convert.ToInt32(config.GetSelfAgencyId()["selfAgentMasterId"]);
                }
                #endregion

                #region Ticketing for LCC
                if (wsTicketRequest.Source != BookingSource.Amadeus && wsTicketRequest.Source != BookingSource.Sabre && wsTicketRequest.Source != BookingSource.WorldSpan && wsTicketRequest.Source != BookingSource.Galileo && wsTicketRequest.Source != BookingSource.UAPI && wsTicketRequest.IsLCC)
                {
                    
                    WSBookRequest wsBookRequest = new WSBookRequest();
                    BookingResponse mseBookingResponse = new BookingResponse();

                    #region Check whether a agent is capable for LCC booking or not
                    //AgentMastertLccLimit = CT.AccountingEngine.LedgerTransaction.IsAgentCapable(AgentMasterId, AccountType.LCC);
                    if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.Deposited && AgentMastertLccLimit < GetTicketPrice(wsTicketRequest.Fare) && userMaster.AgentId > 0)
                    {
                        status.Category = "TK";
                        status.Description = "LCC balance is less than ticket amount";
                        status.StatusCode = "15";
                        wsTicketResponse.Status = status;

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")Lcc balance of AgentMaster is less than ticketed amount", ipAddr, Convert.ToString(wsTicketRequest.Source));


                        if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        wsTicketResponse.ProdType = ProductType.Flight;
                        wsTicketResponse.PaymentReferenceNumber = paymentId;

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                        return wsTicketResponse;
                    }
                    #endregion

                    #region Building itinerary for MSE
                    wsBookRequest.Origin = wsTicketRequest.Origin;
                    wsBookRequest.Destination = wsTicketRequest.Destination;
                    wsBookRequest.Fare = wsTicketRequest.Fare;
                    wsBookRequest.FareBasis = wsTicketRequest.FareBasis;
                    wsBookRequest.FareType = wsTicketRequest.FareType;
                    wsBookRequest.Segment = wsTicketRequest.Segment;
                    wsBookRequest.Passenger = wsTicketRequest.Passenger;
                    wsBookRequest.Source = wsTicketRequest.Source;
                    wsBookRequest.SessionId = wsTicketRequest.SessionId;
                    wsBookRequest.PaymentInformation = wsTicketRequest.PaymentInformation;
                    wsBookRequest.IsInsured = wsTicketRequest.IsInsured;
                    wsBookRequest.GUID = wsTicketRequest.GUID;
                    wsBookRequest.IsLCC = wsTicketRequest.IsLCC;
                    wsBookRequest.IsGSTMandatory = wsTicketRequest.IsGSTMandatory;
                    wsBookRequest.GstCompanyAddress = wsTicketRequest.GstCompanyAddress;
                    wsBookRequest.GstCompanyContactNumber = wsTicketRequest.GstCompanyContactNumber;
                    wsBookRequest.GstCompanyEmail = wsTicketRequest.GstCompanyEmail;
                    wsBookRequest.GstCompanyName = wsTicketRequest.GstCompanyName;
                    wsBookRequest.GstNumber = wsTicketRequest.GstNumber;
                    wsBookRequest.ResultId = wsTicketRequest.ResultId;
                    try
                    {
                        itinerary = BuildItinerary(wsBookRequest, (int)userMaster.ID, AgentMasterId);

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")Itinerary suceessfully build", "", Convert.ToString(wsTicketRequest.Source));


                    }
                    catch (ArgumentException ex)
                    {
                        status.Category = "TK";
                        status.StatusCode = "16";
                        status.Description = ex.Message;
                        wsTicketResponse.Status = status;
                        wsTicketResponse.ProdType = ProductType.Flight;

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")Book: Build Itinerary failed:" + ex.ToString(), "", Convert.ToString(wsTicketRequest.Source));


                        if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        wsTicketResponse.PaymentReferenceNumber = paymentId;

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                        return wsTicketResponse;
                    }
                    catch (Exception ex)
                    {

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Ticketing Status: error in BuildItinerary:" + ex.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));
                        status.Category = "TK";
                        status.StatusCode = "24";
                        status.Description = "Technical Problem 1";
                        wsTicketResponse.Status = status;
                        wsTicketResponse.ProdType = ProductType.Flight;
                        SendErrorMail("while building itinerary of LCC airline for MSE", userMaster, ex, wsTicketRequest);
                        if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                        return wsTicketResponse;
                    }
                    #endregion

                    #region Send request for LCC Ticketing
                    try
                    {

                        //Lead pax GST Call
                        //Updates the GST details for the lead passenger.
                        try
                        {
                            if (wsBookRequest.IsGSTMandatory && itinerary != null && !string.IsNullOrEmpty(itinerary.Endorsement) && !string.IsNullOrEmpty(wsBookRequest.GstCompanyEmail) && !string.IsNullOrEmpty(wsBookRequest.GstCompanyName) && !string.IsNullOrEmpty(wsBookRequest.GstNumber))
                            {
                                mse.UpdateNavitaireGSTContactDetails(wsBookRequest.GstCompanyEmail, wsBookRequest.GstCompanyName, wsBookRequest.GstNumber, itinerary);
                            }
                        }
                        catch (Exception ex)
                        {
                            status.Category = "TK";
                            status.StatusCode = "02";
                            status.Description = ex.Message;
                            wsTicketResponse.Status = status;
                            wsTicketResponse.ProdType = ProductType.Flight;
                            return wsTicketResponse;
                        }

                        Product prod = itinerary;
                        mse.SettingsLoginInfo = AgentLoginInfo;
                        mseBookingResponse = mse.Book(ref itinerary, AgentMasterId, BookingStatus.Ticketed, userMaster, true, "");

                        //If the site name contains "wego" then we need to update the LookToBook Ratio Table
                        if (!string.IsNullOrEmpty(Credential.SiteName) && Credential.SiteName.ToLower().Contains("wego"))
                        {

                            try
                            {
                                bool isBooked = false;
                                string mseWegoBookingErrorMessage = string.Empty;
                                if (mseBookingResponse.Error != null && mseBookingResponse.Error != "")
                                {
                                    mseWegoBookingErrorMessage = mseBookingResponse.Error;
                                }


                                if (mseBookingResponse.Status != BookingResponseStatus.Failed)
                                {
                                    isBooked = true;
                                }

                                Audit.Add(EventType.Ticketing, Severity.Low, 0, "MSE -Save BookingRatio In Progress", "");
                                //Need to update against the same session id ,user id and agency id as isBooked true(Booking Hits)
                                mse.SaveBookingRatio("B2C", ProductType.Flight, string.Empty, wsTicketRequest.SessionId, 0, UserId, AgentLoginInfo.AgentId, mseWegoBookingErrorMessage, Credential.SiteName, isBooked, mseBookingResponse.BookingId, Convert.ToString(wsTicketRequest.Source));

                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Fail to Update the Look To Book Ratio" + ex.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));
                            }
                        }
                        if (mseBookingResponse.Status == BookingResponseStatus.Successful)
                        {                            
                            Invoice invoice = new Invoice();

                            int invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.FlightId, "", (int)userMaster.ID, ProductType.Flight, 1);
                            if (invoiceNumber > 0)
                            {
                                invoice.Load(invoiceNumber);
                                invoice.Status = InvoiceStatus.Paid;
                                invoice.CreatedBy = (int)userMaster.ID;
                                invoice.LastModifiedBy = (int)userMaster.ID;
                                invoice.UpdateInvoice();
                            }

                            #region Added Promotion
                            try
                            {
                                ////For UAPI PromoDetails are saved while Holding the ticket
                                //if (itinerary.FlightBookingSource != BookingSource.UAPI)
                                //{
                                //    PromoDetails promoDet = new PromoDetails();
                                //    promoDet.PromoId = wsTicketRequest.PromoDetail.PromoId;
                                //    promoDet.PromoCode = wsTicketRequest.PromoDetail.PromoCode;
                                //    promoDet.ProductId = (int)ProductType.Flight;
                                //    promoDet.DiscountType = wsTicketRequest.PromoDetail.PromoDiscountType;
                                //    promoDet.DiscountValue = wsTicketRequest.PromoDetail.PromoDiscountValue;
                                //    promoDet.DiscountAmount = wsTicketRequest.PromoDetail.PromoDiscountAmount;
                                //    promoDet.ReferenceId = itinerary.FlightId;
                                //    promoDet.CreatedBy = (int)UserId;
                                //    promoDet.Save();
                                //}
                            }
                            catch 
                            {

                                //Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")B2C-Flight API :Exception while saving PromoDetails.Message:" + ex.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));


                            }

                            #endregion
                        }

                        else if (mseBookingResponse.Status != BookingResponseStatus.Successful)
                        {
                            status.Category = "TK";
                            status.StatusCode = "02";
                            status.Description = mseBookingResponse.Error;
                            wsTicketResponse.Status = status;
                            wsTicketResponse.ProdType = ProductType.Flight;
                            return wsTicketResponse;
                        }
                    }
                    catch (ArgumentException ex)
                    {

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Fail to ticket requested itinerary:" + ex.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));
                        status.Category = "TK";
                        status.StatusCode = "02";
                        status.Description = ex.Message;
                        wsTicketResponse.Status = status;
                        wsTicketResponse.ProdType = ProductType.Flight;
                        SendErrorMail("Ticketing failed for LCC from MSE", userMaster, ex, wsTicketRequest);
                        if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        wsTicketResponse.PaymentReferenceNumber = paymentId;

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                        return wsTicketResponse;
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Failed to ticket. Reason :" + ex.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));
                        status.StatusCode = "10";
                        status.Category = "TK";
                        if (mseBookingResponse.Error != null && mseBookingResponse.Error.ToLower().Contains("bookingId not generated"))
                        {
                            status.Description = "Ticketing Failed : Transaction Failure";
                        }
                        else if (mseBookingResponse.Error != null && mseBookingResponse.Error.ToLower().Contains("timeout"))
                        {
                            status.Description = "Ticketing Failed : TimeOut";
                        }
                        else if (mseBookingResponse.Error != null)
                        {
                            status.Description = "Ticketing Failed : " + mseBookingResponse.Error;
                        }
                        else
                        {
                            status.Description = "Technical Fault 5";
                        }
                        wsTicketResponse.Status = status;
                        wsTicketResponse.ProdType = ProductType.Flight;
                        SendErrorMail("while ticketing of LCC from MSE", userMaster, ex, wsTicketRequest);
                        if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        wsTicketResponse.PaymentReferenceNumber = paymentId;

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);



                        return wsTicketResponse;
                    }
                    if (mseBookingResponse.Error != null && mseBookingResponse.Error != "")
                    {

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Ticketing Response error:" + mseBookingResponse.Error, "", Convert.ToString(wsTicketRequest.Source));


                        remarks = mseBookingResponse.Error;
                    }
                    wsTicketResponse.ConfirmationNo = mseBookingResponse.ConfirmationNo;
                    wsTicketResponse.PNR = mseBookingResponse.PNR;
                    wsTicketResponse.ProdType = ProductType.Flight;
                    wsTicketResponse.SSRDenied = mseBookingResponse.SSRDenied;
                    wsTicketResponse.SSRMessage = mseBookingResponse.SSRMessage;
                    status.Category = "TK";
                    remarks = mseBookingResponse.Error;
                    if (mseBookingResponse.Status == BookingResponseStatus.BookedOther)
                    {
                        status.StatusCode = "11";
                    }
                    else if (mseBookingResponse.Status == BookingResponseStatus.OtherClass)
                    {
                        status.StatusCode = "12";
                    }
                    else if (mseBookingResponse.Status == BookingResponseStatus.OtherFare)
                    {
                        status.StatusCode = "13";
                    }
                    else if (mseBookingResponse.Status == BookingResponseStatus.Successful)
                    {
                        status.StatusCode = "14";
                        remarks = "Sucessfull";
                        wsTicketResponse.BookingId = mseBookingResponse.BookingId.ToString();
                    }
                    else if (mseBookingResponse.Status == BookingResponseStatus.Failed)
                    {
                        status.StatusCode = "10";
                    }
                    status.Description = remarks;
                    wsTicketResponse.Status = status;

                    Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Ticketing Status:" + status.Description, ipAddr, Convert.ToString(wsTicketRequest.Source));

                    if(mseBookingResponse.Status != BookingResponseStatus.Successful)
                    {
                        SendErrorMail("Ticketing failed for LCC", userMaster, new Exception(mseBookingResponse.Error), wsTicketRequest);
                    }

                    if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = mseBookingResponse.BookingId;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                        cardInfo.Charges = wsTicketRequest.PaymentInformation.Charges;
                        cardInfo.Remarks = "Payment Successful";
                        cardInfo.Save();
                    }
                    wsTicketResponse.PaymentReferenceNumber = paymentId;

                    //Log the ticket response in DB
                    strTicketResponseXml = GetTicketXml(wsTicketResponse);
                    bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                    return wsTicketResponse;
                }
                    #endregion
                #endregion

                #region Ticketing for Non LCC flights
                if (wsTicketRequest.BookingID != null && wsTicketRequest.BookingID.Trim() != string.Empty)
                {
                    //set booking status to ready                
                    string pnr = "";
                    try
                    {
                        BookingDetail bd = new BookingDetail(Convert.ToInt32(wsTicketRequest.BookingID));
                        pnr = FlightItinerary.GetPNRByFlightId(Product.GetProductIdByBookingId(Convert.ToInt32(wsTicketRequest.BookingID), ProductType.Flight)).Trim();
                        if (bd.Status == BookingStatus.Hold)
                        {
                            BookingDetail.SetBookingStatus(Convert.ToInt32(wsTicketRequest.BookingID), BookingStatus.Ready, (int)userMaster.ID);
                        }
                        else if (bd.Status == BookingStatus.Ticketed)
                        {
                            status.Category = "TK";
                            status.StatusCode = "09";
                            status.Description = "Ticket created Successfully";
                            wsTicketResponse.Status = status;
                            wsTicketResponse.BookingId = wsTicketRequest.BookingID;
                            wsTicketResponse.PNR = pnr;
                            wsTicketResponse.ProdType = ProductType.Flight;
                            wsTicketResponse.PaymentReferenceNumber = paymentId;

                            //Log the ticket response in DB
                            strTicketResponseXml = GetTicketXml(wsTicketResponse);
                            bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                            

                            return wsTicketResponse;
                        }

                    }
                    catch (Exception ex)
                    {

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Ticketing for nonLCC: FlightItinerary.GetPNRByFlightId Failed. Booking Id:" + wsTicketRequest.BookingID + ex.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));

                        status.Category = "TK";
                        status.StatusCode = "24";
                        status.Description = "Technical Problem 3";
                        wsTicketResponse.Status = status;
                        wsTicketResponse.ProdType = ProductType.Flight;
                        SendErrorMail("while getting PNR / update status", userMaster, ex);
                        if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        wsTicketResponse.PaymentReferenceNumber = paymentId;

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                        return wsTicketResponse;
                    }

                    #region Check whether a agent is capable for Non LCC ticketing or not
                    decimal bookingAmount = 0;
                    if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType != PaymentModeType.CreditCard)
                    {
                        try
                        {
                            //AgentMastertLccLimit = Ledger.IsAgentCapable(AgentMasterId, AccountType.NonLCC);
                            AgentMastertLccLimit = AgentLoginInfo.AgentBalance;
                            bookingAmount = BookingDetail.GetBookingPrice(Convert.ToInt32(wsTicketRequest.BookingID));
                        }
                        catch (Exception ex)
                        {

                            Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Ticketing for nonLCC: FlightItinerary.GetPNRByFlightId Failed. Booking Id:" + wsTicketRequest.BookingID + ex.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));

                            status.Category = "TK";
                            status.StatusCode = "24";
                            status.Description = "Technical Problem 5";
                            wsTicketResponse.Status = status;
                            wsTicketResponse.ProdType = ProductType.Flight;
                            SendErrorMail("while checking AgentMaster Non LCC balance", userMaster, ex);
                            if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                            {
                                cardInfo.ReferenceId = 0;
                                cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                                cardInfo.Save();
                            }

                            //Log the ticket response in DB
                            strTicketResponseXml = GetTicketXml(wsTicketResponse);
                            bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                            return wsTicketResponse;
                        }
                    }
                    if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType != PaymentModeType.CreditCard && AgentMastertLccLimit < bookingAmount && userMaster.AgentId > 0)
                    {
                        status.Category = "TK";
                        status.Description = "Credit balance is less than ticket amount";
                        status.StatusCode = "15";
                        wsTicketResponse.Status = status;

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Non Lcc balance of AgentMaster is less than ticketed amount", ipAddr, Convert.ToString(wsTicketRequest.Source));

                        wsTicketResponse.ProdType = ProductType.Flight;

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                        return wsTicketResponse;
                    }
                    #endregion
                    string corporateCode = string.Empty;
                    TicketingResponse ticketingResponse = new TicketingResponse();
                    try
                    {
                        /***********************************************************************************************************
                         * For Non LCC airlines Ticketing restriction is checked
                         * *********************************************************************************************************/
                        if (preference != null && preference.AllowTicketing)
                        {
                            mse.SettingsLoginInfo = AgentLoginInfo;
                            mse.SabreAuthenicateToken = wsTicketRequest.Remarks;
                            ticketingResponse = mse.Ticket(pnr, new UserMaster((int)userMaster.ID), corporateCode, ipAddr);

                            /////////////////////////////////////////////////////////////////
                            ticketingResponse.PNR = pnr;
                            status.Category = "TK";
                            if (ticketingResponse.Message != null)
                            {
                                remarks = ticketingResponse.Message.Trim();
                            }
                            string description = string.Empty;
                            if (ticketingResponse.Status == TicketingResponseStatus.InProgress)
                            {
                                description = "Ticket is In-Progress " + remarks;
                                status.StatusCode = "03";
                            }
                            else if (ticketingResponse.Status == TicketingResponseStatus.NotAllowed)
                            {
                                description = "Eticket not allowed " + (string.IsNullOrEmpty(remarks) ? string.Empty : " and " + remarks);
                                status.StatusCode = "04";
                            }
                            else if (ticketingResponse.Status == TicketingResponseStatus.NotCreated)
                            {
                                description = "Ticket not created " + remarks;
                                status.StatusCode = "05";
                            }
                            else if (ticketingResponse.Status == TicketingResponseStatus.NotSaved)
                            {
                                description = "Ticket created but not saved " + remarks;
                                status.StatusCode = "06";
                            }
                            else if (ticketingResponse.Status == TicketingResponseStatus.OtherError)
                            {
                                description = "Unhandle Error " + remarks;
                                status.StatusCode = "07";
                            }
                            else if (ticketingResponse.Status == TicketingResponseStatus.PriceChanged)
                            {
                                description = "Fare is not available at time of ticketing " + remarks;
                                status.StatusCode = "08";
                            }
                            else if (ticketingResponse.Status == TicketingResponseStatus.Successful)
                            {
                                description = "Ticket created Successfully";
                                status.StatusCode = "09";
                                //if (isPGOwnedBYTBO && wsTicketRequest.IsOneWayBooking && Credential.SiteName != null)
                                //{
                                //    InvoiceSettlement(paymentId, (int)userMaster.ID, Convert.ToInt32(wsTicketRequest.BookingID), false);
                                //}
                                //else if (isPGOwnedBYTBO && !wsTicketRequest.IsOneWayBooking && Credential.SiteName != null)
                                //{
                                //    paymentId = wsTicketRequest.PaymentInformation.PaymentInformationId;
                                //    InvoiceSettlement(paymentId, (int)userMaster.ID, Convert.ToInt32(wsTicketRequest.BookingID), false);
                                //}

                                try
                                {
                                    itinerary = new FlightItinerary(FlightItinerary.GetFlightId(ticketingResponse.PNR));
                                    Invoice invoice = new Invoice();
                                    int invoiceNumber = 0;

                                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.FlightId, "", (int)userMaster.ID, ProductType.Flight, 1);
                                    if (invoiceNumber > 0)
                                    {
                                        invoice.Load(invoiceNumber);
                                        invoice.Status = InvoiceStatus.Paid;
                                        invoice.CreatedBy = (int)userMaster.ID;
                                        invoice.LastModifiedBy = (int)userMaster.ID;
                                        invoice.UpdateInvoice();
                                    }

                                    #region Added Promotion
                                    try
                                    {
                                        //For UAPI PromoDetails are saved while Holding 
                                        //if (itinerary.FlightBookingSource != BookingSource.UAPI)
                                        //{
                                        //    PromoDetails promoDet = new PromoDetails();
                                        //    promoDet.PromoId = wsTicketRequest.PromoDetail.PromoId;
                                        //    promoDet.PromoCode = wsTicketRequest.PromoDetail.PromoCode;
                                        //    promoDet.ProductId = (int)ProductType.Flight;
                                        //    promoDet.DiscountType = wsTicketRequest.PromoDetail.PromoDiscountType;
                                        //    promoDet.DiscountValue = wsTicketRequest.PromoDetail.PromoDiscountValue;
                                        //    promoDet.DiscountAmount = wsTicketRequest.PromoDetail.PromoDiscountAmount;
                                        //    promoDet.ReferenceId = itinerary.FlightId;
                                        //    promoDet.CreatedBy = (int)UserId;
                                        //    promoDet.Save();
                                        //}
                                    }
                                    catch 
                                    {

                                        //Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ")B2C-Flight API :Exception while saving PromoDetails.. Message:" + ex.ToString(), "", Convert.ToString(wsTicketRequest.Source));

                                    }

                                    #endregion
                                }
                                catch (Exception ex)
                                {

                                    Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") B2C-Ticketing Invoice Failed: " + ex.ToString() + " MSE Message: " + ticketingResponse.Message, ipAddr, Convert.ToString(wsTicketRequest.Source));

                                }
                            }
                            status.Description = description;
                            wsTicketResponse = WSTicketResponse.ReadTicketResponse(ticketingResponse, string.Empty, false, status, ProductType.Flight, paymentId);
                            wsTicketResponse.BookingId = wsTicketRequest.BookingID;
                            wsTicketResponse.ProdType = ProductType.Flight;
                            wsTicketResponse.PaymentReferenceNumber = paymentId;
                            if (ticketingResponse.Status != TicketingResponseStatus.Successful)
                            {
                                SendErrorMail(ticketingResponse.Message, userMaster, new Exception(ticketingResponse.Message), wsTicketRequest.SessionId);
                            }
                            Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") B2C-Ticketing Status:" + status.Description + " MSE Message: " + ticketingResponse.Message, ipAddr, Convert.ToString(wsTicketRequest.Source));

                        }
                        else //Return the proper ticketing response if ticketing not allowed
                        {                            
                            ticketingResponse.Status = TicketingResponseStatus.NotAllowed;
                            string description = "Eticket not allowed " + (string.IsNullOrEmpty(remarks) ? string.Empty : "and " + remarks);//Correcting the error msg
                            status.StatusCode = "04";
                            status.Description = description;

                            wsTicketResponse = WSTicketResponse.ReadTicketResponse(ticketingResponse, string.Empty, false, status, ProductType.Flight, paymentId);
                            wsTicketResponse.BookingId = wsTicketRequest.BookingID;
                            wsTicketResponse.ProdType = ProductType.Flight;
                            wsTicketResponse.PaymentReferenceNumber = paymentId;
                            Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") B2C-Ticketing Not Allowed :" + status.Description + " MSE Message: " + ticketingResponse.Message, ipAddr, Convert.ToString(wsTicketRequest.Source));
                            SendErrorMail("ETicket not allowed", userMaster, new Exception(description), wsTicketRequest.SessionId);
                        }
                    }
                    catch (ArgumentException ex)
                    {

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Fail to ticket requested itinerary: " + ex.ToString(), ipAddr, Convert.ToString(wsTicketRequest.Source));

                        status.Category = "TK";
                        status.StatusCode = "02";
                        status.Description = ex.Message;
                        wsTicketResponse.ProdType = ProductType.Flight;
                        wsTicketResponse.Status = status;
                        SendErrorMail("GDS Ticketing failed from MSE", userMaster, ex, wsTicketRequest);
                        if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        wsTicketResponse.PaymentReferenceNumber = paymentId;

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);
                        
                        return wsTicketResponse;
                    }
                    catch (Exception ex)
                    {

                        Audit.Add(EventType.Ticketing, Severity.High, UserId, "(" + Credential.SiteName + ") Ticketing Failed: " + ex.ToString() + " MSE Message: " + ticketingResponse.Message, ipAddr, Convert.ToString(wsTicketRequest.Source));
                        status.Category = "TK";
                        status.Description = "Technical Problem 4";
                        status.StatusCode = "24";
                        wsTicketResponse.Status = status;
                        wsTicketResponse.ProdType = ProductType.Flight;
                        SendErrorMail("while Ticketing of GDS from MSE", userMaster, ex);
                        if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard && wsTicketRequest.IsOneWayBooking)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        wsTicketResponse.PaymentReferenceNumber = paymentId;

                        //Log the ticket response in DB
                        strTicketResponseXml = GetTicketXml(wsTicketResponse);
                        bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);
                        
                        return wsTicketResponse;
                    }
                }
                #endregion
            }
            else
            {
                wsTicketResponse.Status.Category = "TK";
                wsTicketResponse.Status.Description = "Authentication Failed";
                wsTicketResponse.Status.StatusCode = "01";
                wsTicketResponse.ProdType = ProductType.Flight;
                if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
                {
                    cardInfo.ReferenceId = 0;
                    cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;                    
                    cardInfo.Save();
                }

                //Log the ticket response in DB
                strTicketResponseXml = GetTicketXml(wsTicketResponse);
                bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

                return wsTicketResponse;
            }
            if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
            {
                cardInfo.ReferenceId = Convert.ToInt32(wsTicketResponse.BookingId);
                cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                cardInfo.Remarks = "Payment Successful";//Update success remarks
                cardInfo.PaymentStatus = 1;//Success
                cardInfo.UpdatePaymentDetails();
            }

            //Log the ticket response in DB
            strTicketResponseXml = GetTicketXml(wsTicketResponse);
            bSaveStatus = SaveTicketXML(userMaster, oldSessionId, strTicketRequestXml, strTicketResponseXml, strCredentials, (int)userMaster.ID);

        }
        catch (Exception ex)
        {
            Email.Send(ConfigurationSystem.Email["fromEmail"], "shiva@cozmotravel.com", "Ticketing failed for " + wsTicketRequest.Source.ToString() + " | " + wsTicketRequest.Origin + "-" + wsTicketRequest.Destination + " | Pax:" + wsTicketRequest.Passenger[0].FirstName + " " + wsTicketRequest.Passenger[0].LastName, "<p>Dear Team,</p><p>There is an issue while creating ticket. Reason: " + ex.ToString() + "</p><p>Sent from BookingAPI</p>");
            throw ex;
        }
        return wsTicketResponse;
    }

    private bool SaveTicketXML(UserMaster UserMaster, string strSession, string strTicketRequestXml, string strTicketResponseXml, string strCredentials, int iAccountId)
    {
        string ipAddr = this.Context.Request.UserHostAddress;
        
        SqlParameter[] paramList = new SqlParameter[5];
        paramList[0] = new SqlParameter("@SessionId", strSession);
        paramList[1] = new SqlParameter("@RequestXML", strTicketRequestXml);
        paramList[2] = new SqlParameter("@ResponseXML", strTicketResponseXml);
        paramList[3] = new SqlParameter("@Credentials", strCredentials);
        paramList[4] = new SqlParameter("@CreatedBy", iAccountId);

        try
        {
            int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddEditTicketRequest", paramList);
            if (rowsAffected > 0)
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, "Error in SaveTicketXML method. Error Message " + ex.ToString() + " Stack Trace " + ex.StackTrace, ipAddr);
            return false;
        }
    }

    private bool ValidateSessionValue(string strSession)
    {
        if (strSession == null || strSession == "")
        {
            statusDescription = "Session value is not supplied.";
            statusCode = "02";
            return false;
        }
        return true;
    }

    [WebMethod(EnableSession=true)]
    [SoapHeader("Credential")]
    public WSTicketResponse GetTicketResponse(string SessionId)
    {
        UserMaster UserMaster = new UserMaster();
        WSStatus status = new WSStatus();
        UserMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        WSTicketResponse wsTicketResponse = new WSTicketResponse();
        if (UserMaster != null)
        {
            if (ValidateSessionValue(SessionId))
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@SessionId", SessionId);
                    SqlConnection sqlcon = DBGateway.GetConnection();
                    SqlDataReader dr = DBGateway.ExecuteReaderSP("usp_GetTicketResponse", paramList, sqlcon);
                    if (dr.HasRows)
                    {
                        dr.Read();
                        System.IO.StringReader read = new StringReader(dr["ResponseXML"].ToString());
                        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(wsTicketResponse.GetType());
                        System.Xml.XmlReader reader = new XmlTextReader(read);
                        wsTicketResponse = (WSTicketResponse)serializer.Deserialize(reader);
                        return wsTicketResponse;
                    }
                    else
                    {
                        status.Category = "RB";
                        status.StatusCode = "03";
                        status.Description = "Session Id does not exits.";
                        wsTicketResponse.Status = status;
                        wsTicketResponse.ProdType = ProductType.Flight;
                        return wsTicketResponse;
                    }
                }
                catch (Exception ex)
                {
                    Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, "Error in GetTicketResponse method. Error Message " + ex.ToString() + " Stack Trace " + ex.StackTrace, ipAddr);
                    status.Category = "RB";
                    status.StatusCode = "03";
                    status.Description = "Error in retrieving ticketing response " + ex.Message;
                    wsTicketResponse.Status = status;
                    wsTicketResponse.ProdType = ProductType.Flight;
                    return wsTicketResponse;
                }
            }
            else
            {
                status.Category = "RB";
                status.StatusCode = statusCode;
                status.Description = statusDescription;
                wsTicketResponse.Status = status;
                wsTicketResponse.ProdType = ProductType.Flight;
                return wsTicketResponse;
            }
        }
        else
        {
            wsTicketResponse.Status = new WSStatus();
            wsTicketResponse.Status.Category = "RB";
            wsTicketResponse.Status.Description = "Authentication Failed for GetTicketResponse method";
            wsTicketResponse.Status.StatusCode = "01";
            wsTicketResponse.ProdType = ProductType.Flight;
            return wsTicketResponse;
        }
    }

    [WebMethod(EnableSession=true)]
    [SoapHeader("Credential")]
    public WSTicketResponse LCCTicket(WSTicketRequest wsTicketRequest, string bookRequestXML)
    {
        UserPreference pref = new UserPreference();
        int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
        AgentLoginInfo = UserMaster.GetB2CUser(UserId);
        WSTicketResponse wsTicketResponse = new WSTicketResponse();
        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        decimal AgentMastertLccLimit = 0;
        if (userMaster != null)
        {
            //Add the XML
            Audit.Add(EventType.Ticketing, Severity.High, (int)userMaster.ID, "B2C-Ticket Request XML : " + GetTicketRequestXml(wsTicketRequest), "");
            Audit.Add(EventType.Ticketing, Severity.High, (int)userMaster.ID, "B2C-Navitaire XML : " + bookRequestXML, "");
            bool isLCC = false;
            if (wsTicketRequest.BookingID != null && wsTicketRequest.BookingID.Trim() != string.Empty)
            {
                isLCC = false;
            }
            else if (wsTicketRequest.Source != BookingSource.Amadeus && wsTicketRequest.Source != BookingSource.Sabre && wsTicketRequest.Source != BookingSource.WorldSpan && wsTicketRequest.Source != BookingSource.Galileo && wsTicketRequest.Source != BookingSource.UAPI)
            {
                isLCC = true;
            }

            #region validate input parameter
            if (wsTicketRequest.SessionId == null || !MetaSearchEngine.IsActiveSession(wsTicketRequest.SessionId))
            {
                MetaSearchEngine mseNew = new MetaSearchEngine();
                string sessionId = mseNew.StartSession();
                wsTicketRequest.SessionId = sessionId;
                Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)userMaster.ID, "New sessionId generated " + sessionId, ipAddr);
            }
            if (!(WSTicketInput(wsTicketRequest)))
            {
                WSTicketResponse errorResponse = new WSTicketResponse();
                errorResponse.ProdType = ProductType.Flight;
                WSStatus wsStatus = new WSStatus();
                wsStatus.Category = "TK";
                wsStatus.Description = statusDescription;
                wsStatus.StatusCode = statusCode;
                errorResponse.Status = wsStatus;
                errorResponse.ProdType = ProductType.Flight;
                Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.Normal, (int)userMaster.ID, "Input parameter is incorrect: " + statusDescription, ipAddr);
                return errorResponse;
            }

            #endregion

            WSStatus status = new WSStatus();

            MetaSearchEngine mse = new MetaSearchEngine(wsTicketRequest.SessionId);
            string remarks = string.Empty;


            #region assigning AgentMaster

            int AgentMasterId;
            if (userMaster.AgentId > 0)
            {
                AgentMasterId = userMaster.AgentId;
            }
            else
            {
                ConfigurationSystem config = new ConfigurationSystem();
                AgentMasterId = AgentLoginInfo.AgentId; //Convert.ToInt32(config.GetSelfAgencyId()["selfAgentMasterId"]);
            }
            #endregion

            #region Ticketing for LCC
            if (isLCC)
            {
                FlightItinerary itinerary = new FlightItinerary();
                WSBookRequest wsBookRequest = new WSBookRequest();
                BookingResponse mseBookingResponse = new BookingResponse();

                #region Check whether a agent is capable for LCC booking or not
                //AgentMastertLccLimit = Ledger.IsAgentCapable(AgentMasterId, AccountType.LCC);
                AgentMastertLccLimit = AgentLoginInfo.AgentBalance;
                if (wsTicketRequest.PaymentInformation != null && wsTicketRequest.PaymentInformation.PaymentModeType == PaymentModeType.Deposited && AgentMastertLccLimit < GetTicketPrice(wsTicketRequest.Fare) && userMaster.AgentId > 0)
                {
                    status.Category = "TK";
                    status.Description = "LCC balance is less than ticket amount";
                    status.StatusCode = "15";
                    wsTicketResponse.Status = status;
                    Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.Normal, (int)userMaster.ID, "Lcc balance of AgentMaster is less than ticketed amount", ipAddr);

                    wsTicketResponse.ProdType = ProductType.Flight;
                    wsTicketResponse.PaymentReferenceNumber = "";
                    return wsTicketResponse;
                }
                #endregion

                #region Building itinerary for MSE
                wsBookRequest.Origin = wsTicketRequest.Origin;
                wsBookRequest.Destination = wsTicketRequest.Destination;
                wsBookRequest.Fare = wsTicketRequest.Fare;
                wsBookRequest.FareBasis = wsTicketRequest.FareBasis;
                wsBookRequest.FareType = wsTicketRequest.FareType;
                wsBookRequest.Segment = wsTicketRequest.Segment;
                wsBookRequest.Passenger = wsTicketRequest.Passenger;
                wsBookRequest.Source = wsTicketRequest.Source;
                wsBookRequest.SessionId = wsTicketRequest.SessionId;
                wsBookRequest.PaymentInformation = wsTicketRequest.PaymentInformation;
                try
                {
                    itinerary = BuildItinerary(wsBookRequest, (int)userMaster.ID, AgentMasterId);
                    Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.Low, (int)userMaster.ID, "Itinerary suceessfully build ", "");
                }
                catch (ArgumentException ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "16";
                    status.Description = ex.Message;
                    wsTicketResponse.Status = status;
                    wsTicketResponse.ProdType = ProductType.Flight;
                    Audit.AddAuditBookingAPI(EventType.Book, Severity.High, (int)userMaster.ID, "Book: Build Itinerary failed: " + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                    wsTicketResponse.PaymentReferenceNumber = "";
                    return wsTicketResponse;
                }
                catch (Exception ex)
                {
                    Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)userMaster.ID, "Ticketing Status: error in BuildItinerary:" + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                    status.Category = "TK";
                    status.StatusCode = "24";
                    status.Description = "Technical Problem 1";
                    wsTicketResponse.Status = status;
                    wsTicketResponse.ProdType = ProductType.Flight;
                    SendErrorMail("while building itinerary of LCC airline for MSE", userMaster, ex, wsTicketRequest);
                    return wsTicketResponse;
                }
                #endregion

                #region Send request for LCC Ticketing
                try
                {
                    if (!ValidateTicketRequestXML(itinerary, bookRequestXML))
                    {
                        WSTicketResponse errorResponse = new WSTicketResponse();
                        errorResponse.ProdType = ProductType.Flight;
                        WSStatus wsStatus = new WSStatus();
                        wsStatus.Category = "TK";
                        wsStatus.Description = statusDescription;
                        wsStatus.StatusCode = statusCode;
                        errorResponse.Status = wsStatus;
                        errorResponse.ProdType = ProductType.Flight;
                        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.Normal, (int)userMaster.ID, "Input parameter is incorrect: " + statusDescription, ipAddr);
                        return errorResponse;
                    }
                    Product prod = itinerary;
                    List<string> AgentMasterAllowed = new List<string>(System.Configuration.ConfigurationManager.AppSettings["AgentMasterAllowedToBookByXML"].Split(','));
                    if (AgentMasterAllowed.Contains(AgentMasterId.ToString()))//MMT Specific
                    {
                        mseBookingResponse = mse.Book(ref prod, AgentMasterId, BookingStatus.Ticketed, userMaster.ID, true, BookingType.Book, itinerary.BookingMode);
                    }
                    else
                    {
                        mseBookingResponse = mse.Book(ref prod, AgentMasterId, BookingStatus.Ticketed, userMaster.ID, itinerary.BookingMode);
                    }
                }
                catch (ArgumentException ex)
                {
                    Audit.AddAuditBookingAPI(EventType.Book, Severity.High, (int)userMaster.ID, "Fail to book requested itinerary: " + ex.ToString(), ipAddr);
                    status.Category = "TK";
                    status.StatusCode = "02";
                    status.Description = ex.Message;
                    wsTicketResponse.Status = status;
                    wsTicketResponse.ProdType = ProductType.Flight;
                    wsTicketResponse.PaymentReferenceNumber = "";
                    return wsTicketResponse;
                }
                catch (Exception ex)
                {
                    Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)userMaster.ID, "Ticketing Status: " + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                    status.StatusCode = "10";
                    status.Category = "TK";
                    if (mseBookingResponse.Error != null && mseBookingResponse.Error.ToLower().Contains("bookingId not generated"))
                    {
                        status.Description = "Ticketing Failed : Transaction Failure";
                    }
                    else if (mseBookingResponse.Error != null && mseBookingResponse.Error.ToLower().Contains("timeout"))
                    {
                        status.Description = "Ticketing Failed : TimeOut";
                    }
                    else if (mseBookingResponse.Error != null)
                    {
                        status.Description = "Ticketing Failed : " + mseBookingResponse.Error;
                    }
                    else
                    {
                        status.Description = "Technical Fault 5";
                    }
                    wsTicketResponse.Status = status;
                    wsTicketResponse.ProdType = ProductType.Flight;
                    SendErrorMail("while ticketing of LCC from MSE", userMaster, ex, wsTicketRequest);

                    wsTicketResponse.PaymentReferenceNumber = "";
                    return wsTicketResponse;
                }
                if (mseBookingResponse.Error != null && mseBookingResponse.Error != "")
                {
                    Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.Normal, (int)userMaster.ID, "Ticketing Response error: " + mseBookingResponse.Error, "");
                    remarks = mseBookingResponse.Error;
                }
                wsTicketResponse.ConfirmationNo = mseBookingResponse.ConfirmationNo;
                wsTicketResponse.PNR = mseBookingResponse.PNR;
                wsTicketResponse.ProdType = ProductType.Flight;
                wsTicketResponse.SSRDenied = mseBookingResponse.SSRDenied;
                wsTicketResponse.SSRMessage = mseBookingResponse.SSRMessage;
                status.Category = "TK";
                if (mseBookingResponse.Status == BookingResponseStatus.BookedOther)
                {
                    status.StatusCode = "11";
                }
                else if (mseBookingResponse.Status == BookingResponseStatus.OtherClass)
                {
                    status.StatusCode = "12";
                }
                else if (mseBookingResponse.Status == BookingResponseStatus.OtherFare)
                {
                    status.StatusCode = "13";
                }
                else if (mseBookingResponse.Status == BookingResponseStatus.Successful)
                {
                    status.StatusCode = "14";
                    remarks = "Sucessfull";
                    wsTicketResponse.BookingId = mseBookingResponse.BookingId.ToString();
                }
                else if (mseBookingResponse.Status == BookingResponseStatus.Failed)
                {
                    status.StatusCode = "10";
                }
                status.Description = remarks;
                wsTicketResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)userMaster.ID, "Ticketing Status: " + status.Description, ipAddr);
                wsTicketResponse.PaymentReferenceNumber = "";
                return wsTicketResponse;
                #endregion
            }
            #endregion

        }
        else
        {
            wsTicketResponse.Status = new WSStatus();
            wsTicketResponse.Status.Category = "TK";
            wsTicketResponse.Status.Description = "Authentication Failed";
            wsTicketResponse.Status.StatusCode = "01";
            wsTicketResponse.ProdType = ProductType.Flight;
            return wsTicketResponse;
        }
        return wsTicketResponse;
    }

    
    /// <summary>
    /// Save Pnr in DB
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    private int BookItinerary(BookingStatus status, UserMaster UserMaster, FlightItinerary itinerary)
    {
        // Creating booking detail 
        BookingDetail booking = new BookingDetail();        
        int bookingId = 0;
        CT.Core.Queue queue = new CT.Core.Queue();        
        booking.AgencyId = UserMaster.AgentId;
        booking.CreatedBy = (int)UserMaster.ID;
        booking.Status = status;        
        BookingHistory bh = new BookingHistory();
        bh.EventCategory = EventCategory.Booking;
        bh.CreatedBy = (int)UserMaster.ID;
        bh.Remarks = "Booking created via import pnr from API";        
        itinerary.Origin = (string)itinerary.Segments[0].Origin.CityCode;
        itinerary.Destination = itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode;
        

        // Sending itinerary for booking -- TO DO: check
        
        // TO DO: Fare Rules
        //List<FareRule> fareRule = new List<FareRule>();
        //MetaSearchEngine mse = new MetaSearchEngine();
        //fareRule = mse.GetFareRule(itinerary);        
        //itinerary.FareRules = fareRule;
        queue.QueueTypeId = (int)QueueType.Booking;
        queue.StatusId = (int)QueueStatus.Assigned;
        queue.AssignedTo = (int)UserMaster.ID;
        queue.AssignedBy = (int)UserMaster.ID;
        queue.AssignedDate = DateTime.Now.ToUniversalTime();
        queue.CompletionDate = DateTime.Now.ToUniversalTime().AddDays(7);
        queue.CreatedBy = (int)UserMaster.ID;
        // Saving the booked itinerary to database
        try
        {
            using (TransactionScope updateTransaction = new TransactionScope())
            {
                booking.Save(itinerary, true);
                //UpdateAirlinePNR(itinerary);
                bh.BookingId = booking.BookingId;
                bh.Save();
                //TO DO : Check how bookingId will come
                queue.ItemId = booking.BookingId;
                queue.Save();                
                updateTransaction.Complete();
            }
            bookingId = booking.BookingId;
        }
        catch (ArgumentException excep)
        {
            throw new ArgumentException(excep.Message);
        }
        catch (BookingEngineException excep)
        {
            throw new BookingEngineException(excep.Message);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Ticketing, Severity.High, (int)UserMaster.ID, "B2C-Error while saving itinerary " + ex.ToString() + ex.StackTrace, "");
        }
        
        return bookingId;
    }

    private static void UpdateAirlinePNR(FlightItinerary itinerary)
    {
        int flightId = FlightItinerary.GetFlightId(itinerary.PNR);
        FlightInfo[] segment = FlightInfo.GetSegments(flightId);
        for (int i = 0; i < itinerary.Segments.Length; i++)
        {
            // if worldspan sends the segments in sequence.
            if (Convert.ToInt32(itinerary.Segments[i].FlightNumber) == Convert.ToInt32(segment[i].FlightNumber) &&
                itinerary.Segments[i].Airline == segment[i].Airline &&
                itinerary.Segments[i].Origin.AirportCode == segment[i].Origin.AirportCode &&
                itinerary.Segments[i].Destination.AirportCode == segment[i].Destination.AirportCode)
            {
                segment[i].AirlinePNR = itinerary.Segments[i].AirlinePNR;
                segment[i].Save();
            }
            else    // if not sequential.
            {
                for (int j = 0; j < segment.Length; j++)
                {
                    if (Convert.ToInt32(itinerary.Segments[i].FlightNumber) == Convert.ToInt32(segment[j].FlightNumber) &&
                        itinerary.Segments[i].Airline == segment[j].Airline &&
                        itinerary.Segments[i].Origin.AirportCode == segment[j].Origin.AirportCode &&
                        itinerary.Segments[i].Destination.AirportCode == segment[j].Destination.AirportCode)
                    {
                        segment[j].AirlinePNR = itinerary.Segments[i].AirlinePNR;
                        segment[j].Save();
                        break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// To maintain accounts in case of ticketing
    /// </summary>
    /// <param name="source"></param>
    /// <param name="trackId"></param>
    /// <param name="amount"></param>
    /// <param name="AgentMasterId"></param>
    /// <param name="UserMasterId"></param>
    /// <param name="isLCC"></param>
    /// <returns></returns>
    private int AcceptPayment(PaymentGatewaySource source, string trackId, decimal amount, int AgentMasterId, int UserMasterId, bool isLCC)
    {

        PaymentDetails pd = new PaymentDetails();
        decimal creditCardCharge = (decimal)AccountsDetail.GetCreditCardCharge((CT.AccountingEngine.PaymentGatewaySource)source);
        decimal amountToBeCredited = Math.Round((amount * 100) / (creditCardCharge + 100));
        decimal creditCardChargedAmount = amount;

        try
        {
            int AgentMasterTypeId =  AgentMaster.GetAgentType(AgentMasterId);
            #region Making PaymentDetail object

            pd.Amount = amountToBeCredited;
            pd.RemainingAmount = amountToBeCredited;
            pd.OurBankDetailId = AccountsDetail.GetOurBankDetailIdBySource((int)source);
            pd.ModeOfPayment = CT.AccountingEngine.PaymentMode.CreditCard;
            pd.Invoices = new List<InvoiceCollection>();
            pd.Status = CT.AccountingEngine.PaymentStatus.Accepted;
            pd.CreatedBy = UserMasterId;
            pd.LastModifiedBy = UserMasterId;
            pd.AgencyId = AgentMasterId;
            pd.Remarks = "Pay By Credit Card. TrackId: " + trackId;
            if (AgentMasterTypeId == 1)
            {
                pd.IsLCC = true;
            }
            else
            {
                pd.IsLCC = isLCC;
            }
            pd.PaymentDate = DateTime.Now;


            #endregion

            #region Making LedgerTransaction object

            LedgerTransaction ledgerTxn = new LedgerTransaction();
            
            ledgerTxn.LedgerId = AgentMasterId;
            ledgerTxn.ReferenceType = ReferenceType.PaymentRecieved;
            ledgerTxn.Amount = Convert.ToDecimal(pd.Amount);
            if (AgentMasterTypeId == 1)
            {
                ledgerTxn.IsLCC = true;
            }
            else
            {
                ledgerTxn.IsLCC = isLCC;
            }

            NarrationBuilder objNarration = new NarrationBuilder();

            if (pd.Remarks.Trim().Length > 0)
            {
                objNarration.Remarks = pd.Remarks;
            }
            else
            {
                objNarration.Remarks = "Payment Done";
            }
            ledgerTxn.Narration = objNarration;
            ledgerTxn.Notes = "Payment Done";
            ledgerTxn.Date = DateTime.UtcNow;
            ledgerTxn.CreatedBy = UserMasterId;

            #endregion

            #region Saving PaymentDetail and LedgerTransaction objects and updating AgentMaster balance

            using (TransactionScope updateTransaction = new TransactionScope())
            {
                pd.Save();
                ledgerTxn.ReferenceId = pd.PaymentDetailId;
                ledgerTxn.Save();
                //if (!isLCC)
                //{
                //    if (AgentMasterTypeId != 1)
                //    {
                //        Ledger.UpdateCurrentBalance(pd.AgencyId, pd.Amount, UserMasterId);
                //    }
                //    else
                //    {
                //        AgentMaster.UpdateLCCBalance(pd.AgencyId, pd.Amount, UserMasterId);
                //    }
                //}
                //else
                //{
                //    AgentMaster.UpdateLCCBalance(pd.AgencyId, pd.Amount, UserMasterId);
                //}
                updateTransaction.Complete();
            }

            #endregion

            string filePathForLogging = System.Configuration.ConfigurationManager.AppSettings["PaymentLog"];

            string detail = "Payment is done for track Id : " + trackId + " for AgentMasterId: " + AgentMasterId + " of amount= " + amountToBeCredited + " (" + creditCardChargedAmount + "Charged from Credit Card) on " + DateTime.Now + "\n";

            System.IO.File.AppendAllText(filePathForLogging, detail);
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.High, UserMasterId, "B2C-Error during recording entry in paymentDetail and ledger transaction fail of AgentMaster: " + AgentMasterId + " for amount = " + amount + ", due to: " + ex.ToString() + ex.StackTrace, "");
        }
        return pd.PaymentDetailId;
    }


    /// <summary>
    /// This function will be used to settle given invoice with given paymentDetailId
    /// and maintain booking history for the same.
    /// </summary>
    /// <param name="pnr"></param>
    /// <param name="paymentDetailId"></param>
    /// <param name="UserMasterId"></param>
    /// <param name="bookingId"></param>
    private void InvoiceSettlement(int paymentDetailId, int UserMasterId, int bookingId, bool isLCC)
    {

        PaymentDetails pd = new PaymentDetails();
        InvoiceCollection ic = new InvoiceCollection();
        string remarks = string.Empty;
        decimal invoiceAmount = 0;
        int flightId = Product.GetProductIdByBookingId(bookingId, ProductType.Flight);
        int invoiceNumber = Invoice.GetInvoiceNumberByFlightId(flightId);
        try
        {
            pd.Load(paymentDetailId);
            invoiceAmount = Invoice.GetTotalPrice(invoiceNumber);
            #region Initializing objects for invoice settlement process

            if (pd.RemainingAmount >= invoiceAmount)
            {
                pd.RemainingAmount = pd.RemainingAmount - invoiceAmount;
                ic.Amount = invoiceAmount;
                ic.InvoiceNumber = invoiceNumber;
                ic.IsPartial = false;
                ic.PaymentDetailId = pd.PaymentDetailId;
                remarks = "This Invoice is fully settled";
            }
            else
            {
                pd.RemainingAmount = 0;
                ic.Amount = pd.RemainingAmount;
                ic.InvoiceNumber = invoiceNumber;
                ic.IsPartial = true;
                ic.PaymentDetailId = pd.PaymentDetailId;
                remarks = "This invoice is partially settled by Rs." + ic.Amount.ToString();
            }

            pd.LastModifiedBy = UserMasterId;
            pd.Invoices = new List<InvoiceCollection>();
            pd.Invoices.Add(ic);
            #endregion


            #region Maintaining Booking History

            BookingHistory bh = new BookingHistory();
            bh.BookingId = bookingId;
            bh.EventCategory = EventCategory.Payment;
            bh.Remarks = remarks;
            bh.CreatedBy = UserMasterId;

            #endregion

            #region Saving data

            using (TransactionScope updateTransaction = new TransactionScope())
            {
                if (!isLCC)
                {
                    if (ic.IsPartial)
                    {
                        Invoice.UpdateInvoiceStatus(invoiceNumber, InvoiceStatus.PartiallyPaid, UserMasterId);
                    }
                    else
                    {
                        Invoice.UpdateInvoiceStatus(invoiceNumber, InvoiceStatus.Paid, UserMasterId);
                    }
                    pd.Save();
                    bh.Save();
                }
                updateTransaction.Complete();
            }

            #endregion
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.High, UserMasterId, "B2C-Error in InvoiceSettlement function for invoiceNumber: " + invoiceNumber + ", paymentDetailId: " + paymentDetailId + " due to : " + ex.ToString() + ex.StackTrace, "");

        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public void EmailTicket(int ticketId, string to)
    {
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        if (UserMaster != null)
        {
            Ticket ticket = new Ticket();
            ticket.Load(ticketId);
            List<string> toArray = new List<string>();
            string[] addressArray = to.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            for (int k = 0; k < addressArray.Length; k++)
            {
                if (!toArray.Contains(addressArray[k]))
                {
                    toArray.Add(addressArray[k]);
                }
            }
            Dictionary<string, string> prefList = UserPreference.GetPreferenceList((int)UserMaster.ID, ItemType.Flight);
            if (prefList.ContainsKey("EMAILID"))
            {
                string from = UserMaster.Email;
                try
                {
                    Email.Send(prefList["EMAILID"], prefList["EMAILID"], toArray, "E-Ticket", WSMail.BuildMessage(ticket), new Hashtable());
                }
                catch (Exception ex)
                {
                    Audit.AddAuditBookingAPI(EventType.Email, Severity.High, (int)UserMaster.ID, "Fail to sending email : " + ex.ToString(), "");
                }
            }
        }
    }

    public bool ValidateAddBookingRequest(WSAddBookingRequest saveRequest)
    {
        if (saveRequest == null)
        {
            statusDescription = "Request cannot be null";
            statusCode = "02";
            return false;
        }
        if (saveRequest.PaymentAmount <= 0)
        {
            statusDescription = "Amount cannot be less then or equal to zero";
            statusCode = "03";
            return false;
        }
        if (saveRequest.AirlineCodeOut == null || saveRequest.AirlineCodeOut.Trim().Length <= 0)
        {
            statusDescription = "Outbound airline code cannot be null or blank";
            statusCode = "04";
            return false;
        }
        if (saveRequest.Sector == null || saveRequest.Sector.Trim().Length <= 0)
        {
            statusDescription = "Sector cannot be null or blank";
            statusCode = "05";
            return false;
        }        
        if (saveRequest.FlightNumberOut == null || saveRequest.FlightNumberOut.Trim().Length <= 0)
        {
            statusDescription = "Outbound Flight Number cannot be null or blank";
            statusCode = "07";
            return false;
        }
        if ((int)saveRequest.BookingStatus == 0)
        {
            statusDescription = "Assign booking status";
            statusCode = "08";
            return false;
        }
        if (saveRequest.PassengerInfo == null || saveRequest.PassengerInfo.Trim() == string.Empty)
        {
            statusDescription = "Passenger xml cannot be null or blank";
            statusCode = "09";
            return false;
        }
        if (saveRequest.IsDomesticReturn && (saveRequest.AirlineCodeIn == null || saveRequest.AirlineCodeIn.Trim().Length <= 0))
        {
            statusDescription = "Inbound airline code cannot be null or blank in domestic return";
            statusCode = "10";
            return false;
        }
        if (saveRequest.IsDomesticReturn && (saveRequest.FlightNumberIn == null || saveRequest.FlightNumberIn.Trim().Length <= 0))
        {
            statusDescription = "Inbound Flight Number cannot be null or blank in domestic return";
            statusCode = "11";
            return false;
        }
        if (saveRequest.BookingStatus == WLBookingStatus.Ticketed && (saveRequest.BookingIdOut == null || saveRequest.BookingIdOut.Trim() == string.Empty))
        {
            statusDescription = "Outbound BookingID cannot be null or blank if booking status is ticketed";
            statusCode = "12";
            return false;
        }

        if (saveRequest.BookingStatus == WLBookingStatus.Ticketed && (saveRequest.OutPNR == null || saveRequest.OutPNR.Trim() == string.Empty))
        {
            statusDescription = "Outbound PNR cannot be null or blank if booking status is ticketed";
            statusCode = "13";
            return false;
        }
        if (saveRequest.BookingStatus == WLBookingStatus.Ticketed && saveRequest.IsDomesticReturn && (saveRequest.BookingIdOut == null || saveRequest.BookingIdOut.Trim() == string.Empty))
        {
            statusDescription = "Inbound BookingID cannot be null or blank if booking status is ticketed in domestic return";
            statusCode = "12";
            return false;
        }

        if (saveRequest.BookingStatus == WLBookingStatus.Ticketed && saveRequest.IsDomesticReturn && (saveRequest.OutPNR == null || saveRequest.OutPNR.Trim() == string.Empty))
        {
            statusDescription = "Inbound PNR cannot be null or blank if booking status is ticketed in domestic return";
            statusCode = "13";
            return false;
        }
        return true;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSAddBookingResponse AddBookingDetail(WSAddBookingRequest saveRequest)
    {
        UserPreference pref = new UserPreference();
        int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
        AgentLoginInfo = UserMaster.GetB2CUser(UserId);
        
        UserMaster userMaster = new UserMaster(AgentLoginInfo.UserID);
        WSAddBookingResponse saveResponse = new WSAddBookingResponse();
        WSStatus resStatus = new WSStatus();
        ApiCustomerType customerType;
        string authValue = string.Empty;
        userMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (userMaster != null)
        {
            if (Credential.SiteName != null && Credential.SiteName != "")
            {
                customerType = ApiCustomerType.WhiteLabel;
                authValue = Credential.SiteName;
            }
            else if (Credential.AccountCode != null && Credential.AccountCode != "")
            {
                customerType = ApiCustomerType.B2B2B;
                authValue = Credential.AccountCode;
            }
            else
            {
                customerType = ApiCustomerType.BookingApi;
                authValue = Credential.UserName;
            }                 
            WSStatus wsStatus = new WSStatus();
            wsStatus.Category = "SD";
            if (!(ValidateAddBookingRequest(saveRequest)))
            {                
                wsStatus.Description = statusDescription;
                wsStatus.StatusCode = statusCode;
                saveResponse.Status = wsStatus;
                Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.Normal, (int)userMaster.ID, "Input parameter is incorrect: " + statusDescription, ipAddr);
                return saveResponse;
            }
            saveRequest.UserMasterId = (int)userMaster.ID;
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            try
            {                
                saveResponse = saveRequest.Add(authValue, customerType, AgentLoginInfo.AgentId);
            }
            catch (Exception ex)
            {
                //Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, (int)userMaster.ID, "Add Trip Details failed: " + ex.Message + " Stack Trace = " + ex.StackTrace, ipAddr);
                Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, (int)userMaster.ID, "Add Trip Details failed Detail: " + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                SendErrorMail("while saving website booking", userMaster, ex);
                wsStatus.Description = "Detail Saving Failed";
                wsStatus.StatusCode = "01";
                saveResponse.Status = wsStatus;
            }
            return saveResponse;
        }
        else
        {
            resStatus.Category = "SD";
            resStatus.Description = "Validation Failed";
            resStatus.StatusCode = "2";
            saveResponse.Status = resStatus;
            Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, 0, "saveRequest.Add failed and saveResponse.Status.Description=" + saveResponse.Status.Description, ipAddr);
            return saveResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSAddPendingBookingResponse AddPendingBookingDetail(WSAddPendingBookingRequest saveRequest)
    {
        UserMaster UserMaster = new UserMaster();
        WSAddPendingBookingResponse saveResponse = new WSAddPendingBookingResponse();
        WSStatus resStatus = new WSStatus();
        UserMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (UserMaster != null)
        {
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            try
            {
                saveResponse = saveRequest.Add((int)UserMaster.ID, UserMaster.AgentId);
            }
            catch (Exception ex)
            {
                saveResponse.Status = new WSStatus();
                saveResponse.Status.Category = "SPD";
                saveResponse.Status.Description = "Details Not Saved";
                saveResponse.Status.StatusCode = "4";
                saveResponse.QueueId = "0";
                Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, (int)UserMaster.ID, "Add Pending Booking Details failed: " + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                SendErrorMail("while saving Pending booking", UserMaster, ex);
                return saveResponse;
            }
            return saveResponse;
        }
        else
        {
            resStatus.Category = "SPD";
            resStatus.Description = "Validation Failed";
            resStatus.StatusCode = "3";
            saveResponse.Status = resStatus;
            Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, 0, " AddPending Booking failed and saveResponse.Status.Description=" + saveResponse.Status.Description, ipAddr);
            return saveResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSUpdatePendingBookingResponse UpdatePendingBookingDetail(WSUpdatePendingBookingRequest updateRequest)
    {
        UserMaster UserMaster = new UserMaster();
        WSUpdatePendingBookingResponse updateResponse = new WSUpdatePendingBookingResponse();
        WSStatus resStatus = new WSStatus();
        UserMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (UserMaster != null)
        {
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            try
            {
                updateResponse = updateRequest.Update((int)UserMaster.ID);
            }
            catch (Exception ex)
            {
                updateResponse.Status = new WSStatus();
                updateResponse.Status.Category = "UPD";
                updateResponse.Status.Description = "Details Not Updated";
                updateResponse.Status.StatusCode = "4";
                updateResponse.QueueId = "0";
                Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, (int)UserMaster.ID, "Update Pending Booking Details failed: " + ex.ToString() + " Stack Trace = " + ex.StackTrace, ipAddr);
                SendErrorMail("while updating Pending booking", UserMaster, ex);
                return updateResponse;
            }
            return updateResponse;
        }
        else
        {
            resStatus.Category = "UPD";
            resStatus.Description = "Validation Failed";
            resStatus.StatusCode = "3";
            updateResponse.Status = resStatus;
            Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, 0, " Update Pending Booking failed and updateResponse.Status.Description=" + updateResponse.Status.Description, ipAddr);
            return updateResponse;
        }
    }

    private bool GetBookingScoreStatusInput(WSGetScoreRequest request)
    {
        if (request.IPAddress == null && request.IPAddress.Trim() == string.Empty)
        {
            statusDescription = "IP Address can not be null or blank";
            statusCode = "03";
            return false;
        }
        if (request.Email == null && request.Email.Trim() == string.Empty)
        {
            statusDescription = "Email can not be null or blank";
            statusCode = "04";
            return false;
        }
        if (request.Passenger == null)
        {
            statusDescription = "Passenger can not be null";
            statusCode = "05";
            return false;
        }
        if (request.PaymentInformation == null)
        {
            statusDescription = "Payment Info can not be null or blank";
            statusCode = "06";
            return false;
        }
        if (request.Phone == null && request.Phone.Trim() == string.Empty)
        {
            statusDescription = "Phone can not be null or blank";
            statusCode = "07";
            return false;
        }
        if (request.Segment == null)
        {
            statusDescription = "Segment can not be null or blank";
            statusCode = "08";
            return false;
        }
        return true;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetScoreResponse GetBookingScoreStatus(WSGetScoreRequest request)
    {
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        WSStatus status = new WSStatus();
        WSGetScoreResponse response = new WSGetScoreResponse();
        status.Category = "GS";
        if (UserMaster != null)
        {
            try
            {
                //response = WSGetScoreRequest.GetStatus(request, UserMaster);// Temp SKipping 
                WSGetScoreResponse tempResponse = new WSGetScoreResponse();
                tempResponse.ScoreStatus = ScoreStaus.Genuine;
                response = tempResponse;
                return response;
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.Exception, Severity.High, 0, "Error : " + ex.ToString() + ex.StackTrace, "");
                status.Description = "Technical fault";
                status.StatusCode = "11";
                response.Status = status;
                response.ScoreStatus = ScoreStaus.Genuine;
                return response;
            }
        }
        else
        {
            status.Description = "Validation Failed";
            status.StatusCode = "11";
            response.Status = status;
            response.ScoreStatus = ScoreStaus.Genuine;
            Audit.AddAuditBookingAPI(EventType.Exception, Severity.High, 0, "Validation Failed", "");
            return response;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSSaveIPAddressResponse GetIPAddressStatus(string ipAddress)
    {
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (UserMaster != null)
        {
            WSStatus status = new WSStatus();
            WSSaveIPAddressResponse response = new WSSaveIPAddressResponse();
            response.Status = new WSStatus();
            IPAddressDetails ipAddressDetails = new IPAddressDetails();
            if (!(WSGetIPAddressStatusInput(ipAddress)))
            {
                status.Category = "GIP";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                response.Status = status;
                response.IPAddressStatus = IPAddressStatus.Normal;
                Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.High, (int)UserMaster.ID, " Invalid input : " + statusDescription, ipAddr);
                return response;
            }
            try
            {
                bool isIpAddressEntryExists = IPAddressDetails.IsIPAddressExists(ipAddress);
                if (!isIpAddressEntryExists)
                {
                    int isSaved = 0;
                    isSaved = SaveIPAddress(ipAddress);
                    if (isSaved < 0)
                    {
                        status.Category = "GIP";
                        status.StatusCode = "04";
                        status.Description = "IP address not Saved.";
                        response.Status = status;
                        response.IPAddressStatus = IPAddressStatus.Normal;
                        Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.Normal, (int)UserMaster.ID, status.Description + "\nIPAddress : " + ipAddress, ipAddr);
                        return response;
                    }
                }
                IPAddressStatus ipStatus = IPAddressDetails.GetIPAddressStatus(ipAddress);
                response.Status.Category = "GIP";
                response.Status.StatusCode = "01";
                response.Status.Description = "GetIPAddressStatus successful!!";
                response.IPAddressStatus = ipStatus;
                Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.Normal, (int)UserMaster.ID, response.Status.Description, ipAddr);
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "GIP";
                response.Status.StatusCode = "05";
                response.Status.Description = "Technical Fault 01. Error : " + ex.Message;
                response.IPAddressStatus = IPAddressStatus.Normal;
                Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.High, (int)UserMaster.ID, "\n Error : " + response.Status.Description + "\nError Message : " + ex.ToString() + " StackTrace : " + ex.StackTrace, ipAddr);
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "GIP";
                response.Status.StatusCode = "06";
                response.Status.Description = "Technical Fault 02"; ;
                response.IPAddressStatus = IPAddressStatus.Normal;
                Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.High, (int)UserMaster.ID, "\n Error : " + response.Status.Description + "\nError Message : " + ex.ToString() + " StackTrace : " + ex.StackTrace, ipAddr);
                return response;
            }
        }
        else
        {
            WSSaveIPAddressResponse getIPStatusResponse = new WSSaveIPAddressResponse();
            getIPStatusResponse.Status.StatusCode = "02";
            getIPStatusResponse.Status.Description = "Authentication Failed";
            getIPStatusResponse.Status.Category = "GIP";
            getIPStatusResponse.IPAddressStatus = IPAddressStatus.Normal;
            Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.High, (int)UserMaster.ID, " Authentication Failed", ipAddr);
            return getIPStatusResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSSaveIPAddressResponse GetPaidIPInfo(string ipAddress)
    {
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (UserMaster != null)
        {
            WSStatus status = new WSStatus();
            WSSaveIPAddressResponse response = new WSSaveIPAddressResponse();
            response.Status = new WSStatus();
            IPAddressDetails ipAddressDetails = new IPAddressDetails();
            ipAddress = ipAddress.Trim();
            if (!(WSGetIPAddressStatusInput(ipAddress)))
            {
                status.Category = "GIP";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                response.Status = status;
                response.IPAddressStatus = IPAddressStatus.Normal;
                Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.High, (int)UserMaster.ID, " Invalid input : " + statusDescription, ipAddr);
                return response;
            }
            //try
            {
                bool isIpAddressEntryExists = IPAddressDetails.IsIPAddressExists(ipAddress);
                bool isIPDetailSave = true;
                if (isIpAddressEntryExists)
                {
                    ipAddressDetails.Load(ipAddress);
                    if (ipAddressDetails.CountryCode == null || ipAddressDetails.CountryCode.Trim() == string.Empty || ipAddressDetails.CountryCode.ToUpper().Trim() == "XX")
                    {
                        isIPDetailSave = false;
                    }
                    else if (ipAddressDetails.CountryName == null || ipAddressDetails.CountryName.Trim() == string.Empty || ipAddressDetails.CountryName.ToUpper().Contains("UNKNOWN"))
                    {
                        isIPDetailSave = false;
                    }
                    else if (ipAddressDetails.City == null || ipAddressDetails.City.Trim() == string.Empty || ipAddressDetails.City.ToUpper().ToUpper().Contains("UNKNOWN"))
                    {
                        isIPDetailSave = false;
                    }
                    else if (ipAddressDetails.Region == null || ipAddressDetails.Region.Trim() == string.Empty)
                    {
                        isIPDetailSave = false;
                    }
                }
                else
                {
                    isIPDetailSave = false;
                }
                //if (!isIpAddressEntryExists || !isIPDetailSave)
                if (false) // Skipping as it shows error- Ziyad
                {
                    int isSaved = 0;
                    BookingAPIWebApp.IPService.Ip2LocationWebService ipService = new BookingAPIWebApp.IPService.Ip2LocationWebService();
                    string license = ConfigurationManager.AppSettings["IPServiceLicense"];
                    try
                    {
                        BookingAPIWebApp.IPService.IP2LOCATION ipDetail = new BookingAPIWebApp.IPService.IP2LOCATION();
                        ipDetail = ipService.IP2Location(ipAddress, license);
                        ipAddressDetails = new IPAddressDetails();
                        ipAddressDetails.IPAddress = ipAddress;
                        ipAddressDetails.CountryCode = ipDetail.COUNTRYCODE;
                        ipAddressDetails.City = ipDetail.CITY;
                        ipAddressDetails.CountryName = ipDetail.COUNTRYNAME;
                        ipAddressDetails.CreatedBy = (int)UserMaster.ID;
                        ipAddressDetails.DomainName = ipDetail.DOMAINNAME;
                        ipAddressDetails.IspName = ipDetail.ISPNAME;
                        ipAddressDetails.Latitude = ipDetail.LATITUDE;
                        ipAddressDetails.Longitude = ipDetail.LONGITUDE;
                        ipAddressDetails.Region = ipDetail.REGION;
                        ipAddressDetails.Zipcode = ipDetail.ZIPCODE;
                        if (Convert.ToInt32(ipDetail.CREDITSAVAILABLE) <= 20)
                        {
                            Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.Normal, (int)UserMaster.ID, "Credit availabel of Paid IP Service : " + ipDetail.CREDITSAVAILABLE, "");
                        }
                        if (ipAddressDetails.CountryCode.ToUpper() != "IN")
                        {
                            ipAddressDetails.IPAddressStatus = IPAddressStatus.Suspicious;
                        }
                        else
                        {
                            ipAddressDetails.IPAddressStatus = IPAddressStatus.Normal;
                        }
                        if (!isIpAddressEntryExists)
                        {
                            isSaved = ipAddressDetails.Save();
                        }
                        else
                        {
                            isSaved = ipAddressDetails.Update();
                        }
                        if (isSaved < 0)
                        {
                            status.Category = "GIP";
                            status.StatusCode = "04";
                            status.Description = "IP address not Saved.";
                            response.Status = status;
                            response.IPAddressStatus = IPAddressStatus.Normal;
                            Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.Normal, (int)UserMaster.ID, status.Description + "\nIPAddress : " + ipAddress, "");
                            return response;
                        }
                        else
                        {
                            response.Status = new WSStatus();
                            response.Status.Category = "GIP";
                            response.Status.StatusCode = "01";
                            response.Status.Description = "GetIPAddressStatus successful!!";
                            response.IPAddressStatus = ipAddressDetails.IPAddressStatus;
                            Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.Normal, (int)UserMaster.ID, "Paid IP service called sucessfully for IP " + ipAddress, "");
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        response = GetIPAddressStatus(ipAddress);
                        Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.Normal, (int)UserMaster.ID, ex.ToString() + ex.StackTrace + "\nIPAddress : " + ipAddress, "");
                        return response;
                    }


                }
                else
                {
                    IPAddressStatus ipStatus = IPAddressDetails.GetIPAddressStatus(ipAddress);
                    response.Status.Category = "GIP";
                    response.Status.StatusCode = "01";
                    response.Status.Description = "GetIPAddressStatus successful!!";
                    response.IPAddressStatus = ipStatus;
                    return response;
                }
            }

        }
        else
        {
            WSSaveIPAddressResponse getIPStatusResponse = new WSSaveIPAddressResponse();
            getIPStatusResponse.Status.StatusCode = "02";
            getIPStatusResponse.Status.Description = "Authentication Failed";
            getIPStatusResponse.Status.Category = "GIP";
            getIPStatusResponse.IPAddressStatus = IPAddressStatus.Normal;
            Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.High, (int)UserMaster.ID, " Authentication Failed", ipAddr);
            return getIPStatusResponse;
        }
    }



    private bool WSGetIPAddressStatusInput(string ipAddress)
    {
        if (ipAddress == null || ipAddress.Trim() == "")
        {
            statusDescription = "ipAddress must not be null or empty string";
            statusCode = "03";
            return false;
        }
        return true;
    }

    private int SaveIPAddress(string ipAddress)
    {
        string responseXML = string.Empty;
        string strUrl = "http://api.hostip.info";
        responseXML = SendGetRequest(strUrl, ipAddress);
        IPAddressDetails ipAddressDetails = new IPAddressDetails();
        ipAddressDetails.IPAddress = ipAddress;
        ReadResponse(responseXML, ref ipAddressDetails);
        if (ipAddressDetails.CountryCode.Trim() != "IN")
        {
            ipAddressDetails.IPAddressStatus = IPAddressStatus.Suspicious;
        }
        else
        {
            ipAddressDetails.IPAddressStatus = IPAddressStatus.Normal;
        }
        int status = ipAddressDetails.Save();
        return status;
    }

    private void ReadResponse(string responseXML, ref IPAddressDetails ipAddress)
    {
        responseXML = responseXML.Replace("xmlns=\"http://www.hostip.info/api\" ", string.Empty);
        TextReader textReader = new StringReader(responseXML);
        XmlDocument ipResponse = new XmlDocument();
        ipResponse.Load(textReader);
        // namespace creating
        XmlNamespaceManager nsmgr = new XmlNamespaceManager(ipResponse.NameTable);
        nsmgr.AddNamespace("gml", "http://www.opengis.net/gml");
        XmlNode node = ipResponse.SelectSingleNode("/HostipLookupResultSet/gml:featureUserMaster/Hostip/gml:name", nsmgr);
        if (node != null && node.InnerText != "" && node.InnerText.Trim().ToLower() != "(unknown city)")
        {
            string[] cityArray = node.InnerText.Split(',');
            for (int i = 0; i < cityArray.Length; i++)
            {
                if (i == 0)
                {
                    ipAddress.City = cityArray[i].Trim();
                }
                else
                {
                    ipAddress.Region = cityArray[i].Trim();
                }
            }
        }
        node = ipResponse.SelectSingleNode("/HostipLookupResultSet/gml:featureUserMaster/Hostip/countryName", nsmgr);
        if (node != null && node.InnerText.Trim() != "")
        {
            ipAddress.CountryName = node.InnerText.Trim();
        }
        node = ipResponse.SelectSingleNode("/HostipLookupResultSet/gml:featureUserMaster/Hostip/countryAbbrev", nsmgr);
        if (node != null && node.InnerText.Trim() != "")
        {
            ipAddress.CountryCode = node.InnerText.Trim();
        }
        node = ipResponse.SelectSingleNode("/HostipLookupResultSet/gml:featureUserMaster/Hostip/ipLocation", nsmgr);
        if (node != null)
        {
            XmlNode innerNode = ipResponse.SelectSingleNode("/HostipLookupResultSet/gml:featureUserMaster/Hostip/ipLocation/gml:PointProperty/gml:Point/gml:coordinates", nsmgr);
            if (innerNode != null && innerNode.InnerText.Trim() != "")
            {
                string[] positionArray = innerNode.InnerText.Split(',');
                for (int i = 0; i < positionArray.Length; i++)
                {
                    if (i == 0)
                    {
                        ipAddress.Longitude = float.Parse(positionArray[i].Trim());
                    }
                    else
                    {
                        ipAddress.Latitude = float.Parse(positionArray[i].Trim());
                    }
                }
            }
        }
    }

    private string SendGetRequest(string strUrl, string ipAddress)
    {
        Trace.TraceInformation("BookingApi.SendGetIPRequest entered");
        HttpWebResponse objResponse;
        HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(strUrl);
        objRequest = (HttpWebRequest)HttpWebRequest.Create(strUrl + "/?ip=" + ipAddress + "&position=true");
        objRequest.CookieContainer = new CookieContainer();
        objRequest.ContentType = "application/x-www-form-urlencoded";
        objResponse = (HttpWebResponse)objRequest.GetResponse();
        string strResult;
        using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
        {
            strResult = sr.ReadToEnd();
            sr.Close();
        }
        objResponse.Close();
        Trace.TraceInformation("Mdlr.SendGetRequest exited");
        return strResult;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSSendChangeRequestResponse SendChangeRequest(WSSendChangeRequest request)
    {
        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);
        WSUserPreference wsPref = new WSUserPreference();        
        AgentLoginInfo = UserMaster.GetB2CUser((int)userMaster.ID);
        WSStatus status = new WSStatus();
        WSSendChangeRequestResponse response = new WSSendChangeRequestResponse();
        string ipAddr = this.Context.Request.UserHostAddress;
        if (userMaster != null)
        {
            response = WSSendChangeRequest.AddRequest(request, AgentLoginInfo.AgentId, (int)userMaster.ID);
            return response;
        }
        else
        {
            status.Category = "CR";
            status.Description = "Validation Failed";
            status.StatusCode = "11";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, 0, "saveRequest.Add failed and saveResponse.Status.Description=" + response.Status.Description, ipAddr);
            return response;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSChangeRequestStatusResponse GetChangeRequestStatus(WSChangeRequestStatusRequest request)
    {
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        WSStatus status = new WSStatus();
        WSChangeRequestStatusResponse response = new WSChangeRequestStatusResponse();
        string ipAddr = this.Context.Request.UserHostAddress;
        if (UserMaster != null)
        {
            AgentMaster AgentMaster = new AgentMaster(UserMaster.AgentId);
            response = WSChangeRequestStatusRequest.GetCRStatus(request, AgentMaster);
            return response;
        }
        else
        {
            status.Category = "GC";
            status.Description = "Validation Failed";
            status.StatusCode = "03";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, 0, "saveRequest.Add failed and saveResponse.Status.Description=" + response.Status.Description, ipAddr);
            return response;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSStatus AddWLAgentBooking(int bookingId, string siteId, ProductType productType)
    {
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        WSStatus status = new WSStatus();
        if (UserMaster != null)
        {
            if (bookingId <= 0)
            {
                status.Category = "SB";
                status.Description = "Validation Failed";
                status.StatusCode = "03";
                Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, 0, "Invalid BookingId :" + bookingId.ToString(), "");
                return status;
            }
            if (siteId == null || siteId == string.Empty)
            {
                status.Category = "SB";
                status.Description = "SiteId can't be null";
                status.StatusCode = "04";
                Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, 0, "Invalid BookingId :", "");
                return status;
            }
            try
            {
                //ApiCustomer.AddWLAgentBooking(bookingId, siteId, (int)productType, UserMaster.AgentId);
                status.Category = "SB";
                status.Description = "Successfull";
                status.StatusCode = "06";
            }
            catch (Exception ex)
            {
                status.Category = "SB";
                status.Description = "Unable to save booking";
                status.StatusCode = "05";
                Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, 0, "Unable to save booking:" + ex.ToString() + ex.StackTrace, "");
            }


            return status;
        }
        else
        {
            status.Category = "SB";
            status.Description = "Validation Failed";
            status.StatusCode = "03";
            Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "Unable to authorize", "");
            return status;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetAgentMasterBalanceResponse GetAgentMasterBalance(bool isAirlineLcc)
    {
        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);
        int UserId = 0;
        UserPreference pref = new UserPreference();
        pref.GetMemberIdBySiteName(Credential.SiteName);
        AgentLoginInfo = UserMaster.GetB2CUser(UserId);
        WSStatus status = new WSStatus();
        WSGetAgentMasterBalanceResponse response = new WSGetAgentMasterBalanceResponse();
        if (userMaster != null)
        {
            try
            {
                decimal AgentMasterBalance = 0;
                if (isAirlineLcc)
                {
                    AgentMasterBalance = AgentLoginInfo.AgentBalance; //Ledger.IsAgentCapable(UserMaster.AgentId, AccountType.LCC);
                }
                else
                {
                    AgentMasterBalance = AgentLoginInfo.AgentBalance; //Ledger.IsAgentCapable(UserMaster.AgentId, AccountType.NonLCC);
                }
                response.Balance = AgentMasterBalance;
                status.Category = "GB";
                status.Description = "AgentMaster balance retrieved successfully.";
                status.StatusCode = "01";
            }
            catch (Exception ex)
            {
                status.Category = "GB";
                status.Description = "Unable to retrieve agent balance";
                status.StatusCode = "02";
                Audit.AddAuditBookingAPI(EventType.Exception, Severity.High, 0, "Request to GetAgentMasterBalance Failed. Exception : " + ex.ToString() + ", " + ex.Source + ", " + ex.StackTrace, "");
            }
        }
        else
        {
            status.Category = "GB";
            status.Description = "Login Failed";
            status.StatusCode = "03";
            Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "Authorization failed", "");
        }
        response.Status = status;
        return response;
    }

    private FlightItinerary BuildItinerary(WSBookRequest bookRequest, int UserMasterId, int AgentMasterId)
    {
        #region Build Itinerary from WSBooking.
        SearchResult searchResult = new SearchResult();
        if (Basket.FlightBookingSession != null && Basket.FlightBookingSession.ContainsKey(bookRequest.SessionId))
        {
            searchResult = Basket.FlightBookingSession[bookRequest.SessionId].Result[bookRequest.ResultId];//Added by Lokesh on 4-April-2018
        }
        FlightItinerary itinerary = new FlightItinerary();
        itinerary.FlightBookingSource = bookRequest.Source;
        itinerary.FareType = bookRequest.FareType;
        itinerary.Origin = bookRequest.Origin;
        itinerary.Destination = bookRequest.Destination;
        itinerary.IsInsured= bookRequest.IsInsured;
        itinerary.GUID = bookRequest.GUID;
        itinerary.IsBaggageIncluded = false;
        itinerary.LocationId = (int)AgentLoginInfo.LocationID;
        itinerary.TransactionType = "B2C";
        itinerary.FareType = bookRequest.FareType;
        itinerary.CreatedBy = UserMasterId;//Created by b2b userid
        itinerary.AgencyId = AgentMasterId;
        itinerary.IsLCC = bookRequest.IsLCC;
        if (bookRequest.Source == BookingSource.Jazeera || bookRequest.Source == BookingSource.Indigo || bookRequest.Source == BookingSource.SpiceJet || bookRequest.Source == BookingSource.GoAir)
        {

            itinerary.Endorsement = searchResult.RepriceErrorMessage;
            itinerary.TicketAdvisory = searchResult.FareSellKey;
            itinerary.UniversalRecord = searchResult.JourneySellKey;
            itinerary.ResultType = searchResult.Flights.Length > 1 ? SearchType.Return : SearchType.OneWay;
            itinerary.IsLCC = true;
            itinerary.BookingMode = BookingMode.BookingAPI;
            itinerary.ItineraryAmountDue = 0;
            itinerary.ProductType = ProductType.Flight;
            itinerary.ProductTypeId = (int)ProductType.Flight;
            if (string.IsNullOrEmpty(itinerary.GUID))
            {
                itinerary.GUID = searchResult.GUID;
            }

        }
        if (bookRequest.Source == BookingSource.TBOAir)
        {
            itinerary.TicketAdvisory = searchResult.FareSellKey;//Update TraceId for TBOAir
            itinerary.UniversalRecord = searchResult.JourneySellKey;//For Release PNR
            if (string.IsNullOrEmpty(itinerary.GUID))//GUID will be checked for booking in TBOAir
            {
                itinerary.GUID = searchResult.GUID;
            }
            if (string.IsNullOrEmpty(itinerary.SupplierLocatorCode))//SupplierLocatorCode will be checked for booking in TBOAir
            {
                itinerary.SupplierLocatorCode = searchResult.FareType;
            }
        }
	//Added by Anji , assining required details to Book.
        if (searchResult.ResultBookingSource == BookingSource.FlightInventory)
        {
            itinerary.GUID = searchResult.GUID;//(SeatQuota)
            itinerary.PNR = searchResult.FareSellKey;//(PNR's)
            itinerary.SpecialRequest = searchResult.JourneySellKey;//(InventoryId)
        }
        if(bookRequest.IsGSTMandatory)
        {
            itinerary.GstCompanyAddress = bookRequest.GstCompanyAddress;
            itinerary.GstCompanyContactNumber = bookRequest.GstCompanyContactNumber;
            itinerary.GstCompanyEmail = bookRequest.GstCompanyEmail;
            itinerary.GstCompanyName = bookRequest.GstCompanyName;
            itinerary.GstNumber = bookRequest.GstNumber;
        }
        else
        {
            itinerary.GstCompanyAddress = string.Empty;
            itinerary.GstCompanyContactNumber = string.Empty;
            itinerary.GstCompanyEmail = string.Empty;
            itinerary.GstCompanyName = string.Empty;
            itinerary.GstNumber = string.Empty;
        }
        itinerary.IsGSTMandatory = bookRequest.IsGSTMandatory;

        if(searchResult.ResultBookingSource == BookingSource.TBOAir)
        {
            itinerary.TBOFareBreakdown = searchResult.TBOFareBreakdown;            
        }

        if (bookRequest.Segment != null)
            itinerary.TravelDate = bookRequest.Segment[0].DepTIme;
        else itinerary.TravelDate = DateTime.Now;

        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == PaymentModeType.CreditCard)
        {
            itinerary.BookingMode = BookingMode.WhiteLabel;
        }
        else
        {
            itinerary.BookingMode = BookingMode.BookingAPI;
        }
        itinerary.Passenger = new FlightPassenger[bookRequest.Passenger.Length];
        string paxType = string.Empty;

        for (int i = 0; i < itinerary.Passenger.Length; i++)
        {
            itinerary.Passenger[i] = new FlightPassenger();
            List<KeyValuePair<string, decimal>> taxBreakups = new List<KeyValuePair<string, decimal>>();
            itinerary.Passenger[i].Title = bookRequest.Passenger[i].Title;
            itinerary.Passenger[i].Type = bookRequest.Passenger[i].Type;
            paxType = itinerary.Passenger[i].Type == PassengerType.Adult ? "ADT" : itinerary.Passenger[i].Type == PassengerType.Child ? "CHD" : "INF";
            ////searchResult.Price.PaxTypeTaxBreakUp
            var serviceCharges = searchResult.Price.PaxTypeTaxBreakUp.Where(x => x.Key.ToUpper() == paxType).ToList();
            foreach (var taxbreakup in serviceCharges)
            {
                foreach (var item in taxbreakup.Value)
                {
                    taxBreakups.Add(new KeyValuePair<string, decimal>(item.Key, item.Value));
                }
                itinerary.Passenger[i].TaxBreakup = taxBreakups;
            }

            if (i == 0)
            {
                itinerary.Passenger[i].IsLeadPax = true;
            }
            if (bookRequest.Passenger[i].FirstName == null || bookRequest.Passenger[i].FirstName.Trim().Length == 0)
            {
                throw new ArgumentException("First name can't be blank");
            }
            else
            {
                itinerary.Passenger[i].FirstName = bookRequest.Passenger[i].FirstName;
            }
            if (string.IsNullOrEmpty(bookRequest.Passenger[i].LastName) || bookRequest.Passenger[i].LastName.Replace(" ", "").Replace(".", "").Length < 2)
            {
                throw new ArgumentException("Last name can't be blank or should contain atleast 2 alphabets");
            }
            else
            {
                itinerary.Passenger[i].LastName = bookRequest.Passenger[i].LastName;
            }
            itinerary.Passenger[i].Type = bookRequest.Passenger[i].Type;
            itinerary.Passenger[i].DateOfBirth = bookRequest.Passenger[i].DateOfBirth;
            if (bookRequest.Passenger[i].PassportExpiry != DateTime.MinValue)
                itinerary.Passenger[i].PassportExpiry = bookRequest.Passenger[i].PassportExpiry;
            if (bookRequest.Passenger[i].PassportNumber != null && bookRequest.Passenger[i].PassportNumber.Length > 0)
            {
                itinerary.Passenger[i].PassportNo = bookRequest.Passenger[i].PassportNumber;
            }

            itinerary.Passenger[i].Gender = bookRequest.Passenger[i].Gender;

            if (itinerary.FlightBookingSource == BookingSource.HermesAirLine)
            {
                string namePattern = "^[A-Za-z]+$";
                Match match = Regex.Match(itinerary.Passenger[i].FirstName, namePattern);
                if (!match.Success)
                {
                    throw new ArgumentException("First name should have only alphabets.");
                }
                match = Regex.Match(itinerary.Passenger[i].LastName, namePattern);
                if (!match.Success)
                {
                    throw new ArgumentException("Last name should have only alphabets");
                }
                if (bookRequest.Passenger[i].Gender != Gender.Male && bookRequest.Passenger[i].Gender != Gender.Female)
                {
                    throw new ArgumentException("Gender is required for this booking");
                }
                else
                {
                    itinerary.Passenger[i].Gender = bookRequest.Passenger[i].Gender;
                }
                if (itinerary.Passenger[i].DateOfBirth == null || itinerary.Passenger[i].DateOfBirth == new DateTime())
                {
                    throw new ArgumentException("DOB is required for this booking");
                }
                bool isDomestic = Util.IsDomestic(itinerary.Origin, itinerary.Destination, "AE");
                if (!isDomestic)
                {
                    if (string.IsNullOrEmpty(itinerary.Passenger[i].PassportNo))
                    {
                        throw new ArgumentException("Passport is mandatory for this booking");
                    }
                    match = Regex.Match(itinerary.Passenger[i].PassportNo, "^[A-Za-z0-9]{5,15}$");
                    if (!match.Success)
                    {
                        throw new ArgumentException("Passport should have only alphanumeric chars.");
                    }
                    if (bookRequest.Passenger[i].PassportExpiry == null || bookRequest.Passenger[i].PassportExpiry == new DateTime())
                    {
                        throw new ArgumentException("Passport Expiry date is mandatory for this booking");
                    }
                    else
                    {
                        itinerary.Passenger[i].PassportExpiry = bookRequest.Passenger[i].PassportExpiry;
                    }
                }
            }
            if (itinerary.FlightBookingSource == BookingSource.Jazeera || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || (itinerary.FlightBookingSource == BookingSource.TBOAir && searchResult.IsLCC))
            {
                itinerary.Passenger[i].BaggageCode = bookRequest.Passenger[i].BaggageCode;
                if (itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    itinerary.Passenger[i].BaggageType = bookRequest.Passenger[i].BaggageType;
                    itinerary.Passenger[i].CategoryId = bookRequest.Passenger[i].CategoryId;
                }
                if(itinerary.FlightBookingSource == BookingSource.AirArabia)
                {
                    itinerary.Passenger[i].MealDesc = bookRequest.Passenger[i].Meal.Description;
                    itinerary.Passenger[i].BaggageType = bookRequest.Passenger[i].BaggageType;
                    itinerary.Passenger[i].MealType = bookRequest.Passenger[i].Meal.Code;
                }
            }

            //Added by Lokesh on 14-06-2018
            //This is for Amadeus source
            //Assign Pax Baggage Codes.
            if (itinerary.FlightBookingSource == BookingSource.Amadeus)
            {
                 // comented by bangar for assign baggage to infant
               // if(bookRequest.Passenger[i].Type != PassengerType.Infant)//As infant does not have any baggage. 
               // {
                    if(!string.IsNullOrEmpty(bookRequest.Passenger[i].BaggageCode))
                    {
                        itinerary.Passenger[i].BaggageCode = bookRequest.Passenger[i].BaggageCode;
                    }
              //  }
            }
            if (itinerary.FlightBookingSource == BookingSource.Sabre)
            {
                
                if (!string.IsNullOrEmpty(bookRequest.Passenger[i].BaggageCode))
                {
                    itinerary.Passenger[i].BaggageCode = bookRequest.Passenger[i].BaggageCode;
                }
                
            }



            if (bookRequest.Passenger[i].Meal.Description != null && bookRequest.Passenger[i].Meal.Description.Length != 0)
            {
                itinerary.Passenger[i].MealDesc = bookRequest.Passenger[i].Meal.Description;
            }
            if (bookRequest.Passenger[i].Seat.Code != null && bookRequest.Passenger[i].Seat.Code.Length != 0)
            {
                itinerary.Passenger[i].Seat = bookRequest.Passenger[i].Seat;
            }
            if (bookRequest.Passenger[0].Country == null || bookRequest.Passenger[0].Country.Length == 0)
            {
                throw new ArgumentException("Invalid country Code");
            }
            else
            {
                itinerary.Passenger[i].Country = Country.GetCountry(bookRequest.Passenger[i].Country);
            }
            if (bookRequest.Source == BookingSource.AirArabia || bookRequest.Source == BookingSource.Indigo || bookRequest.Source == BookingSource.SpiceJet || bookRequest.Source == BookingSource.GoAir)
            {
                itinerary.Passenger[i].Nationality = new Country();
                itinerary.Passenger[i].Nationality = Country.GetCountry(bookRequest.Passenger[i].Country);
                itinerary.AliasAirlineCode = searchResult.ResultKey;
                //Added by Lokesh 11-04-2018
                //For G9 Source
                //If the lead pax has state code and tax reg no

                if (i == 0)//For Lead Pax
                {
                    if (!string.IsNullOrEmpty(bookRequest.Passenger[0].GSTStateCode))
                    {
                        itinerary.Passenger[0].GSTStateCode = bookRequest.Passenger[0].GSTStateCode;
                    }
                    if (!string.IsNullOrEmpty(bookRequest.Passenger[0].GSTTaxRegNo))
                    {
                        itinerary.Passenger[0].GSTTaxRegNo = bookRequest.Passenger[0].GSTTaxRegNo;
                    }
                }
            }
            //Lokesh : 17May2017 APIS Mandate Fields For B2C Bookings.
            itinerary.Passenger[i].AddressLine1 = (bookRequest.Passenger[0].AddressLine1.Length == 0 ? "Shj" : bookRequest.Passenger[0].AddressLine1);
            itinerary.Passenger[i].AddressLine2 = (bookRequest.Passenger[0].AddressLine2.Length == 0 ? "Shj" : bookRequest.Passenger[0].AddressLine2);
            if (!string.IsNullOrEmpty(bookRequest.Passenger[i].Nationality) && bookRequest.Passenger[i].Nationality.Length > 0)
            {
                itinerary.Passenger[i].Nationality = Country.GetCountry(bookRequest.Passenger[i].Nationality);
            }
            else
            {
                throw new ArgumentException("Invalid Nationality Code");
            }
            if (i == 0 && itinerary.FlightBookingSource == BookingSource.HermesAirLine)
            {
                string addPattern = @"^[A-Za-z0-9'\/\.\-\, ]+$";
                if (string.IsNullOrEmpty(bookRequest.Passenger[i].AddressLine1))
                {
                    throw new ArgumentException("Please fill passenger address in Address1.");
                }
                Match match = Regex.Match(bookRequest.Passenger[i].AddressLine1, addPattern);
                if (!match.Success)
                {
                    throw new ArgumentException("Special chars other than '/.-, not allowed in Address1.");
                }
                if (string.IsNullOrEmpty(bookRequest.Passenger[i].AddressLine2))
                {
                    throw new ArgumentException("Please fill City Name in Address2.");
                }
                addPattern = @"^[A-Za-z ]+$";
                match = Regex.Match(bookRequest.Passenger[i].AddressLine2, addPattern);
                if (!match.Success)
                {
                    throw new ArgumentException("Only Alphabetic chars are allowed in city name.");
                }
                if (string.IsNullOrEmpty(bookRequest.Passenger[i].PinCode))
                {
                    throw new ArgumentException("Pin Code is required for this booking.");
                }
                addPattern = @"^\d{6}$";
                match = Regex.Match(bookRequest.Passenger[i].PinCode, addPattern);
                if (!match.Success)
                {
                    throw new ArgumentException("Pin code should have 6 digits.");
                }
                if (bookRequest.Passenger[i].Phone == null || bookRequest.Passenger[i].Phone.Length == 0)
                {
                    throw new ArgumentException("Phone/Mobile no must be provided & of 10 digits only.");
                }
                addPattern = @"^\d{10}$";//phone no check
                match = Regex.Match(bookRequest.Passenger[i].Phone, addPattern);
                if (!match.Success)
                {
                    throw new ArgumentException("Phone/Mobile no must have 10 digits only.");
                }
                else
                {
                    itinerary.Passenger[i].CellPhone = bookRequest.Passenger[0].Phone;
                }
                itinerary.Passenger[i].AddressLine2 = itinerary.Passenger[i].AddressLine2 + "|" + bookRequest.Passenger[i].PinCode;
            }
            if (bookRequest.Passenger[0].Phone == null || bookRequest.Passenger[0].Phone.Length == 0)
            {
                throw new ArgumentException("Phone number must be provided");
            }
            else
            {
                itinerary.Passenger[i].CellPhone = bookRequest.Passenger[0].Phone.Replace("+", "");
            }
            if (bookRequest.Passenger[0].Email == null || bookRequest.Passenger[0].Email.Length == 0)
            {
                throw new ArgumentException("Email address must be provided");
            }
            else
            {
                itinerary.Passenger[i].Email = bookRequest.Passenger[0].Email;
            }
            itinerary.Passenger[i].FFAirline = bookRequest.Passenger[i].FFAirline;
            itinerary.Passenger[i].FFNumber = bookRequest.Passenger[i].FFNumber;
            itinerary.Passenger[i].CreatedBy = UserMasterId;
            //Saving StateCode for FraudLab audit for LeadPax only
            itinerary.Passenger[i].StateCode = bookRequest.Passenger[i].StateCode;
        }
        itinerary.Segments = new FlightInfo[bookRequest.Segment.Length];
        List<FlightInfo> Segments = new List<FlightInfo>(searchResult.Flights[0]);
        if (searchResult.Flights.Length > 1)
            Segments.AddRange(searchResult.Flights[1]);
        #region Segment Details
        for (int i = 0; i < bookRequest.Segment.Length; i++)
        {
            if (string.IsNullOrEmpty(bookRequest.Segment[i].FareClass))
                bookRequest.Segment[i].FareClass = "C";//hard code booking class if none is specified as did in b2b
            itinerary.Segments[i] = new FlightInfo();
            itinerary.Segments[i].FlightStatus = FlightStatus.Confirmed;
            if (bookRequest.Segment[i].Airline.AirlineCode == null || bookRequest.Segment[i].Airline.AirlineCode.Length <= 0)
            {
                throw new ArgumentException("Airline code cannot be blank");
            }
            else
            {
                itinerary.Segments[i].Airline = bookRequest.Segment[i].Airline.AirlineCode;
            }
            if (bookRequest.Segment[i].FlightNumber == null || bookRequest.Segment[i].FlightNumber.Length <= 0)
            {
                throw new ArgumentException("Flight number cannot be blank");
            }
            else
            {
                itinerary.Segments[i].FlightNumber = bookRequest.Segment[i].FlightNumber;
            }
            //Cabin Class was not saving 
            itinerary.Segments[i].CabinClass = bookRequest.Segment[i].FareCabinClass;
            itinerary.Segments[i].BookingClass = string.IsNullOrEmpty(bookRequest.Segment[i].FareClass) ? Segments[i].BookingClass : bookRequest.Segment[i].FareClass;
            if (bookRequest.Segment[i].Origin.AirportCode == null || bookRequest.Segment[i].Origin.AirportCode.Length <= 0)
            {
                throw new ArgumentException("Origin airport cannot be blank");
            }
            else
            {
                itinerary.Segments[i].Origin = new Airport(bookRequest.Segment[i].Origin.AirportCode);
            }
            itinerary.Segments[i].DepTerminal = bookRequest.Segment[i].Origin.Terminal;
            itinerary.Segments[i].DepartureTime = bookRequest.Segment[i].DepTIme;
            if (bookRequest.Segment[i].Destination.AirportCode == null || bookRequest.Segment[i].Destination.AirportCode.Length <= 0)
            {
                throw new ArgumentException("Destination cannot be blank");
            }
            else
            {
                itinerary.Segments[i].Destination = new Airport(bookRequest.Segment[i].Destination.AirportCode);
            }
            itinerary.Segments[i].ArrTerminal = bookRequest.Segment[i].Destination.Terminal;
            itinerary.Segments[i].ArrivalTime = bookRequest.Segment[i].ArrTime;
            itinerary.Segments[i].ETicketEligible = bookRequest.Segment[i].ETicketEligible;
            string[] tspanArr = bookRequest.Segment[i].Duration.Split(new string[] { ":" }, StringSplitOptions.None);
            itinerary.Segments[i].Duration = new TimeSpan(Convert.ToInt32(tspanArr[0]), Convert.ToInt32(tspanArr[1]), 0);
            itinerary.Segments[i].Craft = bookRequest.Segment[i].Craft;
            //TODO: ??
            itinerary.Segments[i].CreatedBy = UserMasterId;
            // Assigning UAPI Objects
            itinerary.Segments[i].FareInfoKey = bookRequest.Segment[i].FareInfoKey;
            if (bookRequest.Source != BookingSource.AirArabia)
            {
                itinerary.Segments[i].Group = bookRequest.Segment[i].Group;
            }
            else
            {
                itinerary.Segments[i].Group = bookRequest.Segment[i].SegmentIndicator - 1;
            }
            itinerary.Segments[i].UapiDepartureTime = bookRequest.Segment[i].UapiDepartureTime;
            itinerary.Segments[i].UapiArrivalTime = bookRequest.Segment[i].UapiArrivalTime;
            itinerary.Segments[i].UapiSegmentRefKey = bookRequest.Segment[i].UapiSegmentRefKey;
            itinerary.Segments[i].Stops = bookRequest.Segment[i].Stop;
            itinerary.Segments[i].StopOver = bookRequest.Segment[i].StopOver;
            itinerary.Segments[i].SegmentFareType = bookRequest.Segment[i].SegmentFareType;
            if (bookRequest.Source == BookingSource.UAPI)
            {
                itinerary.Segments[i].UAPIReservationValues = new Hashtable();
                itinerary.Segments[i].UAPIReservationValues.Add(LINK_AVAILABILITY, bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == LINK_AVAILABILITY) != null ? bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == LINK_AVAILABILITY).Value : false);
                itinerary.Segments[i].UAPIReservationValues.Add(AVAILABILITY_SOURCE, bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == AVAILABILITY_SOURCE) != null ? bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == AVAILABILITY_SOURCE).Value : null);
                itinerary.Segments[i].UAPIReservationValues.Add(SEGMENT_REF, bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == SEGMENT_REF) != null ? bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == SEGMENT_REF).Value : null);
                itinerary.Segments[i].UAPIReservationValues.Add(POLLED_AVAILABILITY_OPTION, bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == POLLED_AVAILABILITY_OPTION) != null ? bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == POLLED_AVAILABILITY_OPTION).Value : null);
                itinerary.Segments[i].UAPIReservationValues.Add(PROVIDER_CODE, bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == PROVIDER_CODE) != null ? bookRequest.Segment[i].UAPIReservationValues.Find(c => c.Key.ToString() == PROVIDER_CODE).Value : "Polled avail exists");
             }
        } 
        #endregion

        itinerary.Origin = bookRequest.Origin;
        itinerary.Destination = bookRequest.Destination;
        itinerary.FareRules = ReadFareBasis(bookRequest.FareBasis);
        AirlineCommission airlineCommission = AirlineCommission.Load(itinerary.AirlineCode, AgentMasterId);
        decimal commission = 0;
        if (airlineCommission != null && airlineCommission.AirlineCode != null && airlineCommission.AirlineCode.Trim() != string.Empty && airlineCommission.IsActive && itinerary.BookingMode == BookingMode.WhiteLabel)
        {
            commission = airlineCommission.Commission;
        }
        //Commented by Shiva 27 May 2020
        //if (searchResult.ResultBookingSource == BookingSource.AirArabia)
        //{
        //    //added by Shiva 10 Apr 2018
        //    for (int f = 0; f < searchResult.FareBreakdown.Length; f++)
        //    {
        //        Fare fare = searchResult.FareBreakdown[f];
        //        searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, f, AgentMasterId, 0, 1, "B2C");

        //        searchResult.FareBreakdown[f].AgentMarkup = searchResult.Price.Markup;
        //        searchResult.FareBreakdown[f].B2CMarkup = searchResult.Price.B2CMarkup;
        //    }
        //}

        #region Price Details
        for (int i = 0; i < itinerary.Passenger.Length; i++)
        {
            itinerary.Passenger[i].Price = new PriceAccounts();
            if (searchResult.ResultBookingSource == BookingSource.Indigo || searchResult.ResultBookingSource == BookingSource.SpiceJet || searchResult.ResultBookingSource == BookingSource.GoAir)
            {
                for (int f = 0; f < searchResult.FareBreakdown.Length; f++)
                {
                    if (itinerary.Passenger[i].Type == searchResult.FareBreakdown[f].PassengerType)
                    {
                        itinerary.Passenger[i].Price.HandlingFeeAmount = searchResult.FareBreakdown[f].HandlingFee / searchResult.FareBreakdown[f].PassengerCount;
                        itinerary.Passenger[i].Price.HandlingFeeType = (searchResult.Price.HandlingFeeType != null ? searchResult.Price.HandlingFeeType : string.Empty);
                        itinerary.Passenger[i].Price.HandlingFeeValue = searchResult.Price.HandlingFeeValue;
                        itinerary.Passenger[i].Price.PublishedFare = bookRequest.Passenger[i].Fare.BaseFare - itinerary.Passenger[i].Price.HandlingFeeAmount;
                        //itinerary.Passenger[i].Price.BaseFare = bookRequest.Passehynger[i].Fare.BaseFare - searchResult.Price.HandlingFeeValue;
                        itinerary.Passenger[i].Price.SupplierPrice = Convert.ToDecimal(searchResult.FareBreakdown[f].SupplierFare) / searchResult.FareBreakdown[f].PassengerCount;
                    }
                }
            }
            else
            {
                itinerary.Passenger[i].Price.PublishedFare = bookRequest.Passenger[i].Fare.BaseFare;
                itinerary.Passenger[i].Price.SupplierPrice = bookRequest.Passenger[i].Fare.SupplierPrice;
            }

            itinerary.Passenger[i].Price.Tax = bookRequest.Passenger[i].Fare.Tax;
            if (i == 0 && !bookRequest.Fare.InputVATApplied)//Add input vat for Adult only (lead pax only)
            {
                itinerary.Passenger[i].Price.Tax += bookRequest.Passenger[i].Fare.InputVAT;
            }
            //itinerary.Passenger[i].Price.AirlineTransFee = bookRequest.Passenger[i].Fare.AirTransFee;
            itinerary.Passenger[i].Price.Currency = bookRequest.Passenger[i].Fare.Currency;
            itinerary.Passenger[i].Price.CurrencyCode = bookRequest.Passenger[i].Fare.Currency;
            itinerary.Passenger[i].Price.RateOfExchange = AgentLoginInfo.AgentExchangeRates[bookRequest.Passenger[i].Fare.SupplierCurrency];
            //Get price is called to load different value of price object that is not in WSFare
            //itinerary.Passenger[i].Price = CT.AccountingEngine.AccountingEngine.GetPrice(itinerary, AgentMasterId, i, 0);
            //itinerary.Passenger[i].Price.OtherCharges = bookRequest.Passenger[i].Fare.OtherCharges;
            itinerary.Passenger[i].Price.Discount = bookRequest.Passenger[i].Fare.Discount;
            itinerary.Passenger[i].Price.DiscountType = searchResult.Price.DiscountType;
            itinerary.Passenger[i].Price.DiscountValue = searchResult.Price.DiscountValue;
            //itinerary.Passenger[i].Price.AgentPLB = bookRequest.Passenger[i].Fare.PLB;
            //itinerary.Passenger[i].Price.AgentCommission = bookRequest.Passenger[i].Fare.AgentCommission;
            //Added  additional txn fee in price on 03 jul 2008           
            //itinerary.Passenger[i].Price.AdditionalTxnFee = bookRequest.Passenger[i].Fare.AdditionalTxnFee;
            itinerary.Passenger[i].Price.Markup = bookRequest.Passenger[i].Fare.Markup;
            itinerary.Passenger[i].Price.MarkupType = bookRequest.Passenger[i].Fare.MarkupType;
            itinerary.Passenger[i].Price.MarkupValue = bookRequest.Passenger[i].Fare.MarkupValue;
            itinerary.Passenger[i].Price.B2CMarkup = bookRequest.Passenger[i].Fare.B2CMarkup;
            itinerary.Passenger[i].Price.B2CMarkupType = bookRequest.Passenger[i].Fare.B2CMarkupType;
            itinerary.Passenger[i].Price.B2CMarkupValue = bookRequest.Passenger[i].Fare.B2CMarkupValue;
            itinerary.Passenger[i].Price.SupplierCurrency = bookRequest.Passenger[i].Fare.SupplierCurrency;
            
            itinerary.Passenger[i].Price.OutputVATAmount = bookRequest.Passenger[0].Fare.OutputVAT / bookRequest.Passenger.Length;
            itinerary.Passenger[i].Price.InputVATAmount = bookRequest.Passenger[i].Fare.InputVAT / bookRequest.Passenger.Length;
            itinerary.Passenger[i].Price.TaxDetails = bookRequest.Passenger[i].Fare.TaxDetails;
            
            if (searchResult.ResultBookingSource == BookingSource.TBOAir)
            {
                if (i == 0)
                {
                    itinerary.Passenger[i].Price.ChargeBU = searchResult.Price.ChargeBU;
                    itinerary.Passenger[i].Price.YQTax = searchResult.Price.YQTax;
                    itinerary.Passenger[i].Price.AgentCommission = searchResult.Price.AgentCommission;
                    itinerary.Passenger[i].Price.SServiceFee = searchResult.Price.SServiceFee;
                    itinerary.Passenger[i].Price.IncentiveEarned = searchResult.Price.IncentiveEarned;
                    itinerary.Passenger[i].Price.TDSIncentive = searchResult.Price.TDSIncentive;
                    itinerary.Passenger[i].Price.AgentPLB = searchResult.Price.AgentPLB;
                    itinerary.Passenger[i].Price.TDSPLB = searchResult.Price.TDSPLB;
                    itinerary.Passenger[i].Price.TdsCommission = searchResult.Price.TdsCommission;
                    itinerary.Passenger[i].Price.AdditionalTxnFee = searchResult.Price.AdditionalTxnFee;
                    itinerary.Passenger[i].Price.TransactionFee = searchResult.Price.TransactionFee;
                    itinerary.Passenger[i].Price.OtherCharges = searchResult.Price.OtherCharges;
                    itinerary.Passenger[i].Price.SupplierCurrency = searchResult.Price.SupplierCurrency;
                }

                for (int f = 0; f < searchResult.FareBreakdown.Length; f++)
                {
                    if (itinerary.Passenger[i].Type == searchResult.FareBreakdown[f].PassengerType)
                    {
                        //Save Handling Fee
                        itinerary.Passenger[i].Price.HandlingFeeAmount = searchResult.FareBreakdown[f].HandlingFee / searchResult.FareBreakdown[f].PassengerCount;
                        itinerary.Passenger[i].Price.HandlingFeeType = (searchResult.Price.HandlingFeeType != null ? searchResult.Price.HandlingFeeType : string.Empty);
                        itinerary.Passenger[i].Price.HandlingFeeValue = searchResult.Price.HandlingFeeValue;
                        itinerary.Passenger[i].Price.PublishedFare -= itinerary.Passenger[i].Price.HandlingFeeAmount;
                        itinerary.Passenger[i].TBOPrice = new PriceAccounts();
                        itinerary.Passenger[i].TBOPrice.AccPriceType = PriceType.PublishedFare;
                        itinerary.Passenger[i].TBOPrice.PublishedFare = (decimal)searchResult.TBOPrice.PublishedFare / searchResult.FareBreakdown[f].PassengerCount;
                        itinerary.Passenger[i].TBOPrice.BaseFare = searchResult.TBOPrice.BaseFare;
                        itinerary.Passenger[i].TBOPrice.Tax = (decimal)searchResult.TBOPrice.Tax;
                        itinerary.Passenger[i].TBOPrice.Markup = searchResult.TBOPrice.Markup;
                        itinerary.Passenger[i].TBOPrice.MarkupType = searchResult.TBOPrice.MarkupType;
                        itinerary.Passenger[i].TBOPrice.MarkupValue = searchResult.TBOPrice.MarkupValue;
                        itinerary.Passenger[i].TBOPrice.DecimalPoint = (AgentLoginInfo.IsOnBehalfOfAgent ? AgentLoginInfo.OnBehalfAgentDecimalValue : AgentLoginInfo.DecimalValue);
                        itinerary.Passenger[i].TBOPrice.Markup = searchResult.FareBreakdown[f].AgentMarkup / searchResult.FareBreakdown[f].PassengerCount;
                        itinerary.Passenger[i].TBOPrice.Discount = searchResult.FareBreakdown[f].AgentDiscount / searchResult.FareBreakdown[f].PassengerCount;
                        itinerary.Passenger[i].TBOPrice.RateOfExchange = 1;
                        itinerary.Passenger[i].TBOPrice.Currency = searchResult.TBOPrice.Currency;
                        itinerary.Passenger[i].TBOPrice.CurrencyCode = searchResult.TBOPrice.Currency;
                        itinerary.Passenger[i].TBOPrice.WhiteLabelDiscount = 0;
                        itinerary.Passenger[i].TBOPrice.WLCharge = searchResult.TBOPrice.WLCharge;
                        itinerary.Passenger[i].TBOPrice.NetFare = searchResult.TBOPrice.NetFare;
                        itinerary.Passenger[i].TBOPrice.OtherCharges = searchResult.TBOPrice.OtherCharges;
                        itinerary.Passenger[i].TBOPrice.TransactionFee = searchResult.TBOPrice.TransactionFee;
                        itinerary.Passenger[i].TBOPrice.AdditionalTxnFee = searchResult.TBOPrice.AdditionalTxnFee; //sai               
                        itinerary.Passenger[i].TBOPrice.ChargeBU = searchResult.TBOPrice.ChargeBU;
                        itinerary.Passenger[i].TBOPrice.AgentCommission = searchResult.TBOPrice.AgentCommission;

                        itinerary.Passenger[i].TBOPrice.AirlineTransFee = searchResult.TBOPrice.AirlineTransFee;
                        itinerary.Passenger[i].TBOPrice.AgentPLB = searchResult.TBOPrice.AgentPLB;

                        itinerary.Passenger[i].TBOPrice.IncentiveEarned = searchResult.TBOPrice.IncentiveEarned;
                        itinerary.Passenger[i].TBOPrice.TDSIncentive = searchResult.TBOPrice.TDSIncentive;
                        itinerary.Passenger[i].TBOPrice.TDSPLB = searchResult.TBOPrice.TDSPLB;
                        itinerary.Passenger[i].TBOPrice.TdsCommission = searchResult.TBOPrice.TdsCommission;

                        itinerary.TBOFareBreakdown = searchResult.TBOFareBreakdown;
                    }
                }
            }
            if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.TBOAir || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)// add baggage price for Air G9
            {
                if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    itinerary.Passenger[i].Price.BaggageCharge = bookRequest.Passenger[i].Fare.BaggageCharge;
                    itinerary.Passenger[i].Price.MealCharge = bookRequest.Passenger[i].Fare.MealCharge;
                }
                else if (itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                {
                    itinerary.Passenger[i].FlyDubaiBaggageCharge = bookRequest.Passenger[i].FlyDubaiBaggageCharge;
                    itinerary.Passenger[i].Price.BaggageCharge = bookRequest.Passenger[i].Fare.BaggageCharge;
                    itinerary.Passenger[i].Price.FareInformationID = bookRequest.Passenger[i].FareInformationID;
                }
            }
            if (itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.GoAir)
            {
                itinerary.Passenger[i].Price.BaggageCharge = bookRequest.Passenger[i].Fare.BaggageCharge;
                itinerary.Passenger[i].Price.MealCharge = bookRequest.Passenger[i].Fare.MealCharge;
                itinerary.Passenger[i].BaggageCode = bookRequest.Passenger[i].BaggageCode;
                itinerary.Passenger[i].MealDesc = bookRequest.Passenger[i].MealDesc;
                itinerary.Passenger[i].BaggageType = bookRequest.Passenger[i].BaggageType;
                itinerary.Passenger[i].MealType = bookRequest.Passenger[i].MealType;
                
            }
            if (itinerary.IsInsured)// If flight booking is insured
            {
                itinerary.Passenger[i].Price.InsuranceAmount = bookRequest.Passenger[i].Fare.InsuranceAmount;
            }
            if (commission > 0)
            {
                itinerary.Passenger[i].Price.WhiteLabelDiscount = Math.Round(itinerary.Passenger[i].Price.AgentCommission * commission / 100);
            }
            else
            {
                itinerary.Passenger[i].Price.WhiteLabelDiscount = bookRequest.Passenger[i].Fare.Discount;
            }
            if (bookRequest.Passenger[i].Fare.ChargeBU != null && bookRequest.Passenger[i].Fare.ChargeBU.Length > 0)
            {
                for (int k = 0; k < bookRequest.Passenger[i].Fare.ChargeBU.Length; k++)
                {
                    if (bookRequest.Passenger[i].Fare.ChargeBU[k].ChargeType != ChargeType.OtherCharges)
                    {
                        ChargeBreakUp chargeBU = new ChargeBreakUp();
                        chargeBU.ChargeType = bookRequest.Passenger[i].Fare.ChargeBU[k].ChargeType;
                        chargeBU.PriceId = bookRequest.Passenger[i].Fare.ChargeBU[k].PriceId;
                        if (bookRequest.Passenger[i].Fare.ChargeBU[k].ChargeType == ChargeType.CreditCardCharge || bookRequest.Passenger[i].Fare.ChargeBU[k].ChargeType == ChargeType.OtherCharges)
                        {
                            chargeBU.Amount = Math.Round(bookRequest.Passenger[i].Fare.ChargeBU[k].Amount / bookRequest.Passenger.Length);
                            itinerary.Passenger[i].Price.WLCharge = Math.Round(bookRequest.Passenger[i].Fare.ChargeBU[k].Amount / bookRequest.Passenger.Length);
                        }
                        itinerary.Passenger[i].Price.ChargeBU.Add(chargeBU);
                    }
                }
            }



            //Modified by Lokesh on 5-April-2018
            //Now Update the Price For all Pax Types
            //Since we are reading the AirArabiaBaggagePriceResponse
            if (searchResult.ResultBookingSource == BookingSource.AirArabia)
            {
                if (itinerary != null && itinerary.Passenger.Length > 0)
                {
                    //  for (int g = 0; g < itinerary.Passenger.Length; g++)
                    {
                        if (searchResult.FareBreakdown.Length > i && searchResult.FareBreakdown[i] != null && searchResult.FareBreakdown[i].SupplierFare > 0)
                        {
                            if (itinerary.Passenger[i].Type == searchResult.FareBreakdown[i].PassengerType)
                            {
                                itinerary.Passenger[i].Price.SupplierPrice = (decimal)searchResult.FareBreakdown[i].SupplierFare / searchResult.FareBreakdown[i].PassengerCount;
                                itinerary.Passenger[i].Price.PublishedFare = (decimal)searchResult.FareBreakdown[i].BaseFare / searchResult.FareBreakdown[i].PassengerCount;
                                itinerary.Passenger[i].Price.Tax = (decimal)searchResult.FareBreakdown[i].Tax / searchResult.FareBreakdown[i].PassengerCount;
                            }
                        }
                    }
                }
            }
        } 
        #endregion


        if (bookRequest.Source == BookingSource.UAPI)// TO pass AirPricing info  to get pass Create Reservation
        {
            itinerary.UapiPricingSolution = new UAPIdll.Air46.AirPricingSolution(); //bookRequest.UapiPricingSolution;
            PropertiesCopier.CopyProperties(searchResult.UapiPricingSolution, itinerary.UapiPricingSolution);
            PropertiesCopier.CheckRecursiveDifferentTypes(searchResult.UapiPricingSolution, itinerary.UapiPricingSolution);
        }

        return (itinerary);
        #endregion
    }

    private UserMaster GetUserMasterBySiteName(string siteName)
    {
        UserPreference pref = new UserPreference();
        int UserMasterId = 0;
        try
        {
            UserMasterId = pref.GetMemberIdBySiteName(siteName);
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "GetUserMasterIdBySiteName Failed: " + ex.ToString(), "");
        }
        if (UserMasterId > 0)
        {
            return new UserMaster(UserMasterId);
        }
        else
        {
            return null;
        }
    }

    private UserMaster GetUserMasterByAccountCode(string accountCode)
    {
        UserPreference pref = new UserPreference();
        UserMaster UserMaster = new UserMaster();
        try
        {
            //UserMaster.LoadByAccounCode(accountCode);
            return UserMaster;
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "GetUserMasterByAccountCode Failed: " + ex.ToString(), "");
            return null;
        }
    }

    private List<FareRule> ReadFareBasis(string[] farebasis)
    {
        List<FareRule> fareRule = new List<FareRule>();
        // BADELLON-4MC3RP --> airline code + origin + destination + "-" + farebasiscode.
        for (int i = 0; i < farebasis.Length; i++)
        {
            FareRule fRule = new FareRule();
            fRule.Airline = farebasis[i].Substring(0, 2);
            fRule.Origin = farebasis[i].Substring(2, 3);
            fRule.Destination = farebasis[i].Substring(5, 3);
            fRule.FareBasisCode = farebasis[i].Substring(9);
            fareRule.Add(fRule);
        }
        return fareRule;
    }
    private decimal GetTicketPrice(WSFare fare)
    {
        decimal bookingAmount = (fare.BaseFare + fare.Tax + fare.ServiceTax + fare.AdditionalTxnFee - fare.Discount);
        return (bookingAmount);
    }

    /// <summary>
    /// Method to send error mail
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="UserMaster"></param>
    private void SendErrorMail(string subject, UserMaster UserMaster, Exception ex, string sessionId)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        Hashtable hostPort = con.GetHostPort();
        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
        string fromEmailId = hostPort["fromEmail"].ToString();
        if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
        {
            try
            {
                string mailMessage = string.Empty;
                mailMessage = "UserMaster Name: " + UserMaster.FirstName + " "+ UserMaster.LastName;
                mailMessage += "\nUserMasterId: " + UserMaster.ID.ToString();
                mailMessage += "\nAgentMasterId: " + UserMaster.AgentId.ToString();
                if (sessionId.Trim() != string.Empty)
                {
                    mailMessage += "\nSessionId: " + sessionId;
                }
                mailMessage += "\n\nException: " + ex.ToString();
                mailMessage += "\n\nStack trace: " + ex.StackTrace;
                Email.Send(fromEmailId, errorNotificationMailingId, "Error in BookingAPI " + subject, mailMessage);
            }
            catch (Exception excep)
            {
                CT.Core.Audit.Add(EventType.Book, Severity.High, (int)UserMaster.ID, "B2C-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
            }
        }
    }
    /// <summary>
    /// Method to send error mail
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="UserMaster"></param>
    private void SendErrorMail(string subject, UserMaster UserMaster, Exception ex)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        Hashtable hostPort = con.GetHostPort();
        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
        string fromEmailId = hostPort["fromEmail"].ToString();
        if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
        {
            try
            {
                string mailMessage = string.Empty;
                mailMessage = "UserMaster Name: " + UserMaster.FirstName + " " + UserMaster.LastName;
                mailMessage += "\n AgentMasterId: " + UserMaster.AgentId.ToString();
                mailMessage += "\n\nException: " + ex.ToString();
                mailMessage += "\n\nStack trace: " + ex.StackTrace;
                Email.Send(fromEmailId, errorNotificationMailingId, "Error in Booking API " + subject, mailMessage);
            }
            catch (Exception excep)
            {
                CT.Core.Audit.Add(EventType.Book, Severity.High, (int)UserMaster.ID, "B2C-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
            }
        }
    }
    /// <summary>
    /// Method to send error mail
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="UserMaster"></param>
    private void SendErrorMail(string subject, UserMaster UserMaster, Exception ex, WSBookRequest bookRequest)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        Hashtable hostPort = con.GetHostPort();
        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
        string fromEmailId = hostPort["fromEmail"].ToString();
        if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
        {
            try
            {
                string mailMessage = string.Empty;
                mailMessage = "UserMaster Name: " + UserMaster.FirstName + " " + UserMaster.LastName;
                mailMessage += "\nAgentMasterId: " + UserMaster.AgentId.ToString();
                mailMessage += "\nOrigin: " + bookRequest.Origin;
                mailMessage += "\nDestination: " + bookRequest.Destination;
                mailMessage += "\nAirline: " + bookRequest.Segment[0].Airline.AirlineName;
                mailMessage += "\nFlight Number: " + bookRequest.Segment[0].FlightNumber;
                mailMessage += "\nFlight Date: " + bookRequest.Segment[0].DepTIme.ToShortDateString();
                mailMessage += "\nBooking Source: " + bookRequest.Source.ToString();
                for (int i = 0; i < bookRequest.Passenger.Length; i++)
                {
                    mailMessage += "\nName: " + bookRequest.Passenger[i].FirstName + " " + bookRequest.Passenger[i].LastName;
                    if (bookRequest.Passenger[i].FFAirline != null && bookRequest.Passenger[i].FFAirline != string.Empty)
                    {
                        mailMessage += "\nFrequent Flier Airline: " + bookRequest.Passenger[i].FFAirline;
                    }
                    if (bookRequest.Passenger[i].FFNumber != null && bookRequest.Passenger[i].FFNumber != string.Empty)
                    {
                        mailMessage += "\nFrequent Flier Number: " + bookRequest.Passenger[i].FFNumber;
                    }
                    if (bookRequest.Passenger[i].Meal.Code != null && bookRequest.Passenger[i].Meal.Code != string.Empty)
                    {
                        mailMessage += "\nMeal Code " + bookRequest.Passenger[i].Meal.Code;
                    }
                    if (bookRequest.Passenger[i].PassportNumber != null && bookRequest.Passenger[i].PassportNumber != string.Empty)
                    {
                        mailMessage += "\nPassport " + bookRequest.Passenger[i].PassportNumber;
                    }
                    if (bookRequest.Passenger[i].Seat.Code != null && bookRequest.Passenger[i].Seat.Code != string.Empty)
                    {
                        mailMessage += "\nSeat " + bookRequest.Passenger[i].Seat.Code;
                    }
                    if (i == 0)
                    {
                        mailMessage += "\nEmail: " + bookRequest.Passenger[i].Email;
                        mailMessage += "\nPhone: " + bookRequest.Passenger[i].Phone;
                    }
                }
                if (bookRequest.SessionId != null)
                {
                    mailMessage += "\nSessionID: " + bookRequest.SessionId;
                }
                mailMessage += "\n\nException: " + ex.ToString();
                mailMessage += "\n\nStack trace: " + ex.StackTrace;
                Email.Send(fromEmailId, errorNotificationMailingId, "Error in Booking API " + subject, mailMessage);
            }
            catch (Exception excep)
            {
                CT.Core.Audit.Add(EventType.Book, Severity.High, (int)UserMaster.ID, "B2C-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
            }
        }
    }

    /// <summary>
    /// Method to send error mail
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="UserMaster"></param>
    private void SendErrorMail(string subject, UserMaster UserMaster, Exception ex, WSTicketRequest ticketRequest)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        Hashtable hostPort = con.GetHostPort();
        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
        string fromEmailId = hostPort["fromEmail"].ToString();
        if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
        {
            try
            {
                string mailMessage = string.Empty;
                mailMessage = "UserMaster Name: " + UserMaster.FirstName + " " + UserMaster.LastName;
                mailMessage += "\nAgentMasterId: " + UserMaster.AgentId.ToString();
                mailMessage += "\nOrigin: " + ticketRequest.Origin;
                mailMessage += "\nDestination: " + ticketRequest.Destination;
                mailMessage += "\nAirline: " + ticketRequest.Segment[0].Airline.AirlineName;
                mailMessage += "\nFlight Number: " + ticketRequest.Segment[0].FlightNumber;
                mailMessage += "\nFlight Date: " + ticketRequest.Segment[0].DepTIme.ToShortDateString();
                mailMessage += "\nBooking Source: " + ticketRequest.Source.ToString();
                for (int i = 0; i < ticketRequest.Passenger.Length; i++)
                {
                    mailMessage += "\nName: " + ticketRequest.Passenger[i].FirstName + " " + ticketRequest.Passenger[i].LastName;
                    mailMessage += "\nType: " + ticketRequest.Passenger[i].Type.ToString();
                    if (ticketRequest.Passenger[i].FFAirline != null && ticketRequest.Passenger[i].FFAirline != string.Empty)
                    {
                        mailMessage += "\nFrequent Flier Airline: " + ticketRequest.Passenger[i].FFAirline;
                    }
                    if (ticketRequest.Passenger[i].FFNumber != null && ticketRequest.Passenger[i].FFNumber != string.Empty)
                    {
                        mailMessage += "\nFrequent Flier Number: " + ticketRequest.Passenger[i].FFNumber;
                    }
                    if (ticketRequest.Passenger[i].Meal.Code != null && ticketRequest.Passenger[i].Meal.Code != string.Empty)
                    {
                        mailMessage += "\nMeal Code " + ticketRequest.Passenger[i].Meal.Code;
                    }
                    if (ticketRequest.Passenger[i].PassportNumber != null && ticketRequest.Passenger[i].PassportNumber != string.Empty)
                    {
                        mailMessage += "\nPassport " + ticketRequest.Passenger[i].PassportNumber;
                    }
                    if (ticketRequest.Passenger[i].Seat.Code != null && ticketRequest.Passenger[i].Seat.Code != string.Empty)
                    {
                        mailMessage += "\nSeat " + ticketRequest.Passenger[i].Seat.Code;
                    }
                    if (i == 0)
                    {
                        mailMessage += "\nEmail: " + ticketRequest.Passenger[i].Email;
                        mailMessage += "\nPhone: " + ticketRequest.Passenger[i].Phone;
                    }
                }
                if (ticketRequest.SessionId != null)
                {
                    mailMessage += "\nSessionID: " + ticketRequest.SessionId;
                }
                mailMessage += "\n\nException: " + ex.ToString();
                mailMessage += "\n\nStack trace: " + ex.StackTrace;
                Email.Send(fromEmailId, errorNotificationMailingId, "Error in Booking API " + subject, mailMessage);
            }
            catch (Exception excep)
            {
                CT.Core.Audit.Add(EventType.Book, Severity.High, (int)UserMaster.ID, "B2C-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
            }
        }
    }

    private bool ValidateTicketRequestXML(FlightItinerary itinerary, string bookRequestXML)
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(bookRequestXML);
        // namespace creating
        XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
        nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
        nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
        nsmgr.AddNamespace("ns1", "urn:os:bookreservation");
        nsmgr.AddNamespace("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
        nsmgr.AddNamespace("type", "ns1:BookReservationRequest");
        nsmgr.AddNamespace("ns2", "http://xml.apache.org/xml-soap");
        nsmgr.AddNamespace("type", "ns2:Vector");
        nsmgr.AddNamespace("type", "ns1:AirComponent");
        // checking for any error found from Navitaire           
        XmlNode errorInfo = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring", nsmgr);
        if (errorInfo != null && errorInfo.InnerText.Length > 0)
        {
            throw new BookingEngineException("<br> " + errorInfo.InnerText);
        }
        XmlNode tempNode;
        XmlNode resvNode = xmlDoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:bookreservation/input/Reservation", nsmgr);
        //Price Check
        decimal xmlPrice = 0, itineraryPrice = 0;
        XmlNode price = resvNode.SelectSingleNode("DisplayTotalAmount/Amount/text()");
        if (price != null && price.Value.Length > 0)
        {
            xmlPrice = Convert.ToDecimal(price.Value);
        }
        foreach (FlightPassenger pax in itinerary.Passenger)
        {
            itineraryPrice += pax.Price.PublishedFare + pax.Price.Tax;
        }
        decimal priceMargin = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["priceMargin"]);
        if (Math.Abs(xmlPrice - itineraryPrice) > priceMargin)
        {
            statusDescription = "Itinerary price is different from booking price";
            statusCode = "10";
            return false;
        }
        //Pax Detail Check
        List<FlightPassenger> paxInfoList = new List<FlightPassenger>();
        XmlNodeList paxNodeList = resvNode.SelectNodes("PassengerList/item");
        foreach (XmlNode paxNode in paxNodeList)
        {
            FlightPassenger paxInfo = new FlightPassenger();
            tempNode = paxNode.SelectSingleNode("TypeCode/text()");
            if (tempNode != null)
            {
                switch (tempNode.Value)
                {
                    case "ADT": paxInfo.Type = PassengerType.Adult;
                        break;
                    case "CHD": paxInfo.Type = PassengerType.Child;
                        break;
                    case "INF": paxInfo.Type = PassengerType.Infant;
                        break;
                    default: break;
                }
            }
            tempNode = paxNode.SelectSingleNode("FirstName/text()");
            if (tempNode != null)
            {
                paxInfo.FirstName = tempNode.Value;
            }
            tempNode = paxNode.SelectSingleNode("Title/text()");
            if (tempNode != null)
            {
                paxInfo.Title = tempNode.Value;
            }
            tempNode = paxNode.SelectSingleNode("LastName/text()");
            if (tempNode != null)
            {
                paxInfo.LastName = tempNode.Value;
            }
            paxInfoList.Add(paxInfo);
        }
        //checking the pax name
        if (paxInfoList.Count == itinerary.Passenger.Length)
        {
            int i = 0;
            foreach (FlightPassenger pax in itinerary.Passenger)
            {
                if (pax.FirstName.ToUpper() != paxInfoList[i].FirstName.ToUpper() || pax.LastName.ToUpper() != paxInfoList[i].LastName.ToUpper())
                {
                    statusDescription = "Pax Name is not matching";
                    statusCode = "10";
                    return false;
                }
                i++;
            }
        }
        else
        {
            statusDescription = "Pax Name is not matching";
            statusCode = "10";
            return false;
        }
        //Segment Check
        XmlNode segNode = resvNode.SelectSingleNode("AirComponents/item");
        //calss code
        string classCode = string.Empty;
        XmlNode classNode = segNode.SelectSingleNode("SelectedFareBasisCode/text()");
        if (classNode != null)
        {
            classCode = classNode.Value;
        }
        XmlNodeList flightSegList = segNode.SelectNodes("Flights/item");
        List<FlightInfo> segList = new List<FlightInfo>();
        // Flight string is used for getting fare quote request - as it is taking filght legs as it is coming in avaialibility response
        foreach (XmlNode fltNode in flightSegList)
        {
            FlightInfo info = new FlightInfo();
            info.BookingClass = classCode;
            // Airline
            tempNode = fltNode.SelectSingleNode("CarrierCode");
            if (tempNode != null)
            {
                info.Airline = tempNode.InnerText;
            }
            else
            {
                info.Airline = string.Empty;
            }
            // FlightNumber
            tempNode = fltNode.SelectSingleNode("FlightNumber");
            if (tempNode != null)
            {
                info.FlightNumber = tempNode.InnerText;
            }
            else
            {
                info.FlightNumber = string.Empty;
            }
            // Origin
            tempNode = fltNode.SelectSingleNode("Origin");
            if (tempNode != null)
            {
                info.Origin = new Airport(tempNode.InnerText);
            }
            else
            {
                info.Origin = new Airport();
            }
            // Destination
            tempNode = fltNode.SelectSingleNode("Destination");
            if (tempNode != null)
            {
                info.Destination = new Airport(tempNode.InnerText);
            }
            else
            {
                info.Destination = new Airport();
            }
            segList.Add(info);
        }
        //segment check here
        if (itinerary.Segments.Length == segList.Count)
        {
            int i = 0;
            foreach (FlightInfo fInfo in itinerary.Segments)
            {
                if (fInfo.Airline.ToUpper() != segList[i].Airline.ToUpper() || fInfo.FlightNumber.ToUpper() != segList[i].FlightNumber.ToUpper())
                {
                    statusDescription = "Either Airline code or FlightNo is not matching";
                    statusCode = "10";
                    return false;
                }
                else if (fInfo.Origin.AirportCode.ToUpper() != segList[i].Origin.AirportCode.ToUpper() || fInfo.Destination.AirportCode.ToUpper() != segList[i].Destination.AirportCode.ToUpper())
                {
                    statusDescription = "Either origin or destination sector not matching";
                    statusCode = "10";
                    return false;
                }
                i++;
            }
        }
        else
        {
            statusDescription = "Sector not matching";
            statusCode = "10";
            return false;//missing segment
        }
        return true;
    }
    private string GetTicketRequestXml(WSTicketRequest ticket)
    {
        MemoryStream stream = null;
        TextWriter writer = null;
        try
        {
            stream = new MemoryStream(); // read xml in memory
            writer = new StreamWriter(stream, Encoding.Unicode);
            // get serialise object
            XmlRootAttribute root = new XmlRootAttribute("WSTicketRequest");
            XmlSerializer serializer = new XmlSerializer(typeof(WSTicketRequest), root);

            serializer.Serialize(writer, ticket); // read object
            int count = (int)stream.Length; // saves object in memory stream

            byte[] arr = new byte[count];
            stream.Seek(0, SeekOrigin.Begin);
            // copy stream contents in byte array

            stream.Read(arr, 0, count);
            UnicodeEncoding utf = new UnicodeEncoding(); // convert byte array to string

            return utf.GetString(arr).Trim();
        }
        catch
        {
            return string.Empty;
        }
        finally
        {
            if (stream != null) stream.Close();
            if (writer != null) writer.Close();
        }
    }

    # region Baggage Details
    [WebMethod]
    [SoapHeader("Credential")]
    public DataTable GetBaggageDetails(WSGetFareQuoteRequest fareQuoteRequest)
    {
        DataTable dtBaggage = new DataTable("dtBaggage");
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        if (UserMaster != null)
        {
            if (!(WSBaggageInput(fareQuoteRequest)))
            {
                WSGetFareQuoteResponse errorResponse = new WSGetFareQuoteResponse();
                WSStatus status = new WSStatus();
                status.Category = "BG";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.GetFareQuote, Severity.High, (int)UserMaster.ID, "Baggage Details Input parameter is incorrect: " + statusDescription + " Is LCC flight " + fareQuoteRequest.Result.IsLcc, "");
                //return errorResponse;
            }
            string ipAddr = this.Context.Request.UserHostAddress;
            MetaSearchEngine mse = new MetaSearchEngine(fareQuoteRequest.SessionId);
            SearchResult res = new SearchResult();
            //res = fareQuoteRequest.Result.getResult();
            res = Basket.FlightBookingSession[fareQuoteRequest.SessionId].Result[fareQuoteRequest.Result.ResultId];
            WSUserPreference userPref = new WSUserPreference();
            userPref.Load((int)UserMaster.ID);
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            string sessionId = string.Empty;
            if (fareQuoteRequest.SessionId != null)
            {
                sessionId = fareQuoteRequest.SessionId;
            }
            try
            {
                UserPreference pref = new UserPreference();
                int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
                CT.TicketReceipt.BusinessLayer.LoginInfo login = UserMaster.GetB2CUser(UserId);
                if (fareQuoteRequest.Result.Source == BookingSource.AirArabia)
                {
                    AirArabia airArabia = new AirArabia();
                    res.Airline = "G9";//Add missing airline
                    CT.TicketReceipt.BusinessLayer.SourceDetails source = login.AgentSourceCredentials["G9"];
                    airArabia.UserName = source.UserID;
                    airArabia.Password = source.Password;
                    airArabia.Code = source.HAP;
                    airArabia.SessionID = sessionId;
                    airArabia.ExchangeRates = login.AgentExchangeRates;
                    airArabia.DecimalPoint = login.DecimalValue;
                    airArabia.AgentCurrency = login.Currency;
                    airArabia.AppUserId = UserMaster.ID.ToString();

                    dtBaggage = airArabia.GetBaggageDetails(res, sessionId);
                    dtBaggage.TableName = "dtBaggage";
                    // ziya test serielizer
                    //System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(dtBaggage.GetType());
                    //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    //System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    //ser.Serialize(writer, dtBaggage); 	// Here Classes are converted to XML String. 
                    // This can be viewed in SB or writer.
                    // Above XML in SB can be loaded in XmlDocument object
                    //XmlDocument doc = new XmlDocument();
                    //doc.LoadXml(sb.ToString());
                    //baggageDetails = doc.InnerXml.ToString();
                    // doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\testDTBaggage.xml");
                    //end test
                }
                else if (fareQuoteRequest.Result.Source == BookingSource.FlyDubai)
                {
                    List<int> onwardLFIDS = new List<int>();
                    List<int> returnLFIDS = new List<int>();

                    Airport origin = new Airport(fareQuoteRequest.Result.Origin);

                    List<FlightInfo> outFlights = new List<FlightInfo>();
                    List<FlightInfo> inFlights = new List<FlightInfo>();
                    FlightInfo[][] flights = new FlightInfo[2][];
                    foreach (WSSegment seg in fareQuoteRequest.Result.Segment)
                    {
                        FlightInfo fi = new FlightInfo();
                        fi.Airline = seg.Airline.AirlineCode;
                        fi.ArrivalTime = seg.ArrTime;
                        fi.ArrTerminal = "";
                        fi.CabinClass = seg.FareCabinClass;
                        fi.Craft = seg.Craft;
                        fi.DepartureTime = seg.DepTIme;
                        fi.Destination = new Airport(seg.Destination.AirportCode);
                        fi.Duration = TimeSpan.Parse(seg.Duration);
                        fi.FareInfoKey = seg.FareInfoKey;
                        fi.Group = seg.SegmentIndicator;
                        fi.Origin = new Airport(seg.Origin.AirportCode);
                        fi.UapiDepartureTime = seg.UapiDepartureTime;
                        fi.UapiSegmentRefKey = seg.UapiSegmentRefKey;
                        fi.UapiArrivalTime = seg.UapiArrivalTime;

                        if (seg.Group == 0)
                        {                            
                            outFlights.Add(fi);
                        }
                        else
                        {
                            inFlights.Add(fi);
                        }
                    }

                    flights[0] = outFlights.ToArray();
                    flights[1] = inFlights.ToArray();
                    

                    for (int i = 0; i < flights.Length; i++)
                    {
                        for (int j = 0; j < flights[i].Length; j++)
                        {
                            if (i == 0)
                            {
                                onwardLFIDS.Add(Convert.ToInt32(flights[i][j].UapiSegmentRefKey));
                            }
                            else
                            {
                                returnLFIDS.Add(Convert.ToInt32(flights[i][j].UapiSegmentRefKey));
                            }
                        }
                    }
                    
                    CT.BookingEngine.GDS.FlyDubaiApi flyDubai = new CT.BookingEngine.GDS.FlyDubaiApi(flights[0][0].Origin.AirportCode, fareQuoteRequest.SessionId);
                    CT.TicketReceipt.BusinessLayer.SourceDetails agentDetails = new CT.TicketReceipt.BusinessLayer.SourceDetails();

                    if (login.IsOnBehalfOfAgent)
                    {
                        flyDubai.AgentBaseCurrency = login.OnBehalfAgentCurrency;
                        flyDubai.ExchangeRates = login.OnBehalfAgentExchangeRates;
                        flyDubai.AgentDecimalValue = login.OnBehalfAgentDecimalValue;
                        agentDetails = login.OnBehalfAgentSourceCredentials["FZ"];
                    }
                    else
                    {
                        flyDubai.AgentBaseCurrency = login.Currency;
                        flyDubai.ExchangeRates = login.AgentExchangeRates;
                        flyDubai.AgentDecimalValue = login.DecimalValue;
                        agentDetails = login.AgentSourceCredentials["FZ"];
                    }

                    flyDubai.LoginName = agentDetails.UserID;
                    flyDubai.Password = agentDetails.Password;
                    
                    dtBaggage = flyDubai.GetBaggageServicesQuotes(origin, flights, fareQuoteRequest.Result.GUID);
                    dtBaggage.TableName = "dtBaggage";
                }
                else if (fareQuoteRequest.Result.Source == BookingSource.AirIndiaExpressIntl)
                {
                    List<int> onwardLFIDS = new List<int>();
                    List<int> returnLFIDS = new List<int>();
                    Airport origin = new Airport(fareQuoteRequest.Result.Origin);
                    List<FlightInfo> outFlights = new List<FlightInfo>();
                    List<FlightInfo> inFlights = new List<FlightInfo>();
                    FlightInfo[][] flights = new FlightInfo[2][];
                    foreach (WSSegment seg in fareQuoteRequest.Result.Segment)
                    {
                        FlightInfo fi = new FlightInfo();
                        fi.Airline = seg.Airline.AirlineCode;
                        fi.ArrivalTime = seg.ArrTime;
                        fi.ArrTerminal = "";
                        fi.CabinClass = seg.FareCabinClass;
                        fi.Craft = seg.Craft;
                        fi.DepartureTime = seg.DepTIme;
                        fi.Destination = new Airport(seg.Destination.AirportCode);
                        fi.Duration = TimeSpan.Parse(seg.Duration);
                        fi.FareInfoKey = seg.FareInfoKey;
                        fi.Group = seg.SegmentIndicator;
                        fi.Origin = new Airport(seg.Origin.AirportCode);
                        fi.UapiDepartureTime = seg.UapiDepartureTime;
                        fi.UapiSegmentRefKey = seg.UapiSegmentRefKey;
                        fi.UapiArrivalTime = seg.UapiArrivalTime;

                        if (seg.Group == 0)
                        {
                            outFlights.Add(fi);
                        }
                        else
                        {
                            inFlights.Add(fi);
                        }
                    }
                    flights[0] = outFlights.ToArray();
                    flights[1] = inFlights.ToArray();
                    for (int i = 0; i < flights.Length; i++)
                    {
                        for (int j = 0; j < flights[i].Length; j++)
                        {
                            if (i == 0)
                            {
                                onwardLFIDS.Add(Convert.ToInt32(flights[i][j].UapiSegmentRefKey));
                            }
                            else
                            {
                                returnLFIDS.Add(Convert.ToInt32(flights[i][j].UapiSegmentRefKey));
                            }
                        }
                    }
                    CT.BookingEngine.GDS.AirIndiaExpressApi airIndiaExpress = new CT.BookingEngine.GDS.AirIndiaExpressApi();
                    SourceDetails agentDetails = new SourceDetails();
                    
                    {
                        airIndiaExpress.AgentBaseCurrency = login.Currency;
                        airIndiaExpress.ExchangeRates = login.AgentExchangeRates;
                        airIndiaExpress.AgentDecimalValue = login.DecimalValue;
                        agentDetails = login.AgentSourceCredentials["IX"];
                    }
                    airIndiaExpress.AppUserId = (int)UserMaster.ID;
                    airIndiaExpress.LogonId = agentDetails.UserID;
                    airIndiaExpress.Password = agentDetails.Password;
                    airIndiaExpress.SessionId = sessionId;
                    dtBaggage = airIndiaExpress.GetBaggageServicesQuotes(res, fareQuoteRequest.Result.GUID);
                    dtBaggage.TableName = "dtBaggage";
                }
                else if (fareQuoteRequest.Result.Source == BookingSource.TBOAir)
                {
                    TBOAir.AirV10 tbo = new TBOAir.AirV10();

                    CT.TicketReceipt.BusinessLayer.SourceDetails agentDetails = new CT.TicketReceipt.BusinessLayer.SourceDetails();

                    
                    {
                        tbo.AgentBaseCurrency = login.Currency;
                        tbo.ExchangeRates = login.AgentExchangeRates;
                        tbo.AgentDecimalValue = login.DecimalValue;
                        agentDetails = login.AgentSourceCredentials["TA"];
                    }

                    tbo.SessionId = fareQuoteRequest.SessionId;
                    tbo.AppUserId = (int)login.UserID;
                    tbo.LoginName = agentDetails.UserID;
                    tbo.Password = agentDetails.Password;

                    dtBaggage = tbo.GetBaggage(ref res);
                    dtBaggage.TableName = "dtBaggage";
                }
                else if (fareQuoteRequest.Result.Source == BookingSource.Indigo)
                {
                    CT.BookingEngine.GDS.IndigoAPI Indigo = new CT.BookingEngine.GDS.IndigoAPI();
                    SourceDetails agentDetails = new SourceDetails();
                    if (res != null && res.ResultBookingSource == BookingSource.Indigo)
                    {
                       
                        {
                            Indigo.AgentBaseCurrency = login.Currency;
                            Indigo.ExchangeRates = login.AgentExchangeRates;
                            Indigo.AgentDecimalValue = login.DecimalValue;
                            agentDetails = login.AgentSourceCredentials["6E"];

                            Indigo.LoginName = login.AgentSourceCredentials["6E"].UserID;
                            Indigo.Password = login.AgentSourceCredentials["6E"].Password;
                            Indigo.AgentDomain = login.AgentSourceCredentials["6E"].HAP;
                            Indigo.PromoCode = login.AgentSourceCredentials["6E"].PCC;//Added For Corporate booking Purpose
                        }
                        Indigo.AppUserId = Convert.ToInt32(login.UserID);
                        Indigo.BookingSourceFlag = "6E";
                        Indigo.SessionId = fareQuoteRequest.SessionId;
                        Indigo.CurrencyCode = (login.Currency == "INR") ? "INR" : "AED";
                        dtBaggage = Indigo.GetAvailableSSR(res);
                        dtBaggage.TableName = "dtBaggage";
                    }
                }
                else if (fareQuoteRequest.Result.Source == BookingSource.SpiceJet)
                {
                    CT.BookingEngine.GDS.SpiceJetAPIV1 spiceJet = new CT.BookingEngine.GDS.SpiceJetAPIV1();
                    SourceDetails agentDetails = new SourceDetails();
                    if (res != null && res.ResultBookingSource == BookingSource.SpiceJet)
                    {
                        
                        {
                            spiceJet.AgentBaseCurrency = login.Currency;
                            spiceJet.ExchangeRates = login.AgentExchangeRates;
                            spiceJet.AgentDecimalValue = login.DecimalValue;
                            agentDetails = login.AgentSourceCredentials["SG"];

                            spiceJet.LoginName = login.AgentSourceCredentials["SG"].UserID;
                            spiceJet.Password = login.AgentSourceCredentials["SG"].Password;
                            spiceJet.AgentDomain = login.AgentSourceCredentials["SG"].HAP;
                            spiceJet.PromoCode = login.AgentSourceCredentials["SG"].PCC;//Added For Corporate booking Purpose
                        }
                        spiceJet.AppUserID = Convert.ToInt32(login.UserID);
                        spiceJet.BookingSourceFlag = "SG";
                        spiceJet.CurrencyCode = (login.Currency == "INR") ? "INR" : "AED";
                        spiceJet.SessionID = fareQuoteRequest.SessionId;
                        dtBaggage = spiceJet.GetAvailableSSR(res);
                        dtBaggage.TableName = "dtBaggage";
                    }
                }
                else if (fareQuoteRequest.Result.Source == BookingSource.GoAir)
                {
                    CT.BookingEngine.GDS.GoAirAPI goAirObj = new CT.BookingEngine.GDS.GoAirAPI();
                   
                    if (res != null && res.ResultBookingSource == BookingSource.GoAir)
                    {
                        
                        {
                            goAirObj.LoginName = login.AgentSourceCredentials["G8"].UserID;
                            goAirObj.Password = login.AgentSourceCredentials["G8"].Password;
                            goAirObj.AgentDomain = login.AgentSourceCredentials["G8"].HAP;
                            goAirObj.AgentBaseCurrency = login.Currency;
                            goAirObj.ExchangeRates = login.AgentExchangeRates;
                            goAirObj.AgentDecimalValue = login.DecimalValue;
                            goAirObj.PromoCode = login.AgentSourceCredentials["G8"].PCC;//Added For Corporate booking Purpose
                        }
                        goAirObj.CurrencyCode = (login.Currency == "INR") ? "INR" : "AED";
                        goAirObj.AppUserId = Convert.ToInt32(login.UserID);
                        goAirObj.BookingSourceFlag = "G8";
                        goAirObj.SessionId = fareQuoteRequest.SessionId;
                        dtBaggage = goAirObj.GetAvailableSSR(res);
                        dtBaggage.TableName = "dtBaggage";
                    }
                }
            }
            catch (Exception ex)
            {
                WSStatus status = new WSStatus();
                status.Description = ex.Message;
                status.Category = "BG";
                status.StatusCode = "02";
                Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)UserMaster.ID, "B2C-GetBaggageDetails Failed. Error : " + ex.ToString() + " Stack trace " + ex.StackTrace, ipAddr);
                //SendErrorMail("while Baggae details from MSE", UserMaster, ex, sessionId);
               // return fareQuoteResponse;
            }
            
        }
        else
        {
            WSGetFareQuoteResponse fareQuoteResponse = new WSGetFareQuoteResponse();
            WSStatus status = new WSStatus();
            status.Description = "Baggae Details Authentication Failed.";
            status.Category = "FQ";
            status.StatusCode = "01";
            fareQuoteResponse.Status = status;
           // return fareQuoteResponse;
        }
        return dtBaggage;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public DataTable GetMealDetails(WSGetFareQuoteRequest fareQuoteRequest)
    {
        WSGetFareQuoteResponse fareQuoteResponse = new WSGetFareQuoteResponse();
        DataTable dtMeals = new DataTable("dtMeals");
        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);
        if (userMaster != null)
        {
            if (!(WSBaggageInput(fareQuoteRequest)))
            {
                WSStatus status = new WSStatus();
                status.Category = "BG";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                fareQuoteResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.GetFareQuote, Severity.High, (int)userMaster.ID, "Meal Details Input parameter is incorrect: " + statusDescription + " Is LCC flight " + fareQuoteRequest.Result.IsLcc, "");
            }
            string ipAddr = this.Context.Request.UserHostAddress;
            MetaSearchEngine mse = new MetaSearchEngine(fareQuoteRequest.SessionId);
            SearchResult res = new SearchResult();
            //res = fareQuoteRequest.Result.getResult();
            res = Basket.FlightBookingSession[fareQuoteRequest.SessionId].Result[fareQuoteRequest.Result.ResultId];
            WSUserPreference userPref = new WSUserPreference();
            userPref.Load((int)userMaster.ID);

            string sessionId = string.Empty;
            if (fareQuoteRequest.SessionId != null)
            {
                sessionId = fareQuoteRequest.SessionId;
            }
            try
            {
                UserPreference pref = new UserPreference();
                int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
                CT.TicketReceipt.BusinessLayer.LoginInfo login = UserMaster.GetB2CUser(UserId);
                mse.SettingsLoginInfo = login;
                if (fareQuoteRequest.Result.Source == BookingSource.AirArabia)
                {                   
                    dtMeals = mse.GetMealInfo(res, sessionId);
                    dtMeals.TableName = "dtMeals";
                }
            }
            catch (Exception ex)
            {
                WSStatus status = new WSStatus();
                status.Description = ex.Message;
                status.Category = "FQ";
                status.StatusCode = "01";
                fareQuoteResponse.Status = status;
            }
        }
        else
        {
            WSStatus status = new WSStatus();
            status.Description = "Meal Details Authentication Failed.";
            status.Category = "FQ";
            status.StatusCode = "01";
            fareQuoteResponse.Status = status;
        }

        return dtMeals;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public void GetUpdatedGSTFareBreakUp(ref WSResult WsResult, WSSearchRequest request, int resultId, string sessionId)
    {
        try
        {
            if (Basket.FlightBookingSession.ContainsKey(sessionId))
            {
                MetaSearchEngine mse = new MetaSearchEngine();
                UserMaster userMaster = new UserMaster();
                userMaster = WSValidate(Credential);
                int UserId = 0;
                UserPreference pref = new UserPreference();
                UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
                AgentLoginInfo = UserMaster.GetB2CUser(UserId);

                SearchRequest searchRequest = new SearchRequest();
                searchRequest.AdultCount = request.AdultCount;
                searchRequest.ChildCount = request.ChildCount;
                searchRequest.InfantCount = request.InfantCount;
                SearchResult searchResult = Basket.FlightBookingSession[sessionId].Result[resultId];
                if (searchResult.Flights.Length == 1)
                {
                    searchRequest.Type = SearchType.OneWay;
                    searchRequest.Segments = new FlightSegment[1];
                    searchRequest.Segments[0] = new FlightSegment();
                    searchRequest.Segments[0].Origin = request.Origin;
                    searchRequest.Segments[0].Destination = request.Destination;

                }
                else if (searchResult.Flights.Length == 2)
                {
                    searchRequest.Type = SearchType.Return;
                    searchRequest.Segments = new FlightSegment[2];
                    searchRequest.Segments[0] = new FlightSegment();
                    searchRequest.Segments[0].Origin = request.Origin;
                    searchRequest.Segments[0].Destination = request.Destination;
                    searchRequest.Segments[1] = new FlightSegment();
                    searchRequest.Segments[1].Origin = request.Destination;
                    searchRequest.Segments[1].Destination = request.Origin;
                }
                mse.SettingsLoginInfo = AgentLoginInfo;
                mse.TransactionType = "B2C";
                
                if (searchResult.ResultBookingSource == BookingSource.Indigo)
                    mse.Get6EUpdatedGSTFareBreakUp(ref searchResult, searchRequest);
                else if (searchResult.ResultBookingSource == BookingSource.SpiceJet)
                    mse.GetSGUpdatedGSTFareBreakUp(ref searchResult, searchRequest);
                else if (searchResult.ResultBookingSource == BookingSource.GoAir)
                    mse.GetG8UpdatedGSTFareBreakUp(ref searchResult, searchRequest);

                PriceAccounts price = searchResult.Price;
                price.PaxTypeTaxBreakUp = searchResult.Price.PaxTypeTaxBreakUp;
                WsResult.ResultId = resultId;
                WsResult.Source = searchResult.ResultBookingSource;
                WsResult.Fare = new WSFare();
                WsResult.Fare.TaxDetails = searchResult.Price.TaxDetails;
               
                WsResult.FareBreakdown = new WSPTCFare[searchResult.FareBreakdown.Length];
                PriceAccounts tempPrice = searchResult.Price;
                for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                {

                    searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, j, AgentLoginInfo.AgentId, 0, 1, "B2C");

                    searchResult.FareBreakdown[j].AgentMarkup = searchResult.Price.Markup;
                    searchResult.FareBreakdown[j].AgentDiscount = searchResult.Price.Discount+searchResult.Price.WhiteLabelDiscount;
                    searchResult.FareBreakdown[j].B2CMarkup = searchResult.Price.B2CMarkup;

                    WsResult.Fare.BaseFare += (decimal)(searchResult.FareBreakdown[j].BaseFare) + (searchResult.FareBreakdown[j].HandlingFee);
                    WsResult.Fare.Tax += searchResult.FareBreakdown[j].Tax;
                    WsResult.Fare.Discount += (searchResult.FareBreakdown[j].AgentDiscount * searchResult.FareBreakdown[j].PassengerCount);
                    WsResult.Fare.Currency = searchResult.Currency;
                    WsResult.Fare.Markup += searchResult.FareBreakdown[j].AgentMarkup * searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.Fare.MarkupType = searchResult.Price.MarkupType;
                    WsResult.Fare.MarkupValue = searchResult.Price.MarkupValue;
                    WsResult.Fare.B2CMarkup += searchResult.FareBreakdown[j].B2CMarkup * searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.Fare.B2CMarkupType = searchResult.Price.B2CMarkupType;
                    WsResult.Fare.B2CMarkupValue = searchResult.Price.B2CMarkupValue;
                    WsResult.Fare.SupplierCurrency = searchResult.Price.SupplierCurrency;
                    WsResult.Fare.SupplierPrice = searchResult.Price.SupplierPrice;
                   

                    WsResult.FareBreakdown[j] = new WSPTCFare();
                    WsResult.FareBreakdown[j].PassengerType = searchResult.FareBreakdown[j].PassengerType;
                    WsResult.FareBreakdown[j].PassengerCount = searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.FareBreakdown[j].B2CMarkup = searchResult.FareBreakdown[j].B2CMarkup;
                    WsResult.FareBreakdown[j].Markup = searchResult.FareBreakdown[j].AgentMarkup;
                    WsResult.FareBreakdown[j].SupplierCurrency = searchResult.Price.SupplierCurrency;
                    WsResult.FareBreakdown[j].SupplierPrice = (decimal)searchResult.FareBreakdown[j].SupplierFare;
                    WsResult.FareBreakdown[j].BaseFare = (decimal)searchResult.FareBreakdown[j].BaseFare + (searchResult.FareBreakdown[j].HandlingFee);
                    WsResult.FareBreakdown[j].AirlineTransFee = searchResult.FareBreakdown[j].AirlineTransFee;
                    WsResult.FareBreakdown[j].Tax = (decimal)(searchResult.FareBreakdown[j].TotalFare - searchResult.FareBreakdown[j].BaseFare);
                }

                if (price.TaxDetails != null && price.TaxDetails.OutputVAT != null)
                {
                    WsResult.Fare.OutputVATApplied = price.TaxDetails.OutputVAT.Applied;
                    WsResult.Fare.OutputVATCharge = price.TaxDetails.OutputVAT.Charge;
                    WsResult.Fare.OutputVATOn = price.TaxDetails.OutputVAT.AppliedOn.ToString();
                }

                Basket.FlightBookingSession[sessionId].Result[resultId] = searchResult;
            }
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.Exception, Severity.High, 0,ex.ToString(), "");

        }
    }


    private bool WSBaggageInput(WSGetFareQuoteRequest fareQuoteRequest)
    {
        if (fareQuoteRequest.Result == null)
        {
            statusDescription = "Result cannot be null";
            statusCode = "04";
            return false;
        }
        bool lccCheck = false;
        try
        {

            lccCheck = fareQuoteRequest.Result.IsLcc;
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.GetFareQuote, Severity.High, 0, "Airline.IsAirlineLCC(call 2nd) failed: " + ex.ToString() + ex.StackTrace, "");
        }
        if (!lccCheck)
        {
            statusDescription = "GetFareQuote handles only LCC airline";
            statusCode = "05";
            return false;
        }
        if (fareQuoteRequest.Result.Source != BookingSource.AirArabia && fareQuoteRequest.Result.Source != BookingSource.AirIndiaExpressIntl && fareQuoteRequest.Result.Source != BookingSource.FlyDubai && fareQuoteRequest.Result.Source != BookingSource.TBOAir && fareQuoteRequest.Result.Source != BookingSource.GoAir && fareQuoteRequest.Result.Source != BookingSource.Indigo && fareQuoteRequest.Result.Source != BookingSource.SpiceJet)// for Air Arabia baggage checking 
        {
            statusDescription = "Baggage Details handles only for LCC airline";
            statusCode = "07";
            return false;
        }
        if (fareQuoteRequest.SessionId == null || fareQuoteRequest.SessionId == "")
        {
            statusDescription = "sessionId cannot be null or blank";
            statusCode = "06";
            return false;
        }
        if (fareQuoteRequest.SessionId != null && fareQuoteRequest.SessionId.Contains(","))
        {
            statusDescription = "separte sessionId from comma and pass only one seesionId in request";
            statusCode = "06";
            return false;
        }
        if (!MetaSearchEngine.IsActiveSession(fareQuoteRequest.SessionId))
        {
            statusDescription = "Your booking session is expired please start a new search.";
            statusCode = "06";
            return false;
        }

        return true;
    }
    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetFareQuoteResponse GetBaggageFareQuote(WSGetFareQuoteRequest fareQuoteRequest, WSSearchRequest request, string[] RPH, List<string> paxBaggages)
    {
        UserMaster UserMaster = new UserMaster();
        UserMaster = WSValidate(Credential);
        SearchResult searchResult = null;
        if (UserMaster != null)
        {
            if (!(WSFareQuoteInput(fareQuoteRequest)))
            {
                WSGetFareQuoteResponse errorResponse = new WSGetFareQuoteResponse();
                WSStatus status = new WSStatus();
                status.Category = "FQ";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditBookingAPI(EventType.GetFareQuote, Severity.High, (int)UserMaster.ID, "Input parameter is incorrect: " + statusDescription + " Is LCC flight " + fareQuoteRequest.Result.IsLcc, "");
                return errorResponse;
            }
            string ipAddr = this.Context.Request.UserHostAddress;
            MetaSearchEngine mse = new MetaSearchEngine(fareQuoteRequest.SessionId);
            SearchResult res = new SearchResult();
           // res = fareQuoteRequest.Result.getResult();            
            WSUserPreference userPref = new WSUserPreference();
            userPref.Load((int)UserMaster.ID);
            
            WSGetFareQuoteResponse fareQuoteResponse = new WSGetFareQuoteResponse();
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            string sessionId = string.Empty;
           
            if (fareQuoteRequest.SessionId != null)
            {
                sessionId = fareQuoteRequest.SessionId;
                searchResult = Basket.FlightBookingSession[sessionId].Result[fareQuoteRequest.Result.ResultId];                     
            }
            try
            {
                SearchRequest mRequest = new SearchRequest();
                mRequest.AdultCount = request.AdultCount;
                mRequest.ChildCount = request.ChildCount;
                mRequest.InfantCount = request.InfantCount;
                mRequest.SeniorCount = request.SeniorCount;
                res = searchResult;
                UserPreference pref = new UserPreference();
                int UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
                AirArabia aa = new AirArabia();
                CT.TicketReceipt.BusinessLayer.LoginInfo login = UserMaster.GetB2CUser(UserId);
                CT.TicketReceipt.BusinessLayer.SourceDetails source = login.AgentSourceCredentials["G9"];
                aa.UserName = source.UserID;
                aa.Password = source.Password;
                aa.Code = source.HAP;
                aa.SessionID = sessionId;
                aa.ExchangeRates = login.AgentExchangeRates;
                aa.DecimalPoint = login.DecimalValue;
                aa.AgentCurrency = login.Currency;
                //Added for Updating Baggage fares returned in BaggagePriceQuote in G9
                //string in the Dictionary represents PaxSerialNo i.e. A1, A2, C3, I4 etc and decimal in the Dictionary represents Baggage fare                
                Dictionary<string, decimal> paxBagFares = new Dictionary<string, decimal>();
                string fareXml = "";
                //Loading the near by airport code.
                Dictionary<string, string> sectors = new Dictionary<string, string>();
                /*Checking if near by airport city is available using config for maping near by city*/
                if (ConfigurationSystem.SectorListConfig.ContainsKey("AirArabiaCityMapping"))
                {
                    sectors = ConfigurationSystem.SectorListConfig["AirArabiaCityMapping"];
                    if (sectors.ContainsKey(request.Destination))
                    {
                        request.Destination = sectors[request.Destination];
                    }                    
                    if (sectors.ContainsKey(request.Origin))
                    { 
                        request.Origin = sectors[request.Origin]; 
                    }                    
                }                
                CookieContainer cookie = new CookieContainer();
                AirArabia.SessionData data = (AirArabia.SessionData)Basket.BookingSession[sessionId][res.Airline + "-" + request.Origin +"-"+ request.Destination];
                //Modified by Lokesh on 4-April-2018
                //If the Customer is Travelling from India
                //Source is G9
                //If the Lead Pax Supplies the state code and TaxRegNo.
                string[] baggagePrice = aa.GetBaggageFare(ref res, mRequest, ref data, ref RPH, ref fareXml, sessionId, ref cookie, ref paxBaggages, fareQuoteRequest.StateCode,fareQuoteRequest.TaxRegNo, ref paxBagFares);
                if (searchResult != null)
                {
                    //Update Bag fares in the order as they are added in the Passenger collection
                    fareQuoteResponse.PaxBaggageFares = new List<decimal>();
                    foreach (KeyValuePair<string,decimal> pair in paxBagFares)
                    {
                        fareQuoteResponse.PaxBaggageFares.Add(pair.Value);
                    }
                    decimal tax = 0m, baseFare = 0m, markup = 0m, b2cMarkup = 0m, supplierPrice = 0m, inputVAT = 0m,discount=0m;
                    //Recalculate Markup added by Shiva 10 Apr 2018
                    res.Price = new PriceAccounts();
                    fareQuoteResponse.Fare = new WSPTCFare[res.FareBreakdown.Length];
                    for (int i = 0; i < res.FareBreakdown.Length; i++)
                    {
                        PriceAccounts price = CT.AccountingEngine.AccountingEngine.GetPrice(res, i, login.AgentId, 0, 1, "B2C");
                        res.FareBreakdown[i].B2CMarkup = price.B2CMarkup;
                        res.FareBreakdown[i].AgentMarkup = price.Markup;                        
                        fareQuoteResponse.Fare[i].B2CMarkup = price.B2CMarkup;
                        fareQuoteResponse.Fare[i].BaseFare = (decimal)res.FareBreakdown[i].BaseFare;
                        fareQuoteResponse.Fare[i].Markup = price.Markup;
                        fareQuoteResponse.Fare[i].PassengerCount = res.FareBreakdown[i].PassengerCount;
                        fareQuoteResponse.Fare[i].PassengerType = res.FareBreakdown[i].PassengerType;
                        fareQuoteResponse.Fare[i].SupplierCurrency = price.SupplierCurrency;
                        fareQuoteResponse.Fare[i].SupplierPrice = (decimal)res.FareBreakdown[i].SupplierFare;
                        fareQuoteResponse.Fare[i].Tax = res.FareBreakdown[i].Tax;
                        baseFare += fareQuoteResponse.Fare[i].BaseFare;
                        b2cMarkup += fareQuoteResponse.Fare[i].B2CMarkup;
                        markup += fareQuoteResponse.Fare[i].Markup;
                        supplierPrice += fareQuoteResponse.Fare[i].SupplierPrice;
                        tax += fareQuoteResponse.Fare[i].Tax;
                        discount += (price.Discount + price.WhiteLabelDiscount);
                        inputVAT = price.InputVATAmount;
                    }
                    fareQuoteRequest.Result.Fare.Tax = tax;
                    fareQuoteRequest.Result.Fare.BaseFare = baseFare;
                    fareQuoteRequest.Result.Fare.B2CMarkup = b2cMarkup;
                    fareQuoteRequest.Result.Fare.Markup = markup;
                    fareQuoteRequest.Result.Fare.InputVAT = inputVAT;
                    fareQuoteRequest.Result.Fare.Discount = discount;
                    fareQuoteRequest.Result.Fare.SupplierPrice = supplierPrice;
                    //Recalculate Output VAT
                    decimal vatAmount = 0m;
                    if (searchResult.Price != null && searchResult.Price.TaxDetails != null && searchResult.Price.TaxDetails.OutputVAT != null)
                    {
                        OutputVATDetail outVat = searchResult.Price.TaxDetails.OutputVAT;
                        markup = res.Price.Markup + res.Price.B2CMarkup;decimal baggageCharge = fareQuoteRequest.BaggageCharge;
                        vatAmount = outVat.CalculateVatAmount((decimal)res.TotalFare + baggageCharge, markup, res.Price.DecimalPoint);
                        fareQuoteRequest.Result.Fare.OutputVAT = (vatAmount);
                    }
                    fareQuoteResponse.Result = fareQuoteRequest.Result;
                    searchResult.FareBreakdown = res.FareBreakdown;
                    searchResult.Price.B2CMarkup = res.Price.B2CMarkup;
                    searchResult.Price.Markup = res.Price.Markup;
                }

                
                data.SelectedResultInfo = fareXml;
            }
            catch (Exception ex)
            {
                WSStatus status = new WSStatus();
                status.Description = ex.Message;
                status.Category = "FQ";
                status.StatusCode = "02";
                fareQuoteResponse.Status = status;
                Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)UserMaster.ID, "B2C-GetBaggageFareQuote Failed. Error : " + ex.ToString() + " Stack trace " + ex.StackTrace, ipAddr);
                SendErrorMail("while GetBaggageFare Quote from MSE", UserMaster, ex, sessionId);
                return fareQuoteResponse;
            }
            return fareQuoteResponse;
          
        }
        else
        {
            WSGetFareQuoteResponse fareQuoteResponse = new WSGetFareQuoteResponse();
            WSStatus status = new WSStatus();
            status.Description = "GetFareQuote Authentication Failed.";
            status.Category = "FQ";
            status.StatusCode = "01";
            fareQuoteResponse.Status = status;
            return fareQuoteResponse;
        }
       
    }
    # endregion
    # region insurance(Zeus)
    [WebMethod]
    [SoapHeader("Credential")]
    public CT.BookingEngine.Insurance.InsuranceResponse GetAvailableInsPlans(CT.BookingEngine.Insurance.InsuranceRequest request)
    {
        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);
        int UserId = 0;
        UserPreference pref = new UserPreference();
        UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
        AgentLoginInfo = UserMaster.GetB2CUser(UserId);
        CT.BookingEngine.Insurance.InsuranceResponse availPlans = new CT.BookingEngine.Insurance.InsuranceResponse();
        if (true)
        {
            try
            {
                MetaSearchEngine mse = new MetaSearchEngine();
                mse.SettingsLoginInfo = AgentLoginInfo;
                Audit.Add(EventType.InsuranceBooking, Severity.Low, UserId, "sending insurance request in Booking api", "");
                //Loading all plans
                availPlans = mse.GetAvailablePlans(request);
                if (availPlans != null && availPlans.OTAPlans != null && availPlans.OTAPlans.Count > 0)
                {
                    //calucating B2C markup
                    UpdatePrice(ref availPlans, AgentLoginInfo.AgentId);
                }
                Audit.Add(EventType.InsuranceBooking, Severity.Low, UserId, "getting plans insurance response in Booking api", "");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.InsuranceBooking, Severity.Low, UserId, "getting error Booking APi in GetAvailableInsPlans exception:"+ex.ToString(), "");
                availPlans.ErrorMessage = ex.ToString();
            }
        }
        return availPlans;

    }

    private void UpdatePrice(ref CT.BookingEngine.Insurance.InsuranceResponse availPlans,int agentId)
    {
        try
        {
            DataTable dtMarkup = new DataTable();
            if (agentId > 0)
            {
                //Getting all sources markup
                dtMarkup = UpdateMarkup.GetAllMarkup(agentId, (int)ProductType.Insurance);
            }
            ////calculate B2C Markup
            DataView dv = dtMarkup.DefaultView;
            dv.RowFilter = "TransType IN('B2C') AND SourceId='TUNES'";
            dtMarkup = dv.ToTable();
            decimal b2cMarkup = 0; string b2CMarkupType = string.Empty;
            if (dtMarkup != null && dtMarkup.Rows.Count > 0)
            {
                b2cMarkup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                b2CMarkupType = dtMarkup.Rows[0]["MarkupType"].ToString();
            }
            if (availPlans != null && availPlans.OTAPlans != null && availPlans.OTAPlans.Count > 0)
            {
                for (int i = 0; i < availPlans.OTAPlans.Count; i++)
                {
                    availPlans.OTAPlans[i].B2CMarkup = ((b2CMarkupType == "F") ? b2cMarkup : (Convert.ToDecimal(availPlans.OTAPlans[i].TotalAmount) * (b2cMarkup / 100m)));
                    availPlans.OTAPlans[i].B2CMarkupType = b2CMarkupType;
                    availPlans.OTAPlans[i].B2CMarkupValue = b2cMarkup;
                    availPlans.OTAPlans[i].TotalAmount = Math.Round((availPlans.OTAPlans[i].TotalAmount + availPlans.OTAPlans[i].B2CMarkup), availPlans.OTAPlans[i].DecimalValue);
                }
            }
            if (availPlans != null && availPlans.UpsellPlans != null && availPlans.UpsellPlans.Count > 0)
            {
                for (int i = 0; i < availPlans.UpsellPlans.Count; i++)
                {
                    availPlans.UpsellPlans[i].B2CMarkup = ((b2CMarkupType == "F") ? b2cMarkup : (Convert.ToDecimal(availPlans.UpsellPlans[i].TotalAmount) * (b2cMarkup / 100m)));
                    availPlans.UpsellPlans[i].B2CMarkupType = b2CMarkupType;
                    availPlans.UpsellPlans[i].B2CMarkupValue = b2cMarkup;
                    availPlans.UpsellPlans[i].TotalAmount = Math.Round((availPlans.UpsellPlans[i].TotalAmount + availPlans.UpsellPlans[i].B2CMarkup), availPlans.UpsellPlans[i].DecimalValue);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.InsuranceBooking, Severity.Low, 0, "getting error Booking APi in UpdatePrice Method exception:" + ex.ToString(), "");
        }
    }


    /// <summary>
    /// Method to Confirm the purchase of ZEUS Travel Insurance Policy for the Itinerary.
    /// </summary>
    /// <param name="plan"></param>
    /// <param name="request"></param>
    /// 
    [WebMethod]
    [SoapHeader("Credential")]
    public void ConfirmPolicyPurchase(ref CT.BookingEngine.Insurance.InsuranceHeader header)
    {
        try
        {
            int UserId = 0;
            UserPreference pref = new UserPreference();
            UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
            AgentLoginInfo = UserMaster.GetB2CUser(UserId);
            header.CreatedBy = (int)AgentLoginInfo.UserID;
            header.LocationId =(int)AgentLoginInfo.LocationID;
            header.AgentId = AgentLoginInfo.AgentId;
            header.Currency = AgentLoginInfo.Currency;
            MetaSearchEngine mse = new MetaSearchEngine();
            mse.SettingsLoginInfo = AgentLoginInfo;
            mse.ConfirmPolicyPurchase(ref header);

            if (header.Error == "0")
            {
                //Invoice 
                try
                {

                    Invoice invoice = new Invoice();
                    int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(header.Id, CT.BookingEngine.ProductType.Insurance);

                    if (invoiceNumber > 0)
                    {
                        invoice.Load(invoiceNumber);
                    }
                    else
                    {
                        invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(header.Id, "", (int)AgentLoginInfo.UserID, CT.BookingEngine.ProductType.Insurance, 1);
                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                            invoice.Status = CT.BookingEngine.InvoiceStatus.Paid;
                            invoice.CreatedBy = (int)AgentLoginInfo.UserID;
                            invoice.LastModifiedBy = (int)AgentLoginInfo.UserID;
                            invoice.UpdateInvoice();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.InsuranceBooking, Severity.High,(int)AgentLoginInfo.UserID, "B2C-Exception while Confirming ZEUS Travel Insurance Plan" + ex.ToString(), "");
                }
            }
            else
            {
                Audit.Add(EventType.InsuranceBooking, Severity.High, (int)AgentLoginInfo.UserID, "B2C-Exception while Confirming ZEUS Travel Insurance Plan" + header.Error, "");
            }
        }
        catch (Exception ex)
        {
            header.Error = "InsuranceBooking is Failed" + ex.ToString();
            Audit.Add(EventType.InsuranceBooking, Severity.High, 0, "B2C-Exception while Confirming ZEUS Travel Insurance Plan " + ex.ToString(), "");
        }
    }


    /// <summary>
    /// Method to Cancel the ZEUS Travel Insurance Policy.
    /// </summary>
    /// <param name="plan"></param>
    /// 
    //[WebMethod]
    //[SoapHeader("Credential")]
    //public void CancelPolicy(ref CT.BookingEngine.Insurance.InsurancePlan plan, CT.BookingEngine.Insurance.ZEUSIns request)
    //{
    //    try
    //    {
    //        request.CancelPolicy(ref plan);
    //    }
    //    catch (Exception ex)
    //    {
    //        CT.Core.Audit.Add(CT.Core.EventType.InsuranceBooking, CT.Core.Severity.High, 0, "B2C-Exception while Cancelling ZEUS Travel Insurance Plan " + ex.Message, "");
    //    }
    //}


    /// <summary>
    /// Method to get Insurance Policy.
    /// </summary>
    /// <param name="plan"></param>
    /// 
    [WebMethod]
    [SoapHeader("Credential")]
    public CT.BookingEngine.Insurance.InsuranceHeader RetrievePlanDetails(int headerId)
    {
        CT.BookingEngine.Insurance.InsuranceHeader header = new CT.BookingEngine.Insurance.InsuranceHeader();
        try
        {
            UserMaster UserMaster = new UserMaster();
            UserMaster = WSValidate(Credential);
            if (true)
            {
                string ipAddr = this.Context.Request.UserHostAddress;
                header.RetrieveConfirmedPlan(headerId);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.InsuranceBooking, Severity.High, 0, "B2C-Booking API Exception while fetching RetrievePlanDetails() " + ex.ToString(), "");
        }
        return header;

    }

    [WebMethod]
    [SoapHeader("Credential")]
    /// <summary>
    /// This method will Add Change Request Item for Insurance in B2B.
    /// </summary>
    /// <param name="planId"></param>
    /// <param name="remarks"></param>
    public void SendInsuranceChangeRequest(long planId, string remarks,int headerId)
    {
        ServiceRequest serviceRequest = new ServiceRequest();
        int requestTypeId = 3;
        UserPreference pref = new UserPreference();
        int userId = pref.GetMemberIdBySiteName(Credential.SiteName);
        LoginInfo loginInfo = UserMaster.GetB2CUser(userId);
        try
        {
            CT.BookingEngine.Insurance.InsuranceHeader header = new CT.BookingEngine.Insurance.InsuranceHeader();
            header.RetrieveConfirmedPlan(headerId);
            AgentMaster Agency = new AgentMaster(header.AgentId);
            string planTitle = string.Empty;
            string policyNo = string.Empty;
            if (header != null && header.InsPlans.Count > 0)
            {
                for (int i = 0; i < header.InsPlans.Count; i++)
                {
                    if (planId.ToString() == header.InsPlans[i].PlanId.ToString())
                    {
                        serviceRequest.BookingId = 0;
                        serviceRequest.ReferenceId = Convert.ToInt32(header.InsPlans[i].PlanId);
                        serviceRequest.ProductType = ProductType.Insurance;
                        serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                        serviceRequest.Data = remarks;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
                        serviceRequest.CreatedBy = Convert.ToInt32(loginInfo.UserID);
                        serviceRequest.PaxName = header.InsPassenger[0].FirstName + header.InsPassenger[0].LastName;
                        serviceRequest.Pnr = header.InsPlans[i].PolicyNo;
                        serviceRequest.StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["INS_CANCEL_DATE"])));
                        serviceRequest.SupplierName = "Tune Protect";
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                        serviceRequest.ReferenceNumber = header.InsPlans[i].PolicyNo;
                        serviceRequest.AgencyId = loginInfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.InsuranceBooking;
                        serviceRequest.DocName = "";

                        serviceRequest.AgencyId = loginInfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.InsuranceBooking;
                        serviceRequest.DocName = "";
                        serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                        serviceRequest.LastModifiedOn = DateTime.Now;
                        serviceRequest.CreatedOn = DateTime.Now;
                        //Added by shiva
                        serviceRequest.AgencyTypeId = CT.Core.Agencytype.Cash;
                        serviceRequest.RequestSourceId = RequestSource.BookingAPI;
                        serviceRequest.Save();

                        if (string.IsNullOrEmpty(planTitle))
                        {
                            planTitle = header.InsPlans[i].PlanTitle;
                        }
                        if (string.IsNullOrEmpty(policyNo))
                        {
                            policyNo = header.InsPlans[i].PolicyNo;
                        }
                        //Updating status
                        CT.BookingEngine.Insurance.InsurancePlan.Update(header.InsPlans[i].PlanId, (int)CT.BookingEngine.Insurance.InsuranceBookingStatus.CancelRequest, ZeusInsB2B.Zeus.ProposalStatus.PROPOSAL.ToString(), false);
                        CT.BookingEngine.Insurance.InsuranceHeader.UpdateInsuranceHeaderStatus(header.Id, (int)CT.BookingEngine.Insurance.InsuranceBookingStatus.CancelRequest);
                        break;
                    }
                }
            }
            Hashtable table = new Hashtable(3);
            table.Add("agentName", Agency.Name);
            table.Add("policyName", planTitle);
            table.Add("policyNo", policyNo);

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            UserMaster bookedUser = new UserMaster(header.CreatedBy);
            AgentMaster bookedAgency = new AgentMaster(header.AgentId);
            toArray.Add(bookedUser.Email);
            toArray.Add(bookedAgency.Email1);
            toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]))
            {
                string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]).Split(';');
                foreach (string cnMail in cancelMails)
                {
                    toArray.Add(cnMail);
                }
            }

            string message = ConfigurationManager.AppSettings["INSURANCE_CHANGE_REQUEST"]; //"Your request for cancelling Insurance plan <b>" + plan.Title + " </b> is under process. Policy No:(" + plan.PolicyNo + ")";
            try
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Insurance) Request for cancellation. Policy No:(" + policyNo + ")", message, table);
            }
            catch
            {
            }
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.InsuranceBooking, CT.Core.Severity.High, 0, "B2C-Booking API Exception while Sending B2C Change Request() " + ex.ToString(), HttpContext.Current.Request["REMOTE_ADDR"]);
        }

    }

    # endregion

    [WebMethod]
    [SoapHeader("Credential")]
    public DataTable GetProductPromotions(int productId, DateTime bookingDate, DateTime travelDate, string propertyCode, decimal minTranxAmount, int minPaxCount, int minRoomCount, string city, string source)
    {
        DataTable dtPromotions = new DataTable("Promotions");
        try
        {
            UserPreference pref = new UserPreference();
            int memberId = pref.GetMemberIdBySiteName(Credential.SiteName);

            dtPromotions = PromoMaster.GetProductPromotions(productId, bookingDate, travelDate, propertyCode, minTranxAmount, minPaxCount, minRoomCount, city, source, memberId,0);
            dtPromotions.TableName = "Promotions";
        }
        catch 
        {

        }
        return dtPromotions;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WEGOSearchResponse APISearch(WSSearchRequest request)
    {
        /******* Start: User Authentication and IP restriction for Wego Portals *********/

        WEGOSearchResponse apiResponse = new WEGOSearchResponse();
        UserPreference pref = new UserPreference();
        bool userAuth = false; //Flag which determines whether the request is valid or not.
        string userIPAddress = string.Empty;
        int UserId = 0; //Member Id.
        string userAccountCode = string.Empty;
        string userName = string.Empty;
        string password = string.Empty;
        string xmlPath = ConfigurationManager.AppSettings["WEGOXmlLogs"];

        if (!Directory.Exists(xmlPath))
        {
            Directory.CreateDirectory(xmlPath);
        }

        WEGOSearchRequest wegoSearchReq = new WEGOSearchRequest();
        wegoSearchReq.Credential = Credential;
        wegoSearchReq.SearchRequest = request;
        wegoSearchReq.IPAddress = HttpContext.Current.Request["REMOTE_ADDR"]; 

        try
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(wegoSearchReq.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, wegoSearchReq);  
            string filepath = xmlPath + "SearchRequest_" + DateTime.Now.ToString("yyyyMMMdd_hhmmss") + ".xml";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
            doc.Save(filepath);

           // Audit.Add(EventType.Search, Severity.Low, UserId, "(" + Credential.SiteName + ") Search Request" + filepath, "", request.Sources);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, UserId, "(" + Credential.SiteName + ") Failed to write search request xml. Reason : " + ex.ToString(), "", request.Sources);
        }

        if (!string.IsNullOrEmpty(Credential.SiteName))
        {
            //Audit.Add(EventType.Search, Severity.High, UserId, "Inside the first if loop.Passed site name" + Credential.SiteName,"");

            if (GetUserMasterBySiteName(Credential.SiteName) != null)
            {
               // Audit.Add(EventType.Search, Severity.High, UserId, "Inside the second if loop.Passed site name as there is a member id for the site name" ,"");


                UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
                Dictionary<string, string> prefList = UserPreference.GetPreferenceList(UserId, ItemType.Flight);
                if (prefList.ContainsKey(WLPreferenceKeys.SiteIPAddress))//SITEIPADDRESS
                {
                    userIPAddress = prefList[WLPreferenceKeys.SiteIPAddress];
                }
                if (prefList.ContainsKey(WLPreferenceKeys.AccountCode))//ACCOUNTCODE
                {
                    userAccountCode = prefList[WLPreferenceKeys.AccountCode];
                }
                if (prefList.ContainsKey(WLPreferenceKeys.UserName)) //USERNAME
                {
                    userName = prefList[WLPreferenceKeys.UserName];
                }
                if (prefList.ContainsKey(WLPreferenceKeys.Password))//PASSWORD
                {
                    password = prefList[WLPreferenceKeys.Password];
                }
                if (string.IsNullOrEmpty(Credential.UserName))
                {
                    userAuth = false;
                    apiResponse.ErrorMessage = "Invalid Username.";
                }
                else if (string.IsNullOrEmpty(Credential.Password))
                {
                    userAuth = false;
                    apiResponse.ErrorMessage = "Invalid Password.";
                }
                else if (string.IsNullOrEmpty(Credential.AccountCode))
                {
                    userAuth = false;
                    apiResponse.ErrorMessage = "Invalid Account Code.";
                }
                else if (!userIPAddress.Contains(HttpContext.Current.Request["REMOTE_ADDR"]))
                {
                    userAuth = false;
                    apiResponse.ErrorMessage = "Invalid IP Adress.You are not authorised to access the service.";
                }
                else
                {
                    if (Credential.UserName.ToLower() == userName.ToLower() && Credential.Password == password && Credential.AccountCode.ToLower() == userAccountCode.ToLower())
                    {
                        userAuth = true;
                    }
                    else
                    {
                        userAuth = false;
                        apiResponse.ErrorMessage = "Invalid Username or Password or AccountCode or IP Address";
                    }
                }
            }
            else
            {
                userAuth = false;
                apiResponse.ErrorMessage = "Invalid Site Name !";
            }
        }
        else
        {
            userAuth = false;
            apiResponse.ErrorMessage = "Invalid Site Name !";
        }
        /******* End: User Authentication and IP restriction for Wego Portals *********/

        if (userAuth) //User Successfully authenticated.
        {
            #region WEGO FlightSearch and Result Object Formation.

            List<APIFlightItinerary> results = new List<APIFlightItinerary>();

            int userAgentId = 0; //User Agent Id


            try
            {
                WSSearch wSearch = new WSSearch();


                wSearch.AgentLoginInfo = UserMaster.GetB2CUser(UserId);
                userAgentId = wSearch.AgentLoginInfo.AgentId;


                /***************Start:Wego Audit**************************************************************************/
                Audit.Add(EventType.Search, Severity.High, UserId, "(" + Credential.SiteName + ") Wego Search in progress ", "", request.Sources);

                /***************End:Wego Audit******************************************************************************/


                WSSearchResponse response = wSearch.Search(request, Credential, this.Context.Request.UserHostAddress);
                //Get the preference values for the UserId and for the site name  which is used to navigate the user to the respective site url.
                pref = pref.GetPreference(UserId, UserPreference.AgentWebsite, "SETTINGS");

                if (response != null && response.Result != null)
                {
                    try
                    {
                        //Required in order to know the Markup values and ROE applied
                        //=================================== Write search response into xml file ======================================//
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(response.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                        ser.Serialize(writer, response);  // Here Classes are converted to XML String. 
                                                          //This can be viewed in SB or writer.
                                                          //Above XML in SB can be loaded in XmlDocument object
                        string filepath = xmlPath + "B2CWEGOSearchResponse_" + DateTime.Now.ToString("yyyyMMMdd_hhmmss") + ".xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());
                        doc.Save(filepath);
                    }
                    catch (Exception ex)
                    {
                        Audit.AddAuditBookingAPI(EventType.Search, Severity.High, 0, "(WEGO Search)Failed to write B2C search response xml. Reason : " + ex.ToString(), "");
                    }
                    Audit.Add(EventType.Search, Severity.Low, UserId, "(" + Credential.SiteName + ") Search Response Received", "", request.Sources);
                    foreach (WSResult result in response.Result)
                    {
                        APIFlightItinerary apiresult = new APIFlightItinerary();
                        apiresult.Price = new ItineraryPrice();
                        apiresult.Adults = request.AdultCount;
                        apiresult.Childs = request.ChildCount;
                        apiresult.Infants = request.InfantCount;

                        decimal otherCharges = (result.Fare.OtherCharges + result.Fare.TransactionFee + result.Fare.SServiceFee) / (request.AdultCount + request.ChildCount + request.InfantCount);
                        //for (int i = 0; i < result.FareBreakdown.Length; i++)
                        {
                            WSPTCFare adultFare = result.FareBreakdown[0];

                            apiresult.Price.CurrencyCode = result.Fare.Currency;

                            decimal PricePerAdult = (adultFare.BaseFare + adultFare.Tax + adultFare.AdditionalTxnFee + adultFare.AirlineTransFee + (otherCharges * adultFare.PassengerCount ));
                            if (result.Fare.MarkupType == "P")
                            {
                                PricePerAdult += (adultFare.Markup * result.FareBreakdown[0].PassengerCount);
                            }
                            else
                            {
                                PricePerAdult += result.Fare.MarkupValue * result.FareBreakdown[0].PassengerCount;
                            }

                            if (result.Fare.B2CMarkupType == "P")
                            {
                                PricePerAdult += adultFare.B2CMarkup * result.FareBreakdown[0].PassengerCount;
                            }
                            else
                            {
                                PricePerAdult += result.Fare.B2CMarkupValue * result.FareBreakdown[0].PassengerCount;
                            }

                            apiresult.Price.PricePerAdult = PricePerAdult;
                            apiresult.Price.TotalAmount = apiresult.Price.PricePerAdult;
                            if (request.ChildCount > 0 && result.FareBreakdown[1].PassengerType == PassengerType.Child)
                            {
                                WSPTCFare childFare = result.FareBreakdown[1];
                                decimal PricePerChild = (childFare.BaseFare + childFare.Tax + childFare.AdditionalTxnFee + childFare.AirlineTransFee + (otherCharges * childFare.PassengerCount));
                                if (result.Fare.MarkupType == "P")
                                {
                                    PricePerChild += (childFare.Markup * result.FareBreakdown[1].PassengerCount);
                                }
                                else
                                {
                                    PricePerChild += result.Fare.MarkupValue * result.FareBreakdown[1].PassengerCount;
                                }

                                if (result.Fare.B2CMarkupType == "P")
                                {
                                    PricePerChild += childFare.B2CMarkup * result.FareBreakdown[1].PassengerCount;
                                }
                                else
                                {
                                    PricePerChild += result.Fare.B2CMarkupValue * result.FareBreakdown[1].PassengerCount;
                                }

                                apiresult.Price.PricePerChild = PricePerChild;
                                apiresult.Price.TotalAmount += apiresult.Price.PricePerChild;
                            }
                            if ((request.ChildCount <= 0 && request.InfantCount > 0) && result.FareBreakdown[1].PassengerType == PassengerType.Infant)
                            {
                                WSPTCFare infantFare = result.FareBreakdown[1];
                                decimal PricePerInfant = (infantFare.BaseFare + infantFare.Tax + infantFare.AdditionalTxnFee + infantFare.AirlineTransFee + (otherCharges * infantFare.PassengerCount));
                                if (result.Fare.MarkupType == "P")
                                {
                                    PricePerInfant += (infantFare.Markup * result.FareBreakdown[1].PassengerCount);
                                }
                                else
                                {
                                    PricePerInfant += result.Fare.MarkupValue * result.FareBreakdown[1].PassengerCount;
                                }

                                if (result.Fare.B2CMarkupType == "P")
                                {
                                    PricePerInfant += infantFare.B2CMarkup * result.FareBreakdown[1].PassengerCount;
                                }
                                else
                                {
                                    PricePerInfant += result.Fare.B2CMarkupValue * result.FareBreakdown[1].PassengerCount;
                                }

                                apiresult.Price.PricePerInfant = PricePerInfant;
                                apiresult.Price.TotalAmount += apiresult.Price.PricePerInfant;
                            }
                            if ((request.InfantCount > 0 && request.ChildCount > 0) && result.FareBreakdown[2].PassengerType == PassengerType.Infant)
                            {
                                WSPTCFare infantFare = result.FareBreakdown[2];
                                decimal PricePerInfant = (infantFare.BaseFare + infantFare.Tax + infantFare.AdditionalTxnFee + infantFare.AirlineTransFee + (otherCharges * infantFare.PassengerCount));
                                if (result.Fare.MarkupType == "P")
                                {
                                    PricePerInfant += (infantFare.Markup * result.FareBreakdown[2].PassengerCount);
                                }
                                else
                                {
                                    PricePerInfant += result.Fare.MarkupValue * result.FareBreakdown[2].PassengerCount;
                                }

                                if (result.Fare.B2CMarkupType == "P")
                                {
                                    PricePerInfant += infantFare.B2CMarkup * result.FareBreakdown[2].PassengerCount;
                                }
                                else
                                {
                                    PricePerInfant += result.Fare.B2CMarkupValue * result.FareBreakdown[2].PassengerCount;
                                }

                                apiresult.Price.PricePerInfant = PricePerInfant;
                                apiresult.Price.TotalAmount += apiresult.Price.PricePerInfant;
                            }
                        }

                        apiresult.Price.TotalAmount = Math.Ceiling(apiresult.Price.TotalAmount);

                        apiresult.OutboundLeg = new Leg();
                        apiresult.OutboundLeg.Segments = new Segment[result.ObSegCount];
                        apiresult.InboundLeg = new Leg();
                        apiresult.InboundLeg.Segments = new Segment[result.IbSegCount];
                        int paxcount = 1;
                        if(request.ChildCount > 0)
                        {
                            paxcount++;
                        }
                        for (int i = 0; i < result.Segment.Length; i++)
                        {
                            Audit.Add(EventType.Search, Severity.Low, pref.MemberId, "(WEGO Search)ResultID: "+result.ResultId, "");
                            WSSegment segment = result.Segment[i];

                            if (segment.Group == 0)
                            {
                                apiresult.OutboundLeg.Segments[i] = new Segment();
                                apiresult.OutboundLeg.Segments[i].FlightClass = segment.FareCabinClass;
                                apiresult.OutboundLeg.Segments[i].OperatingCarrier = segment.Airline.AirlineCode;
                                apiresult.OutboundLeg.Segments[i].MarketingCarrier = segment.Airline.AirlineCode;
                                apiresult.OutboundLeg.Segments[i].FlightNumber = segment.FlightNumber;
                                apiresult.OutboundLeg.Segments[i].DepartureTime = segment.DepTIme.ToString("yyyy-MM-dd H:mm:ss tt");
                                apiresult.OutboundLeg.Segments[i].ArrivalTime = segment.ArrTime.ToString("yyyy-MM-dd H:mm:ss tt");
                                apiresult.OutboundLeg.Segments[i].Destination = segment.Destination.AirportCode;
                                apiresult.OutboundLeg.Segments[i].Origin = segment.Origin.AirportCode;
                                apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance = new BaggageAllowance[paxcount];
                                if (!string.IsNullOrEmpty(result.BaggageIncludedInFare))
                                {
                                    if (result.BaggageIncludedInFare.Contains(","))
                                    {
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0] = new BaggageAllowance();
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].PaxType = "Adult";
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit = result.BaggageIncludedInFare.Split(',')[segment.SegmentIndicator - 1];
                                        if (!string.IsNullOrEmpty(apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit) && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit.ToLower().Contains("piece") && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit.ToLower().Contains("pcs") && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit.ToLower().Contains("pc"))
                                        {
                                            apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit += " KG";
                                        }

                                        if (request.ChildCount > 0)
                                        {
                                            apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1] = new BaggageAllowance();
                                            apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].PaxType = "Child";
                                            apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit = result.BaggageIncludedInFare.Split(',')[segment.SegmentIndicator - 1];
                                            if (!string.IsNullOrEmpty(apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit) && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit.ToLower().Contains("piece") && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit.ToLower().Contains("pcs") && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit.ToLower().Contains("pc"))
                                            {
                                                apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit += " KG";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0] = new BaggageAllowance();
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].PaxType = "Adult";
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit = result.BaggageIncludedInFare;
                                        if (!string.IsNullOrEmpty(apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit) && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit.ToLower().Contains("piece") && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit.ToLower().Contains("pcs") && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit.ToLower().Contains("pc"))
                                        {
                                            apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit += " KG";
                                        }

                                        if (request.ChildCount > 0)
                                        {
                                            apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1] = new BaggageAllowance();
                                            apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].PaxType = "Child";
                                            apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit = result.BaggageIncludedInFare;
                                            if (!string.IsNullOrEmpty(apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit) && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit.ToLower().Contains("piece") && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit.ToLower().Contains("pcs") && !apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit.ToLower().Contains("pc"))
                                            {
                                                apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit += " KG";
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0] = new BaggageAllowance();
                                    apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].PaxType = string.Empty;
                                    apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[0].Unit = string.Empty;
                                    if (request.ChildCount > 0)
                                    {
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1] = new BaggageAllowance();
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].PaxType = string.Empty;
                                        apiresult.OutboundLeg.Segments[i].FreeBaggageAllowance[1].Unit = string.Empty;
                                    }
                                }
                            }
                            else
                            {
                                apiresult.InboundLeg.Segments[i - result.ObSegCount] = new Segment();
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].FlightClass = segment.FareCabinClass;
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].OperatingCarrier = segment.Airline.AirlineCode;
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].MarketingCarrier = segment.Airline.AirlineCode;
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].FlightNumber = segment.FlightNumber;
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].DepartureTime = segment.DepTIme.ToString("yyyy-MM-dd H:mm:ss tt");
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].ArrivalTime = segment.ArrTime.ToString("yyyy-MM-dd H:mm:ss tt");
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].Destination = segment.Destination.AirportCode;
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].Origin = segment.Origin.AirportCode;
                                apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance = new BaggageAllowance[paxcount];
                                if (!string.IsNullOrEmpty(result.BaggageIncludedInFare))
                                {
                                    if (result.BaggageIncludedInFare.Contains(","))
                                    {
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0] = new BaggageAllowance();
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].PaxType = "Adult";
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit = result.BaggageIncludedInFare.Split(',')[segment.SegmentIndicator - 1];
                                        if (!string.IsNullOrEmpty(apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit) && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit.ToLower().Contains("piece") && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit.ToLower().Contains("pcs") && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit.ToLower().Contains("pc"))
                                        {
                                            apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit += " KG";
                                        }

                                        if (request.ChildCount > 0)
                                        {
                                            apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1] = new BaggageAllowance();
                                            apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].PaxType = "Child";
                                            apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit = result.BaggageIncludedInFare.Split(',')[segment.SegmentIndicator - 1];
                                            if (!string.IsNullOrEmpty(apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit) && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit.ToLower().Contains("piece") && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit.ToLower().Contains("pcs") && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit.ToLower().Contains("pc"))
                                            {
                                                apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit += " KG";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0] = new BaggageAllowance();
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].PaxType = "Adult";
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit = result.BaggageIncludedInFare;
                                        if (!string.IsNullOrEmpty(apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit) && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit.ToLower().Contains("piece") && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit.ToLower().Contains("pcs") && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit.ToLower().Contains("pc"))
                                        {
                                            apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit += " KG";
                                        }

                                        if (request.ChildCount > 0)
                                        {
                                            apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1] = new BaggageAllowance();
                                            apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].PaxType = "Child";
                                            apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit = result.BaggageIncludedInFare;
                                            if (!string.IsNullOrEmpty(apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit) && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit.ToLower().Contains("piece") && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit.ToLower().Contains("pcs") && !apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit.ToLower().Contains("pc"))
                                            {
                                                apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit += " KG";
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0] = new BaggageAllowance();
                                    apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].PaxType = string.Empty;
                                    apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[0].Unit = string.Empty;
                                    if (request.ChildCount > 0)
                                    {
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1] = new BaggageAllowance();
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].PaxType = string.Empty;
                                        apiresult.InboundLeg.Segments[i - result.ObSegCount].FreeBaggageAllowance[1].Unit = string.Empty;
                                    }
                                }
                            }
                        }
                        //We are sending sitename in order to use it in b2c site for price and booking validation ex: SN=wego.com
                        //apiresult.DeepLinkURL = pref.Value + ConfigurationManager.AppSettings["DeepLinkURL"] + "?SRC=WEGO&PAR=B&RSV=" + response.SessionId + "&RID=" + result.ResultId + "&SN=" + Credential.SiteName;
                        apiresult.DeepLinkURL = pref.Value + ConfigurationManager.AppSettings["DeepLinkURL"] + "?SRC=WEGO&RSV=" + response.SessionId + "&RID=" + result.ResultId + "&SN=" + Credential.SiteName;
                        results.Add(apiresult);
                    }
                }

                apiResponse.FlightItineraries = results;

                /*************Start : LookToBookRatio Log******************/
                //After receiving the response we log into BKE_Booking_Ratio table.
                //Need to update against the same session id or insert new record


                if (response != null && !string.IsNullOrEmpty(response.SessionId))
                {
                    LogBookRatio(request, response.SessionId, results.Count, UserId, userAgentId, string.Empty, Credential.SiteName, false, 0, string.Empty, request.Sources);
                }

                /*************End : LookToBookRatio Log******************/


            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, UserId, "(" + Credential.SiteName + ") Search Failed to get results for WEGO. Reason : " + ex.ToString(), "", request.Sources);
                //Error Occurred but we will log into BKE_Booking_Ratio
                LogBookRatio(request, string.Empty, 0, UserId, userAgentId, ex.ToString(), Credential.SiteName, false, 0, string.Empty, request.Sources);
            }
            #endregion
        }

        try
        {
            //=================================== Write search response into xml file ======================================//
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(apiResponse.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, apiResponse);  // Here Classes are converted to XML String. 
            //This can be viewed in SB or writer.
            //Above XML in SB can be loaded in XmlDocument object
            string filepath = xmlPath + "SearchResponse_" + DateTime.Now.ToString("yyyyMMMdd_hhmmss") + ".xml";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
            doc.Save(filepath);
            Audit.Add(EventType.Search, Severity.Low, UserId, "(" + Credential.SiteName + ") Search Response" + filepath, "", request.Sources);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, UserId, "(" + Credential.SiteName + ") Failed to write search response xml. Reason : " + ex.ToString(), "", request.Sources);
        }

        return apiResponse;
    }

    /// <summary>
    /// Method which logs the wego search requests and bookings which is used to calculate the LookToBookRatio
    /// </summary>
    /// <param name="request"></param>
    /// <param name="sessionId"></param>
    void LogBookRatio(WSSearchRequest request, string sessionId, int resultCount, int userID, int agencyID, string exceptionMessage, string wegoSiteName, bool isBooked, int bookingRefID, string bookedSource, string agentSources)
    {
        try
        {
            //Logging into the BKE_Booking_Ratio table.
            //Need to update against the same session id or insert new record.

            string searchCriteria = string.Format("Origin ={0},Destination ={1},SearchType ={2},DepartureDate = {3},ReturnDate = {4},Adults ={5},Childs = {6},Infants ={7},CabinClass = {8},PreferredCarrier = {9},RefundableFares = {10},MaxStops = {11}", request.Origin, request.Destination, request.Type.ToString(), request.DepartureDate.ToString("dd-MM-yyyy"), request.ReturnDate.ToString("dd-MM-yyyy"), request.AdultCount, request.ChildCount, request.InfantCount, request.CabinClass, request.PreferredCarrier, request.RefundableFares, request.MaxStops);
            MetaSearchEngine mse = new MetaSearchEngine();
            /// <summary>
            /// //Logging into the BKE_Booking_Ratio table  for maintaing the LookToBookRatio Statistics
            /// </summary>
            /// <param name="transType">B2B or B2C</param>
            /// <param name="productType">Flight = 1,Hotel = 2,Packages = 3,Activity = 4,Insurance = 5, SightSeeing = 6,Train = 7,Transfers = 9,SMSPack = 8,MobileRecharge = 10,FixedDeparture = 11,Visa=12,Car=13</param>
            /// <param name="searchCriteria">Eg:Origin =DXB,Destination =DEL,SearchType =Return,DepartureDate = 26-04-2017,ReturnDate = 30-04-2017,Adults =1,Childs = 0,Infants =0,CabinClass = All,PreferredCarrier = ,RefundableFares = False,MaxStops = -1</param>
            /// <param name="sessionId">A unique session id for each search request which will be used till search to book.</param>
            /// <param name="resultCount">Total number of flight results returned for the respective search</param>
            /// <param name="userID">User who passed the search request,his id</param>
            /// <param name="agencyID">User who passed the search request ,his respective agency id</param>
            /// <param name="exceptionMessage">If there is any exception </param>
            /// <param name="siteName">Eg:Wego.com ,Wego.QA.com</param>
            /// <param name="isBooked">true or false</param>
            /// <param name="bookingRefID">Booking id</param>
            /// <param name="bookedSource">Flight result booking source Eg:'UAPI','TBOAir','AirArabia'</param>
            mse.SaveBookingRatio("B2C", ProductType.Flight, searchCriteria, sessionId, resultCount, userID, agencyID, exceptionMessage, wegoSiteName, isBooked, bookingRefID, bookedSource);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, userID, "(" + wegoSiteName + ") Failed to save the record in the BKE_Booking_Ratio table. Error : " + ex.ToString(), "", agentSources);
        }
    }


    /// <summary>
    /// Method used to Encrypt Deep Link URL for WEGO
    /// </summary>
    /// <param name="toEncrypt"></param>
    /// <returns></returns>
    string Encrypt(string toEncrypt)
    {
        byte[] keyArray;
        byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

        // Get the key 
        //string key = DateTime.Now.ToString("yyyy-MM-dd");
        string key =string.Empty;
        if(!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WegoSecurityKey"]))
            key = ConfigurationManager.AppSettings["WegoSecurityKey"];

        //If hashing use get hashcode regards to your key
        MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
        keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
        //Always release the resources and flush data
        // of the Cryptographic service provide. Best Practice

        hashmd5.Clear();

        TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //set the secret key for the tripleDES algorithm
        tdes.Key = keyArray;
        //mode of operation. there are other 4 modes.
        //We choose ECB(Electronic code Book)
        tdes.Mode = CipherMode.ECB;
        //padding mode(if any extra byte added)

        tdes.Padding = PaddingMode.PKCS7;

        ICryptoTransform cTransform = tdes.CreateEncryptor();
        //transform the specified region of bytes array to resultArray
        byte[] resultArray =
          cTransform.TransformFinalBlock(toEncryptArray, 0,
          toEncryptArray.Length);
        //Release resources held by TripleDes Encryptor
        tdes.Clear();
        //Return the encrypted data into unreadable string format
        return Convert.ToBase64String(resultArray, 0, resultArray.Length);
    }
    

    [SoapHeader("Credential")]
    [WebMethod]
    public WSResult GetAPIResult(string sessionId, int resultIndex, ref WSSearchRequest request, ref WSSearchResponse response)
    {
        WSResult result = null;
        int UserId = 0;
        try
        {
            if (WSValidate(Credential) != null && Basket.FlightBookingSession != null)
            {
                UserPreference pref = new UserPreference();
                UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
                LoginInfo loginInfo = UserMaster.GetB2CUser(UserId);
                SearchResult searchResult = Basket.FlightBookingSession[sessionId].Result[resultIndex];
                SearchResult[] Results = Basket.FlightBookingSession[sessionId].Result;
                List<WSResult> ResultList = new List<WSResult>();

                WSSearch search = new WSSearch();
                search.AgentLoginInfo = loginInfo;                

                request.AdultCount = searchResult.FareBreakdown[0].PassengerCount;
                if (searchResult.FareBreakdown.Length > 1)
                {
                    if (searchResult.FareBreakdown[1].PassengerType == PassengerType.Child)
                    {
                        request.ChildCount = searchResult.FareBreakdown[1].PassengerCount;
                    }
                    else
                    {
                        request.InfantCount = searchResult.FareBreakdown[1].PassengerCount;
                    }
                }
                if (searchResult.FareBreakdown.Length > 2)
                {
                    request.InfantCount = searchResult.FareBreakdown[2].PassengerCount;
                }
                request.Type = (searchResult.Flights.Length > 1 ? SearchType.Return : SearchType.OneWay);
                request.Destination = searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.AirportCode;
                request.Origin = searchResult.Flights[0][0].Origin.AirportCode;
                request.DepartureDate = searchResult.Flights[0][0].DepartureTime;
                if (searchResult.Flights.Length > 1)
                {
                    request.ReturnDate = searchResult.Flights[1][0].DepartureTime;
                }
                else
                {
                    request.ReturnDate = searchResult.Flights[0][0].DepartureTime;
                }

                try
                {
                    search.BuildResultList(ref ResultList, Results, request.Origin, request.Destination, 1, loginInfo.AgentId, (int)loginInfo.UserID, Credential);

                    response = new WSSearchResponse
                    {
                        Result = ResultList.ToArray(),
                        SessionId = sessionId
                    };
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)loginInfo.UserID, "Failed to Build WSResult list for WEGO. Reason: " + ex.ToString(), "");
                }

                result = new WSResult();
                result.ResultId = searchResult.ResultId;
                result.Source = searchResult.ResultBookingSource;
                result.Destination = searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.AirportCode;
                result.Fare = new WSFare();
                result.FareBreakdown = new WSPTCFare[searchResult.FareBreakdown.Length];
                for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                {
                    searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, j, loginInfo.AgentId, 0, 1, "B2C");
                    result.Fare.BaseFare += searchResult.Price.PublishedFare * searchResult.FareBreakdown[j].PassengerCount;
                    result.Fare.Tax += searchResult.Price.Tax * searchResult.FareBreakdown[j].PassengerCount;
                    result.Fare.Discount = searchResult.Price.WhiteLabelDiscount;
                    result.Fare.Currency = searchResult.Price.Currency;
                    result.Fare.Markup += searchResult.Price.Markup;
                    result.Fare.MarkupType = searchResult.Price.MarkupType;
                    result.Fare.MarkupValue = searchResult.Price.MarkupValue;
                    result.Fare.B2CMarkup += searchResult.Price.B2CMarkup;
                    result.Fare.B2CMarkupType = searchResult.Price.B2CMarkupType;
                    result.Fare.B2CMarkupValue = searchResult.Price.B2CMarkupValue;
                    result.Fare.SupplierCurrency = searchResult.Price.SupplierCurrency;
                    result.Fare.SupplierPrice = searchResult.Price.SupplierPrice;
                    result.Fare.AdditionalTxnFee = searchResult.Price.AdditionalTxnFee;
                    result.Fare.AirTransFee = searchResult.Price.AirlineTransFee;
                    result.Fare.OtherCharges = searchResult.Price.OtherCharges;
                    result.Fare.SServiceFee = searchResult.Price.SServiceFee;
                    result.Fare.TransactionFee = searchResult.Price.TransactionFee;
                    searchResult.FareBreakdown[j].B2CMarkup = searchResult.Price.B2CMarkup;
                    searchResult.FareBreakdown[j].AgentMarkup = searchResult.Price.Markup;

                    result.FareBreakdown[j] = new WSPTCFare
                    {
                        PassengerType = searchResult.FareBreakdown[j].PassengerType,
                        PassengerCount = searchResult.FareBreakdown[j].PassengerCount,
                        B2CMarkup = searchResult.FareBreakdown[j].B2CMarkup,
                        AdditionalTxnFee = searchResult.FareBreakdown[j].AdditionalTxnFee,
                        AirlineTransFee = searchResult.FareBreakdown[j].AirlineTransFee,
                        Markup = searchResult.FareBreakdown[j].AgentMarkup,
                        SupplierPrice = (decimal)searchResult.FareBreakdown[j].SupplierFare / searchResult.FareBreakdown[j].PassengerCount,
                        BaseFare = (decimal)searchResult.FareBreakdown[j].BaseFare,
                        Tax = (decimal)(searchResult.FareBreakdown[j].TotalFare - searchResult.FareBreakdown[j].BaseFare),
                        SupplierCurrency = searchResult.Price.SupplierCurrency
                    };
                }
                FlightInfo[] mSegment = SearchResult.GetSegments(searchResult);
                result.Segment = new WSSegment[mSegment.Length];
                for (int j = 0; j < mSegment.Length; j++)
                {
                    result.Segment[j] = WSSegment.ReadSegment(mSegment[j]);
                }

                TimeSpan Duration = new TimeSpan();
                TimeSpan ibDuration = new TimeSpan();
                for (int j = 0, l = 0; j < searchResult.Flights.Length; j++)
                {
                    for (int k = 0; k < searchResult.Flights[j].Length; k++, l++)
                    {
                        result.Segment[l] = WSSegment.ReadSegment(searchResult.Flights[j][k]);
                        result.Segment[l].SegmentIndicator = j + 1;

                        //Correcting Duration calculation as per local time zones. Done by Shiva 12 June 2018
                        //Adding durations to get total duration since we are already calculating according to local time zones in MSE
                        if (j == 0)
                        {
                            result.ObSegCount++;
                            Duration = Duration.Add(searchResult.Flights[j][k].Duration);
                            result.ObDuration = (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                            //result.ObDuration = (Duration.Days * 24 + Duration.Hours).ToString("00:") + Duration.Minutes.ToString("00");
                        }
                        else if (j == 1)
                        {
                            result.IbSegCount++;
                            ibDuration = ibDuration.Add(searchResult.Flights[j][k].Duration);
                            result.IbDuration = (ibDuration.Hours.ToString().Length > 1 ? ibDuration.Hours.ToString() : "0" + ibDuration.Hours) + ":" + (ibDuration.Minutes.ToString().Length > 1 ? ibDuration.Minutes.ToString() : "0" + ibDuration.Minutes);
                            //result.IbDuration = (ibDuration.Days * 24 + ibDuration.Hours).ToString("00:") + ibDuration.Minutes.ToString("00");
                        }
                    }
                }
                //if (result.ObSegCount > 0)
                //{
                //    TimeSpan tOut = mSegment[result.ObSegCount - 1].AccumulatedDuration;
                //    result.ObDuration = (tOut.Days * 24 + tOut.Hours).ToString("00:") + tOut.Minutes.ToString("00");
                //    //TODO: 
                //}
                //if (result.IbSegCount > 0)
                //{
                //    TimeSpan tIn = mSegment[result.IbSegCount + result.ObSegCount - 1].AccumulatedDuration;
                //    result.IbDuration = (tIn.Days * 24 + tIn.Hours).ToString("00:") + tIn.Minutes.ToString("00");
                //}

                result.IsLcc = searchResult.IsLCC;
                result.IsRefundable = !searchResult.NonRefundable;
                result.Origin = searchResult.Flights[0][0].Origin.AirportCode;
                result.TripIndicator = searchResult.Flights.Length;

                if (searchResult.ResultBookingSource == BookingSource.UAPI)
                {
                    result.UapiPricingSolution = new UAPIdll.Air46.AirPricingSolution(); //searchResult.UapiPricingSolution;
                    PropertiesCopier.CopyProperties(searchResult.UapiPricingSolution, result.UapiPricingSolution);
                    PropertiesCopier.CheckRecursiveDifferentTypes(searchResult.UapiPricingSolution, result.UapiPricingSolution);
                }

                int counter = 0;
                if (searchResult.FareRules != null)
                {
                    result.FareBasis = new string[searchResult.FareRules.Count];
                    counter = 0;

                    for (int j = 0; j < searchResult.Flights.Length; j++)
                    {
                        for (int k = 0; k < searchResult.Flights[j].Length; k++)
                        {
                            if (counter < searchResult.FareRules.Count)
                            {
                                if (searchResult.FareRules[counter].FareInfoRef != null && searchResult.FareRules[counter].FareInfoRef.Length > 0)
                                {
                                    result.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode + "~" + Convert.ToString(searchResult.FareRules[counter].FareInfoRef) + "~" + searchResult.FareRules[counter].FareRuleKeyValue;
                                }
                                else
                                {
                                    result.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode;
                                }
                                counter = counter + 1;
                            }
                        }
                    }
                }

                try
                {
                    //Required to know Markup and ROE values
                    string xmlPath = ConfigurationManager.AppSettings["WEGOXmlLogs"];
                    //=================================== Write search response into xml file ======================================//
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(result.GetType());
                    string filepath = xmlPath + "WEGORetrievedResult_" + DateTime.Now.ToString("yyyyMMMdd_hhmmss") + ".xml";
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(filepath);
                    ser.Serialize(writer, result);  // Here Classes are converted to XML String. 
                    //This can be viewed in SB or writer.
                    //Above XML in SB can be loaded in XmlDocument object
                    writer.Close();
                }
                catch (Exception ex)
                {
                    Audit.AddAuditBookingAPI(EventType.Search, Severity.High, UserId, "(WEGO)Failed to write API Result. Reason : " + ex.ToString(), "");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, UserId, "(" + Credential.SiteName + ")Error in GetAPIResult method . Reason : " + ex.ToString(), "", request.Sources);
        }
        return result;
    }

    [SoapHeader("Credential")]
    [WebMethod]
    public WSResult RePriceItinerary(int resultId, ref string BookingValuesChanged, string sessionId, WSSearchRequest request)
    {
        WSResult WsResult = new WSResult();
        string errorMsg = string.Empty;
        int userId = 0;
        try
        {
            SearchResult searchResult = Basket.FlightBookingSession[sessionId].Result[resultId];
            UserPreference pref = new UserPreference();
            userId = pref.GetMemberIdBySiteName(Credential.SiteName);
            LoginInfo loginInfo = UserMaster.GetB2CUser(userId);

            #region UAPI Repricing
            if (searchResult != null && searchResult.ResultBookingSource == BookingSource.UAPI)
            {
                /********************************Wego Audit ******************************/

                Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Executing RePriceItinerary method", "", "UAPI");

                /***************************************End:Wego Audit ******************/

                SearchType searchType = SearchType.Return;

                if (searchResult.Flights.Length == 1)
                {
                    searchType = SearchType.OneWay;
                }
                else if (searchResult.Flights.Length == 2)
                {
                    searchType = SearchType.Return;
                }
                else
                {
                    searchType = SearchType.MultiWay;
                }
                Fare[] fares = searchResult.FareBreakdown;

                MetaSearchEngine mse = new MetaSearchEngine(sessionId);
                mse.SettingsLoginInfo = loginInfo;
                /********************************Wego Audit ******************************/

                Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Call To MSE - RePriceUAPI method", "", "UAPI");

                /***************************************End:Wego Audit ******************/


                mse.RePriceUAPI(searchResult, searchType, ref fares, ref BookingValuesChanged);

                PriceAccounts price = searchResult.Price;
                decimal inputVAT = 0m;
                mse.CalculateInputVATForFlight(searchResult.Flights[0][0].Origin.CityCode, searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.CityCode, searchResult.ResultBookingSource, ref price, (decimal)searchResult.TotalFare, ref inputVAT);
                searchResult.TotalFare += (double)inputVAT;
                searchResult.Tax += (double)inputVAT;

                //for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                //{
                //    searchResult.FareBreakdown[j] = fares[j];
                //}

                WsResult.ResultId = resultId;
                WsResult.Source = BookingSource.UAPI;
                WsResult.Fare = new WSFare();
                WsResult.Fare.TaxDetails = searchResult.Price.TaxDetails;
                WsResult.FareBreakdown = new WSPTCFare[searchResult.FareBreakdown.Length];
                for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                {
                    searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, j, loginInfo.AgentId, 0, 1, "B2C");
                    WsResult.Fare.BaseFare += ((searchResult.Price.PublishedFare * searchResult.FareBreakdown[j].PassengerCount)+ (searchResult.FareBreakdown[j].HandlingFee));
                    WsResult.Fare.Tax += searchResult.Price.Tax * searchResult.FareBreakdown[j].PassengerCount;
                    if (j == 0)
                    {
                        WsResult.Fare.Tax += inputVAT;
                    }
                    WsResult.Fare.Discount += (searchResult.Price.Discount + searchResult.Price.WhiteLabelDiscount) * searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.Fare.Currency = searchResult.Price.Currency;
                    WsResult.Fare.Markup += (searchResult.Price.Markup * searchResult.FareBreakdown[j].PassengerCount);
                    WsResult.Fare.MarkupType = searchResult.Price.MarkupType;
                    WsResult.Fare.MarkupValue = searchResult.Price.MarkupValue;
                    WsResult.Fare.B2CMarkup += (searchResult.Price.B2CMarkup * searchResult.FareBreakdown[j].PassengerCount);
                    WsResult.Fare.B2CMarkupType = searchResult.Price.B2CMarkupType;
                    WsResult.Fare.B2CMarkupValue = searchResult.Price.B2CMarkupValue;
                    WsResult.Fare.SupplierCurrency = searchResult.Price.SupplierCurrency;
                    WsResult.Fare.SupplierPrice = searchResult.Price.SupplierPrice;

                    WsResult.FareBreakdown[j] = new WSPTCFare();
                    WsResult.FareBreakdown[j].PassengerType = searchResult.FareBreakdown[j].PassengerType;
                    WsResult.FareBreakdown[j].PassengerCount = searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.FareBreakdown[j].B2CMarkup = searchResult.Price.B2CMarkup;//Assign new markup
                    WsResult.FareBreakdown[j].Markup = searchResult.Price.Markup;//Assign new markup
                    WsResult.FareBreakdown[j].SupplierPrice = (decimal)searchResult.FareBreakdown[j].SupplierFare / searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.FareBreakdown[j].BaseFare = (decimal)searchResult.FareBreakdown[j].BaseFare + (searchResult.FareBreakdown[j].HandlingFee / searchResult.FareBreakdown[j].PassengerCount);
                    WsResult.FareBreakdown[j].Tax = (decimal)(searchResult.FareBreakdown[j].TotalFare - searchResult.FareBreakdown[j].BaseFare);
                    WsResult.FareBreakdown[j].SupplierCurrency = searchResult.Price.SupplierCurrency;
                }

                if (price.TaxDetails != null && price.TaxDetails.OutputVAT != null)
                {
                    WsResult.Fare.OutputVATApplied = price.TaxDetails.OutputVAT.Applied;
                    WsResult.Fare.OutputVATCharge = price.TaxDetails.OutputVAT.Charge;
                    WsResult.Fare.OutputVATOn = price.TaxDetails.OutputVAT.AppliedOn.ToString();
                }

                WsResult.UapiPricingSolution = new UAPIdll.Air46.AirPricingSolution(); //searchResult.UapiPricingSolution;
                PropertiesCopier.CopyProperties(searchResult.UapiPricingSolution, WsResult.UapiPricingSolution);
                PropertiesCopier.CheckRecursiveDifferentTypes(searchResult.UapiPricingSolution, WsResult.UapiPricingSolution);

                #region Building segments of the WsResult

                FlightInfo[] mSegment = SearchResult.GetSegments(searchResult);
                WsResult.Segment = new WSSegment[mSegment.Length];
                for (int j = 0; j < mSegment.Length; j++)
                {
                    WsResult.Segment[j] = WSSegment.ReadSegment(mSegment[j]);
                }
                WsResult.Origin = searchResult.Flights[0][0].Origin.AirportCode;
                TimeSpan Duration = new TimeSpan();
                TimeSpan ibDuration = new TimeSpan();
                for (int j = 0, l = 0; j < searchResult.Flights.Length; j++)
                {
                    for (int k = 0; k < searchResult.Flights[j].Length; k++, l++)
                    {
                        WsResult.Segment[l] = WSSegment.ReadSegment(searchResult.Flights[j][k]);
                        WsResult.Segment[l].SegmentIndicator = j + 1;

                        //Correcting Duration calculation as per local time zones. Done by Shiva 12 June 2018
                        //Adding durations to get total duration since we are already calculating according to local time zones in MSE
                        if (j == 0)
                        {
                            WsResult.ObSegCount++;
                            Duration = Duration.Add(searchResult.Flights[j][k].Duration);
                            WsResult.ObDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                            //result.ObDuration = (Duration.Days * 24 + Duration.Hours).ToString("00:") + Duration.Minutes.ToString("00");
                        }
                        else if (j == 1)
                        {
                            WsResult.IbSegCount++;
                            Duration = ibDuration.Add(searchResult.Flights[j][k].Duration);
                            WsResult.IbDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                            //result.IbDuration = (ibDuration.Days * 24 + ibDuration.Hours).ToString("00:") + ibDuration.Minutes.ToString("00");
                        }
                    }
                }

                //if (WsResult.ObSegCount > 0)
                //{
                //    TimeSpan tOut = mSegment[WsResult.ObSegCount - 1].AccumulatedDuration;
                //    WsResult.ObDuration = (tOut.Days * 24 + tOut.Hours).ToString("00:") + tOut.Minutes.ToString("00");
                //    //TODO: 
                //}
                //if (WsResult.IbSegCount > 0)
                //{
                //    TimeSpan tIn = mSegment[WsResult.IbSegCount + WsResult.ObSegCount - 1].AccumulatedDuration;
                //    WsResult.IbDuration = (tIn.Days * 24 + tIn.Hours).ToString("00:") + tIn.Minutes.ToString("00");
                //}
                #endregion
                int counter = 0;
                if (searchResult.FareRules != null)
                {
                    WsResult.FareBasis = new string[searchResult.FareRules.Count];
                    counter = 0;

                    for (int j = 0; j < searchResult.Flights.Length; j++)
                    {
                        for (int k = 0; k < searchResult.Flights[j].Length; k++)
                        {
                            if (counter < searchResult.FareRules.Count)
                            {
                                if (searchResult.FareRules[counter].FareInfoRef != null && searchResult.FareRules[counter].FareInfoRef.Length > 0)
                                {
                                    WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode + "~" + Convert.ToString(searchResult.FareRules[counter].FareInfoRef) + "~" + searchResult.FareRules[counter].FareRuleKeyValue;
                                }
                                else
                                {
                                    WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode;
                                }
                                counter = counter + 1;
                            }
                        }
                    }
                }
            }
            #endregion
            #region AirArabia Repricing
            else if (searchResult != null && searchResult.ResultBookingSource == BookingSource.AirArabia)
            {
                /********************************Wego Audit ******************************/

                Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Executing RePriceItinerary method", "", searchResult.ResultBookingSource.ToString());

                /***************************************End:Wego Audit ******************/

                SearchRequest mRequest = GenerateSearchRequest(request, searchResult);

                Fare[] fares = searchResult.FareBreakdown;

                MetaSearchEngine mse = new MetaSearchEngine(sessionId);
                mse.SettingsLoginInfo = loginInfo;
                /********************************Wego Audit ******************************/

                Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Call To MSE - RepriceAirArabia method", "", "AirArabia");

                /***************************************End:Wego Audit ******************/

                searchResult = mse.RepriceAirArabia(searchResult.ResultId, mRequest, "B2C");

                //PriceAccounts price = searchResult.Price;
                //decimal inputVAT = 0m;
                //mse.CalculateInputVATForFlight(searchResult.Flights[0][0].Origin.CityCode, searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.CityCode, searchResult.ResultBookingSource, ref price, (decimal)searchResult.TotalFare, ref inputVAT);
                //searchResult.TotalFare += (double)inputVAT;
                //searchResult.Tax += (double)inputVAT;

                //for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                //{
                //    searchResult.FareBreakdown[j] = fares[j];
                //}
                WsResult.ResultId = resultId;
                WsResult.Source = BookingSource.AirArabia;
                WsResult.IsLcc = true;
                WsResult.Fare = new WSFare();
                WsResult.Fare.TaxDetails = searchResult.Price.TaxDetails;
                WsResult.FareBreakdown = new WSPTCFare[searchResult.FareBreakdown.Length];
                for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                {
                    //searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, j, loginInfo.AgentId, 0, 1, "B2C");
                    WsResult.Fare.BaseFare += ((decimal)searchResult.FareBreakdown[j].BaseFare) + (searchResult.FareBreakdown[j].HandlingFee);
                    WsResult.Fare.Tax += searchResult.FareBreakdown[j].Tax;
                    
                    WsResult.Fare.Discount += searchResult.FareBreakdown[j].AgentDiscount;
                    WsResult.Fare.Currency = searchResult.Price.Currency;
                    WsResult.Fare.Markup += searchResult.FareBreakdown[j].AgentMarkup;
                    WsResult.Fare.MarkupType = searchResult.Price.MarkupType;
                    WsResult.Fare.MarkupValue = searchResult.Price.MarkupValue;
                    WsResult.Fare.B2CMarkup += searchResult.FareBreakdown[j].B2CMarkup;
                    WsResult.Fare.B2CMarkupType = searchResult.Price.B2CMarkupType;
                    WsResult.Fare.B2CMarkupValue = searchResult.Price.B2CMarkupValue;
                    WsResult.Fare.SupplierCurrency = searchResult.Price.SupplierCurrency;
                    WsResult.Fare.SupplierPrice += (decimal)searchResult.FareBreakdown[j].SupplierFare;

                    WsResult.FareBreakdown[j] = new WSPTCFare();
                    WsResult.FareBreakdown[j].PassengerType = searchResult.FareBreakdown[j].PassengerType;
                    WsResult.FareBreakdown[j].PassengerCount = searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.FareBreakdown[j].B2CMarkup = searchResult.FareBreakdown[j].B2CMarkup / searchResult.FareBreakdown[j].PassengerCount;//Assign new markup
                    WsResult.FareBreakdown[j].Markup = searchResult.FareBreakdown[j].AgentMarkup / searchResult.FareBreakdown[j].PassengerCount;//Assign new markup
                    WsResult.FareBreakdown[j].SupplierPrice = (decimal)searchResult.FareBreakdown[j].SupplierFare / searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.FareBreakdown[j].BaseFare = (decimal)searchResult.FareBreakdown[j].BaseFare + (searchResult.FareBreakdown[j].HandlingFee / searchResult.FareBreakdown[j].PassengerCount);
                    WsResult.FareBreakdown[j].Tax = (decimal)(searchResult.FareBreakdown[j].TotalFare - searchResult.FareBreakdown[j].BaseFare);
                    WsResult.FareBreakdown[j].SupplierCurrency = searchResult.Price.SupplierCurrency;
                }

                if (searchResult.Price.TaxDetails != null && searchResult.Price.TaxDetails.OutputVAT != null)
                {
                    WsResult.Fare.OutputVATApplied = searchResult.Price.TaxDetails.OutputVAT.Applied;
                    WsResult.Fare.OutputVATCharge = searchResult.Price.TaxDetails.OutputVAT.Charge;
                    WsResult.Fare.OutputVATOn = searchResult.Price.TaxDetails.OutputVAT.AppliedOn.ToString();
                }


                #region Building segments of the WsResult

                FlightInfo[] mSegment = SearchResult.GetSegments(searchResult);
                WsResult.Segment = new WSSegment[mSegment.Length];
                for (int j = 0; j < mSegment.Length; j++)
                {
                    WsResult.Segment[j] = WSSegment.ReadSegment(mSegment[j]);
                }
                WsResult.Origin = searchResult.Flights[0][0].Origin.AirportCode;

                TimeSpan Duration = new TimeSpan();
                TimeSpan ibDuration = new TimeSpan();
                for (int j = 0, l = 0; j < searchResult.Flights.Length; j++)
                {
                    for (int k = 0; k < searchResult.Flights[j].Length; k++, l++)
                    {
                        WsResult.Segment[l] = WSSegment.ReadSegment(searchResult.Flights[j][k]);
                        WsResult.Segment[l].SegmentIndicator = j + 1;
                        WsResult.Destination = searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.AirportCode;

                        //Correcting Duration calculation as per local time zones. Done by Shiva 12 June 2018
                        //Adding durations to get total duration since we are already calculating according to local time zones in MSE
                        if (j == 0)
                        {
                            WsResult.ObSegCount++;
                            Duration = Duration.Add(searchResult.Flights[j][k].Duration);
                            WsResult.ObDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                            //result.ObDuration = (Duration.Days * 24 + Duration.Hours).ToString("00:") + Duration.Minutes.ToString("00");
                        }
                        else if (j == 1)
                        {
                            WsResult.IbSegCount++;
                            Duration = ibDuration.Add(searchResult.Flights[j][k].Duration);
                            WsResult.IbDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                            //result.IbDuration = (ibDuration.Days * 24 + ibDuration.Hours).ToString("00:") + ibDuration.Minutes.ToString("00");
                        }
                    }

                }

                //if (WsResult.ObSegCount > 0)
                //{
                //    TimeSpan tOut = mSegment[WsResult.ObSegCount - 1].AccumulatedDuration;
                //    WsResult.ObDuration = (tOut.Days * 24 + tOut.Hours).ToString("00:") + tOut.Minutes.ToString("00");
                //    //TODO: 
                //}
                //if (WsResult.IbSegCount > 0)
                //{
                //    TimeSpan tIn = mSegment[WsResult.IbSegCount + WsResult.ObSegCount - 1].AccumulatedDuration;
                //    WsResult.IbDuration = (tIn.Days * 24 + tIn.Hours).ToString("00:") + tIn.Minutes.ToString("00");
                //}
                #endregion
                int counter = 0;
                if (searchResult.FareRules != null)
                {
                    WsResult.FareBasis = new string[searchResult.FareRules.Count];
                    counter = 0;

                    for (int j = 0; j < searchResult.Flights.Length; j++)
                    {
                        for (int k = 0; k < searchResult.Flights[j].Length; k++)
                        {
                            if (counter < searchResult.FareRules.Count)
                            {
                                if (searchResult.FareRules[counter].FareInfoRef != null && searchResult.FareRules[counter].FareInfoRef.Length > 0)
                                {
                                    WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode + "~" + Convert.ToString(searchResult.FareRules[counter].FareInfoRef) + "~" + searchResult.FareRules[counter].FareRuleKeyValue;
                                }
                                else
                                {
                                    WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode;
                                }
                                counter = counter + 1;
                            }
                        }
                    }
                }
            } 
            #endregion
            else if(searchResult.ResultBookingSource == BookingSource.TBOAir)
            {
                TBOAir.AirV10 tbo = new TBOAir.AirV10();

                SourceDetails agentDetails = new SourceDetails();
                                
                agentDetails = loginInfo.AgentSourceCredentials["TA"];
                tbo.AgentBaseCurrency = loginInfo.Currency;
                tbo.ExchangeRates = loginInfo.AgentExchangeRates;

                string errorMessage = string.Empty;

                SearchResult res = searchResult;

                tbo.LoginName = agentDetails.UserID;
                tbo.Password = agentDetails.Password;
                tbo.SessionId = sessionId;
                tbo.AppUserId = (int)loginInfo.UserID;

                
                Fare[] fares = tbo.GetFareQuote(ref res, ref errorMessage);
                if (fares.Length >0)
                 res.FareBreakdown =  fares;
                //if(fares.Length == 0)
                //{
                //    fares = searchResult.FareBreakdown;
                //}

                AgentMaster agency = new AgentMaster(loginInfo.AgentId);
                SearchRequest mRequest = GenerateSearchRequest(request, searchResult);
                int totalpax = request.AdultCount + request.ChildCount + request.InfantCount;

                decimal totalFare = 0m, tax = 0m;
                foreach (Fare fare in fares)
                {
                    totalFare += (decimal)fare.TotalFare;
                }
                
                PriceAccounts price = searchResult.Price;
                #region Old Markup Code

                //CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(sessionId);
                //mse.SettingsLoginInfo = loginInfo;
                //mse.CalculateInputVATForFlight(mRequest.Segments[0].Origin, mRequest.Segments[0].Destination, searchResult.ResultBookingSource, ref price, totalFare, ref tax);

                //res.TotalFare += (double)tax;
                //res.Tax += (double)tax;



                //if (fares.Length > 0)
                //{
                //    foreach (Fare fare in fares)
                //    {
                //        fare.TotalFare += Math.Round((double)(tax / totalpax) * fare.PassengerCount, loginInfo.DecimalValue);
                //    }


                //    List<Fare> NewFares = new List<Fare>(fares);

                //    searchResult.TotalFare = 0;
                //    searchResult.BaseFare = 0;
                //    searchResult.Tax = 0;

                //    foreach (Fare pax in searchResult.FareBreakdown)
                //    {
                //        foreach (Fare fare in fares)
                //        {
                //            if (fare.PassengerType == pax.PassengerType)
                //            {
                //                Fare obj = NewFares.Find(delegate (Fare f) { return f.PassengerType == fare.PassengerType; });

                //                searchResult.BaseFare += fare.BaseFare;
                //                searchResult.Tax += (double)fare.Tax;
                //                searchResult.TotalFare = (searchResult.BaseFare + searchResult.Tax);

                //                if (searchResult.Price.MarkupType == "P")
                //                {
                //                    if (agency.AgentAirMarkupType == "TF")
                //                    {
                //                        decimal total = ((decimal)fare.BaseFare + fare.Tax + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / totalpax) * fare.PassengerCount);
                //                        searchResult.Price.Markup = (total * searchResult.Price.MarkupValue / 100);
                //                        obj.AgentMarkup = searchResult.Price.Markup / obj.PassengerCount;
                //                    }
                //                    else if (agency.AgentAirMarkupType == "BF")
                //                    {
                //                        searchResult.Price.Markup = ((decimal)fare.BaseFare * searchResult.Price.MarkupValue / 100);
                //                        obj.AgentMarkup = searchResult.Price.Markup / obj.PassengerCount;
                //                    }
                //                    else if (agency.AgentAirMarkupType == "TX")
                //                    {
                //                        tax = ((decimal)(fare.Tax + (((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / totalpax) * fare.PassengerCount)));
                //                        searchResult.Price.Markup = (tax * searchResult.Price.MarkupValue / 100);
                //                        obj.AgentMarkup = searchResult.Price.Markup / obj.PassengerCount;
                //                    }
                //                }
                //                else
                //                {
                //                    obj.AgentMarkup = searchResult.Price.MarkupValue;
                //                }
                //                if (searchResult.Price.B2CMarkupType == "P")
                //                {
                //                    if (agency.AgentAirMarkupType == "TF")
                //                    {
                //                        decimal total = ((decimal)fare.BaseFare + fare.Tax + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / totalpax) * fare.PassengerCount);
                //                        obj.B2CMarkup = ((total + obj.AgentMarkup) * searchResult.Price.B2CMarkupValue / 100) / obj.PassengerCount;
                //                    }
                //                    else if (agency.AgentAirMarkupType == "BF")
                //                    {
                //                        obj.B2CMarkup = (((Convert.ToDecimal(fare.BaseFare / fare.PassengerCount) + obj.AgentMarkup - price.Discount)) * searchResult.Price.B2CMarkupValue / 100) / obj.PassengerCount;
                //                    }
                //                    else if (agency.AgentAirMarkupType == "TX")
                //                    {
                //                        tax = ((Convert.ToDecimal(fare.Tax + ((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) / fare.PassengerCount) + price.Markup - price.Discount)));
                //                        obj.B2CMarkup = (tax + obj.AgentMarkup * searchResult.Price.B2CMarkupValue / 100) / obj.PassengerCount;
                //                    }
                //                }
                //                else
                //                {
                //                    obj.B2CMarkup = searchResult.Price.B2CMarkupValue;
                //                }

                //                //Calculate HandlingFee only if Login user is from India
                //                if (!string.IsNullOrEmpty(searchResult.LoginCountryCode) && searchResult.LoginCountryCode == "IN" && !string.IsNullOrEmpty(searchResult.Price.HandlingFeeType))
                //                {
                //                    double handlingFee = 0;
                //                    if (searchResult.Price.HandlingFeeType == "P")
                //                    {
                //                        if (obj.PassengerType == PassengerType.Adult && searchResult.ResultBookingSource == BookingSource.TBOAir)
                //                        {
                //                            handlingFee = (obj.TotalFare + (double)((searchResult.Price.AdditionalTxnFee + searchResult.Price.OtherCharges + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee) + obj.AgentMarkup)) * (double)searchResult.Price.HandlingFeeValue / 100;
                //                        }
                //                        else
                //                        {
                //                            handlingFee = (obj.TotalFare + (double)(obj.AgentMarkup)) * (double)searchResult.Price.HandlingFeeValue / 100;
                //                        }
                //                        obj.HandlingFee = (decimal)handlingFee;
                //                    }
                //                    else
                //                    {
                //                        obj.HandlingFee = searchResult.Price.HandlingFeeValue * fare.PassengerCount;
                //                    }
                //                    searchResult.BaseFare += (double)obj.HandlingFee;
                //                }

                //                break;
                //            }
                //        }
                //    }


                //    //searchResult.TotalFare += (double)searchResult.Price.OtherCharges;
                //    searchResult.FareBreakdown = fares;
                //} 
                #endregion
                WsResult.GUID = searchResult.GUID;//Assigning the ResultIndex ex: OB2 for booking                
                WsResult.IsGSTMandatory = searchResult.IsGSTMandatory;
                WsResult.ResultId = resultId;//Assigning the original result index parameter
                WsResult.Source = BookingSource.TBOAir;
                WsResult.IsLcc = searchResult.IsLCC;
                WsResult.Fare = new WSFare();
                WsResult.Fare.TaxDetails = searchResult.Price.TaxDetails;
                WsResult.BaggageIncludedInFare = searchResult.BaggageIncludedInFare;
                WsResult.FareBreakdown = new WSPTCFare[searchResult.FareBreakdown.Length];
                for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                {
                    if (fares.Length > 0)
                        searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, j, (int)agency.ID, 0, 1, "B2C");

                    WsResult.Fare.BaseFare += (decimal)(searchResult.FareBreakdown[j].BaseFare) + searchResult.FareBreakdown[j].HandlingFee;
                    WsResult.Fare.Tax += searchResult.FareBreakdown[j].Tax;
                    if (searchResult.Price.TaxDetails != null && searchResult.Price.TaxDetails.InputVAT != null)
                    {
                        WsResult.Fare.InputVATApplied = searchResult.Price.TaxDetails.InputVAT.CostIncluded;
                        if (j == 0 && !searchResult.Price.TaxDetails.InputVAT.CostIncluded)
                        {                            
                            WsResult.Fare.Tax += searchResult.Price.InputVATAmount;
                        }
                    }
                    WsResult.Fare.Discount += (fares.Length > 0 ? (searchResult.Price.Discount+searchResult.Price.WhiteLabelDiscount) * searchResult.FareBreakdown[j].PassengerCount : searchResult.FareBreakdown[j].AgentDiscount );
                    WsResult.Fare.Currency = searchResult.Price.Currency;
                    WsResult.Fare.Markup += (fares.Length > 0 ? searchResult.Price.Markup * searchResult.FareBreakdown[j].PassengerCount: searchResult.FareBreakdown[j].AgentMarkup );
                    WsResult.Fare.MarkupType = searchResult.Price.MarkupType;
                    WsResult.Fare.MarkupValue = searchResult.Price.MarkupValue;
                    WsResult.Fare.B2CMarkup += (fares.Length > 0 ? searchResult.Price.B2CMarkup * searchResult.FareBreakdown[j].PassengerCount : searchResult.FareBreakdown[j].B2CMarkup);
                    WsResult.Fare.B2CMarkupType = searchResult.Price.B2CMarkupType;
                    WsResult.Fare.B2CMarkupValue = searchResult.Price.B2CMarkupValue;
                    WsResult.Fare.SupplierCurrency = searchResult.Price.SupplierCurrency;
                    WsResult.Fare.SupplierPrice = searchResult.Price.SupplierPrice;
                    WsResult.Fare.OtherCharges = searchResult.Price.OtherCharges;
                    WsResult.Fare.AdditionalTxnFee = searchResult.Price.AdditionalTxnFee;
                    WsResult.Fare.AirTransFee = searchResult.Price.AirlineTransFee;
                    WsResult.Fare.ServiceTax = searchResult.Price.SeviceTax;
                    WsResult.Fare.SServiceFee = searchResult.Price.SServiceFee;
                    WsResult.Fare.TransactionFee = searchResult.Price.TransactionFee;

                    WsResult.FareBreakdown[j] = new WSPTCFare();
                    WsResult.FareBreakdown[j].PassengerType = searchResult.FareBreakdown[j].PassengerType;
                    WsResult.FareBreakdown[j].PassengerCount = searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.FareBreakdown[j].B2CMarkup = searchResult.FareBreakdown[j].B2CMarkup;//Assign new markup
                    WsResult.FareBreakdown[j].Markup = searchResult.FareBreakdown[j].AgentMarkup;//Assign new markup
                    WsResult.FareBreakdown[j].SupplierPrice = (decimal)searchResult.FareBreakdown[j].SupplierFare / searchResult.FareBreakdown[j].PassengerCount;
                    WsResult.FareBreakdown[j].BaseFare = (decimal)searchResult.FareBreakdown[j].BaseFare + (searchResult.FareBreakdown[j].HandlingFee / searchResult.FareBreakdown[j].PassengerCount);
                    WsResult.FareBreakdown[j].Tax = (decimal)(searchResult.FareBreakdown[j].TotalFare - searchResult.FareBreakdown[j].BaseFare);
                    WsResult.FareBreakdown[j].SupplierCurrency = searchResult.Price.SupplierCurrency;
                    WsResult.FareBreakdown[j].AdditionalTxnFee = (decimal)searchResult.FareBreakdown[j].AdditionalTxnFee;
                    WsResult.FareBreakdown[j].AirlineTransFee = (decimal)searchResult.FareBreakdown[j].AirlineTransFee;
                }

                if (price.TaxDetails != null && price.TaxDetails.OutputVAT != null)
                {
                    WsResult.Fare.OutputVATApplied = price.TaxDetails.OutputVAT.Applied;
                    WsResult.Fare.OutputVATCharge = price.TaxDetails.OutputVAT.Charge;
                    WsResult.Fare.OutputVATOn = price.TaxDetails.OutputVAT.AppliedOn.ToString();
                }


                #region Building segments of the WsResult

                FlightInfo[] mSegment = SearchResult.GetSegments(searchResult);
                WsResult.Segment = new WSSegment[mSegment.Length];
                for (int j = 0; j < mSegment.Length; j++)
                {
                    WsResult.Segment[j] = WSSegment.ReadSegment(mSegment[j]);
                }
                WsResult.Origin = searchResult.Flights[0][0].Origin.AirportCode;
                TimeSpan Duration = new TimeSpan();
                TimeSpan ibDuration = new TimeSpan();
                for (int j = 0, l = 0; j < searchResult.Flights.Length; j++)
                {
                    for (int k = 0; k < searchResult.Flights[j].Length; k++, l++)
                    {
                        WsResult.Segment[l] = WSSegment.ReadSegment(searchResult.Flights[j][k]);
                        WsResult.Segment[l].SegmentIndicator = j + 1;

                        //Correcting Duration calculation as per local time zones. Done by Shiva 12 June 2018
                        //Adding durations to get total duration since we are already calculating according to local time zones in MSE
                        if (j == 0)
                        {
                            WsResult.ObSegCount++;
                            Duration = Duration.Add(searchResult.Flights[j][k].Duration);
                            WsResult.ObDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                            //result.ObDuration = (Duration.Days * 24 + Duration.Hours).ToString("00:") + Duration.Minutes.ToString("00");
                        }
                        else if (j == 1)
                        {
                            WsResult.IbSegCount++;
                            Duration = ibDuration.Add(searchResult.Flights[j][k].Duration);
                            WsResult.IbDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                            //result.IbDuration = (ibDuration.Days * 24 + ibDuration.Hours).ToString("00:") + ibDuration.Minutes.ToString("00");
                        }
                    }
                }

                //if (WsResult.ObSegCount > 0)
                //{
                //    TimeSpan tOut = mSegment[WsResult.ObSegCount - 1].AccumulatedDuration;
                //    WsResult.ObDuration = (tOut.Days * 24 + tOut.Hours).ToString("00:") + tOut.Minutes.ToString("00");
                //    //TODO: 
                //}
                //if (WsResult.IbSegCount > 0)
                //{
                //    TimeSpan tIn = mSegment[WsResult.IbSegCount + WsResult.ObSegCount - 1].AccumulatedDuration;
                //    WsResult.IbDuration = (tIn.Days * 24 + tIn.Hours).ToString("00:") + tIn.Minutes.ToString("00");
                //}
                #endregion

                int counter = 0;
                if (searchResult.FareRules != null)
                {
                    WsResult.FareBasis = new string[searchResult.FareRules.Count];
                    counter = 0;

                    for (int j = 0; j < searchResult.Flights.Length; j++)
                    {
                        for (int k = 0; k < searchResult.Flights[j].Length; k++)
                        {
                            if (counter < searchResult.FareRules.Count)
                            {
                                if (searchResult.FareRules[counter].FareInfoRef != null && searchResult.FareRules[counter].FareInfoRef.Length > 0)
                                {
                                    WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode + "~" + Convert.ToString(searchResult.FareRules[counter].FareInfoRef) + "~" + searchResult.FareRules[counter].FareRuleKeyValue;
                                }
                                else
                                {
                                    WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode;
                                }
                                counter = counter + 1;
                            }
                        }
                    }
                }              

            }

            //Added by Lokesh on 15-April-2018
            //This is For Amadeus Air Source
            //This method is called from FlightPaxDetail.aspx.cs page
            //Gets and updates the latest price to the selected result.
            //Fare_InformativePricingWithoutPNR method at amadeus end point.
            #region Amadeus Reprice Method
            else if (searchResult != null && searchResult.ResultBookingSource == BookingSource.Amadeus)
            {
                try
                {
                    Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Executing RePriceItinerary method", "", "AMADEUS");
                   // Fare[] fares = searchResult.FareBreakdown;
                    MetaSearchEngine mse = new MetaSearchEngine(sessionId);
                    mse.SettingsLoginInfo = loginInfo;
                    Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Call To MSE - RePriceAmadeus method", "", "AMADEUS");

                    SearchRequest mRequest = new SearchRequest();

                    mRequest.AdultCount = request.AdultCount;
                    mRequest.ChildCount = request.ChildCount;
                    mRequest.InfantCount = request.InfantCount;
                    mRequest.MaxStops = request.MaxStops;
                    mRequest.RefundableFares = request.RefundableFares;

                    SearchType searchType = SearchType.Return;

                    if (searchResult.Flights.Length == 1)
                    {
                        searchType = SearchType.OneWay;
                    }
                    else if (searchResult.Flights.Length == 2)
                    {
                        searchType = SearchType.Return;
                    }

                    if (searchType == SearchType.OneWay)
                    {
                        mRequest.Segments = new FlightSegment[1];
                    }
                    else if (searchType == SearchType.Return)
                    {
                        mRequest.Segments = new FlightSegment[2];
                    }

                    string[] prefCarrier;
                    if (request.PreferredCarrier != null && request.PreferredCarrier.Trim() != "")
                    {
                        prefCarrier = request.PreferredCarrier.Split(',');
                        for (int i = 0; i < prefCarrier.Length; i++)
                        {
                            prefCarrier[i] = prefCarrier[i].Trim().ToUpper();
                        }
                    }
                    else
                    {
                        prefCarrier = new string[0];
                    }

                    mRequest.Segments[0] = new FlightSegment();
                    mRequest.Segments[0].Origin = request.Origin;
                    mRequest.Segments[0].Destination = request.Destination;
                    mRequest.Segments[0].flightCabinClass = request.CabinClass;
                    mRequest.Segments[0].PreferredDepartureTime = request.DepartureDate;
                    mRequest.Segments[0].PreferredAirlines = prefCarrier;


                    if (mRequest.Type == SearchType.Return)
                    {
                        mRequest.Segments[1] = new FlightSegment();
                        mRequest.Segments[1].Origin = request.Origin;
                        mRequest.Segments[1].Destination = request.Destination;
                        mRequest.Segments[1].flightCabinClass = request.CabinClass;
                        mRequest.Segments[1].PreferredDepartureTime = request.ReturnDate;
                        mRequest.Segments[1].PreferredAirlines = prefCarrier;
                    }



                    mse.RePriceAmadeus(ref searchResult, mRequest,"B2C");
                    PriceAccounts price = searchResult.Price;
                    decimal inputVAT = 0m;
                    mse.CalculateInputVATForFlight(searchResult.Flights[0][0].Origin.CityCode, searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.CityCode, searchResult.ResultBookingSource, ref price, (decimal)searchResult.TotalFare, ref inputVAT);
                    searchResult.TotalFare += (double)inputVAT;
                    searchResult.Tax += (double)inputVAT;
                    
                    
                    WsResult.Source = BookingSource.Amadeus;
                    WsResult.Fare = new WSFare();
                    WsResult.Fare.TaxDetails = searchResult.Price.TaxDetails;
                    WsResult.BaggageIncludedInFare = searchResult.BaggageIncludedInFare;
                    WsResult.FareBreakdown = new WSPTCFare[searchResult.FareBreakdown.Length];
                    for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                    {
                        //searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, j, loginInfo.AgentId, 0, 1, "B2C");
                        WsResult.Fare.BaseFare += (searchResult.Price.PublishedFare * searchResult.FareBreakdown[j].PassengerCount) + searchResult.FareBreakdown[j].HandlingFee;
                        WsResult.Fare.Tax += searchResult.Price.Tax * searchResult.FareBreakdown[j].PassengerCount;
                        if (j == 0)
                        {
                            WsResult.Fare.Tax += inputVAT;
                        }
                        
                        WsResult.Fare.Discount += searchResult.FareBreakdown[j].AgentDiscount;
                        WsResult.Fare.Currency = searchResult.Price.Currency;
                        WsResult.Fare.Markup +=  searchResult.FareBreakdown[j].AgentMarkup;
                        WsResult.Fare.MarkupType = searchResult.Price.MarkupType;
                        WsResult.Fare.MarkupValue = searchResult.Price.MarkupValue;
                        WsResult.Fare.B2CMarkup +=  searchResult.FareBreakdown[j].B2CMarkup;
                        WsResult.Fare.B2CMarkupType = searchResult.Price.B2CMarkupType;
                        WsResult.Fare.B2CMarkupValue = searchResult.Price.B2CMarkupValue;
                        WsResult.Fare.SupplierCurrency = searchResult.Price.SupplierCurrency;
                        WsResult.Fare.SupplierPrice = searchResult.Price.SupplierPrice;
                        WsResult.FareBreakdown[j] = new WSPTCFare();
                        WsResult.FareBreakdown[j].PassengerType = searchResult.FareBreakdown[j].PassengerType;
                        WsResult.FareBreakdown[j].PassengerCount = searchResult.FareBreakdown[j].PassengerCount;
                        WsResult.FareBreakdown[j].B2CMarkup = searchResult.FareBreakdown[j].B2CMarkup / searchResult.FareBreakdown[j].PassengerCount;//Assign new markup
                        WsResult.FareBreakdown[j].Markup = searchResult.FareBreakdown[j].AgentMarkup / searchResult.FareBreakdown[j].PassengerCount;//Assign new markup
                        WsResult.FareBreakdown[j].SupplierPrice = (decimal)searchResult.FareBreakdown[j].SupplierFare / searchResult.FareBreakdown[j].PassengerCount;
                        WsResult.FareBreakdown[j].BaseFare = (decimal)searchResult.FareBreakdown[j].BaseFare + (searchResult.FareBreakdown[j].HandlingFee / searchResult.FareBreakdown[j].PassengerCount);
                        WsResult.FareBreakdown[j].Tax = (decimal)(searchResult.FareBreakdown[j].TotalFare - searchResult.FareBreakdown[j].BaseFare);
                        WsResult.FareBreakdown[j].SupplierCurrency = searchResult.Price.SupplierCurrency;
                    }

                    if (price.TaxDetails != null && price.TaxDetails.OutputVAT != null)
                    {
                        WsResult.Fare.OutputVATApplied = price.TaxDetails.OutputVAT.Applied;
                        WsResult.Fare.OutputVATCharge = price.TaxDetails.OutputVAT.Charge;
                        WsResult.Fare.OutputVATOn = price.TaxDetails.OutputVAT.AppliedOn.ToString();
                    }
                    #region Building segments of the WsResult

                    FlightInfo[] mSegment = SearchResult.GetSegments(searchResult);
                    WsResult.Segment = new WSSegment[mSegment.Length];
                    for (int j = 0; j < mSegment.Length; j++)
                    {
                        WsResult.Segment[j] = WSSegment.ReadSegment(mSegment[j]);
                    }
                    WsResult.Origin = searchResult.Flights[0][0].Origin.AirportCode;
                    TimeSpan Duration = new TimeSpan();
                    TimeSpan ibDuration = new TimeSpan();
                    for (int j = 0, l = 0; j < searchResult.Flights.Length; j++)
                    {
                        for (int k = 0; k < searchResult.Flights[j].Length; k++, l++)
                        {
                            WsResult.Segment[l] = WSSegment.ReadSegment(searchResult.Flights[j][k]);
                            WsResult.Segment[l].SegmentIndicator = j + 1;
                            WsResult.Destination = searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.AirportCode;

                            //Adding durations to get total duration since we are already calculating according to local time zones in MSE
                            if (j == 0)
                            {
                                WsResult.ObSegCount++;
                                Duration = Duration.Add(searchResult.Flights[j][k].Duration);
                                WsResult.ObDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                                //result.ObDuration = (Duration.Days * 24 + Duration.Hours).ToString("00:") + Duration.Minutes.ToString("00");
                            }
                            else if (j == 1)
                            {
                                WsResult.IbSegCount++;
                                Duration = ibDuration.Add(searchResult.Flights[j][k].Duration);
                                WsResult.IbDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                                //result.IbDuration = (ibDuration.Days * 24 + ibDuration.Hours).ToString("00:") + ibDuration.Minutes.ToString("00");
                            }
                        }

                    }

                    //if (WsResult.ObSegCount > 0)
                    //{
                    //    TimeSpan tOut = mSegment[WsResult.ObSegCount - 1].AccumulatedDuration;
                    //    WsResult.ObDuration = (tOut.Days * 24 + tOut.Hours).ToString("00:") + tOut.Minutes.ToString("00");

                    //}
                    //if (WsResult.IbSegCount > 0)
                    //{
                    //    TimeSpan tIn = mSegment[WsResult.IbSegCount + WsResult.ObSegCount - 1].AccumulatedDuration;
                    //    WsResult.IbDuration = (tIn.Days * 24 + tIn.Hours).ToString("00:") + tIn.Minutes.ToString("00");
                    //}
                    #endregion
                    int counter = 0;
                    if (searchResult.FareRules != null)
                    {
                        WsResult.FareBasis = new string[searchResult.FareRules.Count];
                        counter = 0;

                        for (int j = 0; j < searchResult.Flights.Length; j++)
                        {
                            for (int k = 0; k < searchResult.Flights[j].Length; k++)
                            {
                                if (counter < searchResult.FareRules.Count)
                                {
                                    if (searchResult.FareRules[counter].FareInfoRef != null && searchResult.FareRules[counter].FareInfoRef.Length > 0)
                                    {
                                        WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode + "~" + Convert.ToString(searchResult.FareRules[counter].FareInfoRef) + "~" + searchResult.FareRules[counter].FareRuleKeyValue;
                                    }
                                    else
                                    {
                                        WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode;
                                    }
                                    counter = counter + 1;
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Failed to Reprice for Amadeus. Reason : " + ex.ToString(), "", "Amadeus");
                }
            }
            #endregion

            // added by bangar for sabre repricing
            else if (searchResult != null && searchResult.ResultBookingSource == BookingSource.Sabre)
            {
                try
                {
                    Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Executing RePriceItinerary method", "", "Sabre");
                    // Fare[] fares = searchResult.FareBreakdown;
                    MetaSearchEngine mse = new MetaSearchEngine(sessionId);
                    mse.SettingsLoginInfo = loginInfo;
                    Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Call To MSE - RePriceSabre method", "", "Sabre");

                    SearchRequest mRequest = new SearchRequest();

                    mRequest.AdultCount = request.AdultCount;
                    mRequest.ChildCount = request.ChildCount;
                    mRequest.InfantCount = request.InfantCount;
                    mRequest.MaxStops = request.MaxStops;
                    mRequest.RefundableFares = request.RefundableFares;

                    SearchType searchType = SearchType.Return;

                    if (searchResult.Flights.Length == 1)
                    {
                        searchType = SearchType.OneWay;
                        mRequest.Type = SearchType.OneWay;
                    }
                    else if (searchResult.Flights.Length == 2)
                    {
                        searchType = SearchType.Return;
                        mRequest.Type = SearchType.Return;
                    }

                    if (searchType == SearchType.OneWay)
                    {
                        mRequest.Segments = new FlightSegment[1];
                    }
                    else if (searchType == SearchType.Return)
                    {
                        mRequest.Segments = new FlightSegment[2];
                    }

                    string[] prefCarrier;
                    if (request.PreferredCarrier != null && request.PreferredCarrier.Trim() != "")
                    {
                        prefCarrier = request.PreferredCarrier.Split(',');
                        for (int i = 0; i < prefCarrier.Length; i++)
                        {
                            prefCarrier[i] = prefCarrier[i].Trim().ToUpper();
                        }
                    }
                    else
                    {
                        prefCarrier = new string[0];
                    }

                    mRequest.Segments[0] = new FlightSegment();
                    mRequest.Segments[0].Origin = request.Origin;
                    mRequest.Segments[0].Destination = request.Destination;
                    mRequest.Segments[0].flightCabinClass = request.CabinClass;
                    mRequest.Segments[0].PreferredDepartureTime = request.DepartureDate;
                    mRequest.Segments[0].PreferredAirlines = prefCarrier;


                    if (mRequest.Type == SearchType.Return)
                    {
                        mRequest.Segments[1] = new FlightSegment();
                        mRequest.Segments[1].Origin = request.Origin;
                        mRequest.Segments[1].Destination = request.Destination;
                        mRequest.Segments[1].flightCabinClass = request.CabinClass;
                        mRequest.Segments[1].PreferredDepartureTime = request.ReturnDate;
                        mRequest.Segments[1].PreferredAirlines = prefCarrier;
                    }


                    mse.SabreAuthenicateToken = BookingValuesChanged;
                    mse.RePriceSabre(ref searchResult, mRequest, "B2C");
                    PriceAccounts price = searchResult.Price;
                    decimal inputVAT = 0m;
                    mse.CalculateInputVATForFlight(searchResult.Flights[0][0].Origin.CityCode, searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.CityCode, searchResult.ResultBookingSource, ref price, (decimal)searchResult.TotalFare, ref inputVAT);
                    searchResult.TotalFare += (double)inputVAT;
                    searchResult.Tax += (double)inputVAT;


                    WsResult.Source = BookingSource.Sabre;
                    WsResult.Fare = new WSFare();
                    WsResult.Fare.TaxDetails = searchResult.Price.TaxDetails;
                    WsResult.BaggageIncludedInFare = searchResult.BaggageIncludedInFare;
                    WsResult.FareBreakdown = new WSPTCFare[searchResult.FareBreakdown.Length];
                    for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                    {
                        searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, j, loginInfo.AgentId, 0, 1, "B2C");
                        WsResult.Fare.BaseFare += (searchResult.Price.PublishedFare * searchResult.FareBreakdown[j].PassengerCount) + searchResult.FareBreakdown[j].HandlingFee;
                        WsResult.Fare.Tax += searchResult.Price.Tax * searchResult.FareBreakdown[j].PassengerCount;
                        if (j == 0)
                        {
                            WsResult.Fare.Tax += inputVAT;
                        }

                        searchResult.FareBreakdown[j].AgentMarkup = searchResult.Price.Markup;
                        searchResult.FareBreakdown[j].AgentDiscount = searchResult.Price.Discount + searchResult.Price.WhiteLabelDiscount;
                        searchResult.FareBreakdown[j].B2CMarkup = searchResult.Price.B2CMarkup;

                        WsResult.Fare.Discount += searchResult.FareBreakdown[j].AgentDiscount * searchResult.FareBreakdown[j].PassengerCount;
                        WsResult.Fare.Currency = searchResult.Price.Currency;
                        WsResult.Fare.Markup += searchResult.FareBreakdown[j].AgentMarkup * searchResult.FareBreakdown[j].PassengerCount;
                        WsResult.Fare.MarkupType = searchResult.Price.MarkupType;
                        WsResult.Fare.MarkupValue = searchResult.Price.MarkupValue;
                        WsResult.Fare.B2CMarkup += searchResult.FareBreakdown[j].B2CMarkup * searchResult.FareBreakdown[j].PassengerCount;
                        WsResult.Fare.B2CMarkupType = searchResult.Price.B2CMarkupType;
                        WsResult.Fare.B2CMarkupValue = searchResult.Price.B2CMarkupValue;
                        WsResult.Fare.SupplierCurrency = searchResult.Price.SupplierCurrency;
                        WsResult.Fare.SupplierPrice = searchResult.Price.SupplierPrice;

                        WsResult.FareBreakdown[j] = new WSPTCFare();
                        WsResult.FareBreakdown[j].PassengerType = searchResult.FareBreakdown[j].PassengerType;
                        WsResult.FareBreakdown[j].PassengerCount = searchResult.FareBreakdown[j].PassengerCount;
                        WsResult.FareBreakdown[j].B2CMarkup = searchResult.FareBreakdown[j].B2CMarkup;//Assign new markup
                        WsResult.FareBreakdown[j].Markup = searchResult.FareBreakdown[j].AgentMarkup;//Assign new markup
                        WsResult.FareBreakdown[j].SupplierPrice = (decimal)searchResult.FareBreakdown[j].SupplierFare / searchResult.FareBreakdown[j].PassengerCount;
                        WsResult.FareBreakdown[j].BaseFare = (decimal)searchResult.FareBreakdown[j].BaseFare + (searchResult.FareBreakdown[j].HandlingFee / searchResult.FareBreakdown[j].PassengerCount);
                        WsResult.FareBreakdown[j].Tax = (decimal)(searchResult.FareBreakdown[j].TotalFare - searchResult.FareBreakdown[j].BaseFare);
                        WsResult.FareBreakdown[j].SupplierCurrency = searchResult.Price.SupplierCurrency;

                    }

                    if (price.TaxDetails != null && price.TaxDetails.OutputVAT != null)
                    {
                        WsResult.Fare.OutputVATApplied = price.TaxDetails.OutputVAT.Applied;
                        WsResult.Fare.OutputVATCharge = price.TaxDetails.OutputVAT.Charge;
                        WsResult.Fare.OutputVATOn = price.TaxDetails.OutputVAT.AppliedOn.ToString();
                    }
                    #region Building segments of the WsResult

                    for (int j = 0, l = 0; j < searchResult.Flights.Length; j++)
                    {
                        for (int k = 0; k < searchResult.Flights[j].Length; k++, l++)
                        {
                            if (searchResult.Flights[j][k].Origin.TimeZone > 0 && searchResult.Flights[j][k].Destination.TimeZone > 0)
                            {
                                TimeSpan diffTime = searchResult.Flights[j][k].ArrivalTime.Subtract(searchResult.Flights[j][k].DepartureTime);
                                TimeSpan DestinationUTC = TimeSpan.Parse(searchResult.Flights[j][k].Destination.TimeZone.ToString().Replace(".", ":"));
                                TimeSpan OriginUTC = TimeSpan.Parse(searchResult.Flights[j][k].Origin.TimeZone.ToString().Replace(".", ":"));
                                if (DestinationUTC > OriginUTC)
                                {
                                    TimeSpan UTCDiff = DestinationUTC.Subtract(OriginUTC);
                                    searchResult.Flights[j][k].Duration = diffTime.Subtract(UTCDiff);
                                }
                                else
                                {
                                    TimeSpan UTCDiff = OriginUTC.Subtract(DestinationUTC);
                                    searchResult.Flights[j][k].Duration = diffTime.Add(UTCDiff);
                                }
                            }
                        }
                    }

                    FlightInfo[] mSegment = SearchResult.GetSegments(searchResult);
                    WsResult.Segment = new WSSegment[mSegment.Length];
                    for (int j = 0; j < mSegment.Length; j++)
                    {
                        WsResult.Segment[j] = WSSegment.ReadSegment(mSegment[j]);
                    }
                    WsResult.Origin = searchResult.Flights[0][0].Origin.AirportCode;
                    TimeSpan Duration = new TimeSpan();
                    TimeSpan ibDuration = new TimeSpan();
                    for (int j = 0, l = 0; j < searchResult.Flights.Length; j++)
                    {
                        for (int k = 0; k < searchResult.Flights[j].Length; k++, l++)
                        {
                            WsResult.Segment[l] = WSSegment.ReadSegment(searchResult.Flights[j][k]);
                            WsResult.Segment[l].SegmentIndicator = j + 1;
                            WsResult.Destination = searchResult.Flights[0][searchResult.Flights[0].Length - 1].Destination.AirportCode;                           

                            //Adding durations to get total duration since we are already calculating according to local time zones in MSE
                            if (j == 0)
                            {
                                WsResult.ObSegCount++;
                                Duration = Duration.Add(searchResult.Flights[j][k].Duration);
                                WsResult.ObDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                                //result.ObDuration = (Duration.Days * 24 + Duration.Hours).ToString("00:") + Duration.Minutes.ToString("00");
                            }
                            else if (j == 1)
                            {
                                WsResult.IbSegCount++;
                                Duration = ibDuration.Add(searchResult.Flights[j][k].Duration);
                                WsResult.IbDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                                //result.IbDuration = (ibDuration.Days * 24 + ibDuration.Hours).ToString("00:") + ibDuration.Minutes.ToString("00");
                            }
                        }

                    }

                  
                    #endregion
                    int counter = 0;
                    if (searchResult.FareRules != null)
                    {
                        WsResult.FareBasis = new string[searchResult.FareRules.Count];
                        counter = 0;

                        for (int j = 0; j < searchResult.Flights.Length; j++)
                        {
                            for (int k = 0; k < searchResult.Flights[j].Length; k++)
                            {
                                if (counter < searchResult.FareRules.Count)
                                {
                                    if (searchResult.FareRules[counter].FareInfoRef != null && searchResult.FareRules[counter].FareInfoRef.Length > 0)
                                    {
                                        WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode + "~" + Convert.ToString(searchResult.FareRules[counter].FareInfoRef) + "~" + searchResult.FareRules[counter].FareRuleKeyValue;
                                    }
                                    else
                                    {
                                        WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode;
                                    }
                                    counter = counter + 1;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Failed to Reprice for Sabre. Reason : " + ex.ToString(), "", "Sabre");
                }
            }
           
            #region Reprice Logic for LCC's
            else if (searchResult.ResultBookingSource == BookingSource.Indigo || (request.InfantCount<=0 && searchResult.ResultBookingSource==BookingSource.SpiceJet) || (request.InfantCount <= 0 && searchResult.ResultBookingSource == BookingSource.GoAir))
            {
                string source = string.Empty; 
                try
                {
                    SearchRequest searchRequest = new SearchRequest();
                    searchRequest.AdultCount = request.AdultCount;
                    searchRequest.ChildCount = request.ChildCount;
                    searchRequest.InfantCount = request.InfantCount;
                    if (searchResult.Flights.Length == 1)
                    {
                        searchRequest.Type = SearchType.OneWay;
                        searchRequest.Segments = new FlightSegment[1];
                        searchRequest.Segments[0] = new FlightSegment();
                        searchRequest.Segments[0].Origin = request.Origin;
                        searchRequest.Segments[0].Destination = request.Destination;

                    }
                    else if (searchResult.Flights.Length == 2)
                    {
                        searchRequest.Type = SearchType.Return;
                        searchRequest.Segments = new FlightSegment[2];
                        searchRequest.Segments[0] = new FlightSegment();
                        searchRequest.Segments[0].Origin = request.Origin;
                        searchRequest.Segments[0].Destination = request.Destination;
                        searchRequest.Segments[1] = new FlightSegment();
                        searchRequest.Segments[1].Origin = request.Destination;
                        searchRequest.Segments[1].Destination = request.Origin;
                    }

                    MetaSearchEngine mse = new MetaSearchEngine(sessionId);
                    mse.SettingsLoginInfo = loginInfo;
                    mse.SessionId = sessionId;
                    searchResult.FareBreakdown = null;
                    int farebreakdown = 0;
                    if (request.AdultCount > 0)
                        farebreakdown++;
                    if (request.ChildCount > 0)
                        farebreakdown++;
                    if (request.InfantCount > 0)
                        farebreakdown++;
                    searchResult.FareBreakdown = new Fare[farebreakdown];
                    source= SearchResult.GetFlightBookingSource(searchResult.ResultBookingSource);
                    mse.GetItineraryPrice(ref searchResult, searchRequest);
                    PriceAccounts price = searchResult.Price;
                    WsResult.ResultId = resultId;
                    WsResult.Source = searchResult.ResultBookingSource;
                    WsResult.IsLcc = true;
                    WsResult.Fare = new WSFare();
                    WsResult.Fare.TaxDetails = searchResult.Price.TaxDetails;
                    WsResult.FareBreakdown = new WSPTCFare[searchResult.FareBreakdown.Length];
                    WsResult.BaggageIncludedInFare = searchResult.BaggageIncludedInFare;
                    WsResult.IsGSTMandatory = (loginInfo.LocationCountryCode == "IN") ? true : false;

                    PriceAccounts tempPrice = searchResult.Price;
                    for (int j = 0; j < searchResult.FareBreakdown.Length; j++)
                    {

                        searchResult.Price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, j, loginInfo.AgentId, 0, 1, "B2C");

                        searchResult.FareBreakdown[j].AgentMarkup = searchResult.Price.Markup;
                        searchResult.FareBreakdown[j].AgentDiscount = searchResult.Price.Discount + searchResult.Price.WhiteLabelDiscount;
                        searchResult.FareBreakdown[j].B2CMarkup = searchResult.Price.B2CMarkup;

                        WsResult.Fare.BaseFare += (decimal)(searchResult.FareBreakdown[j].BaseFare) + (searchResult.FareBreakdown[j].HandlingFee);
                        WsResult.Fare.Tax += searchResult.FareBreakdown[j].Tax;
                        WsResult.Fare.Discount += (searchResult.FareBreakdown[j].AgentDiscount * searchResult.FareBreakdown[j].PassengerCount); 
                        WsResult.Fare.Currency = searchResult.Currency;
                        WsResult.Fare.Markup += (searchResult.FareBreakdown[j].AgentMarkup * searchResult.FareBreakdown[j].PassengerCount);
                        WsResult.Fare.MarkupType = searchResult.Price.MarkupType;
                        WsResult.Fare.MarkupValue = searchResult.Price.MarkupValue;
                        WsResult.Fare.B2CMarkup += (searchResult.FareBreakdown[j].B2CMarkup * searchResult.FareBreakdown[j].PassengerCount);
                        WsResult.Fare.B2CMarkupType = searchResult.Price.B2CMarkupType;
                        WsResult.Fare.B2CMarkupValue = searchResult.Price.B2CMarkupValue;
                        WsResult.Fare.SupplierCurrency = searchResult.Price.SupplierCurrency;
                        WsResult.Fare.SupplierPrice = searchResult.Price.SupplierPrice;

                        WsResult.FareBreakdown[j] = new WSPTCFare();
                        WsResult.FareBreakdown[j].PassengerType = searchResult.FareBreakdown[j].PassengerType;
                        WsResult.FareBreakdown[j].PassengerCount = searchResult.FareBreakdown[j].PassengerCount;
                        WsResult.FareBreakdown[j].B2CMarkup = searchResult.FareBreakdown[j].B2CMarkup;
                        WsResult.FareBreakdown[j].Markup = searchResult.FareBreakdown[j].AgentMarkup;
                        WsResult.FareBreakdown[j].SupplierCurrency = searchResult.Price.SupplierCurrency;
                        WsResult.FareBreakdown[j].SupplierPrice = (decimal)searchResult.FareBreakdown[j].SupplierFare;
                        WsResult.FareBreakdown[j].BaseFare = (decimal)searchResult.FareBreakdown[j].BaseFare + (searchResult.FareBreakdown[j].HandlingFee);
                        WsResult.FareBreakdown[j].AirlineTransFee = searchResult.FareBreakdown[j].AirlineTransFee;
                        WsResult.FareBreakdown[j].Tax = (decimal)(searchResult.FareBreakdown[j].TotalFare - searchResult.FareBreakdown[j].BaseFare);
                    }
                    if (price.TaxDetails != null && price.TaxDetails.OutputVAT != null)
                    {
                        WsResult.Fare.OutputVATApplied = price.TaxDetails.OutputVAT.Applied;
                        WsResult.Fare.OutputVATCharge = price.TaxDetails.OutputVAT.Charge;
                        WsResult.Fare.OutputVATOn = price.TaxDetails.OutputVAT.AppliedOn.ToString();
                    }
                    #region Building segments of the WsResult

                    FlightInfo[] mSegment = SearchResult.GetSegments(searchResult);
                    WsResult.Segment = new WSSegment[mSegment.Length];
                    for (int j = 0; j < mSegment.Length; j++)
                    {
                        WsResult.Segment[j] = WSSegment.ReadSegment(mSegment[j]);
                    }
                    WsResult.Origin = searchResult.Flights[0][0].Origin.AirportCode;
                    TimeSpan Duration = new TimeSpan();
                    TimeSpan ibDuration = new TimeSpan();
                    for (int j = 0, l = 0; j < searchResult.Flights.Length; j++)
                    {
                        for (int k = 0; k < searchResult.Flights[j].Length; k++, l++)
                        {
                            WsResult.Segment[l] = WSSegment.ReadSegment(searchResult.Flights[j][k]);
                            WsResult.Segment[l].SegmentIndicator = j + 1;

                            //Correcting Duration calculation as per local time zones. Done by Shiva 12 June 2018
                            //Adding durations to get total duration since we are already calculating according to local time zones in MSE
                            if (j == 0)
                            {
                                WsResult.ObSegCount++;
                                Duration = Duration.Add(searchResult.Flights[j][k].Duration);
                                WsResult.ObDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                                //result.ObDuration = (Duration.Days * 24 + Duration.Hours).ToString("00:") + Duration.Minutes.ToString("00");
                            }
                            else if (j == 1)
                            {
                                WsResult.IbSegCount++;
                                Duration = ibDuration.Add(searchResult.Flights[j][k].Duration);
                                WsResult.IbDuration = (Duration.Days >= 1 ? Math.Floor(Duration.TotalHours).ToString() : (Duration.Hours.ToString().Length > 1 ? Duration.Hours.ToString() : "0" + Duration.Hours)) + ":" + (Duration.Minutes.ToString().Length > 1 ? Duration.Minutes.ToString() : "0" + Duration.Minutes);
                                //result.IbDuration = (ibDuration.Days * 24 + ibDuration.Hours).ToString("00:") + ibDuration.Minutes.ToString("00");
                            }
                        }
                    }
                    #endregion
                    int counter = 0;
                    if (searchResult.FareRules != null)
                    {
                        WsResult.FareBasis = new string[searchResult.FareRules.Count];
                        counter = 0;

                        for (int j = 0; j < searchResult.Flights.Length; j++)
                        {
                            for (int k = 0; k < searchResult.Flights[j].Length; k++)
                            {
                                if (counter < searchResult.FareRules.Count)
                                {
                                    if (searchResult.FareRules[counter].FareInfoRef != null && searchResult.FareRules[counter].FareInfoRef.Length > 0)
                                    {
                                        WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode + "~" + Convert.ToString(searchResult.FareRules[counter].FareInfoRef) + "~" + searchResult.FareRules[counter].FareRuleKeyValue;
                                    }
                                    else
                                    {
                                        WsResult.FareBasis[counter] = searchResult.Flights[j][k].Airline + searchResult.Flights[j][k].Origin.CityCode + searchResult.Flights[j][k].Destination.CityCode + "~" + searchResult.FareRules[counter].FareBasisCode;
                                    }
                                    counter = counter + 1;
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    errorMsg = ex.ToString();
                    Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Failed to Reprice for "+ source + " Reason : " + ex.ToString(), "", source);
                    throw ex;
                }
            }
            #endregion
            
            try
            {
                //Required to know Markup and ROE values for Changed Price
                string xmlPath = ConfigurationManager.AppSettings["WEGOXmlLogs"];
                //=================================== Write search response into xml file ======================================//
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(WsResult.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, WsResult);  // Here Classes are converted to XML String. 
                                                  //This can be viewed in SB or writer.
                                                  //Above XML in SB can be loaded in XmlDocument object
                string filepath = xmlPath + "RePriceResponse_" + DateTime.Now.ToString("yyyyMMMdd_hhmmss") + ".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(filepath);
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.Search, Severity.High, 0, "(WEGO RePrice)Failed to write RePrice response xml. Reason : " + ex.ToString(), "");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.RePrice, Severity.High, userId, "(" + Credential.SiteName + ") Failed to Reprice for UAPI. Reason : " + ex.ToString(), "", "UAPI");
            if (!string.IsNullOrEmpty(errorMsg))
                throw new Exception(errorMsg);
        }
        return WsResult;
    }


    SearchRequest GenerateSearchRequest(WSSearchRequest request, SearchResult searchResult)
    {
        SearchRequest mRequest = new SearchRequest();

        mRequest.AdultCount = request.AdultCount;
        mRequest.ChildCount = request.ChildCount;
        mRequest.InfantCount = request.InfantCount;
        mRequest.MaxStops = request.MaxStops;
        mRequest.RefundableFares = request.RefundableFares;

        SearchType searchType = SearchType.Return;

        if (searchResult.Flights.Length == 1)
        {
            searchType = SearchType.OneWay;
        }
        else if (searchResult.Flights.Length == 2)
        {
            searchType = SearchType.Return;
        }

        if (searchType == SearchType.OneWay)
        {
            mRequest.Segments = new FlightSegment[1];
        }
        else if (searchType == SearchType.Return)
        {
            mRequest.Segments = new FlightSegment[2];
        }

        string[] prefCarrier;
        if (request.PreferredCarrier != null && request.PreferredCarrier.Trim() != "")
        {
            prefCarrier = request.PreferredCarrier.Split(',');
            for (int i = 0; i < prefCarrier.Length; i++)
            {
                prefCarrier[i] = prefCarrier[i].Trim().ToUpper();
            }
        }
        else
        {
            prefCarrier = new string[0];
        }

        mRequest.Segments[0] = new FlightSegment();
        mRequest.Segments[0].Origin = request.Origin;
        mRequest.Segments[0].Destination = request.Destination;
        mRequest.Segments[0].flightCabinClass = request.CabinClass;
        mRequest.Segments[0].PreferredDepartureTime = request.DepartureDate;
        mRequest.Segments[0].PreferredAirlines = prefCarrier;


        if (mRequest.Type == SearchType.Return)
        {
            mRequest.Segments[1] = new FlightSegment();
            mRequest.Segments[1].Origin = request.Origin;
            mRequest.Segments[1].Destination = request.Destination;
            mRequest.Segments[1].flightCabinClass = request.CabinClass;
            mRequest.Segments[1].PreferredDepartureTime = request.ReturnDate;
            mRequest.Segments[1].PreferredAirlines = prefCarrier;
        }

        return mRequest;
    }
 //Used for Get availiable plans for VISA
    [WebMethod]
    [SoapHeader("Credential")] //Used for Get availiable plans for VISA
    public CT.BookingEngine.Insurance.InsuranceResponse GetAvailableInsurancePlans(CT.BookingEngine.Insurance.InsuranceRequest request)
    {
        UserMaster userMaster = new UserMaster();
        userMaster = WSValidate(Credential);
        int UserId = 0;
        UserPreference pref = new UserPreference();
        UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
        AgentLoginInfo = UserMaster.GetB2CUser(UserId);
        CT.BookingEngine.Insurance.InsuranceResponse availPlans = new CT.BookingEngine.Insurance.InsuranceResponse();
        if (true)
        {
            try
            {
                MetaSearchEngine mse = new MetaSearchEngine();
                mse.SettingsLoginInfo = AgentLoginInfo;
                Audit.Add(EventType.InsuranceBooking, Severity.Low, UserId, "sending insurance request in Booking api", "");
                //Loading all plans
                availPlans = mse.GetAvailablePlans(request);
                if (availPlans != null && availPlans.UpsellPlans != null && availPlans.UpsellPlans.Count > 0)
                {
                    //calucating B2C markup
                    UpdatePrice(ref availPlans, AgentLoginInfo.AgentId);
                }
                Audit.Add(EventType.InsuranceBooking, Severity.Low, UserId, "getting plans insurance response in Booking api", "");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.InsuranceBooking, Severity.Low, UserId, "getting error Booking APi in GetAvailableInsPlans exception:" + ex.ToString(), "");
                availPlans.ErrorMessage = ex.ToString();
            }
        }
        return availPlans;

    }

    [WebMethod]
    [SoapHeader("Credential")] //Used for Insurance policy confirmation for VISA
    public void ConfirmInsPolicyPurchase(ref CT.BookingEngine.Insurance.InsuranceHeader header)
    {
        try
        {
            int UserId = 0;
            UserPreference pref = new UserPreference();
            UserId = pref.GetMemberIdBySiteName(Credential.SiteName);
            AgentLoginInfo = UserMaster.GetB2CUser(UserId);
            header.CreatedBy = (int)AgentLoginInfo.UserID;
            header.LocationId = (int)AgentLoginInfo.LocationID;
            header.AgentId = AgentLoginInfo.AgentId;
            header.Currency = AgentLoginInfo.Currency;
            header.CreatedBy = UserId;
            MetaSearchEngine mse = new MetaSearchEngine();
            mse.SettingsLoginInfo = AgentLoginInfo;
            mse.ConfirmPolicyPurchase(ref header);

            if (header.Error == "0")
            {
                //Invoice 
                try
                {

                    Invoice invoice = new Invoice();
                    int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(header.Id, CT.BookingEngine.ProductType.Insurance);
                    if (invoiceNumber > 0)
                    {
                        invoice.Load(invoiceNumber);
                    }
                    else
                    {
                        invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(header.Id, "", (int)AgentLoginInfo.UserID, CT.BookingEngine.ProductType.Insurance, 1);
                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                            invoice.Status = CT.BookingEngine.InvoiceStatus.Paid;
                            invoice.CreatedBy = (int)AgentLoginInfo.UserID;
                            invoice.LastModifiedBy = (int)AgentLoginInfo.UserID;
                            invoice.UpdateInvoice();
                        }
                    }
                    header.ReturnFlightNo = invoiceNumber.ToString();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.InsuranceBooking, Severity.High, (int)AgentLoginInfo.UserID, "B2C-Exception while Confirming ZEUS Travel Insurance Plan" + ex.ToString(), "");
                }
            }
            else
            {
                Audit.Add(EventType.InsuranceBooking, Severity.High, (int)AgentLoginInfo.UserID, "B2C-Exception while Confirming ZEUS Travel Insurance Plan" + header.Error, "");
            }
        }
        catch (Exception ex)
        {
            header.Error = "InsuranceBooking is Failed" + ex.ToString();
            Audit.Add(EventType.InsuranceBooking, Severity.High, 0, "B2C-Exception while Confirming ZEUS Travel Insurance Plan " + ex.ToString(), "");
        }
    }
}
