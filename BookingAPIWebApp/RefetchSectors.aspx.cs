﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Configuration;

namespace BookingAPIWebApp
{
    public partial class RefetchSectors : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CT.BookingEngine.SectorList.GetSectorList(true);
            //CacheData.SectorsList = null;
        }
    }
}