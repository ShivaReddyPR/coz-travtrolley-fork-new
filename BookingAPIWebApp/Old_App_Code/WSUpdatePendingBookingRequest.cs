using System;
using CT.BookingEngine.WhiteLabel;

/// <summary>
/// Summary description for WSUpdatePendingBookingRequest
/// </summary>
public class WSUpdatePendingBookingRequest
{

    # region Private Fields
    int queueId;
    PaymentGatewaySource paySource;
    string paymentId;
    string orderId;
    int bookingIdOut;
    int bookingIdIn;
    WLBookingStatus bookingStatus;
    bool inTicketed;
    bool outTicketed;
    string inPNR;
    string outPNR;
    string remarks;
    PaymentStatus paymentStatus;
    # endregion

    # region Public Properties

    public int QueueId
    {
        get
        {
            return queueId;
        }
        set
        {
            queueId = value;
        }
    }

    public int BookingIdOut
    {
        get
        {
            return bookingIdOut;
        }
        set
        {
            bookingIdOut = value;
        }
    }

    public int BookingIdIn
    {
        get
        {
            return bookingIdIn;
        }
        set
        {
            bookingIdIn = value;
        }
    }

    public string OutPNR
    {
        get
        {
            return outPNR;
        }
        set
        {
            outPNR = value;
        }
    }

    public string InPNR
    {
        get
        {
            return inPNR;
        }
        set
        {
            inPNR = value;
        }
    }

    public string PaymentId
    {
        get
        {
            return paymentId;
        }
        set
        {
            paymentId = value;
        }
    }

    public PaymentGatewaySource PaySource
    {
        get
        {
            return paySource;
        }
        set
        {
            paySource = value;
        }
    }

    public WLBookingStatus BookingStatus
    {
        get
        {
            return bookingStatus;
        }
        set
        {
            bookingStatus = value;
        }
    }

    public bool OutTicketed
    {
        set
        {
            outTicketed = value;
        }
        get
        {
            return outTicketed;
        }
    }

    public bool InTicketed
    {
        set
        {
            inTicketed = value;
        }
        get
        {
            return inTicketed;
        }
    }

    public string Remarks
    {
        set
        {
            remarks = value;
        }
        get
        {
            return remarks;
        }
    }

    public PaymentStatus PaymentStatus
    {
        set
        {
            paymentStatus = value;
        }
        get
        {
            return paymentStatus;
        }
    }

    public string OrderId
    {
        set
        {
            orderId = value;
        }
        get
        {
            return orderId;
        }
    }

    #endregion


    public WSUpdatePendingBookingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    # region Methods
    public WSUpdatePendingBookingResponse Update(int UserMasterId)
    {
        WLPendingQueue itPendingQueue = new WLPendingQueue();
        itPendingQueue.BookingIdIn = Convert.ToInt32(bookingIdIn);
        itPendingQueue.BookingIdOut = Convert.ToInt32(bookingIdOut);
        itPendingQueue.BookingStatus = bookingStatus;
        itPendingQueue.InPNR = inPNR;
        itPendingQueue.InTicketed = inTicketed;
        itPendingQueue.LastModifiedBy = UserMasterId;
        itPendingQueue.OrderId = orderId;
        itPendingQueue.OutPNR = outPNR;
        itPendingQueue.OutTicketed = outTicketed;
        itPendingQueue.PaymentId = paymentId;
        itPendingQueue.PaymentStatus = (PaymentStatus)Enum.Parse(typeof(PaymentStatus), Convert.ToString(paymentStatus));
        itPendingQueue.PaySource = (CT.BookingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.BookingEngine.PaymentGatewaySource), Convert.ToString(paySource));
        itPendingQueue.Remarks = remarks;
        itPendingQueue.QueueId = queueId;
        int referenceId = itPendingQueue.Save();

        WSUpdatePendingBookingResponse saveResponse = new WSUpdatePendingBookingResponse();
        saveResponse.Status = new WSStatus();
        saveResponse.Status.Category = "UPD";
        if (referenceId > 0)
        {
            saveResponse.Status.Description = "Details Updated";
            saveResponse.Status.StatusCode = "1";
            saveResponse.QueueId = referenceId.ToString();
        }
        else
        {
            saveResponse.Status.Description = "Detail Update Failed";
            saveResponse.Status.StatusCode = "2";
        }
        return saveResponse;
    }
    # endregion
}
