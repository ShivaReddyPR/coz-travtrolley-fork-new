/// <summary>
/// Summary description for WSGetFareQuoteRequest
/// </summary>
public class WSGetFareQuoteRequest
{
    private WSResult result;
    public WSResult Result
    {
        get { return result; }
        set { result = value; }
    }

    private string sessionId;
    public string SessionId
    {
        get { return sessionId; }
        set { sessionId = value; }
    }

    //Added by lokesh on 4-April-2018
    //Applicable to only G9 Air Source
    //If the customer is travelling from India.
    //And if the lead pax supplies state code and TaxRegNo.

    private string stateCode;
    public string StateCode
    {
        get { return stateCode; }
        set { stateCode = value; }
    }

    private string taxRegNo;
    public string TaxRegNo
    {
        get { return taxRegNo; }
        set { taxRegNo = value; }
    }

    //added by Shiva 10 Apr 2018
    private decimal baggageCharge;
    public decimal BaggageCharge
    {
        get { return baggageCharge; }
        set { baggageCharge = value; }
    }

    public WSGetFareQuoteRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
