﻿//using System.Linq;

/// <summary>
/// Summary description for WSPromoDetail
/// </summary>
public class WSPromoDetail
{    
    int promoId;
    public int PromoId
    {
        get { return promoId; }
        set { promoId = value; }
    }
    string promoCode;
    public string PromoCode
    {
        get { return promoCode; }
        set { promoCode = value; }
    }
    string promoDiscountType;
    public string PromoDiscountType
    {
        get { return promoDiscountType; }
        set { promoDiscountType = value; }
    }
    decimal promoDiscountAmount;
    public decimal PromoDiscountAmount
    {
        get { return promoDiscountAmount; }
        set { promoDiscountAmount = value; }
    }
    decimal promoDiscountValue;
    public decimal PromoDiscountValue
    {
        get { return promoDiscountValue; }
        set { promoDiscountValue = value; }
    }

    public WSPromoDetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
