using CT.BookingEngine;

/// <summary>
/// Summary description for WSCancelItineraryRequest
/// </summary>
public class WSCancelItineraryRequest
{
    private string bookingId;
    public string BookingId
    {
        get { return bookingId; }
        set { bookingId = value; }
    }

    private BookingSource source;
    public BookingSource Source
    {
        get { return source; }
        set { source = value; }
    }

    public WSCancelItineraryRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
