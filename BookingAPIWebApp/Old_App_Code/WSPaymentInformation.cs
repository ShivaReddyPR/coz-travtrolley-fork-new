/// <summary>
/// Summary description for WSPaymentInformation
/// </summary>
public class WSPaymentInformation
{
    private int paymentInformationId;
    private int invoiceNumber;
    private string paymentId;
    private decimal amount;
    private string ipAddress;
    private string trackId;
    private PaymentGatewaySource paymentGateway;
    private PaymentModeType paymentModeType;
    private decimal charges;
    public int PaymentInformationId
    {
        get
        {
            return paymentInformationId;
        }
        set
        {
            paymentInformationId = value;
        }
    }
    public int InvoiceNumber
    {
        get
        {
            return invoiceNumber;
        }
        set
        {
            invoiceNumber = value;
        }
    }
    public string PaymentId
    {
        get
        {
            return paymentId;
        }
        set
        {
            paymentId = value;
        }
    }
    public decimal Amount
    {
        get
        {
            return amount;
        }
        set
        {
            amount = value;
        }
    }
    public string IPAddress
    {
        get
        {
            return ipAddress;
        }
        set
        {
            ipAddress = value;
        }
    }
    public string TrackId
    {
        get
        {
            return trackId;
        }
        set
        {
            trackId = value;
        }
    }
    public PaymentGatewaySource PaymentGateway
    {
        get
        {
            return paymentGateway;
        }
        set
        {
            paymentGateway = value;
        }
    }
    public PaymentModeType PaymentModeType
    {
        get
        {
            return paymentModeType;
        }
        set
        {
            paymentModeType = value;
        }
    }
    public decimal Charges
    {
        get { return charges; }
        set { charges = value; }
    }
    public WSPaymentInformation()
    {

    }
}
public enum PaymentModeType
{
    CreditCard = 1,
    CreditLimit = 2,
    Deposited = 3

}
public enum PaymentGatewaySource
{
    HDFC = 1,
    AMEX = 2,
    ICICI = 3,
    OXICASH = 4,
    APICustomer = 5,
    SBI = 6,    
    CCAvenue = 7,
    Beam = 9,
    TicketVala = 10,
    Axis = 11,
    CCAv=12,
    CozmoPG = 13,
    NEOPG=14,
    KNPay = 15,
    /// <summary>
    /// This is payment gateway for SafexPay 
    /// added by Somasekhar on 27/06/2018
    /// </summary>
    SafexPay = 16,
    /// <summary>
    /// This is payment gateway for PayFort 
    /// added by Somasekhar on 10/11/2018
    /// </summary>
    PayFort = 17,
    /// <summary>
    /// RazorPay PG for India (INR)
    /// </summary>
    RazorPay = 18
}
