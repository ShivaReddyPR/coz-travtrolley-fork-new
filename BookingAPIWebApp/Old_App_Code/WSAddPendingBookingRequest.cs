using System;
using CT.BookingEngine.WhiteLabel;
using CT.Configuration;

/// <summary>
/// Summary description for WSAddPendingBookingRequest
/// </summary>
public class WSAddPendingBookingRequest
{

    # region Private Fields
    int queueId;
    PaymentGatewaySource paySource;
    string paymentId;
    string orderId;    
    decimal paymentAmount;    
    bool isDomesticReturn;
    int bookingIdOut;
    int bookingIdIn;
    int agentMasterId;
    string airlineCodeOut;
    string airlineCodeIn;    
    string sector;
    string flightNumberOut;
    string flightNumberIn;    
    string passengerInfo;
    DateTime depDate;
    DateTime returnDate;    
    WLBookingStatus bookingStatus;
    bool inTicketed;
    bool outTicketed;
    string inPNR;
    string outPNR;
    string remarks;
    string ipAddress;
    string email;
    string phone;
    bool isDomestic;
    PaymentStatus paymentStatus;
    DateTime createdOn;
    int createdBy;
    DateTime lastModifiedOn;
    int lastModifiedBy;    
    # endregion

    # region Public Properties

    public int QueueId
    {
        get
        {
            return queueId;
        }
        set
        {
            queueId = value;
        }
    }

    public int BookingIdOut
    {
        get
        {
            return bookingIdOut;
        }
        set
        {
            bookingIdOut = value;
        }
    }

    public int BookingIdIn
    {
        get
        {
            return bookingIdIn;
        }
        set
        {
            bookingIdIn = value;
        }
    }

    public string OutPNR
    {
        get
        {
            return outPNR;
        }
        set
        {
            outPNR = value;
        }
    }

    public string InPNR
    {
        get
        {
            return inPNR;
        }
        set
        {
            inPNR = value;
        }
    }

    public DateTime CreatedOn
    {
        get
        {
            return createdOn;
        }
        set
        {
            createdOn = value;
        }
    }

    public string PaymentId
    {
        get
        {
            return paymentId;
        }
        set
        {
            paymentId = value;
        }
    }

    public PaymentGatewaySource PaySource
    {
        get
        {
            return paySource;
        }
        set
        {
            paySource = value;
        }
    }

    public string Sector
    {
        get
        {
            return sector;
        }
        set
        {
            sector = value;
        }
    }

    public string PassengerInfo
    {
        get
        {
            return passengerInfo;
        }
        set
        {
            passengerInfo = value;
        }
    }

    public string IPAddress
    {
        get
        {
            return ipAddress;
        }
        set
        {
            ipAddress = value;
        }
    }

    public string Email
    {
        get
        {
            return email;
        }
        set
        {
            email = value;
        }
    }

    public string Phone
    {
        get
        {
            return phone;
        }
        set
        {
            phone = value;
        }
    }

    //public CustomerStatus CustomerStatus
    //{
    //    get
    //    {
    //        return customerStatus;
    //    }
    //    set
    //    {
    //        customerStatus = value;
    //    }
    //}

    public WLBookingStatus BookingStatus
    {
        get
        {
            return bookingStatus;
        }
        set
        {
            bookingStatus = value;
        }
    }

    //public IPAddressStatus Status
    //{
    //    get
    //    {
    //        return status;
    //    }
    //    set
    //    {
    //        status = value;
    //    }
    //}

    public bool IsDomesticReturn
    {
        get
        {
            return isDomesticReturn;
        }
        set
        {
            isDomesticReturn = value;
        }
    }

    public decimal PaymentAmount
    {
        set
        {
            paymentAmount = value;
        }
        get
        {
            return paymentAmount;
        }
    }

    public bool OutTicketed
    {
        set
        {
            outTicketed = value;
        }
        get
        {
            return outTicketed;
        }
    }

    public bool InTicketed
    {
        set
        {
            inTicketed = value;
        }
        get
        {
            return inTicketed;
        }
    }

    public int AgentMasterId
    {
        set
        {
            agentMasterId = value;
        }
        get
        {
            return agentMasterId;
        }
    }

    public string AirlineCodeOut
    {
        set
        {
            airlineCodeOut = value;
        }
        get
        {
            return airlineCodeOut;
        }
    }

    public string AirlineCodeIn
    {
        set
        {
            airlineCodeIn = value;
        }
        get
        {
            return airlineCodeIn;
        }
    }

    public string FlightNumberOut
    {
        set
        {
            flightNumberOut = value;
        }
        get
        {
            return flightNumberOut;
        }
    }

    public string FlightNumberIn
    {
        set
        {
            flightNumberIn = value;
        }
        get
        {
            return flightNumberIn;
        }
    }

    public DateTime DepDate
    {
        set
        {
            depDate = value;
        }
        get
        {
            return depDate;
        }
    }

    public DateTime ReturnDate
    {
        set
        {
            returnDate = value;
        }
        get
        {
            return returnDate;
        }
    }

    public string Remarks
    {
        set
        {
            remarks = value;
        }
        get
        {
            return remarks;
        }
    }

    public DateTime LastModifiedOn
    {
        set
        {
            lastModifiedOn = value;
        }
        get
        {
            return lastModifiedOn;
        }
    }

    public int CreatedBy
    {
        set
        {
            createdBy = value;
        }
        get
        {
            return createdBy;
        }
    }

    public int LastModifiedBy
    {
        set
        {
            lastModifiedBy = value;
        }
        get
        {
            return lastModifiedBy;
        }
    }

    public bool IsDomestic
    {
        set
        {
            isDomestic = value;
        }
        get
        {
            return isDomestic;
        }
    }

    public PaymentStatus PaymentStatus
    {
        set
        {
            paymentStatus = value;
        }
        get
        {
            return paymentStatus;
        }
    }

    public string OrderId
    {
        set
        {
            orderId = value;
        }
        get
        {
            return orderId;
        }
    }

    #endregion

    public WSAddPendingBookingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    # region Methods
    public WSAddPendingBookingResponse Add(int UserMasterId, int AgentMasterId)
    {
        if(AgentMasterId <= 0)
        {
            ConfigurationSystem config = new ConfigurationSystem();
            AgentMasterId = Convert.ToInt32(config.GetSelfAgencyId()["selfAgentMasterId"]);
        }

        WLPendingQueue itPendingQueue = new WLPendingQueue();
        itPendingQueue.AgencyId = AgentMasterId;
        itPendingQueue.AirlineCodeIn = airlineCodeIn;
        itPendingQueue.AirlineCodeOut = airlineCodeOut;
        itPendingQueue.BookingIdIn = Convert.ToInt32(bookingIdIn);
        itPendingQueue.BookingIdOut = Convert.ToInt32(bookingIdOut);
        itPendingQueue.BookingStatus = bookingStatus;
        itPendingQueue.CreatedBy = UserMasterId;
        itPendingQueue.DepDate = depDate;
        itPendingQueue.Email = email;
        itPendingQueue.FlightNumberIn = flightNumberIn;
        itPendingQueue.FlightNumberOut = flightNumberOut;
        itPendingQueue.InPNR = inPNR;
        itPendingQueue.InTicketed = inTicketed;
        itPendingQueue.IPAddress = ipAddress;
        itPendingQueue.IsDomestic = isDomestic;
        itPendingQueue.IsDomesticReturn = isDomesticReturn;
        itPendingQueue.LastModifiedBy = UserMasterId;
        itPendingQueue.OrderId = orderId;
        itPendingQueue.OutPNR = outPNR;
        itPendingQueue.OutTicketed = outTicketed;
        itPendingQueue.PassengerInfo = passengerInfo;
        itPendingQueue.PaymentAmount = paymentAmount;
        itPendingQueue.PaymentId = paymentId;
        itPendingQueue.PaymentStatus = (PaymentStatus)Enum.Parse(typeof(PaymentStatus), Convert.ToString(paymentStatus));  
        itPendingQueue.PaySource = (CT.BookingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.BookingEngine.PaymentGatewaySource), Convert.ToString(paySource));        
        itPendingQueue.Phone = phone;
        itPendingQueue.Remarks = remarks;
        itPendingQueue.ReturnDate = returnDate;
        itPendingQueue.Sector = sector;
        int referenceId = itPendingQueue.Save();

        WSAddPendingBookingResponse saveResponse = new WSAddPendingBookingResponse();
        saveResponse.Status = new WSStatus();
        saveResponse.Status.Category = "SPD";
        if (referenceId > 0)
        {
            saveResponse.Status.Description = "Details Saved";
            saveResponse.Status.StatusCode = "1";
            saveResponse.QueueId = referenceId.ToString();
        }
        else
        {
            saveResponse.Status.Description = "Details Saved Failed";
            saveResponse.Status.StatusCode = "2";
        }
        return saveResponse;
    }
    # endregion
}
