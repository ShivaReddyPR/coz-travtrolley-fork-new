using CT.BookingEngine;
/// <summary>
/// Summary description for WSBookResponse
/// </summary>
/// 
//public enum BookingResponseStatus
//{
//    Successful = 1,
//    Failed = 2,
//    OtherFare = 3,
//    OtherClass = 4,
//    BookedOther = 5
//}
//public enum ProductType
//{
//    Flight = 1,
//    Hotel = 2,
//    Car = 3
//}



public class WSBookResponse
{
    private string pnr;
    private string bookingId;
    private WSStatus status;
    private bool ssrDenied;
    private string ssrMessage;
    private ProductType prodType;
    private string confirmationNo;
    //To be used for Saving Promo Detail AddBookingDetail from FlightBooking page
    private int itineraryId;

    public string PNR
    {
        get
        {
            return pnr;
        }
        set
        {
            pnr = value;
        }
    }

    public string BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }


    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public bool SSRDenied
    {
        get
        {
            return ssrDenied;
        }
        set
        {
            ssrDenied = value;
        }
    }

    public string SSRMessage
    {
        get
        {
            return ssrMessage;
        }
        set
        {
            ssrMessage = value;
        }
    }

    public ProductType ProdType
    {
        get
        {
            return prodType;
        }
        set
        {
            prodType = value;
        }
    }

    public string ConfirmationNo
    {
        get
        {
            return confirmationNo;
        }
        set
        {
            confirmationNo = value;
        }
    }

    /// <summary>
    /// ItineraryId to be used for saving PromoDetail in AddBookingDetail() from FlightBooking page
    /// </summary>
    public int ItineraryID
    {
        get { return itineraryId; }
        set { itineraryId = value; }
    }


    public WSBookResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
