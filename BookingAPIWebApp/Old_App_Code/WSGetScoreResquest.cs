using System;
using System.Collections.Generic;
using System.Configuration;
using CT.BookingEngine.WhiteLabel;
using CT.Core;


/// <summary>
/// Summary description for WSGetScoreResponse
/// </summary>
public class WSGetScoreRequest
{
    public WSGetScoreRequest()
    {
        
    }
    private string email;    
    public string Email
    {
        get
        {
            return email;
        }
        set
        {
            email = value;
        }
    }    
    private string ipAddress;
    public string IPAddress
    {
        get
        {
            return ipAddress;
        }
        set
        {
            ipAddress = value;
        }
    }
    private string phone;
    public string Phone
    {
        get
        {
            return phone;
        }
        set
        {
            phone = value;
        }
    }
    private WSSegment[] segment;
    public WSSegment[] Segment
    {
        get { return segment; }
        set { segment = value; }
    }
    private WSPassenger[] passenger;
    public WSPassenger[] Passenger
    {
        get { return passenger; }
        set { passenger = value; }
    }
    private WSPaymentInformation paymentInformation;
    public WSPaymentInformation PaymentInformation
    {
        get { return paymentInformation; }
        set { paymentInformation = value; }
    }
    public static WSGetScoreResponse GetStatus(WSGetScoreRequest request, CT.TicketReceipt.BusinessLayer.UserMaster UserMaster)
    {
        Dictionary<string, string> prefList = new Dictionary<string, string>();
        WSStatus status = new WSStatus();
        WSGetScoreResponse response = new WSGetScoreResponse();
        decimal bookingAmount = request.PaymentInformation.Amount;
        int perDayLimitOfIp = 0;
        int perWeekLimitOfIP = 0;
        int perMonthLimitOfIp = 0;
        int perDayLimitOfEmail = 0;
        int perWeekLimitOfEmail = 0;
        int perMonthLimitOfEmail = 0;
        decimal suspiciousLimitOfIP = 0;
        decimal suspiciousLimitOfEmail = 0;
        decimal amountLimit = 0;
        IPAddressDetails ipAddressDetail;
        status.Category = "GS";
        CustomerStatus customerStatus;
        try
        {
            prefList = UserPreference.GetPreferenceList((int)UserMaster.ID, ItemType.Flight);
            ipAddressDetail = new IPAddressDetails(request.ipAddress);
            customerStatus = Customer.GetCustomerStatus(request.Email);
        }
        catch (Exception ex)
        {
            status.Description = "Technical fault";
            status.StatusCode = "03";
            response.Status = status;
            response.ScoreStatus = ScoreStaus.Genuine;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, "Load preference / IP status failed " + ex.ToString() + " Stack Trace = " + ex.StackTrace, "");
            return response;
        }
        if (prefList != null && prefList.ContainsKey(WLPreferenceKeys.BookingAmountLimit))
        {
            amountLimit = Convert.ToDecimal(prefList[WLPreferenceKeys.BookingAmountLimit]);
        }
        string remark = "Ticketing blocked because ";
        if (ipAddressDetail.IPAddressStatus == IPAddressStatus.Blocked)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            status.Description = "IP blocked " + request.IPAddress;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + "IP Blocked " + request.IPAddress, request.IPAddress);
            SendScoreStatus(request, response, status);
            return response;
        }
        else if (customerStatus == CustomerStatus.Blocked)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            status.Description = "Customer blocked " + request.Email;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + "Customer Blocked " + request.Email, request.IPAddress);
            SendScoreStatus(request, response, status);
            return response;
        }
        else if (customerStatus == CustomerStatus.Suspicious && ipAddressDetail.IPAddressStatus == IPAddressStatus.Suspicious)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            status.Description = "Customer Suspicious " + request.Email + " IP suspicious " + request.IPAddress;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);

            SendScoreStatus(request, response, status);

            return response;
        }
        else if (customerStatus == CustomerStatus.Suspicious && amountLimit > 0 && bookingAmount > amountLimit)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            status.Description = "Customer Suspicious " + request.Email + " Amount above normal limit " + bookingAmount.ToString();
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);

            SendScoreStatus(request, response, status);
            return response;
        }
        else if (ipAddressDetail.IPAddressStatus == IPAddressStatus.Suspicious && amountLimit > 0 && bookingAmount > amountLimit)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            status.Description = "IP Suspicious " + request.IPAddress + " Amount above normal limit " + bookingAmount.ToString();
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);

            SendScoreStatus(request, response, status);
            return response;
        }

        if (prefList != null)
        {
            if (prefList.ContainsKey(WLPreferenceKeys.BookingsDayForIP))
            {
                perDayLimitOfIp = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsDayForIP]);
                Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, (int)UserMaster.ID, "Per day limit of IP " + perDayLimitOfIp.ToString(), "");
            }
            if (prefList.ContainsKey(WLPreferenceKeys.BookingsWeekForIP))
            {
                perWeekLimitOfIP = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsWeekForIP]);
            }
            if (prefList.ContainsKey(WLPreferenceKeys.BookingsMonthForIP))
            {
                perMonthLimitOfIp = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsMonthForIP]);
            }
            if (prefList.ContainsKey(WLPreferenceKeys.BookingsDayForEmail))
            {
                perDayLimitOfEmail = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsDayForEmail]);
            }
            if (prefList.ContainsKey(WLPreferenceKeys.BookingsWeekForEmail))
            {
                perWeekLimitOfEmail = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsWeekForEmail]);
            }
            if (prefList.ContainsKey(WLPreferenceKeys.BookingsMonthForEmail))
            {
                perMonthLimitOfEmail = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsMonthForEmail]);
            }
            if (prefList.ContainsKey(WLPreferenceKeys.SuspiciousLimitForIP))
            {
                suspiciousLimitOfIP = Convert.ToDecimal(prefList[WLPreferenceKeys.SuspiciousLimitForIP]);
            }
            if (prefList.ContainsKey(WLPreferenceKeys.SuspiciousLimitForEmail))
            {
                suspiciousLimitOfEmail = Convert.ToDecimal(prefList[WLPreferenceKeys.SuspiciousLimitForEmail]);
            }
        }
        ipAddressDetail = new IPAddressDetails();
        Customer customer = new Customer();
        if (perDayLimitOfIp > 0 && WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 1) >= perDayLimitOfIp)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Blocked, (int)UserMaster.ID);
            status.Description = "IP Blocked " + request.IPAddress + " . Per day limit crossed by IP. Booking limit = " + perDayLimitOfIp;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);

            SendScoreStatus(request, response, status);
            return response;
        }
        else if (perWeekLimitOfIP > 0 && WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 7) >= perWeekLimitOfIP)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Blocked, (int)UserMaster.ID);
            status.Description = "IP Blocked " + request.IPAddress + " . Per week limit crossed by IP weekly Booking limit = " + perWeekLimitOfIP;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
            SendScoreStatus(request, response, status);
            return response;
        }
        else if (perMonthLimitOfIp > 0 && WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 30) >= perMonthLimitOfIp)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Blocked, (int)UserMaster.ID);
            status.Description = "IP Blocked " + request.IPAddress + " . Per month limit crossed by IP monthly booking limit = " + perMonthLimitOfIp;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
            SendScoreStatus(request, response, status);
            return response;
        }
        else if (perDayLimitOfEmail > 0 && WLBookingDetail.GetBookingCountFromEmail(request.Email, 1) >= perDayLimitOfEmail)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Blocked, "Per day limit crossed by user");
            status.Description = "Customer Blocked " + request.Email + " . Per day limit crossed by user. Booking limit = " + perDayLimitOfEmail;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
            SendScoreStatus(request, response, status);
            return response;
        }
        else if (perWeekLimitOfEmail > 0 && WLBookingDetail.GetBookingCountFromEmail(request.Email, 7) >= perWeekLimitOfEmail)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Blocked, "Per day limit crossed by user");
            status.Description = "Customer Blocked " + request.Email + " . Per week limit crossed by user weekly Booking limit = " + perWeekLimitOfEmail;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
            SendScoreStatus(request, response, status);
            return response;
        }
        else if (perMonthLimitOfEmail > 0 && WLBookingDetail.GetBookingCountFromEmail(request.Email, 30) >= perMonthLimitOfEmail)
        {
            response.ScoreStatus = ScoreStaus.Dangerous;
            Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Blocked, "Per day limit crossed by user");
            status.Description = "IP Blocked " + request.IPAddress + " . Per month limit crossed by IP monthly booking limit = " + perMonthLimitOfEmail;
            status.StatusCode = "02";
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
            SendScoreStatus(request, response, status);
            return response;
        }
        int perDayBookingFromIP = 0;
        int perWeekBookingFromIP = 0;
        int perMonthBookingFromIP = 0;
        int perDayBookingFromUser = 0;
        int perWeekBookingFromUser = 0;
        int perMonthBookingFromUser = 0;
        bool isIPSuspicious = false;
        bool isUserSuspicious = false;
        string remarksForIP = string.Empty;
        string remarksForUser = string.Empty;
        perDayBookingFromIP = WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 1);
        perWeekBookingFromIP = WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 7);
        perMonthBookingFromIP = WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 30);
        perDayBookingFromUser = WLBookingDetail.GetBookingCountFromEmail(request.Email, 1);
        perWeekBookingFromUser = WLBookingDetail.GetBookingCountFromEmail(request.Email, 7);
        perMonthBookingFromUser = WLBookingDetail.GetBookingCountFromEmail(request.Email, 30);
        ipAddressDetail = new IPAddressDetails();
        if (suspiciousLimitOfIP > 0 && perDayBookingFromIP >= Math.Round(perDayLimitOfIp * suspiciousLimitOfIP / 100))
        {
            isIPSuspicious = true;
            remarksForIP = "IP crossed per day suspicious limit.No of booking by IP " + perDayBookingFromIP.ToString();
            ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Suspicious, (int)UserMaster.ID);
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForIP, request.IPAddress);
        }
        else if (suspiciousLimitOfIP > 0 && perWeekBookingFromIP >= Math.Round(perWeekLimitOfIP * suspiciousLimitOfIP / 100))
        {
            isIPSuspicious = true;
            remarksForIP = "IP crossed per week suspicious limit. No of booking by IP " + perWeekBookingFromIP.ToString();
            ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Suspicious, (int)UserMaster.ID);
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForIP, request.IPAddress);
        }
        else if (perMonthBookingFromIP > 0 && perMonthBookingFromIP >= Math.Round(perMonthLimitOfIp * suspiciousLimitOfIP / 100))
        {
            isIPSuspicious = true;
            remarksForIP = "IP crossed per week suspicious limit. No of booking by IP " + perMonthBookingFromIP.ToString();
            ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Suspicious, (int)UserMaster.ID);
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForIP, request.IPAddress);
        }
        if (suspiciousLimitOfEmail > 0 && perDayBookingFromUser >= Math.Round(perDayLimitOfEmail * suspiciousLimitOfEmail / 100))
        {
            isUserSuspicious = true;
            remarksForUser = "User crossed per day suspicious limit. No of booking by IP " + perDayBookingFromUser.ToString();
            Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Suspicious, "Per day suspicious limit crossed by user");
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForUser, request.IPAddress);
        }
        else if (suspiciousLimitOfEmail > 0 && perWeekBookingFromUser >= Math.Round(perWeekLimitOfEmail * suspiciousLimitOfEmail / 100))
        {
            isUserSuspicious = true;
            remarksForUser = "User crossed per week suspicious limit. No of booking by IP " + perWeekBookingFromUser.ToString();
            Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Suspicious, "Per week suspicious limit crossed by user");
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForUser, request.IPAddress);
        }
        else if (suspiciousLimitOfEmail > 0 && perMonthBookingFromUser >= Math.Round(perMonthLimitOfEmail * suspiciousLimitOfEmail / 100))
        {
            isUserSuspicious = true;
            remarksForUser = "User crossed per week suspicious limit. No of booking by IP " + perMonthBookingFromUser.ToString();
            Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Suspicious, "Per month suspicious limit crossed by user");
            Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForUser, request.IPAddress);
        }

        if (isIPSuspicious)
        {
            response.ScoreStatus = ScoreStaus.Suspicious;
            status.Description = "IP Suspicious " + request.IPAddress + " " + remarksForIP;
            status.StatusCode = "02";
            response.Status = status;
            return response;
        }
        else if (isUserSuspicious)
        {
            response.ScoreStatus = ScoreStaus.Suspicious;
            status.Description = "User Suspicious " + request.Email + " " + remarksForUser;
            status.StatusCode = "02";
            response.Status = status;
            return response;
        }
        else
        {
            Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Genuine, "Per week suspicious limit crossed by user");
            ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Normal, (int)UserMaster.ID);
            response.ScoreStatus = ScoreStaus.Genuine;
            status.Description = "Both user  and IP are normal";
            status.StatusCode = "02";
            response.Status = status;
            return response;
        }
    }

    static void SendScoreStatus(WSGetScoreRequest request, WSGetScoreResponse response, WSStatus status)
    {
        try
        {
            List<string> toArray = new List<string>();
            CT.Configuration.ConfigurationSystem con = new CT.Configuration.ConfigurationSystem();
            System.Collections.Hashtable hostPort = con.GetHostPort();
            toArray.Add(hostPort["ErrorNotificationMailingId"].ToString());
            string message = "<p>Dear Team,</p><p>Ticketing was blocked for the Suspicious activity. Below are the details of Booking.</p> <p><table><tr><td>PNR : </td><td>" + request.Phone.Split(',')[1] + "</td></tr><tr><td>Supicious Email : </td><td>" + request.Email + "</td></tr>";
            foreach (WSPassenger pax in request.Passenger)
            {
                message += "<tr><td>Passenger Name : </td><td>" + pax.FirstName + " " + pax.LastName + "</td></tr>";
            }
            message += "<tr><td>Contact No : </td><td>" + request.Phone.Split(',')[0] + "</td></tr><tr><td>Suspicious IP : </td><td>" + request.IPAddress;
            message += "</td></tr><tr><td>Payment Amount : </td><td>" + Math.Round(request.PaymentInformation.Amount, 2) + "</td></tr><tr><td>Payment ID : </td><td>";
            message += request.PaymentInformation.PaymentId + "</td></tr><tr><td colspan='2'></td></tr><tr><td colspan='2'>Regards,</td></tr><tr><td colspan='2'>CozmoTravel Admin</td></tr></table><p>";

            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, status.Description + ". PNR : " + request.Phone.Split(',')[1], message, new System.Collections.Hashtable());
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.GetIPAddressStatus, Severity.High, 1, "Failed to Send Suspicious booking email. Reason : " + ex.ToString(), request.IPAddress);
        }
    }
    //public static WSGetScoreResponse GetStatus(WSGetScoreRequest request, CT.TicketReceipt.BusinessLayer.UserMaster UserMaster)
    //{        
    //    Dictionary<string, string> prefList = new Dictionary<string, string>();
    //    WSStatus status = new WSStatus();
    //    WSGetScoreResponse response = new WSGetScoreResponse();        
    //    decimal bookingAmount = request.PaymentInformation.Amount;
    //    int perDayLimitOfIp = 0;
    //    int perWeekLimitOfIP = 0;
    //    int perMonthLimitOfIp = 0;        
    //    int perDayLimitOfEmail = 0;
    //    int perWeekLimitOfEmail = 0;
    //    int perMonthLimitOfEmail = 0;
    //    decimal suspiciousLimitOfIP = 0;
    //    decimal suspiciousLimitOfEmail = 0;
    //    decimal amountLimit = 0;
    //    IPAddressDetails ipAddressDetail;
    //    status.Category = "GS";
    //    CustomerStatus customerStatus;
    //    try
    //    {
    //        prefList = UserPreference.GetPreferenceList((int)UserMaster.ID, ItemType.Flight);
    //        ipAddressDetail = new IPAddressDetails(request.ipAddress);            
    //        customerStatus = Customer.GetCustomerStatus(request.Email);            
    //    }
    //    catch (Exception ex)
    //    {            
    //        status.Description = "Technical fault";
    //        status.StatusCode = "03";
    //        response.Status = status;
    //        response.ScoreStatus = ScoreStaus.Genuine;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, "Load preference / IP status failed " + ex.Message + " Stack Trace = " + ex.StackTrace, "");
    //        return response;
    //    }
    //    if (prefList != null && prefList.ContainsKey(WLPreferenceKeys.BookingAmountLimit))
    //    {
    //        amountLimit = Convert.ToDecimal(prefList[WLPreferenceKeys.BookingAmountLimit]);
    //    }
    //    string remark = "Ticketing blocked because ";
    //    if (ipAddressDetail.IPAddressStatus == IPAddressStatus.Blocked)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        status.Description = "IP blocked " + request.IPAddress;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + "IP Blocked " + request.IPAddress, request.IPAddress);
    //        return response;
    //    }
    //    else if (customerStatus == CustomerStatus.Blocked)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        status.Description = "Customer blocked " + request.Email;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + "Customer Blocked " + request.Email, request.IPAddress);
    //        return response;
    //    }
    //    else if (customerStatus == CustomerStatus.Suspicious && ipAddressDetail.IPAddressStatus == IPAddressStatus.Suspicious)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        status.Description = "Customer Suspicious " + request.Email + " IP suspicious "+ request.IPAddress;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }
    //    else if (customerStatus == CustomerStatus.Suspicious && amountLimit > 0 && bookingAmount > amountLimit)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        status.Description = "Customer Suspicious " + request.Email + " Amount above normal limit "+ bookingAmount.ToString();
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }
    //    else if (ipAddressDetail.IPAddressStatus == IPAddressStatus.Suspicious && amountLimit > 0 && bookingAmount > amountLimit)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        status.Description = "IP Suspicious " + request.IPAddress + " Amount above normal limit "+ bookingAmount.ToString();
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }

    //    if (prefList != null)
    //    {
    //        if (prefList.ContainsKey(WLPreferenceKeys.BookingsDayForIP))
    //        {
    //            perDayLimitOfIp = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsDayForIP]);
    //            Audit.AddAuditBookingAPI(EventType.AddAPIBookingDetail, Severity.High, (int)UserMaster.ID, "Per day limit of IP " + perDayLimitOfIp.ToString(), "");
    //        }
    //        if (prefList.ContainsKey(WLPreferenceKeys.BookingsWeekForIP))
    //        {
    //            perWeekLimitOfIP = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsWeekForIP]);
    //        }
    //        if (prefList.ContainsKey(WLPreferenceKeys.BookingsMonthForIP))
    //        {
    //            perMonthLimitOfIp = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsMonthForIP]);
    //        }
    //        if (prefList.ContainsKey(WLPreferenceKeys.BookingsDayForEmail))
    //        {
    //            perDayLimitOfEmail = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsDayForEmail]);
    //        }
    //        if (prefList.ContainsKey(WLPreferenceKeys.BookingsWeekForEmail))
    //        {
    //            perWeekLimitOfEmail = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsWeekForEmail]);
    //        }
    //        if (prefList.ContainsKey(WLPreferenceKeys.BookingsMonthForEmail))
    //        {
    //            perMonthLimitOfEmail = Convert.ToInt32(prefList[WLPreferenceKeys.BookingsMonthForEmail]);
    //        }
    //        if (prefList.ContainsKey(WLPreferenceKeys.SuspiciousLimitForIP))
    //        {
    //            suspiciousLimitOfIP = Convert.ToDecimal(prefList[WLPreferenceKeys.SuspiciousLimitForIP]);
    //        }
    //        if (prefList.ContainsKey(WLPreferenceKeys.SuspiciousLimitForEmail))
    //        {
    //            suspiciousLimitOfEmail = Convert.ToDecimal(prefList[WLPreferenceKeys.SuspiciousLimitForEmail]);
    //        }
    //    }
    //    ipAddressDetail = new IPAddressDetails();
    //    Customer customer = new Customer();
    //    if (perDayLimitOfIp > 0 && WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 1) >= perDayLimitOfIp)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;            
    //        ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Blocked, (int)UserMaster.ID);
    //        status.Description = "IP Blocked " + request.IPAddress + " . Per day limit crossed by IP. Booking limit = " + perDayLimitOfIp;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }
    //    else if (perWeekLimitOfIP > 0 && WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 7) >= perWeekLimitOfIP)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Blocked, (int)UserMaster.ID);
    //        status.Description = "IP Blocked " + request.IPAddress + " . Per week limit crossed by IP weekly Booking limit = " + perWeekLimitOfIP;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }
    //    else if (perMonthLimitOfIp > 0 && WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 30) >= perMonthLimitOfIp)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Blocked, (int)UserMaster.ID);
    //        status.Description = "IP Blocked " + request.IPAddress + " . Per month limit crossed by IP monthly booking limit = " + perMonthLimitOfIp;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }
    //    else if (perDayLimitOfEmail > 0 && WLBookingDetail.GetBookingCountFromEmail(request.Email, 1) >= perDayLimitOfEmail)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        Customer.UpdateCustomerStatus(request.Email,CustomerStatus.Blocked,"Per day limit crossed by user");            
    //        status.Description = "Customer Blocked " + request.Email + " . Per day limit crossed by user. Booking limit = " + perDayLimitOfEmail;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }
    //    else if (perWeekLimitOfEmail > 0 && WLBookingDetail.GetBookingCountFromEmail(request.Email, 7) >= perWeekLimitOfEmail)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Blocked, "Per day limit crossed by user"); 
    //        status.Description = "Customer Blocked " + request.Email + " . Per week limit crossed by user weekly Booking limit = " + perWeekLimitOfEmail;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }
    //    else if (perMonthLimitOfEmail > 0 && WLBookingDetail.GetBookingCountFromEmail(request.Email, 30) >= perMonthLimitOfEmail)
    //    {
    //        response.ScoreStatus = ScoreStaus.Dangerous;
    //        Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Blocked, "Per day limit crossed by user"); 
    //        status.Description = "IP Blocked " + request.IPAddress + " . Per month limit crossed by IP monthly booking limit = " + perMonthLimitOfEmail;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remark + status.Description, request.IPAddress);
    //        return response;
    //    }
    //    int perDayBookingFromIP = 0;
    //    int perWeekBookingFromIP = 0;
    //    int perMonthBookingFromIP = 0;
    //    int perDayBookingFromUser = 0;
    //    int perWeekBookingFromUser = 0;
    //    int perMonthBookingFromUser = 0;
    //    bool isIPSuspicious = false;
    //    bool isUserSuspicious = false;
    //    string remarksForIP = string.Empty;
    //    string remarksForUser = string.Empty;
    //    perDayBookingFromIP = WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 1);
    //    perWeekBookingFromIP = WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 7);
    //    perMonthBookingFromIP = WLBookingDetail.GetBookingCountFromIP(request.ipAddress, 30);
    //    perDayBookingFromUser = WLBookingDetail.GetBookingCountFromEmail(request.Email, 1);
    //    perWeekBookingFromUser = WLBookingDetail.GetBookingCountFromEmail(request.Email, 7);
    //    perMonthBookingFromUser = WLBookingDetail.GetBookingCountFromEmail(request.Email, 30);
    //    ipAddressDetail = new IPAddressDetails();
    //    if (suspiciousLimitOfIP > 0 && perDayBookingFromIP >= Math.Round(perDayLimitOfIp * suspiciousLimitOfIP / 100))
    //    {
    //        isIPSuspicious = true;
    //        remarksForIP = "IP crossed per day suspicious limit.No of booking by IP " + perDayBookingFromIP.ToString();
    //        ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Suspicious, (int)UserMaster.ID);
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForIP, request.IPAddress);
    //    }
    //    else if (suspiciousLimitOfIP > 0 && perWeekBookingFromIP >= Math.Round(perWeekLimitOfIP * suspiciousLimitOfIP / 100))
    //    {
    //        isIPSuspicious = true;
    //        remarksForIP = "IP crossed per week suspicious limit. No of booking by IP " + perWeekBookingFromIP.ToString();
    //        ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Suspicious, (int)UserMaster.ID);
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForIP, request.IPAddress);
    //    }
    //    else if (perMonthBookingFromIP > 0 && perMonthBookingFromIP >= Math.Round(perMonthLimitOfIp * suspiciousLimitOfIP / 100))
    //    {
    //        isIPSuspicious = true;
    //        remarksForIP = "IP crossed per week suspicious limit. No of booking by IP " + perMonthBookingFromIP.ToString();
    //        ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Suspicious, (int)UserMaster.ID);
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForIP, request.IPAddress);
    //    }
    //    if (suspiciousLimitOfEmail > 0 && perDayBookingFromUser >= Math.Round(perDayLimitOfEmail * suspiciousLimitOfEmail / 100))
    //    {
    //        isUserSuspicious = true;
    //        remarksForUser = "User crossed per day suspicious limit. No of booking by IP " + perDayBookingFromUser.ToString();
    //        Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Suspicious, "Per day suspicious limit crossed by user");
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForUser, request.IPAddress);
    //    }
    //    else if (suspiciousLimitOfEmail > 0 && perWeekBookingFromUser >= Math.Round(perWeekLimitOfEmail * suspiciousLimitOfEmail / 100))
    //    {
    //        isUserSuspicious = true;
    //        remarksForUser = "User crossed per week suspicious limit. No of booking by IP " + perWeekBookingFromUser.ToString();
    //        Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Suspicious, "Per week suspicious limit crossed by user");
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForUser, request.IPAddress);
    //    }
    //    else if (suspiciousLimitOfEmail > 0 && perMonthBookingFromUser >= Math.Round(perMonthLimitOfEmail * suspiciousLimitOfEmail / 100))
    //    {
    //        isUserSuspicious = true;
    //        remarksForUser = "User crossed per week suspicious limit. No of booking by IP " + perMonthBookingFromUser.ToString();
    //        Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Suspicious, "Per month suspicious limit crossed by user");
    //        Audit.AddAuditBookingAPI(EventType.Ticketing, Severity.High, (int)UserMaster.ID, remarksForUser, request.IPAddress);
    //    }

    //    if (isIPSuspicious)
    //    {            
    //        response.ScoreStatus = ScoreStaus.Suspicious;
    //        status.Description = "IP Suspicious " + request.IPAddress + " " + remarksForIP;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        return response;
    //    }
    //    else if (isUserSuspicious)
    //    {
    //        response.ScoreStatus = ScoreStaus.Suspicious;
    //        status.Description = "User Suspicious " + request.Email + " " + remarksForUser;
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        return response;
    //    }
    //    else
    //    {
    //        Customer.UpdateCustomerStatus(request.Email, CustomerStatus.Genuine, "Per week suspicious limit crossed by user");
    //        ipAddressDetail.UpdateIPAddressStatus(request.ipAddress.Trim(), IPAddressStatus.Normal, (int)UserMaster.ID);
    //        response.ScoreStatus = ScoreStaus.Genuine;
    //        status.Description = "Both user  and IP are normal";
    //        status.StatusCode = "02";
    //        response.Status = status;
    //        return response;
    //    }
    //}
}
