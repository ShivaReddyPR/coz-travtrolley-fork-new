/// <summary>
/// WsFareRuleRequestTo Get Fare Rule Description
/// </summary>
public class WSFareRuleRequest
{
    /// <summary>
    /// private property of fareRule 
    /// </summary>
    private WSFareRule[] fareRules;
    /// <summary>        
    /// public property of fareRule 
    /// </summary>        
    public WSFareRule[] FareRules
    {
        get
        {
            return fareRules;
        }
        set
        {
            fareRules = value;
        }
    }
    

    private string sessionId;

    public string SessionId
    {
        get
        {
            return sessionId;
        }

        set
        {
            this.sessionId = value;
        }
    }

    private int resultId;

    public int ResultId
    {
        get
        {
            return resultId;
        }

        set
        {
            this.resultId = value;
        }
    }
}

