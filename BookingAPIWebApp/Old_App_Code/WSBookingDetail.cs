using System;
using System.Diagnostics;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for WSBookingDetail
/// </summary>
public class WSBookingDetail
{
    private string bookingId;
    public string BookingId
    {
        get { return bookingId; }
        set { bookingId = value; }
    }

    private WSAgentMaster agentMaster;
    public WSAgentMaster AgentMaster
    {
        get { return agentMaster; }
        set { agentMaster = value; }
    }

    private string pnr;
    public string PNR
    {
        get { return pnr; }
        set { pnr = value; }
    }

    private string errorMsg;
    public string ErrorMsg
    {
        get { return errorMsg; }
        set { errorMsg = value; }
    }

    private string remarks;
    public string Remarks
    {
        get { return remarks; }
        set { remarks = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    private WSFare fare;
    public WSFare Fare
    {
        get { return fare; }
        set { fare = value; }
    }

    private WSPassenger[] passenger;
    public WSPassenger[] Passenger
    {
        get { return passenger; }
        set { passenger = value; }
    }

    private string origin;
    public string Origin
    {
        get { return origin; }
        set { origin = value; }
    }

    private string destination;
    public string Destination
    {
        get { return destination; }
        set { destination = value; }
    }

    private WSSegment[] segment;
    public WSSegment[] Segment
    {
        get { return segment; }
        set { segment = value; }
    }

    private string fareType;
    public string FareType
    {
        get { return fareType; }
        set { fareType = value; }
    }

    private string[] fareBasis;
    public string[] FareBasis
    {
        get { return fareBasis; }
        set { fareBasis = value; }
    }

    private WSTicket[] ticket;
    public WSTicket[] Ticket
    {
        get { return ticket; }
        set { ticket = value; }
    }

    private BookingSource source;
    public BookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    public WSBookingDetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void Load(string bookId)
    {
        Trace.TraceInformation("WSGetBookingResponse.Load entered. BookingId = " + bookId);
        //
        BookingDetail bookingDetail = new BookingDetail(Convert.ToInt32(bookId));
        FlightItinerary itinerary = new FlightItinerary(bookingDetail.ProductsList[0].ProductId);//TODO: Change when there will be more then one Product in the productList of bookingDetail
        //bookingId = bookingDetail.BookingId;
        pnr = itinerary.PNR;
        bookingId = bookId;
        WSStatus status = new WSStatus();
        status.Category = "RT";
        status.Description = bookingDetail.Status.ToString();
        //0 in this line is being added to make it a two digit number like 01,02,03 etc.
        status.StatusCode = "0" + Convert.ToString((int)bookingDetail.Status);
        //TODO: In case there is an additon of more status in BookingStatus please check that we don't need to add this 0 for that here as then it will cross 9 and become a two digit number by itself.
        origin = itinerary.Origin;
        destination = itinerary.Destination;
        fareBasis = new string[itinerary.FareRules.Count];
        for (int i = 0; i < itinerary.FareRules.Count; i++)
        {
            fareBasis[i] = itinerary.FareRules[i].FareBasisCode;
        }
        passenger = new WSPassenger[itinerary.Passenger.Length];
        WSFare fare = new WSFare();
        fare.Currency = itinerary.Passenger[0].Price.Currency;
        for (int i = 0; i < itinerary.Passenger.Length; i++)
        {
            passenger[i] = new WSPassenger();
            fare.BaseFare += itinerary.Passenger[i].Price.PublishedFare;
            fare.Tax += itinerary.Passenger[i].Price.Tax;
            passenger[i].AddressLine1 = itinerary.Passenger[i].AddressLine1;
            passenger[i].AddressLine2 = itinerary.Passenger[i].AddressLine2;
            passenger[i].Country = itinerary.Passenger[i].Country.CountryCode;
            passenger[i].DateOfBirth = itinerary.Passenger[i].DateOfBirth;
            passenger[i].Email = itinerary.Passenger[i].Email;
            WSFare paxFare = new WSFare();
            paxFare.BaseFare = itinerary.Passenger[i].Price.PublishedFare;
            paxFare.OtherCharges = itinerary.Passenger[i].Price.OtherCharges;
            paxFare.ServiceTax = itinerary.Passenger[i].Price.SeviceTax;
            paxFare.Tax = itinerary.Passenger[i].Price.Tax;
            paxFare.AirTransFee = itinerary.Passenger[i].Price.AirlineTransFee;
            paxFare.Currency = itinerary.Passenger[i].Price.Currency;
            passenger[i].Fare = paxFare;
            passenger[i].FFAirline = itinerary.Passenger[i].FFAirline;
            passenger[i].FFNumber = itinerary.Passenger[i].FFNumber;
            passenger[i].FirstName = itinerary.Passenger[i].FirstName;
            passenger[i].LastName = itinerary.Passenger[i].LastName;
            passenger[i].Title = itinerary.Passenger[i].Title;
            passenger[i].Meal = itinerary.Passenger[i].Meal;
            passenger[i].Seat = itinerary.Passenger[i].Seat;
            passenger[i].PassportNumber = itinerary.Passenger[i].PassportNo;
            passenger[i].Phone = itinerary.Passenger[i].CellPhone;
            passenger[i].Type = itinerary.Passenger[i].Type;
        }
        this.fare = fare;

        segment = new WSSegment[itinerary.Segments.Length];
        for (int i = 0; i < itinerary.Segments.Length; i++)
        {
            segment[i] = WSSegment.ReadSegment(itinerary.Segments[i]);
        }
        List<Ticket> ticketList = CT.BookingEngine.Ticket.GetTicketList(itinerary.FlightId);
        ticket = new WSTicket[ticketList.Count];
        for (int i = 0; i < ticketList.Count; i++)
        {
            ticket[i] = new WSTicket();
            //TODO Airline PNR to be moved to segment.
            //ticket[i].AirlinePNR = itinerary.Segments[i].air
            ticket[i].FirstName = ticketList[i].PaxFirstName;
            ticket[i].LastName = ticketList[i].PaxLastName;
            ticket[i].Title = ticketList[i].Title;
            ticket[i].IssueDate = ticketList[i].IssueDate;
            ticket[i].PaxType = ticketList[i].PaxType;
            ticketList[i].PtcDetail = SegmentPTCDetail.GetSegmentPTCDetail(itinerary.FlightId);
            if (ticketList[i].PtcDetail != null)
            {
                ticket[i].SegmentAdditionalInfo = new WSSegAdditionalInfo[ticketList[i].PtcDetail.Count];
                for (int j = 0; j < ticketList[i].PtcDetail.Count; j++)
                {
                    ticket[i].SegmentAdditionalInfo[j] = new WSSegAdditionalInfo();
                    ticket[i].SegmentAdditionalInfo[j].Baggage = ticketList[i].PtcDetail[j].Baggage;
                    ticket[i].SegmentAdditionalInfo[j].FareBasis = ticketList[i].PtcDetail[j].FareBasis;
                    ticket[i].SegmentAdditionalInfo[j].FlightKey = ticketList[i].PtcDetail[j].FlightKey;
                    ticket[i].SegmentAdditionalInfo[j].NVA = ticketList[i].PtcDetail[j].NVA;
                    ticket[i].SegmentAdditionalInfo[j].NVB = ticketList[i].PtcDetail[j].NVB;
                }
            }
            ticket[i].TicketId = ticketList[i].TicketId;
            ticket[i].TicketNumber = ticketList[i].TicketNumber;
            ticket[i].ValidatingAirline = ticketList[i].ValidatingAriline;
            WSFare wsFare = new WSFare();
            wsFare.BaseFare = ticketList[i].Price.PublishedFare;
            wsFare.Tax = ticketList[i].Price.Tax;
            wsFare.Currency = ticketList[i].Price.Currency;
            ticket[i].Fare = wsFare;
        }

        AgentMaster crAgentMaster = new AgentMaster(bookingDetail.AgencyId);
        
        AgentMaster = new WSAgentMaster();
        AgentMaster.AddressLine1 = crAgentMaster.Address;
        AgentMaster.AddressLine2 = "";
        AgentMaster.Email = crAgentMaster.Email1;
        AgentMaster.Name = crAgentMaster.Name;
        AgentMaster.Phone = crAgentMaster.Phone1;
        AgentMaster.Fax = crAgentMaster.Fax;
        AgentMaster.City = crAgentMaster.City;
        AgentMaster.PIN = crAgentMaster.POBox;

        fareType = itinerary.FareType;
        Source = itinerary.FlightBookingSource;

        Trace.TraceInformation("WSGetBookingResponse.Load exiting");
    }
}
