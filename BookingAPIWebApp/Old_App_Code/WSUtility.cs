using System;
using System.Collections.Generic;
using CT.BookingEngine;

/// <summary>
/// Summary description for Utiltiy
/// </summary>
public class WSUtility
{
    public WSUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static List<FareRule> GetFareRule(string[] farebasis)
    {
        List<FareRule> fareRuleList = new List<FareRule>();
        for (int i = 0; i < farebasis.Length; i++)
        {
            FareRule fareRule = new FareRule();
            fareRule.Airline = farebasis[i].Substring(0, 2);
            fareRule.Origin = farebasis[i].Substring(2, 3);
            fareRule.Destination = farebasis[i].Substring(5, 3);
            fareRule.FareBasisCode = farebasis[i].Substring(9);
            fareRuleList.Add(fareRule);
        }
        return fareRuleList;
    }

    /// <summary>
    /// Gets current date and time in IST.
    /// </summary>
    /// <returns>DateTime in IST.</returns>
    public static DateTime GetIST()
    {
        return DateTime.Now.ToUniversalTime() + new TimeSpan(4, 00, 0);
    }

    /// <summary>
    /// IST time convert into UTC time 
    /// </summary>
    /// <param name="dateTime"> datetime </param>
    /// <returns> UST time </returns>
    public static DateTime ISTToUTC(DateTime dateTime)
    {
        return dateTime - new TimeSpan(4, 00, 0);

    }

    /// <summary>
    /// UTC time convert into IST time
    /// </summary>
    /// <param name="dateTime"> UTC time that is to be converted in to IST time </param>
    /// <returns> IST time </returns>
    public static DateTime UTCtoIST(DateTime dateTime)
    {
        // UTC Time zone is (-00:00:00) & IST is (+05:30:00)
        //TimeSpan absoluteOffset = new TimeSpan(00, 00, 00) - new TimeSpan(05, 30, 00);
        //absoluteOffset = absoluteOffset.Duration();
        //DateTime ISTtime = DateTime.Parse(dateTime) + absoluteOffset;
        //return ISTtime;
        return dateTime + new TimeSpan(4, 00, 0);
    }
}

public struct WSAirline
{
    private string airlineCode;
    public string AirlineCode
    {
        get { return airlineCode; }
        set { airlineCode = value; }
    }

    private string airlineName;
    public string AirlineName
    {
        get { return airlineName; }
        set { airlineName = value; }
    }
}

public struct WSPTCFare
{
    private PassengerType passengerType;
    public PassengerType PassengerType
    {
        get { return passengerType; }
        set { passengerType = value; }
    }

    private int passengerCount;
    public int PassengerCount
    {
        get { return passengerCount; }
        set { passengerCount = value; }
    }

    private decimal baseFare;
    public decimal BaseFare
    {
        get { return baseFare; }
        set { baseFare = value; }
    }

    private decimal tax;
    public decimal Tax
    {
        get { return tax; }
        set { tax = value; }
    }
    private decimal airlineTransFee;
    public decimal AirlineTransFee
    {
        get { return airlineTransFee; }
        set { airlineTransFee = value; }
    }
    private decimal additionalTxnFee;
    public decimal AdditionalTxnFee
    {
        get { return additionalTxnFee; }
        set { additionalTxnFee = value; }
    }
    private decimal fuelSurcharge;
    public decimal FuelSurcharge
    {
        get { return fuelSurcharge; }
        set { fuelSurcharge = value; }
    }
    private decimal markup;
    public decimal Markup
    {
        get { return markup; }
        set { markup = value; }
    }
    private decimal b2cMarkup;
    public decimal B2CMarkup
    {
        get { return b2cMarkup; }
        set { b2cMarkup = value; }
    }

    private string supplierCurrency;
    public string SupplierCurrency
    {
        get { return supplierCurrency; }
        set { supplierCurrency = value; }
    }
    private decimal supplierPrice;
    public decimal SupplierPrice
    {
        get { return supplierPrice; }
        set { supplierPrice = value; }
    }

    public Fare GetPTCFare()
    {
        Fare fare = new Fare();
        fare.BaseFare = Convert.ToDouble(baseFare);
        fare.PassengerCount = passengerCount;
        fare.PassengerType = passengerType;
        fare.TotalFare = Convert.ToDouble(baseFare + tax);
        fare.AirlineTransFee = airlineTransFee;
        fare.AdditionalTxnFee = additionalTxnFee;
        fare.AgentMarkup = markup;
        fare.B2CMarkup = b2cMarkup;
        return fare;
    }
}

public class WSFare
{
    private decimal baseFare;
    public decimal BaseFare
    {
        get { return baseFare; }
        set { baseFare = value; }
    }

    private decimal tax;
    public decimal Tax
    {
        get { return tax; }
        set { tax = value; }
    }

    private decimal serviceTax;
    public decimal ServiceTax
    {
        get { return serviceTax; }
        set { serviceTax = value; }
    }  
    /// <summary>
    /// additional txn fee added on 03 july 2008
    /// </summary>
    private decimal additionalTxnFee;
    public decimal AdditionalTxnFee
    {
        get { return additionalTxnFee; }
        set { additionalTxnFee = value; }
    }

    private decimal agentCommission;
    public decimal AgentCommission
    {
        get { return agentCommission; }
        set { agentCommission = value; }
    }

    private decimal publishedPrice;
    public decimal PublishedPrice
    {
        get { return publishedPrice; }
        set { publishedPrice = value; }
    }

    private decimal airTransFee;
    public decimal AirTransFee
    {
        get { return airTransFee; }
        set { airTransFee = value; }
    }
                
    private string currency;
    public string Currency
    {
        get { return currency; }
        set { currency = value; }
    }

    private decimal discount;
    public decimal Discount
    {
        get { return discount; }
        set { discount = value; }
    }
    private ChargeBreakUp[] chargeBU;
    public ChargeBreakUp[] ChargeBU
    {
        get { return chargeBU; }
        set { chargeBU = value; }
    }
    private decimal otherCharges;
    public decimal OtherCharges
    {
        get { return otherCharges; }
        set { otherCharges = value; }
    }
    private decimal fuelSurcharge;
    public decimal FuelSurcharge
    {
        get { return fuelSurcharge; }
        set { fuelSurcharge = value; }
    }
    private decimal plb;
    public decimal PLB {
        get { return plb; }
        set { plb = value; }
    }
    //baggage fare for G9 (AirArabia)
    private decimal baggageCharge;
    public decimal BaggageCharge
    {
        get { return baggageCharge; }
        set { baggageCharge = value; }
    }


    private decimal mealCharge;
    public decimal MealCharge
    {
        get { return mealCharge; }
        set { mealCharge = value; }
    }

    //baggage fare for G9 (AirArabia)
    private decimal insuranceAmount;
    public decimal InsuranceAmount
    {
        get { return insuranceAmount; }
        set { insuranceAmount = value; }
    }

    private List<decimal> flyDubaiBaggageCharge;
    public List<decimal> FlyDubaiBaggageCharge
    {
        get { return flyDubaiBaggageCharge; }
        set { flyDubaiBaggageCharge = value; }
    }

    private decimal markup;
    public decimal Markup
    {
        get { return markup; }
        set { markup = value; }
    }

    private string markupType;
    public string MarkupType
    {
        get { return markupType; }
        set { markupType = value; }
    }

    private decimal markupValue;
    public decimal MarkupValue
    {
        get { return markupValue; }
        set { markupValue = value; }
    }

    private decimal b2cMarkup;
    public decimal B2CMarkup
    {
        get { return b2cMarkup; }
        set { b2cMarkup = value; }
    }

    private string b2cMarkupType;
    public string B2CMarkupType
    {
        get { return b2cMarkupType; }
        set { b2cMarkupType = value; }
    }

    private decimal b2cMarkupValue;
    public decimal B2CMarkupValue
    {
        get { return b2cMarkupValue; }
        set { b2cMarkupValue = value; }
    }

    private string supplierCurrency;
    public string SupplierCurrency
    {
        get { return supplierCurrency; }
        set { supplierCurrency = value; }
    }
    private decimal supplierPrice;
    public decimal SupplierPrice
    {
        get { return supplierPrice; }
        set { supplierPrice = value; }
    }

    private bool inputVATApplied;
    public bool InputVATApplied
    {
        get { return inputVATApplied; }
        set { inputVATApplied = value; }
    }

    private decimal inputVAT;
    public decimal InputVAT
    {
        get { return inputVAT; }
        set { inputVAT = value; }
    }

    private decimal outputVAT;
    public decimal OutputVAT
    {
        get { return outputVAT; }
        set { outputVAT = value; }
    }

    private bool outputVATApplied;
    public bool OutputVATApplied
    {
        get { return outputVATApplied; }
        set { outputVATApplied = value; }
    }

    private decimal outputVATCharge;
    public decimal OutputVATCharge
    {
        get { return outputVATCharge; }
        set { outputVATCharge = value; }
    }

    private string outputVATOn;
    public string OutputVATOn
    {
        get { return outputVATOn; }
        set { outputVATOn = value; }
    }

    private PriceTaxDetails taxDetails;
    public PriceTaxDetails TaxDetails
    {
        get { return taxDetails; }
        set { taxDetails = value; }
    }

    #region TBOAir Members
    private decimal transactionFee;
    public decimal TransactionFee
    {
        get { return transactionFee; }
        set { transactionFee = value; }
    }

    private decimal sserviceFee;
    public decimal SServiceFee
    {
        get { return sserviceFee; }
        set { sserviceFee = value; }
    }
    #endregion
}

public struct WSSegAdditionalInfo
{
    private string fareBasis;
    public string FareBasis
    {
        get { return fareBasis; }
        set { fareBasis = value; }
    }

    private string nva;
    public string NVA
    {
        get { return nva; }
        set { nva = value; }
    }

    private string nvb;
    public string NVB
    {
        get { return nvb; }
        set { nvb = value; }
    }

    private string baggage;
    public string Baggage
    {
        get { return baggage; }
        set { baggage = value; }
    }

    // Airline code + flight number + date + time  eg. BA010227MAR20070710 for BA 0102 27MAR2007 0710 
    private string flightKey;
    // Gets or sets flight key. (Airline code + flight number + date + time)  eg. BA010227MAR20070710 for BA 0102 27MAR2007 0710 
    public string FlightKey
    {
        get { return flightKey; }
        set { flightKey = value; }
    }
}

public enum ChangeRequestType
{
    Cancellation = 1,
    Reissuance = 2,
    Void = 3
}

public enum ChangeRequestStatus
{
    Pending = 1,
    InProgress = 2,
    Processed =3,
    Rejected =4
}
public enum ScoreStaus
{
    Genuine = 1,
    Suspicious = 2,
    Dangerous = 3
}

    
