using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using CT.Core;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for WSMail
/// </summary>
public class WSMail
{
    public WSMail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    //    else if (Request["isEticket"] != null && Convert.ToBoolean(Request["isEticket"]))
    public static void SendTicket(string pnr, string addressList, string from)
    {
        List<Ticket> ticketList = Ticket.GetTicketList(FlightItinerary.GetFlightId(pnr));
        List<string> toArray = new List<string>();
        string[] addressArray = addressList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        for (int k = 0; k < addressArray.Length; k++)
        {
            if (!toArray.Contains(addressArray[k]))
            {
                toArray.Add(addressArray[k]);
            }
        }
        foreach (Ticket ticket in ticketList)
        {
            Email.Send(from, from, toArray, "E-Ticket", BuildMessage(ticket), new Hashtable());
        }
    }

    public static string BuildMessage(Ticket ticket)
    {
        StringBuilder fullString = new StringBuilder();
        int paxIndex = -1;
        string paxName = string.Empty;
        string cityName = string.Empty;
        string logo = string.Empty;
        int paxTypeIndex = 0;

        #region Loading all the objects required for E-Ticket
        //ticket = new Ticket();
        //ticket.Load(ticketId);
        FlightItinerary flightItinerary = new FlightItinerary(ticket.FlightId);
        for (int i = 0; i < flightItinerary.Passenger.Length; i++)
        {
            if (flightItinerary.Passenger[i].PaxId == ticket.PaxId)
            {
                paxIndex = i;
                break;
            }
        }
        if (paxIndex >= 0)
        {
            paxName = FlightPassenger.GetPaxFullName(flightItinerary.Passenger[paxIndex].PaxId);
        }
        else
        {
            paxName = ticket.Title + " " + ticket.PaxFirstName + " " + ticket.PaxLastName;
        }
        paxName = paxName.Trim();

        int AgentMasterId = Ticket.GetAgencyIdForTicket(ticket.TicketId);
        AgentMaster AgentMaster = new AgentMaster(AgentMasterId);
        //if (AgentMaster.Address.CityId > 0)
        {
            cityName = AgentMaster.City;
        }
        //else
        //{
        //    cityName = AgentMaster.Address.CityOther;
        //}
        //logo = AgentMaster.GetAgentMasterLogo(AgentMaster.AgentMasterId);
        ticket.PtcDetail = SegmentPTCDetail.GetSegmentPTCDetail(flightItinerary.FlightId);
        for (int i = 0; i < ticket.PtcDetail.Count; i++)
        {
            if (ticket.PaxType == FlightPassenger.GetPassengerType(ticket.PtcDetail[i].PaxType))
            {
                paxTypeIndex = i;
                break;
            }
        }
        string phoneString = string.Empty;
        if (AgentMaster.Phone1 != null && AgentMaster.Phone1.Length > 0)
        {
            phoneString = AgentMaster.Phone1;
        }
        string eTicket = "Itinerary";
        string message = "This is not an E-Ticket. Please collect your ticket from your travel agent";
        string validString = "(This document is not valid for travel)";
        if (ticket.ETicket)
        {
            eTicket = "E-Ticket";
            message = "This is an electronic ticket. Please carry a positive identification for check in. ";
            validString = "";
        }
        if (logo.Length != 0)
        {
            logo = "<div style=\"float: left; width: 67px\"><img alt=\"Agent Logo\" height=\"66px\" src=\"http://www.travelboutiqueonline.com/UserImages/" + logo + "\" width=\"66px\" /></div>";
        }
        else
        {
            logo = "";
        }
        #endregion
        // calculating IsLcc
        bool isLCC = false;
        string code = string.Empty;
        foreach (FlightInfo segment in flightItinerary.Segments)
        {
            string airlineCode = segment.Airline;
            Airline airline = new Airline();
            airline.Load(airlineCode);
            if (airline.IsLCC)
            {
                isLCC = true;
                code = airline.AirlineCode;
                break;
            }
        }

        //This is a global hash table which we send in Email.Send which replaces 
        //hashvariables with values(code which is not in loop)
        Hashtable globalHashTable = new Hashtable();
        globalHashTable.Add("logo", logo);
        globalHashTable.Add("AgentMasterName", AgentMaster.Name);
        globalHashTable.Add("addressLine1", AgentMaster.Address);
        globalHashTable.Add("addressLine2", "");
        globalHashTable.Add("cityName", cityName);
        globalHashTable.Add("addressPin", AgentMaster.POBox);
        if (AgentMaster.Phone1 != null && AgentMaster.Phone1.Length != 0)
        {
            globalHashTable.Add("addressPhone", "Phone:" + AgentMaster.Phone1);
        }
        else
        {
            globalHashTable.Add("addressPhone", "");
        }
        globalHashTable.Add("eTicketString", eTicket);
        globalHashTable.Add("validString", validString);
        if (!isLCC)
        {
            globalHashTable.Add("PNR", "1P - " + flightItinerary.PNR);
        }
        else
        {
            globalHashTable.Add("PNR", code + " - " + flightItinerary.PNR);
        }
        globalHashTable.Add("issueDate", ticket.IssueDate.ToString("ddd dd MMM yyyy"));
        globalHashTable.Add("paxName", paxName);
        if (!isLCC)
        {
            globalHashTable.Add("ticketNumber", "Ticket Number: " + ticket.ValidatingAriline + ticket.TicketNumber);
        }
        else
        {
            globalHashTable.Add("ticketNumber", "Reference Number: " + flightItinerary.PNR);
        }
        globalHashTable.Add("message", message);
        string transactionFee = string.Empty;
        if (ticket.Price.TransactionFee > 0)
        {
            transactionFee = "<tr><td style=\"width:120px;\"><span style=\"margin:0px;padding:0px;float:right;font-size:14px;text-align:right;font-weight:500;\">Tra Fee:</span></td><td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;float:right;font-size:14px;	text-align:right;font-weight:500;\">Rs. " + ticket.Price.TransactionFee.ToString("N2") + "</span></td></tr>";
        }
        string fareString = string.Empty;

        fareString = "<td style=\"width:240px;\">							<table style=\"width:240px;	margin:-4px 0px 0px 0px;	padding:0px;	float:left;\"border=\"0\" cellspacing=\"0\" cellpadding=\"0\">								<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											AirFare:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Rs. " + ticket.Price.PublishedFare.ToString("N2") + "										</span>									</td>								</tr>								<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Tax:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Rs." + (ticket.Price.Tax + ticket.Price.OtherCharges).ToString("N2") + "										</span>									</td>								</tr>									" + transactionFee + "								<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											TotalAirFare:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Rs." + Convert.ToDouble(ticket.Price.PublishedFare + ticket.Price.Tax + ticket.Price.OtherCharges + ticket.Price.TransactionFee).ToString("N2") + "										</span>									</td>								</tr>							</table>						</td>";

        globalHashTable.Add("fare", fareString);
        if (ticket.TourCode != null && ticket.TourCode.Trim().Length != 0)
        {
            globalHashTable.Add("promotionalCode", "Promotional Code: " + ticket.TourCode);
        }
        else
        {
            globalHashTable.Add("promotionalCode", "");
        }
        //globalHashTable.Add("publishedFare", ticket.Price.PublishedFare.ToString("N2"));
        //globalHashTable.Add("tax", (ticket.Price.Tax + ticket.Price.OtherCharges).ToString("N2"));
        //globalHashTable.Add("pubWithTax", Convert.ToDouble(ticket.Price.PublishedFare + ticket.Price.Tax + ticket.Price.OtherCharges).ToString("N2"));
        //Html code in text file for E-ticket
        string filePath = ConfigurationSystem.Email["ETicketPage"];
        StreamReader sr = new StreamReader(filePath);
        //string contains the code of loop
        string loopString = string.Empty;
        bool loopStarts = false;
        //string contains the code before loop 
        string startString = string.Empty;
        //string contains the code after loop
        string endString = string.Empty;
        bool loopEnds = false;
        #region seperate out the loop code and the other code
        while (!sr.EndOfStream)
        {
            string line = sr.ReadLine();
            if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
            {
                loopEnds = true;
                loopStarts = false;
                if (line.IndexOf("%loopEnds%") >= 0)
                {
                    line = sr.ReadLine();
                }
                endString += line;
            }
            if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
            {
                if (line.IndexOf("%loopStarts%") >= 0)
                {
                    line = sr.ReadLine();
                }
                loopString += line;
                loopStarts = true;
            }
            else
            {
                if (!loopEnds)
                {
                    startString += line;
                }
            }
        }
        #endregion

        string midString = string.Empty;
        for (int count = 0; count < flightItinerary.Segments.Length; count++)
        {
            Airline airline = new Airline();
            airline.Load(flightItinerary.Segments[count].Airline);
            //Temporary hashtable which replaces loopvariable continuously
            Hashtable tempTable = new Hashtable();
            tempTable.Add("airlineLogo", "http://www.travelboutiqueonline.com/Images/AirlineLogo/" + airline.LogoFile);
            tempTable.Add("airlineName", airline.AirlineName);
            tempTable.Add("airlineCode", airline.AirlineCode);
            tempTable.Add("flightNumber", flightItinerary.Segments[count].FlightNumber);
            if (flightItinerary.Segments[count].AirlinePNR != null && flightItinerary.Segments[count].AirlinePNR.Length != 0)
            {
                tempTable.Add("airlinePNR", "Airline Ref: " + flightItinerary.Segments[count].AirlinePNR);
            }
            else
            {
                tempTable.Add("airlinePNR", "");
            }
            if (flightItinerary.Segments[count].FlightStatus == FlightStatus.Confirmed)
            {
                tempTable.Add("flightStatus", "Status : Confirmed");
            }
            else
            {
                //TODO:This is done for demo we'll change it to not confirmed
                tempTable.Add("flightStatus", "Status : Confirmed");
            }
            tempTable.Add("depDay", flightItinerary.Segments[count].DepartureTime.DayOfWeek.ToString().Substring(0, 3));
            tempTable.Add("depDate", flightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy"));
            tempTable.Add("origAirportCode", flightItinerary.Segments[count].Origin.AirportCode);
            tempTable.Add("origAirportName", flightItinerary.Segments[count].Origin.AirportName);
            if (flightItinerary.Segments[count].ArrTerminal != null && flightItinerary.Segments[count].ArrTerminal.Trim() != string.Empty)
            {
                tempTable.Add("arrTerminal", "Terminal " + flightItinerary.Segments[count].ArrTerminal);
            }
            else
            {
                tempTable.Add("arrTerminal", "");
            }
            tempTable.Add("depTime", flightItinerary.Segments[count].DepartureTime.ToShortTimeString());
            tempTable.Add("destAirportCode", flightItinerary.Segments[count].Destination.AirportCode);
            tempTable.Add("destAirportName", flightItinerary.Segments[count].Destination.AirportName);
            if (flightItinerary.Segments[count].DepTerminal != null && flightItinerary.Segments[count].DepTerminal.Trim() != string.Empty)
            {
                tempTable.Add("depTerminal", "Terminal " + flightItinerary.Segments[count].DepTerminal);
            }
            else
            {
                tempTable.Add("depTerminal", "");
            }
            tempTable.Add("arrTime", flightItinerary.Segments[count].ArrivalTime.ToShortTimeString());
            tempTable.Add("arrDay", flightItinerary.Segments[count].ArrivalTime.DayOfWeek.ToString().Substring(0, 3));
            tempTable.Add("bookingClass", flightItinerary.Segments[count].BookingClass);
            tempTable.Add("flightDuration", ((flightItinerary.Segments[count].Duration.Days * 24 + flightItinerary.Segments[count].Duration.Hours) + ":" + flightItinerary.Segments[count].Duration.Minutes));
            string stopString = "Non stop";
            if (flightItinerary.Segments[count].Stops > 0)
            {
                stopString = flightItinerary.Segments[count].Stops + " stops";
            }
            tempTable.Add("stopString", stopString);
            string mealCode = FlightPassenger.GetMealCode(ticket.PaxId);
            string mealDescription = string.Empty;
            if (mealCode != string.Empty)
            {
                mealDescription = Meal.GetMeal(mealCode).Description;
            }
            if (mealDescription != null && mealDescription.Length != 0)
            {
                tempTable.Add("mealDescription", "Meal: " + mealDescription + "<span style=\"font-size: 9px; font-weight: normal; color: #888888;\">(subject to availability)</span>");
            }
            else
            {
                tempTable.Add("mealDescription", "");
            }
            string baggage = "0 kg baggage";
            if (ticket.PtcDetail != null && ticket.PtcDetail.Count > 0)
            {
                baggage = ticket.PtcDetail[paxTypeIndex].Baggage;
            }
            if (baggage != null && baggage.Length != 0)
            {
                tempTable.Add("baggage", "Baggage: " + baggage);
            }
            else
            {
                tempTable.Add("baggage", "");
            }
            if (flightItinerary.Segments[count].Craft != null && flightItinerary.Segments[count].Craft.ToString().Trim() != string.Empty)
            {
                tempTable.Add("craft", "Aircraft: " + flightItinerary.Segments[count].Craft);
            }
            else
            {
                tempTable.Add("craft", "");
            }

            midString += Email.ReplaceHashVariable(loopString, tempTable);
        }
        fullString.Append(startString);
        fullString.Append(midString);
        fullString.Append(endString);

        string messageString = Email.ReplaceHashVariable(fullString.ToString(), globalHashTable);
        return messageString;
    }
}
