/// <summary>
/// Summary description for WSGetTripDetalResponse
/// </summary>
public class WSGetTripDetailResponse
{
    private WSBookingDetail[] bookingDetail;
    public WSBookingDetail[] BookingDetail
    {
        get { return bookingDetail; }
        set { bookingDetail=value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public WSGetTripDetailResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
