using System;
using System.Collections.Generic;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for WSUserPreference
/// </summary>
public class WSUserPreference
{
    private string imagePath = CT.Configuration.ConfigurationSystem.WLAdConfig["logoPath"];// "http://localhost:3350/TekTravelWeb/WLImages/";
    public const string EnbdCCCharge = "ENBD-CCCHARGE";

    //ZIYAD 15-APR-2017    
    private int _memberId;
    public int MemberId
    {
        get { return _memberId; }
        set { _memberId = value; }
    }
    private string siteName;
    public string SiteName
    {
        get { return siteName; }
        set { siteName = value; }
    }

    private string siteTitle;
    public string SiteTitle
    {
        get { return siteTitle; }
        set { siteTitle = value; }
    }
    private string tradingName;
    public string TradingName
    {
        get { return tradingName; }
        set { tradingName = value; }
    }

    private string tagLine;
    public string TagLine
    {
        get { return tagLine; }
        set { tagLine = value; }
    }

    private bool showLogo;
    public bool ShowLogo
    {
        get { return showLogo; }
        set { showLogo = value; }
    }

    private string logo;
    public string Logo
    {
        get { return logo; }
        set { logo = value; }
    }

    private bool showHeader;
    public bool ShowHeader
    {
        get { return showHeader; }
        set { showHeader = value; }
    }

    private string header;
    public string Header
    {
        get { return header; }
        set { header = value; }
    }
    private string headerDetail;
    public string HeaderDetail
    {
        get { return headerDetail; }
        set { headerDetail = value; }
    }
    private string headerImage;
    public string HeaderImage
    {
        get { return headerImage; }
        set { headerImage = value; }
    }
    private string headerText;
    public string HeaderText
    {
        get { return headerText; }
        set { headerText = value; }
    }
    private bool showFooter;
    public bool ShowFooter
    {
        get { return showFooter; }
        set { showFooter = value; }
    }

    private string footer;
    public string Footer
    {
        get { return footer; }
        set { footer = value; }
    }

    private bool showMenuBar;
    public bool ShowMenuBar
    {
        get { return showMenuBar; }
        set { showMenuBar = value; }
    }

    private bool showLoginPage;
    public bool ShowLoginPage
    {
        get { return showLoginPage; }
        set { showLoginPage = value; }
    }

    private bool showContact;
    public bool ShowContact
    {
        get { return showContact; }
        set { showContact = value; }
    }

    private string contact;
    public string Contact
    {
        get { return contact; }
        set { contact = value; }
    }

    private bool showAbout;
    public bool ShowAbout
    {
        get { return showAbout; }
        set { showAbout = value; }
    }

    private string about;
    public string About
    {
        get { return about; }
        set { about = value; }
    }

    private bool showCarrers;
    public bool ShowCarrers
    {
        get { return showCarrers; }
        set { showCarrers = value; }
    }

    private string careers;
    public string Careers
    {
        get { return careers; }
        set { careers = value; }
    }

    private bool allowInternational;
    public bool AllowInternational
    {
        get { return allowInternational; }
        set { allowInternational = value; }
    }

    private bool allowDomestic;
    public bool AllowDomestic
    {
        get { return allowDomestic; }
        set { allowDomestic = value; }
    }

    private bool allowBooking;
    public bool AllowBooking
    {
        get { return allowBooking; }
        set { allowBooking = value; }
    }

    private bool allowTicketing;
    public bool AllowTicketing
    {
        get { return allowTicketing; }
        set { allowTicketing = value; }
    }

    private decimal fee;
    public decimal Fee
    {
        get { return fee; }
        set { fee = value; }
    }

    private decimal discount;
    public decimal Discount
    {
        get { return discount; }
        set { discount = value; }
    }
    private decimal serviceTax;
    public decimal ServiceTax
    {
        get { return serviceTax; }
        set { serviceTax = value; }
    }
    private decimal serviceCharge;
    public decimal ServiceCharge
    {
        get { return serviceCharge; }
        set { serviceCharge = value; }
    }

    private string layout;
    public string Layout
    {
        get { return layout; }
        set { layout = value; }
    }
    private string emailId;
    public string EmailId
    {
        get { return emailId; }
        set { emailId = value; }
    }
    private string phoneNum;
    public string PhoneNum
    {
        get { return phoneNum; }
        set { phoneNum = value; }
    }
    private string faxNum;
    public string FaxNum
    {
        get { return faxNum; }
        set { faxNum = value; }
    }
    private string waitingLogo;
    public string WaitingLogo
    {
        get { return waitingLogo; }
        set { waitingLogo = value; }
    }
    private string waitingText;
    public string WaitingText
    {
        get { return waitingText; }
        set { waitingText = value; }
    }
    private string homePageUrl;
    public string HomePageUrl
    {
        get { return homePageUrl; }
        set { homePageUrl = value; }
    }
    private string leftTopPanelText;
    public string LeftTopPanelText
    {
        get { return leftTopPanelText; }
        set { leftTopPanelText = value; }
    }
    private string hdfcPaymentGateway;
    public string HdfcPaymentGateway
    {
        get { return hdfcPaymentGateway; }
        set { hdfcPaymentGateway = value; }
    }
    private string oxiCashPaymentGateway;
    public string OxiCashPaymentGateway
    {
        get { return oxiCashPaymentGateway; }
        set { oxiCashPaymentGateway = value; }
    }
    private string amexPaymentGateway;
    public string AmexPaymentGateway
    {
        get { return amexPaymentGateway; }
        set { amexPaymentGateway = value; }
    }
    private string iciciPaymentGateway;
    public string IciciPaymentGateway
    {
        get { return iciciPaymentGateway; }
        set { iciciPaymentGateway = value; }
    }
    private string ccavenuePaymentGateway;
    public string CcavenuePaymentGateway
    {
        get { return ccavenuePaymentGateway; }
        set { ccavenuePaymentGateway = value; }
    }
    
    private string ticketValaPaymentGateway;
    public string TicketValaPaymentGateway
    {
        get { return ticketValaPaymentGateway; }
        set { ticketValaPaymentGateway = value; }
    }
    private string axisPaymentGateway;
    public string AxisPaymentGateway
    {
        get { return axisPaymentGateway; }
        set { axisPaymentGateway = value; }
    }
    private string beamPaymentGateway;
    public string BeamPaymentGateway
    {
        get { return beamPaymentGateway; }
        set { beamPaymentGateway = value; }
    }
    private string billDeskPaymentGateway;
    public string BillDeskPaymentGateway
    {
        get { return billDeskPaymentGateway; }
        set { billDeskPaymentGateway = value; }
    }
    private string sbiPaymentGateway;
    public string SbiPaymentGateway
    {
        get { return sbiPaymentGateway; }
        set { sbiPaymentGateway = value; }
    }
    private string additionalTxnFee;
    public string AdditionalTxnFee
    {
        get { return additionalTxnFee; }
        set { additionalTxnFee = value; }
    }
    private string aboutContent;
    public string AboutContent
    {
        get { return aboutContent; }
        set { aboutContent = value; }
    }
    private string contactContent;
    public string ContactContent
    {
        get { return contactContent; }
        set { contactContent = value; }
    }
    private string privacyContent;
    public string PrivacyContent
    {
        get { return privacyContent; }
        set { privacyContent = value; }
    }
    private string termsContent;
    public string TermsContent
    {
        get { return termsContent; }
        set { termsContent = value; }
    }
    private bool showHomePageMiddleImage;
    public bool ShowHomePageMiddleImage
    {
        get { return showHomePageMiddleImage; }
        set { showHomePageMiddleImage = value; }
    }
    private bool showHomePageRightImage;
    public bool ShowHomePageRightImage
    {
        get { return showHomePageRightImage; }
        set { showHomePageRightImage = value; }
    }
    private string homePageMiddleImage;
    public string HomePageMiddleImage
    {
        get { return homePageMiddleImage; }
        set { homePageMiddleImage = value; }
    }
    private string homePageRightImage;
    public string HomePageRightImage
    {
        get { return homePageRightImage; }
        set { homePageRightImage = value; }
    }
    private WSGoogleScriptType googleScriptType = WSGoogleScriptType.Analytics;
    public WSGoogleScriptType GoogleScriptType
    {
        get { return googleScriptType; }
        set { googleScriptType = value; }
    }
    private string googleScript;
    public string GoogleScript
    {
        get { return googleScript; }
        set { googleScript = value; }
    }
    private bool isPGOwnedByTBO;
    public bool IsPGOwnedByTBO
    {
        get { return isPGOwnedByTBO; }
        set { isPGOwnedByTBO = value; }
    }

    private string agentCurrency;
    public string AgentCurrency
    {
        get { return agentCurrency; }
        set { agentCurrency = value; }
    }

    private int agentDecimalPoint;
    public int AgentDecimalPoint
    {
        get { return agentDecimalPoint; }
        set { agentDecimalPoint = value; }
    }

    private string agentEmail;
    public string AgentEmail
    {
        get { return agentEmail; }
        set { agentEmail = value; }
    }

    private int agentId;
    public int AgentID
    {
        get { return agentId; }
        set { agentId = value; }
    }
    private bool enbd_pg;
    public bool ENBD_PG
    {
        get { return enbd_pg; }
        set { enbd_pg = value; }
    }
    private bool ccavenue_pg;
    public bool CCAVENUE_PG
    {
        get { return ccavenue_pg; }
        set { ccavenue_pg = value; }
    }
    private decimal enbd_creditCardCharge;
    public decimal ENBD_CreditCardCharge
    {
        get { return enbd_creditCardCharge; }
        set { enbd_creditCardCharge = value; }
    }
    private decimal ccavenue_creditCardCharge;
    public decimal CCAVENUE_CreditCardCharge
    {
        get { return ccavenue_creditCardCharge; }
        set { ccavenue_creditCardCharge = value; }
    }
    private string agentAddress;
    public string AgentAddress
    {
        get { return agentAddress; }
        set { agentAddress = value; }
    }

    private string agentPhoneNo;
    public string AgentPhoneNo
    {
        get { return agentPhoneNo; }
        set { agentPhoneNo = value; }
    }

    private string allowFlights;
    public string AllowFlights
    {
        get { return allowFlights; }
        set { allowFlights = value; }
    }

    private string allowHotels;
    public string AllowHotels
    {
        get { return allowHotels; }
        set { allowHotels = value; }
    }

    private string allowHolidays;
    public string AllowHolidays
    {
        get { return allowHolidays; }
        set { allowHolidays = value; }
    }

    private string allowVisa;
    public string AllowVisa
    {
        get { return allowVisa; }
        set { allowVisa = value; }
    }

    private string allowInsurance;
    public string AllowInsurance
    {
        get { return allowInsurance; }
        set { allowInsurance = value; }
    }

    private string allowSightSeeing;
    public string AllowSightSeeing
    {
        get { return allowSightSeeing; }
        set { allowSightSeeing = value; }
    }

    private string allowTransfers;
    public string AllowTransfers
    {
        get { return allowTransfers; }
        set { allowTransfers = value; }
    }

    private bool networkinternational_pg;
    public bool NetworkInternational_PG
    {
        get { return networkinternational_pg; }
        set { networkinternational_pg = value; }
    }
    private decimal networkinternational_creditCardCharge;
    public decimal NetworkInternational_CreditCardCharge
    {
        get { return networkinternational_creditCardCharge; }
        set { networkinternational_creditCardCharge = value; }
    }
    //Added by ziya on 14/01/2019 -- for Payfort
private bool razorPayPG;
    private decimal razorPayCreditCardCharge;

    public bool RazorPayPG
    {
        get { return razorPayPG; }
        set { razorPayPG = value; }
    }
    public decimal RazorPayCreditCardCharge
    {
        get { return razorPayCreditCardCharge; }
        set { razorPayCreditCardCharge = value; }
    }

    private bool payfort_pg;
    public bool PAYFORT_PG
    {
        get { return payfort_pg; }
        set { payfort_pg = value; }
    }
    private decimal payfort_creditCardCharge;
    public decimal PAYFORT_CreditCardCharge
    {
        get { return payfort_creditCardCharge; }
        set { payfort_creditCardCharge = value; }
    }
    private string agentCountryCode;
    public string AgentCountryCode
    {
        get { return agentCountryCode; }
        set { agentCountryCode = value; }
    }

    public WSUserPreference()
    {
        //
        // TODO: Add constructor logic here
        //        
    }

    public void Load(int memberId)
    {
        WSUserPreference wsPref = new WSUserPreference();
        UserMaster UserMaster = new UserMaster(memberId);
        _memberId = memberId;
        Dictionary<string, string> prefList = UserPreference.GetPreferenceList((int)(int)UserMaster.ID,ItemType.Flight);
        if (prefList.ContainsKey(WLPreferenceKeys.SiteName))
        {
            siteName = prefList[WLPreferenceKeys.SiteName];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.SiteTitle))
        {
            siteTitle = prefList[WLPreferenceKeys.SiteTitle];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.TradingName))
        {
            tradingName = prefList[WLPreferenceKeys.TradingName];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.TagLine))
        {
            tagLine = prefList[WLPreferenceKeys.TagLine];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowLogo))
        {
            showLogo = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowLogo]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.Logo))
        {
            logo = imagePath + prefList[WLPreferenceKeys.Logo];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowHeader))
        {
            showHeader = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowHeader]);
        }

        if (prefList.ContainsKey(WLPreferenceKeys.ShowHeader))
        {
            showHeader = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowHeader]);
        }

        if (prefList.ContainsKey(WLPreferenceKeys.Header))
        {
            header = imagePath + prefList[WLPreferenceKeys.Header];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HeaderDetail))
        {
            headerDetail = prefList[WLPreferenceKeys.HeaderDetail];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HeaderImage))
        {
            headerImage = imagePath + prefList[WLPreferenceKeys.HeaderImage];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HeaderText))
        {
            headerText = prefList[WLPreferenceKeys.HeaderText];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowFooter))
        {
            showFooter = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowFooter]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.Footer))
        {
            footer = prefList[WLPreferenceKeys.Footer];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowMenuBar))
        {
            showMenuBar = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowMenuBar]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowLoginPage))
        {
            showLoginPage = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowLoginPage]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowContact))
        {
            showContact = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowContact]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.Contact))
        {
            contact = prefList[WLPreferenceKeys.Contact];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowAbout))
        {
            showAbout = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowAbout]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.About))
        {
            about = prefList[WLPreferenceKeys.About];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowCarrers))
        {
            showCarrers = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowCarrers]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.Careers))
        {
            careers = prefList[WLPreferenceKeys.Careers];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowHeader))
        {
            showHeader = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowHeader]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.Header))
        {
            header = prefList[WLPreferenceKeys.Header];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.AllowInternational))
        {
            allowInternational = Convert.ToBoolean(prefList[WLPreferenceKeys.AllowInternational]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.AllowDomestic))
        {
            allowDomestic = Convert.ToBoolean(prefList[WLPreferenceKeys.AllowDomestic]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.AllowBookig))
        {
            allowBooking = Convert.ToBoolean(prefList[WLPreferenceKeys.AllowBookig]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.AllowTicketing))
        {
            allowTicketing = Convert.ToBoolean(prefList[WLPreferenceKeys.AllowTicketing]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.Fee))
        {
            fee = Convert.ToDecimal(prefList[WLPreferenceKeys.Fee]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.Discount))
        {
            if (prefList[WLPreferenceKeys.Discount] != null && prefList[WLPreferenceKeys.Discount] != "")
            {
                try
                {
                    discount = Convert.ToDecimal(prefList[WLPreferenceKeys.Discount]);
                }
                catch (Exception)
                {
                    discount = 0;
                }
            }
            else
            {
                discount = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ServiceCharge))
        {
            if (prefList[WLPreferenceKeys.ServiceCharge] != null && prefList[WLPreferenceKeys.ServiceCharge] != "")
            {
                try
                {
                    serviceCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.ServiceCharge]);
                }
                catch (Exception)
                {
                    serviceCharge = 0;
                }
            }
            else
            {
                serviceCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ServiceTax))
        {
            if (prefList[WLPreferenceKeys.ServiceTax] != null && prefList[WLPreferenceKeys.ServiceTax] != "")
            {
                try
                {
                    serviceTax = Convert.ToDecimal(prefList[WLPreferenceKeys.ServiceTax]);
                }
                catch (Exception)
                {
                    serviceTax = 0;
                }
            }
            else
            {
                serviceTax = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.Layout))
        {
            layout = prefList[WLPreferenceKeys.Layout];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.EmailId))
        {
            emailId = prefList[WLPreferenceKeys.EmailId];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.PhoneNum))
        {
            phoneNum = prefList[WLPreferenceKeys.PhoneNum];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.FaxNum))
        {
            faxNum = prefList[WLPreferenceKeys.FaxNum];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.WaitingLogo))
        {
            waitingLogo = imagePath + prefList[WLPreferenceKeys.WaitingLogo];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.WaitingText))
        {
            waitingText = prefList[WLPreferenceKeys.WaitingText];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HomePageUrl))
        {
            homePageUrl = prefList[WLPreferenceKeys.HomePageUrl];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.LeftTopPanelText))
        {
            leftTopPanelText = prefList[WLPreferenceKeys.LeftTopPanelText];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.AdditionalTxnFee))
        {
            additionalTxnFee = prefList[WLPreferenceKeys.AdditionalTxnFee];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HdfcPaymentGateway))
        {
            hdfcPaymentGateway = prefList[WLPreferenceKeys.HdfcPaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.OxiCashPaymentGateway))
        {
            oxiCashPaymentGateway = prefList[WLPreferenceKeys.OxiCashPaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.AmexPaymentGateway))
        {
            amexPaymentGateway = prefList[WLPreferenceKeys.AmexPaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.IciciPaymentGateway))
        {
            iciciPaymentGateway = prefList[WLPreferenceKeys.IciciPaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenuePaymentGateway))
        {
            ccavenuePaymentGateway = prefList[WLPreferenceKeys.CCAvenuePaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.TicketValaPaymentGateway))
        {
            ticketValaPaymentGateway = prefList[WLPreferenceKeys.TicketValaPaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.AxisPaymentGateway))
        {
            axisPaymentGateway = prefList[WLPreferenceKeys.AxisPaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.BeamPaymentGateway))
        {
            beamPaymentGateway = prefList[WLPreferenceKeys.BeamPaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.BillDeskPaymentGateway))
        {
            billDeskPaymentGateway = prefList[WLPreferenceKeys.BillDeskPaymentGateway];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.SbiPaymentGateway))
        {
            sbiPaymentGateway = prefList[WLPreferenceKeys.SbiPaymentGateway];
        }      
        if (prefList.ContainsKey(WLPreferenceKeys.PrivacyContent))
        {
            privacyContent = prefList[WLPreferenceKeys.PrivacyContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.TermsContent))
        {
            termsContent = prefList[WLPreferenceKeys.TermsContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowHomePageMiddleImage))
        {
            showHomePageMiddleImage = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowHomePageMiddleImage]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ShowHomePageRightImage))
        {
            showHomePageRightImage = Convert.ToBoolean(prefList[WLPreferenceKeys.ShowHomePageRightImage]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HomePageMiddleImage))
        {
            homePageMiddleImage = imagePath + prefList[WLPreferenceKeys.HomePageMiddleImage];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HomePageRightImage))
        {
            homePageRightImage = imagePath + prefList[WLPreferenceKeys.HomePageRightImage];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.GoogleScript))
        {
            googleScript = prefList[WLPreferenceKeys.GoogleScript];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.GoogleScriptType))
        {
            googleScriptType = (WSGoogleScriptType)Enum.Parse(typeof(WSGoogleScriptType), prefList[WLPreferenceKeys.GoogleScriptType].ToString());
        }
        if (prefList.ContainsKey(WLPreferenceKeys.isPGOwnedByTBO))
        {
            isPGOwnedByTBO = Convert.ToBoolean(prefList[WLPreferenceKeys.isPGOwnedByTBO]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.enbdPG))
        {
            enbd_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.enbdPG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ccAvenuePG))
        {
            ccavenue_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.ccAvenuePG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.enbdCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.enbdCreditCardCharges] != null && prefList[WLPreferenceKeys.enbdCreditCardCharges] != "")
            {
                try
                {
                    enbd_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.enbdCreditCardCharges]);
                }
                catch (Exception)
                {
                    enbd_creditCardCharge = 0;
                }
            }
            else
            {
                enbd_creditCardCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ccAvenueCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.ccAvenueCreditCardCharges] != null && prefList[WLPreferenceKeys.ccAvenueCreditCardCharges] != "")
            {
                try
                {
                    ccavenue_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.ccAvenueCreditCardCharges]);
                }
                catch (Exception)
                {
                    ccavenue_creditCardCharge = 0;
                }
            }
            else
            {
                ccavenue_creditCardCharge = 0;
            }
        }

        if (prefList.ContainsKey(WLPreferenceKeys.AllowB2CFlights) && prefList[WLPreferenceKeys.AllowB2CFlights] != null)
        {
            allowFlights = prefList[WLPreferenceKeys.AllowB2CFlights];
        }
        else
        {
            allowFlights = "False";
        }

        if (prefList.ContainsKey(WLPreferenceKeys.AllowB2CHotels) && prefList[WLPreferenceKeys.AllowB2CHotels] != null)
        {
            allowHotels = prefList[WLPreferenceKeys.AllowB2CHotels];
        }
        else
        {
            allowHotels = "False";
        }

        if (prefList.ContainsKey(WLPreferenceKeys.AllowB2CHolidays) && prefList[WLPreferenceKeys.AllowB2CHolidays] != null)
        {
            allowHolidays = prefList[WLPreferenceKeys.AllowB2CHolidays];
        }
        else
        {
            allowHolidays = "False";
        }

        if (prefList.ContainsKey(WLPreferenceKeys.AllowB2CVisa) && prefList[WLPreferenceKeys.AllowB2CVisa] != null)
        {
            allowVisa = prefList[WLPreferenceKeys.AllowB2CVisa];
        }
        else
        {
            allowVisa = "False";
        }

        if (prefList.ContainsKey(WLPreferenceKeys.AllowB2CInsurance) && prefList[WLPreferenceKeys.AllowB2CInsurance] != null)
        {
            allowInsurance = prefList[WLPreferenceKeys.AllowB2CInsurance];
        }
        else
        {
            allowInsurance = "False";
        }

        if (prefList.ContainsKey(WLPreferenceKeys.AllowB2CSightSeeing) && prefList[WLPreferenceKeys.AllowB2CSightSeeing] != null)
        {
            allowSightSeeing = prefList[WLPreferenceKeys.AllowB2CSightSeeing];
        }
        else
        {
            allowSightSeeing = "False";
        }

        if (prefList.ContainsKey(WLPreferenceKeys.AllowB2CTransfers) && prefList[WLPreferenceKeys.AllowB2CTransfers] != null)
        {
            allowTransfers = prefList[WLPreferenceKeys.AllowB2CTransfers];
        }
        else
        {
            allowTransfers = "False";
        }
        if (prefList.ContainsKey(WLPreferenceKeys.networkInternationalPG))
        {
            networkinternational_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.networkInternationalPG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.networkInternationalCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.networkInternationalCreditCardCharges] != null && prefList[WLPreferenceKeys.networkInternationalCreditCardCharges] != "")
            {
                try
                {
                    networkinternational_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.networkInternationalCreditCardCharges]);
                }
                catch (Exception)
                {
                    networkinternational_creditCardCharge = 0;
                }
            }
            else
            {
                networkinternational_creditCardCharge = 0;
            }
        }
  if (prefList.ContainsKey(WLPreferenceKeys.RazorpayPG))
        {
            razorPayPG = Convert.ToBoolean(prefList[WLPreferenceKeys.RazorpayPG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.RazorpayCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.RazorpayCreditCardCharges] != null && prefList[WLPreferenceKeys.RazorpayCreditCardCharges] != "")
            {
                try
                {
                    razorPayCreditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.RazorpayCreditCardCharges]);
                }
                catch (Exception)
                {
                    razorPayCreditCardCharge = 0;
                }
            }
            else
            {
                razorPayCreditCardCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.payfortPG))
        {
            payfort_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.payfortPG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.payfortCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.payfortCreditCardCharges] != null && prefList[WLPreferenceKeys.payfortCreditCardCharges] != "")
            {
                try
                {
                    payfort_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.payfortCreditCardCharges]);
                }
                catch (Exception)
                {
                    payfort_creditCardCharge = 0;
                }
            }
            else
            {
                payfort_creditCardCharge = 0;
            }
        }
    }
}

public enum WSGoogleScriptType
{
    Analytics = 1,
    PPC = 2
}
