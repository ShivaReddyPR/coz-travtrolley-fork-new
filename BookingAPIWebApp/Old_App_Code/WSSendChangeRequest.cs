using System;
using System.Configuration;
using CT.BookingEngine;
using CT.Core;
using System.Transactions;
using CT.TicketReceipt.BusinessLayer;
using System.Collections;

/// <summary>
/// Summary description for WSSendCahngeRequest
/// </summary>
public class WSSendChangeRequest
{
    
    private string bookingId;
    public string BookingId
    {
        get { return bookingId; }
        set { bookingId = value; }
    }    
    private ChangeRequestType requestType;
    public ChangeRequestType RequestType
    {
        get { return requestType; }
        set { requestType = value; }
    }
    private int ticketId;
    public int TicketId
    {
        get { return ticketId; }
        set { ticketId = value; }
    }
    private string remarks;
    public string Remarks
    {
        get { return remarks; }
        set { remarks = value; }
    }

    public static WSSendChangeRequestResponse AddRequest(WSSendChangeRequest request, int AgentMasterId, int UserMasterId)
    {
        WSSendChangeRequestResponse response = new WSSendChangeRequestResponse();
        WSStatus status = new WSStatus();
        status = ValidateRequest(request, AgentMasterId);
        if (status != null && status.StatusCode == "08")
        {
            RequestType requestType;
            Ticket ticket = new Ticket();
            ticket.Load(Convert.ToInt32(request.TicketId));
            FlightItinerary itinerary = new FlightItinerary(ticket.FlightId);
            AgentMaster AgentMaster = new AgentMaster(itinerary.AgencyId);
            DateTime CurrentDateTimeInIST = WSUtility.UTCtoIST(DateTime.UtcNow);
            DateTime RefundedDateTimeInIST = new DateTime(CurrentDateTimeInIST.Year, CurrentDateTimeInIST.Month, CurrentDateTimeInIST.Day, 22, 0, 0);
            if (request.RequestType == ChangeRequestType.Cancellation &&  WSUtility.UTCtoIST(ticket.CreatedOn).Date != CurrentDateTimeInIST.Date)
            {
                requestType = CT.BookingEngine.RequestType.Refund;
            }
            else if (request.RequestType == ChangeRequestType.Cancellation && WSUtility.UTCtoIST(ticket.CreatedOn).Date == CurrentDateTimeInIST.Date && DateTime.Compare(CurrentDateTimeInIST, RefundedDateTimeInIST) < 0)
            {
                requestType = CT.BookingEngine.RequestType.Reissuance;
            }
            else
            {
                requestType = CT.BookingEngine.RequestType.Void;
            }

            ServiceRequest serviceRequest = new ServiceRequest();
            serviceRequest.BookingId = Convert.ToInt32(request.BookingId);
            serviceRequest.ReferenceId = itinerary.FlightId;
            serviceRequest.ProductType = ProductType.Flight;
            serviceRequest.RequestType = requestType;
            serviceRequest.Data = request.Remarks;
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
            serviceRequest.CreatedBy = UserMasterId;
            serviceRequest.ItemTypeId = InvoiceItemTypeId.Ticketed;

            serviceRequest.AgencyId = itinerary.AgencyId;
            serviceRequest.AgencyTypeId = (Agencytype)AgentMaster.AgentType;
            serviceRequest.Details = ServiceRequest.GenerateFlightDetailXML(ticket, itinerary, AgentMaster);
            serviceRequest.IsDomestic = itinerary.IsDomestic;
            serviceRequest.PaxName = ticket.Title + " " + ticket.PaxFirstName + " " + ticket.PaxLastName;
            serviceRequest.Pnr = itinerary.PNR;
            serviceRequest.ReferenceNumber = ticket.TicketNumber;
            serviceRequest.Source = (int)itinerary.FlightBookingSource;
            serviceRequest.PriceId = ticket.Price.PriceId;
            serviceRequest.SupplierName = itinerary.AirlineCode;
            serviceRequest.StartDate = itinerary.TravelDate;

            serviceRequest.TicketStatus = ticket.Status;
            serviceRequest.Tax = ticket.Price.Tax;
            serviceRequest.PublishedFare = ticket.Price.PublishedFare;
            serviceRequest.AirlineCode = itinerary.AirlineCode;
            if (itinerary.BookingMode == BookingMode.BookingAPI)
            {
                serviceRequest.RequestSourceId = RequestSource.BookingAPI;
            }
            else if (itinerary.BookingMode == BookingMode.WhiteLabel)
            {
                serviceRequest.RequestSourceId = RequestSource.WhiteLabel;
            }
            else
            {
                serviceRequest.RequestSourceId = RequestSource.BookingAPI;
            }

            BookingHistory bh = new BookingHistory();
            bh.BookingId = Convert.ToInt32(request.BookingId);
            bh.EventCategory = EventCategory.Ticketing;
            BookingDetail booking = new BookingDetail(Convert.ToInt32(request.BookingId));
           
            bh.CreatedBy = UserMasterId;
            string type = string.Empty;
            TransactionScope setTransaction = new TransactionScope();
            try
            {
                using (setTransaction)
                {
                    if (requestType == CT.BookingEngine.RequestType.Refund)
                    {
                        type = "Refund";
                        bh.Remarks = "Request sent from API to refund " + ticket.TicketNumber;
                        booking.SetBookingStatus(BookingStatus.RefundInProgress, Convert.ToInt32(itinerary.CreatedBy));
                    }
                    else if (requestType == CT.BookingEngine.RequestType.Reissuance)
                    {
                        type = "Modification";
                        bh.Remarks = "Request sent from API to modification " + ticket.TicketNumber;
                        booking.SetBookingStatus(BookingStatus.ModificationInProgress, Convert.ToInt32(itinerary.CreatedBy));
                    }
                    else
                    {
                        type = "Void";
                        bh.Remarks = "Request sent from API to void " + ticket.TicketNumber;
                        booking.SetBookingStatus(BookingStatus.VoidInProgress, Convert.ToInt32(itinerary.CreatedBy));
                    }

                    serviceRequest.Save();
                    bh.Save();
                    status.Category = "CR";
                    status.Description = "Successful";
                    status.StatusCode = "12";
                    setTransaction.Complete();
                }
                try
                {
                    //Sending email.
                    Hashtable table = new Hashtable();
                    table.Add("agentName", AgentMaster.Name);
                    table.Add("pnrNo", itinerary.PNR);
                    //table.Add("reqType", requestType.ToString());

                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                    AgentMaster bookedAgency = new AgentMaster(itinerary.AgencyId);
                    if (bookedBy.Email != null)
                    {
                        toArray.Add(bookedBy.Email);
                    }
                    toArray.Add(bookedAgency.Email1);
                    toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);

                    string message = string.Empty;
                    if (type == "Modification")
                    {
                        message = ConfigurationManager.AppSettings["FLIGHT_CANCEL_REQUEST"];
                    }
                    else if (type == "Refund")
                    {
                        message = ConfigurationManager.AppSettings["FLIGHT_REFUND_REQUEST"];
                    }
                    else
                    {
                        message = ConfigurationManager.AppSettings["FLIGHT_VOID_REQUEST"];
                    }
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Flight)Request for " + type + ". PNR No:(" + itinerary.PNR + ")", message, table);
                }
                catch (Exception ex)
                {
                    Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, UserMasterId, "Problem in sending change request email " + ex.ToString(), "");
                }
            }
            catch (ArgumentException ex)
            {
                Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, UserMasterId, "Problem in saving change request " + ex.ToString(), "");
                status.Category = "CR";
                status.Description = "Save change request Fail";
                status.StatusCode = "09";
                response.Status = status;
                setTransaction.Dispose();
                return response;                
            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.High, UserMasterId, "Problem in saving change request " + ex.Message + ex.StackTrace, "");
                status.Category = "CR";
                status.Description = "Save change request Fail";
                status.StatusCode = "10";
                response.Status = status;
                setTransaction.Dispose();
                return response;
            }
            response.Status = status;
            response.ChangeRequestId = serviceRequest.RequestId.ToString();
            return response;
        }
        else
        {
            response.Status = status;
            Audit.AddAuditBookingAPI(EventType.CancellBooking, Severity.Normal, UserMasterId, "Input parameter is incorrect: " + status.Description, "");
            return response;
        }
    }

    private static WSStatus ValidateRequest(WSSendChangeRequest request, int AgentMasterId)
    {
        WSStatus status = new WSStatus();
        if (request.BookingId == null || request.BookingId.Trim() == string.Empty)
        {
            status.Category = "CR";
            status.Description = "Invalid BookingId";
            status.StatusCode = "01";
            return status;
        }

        if (request.RequestType.ToString() == string.Empty)
        {
            status.Category = "CR";
            status.Description = "Invalid RequestType";
            status.StatusCode = "02";
            return status;
        }

        if (request.ticketId <= 0)
        {
            status.Category = "CR";
            status.Description = "Invalid TicketId";
            status.StatusCode = "03";
            return status;
        }        
        int value;

        if (!int.TryParse(request.BookingId, out value))
        {
            status.Category = "CR";
            status.Description = "Invalid BookingId";
            status.StatusCode = "04";
            return status;
        }

        if (!int.TryParse(request.ticketId.ToString(), out value))
        {
            status.Category = "CR";
            status.Description = "Invalid TicketId";
            status.StatusCode = "05";
            return status;
        }        
        
        int coreAgentMasterId = Ticket.GetAgencyIdForTicket(value);
        if(coreAgentMasterId != AgentMasterId)
        {
            status.Category = "CR";
            status.Description = "Invalid TicketId. This ticket doesn't belong to you. TicketId "+ value.ToString();
            status.StatusCode = "07";
            return status;
        }

        BookingDetail bd = new BookingDetail(Convert.ToInt32(request.BookingId));
        if (bd.AgencyId != AgentMasterId)
        {
            status.Category = "CR";
            status.Description = "Invalid BookingId. This Booking doesn't belong to you. BookingId " + request.BookingId;
            status.StatusCode = "06";
            return status;
        }
        status.Category = "CR";
        status.Description = "Input validate successfully";
        status.StatusCode = "08";
        return status;
    }
}
