/// <summary>
/// Summary description for WSFaqs
/// </summary>
public class WSFaqs
{
    private string question;
    private string answer;
    public string Question
    {
        get
        {
            return question;
        }
        set
        {
            question = value;
        }

    }
    public string Answer
    {
        get
        {
            return answer;
        }
        set
        {
            answer = value;
        }
    }
    public WSFaqs()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Setting the properties of the WLAdvertisement class
    /// </summary>
    /// <param name="ad">object of CT.CMS.Advertisement class so as to Populate the properties of WLAdvertisement</param>
    /// <returns></returns>
    //public static WSFaqs ReadFaqs(WhiteLabelFaqs faq)
    //{
    //    WSFaqs wsFaq = new WSFaqs();
    //    wsFaq.Question = faq.Question;
    //    wsFaq.Answer = faq.Answer;
    //    return wsFaq;
    //}
}
