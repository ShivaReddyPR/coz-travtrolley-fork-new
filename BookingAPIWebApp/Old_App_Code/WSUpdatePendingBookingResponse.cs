/// <summary>
/// Summary description for WSUpdatePendingBookingResponse
/// </summary>
public class WSUpdatePendingBookingResponse
{
    private string queueId;
    public string QueueId
    {
        get { return queueId; }
        set { queueId = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public WSUpdatePendingBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
