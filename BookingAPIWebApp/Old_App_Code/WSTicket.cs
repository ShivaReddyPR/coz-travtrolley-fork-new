using System;
using CT.BookingEngine;

/// <summary>
/// Summary description for WSTicket
/// </summary>
public class WSTicket
{
    private int ticketId;
    public int TicketId
    {
        get { return ticketId; }
        set { ticketId = value; }
    }

    private string ticketNumber;
    public string TicketNumber
    {
        get { return ticketNumber; }
        set { ticketNumber = value; }
    }
    private DateTime issueDate;
    public DateTime IssueDate
    {
        get { return issueDate; }
        set { issueDate = value; }
    }

    private string title;
    public string Title
    {
        get { return title; }
        set { title = value; }
    }

    private string firstName;
    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }

    private string lastName;
    public string LastName
    {
        get { return lastName; }
        set { lastName = value; }
    }

    private PassengerType paxType;
    public PassengerType PaxType
    {
        get { return paxType; }
        set { paxType = value; }
    }

    private WSFare fare;
    public WSFare Fare
    {
        get { return fare; }
        set { fare = value; }
    }

    private WSSegAdditionalInfo[] segmentAdditionalInfo;
    public WSSegAdditionalInfo[] SegmentAdditionalInfo
    {
        get { return segmentAdditionalInfo; }
        set { segmentAdditionalInfo = value; }
    }

    private string validatingAirline;
    public string ValidatingAirline
    {
        get { return validatingAirline; }
        set { validatingAirline = value; }
    }

    public WSTicket()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
