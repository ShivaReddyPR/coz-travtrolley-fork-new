using System;
using CT.BookingEngine;

/// <summary>
/// Summary description for FlightRequest
/// </summary>
public class WSSearchRequest
{
    private string origin;
    public string Origin
    {
        get { return origin; }
        set { origin = value; }
    }

    private string destination;
    public string Destination
    {
        get { return destination; }
        set { destination = value; }
    }

    private DateTime departureDate;
    public DateTime DepartureDate
    {
        get { return departureDate; }
        set { departureDate = value; }
    }

    private SearchType type;
    public SearchType Type
    {
        get { return type; }
        set { type = value; }
    }

    private DateTime returnDate;
    public DateTime ReturnDate
    {
        get { return returnDate; }
        set { returnDate = value; }
    }

    private CabinClass cabinClass;
    public CabinClass CabinClass
    {
        get { return cabinClass; }
        set { cabinClass = value; }
    }

    private string preferredCarrier;
    public string PreferredCarrier
    {
        get { return preferredCarrier; }
        set { preferredCarrier = value; }
    }

    private int adultCount;
    public int AdultCount
    {
        get { return adultCount; }
        set { adultCount = value; }
    }

    private int childCount;
    public int ChildCount
    {
        get { return childCount; }
        set { childCount = value; }
    }

    private int infantCount;
    public int InfantCount
    {
        get { return infantCount; }
        set { infantCount = value; }
    }

    private int seniorCount;
    public int SeniorCount
    {
        get { return seniorCount; }
        set { seniorCount = value; }
    }
    bool refundableFares = false;
    
    public bool RefundableFares
    {
        get
        {
            return refundableFares;
        }
        set
        {
            refundableFares = value;
        }
    }
    string maxStops = string.Empty;
    public string MaxStops
    {
        get
        {
            return maxStops;
        }
        set
        {
            maxStops = value;
        }
    }
    //To maintain the search request sources for audit purpose.
    string sources = string.Empty;
    public string Sources
    {
        get { return sources; }

        set { sources = value; }
    }

    private string tokenVariable;

    public string TokenVariable
    {
        get { return tokenVariable; }

        set { tokenVariable = value; }
    }

    
}
