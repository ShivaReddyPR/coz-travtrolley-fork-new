/// <summary>
/// Summary description for WSGetAgentMasterBalanceResponse
/// </summary>
public class WSGetAgentMasterBalanceResponse
{
    private decimal balance;
    public decimal Balance
    {
        get { return balance; }
        set { balance = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public WSGetAgentMasterBalanceResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
