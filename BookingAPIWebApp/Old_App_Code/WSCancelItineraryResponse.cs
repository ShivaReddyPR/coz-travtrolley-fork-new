/// <summary>
/// Summary description for WSCancelItineraryResponse
/// </summary>
public class WSCancelItineraryResponse
{
    private string bookingId;
    public string BookingId
    {
        get { return bookingId; }
        set { bookingId = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public WSCancelItineraryResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
