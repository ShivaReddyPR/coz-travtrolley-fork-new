/// <summary>
/// Summary description for WSSaveDetailsResponse
/// </summary>
public class WSAddBookingResponse
{
    private string referenceId;
    public string ReferenceId
    {
        get { return referenceId; }
        set { referenceId = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public WSAddBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
