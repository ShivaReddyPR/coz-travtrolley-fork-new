﻿using CT.BookingEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Application.Flight.Dto
{
    /// <summary>
    /// To set flight fare rules results data
    /// </summary>
    public class FlightResultRepriceDto
    {
        public Fare[] FlightFares { get; set; }

        public SearchResult SelectedResult { get; set; }

        public string ChangedBookingValues { get; set; }

        public string ErrorMessage { get; set; }
    }
}
