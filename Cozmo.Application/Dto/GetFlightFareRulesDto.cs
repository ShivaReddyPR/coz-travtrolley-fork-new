﻿using CT.BookingEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Application.Flight.Dto
{
    /// <summary>
    /// To set flight fare rules results data
    /// </summary>
    public class GetFlightFareRulesDto
    {
        public List<FareRule> FlightFareRules { get; set; }

        public SearchResult SelectedResult { get; set; }

        public SearchResult RepricedResult { get; set; }
    }
}
