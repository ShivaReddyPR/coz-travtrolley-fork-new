﻿using CT.BookingEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Application.Flight.Dto
{
    public class FlightSSRAssignmentDto
    {
        public FlightItinerary Itinerary { get; set; }

        public BookingResponse BookResponse { get; set; }
    }
}
