﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cozmo.Application.Shared
{
    public static class ApplicationStatic
    {
        public static LoginInfo GetLoginInfo(ApiAgentInfo clsApiAgentInfo)
        {
            LoginInfo clsLInfo = null;
            try
            {
                clsLInfo = LoginInfo.GetLoginInfo(clsApiAgentInfo.LoginUserId);
                clsLInfo.IsOnBehalfOfAgent = clsApiAgentInfo.OnBelahfAgentLoc > 0 || clsLInfo.AgentId != clsApiAgentInfo.AgentId;

                AgentMaster clsAM = new AgentMaster(clsApiAgentInfo.AgentId);

                if (clsLInfo.IsOnBehalfOfAgent)
                {
                    clsLInfo.OnBehalfAgentID = clsApiAgentInfo.AgentId;
                    clsLInfo.OnBehalfAgentLocation = clsApiAgentInfo.OnBelahfAgentLoc;
                    clsLInfo.OnBehalfAgentCurrency = clsAM.AgentCurrency;
                    clsLInfo.OnBehalfAgentDecimalValue = clsAM.DecimalValue;
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = clsAM.AgentCurrency;
                    clsLInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                    clsLInfo.OnBehalfAgentSourceCredentials = AgentMaster.GetAirlineCredentials(clsApiAgentInfo.AgentId);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to get Login info from Hotel web API:" + ex.Message, "");
            }
            return clsLInfo;
        }

        public static string GetLoginCountry(long iLocId)
        {
            string sLoginCntryCode = string.Empty;
            try
            {
                LocationMaster clsLM = new LocationMaster(iLocId);
                sLoginCntryCode = clsLM.CountryCode;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to get Login country code in Hotel web API:" + ex.Message, "");
            }
            return sLoginCntryCode;
        }
    }
}
