﻿namespace Cozmo.Application.Shared
{
    /// <summary>
    /// To pass agent info in api request 
    /// </summary>
    public class ApiAgentInfo
    {
        public int AgentId { get; set; }

        public int LoginUserId { get; set; }

        public int OnBelahfAgentLoc { get; set; }
    }
}
