﻿using Cozmo.Application.Shared;
using Cozmo.Application.Transfer.Dto;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Application.Transfer
{
   public class BookTransfer
    {
        /// <summary>
        /// Query request to get search results
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public TransferItinerary TransferItineraryReq { get; set; }

            public ApiAgentInfo AgentInfo { get; set; }

            public string SessionId { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="SearchFlights"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    MetaSearchEngine clsMS = new MetaSearchEngine();                   
                    clsMS.SettingsLoginInfo = ApplicationStatic.GetLoginInfo(clsQuery.AgentInfo);
                    TransferItinerary TransItinerary = clsQuery.TransferItineraryReq;
                    TransItinerary.LastCancellationDate= TransItinerary.TransferDate.AddDays(-2);
                    Product ItneraryProduct = TransItinerary;
                    int iAgencyId = !clsMS.SettingsLoginInfo.IsOnBehalfOfAgent ? clsMS.SettingsLoginInfo.AgentId : clsMS.SettingsLoginInfo.OnBehalfAgentID;
                    clsMS.SessionId = clsQuery.SessionId;
                    var searchResults = clsMS.Book(ref ItneraryProduct, iAgencyId, BookingStatus.Ready, clsQuery.AgentInfo.LoginUserId);
                    var viewModel = new ViewModel(searchResults, TransItinerary);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(BookingResponse bookingResp,TransferItinerary Itenerary)
            {
                Data = new TransferBookingDto();
                Data.BookingData = bookingResp;
                Data.TransferId = Itenerary.TransferId;
            }
            public TransferBookingDto Data { get; }
        }
    }
}
