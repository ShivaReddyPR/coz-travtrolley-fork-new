﻿using System;
using Cozmo.Application.Shared;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cozmo.Application.Transfer.Dto;

namespace Cozmo.Application.Transfer
{
    public class TransferVoucher
    {
       
            /// <summary>
            /// Query request to get search results
            /// </summary>
            /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
            public class Query : IRequest<ApplicationResponse>
            {
                public int ItineraryId { get; set; }

                public ApiAgentInfo AgentInfo { get; set; }
            }

            /// <summary>
            /// Validation for the <see cref="SearchTransfer"/> payload
            /// </summary>
            /// <seealso cref="AbstractValidator{Query}" />
            public class Validator : AbstractValidator<Query>
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="Validator"/> class.
                /// </summary>
                public Validator(/* Inject things here if needed*/)
                {

                }
            }

            /// <summary>
            /// Actions to perform for this <see cref="Query"/>.
            /// </summary>
            /// <seealso cref="Query" />
            public class Handler : IRequestHandler<Query, ApplicationResponse>
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="Handler"/> class.
                /// </summary>
                public Handler()
                {

                }

                /// <summary>
                /// Handles the specified <see cref="Query"/>.
                /// </summary>
                /// <param name="clsQuery">The request.</param>
                /// <param name="cancellationToken">The cancellation token.</param>
                /// <returns></returns>
                public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
                {
                try
                {
                    MetaSearchEngine clsMS = new MetaSearchEngine();
                    TransferItinerary ItineraryObj = new TransferItinerary();
                    clsMS.SettingsLoginInfo = ApplicationStatic.GetLoginInfo(clsQuery.AgentInfo);                    
                    ItineraryObj.Load(clsQuery.ItineraryId);
                    string AgentName = string.Empty;
                    string AgentPhone = string.Empty;
                    if (ItineraryObj.AgentId != clsMS.SettingsLoginInfo.AgentId)
                    {
                        AgentMaster behalfAgent= new AgentMaster(ItineraryObj.AgentId);
                        AgentName = behalfAgent.Name;
                        AgentPhone = behalfAgent.Phone1;
                    }
                    else
                    {
                        AgentName = clsMS.SettingsLoginInfo.AgentName;
                        AgentPhone = clsMS.SettingsLoginInfo.AgentPhone;
                    }
                    AgentMaster agent = new AgentMaster(ItineraryObj.AgentId);
                    AgentMaster parentAgency = new AgentMaster(ItineraryObj.AgentId, "GetParentData");
                    LocationMaster location = new LocationMaster(clsMS.SettingsLoginInfo.LocationID);
                    var viewModel = new ViewModel(ItineraryObj, AgentName, AgentPhone, clsMS.SettingsLoginInfo.LocationTerms,location.Address, agent,parentAgency);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    return null;
                }
                }
            }

            /// <summary>
            /// A viewmodel to return to the application
            /// </summary>
            public class ViewModel
            {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(TransferItinerary Itinerary, string AgentName, string AgentMobile, string terms, string locAddr,AgentMaster agent,AgentMaster parentAgent)
            {
                Data = new TransferVoucherDto();
                Data.Itinerary = Itinerary;
                Data.AgentName = AgentName;
                Data.AgentMobile = AgentMobile;
                Data.termsAndCondition = terms;
                Data.address = locAddr;
                Data.agent = agent;
                Data.parentAgent = parentAgent;
            }
                public TransferVoucherDto Data { get; }
            }
       
    }
}
