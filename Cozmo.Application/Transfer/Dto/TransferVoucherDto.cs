﻿using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Application.Transfer.Dto
{
    public class TransferVoucherDto
    {
        public TransferItinerary Itinerary { get; set; }
        public string AgentName { get; set; }
        public string AgentMobile { get; set; }
        public string termsAndCondition { get; set; }
        public string address { get; set; }
        public AgentMaster agent { get; set; }
        public AgentMaster parentAgent { get; set; }
    }
}
