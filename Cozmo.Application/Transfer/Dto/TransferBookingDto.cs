﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CT.BookingEngine;

namespace Cozmo.Application.Transfer.Dto
{
   public class TransferBookingDto
    {
        public BookingResponse BookingData { get; set; }
        public int TransferId { get; set; }
    }
}
