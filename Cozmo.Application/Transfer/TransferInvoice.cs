﻿using Cozmo.Application.Shared;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using FluentValidation;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Application.Transfer
{
    public class TransferInvoice
    {
        /// <summary>
        /// Query request to get transfer queue data
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {           
            public int TransferId { get; set; }
            public string ConfirmNo { get; set; }            
            public ApiAgentInfo AgentInfo { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="TransferCancelBooking"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    MetaSearchEngine mse = new MetaSearchEngine();
                    mse.SettingsLoginInfo = ApplicationStatic.GetLoginInfo(clsQuery.AgentInfo);
                    object CancelResponse = mse.TransferInvoiceGenerate(clsQuery.TransferId, clsQuery.AgentInfo.AgentId, clsQuery.ConfirmNo, clsQuery.AgentInfo.LoginUserId);
                    var viewModel = new ViewModel(CancelResponse);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(object data)
            {
                Data = data;
            }
            public object Data { get; }
        }
    }
}
