﻿using Cozmo.Application.Flight.Dto;
using Cozmo.Application.Shared;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Application.Flight
{
    public class FlightResultReprice
    {
        /// <summary>
        /// Query request to get flight reprice results
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public int ResultId { get; set; }

            public SearchRequest FlightSearchReq { get; set; }

            public SearchResult SelectedResult { get; set; }

            public ApiAgentInfo AgentInfo { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="FlightResultReprice"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    //MetaSearchEngine clsMS = new MetaSearchEngine();
                    //clsMS.SettingsLoginInfo = ApplicationStatic.GetLoginInfo(clsQuery.AgentInfo);

                    //string changedBookingValues = string.Empty; string errorMessage = string.Empty;
                    //Fare[] arrFares = new Fare[0];

                    //var liFlightRules = clsMS.FlightResultReprice(clsQuery.ResultId, clsQuery.FlightSearchReq, clsQuery.SelectedResult, ref arrFares, ref changedBookingValues, ref errorMessage);
                    //var viewModel = new ViewModel(clsQuery.SelectedResult, arrFares, changedBookingValues);
                    //return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                    return null;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(SearchResult selectedResult, Fare[] arrFares, string sChangedBookingValues)
            {
                Data = new FlightResultRepriceDto();
                Data.SelectedResult = selectedResult;
                Data.FlightFares = arrFares;
                Data.ChangedBookingValues = sChangedBookingValues;
            }

            public FlightResultRepriceDto Data { get; }
        }
    }
}
