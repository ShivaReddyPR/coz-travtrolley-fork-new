﻿using Cozmo.Application.Flight.Dto;
using Cozmo.Application.Shared;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Application.Flight
{
    public class FlightPNRReprice
    {
        /// <summary>
        /// Query request to get booked flight pnr reprice results
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public int FlightId { get; set; }

            public string PNR { get; set; }

            public string ClientIPAddress { get; set; }

            public ApiAgentInfo AgentInfo { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="FlightPNRReprice"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    MetaSearchEngine clsMS = new MetaSearchEngine();
                    clsMS.SettingsLoginInfo = ApplicationStatic.GetLoginInfo(clsQuery.AgentInfo);
                    clsMS.SessionId = Guid.NewGuid().ToString();

                    var response = clsMS.RepricePNR(clsQuery.FlightId.ToString(), clsQuery.PNR, clsQuery.ClientIPAddress);
                    var viewModel = new ViewModel(response, clsMS.clsURImpresp, clsMS.clsUAPIRepriceResp);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(TicketingResponse clsTicketResp, UAPIdll.Universal46.UniversalRecordImportRsp clsURImpResp, UAPIdll.Air46.AirPriceRsp clsAirPriceRsp)
            {
                Data = new FlightPNRRepriceDto();
                Data.TicketResp = clsTicketResp;
                Data.URImpResp = clsURImpResp;
                Data.AirPriceRsp = clsAirPriceRsp;
            }

            public FlightPNRRepriceDto Data { get; }
        }
    }
}
