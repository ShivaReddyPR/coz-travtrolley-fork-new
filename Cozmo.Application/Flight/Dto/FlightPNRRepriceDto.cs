﻿using CT.BookingEngine;

namespace Cozmo.Application.Flight.Dto
{
    public class FlightPNRRepriceDto
    {
        public TicketingResponse TicketResp { get; set; }

        public UAPIdll.Universal46.UniversalRecordImportRsp URImpResp { get; set; }

        public UAPIdll.Air46.AirPriceRsp AirPriceRsp { get; set; }
    }
}
