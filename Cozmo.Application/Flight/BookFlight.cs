﻿using Cozmo.Application.Flight.Dto;
using Cozmo.Application.Shared;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Application.Flight
{
    public class BookFlight
    {
        /// <summary>
        /// Query request to get flight booking confirmation response
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {
            public string ResultSession { get; set; }

            public BookingStatus BookStatus { get; set; }

            public FlightItinerary Itinerary { get; set; }

            public ApiAgentInfo AgentInfo { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="BookFlight"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    MetaSearchEngine clsMS = new MetaSearchEngine();
                    clsMS.SettingsLoginInfo = ApplicationStatic.GetLoginInfo(clsQuery.AgentInfo);
                    clsMS.SessionId = clsQuery.ResultSession;

                    FlightItinerary clsFI = clsQuery.Itinerary;
                    var UserInfo = new CT.TicketReceipt.BusinessLayer.UserMaster(clsQuery.AgentInfo.LoginUserId);
                    
                    var response = clsMS.Book(ref clsFI, clsFI.AgencyId, clsQuery.BookStatus, UserInfo, true, clsFI.BookUserIP);
                    var viewModel = new ViewModel(clsFI, response);
                    return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(FlightItinerary clsFI, BookingResponse clsBookResp)
            {
                Data = new FlightSSRAssignmentDto();
                Data.Itinerary = clsFI;
                Data.BookResponse = clsBookResp;
            }

            public FlightSSRAssignmentDto Data { get; }
        }
    }
}
