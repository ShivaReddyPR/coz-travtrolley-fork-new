﻿using Cozmo.Application.Shared;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Application.Flight
{
    public class FlightPNRCancel
    {
        /// <summary>
        /// Query request to get flight pnr cancel response
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class Query : IRequest<ApplicationResponse>
        {           
            public FlightItinerary Itinerary { get; set; }

            public ApiAgentInfo AgentInfo { get; set; }
        }

        /// <summary>
        /// Validation for the <see cref="FlightPNRCancel"/> payload
        /// </summary>
        /// <seealso cref="AbstractValidator{Query}" />
        public class Validator : AbstractValidator<Query>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Validator"/> class.
            /// </summary>
            public Validator(/* Inject things here if needed*/)
            {

            }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<Query, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(Query clsQuery, CancellationToken cancellationToken)
            {
                try
                {
                    //MetaSearchEngine clsMS = new MetaSearchEngine();
                    //clsMS.SettingsLoginInfo = ApplicationStatic.GetLoginInfo(clsQuery.AgentInfo);

                    //var response = clsMS.FlightPNRCancel(clsQuery.Itinerary);
                    //var viewModel = new ViewModel(response);
                    //return await Task.FromResult(new ApplicationResponse(viewModel.Data));
                    return null;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// A viewmodel to return to the application
        /// </summary>
        public class ViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewModel"/> class.
            /// </summary>
            public ViewModel(Dictionary<string, string> dcCacelData)
            {
                Data = dcCacelData;
            }

            public Dictionary<string, string> Data { get; }
        }
    }
}
