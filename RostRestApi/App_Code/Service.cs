﻿using System;
//using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using CT.Core;
using System.Xml;
using System.Xml.Serialization;


public class ServiceAuthentication : SoapHeader
{
    public string UserName;
    public string Password;
}


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Service : System.Web.Services.WebService
{
    Guid sessionId;
    string xmlPath = ConfigurationManager.AppSettings["Path"];
    bool userAuth = false;

    public ServiceAuthentication Credential;
    public Service()
    {
        if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["Path"]))
        {
            System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["Path"]);
        }
    }
    
    //Login
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSAuthenticateResponse Authenticate(WSAuthenticateRequest request)
    {

        WSAuthenticateResponse response = new WSAuthenticateResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSAuthenticateRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosAuthnticationRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                if (string.IsNullOrEmpty(request.StaffId))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "User Name should not be empty";
                }
                else if (request.StaffId.Length > 30)
                {
                    response.ErrorMessage = "User Name should not be greater than 30 characters";
                    response.ErrorCode = "003";
                }
                else if (string.IsNullOrEmpty(request.Password))
                {
                    response.ErrorMessage = "Password should not be empty";
                    response.ErrorCode = "003";
                }
                else if (request.Password.Length > 30)
                {
                    response.ErrorMessage = "Password should not be greater than 30 characters";
                    response.ErrorCode = "003";
                }
                else
                {
                    response = WSAuthenticateResponse.IsAuthenticateEmployee(request.StaffId, request.Password);
                }
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    sessionId = Guid.NewGuid();
                    Session["sessionId"] = sessionId;
                    response.SessionId = sessionId.ToString();
                    response.ErrorCode = "000";
                    response.ErrorMessage = "Success";
                    response.SuccessMessage = "Authentication success."; //TODO ZIYAD
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSAuthenticateResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosAuthnticationResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorMessage = "Data does not exist";
            response.ErrorCode = "002";
            Audit.Add(EventType.ROSauthentication, Severity.High, 1, "Failed to Get Authenticate. Error: " + ex.Message, "0");
        }
        return response;
    }
     
    //Registration
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSEmpRegisterResponse Registration(WSEmpRegisterRequest request)
    {
        WSEmpRegisterResponse response = new WSEmpRegisterResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSEmpRegisterRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosRegistrationRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }

        string pattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }

            }
            if (userAuth)
            {
                if (string.IsNullOrEmpty(request.StaffId))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "StaffId should not be empty";
                }
                else if (request.StaffId.Length > 30)
                {
                    response.ErrorMessage = "StaffId should not be greater than 30 characters";
                    response.ErrorCode = "003";
                }
                else if (string.IsNullOrEmpty(request.Password))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Password should not be empty";
                }
                else if (request.Password.Length > 30)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Password should not be greater than 30 characters";
                }
                else if (string.IsNullOrEmpty(request.Title))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Title should not be empty";
                }
                else if (request.Title.Length > 5)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Title should not be greater than 5 characters";
                }
                else if (string.IsNullOrEmpty(request.FirstName))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "FirstName should not be empty";
                }
                else if (request.FirstName.Length > 50)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "FirstName should not be greater than 50 characters";
                }
                else if (string.IsNullOrEmpty(request.LastName))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LastName should not be empty";
                }
                else if (request.FirstName.Length > 50)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LastName should not be greater than 50 characters";
                }
                else if (string.IsNullOrEmpty(request.Email))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Email should not be empty";
                }
                else if (!Regex.IsMatch(request.Email, pattern))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Not a valid Email address";
                }
                else if (request.Email.Length > 30)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Email should not be greater than 30 characters";
                }
                else if (string.IsNullOrEmpty(request.MobileNo))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "MobileNo should not be empty";
                }
                else if (request.MobileNo.Length > 15)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "MobileNo should not be greater than 15 characters";
                }
                else if (!string.IsNullOrEmpty(request.AlternateNo) && request.AlternateNo.Length > 15)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "AlternateNo should not be greater than 15 characters";
                }
                else if (request.LocationFrom < 1)
                {
                    response.ErrorMessage = "003";
                    response.ErrorMessage = "LocationFrom should not be less than 1";
                }
                else if (!string.IsNullOrEmpty(request.LocationFromMap) && request.LocationFromMap.Length > 100)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LocationFromMap should not be greater than 100 characters";
                }
                else if (!string.IsNullOrEmpty(request.LocationFromDetails) && request.LocationFromDetails.Length > 250)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LocationFromDetails should not be greater than 250 characters";
                }
                else if (request.LocationTo < 1)
                {
                    response.ErrorMessage = "003";
                    response.ErrorMessage = "LocationTo should not be less than 1";
                }
                else if (!string.IsNullOrEmpty(request.LocationToMap) && request.LocationToMap.Length > 100)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LocationToMap should not be greater than 100 characters";
                }
                else if (!string.IsNullOrEmpty(request.LocationToDetails) && request.LocationToDetails.Length > 250)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LocationToDetails should not be greater than 250 characters";
                }
                else if (!string.IsNullOrEmpty(request.Designation) && request.Designation.Length > 50)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Designation should not be greater than 50 characters";
                }
                else if (!string.IsNullOrEmpty(request.ImageUrl) && request.ImageUrl.Length > 500)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "ImageUrl should not be greater than 500 characters";
                }
                else
                {
                    response.Company = 2; //Ziyad told should be hard coded 2
                    response.StaffId = request.StaffId;
                    response.Password = request.Password;
                    response.Title = request.Title;
                    response.FirstName = request.FirstName;
                    response.LastName = request.LastName;
                    response.Email = request.Email;
                    response.MobileNo = request.MobileNo;
                    response.AlternateNo = request.AlternateNo;
                    response.StaffType = string.Empty;
                    response.LocationFrom = request.LocationFrom;
                    response.LocationFromDetails = request.LocationFromDetails;
                    response.LocationFromMap = request.LocationFromMap;
                    response.LocationTo = request.LocationTo;
                    response.LocationToMap = request.LocationToMap;
                    response.LocationToDetails = request.LocationToDetails;
                    response.Designation = request.Designation;
                    response.ImageUrl = request.ImageUrl;
                    response.Save();
                    if (string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        sessionId = Guid.NewGuid();
                        Session["sessionId"] = sessionId;
                        response.SessionId = sessionId.ToString();
                        response.Password = string.Empty;
                        response.ErrorCode = "000";
                        response.ErrorMessage = "Success";
                        response.SuccessMessage = "Profile registered successfully.";//TODO ZIYAD
                    }
                    else
                    {
                        string ErrorCode = response.ErrorCode;
                        string ErrorMessage = response.ErrorMessage;
                        response = new WSEmpRegisterResponse();
                        response.ErrorCode = ErrorCode;
                        response.ErrorMessage = ErrorMessage;
                    }
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSEmpRegisterResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosRegistrationResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response = new WSEmpRegisterResponse();
            response.ErrorMessage = "Transaction Failed To Save.";
            response.ErrorCode = "002";
            Audit.Add(EventType.ROSRegistration, Severity.High, 1, "Failed to Get Registration. Error: " + ex.Message, "0");
        }
        return response;
    }

    //getcrewroster 
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSCrewRosterResponse GetCrewRosterListByEmpId(WSCrewRosterRequest request)
    {
        WSCrewRosterResponse response = new WSCrewRosterResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSCrewRosterRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosCrewRosterListRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                if (request.EmpId < 1)
                {
                    response.ErrorMessage = "003";
                    response.ErrorMessage = "EmpId should not be less than 1";
                }
                else
                {
                    response = WSCrewRoster.GetCrewRosterListByEmpId(request.EmpId);
                    if (string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        //response.SessionId = Session["sessionId"].ToString();
                        response.SessionId = request.SessionId;
                        response.ErrorCode = "000";
                        response.ErrorMessage = "success";
                        response.SuccessMessage = "Successfully getting Crew roster List.";//TODO ZIYAD
                    }
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSCrewRosterResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosCrewRosterListResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Data does Not exist";
            Audit.Add(EventType.ROSCrewRosterList, Severity.High, 1, "Failed to Get CrewRosterLis. Error: " + ex.Message, "0");
        }
        return response;
    }

    //CrewRosterStatusUpdate
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSCrewRosterStatusResponse CrewRosterStatusUpdate(WSCrewRosterStatusRequest request)
    {
        WSCrewRosterStatusResponse response = new WSCrewRosterStatusResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSCrewRosterStatusRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosCrewRosterStatusRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                response.UpdateCrewRosterStatus(request);
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    response.SessionId = request.SessionId;
                    response.ErrorCode = "000";
                    response.ErrorMessage = "success";
                    response.SuccessMessage = "Trip status Confirmed.";//TODO ZIYAD
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSCrewRosterStatusResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosCrewRosterStatusResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Transaction Failed To Update !";
            Audit.Add(EventType.ROSCrewRosterStatus, Severity.High, 0, "Failed to get UpdateCrewRosterStatus. Error: " + ex.Message, "0");
        }
        return response;
    }

    //GetDriverDetails
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSChauffeurJobResponse GetChauffeurJobByDriverId(WSChauffeurJobRequest request)
    {
        WSChauffeurJobResponse response = new WSChauffeurJobResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSChauffeurJobRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosChauffeurJobListRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                if (request.DriverId < 1)
                {
                    response.ErrorMessage = "003";
                    response.ErrorMessage = "EmpId should not be less than 1";
                }
                else
                {
                    response = WSChauffeurJob.GetChauffeurJobListByDriverId(request.DriverId);
                    if (string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        response.SessionId = request.SessionId;
                        response.ErrorCode = "000";
                        response.ErrorMessage = "success";
                        response.SuccessMessage = "Successfully getting Chauffeur job List.";//TODO ZIYAD
                    }
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSChauffeurJobResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosChauffeurJobListResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Data does Not exist";
            Audit.Add(EventType.RosChauffeurJobList, Severity.High, 0, "Failed to get ChauffeurJobList. Error: " + ex.Message, "0");
        }
        return response;
    }


    //ChaufferJobStatusUpdate
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSChauffeurJobStatusResponse ChauffeurJobStatusUpdate(WSChauffeurJobStatusRequest request)
    {
        WSChauffeurJobStatusResponse response = new WSChauffeurJobStatusResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSChauffeurJobStatusRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosChauffeurJobStatusRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                response.UpdateChauffeurJobStatus(request);
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    response.SessionId = request.SessionId;
                    response.ErrorCode = "000";
                    response.ErrorMessage = "success";
                    if (request.ChaupperJobStatus[0].DriTripStatus == "P")
                    {
                        response.SuccessMessage = "Trip status modified to Picked.";//TODO ZIYAD
                    }
                    else if (request.ChaupperJobStatus[0].DriTripStatus == "C")
                    {
                        response.SuccessMessage = "Trip Status modified to Completed.";//TODO ZIYAD
                    }
                    else if (request.ChaupperJobStatus[0].DriTripStatus == "R")
                    {
                        response.SuccessMessage = "Trip status modified to Not responding.";//TODO ZIYAD
                    }
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSChauffeurJobStatusResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosChauffeurJobStatusResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Transaction Failed To Update !";
            Audit.Add(EventType.ROSChauffeurJobStatus, Severity.High, 0, "Failed to get ChauffeurJobStatus. Error: " + ex.Message, "0");
        }
        return response;
    }


    //GetAllLocaton
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSLocationMasterResponse GetLocations()
    {
        WSLocationMasterResponse response = new WSLocationMasterResponse();
       
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                response = WSLocationMaster.GetLocationList();
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    response.ErrorCode = "000";
                    response.ErrorMessage = "success";
                    response.SuccessMessage = "Successfully getting Loaction list.";//TODO ZIYAD
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSLocationMasterResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosLocationListResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Data does Not exist";
            Audit.Add(EventType.RosLocationList, Severity.High, 0, "Failed to get LocationList. Error: " + ex.Message, "0");
        }
        return response;
    }

    //SaveFeedBack
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSFeedBackResponse SaveFeedBack(WSFeedBackRequest request)
    {
        WSFeedBackResponse response = new WSFeedBackResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSFeedBackRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosFeedBackRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                if (string.IsNullOrEmpty(request.FbType))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "FeedBack Type should not be empty";
                }
                else if (request.FbType.Length > 1)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "FeedBack Type should not be greater than 1 character";

                }
                else if (request.EmpId < 1)
                {
                    response.ErrorMessage = "003";
                    response.ErrorMessage = "EmpId should not be less than 1";
                }
                else
                {
                    response.SaveFeedBack(request.FbType, request.FbDescription, request.EmpId);
                    if (string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        response.SessionId = request.SessionId;
                        response.ErrorCode = "000";
                        response.ErrorMessage = "success";
                        response.SuccessMessage = "Thank you for your valuable Feedback";//TODO ZIYAD
                    }
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSFeedBackResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosFeedBackResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Transaction Failed To Save !";
            Audit.Add(EventType.RosFeedBack, Severity.High, 0, "Failed to get SaveFeedBack. Error: " + ex.Message, "0");
        }
        return response;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSUpdateCrewRosterDetailsResponse UpdateCrewRosterDetails(WSUpdateCrewRosterDetailsRequest request)
    {
        WSUpdateCrewRosterDetailsResponse response = new WSUpdateCrewRosterDetailsResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSUpdateCrewRosterDetailsRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosUpdateRosterDetailsRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }

            if (userAuth)
            {
                response.UpdateCrewRosterDetails(request);
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    response.SessionId = request.SessionId;
                    response.ErrorCode = "000";
                    response.ErrorMessage = "success";
                    response.SuccessMessage = "Trip details modified and confirmed.";//TODO ZIYAD
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSUpdateCrewRosterDetailsResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosUpdateRosterDetailsResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Transaction Failed To Update !";
            Audit.Add(EventType.RosUpdateCrewRoster, Severity.High, 0, "Failed to get UpdateRosterDetails. Error: " + ex.Message, "0");
        }
        return response;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSPersonalHireResponse SavePersonalHireDetails(WSPersonalHireRequest request)
    {
        WSPersonalHireResponse response = new WSPersonalHireResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSPersonalHireRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosPersonalHireRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                response.SavePersonalHireDetails(request);
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    //response.SessionId = Session["sessionId"].ToString();
                    response.SessionId = request.SessionId;
                    response.ErrorCode = "000";
                    response.ErrorMessage = "success";
                    response.SuccessMessage = "Your Trip details noted, we will get back to you with confirmation.";//TODO ZIYAD
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSPersonalHireResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosPersonalHireResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Transaction Failed To Save !";
            Audit.Add(EventType.RosPersonalHire, Severity.High, 0, "Failed to get PersonalHire. Error: " + ex.Message, "0");
        }

        return response;
    }


    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSGetEmployeeDataResponse GetEmployeeData(WSGetEmployeeDataRequest request)
    {
        WSGetEmployeeDataResponse response = new WSGetEmployeeDataResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSGetEmployeeDataRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosGetEmployeeDataRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                if (request.EmpId < 1)
                {
                    response.ErrorMessage = "003";
                    response.ErrorMessage = "EmpId should not be less than 1";
                }
                else
                {
                    response = WSGetEmployeeDataResponse.GetEmployeeData(request.EmpId);
                }
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    response.SessionId = request.SessionId;
                    response.ErrorCode = "000";
                    response.ErrorMessage = "success";
                    response.SuccessMessage = "Successfully getting employee data.";//TODO ZIYAD
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSGetEmployeeDataResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosGetEmployeeDataResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Data does Not exist";
            Audit.Add(EventType.RosEmpGetData, Severity.High, 0, "Failed to get EmployeeData. Error: " + ex.Message, "0");
        }
        return response;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSUpdateEmployeeDataResponse UpdateEmployeeData(WSUpdateEmployeeDataRequest request)
    {
        WSUpdateEmployeeDataResponse response = new WSUpdateEmployeeDataResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSUpdateEmployeeDataRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosupdateEmployeeRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }

        string pattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }

            }
            if (userAuth)
            {

                if (string.IsNullOrEmpty(request.Title))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Title should not be empty";
                }
                else if (request.Title.Length > 5)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Title should not be greater than 5 characters";
                }
                else if (string.IsNullOrEmpty(request.FirstName))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "FirstName should not be empty";
                }
                else if (request.FirstName.Length > 50)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "FirstName should not be greater than 50 characters";
                }
                else if (string.IsNullOrEmpty(request.LastName))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LastName should not be empty";
                }
                else if (request.FirstName.Length > 50)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LastName should not be greater than 50 characters";
                }
                else if (string.IsNullOrEmpty(request.Email))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Email should not be empty";
                }
                else if (!Regex.IsMatch(request.Email, pattern))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Not a valid Email address";
                }
                else if (request.Email.Length > 30)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Email should not be greater than 30 characters";
                }
                else if (string.IsNullOrEmpty(request.MobileNo))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "MobileNo should not be empty";
                }
                else if (request.MobileNo.Length > 15)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "MobileNo should not be greater than 50 characters";
                }
                else if (!string.IsNullOrEmpty(request.AlternateNo) && request.AlternateNo.Length > 15)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "AlternateNo should not be greater than 50 characters";
                }
                else if (request.LocationFrom < 1)
                {
                    response.ErrorMessage = "003";
                    response.ErrorMessage = "LocationFrom should not be less than 1";
                }
                else if (!string.IsNullOrEmpty(request.LocationFromMap) && request.LocationFromMap.Length > 100)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LocationFromMap should not be greater than 100 characters";
                }
                else if (!string.IsNullOrEmpty(request.LocationFromDetails) && request.LocationFromDetails.Length > 250)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LocationFromDetails should not be greater than 250 characters";
                }
                else if (request.LocationTo < 1)
                {
                    response.ErrorMessage = "003";
                    response.ErrorMessage = "LocationTo should not be less than 1";
                }
                else if (!string.IsNullOrEmpty(request.LocationToMap) && request.LocationToMap.Length > 100)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LocationToMap should not be greater than 100 characters";
                }
                else if (!string.IsNullOrEmpty(request.LocationToDetails) && request.LocationToDetails.Length > 250)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "LocationToDetails should not be greater than 250 characters";
                }
                else if (!string.IsNullOrEmpty(request.Designation) && request.Designation.Length > 50)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "Designation should not be greater than 50 characters";
                }
                else if (!string.IsNullOrEmpty(request.ImageUrl) && request.ImageUrl.Length > 500)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "ImageUrl should not be greater than 500 characters";
                }
                else
                {
                    response.UpdateEmployeeData(request);
                    if (string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        response.SessionId = request.SessionId;
                        response.ErrorCode = "000";
                        response.ErrorMessage = "Success";
                        response.SuccessMessage = "Profile updated successfully.";//TODO ZIYAD
                    }
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSUpdateEmployeeDataResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosUpdateEmployeeResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorMessage = "Transaction Failed To Update.";
            response.ErrorCode = "002";
            Audit.Add(EventType.RosUpdateEmpData, Severity.High, 1, "Failed to Get UpdateEmployee. Error: " + ex.Message, "0");
        }
        return response;
    }


    //GetContactInfo
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSContactInfoResponse GetContactInfo()
    {
        WSContactInfoResponse response = new WSContactInfoResponse();
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                response.GetContactInfo();
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    response.ErrorCode = "000";
                    response.ErrorMessage = "success";
                    response.SuccessMessage = "Successfully getting contactinfo.";//TODO ZIYAD
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSContactInfoResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosContactResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Data does Not exist";
            Audit.Add(EventType.RosContactInfo, Severity.High, 0, "Failed to get ContactInfo. Error: " + ex.Message, "0");
        }
        return response;
    }

    //ForgotPassword
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSForgotPasswordResponse ForgotPassword(WSForgotPasswordRequest request)
    {
        WSForgotPasswordResponse response = new WSForgotPasswordResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSForgotPasswordRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosForgotPasswordRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch { }
        try
        {
            //Service Authentication checking
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                if (string.IsNullOrEmpty(request.StaffId))
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "StaffId should not be empty";
                }
                else if (request.StaffId.Length > 30)
                {
                    response.ErrorCode = "003";
                    response.ErrorMessage = "StaffId should not be greater than 30 characters";
                }
                else
                {
                    response.GetEmailByStaffId(request.StaffId);
                    if (string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        response.ErrorCode = "000";
                        response.ErrorMessage = "success";
                        response.SuccessMessage = "Password Reset Link sent to your email.";//TODO ZIYAD
                    }
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSForgotPasswordResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosForgotPasswordResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Data does Not exist";
            Audit.Add(EventType.RosForgotPassword, Severity.High, 0, "Failed to get ForgotPassword. Error: " + ex.Message, "0");
        }
        return response;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSThumbnailImageResponse GetThumbnailImages()
    {
        WSThumbnailImageResponse response = new WSThumbnailImageResponse();
        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                if (string.IsNullOrEmpty(Credential.UserName) && Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                if (string.IsNullOrEmpty(Credential.Password) && Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = WSServiceAuthenticate.GetServiceAuthenticate(Credential.UserName, Credential.Password);
                }
            }
            if (userAuth)
            {
                response=WSThumbnailImage.GetThumbnailImages();
                if (string.IsNullOrEmpty(response.ErrorMessage))
                {
                    response.ErrorCode = "000";
                    response.ErrorMessage = "success";
                    response.SuccessMessage = "Successfully getting ThumbnailImages.";//TODO ZIYAD
                }
            }
            else
            {
                response.ErrorMessage = "004";
                response.ErrorMessage = "Service Authentaication Failed !";
            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSThumbnailImageResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_RosThumnnailImagesResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            response.ErrorCode = "002";
            response.ErrorMessage = "Data does Not exist";
            Audit.Add(EventType.RosContactInfo, Severity.High, 0, "Failed to get ThumbnailImages. Error: " + ex.Message, "0");
        }
        return response;
    }
}
