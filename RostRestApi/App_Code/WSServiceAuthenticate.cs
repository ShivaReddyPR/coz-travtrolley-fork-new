﻿using System;
using System.Data;
//using System.Linq;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

/// <summary>
/// Summary description for WSServiceAuthenticate
/// </summary>
public class WSServiceAuthenticate
{
    public WSServiceAuthenticate()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static bool GetServiceAuthenticate(string userName, string password)
    {
        bool authenticate = false;
        try
        {
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@P_USENAME", userName);
            paramList[1] = new SqlParameter("@P_PASSWORD", password);
            paramList[2] = new SqlParameter("@P_ISAuthenticate", SqlDbType.Bit, 250);
            paramList[2].Direction = ParameterDirection.Output;
            DBGateway.FillDataTableSP("P_ROS_WS_UserAuthentication", paramList);
            authenticate = Convert.ToBoolean(paramList[2].Value);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return authenticate;
    }
}
