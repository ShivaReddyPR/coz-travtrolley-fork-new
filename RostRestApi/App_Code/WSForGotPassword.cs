﻿using System;
using System.Data;
using System.Configuration;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using System.Collections.Generic;
using CT.Core;

/// <summary>
/// Summary description for WSForGotPassword
/// </summary>
public class WSForgotPasswordRequest
{
    #region Variable Declaration
    string staffId;
    #endregion
    #region properties
    public string StaffId
    {
        set { staffId = value; }
        get { return staffId; }
    }
    #endregion

}

public class WSForgotPasswordResponse
{
    #region Variable Declaration
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion
    #region properties
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion


    public void GetEmailByStaffId(string staffId)
    {
        string guid = string.Empty;
        try
        {
            string email = string.Empty;
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@P_STAFF_ID", staffId);
            paramList[1] = new SqlParameter("@P_EMAIL", SqlDbType.NVarChar, 250);
            paramList[1].Direction = ParameterDirection.Output;
            paramList[2] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
            paramList[2].Direction = ParameterDirection.Output;
            paramList[3] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
            paramList[3].Direction = ParameterDirection.Output;
            guid = Guid.NewGuid().ToString();
            paramList[4] = new SqlParameter("@P_REQUEST_ID", guid);
            DBGateway.FillDataTableSP("P_ROS_WS_GetEmailByStaffId", paramList);
            string messageType = Convert.ToString(paramList[2].Value);
            if (messageType == "E")
            {
                string message = Convert.ToString(paramList[3].Value);
                if (!string.IsNullOrEmpty(message))
                {
                    errorCode = "001";
                    errorMessage = message;
                }
                else
                {
                    errorCode = "001";
                    errorMessage = "StaffId does not exists !";
                }
            }
            else
            {
                email = Utility.ToString(paramList[1].Value);
                if (!string.IsNullOrEmpty(email))
                {
                    //string pagePath = "http://cozmotravel.com/";
                    string pagePath = System.Configuration.ConfigurationManager.AppSettings["ChangePasswordDomain"].ToString();

                    pagePath = pagePath + "ForgotPassword.aspx?requestId=" + guid;

                    List<string> toArray = new List<string>();
                    toArray.Add(email);
                    string link = "<a href='" + pagePath + "'>" + pagePath + "</a>";
                    string message = ConfigurationManager.AppSettings["changePassowrdMessage"].Replace("%link%", link);
                    try
                    {

                        Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Reset your password", message, null);
                    }
                    catch (Exception ex)
                    {
                        errorCode = "001";
                        errorMessage = "Email could not be sent !";
                        Audit.Add(EventType.Email, Severity.Normal, 0, "Smtp is unable to send the message"+ex.Message, "0");
                    }
                }
                else
                {
                    errorCode = "001";
                    errorMessage = "Email does not exists !";
                }
            }
        }
        catch
        {
            throw;
        }
    }
}
