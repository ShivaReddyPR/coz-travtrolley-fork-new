﻿using System;
using System.Data;
using System.Configuration;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Globalization;

/// <summary>
/// Summary description for WSUpdateCrewRosterDetails
/// </summary>
public class WSUpdateCrewRosterDetails
{
    #region Variable Declaration
    int rosId;
    int empId;
    int fromLoc;
    string fromLocDet;
    string fromLocMap;
    int toLoc;
    string toLocDet;
    string toLocMap;
    string date;
    string tripStatus;
    #endregion
    #region properties
    public int RosId
    {
        set { rosId = value; }
        get { return rosId; }
    }
    public int EmpId
    {
        set { empId = value; }
        get { return empId; }
    }
    public int FromLoc
    {
        set { fromLoc = value; }
        get { return fromLoc; }
    }
    public string FromLocDet
    {
        set { fromLocDet = value; }
        get { return fromLocDet; }
    }
    public string FromLocMap
    {
        set { fromLocMap = value; }
        get { return fromLocMap; }

    }
    public int ToLoc
    {
        set { toLoc = value; }
        get { return toLoc; }
    }
    public string ToLocDet
    {
        set { toLocDet = value; }
        get { return toLocDet; }
    }
    public string ToLocMap
    {
        set { toLocMap = value; }
        get { return toLocMap; }
    }
    public string Date
    {
        set { date = value; }
        get { return date; }
    }
    public string TripStatus
    {
        set { tripStatus = value; }
        get { return tripStatus; }
    }
    #endregion
    public WSUpdateCrewRosterDetails()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public class WSUpdateCrewRosterDetailsRequest
{
    #region Variabule Declaration
    string sessionId;
    WSUpdateCrewRosterDetails[] crewRosterDetails;
    #endregion
    #region properties
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public WSUpdateCrewRosterDetails[] CrewRosterDetails
    {
        set { crewRosterDetails = value; }
        get { return crewRosterDetails; }
    }
    #endregion
}
public class WSUpdateCrewRosterDetailsResponse
{
    #region Variabule Declaration
    string rosTripId;
    string sessionId;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public string RosTripId
    {
        set { rosTripId = value; }
        get { return rosTripId; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion

    public void UpdateCrewRosterDetails(WSUpdateCrewRosterDetailsRequest request)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = DBGateway.CreateConnection();
        cmd.Connection.Open();
        SqlTransaction trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
        cmd.Transaction = trans;
        try
        {
            //string rosTrId = string.Empty;
            if (request.CrewRosterDetails.Length > 0)
            {
                for (int i = 0; i < request.CrewRosterDetails.Length; i++)
                {
                    if (request.CrewRosterDetails[i].RosId < 1)
                    {
                        errorCode = "003";
                        errorMessage = "RosterId " + (i + 1) + " should not be less than 1";
                    }
                    else if (request.CrewRosterDetails[i].EmpId < 1)
                    {
                        errorCode = "003";
                        errorMessage = "Emp Id " + (i + 1) + " should not be less than 1";
                    }
                    else if (request.CrewRosterDetails[i].FromLoc < 1)
                    {
                        errorCode = "003";
                        errorMessage = "FromLoc " + (i + 1) + " should not be less than 1";
                    }
                    else if (request.CrewRosterDetails[i].ToLoc < 1)
                    {
                        errorCode = "003";
                        errorMessage = "ToLoc " + (i + 1) + " should not be less than 1";
                    }
                    else if (!string.IsNullOrEmpty(request.CrewRosterDetails[i].FromLocMap) && request.CrewRosterDetails[i].FromLocMap.Length > 100)
                    {
                        errorCode = "003";
                        errorMessage = "LocationFromMap " + (i + 1) + "  should not be greater than 100 characters";
                    }
                    else if (!string.IsNullOrEmpty(request.CrewRosterDetails[i].FromLocDet) && request.CrewRosterDetails[i].FromLocDet.Length > 250)
                    {
                        errorCode = "003";
                        errorMessage = "LocationFromDet " + (i + 1) + "  should not be greater than 250 characters";
                    }
                    else if (!string.IsNullOrEmpty(request.CrewRosterDetails[i].ToLocMap) && request.CrewRosterDetails[i].ToLocMap.Length > 100)
                    {
                        errorCode = "003";
                        errorMessage = "LocationToMap " + (i + 1) + "  should not be greater than 100 characters";
                    }
                    else if (!string.IsNullOrEmpty(request.CrewRosterDetails[i].ToLocDet) && request.CrewRosterDetails[i].ToLocDet.Length > 250)
                    {
                        errorCode = "003";
                        errorMessage = "LocationToDet " + (i + 1) + "  should not be greater than 250 characters";
                    }
                    else if (string.IsNullOrEmpty(request.CrewRosterDetails[i].Date))
                    {
                        errorCode = "003";
                        errorMessage = "Date " + (i + 1) + "  should not Empty";
                    }
                    else if (string.IsNullOrEmpty(request.CrewRosterDetails[i].TripStatus))
                    {
                        errorCode = "003";
                        errorMessage = "Trip Status " + (i + 1) + " should not be Empty";
                    }
                    else if (request.CrewRosterDetails[i].TripStatus.Length > 1)
                    {
                        errorCode = "003";
                        errorMessage = "Trip Status " + (i + 1) + " should not be greater than 1 characters";
                    }
                    else
                    {
                        DateTime rosDate;
                        try
                        {
                            if (request.CrewRosterDetails[i].Date.ToUpper().Contains("A.M.")) //Android 6.1 version coming a.m. 
                            {
                                request.CrewRosterDetails[i].Date = request.CrewRosterDetails[i].Date.ToUpper().Replace("A.M.", "AM");//Android 6.1 version coming a.m.  to replaced AM
                            }
                            else if (request.CrewRosterDetails[i].Date.ToUpper().Contains("P.M."))
                            {
                                request.CrewRosterDetails[i].Date = request.CrewRosterDetails[i].Date.ToUpper().Replace("P.M.", "PM");
                            }
                            rosDate = DateTime.ParseExact(request.CrewRosterDetails[i].Date, "dd/MM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            DateTime currentDate = DateTime.Now;
                            int minutes = Convert.ToInt32(ConfigurationManager.AppSettings["TimeInterval"]);
                            if ((rosDate - currentDate).TotalMinutes < minutes)
                            {
                                errorCode = "003";
                                errorMessage = "Pickup time should be greater than 3 Hours.";
                                break;
                            }
                        }
                        catch { errorCode = "003"; errorMessage = "Invalid Roster Date Format. Valid Format is dd/MM/yyyy hh:mm:ss tt"; break; }
                        //if (i == 0)
                        //{
                        //    //TO DO ZIYAD
                        //    Random r = new Random();
                        //    int n = r.Next(10000, 99999);
                        //    rosTrId = "TR" + n + request.CrewRosterDetails[i].RosId;
                        //}
                        SqlParameter[] paramList = new SqlParameter[10];
                        paramList[0] = new SqlParameter("@P_ROS_ID", request.CrewRosterDetails[i].RosId);
                        paramList[1] = new SqlParameter("@P_EMP_ID", request.CrewRosterDetails[i].EmpId);
                        paramList[2] = new SqlParameter("@P_ROS_FROM_LOC", request.CrewRosterDetails[i].FromLoc);
                        paramList[3] = new SqlParameter("@P_ROS_FROM_LOC_DET", request.CrewRosterDetails[i].FromLocDet);
                        paramList[4] = new SqlParameter("@P_ROS_FROM_LOC_MAP", request.CrewRosterDetails[i].FromLocMap);
                        paramList[5] = new SqlParameter("@P_ROS_TO_LOC", request.CrewRosterDetails[i].ToLoc);
                        paramList[6] = new SqlParameter("@P_ROS_TO_LOC_DET", request.CrewRosterDetails[i].ToLocDet);
                        paramList[7] = new SqlParameter("@P_ROS_TO_LOC_MAP", request.CrewRosterDetails[i].ToLocMap);
                        paramList[8] = new SqlParameter("@P_ROS_DATE", rosDate);
                        //paramList[9] = new SqlParameter("@P_ROS_TRIPID", rosTrId);
                        paramList[9] = new SqlParameter("@P_ROS_TRIP_STATUS", request.CrewRosterDetails[i].TripStatus);
                        int updated = DBGateway.ExecuteNonQueryDetails(cmd, "P_ROS_WS_UpdateCrewRosterDetails", paramList);
                        if (updated == 0)
                        {
                            errorCode = "001";
                            errorMessage = "Transaction failed to update !";
                            trans.Rollback();
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        trans.Rollback();
                        break;
                    }
                }
            }
            else
            {
                errorCode = "001";
                errorMessage = "Invalid Request";
                trans.Rollback();
            }
            if (string.IsNullOrEmpty(errorMessage))
            {
                //rosTripId = rosTrId;
                rosTripId = string.Empty; // Temp TODO ZIYA
                trans.Commit();
            }
        }
        catch
        {
            trans.Rollback();
            throw;
        }
        finally
        {
            cmd.Connection.Close();
        }
    }
}
