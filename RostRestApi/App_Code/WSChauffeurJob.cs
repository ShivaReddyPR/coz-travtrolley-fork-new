﻿using System;
using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSChauffeurJobRequest
/// </summary>
public class WSChauffeurJobRequest
{
    #region Variable Declaration
    int driverId;
    string sessionId;
    #endregion

    #region properties
    public int DriverId
    {
        set { driverId = value; }
        get { return driverId; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
     
    #endregion
    public WSChauffeurJobRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public class WSChauffeurJob
{
    #region Variable Declaration
    int id;
    string fromType;
    int fromLoc;
    string fromLocDet;
    string fromLocMap;
    string toType;
    int toLoc;
    string toLocDet;
    string toLocMap;
    string date;
    string time;
    string fromLocDescription;
    string toLocDescription;
    string fromTypeDescription;
    string toTypeDescription;
    string driverStatus;
    string driverStatusDesc;
    string driverTripId;
    /* Crew Details   */

    int crewId;
    string crewName;
    string crewMobile;
    string crewAlterNo;
    string crewStatus;
    string crewStatusDescription;
    string crewTripId;
    #endregion

    #region properties
    public int Id
    {
        set { id = value; }
        get { return id; }
    }
    public int FromLoc
    {
        set { fromLoc = value; }
        get { return fromLoc; }
    }
    public string FromLocDet
    {
        set { fromLocDet = value; }
        get { return fromLocDet; }
    }
    public string FromLocMap
    {
        set { fromLocMap = value; }
        get { return fromLocMap; }
    }
    public int ToLoc
    {
        set { toLoc = value; }
        get { return toLoc; }
    }
    public string ToLocDet
    {
        set { toLocDet = value; }
        get { return toLocDet; }
    }
    public string ToLocMap
    {
        set { toLocMap = value; }
        get { return toLocMap; }
    }
    public string Date
    {
        set { date = value; }
        get { return date; }
    }
    public string Time
    {
        set { time = value; }
        get { return time; }

    }
    public string FromType
    {
        set { fromType = value; }
        get { return fromType; }
    }
    public string ToType
    {
        set { toType = value; }
        get { return toType; }
    }
    public string FromLocDescription
    {
        set { fromLocDescription = value; }
        get { return fromLocDescription; }
    }
    public string ToLocDescription
    {
        set { toLocDescription = value; }
        get { return toLocDescription; }
    }
    public string FromTypeDescription
    {
        set { fromTypeDescription = value; }
        get { return fromTypeDescription; }
    }
    public string ToTypeDescription
    {
        set { toTypeDescription = value; }
        get { return toTypeDescription; }
    }
    public string DriverStatus
    {
        set { driverStatus = value; }
        get { return driverStatus; }
    }
    public string DriverStatusDesc
    {
        set { driverStatusDesc = value; }
        get { return driverStatusDesc; }
    }
    public string DriverTripId
    {
        set { driverTripId = value; }
        get { return driverTripId; }
    }
    public int CrewId
    {
        set { crewId = value; }
        get { return crewId; }
    }
    public string CrewName
    {
        set { crewName= value; }
        get { return crewName; }
    }

    public string CrewMobile
    {
        set { crewMobile= value; }
        get { return crewMobile; }
    }

    public string CrewAlterNo
    {
        set { crewAlterNo = value; }
        get { return crewAlterNo; }
    }

    public string CrewStatus
    {
        set { crewStatus = value; }
        get { return crewStatus; }
    }
    public string CrewStatusDescription
    {
        set { crewStatusDescription= value; }
        get { return crewStatusDescription; }
    }
    public string CrewTripId
    {
        set { crewTripId = value; }
        get { return crewTripId; }
    }

    
    

    #endregion
    public static WSChauffeurJobResponse GetChauffeurJobListByDriverId(int driverId)
    {
        WSChauffeurJobResponse response = new WSChauffeurJobResponse();

        try
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_DRIVER_ID", driverId);
            DataTable dt = DBGateway.ExecuteQuery("P_ROS_WS_GetChauffeurJobListByDriverId", paramList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                WSChauffeurJob[] chaufferJobArray = new WSChauffeurJob[dt.Rows.Count];
                int k = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    WSChauffeurJob chaufferJob = new WSChauffeurJob();
                    if (dr["ROS_FROM_LOC"] != DBNull.Value)
                    {
                        chaufferJob.fromLoc = Convert.ToInt32(dr["ROS_FROM_LOC"]);
                    }
                    else
                    {
                        chaufferJob.fromLoc = 0;
                    }
                    if (dr["ROS_FROM_LOC_DET"] != DBNull.Value)
                    {
                        chaufferJob.fromLocDet = Convert.ToString(dr["ROS_FROM_LOC_DET"]);
                    }
                    else
                    {
                        chaufferJob.fromLocDet = string.Empty;
                    }
                    if (dr["ROS_FROM_LOC_MAP"] != DBNull.Value)
                    {
                        chaufferJob.fromLocMap = Convert.ToString(dr["ROS_FROM_LOC_MAP"]);
                    }
                    else
                    {
                        chaufferJob.fromLocMap = string.Empty;
                    }
                    if (dr["ROS_TO_LOC"] != DBNull.Value)
                    {
                        chaufferJob.toLoc = Convert.ToInt32(dr["ROS_TO_LOC"]);
                    }
                    else
                    {
                        chaufferJob.toLoc = 0;
                    }
                    if (dr["ROS_TO_LOC_DET"] != DBNull.Value)
                    {
                        chaufferJob.toLocDet = Convert.ToString(dr["ROS_TO_LOC_DET"]);
                    }
                    else
                    {
                        chaufferJob.toLocDet = string.Empty;

                    }
                    if (dr["ROS_TO_LOC_MAP"] != DBNull.Value)
                    {
                        chaufferJob.toLocMap = Convert.ToString(dr["ROS_TO_LOC_MAP"]);
                    }
                    else
                    {
                        chaufferJob.toLocMap = string.Empty;
                    }
                    if (dr["ROS_DATE"] != DBNull.Value)
                    {
                        chaufferJob.date = Convert.ToDateTime(dr["ROS_DATE"]).ToString("dd/MM/yyyy");
                        //chaufferJob.time = Convert.ToDateTime(dr["ROS_DATE"]).ToString("hh:mm:ss");
                        chaufferJob.time = Convert.ToDateTime(dr["ROS_DATE"]).ToString("hh:mm tt");
                        //rosterDetails.time = Convert.ToDateTime(dr["ROS_DATE"]).ToString("hh:mm tt");
                    }
                    else
                    {
                        chaufferJob.date = string.Empty;
                        chaufferJob.time = string.Empty;
                    }
                    if (dr["ROS_ID"] != DBNull.Value)
                    {
                        chaufferJob.id = Convert.ToInt32(dr["ROS_ID"]);
                    }
                    else
                    {
                        chaufferJob.id = 0;
                    }
                    if (dr["ROS_FROM_TYPE"] != DBNull.Value)
                    {
                        chaufferJob.fromType = Convert.ToString(dr["ROS_FROM_TYPE"]);
                    }
                    else
                    {
                        chaufferJob.fromType = string.Empty;
                    }
                    if (dr["ROS_TO_TYPE"] != DBNull.Value)
                    {
                        chaufferJob.toType = Convert.ToString(dr["ROS_TO_TYPE"]);
                    }
                    else
                    {
                        chaufferJob.toType = string.Empty;
                    }
                    if (dr["locFromName"] != DBNull.Value)
                    {
                        chaufferJob.fromLocDescription = Convert.ToString(dr["locFromName"]);
                    }
                    else
                    {
                        chaufferJob.fromLocDescription = string.Empty;
                    }
                    if (dr["locToName"] != DBNull.Value)
                    {
                        chaufferJob.toLocDescription = Convert.ToString(dr["locToName"]);
                    }
                    else
                    {
                        chaufferJob.toLocDescription = string.Empty;
                    }
                    if (dr["FromTypeDesc"] != DBNull.Value)
                    {
                        chaufferJob.fromTypeDescription = Convert.ToString(dr["FromTypeDesc"]);
                    }
                    else
                    {
                        chaufferJob.fromTypeDescription = string.Empty;
                    }
                    if (dr["ToTypeDesc"] != DBNull.Value)
                    {
                        chaufferJob.toTypeDescription = Convert.ToString(dr["ToTypeDesc"]);
                    }
                    else
                    {
                        chaufferJob.toTypeDescription = string.Empty;
                    }
                    if (dr["ROS_DRIVER_STATUS"] != DBNull.Value)
                    {
                        chaufferJob.driverStatus = Convert.ToString(dr["ROS_DRIVER_STATUS"]);
                    }
                    else
                    {
                        chaufferJob.driverStatus = string.Empty;
                    }
                    if (dr["DriverStatusDesc"] != DBNull.Value)
                    {
                        chaufferJob.driverStatusDesc = Convert.ToString(dr["DriverStatusDesc"]);
                    }
                    else
                    {
                        chaufferJob.driverStatusDesc = string.Empty;
                    }
                    if (dr["ROS_DRIVER_TRIPID"] != DBNull.Value)
                    {
                        chaufferJob.driverTripId = Convert.ToString(dr["ROS_DRIVER_TRIPID"]);
                    }
                    else
                    {
                        chaufferJob.driverTripId = string.Empty;
                    }

                    if (dr["ROS_EMP_ID"] != DBNull.Value)
                    {
                        chaufferJob.crewId = Convert.ToInt32(dr["ROS_EMP_ID"]);
                    }
                    else
                    {
                        chaufferJob.crewId = -1;
                    }

                    if (dr["EMP_NAME"] != DBNull.Value)
                    {
                        chaufferJob.crewName = Convert.ToString(dr["EMP_NAME"]);
                    }
                    else
                    {
                        chaufferJob.crewName = string.Empty;
                    }

                    if (dr["MOBILENO"] != DBNull.Value)
                    {
                        chaufferJob.crewMobile = Convert.ToString(dr["MOBILENO"]);
                    }
                    else
                    {
                        chaufferJob.crewMobile = string.Empty;
                    }
                    if (dr["ALTERNATENO"] != DBNull.Value)
                    {
                        chaufferJob.crewAlterNo = Convert.ToString(dr["ALTERNATENO"]);
                    }
                    else
                    {
                        chaufferJob.crewAlterNo = string.Empty;
                    }
                    if (dr["ROS_TRIPID"] != DBNull.Value)
                    {
                        chaufferJob.crewTripId = Convert.ToString(dr["ROS_TRIPID"]);
                    }
                    else
                    {
                        chaufferJob.crewTripId = string.Empty;
                    }

                    if (dr["ROS_TRIP_STATUS"] != DBNull.Value)
                    {
                        chaufferJob.crewStatus = Convert.ToString(dr["ROS_TRIP_STATUS"]);
                    }
                    else
                    {
                        chaufferJob.crewStatus = string.Empty;
                    }

                    if (dr["ROS_STATUS_DESCRIPTION"] != DBNull.Value)
                    {
                        chaufferJob.crewStatusDescription = Convert.ToString(dr["ROS_STATUS_DESCRIPTION"]);
                    }
                    else
                    {
                        chaufferJob.crewStatusDescription = string.Empty;
                    }

                    chaufferJobArray[k] = chaufferJob;
                    k++;
                }
                response.ChaufferJob = chaufferJobArray;
            }
            else
            {
                response.ErrorCode = "002";
                response.ErrorMessage = "Data does Not exist";
            }

        }
        catch { throw; }
        return response;
    }
}

public class WSChauffeurJobResponse
{
    #region Variable Declaration
    WSChauffeurJob[] chaufferJob;
    string sessionId;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public WSChauffeurJob[] ChaufferJob
    {
        set { chaufferJob = value; }
        get { return chaufferJob; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion
}
