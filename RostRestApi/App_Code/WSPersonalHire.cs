﻿using System;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Globalization;
/// <summary>
/// Summary description for WSPersonalHire
/// </summary>
public class WSPersonalHireRequest
{
    #region Variable Declaration
    int empId;
    int fromLoc;
    string fromLocDet;
    string fromLocMap;
    int toLoc;
    string toLocDet;
    string toLocMap;
    string date;
    string fromType;
    string toType;
    string tripStatus;
    string sessionId;
    #endregion
    #region properties
    public int EmpId
    {
        set { empId = value; }
        get { return empId; }
    }
    public int FromLoc
    {
        set { fromLoc = value; }
        get { return fromLoc; }
    }
    public string FromLocDet
    {
        set { fromLocDet = value; }
        get { return fromLocDet; }
    }
    public string FromLocMap
    {
        set { fromLocMap = value; }
        get { return fromLocMap; }

    }
    public int ToLoc
    {
        set { toLoc = value; }
        get { return toLoc; }
    }
    public string ToLocDet
    {
        set { toLocDet = value; }
        get { return toLocDet; }
    }
    public string ToLocMap
    {
        set { toLocMap = value; }
        get { return toLocMap; }
    }
    public string Date
    {
        set { date = value; }
        get { return date; }
    }
    public string FromType
    {
        set { fromType = value; }
        get { return fromType; }
    }
    public string ToType
    {
        set { toType = value; }
        get { return toType; }
    }
    public string TripStatus
    {
        set { tripStatus = value; }
        get { return tripStatus; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    #endregion
    public WSPersonalHireRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public class WSPersonalHireResponse
{
    #region Variabule Declaration
    string rosTripId;
    string sessionId;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public string RosTripId
    {
        set { rosTripId = value; }
        get { return rosTripId; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion
    public void SavePersonalHireDetails(WSPersonalHireRequest request)
    {
        try
        {
            string rosTrId = string.Empty;

            if (request.EmpId < 1)
            {
                errorCode = "003";
                errorMessage = "Emp Id should not be less than 1";
            }
            else if (request.FromLoc < 1)
            {
                errorCode = "003";
                errorMessage = "FromLoc should not be less than 1";
            }
            else if (request.ToLoc < 1)
            {
                errorCode = "003";
                errorMessage = "ToLoc should not be less than 1";
            }
            else if (!string.IsNullOrEmpty(request.FromLocMap) && request.FromLocMap.Length > 100)
            {
                errorCode = "003";
                errorMessage = "LocationFromMap should not be greater than 100 characters";
            }
            else if (!string.IsNullOrEmpty(request.FromLocDet) && request.FromLocDet.Length > 250)
            {
                errorCode = "003";
                errorMessage = "LocationFromDet should not be greater than 250 characters";
            }
            else if (!string.IsNullOrEmpty(request.ToLocMap) && request.ToLocMap.Length > 100)
            {
                errorCode = "003";
                errorMessage = "LocationToMap should not be greater than 100 characters";
            }
            else if (!string.IsNullOrEmpty(request.ToLocDet) && request.ToLocDet.Length > 250)
            {
                errorCode = "003";
                errorMessage = "LocationToDet should not be greater than 250 characters";
            }
            else if (string.IsNullOrEmpty(request.FromType))
            {
                errorCode = "003";
                errorMessage = "FromType should not be empty";
            }
            else if (request.FromType.Length > 1)
            {
                errorCode = "003";
                errorMessage = "FromType should not be greater than 1 characters";
            }
            else if (string.IsNullOrEmpty(request.ToType))
            {
                errorCode = "003";
                errorMessage = "ToType should not be empty";
            }
            else if (request.ToType.Length > 1)
            {
                errorCode = "003";
                errorMessage = "ToType should not be greater than 1 characters";
            }
            else if (request.Date == null)
            {
                errorCode = "003";
                errorMessage = "Date should not Empty";
            }
            else if (string.IsNullOrEmpty(request.TripStatus))
            {
                errorCode = "003";
                errorMessage = "Trip Status should not be Empty";
            }
            else if (request.TripStatus.Length > 1)
            {
                errorCode = "003";
                errorMessage = "Trip Status should not be greater than 1 characters";
            }
            else
            {

                DateTime rosDate;
                try
                {
                    rosDate = DateTime.ParseExact(request.Date, "dd/MM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                }
                catch { errorCode = "003"; errorMessage = "Invalid Roster Date Format. Valid Format is dd/MM/yyyy hh:mm:ss tt"; return; }
                //Random r = new Random();
                //int n = r.Next(10000, 99999);
                //rosTrId = "TR" + n;

                SqlParameter[] paramList = new SqlParameter[12];
                paramList[0] = new SqlParameter("@P_EMP_ID", request.EmpId);
                paramList[1] = new SqlParameter("@P_ROS_FROM_LOC", request.FromLoc);
                if(!string.IsNullOrEmpty(request.FromLocDet)) paramList[2] = new SqlParameter("@P_ROS_FROM_LOC_DET", request.FromLocDet);
                if (!string.IsNullOrEmpty(request.FromLocMap)) paramList[3] = new SqlParameter("@P_ROS_FROM_LOC_MAP", request.FromLocMap);
                paramList[4] = new SqlParameter("@P_ROS_TO_LOC", request.ToLoc);
                if (!string.IsNullOrEmpty(request.ToLocDet)) paramList[5] = new SqlParameter("@P_ROS_TO_LOC_DET", request.ToLocDet);
                if (!string.IsNullOrEmpty(request.ToLocMap)) paramList[6] = new SqlParameter("@P_ROS_TO_LOC_MAP", request.ToLocMap);
                paramList[7] = new SqlParameter("@P_ROS_DATE", rosDate);
                //paramList[8] = new SqlParameter("@P_ROS_TRIPID", rosTrId);
                paramList[8] = new SqlParameter("@P_ROS_FROM_TYPE", request.FromType);
                paramList[9] = new SqlParameter("@P_ROS_TO_TYPE", request.ToType);
                paramList[10] = new SqlParameter("@P_ROS_CREATED_BY", request.EmpId);
                //paramList[12] = new SqlParameter("@P_ROS_TRIPID_RET", SqlDbType.VarChar, 200);
                //paramList[12].Direction = ParameterDirection.Output;
                paramList[11] = new SqlParameter("@P_ROS_TRIP_STATUS", request.TripStatus);
                int insert= DBGateway.ExecuteNonQuery("P_ROS_WS_SavePersonalHireRequest", paramList);
                if (insert == 0)
                {
                    errorCode = "002";
                    errorMessage = "Transaction Failed To Save";
                }
                //rosTripId = Convert.ToString(paramList[12].Value); //Temp TODO ZIYA
            }
        }
        catch { throw; }
    }
}

