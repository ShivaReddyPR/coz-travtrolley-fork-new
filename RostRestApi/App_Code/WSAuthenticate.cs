﻿using System;
using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSAuthenticate
/// </summary>
public class WSAuthenticateRequest
{
    #region Variable Declaration
    string staffId;
    string password;

    #endregion

    #region properties
    public string StaffId
    {
        set { staffId = value; }
        get { return staffId; }
    }
    public string Password
    {
        set { password = value; }
        get { return password; }
    }
    #endregion
    public WSAuthenticateRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public class WSAuthenticateResponse
{
    #region Variabule Declaration

    int empId;
    int company;
    string staffId;
    string title;
    string firstName;
    string lastName;
    string email;
    string mobileNo;
    string alternateNo;
    string staffType;
    string designation;
    int locationFrom;
    string locationFromDetails;
    string locationFromMap;
    string locationFromDescription;
    int locationTo;
    string locationToDetails;
    string locationToMap;
    string locationToDescription;
    string sessionId;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public int EmpId
    {
        set { empId = value; }
        get { return empId; }
    }
    public int Company
    {
        set { company = value; }
        get { return company; }
    }
    public string StaffId
    {
        set { staffId = value; }
        get { return staffId; }
    }
    public string Title
    {
        set { title = value; }
        get { return title; }
    }
    public string FirstName
    {
        set { firstName = value; }
        get { return firstName; }

    }
    public string LastName
    {
        set { lastName = value; }
        get { return lastName; }
    }
    public string Email
    {
        set { email = value; }
        get { return email; }
    }
    public string MobileNo
    {
        set { mobileNo = value; }
        get { return mobileNo; }
    }
    public string AlternateNo
    {
        set { alternateNo = value; }
        get { return alternateNo; }
    }
    public string StaffType
    {
        set { staffType = value; }
        get { return staffType; }
    }

    public string Designation
    {
        set { designation = value; }
        get { return designation; }
    }
    public int LocationFrom
    {
        set { locationFrom = value; }
        get { return locationFrom; }
    }
    public string LocationFromDetails
    {
        set { locationFromDetails = value; }
        get { return locationFromDetails; }
    }
    public string LocationFromMap
    {
        set { locationFromMap = value; }
        get { return locationFromMap; }
    }
    public string LocationFromDescription
    {
        set { locationFromDescription = value; }
        get { return locationFromDescription; }
    }
    public int LocationTo
    {
        set { locationTo = value; }
        get { return locationTo; }
    }
    public string LocationToDetails
    {
        set { locationToDetails = value; }
        get { return locationToDetails; }
    }
    public string LocationToMap
    {
        set { locationToMap = value; }
        get { return locationToMap; }
    }
    public string LocationToDescription
    {
        set { locationToDescription = value; }
        get { return locationToDescription; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion

    public static WSAuthenticateResponse IsAuthenticateEmployee(string loginName, string password)
    {
        WSAuthenticateResponse response = new WSAuthenticateResponse();
        try
        {
            SqlParameter[] paramList = new SqlParameter[4];

            paramList[0] = new SqlParameter("@P_Staff_ID", loginName);
            paramList[1] = new SqlParameter("@P_PASSWORD", password);
            paramList[2] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
            paramList[2].Direction = ParameterDirection.Output;
            paramList[3] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
            paramList[3].Direction = ParameterDirection.Output;

            DataSet ds = DBGateway.ExecuteQuery("P_ROS_WS_EmpAuthentication", paramList);
            string messageType = Convert.ToString(paramList[2].Value);
            if (messageType == "E")
            {
                string message = Convert.ToString(paramList[3].Value);
                if (!string.IsNullOrEmpty(message))
                {
                    response.errorCode = "001";
                    response.errorMessage = message;
                }
                else
                {
                    response.errorCode = "001";
                    response.errorMessage = "UserName/password does not exist";
                }
            }
            else
            {
                if (ds != null & ds.Tables.Count != 0)
                {
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (dr["emp_Id"] != DBNull.Value)
                        {
                            response.empId = Convert.ToInt32(dr["emp_Id"]);
                        }
                        else
                        {
                            response.empId = 0;
                        }
                        if (dr["company"] != DBNull.Value)
                        {
                            response.company = Convert.ToInt32(dr["company"]);
                        }
                        else
                        {
                            response.company = 0;
                        }
                        if (dr["staff_id"] != DBNull.Value)
                        {
                            response.staffId = Convert.ToString(dr["staff_id"]);
                        }
                        else
                        {
                            response.staffId = string.Empty;
                        }
                        if (dr["title"] != DBNull.Value)
                        {
                            response.title = Convert.ToString(dr["title"]);
                        }
                        else
                        {
                            response.title = string.Empty;

                        }
                        if (dr["firstName"] != DBNull.Value)
                        {
                            response.firstName = Convert.ToString(dr["firstName"]);
                        }
                        else
                        {
                            response.firstName = string.Empty;
                        }
                        if (dr["lastName"] != DBNull.Value)
                        {
                            response.lastName = Convert.ToString(dr["lastName"]);
                        }
                        else
                        {
                            response.lastName = string.Empty;
                        }
                        if (dr["email"] != DBNull.Value)
                        {
                            response.email = Convert.ToString(dr["email"]);
                        }
                        else
                        {
                            response.email = string.Empty;
                        }
                        if (dr["mobileNo"] != DBNull.Value)
                        {
                            response.mobileNo = Convert.ToString(dr["mobileNo"]);
                        }
                        else
                        {
                            response.mobileNo = string.Empty;
                        }
                        if (dr["alternateNo"] != DBNull.Value)
                        {
                            response.alternateNo = Convert.ToString(dr["alternateNo"]);
                        }
                        else
                        {
                            response.alternateNo = string.Empty;
                        }
                        if (dr["staffType"] != DBNull.Value)
                        {
                            response.staffType = Convert.ToString(dr["staffType"]);
                        }
                        else
                        {
                            response.staffType = string.Empty;
                        }
                        if (dr["designation"] != DBNull.Value)
                        {
                            response.designation = Convert.ToString(dr["designation"]);
                        }
                        else
                        {
                            response.designation = string.Empty;
                        }
                        if (dr["locationFrom"] != DBNull.Value)
                        {
                            response.locationFrom = Convert.ToInt32(dr["locationFrom"]);
                        }
                        else
                        {
                            response.locationFrom = 0;
                        }
                        if (dr["locationFromDetails"] != DBNull.Value)
                        {
                            response.locationFromDetails = Convert.ToString(dr["locationFromDetails"]);
                        }
                        else
                        {
                            response.locationFromDetails = string.Empty;
                        }
                        if (dr["locationFromMap"] != DBNull.Value)
                        {
                            response.locationFromMap = Convert.ToString(dr["locationFromMap"]);
                        }
                        else
                        {
                            response.locationFromMap = string.Empty;
                        }
                        if (dr["locFromName"] != DBNull.Value)
                        {
                            response.locationFromDescription = Convert.ToString(dr["locFromName"]);
                        }
                        else
                        {
                            response.locationFromDescription = string.Empty;
                        }
                        if (dr["locationTo"] != DBNull.Value)
                        {
                            response.locationTo = Convert.ToInt32(dr["locationTo"]);
                        }
                        else
                        {
                            response.locationTo = 0;
                        }
                        if (dr["locationToDetails"] != DBNull.Value)
                        {
                            response.locationToDetails = Convert.ToString(dr["locationToDetails"]);
                        }
                        else
                        {
                            response.locationToDetails = string.Empty;
                        }
                        if (dr["locationToMap"] != DBNull.Value)
                        {
                            response.locationToMap = Convert.ToString(dr["locationToMap"]);
                        }
                        else
                        {
                            response.locationToMap = string.Empty;

                        }
                        if (dr["locToName"] != DBNull.Value)
                        {
                            response.locationToDescription = Convert.ToString(dr["locToName"]);
                        }
                        else
                        {
                            response.locationToDescription = string.Empty;
                        }

                    }
                    else
                    {
                        response.ErrorCode = "002";
                        response.errorMessage = "User Name/password does not exist !";

                    }
                }
                else
                {
                    response.ErrorCode = "002";
                    response.errorMessage = "User Name/password does not exist !";
                }
            }
            return response;
        }
        catch
        {
            throw;
        }
    }
}
