﻿using System;
using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSLocationMaster
/// </summary>
public class WSLocationMaster
{
    #region Variable Declaration
    int locId;
    string locCode;
    string locName;
    string locDetail;
    string locType;
    string locTypeDescription;
    #endregion
    #region properties
    public int LocId
    {
        set { locId = value; }
        get { return locId; }
    }
    public string LocCode
    {
        set { locCode = value; }
        get { return locCode; }
    }
    public string LocName
    {
        set { locName = value; }
        get { return locName; }
    }
    public string LocDetail
    {
        set { locDetail = value; }
        get { return locDetail; }
    }
    public string LocType
    {
        set { locType = value; }
        get { return locType; }
    }
    public string LocTypeDescription
    {
        set { locTypeDescription = value; }
        get { return locTypeDescription; }
    }
    #endregion
    public WSLocationMaster()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static WSLocationMasterResponse GetLocationList()
    {
        WSLocationMasterResponse response = new WSLocationMasterResponse();
        try
        {
            SqlParameter[] paramList = new SqlParameter[0];
            DataTable dt = DBGateway.ExecuteQuery("P_ROS_WS_GetLocationList", paramList).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                WSLocationMaster[] Locations = new WSLocationMaster[dt.Rows.Count];
                int k = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    WSLocationMaster Location = new WSLocationMaster();
                    if (dr["loc_id"] != DBNull.Value)
                    {
                        Location.locId = Convert.ToInt32(dr["loc_id"]);
                    }
                    else
                    {
                        Location.locId = 0;
                    }
                    if (dr["loc_code"] != DBNull.Value)
                    {
                        Location.locCode = Convert.ToString(dr["loc_code"]);
                    }
                    else
                    {
                        Location.locCode = string.Empty;
                    }
                    if (dr["loc_name"] != DBNull.Value)
                    {
                        Location.locName = Convert.ToString(dr["loc_name"]);
                    }
                    else
                    {
                        Location.locName = string.Empty;
                    }
                    if (dr["loc_detail"] != DBNull.Value)
                    {
                        Location.locDetail = Convert.ToString(dr["loc_detail"]);
                    }
                    else
                    {
                        Location.locDetail = string.Empty;
                    }
                    if (dr["loc_type"] != DBNull.Value)
                    {
                        Location.locType = Convert.ToString(dr["loc_type"]);
                    }
                    else
                    {
                        Location.locType = string.Empty;
                    }
                    if (dr["LocTypeDesc"] != DBNull.Value)
                    {
                        Location.locTypeDescription = Convert.ToString(dr["LocTypeDesc"]);
                    }
                    else
                    {
                        Location.locTypeDescription = string.Empty;
                    }
                    Locations[k] = Location;
                    k++;
                }
                response.Locations = Locations;
            }
            else
            {
                response.ErrorCode = "002";
                response.ErrorMessage = "Data does Not exist";
            }
            return response;
        }
        catch
        {
            throw;
        }
    }
}
public class WSLocationMasterResponse
{
    #region Variable Declaration
    WSLocationMaster[] locations;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public WSLocationMaster[] Locations
    {
        set { locations = value; }
        get { return locations; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion
    
}
