﻿using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSCrewRosterStatus
/// </summary>
public class WSCrewRosterStatus
{
    #region Variable Declaration
    int empId;
    int rosId;
    string rosTripStatus;
    #endregion

    #region properties
    public int EmpId
    {
        set {empId=value; }
        get { return empId; }
    }
    public int RosId
    {
        set { rosId = value; }
        get { return rosId; }
    }
    public string RosTripStatus
    {
        set { rosTripStatus = value; }
        get { return rosTripStatus; }
    }
    #endregion

}

public class WSCrewRosterStatusRequest
{
    #region Variabule Declaration
    string sessionId;
    WSCrewRosterStatus[] wsCrewRosterStatus;
    #endregion'
    #region properties
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public WSCrewRosterStatus[] WsCrewRosterStatus
    {
        set { wsCrewRosterStatus = value; }
        get { return wsCrewRosterStatus; }
    }
    #endregion
}


public class WSCrewRosterStatusResponse
{
    #region Variabule Declaration
    string rosTripId;
    string sessionId;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public string RosTripId
    {
        set { rosTripId = value; }
        get { return rosTripId; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion
    public void UpdateCrewRosterStatus(WSCrewRosterStatusRequest request)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = DBGateway.CreateConnection();
        cmd.Connection.Open();
        SqlTransaction trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
        cmd.Transaction = trans;
        try
        {
            //string rosTrId = string.Empty;
            if (request.WsCrewRosterStatus.Length > 0)
            {
                for (int i = 0; i < request.WsCrewRosterStatus.Length; i++)
                {
                    if (request.WsCrewRosterStatus[i].EmpId < 1)
                    {
                        errorCode = "003";
                        errorMessage = "EmpId " + (i + 1) + " should not be less than 1";
                    }
                    else if (request.WsCrewRosterStatus[i].RosId < 1)
                    {
                        errorCode = "003";
                        errorMessage = "RosId " + (i + 1) + " should not be less than 1";
                    }
                    else if (string.IsNullOrEmpty(request.WsCrewRosterStatus[i].RosTripStatus))
                    {
                        errorCode = "003";
                        errorMessage = "Trip Status " + (i + 1) + " should not be Empty";
                    }
                    else if (request.WsCrewRosterStatus[i].RosTripStatus.Length > 1)
                    {
                        errorCode = "003";
                        errorMessage = "Trip Status " + (i + 1) + " should not be greater than 1 characters";
                    }
                    else
                    {
                        //if (i == 0)
                        //{
                        //    //TO DO ZIYAD
                        //    Random r = new Random();
                        //    int n = r.Next(10000, 99999);
                        //    rosTrId = "TR" + n + request.WsCrewRosterStatus[i].RosId;
                        //}
                        SqlParameter[] paramList = new SqlParameter[3];
                        paramList[0] = new SqlParameter("@P_ROS_EMP_ID", request.WsCrewRosterStatus[i].EmpId);
                        paramList[1] = new SqlParameter("@P_ROS_ID", request.WsCrewRosterStatus[i].RosId);
                        paramList[2] = new SqlParameter("@P_ROS_TRIP_STATUS", request.WsCrewRosterStatus[i].RosTripStatus);
                        //paramList[3] = new SqlParameter("@P_ROS_TRIPID", rosTrId);
                        int updated = DBGateway.ExecuteNonQueryDetails(cmd, "P_ROS_WS_UpdateCrewRosterStatus", paramList);
                        if (updated == 0)
                        {
                            errorCode = "001";
                            errorMessage = "Transaction failed to update !";
                            trans.Rollback();
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        trans.Rollback();
                        break;
                    }
                }
            }
            else
            {
                errorCode = "001";
                errorMessage = "Invalid request";
                trans.Rollback();
            }
            if (string.IsNullOrEmpty(errorMessage))
            {
                //rosTripId = rosTrId;
                rosTripId = string.Empty; // Temp TODO ZIYA
                trans.Commit();
            }
        }
        catch
        {
            trans.Rollback(); 
            throw;
        }
        finally
        {
            cmd.Connection.Close();
        }
    }
    
}
