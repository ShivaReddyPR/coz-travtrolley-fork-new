﻿using System;
using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSThumbnailImage
/// </summary>
/// 

public class WSThumbnailImage
{
    #region Variable Declaration
    string thumb_name;
    string thumb_href;
    string thumb_url;
    #endregion

    #region properties
    public string Thumb_name
    {
        set { thumb_name = value; }
        get { return thumb_name; }
    }
    public string Thumb_href
    {
        set { thumb_href = value; }
        get { return thumb_href; }
    }
    public string Thumb_url
    {
        set { thumb_url = value; }
        get { return thumb_url; }
    }
    #endregion

    public static WSThumbnailImageResponse GetThumbnailImages()
    {
        WSThumbnailImageResponse response = new WSThumbnailImageResponse();
        try
        {
            SqlParameter[] paramList = new SqlParameter[0];
            DataTable dt = DBGateway.ExecuteQuery("P_ROS_WS_GetThumbnailImages", paramList).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                WSThumbnailImage[] thumbImages = new WSThumbnailImage[dt.Rows.Count];
                int k = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    WSThumbnailImage thumbImage = new WSThumbnailImage();
                    if (dr["thumb_name"] != DBNull.Value)
                    {
                        thumbImage.thumb_name = Convert.ToString(dr["thumb_name"]);
                    }
                    else
                    {
                        thumbImage.thumb_name = string.Empty;
                    }
                    if (dr["thumb_href"] != DBNull.Value)
                    {
                        thumbImage.thumb_href = Convert.ToString(dr["thumb_href"]);
                    }
                    else
                    {
                        thumbImage.thumb_href = string.Empty;
                    }
                    if (dr["thumb_url"] != DBNull.Value)
                    {
                        thumbImage.thumb_url = Convert.ToString(dr["thumb_url"]);
                    }
                    else
                    {
                        thumbImage.thumb_url = string.Empty;
                    }
                    thumbImages[k] = thumbImage;
                    k++;
                }
                response.ThumbImages = thumbImages;
            }
            else
            {
                response.ErrorCode = "002";
                response.ErrorMessage = "Data does Not exist";
            }
            return response;
        }
        catch
        {
            throw;
        }
    }

}


public class WSThumbnailImageResponse
{
    #region Variable Declaration
    WSThumbnailImage[] thumbImages;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public WSThumbnailImage[] ThumbImages
    {
        set { thumbImages = value; }
        get { return thumbImages; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion

    
}

