﻿//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSFeedBack
/// </summary>
public class WSFeedBackRequest
{
    #region Variable Declaration
    string fbType;
    string fbDescription;
    int empId;
    string sessionId;
    #endregion
    #region properties
    public string FbType
    {
        set { fbType = value; }
        get { return fbType; }
    }
    public string FbDescription
    {
        set { fbDescription = value; }
        get { return fbDescription; }
    }
    public int EmpId
    {
        set { empId=value; }
        get { return empId; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    #endregion
    public WSFeedBackRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public class WSFeedBackResponse
{
    #region Variabule Declaration
    string errorCode;
    string errorMessage;
    string sessionId;
    string successMessage;
    #endregion
    #region properties
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion
    public void SaveFeedBack(string fbType, string fbDesc,int empId)
    {
        WSFeedBackResponse response = new WSFeedBackResponse();
        try
        {
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@P_FB_TYPE", fbType);
            if(!string.IsNullOrEmpty(fbDesc))paramList[1] = new SqlParameter("@P_FB_DESCRIPTION", fbDesc);
            paramList[2] = new SqlParameter("@P_EMP_ID", empId);
            int saved = DBGateway.ExecuteNonQuery("P_ROS_WS_SaveFeedBack", paramList);
            if (saved == 0)
            {
                errorCode = "001";
                errorMessage = "Transaction failed to Save !";
            }
        }
        catch { throw; }
    }
}
