﻿using System;
using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSCrewRoster
/// </summary>
public class WSCrewRosterRequest
{
    #region Variable Declaration
    int empId;
    string sessionId;
    #endregion

    #region properties
    public int EmpId
    {
        set { empId = value; }
        get { return empId; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }

    #endregion
}
public class WSCrewRoster
{
     #region Variabule Declaration
    int id;
    string flightNo;
    string fromType;
    int fromLoc;
    string fromLocDet;
    string fromLocMap;
    string toType;
    int toLoc;
    string toLocDet;
    string toLocMap;
    string date;
    string time;
    string fromLocDescription;
    string toLocDescription;
    string fromTypeDescription;
    string toTypeDescription;
    string tripStatus;
    string tripStatusDesc;
    string tripId;
    string driverStatus;
    string driverStatusDesc;
    #endregion

    #region properties
    public int Id
    {
        set { id = value; }
        get { return id; }
    }
    public int FromLoc
    {
        set { fromLoc = value; }
        get { return fromLoc; }
    }
    public string FlightNo
    {
        set { flightNo = value; }
        get { return flightNo; }
    }
    public string FromLocDet
    {
        set { fromLocDet = value; }
        get { return fromLocDet; }
    }
    public string FromLocMap
    {
        set { fromLocMap = value; }
        get { return fromLocMap; }
    }
    public int ToLoc
    {
        set { toLoc = value; }
        get { return toLoc; }
    }
    public string ToLocDet
    {
        set { toLocDet = value; }
        get { return toLocDet; }
    }
    public string ToLocMap
    {
        set { toLocMap = value; }
        get { return toLocMap; }
    }
    public string Date
    {
        set { date = value; }
        get { return date; }
    }
    public string Time
    {
        set { time = value; }
        get { return time; }

    }
    public string FromType
    {
        set { fromType  = value; }
        get { return fromType; }
    }
    public string ToType
    {
        set { toType = value; }
        get { return toType; }
    }
    public string FromLocDescription
    {
        set { fromLocDescription = value; }
        get { return fromLocDescription; }
    }
    public string ToLocDescription
    {
        set { toLocDescription = value; }
        get { return toLocDescription; }
    }
    public string FromTypeDescription
    {
        set { fromTypeDescription = value; }
        get { return fromTypeDescription; }
    }
    public string ToTypeDescription
    {
        set { toTypeDescription = value; }
        get { return toTypeDescription; }
    }
    public string TripStatus
    {
        set { tripStatus = value; }
        get { return tripStatus; }
    }
    public string TripStatusDesc
    {
        set { tripStatusDesc = value; }
        get { return tripStatusDesc; }
    }
    public string TripId
    {
        set { tripId = value; }
        get { return tripId; }
    }

    public string DriverStatus
    {
        set { driverStatus = value; }
        get { return driverStatus; }
    }

    public string DriverStatusDesc
    {
        set { driverStatusDesc = value; }
        get { return driverStatusDesc; }
    }
    #endregion


    public WSCrewRoster()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static WSCrewRosterResponse GetCrewRosterListByEmpId(int empId)
    {
        WSCrewRosterResponse response = new WSCrewRosterResponse();
        
        try
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_EMP_ID", empId);
            DataTable dt = DBGateway.ExecuteQuery("P_ROS_WS_GetCrewRosterListByEmpId", paramList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                WSCrewRoster[] crewRosterArry = new WSCrewRoster[dt.Rows.Count];
                int k = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    WSCrewRoster rosterDetails = new WSCrewRoster();
                    if (dr["ROS_FLIGHT_NO"] != DBNull.Value)
                    {
                        rosterDetails.flightNo = Convert.ToString(dr["ROS_FLIGHT_NO"]);
                    }
                    else
                    {
                        rosterDetails.flightNo = string.Empty;
                    }
                    if (dr["ROS_FROM_LOC"] != DBNull.Value)
                    {
                        rosterDetails.fromLoc = Convert.ToInt32(dr["ROS_FROM_LOC"]);
                    }
                    else
                    {
                        rosterDetails.fromLoc = 0;
                    }
                    if (dr["ROS_FROM_LOC_DET"] != DBNull.Value)
                    {
                        rosterDetails.fromLocDet = Convert.ToString(dr["ROS_FROM_LOC_DET"]);
                    }
                    else
                    {
                        rosterDetails.fromLocDet = string.Empty;
                    }
                    if (dr["ROS_FROM_LOC_MAP"] != DBNull.Value)
                    {
                        rosterDetails.fromLocMap = Convert.ToString(dr["ROS_FROM_LOC_MAP"]);
                    }
                    else
                    {
                        rosterDetails.fromLocMap = string.Empty;
                    }
                    if (dr["ROS_TO_LOC"] != DBNull.Value)
                    {
                        rosterDetails.toLoc = Convert.ToInt32(dr["ROS_TO_LOC"]);
                    }
                    else
                    {
                        rosterDetails.toLoc = 0;
                    }
                    if (dr["ROS_TO_LOC_DET"] != DBNull.Value)
                    {
                        rosterDetails.toLocDet = Convert.ToString(dr["ROS_TO_LOC_DET"]);
                    }
                    else
                    {
                        rosterDetails.toLocDet = string.Empty;
                    }
                    if (dr["ROS_TO_LOC_MAP"] != DBNull.Value)
                    {
                        rosterDetails.toLocMap = Convert.ToString(dr["ROS_TO_LOC_MAP"]);
                    }
                    else
                    {
                        rosterDetails.toLocMap = string.Empty;
                    }
                    if (dr["ROS_DATE"] != DBNull.Value)
                    {
                        rosterDetails.date = Convert.ToDateTime(dr["ROS_DATE"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        rosterDetails.date = string.Empty;
                    }
                    if (dr["ROS_DATE"] != DBNull.Value)
                    {
                        //rosterDetails.time = Convert.ToDateTime(dr["ROS_DATE"]).ToString("hh:mm:ss");
                        rosterDetails.time = Convert.ToDateTime(dr["ROS_DATE"]).ToString("hh:mm tt");
                        
                    }
                    else
                    {
                        rosterDetails.time = string.Empty;
                    }
                    if (dr["ROS_ID"] != DBNull.Value)
                    {
                        rosterDetails.id = Convert.ToInt32(dr["ROS_ID"]);
                    }
                    else
                    {
                        rosterDetails.id = 0;
                    }
                    if (dr["ROS_FROM_TYPE"] != DBNull.Value)
                    {
                        rosterDetails.fromType = Convert.ToString(dr["ROS_FROM_TYPE"]);
                    }
                    else
                    {
                        rosterDetails.fromType = string.Empty;
                    }
                    if (dr["ROS_TO_TYPE"] != DBNull.Value)
                    {
                        rosterDetails.toType = Convert.ToString(dr["ROS_TO_TYPE"]);
                    }
                    else
                    {
                        rosterDetails.toType = string.Empty;
                    }
                    if (dr["locFromName"] != DBNull.Value)
                    {
                        rosterDetails.fromLocDescription = Convert.ToString(dr["locFromName"]);
                    }
                    else
                    {
                        rosterDetails.fromLocDescription = string.Empty;
                    }
                    if (dr["locToName"] != DBNull.Value)
                    {
                        rosterDetails.toLocDescription = Convert.ToString(dr["locToName"]);
                    }
                    else
                    {
                        rosterDetails.toLocDescription = string.Empty;
                    }
                    if (dr["FromTypeDesc"] != DBNull.Value)
                    {
                        rosterDetails.fromTypeDescription = Convert.ToString(dr["FromTypeDesc"]);
                    }
                    else
                    {
                        rosterDetails.fromTypeDescription = string.Empty;
                    }
                    if (dr["ToTypeDesc"] != DBNull.Value)
                    {
                        rosterDetails.toTypeDescription = Convert.ToString(dr["ToTypeDesc"]);
                    }
                    else
                    {
                        rosterDetails.toTypeDescription = string.Empty;
                    }
                    if (dr["ROS_TRIP_STATUS"] != DBNull.Value)
                    {
                        rosterDetails.tripStatus = Convert.ToString(dr["ROS_TRIP_STATUS"]);
                    }
                    else
                    {
                        rosterDetails.tripStatus = string.Empty;
                    }
                    if (dr["TripStatusDesc"] != DBNull.Value)
                    {
                        rosterDetails.tripStatusDesc = Convert.ToString(dr["TripStatusDesc"]);
                    }
                    else
                    {
                        rosterDetails.tripStatusDesc = string.Empty;
                    }
                    if (dr["ROS_TRIPID"] != DBNull.Value)
                    {
                        rosterDetails.tripId = Convert.ToString(dr["ROS_TRIPID"]);
                    }
                    else
                    {
                        rosterDetails.tripId = string.Empty;
                    }

                    if (dr["ROS_DRIVER_STATUS"] != DBNull.Value)
                    {
                        rosterDetails.driverStatus = Convert.ToString(dr["ROS_DRIVER_STATUS"]);
                    }
                    else
                    {
                        rosterDetails.driverStatus = string.Empty;
                    }

                    if (dr["DriverStatusDesc"] != DBNull.Value)
                    {
                        rosterDetails.driverStatusDesc = Convert.ToString(dr["DriverStatusDesc"]);
                    }
                    else
                    {
                        rosterDetails.driverStatusDesc = string.Empty;
                    }

                    crewRosterArry[k] = rosterDetails;
                    k++;
                }
                response.CrewRoster = crewRosterArry;
            }
            else
            {
                response.ErrorCode = "002";
                response.ErrorMessage = "Data does Not exist";
            }

        }
        catch { throw; }
        return response;
    }
}

public class WSCrewRosterResponse
{
     #region Variabule Declaration
    WSCrewRoster[] crewRoster;
    string sessionId;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public WSCrewRoster[] CrewRoster
    {
        set { crewRoster = value; }
        get { return crewRoster; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion
}

