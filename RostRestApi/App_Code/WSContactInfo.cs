﻿using System;
using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSContactInfo
/// </summary>
public class WSContactInfoResponse
{
    #region Variable Declaration
    string email;
    string contactNo;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public string Email
    {
        set { email = value; }
        get { return email; }
    }
    public string ContactNo
    {
        set { contactNo = value; }
        get { return contactNo; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion

    public void GetContactInfo()
    {
        try
        {
            SqlParameter[] paramList = new SqlParameter[0];
            DataTable dt = DBGateway.ExecuteQuery("P_ROS_WS_GetContactInfo", paramList).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                if (dr["email"] != DBNull.Value)
                {
                    email = Convert.ToString(dr["email"]);
                }
                else
                {
                    email = string.Empty;
                }
                if (dr["contactNo"] != DBNull.Value)
                {
                    contactNo = Convert.ToString(dr["contactNo"]);
                }
                else
                {
                    contactNo = string.Empty;
                }

            }
            else
            {
                errorCode = "002";
                errorMessage = "Data does Not exist";
            }

        }
        catch
        {
            throw;
        }
    }
}
