﻿using System;
using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSEmpRegister
/// </summary>
public class WSEmpRegisterRequest
{
    #region Variabule Declaration

    int company;
    string staffId;
    string title;
    string firstName;
    string lastName;
    string email;
    string mobileNo;
    string alternateNo;
    string password;
    string designation;
    int locationFrom;
    string locationFromDetails;
    string locationFromMap;
    int locationTo;
    string locationToDetails;
    string locationToMap;
    string imageUrl;
    #endregion

    #region properties
    public int Company
    {
        set { company = value; }
        get { return company; }
    }
    public string StaffId
    {
        set { staffId = value; }
        get { return staffId; }
    }
    public string Password
    {
        set { password = value; }
        get { return password; }
    }
    public string Title
    {
        set { title = value; }
        get { return title; }
    }
    public string FirstName
    {
        set { firstName = value; }
        get { return firstName; }

    }
    public string LastName
    {
        set { lastName = value; }
        get { return lastName; }
    }
    public string Email
    {
        set { email = value; }
        get { return email; }
    }
    public string MobileNo
    {
        set { mobileNo = value; }
        get { return mobileNo; }
    }
    public string AlternateNo
    {
        set { alternateNo = value; }
        get { return alternateNo; }
    }
    public string Designation
    {
        set { designation = value; }
        get { return designation; }
    }
    public int LocationFrom
    {
        set { locationFrom = value; }
        get { return locationFrom; }
    }
    public string LocationFromDetails
    {
        set { locationFromDetails = value; }
        get { return locationFromDetails; }
    }
    public string LocationFromMap
    {
        set { locationFromMap = value; }
        get { return locationFromMap; }
    }
    public int LocationTo
    {
        set { locationTo = value; }
        get { return locationTo; }
    }
    public string LocationToDetails
    {
        set { locationToDetails = value; }
        get { return locationToDetails; }
    }
    public string LocationToMap
    {
        set { locationToMap = value; }
        get { return locationToMap; }
    }
    public string ImageUrl
    {
        set { imageUrl = value; }
        get { return imageUrl; }
    }
    #endregion
    public WSEmpRegisterRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public class WSEmpRegisterResponse
{
    #region Variable Declaration
    int empId;
    int company;
    string staffId;
    string title;
    string firstName;
    string lastName;
    string email;
    string mobileNo;
    string alternateNo;
    string staffType;
    string password;
    string designation;
    int locationFrom;
    string locationFromDetails;
    string locationFromMap;
    string locationFromDescription;
    int locationTo;
    string locationToDetails;
    string locationToMap;
    string locationToDescription;
    string sessionId;
    string imageUrl;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public int EmpId
    {
        set { empId = value; }
        get { return empId; }
    }
    public int Company
    {
        set { company = value; }
        //get { return company; }
    }
    public string StaffId
    {
        set { staffId = value; }
        get { return staffId; }
    }
    public string Password
    {
        set { password = value; }
        //get { return password; }
    }
    public string Title
    {
        set { title = value; }
        get { return title; }
    }
    public string FirstName
    {
        set { firstName = value; }
        get { return firstName; }

    }
    public string LastName
    {
        set { lastName = value; }
        get { return lastName; }
    }
    public string Email
    {
        set { email = value; }
        get { return email; }
    }
    public string MobileNo
    {
        set { mobileNo = value; }
        get { return mobileNo; }
    }
    public string AlternateNo
    {
        set { alternateNo = value; }
        get { return alternateNo; }
    }
    public string StaffType
    {
        set { staffType = value; }
        get { return staffType; }
    }

    public string Designation
    {
        set { designation = value; }
        get { return designation; }
    }
    public int LocationFrom
    {
        set { locationFrom = value; }
        get { return locationFrom; }
    }
    public string LocationFromDetails
    {
        set { locationFromDetails = value; }
        get { return locationFromDetails; }
    }
    public string LocationFromMap
    {
        set { locationFromMap = value; }
        get { return locationFromMap; }
    }
    public string LocationFromDescription
    {
        set { locationFromDescription = value; }
        get { return locationFromDescription; }
    }
    public int LocationTo
    {
        set { locationTo = value; }
        get { return locationTo; }
    }
    public string LocationToDetails
    {
        set { locationToDetails = value; }
        get { return locationToDetails; }
    }
    public string LocationToMap
    {
        set { locationToMap = value; }
        get { return locationToMap; }
    }
    public string LocationToDescription
    {
        set { locationToDescription = value; }
        get { return locationToDescription; }
    }
    public string ImageUrl
    {
        set { imageUrl = value; }
        get { return imageUrl; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion
    public void Save()
    {
        try
        {
            SqlParameter[] paramList = new SqlParameter[24];
            paramList[0] = new SqlParameter("@P_company", company);
            paramList[1] = new SqlParameter("@P_staff_id", staffId);
            paramList[2] = new SqlParameter("@P_password", password);
            paramList[3] = new SqlParameter("@P_title", title);
            paramList[4] = new SqlParameter("@P_firstName", firstName);
            paramList[5] = new SqlParameter("@P_lastName", lastName);
            paramList[6] = new SqlParameter("@P_email", email);
            paramList[7] = new SqlParameter("@P_mobileNo", mobileNo);
            if (!string.IsNullOrEmpty(alternateNo)) paramList[8] = new SqlParameter("@P_alternateNo", alternateNo);
            if(!string.IsNullOrEmpty(staffType)) paramList[9] = new SqlParameter("@P_staffType", staffType);
            paramList[10] = new SqlParameter("@P_locationFrom", locationFrom);
            if (!string.IsNullOrEmpty(locationFromDetails)) paramList[11] = new SqlParameter("@P_locationFromDetails", locationFromDetails);

            if (!string.IsNullOrEmpty(locationFromMap)) paramList[12] = new SqlParameter("@P_locationFromMap", locationFromMap);
            paramList[13] = new SqlParameter("@P_locationTo", locationTo);
            if (!string.IsNullOrEmpty(locationToDetails)) paramList[14] = new SqlParameter("@P_locationToDetails", locationToDetails);
            if (!string.IsNullOrEmpty(locationToMap)) paramList[15] = new SqlParameter("@P_locationToMap", locationToMap);
            if (!string.IsNullOrEmpty(designation)) paramList[16] = new SqlParameter("@P_designation", designation);

            paramList[17] = new SqlParameter("@P_createdBy", -1); //Credeted by Hard coded -1
            paramList[18] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
            paramList[18].Direction = ParameterDirection.Output;
            paramList[19] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
            paramList[19].Direction = ParameterDirection.Output;
            paramList[20] = new SqlParameter("@P_EMP_ID_RET", SqlDbType.Int, 200);
            paramList[20].Direction = ParameterDirection.Output;
            paramList[21] = new SqlParameter("@P_LOCFROM_DESC_RET", SqlDbType.NVarChar, 200);
            paramList[21].Direction = ParameterDirection.Output;
            paramList[22] = new SqlParameter("@P_LOCTO_DESC_RET", SqlDbType.NVarChar, 200);
            paramList[22].Direction = ParameterDirection.Output;
            if(!string.IsNullOrEmpty(imageUrl)) paramList[23] = new SqlParameter("@P_IMAGE_URL", imageUrl);
            DBGateway.ExecuteNonQuery("P_ROS_WS_EmpAdd", paramList);
            string messageType = Convert.ToString(paramList[18].Value);
            if (messageType == "E")
            {
                string message = Convert.ToString(paramList[19].Value);
                if (!string.IsNullOrEmpty(message))
                {
                    errorCode = "001";
                    errorMessage = message;
                }
                else
                {
                    errorCode = "001";
                    errorMessage = "staffId already exists !";
                }
            }
            else
            {
                empId = Convert.ToInt32(paramList[20].Value);
                locationFromDescription = Convert.ToString(paramList[21].Value);
                locationToDescription = Convert.ToString(paramList[22].Value);
            }
        }
        catch
        {
            throw;
        }
    }
}
