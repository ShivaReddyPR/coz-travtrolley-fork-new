﻿using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for ChauffeurJobStatus
/// </summary>
public class WSChauffeurJobStatus
{
    #region Variabule Declaration
    int empId;
    int rosId;
    string driTripStatus;
    #endregion

    #region properties
    public int EmpId
    {
        set { empId = value; }
        get { return empId; }
    }
    public int RosId
    {
        set { rosId = value; }
        get { return rosId; }
    }
    public string DriTripStatus
    {
        set { driTripStatus = value; }
        get { return driTripStatus; }
    }
    #endregion
    public WSChauffeurJobStatus()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public class WSChauffeurJobStatusRequest
{
    #region Variable Declaration
    string sessionId;
    WSChauffeurJobStatus[] chaupperJobStatus;
    #endregion
    #region properties
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public WSChauffeurJobStatus[] ChaupperJobStatus
    {
        set { chaupperJobStatus = value; }
        get { return chaupperJobStatus; }
    }
    #endregion
}


public class WSChauffeurJobStatusResponse
{
    #region Variabule Declaration
    string rosDriverTripId;
    string sessionId;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public string RosDriverTripId
    {
        set { rosDriverTripId = value; }
        get { return rosDriverTripId; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion
    public void UpdateChauffeurJobStatus(WSChauffeurJobStatusRequest request)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = DBGateway.CreateConnection();
        cmd.Connection.Open();
        SqlTransaction trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
        cmd.Transaction = trans;
        try
        {
            //string rosDriTripId = string.Empty;
            if (request.ChaupperJobStatus.Length > 0)
            {
                for (int i = 0; i < request.ChaupperJobStatus.Length; i++)
                {
                    if (request.ChaupperJobStatus[i].EmpId < 1)
                    {
                        errorCode = "003";
                        errorMessage = "EmpId " + (i + 1) + " should not be less than 1";
                    }
                    else if (request.ChaupperJobStatus[i].RosId < 1)
                    {
                        errorCode = "003";
                        errorMessage = "RosId " + (i + 1) + " should not be less than 1";
                    }
                    else if (string.IsNullOrEmpty(request.ChaupperJobStatus[i].DriTripStatus))
                    {
                        errorCode = "003";
                        errorMessage = "Driver Trip Status " + (i + 1) + " should not be Empty";
                    }
                    else if (request.ChaupperJobStatus[i].DriTripStatus.Length > 1)
                    {
                        errorCode = "003";
                        errorMessage = "Driver Trip Status " + (i + 1) + " should not be greater than 1 characters";
                    }
                    else
                    {
                        //if (i == 0)
                        //{
                        //    //TODO ZIYAD
                        //    Random r = new Random();
                        //    int n = r.Next(10000, 99999);
                        //    rosDriTripId = "DR" + n + request.ChauffeurJobStatus[i].RosId;
                        //}
                        SqlParameter[] paramList = new SqlParameter[3];
                        paramList[0] = new SqlParameter("@P_ROS_EMP_ID", request.ChaupperJobStatus[i].EmpId);
                        paramList[1] = new SqlParameter("@P_ROS_ID", request.ChaupperJobStatus[i].RosId);
                        paramList[2] = new SqlParameter("@P_ROS_DRIVER_STATUS", request.ChaupperJobStatus[i].DriTripStatus);
                        //paramList[3] = new SqlParameter("@P_ROS_DRIVER_TRIPID", rosDriTripId);
                        int updated = DBGateway.ExecuteNonQueryDetails(cmd, "P_ROS_WS_UpdateChauffeurJobStatus", paramList);
                        if (updated == 0)
                        {
                            errorCode = "001";
                            errorMessage = "Transaction failed to update !";
                            trans.Rollback();
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        trans.Rollback();
                        break;
                    }
                }
            }
            else
            {
                errorCode = "001";
                errorMessage = "Invalid Request";
                trans.Rollback();
            }
            if (string.IsNullOrEmpty(errorMessage))
            {
                //rosDriverTripId = rosDriTripId;
                rosDriverTripId = string.Empty;// Temp TODO ZIYA
                trans.Commit();
            }
        }
        catch
        {
            trans.Rollback(); 
            throw;
        }
        finally
        {
            cmd.Connection.Close();
        }
    }
}

