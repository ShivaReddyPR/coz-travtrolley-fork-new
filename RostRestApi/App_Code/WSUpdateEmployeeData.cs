﻿using System;
using System.Data;
//using System.Linq;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// <summary>
/// Summary description for WSUpdateEmployeeData
/// </summary>
public class WSUpdateEmployeeDataRequest
{
    #region Variable Declaration
    int empId;
    string title;
    string firstName;
    string lastName;
    string email;
    string mobileNo;
    string alternateNo;
    string staffType;
    string designation;
    int locationFrom;
    string locationFromDetails;
    string locationFromMap;
    int locationTo;
    string locationToDetails;
    string locationToMap;
    string imageUrl;
    string sessionId;
    #endregion

    #region properties
    public int EmpId
    {
        set { empId = value; }
        get { return empId; }
    }
    public string Title
    {
        set { title = value; }
        get { return title; }
    }
    public string FirstName
    {
        set { firstName = value; }
        get { return firstName; }

    }
    public string LastName
    {
        set { lastName = value; }
        get { return lastName; }
    }
    public string Email
    {
        set { email = value; }
        get { return email; }
    }
    public string MobileNo
    {
        set { mobileNo = value; }
        get { return mobileNo; }
    }
    public string AlternateNo
    {
        set { alternateNo = value; }
        get { return alternateNo; }
    }
    public string StaffType
    {
        set { staffType = value; }
        get { return staffType; }
    }

    public string Designation
    {
        set { designation = value; }
        get { return designation; }
    }
    public int LocationFrom
    {
        set { locationFrom = value; }
        get { return locationFrom; }
    }
    public string LocationFromDetails
    {
        set { locationFromDetails = value; }
        get { return locationFromDetails; }
    }
    public string LocationFromMap
    {
        set { locationFromMap = value; }
        get { return locationFromMap; }
    }
    public int LocationTo
    {
        set { locationTo = value; }
        get { return locationTo; }
    }
    public string LocationToDetails
    {
        set { locationToDetails = value; }
        get { return locationToDetails; }
    }
    public string LocationToMap
    {
        set { locationToMap = value; }
        get { return locationToMap; }
    }
    public string ImageUrl
    {
        set { imageUrl = value; }
        get { return imageUrl; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    #endregion


}

public class WSUpdateEmployeeDataResponse
{ 
    #region Variable Declaration
    int empId;
    string staffId;
    string title;
    string firstName;
    string lastName;
    string email;
    string mobileNo;
    string alternateNo;
    string staffType;
    string designation;
    int locationFrom;
    string locationFromDetails;
    string locationFromMap;
    string locationFromDescription;
    int locationTo;
    string locationToDetails;
    string locationToMap;
    string locationToDescription;
    string imageUrl;
    string sessionId;
    string errorCode;
    string errorMessage;
    string successMessage;
    #endregion

    #region properties
    public string StaffId
    {
        set { staffId = value; }
        get { return staffId; }
    }
    public int EmpId
    {
        set { empId = value; }
        get { return empId; }
    }
    public string Title
    {
        set { title = value; }
        get { return title; }
    }
    public string FirstName
    {
        set { firstName = value; }
        get { return firstName; }

    }
    public string LastName
    {
        set { lastName = value; }
        get { return lastName; }
    }
    public string Email
    {
        set { email = value; }
        get { return email; }
    }
    public string MobileNo
    {
        set { mobileNo = value; }
        get { return mobileNo; }
    }
    public string AlternateNo
    {
        set { alternateNo = value; }
        get { return alternateNo; }
    }
    public string StaffType
    {
        set { staffType = value; }
        get { return staffType; }
    }

    public string Designation
    {
        set { designation = value; }
        get { return designation; }
    }
    public int LocationFrom
    {
        set { locationFrom = value; }
        get { return locationFrom; }
    }
    public string LocationFromDetails
    {
        set { locationFromDetails = value; }
        get { return locationFromDetails; }
    }
    public string LocationFromMap
    {
        set { locationFromMap = value; }
        get { return locationFromMap; }
    }
    public string LocationFromDescription
    {
        set { locationFromDescription = value; }
        get { return locationFromDescription; }
    }
    public int LocationTo
    {
        set { locationTo = value; }
        get { return locationTo; }
    }
    public string LocationToDetails
    {
        set { locationToDetails = value; }
        get { return locationToDetails; }
    }
    public string LocationToMap
    {
        set { locationToMap = value; }
        get { return locationToMap; }
    }
    public string LocationToDescription
    {
        set { locationToDescription = value; }
        get { return locationToDescription; }
    }
    public string ImageUrl
    {
        set { imageUrl = value; }
        get { return imageUrl; }
    }
    public string SessionId
    {
        set { sessionId = value; }
        get { return sessionId; }
    }
    public string ErrorCode
    {
        set { errorCode = value; }
        get { return errorCode; }
    }
    public string ErrorMessage
    {
        set { errorMessage = value; }
        get { return errorMessage; }
    }
    public string SuccessMessage
    {
        set { successMessage = value; }
        get { return successMessage; }
    }
    #endregion

    public void UpdateEmployeeData(WSUpdateEmployeeDataRequest request)
    {
        SqlParameter[] paramList = new SqlParameter[17];
        try
        {
            paramList = new SqlParameter[17];
            paramList[0] = new SqlParameter("@P_emp_Id", request.EmpId);
            paramList[1] = new SqlParameter("@P_title", request.Title);
            paramList[2] = new SqlParameter("@P_firstName", request.FirstName);
            paramList[3] = new SqlParameter("@P_lastName", request.LastName);
            paramList[4] = new SqlParameter("@P_email", request.Email);
            paramList[5] = new SqlParameter("@P_mobileNo", request.MobileNo);
            if (!string.IsNullOrEmpty(request.AlternateNo)) paramList[6] = new SqlParameter("@P_alternateNo", request.AlternateNo);
            if (!string.IsNullOrEmpty(request.StaffType)) paramList[7] = new SqlParameter("@P_staffType", request.StaffType);
            paramList[8] = new SqlParameter("@P_locationFrom", request.LocationFrom);
            if (!string.IsNullOrEmpty(request.LocationFromDetails)) paramList[9] = new SqlParameter("@P_locationFromDetails", request.LocationFromDetails);

            if (!string.IsNullOrEmpty(request.LocationFromMap)) paramList[10] = new SqlParameter("@P_locationFromMap", request.LocationFromMap);
            paramList[11] = new SqlParameter("@P_locationTo", request.LocationTo);
            if (!string.IsNullOrEmpty(request.LocationToDetails)) paramList[12] = new SqlParameter("@P_locationToDetails", request.LocationToDetails);
            if (!string.IsNullOrEmpty(request.LocationToMap)) paramList[13] = new SqlParameter("@P_locationToMap", request.LocationToMap);
            if (!string.IsNullOrEmpty(request.Designation)) paramList[14] = new SqlParameter("@P_designation", request.Designation);
            paramList[15] = new SqlParameter("@P_createdBy", request.EmpId); 
            if (!string.IsNullOrEmpty(request.ImageUrl)) paramList[16] = new SqlParameter("@P_IMAGE_URL", request.ImageUrl);
            int update = DBGateway.ExecuteNonQuery("P_ROS_WS_EmpUpdate", paramList);
            if (update == 0)
            {
                errorCode = "002";
                errorMessage = "EmpId does Not exist !";
            }
            else
            {
                paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_EMP_ID", request.EmpId);
                DataTable dt = DBGateway.ExecuteQuery("P_ROS_WS_EmpGetData", paramList).Tables[0];
                if (dt != null && dt.Rows.Count == 1)
                {
                    DataRow dr = dt.Rows[0];
                    if (dr["emp_Id"] != DBNull.Value)
                    {
                        empId = Convert.ToInt32(dr["emp_Id"]);
                    }
                    else
                    {
                        empId = 0;
                    }
                    if (dr["staff_id"] != DBNull.Value)
                    {
                        staffId = Convert.ToString(dr["staff_id"]);
                    }
                    else
                    {
                        staffId = string.Empty;
                    }
                    if (dr["title"] != DBNull.Value)
                    {
                        title = Convert.ToString(dr["title"]);
                    }
                    else
                    {
                        title = string.Empty;

                    }
                    if (dr["firstName"] != DBNull.Value)
                    {
                        firstName = Convert.ToString(dr["firstName"]);
                    }
                    else
                    {
                        firstName = string.Empty;
                    }
                    if (dr["lastName"] != DBNull.Value)
                    {
                        lastName = Convert.ToString(dr["lastName"]);
                    }
                    else
                    {
                        lastName = string.Empty;
                    }
                    if (dr["email"] != DBNull.Value)
                    {
                        email = Convert.ToString(dr["email"]);
                    }
                    else
                    {
                        email = string.Empty;
                    }
                    if (dr["mobileNo"] != DBNull.Value)
                    {
                        mobileNo = Convert.ToString(dr["mobileNo"]);
                    }
                    else
                    {
                       mobileNo = string.Empty;
                    }
                    if (dr["alternateNo"] != DBNull.Value)
                    {
                        alternateNo = Convert.ToString(dr["alternateNo"]);
                    }
                    else
                    {
                        alternateNo = string.Empty;
                    }
                    if (dr["staffType"] != DBNull.Value)
                    {
                       staffType = Convert.ToString(dr["staffType"]);
                    }
                    else
                    {
                        staffType = string.Empty;
                    }
                    if (dr["designation"] != DBNull.Value)
                    {
                        designation = Convert.ToString(dr["designation"]);
                    }
                    else
                    {
                        designation = string.Empty;
                    }
                    if (dr["locationFrom"] != DBNull.Value)
                    {
                        locationFrom = Convert.ToInt32(dr["locationFrom"]);
                    }
                    else
                    {
                        locationFrom = 0;
                    }
                    if (dr["locationFromDetails"] != DBNull.Value)
                    {
                        locationFromDetails = Convert.ToString(dr["locationFromDetails"]);
                    }
                    else
                    {
                        locationFromDetails = string.Empty;
                    }
                    if (dr["locationFromMap"] != DBNull.Value)
                    {
                        locationFromMap = Convert.ToString(dr["locationFromMap"]);
                    }
                    else
                    {
                        locationFromMap = string.Empty;
                    }
                    if (dr["locFromName"] != DBNull.Value)
                    {
                        locationFromDescription = Convert.ToString(dr["locFromName"]);
                    }
                    else
                    {
                        locationFromDescription = string.Empty;
                    }
                    if (dr["locationTo"] != DBNull.Value)
                    {
                        locationTo = Convert.ToInt32(dr["locationTo"]);
                    }
                    else
                    {
                        locationTo = 0;
                    }
                    if (dr["locationToDetails"] != DBNull.Value)
                    {
                        locationToDetails = Convert.ToString(dr["locationToDetails"]);
                    }
                    else
                    {
                        locationToDetails = string.Empty;
                    }
                    if (dr["locationToMap"] != DBNull.Value)
                    {
                        locationToMap = Convert.ToString(dr["locationToMap"]);
                    }
                    else
                    {
                        locationToMap = string.Empty;

                    }
                    if (dr["locToName"] != DBNull.Value)
                    {
                        locationToDescription = Convert.ToString(dr["locToName"]);
                    }
                    else
                    {
                        locationToDescription = string.Empty;
                    }
                    if (dr["imageUrl"] != DBNull.Value)
                    {
                        imageUrl = Convert.ToString(dr["imageUrl"]);
                    }
                    else
                    {
                        imageUrl = string.Empty;
                    }

                }
                else
                {
                    errorCode = "002";
                    errorMessage = "Data does Not exist";

                }
            }
        }
        catch
        {
            throw;
        }
    }
}
