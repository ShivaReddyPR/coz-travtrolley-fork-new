﻿using Autofac;
using Autofac.Integration.WebApi;
using Cozmo.Api.Application.Infrastructure;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Web.Http;

[assembly: OwinStartup(typeof(Cozmo.Expense.Api.Startup))]
namespace Cozmo.Expense.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //GlobalConfiguration.Configure(WebApiConfig.Register);

            //var config = WebApiConfig.Register();

            //var builder = new ContainerBuilder();
            //builder.RegisterHttpRequestMessage(config);

            //builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            //// Register .Net components
            //builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            //builder.RegisterWebApiFilterProvider(config);

            //var container = builder.Build();

            //// Wire-up AppInsights
            ////var configMgr = container.Resolve<IConfigurationManager>();
            ////Microsoft.ApplicationInsights.Extensibility.TelemetryConfiguration.Active.InstrumentationKey = configMgr.AppSettings["ikey"];

            ////config.Filters.Add(new OmegaAuthorizationFilter(new ApiClient(new HttpClient()), configMgr.AppSettings["ConfigApiUrl"]));

            //// DependencyResolver for API
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            ////app.UseAutofacMiddleware(container);
            ////app.UseAutofacWebApi(config);

            ////var authority = configMgr.AppSettings["OmegaSTS"];
            ////if (string.IsNullOrEmpty(authority))
            ////    throw new ArgumentNullException("OmegaSTS", "OmegaSTS is not present in appSettings");

            ////JwtSecurityTokenHandler.DefaultInboundClaimTypeMap = new Dictionary<string, string>();
            ////app.UseIdentityServerBearerTokenAuthentication(
            ////    new IdentityServerBearerTokenAuthenticationOptions
            ////    {
            ////        Authority = authority,
            ////        RequiredScopes = new[] { "cozmoflight-api" },
            ////    });

            //app.UseWebApi(config);

            //// Inject logging
            //config.Services.Add(typeof(IExceptionLogger),
            //    new CommonExceptionLogger(LogManager.GetLogger<Startup>()));            
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            //this is very important line cross orgin source(CORS)it is used to enable cross-site HTTP requests  
            //For security reasons, browsers restrict cross-origin HTTP requests  
            app.UseCors(CorsOptions.AllowAll);

            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenProvider = new AuthenticationTokenProvider
                {
                    OnCreate = (context) =>
                    {
                        var token = context.SerializeTicket();
                        var guid = Guid.NewGuid().ToString("N");
                        var rsp = ApiGeneral.SetOwinToken(guid, token);
                        if (!string.IsNullOrEmpty(rsp))
                            context.SetToken(guid);
                    },
                    OnReceive = (context) =>
                    {
                        var token = ApiGeneral.GetOwinToken(context.Token);
                        context.DeserializeTicket(token);
                    }
                },
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),//token expiration time  
                Provider = new OauthProvider()
            };

            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            var builder = new ContainerBuilder();
            builder.RegisterHttpRequestMessage(config);

            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            var assembly = AppDomain.CurrentDomain.Load("Cozmo.Expense.Application");
            builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly).AsImplementedInterfaces();

            var mediatrOpenTypes = new[]
            {
                typeof(IRequestHandler<,>),
                typeof(IRequestExceptionHandler<,,>),
                typeof(IRequestExceptionAction<,>),
                typeof(INotificationHandler<>),
            };

            foreach (var mediatrOpenType in mediatrOpenTypes)
            {
                builder
                    .RegisterAssemblyTypes(typeof(Ping).GetTypeInfo().Assembly)
                    .AsClosedTypesOf(mediatrOpenType)
                    // when having a single class implementing several handler types
                    // this call will cause a handler to be called twice
                    // in general you should try to avoid having a class implementing for instance `IRequestHandler<,>` and `INotificationHandler<>`
                    // the other option would be to remove this call
                    // see also https://github.com/jbogard/MediatR/issues/462
                    .AsImplementedInterfaces();
            }

            //builder.RegisterInstance(writer).As<TextWriter>();

            // It appears Autofac returns the last registered types first
            builder.RegisterGeneric(typeof(RequestPostProcessorBehavior<,>)).As(typeof(IPipelineBehavior<,>));
            builder.RegisterGeneric(typeof(RequestPreProcessorBehavior<,>)).As(typeof(IPipelineBehavior<,>));
            builder.RegisterGeneric(typeof(RequestExceptionActionProcessorBehavior<,>)).As(typeof(IPipelineBehavior<,>));
            builder.RegisterGeneric(typeof(RequestExceptionProcessorBehavior<,>)).As(typeof(IPipelineBehavior<,>));

            builder.Register<ServiceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            // Register .Net components
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);

            var container = builder.Build();

            // DependencyResolver for API
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);

            WebApiConfig.Register(config);//register the request  
        }
    }
}
