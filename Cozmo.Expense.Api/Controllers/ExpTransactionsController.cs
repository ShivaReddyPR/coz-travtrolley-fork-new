﻿using Cozmo.Expense.Application.BusinessOps;
using MediatR;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Cozmo.Expense.Api.Controllers
{
    /// <summary>
    /// Controller to perform all expense transactions screens related actions
    /// </summary>
    //[Authorize]
    [RoutePrefix("api/expTransactions")]
    public class ExpTransactionsController : ApiController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// ExpTransactions constructor
        /// </summary>
        /// <param name="mediator"></param>
        public ExpTransactionsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Gets the available trips info to link expense report
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExpReportScreenData")]
        [ResponseType(typeof(GetExpReportScreenData.ViewModel))]
        public async Task<IHttpActionResult> GetExpReportScreenData([FromBody]GetExpReportScreenData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the available trips info to link expense report
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getTripsToLinkExpense")]
        [ResponseType(typeof(GetAvailableTrips.ViewModel))]
        public async Task<IHttpActionResult> GetTripsToLinkExpense([FromBody]GetAvailableTrips.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the available trips info to link expense report
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveExpenseReport")]
        [ResponseType(typeof(SaveExpenseReport.ViewModel))]
        public async Task<IHttpActionResult> SaveExpenseReport([FromBody]SaveExpenseReport.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the available trips info to link expense report
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveTripsExpenseStatus")]
        [ResponseType(typeof(SaveTripsExpenseStatus.ViewModel))]
        public async Task<IHttpActionResult> SaveTripsExpenseStatus([FromBody]SaveTripsExpenseStatus.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the expense create screen load data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExpCreateData")]
        [ResponseType(typeof(GetExpCreateData.ViewModel))]
        public async Task<IHttpActionResult> GetExpCreateData([FromBody]GetExpCreateData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the expense create screen feilds data based on expense type
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExpTypeFieldsData")]
        [ResponseType(typeof(GetExpTypeFieldsData.ViewModel))]
        public async Task<IHttpActionResult> GetExpTypeFieldsData([FromBody]GetExpTypeFieldsData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Saves the expense detail data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveExpDtlData")]
        [ResponseType(typeof(SaveExpDtlData.ViewModel))]
        public async Task<IHttpActionResult> SaveExpDtlData([FromBody]SaveExpDtlData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Deletes the expense detail data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("removeExpDtlData")]
        [ResponseType(typeof(RemoveExpDtlData.ViewModel))]
        public async Task<IHttpActionResult> RemoveExpDtlData([FromBody]RemoveExpDtlData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the company attendess info from attendee master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("generateAffidavit")]
        [ResponseType(typeof(GenerateAffidavit.ViewModel))]
        public async Task<IHttpActionResult> GetCompAttendees([FromBody]GenerateAffidavit.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the available trips info to link expense report
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getOpenRceiptsDetails")]
        [ResponseType(typeof(GetExpenseReceipt.ViewModel))]
        public async Task<IHttpActionResult> GetOpenRceiptsDetails([FromBody]GetOpenRceiptsDetails.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the available trips info to link expense report
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExpenseReceipts")]
        [ResponseType(typeof(GetExpenseReceipt.ViewModel))]
        public async Task<IHttpActionResult> GetExpenseReceipts([FromBody]GetExpenseReceipt.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the available trips info to link expense report
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveExpenseReceipts")]
        [ResponseType(typeof(SaveExpReceipts.ViewModel))]
        public async Task<IHttpActionResult> SaveExpenseReceipts([FromBody]SaveExpReceipts.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Submits the expense report and details for approval
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("subDelExpReport")]
        [ResponseType(typeof(SaveExpReceipts.ViewModel))]
        public async Task<IHttpActionResult> SubDelExpReport([FromBody]SubDelExpReport.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the  Expense Approver Reports Dto.
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExpenseApproverQueue")]
        [ResponseType(typeof(GetExpApproverQueue.ViewModel))]
        public async Task<IHttpActionResult> GetExpenseApproverQueue([FromBody]GetExpApproverQueue.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }
        /// <summary>
        /// Gets the  Expense Approver Reports.
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveExpApprovalReports")]
        [ResponseType(typeof(SaveExpApprovalReports.ViewModel))]
        public async Task<IHttpActionResult> SaveExpApprovalReports([FromBody]SaveExpApprovalReports.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Save the  Expense Approver Reports.
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExpReportDetails")]
        [ResponseType(typeof(GetExpReportDetails.ViewModel))]
        public async Task<IHttpActionResult> GetExpReportDetails([FromBody]GetExpReportDetails.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the company attendess info from attendee master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCompanyAttendees")]
        [ResponseType(typeof(GetCompAttendees.ViewModel))]
        public async Task<IHttpActionResult> GetCompAttendees([FromBody]GetCompAttendees.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }
    }
}
