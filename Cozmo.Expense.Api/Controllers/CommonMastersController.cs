﻿using Cozmo.Expense.Application.Common;
using MediatR;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Cozmo.Expense.Api.Controllers
{
    /// <summary>
    /// Controller to perform all common master screens related actions
    /// </summary>
    //[Authorize]
    [RoutePrefix("api/commonMasters")]
    public class CommonMastersController : ApiController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// CommonMasters constructor
        /// </summary>
        /// <param name="mediator"></param>
        public CommonMastersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Gets the cards info from card master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpGet, Route("getDynamicList")]
        [ResponseType(typeof(GetDynamicList.ViewModel))]
        public async Task<IHttpActionResult> GetDynamicList([FromBody]GetDynamicList.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the duplicate records for the given key values
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getDuplicates")]
        [ResponseType(typeof(GetDynamicList.ViewModel))]
        public async Task<IHttpActionResult> GetDuplicates([FromBody]GetDuplicates.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the city wise currency from city master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCityWiseCurrency")]
        [ResponseType(typeof(GetCityWiseCurrency.ViewModel))]
        public async Task<IHttpActionResult> GetCityWiseCurrency([FromBody]GetCityWiseCurrency.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the cards info from card master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCardsInfo")]
        [ResponseType(typeof(GetCardsInfo.ViewModel))]
        public async Task<IHttpActionResult> GetCardsInfo([FromBody]GetCardsInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Save the cards info into card master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("addCardsInfo")]
        [ResponseType(typeof(AddCardsInfo.ViewModel))]
        public async Task<IHttpActionResult> AddCardsInfo([FromBody]AddCardsInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get card master screen data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCardMasterScreenLoadData")]
        [ResponseType(typeof(GetCardMasterScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetCardMasterScreenLoadData([FromBody]GetCardMasterScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get cost center data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCostCenterData")]
        [ResponseType(typeof(GetCostCenterData.ViewModel))]
        public async Task<IHttpActionResult> GetCostCenterData([FromBody]GetCostCenterData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get UOM List
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getUOMData")]
        [ResponseType(typeof(GetUOMData.ViewModel))]
        public async Task<IHttpActionResult> GetUOMData([FromBody]GetUOMData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To save payment Type info
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("savePaymentTypeInfo")]
        [ResponseType(typeof(SavePaymentType.ViewModel))]
        public async Task<IHttpActionResult> savePaymentTypeInfo([FromBody]SavePaymentType.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get Payment Type Information
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getPaymentTypeInfo")]
        [ResponseType(typeof(GetPaymentTypeInfo.ViewModel))]
        public async Task<IHttpActionResult> GetPaymentTypeInfo([FromBody]GetPaymentTypeInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get screenList 
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getScreenList")]
        [ResponseType(typeof(GetScreenList.ViewModel))]
        public async Task<IHttpActionResult> GetScreenList([FromBody]GetScreenList.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get paymentType screenload data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getPaymentTypeScreenLoadData")]
        [ResponseType(typeof(GetPaymentTypeScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetPaymentTypeScreenLoadData([FromBody]GetPaymentTypeScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

    }
}
