﻿using Cozmo.Expense.Application.BusinessOps;
using MediatR;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Cozmo.Expense.Api.Controllers
{
    /// <summary>
    /// Controller to perform all expense master screens related actions
    /// </summary>
    //[Authorize]
    [RoutePrefix("api/expenseMasters")]
    public class ExpMasterController : ApiController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// ExpMaster constructor
        /// </summary>
        /// <param name="mediator"></param>
        public ExpMasterController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// get attendee master screen data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getAttendeeMasterScreenLoadData")]
        [ResponseType(typeof(GetAttendeeMasterScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetAttendeeMasterScreenLoadData([FromBody]GetAttendeeMasterScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Save the attendee info into attendee master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("addAttendeeInfo")]
        [ResponseType(typeof(AddAttendeeInfo.ViewModel))]
        public async Task<IHttpActionResult> AddAttendeeInfo([FromBody]AddAttendeeInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get company vendor data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCompanyVendorData")]
        [ResponseType(typeof(GetCompanyVendorData.ViewModel))]
        public async Task<IHttpActionResult> GetCompanyVendorData([FromBody]GetCompanyVendorData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the attendee info from attendee master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getAttendeeInfo")]
        [ResponseType(typeof(GetAttendeeInfo.ViewModel))]
        public async Task<IHttpActionResult> GetAttendeeInfo([FromBody]GetAttendeeInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get policy master screen data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getPolicyMasterScreenLoadData")]
        [ResponseType(typeof(GetPolicyMasterScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetPolicyMasterScreenLoadData([FromBody]GetPolicyMasterScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the expenses type data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExpTypeData")]
        [ResponseType(typeof(GetExpTypeData.ViewModel))]
        public async Task<IHttpActionResult> GetExpTypeInfo([FromBody]GetExpTypeData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the UOM, paytype, costcenter info based on expense type
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExpTypeChangeData")]
        [ResponseType(typeof(GetExpTypeChangeData.ViewModel))]
        public async Task<IHttpActionResult> GetExpTypeInfo([FromBody]GetExpTypeChangeData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Save the policy info into policy master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("addPolicyInfo")]
        [ResponseType(typeof(AddPolicyInfo.ViewModel))]
        public async Task<IHttpActionResult> AddPolicyInfo([FromBody]AddPolicyInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Gets the policy info from policy master
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getPolicyInfo")]
        [ResponseType(typeof(GetPolicyInfo.ViewModel))]
        public async Task<IHttpActionResult> GetPolicyInfo([FromBody]GetPolicyInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get companyVendor screen data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCompanyVendorScreenLoadData")]
        [ResponseType(typeof(GetCompanyVendorScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetCompanyVendorScreenLoadData([FromBody]GetCompanyVendorScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To save company vendor masters information 
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveCompanyVendor")]
        [ResponseType(typeof(SaveCompanyVendor.ViewModel))]
        public async Task<IHttpActionResult> SaveCompanyVendor([FromBody]SaveCompanyVendor.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get company vendor masters information 
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCompanyVendor")]
        [ResponseType(typeof(GetCompanyVendor.ViewModel))]
        public async Task<IHttpActionResult> GetCompanyVendor([FromBody]GetCompanyVendor.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get categories screen data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCategoriesScreenLoadData")]
        [ResponseType(typeof(GetCategoriesScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetCategoriesScreenLoadData([FromBody]GetCategoriesScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get categories data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCategoriesData")]
        [ResponseType(typeof(GetCategoriesData.ViewModel))]
        public async Task<IHttpActionResult> GetCategoriesData([FromBody]GetCategoriesData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To save categories masters information 
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveCategories")]
        [ResponseType(typeof(SaveCategories.ViewModel))]
        public async Task<IHttpActionResult> SaveCategories([FromBody]SaveCategories.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get categories masters information 
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCategories")]
        [ResponseType(typeof(GetCategoriesInfo.ViewModel))]
        public async Task<IHttpActionResult> GetCategories([FromBody]GetCategoriesInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To save type masters information 
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveTypes")]
        [ResponseType(typeof(SaveTypes.ViewModel))]
        public async Task<IHttpActionResult> SaveTypes([FromBody]SaveTypes.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get type masters information 
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getTypes")]
        [ResponseType(typeof(GetTypesInfo.ViewModel))]
        public async Task<IHttpActionResult> GetTypes([FromBody]GetTypesInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get types data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getTypesData")]
        [ResponseType(typeof(GetTypesData.ViewModel))]
        public async Task<IHttpActionResult> GetTypesData([FromBody]GetTypesData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get OrderDepend data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getOrderDepend")]
        [ResponseType(typeof(GetOrderDepend.ViewModel))]
        public async Task<IHttpActionResult> GetOrderDepend([FromBody]GetOrderDepend.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get DisplayDepend data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getDisplayDepend")]
        [ResponseType(typeof(GetDisplayDepend.ViewModel))]
        public async Task<IHttpActionResult> GetDisplayDepend([FromBody]GetDisplayDepend.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get flex data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getFlexInfo")]
        [ResponseType(typeof(GetFlexMaster.ViewModel))]
        public async Task<IHttpActionResult> GetFlexInfo([FromBody]GetFlexMaster.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To save flex info
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveFlexInfo")]
        [ResponseType(typeof(SaveFlexMaster.ViewModel))]
        public async Task<IHttpActionResult> SaveFlexInfo([FromBody]SaveFlexMaster.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }        

        /// <summary>
        /// get flex master screen data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getFlexMasterScreenLoadData")]
        [ResponseType(typeof(GetFlexMasterScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetFlexMasterScreenLoadData([FromBody]GetFlexMasterScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get Types screen data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getTypesScreenLoadData")]
        [ResponseType(typeof(GetTypesScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetTypesScreenLoadData([FromBody]GetTypesScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

       /// <summary>
       /// get city master screen data
       /// </summary>
       /// <param name="clsQuery"></param>
       /// <returns></returns>
        [HttpPost, Route("getCityMasterScreenLoadData")]
        [ResponseType(typeof(GetCityMasterScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetCityMasterScreenLoadData([FromBody]GetCityMasterScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get city master data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getCityData")]
        [ResponseType(typeof(GetCityInfo.ViewModel))]
        public async Task<IHttpActionResult> GetCityInfo([FromBody]GetCityInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To save city info
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveCityInfo")]
        [ResponseType(typeof(SaveExpCityMaster.ViewModel))]
        public async Task<IHttpActionResult> SaveCityInfo([FromBody]SaveExpCityMaster.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// get exchange master screen data
        /// </summary>
        /// <param name="clsQuery"></param>
        /// <returns></returns>
        [HttpPost, Route("getExchangeMasterScreenLoadData")]
        [ResponseType(typeof(GetExchangeMasterScreenLoadData.ViewModel))]
        public async Task<IHttpActionResult> GetExchangeMasterScreenLoadData([FromBody]GetExchangeMasterScreenLoadData.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To get exchange master data
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("getExchangeData")]
        [ResponseType(typeof(GetExchangeInfo.ViewModel))]
        public async Task<IHttpActionResult> GetExchangeInfo([FromBody]GetExchangeInfo.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// To save Exchange info
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("saveExchangeInfo")]
        [ResponseType(typeof(SaveExpExchangeMaster.ViewModel))]
        public async Task<IHttpActionResult> SaveExchangeInfo([FromBody]SaveExpExchangeMaster.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }
    }
}
