﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Cozmo.Application.Transfer;


namespace Cozmo.Transfer.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/transfer")]
    public class TransferController : ApiController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Transfer constructor
        /// </summary>
        /// <param name="mediator"></param>
        public TransferController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Gets the transfer results using the search request
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("searchTransfer")]
        [ResponseType(typeof(SearchTransfer.ViewModel))]
        public async Task<IHttpActionResult> SearchTransfer([FromBody]SearchTransfer.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Book transfer using the Itinerary
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("bookTransfer")]
        [ResponseType(typeof(BookTransfer.ViewModel))]
        public async Task<IHttpActionResult> BookTransfer([FromBody]BookTransfer.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Get transfer booking confirmation
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("transferConfirmation")]
        [ResponseType(typeof(TransferConfirmation.ViewModel))]
        public async Task<IHttpActionResult> TransferConfirmation([FromBody]TransferConfirmation.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }
        /// <summary>
        /// Get transfer booking queue
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("transferQueue")]
        [ResponseType(typeof(TransferQueue.ViewModel))]
        public async Task<IHttpActionResult> TransferQueue([FromBody]TransferQueue.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Get transfer booking queue
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("transferCancel")]
        [ResponseType(typeof(TransferCancelBooking.ViewModel))]
        public async Task<IHttpActionResult> TransferCancelBooking([FromBody]TransferCancelBooking.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }
        /// <summary>
        /// Get transfer booking queue
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("transferInvoice")]
        [ResponseType(typeof(TransferInvoice.ViewModel))]
        public async Task<IHttpActionResult> TransferInvoice([FromBody]TransferInvoice.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }

        /// <summary>
        /// Get transfer booking queue
        /// </summary>
        /// <param name="clsQuery"></param>
        [HttpPost, Route("transferVoucher")]
        [ResponseType(typeof(TransferInvoice.ViewModel))]
        public async Task<IHttpActionResult> TransferVoucher([FromBody]TransferVoucher.Query clsQuery)
        {
            var result = await _mediator.Send(clsQuery);

            return Ok(result);
        }
    }
}
