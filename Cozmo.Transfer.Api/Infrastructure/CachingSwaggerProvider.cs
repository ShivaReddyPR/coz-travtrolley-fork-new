﻿using Swashbuckle.Swagger;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cozmo.Transfer.Api.Infrastructure
{
    /// <summary>
    /// Prevents expensive doc regeneration.
    /// </summary>
    /// <seealso cref="Swashbuckle.Swagger.ISwaggerProvider" />
    public class CachingSwaggerProvider : ISwaggerProvider
    {
        private static ConcurrentDictionary<string, SwaggerDocument> _cache =
            new ConcurrentDictionary<string, SwaggerDocument>();

        private readonly ISwaggerProvider _swaggerProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachingSwaggerProvider"/> class.
        /// </summary>
        /// <param name="swaggerProvider">The swagger provider.</param>
        public CachingSwaggerProvider(ISwaggerProvider swaggerProvider)
        {
            _swaggerProvider = swaggerProvider;
        }

        /// <summary>
        /// Gets the swagger document.
        /// </summary>
        /// <param name="rootUrl">The root URL.</param>
        /// <param name="apiVersion">The API version.</param>
        /// <returns></returns>
        public SwaggerDocument GetSwagger(string rootUrl, string apiVersion)
        {
            var cacheKey = string.Format("{0}_{1}", rootUrl, apiVersion);
            return _cache.GetOrAdd(cacheKey, (key) => _swaggerProvider.GetSwagger(rootUrl, apiVersion));
        }
    }
}