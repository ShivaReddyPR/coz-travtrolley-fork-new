﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Channels;

namespace PKTicketService
{
    public class RawContentTypeMapper : WebContentTypeMapper
    {


        public override WebContentFormat GetMessageFormatForContentType(string contentType)
        {

            if (string.IsNullOrWhiteSpace(contentType))
                return WebContentFormat.Raw;
            else
                return WebContentFormat.Json;

        }
    }

}