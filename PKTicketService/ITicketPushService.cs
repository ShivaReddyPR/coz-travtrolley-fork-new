﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;

namespace PKTicketService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ITicketPushService
    {

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,RequestFormat =WebMessageFormat.Json)]
        TicketNumResponse ImportTickets(TicketPush ticket);
        
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class TicketPush
    {
        string airPnr;
        string orderNum ;
        string paymentGate;
        string serialNum;
        string merchantOrder;
        TicketNumberData[] ticketNums;

        //MODIFIED BY LOKESH ON 10AUG2019 AS PER PKFARE API Document-Ticketing Number Push_V2
        string pnr;//GDS PNR//HYFBKU
        int permitVoid;//allow to void ticket or not. [1-yes, 0-nt allow]
        string lastVoidTime;//voiding deadline. Voiding operation would not be allow after lastVoidTime. (GMT) [yyyy-MM-dd HH:mm:ss]
        decimal voidServiceFee;//void service fee//5.00
        string currency;//currency for void service fee//USD
        object data;//Include remark field for reference

        [DataMember(Name ="airPnr")]
        public string AirPnr
        {
            get
            {
                return airPnr;
            }

            set
            {
                airPnr = value;
            }
        }

        [DataMember(Name ="orderNum")]
        public string OrderNum
        {
            get
            {
                return orderNum;
            }

            set
            {
                orderNum = value;
            }
        }

        [DataMember(Name ="paymentGate")]
        public string PaymentGate
        {
            get
            {
                return paymentGate;
            }

            set
            {
                paymentGate = value;
            }
        }

        [DataMember(Name ="serialNum")]
        public string SerialNum
        {
            get
            {
                return serialNum;
            }

            set
            {
                serialNum = value;
            }
        }
        [DataMember(Name ="merchantOrder")]
        public string MerchantOrder
        {
            get
            {
                return merchantOrder;
            }

            set
            {
                merchantOrder = value;
            }
        }

        [DataMember(Name ="ticketNums")]
        public TicketNumberData[] TicketNums
        {
            get
            {
                return ticketNums;
            }

            set
            {
                ticketNums = value;
            }
        }

        //MODIFIED BY LOKESH ON 10AUG2019 AS PER PKFARE API Document-Ticketing Number Push_V2

        [DataMember(Name = "pnr")]
        public string Pnr
        {
            get
            {
                return pnr;
            }

            set
            {
                pnr = value;
            }
        }

        [DataMember(Name = "permitVoid")]
        public int  PermitVoid
        {
            get
            {
                return permitVoid;
            }

            set
            {
                permitVoid = value;
            }
        }

        [DataMember(Name = "LastVoidTime")]
        public string LastVoidTime
        {
            get
            {
                return lastVoidTime;
            }

            set
            {
                lastVoidTime = value;
            }
        }


        [DataMember(Name = "voidServiceFee")]
        public decimal VoidServiceFee
        {
            get
            {
                return voidServiceFee;
            }

            set
            {
                voidServiceFee = value;
            }
        }

        [DataMember(Name = "currency")]
        public string Currency
        {
            get
            {
                return currency;
            }

            set
            {
                currency = value;
            }
        }

        [DataMember(Name = "data")]
        public object  Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

    }

    [DataContract]
    public class TicketNumberData
    {
        string ticketNum;
        //string psgName;//NOT IN USE AS PER TicketingNumPush_V2

        //MODIFIED BY LOKESH ON 10AUG2019 AS PER PKFARE API Document-Ticketing Number Push_V2
        string lastName;//WANG
        string firstName;//JANGO
        string cardType;//travel document type. //P
        string cardNum;//travel document number//P1234567
        string sex;//gender//F
        string birthday;//birthday, format[YYYY-MM-DD]//2000-01-01


        [DataMember(IsRequired =true,Name ="ticketNum")]
        public string TicketNum
        {
            get
            {
                return ticketNum;
            }

            set
            {
                ticketNum = value;
            }
        }

        ////NOT IN USE AS PER TicketingNumPush_V2
        //[DataMember(IsRequired =true,Name ="psgName")]
        //public string PsgName
        //{
        //    get
        //    {
        //        return psgName;
        //    }

        //    set
        //    {
        //        psgName = value;
        //    }
        //}

        //MODIFIED BY LOKESH ON 10AUG2019 AS PER PKFARE API Document-Ticketing Number Push_V2
        [DataMember(IsRequired = true, Name = "lastName")]
        public string LastName
        {
            get
            {
                return lastName;
            }

            set
            {
                lastName = value;
            }
        }
        
        [DataMember(IsRequired = true, Name = "firstName")]
        public string FirstName
        {
            get
            {
                return firstName;
            }

            set
            {
                firstName = value;
            }
        }

        [DataMember(IsRequired = true, Name = "cardType")]
        public string CardType
        {
            get
            {
                return cardType;
            }

            set
            {
                cardType = value;
            }
        }

        [DataMember(IsRequired = true, Name = "cardNum")]
        public string CardNum
        {
            get
            {
                return cardNum;
            }

            set
            {
                cardNum = value;
            }
        }

        [DataMember(IsRequired = true, Name = "sex")]
        public string Sex
        {
            get
            {
                return sex;
            }

            set
            {
                sex = value;
            }
        }

        [DataMember(IsRequired = true, Name = "birthday")]
        public string Birthday
        {
            get
            {
                return birthday;
            }
            set
            {
                birthday = value;
            }
        }

    }

    [DataContract]
    public class TicketNumResponse
    {
        int errorCode;
        string errorMsg;

        [DataMember(Name ="errorCode")]
        public int ErrorCode
        {
            get
            {
                return errorCode;
            }

            set
            {
                errorCode = value;
            }
        }

        [DataMember(Name ="errorMsg")]
        public string ErrorMsg
        {
            get
            {
                return errorMsg;
            }

            set
            {
                errorMsg = value;
            }
        }
    }
}
