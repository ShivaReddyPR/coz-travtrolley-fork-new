﻿using CT.BookingEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using CT.Core;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using System.Xml;
using System.Data;

namespace PKTicketService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class TicketService : ITicketPushService
    {
        protected string xmlLogPath;

        public TicketService()
        {
            xmlLogPath = ConfigurationManager.AppSettings["XmlLogPath"];
            xmlLogPath += DateTime.Now.ToString("ddMMMyyyy") + "\\";
            if(!Directory.Exists(xmlLogPath))
            {
                Directory.CreateDirectory(xmlLogPath);
            }            
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,RequestFormat = WebMessageFormat.Json)]
        public TicketNumResponse ImportTickets(TicketPush ticket)
        {          
            int counter = 0;
            string pnr = string.Empty, responseMsg=string.Empty;
            TicketNumResponse response = new TicketNumResponse();
            
            try
            {
                if (ticket != null && !string.IsNullOrEmpty(ticket.AirPnr) && ticket.TicketNums != null)
                {
                    pnr = ticket.AirPnr.Split('/')[1];
                    string filepath = xmlLogPath + "TicketNumPushRequest_" + ticket.OrderNum + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");

                    //NOT IN USE AS OF NOW
                    //XmlSerializer ser = new XmlSerializer(typeof(TicketPush));
                    //StreamWriter sw = new StreamWriter(filepath + ".xml");
                    //ser.Serialize(sw, ticket);
                    //sw.Close();

                    //SAVE THE JSON RESPONSE FROMAT AS THE SUPPLIER SENDS IN JSON FORMAT
                    //CATCH THE EXCEPTION IF THE RESPONSE IS NOT A VALID JSON.
                    try
                    {
                        //RESPONSE IN JSON FORMAT
                        StreamWriter sw = new StreamWriter(filepath + ".json");
                        string rsp = JsonConvert.SerializeObject(ticket, Newtonsoft.Json.Formatting.Indented);
                        sw.Write(rsp);
                        sw.Close();
                    }
                    catch(Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.Normal, 0, "PK FARES TICKETING SERVICE FAILED TO SERIALIZE.Reason:"+ex.ToString(), "");
                    }

                    int flightId = FlightItinerary.GetPNRforPKFareOrderNumber(ticket.OrderNum);

                    if (flightId > 0)
                    {
                        try
                        {
                            int count = FlightItinerary.UpdateAirlinePNR(flightId, pnr);
                            if (count > 0)
                            {
                                Audit.Add(EventType.Exception, Severity.Normal, 0, "Updated PKFares AirlinePNR (" + pnr + ") for flightId (" + flightId + ") for " + count + " Segments.", "");
                            }

                            //Updated by lokesh on 19Aug2018
                            //UPDATE GDS PNR AS PER PKFARE API Document-Ticketing Number Push_V2 in BKE_FLIGHT_ITINERARY  under "universalRecord" column
                            count = 0;
                            count = FlightItinerary.UpdatePKFaresGDSPNR(flightId, ticket.Pnr);
                            if (count > 0)
                            {
                                Audit.Add(EventType.Exception, Severity.Normal, 0, "Updated PKFares GDSPNR (" + ticket.Pnr + ") for flightId (" + flightId + ") for " + count + " Segments.", "");
                            }
                        }
                        catch { }

                        DataTable dtTickets = Ticket.GetTicketsForPKFare(flightId);

                        if (dtTickets != null && dtTickets.Rows.Count > 0)
                        {
                            TicketNumberData[] ticketNums = ticket.TicketNums;

                            for (int j = 0; j < ticketNums.Length; j++)
                            {
                                //Ticket ticketObj = tickets.Find(delegate (Ticket t) { return t.PaxLastName.Trim().ToLower() + "/" + t.PaxFirstName.Trim().ToLower() == ticketNums[j].PsgName.ToLower(); });
                                DataRow[] ticketRow = dtTickets.Select("PaxLastName='" + ticketNums[j].LastName + "' AND PaxFirstName='" + ticketNums[j].FirstName + "'");
                                if (ticketRow != null && ticketRow.Length > 0)
                                {
                                    Ticket ticketObj = new Ticket();
                                    ticketObj.Load(Convert.ToInt32(ticketRow[0]["ticketId"]));
                                    ticketObj.TicketNumber = ticketNums[j].TicketNum;
                                    try
                                    {
                                        counter += ticketObj.UpdateTicketNumber();
                                        Audit.Add(EventType.Ticketing, Severity.Normal, ticketObj.CreatedBy, "(PKFare TicketNumPush)Updated Ticket Number: " + ticketObj.TicketNumber + " updated for PNR :" + ticket.AirPnr + " PaxName : " + ticketObj.PaxFirstName + " " + ticketObj.PaxLastName, "");
                                    }
                                    catch (Exception ex)
                                    {
                                        response.ErrorCode = 1;
                                        response.ErrorMsg = "Failed to update Tickets for Order Number " + ticket.OrderNum + ". Reason : " + ex.ToString();
                                        Audit.Add(EventType.Ticketing, Severity.Normal, ticketObj.CreatedBy, "(PKFare TicketNumPush)Failed to Update Ticket Number: " + ticketObj.TicketNumber + " updated for PNR :" + ticket.AirPnr + " PaxName : " + ticketObj.PaxFirstName + " " + ticketObj.PaxLastName + ". Reason : " + ex.ToString(), "");
                                    }
                                }
                            }

                            if ((counter) == ticketNums.Length)//Success
                            {
                                response.ErrorCode = 0;
                                response.ErrorMsg = "ok";
                            }
                            else
                            {
                                response.ErrorCode = 1;
                                response.ErrorMsg = "Failed to update Tickets for Order Number " + ticket.OrderNum;
                            }
                        }
                        else
                        {
                            response.ErrorCode = 1;
                            response.ErrorMsg = "Unable to retrieve Tickets for Order Number " + ticket.OrderNum;
                        }
                    }
                    else
                    {
                        response.ErrorCode = 1;
                        response.ErrorMsg = "Unable to retrieve Flight for Order Number " + ticket.OrderNum;
                    }
                }
            }
            catch(Exception ex)
            {
                response.ErrorCode = 1;
                response.ErrorMsg = "Failed to update Tickets for Order Number " + ticket.OrderNum + ". Reason : " + ex.ToString();
                Audit.Add(EventType.Ticketing, Severity.Normal, 0, "(PKFare TicketNumPush)Failed to Update Ticket Number. Reason : " + ex.ToString(), "");
            }
            finally
            {
                string filepath = xmlLogPath + "TicketNumPushResponse_" + ticket.OrderNum + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                XmlSerializer ser = new XmlSerializer(typeof(TicketNumResponse));
                //StreamWriter sw = new StreamWriter(filepath + ".xml");
                //ser.Serialize(sw, response);
                //sw.Close();

                StreamWriter sw  = new StreamWriter(filepath + ".json");
                responseMsg = JsonConvert.SerializeObject(response, Newtonsoft.Json.Formatting.Indented);
                sw.Write(responseMsg);
                sw.Close();
            }

            return response;
        }
    }
}
