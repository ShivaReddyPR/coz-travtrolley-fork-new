﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ActivityAPI
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IActivityService" in both code and config file together.
    [ServiceContract]
    public interface IActivityService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        AuthenticationResponse Authenticate(AuthenticationRequest authenticationRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ActivitySearchResponse ActivitySearch(ActivitySearchRequest searchRequest);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ActivityBookingResponse ActivityBooking(ActivityBookingRequest activityBookingRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ActivityCancellationResponse ActivityCancellationDetails(ActivityCancellationRequest activityCancellationRequest);
    }
}
