﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace ActivityAPI
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ActivityService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ActivityService.svc or ActivityService.svc.cs at the Solution Explorer and start debugging.
    public class ActivityService : IActivityService
    {
        protected string XmlPath = ConfigurationManager.AppSettings["xmlFlieSavingPath"];
        int userId = 0;
        string ActivityServices = "(ActivityService):";

        public ActivityService()
        {
            if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["xmlFlieSavingPath"]))
            {
                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["xmlFlieSavingPath"]);
            }
        }

        public AuthenticationResponse Authenticate(AuthenticationRequest authenticationRequest)
        {
            AuthenticationResponse response = new AuthenticationResponse();
            try
            {
                Random random = new Random();
                string rndNumber = random.Next(100000, 999999).ToString();
                XmlSerializer serializer = new XmlSerializer(authenticationRequest.GetType());
                StringBuilder sb = new StringBuilder();
                StringWriter strwriter = new StringWriter(sb);
                serializer.Serialize(strwriter, authenticationRequest);

                string filepath = XmlPath + "AuthenticationRequest_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + "_"+rndNumber+".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(filepath);

                Audit.Add(EventType.Login, Severity.Normal, 0, ActivityServices + filepath, authenticationRequest.UserIPAddress);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.Normal, 0, ActivityServices + "Failed to save the ActivitySearchRequest:" + ex.Message, (!string.IsNullOrEmpty(authenticationRequest.UserIPAddress) ? authenticationRequest.UserIPAddress : string.Empty));//, Convert.ToInt32(CT.BookingEngine.ProductType.Activity), "ActivityService", "Cozmo", "AuthenticationRequest, UserName:" + authenticationRequest.UserName, "Authentication Request"
            }
            try
            {
                string errorMessage = string.Empty;
                if (string.IsNullOrEmpty(authenticationRequest.UserName))
                {
                    errorMessage = "Please send the UserName";
                }
                else if (string.IsNullOrEmpty(authenticationRequest.Password))
                {
                    errorMessage = "Please send the Password";
                }
                else if (string.IsNullOrEmpty(authenticationRequest.UserIPAddress))
                {
                    errorMessage = "Please send the IPAddress";
                }
                if (string.IsNullOrEmpty(errorMessage))
                {
                    response = AuthenticationResponse.ActivityAuthentication(authenticationRequest, ref userId);
                }
                else
                {
                    Error error = new Error();
                    error.ErrorCode = "002";
                    error.ErrorMessage = errorMessage;
                    response.Error = error;
                    response.FirstName = string.Empty;
                    response.LastName = string.Empty;
                    response.LoginTime = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt"); ;
                    response.TokenId = string.Empty;
                    response.EmailId = string.Empty;
                    response.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"); ;
                }
            }
            catch (Exception ex)
            {
                Error error = new Error();
                error.ErrorCode = "003";
                error.ErrorMessage = "Authentication Failed, due to technical error";
                response.Error = error;
                response.FirstName = string.Empty;
                response.LastName = string.Empty;
                response.LoginTime = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt"); ;
                response.TokenId = string.Empty;
                response.EmailId = string.Empty;
                response.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"); ;

                Audit.Add(EventType.Login, Severity.High, 0, ActivityServices + "Failed to generate the Authentication:" + ex.ToString(), authenticationRequest.UserIPAddress);
            }
            try
            {
                Random random = new Random();
                string rndNumber = random.Next(100000, 999999).ToString();

                XmlSerializer serializer = new XmlSerializer(response.GetType());
                StringBuilder sb = new StringBuilder();
                StringWriter strwriter = new StringWriter(sb);
                serializer.Serialize(strwriter, response);

                string filepath = XmlPath + "AuthenticationResponse_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") +"_"+rndNumber +".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(filepath);

                Audit.Add(EventType.Login, Severity.Normal, userId, ActivityServices + filepath, authenticationRequest.UserIPAddress);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.Normal, userId, ActivityServices + "Failed to save the Authentication Response:" + ex.Message, (!string.IsNullOrEmpty(authenticationRequest.UserIPAddress) ? authenticationRequest.UserIPAddress : string.Empty));
            }
            return response;
        }

        public ActivitySearchResponse ActivitySearch(ActivitySearchRequest activitySearchRequest)
        {
            ActivitySearchResponse searchResponse = new ActivitySearchResponse();

            try
            {
                //if getting details are missed throwing exception
                ValidateSearchRequests(activitySearchRequest);
                try
                {
                    Random random = new Random();
                    string rndNumber = random.Next(100000, 999999).ToString();

                    XmlSerializer serialize = new XmlSerializer(activitySearchRequest.GetType());
                    StringBuilder sb = new StringBuilder();
                    StringWriter strwriter = new StringWriter(sb);
                    serialize.Serialize(strwriter, activitySearchRequest);

                    string filePath = XmlPath + "ActivitySearchRequest_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + "_" + activitySearchRequest.TokenID + "_"+rndNumber + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    doc.Save(filePath);

                    Audit.Add(EventType.Search, Severity.Normal, userId, ActivityServices + filePath, (!string.IsNullOrEmpty(activitySearchRequest.UserIPAddress) ? activitySearchRequest.UserIPAddress : string.Empty));
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.Normal, userId, ActivityServices + "Failed to save the ActivitySearchRequest" + ex.ToString(), (!string.IsNullOrEmpty(activitySearchRequest.UserIPAddress) ? activitySearchRequest.UserIPAddress : string.Empty));
                }

                LoginInfo loginInfo = AuthenticationResponse.ValidateUser(activitySearchRequest.TokenID);
                if (loginInfo != null)
                {
                    userId = Convert.ToInt32(loginInfo.UserID);
                    ActivitySearchResults[] activitySearchResults = ActivitySearchResults.GetSearchResults(activitySearchRequest, activitySearchRequest.AgencyId);

                    if (activitySearchResults != null && activitySearchResults.Length > 0)
                    {
                        Error error = new Error();
                        error.ErrorCode = "000";
                        error.ErrorMessage = "Search Successful";
                        searchResponse.Error = error;
                        searchResponse.ResultsCount = activitySearchResults.Length;
                        searchResponse.SearchResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                        searchResponse.SearchResults = activitySearchResults;
                        searchResponse.TokenId = activitySearchRequest.TokenID;
                    }
                    else
                    {
                        Error error = new Error();
                        error.ErrorCode = "005";
                        error.ErrorMessage = "Failed due to technical error";
                        searchResponse.Error = error;
                        searchResponse.ResultsCount = 0;
                        searchResponse.SearchResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                        searchResponse.SearchResults = null;
                        searchResponse.TokenId = activitySearchRequest.TokenID;
                    }
                }
                else
                {
                    Error error = new Error();
                    error.ErrorCode = "004";
                    error.ErrorMessage = "Login Failed, session expired";
                    searchResponse.Error = error;
                    searchResponse.ResultsCount = 0;
                    searchResponse.SearchResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                    searchResponse.SearchResults = null;
                    searchResponse.TokenId = activitySearchRequest.TokenID;
                }
            }
            catch (Exception ex)
            {
                Error error = new Error();
                error.ErrorCode = "006";
                error.ErrorMessage = ex.Message;
                searchResponse.Error = error;
                searchResponse.ResultsCount = 0;
                searchResponse.SearchResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                searchResponse.SearchResults = null;
                searchResponse.TokenId = activitySearchRequest.TokenID;

                Audit.Add(EventType.Login, Severity.Normal, userId, ActivityServices + "Failed to generate the ActivitySearch:" + ex.ToString(), (!string.IsNullOrEmpty(activitySearchRequest.UserIPAddress) ? activitySearchRequest.UserIPAddress : string.Empty));
            }
            try
            {
                Random random = new Random();
                string rndNumber = random.Next(100000, 999999).ToString();

                XmlSerializer serializer = new XmlSerializer(searchResponse.GetType());
                StringBuilder sb = new StringBuilder();
                StringWriter strwriter = new StringWriter(sb);
                serializer.Serialize(strwriter, searchResponse);

                string filepath = XmlPath + "ActivitySearchResponse_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + "_" + activitySearchRequest.TokenID +"_"+rndNumber+ ".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(filepath);

                Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, ActivityServices + filepath, activitySearchRequest.UserIPAddress);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, ActivityServices + "Failed to save the ActivitySearchResponse:" + ex.Message.ToString(), (!string.IsNullOrEmpty(activitySearchRequest.UserIPAddress) ? activitySearchRequest.UserIPAddress : string.Empty));
            }

            return searchResponse;
        }

        public ActivityBookingResponse ActivityBooking(ActivityBookingRequest activityBookingRequest)
        {
            ActivityBookingResponse bookingResponse = new ActivityBookingResponse();

            try
            {
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                // here we are validating the BookingRequest Details                                                //
                // if any errors are raised will validating the BookingRequest details it will be handled by catch  // 
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                ValidateBookingRequest(activityBookingRequest);
                try
                {
                    Random random = new Random();
                    string rndNumber = random.Next(100000, 999999).ToString();

                    XmlSerializer serializer = new XmlSerializer(activityBookingRequest.GetType());
                    StringBuilder sb = new StringBuilder();
                    StringWriter strwriter = new StringWriter(sb);
                    serializer.Serialize(strwriter, activityBookingRequest);

                    string filePath = XmlPath + "ActivityBookingRequest" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_" + activityBookingRequest.TokenID +"_" +rndNumber+".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    doc.Save(filePath);

                    Audit.Add(EventType.Book, Severity.High, userId, ActivityServices + filePath, activityBookingRequest.UserIPAddress);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Book, Severity.High, userId, ActivityServices + "Failed to save the BookingRequest details" + ex.ToString(), activityBookingRequest.UserIPAddress);
                }

                LoginInfo loginInfo = AuthenticationResponse.ValidateUser(activityBookingRequest.TokenID);
                if (loginInfo != null)
                {
                    userId = Convert.ToInt32(loginInfo.UserID);
                    ActivityDetails.ExchangeRates = loginInfo.AgentExchangeRates;
                    ActivityDetails requestedActivityDetails = new ActivityDetails(activityBookingRequest.ResultIndex);

                    //ActivitySearchResults.ExchangeRates = loginInfo.AgentExchangeRates;
                    //ActivitySearchResults requestedActivityDetails = new ActivitySearchResults(activityBookingRequest.ResultIndex);

                    //////////////////////////////////////////////////////////////////////////////////////////////////////
                    // here we need to check the following Details                                                      //
                    // 1) Check the total price sent by them and calculate the amount once again.                       // 
                    // 2) Check the BookingCutOff days.                                                                 // 
                    // 3) Check the Agent Current Balance.                                                              //
                    // 4) Check the stock in hand and used.                                                              //
                    //////////////////////////////////////////////////////////////////////////////////////////////////////
                    decimal totalAmount = 0;
                    for (int k = 0; k < requestedActivityDetails.PriceDetails.Rows.Count; k++)
                    {
                        if (requestedActivityDetails.PriceDetails.Rows[k]["label"].ToString().Trim().ToUpper() == "ADULT" && activityBookingRequest.AdultCount > 0)
                        {
                            totalAmount += Convert.ToDecimal(requestedActivityDetails.PriceDetails.Rows[k]["amount"]) * activityBookingRequest.AdultCount;
                        }
                        else if (requestedActivityDetails.PriceDetails.Rows[k]["label"].ToString().Trim().ToUpper() == "CHILD" && activityBookingRequest.ChildCount > 0)
                        {
                            totalAmount += Convert.ToDecimal(requestedActivityDetails.PriceDetails.Rows[k]["amount"]) * activityBookingRequest.ChildCount;
                        }
                        else if (requestedActivityDetails.PriceDetails.Rows[k]["label"].ToString().Trim().ToUpper() == "INFANT" && activityBookingRequest.InfantCount > 0)
                        {
                            totalAmount += Convert.ToDecimal(requestedActivityDetails.PriceDetails.Rows[k]["amount"]) * activityBookingRequest.InfantCount;
                        }
                    }

                    //Subtracting today's date from the journeyDate and finding the difference
                    TimeSpan difference = Convert.ToDateTime(activityBookingRequest.JourneyDate).Subtract(DateTime.Now);
                    double bookingCutoffDays = difference.Days;

                    //Getting the Agent CurrentBalance

                    decimal currentAgentBalance = AgentMaster.UpdateAgentBalance((int)requestedActivityDetails.AgencyId, 0, userId);

                    if (((requestedActivityDetails.StockInHand - requestedActivityDetails.StockUsed) >= (activityBookingRequest.AdultCount + activityBookingRequest.ChildCount + activityBookingRequest.InfantCount)))
                    {
                        //here we are checking the amount sent and we calculated can have a max difference "1", not more than that.
                        if (bookingCutoffDays >= requestedActivityDetails.BookingCutOff && ((activityBookingRequest.TransType == "B2C") || (activityBookingRequest.TransType == "B2B" && currentAgentBalance >= Convert.ToDecimal(totalAmount) && (activityBookingRequest.TotalAmount - totalAmount >= 0 && activityBookingRequest.TotalAmount - totalAmount <= 1))))
                        //if (activityBookingRequest.TransType == "B2C")
                        {
                            //Transaction Header Details
                            DataRow drTransactionHeader = requestedActivityDetails.TransactionHeader.NewRow();
                            drTransactionHeader["ActivityId"] = activityBookingRequest.ResultIndex;
                            drTransactionHeader["activityName"] = requestedActivityDetails.Name;
                            drTransactionHeader["TripId"] = null;
                            drTransactionHeader["TransactionDate"] = DateTime.Now;
                            drTransactionHeader["Adult"] = activityBookingRequest.AdultCount;
                            drTransactionHeader["Child"] = activityBookingRequest.ChildCount;
                            drTransactionHeader["Infant"] = activityBookingRequest.InfantCount;
                            drTransactionHeader["Booking"] = DateTime.ParseExact(activityBookingRequest.JourneyDate, "yyyy-MM-dd", null);
                            drTransactionHeader["TotalPrice"] = activityBookingRequest.TotalAmount;
                            drTransactionHeader["CreatedBy"] = userId;
                            drTransactionHeader["CreatedDate"] = DateTime.Now;
                            drTransactionHeader["Status"] = "A";
                            drTransactionHeader["AgencyId"] = requestedActivityDetails.AgencyId;
                            drTransactionHeader["TransactionType"] = activityBookingRequest.TransType;
                            drTransactionHeader["LocationId"] = loginInfo.LocationID;
                            drTransactionHeader["ATHId"] = 0;
                            drTransactionHeader["isFixedDeparture"] = "N";
                            drTransactionHeader["RoomCount"] = DBNull.Value;
                            drTransactionHeader["QuotedStatus"] = DBNull.Value;
                            drTransactionHeader["PaymentStatus"] = 0;
                            drTransactionHeader["DeparturePoint"] = activityBookingRequest.DeparturePoint;
                            drTransactionHeader["DepartureTime"] = activityBookingRequest.DepartureTime;

                            requestedActivityDetails.TransactionHeader.Rows.Add(drTransactionHeader);

                            //TransactionDetails
                            DataRow drTransactionDetail = requestedActivityDetails.TransactionDetail.NewRow();
                            drTransactionDetail["FirstName"] = activityBookingRequest.PassengerFirstName;
                            drTransactionDetail["LastName"] = activityBookingRequest.PassengerLastName;
                            drTransactionDetail["PhoneCountryCode"] = "";
                            drTransactionDetail["Phone"] = activityBookingRequest.PhoneNumber;
                            drTransactionDetail["Email"] = activityBookingRequest.EmailAddress;
                            drTransactionDetail["Nationality"] = activityBookingRequest.Nationality;
                            drTransactionDetail["PaxSerial"] = 1;
                            drTransactionDetail["CreatedBy"] = userId;
                            drTransactionDetail["CreatedDate"] = DateTime.Now;

                            requestedActivityDetails.TransactionDetail.Rows.Add(drTransactionDetail);

                            //Transaction Price Details
                            for (int j = 0; j < requestedActivityDetails.PriceDetails.Rows.Count; j++)
                            {
                                if ((requestedActivityDetails.PriceDetails.Rows[j]["label"].ToString().Trim().ToUpper() == "ADULT" && activityBookingRequest.AdultCount > 0) || (requestedActivityDetails.PriceDetails.Rows[j]["label"].ToString().Trim().ToUpper() == "CHILD" && activityBookingRequest.ChildCount > 0) || (requestedActivityDetails.PriceDetails.Rows[j]["label"].ToString().Trim().ToUpper() == "INFANT" && activityBookingRequest.InfantCount > 0))
                                {
                                    DataRow drTransactionPrice = requestedActivityDetails.TransactionPrice.NewRow();
                                    drTransactionPrice["CreatedBy"] = userId;
                                    drTransactionPrice["CreatedDate"] = DateTime.Now;
                                    drTransactionPrice["AgentCurrency"] = loginInfo.Currency;
                                    drTransactionPrice["AgentROE"] = loginInfo.ExchangeRate;
                                    drTransactionPrice["Label"] = requestedActivityDetails.PriceDetails.Rows[j]["label"];

                                    switch (requestedActivityDetails.PriceDetails.Rows[j]["label"].ToString().Trim().ToUpper())
                                    {
                                        case "ADULT":
                                            drTransactionPrice["LabelQty"] = activityBookingRequest.AdultCount;
                                            break;
                                        case "CHILD":
                                            drTransactionPrice["LabelQty"] = activityBookingRequest.ChildCount;
                                            break;
                                        case "INFANT":
                                            drTransactionPrice["LabelQty"] = activityBookingRequest.InfantCount;
                                            break;
                                    }
                                    decimal paxTypeTotalAmount = Convert.ToDecimal(requestedActivityDetails.PriceDetails.Rows[j]["amount"]);
                                    if (requestedActivityDetails.PriceDetails.Rows[j]["label"].ToString().Trim().ToUpper() == "ADULT")
                                    {
                                        drTransactionPrice["Amount"] = paxTypeTotalAmount * activityBookingRequest.AdultCount;
                                    }
                                    else if (requestedActivityDetails.PriceDetails.Rows[j]["label"].ToString().Trim().ToUpper() == "CHILD")
                                    {
                                        drTransactionPrice["Amount"] = paxTypeTotalAmount * activityBookingRequest.ChildCount;
                                    }
                                    else if (requestedActivityDetails.PriceDetails.Rows[j]["label"].ToString().Trim().ToUpper() == "INFANT")
                                    {
                                        drTransactionPrice["Amount"] = paxTypeTotalAmount * activityBookingRequest.InfantCount;
                                    }
                                    drTransactionPrice["Labelamount"] = requestedActivityDetails.PriceDetails.Rows[j]["amount"];
                                    drTransactionPrice["MarkupValue"] = 0;
                                    drTransactionPrice["MarkupType"] = DBNull.Value;
                                    drTransactionPrice["SourceCurrency"] = loginInfo.Currency;
                                    drTransactionPrice["SourceAmount"] = requestedActivityDetails.PriceDetails.Rows[j]["amount"];
                                    drTransactionPrice["Markup"] = 0;
                                    requestedActivityDetails.TransactionPrice.Rows.Add(drTransactionPrice);
                                }
                            }

                            requestedActivityDetails.SaveActivityTransaction();


                            int ATHId = Convert.ToInt32(requestedActivityDetails.TransactionHeader.Rows[0]["ATHId"]);

                            if (ATHId > 0)
                            {
                                try
                                {
                                   

                                    if (activityBookingRequest.TransType == "B2B")
                                    {
                                        AgentMaster.UpdateAgentBalance((int)requestedActivityDetails.AgencyId, -activityBookingRequest.TotalAmount, (int)loginInfo.UserID);
                                    }
                                    Invoice invoice = new Invoice();
                                    int invoiceNumber = Invoice.isInvoiceGenerated(ATHId, ProductType.Activity);

                                    if (invoiceNumber > 0)
                                    {
                                        invoice.Load(invoiceNumber);
                                    }
                                    else
                                    {
                                        invoiceNumber = AccountUtility.RaiseInvoice(ATHId, "", (int)loginInfo.UserID, ProductType.Activity, 1);
                                        if (invoiceNumber > 0)
                                        {
                                            invoice.Load(invoiceNumber);
                                            invoice.Status = InvoiceStatus.Paid;
                                            invoice.CreatedBy = (int)loginInfo.UserID;
                                            invoice.LastModifiedBy = (int)loginInfo.UserID;
                                            invoice.UpdateInvoice();

                                            Error error = new Error();
                                            error.ErrorCode = "000";
                                            error.ErrorMessage = "Activity Booked Successfully";
                                            bookingResponse.Error = error;
                                            bookingResponse.DateBooked = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                                            bookingResponse.Duration = requestedActivityDetails.DurationHours;
                                            bookingResponse.JourneyDate = activityBookingRequest.JourneyDate;
                                            bookingResponse.ResultIndex = activityBookingRequest.ResultIndex;
                                            bookingResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                                            bookingResponse.TokenId = activityBookingRequest.TokenID;
                                            bookingResponse.TourName = requestedActivityDetails.Name;
                                            bookingResponse.BookingId = ATHId;
                                            bookingResponse.TripId = ActivitySearchResults.GetTripID(ATHId, userId, activityBookingRequest.TokenID, activityBookingRequest.UserIPAddress);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Error error = new Error();
                                    error.ErrorCode = "013";
                                    error.ErrorMessage = "Activity Booked Successfully but invoice genaratation failed ";
                                    bookingResponse.Error = error;
                                    //Added on 21-06-2017  
                                    //after confirming the Vinay.
                                    bookingResponse.DateBooked = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                                    bookingResponse.Duration = requestedActivityDetails.DurationHours;
                                    bookingResponse.JourneyDate = activityBookingRequest.JourneyDate;
                                    bookingResponse.ResultIndex = activityBookingRequest.ResultIndex;
                                    bookingResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                                    bookingResponse.TokenId = activityBookingRequest.TokenID;
                                    bookingResponse.TourName = requestedActivityDetails.Name;
                                    bookingResponse.BookingId = ATHId;
                                    bookingResponse.TripId = ActivitySearchResults.GetTripID(ATHId, userId, activityBookingRequest.TokenID, activityBookingRequest.UserIPAddress);
                                    //bookingResponse.DateBooked = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                                    //bookingResponse.Duration = "0Hrs 0Min";
                                    //bookingResponse.JourneyDate = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                                    //bookingResponse.ResultIndex = 0;
                                    //bookingResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                                    //bookingResponse.TokenId = activityBookingRequest.TokenID;
                                    //bookingResponse.TourName = string.Empty;
                                    //bookingResponse.TripId = string.Empty;
                                    Audit.Add(EventType.Book, Severity.High, userId, ActivityServices + "Failed to generate Activity Booking:" + ex.ToString(), activityBookingRequest.UserIPAddress);
                                }
                                try
                                {
                                    Hashtable tempTable = new Hashtable();
                                    string subject = ConfigurationManager.AppSettings["ACT_Confirm_Subject"];
                                    string message = ConfigurationManager.AppSettings["ACT_Confirm_Message"];
                                    string ccEmail = ConfigurationManager.AppSettings["ACT_Confirm_EMail"];
                                    List<string> toList = new List<string>();
                                    if (!string.IsNullOrEmpty(requestedActivityDetails.AgentEmail))
                                    {
                                        toList.AddRange(requestedActivityDetails.AgentEmail.Split(','));
                                    }
                                    string messageText = string.Empty;
                                    string filePath = ConfigurationManager.AppSettings["ActivityHtmlPath"];
                                    StreamReader sr = new StreamReader(filePath);
                                    messageText = sr.ReadToEnd();

                                    tempTable.Add("ActivityMessage", message);
                                    tempTable.Add("ConfirmationNo", "<b>" + bookingResponse.TripId + "</b>");
                                    tempTable.Add("ActivityName", "<b>" + requestedActivityDetails.Name + "</b>");
                                    tempTable.Add("LeadPaxName", activityBookingRequest.PassengerFirstName + " " + activityBookingRequest.PassengerLastName);
                                    tempTable.Add("DateOfTour", activityBookingRequest.JourneyDate);
                                    if (!string.IsNullOrEmpty(activityBookingRequest.DeparturePoint))
                                    {
                                        tempTable.Add("HotelPickup", activityBookingRequest.DeparturePoint);
                                    }
                                    else
                                    {
                                        tempTable.Add("HotelPickup", "N/A");
                                    }
                                    tempTable.Add("BookingDate", DateTime.Now);
                                    tempTable.Add("NoOfPax", (activityBookingRequest.AdultCount + activityBookingRequest.ChildCount));
                                    if (activityBookingRequest.FlightInfo != null)
                                    {
                                        tempTable.Add("HotelName", (activityBookingRequest.FlightInfo.HotelName));
                                        tempTable.Add("PNR", (activityBookingRequest.FlightInfo.PNR));
                                        tempTable.Add("FlightNo", (activityBookingRequest.FlightInfo.FligtNo));
                                    }
                                    else {
                                        tempTable.Add("HotelName", "N/A");
                                        tempTable.Add("PNR", "N/A");
                                        tempTable.Add("FlightNo", "N/A");
                                    }
                                    if (activityBookingRequest.FlightInfo!=null && !string.IsNullOrEmpty(activityBookingRequest.FlightInfo.Origin))
                                    {
                                        tempTable.Add("Origin", (activityBookingRequest.FlightInfo.Origin));
                                        tempTable.Add("DepartureTime", (activityBookingRequest.FlightInfo.DepartureTime));
                                        tempTable.Add("DeptFlightNo", (activityBookingRequest.FlightInfo.DeptFligtNo));
                                    }
                                    else
                                    {
                                        tempTable.Add("Origin", "N/A");
                                        tempTable.Add("DepartureTime", "N/A");
                                        tempTable.Add("DeptFlightNo", "N/A");
                                    }
                                    if (activityBookingRequest.FlightInfo != null)
                                    {
                                        tempTable.Add("Destination", (activityBookingRequest.FlightInfo.Destination));
                                        tempTable.Add("ArrivalTime", (activityBookingRequest.FlightInfo.Arrivaltime));
                                        tempTable.Add("ChildCount", (activityBookingRequest.FlightInfo.ChildCount));
                                    }
                                    else {
                                        tempTable.Add("Destination", "N/A");
                                        tempTable.Add("ArrivalTime", "N/A");
                                        tempTable.Add("ChildCount", "N/A");
                                    }
                                    if (activityBookingRequest.FlightInfo != null && !string.IsNullOrEmpty(activityBookingRequest.FlightInfo.ChildAges))
                                    {
                                        tempTable.Add("ChildAges", (activityBookingRequest.FlightInfo.ChildAges));
                                    }
                                    else
                                    {
                                        tempTable.Add("ChildAges", "N/A");
                                    }
                                   // Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toList, subject, messageText, tempTable, ccEmail);
                                }
                                catch (Exception ex) { Audit.Add(EventType.Email, Severity.Normal, 0, "Sending Failed To Supplier Email. Error : " + ex.Message, ""); }
                            }
                            else
                            {
                                Error error = new Error();
                                error.ErrorCode = "007";
                                error.ErrorMessage = "Booking Save Failed, due to technical error";
                                bookingResponse.Error = error;
                                bookingResponse.DateBooked = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                                bookingResponse.Duration = "0Hrs 0Min";
                                bookingResponse.JourneyDate = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                                bookingResponse.ResultIndex = 0;
                                bookingResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                                bookingResponse.TokenId = activityBookingRequest.TokenID;
                                bookingResponse.TourName = string.Empty;
                                bookingResponse.TripId = string.Empty;
                            }
                        }
                        else
                        {
                         
                            Error error = new Error();
                            error.ErrorCode = "008";
                            error.ErrorMessage = "Booking Failed, due to technical error"; 
                            bookingResponse.Error = error;
                            bookingResponse.DateBooked = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                            bookingResponse.Duration = "0Hrs 0Min";
                            bookingResponse.JourneyDate = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                            bookingResponse.ResultIndex = 0;
                            bookingResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                            bookingResponse.TokenId = activityBookingRequest.TokenID;
                            bookingResponse.TourName = string.Empty;
                            bookingResponse.TripId = string.Empty;

                        }
                    }
                    else {
                        Error error = new Error();
                        error.ErrorCode = "014";
                        error.ErrorMessage = "Booking Failed, due to passenger count exceeded available stock";
                        bookingResponse.Error = error;
                        bookingResponse.DateBooked = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                        bookingResponse.Duration = "0Hrs 0Min";
                        bookingResponse.JourneyDate = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                        bookingResponse.ResultIndex = 0;
                        bookingResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                        bookingResponse.TokenId = activityBookingRequest.TokenID;
                        bookingResponse.TourName = string.Empty;
                        bookingResponse.TripId = string.Empty;
                    }
                }
                else
                {
                    Error error = new Error();
                    error.ErrorCode = "004";
                    error.ErrorMessage = "Login session expired";
                    bookingResponse.Error = error;
                    bookingResponse.DateBooked = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                    bookingResponse.Duration = "0Hrs 0Min";
                    bookingResponse.JourneyDate = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                    bookingResponse.ResultIndex = 0;
                    bookingResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                    bookingResponse.TokenId = activityBookingRequest.TokenID;
                    bookingResponse.TourName = string.Empty;
                    bookingResponse.TripId = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Error error = new Error();
                error.ErrorCode = "009";
                error.ErrorMessage = ex.Message;
                bookingResponse.Error = error;
                bookingResponse.DateBooked = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                bookingResponse.Duration = "0Hrs 0Min";
                bookingResponse.JourneyDate = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                bookingResponse.ResultIndex = 0;
                bookingResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                bookingResponse.TokenId = activityBookingRequest.TokenID;
                bookingResponse.TourName = string.Empty;
                bookingResponse.TripId = string.Empty;

                Audit.Add(EventType.Book, Severity.High, userId, ActivityServices + "Failed to generate Activity Booking:" + ex.ToString(), activityBookingRequest.UserIPAddress);
            }
            try
            {
                Random random = new Random();
                string rndNumber = random.Next(100000, 999999).ToString();

                XmlSerializer serializer = new XmlSerializer(bookingResponse.GetType());
                StringBuilder sb = new StringBuilder();
                StringWriter strwriter = new StringWriter(sb);
                serializer.Serialize(strwriter, bookingResponse);

                string filepath = XmlPath + "ActivitybookingResponse_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + "_" + activityBookingRequest.TokenID + "_"+rndNumber+".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(filepath);

                Audit.Add(EventType.SightseeingBooking, Severity.Normal, userId, ActivityServices + filepath, activityBookingRequest.UserIPAddress);

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, userId, ActivityServices + "Failed to save Activity Booking Response:" + ex.ToString(), activityBookingRequest.UserIPAddress);
            }
            return bookingResponse;
        }

        public ActivityCancellationResponse ActivityCancellationDetails(ActivityCancellationRequest request)
        {
            ActivityCancellationResponse response = new ActivityCancellationResponse();
            try
            {
                //Validating the CancellationRequest Details.
                if (!string.IsNullOrEmpty(request.TokenId) && request.ResultIndex > 0)
                {
                    try
                    {
                        Random random = new Random();
                        string rndNumber = random.Next(100000, 999999).ToString();

                        XmlSerializer serializer = new XmlSerializer(request.GetType());
                        StringBuilder sb = new StringBuilder();
                        StringWriter strwriter = new StringWriter(sb);
                        serializer.Serialize(strwriter, request);

                        string filepath = XmlPath + "ActivityCancellationRequest_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + "_"+rndNumber+".xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());
                        doc.Save(filepath);

                        Audit.Add(EventType.SightseeingCancel, Severity.Normal, 0, ActivityServices + filepath, request.UserIPAddress);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SightseeingCancel, Severity.Normal, 0, ActivityServices + "Failed to save the ActivityCancellationRequest:" + ex.Message, (!string.IsNullOrEmpty(request.UserIPAddress) ? request.UserIPAddress : string.Empty));
                    }

                    LoginInfo loginInfo = AuthenticationResponse.ValidateUser(request.TokenId);
                    if (loginInfo != null && loginInfo.UserID > 0)
                    {
                        userId = Convert.ToInt32(loginInfo.UserID);

                        CancellationDetails cancellationDetails = CancellationDetails.getCancellationDetails(request);
                        if (cancellationDetails != null && cancellationDetails.ResultIndex > 0)
                        {
                            Error error = new Error();
                            error.ErrorCode = "000";
                            error.ErrorMessage = "Successful";
                            response.Error = error;
                            response.CancellationDetails = cancellationDetails;
                            response.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                        }
                        else
                        {
                            Error error = new Error();
                            error.ErrorCode = "010";
                            error.ErrorMessage = "failed";
                            response.Error = error;
                            response.CancellationDetails = null;
                            response.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                        }
                    }
                    else
                    {
                        Error error = new Error();
                        error.ErrorCode = "004";
                        error.ErrorMessage = "Login Failed, session expired";
                        response.Error = error;
                        response.CancellationDetails = null;
                        response.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                    }
                }
                else
                {
                    Error error = new Error();
                    error.ErrorCode = "011";
                    error.ErrorMessage = "Invalid details";
                    response.Error = error;
                    response.CancellationDetails = null;
                    response.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                }
            }
            catch (Exception ex)
            {
                Error error = new Error();
                error.ErrorCode = "012";
                error.ErrorMessage = "failed due to technical error";
                response.Error = error;
                response.CancellationDetails = null;
                response.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");

                Audit.Add(EventType.SightseeingCancel, Severity.Normal, 0, ActivityServices + "Failed to get the activityCancellationDetails:" + ex.ToString(), (!string.IsNullOrEmpty(request.UserIPAddress) ? request.UserIPAddress : string.Empty));
            }
            try
            {
                Random random = new Random();
                string rndNumber = random.Next(100000, 999999).ToString();

                XmlSerializer serializer = new XmlSerializer(response.GetType());
                StringBuilder sb = new StringBuilder();
                StringWriter strwriter = new StringWriter(sb);
                serializer.Serialize(strwriter, response);

                string filepath = XmlPath + "ActivityCancellationResponse_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + "_"+rndNumber+".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(filepath);

                Audit.Add(EventType.SightseeingCancel, Severity.Normal, 0, ActivityServices + filepath, request.UserIPAddress);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.Normal, 0, ActivityServices + "Failed to save the ActivityCancellationResponse:" + ex.Message, (!string.IsNullOrEmpty(request.UserIPAddress) ? request.UserIPAddress : string.Empty));
            }
            return response;
        }

        #region validating Methods

        /// <summary>
        /// Validating the ActivitySearchRequest Parameters
        /// </summary>
        /// <param name="activitySearchRequest"></param>
        /// <returns></returns>
        protected bool ValidateSearchRequests(ActivitySearchRequest activitySearchRequest)
        {
            bool validDetails = true;
            if (activitySearchRequest != null)
            {
                if (string.IsNullOrEmpty(activitySearchRequest.CityCode))
                {
                    throw new Exception("City Code should not be empty");
                }
                else if (string.IsNullOrEmpty(activitySearchRequest.CountryCode))
                {
                    throw new Exception("Country Code should not be empty");
                }
                else if (string.IsNullOrEmpty(activitySearchRequest.FromDate) || string.IsNullOrEmpty(activitySearchRequest.ToDate))
                {
                    string date = string.IsNullOrEmpty(activitySearchRequest.FromDate) ? "From Date value" : "ToDate value";
                    throw new Exception(date + " should not be empty");
                }
                else if (activitySearchRequest.AdultCount <= 0)
                {
                    throw new Exception("Should be atleast one Adult passenger");
                }
                else if (activitySearchRequest.AdultCount < activitySearchRequest.InfantCount)
                {
                    throw new Exception("Adult count should not be less than the Infant count");
                }
                else if (string.IsNullOrEmpty(activitySearchRequest.TokenID))
                {
                    throw new Exception("TokenId should not be empty");
                }
                else if (activitySearchRequest.TokenID.Length < 36)
                {
                    throw new Exception("Please send valid TokenId");
                }
                else if (string.IsNullOrEmpty(activitySearchRequest.UserIPAddress))
                {
                    throw new Exception("IPAddress should not be empty");
                }
                if (!string.IsNullOrEmpty(activitySearchRequest.FromDate) || !string.IsNullOrEmpty(activitySearchRequest.ToDate))
                {
                    DateTime date = DateTime.ParseExact(activitySearchRequest.FromDate, "yyyy-MM-dd", null);
                    if (date < DateTime.Now)
                    {
                        throw new Exception("From Date should be greater than present date");
                    }
                    date = DateTime.ParseExact(activitySearchRequest.ToDate, "yyyy-MM-dd", null);
                    if (date < DateTime.Now)
                    {
                        throw new Exception("ToDate should be greater than present date");
                    }
                }
            }
            else
            {
                throw new Exception("Please send valid details");
            }
            return validDetails;
        }

        /// <summary>
        /// Validating the Activity Booking Request fields
        /// </summary>
        /// <param name="bookingRequest"></param>
        protected void ValidateBookingRequest(ActivityBookingRequest bookingRequest)
        {
            string nameRegex = "[a-zA-Z. ]+$";
            string ValidEmail = "[_a-zA-Z0-9-]+([_a-zA-Z0-9.-]+)*@[a-zA-Z0-9-]+([a-zA-Z0-9.-]+)*([a-zA-Z.][a-zA-Z]+)$";
            string validPhoneNumber = "[0-9]+$";

            if (bookingRequest != null)
            {
                if (string.IsNullOrEmpty(bookingRequest.TokenID))
                {
                    throw new Exception("TokenId Should not be empty");
                }
                else if (string.IsNullOrEmpty(bookingRequest.EmailAddress))
                {
                    throw new Exception("Email Id should not be empty");
                }
                else if (!Regex.IsMatch(bookingRequest.EmailAddress, ValidEmail))
                {
                    throw new Exception("Please send valid Email");
                }
                else if (string.IsNullOrEmpty(bookingRequest.JourneyDate))
                {
                    throw new Exception("JourneyDate should not be empty");
                }
                else if (string.IsNullOrEmpty(bookingRequest.Nationality))
                {
                    throw new Exception("Nationality should not be empty");
                }
                else if (string.IsNullOrEmpty(bookingRequest.PassengerFirstName))
                {
                    throw new Exception("Passenger FirstName should not be empty");
                }
                else if (!Regex.IsMatch(bookingRequest.PassengerFirstName, nameRegex))
                {
                    throw new Exception("FirstName should not contain Special Characters");
                }
                else if (bookingRequest.PassengerFirstName.Length <= 1)
                {
                    throw new Exception("Passenger FirstName should be minimum 2 characters");
                }
                else if (string.IsNullOrEmpty(bookingRequest.PassengerLastName))
                {
                    throw new Exception("Passenger LastName should not be empty");
                }
                else if (bookingRequest.PassengerLastName.Length <= 1)
                {
                    throw new Exception("Passenger LastName should be minimum 2 characters");
                }
                else if (!Regex.IsMatch(bookingRequest.PassengerLastName, nameRegex))
                {
                    throw new Exception("LastName should not contain Special Characters");
                }
                else if (string.IsNullOrEmpty(bookingRequest.UserIPAddress))
                {
                    throw new Exception("UserIPAddress should not be empty");
                }
                else if (string.IsNullOrEmpty(bookingRequest.PhoneNumber))
                {
                    throw new Exception("Phonenumber should not be empty");
                }
                else if (!Regex.IsMatch(bookingRequest.PhoneNumber, validPhoneNumber))
                {
                    throw new Exception("Phone number should contain only numeric values");
                }
                else if (bookingRequest.PhoneNumber.Length < 7)
                {
                    throw new Exception("Minimum length of the Phone number is 7 characters");
                }
                //else if (string.IsNullOrEmpty(bookingRequest.PhoneCountryCode))
                //{
                //    throw new Exception("Phone CountryCode should not be empty");
                //}
                else if (bookingRequest.AdultCount <= 0)
                {
                    throw new Exception("Adult count should not be less than zero");
                }
                else if (bookingRequest.AdultCount < bookingRequest.InfantCount)
                {
                    throw new Exception("Adult count should not be less than the Infant count");
                }
                else if (bookingRequest.ResultIndex <= 0)
                {
                    throw new Exception("Please send the valid ResultIndex");
                }

                if (!string.IsNullOrEmpty(bookingRequest.JourneyDate))
                {
                    DateTime journeyDate = DateTime.ParseExact(bookingRequest.JourneyDate, "yyyy-MM-dd", null);
                    if (journeyDate < DateTime.Now)
                    {
                        throw new Exception("Journey date should be greater than present date");
                    }
                }
            }
        }

        #endregion
    }
}
