﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivityPriceDetails
    {
        string passengerType;
        double amount;

        [DataMember]
        public string PassengerType
        {
            get { return passengerType; }
            set { passengerType = value; }
        }
        [DataMember]
        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }
    }
}