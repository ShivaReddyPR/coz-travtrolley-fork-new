﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivitySearchRequest
    {

        string tokenId;
        string userIPAddress;
        string cityCode;
        string countryCode;
        string fromDate;
        string toDate;
        int adultCount;
        int childCount;
        int infantCount;
        string activityName;
        string[] category;
        string[] type;
        int agencyid;

        [DataMember]
        public string TokenID
        {
            get { return tokenId; }
            set { tokenId = value; }
        }
        [DataMember]
        public string UserIPAddress
        {
            get { return userIPAddress; }
            set { userIPAddress = value; }
        }
        [DataMember]
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        [DataMember]
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
        [DataMember]
        public string FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        [DataMember]
        public string ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        [DataMember]
        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }
        [DataMember]
        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }
        [DataMember]
        public int InfantCount
        {
            get { return infantCount; }
            set { infantCount = value; }
        }
        [DataMember]
        public string[] Category
        {
            get { return category; }
            set { category = value; }
        }
        [DataMember]
        public string[] Type
        {
            get { return type; }
            set { type = value; }
        }
        [DataMember]
        public string ActivityName
        {
            get { return activityName; }
            set { activityName = value; }
        }
        [DataMember]
        public int AgencyId
        {
            get { return agencyid; }
            set { agencyid = value; }
        }

    }
}
