﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivityBookingResponse
    {
        Error error;
        string tripId;
        int resultIndex;
        string tourName;
        string journeyDate;
        string duration;
        string datedBooked;
        string responseTime;
        string tokenId;
        int bookingId;

        [DataMember]
        public Error Error
        {
            get { return error; }
            set { error = value; }
        }
        [DataMember]
        public string TripId
        {
            get { return tripId; }
            set { tripId = value; }
        }
        [DataMember]
        public int ResultIndex
        {
            get { return resultIndex; }
            set { resultIndex = value; }
        }
        [DataMember]
        public string TourName
        {
            get { return tourName; }
            set { tourName = value; }
        }
        [DataMember]
        public string JourneyDate
        {
            get { return journeyDate; }
            set { journeyDate = value; }
        }
        [DataMember]
        public string Duration
        {
            get { return duration; }
            set { duration = value; }
        }
        [DataMember]
        public string DateBooked
        {
            get { return datedBooked; }
            set { datedBooked = value; }
        }
        [DataMember]
        public string ResponseTime
        {
            get { return responseTime; }
            set { responseTime = value; }
        }
        [DataMember]
        public string TokenId
        {
            get { return tokenId; }
            set { tokenId = value; }
        }
        [DataMember]
        public int BookingId
        {
            get { return bookingId; }
            set { bookingId = value; }
        }
    }
}
