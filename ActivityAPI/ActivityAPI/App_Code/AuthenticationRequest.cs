﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class AuthenticationRequest
    {
        string userName;
        string password;
        string userIPAddress;

        [DataMember]
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        [DataMember]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        [DataMember]
        public string UserIPAddress
        {
            get { return userIPAddress; }
            set { userIPAddress = value; }
        }
    }
}