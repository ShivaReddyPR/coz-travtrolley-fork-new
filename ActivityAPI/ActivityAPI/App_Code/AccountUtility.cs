﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Transactions;


namespace ActivityAPI
{
    public class AccountUtility
    {
        /// This Overload method is used to Raise Invoice with extra parameter Rate of Exchange.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="prodType"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoice(int productId, string remarks, int memberId, ProductType prodType, decimal rateOfExchange)
        {
            int raiseInvoice = 0;
            if (prodType == ProductType.Activity)
            {
                ActivityDetails activity = new ActivityDetails();
                activity.GetActivityForQueue(productId);
                raiseInvoice = RaiseInvoice(activity, remarks, memberId, rateOfExchange, ProductType.Activity);
            }
            return raiseInvoice;
        }

        /// <summary>
        /// Raise invoice for Activity.
        /// </summary>
        /// <param name="Activity"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoice(ActivityDetails activities, string remarks, int memberId, decimal rateOfExchange, ProductType prodType)
        {
            Audit.Add(EventType.Book, Severity.Normal, 1, "Activity Raise Invoice entered. ActivityId : " + activities.Id, "");
            return RaiseInvoicePart2(activities, remarks, memberId, rateOfExchange, prodType);
        }

        /// <summary>
        /// Raise invoice for Activity
        /// </summary>
        /// <param name="Activities"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoicePart2(ActivityDetails Activities, string remarks, int memberId, decimal rateOfExchange, ProductType prodType)
        {
            Invoice invoice = new Invoice();
            int invoiceCount = 0;
            if (prodType == ProductType.Activity)
            {
                invoiceCount = invoice.InvoiceRaised((int)Activities.Id, ProductType.Activity);
            }
            else
            {
                invoiceCount = invoice.InvoiceRaised((int)Activities.Id, ProductType.FixedDeparture);
            }
            if (invoiceCount == 0)
            {
                decimal totalAmount = 0;
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = remarks;
                invoice.AgencyId = Convert.ToInt32(Activities.TransactionHeader.Rows[0]["AgencyId"]);
                invoice.Status = InvoiceStatus.Paid;
                invoice.CreatedBy = memberId;
                invoice.CreatedOn = DateTime.UtcNow;
                invoice.TranxHeaderId = Activities.Id;
                invoice.TotalPrice = Convert.ToDecimal(Activities.TransactionHeader.Rows[0]["TotalPrice"]);
                // Generating Invoice line items.
                invoice.LineItem = new List<InvoiceLineItem>();
                DataTable passengers = Activities.TransactionDetail;
                for (int i = 0; i < passengers.Rows.Count; i++)
                {
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    //line.ItemReferenceNumber = Convert.ToInt32(passengers.Rows[i]["ATDId"]);
                    line.ItemReferenceNumber = Convert.ToInt32(passengers.Rows[i]["ATHDId"]);
                    if (prodType == ProductType.Activity)
                    {
                        line.ItemTypeId = (int)InvoiceItemTypeId.ActivityBooking;
                    }
                    else if (prodType == ProductType.FixedDeparture)
                    {
                        line.ItemTypeId = (int)InvoiceItemTypeId.FixedDepartureBooking;
                    }
                    //line.Price = new PriceAccounts();
                    line.PriceId = Convert.ToInt32(passengers.Rows[i]["ATDId"]);
                    invoice.LineItem.Add(line);
                }

                List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();
                for (int i = 0; i < passengers.Rows.Count; i++)
                {
                    NarrationBuilder objNarration = new NarrationBuilder();
                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                    ledgerTxn.LedgerId = (int)Activities.TransactionHeader.Rows[0]["AgencyId"];
                    decimal ledgerAmount = Convert.ToDecimal(Activities.TransactionHeader.Rows[0]["TotalPrice"]);
                    ledgerTxn.Amount = -ledgerAmount;
                    totalAmount += ledgerAmount;
                    objNarration.PaxName = string.Format("{0} {1}", passengers.Rows[i]["FirstName"].ToString(), passengers.Rows[i]["LastName"].ToString());
                    objNarration.PolicyNo = Activities.TransactionHeader.Rows[0]["TripId"].ToString();
                    objNarration.TravelDate = Convert.ToDateTime(Activities.TransactionHeader.Rows[0]["Booking"]).ToString("MM/dd/yyyy");
                    if (prodType == ProductType.Activity)
                    {
                        objNarration.Remarks = "Activity Booking";
                        ledgerTxn.ReferenceType = ReferenceType.ActivityBooked;
                    }
                    else if (prodType == ProductType.FixedDeparture)
                    {
                        objNarration.Remarks = "FixedDeparture Booking";
                        ledgerTxn.ReferenceType = ReferenceType.FixedDepartureBooked;
                    }
                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.ReferenceId = Convert.ToInt32(passengers.Rows[i]["ATDId"]);
                    ledgerTxn.Notes = "";
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = memberId;
                    ledgerTransactionList.Add(ledgerTxn);
                }
                invoice.Status = InvoiceStatus.Paid;
                using (TransactionScope updateTransaction = new TransactionScope())
                {
                    if (prodType == ProductType.Activity)
                    {
                        invoice.Save(true, 0, ProductType.Activity);
                    }
                    else if (prodType == ProductType.FixedDeparture)
                    {
                        invoice.Save(true, 0, ProductType.FixedDeparture);
                    }
                    foreach (LedgerTransaction ledgerTransaction in ledgerTransactionList)
                    {
                        ledgerTransaction.Narration.DocNo = Convert.ToString(invoice.DocTypeCode + Convert.ToString(invoice.DocumentNumber));
                        ledgerTransaction.CreatedOn = invoice.CreatedOn;
                        if (prodType == ProductType.Activity)
                        {
                            ledgerTransaction.Save();
                            LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTransaction.TxnId);
                        }
                    }

                    updateTransaction.Complete();
                }
            }
            else if (invoiceCount == Activities.TransactionDetail.Rows.Count)
            {
                if (prodType == ProductType.Activity)
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(Convert.ToInt32(Activities.TransactionDetail.Rows[0]["ATDId"]), ProductType.Activity);
                }
                else if (prodType == ProductType.FixedDeparture)
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(Convert.ToInt32(Activities.TransactionDetail.Rows[0]["ATDId"]), ProductType.FixedDeparture);
                }
            }
            else
            {
                throw new Exception("Invoice Error");
            }
            return invoice.InvoiceNumber;
        }
    }
}