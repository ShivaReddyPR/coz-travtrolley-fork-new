﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    public class LedgerTransaction
    {
        /// <summary>
        /// Unique Field txnId
        /// </summary>
        private int txnId;
        /// <summary>
        /// LedgerId field
        /// </summary>
        private int ledgerId;
        /// <summary>
        /// Amount of a transaction
        /// </summary>
        private decimal amount;
        /// <summary>
        /// narration of a transaction
        /// </summary>
        private string narration;
        /// <summary>
        /// Notes of a transaction
        /// </summary>
        private string notes;
        /// <summary>
        /// Date of transaction
        /// </summary>
        private DateTime date;
        /// <summary>
        /// agencyId field
        /// </summary>
        private int agencyId;
        /// <summary>
        /// Unique ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the record was created
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// Unique ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// Date when the record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// Field tells whether this transaction is for LCCs or not
        /// </summary>
        private bool isLCC;
        /// <summary>
        /// referenceId field contains TicketId ot paymentDetailId(as for the reference Type)
        /// </summary>
        private int referenceId;
        /// <summary>
        /// referenceType(like TicketRefund,Ticket Created etc) is the for Transaction
        /// </summary>        
        private ReferenceType referenceType;
        /// <summary>
        /// paxName field contains Passenger First Name for transaction
        /// </summary>
        private string paxFirstName = "-";
        /// <summary>
        /// paxName field contains Passenger Last Name for transaction
        /// </summary>
        private string paxLastName = string.Empty;
        /// <summary>
        /// paxName field contains Sector for transaction
        /// </summary>
        private string sector = "-";
        /// <summary>
        /// paxName field contains TicketNo for transaction
        /// </summary>
        private string ticketNo = "-";
        /// <summary>
        /// paxName field contains Check No for transaction
        /// </summary>
        private string checkNo = "-";
        /// <summary>
        /// UniqueId Property txnId
        /// </summary>
        private decimal runningTotal;

        private string transType = "B2B";

        public int TxnId
        {
            get
            {
                return this.txnId;
            }
            set
            {
                this.txnId = value;
            }
        }
        /// <summary>
        /// Ledger Id property of a transaction
        /// </summary>
        public int LedgerId
        {
            get
            {
                return this.ledgerId;
            }
            set
            {
                this.ledgerId = value;
            }
        }
        /// <summary>
        /// Gets or Set the amount of a transaction
        /// </summary>
        public decimal Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.amount = value;
            }
        }
        /// <summary>
        /// Get or Sets the narration of a transaction
        /// </summary>
        public NarrationBuilder Narration
        {
            get
            {
                NarrationBuilder obj = new NarrationBuilder();
                string[] arr = this.narration.Split(new char[]
				{
					'|'
				});
                obj.Remarks = arr[0];
                if (arr.GetUpperBound(0) > 1)
                {
                    obj.PaxName = arr[1];
                    obj.Sector = arr[2];
                    obj.TicketNo = arr[3];
                    obj.DocNo = arr[4];
                    obj.TravelDate = arr[5];
                    obj.ChequeNo = ((arr[6] != "") ? ("-" + arr[6]) : "");
                    obj.FlightNo = arr[7];
                    obj.HotelConfirmationNo = arr[8];
                    obj.DestinationCity = arr[9];
                    obj.PolicyNo = arr[10];
                    obj.PaymentMode = arr[11];
                }
                return obj;
            }
            set
            {
                this.narration = string.Concat(new string[]
				{
					value.Remarks,
					"|",
					value.PaxName,
					"|",
					value.Sector,
					"|",
					value.TicketNo,
					"|",
					value.DocNo,
					"|",
					value.TravelDate,
					"|",
					value.ChequeNo,
					"|",
					value.FlightNo,
					"|",
					value.HotelConfirmationNo,
					"|",
					value.DestinationCity,
					"|",
					value.PolicyNo,
					"|",
					value.PaymentMode
				});
            }
        }
        public string NarrationString
        {
            get
            {
                return this.narration;
            }
        }
        /// <summary>
        /// Gets or sets the notes of a transaction
        /// </summary>
        public string Notes
        {
            get
            {
                return this.notes;
            }
            set
            {
                this.notes = value;
            }
        }
        /// <summary>
        /// Gets or sets the date of a transaction
        /// </summary>
        public DateTime Date
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date = value;
            }
        }
        /// <summary>
        /// Agency id property of a transaction
        /// </summary>
        public int AgencyId
        {
            get
            {
                return this.agencyId;
            }
            set
            {
                this.agencyId = value;
            }
        }
        /// <summary>
        /// Gets Id of member who created the record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return this.createdBy;
            }
            set
            {
                this.createdBy = value;
            }
        }
        /// <summary>
        /// Gets the date when the record was created on
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return this.createdOn;
            }
            set
            {
                this.createdOn = value;
            }
        }
        /// <summary>
        /// Gets Id of member who modified the record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return this.lastModifiedBy;
            }
            set
            {
                this.lastModifiedBy = value;
            }
        }
        /// <summary>
        /// Gets the date when the record was last Modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return this.lastModifiedOn;
            }
            set
            {
                this.lastModifiedOn = value;
            }
        }
        /// <summary>
        /// Gets or sets isLCC flag
        /// </summary>
        public bool IsLCC
        {
            get
            {
                return this.isLCC;
            }
            set
            {
                this.isLCC = value;
            }
        }
        /// <summary>
        /// Gets and Sets the referenceId field
        /// </summary>
        public int ReferenceId
        {
            get
            {
                return this.referenceId;
            }
            set
            {
                this.referenceId = value;
            }
        }
        /// <summary>
        /// Gets and sets the referenceType field
        /// </summary>
        public ReferenceType ReferenceType
        {
            get
            {
                return this.referenceType;
            }
            set
            {
                this.referenceType = value;
            }
        }
        /// <summary>
        /// Gets and sets the paxFirstName field
        /// </summary>
        public string PaxFirstName
        {
            get
            {
                return this.paxFirstName;
            }
            set
            {
                this.paxFirstName = value;
            }
        }
        /// <summary>
        /// Gets the PaxFullName
        /// </summary>
        public string PaxFullName
        {
            get
            {
                return string.Format("{0} {1}", this.paxFirstName, this.paxLastName);
            }
        }
        /// <summary>
        /// Gets and sets the paxFirstName field
        /// </summary>
        public string PaxLastName
        {
            get
            {
                return this.paxLastName;
            }
            set
            {
                this.paxLastName = value;
            }
        }
        /// <summary>
        /// Gets and sets the sector field
        /// </summary>
        public string Sector
        {
            get
            {
                return this.sector;
            }
            set
            {
                this.sector = value;
            }
        }
        /// <summary>
        /// Gets and sets the ticketNo field
        /// </summary>
        public string TicketNo
        {
            get
            {
                return this.ticketNo;
            }
            set
            {
                this.ticketNo = value;
            }
        }
        /// <summary>
        /// Gets and sets the ticketNo field
        /// </summary>
        public string CheckNo
        {
            get
            {
                return this.checkNo;
            }
            set
            {
                this.checkNo = value;
            }
        }

        public decimal RunningTotal
        {
            get
            {
                return runningTotal;
            }
            set
            {
                runningTotal = value;
            }
        }

        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }


        /// <summary>
        /// Methos saves the Transaction
        /// </summary>
        public void Save()
        {
            if (this.txnId > 0)
            {
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateLedgerTransaction", new SqlParameter[]
				{
					new SqlParameter("@txnId", this.txnId),
					new SqlParameter("@ledgerId", this.ledgerId),
					new SqlParameter("@amount", this.amount),
					new SqlParameter("@narration", this.narration),
					new SqlParameter("@notes", this.notes),
					new SqlParameter("@referenceId", this.referenceId),
					new SqlParameter("@referenceTypeId", (int)this.referenceType),
					new SqlParameter("@isLCC", this.isLCC),
					new SqlParameter("@lastModifiedOn", this.lastModifiedOn),
					new SqlParameter("@lastModifiedBy", this.lastModifiedBy)
				});
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[12];
                paramList[0] = new SqlParameter("@txnId", this.txnId);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[1] = new SqlParameter("@ledgerId", this.ledgerId);
                paramList[2] = new SqlParameter("@amount", this.amount);
                paramList[3] = new SqlParameter("@narration", this.narration);
                paramList[4] = new SqlParameter("@notes", this.notes);
                paramList[5] = new SqlParameter("@date", this.date);
                paramList[6] = new SqlParameter("@referenceId", this.referenceId);
                paramList[7] = new SqlParameter("@referenceTypeId", (int)this.referenceType);
                paramList[8] = new SqlParameter("@isLCC", this.isLCC);
                paramList[9] = new SqlParameter("@createdBy", this.createdBy);
                if (this.CreatedOn != new DateTime(1, 1, 1))
                {
                    paramList[10] = new SqlParameter("@createdOn", this.createdOn);
                }
                else
                {
                    paramList[10] = new SqlParameter("@createdOn", DateTime.UtcNow);
                }
                paramList[11] = new SqlParameter("@transType", this.transType);
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddLedgerTransaction", paramList);
                this.txnId = Convert.ToInt32(paramList[0].Value);
            }
        }


        /// <summary>
        /// Method adds entry in Invoice transactiion 
        /// </summary>
        /// <param name="invoiceNumber">invoice Number</param>
        /// <param name="ledgerTxnId">txnId</param>
        public static void AddInvoiceTxn(int invoiceNumber, int ledgerTxnId)
        {
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number Id should be positive integer invoiceNumber=" + invoiceNumber);
            }
            int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddInvoiceTxn", new SqlParameter[]
			{
				new SqlParameter("@invoiceNumber", invoiceNumber),
				new SqlParameter("@ledgerTxnId", ledgerTxnId)
			});
        }

    }
}