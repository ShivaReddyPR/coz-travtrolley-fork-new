﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivitySearchResponse
    {
        Error error;
        ActivitySearchResults[] searchResults;
        string searchResponseTime;
        string tokenId;
        int resultCount;

        [DataMember]
        public Error Error
        {
            get { return error; }
            set { error = value; }
        }
        [DataMember]
        public ActivitySearchResults[] SearchResults
        {
            get { return searchResults; }
            set { searchResults = value; }
        }
        [DataMember]
        public string SearchResponseTime
        {
            get { return searchResponseTime; }
            set { searchResponseTime = value; }
        }
        public string TokenId
        {
            get { return tokenId; }
            set { tokenId = value; }
        }
        public int ResultsCount
        {
            get { return resultCount; }
            set { resultCount = value; }
        }


    }
}