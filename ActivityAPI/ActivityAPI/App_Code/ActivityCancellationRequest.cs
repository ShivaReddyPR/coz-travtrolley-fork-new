﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivityCancellationRequest
    {
        string tokenId;
        int resultIndex;
        string userIPAddres;

        [DataMember]
        public string TokenId
        {
            get { return tokenId; }
            set { tokenId = value; }
        }
        [DataMember]
        public int ResultIndex
        {
            get { return resultIndex; }
            set { resultIndex = value; }
        }
        [DataMember]
        public string UserIPAddress
        {
            get { return userIPAddres; }
            set { userIPAddres = value; }
        }
    }
}