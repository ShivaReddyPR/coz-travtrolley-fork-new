﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityAPI
{
    public enum ProductType
    {
        Flight = 1,
        Hotel = 2,
        Packages = 3,
        Activity = 4,
        Insurance = 5,
        SightSeeing = 6,
        Train = 7,
        Transfers = 9,
        SMSPack = 8,
        MobileRecharge = 10,
        FixedDeparture = 11,
        Visa = 12
    }
    public enum ReferenceType
    {
        /// <summary>
        /// ReferenceId -- TicketId
        /// </summary>
        TicketCreated = 1,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        PaymentRecieved = 2,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        PaymentReversed = 3,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        TicketRefund = 4,
        /// <summary>
        /// ReferenceId -- 0
        /// </summary>
        ManualEntry = 5,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        TicketCancellationCharge = 6,
        /// <summary>
        /// ReferenceId -- OfflineBookingId
        /// </summary>
        OfflineTicket = 7,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        OfflineRefund = 8,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        OfflineCancellationCharge = 9,
        /// <summary>
        /// ReferenceId -- TicketId
        /// </summary>
        TicketReverted = 10,
        /// <summary>
        /// PLB Revereted for earlier tickets
        /// </summary>
        PLBReverted = 11,
        /// <summary>
        /// PLB for Earlier entries in DB
        /// </summary>
        PLB = 12,
        /// <summary>
        /// Hotel Booking successful
        /// </summary>
        HotelBooked = 13,
        /// <summary>
        /// Hotel Refund
        /// </summary>
        HotelRefund = 14,
        /// <summary>
        /// Hotel Cancellation Charge
        /// </summary>
        HotelCancellationCharge = 15,
        /// <summary>
        /// Insurance Creation Successful 
        /// </summary>
        InsuranceCreated = 16,
        /// <summary>
        /// Insurance Creation Successful 
        /// </summary>
        InvoiceEdited = 17,
        /// <summary>
        /// Transfer Booking Successful
        /// </summary>
        TransferBooked = 18,
        /// <summary>
        /// Sightseeing Booking successful
        /// </summary>
        SightseeingBooked = 19,
        /// <summary>
        /// Train ticket created
        /// </summary>
        TrainTicket = 20,
        /// <summary>
        /// Train ticket refunded
        /// </summary>
        TrainTicketRefund = 21,
        /// <summary>
        /// Cash Discount recieved 
        /// </summary>
        CashDiscount = 22,
        /// <summary>
        /// Cash Discount refunded
        /// </summary>
        CashDiscountReversal = 23,
        /// <summary>
        /// For Misc Invoice
        /// </summary>
        MiscItem = 24,
        ///<summary>
        ///For insurance Refund
        ///<summary>
        InsuranceRefund = 25,
        ///<summary>
        ///Insurance Cancellation Charge
        ///<summary>
        InsuranceCancellationCharge = 26,
        /// <summary>
        /// for Mobile Recharge
        /// </summary>
        MobileRecharge = 27,
        /// <summary>
        /// for Activity Booking
        /// </summary>
        ActivityBooked = 28,

        /// <summary>
        /// TicketRefund -- PaymentDetailId
        /// </summary>
        TicketVoid = 29,

        /// <summary>
        /// TicketModification -- PaymentDetailId
        /// </summary>
        TicketModification = 30,


        /// <summary>
        /// TicketModification -- PaymentDetailId
        /// </summary>
        FixedDepartureBooked = 31,


        SightSeeingRefund = 32,
        SightseeingCancellationCharge = 33,
        CardHotelBooked = 34,
        CardHotelRefund = 35,
        CardHotelCancellationCharge = 36,
        CardTicketCreated = 37,
        CardTicketCancellationCharge = 38,
        CardTicketVoid = 39,
        CardTicketRefund = 40,
        CardTicketModification = 41,
        /// <summary>
        /// Transfer Refund
        /// </summary>
        TransferRefund = 42,
        TransferCancellationCharge = 43, //added by chandan on 18032016
        CarBooked = 44,
        CarCancellationCharge = 45,
        CarRefund = 46,

        VisaReceipt = 47,
        OK2BReceipt = 48

    }
    public enum MemberType
    {
        SUPER = 0,
        ADMIN = 1,
        CASHIER = 2,
        OPERATIONS = 3,
        BACKOFFICE = 4,
        SUPERVISOR = 5,
        B2C = 6
    }
    public enum InvoiceItemTypeId
    {
        Ticketed = 1,
        Offline = 2,
        HotelBooking = 3,
        ActivityBooking = 4,
        InsuranceBooking = 5,
        SightseeingBooking = 6,
        TrainBooking = 7,
        Misc = 8,
        TransferBooking = 9,
        MobileRecharge = 10,
        FixedDepartureBooking = 11,
        VisaBooking = 14,
    }
    public enum ChargeType
    {

        /// <summary>
        /// OtherCharges
        /// </summary>
        OtherCharges = 1,
        /// <summary>
        /// ServiceCharge
        /// </summary>
        ServiceCharge = 2,
        /// <summary>
        /// CreditCardCharge
        /// </summary>
        CreditCardCharge = 3,
        TrainSuperFastCharge = 4,
        TrainReservationCharge = 5,
        TrainOtherCharge = 6,
        TrainBedRollCharge = 7,
        CurrencyConversionCharges = 8,
        SupplierMarkup = 9,
        TBOMarkup = 10,
        BankHandlingCharges = 11,
        RevHandlingCharge = 12,
        ConvenienceCharge = 13

    }
}