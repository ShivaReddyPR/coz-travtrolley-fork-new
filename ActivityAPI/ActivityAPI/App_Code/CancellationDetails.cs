﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    [DataContract]
    public class CancellationDetails
    {
        int resultIndex;
        string introduction;
        string[] itineraryDetails;
        string activityDetails;
        string[] cancellationPolicy;
        string activityName;
        string activityCityCode;
        ActivityFlexDetails[] activityFlexDetails;
        string[] notes;
        string image;
        string includes;
        string departurePoint;
        List<TourOperation> tourOperation;
        string excludes;

        [DataMember]
        public int ResultIndex
        {
            get { return resultIndex; }
            set { resultIndex = value; }
        }
        [DataMember]
        public string Introduction
        {
            get { return introduction; }
            set { introduction = value; }
        }
        [DataMember]
        public string[] ItineraryDetails
        {
            get { return itineraryDetails; }
            set { itineraryDetails = value; }
        }
        [DataMember]
        public string[] CancellationPolicies
        {
            get { return cancellationPolicy; }
            set { cancellationPolicy = value; }
        }
        [DataMember]
        public string ActivityDetails
        {
            get { return activityDetails; }
            set { activityDetails = value; }
        }
        [DataMember]
        public string ActivityCityCode
        {
            get { return activityCityCode; }
            set { activityCityCode = value; }
        }
        [DataMember]
        public string ActivityName
        {
            get { return activityName; }
            set { activityName = value; }
        }
        [DataMember]
        public ActivityFlexDetails[] ActivityFlexDetails
        {
            get { return activityFlexDetails; }
            set { activityFlexDetails = value; }
        }
        [DataMember]
        public string[] Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        [DataMember]
        public string Image
        {
            get { return image; }
            set { image = value; }
        }
        [DataMember]
        public string Includes
        {
            get { return includes; }
            set { includes = value; }
        }
        [DataMember]
        public string DeparturePoint
        {
            get { return departurePoint; }
            set { departurePoint = value; }
        }
        [DataMember]
        public List<TourOperation> TourOperation
        {
            get { return tourOperation; }
            set { tourOperation = value; }
        }
        [DataMember]
        public string Excludes
        {
            get { return excludes; }
            set { excludes = value; }
        }

        public static CancellationDetails getCancellationDetails(ActivityCancellationRequest request)
        {
            CancellationDetails cancellationDetails = new CancellationDetails();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@activityId", request.ResultIndex);

            DataSet ds = DBGateway.ExecuteQuery("Act_Service_CancellationDetails", paramList);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                List<TourOperation> lstTourOperation = new List<TourOperation>();
                DataRow dr = ds.Tables[0].Rows[0];
                {
                    TourOperation tourOperation = new TourOperation();
                    if (dr["activityCity"] != DBNull.Value)
                    {
                        cancellationDetails.ActivityCityCode = Convert.ToString(dr["activityCity"]);
                    }
                    if (dr["details"] != DBNull.Value)
                    {
                        cancellationDetails.ActivityDetails = Convert.ToString(dr["details"]);
                    }

                    if (dr["activityName"] != DBNull.Value)
                    {
                        cancellationDetails.ActivityName = Convert.ToString(dr["activityName"]);
                    }
                    if (dr["cancellPolicy"] != DBNull.Value)
                    {
                        string[] cancellationDet = Convert.ToString(dr["cancellPolicy"]).Split(new string[] { "#&#Please Note:" }, StringSplitOptions.RemoveEmptyEntries);
                        if (cancellationDet.Length >= 1)
                        {
                            for (int i = 0; i < cancellationDet.Length; i++)
                            {
                                if (i == 0)
                                {
                                    string[] cancelPolicy = cancellationDet[0].Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                    cancellationDetails.CancellationPolicies = cancelPolicy;
                                }
                                else if (i == 1)
                                {
                                    string[] notes = cancellationDet[1].Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                    cancellationDetails.Notes = notes;
                                }
                            }
                        }
                    }
                    if (dr["introduction"] != DBNull.Value)
                    {
                        cancellationDetails.Introduction = Convert.ToString(dr["introduction"]);
                    }
                    if (dr["activityId"] != DBNull.Value && Convert.ToInt32(dr["activityId"]) > 0)
                    {
                        cancellationDetails.ResultIndex = Convert.ToInt32(dr["activityId"]);
                    }
                    List<string> stringDetails = new List<string>();
                    if (dr["itinerary1"] != DBNull.Value)
                    {
                        stringDetails.Add(Convert.ToString(dr["itinerary1"]));
                    }
                    if (dr["itinerary2"] != DBNull.Value)
                    {
                        stringDetails.Add(Convert.ToString(dr["itinerary2"]));
                    }
                    cancellationDetails.ItineraryDetails = stringDetails.ToArray();
                    if (dr["inclusions"] != DBNull.Value)
                    {
                        cancellationDetails.Includes = Convert.ToString(dr["inclusions"]);
                    }
                    if (dr["imagePath1"] != DBNull.Value)
                    {
                        cancellationDetails.Image = Convert.ToString(dr["imagePath1"]);
                    }
                    if ((dr["transferIncluded"] != DBNull.Value) && Convert.ToString(dr["transferIncluded"]) == "Yes" && dr["pickupLocation"] != DBNull.Value)
                    {
                        cancellationDetails.DeparturePoint = Convert.ToString(dr["pickupLocation"]);

                        tourOperation.DepName = Convert.ToString(dr["pickupLocation"]);

                    }

                    if (dr["startFrom"] != DBNull.Value && Convert.ToDateTime(dr["startFrom"]) != DateTime.MinValue && dr["endTo"] != DBNull.Value && Convert.ToDateTime(dr["endTo"]) != DateTime.MinValue)
                    {
                        tourOperation.FromTime = Convert.ToDateTime(dr["startFrom"]).ToString("hh:mm:ss tt");
                        tourOperation.ToTime = Convert.ToDateTime(dr["endTo"]).ToString("hh:mm:ss tt");
                    }
                    if (dr["activityId"] != DBNull.Value)
                    {
                        tourOperation.ItemCode = Convert.ToString(dr["activityId"]);
                    }
                    if (dr["activityCity"] != DBNull.Value)
                    {
                        tourOperation.CityCode = Convert.ToString(dr["activityCity"]);
                    }
                    if (dr["exclusions"] != DBNull.Value)
                    {
                        cancellationDetails.Excludes = Convert.ToString(dr["exclusions"]);
                    }
                    if (dr["availableFrom"] != DBNull.Value)
                    {
                        tourOperation.FromDate = Convert.ToDateTime(dr["availableFrom"]);
                    }
                    if (dr["availableTo"] != DBNull.Value)
                    {
                        tourOperation.ToDate = Convert.ToDateTime(dr["availableTo"]);
                    }
                    lstTourOperation.Add(tourOperation);
                    cancellationDetails.TourOperation = lstTourOperation;

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        List<ActivityFlexDetails> lstFlexDet = new List<ActivityFlexDetails>();
                        foreach (DataRow dr1 in ds.Tables[1].Rows)
                        {
                            ActivityFlexDetails flexDetails = new ActivityFlexDetails();
                            flexDetails.FlexFieldDataType = Convert.ToString(dr1["flexDataType"]);
                            flexDetails.FlexFieldsMandatory = Utility.ToBoolean(dr1["flexMandatoryStatus"]);
                            flexDetails.FlexFieldsName = Convert.ToString(dr1["flexLabel"]);
                            lstFlexDet.Add(flexDetails);
                        }
                        cancellationDetails.ActivityFlexDetails = lstFlexDet.ToArray();
                    }
                }
            }
            return cancellationDetails;
        }
    }
}
