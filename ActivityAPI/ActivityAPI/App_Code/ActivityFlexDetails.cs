﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivityFlexDetails
    {
        string flexFieldsName;
        string flexFieldDataType;
        bool flexFieldMandatory;

        [DataMember]
        public string FlexFieldsName
        {
            get { return flexFieldsName; }
            set { flexFieldsName = value; }
        }
        [DataMember]
        public string FlexFieldDataType
        {
            get { return flexFieldDataType; }
            set { flexFieldDataType = value; }
        }
        [DataMember]
        public bool FlexFieldsMandatory
        {
            get { return flexFieldMandatory; }
            set { flexFieldMandatory = value; }
        }
    }
}