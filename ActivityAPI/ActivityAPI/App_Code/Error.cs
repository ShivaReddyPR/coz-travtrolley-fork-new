﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    
    public class Error
    {
        string errorCode;
        string errorMessage;

        
        public string ErrorCode
        {
            get { return errorCode; }
            set { errorCode = value; }
        }
        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }
    }
}