﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    public enum InvoiceStatus
    {
        Raised = 1,
        PartiallyPaid = 2,
        Paid = 3,
        Inactive = 4,
        CreditNote = 5
    }

    public class Invoice
    {
        /// <summary>
        /// Unique number of an invoice
        /// </summary>
        private int invoiceNumber;
        /// <summary>
        /// Gets or sets invoice number.
        /// </summary>
        public int InvoiceNumber
        {
            get { return invoiceNumber; }
            set { invoiceNumber = value; }
        }
        private string leadPaxName = string.Empty;
        private string itinerary = string.Empty;
        private decimal totalPrice = 0;
        //private int manualInvoiceCounter = 0;

        /// <summary>
        /// Prefix Code for Domestic And International Flight
        /// </summary>
        private string docTypeCode;

        public string LeadPaxName
        {
            get { return leadPaxName; }
            set { leadPaxName = value; ; }
        }
        public string Itinerary
        {
            get { return itinerary; }
            set { itinerary = value; ; }
        }
        public decimal TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; ; }
        }
        /// <summary>
        /// Gets or Sets docTypeCode
        /// </summary>
        public string DocTypeCode
        {
            get { return docTypeCode; }
            set { docTypeCode = value; }
        }
        /// <summary>
        /// Sequence Number for Domestic And International Flight
        /// </summary>
        private int documentNumber;
        /// <summary>
        /// Gets or Sets DocumentNumber
        /// </summary>
        public int DocumentNumber
        {
            get { return documentNumber; }
            set { documentNumber = value; }
        }
        //private string completeInvoiceNumber;
        ///// <summary>
        ///// Gets or Sets DocumentNumber
        ///// </summary>
        //public string CompleteInvoiceNumber
        //{
        //    get { return docTypeCode + documentNumber.ToString(); }
        //}

        /// <summary>
        /// Agency Id to which the invoice corresponds.
        /// </summary>
        private int agencyId;
        /// <summary>
        /// Gets or sets the agency id to which the invoice corresponds.
        /// </summary>
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }

        private decimal agentBalance;

        public decimal AgentBalance
        {
            get { return agentBalance; }
            set { agentBalance = value; }
        }

        /// <summary>
        /// Status of the invoice
        /// </summary>
        private InvoiceStatus status;
        /// <summary>
        /// Gets or sets the status of the invoice
        /// </summary>
        public InvoiceStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        /// <summary>
        /// Mode of payment
        /// </summary>
        private string paymentMode;
        /// <summary>
        /// Gets or sets the mode of payment
        /// </summary>
        public string PaymentMode
        {
            get { return paymentMode; }
            set { paymentMode = value; }
        }

        /// <summary>
        /// array of invoice line items
        /// </summary>
        private List<InvoiceLineItem> lineItem;
        /// <summary>
        /// Gets or sets invoice line items
        /// </summary>
        public List<InvoiceLineItem> LineItem
        {
            get { return lineItem; }
            set { lineItem = value; }
        }

        //List<Li

        /// <summary>
        /// Remarks
        /// </summary>
        private string remarks;
        /// <summary>
        /// Gets or sets the remarks
        /// </summary>
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }

        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }
        /// <summary>
        /// Invoice due date field contains due date of that invoice
        /// </summary>
        private DateTime invoiceDueDate;
        /// <summary>
        /// Invoice due date property
        /// </summary>
        public DateTime InvoiceDueDate
        {
            get
            {
                return this.invoiceDueDate;
            }
            set
            {
                this.invoiceDueDate = value;
            }
        }
        /// <summary>
        /// Gets total Amount(price) of the invoice
        /// </summary>


        /// <summary>
        /// Boolean value to indicate whether the invoice is manually updated or not
        ///</summary>
        private bool manuallyUpdated;
        /// <summary>
        ///Property for manuallyUpdated field
        ///</summary>
        public bool ManuallyUpdated
        {
            get
            {
                return manuallyUpdated;

            }
            set
            {
                manuallyUpdated = value;

            }
        }


        /// <summary>
        ///XONumber for supplier 
        ///</summary>
        private string xoNumber;
        /// <summary>
        ///Property for XONumber field
        ///</summary>
        public string XONumber
        {
            get
            {
                return xoNumber;
            }
            set
            {
                xoNumber = value;
            }
        }

        /// <summary>
        /// Staff Remarks
        /// </summary>
        private string staffRemarks;
        /// <summary>
        /// Property  to Access Staff Remarks
        /// </summary>

        public string StaffRemarks
        {
            get
            {
                return (staffRemarks);
            }
            set
            {
                staffRemarks = value;
            }

        }

        public decimal Amount
        {
            get
            {
                decimal amount = 0;
                return amount;
            }
        }

        private long tranxHeaderId;

        public long TranxHeaderId
        {
            get
            {
                return tranxHeaderId;
            }
            set
            {
                tranxHeaderId = value;
            }
        }

        public static int isInvoiceGenerated(int itemRefId, ProductType prodType)
        {
            int invoiceNumber = 0;
            
            if (itemRefId <= 0)
            {
                throw new ArgumentException("Item Reference Id should be positive integer =" + itemRefId);
            }

            if (prodType == ProductType.Activity)
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ActivityId", itemRefId);
                SqlDataReader data = DBGateway.ExecuteReaderSP("Act_Service_IsInvoiceGeneratedForActivity", paramList, connection);
                if (data.Read())
                {
                    invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                }
                data.Close();
                connection.Close();
            }

            return invoiceNumber;
        }

        /// <summary>
        /// Load method for a invoice
        /// </summary>
        /// <param name="invoiceNumber">invoice number</param>
        public void Load(int invoiceNumber)
        {
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number should be positive integer invoiceNumber=" + invoiceNumber);
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP("Act_Service_GetInvoice", paramList, connection);
            this.invoiceNumber = invoiceNumber;
            if (dataReader.Read())
            {
                this.docTypeCode = Convert.ToString(dataReader["docTypeCode"]);
                this.documentNumber = Convert.ToInt32(dataReader["documentNumber"]);
                this.agencyId = Convert.ToInt32(dataReader["agencyId"]);
                this.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), dataReader["status"].ToString());
                InvoiceLineItem temp = new InvoiceLineItem();
                this.remarks = dataReader["remarks"].ToString();
                this.lineItem = temp.GetLineItems(invoiceNumber);
                if (dataReader["invoiceDueDate"] != DBNull.Value)
                {
                    this.invoiceDueDate = Convert.ToDateTime(dataReader["invoiceDueDate"]);
                }
                this.xoNumber = dataReader["XONumber"].ToString();
                this.staffRemarks = dataReader["StaffRemarks"].ToString();
                this.createdBy = Convert.ToInt32(dataReader["createdBy"]);
                this.createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                this.lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                this.lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                if (dataReader["invoiceDetail"] != DBNull.Value)
                {
                    string[] invoiceDetail = Convert.ToString(dataReader["invoiceDetail"]).Split('|');
                    if (invoiceDetail.Length == 2)
                    {
                        itinerary = invoiceDetail[0];
                        leadPaxName = invoiceDetail[1];
                    }
                }
                if (dataReader["totalPrice"] != DBNull.Value)
                {
                    totalPrice = Convert.ToDecimal(dataReader["totalPrice"]);
                }
            }
            dataReader.Close();
            connection.Close();
        }

        public void UpdateInvoice()
        {
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("invoiceNumber must have a positive non zero integer value", "invoiceNumber");
            }
            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@status", (int)status);
            paramList[3] = new SqlParameter("@remarks", remarks);
            paramList[4] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[5] = new SqlParameter("@lastModifiedOn", lastModifiedOn);
            paramList[6] = new SqlParameter("@manuallyUpdated", manuallyUpdated);
            paramList[7] = new SqlParameter("@totalPrice", Math.Round(totalPrice));
            paramList[8] = new SqlParameter("@staffRemarks", staffRemarks);
            paramList[9] = new SqlParameter("@P_RET_AGENT_BALANCE", 0);
            paramList[9].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP("Act_Service_UpdateInvoice", paramList);

            agentBalance = Convert.ToDecimal(paramList[9].Value);
        }

        public int InvoiceRaised(int productId, ProductType prodType)
        {
            SqlConnection connection = DBGateway.GetConnection();
            int lineItemCount = 0;
            if (prodType == ProductType.Activity)
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ActivityId", productId);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP("Act_Service_InvoiceRaisedForActivity", paramList, connection);
                if (dataReader.Read())
                {
                    lineItemCount = Convert.ToInt32(dataReader["lineCount"]);
                }
                dataReader.Close();
            }
            connection.Close();
            return lineItemCount;
        }

        public void Save(bool isDomestic, byte counter, ProductType prodType)
        {
            
            int rowsAffected = 0;

            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            if ((int)status == 0)
            {
                throw new ArgumentException("status must have a value", "status");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            }
            GetInvoiceDueDate(isDomestic);
            SqlParameter[] paramList = new SqlParameter[18];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@status", (int)status);
            paramList[3] = new SqlParameter("@paymentMode", paymentMode);
            paramList[4] = new SqlParameter("@remarks", remarks);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@invoiceDueDate", invoiceDueDate);
            paramList[7] = new SqlParameter("@isDomestic", isDomestic);
            paramList[8] = new SqlParameter("@productType", prodType);
            paramList[9] = new SqlParameter("@xONumber", xoNumber);
            paramList[10] = new SqlParameter("@staffRemarks", staffRemarks);

            if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
            {
                paramList[11] = new SqlParameter("@createdon", DateTime.UtcNow);
            }
            else
            {
                paramList[11] = new SqlParameter("@createdon", CreatedOn);
            }
            //int agencyTypeId = Agency.GetAgencyTypeId(agencyId);
           // int agencyTypeId = 1;
            string roundPrecision = System.Configuration.ConfigurationManager.AppSettings["RoundPrecision"].ToString();
            //foreach (InvoiceLineItem invoiceLineItem in this.LineItem)
            //{

            //    if (prodType == ProductType.Insurance)
            //    {

            //        totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetAgentInsurancePrice(), Convert.ToInt32(roundPrecision));
            //    }
            //    else
            //    {
            //        //if (agencyTypeId == (int)Agencytype.Service)
            //        if (agencyTypeId == 1)
            //        {
            //            totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetServiceAgentPrice(), Convert.ToInt32(roundPrecision));
            //        }
            //        else
            //        {
            //            totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetAgentPrice(), Convert.ToInt32(roundPrecision));
            //        }
            //    }
            //}
            string invoiceDetail = "";
            //if (prodType == ProductType.Flight)  ziya-todo
            //{
            //    Ticket ticket = new Ticket();
            //    ticket.Load(this.lineItem[0].ItemReferenceNumber);
            //    FlightItinerary flightItinerary = new FlightItinerary(ticket.FlightId);
            //    itinerary = GetItineraryString(flightItinerary);
            //    leadPaxName = GetLeadPax(flightItinerary);
            //    invoiceDetail = itinerary + "|" + leadPaxName;
            //}
            paramList[12] = new SqlParameter("@invoiceDetail", invoiceDetail);
            if (prodType == ProductType.MobileRecharge)
            {
                paramList[13] = new SqlParameter("@totalPrice", Math.Round(totalPrice, Convert.ToInt32(roundPrecision)));

            }
            else
            {
                paramList[13] = new SqlParameter("@totalPrice", Math.Round(totalPrice));
            }
            paramList[14] = new SqlParameter("@docTypeCode", "");
            paramList[14].Direction = ParameterDirection.Output;
            paramList[14].Size = 2;
            paramList[15] = new SqlParameter("@documentNumber", 0);
            paramList[15].Direction = ParameterDirection.Output;
            paramList[16] = new SqlParameter("@P_RET_AGENT_BALANCE", 0);
            paramList[16].Direction = ParameterDirection.Output;
            if (TranxHeaderId > 0)
            {
                paramList[17] = new SqlParameter("@tranxHeaderId", TranxHeaderId);
            }
            else
            {
                paramList[17] = new SqlParameter("@tranxHeaderId", null);
            }
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP("Act_Service_AddInvoice", paramList);
            }
            catch (CT.TicketReceipt.DataAccessLayer.DALException exQc)
            {
                // If method fails once it should try once again.
                if (counter < 1)
                {
                    counter++;
                    Save(isDomestic, counter, prodType);
                }
                else
                {
                    throw exQc;
                }
            }

            invoiceNumber = Convert.ToInt32(paramList[0].Value);
            docTypeCode = Convert.ToString(paramList[14].Value);
            documentNumber = Convert.ToInt32(paramList[15].Value);
            agentBalance = Convert.ToDecimal(paramList[16].Value);
            for (int i = 0; i < lineItem.Count; i++)
            {
                InvoiceLineItem line = lineItem[i];
                line.InvoiceNumber = invoiceNumber;
                line.CreatedBy = createdBy;
                if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
                {
                    line.CreatedOn = DateTime.UtcNow;
                }
                else
                {
                    line.CreatedOn = CreatedOn;
                }



                line.CreatedOn = createdOn;
                line.Save();
                lineItem[i] = line;
            }
        }

        public void GetInvoiceDueDate(bool isDomestic)
        {

            DateTime currentTime = DateTime.Now.ToUniversalTime() + new TimeSpan(4, 00, 0);
            ////TODO: This 13 and 28 are hard coded later it will be in config file
            if (isDomestic)
            {
                if (currentTime.Day >= 1 && currentTime.Day <= 15)
                {
                    invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month, 25, 23, 59, 59);
                }
                else
                {
                    if (currentTime.Month == 12)
                    {
                        invoiceDueDate = new DateTime(currentTime.Year + 1, 1, 10, 23, 59, 59);
                    }
                    else
                    {
                        invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month + 1, 10, 23, 59, 59);
                    }
                }
            }
            else
            {
                if (currentTime.Day >= 1 && currentTime.Day <= 15)
                {
                    DateTime currentISTDate = DateTime.Now.ToUniversalTime() + new TimeSpan(4, 00, 0); ;
                    int daysInMonth = DateTime.DaysInMonth(currentISTDate.Year, currentISTDate.Month);
                    invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month, daysInMonth, 23, 59, 59);
                }
                else
                {
                    if (currentTime.Month == 12)
                    {
                        invoiceDueDate = new DateTime(currentTime.Year + 1, 1, 15, 23, 59, 59);
                    }
                    else
                    {
                        invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month + 1, 15, 23, 59, 59);
                    }
                }
            }
        }
    }

    public struct InvoiceLineItem
    {
        /// <summary>
        /// unique invoice number
        /// </summary>
        private int invoiceNumber;
        /// <summary>
        /// Gets or sets the invoice number
        /// </summary>
        public int InvoiceNumber
        {
            get { return invoiceNumber; }
            set { invoiceNumber = value; }
        }

        /// <summary>
        /// Reference number of item.
        /// </summary>
        private int itemReferenceNumber;
        /// <summary>
        /// Gets or sets reference number of item.
        /// </summary>
        public int ItemReferenceNumber
        {
            get { return itemReferenceNumber; }
            set { itemReferenceNumber = value; }
        }

        /// <summary>
        /// Description of item
        /// </summary>
        private string itemDescription;
        /// <summary>
        /// Gets or sets the description of item
        /// </summary>
        public string ItemDescription
        {
            get { return itemDescription; }
            set { itemDescription = value; }
        }

        /// <summary>
        /// Id of item type
        /// </summary>
        private int itemTypeId;
        /// <summary>
        /// Gets or sets the Id of item type
        /// </summary>
        public int ItemTypeId
        {
            get { return itemTypeId; }
            set { itemTypeId = value; }
        }
        /// <summary>
        /// Field for Price
        /// </summary>
        private int priceId;
        /// <summary>
        /// Property for price
        /// </summary>
        public int PriceId
        {
            get
            {
                return this.priceId;
            }
            set
            {
                this.priceId = value;
            }
        }

        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }

        public void Save()
        {
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("invoiceNumber must have a value", "invoiceNumber");
            }
            if (itemReferenceNumber <= 0)
            {
                throw new ArgumentException("itemReferenceNumber must have a value", "itemReferenceNumber");
            }
            if (itemTypeId <= 0)
            {
                throw new ArgumentException("itemTypeId must have a value", "itemTypeId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a value", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@itemReferenceNumber", itemReferenceNumber);
            paramList[2] = new SqlParameter("@itemDescription", itemDescription);
            paramList[3] = new SqlParameter("@itemTypeId", itemTypeId);
            paramList[4] = new SqlParameter("@priceId", priceId);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@createdOn", createdOn);
            int rowsAffected = DBGateway.ExecuteNonQuerySP("Act_Service_AddInvoiceLineItem", paramList);
        }

        //public void Save(BookingStatus bookingStatus)
        //{
        //    Trace.TraceInformation("InvoiceLineItem.Save entered : invoiceNumber = " + invoiceNumber + ", itemReferenceNumber = " + itemReferenceNumber);
        //    if (invoiceNumber <= 0)
        //    {
        //        throw new ArgumentException("invoiceNumber must have a value", "invoiceNumber");
        //    }
        //    if (itemReferenceNumber <= 0)
        //    {
        //        throw new ArgumentException("itemReferenceNumber must have a value", "itemReferenceNumber");
        //    }
        //    if (itemTypeId <= 0)
        //    {
        //        throw new ArgumentException("itemTypeId must have a value", "itemTypeId");
        //    }
        //    if (createdBy <= 0)
        //    {
        //        throw new ArgumentException("createdBy must have a value", "createdBy");
        //    }
        //    SqlParameter[] paramList = new SqlParameter[7];
        //    paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
        //    paramList[1] = new SqlParameter("@itemReferenceNumber", itemReferenceNumber);
        //    paramList[2] = new SqlParameter("@itemDescription", itemDescription);
        //    paramList[3] = new SqlParameter("@itemTypeId", itemTypeId);
        //    paramList[4] = new SqlParameter("@priceId", price.PriceId);
        //    paramList[5] = new SqlParameter("@createdBy", createdBy);
        //    paramList[6] = new SqlParameter("@createdOn", createdOn);
        //    int rowsAffected = 0;
        //    if (bookingStatus == BookingStatus.Cancelled || bookingStatus == BookingStatus.Void)
        //    {
        //        rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddCreditNoteLineItem, paramList);
        //    }
        //    else
        //    {
        //        rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoiceLineItem, paramList);
        //    }
        //    Trace.TraceInformation("InvoiceLineItem.Save exiting : rowsAffected = " + rowsAffected);
        //}

        //public void Update()
        //{
        //    if (invoiceNumber <= 0)
        //    {
        //        throw new ArgumentException("invoiceNumber must have a value", "invoiceNumber");
        //    }
        //    if (itemReferenceNumber <= 0)
        //    {
        //        throw new ArgumentException("itemReferenceNumber must have a value", "itemReferenceNumber");
        //    }
        //    if (itemTypeId <= 0)
        //    {
        //        throw new ArgumentException("itemTypeId must have a value", "itemTypeId");
        //    }
        //    if (createdBy <= 0)
        //    {
        //        throw new ArgumentException("createdBy must have a value", "createdBy");
        //    }
        //    SqlParameter[] paramList = new SqlParameter[7];
        //    paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
        //    paramList[1] = new SqlParameter("@itemReferenceNumber", itemReferenceNumber);
        //    paramList[2] = new SqlParameter("@itemDescription", itemDescription);
        //    paramList[3] = new SqlParameter("@itemTypeId", itemTypeId);
        //    paramList[4] = new SqlParameter("@priceId", priceId);
        //    paramList[5] = new SqlParameter("@createdBy", lastModifiedBy);
        //    paramList[6] = new SqlParameter("@createdOn", DateTime.UtcNow);
        //    int rowsAffected = DBGateway.ExecuteNonQuerySP("Act_Service_UpdateInvoice", paramList);
        //}

        /// <summary>
        /// updating invoice line item by updating referene number using oldrefrencenumber 
        /// </summary>
        /// <param name="oldItemReferenceNumber">reference number to be replaced with new one</param>
        //public void UpdateByReferenceNumber(int oldItemReferenceNumber)
        //{
        //    if (invoiceNumber <= 0)
        //    {
        //        throw new ArgumentException("invoiceNumber must have a value", "invoiceNumber");
        //    }
        //    if (itemReferenceNumber <= 0)
        //    {
        //        throw new ArgumentException("itemReferenceNumber must have a value", "itemReferenceNumber");
        //    }
        //    if (itemTypeId <= 0)
        //    {
        //        throw new ArgumentException("itemTypeId must have a value", "itemTypeId");
        //    }
        //    if (createdBy <= 0)
        //    {
        //        throw new ArgumentException("createdBy must have a value", "createdBy");
        //    }
        //    SqlParameter[] paramList = new SqlParameter[8];
        //    paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
        //    paramList[1] = new SqlParameter("@itemReferenceNumber", itemReferenceNumber);
        //    paramList[2] = new SqlParameter("@itemDescription", itemDescription);
        //    paramList[3] = new SqlParameter("@itemTypeId", itemTypeId);
        //    paramList[4] = new SqlParameter("@priceId", price.PriceId);
        //    paramList[5] = new SqlParameter("@createdBy", lastModifiedBy);
        //    paramList[6] = new SqlParameter("@createdOn", DateTime.UtcNow);
        //    paramList[7] = new SqlParameter("@oldItemReferenceNumber", oldItemReferenceNumber);
        //    int rowsAffected = DBGateway.ExecuteNonQuerySP("Act_Service_UpdateInvoiceLineItemByReferenceNumber", paramList);
        //}

        /// <summary>
        /// Gets the list of line items of a invoice
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        public List<InvoiceLineItem> GetLineItems(int invoiceNumber)
        {
            List<InvoiceLineItem> tempList = new List<InvoiceLineItem>();
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number Id should be positive integer invoiceNumber=" + invoiceNumber);
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            SqlDataReader data = DBGateway.ExecuteReaderSP("Act_Service_GetLineItems", paramList, connection);
            while (data.Read())
            {
                InvoiceLineItem instance = new InvoiceLineItem();
                instance.InvoiceNumber = invoiceNumber;
                instance.itemDescription = data["itemDescription"].ToString();
                instance.itemReferenceNumber = Convert.ToInt32(data["itemReferenceNumber"]);
                instance.itemTypeId = Convert.ToInt32(data["itemTypeId"]);
                //instance.price = new PriceAccounts();
                instance.priceId = Convert.ToInt32(data["priceId"]);
                createdBy = Convert.ToInt32(data["createdBy"]);
                createdOn = Convert.ToDateTime(data["createdOn"]);
                lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                tempList.Add(instance);
            }
            data.Close();
            connection.Close();
            return tempList;
        }
    }
}