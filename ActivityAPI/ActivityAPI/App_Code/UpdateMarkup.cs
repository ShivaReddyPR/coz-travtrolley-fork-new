﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    public class UpdateMarkup
    {
        #region Member Variables
        private const long NEW_RECORD = -1;
        private int _id;
        private int _mrDId;
        private int _productId;
        private string _sourceId;
        private int _createdBy;

        private int _agentId;
        private decimal _markup;
        private string _markupType;
        private decimal _discount;
        private string _discountType;

        private decimal _agentMarkup;
        private decimal _ourCommission;

        private int _userId;
        private string b2cMarkupType = "B2B";
        #endregion

        #region properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int MrDId
        {
            get { return _mrDId; }
            set { _mrDId = value; }
        }
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public string SourceId
        {
            get { return _sourceId; }
            set { _sourceId = value; }
        }

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public decimal Markup
        {
            get { return _markup; }
            set { _markup = value; }
        }

        public string MarkupType
        {
            get { return _markupType; }
            set { _markupType = value; }
        }

        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        public string DiscountType
        {
            get { return _discountType; }
            set { _discountType = value; }
        }
        public decimal AgentMarkup
        {
            get { return _agentMarkup; }
            set { _agentMarkup = value; }
        }
        public decimal OurCommission
        {
            get { return _ourCommission; }
            set { _ourCommission = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public string B2CMarkupType
        {
            get { return b2cMarkupType; }
            set { b2cMarkupType = value; }
        }

        #endregion

        public static DataTable Load(int agentId, string source, int prodId)
        {
            //UpdateMarkup objUpdateMarkup = new UpdateMarkup();
            //SqlDataReader data = null;
            DataTable dt = new DataTable();
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[3];
                    if (agentId > 1)
                    {
                        paramList[0] = new SqlParameter("@AgentId", agentId);
                    }
                    paramList[1] = new SqlParameter("@Source", source);
                    paramList[2] = new SqlParameter("@ProductId", prodId);
                    dt = DBGateway.FillDataTableSP("Act_Service_GetAgentMarkup", paramList);
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
    }
}