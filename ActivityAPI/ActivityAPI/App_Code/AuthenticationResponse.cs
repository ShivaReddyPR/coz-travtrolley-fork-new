﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Web;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    [DataContract]
    public class AuthenticationResponse
    {
        string tokenId;
        string firstName;
        string lastName;
        string emailId;
        string loginTime;
        string responseTimeStamp;
        Error error;

        [DataMember]
        public string TokenId
        {
            get { return tokenId; }
            set { tokenId = value; }
        }
        [DataMember]
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        [DataMember]
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        [DataMember]
        public string EmailId
        {
            get { return emailId; }
            set { emailId = value; }
        }
        [DataMember]
        public string ResponseTime
        {
            get { return responseTimeStamp; }
            set { responseTimeStamp = value; }
        }
        [DataMember]
        public string LoginTime
        {
            get { return loginTime; }
            set { loginTime = value; }
        }
        [DataMember]
        public Error Error
        {
            get { return error; }
            set { error = value; }
        }




        /// <summary>
        /// Authenticating the user by UserName and Password 
        /// </summary>
        /// <param name="authenticationRequest"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static AuthenticationResponse ActivityAuthentication(AuthenticationRequest authenticationRequest, ref int userId)
        {
            //int userId = 0;
            AuthenticationResponse authenticationResponse = new AuthenticationResponse();
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@UserName", authenticationRequest.UserName);
                paramList[1] = new SqlParameter("@Password", authenticationRequest.Password);
                paramList[2] = new SqlParameter("@P_MSG_Type", SqlDbType.VarChar, 10);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@P_MSG_Text", SqlDbType.VarChar, 100);
                paramList[3].Direction = ParameterDirection.Output;
                paramList[4] = new SqlParameter("@P_TokenID", SqlDbType.VarChar, 40);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@P_LoginTime", SqlDbType.DateTime);
                paramList[5].Direction = ParameterDirection.Output;

                DataSet ds = DBGateway.ExecuteQuery("Act_Service_Authentication", paramList);
                string messageType = Utility.ToString(paramList[2]);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[3].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 1)
                {
                    DataRow dr = ds.Tables[0].Rows[0];

                    Error error = new Error();
                    error.ErrorCode = "000";
                    error.ErrorMessage = "Successfully Authentication";
                    authenticationResponse.Error = error;
                    userId = Utility.ToInteger(dr["user_id"]);
                    authenticationResponse.FirstName = Utility.ToString(dr["user_first_name"]);
                    authenticationResponse.LastName = Utility.ToString(dr["user_last_name"]);
                    authenticationResponse.EmailId = Utility.ToString(dr["user_email"]);
                    authenticationResponse.TokenId = Utility.ToString(paramList[4].Value);
                    authenticationResponse.LoginTime = Utility.ToString(paramList[5].Value);
                    authenticationResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");

                }
                else
                {
                    throw new Exception("User Id/password does not exist !");
                }
            }
            catch (Exception ex)
            {
                Error error = new Error();
                error.ErrorCode = "001";
                error.ErrorMessage = ex.Message;
                authenticationResponse.Error = error;
                authenticationResponse.FirstName = string.Empty;
                authenticationResponse.LastName = string.Empty;
                authenticationResponse.EmailId = string.Empty;
                authenticationResponse.TokenId = string.Empty;
                authenticationResponse.LoginTime = DateTime.MinValue.ToString("dd-MM-yyyy hh:mm:ss tt");
                authenticationResponse.ResponseTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");

                Audit.Add(EventType.Login, Severity.High, 0, "ActivityService:" + ex.ToString(), authenticationRequest.UserIPAddress);
            }

            return authenticationResponse;
        }

        /// <summary>
        /// Passing the TokenId and Checking the request user is valid user or not.
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        public static LoginInfo ValidateUser(string tokenId)
        {
            LoginInfo loginInfo = null;
            int loginTimeOutValue = Convert.ToInt32(ConfigurationManager.AppSettings["LoginTimeOut"]);

            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@TokenID", tokenId);
                paramList[1] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[1].Direction = ParameterDirection.Output;
                paramList[2] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@LoginTimeOut", loginTimeOutValue);

                DataSet ds = DBGateway.FillSP("Act_Service_CheckingTokenId", paramList);
                string messageType = Utility.ToString(paramList[1].Value);
                string message = null;
                if (messageType == "E")
                {
                    message = Utility.ToString(paramList[2].Value);
                    throw new Exception(message);
                }
                if (ds != null && ds.Tables.Count > 0)
                {
                    try
                    {
                        loginInfo = new LoginInfo(ds.Tables[0].Rows[0]);

                        StaticData sd = new StaticData();
                        sd.BaseCurrency = loginInfo.Currency;
                        loginInfo.AgentExchangeRates = sd.CurrencyROE;


                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Login, Severity.Normal, Convert.ToInt32(loginInfo.UserID), "ActivityService:" + ex.Message.ToString(), string.Empty);//, Convert.ToInt32(CT.BookingEngine.ProductType.Activity), "ActivityService", "Cozmo", string.Empty, "TokenId:" + tokenId
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.Normal, 0, "ActivityService:" + ex.Message.ToString(), string.Empty);//, Convert.ToInt32(CT.BookingEngine.ProductType.Activity), "ActivityService", "Cozmo", string.Empty, "TokenId:" + tokenId
            }
            return loginInfo;
        }
    }
}
