﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    public class StaticData
    {
        private int currencyId;
        private string currency;
        private string currencyCode;
        private decimal rateOfExchange;
        private bool active;
        private DateTime lastModifiedOn;
        private int lastModifiedBy;
        private string baseCurrency;


        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }

        }
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }
        public decimal RateOfExchange
        {
            get { return rateOfExchange; }
            set { rateOfExchange = value; }
        }
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }
        public int LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }
        public string BaseCurrency
        {
            get { return baseCurrency; }
            set { baseCurrency = value; }
        }
        private Dictionary<string, decimal> currencyROE = null;

        public Dictionary<string, decimal> CurrencyROE
        {
            get
            {
                if (currencyROE != null)
                {
                    return currencyROE;
                }
                else
                {
                    currencyROE = new Dictionary<string, decimal>();
                    SqlParameter[] paramList = new SqlParameter[1];
                    if (string.IsNullOrEmpty(this.baseCurrency))
                    {
                        throw new ArgumentException("base currency not defined", "ROE");
                    }
                    paramList[0] = new SqlParameter("@baseCurrency", this.baseCurrency);
                    DataTable dtROE = DBGateway.FillDataTableSP("usp_GetStaticData", paramList);

                    if (dtROE != null)
                    {
                        foreach (DataRow dr in dtROE.Rows)
                        {
                            currencyCode = dr["ex_rate_currency_code"].ToString();
                            rateOfExchange = Convert.ToDecimal(dr["ex_rate_rate"].ToString());
                            currencyROE.Add(currencyCode, rateOfExchange);
                        }
                    }

                    return currencyROE;
                }
            }

        }
    }
}