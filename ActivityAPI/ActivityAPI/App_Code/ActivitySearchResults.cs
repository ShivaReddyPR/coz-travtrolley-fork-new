﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Web;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    [DataContract]
    public class ActivitySearchResults
    {
        int resultIndex;
        string activityName;
        string activityCityCode;
        string imagePath1;
        string imagePath2;
        string imagePath3;
        string overview;
        string durationHours;
        string[] unavialbleDates;
        bool transferIncluded;
        string pickupLocation;
        string cancellationPolicy;
        string thingsToBring;
        double totalAmount;
        string[] themeNames;
        string currency;
        string inclusions;

        [DataMember]
        public int ResultIndex
        {
            get { return resultIndex; }
            set { resultIndex = value; }
        }
        [DataMember]
        public string ActivityName
        {
            get { return activityName; }
            set { activityName = value; }
        }
        [DataMember]
        public string ActivityCityCode
        {
            get { return activityCityCode; }
            set { activityCityCode = value; }
        }
        [DataMember]
        public string ImagePath1
        {
            get { return imagePath1; }
            set { imagePath1 = value; }
        }
        [DataMember]
        public string ImagePath2
        {
            get { return imagePath2; }
            set { imagePath2 = value; }
        }
        [DataMember]
        public string ImagePath3
        {
            get { return imagePath3; }
            set { imagePath3 = value; }
        }
        [DataMember]
        public string Overview
        {
            get { return overview; }
            set { overview = value; }
        }
        [DataMember]
        public string DurationHours
        {
            get { return durationHours; }
            set { durationHours = value; }
        }
        [DataMember]
        public string[] UnavailableDates
        {
            get { return unavialbleDates; }
            set { unavialbleDates = value; }
        }
        [DataMember]
        public bool TransferIncluded
        {
            get { return transferIncluded; }
            set { transferIncluded = value; }
        }
        [DataMember]
        public string PickupLocation
        {
            get { return pickupLocation; }
            set { pickupLocation = value; }
        }
        [DataMember]
        public string CancellationPolicy
        {
            get { return cancellationPolicy; }
            set { cancellationPolicy = value; }
        }
        [DataMember]
        public string ThingsToBring
        {
            get { return thingsToBring; }
            set { thingsToBring = value; }
        }
        [DataMember]
        public double TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }
        [DataMember]
        public string[] ThemeNames
        {
            get { return themeNames; }
            set { themeNames = value; }
        }
        [DataMember]
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        [DataMember]
        public string Inclusions
        {
            get { return inclusions; }
            set { inclusions = value; }
        }
        public static ActivitySearchResults[] GetSearchResults(ActivitySearchRequest activitySearchRequest, int agencyId)
        {
            ActivitySearchResults[] activitySearchResults = null;
            string activityImagesFolderPath = ConfigurationManager.AppSettings["ActivityImagesFolder"];
            try
            {
                SqlParameter[] paramList = new SqlParameter[11];
                if (!string.IsNullOrEmpty(activitySearchRequest.ActivityName)) paramList[0] = new SqlParameter("@ActivityName", activitySearchRequest.ActivityName);
                if (activitySearchRequest.Type != null && activitySearchRequest.Type.Length > 0)
                {
                    if (!string.IsNullOrEmpty(activitySearchRequest.Type[0]))
                    {
                        paramList[1] = new SqlParameter("@Type", activitySearchRequest.Type[0]);
                    }
                    else
                    {
                        paramList[1] = new SqlParameter("@Type", null);
                    }
                }
                else
                {
                    paramList[1] = new SqlParameter("@Type", null);
                }
                paramList[2] = new SqlParameter("@CityCode", activitySearchRequest.CityCode);
                paramList[3] = new SqlParameter("@CountryCode", activitySearchRequest.CountryCode);
                paramList[4] = new SqlParameter("@FromDate", DateTime.ParseExact(activitySearchRequest.FromDate, "yyyy-MM-dd", null));
                paramList[5] = new SqlParameter("@ToDate", DateTime.ParseExact(activitySearchRequest.ToDate, "yyyy-MM-dd", null));
                paramList[6] = new SqlParameter("@AdultCount", activitySearchRequest.AdultCount);
                paramList[7] = new SqlParameter("@ChildCount", activitySearchRequest.ChildCount);
                paramList[8] = new SqlParameter("@InfantCount", activitySearchRequest.InfantCount);
                paramList[9] = new SqlParameter("@agencyId", agencyId);
                if (activitySearchRequest.Category != null && activitySearchRequest.Category.Length > 0)
                {
                    if (!string.IsNullOrEmpty(activitySearchRequest.Category[0]))
                    {
                        paramList[10] = new SqlParameter("@Category", activitySearchRequest.Category[0]);
                    }
                    else
                    {
                        paramList[10] = new SqlParameter("@Category", null);
                    }
                }
                else
                {
                    paramList[10] = new SqlParameter("@Category", null);
                }


                DataSet ds = DBGateway.ExecuteQuery("Act_Service_SearchActivity", paramList);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {

                    List<ActivitySearchResults> lstActivitySearchResults = new List<ActivitySearchResults>();
                    
                    Dictionary<int, string> themeDetails = new Dictionary<int, string>();

                    //Getting the default ThemeNames from the Database which are related to Activity.
                    DataTable dt = GetThemes("A");
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            themeDetails.Add(Utility.ToInteger(dr["themeId"]), Utility.ToString(dr["themeName"]));
                        }
                    }
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ActivitySearchResults activityResult = new ActivitySearchResults();

                        activityResult.ResultIndex = Utility.ToInteger(dr["activityId"]);
                        activityResult.ActivityName = Utility.ToString(dr["activityName"]);
                        activityResult.ActivityCityCode = Utility.ToString(dr["activityCity"]);
                        activityResult.ImagePath1 = activityImagesFolderPath + Utility.ToString(dr["imagePath1"]);
                        activityResult.ImagePath2 = activityImagesFolderPath + Utility.ToString(dr["imagePath2"]);
                        activityResult.ImagePath3 = activityImagesFolderPath + Utility.ToString(dr["imagePath3"]);
                        if (dr["Currency"] != DBNull.Value)
                        {
                            activityResult.Currency = Utility.ToString(dr["Currency"]);
                        }

                        //Now from the default ThemeNames we are matching the ThemeId's and getting the ThemeNames for that Particular Activity.
                        string[] themeIDs = Utility.ToString(dr["themeId"]).Split(',');
                        List<string> themeNames = new List<string>();
                        for (int i = 0; i < themeIDs.Length; i++)
                        {
                            if (themeDetails.ContainsKey(Convert.ToInt32(themeIDs[i])))
                            {
                                themeNames.Add(themeDetails[Convert.ToInt32(themeIDs[i])]);
                            }
                        }

                        activityResult.ThemeNames = themeNames.ToArray();
                        activityResult.Overview = Utility.ToString(dr["overview"]);
                        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                        activityResult.DurationHours = Utility.ToString(dr["durationHours"]);
                        activityResult.UnavailableDates = Utility.ToString(dr["unavailableDates"]).Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);

                        if (dr["transferIncluded"] != DBNull.Value)
                        {
                            if ((Utility.ToString(dr["transferIncluded"]).ToUpper() == "YES" || (Utility.ToString(dr["transferIncluded"]).ToUpper() != "NO" && Utility.ToString(dr["transferIncluded"]).ToUpper() != "NA")) && dr["pickupLocation"] != DBNull.Value && !string.IsNullOrEmpty(dr["pickupLocation"].ToString()))
                            {
                                activityResult.TransferIncluded = true;
                                if (dr["pickupLocation"] != DBNull.Value && dr["pickupDate"] != DBNull.Value)
                                {
                                    DateTime pickupTime = Utility.ToDate(dr["pickupDate"]);
                                    string pickupLocationTime = Utility.ToString(dr["pickupLocation"]);//+ "," + pickupTime.ToString("hh:mm tt")
                                    activityResult.PickupLocation = pickupLocationTime;
                                }
                                else if (dr["pickupLocation"] != DBNull.Value)
                                {
                                    activityResult.PickupLocation = Utility.ToString(dr["pickupLocation"]);
                                }
                            }
                            else
                            {
                                activityResult.TransferIncluded = false;
                                activityResult.PickupLocation = null;
                            }
                        }
                        else
                        {
                            activityResult.TransferIncluded = false;
                            activityResult.PickupLocation = null;
                        }

                        activityResult.CancellationPolicy = Utility.ToString(dr["cancellPolicy"]);
                        activityResult.ThingsToBring = Utility.ToString(dr["thingsToBring"]);
                        activityResult.Inclusions = Utility.ToString(dr["inclusions"]);
                        List<ActivityPriceDetails> lstActivityPriceDetails = new List<ActivityPriceDetails>();
                        foreach (DataRow priceDetails in ds.Tables[1].Rows)
                        {
                            if (activityResult.ResultIndex == Utility.ToInteger(priceDetails["priceActivityId"]))
                            {
                                ActivityPriceDetails activityPriceDetails = new ActivityPriceDetails();
                                activityPriceDetails.PassengerType = Utility.ToString(priceDetails["label"]);
                                activityPriceDetails.Amount = Utility.ToDouble(priceDetails["amount"]);
                                lstActivityPriceDetails.Add(activityPriceDetails);
                            }
                        }
                        Double totalAmount = 0;
                        for (int i = 0; i < lstActivityPriceDetails.Count; i++)
                        {
                            if (lstActivityPriceDetails[i].PassengerType.Trim().ToLower() == "adult")
                            {
                                totalAmount += lstActivityPriceDetails[i].Amount * activitySearchRequest.AdultCount;
                            }
                            if (lstActivityPriceDetails[i].PassengerType.Trim().ToLower() == "child")
                            {
                                totalAmount += lstActivityPriceDetails[i].Amount * activitySearchRequest.ChildCount;
                            }
                            if (lstActivityPriceDetails[i].PassengerType.Trim().ToLower() == "infant")
                            {
                                totalAmount += lstActivityPriceDetails[i].Amount * activitySearchRequest.InfantCount;
                            }
                        }
                        activityResult.TotalAmount = totalAmount;

                        lstActivitySearchResults.Add(activityResult);
                    }
                    activitySearchResults = lstActivitySearchResults.ToArray();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.Normal, 0, "(ActivityService):" + ex.ToString(), activitySearchRequest.UserIPAddress);//, Convert.ToInt32(CT.BookingEngine.ProductType.Activity), "ActivityService", "Cozmo", String.Empty, "TokenId:" + activitySearchRequest.TokenID
            }
            return activitySearchResults;
        }

        public static string GetTripID(int ATHID, int userId, string tokenID, string userIPAddress)
        {
            string tripId = string.Empty;
            try
            {
                if (ATHID > 0)
                {
                    tripId = Convert.ToString(DBGateway.ExecuteScalar("exec Act_Service_GetTripId " + ATHID));
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, userId, "(ActivityService): Failed to get TripId after booking the activity:" + ex.ToString(), userIPAddress);//, Convert.ToInt32(CT.BookingEngine.ProductType.Activity), "ActivityService", "Cozmo", string.Empty, "TokenId:" + tokenID
            }
            return tripId;
        }

        public static DataTable GetThemes(string themeType)
        {
            DataTable themesData = null;
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@themeType", themeType);
                themesData = DBGateway.FillDataTableSP("Act_Service_GetActivityThemeInfo", paramList);
            }
            catch 
            {
                throw new Exception();
            }
            return themesData;
        }
    }
}
