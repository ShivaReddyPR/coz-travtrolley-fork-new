﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivityFlexFieldsDetails
    {
        string flexFieldName;
        string flexFieldDetails;

        [DataMember]
        public string FlexFieldName
        {
            get { return flexFieldName; }
            set { flexFieldName = value; }
        }
        [DataMember]
        public string FlexFieldDetails
        {
            get { return flexFieldDetails; }
            set { flexFieldDetails = value; }
        }
    }
}