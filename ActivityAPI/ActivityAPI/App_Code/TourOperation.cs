﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class TourOperation
    {
        string itemCode;
        string cityCode;
        string depName;
        string fromTime;
        string toTime;
        DateTime fromDate;
        DateTime toDate;

        [DataMember]
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        [DataMember]
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        [DataMember]
        public string DepName
        {
            get { return depName; }
            set { depName = value; }
        }
        [DataMember]
        public string FromTime
        {
            get { return fromTime; }
            set { fromTime = value; }
        }
        [DataMember]
        public string ToTime
        {
            get { return toTime; }
            set { toTime = value; }
        }
        [DataMember]
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        [DataMember]
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
    }
}
