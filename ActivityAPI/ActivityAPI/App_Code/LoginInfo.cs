﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    public class LoginInfo
    {
        #region Member Variables
        private const string LOGIN_SESSION = "_LoginInfo";
        private long _userID;
        private string _firstName;
        private string _fullName;
        private string _lastName;
        private string _loginName;
        private string _email;
        private MemberType _memberType;
        private long _locationID;
        private string _locationName;
        private int _companyId;
        private string _currency;
        private decimal _exchangeRate;

        private string _locationAddress;
        private string _locationTerms;
        private string _contactDetails;

        private int _agentId;
        private string _agentName;
        private decimal _agentBalance;
        private string _agentPhone;
        private string _agentEmail;
        private string _agentGrntrStatus;
        private string _agentVisaSubmissionStatus;
        private string _agentLogoPath;
        private string _agentProduct;
        private int _decimalValue;
        private AgentType _agentType;
        private Dictionary<string, SourceDetails> _sourceCredentials;
        private Dictionary<string, decimal> _agentExchangeRates;
        private bool _isOnBehalf;
        private string _agentTheme;
        private string _copyRight;
        /// <summary>
        /// Original Logged in Agent.
        /// </summary>
        private int _onBehalfAgentId;
        private string _onBehalfAgentCurrency;
        private int _onBehalfAgentDecimalValue;
        private Dictionary<string, decimal> _onBehalfAgentExchangeRates;
        private Dictionary<string, SourceDetails> _onBehalfAgentSourceCredentials;
        private string _transType;
        //private int _corporateProfileId = 0;
        //string _isCorporate = "N";
        //string _corporateProfileGrade = string.Empty;


        #endregion

        #region Properties
        public long UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }
        public string FullName
        {
            get
            {
                _fullName = _firstName + " " + _lastName;
                return _fullName;
            }
            //set { _fullName = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LocationName
        {
            get { return _locationName; }
            set { _locationName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string LoginName
        {
            get { return _loginName; }
            set { _loginName = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public MemberType MemberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }
        public long LocationID
        {
            get { return _locationID; }
            set { _locationID = value; }
        }
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        public decimal ExchangeRate
        {
            get { return _exchangeRate; }
            set { _exchangeRate = value; }
        }
        public int CompanyID
        {
            get { return _companyId; }
            set { _companyId = value; }
        }

        public string LocationAddress
        {
            get { return _locationAddress; }
            set { _locationAddress = value; }
        }
        public string LocationTerms
        {
            get { return _locationTerms; }
            set { _locationTerms = value; }
        }

        public string ContactDetails
        {
            get { return _contactDetails; }
            set { _contactDetails = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public string AgentName
        {
            get { return _agentName; }
            set { _agentName = value; }
        }
        public decimal AgentBalance
        {
            get { return _agentBalance; }
            set { _agentBalance = value; }
        }
        public string AgentPhone
        {
            get { return _agentPhone; }
            set { _agentPhone = value; }
        }
        public string AgentEmail
        {
            get { return _agentEmail; }
            set { _agentEmail = value; }
        }

        public String AgentGrntrStatus
        {
            get { return _agentGrntrStatus; }
            set { value = _agentGrntrStatus; }
        }

        public string AgentVisaSubmissionStatus
        {
            get { return _agentVisaSubmissionStatus; }
            set { value = _agentVisaSubmissionStatus; }

        }
        public string AgentLogoPath
        {
            get { return _agentLogoPath; }
            set { value = _agentLogoPath; }

        }

        public string AgentProduct
        {
            get { return _agentProduct; }
            set { value = _agentProduct; }

        }
        public int DecimalValue
        {
            get { return _decimalValue; }
            set { _decimalValue = value; }
        }
        public AgentType AgentType
        {

            get { return _agentType; }
            set { _agentType = value; }

        }
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }


        /// <summary>
        /// Source wise Credential store.
        /// </summary>
        public Dictionary<string, SourceDetails> AgentSourceCredentials
        {
            get { return _sourceCredentials; }
            set { _sourceCredentials = value; }
        }

        /// <summary>
        /// Exchanges rates against Agent base currency.
        /// </summary>
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return _agentExchangeRates; }
            set { _agentExchangeRates = value; }
        }

        /// <summary>
        /// Set it 'True' when you are making On Behalf booking.                      
        /// Important: Please set 'False' on Hotel Search page_load method and set 'True' only when behalf Agent selection is made.
        /// <example>IsOnBehalfOfAgent=True|False</example>
        /// </summary>          
        public bool IsOnBehalfOfAgent
        {
            get { return _isOnBehalf; }
            set { _isOnBehalf = value; }
        }
        /// <summary>
        /// Original Logged in Agent.
        /// </summary>
        public int OnBehalfAgentID
        {
            get { return _onBehalfAgentId; }
            set { _onBehalfAgentId = value; }
        }

        public string OnBehalfAgentCurrency
        {
            get { return _onBehalfAgentCurrency; }
            set { _onBehalfAgentCurrency = value; }
        }

        public int OnBehalfAgentDecimalValue
        {
            get { return _onBehalfAgentDecimalValue; }
            set { _onBehalfAgentDecimalValue = value; }
        }

        public Dictionary<string, decimal> OnBehalfAgentExchangeRates
        {
            get { return _onBehalfAgentExchangeRates; }
            set { _onBehalfAgentExchangeRates = value; }
        }

        public Dictionary<string, SourceDetails> OnBehalfAgentSourceCredentials
        {
            get { return _onBehalfAgentSourceCredentials; }
            set { _onBehalfAgentSourceCredentials = value; }
        }
        public string AgentTheme
        {
            get { return _agentTheme; }
            set { _agentTheme = value; }
        }
        public string CopyRight
        {
            get { return _copyRight; }
            set { _copyRight = value; }
        }

        //public int CorporateProfileId
        //{
        //    get { return _corporateProfileId; }
        //    set { _corporateProfileId = value; }
        //}
        //public string IsCorporate
        //{
        //    get { return _isCorporate; }
        //    set { _isCorporate = value; }
        //}
        //public string CorporateProfileGrade
        //{
        //    get { return _corporateProfileGrade; }
        //    set { _corporateProfileGrade = value; }
        //}

        #endregion

        #region Constructor
        public LoginInfo(DataRow dr)
        {
            try
            {
                _userID = Utility.ToLong(dr["USER_ID"]);
                _firstName = Utility.ToString(dr["USER_FIRST_NAME"]);
                _lastName = Utility.ToString(dr["USER_LAST_NAME"]);

                _loginName = Utility.ToString(dr["USER_LOGIN_NAME"]);
                _locationID = Utility.ToLong(dr["USER_LOCATION_ID"]);
                _locationName = Utility.ToString(dr["LOCATION_NAME"]);
                _locationAddress = Utility.ToString(dr["LOCATION_ADDRESS"]);
                _contactDetails = Utility.ToString(dr["location_contact_details"]);
                _locationTerms = Utility.ToString(dr["LOCATION_TERMS"]);
                _companyId = Utility.ToInteger(dr["USER_COMPANY_ID"]);
                _email = Utility.ToString(dr["USER_EMAIL"]);
                string type = Utility.ToString(dr["USER_MEMBER_TYPE"]);
                //_currency = Utility.ToString(dr["Location_currency"]);
                _currency = Utility.ToString(dr["agent_currency"]);

                //_exchangeRate = Utility.ToDecimal(dr["EX_RATE_RATE"]);
                _agentId = Utility.ToInteger(dr["USER_AGENT_ID"]);
                _agentName = Utility.ToString(dr["USER_AGENT_NAME"]);
                _agentBalance = Utility.ToDecimal(dr["AGENTBALANCE"]);
                _agentPhone = Utility.ToString(dr["AGENT_PHONE1"]);
                _agentEmail = Utility.ToString(dr["AGENT_EMAIL1"]);
                _agentGrntrStatus = Utility.ToString(dr["AGENT_GUARANTOR_STATUS"]); ;
                _agentVisaSubmissionStatus = Utility.ToString(dr["AGENT_VISA_SUBMISSION_STATUS"]);
                _agentProduct = Utility.ToString(dr["AGENT_PRODUCT"]);
                _agentLogoPath = Utility.ToString(dr["agent_img_filename"]);
                _agentType = (AgentType)Utility.ToInteger(dr["AGENT_TYPE"]);
                _decimalValue = Utility.ToInteger(dr["AGENT_DECIMAL"]);
                _agentTheme = Utility.ToString(dr["AGENT_THEME"]);
                _copyRight = Utility.ToString(dr["AGENT_COPY_RIGHT"]);

                if (type == MemberType.SUPER.ToString())
                {
                    _memberType = MemberType.SUPER;
                }
                else if (type == MemberType.ADMIN.ToString())
                {
                    _memberType = MemberType.ADMIN;
                }
                else if (type == MemberType.CASHIER.ToString())
                {
                    _memberType = MemberType.CASHIER;
                }
                else if (type == MemberType.OPERATIONS.ToString())
                {
                    _memberType = MemberType.OPERATIONS;
                }
                else if (type == MemberType.BACKOFFICE.ToString())
                {
                    _memberType = MemberType.BACKOFFICE;
                }
                else if (type == MemberType.SUPERVISOR.ToString())
                {
                    _memberType = MemberType.SUPERVISOR;
                }
                _transType = Utility.ToString(dr["user_TransType"]);
                //_corporateProfileId = Utility.ToInteger(dr["user_corp_profile_id"]); ;
                //_isCorporate = Utility.ToString(dr["agent_corporate"]); ;

            }
            catch
            {
                throw;
            }
        }

        public LoginInfo()
        {
        }
        #endregion
    }

    public struct SourceDetails
    {
        /// <summary>
        /// Source UserID
        /// </summary>
        public string UserID;
        /// <summary>
        ///  Source Password
        /// </summary>
        public string Password;
        /// <summary>
        /// Airline HAP
        /// </summary>
        public string HAP;
        /// <summary>
        /// Airline PCC
        /// </summary>
        public string PCC;
        /// <summary>
        /// AgentCode/Company Code used in Hotel Source.
        /// </summary>
        public string AgentCode;
        /// <summary>
        /// Type Added to differnatiate Sources which are having the same name in different product, to updateSource Credentilas for the selected products,
        /// </summary>
        public int Type; // Added by  Ravi
    }

    public enum AgentType
    {
        BaseAgent = 1,
        Agent = 2,
        B2B = 3,
        B2B2B = 4
    }

    public class AgentMaster
    {
        public static decimal UpdateAgentBalance(int agentId, decimal transactionAmount, int createdBy)
        {
            try
            {
                decimal currentBalance = 0;
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("p_agent_id", agentId);
                paramList[1] = new SqlParameter("p_trans_amount", transactionAmount);
                paramList[2] = new SqlParameter("p_modified_by", createdBy);
                paramList[3] = new SqlParameter("p_current_balance", SqlDbType.Money);
                paramList[3].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("Act_Service_agent_balance_update", paramList);
                currentBalance = Utility.ToDecimal(paramList[3].Value);
                return currentBalance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}