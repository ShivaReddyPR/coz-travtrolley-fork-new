﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivityCancellationResponse
    {
        Error error;
        string responseTime;
        CancellationDetails cancelDetails;

        [DataMember]
        public Error Error
        {
            get { return error; }
            set { error = value; }
        }
        [DataMember]
        public string ResponseTime
        {
            get { return responseTime; }
            set { responseTime = value; }
        }
        [DataMember]
        public CancellationDetails CancellationDetails
        {
            get { return cancelDetails; }
            set { cancelDetails = value; }
        }

    }
}