﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ActivityAPI
{
    [DataContract]
    public class ActivityBookingRequest
    {
        string tokenId;
        string userIPAddress;
        int resultIndex;
        int adultCount;
        int childCount;
        int infantCount;
        string passengerFirstName;
        string passengerLastName;
        string emailAddress;
        string nationality;
        string phoneCountryCode;
        string phoneNumber;
        string journeyDate;
        decimal totalAmount;
        ActivityFlexFieldsDetails[] activityFlexDetails;
        string transType;
        string departurePoint;
        DateTime departureTime;
        FlightDetails flightInfo;

        [DataMember]
        public string TokenID
        {
            get { return tokenId; }
            set { tokenId = value; }
        }
        [DataMember]
        public string UserIPAddress
        {
            get { return userIPAddress; }
            set { userIPAddress = value; }
        }
        [DataMember]
        public int ResultIndex
        {
            get { return resultIndex; }
            set { resultIndex = value; }
        }
        [DataMember]
        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }
        [DataMember]
        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }
        [DataMember]
        public int InfantCount
        {
            get { return infantCount; }
            set { infantCount = value; }
        }
        [DataMember]
        public string PassengerFirstName
        {
            get { return passengerFirstName; }
            set { passengerFirstName = value; }
        }
        [DataMember]
        public string PassengerLastName
        {
            get { return passengerLastName; }
            set { passengerLastName = value; }
        }
        [DataMember]
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }
        [DataMember]
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        [DataMember]
        public string PhoneCountryCode
        {
            get { return phoneCountryCode; }
            set { phoneCountryCode = value; }
        }
        [DataMember]
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }
        [DataMember]
        public ActivityFlexFieldsDetails[] ActivityFlexDetails
        {
            get { return activityFlexDetails; }
            set { activityFlexDetails = value; }
        }
        [DataMember]
        public string JourneyDate
        {
            get { return journeyDate; }
            set { journeyDate = value; }
        }
        [DataMember]
        public decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }
         [DataMember]
        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }
         [DataMember]
         public string DeparturePoint
         {
             get { return departurePoint; }
             set { departurePoint = value; }
         }
         [DataMember]
         public DateTime DepartureTime
         {
             get { return departureTime; }
             set { departureTime = value; }
         }
        [DataMember]
         public FlightDetails FlightInfo
         {
             get { return flightInfo; }
             set { flightInfo = value; }
         }
    }
    [DataContract]
    public class FlightDetails
    {
        #region variables
        string pnr;
        string fligtNo;
        string destination;
        DateTime arrivaltime;
        int childCount;
        string childAges;
        string hotelName;
        string origin;
        DateTime departureTime;
        string deptFligtNo;
        #endregion
        #region Properities
        [DataMember]
        public string PNR
        {
            get { return pnr; }
            set { pnr = value; }
        }
        [DataMember]
        public string FligtNo
        {
            get { return fligtNo; }
            set { fligtNo = value; }
        }
        [DataMember]
        public string Destination
        {
            get { return destination; }
            set { destination = value; }
        }
        [DataMember]
        public DateTime Arrivaltime
        {
            get { return arrivaltime; }
            set { arrivaltime = value; }
        }
        [DataMember]
        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }
        [DataMember]
        public string ChildAges
        {
            get { return childAges; }
            set { childAges = value; }
        }
        [DataMember]
        public string HotelName
        {
            get { return hotelName; }
            set { hotelName = value; }
        }
        [DataMember]
        public string Origin
        {
            get { return origin; }
            set { origin = value; }
        }
        [DataMember]
        public DateTime DepartureTime
        {
            get { return departureTime; }
            set { departureTime = value; }
        }
        [DataMember]
        public string DeptFligtNo
        {
            get { return deptFligtNo; }
            set { deptFligtNo = value; }
        }
        #endregion
    }
}
