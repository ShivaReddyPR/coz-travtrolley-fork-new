﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    public class ActivityTheme
    {
        public ActivityTheme()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ActivityTheme(string code)
        {
            Load(code);
        }

        int themeId;
        string themeCode;
        string themeName;
        string themeStatus;
        int createdBy;
        DateTime createdOn;
        int modifiedBy;
        DateTime modifiedOn;

        public int ThemeId
        {
            get { return themeId; }
            set { themeId = value; }
        }
        public string ThemeCode
        {
            get { return themeCode; }
            set { themeCode = value; }
        }
        public string ThemeName
        {
            get { return themeName; }
            set { themeName = value; }
        }
        public string ThemeStatus
        {
            get { return themeStatus; }
            set { themeStatus = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }

        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }

        void Load(string code)
        {
            try
            {
                using (SqlConnection connection = DBGateway.GetConnection())
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    //paramList[0] = new SqlParameter("@themeCode", code);
                    SqlDataReader reader = DBGateway.ExecuteReaderSP("Act_Service_GetTheme", paramList, connection);

                    while (reader.Read())
                    {
                        themeId = Convert.ToInt32(reader["themeId"]);
                        themeCode = reader["themeCode"].ToString();
                        themeName = reader["themeName"].ToString();
                        themeStatus = reader["themeStatus"].ToString();
                        createdBy = Convert.ToInt32(reader["createdBy"]);
                        createdOn = Convert.ToDateTime(reader["createdOn"]);
                        modifiedBy = Convert.ToInt32(reader["modifiedBy"]);
                        modifiedOn = Convert.ToDateTime(reader["modifiedOn"]);
                    }
                }
            }
            catch
            { }
        }
    }
}