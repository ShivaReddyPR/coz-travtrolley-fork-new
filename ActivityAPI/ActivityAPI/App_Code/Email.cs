﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace ActivityAPI
{
    public class Email
    {

        /// <summary>
        /// Send mail with bcc option
        /// </summary>
        /// <param name="from"></param>
        /// <param name="replyTo"></param>
        /// <param name="toArray"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="varMapping"></param>
        public static void Send(string from, string replyTo, List<string> toArray, string subject, string message, Hashtable varMapping, string bcc)
        {
            string commaSeperatedValues = string.Empty;
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            mail.Subject = subject + (System.Configuration.ConfigurationManager.AppSettings["TestMode"].Equals("True") ? "(Test)" : string.Empty);

            foreach (string s in toArray)
            {
                mail.To.Add(s);
            }
            mail.ReplyTo = new MailAddress(replyTo);
            mail.From = new MailAddress(from, "Cozmo Travel");
            mail.IsBodyHtml = true;
            //These values are taken from a new config file HostPort.config
            //ConfigurationSystem con = new ConfigurationSystem();
            //Hashtable hostPort = con.GetHostPort();
            //smtp.Host = hostPort["Host"].ToString();
            //smtp.Port = Convert.ToInt32(hostPort["Port"]);
            //smtp.EnableSsl = true;// for gmail
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            //smtp.Port = Convert.ToInt32(hostPort["Port"]);
            smtp.Credentials = new NetworkCredential("brahmam.naidu98@gmail.com", "Mara9949");
            smtp.EnableSsl = true;
            // mail.Bcc.Add(hostPort["Bcc"].ToString());
            mail.Bcc.Add(bcc);
            if (varMapping != null)
            {
                foreach (DictionaryEntry de in varMapping)
                {
                    string keyString = "%" + de.Key.ToString() + "%";
                    if (de.Value != null)
                    {
                        message = message.Replace(keyString, de.Value.ToString());
                    }
                    else
                    {
                        message = message.Replace(keyString, " ");
                    }
                }
            }
            message = message.Replace("\\n", "<br/>");
            mail.Body = message;
            try
            {
                smtp.Send(mail);
            }
            catch (SmtpException ex)
            {
                throw new SmtpException("Smtp is unable to send the message" + ex.Message);
            }
        }
    }
}