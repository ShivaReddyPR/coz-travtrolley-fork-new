﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CT.TicketReceipt.DataAccessLayer;

namespace ActivityAPI
{
    public class ActivityDetails
    {
        #region Variables

        long id;
        string name;
        int days;
        int stockInHand;
        int stockUsed;
        int agencyId;
        decimal startingFrom;
        string city;
        string country;
        string imagePath1;
        string imagePath2;
        string imagePath3;
        ActivityTheme theme;
        string introduction;
        string overview;
        DateTime startFrom;
        DateTime endTo;
        string durationHours;
        DateTime availableFrom;
        DateTime availableTo;
        List<DateTime> unavailableDates;
        string itinerary1;
        string itinerary2;
        string details;
        string supplierName;
        string supplierEmail;
        string mealsIncluded;
        string transferIncluded;
        string pickupLocation;
        DateTime pickupDate;
        string dropoffLocation;
        DateTime dropoffDate;
        List<string> exclusions;
        List<string> inclusions;
        string cancelPolicy;
        string thingsToBring;
        int unavailableDays;
        int bookingCutOff;
        string status;
        int createdBy;
        DateTime createdOn;
        int modifiedBy;
        DateTime modifiedOn;

        DataTable dtFlexMaster;
        DataTable dtPriceDetails;
        DataTable dtTransactionHeader;
        DataTable dtTransactionDetail;
        DataTable dtTransactionPrice;
        DataTable dtFlexDetails;
        DataTable dtFDPaymentDetails;
        DataTable dtFDItineraryDetails;
        DataTable dtinitPriceDetails;
        DataTable dtFDVisaFollowup;
        decimal agentBalance;

        public static Dictionary<string, decimal> ExchangeRates;
        public static string AgentCurrency;
        public static long AgentId;
        string isFixedDeparture;
        string discountCode;
        decimal discount;
        string agentEmail;
        #endregion

        #region Properties

        public long Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int Days
        {
            get { return days; }
            set { days = value; }
        }
        public int StockInHand
        {
            get { return stockInHand; }
            set { stockInHand = value; }
        }
        public int StockUsed
        {
            get { return stockUsed; }
            set { stockUsed = value; }
        }
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }
        public decimal StartingFrom
        {
            get { return startingFrom; }
            set { startingFrom = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string ImagePath1
        {
            get { return imagePath1; }
            set { imagePath1 = value; }
        }
        public string ImagePath2
        {
            get { return imagePath2; }
            set { imagePath2 = value; }
        }
        public string ImagePath3
        {
            get { return imagePath3; }
            set { imagePath3 = value; }
        }

        public ActivityTheme Theme
        {
            get { return theme; }
            set { theme = value; }
        }
        public string Introduction
        {
            get { return introduction; }
            set { introduction = value; }
        }
        public string Overview
        {
            get { return overview; }
            set { overview = value; }
        }
        public DateTime StartFrom
        {
            get { return startFrom; }
            set { startFrom = value; }
        }
        public DateTime EndTo
        {
            get { return endTo; }
            set { endTo = value; }
        }
        public string DurationHours
        {
            get { return durationHours; }
            set { durationHours = value; }
        }
        public DateTime AvailableFrom
        {
            get { return availableFrom; }
            set { availableFrom = value; }
        }
        public DateTime AvailableTo
        {
            get { return availableTo; }
            set { availableTo = value; }
        }
        public List<DateTime> UnavailableDates
        {
            get { return unavailableDates; }
            set { unavailableDates = value; }
        }
        public string Itinerary1
        {
            get { return itinerary1; }
            set { itinerary1 = value; }
        }
        public string Itinerary2
        {
            get { return itinerary2; }
            set { itinerary2 = value; }
        }
        public string Details
        {
            get { return details; }
            set { details = value; }
        }
        public string SupplierName
        {
            get { return supplierName; }
            set { supplierName = value; }
        }
        public string SupplierEmail
        {
            get { return supplierEmail; }
            set { supplierEmail = value; }
        }
        public string MealsIncluded
        {
            get { return mealsIncluded; }
            set { mealsIncluded = value; }
        }
        public string TransferIncluded
        {
            get { return transferIncluded; }
            set { transferIncluded = value; }
        }
        public string PickupLocation
        {
            get { return pickupLocation; }
            set { pickupLocation = value; }
        }
        public DateTime PickupDate
        {
            get { return pickupDate; }
            set { pickupDate = value; }
        }
        public string DropoffLocation
        {
            get { return dropoffLocation; }
            set { dropoffLocation = value; }
        }
        public DateTime DropoffDate
        {
            get { return dropoffDate; }
            set { dropoffDate = value; }
        }
        public List<string> Exclusions
        {
            get { return exclusions; }
            set { exclusions = value; }
        }
        public List<string> Inclusions
        {
            get { return inclusions; }
            set { inclusions = value; }
        }
        public string CancelPolicy
        {
            get { return cancelPolicy; }
            set { cancelPolicy = value; }
        }
        public string ThingsToBring
        {
            get { return thingsToBring; }
            set { thingsToBring = value; }
        }
        public int UnavailableDays
        {
            get { return unavailableDays; }
            set { unavailableDays = value; }
        }
        public int BookingCutOff
        {
            get { return bookingCutOff; }
            set { bookingCutOff = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }
        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }

        public DataTable FlexMaster
        {
            get { return dtFlexMaster; }
            set { dtFlexMaster = value; }
        }

        public DataTable PriceDetails
        {
            get { return dtPriceDetails; }
            set { dtPriceDetails = value; }
        }

        public DataTable TransactionHeader
        {
            get { return dtTransactionHeader; }
            set { dtTransactionHeader = value; }
        }
        public DataTable TransactionDetail
        {
            get { return dtTransactionDetail; }
            set { dtTransactionDetail = value; }
        }

        public DataTable TransactionPrice
        {
            get { return dtTransactionPrice; }
            set { dtTransactionPrice = value; }
        }

        public DataTable FlexDetails
        {
            get { return dtFlexDetails; }
            set { FlexDetails = value; }
        }

        public decimal AgentBalance
        {
            get { return agentBalance; }
            set { agentBalance = value; }
        }
        public string IsFixedDeparture
        {
            get { return isFixedDeparture; }
            set { isFixedDeparture = value; }
        }

        public DataTable FixedDepartureDetails
        {
            get { return dtFDPaymentDetails; }
            set { dtFDPaymentDetails = value; }
        }
        public DataTable FixedItineraryDetails
        {
            get { return dtFDItineraryDetails; }
            set { dtFDItineraryDetails = value; }
        }
        public DataTable FixedinitPriceDetails
        {
            get { return dtinitPriceDetails; }
            set { dtinitPriceDetails = value; }
        }
        public DataTable FixedVisaFollowup
        {
            get { return dtFDVisaFollowup; }
            set { dtFDVisaFollowup = value; }
        }
        public string DiscountCode
        {
            get { return discountCode; }
            set { discountCode = value; }
        }
        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        public string AgentEmail
        {
            get { return agentEmail; }
            set { agentEmail = value; }
        }
        #endregion

        public ActivityDetails()
        { }

        public ActivityDetails(long id)
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            DataSet ds = new DataSet();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@ActivityId", id);
                    SqlDataReader reader = DBGateway.ExecuteReaderSP("Act_Service_GetActivityById", paramList, connection);

                    while (reader.Read())
                    {
                        this.id = id;
                        if (reader["activityName"] != DBNull.Value)
                        {
                            name = reader["activityName"].ToString();
                        }
                        if (reader["activityDays"] != DBNull.Value)
                        {
                            days = Convert.ToInt32(reader["activityDays"]);
                        }
                        if (reader["stockInHand"] != DBNull.Value)
                        {
                            stockInHand = Convert.ToInt32(reader["stockInHand"]);
                        }
                        if (reader["stockUsed"] != DBNull.Value)
                        {
                            stockUsed = Convert.ToInt32(reader["stockUsed"]);
                        }
                        if (reader["agencyId"] != DBNull.Value)
                        {
                            agencyId = Convert.ToInt32(reader["agencyId"]);
                        }
                        if (reader["isFixedDeparture"] != DBNull.Value)
                        {
                            isFixedDeparture = Convert.ToString(reader["isFixedDeparture"]);
                        }
                        if (reader["activityStartingFrom"] != DBNull.Value)
                        {
                            startingFrom = Convert.ToDecimal(reader["activityStartingFrom"]);
                        }
                        if (reader["ActivityCity"] != DBNull.Value)
                        {
                            city = reader["ActivityCity"].ToString();
                        }
                        if (reader["activityCountry"] != DBNull.Value)
                        {
                            country = reader["activityCountry"].ToString();
                        }
                        if (reader["imagePath1"] != DBNull.Value)
                        {
                            imagePath1 = reader["imagePath1"].ToString();
                        }
                        if (reader["imagePath2"] != DBNull.Value)
                        {
                            imagePath2 = reader["imagePath2"].ToString();
                        }
                        if (reader["imagePath3"] != DBNull.Value)
                        {
                            imagePath3 = reader["imagePath3"].ToString();
                        }
                        //if (reader["themeId"] != DBNull.Value)
                        //{
                        //    theme = new ActivityTheme(reader["themeId"].ToString());
                        //}
                        if (reader["introduction"] != DBNull.Value)
                        {
                            introduction = reader["introduction"].ToString();
                        }
                        if (reader["overview"] != DBNull.Value)
                        {
                            overview = reader["overview"].ToString();
                        }
                        if (reader["startFrom"] != DBNull.Value)
                        {
                            startFrom = Convert.ToDateTime(reader["startFrom"]);
                        }
                        if (reader["endTo"] != DBNull.Value)
                        {
                            endTo = Convert.ToDateTime(reader["endTo"]);
                        }
                        if (reader["durationHours"] != DBNull.Value)
                        {
                            durationHours = Convert.ToString(reader["durationHours"]);
                        }
                        if (reader["availableFrom"] != DBNull.Value)
                        {
                            availableFrom = Convert.ToDateTime(reader["availableFrom"]);
                        }
                        if (reader["availableTo"] != DBNull.Value)
                        {
                            availableTo = Convert.ToDateTime(reader["availableTo"]);
                        }
                        unavailableDates = new List<DateTime>();
                        if (reader["unavailableDates"] != DBNull.Value)
                        {
                            try
                            {
                                //string[] dates = reader["unavailableDates"].ToString().Split(',');
                                string[] dates = reader["unavailableDates"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string date in dates)
                                {
                                    unavailableDates.Add(Convert.ToDateTime(date, dateFormat));
                                }
                            }
                            catch { }
                        }
                        if (reader["itinerary1"] != DBNull.Value)
                        {
                            itinerary1 = reader["itinerary1"].ToString();
                        }
                        if (reader["itinerary2"] != DBNull.Value)
                        {
                            itinerary2 = reader["itinerary2"].ToString();
                        }
                        if (reader["details"] != DBNull.Value)
                        {
                            details = reader["details"].ToString();
                        }
                        if (reader["supplierName"] != DBNull.Value)
                        {
                            supplierName = reader["supplierName"].ToString();
                        }
                        if (reader["supplierEmail"] != DBNull.Value)
                        {
                            supplierEmail = reader["supplierEmail"].ToString();
                        }
                        if (reader["mealsIncluded"] != DBNull.Value)
                        {
                            mealsIncluded = reader["mealsIncluded"].ToString();
                        }
                        if (reader["transferIncluded"] != DBNull.Value)
                        {
                            transferIncluded = reader["transferIncluded"].ToString();
                        }
                        if (reader["pickupLocation"] != DBNull.Value)
                        {
                            pickupLocation = reader["pickupLocation"].ToString();
                        }
                        if (reader["pickupDate"] != DBNull.Value)
                        {
                            pickupDate = Convert.ToDateTime(reader["pickupDate"]);
                        }
                        if (reader["dropOffLocation"] != DBNull.Value)
                        {
                            dropoffLocation = reader["dropOffLocation"].ToString();
                        }
                        if (reader["dropOffDate"] != DBNull.Value)
                        {
                            dropoffDate = Convert.ToDateTime(reader["dropOffDate"]);
                        }
                        if (reader["exclusions"] != DBNull.Value)
                        {
                            exclusions = new List<string>();
                            string[] list = reader["exclusions"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string data in list)
                            {
                                exclusions.Add(data);
                            }
                        }
                        if (reader["inclusions"] != DBNull.Value)
                        {
                            inclusions = new List<string>();
                            string[] list1 = reader["inclusions"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string data in list1)
                            {
                                inclusions.Add(data);
                            }
                        }

                        if (reader["cancellPolicy"] != DBNull.Value)
                        {
                            cancelPolicy = reader["cancellPolicy"].ToString();
                        }
                        if (reader["thingsToBring"] != DBNull.Value)
                        {
                            thingsToBring = reader["thingsToBring"].ToString();
                        }
                        if (reader["unavailableDays"] != DBNull.Value && !string.IsNullOrEmpty(reader["unavailableDays"].ToString()))
                        {
                            unavailableDays = Convert.ToInt32(reader["unavailableDays"]);
                        }
                        if (reader["bookingCutOff"] != DBNull.Value)
                        {
                            bookingCutOff = Convert.ToInt32(reader["bookingCutOff"]);
                        }
                        if (reader["activityStatus"] != DBNull.Value)
                        {
                            status = reader["activitystatus"].ToString();
                        }
                        if (reader["createdBy"] != DBNull.Value)
                        {
                            createdBy = Convert.ToInt32(reader["createdBy"]);
                        }
                        if (reader["createdOn"] != DBNull.Value)
                        {
                            createdOn = Convert.ToDateTime(reader["createdOn"]);
                        }
                        if (reader["modifiedBy"] != DBNull.Value)
                        {
                            modifiedBy = Convert.ToInt32(reader["modifiedBy"]);
                        }
                        if (reader["modifiedOn"] != DBNull.Value)
                        {
                            modifiedOn = Convert.ToDateTime(reader["modifiedOn"]);
                        }
                        if (reader["discountCode"] != DBNull.Value)
                        {
                            discountCode = Convert.ToString(reader["discountCode"]);
                        }
                        if (reader["discount"] != DBNull.Value)
                        {
                            discount = Convert.ToDecimal(reader["discount"]);
                        }
                        if (reader["agentEmail"] != DBNull.Value)
                        {
                            agentEmail = Convert.ToString(reader["agentEmail"]);
                        }

                        //Load the Flex details
                        SqlParameter[] paramListFlex = new SqlParameter[1];
                        paramListFlex[0] = new SqlParameter("@flexActivityId", id);
                        dtFlexMaster = DBGateway.FillDataTableSP("Act_Service_GetActivityFlexMaster", paramListFlex);

                        //Load the Price details
                        SqlParameter[] paramListPrice = new SqlParameter[1];
                        paramListPrice[0] = new SqlParameter("@priceActivityId", id);
                        dtPriceDetails = DBGateway.FillDataTableSP("Act_Service_GetActivityPrice", paramListPrice);

                        dtinitPriceDetails = dtPriceDetails;
                        //Load the Transaction Header
                        SqlParameter[] paramListHeader = new SqlParameter[1];
                        paramListHeader[0] = new SqlParameter("@ATHId", -1);
                        dtTransactionHeader = DBGateway.FillDataTableSP("Act_Service_GetActivityTransactionHeader", paramListHeader);

                        //Load the Transaction Detail

                        SqlParameter[] paramListDetail = new SqlParameter[1];
                        paramListDetail[0] = new SqlParameter("@athdId", -1);
                        dtTransactionDetail = DBGateway.FillDataTableSP("Act_Service_GetActivityTransactionDetail", paramListDetail);


                        //Load the Transaction Price Details
                        SqlParameter[] paramListTransPrice = new SqlParameter[1];
                        paramListTransPrice[0] = new SqlParameter("@ATHId", -1);
                        dtTransactionPrice = DBGateway.FillDataTableSP("Act_Service_GetActivityTransactionPrice", paramListTransPrice);

                        //Load the Flex Details
                        SqlParameter[] paramListFlexDetails = new SqlParameter[1];
                        paramListFlexDetails[0] = new SqlParameter("@ATHId", -1);
                        dtFlexDetails = DBGateway.FillDataTableSP("Act_Service_GetActivityFlexDetails", paramListFlexDetails);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), "");
                    throw ex;
                }
            }
        }

        public void SaveActivityTransaction()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                int totalPaxCount = 0;
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction();
                cmd.Transaction = trans;
                {

                    long headerId = 0;
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    if (dtTransactionHeader != null && dtTransactionHeader.Rows.Count > 0)
                    {
                        DataRow dr = dtTransactionHeader.Rows[0];

                        SqlParameter[] paramList = new SqlParameter[25];
                        {
                            paramList[0] = new SqlParameter("@ActivityId", Convert.ToInt32(dr["ActivityId"]));
                            paramList[1] = new SqlParameter("@TripId", "");
                            paramList[2] = new SqlParameter("@TransactionDate", DateTime.Now);
                            paramList[3] = new SqlParameter("@Adult", Convert.ToInt32(dr["Adult"]));
                            paramList[4] = new SqlParameter("@Child", Convert.ToInt32(dr["Child"]));
                            paramList[5] = new SqlParameter("@Infant", Convert.ToInt32(dr["Infant"]));
                            totalPaxCount = Convert.ToInt32(dr["Adult"])+Convert.ToInt32(dr["Child"])+Convert.ToInt32(dr["Infant"]);
                            paramList[6] = new SqlParameter("@Booking", Convert.ToDateTime(dr["Booking"], dateFormat));
                            paramList[7] = new SqlParameter("@TotalPrice", Convert.ToDecimal(dr["TotalPrice"]));
                            paramList[8] = new SqlParameter("@CreatedBy", Convert.ToInt32(dr["CreatedBy"]));
                            paramList[9] = new SqlParameter("@CreatedDate", DateTime.Now);
                            paramList[10] = new SqlParameter("@Status", dr["Status"].ToString());
                            paramList[11] = new SqlParameter("@AgencyId", Convert.ToInt32(dr["AgencyId"]));
                            paramList[12] = new SqlParameter("@TransactionType", dr["TransactionType"].ToString());
                            paramList[13] = new SqlParameter("@ATHId", headerId);
                            paramList[13].Direction = ParameterDirection.Output;
                            paramList[14] = new SqlParameter("@P_RET_AGENT_BALANCE", SqlDbType.Decimal, 0);
                            paramList[14].Direction = ParameterDirection.Output;
                            paramList[15] = new SqlParameter("@LocationId", Convert.ToInt32(dr["LocationId"]));
                            paramList[16] = new SqlParameter("@ActivityName", dr["ActivityName"].ToString());
                            //For setting Booking Ref no AD or FD 
                            paramList[17] = new SqlParameter("@IsFixedDeparture", dr["IsFixedDeparture"].ToString());
                            paramList[18] = new SqlParameter("@RoomCount", DBNull.Value);
                            paramList[19] = new SqlParameter("@QuotedStatus", DBNull.Value);
                            paramList[20] = new SqlParameter("@ATID", -1);
                            paramList[21] = new SqlParameter("@PaymentStatus", 0);

                            if (dr["CustomerId"] != DBNull.Value)
                            {
                                paramList[22] = new SqlParameter("@CustomerId", Convert.ToInt32(dr["CustomerId"].ToString()));
                            }
                            else
                            {
                                paramList[22] = new SqlParameter("@CustomerId", Convert.ToInt32(dr["CreatedBy"]));
                            }

                            if (!string.IsNullOrEmpty(dr["DeparturePoint"].ToString()))
                            {
                                paramList[23] = new SqlParameter("@DeparturePoint", dr["DeparturePoint"].ToString());
                            }
                            else {
                                paramList[23] = new SqlParameter("@DeparturePoint", DBNull.Value);
                            }
                            if (!string.IsNullOrEmpty(dr["DepartureTime"].ToString()))
                            {
                               // paramList[24] = new SqlParameter("@DepartureTime", dr["DepartureTime"].ToString());
                            }
                            else
                            {
                               // paramList[24] = new SqlParameter("@DepartureTime", DBNull.Value);
                            }
                            DBGateway.ExecuteNonQueryDetails(cmd, "Act_Service_AddActivityTransactionHeader", paramList);
                            headerId = Convert.ToInt64(paramList[13].Value);
                            if (isFixedDeparture == "N")
                            {
                                if (paramList[14].Value != DBNull.Value)
                                {
                                    agentBalance = Convert.ToDecimal(paramList[14].Value);
                                }
                            }
                            dtTransactionHeader.Rows[0]["ATHId"] = headerId;
                            id = Convert.ToInt32(dr["ActivityId"]);
                        }
                    }

                    //Save Transaction Detail
                    if (dtTransactionDetail.Rows.Count > 0 )
                    {
                        SqlParameter[] paramList = new SqlParameter[21];
                        foreach (DataRow dr in dtTransactionDetail.Rows)
                        {
                            dr["ATHDId"] = headerId;
                            paramList[0] = new SqlParameter("@FirstName", dr["FirstName"].ToString());
                            paramList[1] = new SqlParameter("@LastName", dr["LastName"].ToString());
                            paramList[2] = new SqlParameter("@Phone", dr["Phone"].ToString());
                            paramList[3] = new SqlParameter("@PhoneCountryCode", dr["PhoneCountryCode"].ToString());
                            paramList[4] = new SqlParameter("@Email", dr["Email"].ToString());
                            paramList[5] = new SqlParameter("@Nationality", dr["Nationality"].ToString());
                            paramList[6] = new SqlParameter("@PaxSerial", dr["PaxSerial"].ToString());
                            paramList[7] = new SqlParameter("@CreatedBy", dr["CreatedBy"].ToString());
                           // paramList[8] = new SqlParameter("@CreatedOn", dr["CreatedDate"].ToString());
                            paramList[9] = new SqlParameter("@ATHDId", headerId);
                            paramList[10] = new SqlParameter("@ATDId", 0);
                            paramList[10].Direction = ParameterDirection.Output;
                            paramList[11] = new SqlParameter("@Title", null);
                            paramList[12] = new SqlParameter("@MealChoice", null);
                            paramList[13] = new SqlParameter("@PassportNo", null);
                            paramList[14] = new SqlParameter("@DateOfBirth", null);
                            paramList[15] = new SqlParameter("@PassportExpDate", null);
                            paramList[16] = new SqlParameter("@RoomType", null);
                            paramList[17] = new SqlParameter("@ReceiptNo", null);
                            paramList[18] = new SqlParameter("@RoomNo", null);
                            paramList[19] = new SqlParameter("@ATId", -1);
                            paramList[20] = new SqlParameter("@PaxType", null);
                            DBGateway.ExecuteNonQueryDetails(cmd, "Act_Service_AddActivityTransactionDetail", paramList);
                            dr["ATDId"] = paramList[10].Value;
                        }
                    }

                    //Save Transaction Price
                    if (dtTransactionPrice.Rows.Count > 0 )
                    {
                        int i = 0;
                        SqlParameter[] paramList = new SqlParameter[24];
                        foreach (DataRow dr in dtTransactionPrice.Rows)
                        {
                            dr["ATHId"] = headerId;
                            paramList[0] = new SqlParameter("@ATHId", headerId);
                            paramList[1] = new SqlParameter("@Label", dr["Label"].ToString());
                            paramList[2] = new SqlParameter("@Amount", (Convert.ToDecimal(dr["Amount"]) - Convert.ToDecimal(dr["Markup"])));
                            paramList[3] = new SqlParameter("@LabelQty", Convert.ToInt32(dr["LabelQty"]));
                            paramList[4] = new SqlParameter("@LabelAmount", Convert.ToDecimal(dr["LabelAmount"]));
                            paramList[5] = new SqlParameter("@CreatedBy", Convert.ToInt32(dr["CreatedBy"]));
                            paramList[6] = new SqlParameter("@CreatedDate", Convert.ToDateTime(dr["CreatedDate"]));
                            paramList[7] = new SqlParameter("@LastModifiedBy", Convert.ToInt32(dr["CreatedBy"]));
                            paramList[8] = new SqlParameter("@LastModifiedDate", Convert.ToDateTime(dr["CreatedDate"]));
                            paramList[9] = new SqlParameter("@ATPId", 0);
                            paramList[9].Direction = ParameterDirection.Output;
                            paramList[10] = new SqlParameter("@AgentCurrency", Convert.ToString(dr["AgentCurrency"]));
                            paramList[11] = new SqlParameter("@AgentROE", Convert.ToDecimal(dr["AgentROE"]));
                            paramList[12] = new SqlParameter("@MarkupValue", Convert.ToDecimal(dr["MarkupValue"]));
                            paramList[13] = new SqlParameter("@MarkupType", Convert.ToString(dr["MarkupType"]));
                            paramList[14] = new SqlParameter("@SourceCurrency", Convert.ToString(dr["SourceCurrency"]));
                            paramList[15] = new SqlParameter("@SourceAmount", Convert.ToDecimal(dr["SourceAmount"]));
                            paramList[16] = new SqlParameter("@Markup", Convert.ToDecimal(dr["Markup"]));
                            paramList[17] = new SqlParameter("@ATId", -1);
                            paramList[18] = new SqlParameter("@ActivityPriceId", null);
                            paramList[19] = new SqlParameter("@DiscountType", null);
                            paramList[20] = new SqlParameter("@DiscountRemarks", null);
                            paramList[21] = new SqlParameter("@PromotionCode", null);
                            paramList[22] = new SqlParameter("@PromotionAmount", null);
                            paramList[23] = new SqlParameter("@ATDID", Convert.ToInt32(dtTransactionDetail.Rows[0]["ATDId"]));

                            i++;
                            DBGateway.ExecuteNonQueryDetails(cmd, "Act_Service_AddActivityTransactionPrice", paramList);
                            dr["ATPId"] = paramList[9].Value;
                        }
                    }

                    //Save Flex Details
                    if (dtFlexDetails.Rows.Count > 0  )
                    {
                        SqlParameter[] paramList = new SqlParameter[8];
                        foreach (DataRow dr in dtFlexDetails.Rows)
                        {
                            paramList[0] = new SqlParameter("@ATHId", headerId);
                            paramList[1] = new SqlParameter("@ActivityId", Convert.ToInt32(dtTransactionHeader.Rows[0]["ActivityId"]));
                            paramList[2] = new SqlParameter("@FlexId", Convert.ToInt64(dr["flexId"]));
                            paramList[3] = new SqlParameter("@FlexLabel", dr["flexLabel"].ToString());
                            paramList[4] = new SqlParameter("@FlexData", dr["FlexData"].ToString());
                            paramList[5] = new SqlParameter("@flexCreatedBy", Convert.ToInt64(dr["flexCreatedBy"]));
                            paramList[6] = new SqlParameter("@flexCreatedOn", Convert.ToDateTime(dr["flexCreatedOn"]));
                            paramList[7] = new SqlParameter("@ATFDId", 0);
                            paramList[7].Direction = ParameterDirection.Output;

                            DBGateway.ExecuteNonQueryDetails(cmd, "Act_Service_AddActivityFlexDetails", paramList);
                            dr["ATFDId"] = paramList[7].Value;
                        }
                    }
                    if (isFixedDeparture == "N")
                    {
                        //Update Stock Used
                        SqlParameter[] paramListStock = new SqlParameter[2];
                        paramListStock[0] = new SqlParameter("@ActivityId", id);
                        paramListStock[1] = new SqlParameter("@PaxCount", totalPaxCount);
                        int updated = DBGateway.ExecuteNonQuerySP("Act_Service_UpdateActivityStock", paramListStock);
                        if (updated < 0)
                        {
                            throw new Exception("passenger count exceeded the available stock");
                        }
                    }
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();

                Audit.Add(EventType.SightseeingBooking, Severity.High, 0, ex.ToString(), "");
                throw ex;
            }
            finally
            {
                DBGateway.CloseConnection(cmd);
            }
        }

        public void GetActivityForQueue(Int64 id)
        {
            Load(id, true);
        }

        private void Load(long id, bool loadData)
        {
            DataSet ds = new DataSet();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramListHeader = new SqlParameter[1];
                    paramListHeader[0] = new SqlParameter("@ATHId", id);

                    dtTransactionHeader = DBGateway.FillDataTableSP("Act_Service_GetActivityTransactionHeader", paramListHeader);
                    if (dtTransactionHeader.Rows.Count > 0)
                    {
                        Int64 activityId = Convert.ToInt64(dtTransactionHeader.Rows[0]["ActivityId"]);
                        SqlParameter[] paramList = new SqlParameter[1];
                        paramList[0] = new SqlParameter("@ActivityId", activityId);
                        SqlDataReader reader = DBGateway.ExecuteReaderSP("Act_Service_GetActivity", paramList, connection);

                        while (reader.Read())
                        {
                            this.id = id;
                            if (reader["activityName"] != DBNull.Value)
                            {
                                name = reader["activityName"].ToString();
                            }
                            if (reader["activityDays"] != DBNull.Value)
                            {
                                days = Convert.ToInt32(reader["activityDays"]);
                            }
                            if (reader["stockInHand"] != DBNull.Value)
                            {
                                stockInHand = Convert.ToInt32(reader["stockInHand"]);
                            }
                            if (reader["stockUsed"] != DBNull.Value)
                            {
                                stockUsed = Convert.ToInt32(reader["stockUsed"]);
                            }
                            if (reader["agencyId"] != DBNull.Value)
                            {
                                agencyId = Convert.ToInt32(reader["agencyId"]);
                            }
                            if (reader["activityStartingFrom"] != DBNull.Value)
                            {
                                startingFrom = Convert.ToInt32(reader["activityStartingFrom"]);
                            }
                            if (reader["ActivityCity"] != DBNull.Value)
                            {
                                city = reader["ActivityCity"].ToString();
                            }
                            if (reader["activityCountry"] != DBNull.Value)
                            {
                                country = reader["activityCountry"].ToString();
                            }
                            if (reader["imagePath1"] != DBNull.Value)
                            {
                                imagePath1 = reader["imagePath1"].ToString();
                            }
                            if (reader["imagePath2"] != DBNull.Value)
                            {
                                imagePath2 = reader["imagePath2"].ToString();
                            }
                            if (reader["imagePath3"] != DBNull.Value)
                            {
                                imagePath3 = reader["imagePath3"].ToString();
                            }
                            //if (reader["themeId"] != DBNull.Value)
                            //{
                            //    theme = new ActivityTheme(reader["themeId"].ToString());
                            //}
                            if (reader["introduction"] != DBNull.Value)
                            {
                                introduction = reader["introduction"].ToString();
                            }
                            if (reader["overview"] != DBNull.Value)
                            {
                                overview = reader["overview"].ToString();
                            }
                            if (reader["startFrom"] != DBNull.Value)
                            {
                                startFrom = Convert.ToDateTime(reader["startFrom"]);
                            }
                            if (reader["endTo"] != DBNull.Value)
                            {
                                endTo = Convert.ToDateTime(reader["endTo"]);
                            }
                            if (reader["durationHours"] != DBNull.Value)
                            {
                                durationHours = Convert.ToString(reader["durationHours"]);
                            }
                            if (reader["availableFrom"] != DBNull.Value)
                            {
                                availableFrom = Convert.ToDateTime(reader["availableFrom"]);
                            }
                            if (reader["availableTo"] != DBNull.Value)
                            {
                                availableTo = Convert.ToDateTime(reader["availableTo"]);
                            }
                            unavailableDates = new List<DateTime>();
                            if (reader["unavailableDates"] != DBNull.Value)
                            {
                                string[] dates = reader["unavailableDates"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string date in dates)
                                {
                                    try
                                    {
                                        unavailableDates.Add(Convert.ToDateTime(date));
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            if (reader["itinerary1"] != DBNull.Value)
                            {
                                itinerary1 = reader["itinerary1"].ToString();
                            }
                            if (reader["itinerary2"] != DBNull.Value)
                            {
                                itinerary2 = reader["itinerary2"].ToString();
                            }
                            if (reader["details"] != DBNull.Value)
                            {
                                details = reader["details"].ToString();
                            }
                            if (reader["supplierName"] != DBNull.Value)
                            {
                                supplierName = reader["supplierName"].ToString();
                            }
                            if (reader["supplierEmail"] != DBNull.Value)
                            {
                                supplierEmail = reader["supplierEmail"].ToString();
                            }
                            if (reader["mealsIncluded"] != DBNull.Value)
                            {
                                mealsIncluded = reader["mealsIncluded"].ToString();
                            }
                            if (reader["transferIncluded"] != DBNull.Value)
                            {
                                transferIncluded = reader["transferIncluded"].ToString();
                            }
                            if (reader["pickupLocation"] != DBNull.Value)
                            {
                                pickupLocation = reader["pickupLocation"].ToString();
                            }
                            if (reader["pickupDate"] != DBNull.Value)
                            {
                                pickupDate = Convert.ToDateTime(reader["pickupDate"]);
                            }
                            if (reader["dropOffLocation"] != DBNull.Value)
                            {
                                dropoffLocation = reader["dropOffLocation"].ToString();
                            }
                            if (reader["dropOffDate"] != DBNull.Value)
                            {
                                dropoffDate = Convert.ToDateTime(reader["dropOffDate"]);
                            }
                            if (reader["exclusions"] != DBNull.Value)
                            {
                                exclusions = new List<string>();
                                string[] list = reader["exclusions"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string data in list)
                                {
                                    exclusions.Add(data);
                                }
                            }
                            if (reader["inclusions"] != DBNull.Value)
                            {
                                inclusions = new List<string>();
                                string[] list1 = reader["inclusions"].ToString().Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string data in list1)
                                {
                                    inclusions.Add(data);
                                }
                            }
                            if (reader["cancellPolicy"] != DBNull.Value)
                            {
                                cancelPolicy = reader["cancellPolicy"].ToString();
                            }
                            if (reader["thingsToBring"] != DBNull.Value)
                            {
                                thingsToBring = reader["thingsToBring"].ToString();
                            }
                            if (reader["unavailableDays"] != DBNull.Value && !string.IsNullOrEmpty(reader["unavailableDays"].ToString()))
                            {
                                unavailableDays = Convert.ToInt32(reader["unavailableDays"]);
                            }
                            if (reader["bookingCutOff"] != DBNull.Value)
                            {
                                bookingCutOff = Convert.ToInt32(reader["bookingCutOff"]);
                            }
                            if (reader["activityStatus"] != DBNull.Value)
                            {
                                status = reader["activitystatus"].ToString();
                            }
                            if (reader["createdBy"] != DBNull.Value)
                            {
                                createdBy = Convert.ToInt32(reader["createdBy"]);
                            }
                            if (reader["createdOn"] != DBNull.Value)
                            {
                                createdOn = Convert.ToDateTime(reader["createdOn"]);
                            }
                            if (reader["modifiedBy"] != DBNull.Value)
                            {
                                modifiedBy = Convert.ToInt32(reader["modifiedBy"]);
                            }
                            if (reader["modifiedOn"] != DBNull.Value)
                            {
                                modifiedOn = Convert.ToDateTime(reader["modifiedOn"]);
                            }
                            if (reader["IsFixedDeparture"] != DBNull.Value)
                            {
                                isFixedDeparture = reader["IsFixedDeparture"].ToString();
                            }
                            if (reader["discountCode"] != DBNull.Value)
                            {
                                discountCode = Convert.ToString(reader["discountCode"]);
                            }
                            if (reader["discount"] != DBNull.Value)
                            {
                                discount = Convert.ToDecimal(reader["discount"]);
                            }
                            //Load the Flex details
                            SqlParameter[] paramListFlex = new SqlParameter[1];
                            paramListFlex[0] = new SqlParameter("@flexActivityId", activityId);
                            dtFlexMaster = DBGateway.FillDataTableSP("Act_Service_GetActivityFlexMaster", paramListFlex);

                            //Load the Price details
                            SqlParameter[] paramListPrice = new SqlParameter[1];
                            paramListPrice[0] = new SqlParameter("@priceActivityId", activityId);
                            dtPriceDetails = DBGateway.FillDataTableSP("Act_Service_GetActivityPrice", paramListPrice);




                            //Load the Transaction Detail
                            SqlParameter[] paramListDetail = new SqlParameter[1];
                            if (!loadData)
                            {
                                paramListDetail[0] = new SqlParameter("@athdId", -1);
                            }
                            else
                            {
                                paramListDetail[0] = new SqlParameter("@athdId", id);
                            }
                            dtTransactionDetail = DBGateway.FillDataTableSP("Act_Service_GetActivityTransactionDetail", paramListDetail);


                            //Load the Transaction Price Details
                            SqlParameter[] paramListTransPrice = new SqlParameter[1];
                            if (!loadData)
                            {
                                paramListTransPrice[0] = new SqlParameter("@ATHId", -1);
                            }
                            else
                            {
                                paramListTransPrice[0] = new SqlParameter("@ATHId", id);
                            }
                            dtTransactionPrice = DBGateway.FillDataTableSP("Act_Service_GetActivityTransactionPrice", paramListTransPrice);

                            //Load the Flex Details
                            SqlParameter[] paramListFlexDetails = new SqlParameter[1];
                            if (!loadData)
                            {
                                paramListFlexDetails[0] = new SqlParameter("@ATHId", -1);
                            }
                            else
                            {
                                paramListFlexDetails[0] = new SqlParameter("@ATHId", id);
                            }
                            dtFlexDetails = DBGateway.FillDataTableSP("Act_Service_GetActivityFlexDetails", paramListFlexDetails);
                        }
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "");
                }
            }
        }
    }
}
