﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityAPI
{
    public class NarrationBuilder
    {
        public string Remarks = string.Empty;
        public string PaxName = string.Empty;
        public string Sector = string.Empty;
        public string TicketNo = string.Empty;
        public string DocNo = string.Empty;
        public string TravelDate = string.Empty;
        public string ChequeNo = string.Empty;
        public string FlightNo = string.Empty;
        /// <summary>
        /// For destination where hotel is booked
        /// </summary>
        public string DestinationCity = string.Empty;
        public string HotelConfirmationNo = string.Empty;
        public string PolicyNo = string.Empty;
        public string PaymentMode = string.Empty;
        public string TourConfirmationNo = string.Empty;
        public string FleetConfirmationNo = string.Empty;
    }
}