USE [COZMOVMSB2BLiveTest-1]
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_AddActivityTransactionDetail]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Act_Service_AddActivityTransactionDetail]
(			
           @FirstName		nvarchar(50)
           ,@LastName		nvarchar(50)
           ,@Phone			nvarchar(15)
           ,@PhoneCountryCode nvarchar(5)
           ,@Email			nvarchar(50)
           ,@Nationality	nvarchar(50)
           ,@PaxSerial		int
           ,@CreatedBy		bigint
           ,@CreatedOn		datetime           
           ,@ATHDId			bigint
           ,@ATDId			bigint output
           ,@Title          nvarchar(10)=null
           ,@MealChoice      nvarchar(20)=null
           ,@PassportNo      nvarchar(20)=null
           ,@DateOfBirth     datetime=null
           ,@PassportExpDate datetime=null
           ,@RoomType nvarchar(30)=null
           ,@RoomNo bigint=null
           ,@ReceiptNo nvarchar(50)=null
           ,@ATId bigint
           ,@PaxType nvarchar(20)=null
)
AS
BEGIN


INSERT INTO ACT_Activity_Transaction_Detail
           (ATHDId
           ,FirstName
           ,LastName
           ,Phone
           ,PhoneCountryCode
           ,Email
           ,Nationality
           ,PaxSerial
           ,CreatedBy
           ,CreatedDate
           ,Title
           ,MealChoice
           ,DateOfBirth
           ,PassportNo
           ,PassportExpDate
           ,RoomType
           ,RoomNo
           ,ReceiptNo
           ,paxType)
     VALUES
           (@ATHDId			
           ,@FirstName		
           ,@LastName		
           ,@Phone	
           ,@PhoneCountryCode		
           ,@Email			
           ,@Nationality	
           ,@PaxSerial		
           ,@CreatedBy		
           ,@CreatedOn
           ,@Title
           ,@MealChoice
           ,@DateOfBirth
           ,@PassportNo
           ,@PassportExpDate
           ,@RoomType
           ,@RoomNo
           ,@ReceiptNo
           ,@PaxType)
           
           select @ATDId = @@IDENTITY from ACT_Activity_Transaction_Detail
           
           
END

GO
/****** Object:  StoredProcedure [dbo].[Act_Service_AddActivityFlexDetails]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Act_Service_AddActivityFlexDetails]
(
			@ATHId bigint=null
           ,@ActivityId bigint=null
           ,@flexId bigint=null
           ,@flexLabel nvarchar(50)=null
           ,@flexData nvarchar(50)=null
           ,@flexCreatedBy bigint=null
           ,@flexCreatedOn datetime=null
           ,@ATFDId bigint output
)
AS
Begin
INSERT INTO ACT_Activity_Flex_Details
           (ATHId
           ,ActivityId
           ,flexId
           ,flexLabel
           ,flexData
           ,flexCreatedBy
           ,flexCreatedOn)
     VALUES
           (@ATHId
           ,@ActivityId
           ,@flexId
           ,@flexLabel
           ,@flexData
           ,@flexCreatedBy
           ,@flexCreatedOn)
 Select @ATFDId = @@IDENTITY from ACT_Activity_Flex_Details
End
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_AddAudit]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Act_Service_AddAudit]
(
            @eventType int,
            @severity int,
            @userId int,
            @detail text,
        @eventTime DateTime,
        @ipAddress nvarchar(50)
)
AS
      INSERT INTO BKE_Audit
                  (eventType,
                   severity,
                   userId,
                   detail,
                   eventTime,
                   ipAddress
                  )
				VALUES     
				(@eventType,
				 @severity,
				 @userId,
				 @detail, 
				 GetUTCDate(),
				 @ipAddress
				)
      RETURN
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_AddActivityTransactionPrice]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Act_Service_AddActivityTransactionPrice]
(
			@ATHId bigint
           ,@Label nvarchar(50)
           ,@Amount numeric(18,4)
           ,@LabelQty int
           ,@Labelamount numeric(18,2)
           ,@CreatedBy bigint
           ,@CreatedDate datetime
           ,@LastModifiedBy bigint
           ,@LastModifiedDate datetime
           ,@ATPId bigint output
           ,@AgentCurrency nvarchar(3)
           ,@AgentROE money
           ,@MarkupValue money
           ,@MarkupType nvarchar(1)
           ,@SourceCurrency nvarchar(3)
           ,@SourceAmount money
           ,@Markup money
           --,@RoomNo bigint=null
           --,@PaxType nvarchar(20)=null
           ,@ATId bigint
           ,@ActivityPriceId bigint=null
           ,@ATDID bigint
           ,@DiscountType nvarchar(25)=null
           ,@DiscountRemarks nvarchar(250)=null
           ,@PromotionCode nvarchar(10)=null
           ,@promotionAmount money =null
           ,@B2CMarkupValue money=null
           ,@B2CMarkupType nvarchar(1)=null
           ,@B2CMarkup money=null
)
AS
BEGIN

Declare @status nvarchar(1)
select  @status=QuotedStatus from ACT_Activity_Transaction_Header where ATHId=@ATHId

INSERT INTO ACT_Activity_Transaction_Price
           (ATHId
           ,Label
           ,Amount
           ,LabelQty
           ,Labelamount
           ,CreatedBy
           ,CreatedDate
           ,AgentCurrency
           ,AgentROE
           ,MarkupValue
           ,MarkupType
           ,SourceCurrency
           ,SourceAmount
           ,Markup
           --,RoomNo
           --,PaxType
           ,ActivityPriceId
           ,ATDID
           ,DiscountType
           ,DiscountRemarks
           ,PromotionCode
           ,PromotionAmount
           ,B2CMarkupValue
           ,B2CMarkupType
           ,B2CMarkup)
     VALUES
           (@ATHId 
           ,@Label 
           ,@Amount 
           ,@LabelQty 
           ,@Labelamount 
           ,@CreatedBy 
           ,@CreatedDate 
           ,@AgentCurrency
           ,@AgentROE
           ,@MarkupValue
           ,@MarkupType
           ,@SourceCurrency
           ,@SourceAmount
           ,@markup
           --,@RoomNo
           --,@PaxType
           ,@ActivityPriceId
           ,@ATDID
           ,@DiscountType
           ,@DiscountRemarks
           ,@PromotionCode
           ,@promotionAmount
           ,@B2CMarkupValue
           ,@B2CMarkupType
           ,@B2CMarkup)
           
     
     
    Select @ATPId = @@IDENTITY from ACT_Activity_Transaction_Price
    
    if(@status='C')
    BEGIN
    Update ACT_Activity_Price Set StockUsed = (stockUsed + @LabelQty)
     Where priceId=@ActivityPriceId
    END
    
END
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_CancellationDetails]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Act_Service_CancellationDetails]
(
@activityId int
)
as
Begin
	select activityId
	,activityName
	,activityCity
	,activityCountry
	,introduction
	,overview
	,itinerary1
	,itinerary2
	,details
	,inclusions
	,exclusions
	,cancellPolicy
	,imagePath1
	,transferIncluded
	,pickupLocation
	,startFrom
    ,endTo
    ,durationHours
    ,pickupLocation
	from ACT_Activity_Master AM
	 where activityId = @activityId 
	 and activityStatus = 'A' and IsfixedDeparture='N'
	 
	 if(@@ROWCOUNT>0)
	 Begin
	 		select flexActivityId
					,flexLabel
					,flexDataType
					,flexMandatoryStatus 
					 from ACT_Activity_FlexMaster where flexActivityId = @activityId and flexStatus = 'A'
	 End
End
--exec Act_Service_CancellationDetails 1
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_Authentication]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Act_Service_Authentication]
@UserName nvarchar(50)
,@Password nvarchar(50)
,@P_MSG_Type nvarchar(10) output
,@P_MSG_Text nvarchar(100) output
,@P_TokenID nvarchar(40)output
,@P_LoginTime datetime output
as
Begin
	IF(Not Exists(select User_Login_Name from CT_T_USER_MASTER where user_login_name = @UserName and 
	user_password = @Password and user_status = 'A'))
	Begin
		set @P_MSG_Type = 'E'
		set @P_MSG_Text = 'UserName/Password does not exist!'
	End
	Else
	Begin
		select [user_id]
				,user_first_name
				,user_last_name
				,user_email
		from CT_T_USER_MASTER
		where user_login_name = @UserName and user_password = @Password and user_status = 'A'
		
		IF(@@ROWCOUNT<=0)
		Begin 
			set @P_MSG_Type = 'E'
			set @P_MSG_Text = 'UserName/Password does not exist!'
			Return
		End
		ELSE
		BEGIN
			set @P_MSG_Type = 'S'
			set @P_MSG_Text = 'Success'
			
			set @P_TokenID = NEWID()
			set @P_LoginTime =  GETDATE()
			
			INSERT INTO BKE_AirLoginSessions
			(
			 userid
			,logintime
			,sessionid
			,isactive
			,createdon
			)
			VALUES
			(
			(select user_id from CT_T_USER_MASTER where user_login_name=@UserName and user_password=@Password and user_status = 'A')
			,@P_LoginTime
			,@P_TokenID
			,1
			,@P_LoginTime
			)
			END
	End
End
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_AddInvoiceLineItem]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Act_Service_AddInvoiceLineItem]
	(
		@invoiceNumber INT,
		@itemReferenceNumber INT,
		@itemDescription VARCHAR(MAX),
		@itemTypeId INT,
		@priceId int,
		@CREATEdBy INT,
		@CREATEdOn DATETIME
	)
	
AS
	INSERT INTO BKE_Invoice_Line_Item
	                      (invoiceNumber, itemDescription, itemReferenceNumber, itemTypeId, priceId, CREATEdOn, CREATEdBy, lastModifiedOn, lastModifiedBy)
	VALUES     (@invoiceNumber,@itemDescription,@itemReferenceNumber,@itemTypeId,@priceId, @CREATEdOn,@CREATEdBy, @CREATEdOn, @CREATEdBy)
	
	RETURN
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivityTransactionPrice]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Act_Service_GetActivityTransactionPrice]
(
	@ATHId bigint
)
AS
Begin
SELECT *  FROM ACT_Activity_Transaction_Price 
WHERE ATHId = @ATHId and LabelQty <> 0
End
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivityTransactionHeader]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Act_Service_GetActivityTransactionHeader]
(   
	@ATHId bigint 
)
AS
        
 Select * From ACT_Activity_Transaction_Header
WHERE ATHId = @ATHId
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivityTransactionDetail]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Act_Service_GetActivityTransactionDetail]
(	
   @athdId bigint
)
AS
Select * from ACT_Activity_Transaction_Detail 
WHERE ATHDId = @athdId
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivityThemeInfo]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Act_Service_GetActivityThemeInfo]
@themeType nvarchar(1)
AS
BEGIN
Declare @IsFD varchar(1)
IF @themeType = 'F'
SET @IsFD = 'Y'
ELSE
SET @IsFD = 'N'


select distinct TM.themeId,TM.themeName  from ACT_Activity_Master AM 
CROSS APPLY dbo.FnSplit(AM.themeId,',') AS S
 INNER JOIN ThemeMaster TM
ON TM.themeId = S.s
WHERE AM.isFixedDeparture=@IsFD AND TM.themeType=@themeType
and AM.activityStatus='A'
END
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivityPrice]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Act_Service_GetActivityPrice]
(	
   @priceActivityId bigint
)
AS
Select * from ACT_Activity_Price 
WHERE priceActivityId = @priceActivityId
and priceStatus='A'
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivityFlexMaster]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Act_Service_GetActivityFlexMaster]
(	
   @flexActivityId bigint
)
AS
Select * from ACT_Activity_FlexMaster 
WHERE flexActivityId = @flexActivityId
AND flexStatus='A'
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivityFlexDetails]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Act_Service_GetActivityFlexDetails]
(
	@ATHId bigint
)
AS
SELECT ATFDId
      ,ATHId
      ,ActivityId
      ,flexId
      ,flexLabel
      ,flexData
      ,flexCreatedBy
      ,flexCreatedOn
  FROM ACT_Activity_Flex_Details
  WHERE ATHId = @ATHId
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivityById]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Act_Service_GetActivityById]
(   
	@ActivityId bigint 
)
AS
Begin     

 Select * From ACT_Activity_Master
WHERE activityId = @ActivityId

end

exec Act_Service_GetActivityById 1
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetActivity]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Act_Service_GetActivity]
(   
	@ActivityId bigint 
)
AS
        
 Select * From ACT_Activity_Master
WHERE activityId = @ActivityId
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_UpdateActivityStock]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Act_Service_UpdateActivityStock]
(	
	@ActivityId bigint
)
AS
Begin
Update ACT_Activity_Master Set stockUsed = (stockUsed + 1) Where activityId = @ActivityId
End
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_SearchActivity]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Act_Service_SearchActivity]
(
@ActivityName nvarchar(100) = null
,@Type nvarchar(50) = null
,@CityCode nvarchar(50)
,@CountryCode nvarchar(50)
,@FromDate datetime
,@ToDate datetime
,@TotalPassengerCount int
,@agencyId int
,@Category nvarchar(50) = null
)
as
Begin
--declare @tempTable table(activityIds int)

	select DISTINCT activityId
	,activityName
	,activityDays
	,activityCity
	,activityCountry
	,imagePath1
	,imagePath2
	,imagePath3
	,themeId
	,introduction
	,overview
	,startFrom 
	,endTo
	,durationHours
	,unavailableDates
	,itinerary1
	,itinerary2
	,details
	,mealsIncluded
	,transferIncluded
	,pickupLocation
	,pickupDate
	,inclusions
	,exclusions
	,cancellPolicy
	,thingsToBring
	,bookingCutoff
	 from ACT_Activity_Master AM
	 left join ACT_Activity_Price AP on AP.priceActivityId = AM.activityId
	 where activityCity = @CityCode and activityCountry=@CountryCode 
	 and (availableFrom <= @FromDate) and (availableTo >= @ToDate)
	 and (label = 'Adult' or label = 'Child' or label = 'Infant') 
	 and activityName like '%'+isnull(@ActivityName,AM.activityName) +'%'
	 and AM.stockInHand > @TotalPassengerCount 
	 and IsfixedDeparture = 'N' and activityStatus = 'A' and agencyId = @agencyId
	 and exists (select s from dbo.fnsplit(AM.themeId,',') where s in (select s from dbo.FnSplit(ISNULL(@Type,AM.themeId),','))) 
	 
	 
	 --in (select s from dbo.fnsplit(isnull(@Category,(select s from dbo.fnsplit(AM.themeId,','))),','))
	 if(@@ROWCOUNT>0)
	 Begin
			select priceActivityId
					,label
					,amount 
			from ACT_Activity_Price AP
			inner join ACT_Activity_Master AM on AM.activityId = AP.priceActivityId
			where activityCity = @CityCode and activityCountry=@CountryCode 
			and (availableFrom <= @FromDate) and (availableTo >= @ToDate)
			and(label = 'Adult' or label = 'Child' or label = 'Infant') 
			and exists (select s from dbo.fnsplit(AM.themeId,',') where s in (select s from dbo.FnSplit(ISNULL(@Type,AM.themeId),',')))
			and activityName like '%'+isnull(@ActivityName,AM.activityName) +'%'
			and AM.stockInHand > @TotalPassengerCount 
			and IsfixedDeparture = 'N' and activityStatus = 'A' and agencyId = @agencyId
			and priceStatus = 'A'
			group by priceActivityId,label,amount 
			
			select flexActivityId
					,flexLabel
					,flexDataType
					,flexMandatoryStatus 
			from ACT_Activity_FlexMaster FM
			inner join ACT_Activity_Master AM on AM.activityId = FM.flexActivityId
			inner join ACT_Activity_Price AP on AP.priceActivityId = AM.activityId
			where activityCity = @CityCode and activityCountry=@CountryCode 
			and (availableFrom <= @FromDate) and (availableTo >= @ToDate)
			and(label = 'Adult' or label = 'Child' or label = 'Infant') 
			and exists (select s from dbo.fnsplit(AM.themeId,',') where s in (select s from dbo.FnSplit(ISNULL(@Type,AM.themeId),',')))
			and activityName like '%'+isnull(@ActivityName,AM.activityName) +'%'
			and AM.stockInHand > @TotalPassengerCount 
			and IsfixedDeparture = 'N' and activityStatus = 'A' and agencyId = @agencyId
			and flexStatus = 'A'
			group by flexActivityId,flexLabel,flexDataType,flexMandatoryStatus 
	 End
End



--exec Act_Service_SearchActivity null,null,'DXB','AE','2017-02-23','2017-04-22',2,1,null
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_IsInvoiceGeneratedForActivity]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[Act_Service_IsInvoiceGeneratedForActivity]
(
	@ActivityId int
)
as 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

Select invoiceNumber from BKE_Invoice_Line_Item
WHERE itemReferenceNumber = @ActivityId and 
	  itemTypeId=4
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_InvoiceRaisedForActivity]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Act_Service_InvoiceRaisedForActivity]
	(
		@ActivityId INT
	)
	
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT COUNT(distinct itemReferenceNumber) AS lineCount 
	FROM BKE_Invoice_Line_Item 
	WHERE itemReferenceNumber =@ActivityId 
	AND itemtypeid = 4
	
	RETURN
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetTripId]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Act_Service_GetTripId]
(
@ATHID int
) as 
Begin
	select TripId from ACT_Activity_Transaction_Header where ATHId = @ATHID
End
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetTheme]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Act_Service_GetTheme]
 @themeType nvarchar(1)=null
As
Begin
select *
      from ThemeMaster 
      where themeStatus='A'
      AND themeType=isnull(@themeType,themeType)-- AND themeCode = @themeCode
      
end
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetPriceDetail]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Act_Service_GetPriceDetail]
 (
	@priceId int
 )
 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

 SELECT     *
 FROM        BKE_PRICE
 WHERE     (priceId = @priceId) 
 
 RETURN
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetLineItems]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Act_Service_GetLineItems]
(
@invoiceNumber int
)

AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT   *
from BKE_Invoice_Line_Item where
invoiceNumber=@invoiceNumber

RETURN

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetInvoice]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Act_Service_GetInvoice]
 (
  @invoiceNumber int
 )
 
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

 SELECT   *
 from BKE_INVOICE where
 invoiceNumber=@invoiceNumber

 RETURN
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetChargeBreakUpEntry]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Act_Service_GetChargeBreakUpEntry]
(
		@priceId INT
)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT *
	FROM BKE_Charge_BreakUp
	WHERE priceId = @priceId
	
	RETURN
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_UpdateInvoice]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Act_Service_UpdateInvoice]
	(
		@invoiceNumber INT,
		@agencyId INT,
		@status INT,
		@remarks NTEXT,
		@lastModifiedBy INT,
		@lastModifiedOn DATETIME,
		@manuallyUpdated bit,
		@totalPrice money,
        @staffRemarks varchar(max)=null,
        @P_RET_AGENT_BALANCE decimal output
	)
	
AS
  
BEGIN
  
BEGIN TRAN
  UPDATE BKE_Invoice 
  SET
  agencyId=@agencyId,status=@status,remarks=@remarks,lastModifiedBy=@lastModifiedBy,totalPrice=@totalPrice,
  lastModifiedOn=getutcdate(),manuallyUpdated=@manuallyUpdated,staffRemarks=@staffRemarks 
  WHERE
  invoiceNumber=@invoiceNumber

    
 --Return the updated agency balance
 SET @P_RET_AGENT_BALANCE = (SELECT [dbo].[getAgentBalance](@agencyId,0,'N') )
 
	IF @status=2
	BEGIN
		UPDATE BKE_Payment_Invoice SET isPartial=1 WHERE invoiceNumber=@invoiceNumber	
	
 	
	END
	IF @@ERROR<>0
	BEGIN
		ROLLBACK TRAN
		RAISERROR ('An error occured during setting status of isPartial column is true in paymentInvoice table',10,1)
	END
	ELSE
	BEGIN
		COMMIT TRAN
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_GetAgentMarkup]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Act_Service_GetAgentMarkup]
 @AgentId int=null
,@Source nvarchar(50)
,@ProductId int
as
begin

    IF(EXISTS(select MR.MRId , MD.Markup, MD.MarkupType, MD.Discount, MD.DiscountType
 from bke_markupRules MR
 inner join bke_MarkupRuleDetails MD
 on MR.MRId = MD.MRId
 where  MR.SourceId = @Source and MR.ProductId = @ProductId and MD.AgentId = @AgentId))
  BEGIN
   select MR.MRId , MD.Markup, MD.MarkupType, MD.Discount, MD.DiscountType,MD.TransType
   from bke_markupRules MR
   inner join bke_MarkupRuleDetails MD
   on MR.MRId = MD.MRId
   where  MR.SourceId = @Source and MR.ProductId = @ProductId and MD.AgentId = @AgentId
        END
        
        ELSE
        BEGIN
   select MR.MRId , MD.Markup, MD.MarkupType, MD.Discount, MD.DiscountType,MD.TransType
   from bke_markupRules MR
   inner join bke_MarkupRuleDetails MD
   on MR.MRId = MD.MRId
   where  MR.SourceId = @Source and MR.ProductId = @ProductId and MD.AgentId is null
        END
 
end

--EXEC Act_Service_GetAgentMarkup 1,'',6
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_CheckingTokenId]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Act_Service_CheckingTokenId]
(
@TokenId nvarchar(40)
,@P_MSG_TYPE nvarchar(10) output
,@P_MSG_TEXT nvarchar(100) output
,@LoginTimeOut int --this parameter is for checking Login Time.
)
as
Begin
	
		declare @userId int,@createdDateTime datetime;
		  (select @userId = userid,@createdDateTime=createdon from BKE_AirLoginSessions where sessionid = @TokenId and isactive = 1)
		
		if(@userId > 0 and ((select DATEDIFF(MINUTE,@createdDateTime,GETDATE()))<@LoginTimeOut))  --here we are checking userId and the time is less than 15 minutes or not
		Begin
			
				
			SELECT  [USER_ID]
					  ,USER_FIRST_NAME 
					  ,USER_LAST_NAME 
					  ,USER_EMAIL 
					  ,USER_LOGIN_NAME 
					  ,USER_MEMBER_TYPE 
					  ,USER_LOCATION_ID
					  ,FM.FIELD_TEXT
					  ,USER_ADDRESS 
					  ,USER_STATUS
					  ,USER_AGENT_ID
					  ,USER_CREATED_BY 
					  ,USER_CREATED_ON 
					  ,[USER_MODIFIED_BY]
					  ,[USER_MODIFIED_ON]
					,LM.location_name
					,LM.location_name
					,LM.location_address
					,LM.location_terms
					,LM.location_contact_details
					,1 USER_COMPANY_ID
					,AM.AGENT_NAME USER_AGENT_NAME
					,[dbo].[getAgentBalance](USER_AGENT_ID,0,'N') agentBalance
					,AM.AGENT_PHONE1
					,AM.AGENT_EMAIL1 
					,AM.agent_guarantor_status
					,AM.agent_visa_submission_status
					,AM.agent_product
					,AM.agent_img_filename,
					AM.agent_decimal,
					AM.agent_type
					,AM.agent_currency
					,AM.n_flex_1 AGENT_THEME
					,AM.n_flex_2 AGENT_COPY_RIGHT
					,user_TransType
				    
				  FROM [CT_T_USER_MASTER] UM 
				 LEFT JOIN CT_T_LOCATION_MASTER LM
				 ON UM.USER_LOCATION_ID=LM.LOCATION_ID
				  LEFT OUTER JOIN CT_T_FIELD_TYPE_MASTER FM
				  ON UM.[USER_MEMBER_TYPE]=FM.FIELD_VALUE
				 inner JOIN CT_T_AGENT_MASTER AM
				 ON UM.USER_AGENT_ID=AM.AGENT_ID  
					WHERE [USER_ID] = @userId
				 AND USER_STATUS='A'
				 
				 if(@@rowcount<=0)
				 BEGIN
					  SET @P_MSG_TYPE='E'
					  SET @P_MSG_TEXT='User Id/password does not exist ! '
					  PRINT 'User Id/password does not exist '
					  
					  RETURN
				 END
				 ELSE
				 begin
						 SET @P_MSG_TYPE='S'
						 SET @P_MSG_TEXT='Success ! '
				 end
		End
		else
		Begin
				SET @P_MSG_TYPE='E'
				SET @P_MSG_TEXT='User Id/password does not exist ! '
				
				update BKE_AirLoginSessions set isactive=0,createdon = GETDATE() where sessionid = @TokenId
		End
	
End
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_AddInvoice]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Act_Service_AddInvoice]
 (  
  @invoiceNumber INT OUTPUT,  
  @agencyId INT,  
  @status INT,  
  @paymentMode NCHAR(10),  
  @remarks NTEXT,  
  @CREATEdBy INT,  
  @invoiceDueDate DATETIME,  
  @isDomestic bit,  
  @productType TINYINT,  
  @xONumber varchar(20)=NULL,  
  @staffRemarks varchar(100)=NULL,  
  @bookingStatus int=null,  
  @invoiceDetail varchar(max)=null,  
  @totalPrice money=0,  
  @createdOn DATETIME,
  @tranxHeaderId bigint,  
  @docTypeCode char(2) output,  
  @documentNumber int output,
  @P_RET_AGENT_BALANCE decimal output
 )  
   
AS  
   
  
 Declare @maxValue int  
 Declare @docCode char(2)  
  
    If(@productType = 1) -- For Flight Product Type is 1  
 BEGIN  
  If(@bookingStatus = 3) -- For cancellation  
  BEGIN  
   If(@isDomestic = 1)  
   BEGIN     
    Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number  with(updlock)  Where docTypeCode = 'NZ')  
    Set @docCode = 'NZ'     
   END  
   ELSE  
   BEGIN  
    Set @docCode = 'KZ'  
    Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'KZ')     
   END  
  
   END  
  ELSE IF(@bookingStatus = 6) -- For void  
  BEGIN  
   If(@isDomestic = 1)  
    BEGIN     
     Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'NY')  
     Set @docCode = 'NY'     
    END  
   ELSE  
    BEGIN  
     Set @docCode = 'KY'  
     Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'KY')     
    END  
     
  END  
  ELSE  
  BEGIN  
   If(@isDomestic = 1)  
    BEGIN     
     Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'NT')  
     Set @docCode = 'NT'     
    END  
   ELSE  
    BEGIN  
     Set @docCode = 'KW'  
     Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'KW')   
    END  
   END  
  END  
ELSE If(@productType = 2) -- For Hotel Product Type is 2  
BEGIN  
      
   Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'HW')  
   Set @docCode = 'HW'     
   
     
    
END   
   
ELSE If(@productType = 4) -- For Activity Product Type is 4  
BEGIN  
   Set @docCode = 'AC'  
   Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'AC')     
END
ELSE If(@productType = 11) -- For FixedDeposit Product Type is 11 
BEGIN  
   Set @docCode = 'FD'  
   Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'FD')     
END
ELSE If(@productType = 6) -- For SightSeeing Product Type is 6 
BEGIN  
   Set @docCode = 'SS'  
   Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'SS')     
END
ELSE If(@productType = 5) -- For Insurance Product Type is 5  
BEGIN  
   Set @docCode = 'IN'  
   Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'IN')     
END   
ELSE If(@productType = 9) -- For Transfers Product Type is 9
BEGIN  
  
   Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'TF')  
   Set @docCode = 'TF'     
   
END  
ELSE If(@productType = 13) -- For FleetItinerary Product Type is 12
BEGIN  
  
   Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'FT')  
   Set @docCode = 'FT'     
   
END   
ELSE If(@productType in (7,8,10)) -- For Sightseeings,Train , SMS & MobileRecharge  
BEGIN  
  
     
   Set @maxValue = (Select lastSequenceNumber + 1 from BKE_Document_Sequence_Number with(updlock)  Where docTypeCode = 'HW')  
   Set @docCode = 'HW'     
   
END   
  
  If(@bookingStatus = 3 OR @bookingStatus = 6)   
 BEGIN  
INSERT INTO BKE_INVOICE  
       (agencyId,docTypeCode, documentNumber, status, paymentMode, remarks,invoiceDueDate,isCreditNote, CREATEdOn, CREATEdBy, lastModifiedOn, lastModifiedBy,xONumber,staffRemarks,invoiceDetail,totalPrice,tranxHeaderId)  
  VALUES     (@agencyId,@docCode,@maxValue, @status,@paymentMode,@remarks,@invoiceDueDate,1, @createdOn,@CREATEdBy, getUTCDate(),@CREATEdBy,@xONumber,@staffRemarks,@invoiceDetail,@totalPrice,@tranxHeaderId)  
 END  
  ELSE  
 BEGIN  
INSERT INTO BKE_INVOICE  
       (agencyId,docTypeCode, documentNumber, status, paymentMode, remarks,invoiceDueDate, CREATEdOn, CREATEdBy, lastModifiedOn, lastModifiedBy,xONumber,staffRemarks,invoiceDetail,totalPrice,tranxHeaderId)  
  VALUES     (@agencyId,@docCode,@maxValue, @status,@paymentMode,@remarks,@invoiceDueDate, @createdOn,@CREATEdBy, getUTCDate(),@CREATEdBy,@xONumber,@staffRemarks,@invoiceDetail,@totalPrice,@tranxHeaderId)  
 END  
   
 SET @invoiceNumber = @@IDENTITY  
 set @docTypeCode=@docCode  
 set @documentNumber=@maxValue  
   
    If(@productType = 1) -- For Flight Product Type is 1  
 BEGIN  
  IF(@bookingStatus=6) -- Void  
   BEGIN  
    If(@isDomestic=1)  
     BEGIN  
      Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
      Where docTypeCode = 'NY'  
     END  
    ELSE  
     BEGIN     
      Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
      Where docTypeCode = 'KY'  
     END  
   END  
  ELSE IF(@bookingStatus=3) -- Refund  
   BEGIN  
    If(@isDomestic=1)  
     BEGIN  
      Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
      Where docTypeCode = 'NZ'  
     END  
    ELSE  
     BEGIN     
      Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
      Where docTypeCode = 'KZ'  
     END  
   END  
  ELSE  
   BEGIN  
    If(@isDomestic=1)  
     BEGIN  
      Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
      Where docTypeCode = 'NT'  
     END  
    ELSE  
     BEGIN     
      Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
      Where docTypeCode = 'KW'  
     END  
   END  
 END  
    ELSE If(@productType = 2) -- For Hotel Product Type is 2  
 BEGIN  
    
    Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
    Where docTypeCode = 'HW'  
 END  
 ELSE If(@productType = 4) -- For Activity Product Type is 4  
 BEGIN  
    
    Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
    Where docTypeCode = 'AC'  
 END
 ELSE If(@productType = 11) -- For FixedDeposit Product Type is 11 
 BEGIN  
    
    Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
    Where docTypeCode = 'FD'  
 END  
    ELSE If(@productType = 6) -- For FixedDeposit Product Type is 11 
 BEGIN  
    
    Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
    Where docTypeCode = 'SS'  
 END  
 ELSE If(@productType = 5) -- For Insurance Product Type is 5
 BEGIN  
    Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
    Where docTypeCode = 'IN'  
 END  
ELSE If(@productType = 9) -- For Transfers Product Type is 9 
 BEGIN  
    Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
    Where docTypeCode = 'TF'   
 END  
 ELSE If(@productType = 13) -- For Fleet Product Type is 13
 BEGIN  
    Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
    Where docTypeCode = 'FT'   
 END 
ELSE If(@productType in (7,8,10)) -- For Sightseeings,Train ,SMS & MobileRecharge  
 BEGIN  
  
    Update [BKE_Document_Sequence_Number] Set lastSequenceNUmber = @maxValue  
    Where docTypeCode = 'HW'  
 END  
 
 --Deduct the Agent balance after Invoice generation

    
 --Return the updated agency balance
 SET @P_RET_AGENT_BALANCE = (SELECT [dbo].[getAgentBalance](@agencyId,0,'N') )
 
 RETURN
GO
/****** Object:  StoredProcedure [dbo].[Act_Service_AddActivityTransactionHeader]    Script Date: 05/05/2017 12:30:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Act_Service_AddActivityTransactionHeader]
(			
           @ActivityId bigint
           ,@TripId nvarchar(25)
           ,@TransactionDate datetime
           ,@Adult int
           ,@Child int
           ,@Infant int
           ,@Booking datetime
           ,@TotalPrice numeric(18,4)
           ,@CreatedBy bigint
           ,@CreatedDate datetime           
           ,@Status nvarchar(10)  
           ,@AgencyId int         
           ,@TransactionType nvarchar(10)
           ,@ATHId bigint output
           ,@P_RET_AGENT_BALANCE decimal output
           ,@LocationId bigint
           ,@ActivityName nvarchar(50)
           ,@IsFixedDeparture nvarchar(1)
           ,@RoomCount bigint=null
           ,@QuotedStatus nvarchar=null
           ,@ATID Bigint
           ,@PaymentStatus int=0
           ,@CustomerId bigint
           
           
)
AS
BEGIN
IF (@ATID=-1)
BEGIN
Declare @BkgRef nvarchar(50)
Declare @Year varchar(2)
Declare @month varchar(2)
Declare @Id int


Set @year = SUBSTRING(CONVERT(VARCHAR,DATEPART(YEAR,GETDATE()),107),3,2)

Set @month = CONVERT(VARCHAR,DATEPART(MONTH,GETDATE()),103)
if LEN(@month) = 1
Begin
	set @month = '0'+@month
End

Select @Id=MAX(ATHID) from ACT_Activity_Transaction_Header

Set @TripId = @Id + 1
if (@IsFixedDeparture='Y')
begin
Set @BkgRef = 'FD/'+ @year + @month + @TripId

end
else
begin
Set @BkgRef = 'AD/'+ @year + @month + @TripId
end

INSERT INTO ACT_Activity_Transaction_Header
           (ActivityId
           ,TripId
           ,TransactionDate
           ,Adult
           ,Child
           ,Infant
           ,Booking
           ,TotalPrice
           ,CreatedBy
           ,CreatedDate           
           ,Status           
           ,AgencyId
           ,CustomerId
           ,TransactionType
           ,LocationId
           ,ActivityName
           ,isFixedDeparture
           ,PaymentStatus
           ,RoomCount
           ,QuotedStatus)
     VALUES
           (@ActivityId		
           ,@BkgRef			
           ,@TransactionDate
           ,@Adult			
           ,@Child			
           ,@Infant			
           ,@Booking		
           ,@TotalPrice		
           ,@CreatedBy		
           ,@CreatedDate	           
           ,@Status			            
           ,@AgencyId
           ,@CustomerId		
           ,@TransactionType
           ,@LocationId
           ,@ActivityName
           ,@IsFixedDeparture
           ,@PaymentStatus
           ,@RoomCount
           ,@QuotedStatus)
           
	Select @ATHId = @@IDENTITY from ACT_Activity_Transaction_Header
	
	if(@IsFixedDeparture ='N' AND @TransactionType='B2B')
	begin
		--Deduct the Agent balance after Invoice generation
		UPDATE CT_T_AGENT_MASTER
		SET  agent_current_balance = agent_current_balance - @TotalPrice
		WHERE agent_id = @AgencyId
	    
		--Return the updated agency balance
		SET @P_RET_AGENT_BALANCE = (SELECT [dbo].[getAgentBalance](@AgencyId,0,'N') )
	end
 
END
ELSE
BEGIN
  UPDATE ACT_Activity_Transaction_Header
  SET 
           ActivityId=@ActivityId
           ,TransactionDate=@TransactionDate
           ,Adult=@Adult
           ,Child=@Child
           ,Infant=@Infant
           ,Booking=@Booking
           ,TotalPrice=@TotalPrice
           ,LastModifiedBy=@CreatedBy
           ,LastModifiedDate=getdate()           
           ,Status =@Status          
           ,AgencyId=@AgencyId
           ,CustomerId=@CustomerId
           ,TransactionType=@TransactionType
           ,LocationId=@LocationId
           ,ActivityName=@ActivityName
           ,isFixedDeparture=@IsFixedDeparture
           ,PaymentStatus=@PaymentStatus
           ,RoomCount=@RoomCount
           ,QuotedStatus=@QuotedStatus
           where ATHId=@ATID
           Select @ATHId = @ATID
END
END
GO


CREATE PROCEDURE Act_Service_agent_balance_update
(
 @p_agent_id int,
 @p_trans_amount decimal(18,2),
 @p_modified_by int,
 @p_current_balance decimal(18,2) OUTPUT
 
)
AS
BEGIN
 if(@p_trans_amount!=0)
 Begin
  Update CT_T_AGENT_MASTER set agent_current_balance = agent_current_balance + @p_trans_amount,
  agent_modified_by=@p_modified_by,agent_modified_on = GETDATE()
  WHERE agent_id = @p_agent_id;
 End
  
  set @p_current_balance =(select agent_current_balance from ct_t_agent_master WHERE agent_id = @p_agent_id);
  
 
END

Create PROCEDURE Act_Service_agent_getdata
@P_AGENT_ID  INT

AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 SELECT [agent_company_id]
   ,[agent_id]
   ,[agent_code]
   ,[agent_name]
   ,[agent_address]
   ,[agent_city]
   ,[agent_state]
   ,[agent_country]
   ,[agent_po_box]
   ,[agent_phone1]
   ,[agent_phone2]
   ,[agent_fax]
   ,[agent_telex]
   ,[agent_email1]
   ,[agent_email2]
   ,[agent_alert_balance]
   ,[agent_website]
   ,[agent_img_filename]
   ,[n_flex_1]
   ,[n_flex_2]
   ,[agent_alert_balance]
   ,[agent_reduce_balance]  --Status
   ,[agent_receipt_total]
   ,[agent_current_balance] 
   ,[agent_license_no]
   ,[agent_license_exp_date] 
   ,[agent_passport_no] 
   ,[agent_passport_issue_date] 
   ,[agent_passport_expiry_date] 
   ,[agent_nationality] 
   ,[agent_guarantor_status] 
   ,[agent_visa_submission_status]
   ,[agent_status]
   ,[agent_email_content1]
   ,[agent_email_content2]
   ,[agent_created_by] 
   ,[agent_currency]
   ,[agent_block_status]
   ,[agent_remarks]
   ,agent_parent_id
   ,agent_product
   ,agent_air_markup_type
   ,add_pax_details
   ,agent_decimal
    ,agent_type
    ,agent_payment_mode
    ,agent_ticket_right
  FROM [dbo].[CT_T_AGENT_MASTER] 
  WHERE [agent_id] = @P_AGENT_ID
END
