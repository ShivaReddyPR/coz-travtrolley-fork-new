USE [COZMOVMSB2BLiveTest-1]
GO

/****** Object:  Table [dbo].[BKE_AirLoginSessions]    Script Date: 05/05/2017 13:23:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BKE_AirLoginSessions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[logintime] [datetime] NULL,
	[sessionid] [nvarchar](40) NULL,
	[isactive] [bit] NULL,
	[createdon] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

