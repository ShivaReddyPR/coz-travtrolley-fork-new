﻿using Cozmo.Api.Application.Models;
using Cozmo.Insurance.Application.BusinessOps;
using Cozmo.Insurance.Application.Models;
using MediatR;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Cozmo.Insurance.Api.Controllers
{
    /// <summary>
    /// Insurance controller to get/book insurance from various api sources
    /// </summary>
    [Authorize]
    [RoutePrefix("api/insurance")]
    public class InsuranceController : ApiController
    {
        private readonly IMediator _mediator;

        private readonly string _traceId;

        private readonly string _reqIP;

        /// <summary>
        /// Insurance constructor
        /// </summary>
        /// <param name="mediator"></param>
        public InsuranceController(IMediator mediator)
        {
            _mediator = mediator;
            var KeyStartPos = HttpContext.Current.Request.Headers.ToString().IndexOf("&Authorization=");
            _traceId = HttpContext.Current.Request.Headers.ToString().Substring(KeyStartPos + 15, 43).Replace("Bearer+", string.Empty).Trim();
            _reqIP = HttpContext.Current.Request.UserHostAddress;
        }

        /// <summary>
        /// Gets the insurance plans using the plans request
        /// </summary>
        /// <param name="InsuranceApiReq"></param>
        [HttpPost, Route("getInsurancePlans")]
        public async Task<IHttpActionResult> GetInsurancePlans([FromBody]GetInsurancePlans.InsurancePlansReq InsuranceApiReq)
        {
            ApiLogs.Save(InsuranceApiReq.ApiReqInfo.AgentRefKey, _traceId, 5, (int)InsuranceApiCall.InsurancePlansReq, JsonConvert.SerializeObject(InsuranceApiReq), _reqIP);

            InsuranceApiReq.ApiReqInfo.SSRefKey = _traceId + (string.IsNullOrEmpty(InsuranceApiReq.ApiReqInfo.SSRefKey) ? string.Empty : "^" + InsuranceApiReq.ApiReqInfo.SSRefKey);
            InsuranceApiReq.ApiReqInfo.SourceIP = _reqIP;

            var result = await _mediator.Send(InsuranceApiReq);

            ApiLogs.Save(InsuranceApiReq.ApiReqInfo.AgentRefKey, _traceId, 5, (int)InsuranceApiCall.InsurancePlansResp,
                result.Errors.Count() > 0 ? "Error : " + JsonConvert.SerializeObject(result.Errors) : "Results sent", _reqIP);

            return Ok(result);
        }

        /// <summary>
        /// Gets the insurance purchase confirmation 
        /// </summary>
        /// <param name="InsuranceApiReq"></param>
        [HttpPost, Route("insurancePurchase")]
        public async Task<IHttpActionResult> InsurancePurchase([FromBody]InsurancePurchase.InsurancePurchaseReq InsuranceApiReq)
        {
            ApiLogs.Save(InsuranceApiReq.ApiReqInfo.AgentRefKey, _traceId, 5, (int)InsuranceApiCall.InsurancePurchaseReq, JsonConvert.SerializeObject(InsuranceApiReq), _reqIP);
            InsuranceApiReq.ApiReqInfo.SSRefKey = _traceId;
            InsuranceApiReq.ApiReqInfo.SourceIP = _reqIP;

            var result = await _mediator.Send(InsuranceApiReq);

            ApiLogs.Save(InsuranceApiReq.ApiReqInfo.AgentRefKey, _traceId, 5, (int)InsuranceApiCall.InsurancePurchaseResp, JsonConvert.SerializeObject(result), _reqIP);

            return Ok(result);
        }
    }
}
