﻿using Cozmo.Api.Application.Models;
using Cozmo.Insurance.Application.Models;
using CT.BookingEngine;
using CT.BookingEngine.Insurance;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Insurance.Application.BusinessOps
{
    public class GetInsurancePlans
    {
        /// <summary>
        /// Query request to get insurance plans
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class InsurancePlansReq : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public InsuranceRequest ReqInsurancePlans { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<InsurancePlansReq, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(InsurancePlansReq clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine();

                try
                {
                    ApiCommon.ValApiReq(clsQuery.ApiReqInfo);
                    //Cozmo.Api.Application.Models.ApiCommon.save

                    InsuranceValidations.InsurancePlanReq(clsQuery.ReqInsurancePlans);

                    clsMS.SettingsLoginInfo = ApiCommon.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey, (int)ProductType.Insurance);
                    clsMS.SessionId = clsQuery.ApiReqInfo.SSRefKey;

                    var liConfigs = ApiConfigs.GetApiConfigs(clsMS.SettingsLoginInfo.AgentId, 0, string.Empty, string.Empty);

                    if (liConfigs != null && liConfigs.Count > 0 && clsQuery.ReqInsurancePlans.TransType != "B2B")
                        clsQuery.ReqInsurancePlans.TransType = liConfigs.Where(x => x.AC_AppKey.ToLower() == "transtype").Select(x => x.AC_AppValue).FirstOrDefault();

                    clsQuery.ReqInsurancePlans.CountryCode = clsQuery.ReqInsurancePlans.DepartureCountryCode;
                    clsQuery.ReqInsurancePlans.CultureCode = string.IsNullOrEmpty(clsQuery.ReqInsurancePlans.CultureCode) ? "EN" : clsQuery.ReqInsurancePlans.CultureCode;
                    clsQuery.ReqInsurancePlans.TransType = string.IsNullOrEmpty(clsQuery.ReqInsurancePlans.TransType) ? "API" : clsQuery.ReqInsurancePlans.TransType;

                    ApiCommon.SaveAPICache(clsQuery.ApiReqInfo.SSRefKey, clsQuery.ReqInsurancePlans, "InsurancePlanReq");

                    var insurancePlans = clsMS.GetAvailablePlans(clsQuery.ReqInsurancePlans);

                    if (insurancePlans != null)
                        ApiCommon.SaveAPICache(clsQuery.ApiReqInfo.SSRefKey, insurancePlans, "InsurancePlanResp");

                    if (clsQuery.ReqInsurancePlans.TransType == "B2B")
                        return await Task.FromResult(new ApplicationResponse(insurancePlans));
                    else
                    {
                        if (!string.IsNullOrEmpty(insurancePlans.ErrorMessage))
                            throw new ApiReqException(910, insurancePlans.Error + "-" + insurancePlans.ErrorMessage);

                        var liPlans = insurancePlans.OTAPlans != null && insurancePlans.OTAPlans.Count > 0 ? insurancePlans.OTAPlans : new List<InsurancePlanDetails>();

                        if (insurancePlans.UpsellPlans != null && insurancePlans.UpsellPlans.Count > 0)
                            liPlans.AddRange(insurancePlans.UpsellPlans);

                        return await Task.FromResult(
                            new ApplicationResponse(ApiCommon.IgnoreProps<InsurancePlanDetails>(liPlans, (int)ProductType.Insurance, ApiObjects.InsPlans, clsMS.SettingsLoginInfo.AgentId))
                        );
                    }
                }
                catch (ApiReqException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.InsurnaceApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -5,
                       "Insurance Api: GetInsurancePlans handler class. Error: " + ex.ErrorCode.ToString() + "-" + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail("Insurance", ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError(ex.ErrorCode.ToString(), ex.Message);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.InsurnaceApi, CT.Core.Severity.High, -5,
                       "Insurance Api: GetInsurancePlans handler class. Error: " + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail("Insurance", ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError("Insurance ApiException", "Failed to get Insurance plans, please contact admin.");
                }
            }
        }
    }
}
