﻿using Cozmo.Api.Application.Models;
using Cozmo.Insurance.Application.Models;
using CT.BookingEngine;
using CT.BookingEngine.Insurance;
using CT.MetaSearchEngine;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cozmo.Insurance.Application.BusinessOps
{
    public class InsurancePurchase
    {
        /// <summary>
        /// Query request to get insurance plans
        /// </summary>
        /// <seealso cref="MediatR.IRequest{ApplicationResponse}" />
        public class InsurancePurchaseReq : IRequest<ApplicationResponse>
        {
            public ApiReqInfo ApiReqInfo { get; set; }

            public InsuranceHeader ReqInsurancePurchase { get; set; }
        }

        /// <summary>
        /// Actions to perform for this <see cref="Query"/>.
        /// </summary>
        /// <seealso cref="Query" />
        public class Handler : IRequestHandler<InsurancePurchaseReq, ApplicationResponse>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Handler"/> class.
            /// </summary>
            public Handler()
            {

            }

            /// <summary>
            /// Handles the specified <see cref="Query"/>.
            /// </summary>
            /// <param name="clsQuery">The request.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            /// <returns></returns>
            public async Task<ApplicationResponse> Handle(InsurancePurchaseReq clsQuery, CancellationToken cancellationToken)
            {
                MetaSearchEngine clsMS = new MetaSearchEngine();

                try
                {
                    ApiCommon.ValApiReq(clsQuery.ApiReqInfo);

                    InsuranceValidations.InsurancePurchaseReq(clsQuery.ReqInsurancePurchase);

                    clsMS.SettingsLoginInfo = ApiCommon.GetLoginInfo(clsQuery.ApiReqInfo.AgentRefKey, clsQuery.ApiReqInfo.LocRefKey, (int)ProductType.Insurance);
                    clsMS.SessionId = clsQuery.ApiReqInfo.SSRefKey;

                    var cacheData = ApiCommon.GetAPICache(clsMS.SessionId, "InsurancePlanReq");
                    var insReq = cacheData != null && cacheData.Length > 0 ? (InsuranceRequest)CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    InsuranceValidations.InsurancePlanReq(insReq);

                    cacheData = ApiCommon.GetAPICache(clsMS.SessionId, "InsurancePlanResp");
                    var insPlansResp = cacheData != null && cacheData.Length > 0 ? (InsuranceResponse)CT.Core.GenericStatic.GetObjectWithByteArray(cacheData) : null;

                    ApiCommon.ValApiSession(940, insPlansResp);

                    // Prepare plans object
                    for (int i = 0; i < clsQuery.ReqInsurancePurchase.InsPlans.Count; i++)
                    {
                        var insPlan = insPlansResp.OTAPlans.Where(x => x.Code == clsQuery.ReqInsurancePurchase.InsPlans[i].InsPlanCode && x.SSRFeeCode == clsQuery.ReqInsurancePurchase.InsPlans[i].SSRFeeCode).FirstOrDefault();
                        insPlan = insPlan != null ? insPlan : 
                            insPlansResp.UpsellPlans.Where(x => x.Code == clsQuery.ReqInsurancePurchase.InsPlans[i].InsPlanCode && x.SSRFeeCode == clsQuery.ReqInsurancePurchase.InsPlans[i].SSRFeeCode).FirstOrDefault();

                        if (insPlan != null)
                            clsQuery.ReqInsurancePurchase.InsPlans[i] = InsurancePlan.GetInsurancePlan(insPlan, clsMS.SettingsLoginInfo.DecimalValue);
                    }

                    // Prepare header info
                    clsQuery.ReqInsurancePurchase.CountryCode = insReq.CountryCode;
                    clsQuery.ReqInsurancePurchase.CultureCode = insReq.CultureCode;
                    clsQuery.ReqInsurancePurchase.DepartureCountryCode = insReq.DepartureCountryCode;
                    clsQuery.ReqInsurancePurchase.ArrivalCountryCode = insReq.ArrivalCountryCode;
                    clsQuery.ReqInsurancePurchase.DepartureStationCode = insReq.DepartureStationCode;
                    clsQuery.ReqInsurancePurchase.ArrivalStationCode = insReq.ArrivalStationCode;
                    clsQuery.ReqInsurancePurchase.DepartureAirlineCode = insReq.DepartureAirlineCode;
                    clsQuery.ReqInsurancePurchase.DepartureFlightNo = insReq.DepartureFlightNo;
                    clsQuery.ReqInsurancePurchase.DepartureDateTime = insReq.DepartureDateTime;
                    clsQuery.ReqInsurancePurchase.ReturnFlightNo = insReq.ReturnFlightNo;
                    clsQuery.ReqInsurancePurchase.ReturnDateTime = insReq.ReturnDateTime;
                    clsQuery.ReqInsurancePurchase.Adults = insReq.Adults;
                    clsQuery.ReqInsurancePurchase.Childs = insReq.Childs;
                    clsQuery.ReqInsurancePurchase.Infants = insReq.Infants;
                    clsQuery.ReqInsurancePurchase.Currency = insReq.CurrencyCode;
                    clsQuery.ReqInsurancePurchase.TransType = insReq.TransType;
                    clsQuery.ReqInsurancePurchase.PaymentMode = ModeOfPayment.Credit;
                    clsQuery.ReqInsurancePurchase.CreatedBy = clsQuery.ReqInsurancePurchase.CreatedBy >= 0 ? (int)clsMS.SettingsLoginInfo.UserID : clsQuery.ReqInsurancePurchase.CreatedBy;
                    clsQuery.ReqInsurancePurchase.OutputVATAmount = clsQuery.ReqInsurancePurchase.InsPlans.Sum(x => x.OutputVATAmount);
                    clsQuery.ReqInsurancePurchase.TotalAmount = clsQuery.ReqInsurancePurchase.InsPlans.Sum(x => x.NetAmount + x.Markup + x.InputVATAmount + x.OutputVATAmount - x.Discount);
                    clsQuery.ReqInsurancePurchase.SessionId = insPlansResp.SessionId;
                    clsQuery.ReqInsurancePurchase.Product = "S";

                    if (clsMS.SettingsLoginInfo.IsOnBehalfOfAgent)
                    {
                        clsQuery.ReqInsurancePurchase.AgentId = clsMS.SettingsLoginInfo.OnBehalfAgentID;
                        clsQuery.ReqInsurancePurchase.LocationId = clsMS.SettingsLoginInfo.OnBehalfAgentLocation;
                    }
                    else
                    {
                        clsQuery.ReqInsurancePurchase.AgentId = clsMS.SettingsLoginInfo.AgentId;
                        clsQuery.ReqInsurancePurchase.LocationId = (int)clsMS.SettingsLoginInfo.LocationID;
                    }

                    var req = clsQuery.ReqInsurancePurchase;
                    clsMS.ConfirmPolicyPurchase(ref req);

                    if (insReq.TransType != "B2B")
                    {
                        if (!string.IsNullOrEmpty(req.Error) && req.Error != "0")
                            throw new ApiReqException(940, req.Error);

                        List<InsPurchaseResponse> purchaseResp = new List<InsPurchaseResponse>();                        

                        for (int i = 0; i < req.InsPlans.Count; i++)
                        {
                            var paxDetails = req.InsPassenger;
                            for (int p = 0; p < req.InsPassenger.Count; p++)
                            {
                                paxDetails[p].PolicyNo = req.InsPassenger[p].PolicyNo.Split('|')[i];
                                paxDetails[p].PolicyUrlLink = req.InsPassenger[p].PolicyUrlLink.Split('|')[i];
                            }

                            purchaseResp.Add(new InsPurchaseResponse
                            {
                                PlanErrorCode = req.InsPlans[i].ErrorCode,
                                PlanErrorMessage = req.InsPlans[i].ErrorMessage,
                                PlanStatus = req.InsPlans[i].PlanStatus == InsuranceBookingStatus.Confirmed ? req.InsPlans[i].PlanStatus.ToString() : InsuranceBookingStatus.Failed.ToString(),
                                ItineraryID = req.InsPlans[i].ItineraryID,
                                PolicyNo = req.InsPlans[i].PolicyNo,
                                PolicyPurchasedDate = req.InsPlans[i].PolicyPurchasedDate,
                                ProposalState = req.InsPlans[i].ProposalState,
                                InsPlanCode = req.InsPlans[i].InsPlanCode,
                                SSRFeeCode = req.InsPlans[i].SSRFeeCode,
                                InsPassenger = paxDetails
                            });
                        }

                        return await Task.FromResult(new ApplicationResponse(purchaseResp));
                    }
                    else
                        return await Task.FromResult(new ApplicationResponse(req));
                }
                catch (ApiReqException ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.InsurnaceApi, CT.Core.Severity.High, clsMS.SettingsLoginInfo != null ? (int)clsMS.SettingsLoginInfo.UserID : -5,
                       "Insurance Api: InsurancePurchase handler class. Error: " + ex.ErrorCode.ToString() + "-" + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail("Insurance", ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError(ex.ErrorCode.ToString(), ex.Message);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.InsurnaceApi, CT.Core.Severity.High, -5,
                       "Insurance Api: InsurancePurchase handler class. Error: " + ex.ToString(), clsQuery.ApiReqInfo != null ? clsQuery.ApiReqInfo.SourceIP : string.Empty);

                    clsMS.SettingsLoginInfo = clsMS.SettingsLoginInfo == null ? new CT.TicketReceipt.BusinessLayer.LoginInfo() : clsMS.SettingsLoginInfo;
                    ApiCommon.SendErrorMail("Insurance", ex.ToString(), clsMS.SettingsLoginInfo.AgentId, (int)clsMS.SettingsLoginInfo.UserID);

                    return new ApplicationResponse().AddError("Insurance ApiException", "Failed to purchase Insurance, please contact admin.");
                }
            }
        }
    }
}
