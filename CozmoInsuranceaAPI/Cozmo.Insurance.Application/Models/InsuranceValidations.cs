﻿using Cozmo.Api.Application.Infrastructure;
using Cozmo.Api.Application.Models;
using CT.BookingEngine;
using CT.BookingEngine.Insurance;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Cozmo.Insurance.Application.Models
{
    public static class InsuranceValidations
    {
        /// <summary>
        /// To validate hotel session
        /// </summary>
        /// <param name="hotelReq"></param>
        public static void HotelSession(int errorCode, object hotelReq)
        {
            if (hotelReq == null)
                throw new ApiReqException(errorCode, "Api session expired, please relogin and start booking.");
        }

        /// <summary>
        /// To validate api req info params
        /// </summary>
        /// <param name="apiReqInfo"></param>
        public static void ApiReq(ApiReqInfo apiReqInfo)
        {
            if (apiReqInfo == null)
                throw new ApiReqException(800, "Api request info missing.");

            if (string.IsNullOrEmpty(apiReqInfo.AgentRefKey))
                throw new ApiReqException(801, "Agent reference key missing in Api request info.");
        }

        /// <summary>
        /// To validate insurance plans req info params
        /// </summary>
        /// <param name="insuranceReq"></param>
        public static void InsurancePlanReq(InsuranceRequest insuranceReq)
        {
            if (insuranceReq == null)
                throw new ApiReqException(900, "Invalid insurance request.");

            if (string.IsNullOrEmpty(insuranceReq.DepartureCountryCode) || string.IsNullOrEmpty(insuranceReq.ArrivalCountryCode))
                throw new ApiReqException(901, "Origin/Destination info missing.");

            if (string.IsNullOrEmpty(insuranceReq.DepartureDateTime) || string.IsNullOrEmpty(insuranceReq.ReturnDateTime))
                throw new ApiReqException(902, "Insurance request departure date/return date missing.");

            if (!DateTime.TryParseExact(insuranceReq.DepartureDateTime, StaticConstants.formats, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime dateValue))
                throw new ApiReqException(903, "Invalid departure date.");

            if (!DateTime.TryParseExact(insuranceReq.ReturnDateTime, StaticConstants.formats, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime dateValue1))
                throw new ApiReqException(904, "Invalid return date.");

            if (Convert.ToDateTime(insuranceReq.DepartureDateTime) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
                throw new ApiReqException(905, "Insurance departure date should be greater than or equal to current date.");

            if (Convert.ToDateTime(insuranceReq.DepartureDateTime) >= Convert.ToDateTime(insuranceReq.ReturnDateTime))
                throw new ApiReqException(906, "Insurance departure date should be less than return date.");

            if (insuranceReq.Adults == 0 && insuranceReq.Childs == 0)
                throw new ApiReqException(907, "No of pax should not be 0.");
        }

        /// <summary>
        /// To validate insurance purchase req info params
        /// </summary>
        /// <param name="insPurchaseReq"></param>
        public static void InsurancePurchaseReq(InsuranceHeader insPurchaseReq)
        {
            if (insPurchaseReq == null)
                throw new ApiReqException(920, "Invalid insurance request.");

            //if (string.IsNullOrEmpty(insPurchaseReq.DepartureCountryCode) || string.IsNullOrEmpty(insPurchaseReq.ArrivalCountryCode))
            //    throw new ApiReqException(921, "Origin/Destination info missing.");

            //if (string.IsNullOrEmpty(insPurchaseReq.DepartureDateTime) || string.IsNullOrEmpty(insPurchaseReq.ReturnDateTime))
            //    throw new ApiReqException(922, "Insurance departure date/return date missing.");

            //if (!DateTime.TryParseExact(insPurchaseReq.DepartureDateTime, StaticConstants.formats, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime dateValue))
            //    throw new ApiReqException(923, "Invalid departure date.");

            //if (!DateTime.TryParseExact(insPurchaseReq.ReturnDateTime, StaticConstants.formats, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime dateValue1))
            //    throw new ApiReqException(924, "Invalid return date.");

            //if (Convert.ToDateTime(insPurchaseReq.DepartureDateTime) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
            //    throw new ApiReqException(925, "Insurance departure date should be greater than or equal to current date.");

            //if (Convert.ToDateTime(insPurchaseReq.DepartureDateTime) >= Convert.ToDateTime(insPurchaseReq.ReturnDateTime))
            //    throw new ApiReqException(926, "Insurance departure date should be less than return date.");

            //if (insPurchaseReq.Adults == 0 && insPurchaseReq.Childs == 0)
            //    throw new ApiReqException(907, "No of pax should not be 0.");

            if (string.IsNullOrEmpty(insPurchaseReq.PNR))
                throw new ApiReqException(921, "Insurance travel PNR missing.");

            if (insPurchaseReq.InsPassenger == null || insPurchaseReq.InsPassenger.Count == 0)
                throw new ApiReqException(928, "Passenger info missing");

            string errMsg = string.Empty; string[] titles = { "Mr", "Ms", "Mrs", "Dr" }; Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            for (int i = 0; i < insPurchaseReq.InsPassenger.Count; i++)
            {
                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].Title) ? "Passenger - " + (i + 1).ToString() + "Title Mandatory" + Environment.NewLine : string.Empty;
                errMsg += (string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].Title) ||  !titles.Contains(insPurchaseReq.InsPassenger[i].Title)) ? 
                    "Passenger - " + (i + 1).ToString() + "Invalid Title" + Environment.NewLine : string.Empty;
                
                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].FirstName) ? "Passenger - " + (i + 1).ToString() + "First Name Mandatory" + Environment.NewLine : string.Empty;
                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].LastName) ? "Passenger - " + (i + 1).ToString() + "Last Name Mandatory" + Environment.NewLine : string.Empty;
                //errMsg += insPurchaseReq.InsPassenger[i].Gender == null ? "Passenger - " + (i + 1).ToString() + "Title Mandatory" + Environment.NewLine : string.Empty;
                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].Nationality) ? "Passenger - " + (i + 1).ToString() + "Nationality Mandatory" + Environment.NewLine : string.Empty;
                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].PhoneNumber) ? "Passenger - " + (i + 1).ToString() + "Phone Number Mandatory" + Environment.NewLine : string.Empty;
                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].Country) ? "Passenger - " + (i + 1).ToString() + "Country Mandatory" + Environment.NewLine : string.Empty;
                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].City) ? "Passenger - " + (i + 1).ToString() + "City Mandatory" + Environment.NewLine : string.Empty;
                //errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].DocumentType) ? "Passenger - " + (i + 1).ToString() + "Title Mandatory" + Environment.NewLine : string.Empty;
                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].DocumentNo) ? "Passenger - " + (i + 1).ToString() + "Document Number Mandatory" + Environment.NewLine : string.Empty;

                errMsg += string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].Email) ? "Passenger - " + (i + 1).ToString() + "Email Mandatory" + Environment.NewLine : string.Empty;
                errMsg += !string.IsNullOrEmpty(insPurchaseReq.InsPassenger[i].Email) && !regex.Match(insPurchaseReq.InsPassenger[i].Email).Success ?
                    "Passenger - " + (i + 1).ToString() + "Invalid Email" + Environment.NewLine : string.Empty;

                errMsg += insPurchaseReq.InsPassenger[i].DOB == DateTime.MinValue ? "Passenger - " + (i + 1).ToString() + "Invalid Date Of Birth" + Environment.NewLine : string.Empty;
                if (!string.IsNullOrEmpty(errMsg))
                    throw new ApiReqException(930, errMsg);
            }
        }
    }
}
