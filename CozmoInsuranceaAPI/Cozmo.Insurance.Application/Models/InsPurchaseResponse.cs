﻿using CT.BookingEngine.Insurance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Insurance.Application.Models
{
    [Serializable]
    public class InsPurchaseResponse
    {
        public string PlanErrorCode { get; set; }
        public string PlanErrorMessage { get; set; }
        public string PlanStatus { get; set; }
        public string ItineraryID { get; set; }
        public string PolicyNo { get; set; }
        public DateTime PolicyPurchasedDate { get; set; }
        public string ProposalState { get; set; }
        public string InsPlanCode { get; set; }
        public string SSRFeeCode { get; set; }
        public List<InsurancePassenger> InsPassenger { get; set; }
    }
}
