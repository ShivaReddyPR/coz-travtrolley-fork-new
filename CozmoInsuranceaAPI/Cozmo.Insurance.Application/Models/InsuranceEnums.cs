﻿namespace Cozmo.Insurance.Application.Models
{
    public enum InsuranceApiCall
    {
        InsurancePlansReq = 10,
        InsurancePlansResp = 11,
        InsurancePurchaseReq = 20,
        InsurancePurchaseResp = 21
    }
}
