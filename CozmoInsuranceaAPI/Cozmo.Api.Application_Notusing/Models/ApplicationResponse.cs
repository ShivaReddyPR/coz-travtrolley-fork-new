﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Cozmo.Api.Application.Models
{
    /// <summary>
    /// A wrapper class for returning both errors or the result
    /// </summary>
    public class ApplicationResponse
    {
        private readonly IList<KeyValuePair<string, string>> _messages = new List<KeyValuePair<string, string>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationResponse" /> class.
        /// </summary>
        public ApplicationResponse() => Errors = new ReadOnlyCollection<KeyValuePair<string, string>>(_messages);

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationResponse" /> class.
        /// </summary>
        /// <param name="result">The result.</param>
        public ApplicationResponse(object result) : this() => Result = result;

        /// <summary>
        /// Gets the errors.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        public IEnumerable<KeyValuePair<string, string>> Errors { get; }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public object Result { get; }

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="key">The error message key.</param>
        /// <param name="value">The error message value.</param>
        public ApplicationResponse AddError(string key, string value)
        {
            var message = new KeyValuePair<string, string>(key, value);
            return AddError(message);
        }

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="message">The message.</param>
        public ApplicationResponse AddError(KeyValuePair<string, string> message)
        {
            _messages.Add(message);
            return this;
        }
    }
}
