﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cozmo.Api.Application.Models
{
    public class ApiLogs
    {
        private static string ConnectionString = ConfigurationManager.AppSettings["dbSettingApi"];

        public int AL_ID { get; set;}
        public string AL_User { get; set;}
        public string AL_Session { get; set; }
        public int AL_Product { get; set; }
        public int AL_Type { get; set; }
        public string AL_Data { get; set; }
        public DateTime AL_EventTimeUTC { get; set; }

        /// <summary>
        /// To save api logs
        /// </summary>
        /// <param name="sAL_User"></param>
        /// <param name="sAL_Session"></param>
        /// <param name="iAL_Product"></param>
        /// <param name="iAL_Type"></param>
        /// <param name="sAL_Data"></param>
        /// <param name="sAL_ClientIP"></param>
        public static void Save(string sAL_User, string sAL_Session, int iAL_Product, int iAL_Type, string sAL_Data, string sAL_ClientIP)
        {
            SqlConnection con = GetSqlConnection();

            if (con == null)
                return;

            try
            {
                int AL_ID = 0;

                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@AL_ID", AL_ID));
                liParams.Add(new SqlParameter("@AL_User", sAL_User));
                liParams.Add(new SqlParameter("@AL_Session", sAL_Session));
                liParams.Add(new SqlParameter("@AL_Product", iAL_Product));
                liParams.Add(new SqlParameter("@AL_Type", iAL_Type));
                liParams.Add(new SqlParameter("@AL_Data", sAL_Data));
                liParams.Add(new SqlParameter("@AL_ClientIP", sAL_ClientIP));
                                
                SqlCommand cmd = con.CreateCommand();
                DBGateway.ExecuteNonQueryDetails(cmd, "usp_SaveApiLogs", liParams.ToArray());
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        /// <summary>
        /// To get sql connection
        /// </summary>
        /// <returns></returns>
        public static SqlConnection GetSqlConnection()
        {
            SqlConnection con = null;

            if (string.IsNullOrEmpty(ConnectionString))
                return con;

            con = new SqlConnection(ConnectionString);
            con.Open();
            return con;
        }
    }
}
