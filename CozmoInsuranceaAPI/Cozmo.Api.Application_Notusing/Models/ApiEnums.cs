﻿namespace Cozmo.Api.Application.Models
{
    public enum ApiObjects
    {
        HotelSearchResult = 1,
        RoomResult = 2,
        BookResponse = 3,
        InsPlans = 4
    }
}
