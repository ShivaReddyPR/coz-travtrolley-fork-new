﻿using Cozmo.Api.Application.Models;
using CT.TicketReceipt.BusinessLayer;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Cozmo.Api.Application.Infrastructure
{
    public class OauthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            await Task.Run(() => context.Validated());
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            string newSession = Guid.NewGuid().ToString();

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            ApiGeneral.GenerateLogs(true, false, JsonConvert.SerializeObject(new { Data = "User Name: (" + context.UserName + "), Password: (" + context.Password + ")" } ), 
                "ApiRequest_Login_" + newSession);
            ApiLogs.Save(context.UserName, newSession, 0, 0, "User Name: (" + context.UserName + "), Password: (" + context.Password + ")", context.Request.LocalIpAddress);

            string Message = ApiCommon.GetUserInfo(context.UserName, context.Password);
            
            if (!string.IsNullOrEmpty(Message))
            {
                ApiGeneral.GenerateLogs(true, false, JsonConvert.SerializeObject(new { Data = Message + " (" + context.Password + ")." }), "ApiResponse_Login_" + newSession);
                ApiLogs.Save(context.UserName, newSession, 0, 2, Message + " (" + context.Password + ").", context.Request.LocalIpAddress);

                context.SetError("invalid_grant", Message);
                return;
            }

            identity.AddClaim(new Claim(ClaimTypes.Role, "User"));
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim("LoggedOn", DateTime.Now.ToString()));
            identity.AddClaim(new Claim(StaticConstants.TraceType, newSession));

            ApiLogs.Save(context.UserName, newSession, 0, 1, "User Name: (" + context.UserName + "), Authentication Success, token genrated.", context.Request.LocalIpAddress);

            await Task.Run(() => context.Validated(identity));
        }
    }
}