﻿namespace Cozmo.Api.Application.Infrastructure
{
    /// <summary>
    /// Common static class to define constant strings
    /// </summary>
    public static class StaticConstants
    {
        public static string TraceType = "TraceId";
        public static string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                   "M/d/yyyy h:mm", "M/d/yyyy h:mm",
                   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm", "yyyy-MM-dd hh:mm:ss"};
    }
}
