﻿using System;

namespace Cozmo.Api.Application.Models
{
    public class ApiReqException : Exception
    {
        /// <summary>
        /// Exception number;
        /// </summary>
        private int errorCode;
        /// <summary>
        /// Gets the exception number.
        /// </summary>
        public int ErrorCode
        {
            get { return errorCode; }
        }

        public ApiReqException(string message)
            : base(message)
        {
            errorCode = 0;
        }

        public ApiReqException(int errorCode, string message)
            : base(message)
        {
            this.errorCode = errorCode;
        }
    }
}
