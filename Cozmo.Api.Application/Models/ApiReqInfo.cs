﻿using System;

namespace Cozmo.Api.Application.Models
{
    [Serializable]
    public class ApiReqInfo
    {
        public string SourceIP { get; set; }

        public string AgentRefKey { get; set; }

        public string SSRefKey { get; set; }

        public int LocRefKey { get; set; }
    }
}
