﻿using CT.Configuration;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Cozmo.Api.Application.Models
{
    public class ApiCommon
    {
        /// <summary>
        /// To get ignore properties for json serialization
        /// </summary>
        /// <param name="Product"></param>
        /// <param name="objName"></param>
        /// <param name="AgentId"></param>
        public static string GetUserInfo(string UserName, string Password)
        {
            string Message = string.Empty;
            try
            {
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@P_USER_LOGIN_NAME", UserName));
                liParams.Add(new SqlParameter("@P_USER_PASSWORD", Password));
                DataTable dt = DBGateway.FillDataTableSP("usp_GetApiUserInfo", liParams.ToArray());

                if (dt == null || dt.Rows.Count == 0)
                    Message = "User Name/Password incorrect.";
                else if (Convert.ToString(dt.Rows[0]["USER_STATUS"]) != "A")
                    Message = "User is inactive, please contact admin.";
            }
            catch (Exception ex)
            {

            }

            return Message;
        }

        /// <summary>
        /// To get ignore properties for json serialization
        /// </summary>
        /// <param name="Product"></param>
        /// <param name="objName"></param>
        /// <param name="AgentId"></param>
        public static DataTable GetIgnoreProps(int Product, int objName, int AgentId)
        {
            DataTable dt = new DataTable();
            try
            {
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@PI_Product", (int)Product));
                liParams.Add(new SqlParameter("@PI_Type", (int)objName));
                liParams.Add(new SqlParameter("@PE_AgentId", AgentId));

                dt = DBGateway.FillDataTableSP("usp_GetIgnoreProps", liParams.ToArray());
            }
            catch (Exception ex)
            {
                
            }

            return dt;
        }

        /// <summary>
        /// To ignore unwanted props and return the response
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="responseData"></param>
        /// <param name="Product"></param>
        /// <param name="objName"></param>
        /// <param name="AgentId"></param>
        /// <returns></returns>
        public static object IgnoreProps<T>(object responseData, int Product, ApiObjects objName, int AgentId)
        {
            object newObj = responseData;

            try
            {
                var serializerSettings = ApiCommon.GetJsonResolver<T>(Product, objName, AgentId);
                var json = JsonConvert.SerializeObject(responseData, serializerSettings);
                newObj = JsonConvert.DeserializeObject(json);
            }
            catch (Exception ex)
            {

            }

            return newObj;
        }

        /// <summary>
        /// To get json resolver with all expculded props
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Product"></param>
        /// <param name="objName"></param>
        /// <param name="AgentId"></param>
        /// <returns></returns>
        public static JsonSerializerSettings GetJsonResolver<T>(int Product, ApiObjects objName, int AgentId)
        {
            var newObj = new JsonSerializerSettings();

            try
            {
                var dtProps = GetIgnoreProps(Product, (int)objName, AgentId);

                if (dtProps != null && dtProps.Rows.Count > 0)
                {
                    var jsonResolver = new JsonSerializeContractor();

                    for (int i = 0; i < dtProps.Rows.Count; i++)
                        jsonResolver.IgnoreProperty(typeof(T), Convert.ToString(dtProps.Rows[i]["PI_Name"]));

                    newObj.ContractResolver = jsonResolver;
                }                
            }
            catch (Exception ex)
            {

            }

            return newObj;
        }

        /// <summary>
        /// To send error email to tech team
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="responseData"></param>
        /// <param name="Product"></param>
        /// <param name="objName"></param>
        /// <param name="AgentId"></param>
        /// <returns></returns>
        public static void SendErrorMail(string Product, string Error, int AgentId, int UserId)
        {            
            try
            {
                ConfigurationSystem con = new ConfigurationSystem();
                Hashtable hostPort = con.GetHostPort();

                EmailParams clsParams = new EmailParams();

                clsParams.FromEmail = hostPort["fromEmail"].ToString();
                clsParams.ToEmail = hostPort["HotelErrorNotificationMailingId"].ToString();                
                clsParams.Subject = Product + " API Error";
                clsParams.EmailBody = "Dear Support Team:<br /><br />" + clsParams.Subject + ", please see below details. <br /><br />";

                if (AgentId > 0 && UserId > 0)
                {
                    AgentMaster clsAM = new AgentMaster(AgentId);
                    UserMaster clsUM = new UserMaster(UserId);
                    clsParams.EmailBody += "<strong>Agent :</strong> " + clsAM.Name + "(" + clsAM.ID + ")<br /> <strong>User :</strong> " + 
                        clsUM.FirstName + " " + clsUM.LastName + "(" + clsUM.ID + ")" + "<br /><br />" ;
                }

                clsParams.EmailBody += Error + "<br /><br />";

                Email.Send(clsParams);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// To get login info
        /// </summary>
        /// <param name="ApiKey"></param>
        /// <param name="iOnBehalfLoc"></param>
        /// <returns></returns>
        public static LoginInfo GetLoginInfo(string ApiKey, int iOnBehalfLoc, int product)
        {
            LoginInfo clsLInfo = null;
            try
            {
                clsLInfo = LoginInfo.GetLoginInfo(ApiKey, product);
                clsLInfo.IsOnBehalfOfAgent = iOnBehalfLoc > 0;

                if (clsLInfo.IsOnBehalfOfAgent)
                {
                    LocationMaster clsLM = new LocationMaster(iOnBehalfLoc);
                    AgentMaster clsAM = new AgentMaster(clsLM.AgentId);

                    clsLInfo.IsOnBehalfOfAgent = true;
                    clsLInfo.OnBehalfAgentID = clsLM.AgentId;
                    clsLInfo.OnBehalfAgentLocation = iOnBehalfLoc;
                    clsLInfo.OnBehalfAgentCurrency = clsAM.AgentCurrency;
                    clsLInfo.OnBehalfAgentDecimalValue = clsAM.DecimalValue;
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = clsAM.AgentCurrency;
                    clsLInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                    clsLInfo.OnBehalfAgentSourceCredentials = AgentMaster.GetAirlineCredentials(clsLM.AgentId);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to get Login info from web API:" + ex.Message, "");
            }
            return clsLInfo;
        }
               
        /// <summary>
        /// To save api req/resp cache memeory data
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="dataObject"></param>
        /// <param name="ResultType"></param>
        public static void SaveAPICache(string sessionId, object dataObject, string ResultType)
        {
            if (sessionId == null || dataObject == null)
                return;

            try
            {
                byte[] data = GenericStatic.GetByteArrayWithObject(dataObject);
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@sessionId", sessionId));
                liParams.Add(new SqlParameter("@resultData", data));
                liParams.Add(DBGateway.GetParameter("ResulType", ResultType));

                DBGateway.ExecuteNonQuerySP("usp_AddHotelSearchResultCache", liParams.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To Get api req/resp cache memeory data
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="ResultType"></param>
        public static byte[] GetAPICache(string sessionId, string ResultType)
        {
            if (sessionId == null)
                throw new Exception("Session id missing to get hotel cache data.");

            byte[] data = new byte[0];
            try
            {
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@sessionId", sessionId));
                liParams.Add(DBGateway.GetParameter("ResulType", ResultType));

                var sqlReader = DBGateway.ExecuteReaderSP("usp_GetHotelSearchResultCache", liParams.ToArray(), DBGateway.GetConnection());

                ValApiSession(00006, sqlReader);

                if (sqlReader.Read())
                    data = (byte[])sqlReader["resultData"];

                ValApiSession(00007, data);

                sqlReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }
        
        /// <summary>
        /// To validate api session
        /// </summary>
        /// <param name="insReq"></param>
        public static void ValApiSession(int errorCode, object apiReq)
        {
            if (apiReq == null)
                throw new ApiReqException(errorCode, "Api session expired, please start new session.");
        }

        /// <summary>
        /// To validate api req info params
        /// </summary>
        /// <param name="apiReqInfo"></param>
        public static void ValApiReq(ApiReqInfo apiReqInfo)
        {
            if (apiReqInfo == null)
                throw new ApiReqException(800, "Api request info missing.");

            if (string.IsNullOrEmpty(apiReqInfo.AgentRefKey))
                throw new ApiReqException(801, "Agent reference key missing in Api request info.");
        }
    }
}
