﻿namespace Cozmo.Api.Application.Models
{
    /// <summary>
    /// To pass agent info in api request 
    /// </summary>
    public class ApiAgentInfo
    {
        public int AgentId { get; set; }

        public int LoginUserId { get; set; }

        public int OnBelahfAgentLoc { get; set; }
    
        public string IPAddress { get; set; }

        public int LoginUserCorpId { get; set; }
    }
}
