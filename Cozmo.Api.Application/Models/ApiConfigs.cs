﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Cozmo.Api.Application.Models
{
    public class ApiConfigs
    {
        public int AC_Id { get; set; }
        public int AC_ProductID { get; set; }
        public int AC_AgentID { get; set; }
        public string AC_Source { get; set; }
        public string AC_AppKey { get; set; }
        public string AC_AppValue { get; set; }
        public string AC_Description { get; set; }
        public string AC_BookingStatus { get; set; }
        public bool AC_Status { get; set; }
        public int AC_CreatedBy { get; set; }
        public DateTime AC_CreatedDate { get; set; }
        public int AC_ModifiedBy { get; set; }
        public DateTime AC_ModifiedDate { get; set; }

        /// <summary>
        /// To get api configs
        /// </summary>
        /// <param name="iAgentId"></param>
        /// <param name="iProductId"></param>
        /// <param name="Source"></param>
        /// <param name="ApiKey"></param>
        /// <returns></returns>
        public static List<ApiConfigs> GetApiConfigs(int iAgentId, int iProductId, string Source, string ApiKey)
        {
            List<ApiConfigs> liConfigs = new List<ApiConfigs>();
            try
            {
                List<SqlParameter> liParams = new List<SqlParameter>();
                liParams.Add(new SqlParameter("@AC_AgentID", iAgentId));
                liParams.Add(new SqlParameter("@AC_ProductID", iProductId));
                liParams.Add(DBGateway.GetParameter("AC_Source", Source));
                liParams.Add(DBGateway.GetParameter("AC_AppKey", ApiKey));
                DataTable dtcinfo = DBGateway.ExecuteQuery("usp_GetApiConfigs", liParams.ToArray()).Tables[0];
                liConfigs = GenericStatic.ConvertToList<ApiConfigs>(dtcinfo);
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Failed to GetCategories Info. Reason: " + ex.ToString(), "");
            }
            return liConfigs;
        }
    }
}
