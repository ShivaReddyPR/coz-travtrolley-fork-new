using System.Collections.Generic;

/// <summary>
/// Summary description for WSGetActiveHotelDealsResponse
/// </summary>
public class WSGetActiveHotelDealsResponse
{
    private List<WSHotelDeal> hotelDeals;
    public List<WSHotelDeal> HotelDeals
    {
        get
        {
            return hotelDeals;
        }
        set
        {
            hotelDeals = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    public WSGetActiveHotelDealsResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
