/// <summary>
/// Summary description for WSAddHotelBookingResponse
/// </summary>
public class WSAddHotelBookingResponse
{
    private string referenceId;
    public string ReferenceId
    {
        get { return referenceId; }
        set { referenceId = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    public WSAddHotelBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
