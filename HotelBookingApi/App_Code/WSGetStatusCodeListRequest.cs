/// <summary>
/// Summary description for WSGetStatusCodeListRequest
/// </summary>
public class WSGetStatusCodeListRequest
{
    private string countryCode;
    public string CountryCode
    {
        get
        {
            return countryCode;
        }
        set
        {
            countryCode = value;
        }
    }

    public WSGetStatusCodeListRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
