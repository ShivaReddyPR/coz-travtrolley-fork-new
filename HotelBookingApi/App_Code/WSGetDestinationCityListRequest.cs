/// <summary>
/// Summary description for WSGetDestinationCityListRequest
/// </summary>
public class WSGetDestinationCityListRequest
{
    private string countryName;
    public string CountryName
    {
        get
        {
            return countryName;
        }
        set
        {
            countryName = value;
        }
    }

    public WSGetDestinationCityListRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
