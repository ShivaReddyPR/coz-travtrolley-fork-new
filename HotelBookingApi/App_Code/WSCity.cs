using System.Collections.Generic;
//using Technology.BookingEngine;
using CT.BookingEngine;

/// <summary>
/// Summary description for WSCity
/// </summary>
public class WSCity
{
    /// <summary>
    /// Private variables
    /// </summary>

    private string cityCode;
    private string cityName;
    private string countryName;
    private string countryCode;
    private string currencyCode;
    /// <summary>
    /// Public Properties
    /// </summary>
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }

    }
    public string CityName
    {
        get { return cityName; }
        set { cityName = value; }
    }
    public string CountryCode
    {
        get { return countryCode; }
        set { countryCode = value; }
    }
    public string CountryName
    {
        get { return countryName; }
        set { countryName = value; }
    }
    public string CurrencyCode
    {
        get { return currencyCode; }
        set { currencyCode = value; }
    }

    public WSCity()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public List<WSCity> GetCityListByCountryName(string countryName)
    {
        List<WSCity> cityList = new List<WSCity>();
        HotelCity hCity = new HotelCity();
        List<HotelCity> hCityList = hCity.GetHotelCities(countryName);
        foreach (HotelCity cityInfo in hCityList)
        {
            WSCity city = new WSCity();
            city.cityCode = cityInfo.CityId.ToString();
            city.cityName = cityInfo.CityName;
            city.countryCode = cityInfo.CountryCode;
            city.countryName = cityInfo.CountryName;
            city.currencyCode = cityInfo.CurrencyCode;
            cityList.Add(city);
        }
        return cityList;
    }

    public List<WSCity> GetTopDestinations()
    {
        GTACity GTACityInfo = new GTACity();
        List<GTACity> citysInfo = new List<GTACity>();
        List<WSCity> cityList = new List<WSCity>();
        citysInfo = GTACityInfo.GetGTATopDestinationList();
        foreach (GTACity cityInfo in citysInfo)
        {
            WSCity city = new WSCity();
            city.cityCode = cityInfo.CityCode;//will give the city id
            city.cityName = cityInfo.CityName;
            city.countryCode = cityInfo.CountryCode;
            city.countryName = cityInfo.CountryName;
            city.currencyCode = cityInfo.CurrencyCode;
            cityList.Add(city);
        }
        return cityList;
    }
}
