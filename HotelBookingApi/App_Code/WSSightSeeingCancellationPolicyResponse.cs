﻿//using System.Linq;
//using System.Xml.Linq;

/// <summary>
/// Summary description for WSSightSeeingCancellationPolicyResponse
/// </summary>
public class WSSightSeeingCancellationPolicyResponse
{
    private string[] cancellationPolicy;
    public string[] CancellationPolicy
    {
        get
        {
            return cancellationPolicy;
        }
        set
        {
            cancellationPolicy = value;
        }
    }
    private string[] amendmentMessage;
    public string[] AmendmentMessage
    {
        get
        {
            return amendmentMessage;
        }
        set
        {
            amendmentMessage = value;
        }
    }
    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }
    public WSSightSeeingCancellationPolicyResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
