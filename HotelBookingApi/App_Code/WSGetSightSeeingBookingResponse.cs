﻿//using System.Linq;
//using System.Xml.Linq;

/// <summary>
/// Summary description for WSGetSightSeeingBookingResponse
/// </summary>
public class WSGetSightSeeingBookingResponse
{
    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    private WSSightSeeingBookingDetail bookingDetail;
    public WSSightSeeingBookingDetail BookingDetail
    {
        get
        {
            return bookingDetail;
        }
        set
        {
            bookingDetail = value;
        }
    }

    public WSGetSightSeeingBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
