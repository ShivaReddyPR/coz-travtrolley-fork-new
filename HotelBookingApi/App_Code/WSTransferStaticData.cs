﻿using System;
////using System.Linq;
//using System.Xml.Linq;


/// <summary>
/// Summary description for WLTransferStaticData
/// </summary>

[Serializable]
public class WSTransferStaticData
{
    private string itemCode;
    private string cityCode;
    private string transferTime;
    private string description;
    private string meetingPoint;
    private WSTransferType type;
    private WSTransferBookingSource source;
    private string conditions;
    private string flashLinks;
    private DateTime timeStamp;

    /// <summary>
    /// Item Code
    /// </summary>
    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
    /// <summary>
    /// City Code
    /// </summary>
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }
    /// <summary>
    /// Approximate Transfer Time
    /// </summary>
    public string TransferTime
    {
        get { return transferTime; }
        set { transferTime = value; }
    }
    /// <summary>
    /// Description about the Transfer
    /// </summary>
    public string Description
    {
        get { return description; }
        set { description = value; }
    }
    /// <summary>
    /// Meeting Point Information
    /// </summary>
    public string MeetingPoint
    {
        get { return meetingPoint; }
        set { meetingPoint = value; }
    }
    /// <summary>
    /// Type of Transfer
    /// </summary>
    public WSTransferType Type
    {
        get { return type; }
        set { type = value; }
    }
    /// <summary>
    /// Source of the Transfer
    /// </summary>
    public WSTransferBookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    /// <summary>
    /// Transfer Conditions
    /// </summary>
    public string Conditions
    {
        get { return conditions; }
        set { conditions = value; }
    }
    /// <summary>
    /// Falsh Links of the Transfer
    /// </summary>
    public string FlashLinks
    {
        get { return flashLinks; }
        set { flashLinks = value; }
    }
    /// <summary>
    /// Time stamp of the static Data
    /// </summary>
    public DateTime TimeStamp
    {
        get { return timeStamp; }
        set { timeStamp = value; }
    }

   
}

public enum WSTransferType
{
    pvtTransfer = 0,
    shared = 1
}