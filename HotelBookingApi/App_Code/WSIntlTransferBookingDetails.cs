﻿using System;
//using System.Linq;
//using System.Xml.Linq;

/// <summary>
/// Summary description for WLIntlTransferBookingDetails
/// </summary>
public enum WLTypeOfDocument
{
    PassportFront1 = 1,
    PassportFront2 = 2,
    PassportAdressPage1 = 3,
    PassportBack2 = 4,
    BTQForm = 5,
    TravellerCheque = 6,
    PassportRenew = 7
}
[Serializable]
public class WSIntlTransferBookingDetails
{
    private int transferId;
    WLTypeOfDocument documentName;
    private string fileName;

    public int TransferId
    {
        get { return transferId; }
        set { transferId = value; }
    }

    public WLTypeOfDocument DocumentName
    {
        get { return documentName; }
        set { documentName = value; }
    }

    public string FileName
    {
        get { return fileName; }
        set { fileName = value; }
    }

    
    
}
