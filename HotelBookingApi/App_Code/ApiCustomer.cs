using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.Core
{
    public class ApiCustomer
    {
        #region Private Members

        /// <summary>
        /// Unique id for each customer
        /// </summary>
        private int apiCustomerId;
        /// <summary>
        /// Api Customer Name
        /// </summary>
        private string apiCustomerName;
        /// <summary>
        /// Three digits Code of Api Customer
        /// </summary>
        private string apiCustomerCode;
        /// <summary>
        /// Type of Api Customer
        /// </summary>
        private ApiCustomerType apiCustomerType;
        /// <summary>
        /// site Name of Api Customer
        /// </summary>
        private string siteName;
        /// <summary>
        /// userName of Api Customer
        /// </summary>
        private string userName;
        /// <summary>
        /// AccountCode of Api Customer
        /// </summary>
        private string accountCode;

        #endregion

        #region Public Properties

        public int ApiCustomerId
        {
            get
            {
                return apiCustomerId;
            }
            set
            {
                apiCustomerId = value;
            }

        }
        public string ApiCustomerName
        {
            get
            {
                return apiCustomerName;
            }
            set
            {
                apiCustomerName = value;
            }

        }
        public string ApiCustomerCode
        {
            get
            {
                return apiCustomerCode;
            }
            set
            {
                apiCustomerCode = value;
            }

        }
        public ApiCustomerType ApiCustomerType
        {
            get
            {
                return apiCustomerType;
            }
            set
            {
                apiCustomerType = value;
            }

        }
        public string SiteName
        {
            get
            {
                return siteName;
            }
            set
            {
                siteName = value;
            }

        }
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
            }

        }
        public string AccountCode
        {
            get
            {
                return accountCode;
            }
            set
            {
                accountCode = value;
            }

        }
        #endregion

        /// <summary>
        /// Add or Updates ApiCustomer
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("ApiCustomer.Save entered  ");
            if (apiCustomerName == null || apiCustomerName == string.Empty)
            {
                throw new ArgumentException("ApiCustomer Name cannot be blank or null");
            }
            if (apiCustomerCode == null || apiCustomerCode == string.Empty)
            {
                throw new ArgumentException("Api CustomerCode cannot be blank or null");
            }
            if ((apiCustomerType == ApiCustomerType.WhiteLabel) && (siteName == null || siteName == string.Empty))
            {
                throw new ArgumentException("siteName cannot be blank or null for WhiteLabel Customer");
            }
            if ((apiCustomerType == ApiCustomerType.BookingApi) && (userName == null || userName == string.Empty))
            {
                throw new ArgumentException("userName can not be blank or null for BookingApi Customer");
            }
            if ((apiCustomerType == ApiCustomerType.B2B2B) && (accountCode == null || accountCode == string.Empty))
            {
                throw new ArgumentException("accountCode can not be blank or null for B2B2B Customer");
            }
            if (siteName == string.Empty)
            {
                siteName = null;
            }
            if (userName == string.Empty)
            {
                userName = null;
            }
            if (accountCode == string.Empty)
            {
                accountCode = null;
            }
            if (apiCustomerId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@apiCustomerId", apiCustomerId);
                paramList[1] = new SqlParameter("@apiCustomerName", apiCustomerName);
                paramList[2] = new SqlParameter("@apiCustomerCode", apiCustomerCode);
                paramList[3] = new SqlParameter("@apiCustomerType", Convert.ToInt16(apiCustomerType));
                if (siteName != null)
                {
                    paramList[4] = new SqlParameter("@siteName", siteName);
                }
                else
                {
                    paramList[4] = new SqlParameter("@siteName", DBNull.Value);
                }
                if (userName != null)
                {
                    paramList[5] = new SqlParameter("@userName", userName);
                }
                else
                {
                    paramList[5] = new SqlParameter("@userName", DBNull.Value);
                }
                if (accountCode != null)
                {
                    paramList[6] = new SqlParameter("@accountCode", accountCode);
                }
                else
                {
                    paramList[6] = new SqlParameter("@accountCode", DBNull.Value);
                }
                paramList[7] = new SqlParameter("@status", SqlDbType.Int);
                int status = 0;
                paramList[7].Direction = ParameterDirection.Output;
                int retValue = DBGateway.ExecuteNonQuerySP("usp_UpdateApiCustomer", paramList);
                status = (int)paramList[7].Value;
                if (status == -5)
                {
                    throw new ArgumentException("apiCustomerId not Exists");
                }
                else if (status == -1)
                {
                    throw new ArgumentException("apiCustomerCode already Exists");
                }
                else if (status == -2)
                {
                    throw new ArgumentException("siteName already Exists");
                }
                else if (status == -3)
                {
                    throw new ArgumentException("userName already Exists");
                }
                else if (status == -4)
                {
                    throw new ArgumentException("accountCode already Exists");
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@apiCustomerId", apiCustomerId);
                paramList[1] = new SqlParameter("@apiCustomerName", apiCustomerName);
                paramList[2] = new SqlParameter("@apiCustomerCode", apiCustomerCode);
                paramList[3] = new SqlParameter("@apiCustomerType", Convert.ToInt16(apiCustomerType));
                if (siteName != null)
                {
                    paramList[4] = new SqlParameter("@siteName", siteName);
                }
                else
                {
                    paramList[4] = new SqlParameter("@siteName", DBNull.Value);
                }
                if (userName != null)
                {
                    paramList[5] = new SqlParameter("@userName", userName);
                }
                else
                {
                    paramList[5] = new SqlParameter("@userName", DBNull.Value);
                }
                if (accountCode != null)
                {
                    paramList[6] = new SqlParameter("@accountCode", accountCode);
                }
                else
                {
                    paramList[6] = new SqlParameter("@accountCode", DBNull.Value);
                }
                paramList[0].Direction = ParameterDirection.Output;
                int retValue = DBGateway.ExecuteNonQuerySP("usp_AddApiCustomer", paramList);
                apiCustomerId = (int)paramList[0].Value;
                if (apiCustomerId == -1)
                {
                    throw new ArgumentException("apiCustomerCode already Exists");
                }
                else if (apiCustomerId == -2)
                {
                    throw new ArgumentException("siteName already Exists");
                }
                else if (apiCustomerId == -3)
                {
                    throw new ArgumentException("userName already Exists");
                }
                else if (apiCustomerId == -4)
                {
                    throw new ArgumentException("accountCode already Exists");
                }
            }
        }
        

        /// <summary>
        /// Load ApiCustomer by Authentication value
        /// </summary>
        /// <param name="authenticationValue"></param>
        /// <param name="apiCustomerType"></param>
        public void Load(string authenticationValue, ApiCustomerType apiCustomerType)
        {
            Trace.TraceInformation("ApiCustomer.Load entered : authenticationValue = " + authenticationValue);
            if (authenticationValue == null || authenticationValue == string.Empty)
            {
                throw new ArgumentException("Authentication Value can not be null or blank");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@authenticationValue", authenticationValue);
            paramList[1] = new SqlParameter("@apiCustomerType", Convert.ToInt16(apiCustomerType));
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetApiCustomerByAuthenticationValue", paramList, con);
            ReadData(data);
            data.Close();
            con.Close();
        }
        

        /// <summary>
        /// Reads the data from SqlDataReader
        /// </summary>
        /// <param name="data"></param>
        private void ReadData(SqlDataReader data)
        {
            if (data.Read())
            {
                apiCustomerId = (int)data["apiCustomerId"];
                apiCustomerName = (string)data["apiCustomerName"];
                apiCustomerCode = (string)data["apiCustomerCode"];
                apiCustomerType = (ApiCustomerType)Convert.ToInt32(data["apiCustomerType"]);
                if (data["siteName"] != DBNull.Value)
                {
                    siteName = (string)data["siteName"];
                }
                else
                {
                    siteName = string.Empty;
                }
                if (data["userName"] != DBNull.Value)
                {
                    userName = (string)data["userName"];
                }
                else
                {
                    userName = string.Empty;
                }
                if (data["accountCode"] != DBNull.Value)
                {
                    accountCode = (string)data["accountCode"];
                }
                else
                {
                    accountCode = string.Empty;
                }
            }
        }

        /// <summary>
        /// Returns the list of all ApiCustomer
        /// </summary>
        /// <returns>List of ApiCustomer</returns>
        public static List<ApiCustomer> GetAllApiCustomer()
        {
            Trace.TraceInformation("ApiCustomer.GetAllApiCustomer entered");
            List<ApiCustomer> apiCustomers = new List<ApiCustomer>();
            SqlConnection con = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetAllApiCustomer", paramList, con);
            while (data.Read())
            {
                ApiCustomer customer = new ApiCustomer();
                customer.apiCustomerId = (int)data["apiCustomerId"];
                customer.apiCustomerName = (string)data["apiCustomerName"];
                customer.apiCustomerCode = (string)data["apiCustomerCode"];
                customer.apiCustomerType = (ApiCustomerType)Convert.ToInt32(data["apiCustomerType"]);
                if (data["siteName"] != DBNull.Value)
                {
                    customer.siteName = (string)data["siteName"];
                }
                else
                {
                    customer.siteName = string.Empty;
                }
                if (data["userName"] != DBNull.Value)
                {
                    customer.userName = (string)data["userName"];
                }
                else
                {
                    customer.userName = string.Empty;
                }
                if (data["accountCode"] != DBNull.Value)
                {
                    customer.accountCode = (string)data["accountCode"];
                }
                else
                {
                    customer.accountCode = string.Empty;
                }
                apiCustomers.Add(customer);
            }
            data.Close();
            con.Close();
            return apiCustomers;
        }

        /// <summary>
        /// Add WL booking correspond to site
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="siteId"></param>
        /// <param name="agencyId"></param>
        public static void AddWLAgentBooking(int bookingId, string siteId, int productTypeId, int agencyId)
        {
           SqlParameter[] paramList = new SqlParameter[4];
           paramList[0] = new SqlParameter("@bookingId", bookingId);
           paramList[1] = new SqlParameter("@siteId", siteId);
           paramList[2] = new SqlParameter("@productTypeId", productTypeId);
           paramList[3] = new SqlParameter("@agencyId", agencyId);
           DBGateway.ExecuteNonQuerySP("usp_AddWLAgentBooking", paramList);
        }

    }
}
