using System;
//using Technology.BookingEngine;
//using Technology.BookingEngine.GDS;
using System.Collections.Generic;
//using Technology.MetaSearchEngine;
using CT.MetaSearchEngine;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;

/// <summary>
/// Summary description for WSGetCancellationPolicyRequest
/// </summary>
public class WSGetCancellationPolicyRequest
{
    private int index;
    public int Index
    {
        get { return index; }
        set { index = value; }
    }

    private string ratePlanCode;
    public string RatePlanCode
    {
        get
        {
            return ratePlanCode;
        }
        set
        {
            ratePlanCode = value;
        }
    }

    private string roomTypeCode;
    public string RoomTypeCode
    {
        get
        {
            return roomTypeCode;
        }
        set
        {
            roomTypeCode = value;
        }
    }

    private int noOfRooms;
    public int NoOfRooms
    {
        get
        {
            return noOfRooms;
        }
        set
        {
            noOfRooms = value;
        }
    }

    private string sessionId;
    public string SessionId
    {
        get
        {
            return sessionId;
        }
        set
        {
            sessionId = value;
        }
    }
    //Added by brahmam  LoH purpose 31.03.2016
    private string nationalityCode;
    public string NationalityCode
    {
        get { return nationalityCode; }
        set { nationalityCode = value; }
    }
    public WSGetCancellationPolicyRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static HotelSearchResult GetSelectedSearchResult(int index, HotelSearchResult[] mseResults)
    {
        HotelSearchResult mseResult = new HotelSearchResult();
        mseResult = mseResults[index-1];
        return mseResult;
    }

    public static WSGetCancellationPolicyResponse LoadCancellationPolicy(WSGetCancellationPolicyRequest cancellationRequest, HotelSearchResult hotelResult, string[] ratePlanCodeList, string[] roomTypeCodeList, LoginInfo AgentLoginInfo)
    {
        WSGetCancellationPolicyResponse cpResponse = new WSGetCancellationPolicyResponse();
        cpResponse.Status = new WSStatus();
        Dictionary<string, string> response = new Dictionary<string, string>();
        HotelItinerary itinerary = new HotelItinerary();
        List<HotelPenality> penalityList = new List<HotelPenality>();
        itinerary.StartDate = hotelResult.StartDate;
        itinerary.EndDate = hotelResult.EndDate;
        itinerary.CityCode = hotelResult.CityCode;
        itinerary.HotelCode = hotelResult.HotelCode;
        itinerary.HotelName = hotelResult.HotelName;
        itinerary.NoOfRooms = cancellationRequest.NoOfRooms;
        itinerary.Source = hotelResult.BookingSource;
       
        if (itinerary.Source == HotelBookingSource.DOTW)
        {
            HotelRoom[] roomsInfo = new HotelRoom[ratePlanCodeList.Length];
            for (int i = 0; i < ratePlanCodeList.Length; i++)
            {
                HotelRoom room = new HotelRoom();
                PriceAccounts priceInfo = new PriceAccounts();
                priceInfo.Currency = hotelResult.Currency;
                room.AdultCount = hotelResult.RoomGuest[i].noOfAdults;
                room.ChildCount = hotelResult.RoomGuest[i].noOfChild;
                room.ChildAge = hotelResult.RoomGuest[i].childAge;
                room.Price = priceInfo;
                //if (itinerary.Source == HotelBookingSource.DOTW || itinerary.Source == HotelBookingSource.LOH)
                {

                    room.RatePlanCode = ratePlanCodeList[i];
                    room.NoOfCots = 0;
                    room.ExtraBed = false;
                    room.NoOfUnits = "1";

                    for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                    {
                        if (hotelResult.RoomDetails[j].RatePlanCode.Equals(ratePlanCodeList[i]) && hotelResult.RoomDetails[j].RoomTypeCode.Equals(roomTypeCodeList[i]))
                        {
                            roomsInfo[i] = room;
                            room.RoomName = hotelResult.RoomDetails[j].RoomTypeName;
                            room.RoomTypeCode = hotelResult.RoomDetails[j].RoomTypeCode;
                            room.Price.SupplierPrice = hotelResult.RoomDetails[j].supplierPrice;
                            break;
                        }
                    }


                }

                //else if (itinerary.Source == HotelBookingSource.Desiya)
                //{
                //    room.RoomTypeCode = cancellationRequest.RoomTypeCode;
                //    room.RatePlanCode = cancellationRequest.RatePlanCode;
                //}
                //else if (itinerary.Source == HotelBookingSource.GTA)
                //{
                //    string[] roomList = cancellationRequest.RoomTypeCode.Split('|');
                //    room.RoomTypeCode = roomList[0];
                //    room.NoOfCots = Convert.ToInt16(roomList[1]);
                //    room.ExtraBed = Convert.ToBoolean(Convert.ToInt16(roomList[2]));
                //    room.NoOfUnits = "1";
                //}
                //else if (itinerary.Source == HotelBookingSource.HotelBeds || itinerary.Source == HotelBookingSource.Tourico || itinerary.Source == HotelBookingSource.IAN || itinerary.Source == HotelBookingSource.DOTW || itinerary.Source == HotelBookingSource.Miki || itinerary.Source == HotelBookingSource.Travco)
                //{
                //    room.RoomTypeCode = cancellationRequest.RoomTypeCode;
                //    room.RatePlanCode = cancellationRequest.RatePlanCode;
                //    room.NoOfCots = 0;
                //    room.ExtraBed = false;
                //    room.NoOfUnits = "1";
                //    for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                //    {
                //        if (hotelResult.RoomDetails[j].RoomTypeCode.Equals(cancellationRequest.RoomTypeCode))
                //        {
                //            room.RoomName = hotelResult.RoomDetails[j].RoomTypeName;
                //        }
                //    }
                //}
                roomsInfo[i] = room;
            }
            itinerary.Roomtype = roomsInfo;
        }
        else if (itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.RezLive)
        {
            HotelRoom[] roomsInfo = new HotelRoom[cancellationRequest.noOfRooms];
            int l = 0;
            for (int i = 0; i < hotelResult.RoomDetails.Length; i++)
            {
                if (hotelResult.RoomDetails[i].RatePlanCode == ratePlanCodeList[0])
                {
                    HotelRoom room = new HotelRoom();
                    PriceAccounts priceInfo = new PriceAccounts();
                    priceInfo.Currency = hotelResult.Currency;
                    room.AdultCount = hotelResult.RoomGuest[l].noOfAdults;
                    room.ChildCount = hotelResult.RoomGuest[l].noOfChild;
                    room.ChildAge = hotelResult.RoomGuest[l].childAge;
                    room.Price = priceInfo;

                    room.NoOfCots = 0;
                    room.ExtraBed = false;
                    room.NoOfUnits = "1";
                    room.RoomName = hotelResult.RoomDetails[i].RoomTypeName;
                    room.RoomTypeCode = hotelResult.RoomDetails[i].RoomTypeCode;
                    room.RatePlanCode = hotelResult.RoomDetails[i].RatePlanCode;
                    room.Price.SupplierPrice = hotelResult.RoomDetails[i].supplierPrice;
                    room.Price.SupplierCurrency = hotelResult.Price.SupplierCurrency;
                    roomsInfo[l] = room;
                    l++;
                }
            }
            itinerary.Roomtype = roomsInfo;
        }
        else if (itinerary.Source == HotelBookingSource.HotelBeds)
        {
            List<RoomGuestData> roomsList = new List<RoomGuestData>();
            roomsList.AddRange(hotelResult.RoomGuest);
            HotelRoom[] roomsInfo = new HotelRoom[ratePlanCodeList.Length];
            for (int n = 0; n < ratePlanCodeList.Length; n++)
            {

                for (int i = 0; i < hotelResult.RoomDetails.Length; i++)
                {
                    if (hotelResult.RoomDetails[i].RoomTypeCode == roomTypeCodeList[n].Replace(" ", "+"))
                    {
                        HotelRoom room = new HotelRoom();
                        PriceAccounts priceInfo = new PriceAccounts();
                        priceInfo.Currency = hotelResult.Currency;
                        //Added by brahmam price validation purpose
                        priceInfo.SupplierPrice = hotelResult.RoomDetails[i].supplierPrice;
                        priceInfo.NetFare = hotelResult.RoomDetails[i].supplierPrice;
                        List<RoomGuestData> duplicateRoom = roomsList.FindAll(delegate(RoomGuestData checkRoom)
                        { return (checkRoom.noOfAdults == hotelResult.RoomDetails[i].Occupancy["AdultCount"] && checkRoom.noOfChild == hotelResult.RoomDetails[i].Occupancy["ChildCount"]); });
                        List<int> childAges = new List<int>();
                        room.AdultCount = hotelResult.RoomDetails[i].Occupancy["AdultCount"];
                        room.ChildCount = hotelResult.RoomDetails[i].Occupancy["ChildCount"];
                        if (room.ChildCount > 0)
                        {
                            if (duplicateRoom.Count > 1 && cancellationRequest.NoOfRooms != hotelResult.RoomGuest.Length)
                            {
                                for (int j = 0; j < hotelResult.RoomGuest.Length; j++)
                                {
                                    if (hotelResult.RoomGuest[j].noOfAdults == hotelResult.RoomDetails[i].Occupancy["AdultCount"] && hotelResult.RoomGuest[j].noOfChild == hotelResult.RoomDetails[i].Occupancy["ChildCount"])
                                    {
                                        for (int k = 0; k < hotelResult.RoomGuest[j].noOfChild; k++)
                                        {
                                            childAges.Add(hotelResult.RoomGuest[j].childAge[k]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (int age in hotelResult.RoomGuest[Convert.ToInt32(hotelResult.RoomDetails[i].SequenceNo) - 1].childAge)
                                {
                                    childAges.Add(age);
                                }
                            }
                        }
                        room.ChildAge = childAges;
                        room.Price = priceInfo;
                        room.RoomName = hotelResult.RoomDetails[i].RoomTypeName;
                        room.NoOfCots = 0;
                        room.ExtraBed = false;
                        if (duplicateRoom.Count > 1)
                        {
                            room.NoOfUnits = duplicateRoom.Count.ToString();
                        }
                        else
                        {
                            room.NoOfUnits = "1";
                        }
                        room.RoomTypeCode = hotelResult.RoomDetails[i].RoomTypeCode;
                        roomsInfo[n] = room;
                        break;
                    }
                }
            }
            itinerary.Roomtype = roomsInfo;
        }
        LoginInfo loginInfo = AgentLoginInfo;
        if (itinerary.Source == HotelBookingSource.LOH)
        {
            LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine(cancellationRequest.SessionId);

            jxe.AgentCurrency = loginInfo.Currency;
            jxe.ExchangeRates = loginInfo.AgentExchangeRates;
            jxe.DecimalPoint = loginInfo.DecimalValue;

            decimal supplierPrice = 0;

            foreach (HotelRoom room in itinerary.Roomtype)
            {
                supplierPrice += room.Price.SupplierPrice;
            }
            //Added By brahmam  cancelation policy is coming based on the nationality
            DOTWCountry dotwApi = new DOTWCountry();
            Dictionary<string, string> Countries = dotwApi.GetAllCountries();
            itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[cancellationRequest.nationalityCode]);
            response = jxe.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode.Replace(" ", "+"), itinerary.StartDate, itinerary.EndDate, hotelResult.SequenceNumber, supplierPrice,itinerary.PassengerNationality);
        }
        else if (itinerary.Source == HotelBookingSource.GTA)
        {
            foreach (HotelRoomsDetails rd in hotelResult.RoomDetails)
            {
                if (rd.RoomTypeCode == roomTypeCodeList[0])
                {
                    string[] splitter = { "@#" };
                    response.Add("lastCancellationDate", itinerary.StartDate.ToString());
                    response.Add("CancelPolicy", rd.CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0]);
                    response.Add("AmendmentPolicy", rd.CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1]);
                    response.Add("HotelPolicy", "");
                    break;
                }
            }
        }
        else
        {
            MetaSearchEngine mse = new MetaSearchEngine(cancellationRequest.SessionId);
            mse.SettingsLoginInfo = loginInfo;
            response = mse.GetCancellationDetails(itinerary, ref penalityList, false, itinerary.Source);
            
        }
        if (response.ContainsKey("CancelPolicy"))
        {
            cpResponse.CancellationPolicy = response["CancelPolicy"].Split('|');
        }
        if (response.ContainsKey("HotelPolicy"))
        {
            cpResponse.HotelNorms = response["HotelPolicy"].Split('|');
        }
        cpResponse.PriceStatus = true;
        if (itinerary.Source == HotelBookingSource.LOH)
        {
            if (response.ContainsKey("PRICE_CHANGED"))
            {
                cpResponse.PriceStatus = false;
            }
        }
        else if(itinerary.Source == HotelBookingSource.HotelBeds)//Price validation HotelBeds
        {
            foreach (HotelRoom room in itinerary.Roomtype)
            {
                if (room.PreviousFare > 0 && room.PreviousFare < room.Price.NetFare)
                {
                    cpResponse.PriceStatus = false;
                    break;
                }
            }
        }
        //added by brahmam LOH Purpose 07.12.2015
        if (itinerary.Source == HotelBookingSource.LOH)
        {
            cpResponse.PostalCode = false;
            if (response.ContainsKey("RequirePostalCode"))
            {
                cpResponse.PostalCode = true;
            }
        }
        return cpResponse;
    }
}
