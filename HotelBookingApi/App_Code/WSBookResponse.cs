/// <summary>
/// Summary description for WSBookResponse
/// </summary>
public class WSBookResponse
{
    private string bookingId;
    public string BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }

    private int hotelId;
    public int HotelId
    {
        get
        {
            return hotelId;
        }
        set
        {
            hotelId = value;
        }
    }

    private string confirmationNo;
    public string ConfirmationNo
    {
        get
        {
            return confirmationNo;
        }
        set
        {
            confirmationNo = value;
        }
    }

    private string referenceNo;
    public string ReferenceNo
    {
        get
        {
            return referenceNo;
        }
        set
        {
            referenceNo = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }
    public WSBookResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
