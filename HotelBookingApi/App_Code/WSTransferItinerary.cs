﻿using System;
//using System.Linq;
//using System.Xml.Linq;
using CT.BookingEngine;
using System.Collections.Generic;

/// <summary>
/// Summary description for WLTransferItinerary
/// </summary>
public enum WSTransferBookingStatus
{
    Confirmed = 1,
    Cancelled = 2,
    Failed = 0,
    Pending = 3,
    Error = 4
}
public enum WSTransferBookingSource
{
    GTA = 1
}
public enum WSPickDropType { ACCOMODATION, AIRPORT, STATION, PORT, OTHER }
[Serializable]
public class WSTransferItinerary 
{
    #region private variables
    int transferId;
    string confirmationNo;
    string cityCode;
    string itemCode;
    string itemName;
    string passInfo;
    PriceAccounts price;
    WSTransferBookingSource source;
    string cancelId;
    DateTime transferDate;
    WSPickDropType pickUpType;
    WSPickDropType dropOffType;
    DateTime createdOn;
    int createdBy;
    DateTime lastModifiedOn;
    int lastModifiedBy;
    List<WSTransferVehicle> transferDetails;
    WSTransferBookingStatus status;
    string pickUpCode;
    string pickUpTime;
    string pickUpDescription;
    string pickUpRemarks;
    string dropOffCode;
    string dropOffTime;
    string dropOffDescription;
    string dropOffRemarks;
    List<WSTransferPenalty> penalityInfo;
    string cancelPolicy;
    DateTime lastCancellationDate;
    string bookingRef;
    bool isDomestic;
    bool voucherStatus;
    string transferTime;
    string language;
    int numOfPax;
    string conditions;
    #endregion


    #region Public Properties
    /// <summary>
    /// Transfer Id
    /// </summary>
    public int TransferId
    {
        get { return transferId; }
        set { transferId = value; }
    }
    /// <summary>
    /// Confirmation Number
    /// </summary>
    public string ConfirmationNo
    {
        get { return confirmationNo; }
        set { confirmationNo = value; }
    }
    /// <summary>
    /// City Code
    /// </summary>
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }
    /// <summary>
    /// Item Code
    /// </summary>
    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
    /// <summary>
    /// Item Name
    /// </summary>
    public string ItemName
    {
        get { return itemName; }
        set { itemName = value; }
    }
    /// <summary>
    /// Passenger Information
    /// </summary>
    public string PassengerInfo
    {
        get { return passInfo; }
        set { passInfo = value; }
    }
    /// <summary>
    /// Price Information
    /// </summary>
    public PriceAccounts Price
    {
        get { return price; }
        set { price = value; }
    }
    /// <summary>
    /// booking source
    /// </summary>
    public WSTransferBookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    /// <summary>
    /// Cancellation ID
    /// </summary>
    public string CancelId
    {
        get { return cancelId; }
        set { cancelId = value; }
    }
    /// <summary>
    /// Transfer Date
    /// </summary>
    public DateTime TransferDate
    {
        get { return transferDate; }
        set { transferDate = value; }
    }
    /// <summary>
    /// The Type of the Pick up
    /// </summary>
    public WSPickDropType PickUpType
    {
        get { return pickUpType; }
        set { pickUpType = value; }
    }
    /// <summary>
    /// The Type of the DropOff
    /// </summary>
    public WSPickDropType DropOffType
    {
        get { return dropOffType; }
        set { dropOffType = value; }
    }
    /// <summary>
    /// Tranfer Details Information
    /// </summary>
    public List<WSTransferVehicle> TransferDetails
    {
        get { return transferDetails; }
        set { transferDetails = value; }
    }
    ///// <summary>
    /// Status of the Booking
    /// </summary>
    public WSTransferBookingStatus BookingStatus
    {
        get { return status; }
        set { status = value; }
    }
    /// <summary>
    /// PickUps Code like Airport Code etc
    /// </summary>
    public string PickUpCode
    {
        get { return pickUpCode; }
        set { pickUpCode = value; }
    }
    /// <summary>
    /// PickUp Time
    /// </summary>
    public string PickUpTime
    {
        get { return pickUpTime; }
        set { pickUpTime = value; }
    }
    /// <summary>
    /// Description about the pickUp
    /// </summary>
    public string PickUpDescription
    {
        get { return pickUpDescription; }
        set { pickUpDescription = value; }
    }
    /// <summary>
    /// Remarks /Other Info about the PickUp
    /// </summary>
    public string PickUpRemarks
    {
        get { return pickUpRemarks; }
        set { pickUpRemarks = value; }
    }

    /// <summary>
    /// DropOffs Code like Airport Code etc
    /// </summary>
    public string DropOffCode
    {
        get { return dropOffCode; }
        set { dropOffCode = value; }
    }
    /// <summary>
    /// DropOff Time
    /// </summary>
    public string DropOffTime
    {
        get { return dropOffTime; }
        set { dropOffTime = value; }
    }
    /// <summary>
    /// Description about the DropOff
    /// </summary>
    public string DropOffDescription
    {
        get { return dropOffDescription; }
        set { dropOffDescription = value; }
    }
    /// <summary>
    /// Remarks /Other Info about the DropOff
    /// </summary>
    public string DropOffRemarks
    {
        get { return dropOffRemarks; }
        set { dropOffRemarks = value; }
    }
    /// <summary>
    /// Gets or sets createdBy
    /// </summary>
    public int CreatedBy
    {
        get
        {
            return createdBy;
        }
        set
        {
            createdBy = value;
        }
    }

    /// <summary>
    /// Gets or sets createdOn Date
    /// </summary>
    public DateTime CreatedOn
    {
        get
        {
            return createdOn;
        }
        set
        {
            createdOn = value;
        }
    }

    /// <summary>
    /// Gets or sets lastModifiedBy
    /// </summary>
    public int LastModifiedBy
    {
        get
        {
            return lastModifiedBy;
        }
        set
        {
            lastModifiedBy = value;
        }
    }

    /// <summary>
    /// Gets or sets lastModifiedOn Date
    /// </summary>
    public DateTime LastModifiedOn
    {
        get
        {
            return lastModifiedOn;
        }
        set
        {
            lastModifiedOn = value;
        }
    }

    public List<WSTransferPenalty> PenalityInfo
    {
        get { return penalityInfo; }
        set { penalityInfo = value; }
    }
    public string CancellationPolicy
    {
        get { return cancelPolicy; }
        set { cancelPolicy = value; }
    }
    public DateTime LastCancellationDate
    {
        get { return lastCancellationDate; }
        set { lastCancellationDate = value; }
    }
    public string BookingReference
    {
        get { return bookingRef; }
        set { bookingRef = value; }
    }
    public bool IsDomestic
    {
        get { return isDomestic; }
        set { isDomestic = value; }
    }
    public bool VoucherStatus
    {
        get { return voucherStatus; }
        set { voucherStatus = value; }
    }
    public string TransferTime
    {
        get { return transferTime; }
        set { transferTime = value; }
    }
    public int NumOfPax
    {
        get { return numOfPax; }
        set { numOfPax = value; }
    }
    public string Language
    {
        get { return language; }
        set { language = value; }
    }
    public string TransferConditions
    {
        get { return conditions; }
        set { conditions = value; }
    }
    #endregion




}
