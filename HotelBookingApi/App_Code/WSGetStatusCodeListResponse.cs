/// <summary>
/// Summary description for WSGetStatusCodeListResponse
/// </summary>
public class WSGetStatusCodeListResponse
{
    private string stateList;
    public string StateList
    {
        get
        {
            return stateList;
        }
        set
        {
            stateList = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    public WSGetStatusCodeListResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
