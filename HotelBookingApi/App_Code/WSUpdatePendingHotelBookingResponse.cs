/// <summary>
/// Summary description for WSUpdatePendingHotelBookingResponse
/// </summary>
public class WSUpdatePendingHotelBookingResponse
{
    private string hotelQueueId;
    public string HotelQueueId
    {
        get { return hotelQueueId; }
        set { hotelQueueId = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }
    public WSUpdatePendingHotelBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
