﻿using System;
using System.Data;
//using System.Linq;
//using System.Xml.Linq;
using System.Collections.Generic;
using CT.BookingEngine;

/// <summary>
/// Summary description for WSSightSeeingRequest
/// </summary>
public class WSSightSeeingRequest
{
    private string countryName;                      //Country Code
    private string destinationType;                  //Destination type, can be "city" or "area"
    private string destinationCode;                  //Destination code according to the DestinationType
    private string itemName;                         //filter by itemName
    private string itemCode;                         //filter by itemCode
    private DateTime tourDate;                       //Tour date
    private int noOfAdults;                          //number of adults
    private List<int> childrenList;                  //children age list
    private List<string> typeCodeList;               //filter by types
    private List<string> categoryCodeList;           //filter by category
    private int childCount;
    //Not Needed for Search
    private string specialCode;
    private string cityName;
    private string language; //Tour Language
    private int noDays;
    public string currency;
    #region Properties
    public string CountryName
    {
        get { return countryName; }
        set { countryName = value; }
    }
    public string DestinationType
    {
        get { return destinationType; }
        set { destinationType = value; }
    }
    public string DestinationCode
    {
        get { return destinationCode; }
        set { destinationCode = value; }
    }
    public string ItemName
    {
        get { return itemName; }
        set { itemName = value; }
    }
    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
    public DateTime TourDate
    {
        get { return tourDate; }
        set { tourDate = value; }
    }
    public int NoOfAdults
    {
        get { return noOfAdults; }
        set { noOfAdults = value; }
    }
    public List<int> ChildrenList
    {
        get { return childrenList; }
        set { childrenList = value; }
    }
    public List<string> TypeCodeList
    {
        get { return typeCodeList; }
        set { typeCodeList = value; }
    }
    public List<string> CategoryCodeList
    {
        get { return categoryCodeList; }
        set { categoryCodeList = value; }
    }
    public int ChildCount
    {
        get { return childCount; }
        set { childCount = value; }
    }
    public string SpecialCode
    {
        get { return specialCode; }
        set { specialCode = value; }
    }
    public string CityName
    {
        get { return cityName; }
        set { cityName = value; }
    }
    public string Language
    {
        get { return language; }
        set { language = value; }
    }
    public int NoDays
    {
        get { return noDays; }
        set { noDays = value; }
    }
    public string Currency
    {
        get { return currency; }
        set { currency = value; }
    }
    #endregion

    public WSSightSeeingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void UpdatePrice(ref SightseeingSearchResult[] mseResult, SightSeeingReguest req, int agencyId,int decimalValue)
    {
        PriceAccounts price = new PriceAccounts();
        DataTable dtAllAgentsMarkup = new DataTable();
        if (agencyId > 0)
        {
            dtAllAgentsMarkup = UpdateMarkup.GetAllMarkup(agencyId, (int)ProductType.SightSeeing);
        }
        for (int i = 0; i < mseResult.Length; i++)
        {
            for (int count = 0; count < mseResult[i].TourOperationList.Length; count++)
            {
                #region B2CAgentMarkup calculations
                //price = mseResult[i].Price;
                DataView dv = dtAllAgentsMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2C') AND SourceId='" + mseResult[i].Source.ToString() + "'";
                DataTable dtAgentMarkup = dv.ToTable();

                if (dtAgentMarkup != null && dtAgentMarkup.Rows.Count > 0)
                {
                    decimal agtB2CMarkUp = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Markup"]);

                    mseResult[i].TourOperationList[count].PriceInfo.B2CMarkupType = dtAgentMarkup.Rows[0]["MarkupType"].ToString();
                    mseResult[i].TourOperationList[count].PriceInfo.B2CMarkupValue = agtB2CMarkUp;
                    decimal totalB2CMarkup = 0m;

                    if (Convert.ToString(dtAgentMarkup.Rows[0]["MarkupType"]) == "P")
                    {
                        totalB2CMarkup=(Convert.ToDecimal(mseResult[i].TourOperationList[count].PriceInfo.Markup + mseResult[i].TourOperationList[count].ItemPrice) * (agtB2CMarkUp / 100m));
                        mseResult[i].TourOperationList[count].PriceInfo.B2CMarkup = Math.Round(totalB2CMarkup, decimalValue);
                    }
                    else
                    {
                        mseResult[i].TourOperationList[count].PriceInfo.B2CMarkup = agtB2CMarkUp;
                    }
                }

                #endregion
            }
        }
    }

    public static WSSightSeeingResult[] GetSightSeeingSearchResults(SightseeingSearchResult[] mseResult, SightSeeingReguest req, int agencyId,string sessionId)
    {
        
        WSSightSeeingResult[] results = new WSSightSeeingResult[mseResult.Length];
        for (int i = 0; i < mseResult.Length; i++)
        {
            results[i] = new WSSightSeeingResult();
            results[i].AdditionalInformationList = mseResult[i].AdditionalInformationList;
            results[i].CityCode = mseResult[i].CityCode;
            results[i].CityName = mseResult[i].CityName;
            results[i].CityCode = mseResult[i].CityCode;
            results[i].DepaturePointRequired = mseResult[i].DepaturePointRequired;
            results[i].Description = mseResult[i].Description;
            results[i].Duration = mseResult[i].Duration;
            results[i].EssentialInformationList = mseResult[i].EssentialInformationList;
            results[i].FlashLink = mseResult[i].FlashLink;
            results[i].HasExtraInfo = mseResult[i].HasExtraInfo;
            results[i].HasFlash = mseResult[i].HasFlash;
            results[i].Image = mseResult[i].Image;
            results[i].ItemCode = mseResult[i].ItemCode;
            results[i].ItemName = mseResult[i].ItemName;
            results[i].SightseeingCategoryList = new WSSightseeingCategoryList[mseResult[i].SightseeingCategoryList.Count];
            int k = 0;
            foreach (KeyValuePair<string, string> pair in mseResult[i].SightseeingCategoryList)
            {
                results[i].SightseeingCategoryList[k].Code=pair.Key;
                results[i].SightseeingCategoryList[k].Value = pair.Value;
                k++;
            }
            results[i].SightseeingTypeList = new WSSightseeingTypeList[mseResult[i].SightseeingTypeList.Count];
            int l = 0;
            foreach (KeyValuePair<string, string> pair in mseResult[i].SightseeingTypeList)
            {
                results[i].SightseeingTypeList[l].Code = pair.Key;
                results[i].SightseeingTypeList[l].Value = pair.Value;
                l++;
            }
            if (mseResult[i].Source == SightseeingBookingSource.GTA)
            {
                results[i].Source = WSSightseeingBookingSource.GTA;
            }
            results[i].TourOperationList = new WSTourOperation[mseResult[i].TourOperationList.Length];
            for (int j = 0; j < mseResult[i].TourOperationList.Length; j++)
            {
                results[i].TourOperationList[j].ConfirmationCodeList = new WSConfirmationCodeList[mseResult[i].TourOperationList[j].ConfirmationCodeList.Count];
                int m = 0;
                foreach (KeyValuePair<string, string> pair in mseResult[i].TourOperationList[j].ConfirmationCodeList)
                {
                    results[i].TourOperationList[j].ConfirmationCodeList[m].Code=pair.Key;
                    results[i].TourOperationList[j].ConfirmationCodeList[m].Value = pair.Value;
                    m++;
                }
                results[i].TourOperationList[j].Currency = mseResult[i].TourOperationList[j].Currency;
                results[i].TourOperationList[j].ItemPrice = mseResult[i].TourOperationList[j].ItemPrice;
                results[i].TourOperationList[j].LangName = mseResult[i].TourOperationList[j].LangName;
                results[i].TourOperationList[j].LanguageCode = mseResult[i].TourOperationList[j].LanguageCode;
                results[i].TourOperationList[j].PriceInfo = mseResult[i].TourOperationList[j].PriceInfo;
                results[i].TourOperationList[j].SpecialItemList = mseResult[i].TourOperationList[j].SpecialItemList;
                results[i].TourOperationList[j].SpecialItemName = mseResult[i].TourOperationList[j].SpecialItemName;
                results[i].TourOperationList[j].TourLanguageList = mseResult[i].TourOperationList[j].TourLanguageList;
            }
            results[i].SessionId = sessionId;
            results[i].Index = i;
        }
        return results;
    }

    public static SightSeeingReguest GenarateSightSeeingReguest(WSSightSeeingRequest request)
    {
        SightSeeingReguest mseRequest = new SightSeeingReguest();
        mseRequest.CategoryCodeList = request.CategoryCodeList;
        mseRequest.ChildCount = request.ChildCount;
        mseRequest.ChildrenList = request.ChildrenList;
        mseRequest.CityName = request.CityName;
        mseRequest.CountryName = request.CountryName;
        mseRequest.DestinationCode = request.DestinationCode;
        mseRequest.DestinationType = request.DestinationType;
        mseRequest.ItemCode = request.ItemCode;
        mseRequest.ItemName = request.ItemName;
        mseRequest.Language = request.Language;
        mseRequest.NoOfAdults = request.NoOfAdults;
        mseRequest.SpecialCode = request.SpecialCode;
        mseRequest.TourDate = request.TourDate;
        mseRequest.TypeCodeList = request.TypeCodeList;
        mseRequest.NoDays = request.NoDays;
        return mseRequest;
    }

    public static SightseeingSearchResult GenarateSightSeeingResult(WSSightSeeingResult result)
    {
        SightseeingSearchResult mseResult = new SightseeingSearchResult();
        mseResult.AdditionalInformationList = result.AdditionalInformationList;
        mseResult.CityCode = result.CityCode;
        mseResult.CityName = result.CityName;
        mseResult.DepaturePointRequired = result.DepaturePointRequired;
        mseResult.Description = result.Description;
        mseResult.Duration = result.Duration;
        mseResult.EssentialInformationList = result.EssentialInformationList;
        mseResult.FlashLink = result.FlashLink;
        mseResult.HasExtraInfo = result.HasExtraInfo;
        mseResult.HasFlash = result.HasFlash;
        mseResult.Image = result.Image;
        mseResult.ItemCode = result.ItemCode;
        mseResult.ItemName = result.ItemName;
        mseResult.SightseeingCategoryList = new Dictionary<string, string>();
        for (int i = 0; i < result.SightseeingCategoryList.Length; i++)
        {
            mseResult.SightseeingCategoryList.Add(result.SightseeingCategoryList[i].Code, result.SightseeingCategoryList[i].Value);
        }
        mseResult.SightseeingTypeList = new Dictionary<string, string>();
        for (int m = 0; m < result.SightseeingTypeList.Length; m++)
        {
            mseResult.SightseeingTypeList.Add(result.SightseeingTypeList[m].Code, result.SightseeingTypeList[m].Value);
        }
        if (result.Source == WSSightseeingBookingSource.GTA)
        {
            mseResult.Source = SightseeingBookingSource.GTA;
        }
        mseResult.TourOperationList = new TourOperation[result.TourOperationList.Length];
        for (int j = 0; j < result.TourOperationList.Length; j++)
        {
            mseResult.TourOperationList[j].ConfirmationCodeList = new Dictionary<string, string>();
            for (int k = 0; k < result.TourOperationList[j].ConfirmationCodeList.Length; k++)
            {
                mseResult.TourOperationList[j].ConfirmationCodeList.Add(result.TourOperationList[j].ConfirmationCodeList[k].Code, result.TourOperationList[j].ConfirmationCodeList[k].Value);
            }
            mseResult.TourOperationList[j].Currency = result.TourOperationList[j].Currency;
            mseResult.TourOperationList[j].ItemPrice = result.TourOperationList[j].ItemPrice;
            mseResult.TourOperationList[j].LangName = result.TourOperationList[j].LangName;
            mseResult.TourOperationList[j].LanguageCode = result.TourOperationList[j].LanguageCode;
            mseResult.TourOperationList[j].PriceInfo = result.TourOperationList[j].PriceInfo;
            mseResult.TourOperationList[j].SpecialItemList = result.TourOperationList[j].SpecialItemList;
            mseResult.TourOperationList[j].SpecialItemName = result.TourOperationList[j].SpecialItemName;
            mseResult.TourOperationList[j].TourLanguageList = result.TourOperationList[j].TourLanguageList;
        }
        return mseResult;
    }
}
