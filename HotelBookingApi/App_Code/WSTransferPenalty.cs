﻿using System;
//using System.Linq;
//using System.Xml.Linq;

/// <summary>
/// Enum for Type of Transfer Change Policy
/// </summary>

[Serializable]
public class WSTransferPenalty 
{
    //private Variables
    int transferId;
    WLTransferChangePoicyType policyType;
    WSTransferBookingSource bookingSource;
    bool isAllowed;
    DateTime fromDate;
    DateTime toDate;
    decimal price;
    string remarks;
    //public Properties.
    public int TransferId
    {
        get { return transferId; }
        set { transferId = value; }
    }
    public WLTransferChangePoicyType PolicyType
    {
        get { return policyType; }
        set { policyType = value; }
    }
    public WSTransferBookingSource BookingSource
    {
        get { return bookingSource; }
        set { bookingSource = value; }
    }
    public bool IsAllowed
    {
        get { return isAllowed; }
        set { isAllowed = value; }
    }
    public DateTime FromDate
    {
        get { return fromDate; }
        set { fromDate = value; }
    }
    public DateTime ToDate
    {
        get { return toDate; }
        set { toDate = value; }
    }
    public string Remarks
    {
        get { return remarks; }
        set { remarks = value; }
    }
    public decimal Price
    {
        get { return price; }
        set { price = value; }
    }
}

public enum WLTransferChangePoicyType
{
    Amendment = 1,
    Namechange = 2,
    Other = 3
}