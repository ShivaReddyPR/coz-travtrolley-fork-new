﻿using System;
using System.Collections;
//using System.Linq;
using System.Web.Services;
using System.Web.Services.Protocols;
//using System.Xml.Linq;
using System.Diagnostics;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.Core;
using CT.MetaSearchEngine;
using CT.Configuration;
using System.Collections.Generic;
using CT.AccountingEngine;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;


/// <summary>
/// Summary description for WLSightSeeingService
/// </summary>
[WebService(Namespace = "http://CT/HotelBookingApi",
            Description = "Sight Seeing Api for Search and Book",
            Name = "WLSightSeeingService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WLSightSeeingService : System.Web.Services.WebService
{
    #region Variables
    public AuthenticationData Credential;
    private string statusDescription;
    private string statusCode;
    private LoginInfo AgentLoginInfo;
    #endregion
    public WLSightSeeingService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    #region Public Method
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSSightSeeingSearchResponse Search(WSSightSeeingRequest request)
    {
        UserMaster member = new UserMaster();
        member = WSValidate(Credential); //validating Credential
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID); //Loading AgentLoginInfo
        if (member != null)
        {
            if (!(WSSearchInput(request))) //Checking Validate or not Request Object
            {
                WSSightSeeingSearchResponse errorResponse = new WSSightSeeingSearchResponse();
                WSStatus status = new WSStatus();
                status.Category = "SR";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                errorResponse.Result = new WSSightSeeingResult[0];
                Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }
            WSSightSeeingSearchResponse response = new WSSightSeeingSearchResponse();
            WSSightSeeingResult[] results = new WSSightSeeingResult[0]; 
            SightseeingSearchResult[] result;
            response.Status = new WSStatus();
            
            //Creating request object
            SightSeeingReguest mseRequest = WSSightSeeingRequest.GenarateSightSeeingReguest(request);
            MetaSearchEngine mse = new MetaSearchEngine();
            try
            {
                LoginInfo loginfo = AgentLoginInfo;
                mse.SettingsLoginInfo = loginfo;
                result = mse.GetSightseeingResults(mseRequest, loginfo.AgentId);
                response.SessionId = mse.SessionId;
                if (result.Length == 0)
                {
                    response.Status.Category = "SR";
                    response.Status.StatusCode = "13";
                    response.Status.Description = "No SightSeeing Found";
                    response.Result = new WSSightSeeingResult[0];
                    Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "No SightSeeing Found", "");
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status.Category = "SR";
                response.Status.StatusCode = "14";
                response.Status.Description = "SightSeeing Search Failed, Technical Fault 01";
                response.Result = new WSSightSeeingResult[0];
                try
                {
                    Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("SightSeeing Search Method Failed, due to BookingEngine Exception", member, ex, "\n\n Destination city reference:" + request.CityName + "\n\n Destination Country Name : " + request.CountryName + "\n\n Tour Date : " + request.TourDate.ToShortDateString() + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            try
            {
                //Updating Price in Search Results
                WSSightSeeingRequest.UpdatePrice(ref result, mseRequest, member.AgentId,AgentLoginInfo.DecimalValue);
                //Loading Search results in Api Search Results format
                results = WSSightSeeingRequest.GetSightSeeingSearchResults(result, mseRequest, member.AgentId, mse.SessionId);
            }
            catch (Exception ex)
            {
                response.Status.Category = "SR";
                response.Status.StatusCode = "17";
                response.Status.Description = "Technical Fault 03: ";
                response.Result = new WSSightSeeingResult[0];
                try
                {
                    Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("SightSeeing Search Method Failed, due to Exception", member, ex, "\n\n Destination city reference:" + request.CityName + "\n\n Destination Country Name : " + request.CountryName + "\n\n TourDate Date : " + request.TourDate + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            response.Result = results;
            response.Status.Category = "SR";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            return response;
        }
        else
        {
            WSSightSeeingSearchResponse response = new WSSightSeeingSearchResponse();
            WSStatus wsStatus = new WSStatus();
            wsStatus.Category = "SR";
            wsStatus.Description = "login failed";
            wsStatus.StatusCode = "02";
            response.Status = wsStatus;
            response.Result = new WSSightSeeingResult[0];
            Audit.Add(EventType.SightseeingSearch, Severity.High, 0, "B2C-SightSeeing-Login Failed for Member for Search Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return response;
        }

    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSSightSeeingCancellationPolicyResponse GetCancellationPolicy(WSSightSeeingRequest request, WSSightSeeingResult result, int chosenTour)
    {
        Trace.TraceInformation("SightSeeingApi.GetSightSeeingCancellationPolicy entered.");
        UserMaster member = new UserMaster();
        //validating and loading member from credential
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        if (member != null)
        {
            //Validating GetCancellationPolicy input
            if (!(WSSearchInput(request))) //Checking Validate or not Request Object
            {
                WSSightSeeingCancellationPolicyResponse errorResponse = new WSSightSeeingCancellationPolicyResponse();
                WSStatus status = new WSStatus();
                status.Category = "SR";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }
            WSSightSeeingCancellationPolicyResponse response = new WSSightSeeingCancellationPolicyResponse();
            response.Status = new WSStatus();

            //Creating request object
            SightSeeingReguest mseRequest = WSSightSeeingRequest.GenarateSightSeeingReguest(request);
            SightseeingSearchResult mseResult = WSSightSeeingRequest.GenarateSightSeeingResult(result);
            try
            {
                CT.BookingEngine.GDS.GTA gta = new CT.BookingEngine.GDS.GTA();
                gta.AgentCurrency = AgentLoginInfo.Currency;
                gta.AgentExchangeRates = AgentLoginInfo.AgentExchangeRates;
                gta.AgentDecimalPoint = AgentLoginInfo.DecimalValue;
                Dictionary<string, string> chargeResponse = new Dictionary<string, string>();
                chargeResponse = gta.GetSightseeingChargeCondition(mseRequest, mseResult, chosenTour);
                if (chargeResponse.ContainsKey("CancelPolicy"))
                {
                    response.CancellationPolicy = chargeResponse["CancelPolicy"].Split('|');
                }
                if (chargeResponse.ContainsKey("AmendmentPolicy"))
                {
                    response.AmendmentMessage = chargeResponse["AmendmentPolicy"].Split('|');
                }

                if (response.CancellationPolicy.Length <= 0)
                {
                    response.Status.Category = "CP";
                    response.Status.StatusCode = "09";
                    response.Status.Description = "Cancellation Policies not Found.";
                    Audit.Add(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-SightSeeing-Cancillation Policies not Found.", "");
                    return response;
                }
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "CP";
                response.Status.StatusCode = "10";
                response.Status.Description = "GetCancellationPolicy Failed. Technical fault 01";
                try
                {
                    Audit.Add(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetCancellationPolicy Method Failed, due to BookingEngineException Exception", member, ex, "\n\n itemName:" + result.ItemName + "\n\n Booking Source : " + result.Source + "\n\n item Code : " + result.ItemCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "CP";
                response.Status.StatusCode = "11";
                response.Status.Description = "GetCancellationPolicy function returning error: " + ex.Message;
                try
                {
                    Audit.Add(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetCancellationPolicy Method Failed, due to ArgumentException Exception", member, ex, "\n\n itemName:" + result.ItemName + "\n\n Booking Source : " + result.Source + "\n\n item Code : " + result.ItemCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "CP";
                response.Status.StatusCode = "12";
                response.Status.Description = "Technical fault 03";
                try
                {
                    Audit.Add(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetCancellationPolicy Method Failed, due to  Exception", member, ex, "\n\n itemName:" + result.ItemName + "\n\n Booking Source : " + result.Source + "\n\n item Code : " + result.ItemCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            response.Status.Category = "CP";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.Add(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-SightSeeing-GetCancellationPolicy Successful", "");
            return response;
        }
        else
        {
            WSSightSeeingCancellationPolicyResponse errorResponse = new WSSightSeeingCancellationPolicyResponse();
            WSStatus status = new WSStatus();
            status.Category = "CP";
            status.Description = "GetCancellationPolicies Authentication Failed";
            status.StatusCode = "02";
            errorResponse.Status = status;
            Audit.Add(EventType.GetCancellationPolicy, Severity.High, 0, "B2C-SightSeeing-Login Failed for Member for GetCancellationPolicy Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }

    /// <summary>
    /// SightSeeing Static Data
    /// </summary>
    /// <param name="cityCode"></param>
    /// <param name="itemName"></param>
    /// <param name="itemCode"></param>
    /// <param name="sessionId"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetSightSeeingDetailResponse GetSightSeeingDetails(string cityCode, string itemName, string itemCode)
    {
        Trace.TraceInformation("SightSeeing.GetSightSeeingDetails entered");
        UserMaster member = new UserMaster();

        //validating and loading member from credential
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        if (member != null)
        {
            WSGetSightSeeingDetailResponse response = new WSGetSightSeeingDetailResponse();
            WSSightseeingStaticData sDetail = new WSSightseeingStaticData();
            SightseeingStaticData SightDetail = new SightseeingStaticData();
            response.Status = new WSStatus();
            try
            {
                CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                LoginInfo loginInfo = AgentLoginInfo;
                SightDetail = gtaApi.GetSightseeingItemInformation(cityCode, itemName, itemCode);
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "HD";
                response.Status.StatusCode = "06";
                response.Status.Description = "GetSightSeeingDetails Failed, Technical Fault 01. ";
                try
                {
                    Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetSightSeeingDetails Method Failed, due to BookingEngineException Exception", member, ex, "\n\n itemName:" + SightDetail.ItemName + "\n\n Booking Source : " + SightDetail.Source + "\n\n SightSeeing item Code : " + SightDetail.ItemCode + "\n\n CityCode " + SightDetail.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "HD";
                response.Status.StatusCode = "07";
                response.Status.Description = "Error returned while getting SightSeeing details," + ex.Message;
                try
                {
                    Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetSightSeeingDetails Method Failed, due to ArgumentException Exception", member, ex, "\n\n itemName:" + SightDetail.ItemName + "\n\n Booking Source : " + SightDetail.Source + "\n\n SightSeeing item Code : " + SightDetail.ItemCode + "\n\n CityCode " + SightDetail.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "HD";
                response.Status.StatusCode = "08";
                response.Status.Description = "Technical Fault 02. ";
                try
                {
                    Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetSightSeeingDetails Method Failed, due to Exception", member, ex, "\n\n itemName:" + SightDetail.ItemName + "\n\n Booking Source : " + SightDetail.Source + "\n\n SightSeeing item Code : " + SightDetail.ItemCode + "\n\n CityCode " + SightDetail.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            try
            {
                //Filling SightSeeingDetails in Api SightSeeingDetail object
                sDetail = WSGetSightSeeingDetailResponse.FillSightSeeingDetails(SightDetail);
            }
            catch (Exception ex)
            {
                response.Status.Category = "HD";
                response.Status.StatusCode = "09";
                response.Status.Description = "Technical Fault 03. ";
                try
                {
                    Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetSightSeeingDetails Method Failed, due to Exception", member, ex, "\n\n itemName:" + SightDetail.ItemName + "\n\n Booking Source : " + SightDetail.Source + "\n\n SightSeeing item Code : " + SightDetail.ItemCode + "\n\n CityCode " + SightDetail.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            response.SightSeeingDetail = sDetail;
            response.Status.Category = "HD";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.Add(EventType.SightseeingSearch, Severity.High, (int)member.ID, "B2C-SightSeeing-GetSightSeeingDetails Successful", "");
            return response;
        }
        else
        {
            WSGetSightSeeingDetailResponse errorResponse = new WSGetSightSeeingDetailResponse();
            WSStatus status = new WSStatus();
            status.Category = "HD";
            status.Description = "GetSightSeeingDetails Authentication Failed";
            status.StatusCode = "02";
            errorResponse.Status = status;
            Audit.Add(EventType.SightseeingSearch, Severity.High, 0, "B2C-SightSeeing-Login Failed for Member for GetSightSeeingDetails Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSSightSeeingBookingResponse Book(WSSightSeeingBookingRequest request)
    {
        Audit.Add(EventType.SightseeingBooking, Severity.High, 1, "B2C-SightSeeing-Root:SightSeeing Book Request: " + Serialize<WSSightSeeingBookingRequest>(request) + " :Credentials:" + Serialize<AuthenticationData>(Credential), "");
        Trace.TraceInformation("SeightSeeingApi.Book entered.");
        CreditCardPaymentInformation cardInfo = new CreditCardPaymentInformation();
        #region Checking if paymentMode is creditcard and payment info not null
        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
        {
            cardInfo.Amount = request.PaymentInfo.Amount;
            cardInfo.PaymentGateway = (CT.AccountingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.AccountingEngine.PaymentGatewaySource), request.PaymentInfo.PaymentGateway.ToString());
            cardInfo.PaymentId = request.PaymentInfo.PaymentId;
            cardInfo.TrackId = request.PaymentInfo.TrackId;
            cardInfo.IPAddress = request.PaymentInfo.IPAddress;
            cardInfo.Charges = request.PaymentInfo.CreditCardCharges;
        }

        #endregion
        UserMaster member = new UserMaster();

        //Validating member from credentials
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        WSSightSeeingBookingResponse response = new WSSightSeeingBookingResponse();
        WSStatus status = new WSStatus();
        if (member != null)
        {
            //Validating BookRequest input
            if (!(WSBookInput(request)))
            {
                WSSightSeeingBookingResponse errorResponse = new WSSightSeeingBookingResponse();
                status.Category = "BK";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.Add(EventType.SightseeingBooking, Severity.High, (int)member.ID, "B2C-SightSeeing-Input parameter is incorrect: " + statusDescription, "");
                if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                {
                    cardInfo.ReferenceId = 0;
                    cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                    cardInfo.Save();
                }
                return errorResponse;
            }
            else
            {
                try
                {
                    SightseeingItinerary itineary = WSSightSeeingBookingRequest.BuildSightSeeingItinerary(request, AgentLoginInfo);
                    BookingResponse bookRes = new BookingResponse();
                    itineary.ProductType = ProductType.SightSeeing;
                    Product prod = (Product)itineary;
                    MetaSearchEngine mse = new MetaSearchEngine(request.SessionId);
                    #region Meta Search Engine and Booking Processing
                    bookRes = mse.Book(ref prod, AgentLoginInfo.AgentId, BookingStatus.Ready, AgentLoginInfo.UserID, true);
                    response.ConfirmationNo = bookRes.ConfirmationNo;
                    response.BookingId = Convert.ToString(bookRes.BookingId);
                    response.ReferenceNo = itineary.BookingReference;
                    response.SightSeeingId = itineary.SightseeingId;
                    if (bookRes.Status == BookingResponseStatus.Successful)
                    {
                        #region invoice generation for SightSeeing
                        //if (hotelResult.BookingSource == HotelBookingSource.TBOConnect)
                        {
                            //Price info
                            int invoiceNumber = 0;
                            Invoice invoice = new Invoice();
                            try
                            {
                                invoiceNumber = Invoice.isInvoiceGenerated(response.SightSeeingId, ProductType.SightSeeing);

                                if (invoiceNumber == 0)
                                {
                                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(response.SightSeeingId, string.Empty, (int)member.ID, ProductType.SightSeeing, 1);
                                }
                            }
                            catch (Exception exp)
                            {
                                Audit.Add(EventType.SightseeingBooking, Severity.High, (int)member.ID, "B2C-SightSeeing API Book:Exception while generating Invoice.. Message:" + exp.Message, "");
                            }
                        }
                        status.Category = "BK";
                        status.Description = "Successful";
                        status.StatusCode = "01";
                        response.Status = status;

                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.Add(EventType.SightseeingBooking, Severity.High, (int)member.ID, "Book Successful", "");
                        #endregion
                    }
                    else if (bookRes.Status == BookingResponseStatus.Failed)
                    {
                        status.Category = "BK";
                        status.Description = "Booking Failed.";
                        status.StatusCode = "42";
                        response.Status = status;
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.Add(EventType.SightseeingBooking, Severity.High, (int)member.ID, "Booking Failed", "");
                        return response;
                    }
                    else     
                    {
                        status.Category = "BK";
                        status.Description = "Unable to Book the SightSeeing";
                        status.StatusCode = "43";
                        response.Status = status;
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.Add(EventType.SightseeingBooking, Severity.High, (int)member.ID, "Unable to Book the SightSeeing", "");
                        return response;
                    }
                }
                catch (BookingEngineException ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "44";
                    status.Description = "Technical fault 03";
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.SightseeingBooking, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to BookingEngineException Exception", member, ex, "\n\n ItemName:" + request.ItemName + "\n\n Booking Source : " + request.Source + "\n\n Item Code : " + request.ItemCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard )
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                    #endregion

                #region catch (ArgumentException ex)
                catch (ArgumentException ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "45";
                    status.Description = ex.Message.ToString() + "| Date Time:" + DateTime.Now.ToString();
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.SightseeingBooking, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to ArgumentException Exception", member, ex, "\n\n ItemName:" + request.ItemName + "\n\n Booking Source : " + request.Source + "\n\n Item Code : " + request.ItemCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                #endregion

                #region catch (Exception ex)
                catch (Exception ex)
                {
                    status.Category = "BK";
                    status.Description = "Technical fault 04";
                    status.StatusCode = "46";
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.SightseeingBooking, Severity.High, (int)member.ID, "B2C-SightSeeing-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to Exception Exception", member, ex, "\n\n ItemName:" + request.ItemName + "\n\n Booking Source : " + request.Source + "\n\n Item Code : " + request.ItemCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                #endregion

                return response;
            }
        }
        else
        {
            WSSightSeeingBookingResponse errorResponse = new WSSightSeeingBookingResponse();
            status.Category = "BK";
            status.Description = "Authentication failed for book";
            status.StatusCode = "02";
            errorResponse.Status = status;
            if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
            {
                cardInfo.ReferenceId = 0;
                cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                cardInfo.Save();
            }
            Audit.Add(EventType.SightseeingBooking, Severity.High, 0, "B2C-SightSeeing-Login Failed for Member for Book Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
        
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetSightSeeingBookingResponse GetSightSeeingBooking(int sightSeeingId,string conNo)
    {

        Trace.TraceInformation("SightSeeingApi.GetSightSeeingBooking entered.");
        UserMaster member = new UserMaster();
        member = WSValidate(Credential);
        if (member != null)
        {
            if (sightSeeingId < 0 && string.IsNullOrEmpty(conNo))
            {
                WSGetSightSeeingBookingResponse errorResponse = new WSGetSightSeeingBookingResponse();
                WSStatus status = new WSStatus();
                status.Category = "GB";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.Add(EventType.GetBooking, Severity.High, (int)member.ID, "B2C-SightSeeing-Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }
            else
            {
                WSGetSightSeeingBookingResponse response = new WSGetSightSeeingBookingResponse();
                WSStatus status = new WSStatus();
                SightseeingItinerary itineary = new SightseeingItinerary();
                try
                {
                    if (!string.IsNullOrEmpty(conNo))
                    {
                        sightSeeingId = SightseeingItinerary.GetSightseeingId(conNo);
                    }
                    itineary.Load(sightSeeingId);
                }
                catch (Exception ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "06";
                    status.Description = "Technical Fault 02";
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.GetBooking, Severity.High, (int)member.ID, "B2C-SightSeeing- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetSightSeeingBooking Method Failed, due to Exception", member, ex, "\n\n SightSeeingId : " + sightSeeingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                try
                {
                    WSSightSeeingBookingDetail hBookingDetail = new WSSightSeeingBookingDetail();
                    hBookingDetail = WSSightSeeingBookingDetail.BuildGetBookingResponse(itineary);
                    response.BookingDetail = hBookingDetail;
                    status.Category = "GB";
                    status.StatusCode = "01";
                    status.Description = "Successful";
                    response.Status = status;
                    Audit.Add(EventType.GetBooking, Severity.High, (int)member.ID, "GetSightSeeingBooking Successful", "");
                }
                catch (BookingEngineException ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "07";
                    status.Description = "Technical Fault 03";
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.GetBooking, Severity.High, (int)member.ID, "B2C-SightSeeing- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetSightSeeingBooking Method Failed, due to BookingEngineException Exception", member, ex, "\n\n SightSeeingId : " + sightSeeingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                catch (ArgumentException ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "08";
                    status.Description = "Error Returned while filling booking details.: " + ex.Message;
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.GetBooking, Severity.High, (int)member.ID, "B2C-SightSeeing- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetSightSeeingBooking Method Failed, due to ArgumentException Exception", member, ex, "\n\n SightSeeingId : " + sightSeeingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "09";
                    status.Description = "Technical Fault 04";
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.GetBooking, Severity.High, (int)member.ID, "B2C-SightSeeing- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetSightSeeingBooking Method Failed, due to Exception Exception", member, ex, "\n\n SightSeeingId : " + sightSeeingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                return response;
            }
        }
        else
        {
            WSGetSightSeeingBookingResponse errorResponse = new WSGetSightSeeingBookingResponse();
            WSStatus status = new WSStatus();
            status.Category = "GB";
            status.Description = "Authentication failed for book";
            status.StatusCode = "02";
            errorResponse.Status = status;
            Audit.Add(EventType.GetBooking, Severity.High, 0, "B2C-SightSeeing-Login Failed for Member for GetSightSeeingBooking Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }


    [WebMethod]
    [SoapHeader("Credential")]
    public bool CancelBooking(int bookingId, string data, string custEmail)
    {
        try
        {
            UserMaster member = new UserMaster();
            //validating and loading member from credential
            member = WSValidate(Credential);
            AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
            LoginInfo loginfo = AgentLoginInfo;
            int loggedMemberId = Convert.ToInt32(loginfo.UserID);
            int requestTypeId = 6;
            AgentMaster agency = null;
            ServiceRequest serviceRequest = new ServiceRequest();
            BookingDetail booking = new BookingDetail();
            booking = new BookingDetail(bookingId);
            SightseeingItinerary itineary = new SightseeingItinerary();
            Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].ProductTypeId == (int)ProductType.SightSeeing)
                {
                    itineary.Load(products[i].ProductId);
                    break;
                }
            }
            agency = new AgentMaster(booking.AgencyId);
            serviceRequest.BookingId = bookingId;
            serviceRequest.ReferenceId = itineary.SightseeingId;
            serviceRequest.ProductType = ProductType.SightSeeing;

            serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
            serviceRequest.Data = data;
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;

            serviceRequest.CreatedBy = Convert.ToInt32(loginfo.UserID);
            //serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
            serviceRequest.IsDomestic = itineary.IsDomestic;

            serviceRequest.PaxName = itineary.PaxNames[0];
            serviceRequest.Pnr = itineary.ConfirmationNo;
            serviceRequest.Source = (int)itineary.Source;
            serviceRequest.StartDate = itineary.TourDate;
            serviceRequest.SupplierName = itineary.ItemName;
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
            serviceRequest.ReferenceNumber = itineary.BookingReference;
            serviceRequest.PriceId = itineary.Price.PriceId;
            serviceRequest.AgencyId = loginfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
            serviceRequest.ItemTypeId = InvoiceItemTypeId.SightseeingBooking;
            serviceRequest.DocName = "";
            serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
            serviceRequest.LastModifiedOn = DateTime.Now;
            serviceRequest.CreatedOn = DateTime.Now;
            //Added by shiva
            serviceRequest.IsDomestic = true;
            serviceRequest.AgencyTypeId = Agencytype.Cash;
            serviceRequest.RequestSourceId = RequestSource.BookingAPI;
            serviceRequest.Save();
            // Setting booking details status 
            if (requestTypeId == 6)
            {
                booking.SetBookingStatus(BookingStatus.InProgress, loggedMemberId);
            }
            else
            {
                booking.SetBookingStatus(BookingStatus.AmendmentInProgress, loggedMemberId);
            }

            BookingHistory bh = new BookingHistory();
            bh.BookingId = bookingId;
            bh.Remarks = "Request sent to " + serviceRequest.RequestType.ToString() + " of the SightSeeing booking.";

            if (requestTypeId == 6)
            {
                bh.EventCategory = EventCategory.SightSeeingCancel;
            }
            else
            {
                bh.EventCategory = EventCategory.SightSeeingAmendment;
            }
            bh.CreatedBy = (int)loginfo.UserID;
            bh.Save();

            try
            {
                //Sending email.
                Hashtable table = new Hashtable();
                table.Add("agentName", agency.Name);
                table.Add("ItemName", itineary.ItemName);
                table.Add("confirmationNo", itineary.ConfirmationNo);

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                UserMaster bookedBy = new UserMaster(itineary.CreatedBy);
                AgentMaster bookedAgency = new AgentMaster(itineary.AgentId);
                toArray.Add(bookedBy.Email);
                toArray.Add(bookedAgency.Email1);
                toArray.Add(custEmail);
                string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["SIGHTSEEING_CANCEL_MAIL"]).Split(';');
                foreach (string cnMail in cancelMails)
                {
                    toArray.Add(cnMail);
                }
                toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);

                string message = ConfigurationManager.AppSettings["SIGHTSEEING_CANCEL_REQUEST"];
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(SightSeeing)Request for cancellation. Confirmation No:(" + itineary.ConfirmationNo + ")", message, table);
            }
            catch { }
            return true;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelBook, Severity.High, 1, "Exception in the SightSeeing Change Request. Message:" + ex.Message, "");
            return false;
        }

    }
    
    #endregion

    #region private methods
    private bool WSBookInput(WSSightSeeingBookingRequest request)
    {
        try
        {
            if (request.SessionId == null || request.SessionId == "")
            {
                statusDescription = "Session Id cannot be blank or null";
                statusCode = "03";
                return false;
            }
            if (request.ItemCode == null || request.ItemCode == "")
            {
                statusDescription = "ItemCode cannot be blank or null";
                statusCode = "35";
                return false;
            }
            if (request.ItemName == null || request.ItemName == "")
            {
                statusDescription = "ItemName cannot be blank or null";
                statusCode = "36";
                return false;
            }
            if (request.PaxNames == null || request.PaxNames.Count <= 0)
            {
                statusDescription = "Pax Details Canot be blank or null";
                statusCode = "48";
                return false;
            }
        }
        catch (Exception Ex)
        {
            Audit.Add(EventType.SightseeingBooking, Severity.Low, 1, Ex.Message.ToString() + "|" + Ex.StackTrace.ToString(), "");
        }
        return true;
    }
    private bool WSSearchInput(WSSightSeeingRequest request)
    {
        DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
        if (request.TourDate < currentDate)
        {
            statusDescription = "Invalid checkIn date " + request.TourDate.ToString() + ". tour date must be greater then today date.";
            statusCode = "03";
            return false;
        }
        if (request.CityName == null || (request.CityName != null && request.CityName == ""))
        {
            statusDescription = "City Name should not be null or blank.";
            statusCode = "05";
            return false;
        }
        if (request.NoOfAdults <= 0) //request.NoOfAdults == null || 
        {
            statusDescription = "RoomGuest array should not be null or of length zero.";
            statusCode = "08";
            return false;
        }
        return true;
    }
    private static UserMaster WSValidate(AuthenticationData credential)
    {
        UserMaster member = new UserMaster();
        WLSightSeeingService bApi = new WLSightSeeingService();
        if (credential.SiteName != null && credential.SiteName != "")
        {
            try
            {
                member = bApi.GetMemberBySiteName(credential.SiteName);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.High, 0, "B2C-SightSeeing-Credential Failed " + credential.SiteName + " Site is not configure for whitelabel " + ex.Message, "");
                return null;
            }
        }
        return member;
    }

   

    /// <summary>
    /// Method to send error mail
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="member"></param>
    private void SendErrorMail(string subject, UserMaster member, Exception ex, string message)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        Hashtable hostPort = con.GetHostPort();
        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
        string fromEmailId = hostPort["fromEmail"].ToString();
        if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
        {
            try
            {
                string mailMessage = string.Empty;
                mailMessage = "Member Name: " + member.FirstName + "" + member.LastName;                 //member.FullName;
                mailMessage += "\n AgencyId: " + member.AgentId.ToString();
                mailMessage += message;
                mailMessage += "\n\n\n\nException: " + ex.Message;
                mailMessage += "\n\nStack trace: " + ex.StackTrace;
                Email.Send(fromEmailId, errorNotificationMailingId, "B2C-SightSeeing-Error in SightSeeing Booking API " + subject, mailMessage);
            }
            catch (Exception excep)
            {
                Audit.Add(EventType.Email, Severity.High, (int)member.ID, "B2C-SightSeeing-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
            }
        }
    }
    private UserMaster GetMemberBySiteName(string siteName)
    {
        UserPreference pref = new UserPreference();
        int memberId = 0;
        try
        {
            memberId = pref.GetMemberIdBySiteName(siteName);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Login, Severity.High, 0, "B2C-SightSeeing-GetMemberIdBySiteName Failed: " + ex.Message, "");
        }
        if (memberId > 0)
        {
            return new UserMaster(memberId);
        }
        else
        {
            return null;
        }
    }
    /// <summary>
    /// Serializes the given object.
    /// </summary>
    /// <typeparam name="T">The type of the object to be serialized.</typeparam>
    /// <param name="obj">The object to be serialized.</param>
    /// <returns>String representation of the serialized object.</returns>
    protected string Serialize<T>(T obj)
    {
        XmlSerializer xs = null;
        StringWriter sw = null;
        try
        {
            xs = new XmlSerializer(typeof(T));
            sw = new StringWriter();
            xs.Serialize(sw, obj);
            sw.Flush();
            return sw.ToString();
        }
        catch { }
        finally
        {
            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }
        }
        return "Error in Serialisation";
    }
    
    #endregion

}

