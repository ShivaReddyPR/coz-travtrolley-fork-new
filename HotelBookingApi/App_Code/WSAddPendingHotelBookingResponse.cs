/// <summary>
/// Summary description for WSAddPendingHotelBookingResponse
/// </summary>
public class WSAddPendingHotelBookingResponse
{
    private string hotelQueueId;
    public string HotelQueueId
    {
        get { return hotelQueueId; }
        set { hotelQueueId = value; }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }
    public WSAddPendingHotelBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
