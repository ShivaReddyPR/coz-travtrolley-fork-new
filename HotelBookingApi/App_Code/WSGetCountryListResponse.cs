using System.Collections.Generic;

/// <summary>
/// Summary description for WSGetCountryListResponse
/// </summary>
public class WSGetCountryListResponse
{
    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    private List<string> countryList;
    public List<string> CountryList
    {
        get
        {
            return countryList;
        }
        set
        {
            countryList = value;
        }
    }

    public WSGetCountryListResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
