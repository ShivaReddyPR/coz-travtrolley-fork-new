/// <summary>
/// Summary description for WSPaymentInformation
/// </summary>
public class WSPaymentInformation
{
    private string paymentId;
    public string PaymentId
    {
        get
        {
            return paymentId;
        }
        set
        {
            paymentId = value;
        }
    }

    private decimal amount;
    public decimal Amount
    {
        get
        {
            return amount;
        }
        set
        {
            amount = value;
        }
    }

    private string ipAddress;
    public string IPAddress
    {
        get
        {
            return ipAddress;
        }
        set
        {
            ipAddress = value;
        }
    }

    private string trackId;
    public string TrackId
    {
        get
        {
            return trackId;
        }
        set
        {
            trackId = value;
        }
    }

    private WSPaymentGatewaySource paymentGateway;
    public WSPaymentGatewaySource PaymentGateway
    {
        get
        {
            return paymentGateway;
        }
        set
        {
            paymentGateway = value;
        }
    }

    private WSPaymentModeType paymentModeType;
    public WSPaymentModeType PaymentModeType
    {
        get
        {
            return paymentModeType;
        }
        set
        {
            paymentModeType = value;
        }
    }
    //CreditCaredCharges
    private decimal creditCardCharges;
    public decimal CreditCardCharges
    {
        get { return creditCardCharges; }
        set { creditCardCharges = value; }
    }

    

    public WSPaymentInformation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public enum WSPaymentModeType
{
    CreditCard = 1,
    Deposited = 2
}
public enum WSPaymentGatewaySource
{
    HDFC = 1,
    AMEX = 2,
    ICICI = 3,
    OXICASH = 4,
    APICustomer = 5,
    SBI = 6,
    CCAvenue = 7,
    IANSource = 8,
    Beam = 9,
    TicketVala = 10,
    Axis = 11,
    CCAv=12,
    CozmoPG = 13,
    NEOPG=14
}
