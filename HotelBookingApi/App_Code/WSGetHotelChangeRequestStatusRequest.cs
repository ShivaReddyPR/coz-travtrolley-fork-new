using System;
//using Technology.BookingEngine;
//using CoreLogic;
//using CoreLogic;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
/// <summary>
/// Summary description for WSGetHotelChangeRequestStatusRequest
/// </summary>
public class WSGetHotelChangeRequestStatusRequest
{
    private int changeRequestId;
    public int ChangeRequestId
    {
        get
        {
            return changeRequestId;
        }
        set
        {
            changeRequestId = value;
        }
    }    

    public WSGetHotelChangeRequestStatusRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static WSGetHotelChangeRequestStatusResponse GetChangeRequestStatus(WSGetHotelChangeRequestStatusRequest request, UserMaster member)
    {
        WSGetHotelChangeRequestStatusResponse response = new WSGetHotelChangeRequestStatusResponse();
        WSStatus status = new WSStatus();
        ServiceRequest serviceRequest;
        BookingDetail booking = new BookingDetail();
        HotelItinerary itineary = new HotelItinerary();
        try
        {
            serviceRequest = new ServiceRequest(request.ChangeRequestId);
            if (serviceRequest != null)
            {
                if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Unassigned)
                {
                    response.ChangeRequestStatus = WSChangeRequestStatus.Pending;
                }
                else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Assigned)
                {
                    response.ChangeRequestStatus = WSChangeRequestStatus.InProgress;
                }
                else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Acknowledged)
                {
                    response.ChangeRequestStatus = WSChangeRequestStatus.InProgress;
                }
                else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Completed)
                {
                    response.ChangeRequestStatus = WSChangeRequestStatus.Processed;
                }
                else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Closed)
                {
                    response.ChangeRequestStatus = WSChangeRequestStatus.Processed;
                }
                else if (serviceRequest.ServiceRequestStatusId == ServiceRequestStatus.Rejected)
                {
                    response.ChangeRequestStatus = WSChangeRequestStatus.Rejected;
                }
                else
                {
                    response.ChangeRequestStatus = WSChangeRequestStatus.Processed;
                }
            }            
            booking = new BookingDetail(serviceRequest.BookingId);
            Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
            HotelRoom room = new HotelRoom();
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].ProductTypeId == (int)ProductType.Hotel)
                {
                    itineary.Load(products[i].ProductId);
                    itineary.Roomtype = room.Load(products[i].ProductId);
                    break;
                }
            }            
            CancellationCharges cancelCharge = new CancellationCharges();
            cancelCharge.Load(serviceRequest.ReferenceId, ProductType.Hotel);            
            decimal totalPrice = 0;
            for (int j = 0; j < itineary.Roomtype.Length; j++)
            {
                if (itineary.IsDomestic)
                {
                    totalPrice += itineary.Roomtype[j].Price.PublishedFare + itineary.Roomtype[j].Price.Tax;
                }
                else
                {
                    totalPrice += itineary.Roomtype[j].Price.NetFare + itineary.Roomtype[j].Price.Markup;
                }
            }
            decimal amount = totalPrice - cancelCharge.AdminFee - cancelCharge.SupplierFee;
            if (response.CancellationCharge > 0)
            {
                response.RefundedAmount = amount;
            }
            else
            {
                response.RefundedAmount = 0;
            }
            response.CancellationCharge = cancelCharge.AdminFee + cancelCharge.SupplierFee;
            response.ChangeRequestId = request.ChangeRequestId;
            status.Category = "CRS";
            status.Description = "Sucessfull";
            status.StatusCode = "01";            
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.GetHotelChangeRequestStatus, Severity.High, (int)member.ID, "B2C-Hotel-GetHotelChangeRequestStatus Successful", "");
        }
        catch (BookingEngineException ex)
        {
            status.Category = "CRS";
            status.StatusCode = "04";
            status.Description = "Technical Fault 01.";
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.GetHotelChangeRequestStatus, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
            return response;
        }
        catch (ArgumentException ex)
        {
            status.Category = "CRS";
            status.StatusCode = "05";
            status.Description = "GetHotelChangeRequestStatus method Failed.: " + ex.Message;
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.GetHotelChangeRequestStatus, Severity.High, (int)member.ID, " B2C-Hotel-Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
            return response;
        }
        catch (Exception ex)
        {
            status.Category = "CRS";
            status.StatusCode = "06";
            status.Description = "Technical Fault 02";
            response.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.GetHotelChangeRequestStatus, Severity.High, (int)member.ID, " B2C-Hotel-Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
            return response;
        }
        return response;
    }
}
