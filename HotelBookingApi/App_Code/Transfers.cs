﻿using System;
using System.Collections;
//using System.Linq;
using System.Web.Services;
using System.Web.Services.Protocols;
//using System.Xml.Linq;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.MetaSearchEngine;
using CT.AccountingEngine;
using System.Configuration;
using System.Xml.Serialization;
using System.IO;
using CT.Configuration;

/// <summary>
/// Summary description for Transfers
/// </summary>
[WebService(Namespace = "http://CT/HotelBookingApi")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Transfers : System.Web.Services.WebService
{
    #region Variables
    public AuthenticationData Credential;
    private string statusDescription;
    private string statusCode;
    private LoginInfo AgentLoginInfo;
    #endregion

    public Transfers()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }



    [WebMethod]
    [SoapHeader("Credential")]
    public WSTransferSearchResult[] GetTransferAvailability(WSTransferRequest req)
    {
        List<WSTransferSearchResult> TransferResults = new List<WSTransferSearchResult>();

        try
        {
            UserPreference pref = new UserPreference();
            int memberId = pref.GetMemberIdBySiteName(Credential.SiteName);
            AgentLoginInfo = UserMaster.GetB2CUser(memberId);

            TransferRequest request = new TransferRequest();
            PropertiesCopier.CopyProperties(req, request);
            PropertiesCopier.CheckRecursiveDifferentTypes(req, request);
            

            request.CityName = req.CityName;

            MetaSearchEngine mse = new MetaSearchEngine();
            mse.SettingsLoginInfo = AgentLoginInfo;
            TransferSearchResult[] results = mse.GetTransferResults(request, new UserMaster(memberId));

            //Updating B2C Price in Search Results
            WSTransferRequest.UpdatePrice(ref results, AgentLoginInfo.AgentId);
            //DataTable dtMarkup = UpdateMarkup.Load((int)AgentLoginInfo.AgentId, HotelBookingSource.GTA.ToString(), (int)ProductType.Transfers);
            //DataView dv = dtMarkup.DefaultView;
            
            List<TransferSearchResult> tranResults = new List<TransferSearchResult>();

            foreach (TransferSearchResult result in results)
            {
                WSTransferSearchResult wlresult = new WSTransferSearchResult();
                wlresult.ApproximateTransferTime = result.ApproximateTransferTime;
                wlresult.ApproximateTransferTime_Str = result.ApproximateTransferTime_Str;
                wlresult.City = result.City;
                wlresult.CityCode = result.CityCode;
                wlresult.Currency = result.Currency;
                wlresult.DropOff = new WSTransferSearchResult.WLTransferDropOffDetails();
                wlresult.DropOff.DropOffAllowForCheckInTime = result.DropOff.DropOffAllowForCheckInTime;
                wlresult.DropOff.DropOffCode = result.DropOff.DropOffCode;
                wlresult.DropOff.DropOffDetailCode = result.DropOff.DropOffDetailCode;
                wlresult.DropOff.DropOffDetailName = result.DropOff.DropOffDetailName;
                wlresult.DropOff.DropOffName = result.DropOff.DropOffName;
                
                wlresult.HasExtraInfo = result.HasExtraInfo;
                wlresult.HasIdeas = result.HasIdeas;
                wlresult.ItemCode = result.ItemCode;
                wlresult.ItemName = result.ItemName;
                wlresult.OutOfHoursSupplements = new WSTransferSearchResult.WLTransferOutOfHoursSupplement[result.OutOfHoursSupplements.Length];
                int counter = 0;
                foreach (TransferSearchResult.TransferOutOfHoursSupplement supplement in result.OutOfHoursSupplements)
                {
                    wlresult.OutOfHoursSupplements[counter].Details = supplement.Details;
                    wlresult.OutOfHoursSupplements[counter].FromTime = supplement.FromTime;
                    wlresult.OutOfHoursSupplements[counter].Supplement = supplement.Supplement;
                    wlresult.OutOfHoursSupplements[counter].ToTime = supplement.ToTime;
                    counter++;
                }
                wlresult.PickUp = new WSTransferSearchResult.WLTransferPickUpDetails();
                wlresult.PickUp.PickUpCode = result.PickUp.PickUpCode;
                wlresult.PickUp.PickUpDetailCode = result.PickUp.PickUpDetailCode;
                wlresult.PickUp.PickUpDetailName = result.PickUp.PickUpDetailName;
                wlresult.PickUp.PickUpName = result.PickUp.PickUpName;
                wlresult.PickUp.PickUpPointDetails = new List<WSTransferSearchResult.WSTransferPickUpPointDetails>();
                if (result.PickUp.PickUpPointDetails != null)
                {
                    foreach (TransferSearchResult.TransferPickUpPointDetails pickup in result.PickUp.PickUpPointDetails)
                    {
                        WSTransferSearchResult.WSTransferPickUpPointDetails wlpickup = new WSTransferSearchResult.WSTransferPickUpPointDetails();
                        wlpickup.PickUpPointCityCode = pickup.PickUpPointCityCode;
                        wlpickup.PickUpPointCode = pickup.PickUpPointCode;
                        wlpickup.PickUpPointName = pickup.PickUpPointName;

                        wlresult.PickUp.PickUpPointDetails.Add(wlpickup);
                    }
                }
                wlresult.price = result.price;
                wlresult.Source = (WSTransferBookingSource)result.Source;
                wlresult.Conditions = result.Conditions;
                wlresult.Vehicles = new WSTransferSearchResult.WLTransferVehicleDetails[result.Vehicles.Length];
                CT.BookingEngine.Airport.GetInfoForCityOrAirport("");
                /////Apply B2C Markup
                //dv.RowFilter = "TransType IN('B2C')";
                //dtMarkup = dv.ToTable();
                //decimal markup = 0; string markupType = string.Empty;
                //if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                //{
                //    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                //    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                //}
                for (int i = 0; i < result.Vehicles.Length; i++)
                {
                    wlresult.Vehicles[i].Confirmation = result.Vehicles[i].Confirmation;
                    wlresult.Vehicles[i].ConfirmationCode = result.Vehicles[i].ConfirmationCode;
                    wlresult.Vehicles[i].ItemPrice = result.Vehicles[i].ItemPrice;
                    wlresult.Vehicles[i].ItemPriceCurrency = result.Vehicles[i].ItemPriceCurrency;
                    wlresult.Vehicles[i].Language = result.Vehicles[i].Language;
                    wlresult.Vehicles[i].LanguageCode = result.Vehicles[i].LanguageCode;
                    wlresult.Vehicles[i].PriceInfo = result.Vehicles[i].PriceInfo;
                    //commented by brahmam below code is not required
                    //wlresult.Vehicles[i].PriceInfo = new PriceAccounts();
                    //wlresult.Vehicles[i].PriceInfo.AccPriceType = result.Vehicles[i].PriceInfo.AccPriceType;
                    //wlresult.Vehicles[i].PriceInfo.Markup = result.Vehicles[i].PriceInfo.Markup;
                    //wlresult.Vehicles[i].PriceInfo.MarkupType = result.Vehicles[i].PriceInfo.MarkupType;
                    //wlresult.Vehicles[i].PriceInfo.MarkupValue = result.Vehicles[i].PriceInfo.MarkupValue;
                    //wlresult.Vehicles[i].PriceInfo.NetFare = result.Vehicles[i].PriceInfo.NetFare;
                    //wlresult.Vehicles[i].PriceInfo.Tax = result.Vehicles[i].PriceInfo.Tax;

                    //if (markupType == "P")
                    //{
                    //    wlresult.Vehicles[i].PriceInfo.B2CMarkup = (wlresult.Vehicles[i].PriceInfo.NetFare + wlresult.Vehicles[i].PriceInfo.Markup) * markup / 100;
                    //}
                    //else
                    //{
                    //    wlresult.Vehicles[i].PriceInfo.B2CMarkup = markup;
                    //}
                    //wlresult.Vehicles[i].PriceInfo.B2CMarkupType = markupType;
                    //wlresult.Vehicles[i].PriceInfo.B2CMarkupValue = markup;
                    //wlresult.Vehicles[i].PriceInfo.SupplierCurrency = result.Vehicles[i].PriceInfo.SupplierCurrency;
                    //wlresult.Vehicles[i].PriceInfo.SupplierPrice = result.Vehicles[i].PriceInfo.SupplierPrice;

                    wlresult.Vehicles[i].Vehicle = result.Vehicles[i].Vehicle;
                    wlresult.Vehicles[i].VehicleCode = result.Vehicles[i].VehicleCode;
                    wlresult.Vehicles[i].VehicleMaximumLuggage = result.Vehicles[i].VehicleMaximumLuggage;
                    wlresult.Vehicles[i].VehicleMaximumPassengers = result.Vehicles[i].VehicleMaximumPassengers;
                }
                TransferResults.Add(wlresult);
            }            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.TransferSearch, Severity.High, 1, "(B2C Transfers)Failed to search for Transfers. Reason : " + ex.ToString(), "");
        }

        return TransferResults.ToArray();
    }
    
    [WebMethod]
    [SoapHeader("Credential")]
    public WSTransferStaticData GetTransferItemInformation(string cityCode, string itemCode)
    {
        WSTransferStaticData staticData = new WSTransferStaticData();
       
        CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
        TransferStaticData data = gtaApi.GetTransferItemInformation(cityCode, itemCode);

        staticData.CityCode = data.CityCode;
        staticData.Conditions = data.Conditions;
        staticData.Description = data.Description;
        staticData.FlashLinks = data.FlashLinks;
        staticData.ItemCode = data.ItemCode;
        staticData.MeetingPoint = data.MeetingPoint;
        staticData.Source = WSTransferBookingSource.GTA;
        staticData.TimeStamp = data.TimeStamp;
        staticData.TransferTime = data.TransferTime;
        staticData.Type = (WSTransferType)data.Type;
    
        return staticData;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSTransferCancellationData[] GetTransferChargeCondition(WSTransferItinerary itinearary, ref List<WSTransferPenalty> penalityList, bool savePenality)
    {
        List<WSTransferCancellationData> CancellationData = new List<WSTransferCancellationData>();

        UserPreference pref = new UserPreference();
        int memberId = pref.GetMemberIdBySiteName(Credential.SiteName);
        AgentLoginInfo = UserMaster.GetB2CUser(memberId);

        CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();


        gtaApi.AgentExchangeRates = AgentLoginInfo.AgentExchangeRates;
        gtaApi.AgentCurrency = AgentLoginInfo.Currency;
        gtaApi.AgentDecimalPoint = AgentLoginInfo.DecimalValue;

        List<TransferPenalty> penaltyList = new List<TransferPenalty>();
        TransferItinerary itinerary = new TransferItinerary();
        itinerary.BookingReference = itinearary.BookingReference;
        itinerary.BookingStatus = (TransferBookingStatus)itinearary.BookingStatus;
        itinerary.CancelId = itinearary.CancelId;
        itinerary.CancellationPolicy = itinearary.CancellationPolicy;
        itinerary.CityCode = itinearary.CityCode;
        itinerary.ConfirmationNo = itinearary.ConfirmationNo;
        itinerary.CreatedBy = itinearary.CreatedBy;
        itinerary.CreatedOn = itinearary.CreatedOn;
        itinerary.DropOffCode = itinearary.DropOffCode;
        itinerary.DropOffDescription = itinearary.DropOffDescription;
        itinerary.DropOffRemarks = itinearary.DropOffRemarks;
        itinerary.DropOffTime = itinearary.DropOffTime;
        itinerary.DropOffType = (PickDropType)itinearary.DropOffType;
        itinerary.IsDomestic = itinearary.IsDomestic;
        itinerary.ItemCode = itinearary.ItemCode;
        itinerary.ItemName = itinearary.ItemName;
        itinerary.Language = itinearary.Language;
        itinerary.TransferDate = itinearary.TransferDate;
        itinerary.TransferDetails = new List<TransferVehicle>();
        foreach (WSTransferVehicle vehicle in itinearary.TransferDetails)
        {
            TransferVehicle transVehicle = new TransferVehicle();
            transVehicle.Currency = vehicle.Currency;
            transVehicle.VehicleCode = vehicle.VehicleCode;
            transVehicle.VehicleMaximumLuggage = vehicle.VehicleMaximumLuggage;
            transVehicle.VehicleMaximumPassengers = vehicle.VehicleMaximumPassengers;

            itinerary.TransferDetails.Add(transVehicle);
        }

        Dictionary<string, string> cancelData = gtaApi.GetTransferChargeCondition(itinerary, ref penaltyList, true);

        foreach (KeyValuePair<string, string> pair in cancelData)
        {
            WSTransferCancellationData item = new WSTransferCancellationData();
            item.CancelKey = pair.Key;
            item.CancelValue = pair.Value;
            CancellationData.Add(item);
        }
        return CancellationData.ToArray();
    }

    [WebMethod]
    public WSTransferCancellationData[] GetStations(string cityCode)
    {
        WSTransferCancellationData[] stationList = new WSTransferCancellationData[0];

        GTACity cityList = new GTACity();
        Dictionary<string,string> stations = cityList.GetStations(cityCode);

        List<WSTransferCancellationData> Values = new List<WSTransferCancellationData>();
        foreach (KeyValuePair<string, string> pair in stations)
        {
            WSTransferCancellationData item = new WSTransferCancellationData();
            item.CancelKey = pair.Key;
            item.CancelValue = pair.Value;
            Values.Add(item);
        }

        stationList = Values.ToArray();

        return stationList;
    }

    [WebMethod]
    public WSTransferCancellationData[] GetAirportsForCity(string cityCode)
    {
        List<WSTransferCancellationData> Airports = new List<WSTransferCancellationData>();

        List<Airport> AirportList = Airport.GetInfoForCityOrAirport(cityCode);

        foreach (Airport airport in AirportList)
        {
            WSTransferCancellationData item = new WSTransferCancellationData();
            item.CancelKey = airport.AirportName;
            item.CancelValue = airport.AirportCode;
            Airports.Add(item);
        }

        return Airports.ToArray();
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public PriceAccounts GetTransferVehicleItemPrice(WSTransferSearchResult result, int agencyId, decimal otherCharges, int dayIndex, PriceType priceType)
    {
        PriceAccounts price = new PriceAccounts();

        TransferSearchResult searchResult = new TransferSearchResult();
        searchResult.ApproximateTransferTime = result.ApproximateTransferTime;
        searchResult.ApproximateTransferTime_Str = result.ApproximateTransferTime_Str;
        searchResult.City = result.City;
        searchResult.CityCode = result.CityCode;
        searchResult.Currency = result.Currency;
        searchResult.DropOff = new TransferSearchResult.TransferDropOffDetails();
        searchResult.DropOff.DropOffAllowForCheckInTime = result.DropOff.DropOffAllowForCheckInTime;
        searchResult.DropOff.DropOffCode = result.DropOff.DropOffCode;
        searchResult.DropOff.DropOffDetailCode = result.DropOff.DropOffDetailCode;
        searchResult.DropOff.DropOffDetailName = result.DropOff.DropOffDetailName;
        searchResult.DropOff.DropOffName = result.DropOff.DropOffName;
        searchResult.HasExtraInfo = result.HasExtraInfo;
        searchResult.HasIdeas = result.HasIdeas;
        searchResult.ItemCode = result.ItemCode;
        searchResult.ItemName = result.ItemName;
        searchResult.OutOfHoursSupplements = new TransferSearchResult.TransferOutOfHoursSupplement[result.OutOfHoursSupplements.Length];
        if (result.OutOfHoursSupplements != null)
        {
            for (int i = 0; i < result.OutOfHoursSupplements.Length; i++)
            {
                searchResult.OutOfHoursSupplements[i] = new TransferSearchResult.TransferOutOfHoursSupplement();
                searchResult.OutOfHoursSupplements[i].Details = result.OutOfHoursSupplements[i].Details;
                searchResult.OutOfHoursSupplements[i].FromTime = result.OutOfHoursSupplements[i].FromTime;
                searchResult.OutOfHoursSupplements[i].Supplement = result.OutOfHoursSupplements[i].Supplement;
                searchResult.OutOfHoursSupplements[i].ToTime = result.OutOfHoursSupplements[i].ToTime;
            }
        }

        searchResult.PickUp = new TransferSearchResult.TransferPickUpDetails();
        searchResult.PickUp.PickUpCode = result.PickUp.PickUpCode;
        searchResult.PickUp.PickUpDetailCode = result.PickUp.PickUpDetailCode;
        searchResult.PickUp.PickUpDetailName = result.PickUp.PickUpDetailName;
        searchResult.PickUp.PickUpName = result.PickUp.PickUpName;
        searchResult.PickUp.PickUpPointDetails = new List<TransferSearchResult.TransferPickUpPointDetails>();
        foreach (WSTransferSearchResult.WSTransferPickUpPointDetails pickUpPoint in result.PickUp.PickUpPointDetails)
        {
            TransferSearchResult.TransferPickUpPointDetails PUPoint = new TransferSearchResult.TransferPickUpPointDetails();
            PUPoint.PickUpPointCityCode = pickUpPoint.PickUpPointCityCode;
            PUPoint.PickUpPointCode = pickUpPoint.PickUpPointCode;
            PUPoint.PickUpPointName = pickUpPoint.PickUpPointName;

            searchResult.PickUp.PickUpPointDetails.Add(PUPoint);
        }
        searchResult.price = result.price;
        searchResult.Source = (TransferBookingSource)result.Source;
        searchResult.Vehicles = new TransferSearchResult.TransferVehicleDetails[result.Vehicles.Length];
        for (int i = 0; i < result.Vehicles.Length; i++)
        {
            searchResult.Vehicles[i] = new TransferSearchResult.TransferVehicleDetails();
            searchResult.Vehicles[i].Confirmation = result.Vehicles[i].Confirmation;
            searchResult.Vehicles[i].ConfirmationCode = result.Vehicles[i].ConfirmationCode;
            searchResult.Vehicles[i].ItemPrice = result.Vehicles[i].ItemPrice;
            searchResult.Vehicles[i].ItemPriceCurrency = result.Vehicles[i].ItemPriceCurrency;
            searchResult.Vehicles[i].Language = result.Vehicles[i].Language;
            searchResult.Vehicles[i].LanguageCode = result.Vehicles[i].LanguageCode;
            searchResult.Vehicles[i].PriceInfo = result.Vehicles[i].PriceInfo;
            searchResult.Vehicles[i].Vehicle = result.Vehicles[i].Vehicle;
            searchResult.Vehicles[i].VehicleCode = result.Vehicles[i].VehicleCode;
            searchResult.Vehicles[i].VehicleMaximumLuggage = result.Vehicles[i].VehicleMaximumLuggage;
            searchResult.Vehicles[i].VehicleMaximumPassengers = result.Vehicles[i].VehicleMaximumPassengers;
        }
        

        price = CT.AccountingEngine.AccountingEngine.GetPrice(searchResult, agencyId, otherCharges, dayIndex, priceType);

        return price;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public DateTime GetLastCancellationDateForTransfers(int lastCancellationDate, DateTime transferDate, WSTransferBookingSource source)
    {
        DateTime CancellationDate = new DateTime();

        MetaSearchEngine mse = new MetaSearchEngine();
        CancellationDate = mse.GetLastCancellationDateforTransfer(lastCancellationDate, transferDate, (TransferBookingSource)source);

        return CancellationDate;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSBookResponse BookTransfer(WSTransferBookRequest bookRequest)
    {
        Audit.Add(EventType.TransferBooking, Severity.High, 1, "B2C-TransfersBooking-Root:Transfers Book Request: " + Serialize<WSTransferBookRequest>(bookRequest) + " :Credentials:" + Serialize<AuthenticationData>(Credential), "");
        CreditCardPaymentInformation cardInfo = new CreditCardPaymentInformation();
        #region Checking if paymentMode is creditcard and payment info not null
        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
        {
            cardInfo.Amount = bookRequest.PaymentInformation.Amount;
            cardInfo.PaymentGateway = (CT.AccountingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.AccountingEngine.PaymentGatewaySource), bookRequest.PaymentInformation.PaymentGateway.ToString());
            cardInfo.PaymentId = bookRequest.PaymentInformation.PaymentId;
            cardInfo.TrackId = bookRequest.PaymentInformation.TrackId;
            cardInfo.IPAddress = bookRequest.PaymentInformation.IPAddress;
            cardInfo.Charges = bookRequest.PaymentInformation.CreditCardCharges;
        }
        #endregion

        UserMaster member = new UserMaster();

        //Validating member from credentials
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        WSBookResponse response = new WSBookResponse();
        WSStatus status = new WSStatus();
        if (member != null)
        {
            //Validating BookRequest input
            if (!(WSBookInput(bookRequest)))
            {
                WSBookResponse errorResponse = new WSBookResponse();
                status.Category = "BK";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.Add(EventType.TransferBooking, Severity.High, (int)member.ID, "B2C-Transfers-Input parameter is incorrect: " + statusDescription, "");
                if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                {
                    cardInfo.ReferenceId = 0;
                    cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                    cardInfo.Save();
                }
                return errorResponse;
            }
            else
            {
                try
                {
                    TransferItinerary itinerary = bookRequest.BuildItinerary();
                    BookingResponse bookResponse = new BookingResponse();
                    itinerary.ProductType = ProductType.Transfers;
                    itinerary.AgentId = AgentLoginInfo.AgentId;
                    itinerary.LocationId = (int)AgentLoginInfo.LocationID;
                    Product prod = (Product)itinerary;
                    MetaSearchEngine mse = new MetaSearchEngine();
                    #region Meta Search Engine and Booking Processing
                    bookResponse = mse.Book(ref prod, AgentLoginInfo.AgentId, BookingStatus.Ready, AgentLoginInfo.UserID, true);
                    response.ConfirmationNo = bookResponse.ConfirmationNo;
                    response.BookingId = Convert.ToString(bookResponse.BookingId);
                    response.ReferenceNo = itinerary.BookingReference;
                    response.HotelId = itinerary.TransferId;
                    if (bookResponse.Status == BookingResponseStatus.Successful)
                    {
                        #region invoice generation for Transfers

                        //Price info
                        int invoiceNumber = 0;
                        Invoice invoice = new Invoice();
                        try
                        {
                            invoiceNumber = Invoice.isInvoiceGenerated(response.HotelId, ProductType.Transfers);

                            if (invoiceNumber == 0)
                            {
                                invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(response.HotelId, string.Empty, (int)member.ID, ProductType.Transfers, 1);
                            }
                        }
                        catch (Exception exp)
                        {
                            Audit.Add(EventType.TransferBooking, Severity.High, (int)member.ID, "B2C-TransferBooking API Book:Exception while generating Invoice.. Message:" + exp.Message, "");
                        }
                        status.Category = "BK";
                        status.Description = "Successful";
                        status.StatusCode = "01";
                        response.Status = status;

                        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.Add(EventType.TransferBooking, Severity.High, (int)member.ID, "Book Successful", "");
                        #endregion
                    }
                    else if (bookResponse.Status == BookingResponseStatus.Failed)
                    {
                        status.Category = "BK";
                        status.Description = "Booking Failed.";
                        status.StatusCode = "42";
                        response.Status = status;
                        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.Add(EventType.TransferBooking, Severity.High, (int)member.ID, "Booking Failed", "");
                        return response;
                    }
                    else
                    {
                        status.Category = "BK";
                        status.Description = "Unable to Book the Transfers";
                        status.StatusCode = "43";
                        response.Status = status;
                        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.Add(EventType.TransferBooking, Severity.High, (int)member.ID, "Unable to Book the Transfers", "");
                        return response;
                    }

                }
                catch (BookingEngineException ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "44";
                    status.Description = "Technical fault 03";
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.TransferBooking, Severity.High, (int)member.ID, "B2C-Transfers-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to BookingEngineException Exception", member, ex, "\n\n ItemName:" + bookRequest.TransferItinerary.ItemName + "\n\n Booking Source : " + bookRequest.TransferItinerary.Source + "\n\n Item Code : " + bookRequest.TransferItinerary.ItemCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                    #endregion
                #region catch (ArgumentException ex)
                catch (ArgumentException ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "45";
                    status.Description = ex.Message.ToString() + "| Date Time:" + DateTime.Now.ToString();
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.TransferBooking, Severity.High, (int)member.ID, "B2C-Transfers-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to ArgumentException Exception", member, ex, "\n\n ItemName:" + bookRequest.TransferItinerary.ItemName + "\n\n Booking Source : " + bookRequest.TransferItinerary.Source + "\n\n Item Code : " + bookRequest.TransferItinerary.ItemCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                #endregion

                #region catch (Exception ex)
                catch (Exception ex)
                {
                    status.Category = "BK";
                    status.Description = "Technical fault 04";
                    status.StatusCode = "46";
                    response.Status = status;
                    try
                    {
                        Audit.Add(EventType.TransferBooking, Severity.High, (int)member.ID, "B2C-TransferBookingBookingtSeeing-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to Exception Exception", member, ex, "\n\n ItemName:" + bookRequest.TransferItinerary.ItemName + "\n\n Booking Source : " + bookRequest.TransferItinerary.Source + "\n\n Item Code : " + bookRequest.TransferItinerary.ItemCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                #endregion
            }
        }
        else
        {
            WSBookResponse errorResponse = new WSBookResponse();
            status.Category = "BK";
            status.Description = "Authentication failed for book";
            status.StatusCode = "02";
            errorResponse.Status = status;
            if (bookRequest.PaymentInformation != null && bookRequest.PaymentInformation.PaymentModeType == WSPaymentModeType.CreditCard)
            {
                cardInfo.ReferenceId = 0;
                cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                cardInfo.Save();
            }
            Audit.Add(EventType.TransferBooking, Severity.High, 0, "B2C-Transfers-Login Failed for Member for Book Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
        return response;
    }

    [WebMethod]
    public bool SaveIntlBookingDetails(WSIntlTransferBookingDetails bookingDetails)
    {
        bool isSaved = true;
        IntlTransferBookingDetails details = new IntlTransferBookingDetails();
        try
        {            
            details.DocumentName = (TypeOfDocument)bookingDetails.DocumentName;
            details.FileName = bookingDetails.FileName;
            details.TransferId = bookingDetails.TransferId;

            details.Save();
        }
        catch (Exception ex)
        {
            isSaved = false;
            Audit.AddAuditHotelBookingAPI(EventType.TransferBooking, Severity.High, 1, "(B2C Transfers)Failed to Save " + details.DocumentName + ". Reason : " + ex.ToString(), "");
        }
        return isSaved;
    }

    [WebMethod]
    public WSIntlTransferBookingDetails[] GetIntlTransferBookingDetails(int transferId)
    {
        List<WSIntlTransferBookingDetails> bookingList = new List<WSIntlTransferBookingDetails>();
        try
        {
            List<IntlTransferBookingDetails> detailsList = new List<IntlTransferBookingDetails>();
            IntlTransferBookingDetails details = new IntlTransferBookingDetails();
            detailsList = details.Load(transferId);

            foreach (IntlTransferBookingDetails detail in detailsList)
            {
                WSIntlTransferBookingDetails bdetail = new WSIntlTransferBookingDetails();
                bdetail.DocumentName = (WLTypeOfDocument)detail.DocumentName;
                bdetail.FileName = detail.FileName;
                bdetail.TransferId = detail.TransferId;

                bookingList.Add(bdetail);
            }
        }
        catch (Exception ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.TransferBooking, Severity.High, 1, "(B2C Transfers)Failed to Load IntlTransferBookingDetails for TransferId : " + transferId + ". Reason : " + ex.ToString(), "");
        }
        return bookingList.ToArray();
    }

    [WebMethod]
    public WSTransferItinerary RetrieveBooking(string confirmationNo)
    {
        WSTransferItinerary itinerary = new WSTransferItinerary();
        try
        {
            int transferId = TransferItinerary.GetTransferId(confirmationNo);

            TransferItinerary transfer = new TransferItinerary();
            transfer.Load(transferId);

            itinerary.BookingReference = transfer.BookingReference;
            itinerary.BookingStatus = (WSTransferBookingStatus)transfer.BookingStatus;
            itinerary.CancelId = transfer.CancelId;
            itinerary.CancellationPolicy = transfer.CancellationPolicy;
            itinerary.CityCode = transfer.CityCode;
            itinerary.ConfirmationNo = transfer.ConfirmationNo;
            itinerary.CreatedBy = transfer.CreatedBy;
            itinerary.CreatedOn = transfer.CreatedOn;
            
            itinerary.DropOffCode = transfer.DropOffCode;
            itinerary.DropOffDescription = transfer.DropOffDescription;
            itinerary.DropOffRemarks = transfer.DropOffRemarks;
            itinerary.DropOffTime = transfer.DropOffTime;
            itinerary.DropOffType = (WSPickDropType)transfer.DropOffType;

            itinerary.IsDomestic = transfer.IsDomestic;
            itinerary.ItemCode = transfer.ItemCode;
            itinerary.ItemName = transfer.ItemName;
            itinerary.Language = transfer.Language;
            itinerary.LastCancellationDate = transfer.LastCancellationDate;
            itinerary.LastModifiedBy = transfer.LastModifiedBy;
            itinerary.LastModifiedOn = transfer.LastModifiedOn;
            itinerary.NumOfPax = transfer.NumOfPax;
            itinerary.PassengerInfo = transfer.PassengerInfo;
            itinerary.TransferConditions = transfer.TransferConditions;
            itinerary.PenalityInfo = new List<WSTransferPenalty>();
            if (transfer.PenalityInfo != null)
            {
                foreach (TransferPenalty penalty in transfer.PenalityInfo)
                {
                    WSTransferPenalty wlpenalty = new WSTransferPenalty();
                    wlpenalty.BookingSource = (WSTransferBookingSource)penalty.BookingSource;
                    wlpenalty.FromDate = penalty.FromDate;
                    wlpenalty.IsAllowed = penalty.IsAllowed;
                    wlpenalty.PolicyType = (WLTransferChangePoicyType)penalty.PolicyType;
                    wlpenalty.Price = penalty.Price;
                    wlpenalty.Remarks = penalty.Remarks;
                    wlpenalty.ToDate = penalty.ToDate;
                    wlpenalty.TransferId = penalty.TransferId;
                    itinerary.PenalityInfo.Add(wlpenalty);
                }
            }

            itinerary.PickUpCode = transfer.PickUpCode;
            itinerary.PickUpDescription = transfer.PickUpDescription;
            itinerary.PickUpRemarks = transfer.PickUpRemarks;
            itinerary.PickUpTime = transfer.PickUpTime;
            itinerary.PickUpType = (WSPickDropType)transfer.PickUpType;

            itinerary.Price = transfer.Price;
            itinerary.Source =  WSTransferBookingSource.GTA;
            itinerary.TransferDate = transfer.TransferDate;
            itinerary.TransferDetails = new List<WSTransferVehicle>();
            if (transfer.TransferDetails != null)
            {
                foreach (TransferVehicle vehicle in transfer.TransferDetails)
                {
                    WSTransferVehicle wlvehicle = new WSTransferVehicle();
                    wlvehicle.Currency = vehicle.Currency;
                    wlvehicle.ItemPrice = vehicle.ItemPrice;
                    wlvehicle.OccupiedPax = vehicle.OccupiedPax;
                    wlvehicle.TransferId = vehicle.TransferId;
                    wlvehicle.Vehicle = vehicle.Vehicle;
                    wlvehicle.VehicleCode = vehicle.VehicleCode;
                    wlvehicle.VehicleMaximumLuggage = vehicle.VehicleMaximumLuggage;
                    wlvehicle.VehicleMaximumPassengers = vehicle.VehicleMaximumPassengers;

                    itinerary.TransferDetails.Add(wlvehicle);
                }
            }
            itinerary.TransferId = transfer.TransferId;
            itinerary.TransferTime = transfer.TransferTime;
            itinerary.VoucherStatus = transfer.VoucherStatus;
        }
        catch (Exception ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.TransferBooking, Severity.High, 1, "(B2C Transfers)Failed to get Transfer details for Confirmation No : " + confirmationNo + ". Reason : " + ex.ToString(), "");
        }
        return itinerary;
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public bool SendChangeRequest(int bookingId, string data, string custEmail)
    {
        try
        {
            UserMaster member = new UserMaster();
            member = WSValidate(Credential);
            AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
            LoginInfo loginfo = AgentLoginInfo;
            int loggedMemberId = Convert.ToInt32(loginfo.UserID);
            int requestTypeId = 8;
            AgentMaster agency = null;
            ServiceRequest serviceRequest = new ServiceRequest();
            BookingDetail booking = new BookingDetail();
            booking = new BookingDetail(bookingId);
            TransferItinerary itineary = new TransferItinerary();
            Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].ProductTypeId == (int)ProductType.Transfers)
                {
                    itineary.Load(products[i].ProductId);
                    break;
                }
            }
            agency = new AgentMaster(booking.AgencyId);

            serviceRequest.BookingId = bookingId;
            serviceRequest.ReferenceId = itineary.TransferId;
            serviceRequest.ProductType = ProductType.Transfers;

            serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
            serviceRequest.Data = data;
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;

            serviceRequest.CreatedBy = Convert.ToInt32(loginfo.UserID);
            //serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
            serviceRequest.IsDomestic = itineary.IsDomestic;

            serviceRequest.PaxName = itineary.PassengerInfo;
            serviceRequest.Pnr = itineary.ConfirmationNo;
            serviceRequest.Source = (int)itineary.Source;
            serviceRequest.StartDate = itineary.TransferDate;
            serviceRequest.SupplierName = itineary.ItemName;
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
            serviceRequest.ReferenceNumber = itineary.BookingReference;
            serviceRequest.PriceId = itineary.TransferDetails[0].ItemPrice.PriceId;
            serviceRequest.AgencyId = loginfo.AgentId;
            serviceRequest.ItemTypeId = InvoiceItemTypeId.TransferBooking;
            serviceRequest.DocName = "";
            serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
            serviceRequest.LastModifiedOn = DateTime.Now;
            serviceRequest.CreatedOn = DateTime.Now;

            serviceRequest.IsDomestic = true;
            serviceRequest.AgencyTypeId = Agencytype.Cash;
            serviceRequest.RequestSourceId = RequestSource.BookingAPI;
            serviceRequest.Save();
            // Setting booking details status 
            if (requestTypeId == 8)
            {
                booking.SetBookingStatus(BookingStatus.InProgress, loggedMemberId);
            }
            else
            {
                booking.SetBookingStatus(BookingStatus.AmendmentInProgress, loggedMemberId);
            }

            BookingHistory bh = new BookingHistory();
            bh.BookingId = bookingId;
            bh.Remarks = "Request sent to " + serviceRequest.RequestType.ToString() + " of the transfer booking.";

            if (requestTypeId == 8)
            {
                bh.EventCategory = EventCategory.TransferCancel;
            }
            else
            {
                bh.EventCategory = EventCategory.TransferAmendment;
            }
            bh.CreatedBy = (int)loginfo.UserID;
            bh.Save();

            try
            {
                //Sending email.
                Hashtable table = new Hashtable();
                table.Add("agentName", agency.Name);
                table.Add("ItemName", itineary.ItemName);
                table.Add("confirmationNo", itineary.ConfirmationNo);

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                UserMaster bookedBy = new UserMaster(itineary.CreatedBy);
                AgentMaster bookedAgency = new AgentMaster(itineary.AgentId);
                toArray.Add(bookedBy.Email);
                toArray.Add(bookedAgency.Email1);
                toArray.Add(custEmail);
                string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["TRANSFER_CANCEL_MAIL"]).Split(';');
                foreach (string cnMail in cancelMails)
                {
                    toArray.Add(cnMail);
                }
                toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);

                string message = ConfigurationManager.AppSettings["TRANSFER_CANCEL_REQUEST"];
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Transfer)Request for cancellation. Confirmation No:(" + itineary.ConfirmationNo + ")", message, table);
            }
            catch { }
            return true;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelBook, Severity.High, 1, "Exception in the transfers Change Request. Message:" + ex.Message, "");
            return false;
        }

    }

    #region private methods
    /// <summary>
    /// Serializes the given object.
    /// </summary>
    /// <typeparam name="T">The type of the object to be serialized.</typeparam>
    /// <param name="obj">The object to be serialized.</param>
    /// <returns>String representation of the serialized object.</returns>
    protected string Serialize<T>(T obj)
    {
        XmlSerializer xs = null;
        StringWriter sw = null;
        try
        {
            xs = new XmlSerializer(typeof(T));
            sw = new StringWriter();
            xs.Serialize(sw, obj);
            sw.Flush();
            return sw.ToString();
        }
        catch { }
        finally
        {
            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }
        }
        return "Error in Serialisation";
    }

    //checking credential validate or not
    private static UserMaster WSValidate(AuthenticationData credential)
    {
        UserMaster member = new UserMaster();
        Transfers bApi = new Transfers();
        if (credential.SiteName != null && credential.SiteName != "")
        {
            try
            {
                member = bApi.GetMemberBySiteName(credential.SiteName);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.High, 0, "B2C-Transfers-Credential Failed " + credential.SiteName + " Site is not configure for whitelabel " + ex.Message, "");
                return null;
            }
        }
        return member;
    }

    private UserMaster GetMemberBySiteName(string siteName)
    {
        UserPreference pref = new UserPreference();
        int memberId = 0;
        try
        {
            memberId = pref.GetMemberIdBySiteName(siteName);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Login, Severity.High, 0, "B2C-Transfers-GetMemberIdBySiteName Failed: " + ex.Message, "");
        }
        if (memberId > 0)
        {
            return new UserMaster(memberId);
        }
        else
        {
            return null;
        }
    }
    private bool WSBookInput(WSTransferBookRequest request)
    {
        try
        {
            if (request.TransferItinerary.ItemCode == null || request.TransferItinerary.ItemCode == "")
            {
                statusDescription = "ItemCode cannot be blank or null";
                statusCode = "35";
                return false;
            }
            if (request.TransferItinerary.ItemName == null || request.TransferItinerary.ItemName == "")
            {
                statusDescription = "ItemName cannot be blank or null";
                statusCode = "36";
                return false;
            }
            if (request.TransferItinerary.PassengerInfo == null || request.TransferItinerary.PassengerInfo == "")
            {
                statusDescription = "Pax Details Canot be blank or null";
                statusCode = "48";
                return false;
            }
        }
        catch (Exception Ex)
        {
            Audit.Add(EventType.TransferBooking, Severity.Low, 1, Ex.Message.ToString() + "|" + Ex.StackTrace.ToString(), "");
        }
        return true;
    }
    /// <summary>
    /// Method to send error mail
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="member"></param>
    private void SendErrorMail(string subject, UserMaster member, Exception ex, string message)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        Hashtable hostPort = con.GetHostPort();
        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
        string fromEmailId = hostPort["fromEmail"].ToString();
        if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
        {
            try
            {
                string mailMessage = string.Empty;
                mailMessage = "Member Name: " + member.FirstName + "" + member.LastName;                 //member.FullName;
                mailMessage += "\n AgencyId: " + member.AgentId.ToString();
                mailMessage += message;
                mailMessage += "\n\n\n\nException: " + ex.Message;
                mailMessage += "\n\nStack trace: " + ex.StackTrace;
                Email.Send(fromEmailId, errorNotificationMailingId, "B2C-Transfers-Error in Transfers Booking API " + subject, mailMessage);
            }
            catch (Exception excep)
            {
                Audit.Add(EventType.Email, Severity.High, (int)member.ID, "B2C-Transfers-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
            }
        }
    }
    #endregion
}

