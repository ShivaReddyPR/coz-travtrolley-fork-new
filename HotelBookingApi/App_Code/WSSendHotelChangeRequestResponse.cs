/// <summary>
/// Summary description for WSSendHotelChangeResponse
/// </summary>
public class WSSendHotelChangeRequestResponse
{
    private int changeRequestId;
    public int ChangeRequestId
    {
        get
        {
            return changeRequestId;
        }
        set
        {
            changeRequestId = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    public WSSendHotelChangeRequestResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
