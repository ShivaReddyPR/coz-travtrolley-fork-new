using System;
//using Technology.Indiatimes;
using CT.BookingEngine.WhiteLabel;

/// <summary>
/// Summary description for WSUpdatePendingHotelBookingRequest
/// </summary>
public class WSUpdatePendingHotelBookingRequest
{
    # region Private Fields
    int hotelQueueId;
    int bookingId;
    string paymentId;
    string orderId;
    WLHotelBookingStatus bookingStatus;
    string remarks;
    PaymentStatus paymentStatus;
    int lastModifiedBy;
    # endregion

    #region public Properties
    public int HotelQueueId
    {
        get
        {
            return hotelQueueId;
        }
        set
        {
            hotelQueueId = value;
        }
    }

    public int BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }

    public string PaymentId
    {
        get
        {
            return paymentId;
        }
        set
        {
            paymentId = value;
        }
    }

    public string OrderId
    {
        get
        {
            return orderId;
        }
        set
        {
            orderId = value;
        }
    }

    public WLHotelBookingStatus BookingStatus
    {
        get
        {
            return bookingStatus;
        }
        set
        {
            bookingStatus = value;
        }
    }

    public string Remarks
    {
        get
        {
            return remarks;
        }
        set
        {
            remarks = value;
        }
    }

    public PaymentStatus PaymentStatus
    {
        get
        {
            return paymentStatus;
        }
        set
        {
            paymentStatus = value;
        }
    }

    public int LastModifiedBy
    {
        get
        {
            return lastModifiedBy;
        }
        set
        {
            lastModifiedBy = value;
        }
    }

    #endregion

    public WSUpdatePendingHotelBookingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    # region Methods
    public WSUpdatePendingHotelBookingResponse Update(int memberId)
    {
        CT.BookingEngine.WhiteLabel.WLHotelPendingQueue ithPendingQueue = new CT.BookingEngine.WhiteLabel.WLHotelPendingQueue();
        ithPendingQueue.BookingId = bookingId;
        ithPendingQueue.PaymentId = paymentId;
        ithPendingQueue.OrderId = orderId;
        ithPendingQueue.PaymentStatus = (PaymentStatus)Enum.Parse(typeof(PaymentStatus), Convert.ToString(paymentStatus));
        ithPendingQueue.BookingStatus = bookingStatus;
        ithPendingQueue.Remarks = remarks;
        ithPendingQueue.LastModifiedBy = memberId;
        ithPendingQueue.HotelQueueId = hotelQueueId;
        ithPendingQueue.CheckInDate = DateTime.Now;
        ithPendingQueue.CheckOutDate = DateTime.Now;
        int referenceId = ithPendingQueue.Save();

        WSUpdatePendingHotelBookingResponse saveResponse = new WSUpdatePendingHotelBookingResponse();
        saveResponse.Status = new WSStatus();
        saveResponse.Status.Category = "UPHD";
        if (referenceId > 0)
        {
            saveResponse.Status.Description = "Details Updated";
            saveResponse.Status.StatusCode = "1";
            saveResponse.HotelQueueId = referenceId.ToString();
        }
        else
        {
            saveResponse.Status.Description = "Detail Update Failed";
            saveResponse.Status.StatusCode = "2";
        }
        return saveResponse;
    }
    # endregion
}
