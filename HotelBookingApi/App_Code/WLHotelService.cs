using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
// For Visa Change REquest
// ************
using Visa;
using CT.BookingEngine;
using System.Configuration;
// ************
/// <summary>
/// Summary description for WLHotelService
/// </summary>
[WebService(Namespace = "http://CT/HotelBookingApi",
            Description="Api for Hotel WhiteLabel Settings",
            Name="WLHotelService")]


[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WLHotelService : System.Web.Services.WebService
{
    public AuthenticationData Credential;

    public WLHotelService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    //Commented by brahmam
    //[WebMethod]
    //[SoapHeader("Credential")]
    //public List<WLAdvertisement> GetAdsOfModule(string adModule)
    //{
    //    UserPreference preference = new UserPreference();
    //    string siteName = Credential.SiteName;
    //    int memberId = 0;
    //    try
    //    {
    //        memberId = preference.GetMemberIdBySiteNameForHotel(siteName);
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.Normal, 0, "GetAdsOfModule Failed: " + ex.Message, "");
    //    }
    //    UserMaster member = new UserMaster(memberId);
    //    //Commented by brahmam
    //    //AgentMaster agency = AgentMaster.GetAgencyByMemberId(memberId);
    //    List<WLAdvertisement> WLadList = new List<WLAdvertisement>();
    //    List<Advertisement> adList = new List<Advertisement>();
    //    Advertisement ad = new Advertisement();
    //    AdModule adMod;
    //    try
    //    {
    //        adMod = (AdModule)Enum.Parse(typeof(AdModule), adModule);
    //    }
    //    catch (ArgumentException)
    //    {
    //        return WLadList;
    //    }
    //    adList = ad.GetAdsOfModule((int)agency.ID, adMod);
    //    foreach (Advertisement advert in adList)
    //    {
    //        WLAdvertisement adv = new WLAdvertisement();
    //        adv = WLAdvertisement.ReadAdvertisement(advert, member.LoginName);
    //        WLadList.Add(adv);
    //    }
    //    return WLadList;
    //}

    //[WebMethod]
    //[SoapHeader("Credential")]
    //public WLAdvertisement Load(int advertisementId)
    //{
    //    UserPreference preference = new UserPreference();
    //    string siteName = Credential.SiteName;
    //    int memberId = preference.GetMemberIdBySiteNameForHotel(siteName);
    //    Member member = new Member(memberId);
    //    Advertisement ad = new Advertisement();
    //    ad.Load(advertisementId);
    //    WLAdvertisement WLad = new WLAdvertisement();
    //    WLad = WLAdvertisement.ReadAdvertisement(ad, member.LoginName);
    //    return WLad;
    //}

    //[WebMethod]
    //[SoapHeader("Credential")]
    //public List<WSFaqs> GetFAQs()
    //{
    //    UserPreference preference = new UserPreference();
    //    string siteName = Credential.SiteName;
    //    int memberId = preference.GetMemberIdBySiteNameForHotel(siteName);
    //    Agency agency = Agency.GetAgencyByMemberId(memberId);
    //    List<WhiteLabelFaqs> faqList = new List<WhiteLabelFaqs>();
    //    faqList = WhiteLabelFaqs.LoadRecentFAQs(agency.AgencyId, ItemType.Hotel);
    //    List<WSFaqs> wsFaqList = new List<WSFaqs>();
    //    foreach (WhiteLabelFaqs faq in faqList)
    //    {
    //        WSFaqs wsFaq = new WSFaqs();
    //        wsFaq = WSFaqs.ReadFaqs(faq);
    //        wsFaqList.Add(wsFaq);
    //    }
    //    return wsFaqList;
    //}

    //[WebMethod]
    //[SoapHeader("Credential")]
    //public List<WSFaqs> GetAllFAQs()
    //{
    //    UserPreference preference = new UserPreference();
    //    string siteName = Credential.SiteName;
    //    int memberId = 0;
    //    try
    //    {
    //        memberId = preference.GetMemberIdBySiteNameForHotel(siteName);
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.AddAuditBookingAPI(EventType.Login, Severity.Normal, 0, "GetAllFAQs Failed: " + ex.Message, "");
    //    }
    //    Agency agency = Agency.GetAgencyByMemberId(memberId);
    //    List<WhiteLabelFaqs> faqList = new List<WhiteLabelFaqs>();
    //    faqList = WhiteLabelFaqs.LoadAllFAQs(agency.AgencyId, ItemType.Hotel);
    //    List<WSFaqs> wsFaqList = new List<WSFaqs>();
    //    foreach (WhiteLabelFaqs faq in faqList)
    //    {
    //        WSFaqs wsFaq = new WSFaqs();
    //        wsFaq = WSFaqs.ReadFaqs(faq);
    //        wsFaqList.Add(wsFaq);
    //    }
    //    return wsFaqList;
    //}

    [WebMethod]
    [SoapHeader("Credential")]
    public WSUserPreference GetPreferences()
    {
        WSUserPreference userPref = new WSUserPreference();
        UserPreference pref = new UserPreference();
        int memberId;
        try
        {
            memberId = pref.GetMemberIdBySiteName(Credential.SiteName);
        }
        catch (Exception ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.High, 0, "Site Name is not valid: " + ex.Message, "");
            throw new ArgumentException("Site Name is not valid");
        }
        try
        {

            LoginInfo AgentLoginInfo = UserMaster.GetB2CUser(memberId);
            userPref.Load(memberId);
            userPref.MemberId = Convert.ToInt32(AgentLoginInfo.UserID);
            userPref.AgentCurrency = AgentLoginInfo.Currency;
            userPref.AgentDecimalPoint = AgentLoginInfo.DecimalValue;
            userPref.AgentEmail = AgentLoginInfo.AgentEmail;
            userPref.AgentID = AgentLoginInfo.AgentId;
            AgentMaster agent = new AgentMaster(AgentLoginInfo.AgentId);
            userPref.AgentAddress = agent.Address;
            userPref.AgentPhoneNo = agent.Phone1;
            userPref.LocationCountryCode = AgentLoginInfo.LocationCountryCode;
        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Site Name is not valid: " + ex.Message, "");
            throw new ArgumentException("Site Name is not valid");
        }
        //Commented by brahmam 13.07.2015
        //UserMaster member = new UserMaster(memberId);
        //userPref.Load(member.LoginName);
        //userPref.Load(memberId);
        return userPref;
    }

    //Using Activity and fixedDeparture

    [WebMethod]
    [SoapHeader("Credential")]
    public WSUserPreference GetPreferenceByItemType(ItemType item)
    {
        WSUserPreference userPref = new WSUserPreference();
        UserPreference pref = new UserPreference();
        int memberId;
        try
        {
            memberId = pref.GetMemberIdBySiteName(Credential.SiteName);
        }
        catch (Exception ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.High, 0, "Site Name is not valid("+Credential.SiteName+"): " + ex.ToString(), "");
            throw new ArgumentException("Site Name is not valid");
        }
        try
        {

            LoginInfo AgentLoginInfo = UserMaster.GetB2CUser(memberId);
            userPref.Load(memberId,item);
            userPref.AgentCurrency = AgentLoginInfo.Currency;
            userPref.AgentDecimalPoint = AgentLoginInfo.DecimalValue;
            userPref.AgentEmail = AgentLoginInfo.AgentEmail;
            userPref.AgentID = AgentLoginInfo.AgentId;
            AgentMaster agent = new AgentMaster(AgentLoginInfo.AgentId);
            userPref.AgentAddress = agent.Address;
            userPref.AgentPhoneNo = agent.Phone1;
            userPref.MemberId =Convert.ToInt32(AgentLoginInfo.UserID);
            userPref.LocationId = Convert.ToInt32(AgentLoginInfo.LocationID);
            userPref.LocationCountryCode = AgentLoginInfo.LocationCountryCode;

        }
        catch (Exception ex)
        {
            Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Site Name is not valid("+Credential.SiteName+"): " + ex.ToString(), "");
            throw new ArgumentException("Site Name is not valid");
        }
        return userPref;
    }

    public WSUserPreference GetPreferences(int memberId)    //GetPreferences(string loginName) 
    {
        WSUserPreference userPref = new WSUserPreference();
        UserPreference pref = new UserPreference();
        userPref.Load(memberId);//userPref.Load(loginName);
        return userPref;
    }
    // ************
    [WebMethod]
    [SoapHeader("Credential")]
    public void SendVisaChangeRequest(int visaId, string remarks)
    {

        int memberId = 0;
        try
        {
            UserPreference pref = new UserPreference();

            try
            {
                memberId = pref.GetMemberIdBySiteName(Credential.SiteName);
            }
            catch { }
            LoginInfo AgentLoginInfo = UserMaster.GetB2CUser(memberId);

            VisaApplication app = new VisaApplication(visaId);
            VisaPassenger pax = app.PassengerList[0];
            ServiceRequest serviceRequest = new ServiceRequest();
            int requestTypeId = 3;
            serviceRequest.BookingId = 0; // sai // 
            serviceRequest.ReferenceId = Convert.ToInt32(visaId);
            serviceRequest.ProductType = ProductType.Visa;
            serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
            serviceRequest.Data = remarks;
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
            serviceRequest.CreatedBy = Convert.ToInt32(memberId);
            serviceRequest.AgencyId = AgentLoginInfo.AgentId;
            serviceRequest.PaxName = pax.Firstname + " " + pax.LastName;
            serviceRequest.Pnr = "";
            serviceRequest.StartDate = DateTime.Now;
            serviceRequest.SupplierName = "Visa";
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
            serviceRequest.ReferenceNumber = "";
            serviceRequest.AgencyId = AgentLoginInfo.AgentId;
            serviceRequest.ItemTypeId = InvoiceItemTypeId.VisaBooking;
            serviceRequest.DocName = "";
            serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
            serviceRequest.LastModifiedOn = DateTime.Now;
            serviceRequest.CreatedOn = DateTime.Now;
            //Added by shiva
            serviceRequest.AgencyTypeId = Agencytype.Cash;
            serviceRequest.RequestSourceId = RequestSource.BookingAPI;

            serviceRequest.Save();

            app.Status = VisaStatus.RequestChange;
            app.UpdateVisaStatus();

            try
            {
                List<string> adminTo = new List<string>();
                adminTo.Add(ConfigurationManager.AppSettings["TECH_MAIL"]);
                Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], adminTo, "Visa Change Request", "<p>Dear Team,<p> <p>Customer requested for change in VisaId:  " + app.VisaId + ".</p><p>Regards,<br/>Cozmo Travel B2C.", new Hashtable());
            }
            catch { }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, memberId, "(B2C Visa)Failed to add VisaChangeRequest. Reason : " + ex.ToString(), HttpContext.Current.Request["REMOTE_ADDR"]);
            throw ex;
        }
    }
}

