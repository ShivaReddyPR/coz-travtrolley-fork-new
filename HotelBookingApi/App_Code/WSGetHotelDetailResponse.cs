/// <summary>
/// Summary description for WSGetHotelDetailResponse
/// </summary>
public class WSGetHotelDetailResponse
{
    private WSHotelDetail hotelDetail;
    public WSHotelDetail HotelDetail
    {
        get
        {
            return hotelDetail;
        }
        set
        {
            hotelDetail = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }


    public WSGetHotelDetailResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
