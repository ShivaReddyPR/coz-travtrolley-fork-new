﻿//using System.Linq;
//using System.Xml.Linq;

/// <summary>
/// Summary description for WSSightSeeingBookingResponse
/// </summary>
public class WSSightSeeingBookingResponse
{
    private string bookingId;
    public string BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }

    private int sightSeeingId;
    public int SightSeeingId
    {
        get
        {
            return sightSeeingId;
        }
        set
        {
            sightSeeingId = value;
        }
    }

    private string confirmationNo;
    public string ConfirmationNo
    {
        get
        {
            return confirmationNo;
        }
        set
        {
            confirmationNo = value;
        }
    }

    private string referenceNo;
    public string ReferenceNo
    {
        get
        {
            return referenceNo;
        }
        set
        {
            referenceNo = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }
    public WSSightSeeingBookingResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
