using System;
//using Technology.Indiatimes;
//using Technology.Configuration;
//using Technology.BookingEngine;
using CT.Configuration;
using CT.BookingEngine;
using CT.BookingEngine.WhiteLabel;

/// <summary>
/// Summary description for WSAddPendingHotelBookingRequest
/// </summary>
public class WSAddPendingHotelBookingRequest
{
    #region private Fields
    string sessionId;
    int hotelIndex;
    int bookingId;
    int hotelQueueId;
    WLHotelRating starRating;
    WSPaymentGatewaySource paySource;
    string paymentId;
    string orderId;
    decimal paymentAmount;
    int agencyId;
    string passengerInfo;
    string cityRef;
    string hotelName;
    string hotelCode;
    string address1;
    string address2;
    DateTime checkInDate;
    DateTime checkOutDate;
    int numberOfRooms;
    string roomName;
    WLHotelBookingStatus bookingStatus;
    WLHotelBookingSource source;
    bool isDomestic;
    string cityCode;
    PaymentStatus paymentStatus;
    string remarks;
    string ipAddress;
    string email;
    string phone;
    int adultCount;
    int childCount;
    string country; 
    DateTime createdOn;
    int createdBy;
    DateTime lastModifiedOn;
    int lastModifiedBy;
    #endregion

    #region public Properties
    public string SessionId
    {
        get
        {
            return sessionId;
        }
        set
        {
            sessionId = value;
        }
    }

    public int HotelIndex
    {
        get
        {
            return hotelIndex;
        }
        set
        {
            hotelIndex = value;
        }
    }

    public int BookingId
    {
        get
        {
            return bookingId;
        }
        set
        {
            bookingId = value;
        }
    }

    public int HotelQueueId
    {
        get
        {
            return hotelQueueId;
        }
        set
        {
            hotelQueueId = value;
        }
    }

    public WLHotelRating StarRating
    {
        get
        {
            return starRating;
        }
        set
        {
            starRating = value;
        }
    }

    public WSPaymentGatewaySource PaySource
    {
        get
        {
            return paySource;
        }
        set
        {
            paySource = value;
        }
    }

    public string PaymentId
    {
        get
        {
            return paymentId;
        }
        set
        {
            paymentId = value;
        }
    }

    public string OrderId
    {
        get
        {
            return orderId;
        }
        set
        {
            orderId = value;
        }
    }

    public decimal PaymentAmount
    {
        get
        {
            return paymentAmount;
        }
        set
        {
            paymentAmount = value;
        }
    }

    public int AgencyId
    {
        get
        {
            return agencyId;
        }
        set
        {
            agencyId = value;
        }
    }

    public string PassengerInfo
    {
        get
        {
            return passengerInfo;
        }
        set
        {
            passengerInfo = value;
        }
    }

    public string CityRef
    {
        get
        {
            return cityRef;
        }
        set
        {
            cityRef = value;
        }
    }

    public string HotelName
    {
        get
        {
            return hotelName;
        }
        set
        {
            hotelName = value;
        }
    }

    public string HotelCode
    {
        get
        {
            return hotelCode;
        }
        set
        {
            hotelCode = value;
        }
    }

    public string Address1
    {
        get
        {
            return address1;
        }
        set
        {
            address1 = value;
        }
    }

    public string Address2
    {
        get
        {
            return address2;
        }
        set
        {
            address2 = value;
        }
    }

    public DateTime CheckInDate
    {
        get
        {
            return checkInDate;
        }
        set
        {
            checkInDate = value;
        }
    }

    public DateTime CheckOutDate
    {
        get
        {
            return checkOutDate;
        }
        set
        {
            checkOutDate = value;
        }
    }

    public int NumberOfRooms
    {
        get
        {
            return numberOfRooms;
        }
        set
        {
            numberOfRooms = value;
        }
    }

    public string RoomName
    {
        get
        {
            return roomName;
        }
        set
        {
            roomName = value;
        }
    }

    public WLHotelBookingStatus BookingStatus
    {
        get
        {
            return bookingStatus;
        }
        set
        {
            bookingStatus = value;
        }
    }

    public WLHotelBookingSource Source
    {
        get
        {
            return source;
        }
        set
        {
            source = value;
        }
    }

    public bool IsDomestic
    {
        get
        {
            return isDomestic;
        }
        set
        {
            isDomestic = value;
        }
    }

    public string CityCode
    {
        get
        {
            return cityCode;
        }
        set
        {
            cityCode = value;
        }
    }

    public PaymentStatus PaymentStatus
    {
        get
        {
            return paymentStatus;
        }
        set
        {
            paymentStatus = value;
        }
    }

    public string Remarks
    {
        get
        {
            return remarks;
        }
        set
        {
            remarks = value;
        }
    }

    public string IPAddress
    {
        get
        {
            return ipAddress;
        }
        set
        {
            ipAddress = value;
        }
    }

    public string Email
    {
        get
        {
            return email;
        }
        set
        {
            email = value;
        }
    }

    public string Phone
    {
        get
        {
            return phone;
        }
        set
        {
            phone = value;
        }
    }

    public int AdultCount
    {
        get
        {
            return adultCount;
        }
        set
        {
            adultCount = value;
        }
    }

    public int ChildCount
    {
        get
        {
            return childCount;
        }
        set
        {
            childCount = value;
        }
    }

    public string Country
    {
        get
        {
            return country;
        }
        set
        {
            country = value;
        }
    }

    public DateTime CreatedOn
    {
        get
        {
            return createdOn;
        }
        set
        {
            createdOn = value;
        }
    }

    public int CreatedBy
    {
        set
        {
            createdBy = value;
        }
        get
        {
            return createdBy;
        }
    }

    public DateTime LastModifiedOn
    {
        set
        {
            lastModifiedOn = value;
        }
        get
        {
            return lastModifiedOn;
        }
    }

    public int LastModifiedBy
    {
        set
        {
            lastModifiedBy = value;
        }
        get
        {
            return lastModifiedBy;
        }
    }
    #endregion

    public WSAddPendingHotelBookingRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    # region Methods
    public WSAddPendingHotelBookingResponse Add(int memberId, int agencyId, HotelBookingSource source)
    {
        if (agencyId <= 0)
        {
            ConfigurationSystem config = new ConfigurationSystem();
            agencyId = Convert.ToInt32(config.GetSelfAgencyId()["selfAgencyId"]);
        }

        WLHotelPendingQueue ithPendingQueue = new WLHotelPendingQueue();
        ithPendingQueue.AgencyId = agencyId;
        ithPendingQueue.Address1 = address1;
        ithPendingQueue.Address2 = address2;
        ithPendingQueue.AdultCount = adultCount;
        ithPendingQueue.ChildCount = childCount;
        ithPendingQueue.Country = country;
        ithPendingQueue.BookingId = bookingId;
        ithPendingQueue.BookingStatus = bookingStatus;
        ithPendingQueue.CheckInDate = checkInDate;
        ithPendingQueue.CheckOutDate = checkOutDate;
        ithPendingQueue.CityCode = cityCode;
        ithPendingQueue.CityRef = cityRef;
        ithPendingQueue.CreatedBy = memberId;
        ithPendingQueue.Email = email;
        ithPendingQueue.HotelCode = hotelCode;
        ithPendingQueue.HotelName = hotelName;
        ithPendingQueue.HotelQueueId = 0;
        ithPendingQueue.IPAddress = ipAddress;
        ithPendingQueue.IsDomestic = isDomestic;
        ithPendingQueue.NumberOfRooms = numberOfRooms;
        ithPendingQueue.OrderId = orderId;
        ithPendingQueue.PassengerInfo = passengerInfo;
        ithPendingQueue.PaymentAmount = paymentAmount;
        ithPendingQueue.PaymentId = paymentId;
        ithPendingQueue.PaymentStatus = (PaymentStatus)Enum.Parse(typeof(PaymentStatus), Convert.ToString(paymentStatus));
        ithPendingQueue.PaySource = (CT.BookingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.BookingEngine.PaymentGatewaySource), Convert.ToString(paySource));
        ithPendingQueue.Phone = phone;
        ithPendingQueue.Remarks = remarks;
        ithPendingQueue.RoomName = roomName;
        ithPendingQueue.Source = (WLHotelBookingSource)Enum.Parse(typeof(WLHotelBookingSource), source.ToString()); ;
        ithPendingQueue.StarRating = starRating;
        int referenceId = ithPendingQueue.Save();

        WSAddPendingHotelBookingResponse saveResponse = new WSAddPendingHotelBookingResponse();
        saveResponse.Status = new WSStatus();
        saveResponse.Status.Category = "SPHD";
        if (referenceId > 0)
        {
            saveResponse.Status.Description = "Details Saved";
            saveResponse.Status.StatusCode = "1";
            saveResponse.HotelQueueId = referenceId.ToString();
        }
        else
        {
            saveResponse.Status.Description = "Details Saved Failed";
            saveResponse.Status.StatusCode = "2";
        }
        return saveResponse;
    }
    # endregion
}
