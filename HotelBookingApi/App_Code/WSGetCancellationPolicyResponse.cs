/// <summary>
/// Summary description for WSGetCancellationPolicyResponse
/// </summary>
public class WSGetCancellationPolicyResponse
{
    private string[] cancellationPolicy;
    public string[] CancellationPolicy
    {
        get
        {
            return cancellationPolicy;
        }
        set
        {
            cancellationPolicy = value;
        }
    }

    private string[] hotelNorms;
    public string[] HotelNorms
    {
        get
        {
            return hotelNorms;
        }
        set
        {
            hotelNorms = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }
    private bool priceStatus;
    public bool PriceStatus
    {
        get
        {
            return priceStatus;
        }
        set
        {
            priceStatus = value;
        }
    }
    //only LOH Purpose  added by brahmam
    private bool postalCode;
    public bool PostalCode
    {
        get { return postalCode; }
        set { postalCode = value; }
    }

 
    public WSGetCancellationPolicyResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
