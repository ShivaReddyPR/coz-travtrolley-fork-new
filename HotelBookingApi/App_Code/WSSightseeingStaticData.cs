﻿using System;
//using System.Linq;
//using System.Xml.Linq;
using CT.BookingEngine;
using System.Collections.Generic;
/// <summary>
/// Summary description for WSSightseeingStaticData
/// </summary>
[Serializable]
public class WSSightseeingStaticData
{
    private string itemCode;
    private string cityCode;
    private string description;
    private string tourInfo;
    private string includes;
    private string departurePoint;
    private string flashLink;
    private string notes;
    private List<WSTourOperations> tourOperation;
    private SightseeingBookingSource source;
    private DateTime timeStamp;
    private string itemName;
    private string imageInfo;
    private string closedDates;
    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
    public string ItemName
    {
        get { return itemName; }
        set { itemName = value; }
    }
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }
    public string Description
    {
        get { return description; }
        set { description = value; }
    }
    public SightseeingBookingSource Source
    {
        get { return source; }
        set { source = value; }
    }
    public string ImageInfo
    {
        get { return imageInfo; }
        set { imageInfo = value; }
    }
    public string TourInfo
    {
        get { return tourInfo; }
        set { tourInfo = value; }
    }
    public string Includes
    {
        get { return includes; }
        set { includes = value; }
    }
    public string DeparturePoint
    {
        get { return departurePoint; }
        set { departurePoint = value; }
    }
    public string FlashLink
    {
        get { return flashLink; }
        set { flashLink = value; }
    }
    public string Notes
    {
        get { return notes; }
        set { notes = value; }
    }
    public DateTime TimeStamp
    {
        get { return timeStamp; }
        set { timeStamp = value; }
    }
    public List<WSTourOperations> TourOperationInfo
    {
        get { return tourOperation; }
        set { tourOperation = value; }
    }
    public string ClosedDates
    {
        get { return closedDates; }
        set { closedDates = value; }
    }
    public WSSightseeingStaticData()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public struct WSTourOperations
{
    public string ItemCode;
    public string CityCode;
    public string Language;
    public string Commentary;
    public DateTime FromDate;
    public DateTime ToDate;
    public string Frequency;
    public string DepTime;
    public string DepCode;
    public string DepName;
    public string FromTime;
    public string ToTime;
    public string Interval;
    public string Telephone;
    public string Address;
}
