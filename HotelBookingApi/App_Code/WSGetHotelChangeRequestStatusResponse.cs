/// <summary>
/// Summary description for WSGetHotelChangeRequestStatusResponse
/// </summary>
public class WSGetHotelChangeRequestStatusResponse
{
    private int changeRequestId;
    public int ChangeRequestId
    {
        get
        {
            return changeRequestId;
        }
        set
        {
            changeRequestId = value;
        }
    }

    private decimal refundedAmount;
    public decimal RefundedAmount
    {
        get
        {
            return refundedAmount;
        }
        set
        {
            refundedAmount = value;
        }
    }

    private decimal cancellationCharge;
    public decimal CancellationCharge
    {
        get
        {
            return cancellationCharge;
        }
        set
        {
            cancellationCharge = value;
        }
    }

    private WSChangeRequestStatus changeRequestStatus;
    public WSChangeRequestStatus ChangeRequestStatus
    {
        get
        {
            return changeRequestStatus;
        }
        set
        {
            changeRequestStatus = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }  

    public WSGetHotelChangeRequestStatusResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public enum WSChangeRequestStatus
{
    Pending = 1,
    InProgress = 2,
    Processed = 3,
    Rejected = 4
}