﻿//using System.Linq;
//using System.Xml.Linq;
using CT.BookingEngine;
using System.Collections.Generic;
/// <summary>
/// Summary description for WSGetSightSeeingDetailResponse
/// </summary>
public class WSGetSightSeeingDetailResponse
{
    private WSSightseeingStaticData sightSeeingDetail;
    public WSSightseeingStaticData SightSeeingDetail
    {
        get
        {
            return sightSeeingDetail;
        }
        set
        {
            sightSeeingDetail = value;
        }
    }

    private WSStatus status;
    public WSStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }
    public WSGetSightSeeingDetailResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static WSSightseeingStaticData FillSightSeeingDetails(SightseeingStaticData staticData)
    {
        WSSightseeingStaticData sightSeeingDetail = new WSSightseeingStaticData();

        sightSeeingDetail.CityCode = staticData.CityCode;
        sightSeeingDetail.ClosedDates = staticData.ClosedDates;
        sightSeeingDetail.DeparturePoint = staticData.DeparturePoint;
        sightSeeingDetail.Description = staticData.Description;
        sightSeeingDetail.FlashLink = staticData.FlashLink;
        sightSeeingDetail.ImageInfo = staticData.ImageInfo;
        sightSeeingDetail.Includes = staticData.Includes;
        sightSeeingDetail.ItemCode = staticData.ItemCode;
        sightSeeingDetail.ItemName = staticData.ItemName;
        sightSeeingDetail.Notes = staticData.Notes;
        sightSeeingDetail.Source = staticData.Source;
        sightSeeingDetail.TimeStamp = staticData.TimeStamp;
        sightSeeingDetail.TourInfo = staticData.TourInfo;
        sightSeeingDetail.TourOperationInfo=new List<WSTourOperations>();
        WSTourOperations operation=new WSTourOperations();

        for (int j = 0; j < staticData.TourOperationInfo.Count; j++)
        {
            operation.Address=staticData.TourOperationInfo[j].Address;
            operation.CityCode = staticData.TourOperationInfo[j].CityCode;
            operation.Commentary = staticData.TourOperationInfo[j].Commentary;
            operation.DepCode = staticData.TourOperationInfo[j].DepCode;
            operation.DepName = staticData.TourOperationInfo[j].DepName;
            operation.DepTime = staticData.TourOperationInfo[j].DepTime;
            operation.Frequency = staticData.TourOperationInfo[j].Frequency;
            operation.FromDate = staticData.TourOperationInfo[j].FromDate;
            operation.FromTime = staticData.TourOperationInfo[j].FromTime;
            operation.Interval = staticData.TourOperationInfo[j].Interval;
            operation.ItemCode = staticData.TourOperationInfo[j].ItemCode;
            operation.Language = staticData.TourOperationInfo[j].Language;
            operation.Telephone = staticData.TourOperationInfo[j].Telephone;
            operation.ToDate = staticData.TourOperationInfo[j].ToDate;
            operation.ToTime = staticData.TourOperationInfo[j].ToTime;
            sightSeeingDetail.TourOperationInfo.Add(operation);
        }
        return sightSeeingDetail;
    }
}


