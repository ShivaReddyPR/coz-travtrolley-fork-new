using System.Collections.Generic;

/// <summary>
/// Summary description for WSHotelSearchResponse
/// </summary>
public class WSHotelSearchResponse
{
    private WSStatus status;
    public WSStatus Status
    {
        get { return status; }
        set { status = value; }
    }

    private WSHotelResult[] result;
    public WSHotelResult[] Result
    {
        get { return result; }
        set { result = value; }
    }

    private string sessionId;
    public string SessionId
    {
        get { return sessionId; }
        set { sessionId = value; }
    }

    private bool isDomestic;
    public bool IsDomestic
    {
        get { return isDomestic; }
        set { isDomestic = value; }
    }

    private List<CityInfo> cityList;
    public List<CityInfo> CityList
    {
        get { return cityList; }
        set { cityList = value; }
    }   

    public WSHotelSearchResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public class CityInfo
{
    private string cityName;
    private string cityCode;
    private string countryName;
    private string countryCode;
    private string currencyCode;

    public string CityName
    {
        get { return cityName; }
        set { cityName = value; }
    }
    public string CityCode
    {
        get { return cityCode; }
        set { cityCode = value; }
    }
    public string CountryName
    {
        get { return countryName; }
        set { countryName = value; }
    }
    public string CountryCode
    {
        get { return countryCode; }
        set { countryCode = value; }
    }
    public string CurrencyCode
    {
        get { return currencyCode; }
        set { currencyCode = value; }
    }
}

