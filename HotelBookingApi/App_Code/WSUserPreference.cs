using System;
using System.Collections.Generic;
using CT.Core;

/// <summary>
/// Summary description for WSUserPreference
/// </summary>
public class WSUserPreference
{

    private string imagePath = CT.Configuration.ConfigurationSystem.WLAdConfig["hotelLogoPath"];

    private string siteName;
    public string SiteName
    {
        get { return siteName; }
        set { siteName = value; }
    }

    private string siteTitle;
    public string SiteTitle
    {
        get { return siteTitle; }
        set { siteTitle = value; }
    }
    private string tradingName;
    public string TradingName
    {
        get { return tradingName; }
        set { tradingName = value; }
    }

    private string tagLine;
    public string TagLine
    {
        get { return tagLine; }
        set { tagLine = value; }
    }

    private string logo;
    public string Logo
    {
        get { return logo; }
        set { logo = value; }
    }

    private bool showHeader;
    public bool ShowHeader
    {
        get { return showHeader; }
        set { showHeader = value; }
    }

    private string headerDetail;
    public string HeaderDetail
    {
        get { return headerDetail; }
        set { headerDetail = value; }
    }
    private string headerImage;
    public string HeaderImage
    {
        get { return headerImage; }
        set { headerImage = value; }
    }
    private string headerText;
    public string HeaderText
    {
        get { return headerText; }
        set { headerText = value; }
    }
    private bool showFooter;
    public bool ShowFooter
    {
        get { return showFooter; }
        set { showFooter = value; }
    }

    private bool showMenuBar;
    public bool ShowMenuBar
    {
        get { return showMenuBar; }
        set { showMenuBar = value; }
    }

    private bool showLoginPage;
    public bool ShowLoginPage
    {
        get { return showLoginPage; }
        set { showLoginPage = value; }
    }

    private decimal discount;
    public decimal Discount
    {
        get { return discount; }
        set { discount = value; }
    }
    private decimal creditCardCharge;
    public decimal CreditCardCharge
    {
        get { return creditCardCharge; }
        set { creditCardCharge = value; }
    }
    private decimal serviceCharge;
    public decimal ServiceCharge
    {
        get { return serviceCharge; }
        set { serviceCharge = value; }
    }

    private string emailId;
    public string EmailId
    {
        get { return emailId; }
        set { emailId = value; }
    }

    private string phoneNum;
    public string PhoneNum
    {
        get { return phoneNum; }
        set { phoneNum = value; }
    }

    private string faxNum;
    public string FaxNum
    {
        get { return faxNum; }
        set { faxNum = value; }
    }

    private string waitingLogo;
    public string WaitingLogo
    {
        get { return waitingLogo; }
        set { waitingLogo = value; }
    }

    private string waitingText;
    public string WaitingText
    {
        get { return waitingText; }
        set { waitingText = value; }
    }

    private string homePageUrl;
    public string HomePageUrl
    {
        get { return homePageUrl; }
        set { homePageUrl = value; }
    }

    private string leftTopPanelText;
    public string LeftTopPanelText
    {
        get { return leftTopPanelText; }
        set { leftTopPanelText = value; }
    }

    private bool hdfcPaymentGateway;
    public bool HdfcPaymentGateway
    {
        get { return hdfcPaymentGateway; }
        set { hdfcPaymentGateway = value; }
    }

    private bool oxiCashPaymentGateway;
    public bool OxiCashPaymentGateway
    {
        get { return oxiCashPaymentGateway; }
        set { oxiCashPaymentGateway = value; }
    }

    private bool amexPaymentGateway;
    public bool AmexPaymentGateway
    {
        get { return amexPaymentGateway; }
        set { amexPaymentGateway = value; }
    }

    private bool iciciPaymentGateway;
    public bool IciciPaymentGateway
    {
        get { return iciciPaymentGateway; }
        set { iciciPaymentGateway = value; }
    }

    private bool sbiPaymentGateway;
    public bool SbiPaymentGateway
    {
        get { return sbiPaymentGateway; }
        set { sbiPaymentGateway = value; }
    }

    private string aboutContent;
    public string AboutContent
    {
        get { return aboutContent; }
        set { aboutContent = value; }
    }

    private string contactContent;
    public string ContactContent
    {
        get { return contactContent; }
        set { contactContent = value; }
    }

    private string privacyContent;
    public string PrivacyContent
    {
        get { return privacyContent; }
        set { privacyContent = value; }
    }

    private string termsContent;
    public string TermsContent
    {
        get { return termsContent; }
        set { termsContent = value; }
    }

    private bool desiyaAllowed;
    public bool DesiyaAllowed
    {
        get { return desiyaAllowed; }
        set { desiyaAllowed = value; }
    }

    private bool ianDomesticAllowed;
    public bool IanDomesticAllowed
    {
        get { return ianDomesticAllowed; }
        set { ianDomesticAllowed = value; }
    }

    private bool touricoAllowed;
    public bool TouricoAllowed
    {
        get { return touricoAllowed; }
        set { touricoAllowed = value; }
    }

    private bool hotelBedsAllowed;
    public bool HotelBedsAllowed
    {
        get { return hotelBedsAllowed; }
        set { hotelBedsAllowed = value; }
    }

    private bool ianInternationAllowed;
    public bool IanInternationAllowed
    {
        get { return ianInternationAllowed; }
        set { ianInternationAllowed = value; }
    }

    private bool gtaAllowed;
    public bool GtaAllowed
    {
        get { return gtaAllowed; }
        set { gtaAllowed = value; }
    }
    //Added 07.08.2015
    private string agentCurrency;
    public string AgentCurrency
    {
        get { return agentCurrency; }
        set { agentCurrency = value; }
    }

    private int agentDecimalPoint;
    public int AgentDecimalPoint
    {
        get { return agentDecimalPoint; }
        set { agentDecimalPoint = value; }
    }

    private string agentEmail;
    public string AgentEmail
    {
        get { return agentEmail; }
        set { agentEmail = value; }
    }

    private int agentId;
    public int AgentID
    {
        get { return agentId; }
        set { agentId = value; }
    }
    //Creditcard charges
    private bool enbd_pg;
    public bool ENBD_PG
    {
        get { return enbd_pg; }
        set { enbd_pg = value; }
    }
    private bool ccavenue_pg;
    public bool CCAVENUE_PG
    {
        get { return ccavenue_pg; }
        set { ccavenue_pg = value; }
    }
    private decimal enbd_creditCardCharge;
    public decimal ENBD_CreditCardCharge
    {
        get { return enbd_creditCardCharge; }
        set { enbd_creditCardCharge = value; }
    }
    private decimal ccavenue_creditCardCharge;
    public decimal CCAVENUE_CreditCardCharge
    {
        get { return ccavenue_creditCardCharge; }
        set { ccavenue_creditCardCharge = value; }
    }
    private string agentAddress;
    public string AgentAddress
    {
        get { return agentAddress; }
        set { agentAddress = value; }
    }

    private string agentPhoneNo;
    public string AgentPhoneNo
    {
        get { return agentPhoneNo; }
        set { agentPhoneNo = value; }
    }
    //Activity Purpose INFORMED BY ZIYAD 10-OCT-2015    Added by brahmam
    private int memberId;
    public int MemberId
    {
        get { return memberId; }
        set { memberId = value; }
    }

    private int locationId;
    public int LocationId
    {
        get { return locationId; }
        set { locationId = value; }
    }
    string alternateEmail = string.Empty;
    public string AlternateEmail // for visa doc page..displaying contact address
    {
        get { return alternateEmail; }
        set { alternateEmail = value; }
    }
    //CCAvenue PaymentGetWay
    string ccAvenue_AccessCode;
    public string CCAvenue_AccessCode
    {
        get { return ccAvenue_AccessCode; }
        set { ccAvenue_AccessCode = value; }
    }
    string ccAvenue_EncryptKey;
    public string CCAvenue_EncryptKey
    {
        get { return ccAvenue_EncryptKey; }
        set { ccAvenue_EncryptKey = value; }
    }
    string ccAvenue_Url;
    public string CCAvenue_Url
    {
        get { return ccAvenue_Url; }
        set { ccAvenue_Url = value; }
    }
    string ccAvenue_MerchantId;
    public string CCAvenue_MerchantId
    {
        get { return ccAvenue_MerchantId; }
        set { ccAvenue_MerchantId = value; }
    }
    //ENBD PaymentGetWay
    string enbd_QueryString;
    public string ENBD_QueryString
    {
        get { return enbd_QueryString; }
        set { enbd_QueryString = value; }
    }
    string enbd_Command;
    public string ENBD_Command
    {
        get { return enbd_Command; }
        set { enbd_Command = value; }
    }
    string enbd_AccessCode;
    public string ENBD_AccessCode
    {
        get { return enbd_AccessCode; }
        set { enbd_AccessCode = value; }
    }
    string enbd_Merchant;
    public string ENBD_Merchant
    {
        get { return enbd_Merchant; }
        set { enbd_Merchant = value; }
    }
    string enbd_SecureSecret;
    public string ENBD_SecureSecret
    {
        get { return enbd_SecureSecret; }
        set { enbd_SecureSecret = value; }
    }
    string enbd_Locale;
    public string ENBD_Locale
    {
        get { return enbd_Locale; }
        set { enbd_Locale = value; }
    }

    private bool networkinternational_pg;
    public bool NetworkInternational_PG
    {
        get { return networkinternational_pg; }
        set { networkinternational_pg = value; }
    }
    private decimal networkinternational_creditCardCharge;
    public decimal NetworkInternational_CreditCardCharge
    {
        get { return networkinternational_creditCardCharge; }
        set { networkinternational_creditCardCharge = value; }
    }

    //Added by Lokesh On 14-DEC-2017
    //Regarding Input/Output VAT Purpose Calculation.
    private string locationCountryCode;
    public string LocationCountryCode
    {
        get { return locationCountryCode; }
        set { locationCountryCode = value; }
    }
    public WSUserPreference()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void Load(int memberId)    //insted of loginName i changed memberid
    {
        WSUserPreference wsPref = new WSUserPreference();
        Dictionary<string, string> prefList = UserPreference.GetPreferenceList(memberId, ItemType.Hotel);
        if (prefList.ContainsKey(WLPreferenceKeys.HotelSiteName))
        {
            siteName = prefList[WLPreferenceKeys.HotelSiteName];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelTradingName))
        {
            tradingName = prefList[WLPreferenceKeys.HotelTradingName];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelSiteTitle))
        {
            siteTitle = prefList[WLPreferenceKeys.HotelSiteTitle];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelTagLine))
        {
            tagLine = prefList[WLPreferenceKeys.HotelTagLine];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelEmailId))
        {
            emailId = prefList[WLPreferenceKeys.HotelEmailId];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelSiteLogo))
        {
            logo = imagePath + prefList[WLPreferenceKeys.HotelSiteLogo];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelPhoneNumber))
        {
            phoneNum = prefList[WLPreferenceKeys.HotelPhoneNumber];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelFaxNumber))
        {
            faxNum = prefList[WLPreferenceKeys.HotelFaxNumber];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelWaitingLogo))
        {
            waitingLogo = imagePath + prefList[WLPreferenceKeys.HotelWaitingLogo];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelWaitingText))
        {
            WaitingText = prefList[WLPreferenceKeys.HotelWaitingText];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelContactContect))
        {
            contactContent = prefList[WLPreferenceKeys.HotelContactContect];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelAboutContent))
        {
            aboutContent = prefList[WLPreferenceKeys.HotelAboutContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelTermsContent))
        {
            termsContent = prefList[WLPreferenceKeys.HotelTermsContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelPrivacyContent))
        {
            privacyContent = prefList[WLPreferenceKeys.HotelPrivacyContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelShowLoginPage))
        {
            showLoginPage = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelShowLoginPage]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelShowMenubar))
        {
            showMenuBar = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelShowMenubar]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelShowHeader))
        {
            showHeader = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelShowHeader]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelShowFooter))
        {
            showFooter = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelShowFooter]);
        } 
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHeaderDetails))
        {
            headerDetail = prefList[WLPreferenceKeys.HotelHeaderDetails];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHeaderImage))
        {
            headerImage = imagePath + prefList[WLPreferenceKeys.HotelHeaderImage];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHtmlContent))
        {
            headerText = prefList[WLPreferenceKeys.HotelHtmlContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelLeftTopPaneltext))
        {
            leftTopPanelText = prefList[WLPreferenceKeys.HotelLeftTopPaneltext];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHomePageUrl))
        {
            homePageUrl = prefList[WLPreferenceKeys.HotelHomePageUrl];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelDiscount))
        {
            if (prefList[WLPreferenceKeys.HotelDiscount] != null && prefList[WLPreferenceKeys.HotelDiscount] != "")
            {
                try
                {
                    discount = Convert.ToDecimal(prefList[WLPreferenceKeys.HotelDiscount]);
                }
                catch (Exception)
                {
                    discount = 0;
                }
            }
            else
            {
                discount = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelServiceCharge))
        {
            if (prefList[WLPreferenceKeys.HotelServiceCharge] != null && prefList[WLPreferenceKeys.HotelServiceCharge] != "")
            {
                try
                {
                    serviceCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.HotelServiceCharge]);
                }
                catch (Exception)
                {
                    serviceCharge = 0;
                }
            }
            else
            {
                serviceCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelCreditCardCharge))
        {
            if (prefList[WLPreferenceKeys.HotelCreditCardCharge] != null && prefList[WLPreferenceKeys.HotelCreditCardCharge] != "")
            {
                try
                {
                    creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.HotelCreditCardCharge]);
                }
                catch (Exception)
                {
                    creditCardCharge = 0;
                }
            }
            else
            {
                creditCardCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHDFCPaymentGateway))
        {
            hdfcPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelHDFCPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelOXICASHPaymentGateway))
        {
            oxiCashPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelOXICASHPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelSBIPaymentGateway))
        {
            sbiPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelSBIPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelICICIPaymentGateway))
        {
            iciciPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelICICIPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelAMEXPaymentGateway))
        {
            amexPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelAMEXPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelAMEXPaymentGateway))
        {
            amexPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelAMEXPaymentGateway]);
        }

        if (prefList.ContainsKey(WLPreferenceKeys.DesiyaAllowed))
        {
            desiyaAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.DesiyaAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.IanDomesticAllowed))
        {
            ianDomesticAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.IanDomesticAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.TouricoAllowed))
        {
            touricoAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.TouricoAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelBedsAllowed))
        {
            hotelBedsAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelBedsAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.IanInternationAllowed))
        {
            ianInternationAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.IanInternationAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.GtaAllowed))
        {
            gtaAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.GtaAllowed]);
        }

        if (prefList.ContainsKey(WLPreferenceKeys.enbdPG))
        {
            enbd_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.enbdPG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ccAvenuePG))
        {
            ccavenue_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.ccAvenuePG]);
        }

        if (prefList.ContainsKey(WLPreferenceKeys.networkInternationalPG))
        {
            networkinternational_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.networkInternationalPG]);
        }


        if (prefList.ContainsKey(WLPreferenceKeys.enbdCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.enbdCreditCardCharges] != null && prefList[WLPreferenceKeys.enbdCreditCardCharges] != "")
            {
                try
                {
                    enbd_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.enbdCreditCardCharges]);
                }
                catch (Exception)
                {
                    enbd_creditCardCharge = 0;
                }
            }
            else
            {
                enbd_creditCardCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ccAvenueCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.ccAvenueCreditCardCharges] != null && prefList[WLPreferenceKeys.ccAvenueCreditCardCharges] != "")
            {
                try
                {
                    ccavenue_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.ccAvenueCreditCardCharges]);
                }
                catch (Exception)
                {
                    ccavenue_creditCardCharge = 0;
                }
            }
            else
            {
                ccavenue_creditCardCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.networkInternationalCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.networkInternationalCreditCardCharges] != null && prefList[WLPreferenceKeys.networkInternationalCreditCardCharges] != "")
            {
                try
                {
                    networkinternational_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.networkInternationalCreditCardCharges]);
                }
                catch (Exception)
                {
                    networkinternational_creditCardCharge = 0;
                }
            }
            else
            {
                networkinternational_creditCardCharge = 0;
            }
        }



         //CCAvenue PaymentGetWay (loading CCAvenue keys)
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenueAccessCode))
        {
            ccAvenue_AccessCode = prefList[WLPreferenceKeys.CCAvenueAccessCode];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenueEncryptKey))
        {
            ccAvenue_EncryptKey = prefList[WLPreferenceKeys.CCAvenueEncryptKey];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenueMerchantId))
        {
            ccAvenue_MerchantId = prefList[WLPreferenceKeys.CCAvenueMerchantId];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenueUrl))
        {
            ccAvenue_Url = prefList[WLPreferenceKeys.CCAvenueUrl];
        }
        //ENBD PaymentGetWay (loading ENBD keys)
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDQueryString))
        {
            enbd_QueryString = prefList[WLPreferenceKeys.ENBDQueryString];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDCommand))
        {
            enbd_Command = prefList[WLPreferenceKeys.ENBDCommand];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDAccessCode))
        {
            enbd_AccessCode = prefList[WLPreferenceKeys.ENBDAccessCode];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDMerchant))
        {
            enbd_Merchant = prefList[WLPreferenceKeys.ENBDMerchant];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDSecureSecret))
        {
            enbd_SecureSecret = prefList[WLPreferenceKeys.ENBDSecureSecret];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDLocale))
        {
            enbd_Locale = prefList[WLPreferenceKeys.ENBDLocale];
        }
    }

    public void Load(int memberId,ItemType item) 
    {
        WSUserPreference wsPref = new WSUserPreference();
        Dictionary<string, string> prefList = UserPreference.GetPreferenceList(memberId,item);

        if (prefList.ContainsKey(WLPreferenceKeys.networkInternationalPG))
        {
            networkinternational_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.networkInternationalPG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.networkInternationalCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.networkInternationalCreditCardCharges] != null && prefList[WLPreferenceKeys.networkInternationalCreditCardCharges] != "")
            {
                try
                {
                    networkinternational_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.networkInternationalCreditCardCharges]);
                }
                catch (Exception)
                {
                    networkinternational_creditCardCharge = 0;
                }
            }
            else
            {
                networkinternational_creditCardCharge = 0;
            }
        }



        if (prefList.ContainsKey(WLPreferenceKeys.HotelSiteName))
        {
            siteName = prefList[WLPreferenceKeys.HotelSiteName];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelTradingName))
        {
            tradingName = prefList[WLPreferenceKeys.HotelTradingName];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelSiteTitle))
        {
            siteTitle = prefList[WLPreferenceKeys.HotelSiteTitle];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelTagLine))
        {
            tagLine = prefList[WLPreferenceKeys.HotelTagLine];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelEmailId))
        {
            emailId = prefList[WLPreferenceKeys.HotelEmailId];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelSiteLogo))
        {
            logo = imagePath + prefList[WLPreferenceKeys.HotelSiteLogo];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelPhoneNumber))
        {
            phoneNum = prefList[WLPreferenceKeys.HotelPhoneNumber];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelFaxNumber))
        {
            faxNum = prefList[WLPreferenceKeys.HotelFaxNumber];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelWaitingLogo))
        {
            waitingLogo = imagePath + prefList[WLPreferenceKeys.HotelWaitingLogo];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelWaitingText))
        {
            WaitingText = prefList[WLPreferenceKeys.HotelWaitingText];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelContactContect))
        {
            contactContent = prefList[WLPreferenceKeys.HotelContactContect];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelAboutContent))
        {
            aboutContent = prefList[WLPreferenceKeys.HotelAboutContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelTermsContent))
        {
            termsContent = prefList[WLPreferenceKeys.HotelTermsContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelPrivacyContent))
        {
            privacyContent = prefList[WLPreferenceKeys.HotelPrivacyContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelShowLoginPage))
        {
            showLoginPage = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelShowLoginPage]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelShowMenubar))
        {
            showMenuBar = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelShowMenubar]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelShowHeader))
        {
            showHeader = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelShowHeader]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelShowFooter))
        {
            showFooter = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelShowFooter]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHeaderDetails))
        {
            headerDetail = prefList[WLPreferenceKeys.HotelHeaderDetails];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHeaderImage))
        {
            headerImage = imagePath + prefList[WLPreferenceKeys.HotelHeaderImage];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHtmlContent))
        {
            headerText = prefList[WLPreferenceKeys.HotelHtmlContent];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelLeftTopPaneltext))
        {
            leftTopPanelText = prefList[WLPreferenceKeys.HotelLeftTopPaneltext];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHomePageUrl))
        {
            homePageUrl = prefList[WLPreferenceKeys.HotelHomePageUrl];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelDiscount))
        {
            if (prefList[WLPreferenceKeys.HotelDiscount] != null && prefList[WLPreferenceKeys.HotelDiscount] != "")
            {
                try
                {
                    discount = Convert.ToDecimal(prefList[WLPreferenceKeys.HotelDiscount]);
                }
                catch (Exception)
                {
                    discount = 0;
                }
            }
            else
            {
                discount = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelServiceCharge))
        {
            if (prefList[WLPreferenceKeys.HotelServiceCharge] != null && prefList[WLPreferenceKeys.HotelServiceCharge] != "")
            {
                try
                {
                    serviceCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.HotelServiceCharge]);
                }
                catch (Exception)
                {
                    serviceCharge = 0;
                }
            }
            else
            {
                serviceCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelCreditCardCharge))
        {
            if (prefList[WLPreferenceKeys.HotelCreditCardCharge] != null && prefList[WLPreferenceKeys.HotelCreditCardCharge] != "")
            {
                try
                {
                    creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.HotelCreditCardCharge]);
                }
                catch (Exception)
                {
                    creditCardCharge = 0;
                }
            }
            else
            {
                creditCardCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelHDFCPaymentGateway))
        {
            hdfcPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelHDFCPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelOXICASHPaymentGateway))
        {
            oxiCashPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelOXICASHPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelSBIPaymentGateway))
        {
            sbiPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelSBIPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelICICIPaymentGateway))
        {
            iciciPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelICICIPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelAMEXPaymentGateway))
        {
            amexPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelAMEXPaymentGateway]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelAMEXPaymentGateway))
        {
            amexPaymentGateway = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelAMEXPaymentGateway]);
        }

        if (prefList.ContainsKey(WLPreferenceKeys.DesiyaAllowed))
        {
            desiyaAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.DesiyaAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.IanDomesticAllowed))
        {
            ianDomesticAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.IanDomesticAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.TouricoAllowed))
        {
            touricoAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.TouricoAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.HotelBedsAllowed))
        {
            hotelBedsAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.HotelBedsAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.IanInternationAllowed))
        {
            ianInternationAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.IanInternationAllowed]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.GtaAllowed))
        {
            gtaAllowed = Convert.ToBoolean(prefList[WLPreferenceKeys.GtaAllowed]);
        }

        if (prefList.ContainsKey(WLPreferenceKeys.enbdPG))
        {
            enbd_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.enbdPG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ccAvenuePG))
        {
            ccavenue_pg = Convert.ToBoolean(prefList[WLPreferenceKeys.ccAvenuePG]);
        }
        if (prefList.ContainsKey(WLPreferenceKeys.enbdCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.enbdCreditCardCharges] != null && prefList[WLPreferenceKeys.enbdCreditCardCharges] != "")
            {
                try
                {
                    enbd_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.enbdCreditCardCharges]);
                }
                catch (Exception)
                {
                    enbd_creditCardCharge = 0;
                }
            }
            else
            {
                enbd_creditCardCharge = 0;
            }
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ccAvenueCreditCardCharges))
        {
            if (prefList[WLPreferenceKeys.ccAvenueCreditCardCharges] != null && prefList[WLPreferenceKeys.ccAvenueCreditCardCharges] != "")
            {
                try
                {
                    ccavenue_creditCardCharge = Convert.ToDecimal(prefList[WLPreferenceKeys.ccAvenueCreditCardCharges]);
                }
                catch (Exception)
                {
                    ccavenue_creditCardCharge = 0;
                }
            }
            else
            {
                ccavenue_creditCardCharge = 0;
            }
        }
        //CCAvenue PaymentGetWay (loading CCAvenue keys)
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenueAccessCode))
        {
            ccAvenue_AccessCode = prefList[WLPreferenceKeys.CCAvenueAccessCode];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenueEncryptKey))
        {
            ccAvenue_EncryptKey = prefList[WLPreferenceKeys.CCAvenueEncryptKey];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenueMerchantId))
        {
            ccAvenue_MerchantId = prefList[WLPreferenceKeys.CCAvenueMerchantId];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.CCAvenueUrl))
        {
            ccAvenue_Url = prefList[WLPreferenceKeys.CCAvenueUrl];
        }


        //ENBD PaymentGetWay (loading ENBD keys)
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDQueryString))
        {
            enbd_QueryString = prefList[WLPreferenceKeys.ENBDQueryString];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDCommand))
        {
            enbd_Command = prefList[WLPreferenceKeys.ENBDCommand];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDAccessCode))
        {
            enbd_AccessCode = prefList[WLPreferenceKeys.ENBDAccessCode];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDMerchant))
        {
            enbd_Merchant = prefList[WLPreferenceKeys.ENBDMerchant];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDSecureSecret))
        {
            enbd_SecureSecret = prefList[WLPreferenceKeys.ENBDSecureSecret];
        }
        if (prefList.ContainsKey(WLPreferenceKeys.ENBDLocale))
        {
            enbd_Locale = prefList[WLPreferenceKeys.ENBDLocale];
        }
    }

}
