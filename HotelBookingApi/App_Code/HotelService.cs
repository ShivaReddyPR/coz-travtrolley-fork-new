#region Summary
///////////////////////////////////////////////////////////////////////////////
/// AUTHOR		 : TBO
/// CREATE DATE	 : 
/// PURPOSE		 : 
/// SPECIAL NOTES: 
/// FILE NAME	 : $Workfile: HotelService.cs $	
/// VSS ARCHIVE	 : $Archive: VSS Path $
/// VERSION		 : $Revision: 1 $
///
/// ===========================================================================
/*/ $History: HotelService.cs $
 * 
 * *****************  Version 1  *****************
 * User: Kamal             Date:  16 Apr           Time: 
 * Description of change:Apply null check for HotelName field in request
 * 
 * *****************  Version 2  *****************
 * User:              Date:             Time: 
 * Description of change:
 * 

/*/
///
///////////////////////////////////////////////////////////////////////////////
#endregion

#region QA Status
#region Review History
///////////////////////////////////////////////////////////////////////////////
/// CODE STATUS	: NOT REVIEWED
///
/// ===========================================================================
///	REVIEW HISTORY
/// ---------------------------------------------------------------------------
/// DATE		| NAME			| COMMENTS
/// ---------------------------------------------------------------------------
/// 
///
///////////////////////////////////////////////////////////////////////////////
#endregion

#region Known Issues and Limitations
///////////////////////////////////////////////////////////////////////////////
/// DATE		: 16 Apr 2011
///	NAME		:  Ziyad
///	METHOD NAME	: Search
///	DESCRIPTION	: 
///
///----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
#endregion
#endregion

#region .Net Base Class Namespace Imports & Custom Namespace
using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Collections.Generic;
using System.Web.Caching;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.IO;

//Custom Namespace
//using CoreLogic;
//using CoreLogic;
//using Technology.BookingEngine;
//using Technology.BookingEngine.GDS;
//using AccountingEngine;
//using Technology.MetaSearchEngine;
//using Technology.Configuration;
//using Technology.CMS;
//using Technology.Data;
//using Technology.Indiatimes;
using CT.BookingEngine;
using CT.AccountingEngine;
using CT.MetaSearchEngine;
using CT.Configuration;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using CT.CMS;
using CT.BookingEngine.WhiteLabel;
using System.Data;

#endregion


public class AuthenticationData : SoapHeader
{
    public string SiteName;
    public string AccountCode;
    public string UserName;
    public string Password;
}

[WebService(Namespace = "http://CT/HotelBookingApi",
            Description = "Hotal Api for Search and Book",
            Name = "HotelBookingAPI")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class HotelBookingAPI : System.Web.Services.WebService
{

    #region Variables
    public AuthenticationData Credential;
    private string statusDescription;
    private string statusCode;
    private LoginInfo AgentLoginInfo;
    #endregion

    #region Constructor
    public HotelBookingAPI()
    {

    }

    #endregion


    #region Public Method
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSHotelSearchResponse Search(WSHotelSearchRequest request,bool isSightSeeing)
    {
        Trace.TraceInformation("HotelBookingAPI.Search entered. City Reference : " + request.CityReference + ", CheckIn date : " + request.CheckInDate.ToString("dd/MM/yyyy") + ", Checkout date : " + request.CheckOutDate.ToString("dd/MM/yyyy"));
        UserMaster member = new UserMaster();
        //validating and loading member from credential
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        if (member != null)
        {
            //validating Search Request
            if (!(WSSearchInput(request)))
            {
                WSHotelSearchResponse errorResponse = new WSHotelSearchResponse();
                WSStatus status = new WSStatus();
                status.Category = "SR";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                errorResponse.Result = new WSHotelResult[0];
                Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }
            WSHotelSearchResponse response = new WSHotelSearchResponse();
            WSHotelResult[] results = new WSHotelResult[0]; 
            HotelSearchResult[] result;
            response.Status = new WSStatus();
            response.IsDomestic = request.IsDomestic;
            bool isMultiRoom = false;
            HotelRequest mseRequest = new HotelRequest();
            DateTime before5 = DateTime.Now;
            #region Building Request
            //user preference
            WLHotelService wlHotelService = new WLHotelService();
            wlHotelService.Credential = Credential;
            //commented by brahmam code optimization
            //WSUserPreference userPreference = wlHotelService.GetPreferences((int)member.ID); //insted of member.LoginName i changed memberID
            //Request for the hotel sources available
            mseRequest.Sources = new List<string>();

            //commented by brahmam code optimization
            //bool isInternationalSearchAllowed = userPreference.IanInternationAllowed || userPreference.GtaAllowed || userPreference.HotelBedsAllowed || userPreference.TouricoAllowed;
            //bool isDomesticSearchAllowed = userPreference.DesiyaAllowed || userPreference.IanDomesticAllowed;

            #region HardCoded By brahmam testing purpose only
            //mseRequest.Sources.Add("DOTW");
            //mseRequest.Sources.Add("LOH");
            //mseRequest.Sources.Add("GTA");
            //mseRequest.Sources.Add("HotelBeds");
            if (!isSightSeeing)
            {
                DataTable dtSources = AgentMaster.GetAgentSources(AgentLoginInfo.AgentId, 2);
                if (dtSources.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtSources.Rows)
                    {
                        mseRequest.Sources.Add(dr["Name"].ToString());
                    }
                }
            }
            else
            {
                mseRequest.Sources.Add("GTA");// For Sight Seeing
            }
            #endregion
            mseRequest.AvailabilityType = AvailabilityType.Confirmed;
            mseRequest.StartDate = request.CheckInDate.Date;
            mseRequest.EndDate = request.CheckOutDate.Date;
            mseRequest.PassengerNationality = request.PassengerNationality;
            mseRequest.PassengerCountryOfResidence= request.PassengerCountryOfResidence;
            if (request.HotelName == null)
                mseRequest.HotelName = "";
            else
                mseRequest.HotelName = request.HotelName;
            mseRequest.IsDomestic = request.IsDomestic;
            mseRequest.NoOfRooms = request.NoOfRooms;
            mseRequest.Rating = WSHotelSearchRequest.GetWSHotelRating(request.Rating);
            mseRequest.RoomGuest = new RoomGuestData[request.NoOfRooms];
            for (int i = 0; i < request.NoOfRooms; i++)
            {
                mseRequest.RoomGuest[i] = new RoomGuestData();
                mseRequest.RoomGuest[i].noOfAdults = request.RoomGuest[i].NoOfAdults;
                mseRequest.RoomGuest[i].noOfChild = request.RoomGuest[i].NoOfChild;
                mseRequest.RoomGuest[i].childAge = request.RoomGuest[i].ChildAge;
            }
            mseRequest.ExtraCots = 0;
            mseRequest.SearchByArea = false;
            if (!request.IsDomestic && request.NoOfRooms > 1)
            {
                isMultiRoom = true;
            }
            mseRequest.IsMultiRoom = isMultiRoom;
            //City code and Name for the hotel search
            if (request.IsDomestic)
            {
                mseRequest.CityId = HotelCity.GetCityIdFromCityName(request.CityReference);
            }
            else
            {
                mseRequest.CityId = request.CityId;
            }
            mseRequest.CityName = request.CityReference;
            mseRequest.CountryName = request.CountryName;
            mseRequest.RequestMode = HotelRequestMode.BookingAPI;
            mseRequest.MinRating = 0;
            switch (request.Rating)
            {
                case WSHotelRatingInput.All:
                    mseRequest.MaxRating = 5;
                    break;
                case WSHotelRatingInput.FiveStarOrLess:
                    mseRequest.MaxRating = 5;
                    break;
                case WSHotelRatingInput.FourStarOrLess:
                    mseRequest.MaxRating = 4;
                    break;
                case WSHotelRatingInput.ThreeStarOrLess:
                    mseRequest.MaxRating = 3;
                    break;
                case WSHotelRatingInput.TwoStarOrLess:
                    mseRequest.MaxRating = 2;
                    break;
                case WSHotelRatingInput.OneStarOrLess:
                    mseRequest.MaxRating = 1;
                    break;
            }
            #endregion
            DateTime after5 = DateTime.Now;
            TimeSpan ts5 = after5 - before5;
            Audit.Add(EventType.Search, Severity.Normal, 1, "Gnarate Seac Rqet" + ts5.Minutes + ":" + ts5.Seconds + ":" + ts5.Milliseconds, "1");
            MetaSearchEngine mse = new MetaSearchEngine();
            try
            {
                result = new HotelSearchResult[0];
                //Getting Search results
                LoginInfo loginfo = AgentLoginInfo; //Added by brahmam
                mse.SettingsLoginInfo = loginfo;
                //for VAT IMPLEMENTATION
                mseRequest.LoginCountryCode = loginfo.LocationCountryCode;
                try
                {
                    mseRequest.CountryCode = Country.GetCountryCodeFromCountryName(request.CountryName);
                }
                catch { }

                DateTime before = DateTime.Now;
                result = mse.GetHotelResults(mseRequest, loginfo.AgentId);
                DateTime after = DateTime.Now;
                TimeSpan ts = after - before;
                Audit.Add(EventType.Search, Severity.Normal, 1, "Metasearch Engine" +ts.Minutes+":" +ts.Seconds+":"+ts.Milliseconds, "1");

                //HotelSearchResult hotelResult = new HotelSearchResult();
                //hotelResult.SortResultsByPrice(ref result, request.NoOfRooms);
                response.SessionId = mse.SessionId;
                if (result.Length == 0)
                {
                    response.Status.Category = "SR";
                    response.Status.StatusCode = "13";
                    response.Status.Description = "No Hotel Found";
                    response.Result = new WSHotelResult[0];
                    Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "No Hotel Found", "");
                    return response;
                }
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "SR";
                response.Status.StatusCode = "14";
                response.Status.Description = "Child should not be lead Guest of a room Search Failed, Technical Fault 01";
                response.Result = new WSHotelResult[0];
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("Hotel Search Method Failed, due to BookingEngine Exception", member, ex, "\n\n Destination city reference:" + request.CityReference + "\n\n Destination Country Name : " + request.CountryName + "\n\n CheckIn Date : " + request.CheckInDate.ToShortDateString() + "\n\n CheckOut Date " + request.CheckOutDate.ToShortDateString() + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (ArgumentException excep)
            {
                response.Status.Category = "SR";
                response.Status.StatusCode = "15";
                response.Status.Description = "Error comes While Searching: " + excep.Message;
                response.Result = new WSHotelResult[0];
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + excep.Message + " Stack Trace: " + excep.StackTrace, "");
                    SendErrorMail("Hotel Search Method Failed, due to ArgumentException Exception", member, excep, "\n\n Destination city reference:" + request.CityReference + "\n\n Destination Country Name : " + request.CountryName + "\n\n CheckIn Date : " + request.CheckInDate + "\n\n CheckOut Date " + request.CheckOutDate + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "SR";
                response.Status.StatusCode = "16";
                response.Status.Description = "Technical Fault 02: ";
                response.Result = new WSHotelResult[0];
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("Hotel Search Method Failed, due to Exception", member, ex, "\n\n Destination city reference:" + request.CityReference + "\n\n Destination Country Name : " + request.CountryName + "\n\n CheckIn Date : " + request.CheckInDate + "\n\n CheckOut Date " + request.CheckOutDate + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            try
            {
                DateTime before1 = DateTime.Now;
                //Updating Price in Search Results
                WSHotelSearchRequest.UpdatePrice(ref result, mseRequest, member.AgentId);  //Commented by brahmam

                //DateTime after1 = DateTime.Now;
                //TimeSpan ts1 = after1 - before1;
                //Audit.Add(EventType.Search, Severity.Normal, 1, "B2C Price Loading" + ts1.Minutes + ":" + ts1.Seconds + ":" + ts1.Milliseconds, "1");
                //long StopBytes = 0;
                //long StartBytes = System.GC.GetTotalMemory(true);
                int cacheTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["CacheTimeOut"]);
                //Inserting Result list in cache memory
                HttpContext.Current.Cache.Insert(response.SessionId + "-ResultList", result, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, cacheTimeOut, 0));
                //StopBytes = System.GC.GetTotalMemory(true);

                GC.KeepAlive(HttpContext.Current.Cache[response.SessionId + "-ResultList"]);
                // This ensure a reference to object keeps object in memory
                //long sizeInBytes = (long)(StopBytes - StartBytes);      

                //DateTime before2 = DateTime.Now;
                //Loading Search results in Api Search Results format
                results = WSHotelSearchRequest.GetHotelSearchResults(result, mseRequest, member.AgentId);

                DateTime after2 = DateTime.Now;
                TimeSpan ts2 = after2 - before1;
                Audit.Add(EventType.Search, Severity.Normal, 1, "Loading Result Object" + ts2.Minutes + ":" + ts2.Seconds + ":" + ts2.Milliseconds, "1");
            }
            catch (Exception ex)
            {
                response.Status.Category = "SR";
                response.Status.StatusCode = "17";
                response.Status.Description = "Technical Fault 03: ";
                response.Result = new WSHotelResult[0];
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("Hotel Search Method Failed, due to Exception", member, ex, "\n\n Destination city reference:" + request.CityReference + "\n\n Destination Country Name : " + request.CountryName + "\n\n CheckIn Date : " + request.CheckInDate + "\n\n CheckOut Date " + request.CheckOutDate + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            response.Result = results;
            response.Status.Category = "SR";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "Hotel Search Response:"
                + Serialize<WSHotelSearchResponse>(response), "");
            return response;
        }
        else
        {
            WSHotelSearchResponse response = new WSHotelSearchResponse();
            WSStatus wsStatus = new WSStatus();
            wsStatus.Category = "SR";
            wsStatus.Description = "login failed";
            wsStatus.StatusCode = "02";
            response.Status = wsStatus;
            response.Result = new WSHotelResult[0];
            Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, 0, "B2C-Hotel-Login Failed for Member for Search Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return response;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    //getRoom Details purpose only   
    public void GetRoomDetails(ref WSHotelResult result, WSHotelSearchRequest request, string sessionId)
    {
        Trace.TraceInformation("HotelBookingAPI.Search entered. City Reference : " + request.CityReference + ", CheckIn date : " + request.CheckInDate.ToString("dd/MM/yyyy") + ", Checkout date : " + request.CheckOutDate.ToString("dd/MM/yyyy"));
        UserMaster member = new UserMaster();
        //validating and loading member from credential
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
       // UserMaster.IsAuthenticatedUser(member.LoginName, "sadmin123", 1); //Testing pupose

        if (member != null)
        {
            if (!(WSSearchInput(request)))
            {
                WSHotelSearchResponse errorResponse = new WSHotelSearchResponse();
                WSStatus status = new WSStatus();
                status.Category = "SR";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                errorResponse.Result = new WSHotelResult[0];
                Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "Input parameter is incorrect: " + statusDescription, "");
            }
            bool isMultiRoom = false;
            HotelRequest mseRequest = new HotelRequest();

            #region Building Request
            //user preference
            WLHotelService wlHotelService = new WLHotelService();
            wlHotelService.Credential = Credential;
            WSUserPreference userPreference = wlHotelService.GetPreferences((int)member.ID); //insted of member.LoginName i changed memberID
            //Request for the hotel sources available
            mseRequest.Sources = new List<string>();
            bool isInternationalSearchAllowed = userPreference.IanInternationAllowed || userPreference.GtaAllowed || userPreference.HotelBedsAllowed || userPreference.TouricoAllowed;
            bool isDomesticSearchAllowed = userPreference.DesiyaAllowed || userPreference.IanDomesticAllowed;
            mseRequest.Sources.Add("DOTW");   //HardCoded By brahmam testing purpose only
            mseRequest.AvailabilityType = AvailabilityType.Confirmed;
            mseRequest.StartDate = request.CheckInDate.Date;
            mseRequest.EndDate = request.CheckOutDate.Date;
            mseRequest.PassengerNationality = request.PassengerNationality;
            mseRequest.PassengerCountryOfResidence= request.PassengerCountryOfResidence;
            if (request.HotelName == null)
                mseRequest.HotelName = "";
            else
                mseRequest.HotelName = request.HotelName;
            mseRequest.IsDomestic = request.IsDomestic;
            mseRequest.NoOfRooms = request.NoOfRooms;
            mseRequest.Rating = WSHotelSearchRequest.GetWSHotelRating(request.Rating);
            mseRequest.RoomGuest = new RoomGuestData[request.NoOfRooms];
            for (int i = 0; i < request.NoOfRooms; i++)
            {
                mseRequest.RoomGuest[i] = new RoomGuestData();
                mseRequest.RoomGuest[i].noOfAdults = request.RoomGuest[i].NoOfAdults;
                mseRequest.RoomGuest[i].noOfChild = request.RoomGuest[i].NoOfChild;
                mseRequest.RoomGuest[i].childAge = request.RoomGuest[i].ChildAge;
            }
            mseRequest.ExtraCots = 0;
            mseRequest.SearchByArea = false;
            if (!request.IsDomestic && request.NoOfRooms > 1)
            {
                isMultiRoom = true;
            }
            mseRequest.IsMultiRoom = isMultiRoom;
            //City code and Name for the hotel search
            if (request.IsDomestic)
            {
                mseRequest.CityId = HotelCity.GetCityIdFromCityName(request.CityReference);
            }
            else
            {
                mseRequest.CityId = request.CityId;
            }
            mseRequest.CityName = request.CityReference;
            mseRequest.CountryName = request.CountryName;
            mseRequest.RequestMode = HotelRequestMode.BookingAPI;
            mseRequest.MinRating = 0;
            switch (request.Rating)
            {
                case WSHotelRatingInput.All:
                    mseRequest.MaxRating = 5;
                    break;
                case WSHotelRatingInput.FiveStarOrLess:
                    mseRequest.MaxRating = 5;
                    break;
                case WSHotelRatingInput.FourStarOrLess:
                    mseRequest.MaxRating = 4;
                    break;
                case WSHotelRatingInput.ThreeStarOrLess:
                    mseRequest.MaxRating = 3;
                    break;
                case WSHotelRatingInput.TwoStarOrLess:
                    mseRequest.MaxRating = 2;
                    break;
                case WSHotelRatingInput.OneStarOrLess:
                    mseRequest.MaxRating = 1;
                    break;
            }
            mseRequest.LoginCountryCode = AgentLoginInfo.LocationCountryCode;
            try
            {
                mseRequest.CountryCode = Country.GetCountryCodeFromCountryName(request.CountryName);
            }
            catch { }
            
            #endregion


            WSHotelSearchResponse response = new WSHotelSearchResponse();
            HotelSearchResult[] mseResults = (HotelSearchResult[])HttpContext.Current.Cache[sessionId + "-ResultList"]; 
            HotelSearchResult hotelResult=new HotelSearchResult();
            try
            {
                //mseResults = (HotelSearchResult[])HttpContext.Current.Cache[sessionId + "-ResultList"];
                if (mseResults != null && mseResults.Length > 0)
                {
                    hotelResult = WSGetCancellationPolicyRequest.GetSelectedSearchResult(result.Index, mseResults);
                }
                else
                {
                    throw new ArgumentException("Hotelresult not loaded from Cache, This may be because cache data has expired");
                }
            }
            catch (Exception ex)
            {
                response.Status = new WSStatus();
                response.Status.Category = "HD";
                response.Status.StatusCode = "05";
                response.Status.Description = "Technical Fault 04. ";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, (int)member.ID, "B2C-Hotel-Error, Hotelresult not loaded from Cache : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetHotelDetails Method Failed, due to Exception", member, ex, "\n\n Hotelresult not loaded from Cache  \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    
                }
                
            }
            Dictionary<string, decimal> ExchangeRates = new Dictionary<string, decimal>();
            int agencyId = 0;
            DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load(member.AgentId, hotelResult.BookingSource.ToString(), (int)ProductType.Hotel);
            DataView dv = dtMarkup.DefaultView;
            dv.RowFilter = "TransType IN('B2B')";
            dtMarkup = dv.ToTable();
            CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(sessionId.ToString());
            LoginInfo loginfo = AgentLoginInfo;
            agencyId = loginfo.AgentId;
            dotw.AgentCurrency = loginfo.Currency;
            ExchangeRates = loginfo.AgentExchangeRates;
            dotw.ExchangeRates = loginfo.AgentExchangeRates;
            dotw.DecimalPoint = loginfo.DecimalValue;
            try
            {
                Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);

                if (activeSources != null && activeSources.Count > 0)
                {
                    dotw.SourceCountryCode = activeSources[HotelBookingSource.DOTW.ToString()];
                }
                else
                {
                    dotw.SourceCountryCode = "AE";// TODO Ziyad
                }
            }
            catch
            {
                dotw.SourceCountryCode = "AE";
            }

            decimal markup = 0; string markupType = string.Empty;

            if (dtMarkup != null && dtMarkup.Rows.Count > 0)
            {
                markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
            }
            dotw.GetRooms(ref hotelResult, mseRequest, markup, markupType);
            mseResults[result.Index-1] = hotelResult;
            try
            {
                HotelSearchResult[] searchresult = new HotelSearchResult[1];
              
                searchresult[0] = hotelResult;
                //Updating Price in Search Results
                WSHotelSearchRequest.UpdatePrice(ref searchresult, mseRequest, member.AgentId);  //Commented by brahmam
                int cacheTimeOut=Convert.ToInt32(ConfigurationManager.AppSettings["CacheTimeOut"]);
                //Inserting Result list in cache memory
                HttpContext.Current.Cache.Insert(sessionId + "-ResultList", mseResults, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, cacheTimeOut, 0));

                GC.KeepAlive(HttpContext.Current.Cache[sessionId + "-ResultList"]);

                WSHotelResult[] arrResults = WSHotelSearchRequest.GetHotelSearchResults(searchresult, mseRequest, member.AgentId);
                result = arrResults[0];
            }
            catch (Exception ex)
            {
                response.Status.Category = "SR";
                response.Status.StatusCode = "17";
                response.Status.Description = "Technical Fault 03: ";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("Hotel Search Method Failed, due to Exception", member, ex, "\n\n Destination city reference:" + request.CityReference + "\n\n Destination Country Name : " + request.CountryName + "\n\n CheckIn Date : " + request.CheckInDate + "\n\n CheckOut Date " + request.CheckOutDate + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    
                }
                
            }

        }
        else
        {
            WSHotelSearchResponse response = new WSHotelSearchResponse();
            WSStatus wsStatus = new WSStatus();
            wsStatus.Category = "SR";
            wsStatus.Description = "login failed";
            wsStatus.StatusCode = "02";
            response.Status = wsStatus;
            response.Result = new WSHotelResult[0];
            Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, 0, "B2C-Hotel-Login Failed for Member for Search Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetHotelDetailResponse GetHotelDetails(WSGetHotelDetailRequest request, string paxNationality, string paxResidence)
    {
        Trace.TraceInformation("HotelBookingAPI.GetHotelDetails entered");
        UserMaster member = new UserMaster();

        //validating and loading member from credential
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        if (member != null)
        {
            //Validating GetHotelDetails Request
            if (!(WSGetHotelDetailsInput(request)))
            {
                WSGetHotelDetailResponse errorResponse = new WSGetHotelDetailResponse();
                WSStatus status = new WSStatus();
                status.Category = "HD";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect index = " + request.Index + " " + statusDescription, "");
                return errorResponse;
            }
            WSGetHotelDetailResponse response = new WSGetHotelDetailResponse();
            WSHotelDetail hDetail = new WSHotelDetail();
            HotelDetails hotelDetail = new HotelDetails();
            HotelSearchResult[] mseResults;
            HotelSearchResult hotelResult;
            try
            {
                mseResults = (HotelSearchResult[])HttpContext.Current.Cache[request.SessionId + "-ResultList"];
                if (mseResults != null && mseResults.Length > 0)
                {
                    hotelResult = WSGetCancellationPolicyRequest.GetSelectedSearchResult(request.Index, mseResults);
                }
                else
                {
                    throw new ArgumentException("Hotelresult not loaded from Cache, This may be because cache data has expired");
                }
            }
            catch (Exception ex)
            {
                response.Status = new WSStatus();
                response.Status.Category = "HD";
                response.Status.StatusCode = "05";
                response.Status.Description = "Technical Fault 04. ";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, (int)member.ID, "B2C-Hotel-Error, Hotelresult not loaded from Cache : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetHotelDetails Method Failed, due to Exception", member, ex, "\n\n Hotelresult not loaded from Cache  \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            //HotelBookingSource source = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), request.BookingSource.ToString());
            response.Status = new WSStatus();
            try
            {
                MetaSearchEngine mse = new MetaSearchEngine(request.SessionId);
                mse.SettingsLoginInfo = AgentLoginInfo;
                //Getting Hotel details
                //Commented by brahmam     in hotelResult class we need add two more parameters PassengerNationality and PassengerCountryOfResidence based on new HotelAppi

                //hotelDetail = mse.GetHotelDetails(hotelResult.CityCode, hotelResult.HotelName, hotelResult.HotelCode, hotelResult.BookingSource);
                hotelDetail = mse.GetHotelDetails(hotelResult.CityCode,hotelResult.HotelCode,hotelResult.StartDate,hotelResult.EndDate,paxNationality,paxResidence,hotelResult.BookingSource);
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "HD";
                response.Status.StatusCode = "06";
                response.Status.Description = "GetHotelDetails Failed, Technical Fault 01. ";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetHotelDetails Method Failed, due to BookingEngineException Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + "\n\n CityCode " + hotelResult.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "HD";
                response.Status.StatusCode = "07";
                response.Status.Description = "Error returned while getting hotel details," + ex.Message;
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetHotelDetails Method Failed, due to ArgumentException Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + "\n\n CityCode " + hotelResult.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "HD";
                response.Status.StatusCode = "08";
                response.Status.Description = "Technical Fault 02. ";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetHotelDetails Method Failed, due to Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + "\n\n CityCode " + hotelResult.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            try
            {
                //Filling HotelDetails in Api HotelDetail object
                hDetail = WSGetHotelDetailRequest.FillHotelDetails(hotelDetail, hotelResult);
            }
            catch (Exception ex)
            {
                response.Status.Category = "HD";
                response.Status.StatusCode = "09";
                response.Status.Description = "Technical Fault 03. ";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetHotelDetails Method Failed, due to Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + "\n\n CityCode " + hotelResult.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            response.HotelDetail = hDetail;
            response.Status.Category = "HD";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, (int)member.ID, "B2C-Hotel-GetHotelDetails Successful", "");
            return response;
        }
        else
        {
            WSGetHotelDetailResponse errorResponse = new WSGetHotelDetailResponse();
            WSStatus status = new WSStatus();
            status.Category = "HD";
            status.Description = "GetHotelDetails Authentication Failed";
            status.StatusCode = "02";
            errorResponse.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.GetHotelDetails, Severity.High, 0, "B2C-Hotel-Login Failed for Member for GetHotelDetails Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetCancellationPolicyResponse GetCancellationPolicy(WSGetCancellationPolicyRequest request, string[] ratePlanCodeList, string[] roomTypeCodeList)
    {
        Trace.TraceInformation("HotelBookingAPI.GetCancellationPolicy entered.");
        UserMaster member = new UserMaster();

        //validating and loading member from credential
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        if (member != null)
        {
            //Validating GetCancellationPolicy input
            if (!(WSGetCancellationPolicyInput(request)))
            {
                WSGetCancellationPolicyResponse errorResponse = new WSGetCancellationPolicyResponse();
                WSStatus status = new WSStatus();
                status.Category = "CP";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditHotelBookingAPI(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect Index = " + request.Index + " and NoOfRooms = " + request.NoOfRooms + " " + statusDescription, "");
                return errorResponse;
            }
            WSGetCancellationPolicyResponse response = new WSGetCancellationPolicyResponse();
            response.Status = new WSStatus();
            HotelSearchResult[] mseResults;
            HotelSearchResult hotelResult = new HotelSearchResult();
            try
            {
                mseResults = (HotelSearchResult[])HttpContext.Current.Cache[request.SessionId + "-ResultList"];
                if (mseResults != null && mseResults.Length > 0)
                {
                    hotelResult = WSGetCancellationPolicyRequest.GetSelectedSearchResult(request.Index, mseResults);
                }
                else
                {
                    throw new ArgumentException("Hotelresult not loaded from Cache, This may be because cache data has expired");
                }
                request.RatePlanCode = request.RatePlanCode.Replace("+", "#");
                request.RoomTypeCode = request.RoomTypeCode.Replace("+", "#");
            }
            catch (Exception ex)
            {
                response.Status.Category = "CP";
                response.Status.StatusCode = "08";
                response.Status.Description = "Technical fault 02";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-Hotel-Error, HotelResult not loaded from Cache : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetCancellationPolicy Method Failed, due to ArgumentException Exception", member, ex, "\n\n , HotelResult not loaded from Cache  \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            try
            {

                response = WSGetCancellationPolicyRequest.LoadCancellationPolicy(request, hotelResult, ratePlanCodeList, roomTypeCodeList, AgentLoginInfo);
                if (response.CancellationPolicy.Length <= 0)
                {
                    response.Status.Category = "CP";
                    response.Status.StatusCode = "09";
                    response.Status.Description = "Cancellation Policies not Found.";
                    Audit.AddAuditHotelBookingAPI(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-Hotel-Cancillation Policies not Found.", "");
                    return response;
                }
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "CP";
                response.Status.StatusCode = "10";
                response.Status.Description = "GetCancellationPolicy Failed. Technical fault 01";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetCancellationPolicy Method Failed, due to BookingEngineException Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + "\n\n CityCode " + hotelResult.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "CP";
                response.Status.StatusCode = "11";
                response.Status.Description = "GetCancellationPolicy function returning error: " + ex.Message;
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetCancellationPolicy Method Failed, due to ArgumentException Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + "\n\n CityCode " + hotelResult.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "CP";
                response.Status.StatusCode = "12";
                response.Status.Description = "Technical fault 03";
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace: " + ex.StackTrace, "");
                    SendErrorMail("GetCancellationPolicy Method Failed, due to ArgumentException Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + "\n\n CityCode " + hotelResult.CityCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    return response;
                }
                return response;
            }
            response.Status.Category = "CP";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.AddAuditHotelBookingAPI(EventType.GetCancellationPolicy, Severity.High, (int)member.ID, "B2C-Hotel-GetCancellationPolicy Successful", "");
            return response;
        }
        else
        {
            WSGetCancellationPolicyResponse errorResponse = new WSGetCancellationPolicyResponse();
            WSStatus status = new WSStatus();
            status.Category = "CP";
            status.Description = "GetCancellationPolicies Authentication Failed";
            status.StatusCode = "02";
            errorResponse.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.GetCancellationPolicy, Severity.High, 0, "B2C-Hotel-Login Failed for Member for GetCancellationPolicy Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }


    #region Commented by brahmam because CreditCardPaymentInformation Class is not unavability in AccountEngine
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public WSBookResponse Book(WSBookRequest request)
    {
        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, 1, "B2C-Hotel-Root:Hotel Book Request: " + Serialize<WSBookRequest>(request) + " :Credentials:" + Serialize<AuthenticationData>(Credential), "");
        Trace.TraceInformation("HotelBookingAPI.Book entered.");
        CreditCardPaymentInformation cardInfo = new CreditCardPaymentInformation();

        #region Checking if paymentMode is creditcard and payment info not null
        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
        {
            //filling CreditCardPaymentInformation object
            cardInfo.Amount = request.PaymentInfo.Amount;
            cardInfo.PaymentGateway = (CT.AccountingEngine.PaymentGatewaySource)Enum.Parse(typeof(CT.AccountingEngine.PaymentGatewaySource), request.PaymentInfo.PaymentGateway.ToString());
            cardInfo.PaymentId = request.PaymentInfo.PaymentId;
            cardInfo.TrackId = request.PaymentInfo.TrackId;
            cardInfo.IPAddress = request.PaymentInfo.IPAddress;
            cardInfo.Charges = request.PaymentInfo.CreditCardCharges;
        }

        #endregion

        UserMaster member = new UserMaster();

        //Validating member from credentials
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        WSBookResponse response = new WSBookResponse();
        WSStatus status = new WSStatus();
        HotelSearchResult[] mseResults;
        HotelSearchResult hotelResult;
        //WSHotelResult result;
        #region if (member != null)
        if (member != null)
        {
            try
            {
                UserPreference pref = new UserPreference();
                bool isPGOwnedBYTBO = false;
                pref = pref.GetPreference((int)member.ID, "ISPGOWNEDBYTBO", "Booking & Confirmation");
                if (pref != null && pref.Value != null)
                {
                    isPGOwnedBYTBO = Convert.ToBoolean(pref.Value);
                }
                Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-isPGOwnedBYTBO : " + isPGOwnedBYTBO.ToString() + " -- request.PaymentInfo.Amount : " + request.PaymentInfo.Amount.ToString(), "");
                if (request.PaymentInfo != null && request.PaymentInfo.PaymentGateway.ToString().Trim() != "" && request.PaymentInfo.PaymentGateway.ToString().Trim() != "0" && request.PaymentInfo.Amount > 0 && (request.Guest != null && request.Guest[0].FirstName != null && request.Guest[0].LastName != null && request.Guest[0].FirstName != "" && request.Guest[0].LastName != "") && isPGOwnedBYTBO)
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Entered Accpet Payment", "");
                    int paymentId = AcceptPayment(request.PaymentInfo.PaymentGateway, request.PaymentInfo.TrackId, request.PaymentInfo.Amount, member.AgentId, (int)member.ID, request.Guest[0], true);
                }

                #region Cache Search Result
                mseResults = (HotelSearchResult[])HttpContext.Current.Cache[request.SessionId + "-ResultList"];
                if (mseResults != null && mseResults.Length > 0)
                {
                    hotelResult = WSGetCancellationPolicyRequest.GetSelectedSearchResult(request.Index, mseResults);
                    //hotelResult = WSBookRequest.GetSelectedSearchResult(request, mseResults, (int)member.ID); //price validate not required
                }
                else
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Hotelresult not loaded from Cache, This may be because cache data has expired", "");
                    throw new ArgumentException("Hotelresult not loaded from Cache, This may be because cache data has expired");
                }
                #endregion

                #region if (hotelResult == null)
                if (hotelResult == null)
                {
                    status.Category = "BK";
                    status.Description = "Technical Fault 05:Hotel Search Results not found, any of HotelCode, HotelName or Index is incorrect"; // Actual fault
                    status.StatusCode = "47";
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Technical Fault 05:Hotel Search Results not found, any of HotelCode, HotelName or Index is incorrect", "");
                    }
                    catch (Exception)
                    {
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                #endregion
                //result = WSBookRequest.GetSelectedApiSearchResult(hotelResult, request.Index, request.NoOfRooms, member.AgencyId);
            }
            #region catch (ArgumentException ex)
            catch (ArgumentException ex)
            {
                status.Category = "BK";
                status.StatusCode = "47";
                status.Description = ex.Message.ToString() + "| Date Time:" + DateTime.Now.ToString();
                response.Status = status;
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                    SendErrorMail("Book Method Failed, because any of HotelCode, HotelName, Index or RoomDetails is incorrect", member, ex, "\n\n HotelName:" + request.HotelName + "\n\n Index : " + request.Index + "\n\n Hotel Code : " + request.HotelCode + " \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;      //CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                {
                    cardInfo.ReferenceId = 0;
                    cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;//CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                    cardInfo.Save();
                }
                return response;
            }
            catch (Exception ex)
            {
                status.Category = "BK";
                status.Description = "Technical Fault 01";
                status.StatusCode = "37";
                response.Status = status;
                try
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                    SendErrorMail("Book Method Failed, due to Exception", member, ex, "\n\n Hotel Search Results not loaded from Cache \n\n Status Description : " + response.Status.Description);
                }
                catch (Exception)
                {
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                {
                    cardInfo.ReferenceId = 0;
                    cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                    cardInfo.Save();
                }
                return response;
            }
            #endregion


            //Validating BookRequest input
            #region if (!(WSBookInput(request, hotelResult)))
            if (!(WSBookInput(request, hotelResult)))
            {
                WSBookResponse errorResponse = new WSBookResponse();


                status.Category = "BK";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect: " + statusDescription, "");
                if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                {
                    cardInfo.ReferenceId = 0;
                    cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                    cardInfo.Save();
                }
                return errorResponse;
            }
            #endregion
            else
            {

                decimal bookingAmount = 0;
                if (request.SpecialRequest == null)
                {
                    request.SpecialRequest = "";
                }
                if (request.FlightInfo == null)
                {
                    request.FlightInfo = "";
                }
                //get the booking amount

                #region Check Agent Capable
                decimal agentCapable = 0;
                if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType != WSPaymentModeType.CreditCard)
                {
                    bookingAmount = request.PaymentInfo.Amount;
                    try
                    {
                        //agentCapable = Ledger.IsAgentCapable(member.ID, AccountType.Hotel);  //Commented by brahmam
                    }
                    catch (Exception ex)
                    {

                        status.Category = "BK";
                        status.Description = "Technical Fault 02";
                        status.StatusCode = "38";
                        response.Status = status;
                        try
                        {
                            Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-AccountingEngine.Ledger.IsAgentCapable failed: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                            SendErrorMail("Book Method Failed, due to Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + " \n\n Status Description : " + response.Status.Description);
                        }
                        catch (Exception)
                        {
                            return response;
                        }
                        return response;
                    }
                }
                #endregion

                #region Check Insufficient amount
                if ((request.PaymentInfo != null && request.PaymentInfo.PaymentModeType != WSPaymentModeType.CreditCard) && (agentCapable < bookingAmount))
                {
                    status.Category = "BK";
                    status.Description = "Insufficient amount to book this Hotel";
                    status.StatusCode = "39";
                    response.Status = status;
                    Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Insufficient amount to book this Hotel", "");
                    return response;
                }
                #endregion
                if (string.IsNullOrEmpty(hotelResult.HotelAddress) || hotelResult.HotelAddress.Trim().Length < 1)
                {
                    #region Loading static datat
                    try
                    {
                        //Added by brahmam avoid static data download in search time
                        //Load static data and apply hotelResult object
                        MetaSearchEngine mse = new MetaSearchEngine(request.SessionId);
                        mse.SettingsLoginInfo = AgentLoginInfo;
                        HotelDetails hotelDetail = mse.GetHotelDetails(hotelResult.CityCode, hotelResult.HotelCode, hotelResult.StartDate, hotelResult.EndDate, request.PassengerNationality, request.PassengerCountryOfResidence, hotelResult.BookingSource);
                        hotelResult.HotelAddress = hotelDetail.Address;
                        hotelResult.HotelDescription = hotelDetail.Description;
                        hotelResult.HotelContactNo = hotelDetail.PhoneNumber;
                        if (hotelDetail.Email != null && hotelDetail.Email.Length > 0 && !hotelDetail.Email.Contains("NA"))
                        {
                            hotelResult.HotelAddress += "\n E-mail :" + hotelDetail.Email;
                        }
                        if (hotelDetail.URL != null && hotelDetail.URL.Length > 0 && !hotelDetail.URL.Contains("NA"))
                        {
                            hotelResult.HotelAddress += "\n Website :" + hotelDetail.URL;
                        }
                    }
                    catch (Exception ex)
                    {

                        status.Category = "BK";
                        status.Description = "Technical Fault 02";
                        status.StatusCode = "38";
                        response.Status = status;
                        try
                        {
                            Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "Loading ststic data failed: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                            SendErrorMail("Book Method Failed, due to Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + " \n\n Status Description : " + response.Status.Description);
                        }
                        catch (Exception)
                        {
                            return response;
                        }
                        return response;
                    }
                }
                #endregion
                try
                {
                    //get the itinerary completed from the booking request

                    HotelItinerary itineary = WSBookRequest.BuildHotelItinerary(request, member, hotelResult,AgentLoginInfo);


                    //AgentMaster agency = new AgentMaster(member.ID);
                    AgentMaster agency = new AgentMaster(AgentLoginInfo.AgentId);
                    decimal hotelRate = 0;
                    decimal amountToCompare = agentCapable;
                    for (int k = 0; k < itineary.Roomtype.Length; k++)
                    {
                        hotelRate += itineary.Roomtype[k].Price.GetAgentPrice();
                    }
                    #region Agency account balance check
                    if (amountToCompare < hotelRate && request.PaymentInfo != null && request.PaymentInfo.PaymentModeType != WSPaymentModeType.CreditCard)
                    {
                        status.Category = "BK";
                        status.Description = "Remaining account balance of agency is insufficient for this booking";
                        status.StatusCode = "40";
                        response.Status = status;
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Remaining account balance of agency is insufficient for this booking", "");
                        return response;
                    }
                    #endregion

                    BookingResponse bookRes = new BookingResponse();
                    itineary.ProductType = ProductType.Hotel;
                    Product prod = (Product)itineary;
                    MetaSearchEngine mse = new MetaSearchEngine(request.SessionId);
                    //mse.SettingsLoginInfo = UserMaster.IsAuthenticatedUser(member.LoginName, "sadmin123", 1, true);
                    // sending for booking
                    #region Meta Search Engine and Booking Processing
                    bookRes = mse.Book(ref prod, (int)agency.ID, BookingStatus.Ready ,member.ID, true, BookingType.Book, itineary.BookingMode);

                    //Root:Hotel Book Response: Error in Serialisation

                    //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, "bookRes.BookingId: " + bookRes.BookingId, "");
                    //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, "bookRes.ConfirmationNo: " + bookRes.ConfirmationNo, "");
                    //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, "bookRes.Error: " + bookRes.Error, "");
                    //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, "bookRes.PNR: " + bookRes.PNR, "");
                    //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, "bookRes.ProdType: " + bookRes.ProdType, "");
                    //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, "bookRes.SSRDenied: " + bookRes.SSRDenied, "");
                    //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, "bookRes.SSRMessage: " + bookRes.SSRMessage, "");
                    //Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, "bookRes.Status: " + bookRes.Status, "");

                    if (bookRes.Status == BookingResponseStatus.OtherFare)
                    {
                        status.Category = "BK";
                        status.Description = "Fare Not available";
                        status.StatusCode = "41";
                        response.Status = status;
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-OtherFare: Fare Not available", "");
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    response.ConfirmationNo = bookRes.ConfirmationNo;
                    response.BookingId = Convert.ToString(bookRes.BookingId);
                    response.ReferenceNo = itineary.BookingRefNo;
                    response.HotelId = itineary.HotelId;

                    if (bookRes.Status == BookingResponseStatus.Successful)
                    {

                        #region Added Promotion
                        //try
                        //{
                        //    PromoDetails promoDet = new PromoDetails();
                        //    promoDet.PromoId = request.PromoDetail.PromoId;
                        //    promoDet.PromoCode = request.PromoDetail.PromoCode;
                        //    promoDet.ProductId = (int)ProductType.Hotel;
                        //    promoDet.DiscountType = request.PromoDetail.PromoDiscountType;
                        //    promoDet.DiscountValue = request.PromoDetail.PromoDiscountValue;
                        //    promoDet.DiscountAmount = request.PromoDetail.PromoDiscountAmount;
                        //    promoDet.ReferenceId = itineary.HotelId;
                        //    promoDet.CreatedBy = (int)member.ID;
                        //    promoDet.Save();
                        //}
                        //catch (Exception ex)
                        //{
                        //    Audit.Add(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Hotel API Book:Exception while saving PromoDetails.. Message:" + ex.Message, "");
                        //}

                        #endregion


                        #region invoice generation for TBO Connect Hotel
                        //if (hotelResult.BookingSource == HotelBookingSource.TBOConnect)
                        {
                            HotelRoom hRoom = new HotelRoom();
                            HotelRoom[] hRoomList = hRoom.Load(response.HotelId);
                            //Price info
                            int invoiceNumber = 0;
                            Invoice invoice = new Invoice();
                            try
                            {
                                // Generating invoice.


                                if (hRoomList.Length > 0)
                                {
                                    invoiceNumber = Invoice.isInvoiceGenerated(hRoomList[0].RoomId, ProductType.Hotel);
                                }
                                if (invoiceNumber == 0)
                                {
                                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(response.HotelId, string.Empty, (int)member.ID, ProductType.Hotel, 1);
                                }
                            }
                            catch (Exception exp)
                            {
                                Audit.Add(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Hotel API Book:Exception while generating Invoice.. Message:" + exp.Message, "");
                            }
                        }
                        #endregion
                        status.Category = "BK";
                        status.Description = "Successful";
                        status.StatusCode = "01";
                        response.Status = status;

                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "Book Successful", "");
                    }
                    else if (bookRes.Status == BookingResponseStatus.Failed)
                    {
                        status.Category = "BK";
                        status.Description = "Booking Failed.";
                        status.StatusCode = "42";
                        response.Status = status;
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "Booking Failed", "");
                        return response;
                    }
                    else    //"Unable to Book the Hotel"
                    {
                        status.Category = "BK";
                        status.Description = "Unable to Book the Hotel";
                        status.StatusCode = "43";
                        response.Status = status;
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                        {
                            cardInfo.ReferenceId = Convert.ToInt32(response.BookingId);
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "Unable to Book the Hotel", "");
                        return response;
                    }
                    #endregion

                }
                #region    catch (BookingEngineException ex)
                catch (BookingEngineException ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "44";
                    status.Description = "Technical fault 03";
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to BookingEngineException Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                #endregion

                #region catch (ArgumentException ex)
                catch (ArgumentException ex)
                {
                    status.Category = "BK";
                    status.StatusCode = "45";
                    status.Description = ex.Message.ToString() + "| Date Time:" + DateTime.Now.ToString();
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to BookingEngineException Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                #endregion

                #region catch (Exception ex)
                catch (Exception ex)
                {
                    status.Category = "BK";
                    status.Description = "Technical fault 04";
                    status.StatusCode = "46";
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Error : " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("Book Method Failed, due to BookingEngineException Exception", member, ex, "\n\n HotelName:" + hotelResult.HotelName + "\n\n Booking Source : " + hotelResult.BookingSource + "\n\n Hotel Code : " + hotelResult.HotelCode + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                        {
                            cardInfo.ReferenceId = 0;
                            cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                            cardInfo.Save();
                        }
                        return response;
                    }
                    if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard && hotelResult.BookingSource != HotelBookingSource.IAN)
                    {
                        cardInfo.ReferenceId = 0;
                        cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                        cardInfo.Save();
                    }
                    return response;
                }
                #endregion

                return response;
            }
        }
        #endregion
        else
        {
            WSBookResponse errorResponse = new WSBookResponse();
            status.Category = "BK";
            status.Description = "Authentication failed for book";
            status.StatusCode = "02";
            errorResponse.Status = status;
            if (request.PaymentInfo != null && request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
            {
                cardInfo.ReferenceId = 0;
                cardInfo.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;//ReferenceIdType.Invoice;
                cardInfo.Save();
            }
            Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, 0, "B2C-Hotel-Login Failed for Member for Book Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }
    #endregion

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetHotelBookingResponse GetHotelBooking(WSGetHotelBookingRequest request)
    {

        Trace.TraceInformation("HotelBookingAPI.GetHotelBooking entered.");
        UserMaster member = new UserMaster();
        member = WSValidate(Credential);
        if (member != null)
        {
            if (!(WSGetHotelBookingInput(request)))
            {
                WSGetHotelBookingResponse errorResponse = new WSGetHotelBookingResponse();
                WSStatus status = new WSStatus();
                status.Category = "GB";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }
            else
            {
                WSGetHotelBookingResponse response = new WSGetHotelBookingResponse();
                WSStatus status = new WSStatus();
                BookingDetail booking = new BookingDetail();
                HotelItinerary itineary = new HotelItinerary();
                try
                {
                    string bookingIdRegex = "^[0-9]+$";
                    int hotelId=0;
                    bool isInteger = Regex.IsMatch(request.BookingId.Trim(), bookingIdRegex);
                    if (!isInteger)
                    {
                        try
                        {
                            hotelId = HotelItinerary.GetHotelId(request.BookingId.Trim());
                            if (hotelId > 0)
                            {
                                string bookId = BookingDetail.GetBookingIdByProductId(hotelId, ProductType.Hotel).ToString();
                                request.BookingId = bookId;
                            }
                        }
                        catch (ArgumentException ex)
                        {
                            throw new ArgumentException("Invalid BookingId", ex);
                        }
                        catch (SqlException sqlExec)
                        {
                            throw new DALException("Query error occured", sqlExec);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    try
                    {
                        booking = new BookingDetail(Convert.ToInt32(request.BookingId));
                    }
                    catch
                    {
                        //As in case of some hotel bookings there is only digits in confirmation no.
                        hotelId = HotelItinerary.GetHotelId(request.BookingId.Trim());
                        if (hotelId > 0)
                        {
                            string bookId = BookingDetail.GetBookingIdByProductId(hotelId, ProductType.Hotel).ToString();
                            request.BookingId = bookId;
                            booking = new BookingDetail(Convert.ToInt32(request.BookingId));
                        }
                    }
                    

                    Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.Hotel)
                        {
                            itineary.Load(products[i].ProductId);
                            itineary.BookingMode = products[i].BookingMode;
                            itineary.ProductId = products[i].ProductId;
                            itineary.ProductType = products[i].ProductType;
                            itineary.ProductTypeId = products[i].ProductTypeId;
                            //foreach (HotelPenality hPenality in itineary.PenalityInfo)
                            //{
                            //    hPenality.BookingMode = products[i].BookingMode;
                            //    hPenality.ProductId = products[i].ProductId;
                            //    hPenality.ProductType = products[i].ProductType;
                            //    hPenality.ProductTypeId = products[i].ProductTypeId;
                            //}
                            // Getting Passenger details
                            HotelPassenger hotelPax = new HotelPassenger();
                            hotelPax.Load(products[i].ProductId);
                            itineary.HotelPassenger = hotelPax;
                            DataTable dtPromoDetails = PromoDetails.GetPromoDetails(itineary.HotelId);

                            if (dtPromoDetails != null && dtPromoDetails.Rows.Count > 0)
                            {
                                response.PromoDetail = new WSPromoDetail();
                                response.PromoDetail.PromoCode = dtPromoDetails.Rows[0]["Promo_Code"].ToString();
                                response.PromoDetail.PromoDiscountAmount = Convert.ToDecimal(dtPromoDetails.Rows[0]["Promo_Discount_Amount"]);
                            }
                            //Getting HotelRoom Details                
                            HotelRoom hRoom = new HotelRoom();
                            itineary.Roomtype = hRoom.Load(products[i].ProductId);
                            for (int j = 0; j < itineary.Roomtype.Length; j++)
                            {
                                PriceAccounts price = new PriceAccounts();
                                price = itineary.Roomtype[j].Price;
                                //Commented by brahmam
                                //WLAccountingEngine.PriceWithHotelWLDiscount(ref price, member.AgencyId, price.AccPriceType);
                                itineary.Roomtype[j].Price = price;
                                if (itineary.Source == HotelBookingSource.IAN)
                                {
                                    itineary.Roomtype[j].Price.WLCharge = 0;
                                }
                            }
                            break;
                        }
                    }
                }
                catch (BookingEngineException ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "04";
                    status.Description = "GetHotelBooking method Failed. Technical Fault 01";
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, (int)member.ID, " B2C-Hotel-Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetHotelBooking Method Failed, due to BookingEngineException Exception", member, ex, "\n\n BookingId : " + request.BookingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                catch (ArgumentException ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "05";
                    status.Description = "Error returned from GetHotelBooking method : " + ex.Message;
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetHotelBooking Method Failed, due to ArgumentException Exception", member, ex, "\n\n BookingId : " + request.BookingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "06";
                    status.Description = "Technical Fault 02";
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetHotelBooking Method Failed, due to Exception", member, ex, "\n\n BookingId : " + request.BookingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                try
                {
                    WSHotelBookingDetail hBookingDetail = new WSHotelBookingDetail();
                    hBookingDetail = WSGetHotelBookingRequest.BuildGetBookingResponse(itineary, booking);
                    response.BookingDetail = hBookingDetail;
                    status.Category = "GB";
                    status.StatusCode = "01";
                    status.Description = "Successful";
                    response.Status = status;
                    Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, (int)member.ID, "GetHotelBooking Successful", "");
                }
                catch (BookingEngineException ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "07";
                    status.Description = "Technical Fault 03";
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetHotelBooking Method Failed, due to BookingEngineException Exception", member, ex, "\n\n BookingId : " + request.BookingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                catch (ArgumentException ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "08";
                    status.Description = "Error Returned while filling booking details.: " + ex.Message;
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetHotelBooking Method Failed, due to ArgumentException Exception", member, ex, "\n\n BookingId : " + request.BookingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    status.Category = "GB";
                    status.StatusCode = "09";
                    status.Description = "Technical Fault 04";
                    response.Status = status;
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                        SendErrorMail("GetHotelBooking Method Failed, due to Exception", member, ex, "\n\n BookingId : " + request.BookingId + " \n\n Status Description : " + response.Status.Description);
                    }
                    catch (Exception)
                    {
                        return response;
                    }
                    return response;
                }
                return response;
            }
        }
        else
        {
            WSGetHotelBookingResponse errorResponse = new WSGetHotelBookingResponse();
            WSStatus status = new WSStatus();
            status.Category = "GB";
            status.Description = "Authentication failed for book";
            status.StatusCode = "02";
            errorResponse.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.GetHotelBooking, Severity.High, 0, "B2C-Hotel-Login Failed for Member for GetHotelBooking Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSSendHotelChangeRequestResponse SendHotelChangeRequest(WSSendHotelChangeRequest request)
    {
        Trace.TraceInformation("HotelBookingAPI.SendHotelChangeRequest entered. BookingId : " + request.BookingId);
        UserMaster member = new UserMaster();
        member = WSValidate(Credential);
        if (member != null)
        {
            if (!(WSSendHotelChangeRequestInput(request)))
            {
                WSSendHotelChangeRequestResponse errorResponse = new WSSendHotelChangeRequestResponse();
                WSStatus status = new WSStatus();
                status.Category = "CR";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }
            else
            {
                WSSendHotelChangeRequestResponse response = new WSSendHotelChangeRequestResponse();
                response = WSSendHotelChangeRequest.SendChangeRequest(request, member);
                return response;
            }
        }
        else
        {
            WSSendHotelChangeRequestResponse errorResponse = new WSSendHotelChangeRequestResponse();
            WSStatus status = new WSStatus();
            status.Category = "CR";
            status.Description = "Authentication failed for SendHotelChangeRequest";
            status.StatusCode = "02";
            errorResponse.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.SendHotelChangeRequest, Severity.High, 0, "B2C-Hotel-Login Failed for Member for SendHotelChangeRequest Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetHotelChangeRequestStatusResponse GetHotelChangeRequestStatus(WSGetHotelChangeRequestStatusRequest request)
    {
        Trace.TraceInformation("HotelBookingAPI.GetHotelChangeRequestStatus entered. RequestId : " + request.ChangeRequestId);
        UserMaster member = new UserMaster();
        member = WSValidate(Credential);
        if (member != null)
        {
            if (!(WSGetHotelChangeRequestStatusInput(request)))
            {
                WSGetHotelChangeRequestStatusResponse errorResponse = new WSGetHotelChangeRequestStatusResponse();
                WSStatus status = new WSStatus();
                status.Category = "CRS";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                Audit.AddAuditHotelBookingAPI(EventType.GetHotelChangeRequestStatus, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }
            else
            {
                WSGetHotelChangeRequestStatusResponse response = new WSGetHotelChangeRequestStatusResponse();
                response = WSGetHotelChangeRequestStatusRequest.GetChangeRequestStatus(request, member);
                return response;
            }
        }
        else
        {
            WSGetHotelChangeRequestStatusResponse errorResponse = new WSGetHotelChangeRequestStatusResponse();
            WSStatus status = new WSStatus();
            status.Category = "CRS";
            status.Description = "Authentication failed for GetHotelChangeRequestStatus";
            status.StatusCode = "02";
            errorResponse.Status = status;
            Audit.AddAuditHotelBookingAPI(EventType.GetHotelChangeRequestStatus, Severity.High, 0, "B2C-Hotel-Login Failed for Member for GetHotelChangeRequestStatus Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return errorResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetCountryListResponse GetCountryList()
    {
        Trace.TraceInformation("HotelBookingAPI.GetCountryList entered.");
        UserMaster member = new UserMaster();
        member = WSValidate(Credential);
        WSGetCountryListResponse response = new WSGetCountryListResponse();
        response.Status = new WSStatus();
        response.CountryList = new List<string>();
        if (member != null)
        {
            try
            {
                GTACity GTACityInfo = new GTACity();
                response.CountryList = GTACityInfo.GetAllCountries();
                if (response.CountryList.Count <= 0)
                {
                    response.Status.Category = "CN";
                    response.Status.StatusCode = "03";
                    response.Status.Description = "No Country found.";
                    Audit.AddAuditHotelBookingAPI(EventType.GetCountryList, Severity.High, (int)member.ID, "B2C-Hotel-No Country found.", "");
                    return response;
                }
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "CN";
                response.Status.StatusCode = "04";
                response.Status.Description = "GetCountryList Failed. Technical Fault 01";
                Audit.AddAuditHotelBookingAPI(EventType.GetCountryList, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "CN";
                response.Status.StatusCode = "05";
                response.Status.Description = "Error Returned. Error Description: " + ex.Message;
                Audit.AddAuditHotelBookingAPI(EventType.GetCountryList, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "CN";
                response.Status.StatusCode = "06";
                response.Status.Description = "Technical Fault 02.";
                Audit.AddAuditHotelBookingAPI(EventType.GetCountryList, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            response.Status.Category = "CN";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.AddAuditHotelBookingAPI(EventType.GetCountryList, Severity.High, (int)member.ID, "B2C-Hotel-GetCountryList Successful", "");
            return response;
        }
        else
        {
            response.Status.Category = "CN";
            response.Status.StatusCode = "02";
            response.Status.Description = "login failed";
            Audit.AddAuditHotelBookingAPI(EventType.GetCountryList, Severity.High, 0, "B2C-Hotel-Login Failed for Member for GetCountryList Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return response;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetDestinationCityListResponse GetDestinationCityList(WSGetDestinationCityListRequest request)
    {
        Trace.TraceInformation("HotelBookingAPI.GetDestinationCityList entered.");
        UserMaster member = new UserMaster();
        member = WSValidate(Credential);
        WSGetDestinationCityListResponse response = new WSGetDestinationCityListResponse();
        response.Status = new WSStatus();
        response.CityList = new List<WSCity>();
        if (member != null)
        {
            //validating Request
            if (!(WSGetDestinationCityListInput(request)))
            {
                WSGetDestinationCityListResponse errorResponse = new WSGetDestinationCityListResponse();
                WSStatus status = new WSStatus();
                status.Category = "DCT";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                errorResponse.CityList = new List<WSCity>();
                Audit.AddAuditHotelBookingAPI(EventType.GetDestinationCityList, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect: " + statusDescription, "");
                return errorResponse;
            }

            try
            {
                WSCity city = new WSCity();
                response.CityList = city.GetCityListByCountryName(request.CountryName);
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "DCT";
                response.Status.StatusCode = "04";
                response.Status.Description = "GetDestinationCityList Failed, Technical Fault 01.";
                Audit.AddAuditHotelBookingAPI(EventType.GetDestinationCityList, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "DCT";
                response.Status.StatusCode = "05";
                response.Status.Description = "Error Returned. Error Description: " + ex.Message;
                Audit.AddAuditHotelBookingAPI(EventType.GetDestinationCityList, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "DCT";
                response.Status.StatusCode = "06";
                response.Status.Description = "Technical Fault 02.";
                Audit.AddAuditHotelBookingAPI(EventType.GetDestinationCityList, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            response.Status.Category = "DCT";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.AddAuditHotelBookingAPI(EventType.GetDestinationCityList, Severity.High, (int)member.ID, "B2C-Hotel-GetDestinationCityList Successful", "");
            return response;
        }
        else
        {
            response.Status.Category = "DCT";
            response.Status.StatusCode = "02";
            response.Status.Description = "login failed";
            Audit.AddAuditHotelBookingAPI(EventType.GetDestinationCityList, Severity.High, 0, "B2C-Hotel-Login Failed for Member for GetDestinationCityList Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return response;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetTopDestinationsResponse GetTopDestinations()
    {
        Trace.TraceInformation("HotelBookingAPI.GetTopDestinations entered.");
        UserMaster member = new UserMaster();
        member = WSValidate(Credential);
        WSGetTopDestinationsResponse response = new WSGetTopDestinationsResponse();
        response.Status = new WSStatus();
        response.CityList = new List<WSCity>();
        if (member != null)
        {
            try
            {
                WSCity city = new WSCity();
                response.CityList = city.GetTopDestinations();
                if (response.CityList.Count <= 0)
                {
                    response.Status.Category = "GTD";
                    response.Status.StatusCode = "03";
                    response.Status.Description = "No City found.";
                    Audit.AddAuditHotelBookingAPI(EventType.GetTopDestinations, Severity.High, (int)member.ID, "B2C-Hotel-No City found.", "");
                    return response;
                }
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "GTD";
                response.Status.StatusCode = "04";
                response.Status.Description = "GetTopDestinations Failed. Technical Fault 01";
                Audit.AddAuditHotelBookingAPI(EventType.GetTopDestinations, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "GTD";
                response.Status.StatusCode = "05";
                response.Status.Description = "Error Returned. Error Description: " + ex.Message;
                Audit.AddAuditHotelBookingAPI(EventType.GetTopDestinations, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "GTD";
                response.Status.StatusCode = "06";
                response.Status.Description = "Technical Fault 02.";
                Audit.AddAuditHotelBookingAPI(EventType.GetTopDestinations, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            response.Status.Category = "GTD";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.AddAuditHotelBookingAPI(EventType.GetTopDestinations, Severity.High, (int)member.ID, "B2C-Hotel-GetTopDestinations Successful", "");
            return response;
        }
        else
        {
            response.Status.Category = "GTD";
            response.Status.StatusCode = "02";
            response.Status.Description = "login failed";
            Audit.AddAuditHotelBookingAPI(EventType.GetDestinationCityList, Severity.High, 0, "B2C-Hotel-Login Failed for Member for GetTopDestinations Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return response;
        }
    }

    //[WebMethod]
    //[SoapHeader("Credential")]
    //public WSAddPendingHotelBookingResponse AddPendingHotelBookingDetail(WSAddPendingHotelBookingRequest saveRequest)
    //{
    //    Member member = new Member();
    //    WSAddPendingHotelBookingResponse saveResponse = new WSAddPendingHotelBookingResponse();
    //    WSStatus resStatus = new WSStatus();
    //    member = WSValidate(Credential);
    //    string ipAddr = this.Context.Request.UserHostAddress;
    //    if (member != null)
    //    {
    //        HotelSearchResult[] mseResults;
    //        HotelSearchResult hotelResult;
    //        mseResults = (HotelSearchResult[])HttpContext.Current.Cache[saveRequest.SessionId + "-ResultList"];
    //        if (mseResults != null && mseResults.Length > 0)
    //        {
    //            hotelResult = WSGetCancellationPolicyRequest.GetSelectedSearchResult(saveRequest.HotelIndex, mseResults);
    //        }
    //        else
    //        {
    //            throw new ArgumentException("Hotelresult not loaded from Cache, This may be because cache data has expired");
    //        }
    //        HotelBookingSource source = hotelResult.BookingSource;
    //        ConfigurationSystem con = new ConfigurationSystem();
    //        Hashtable hostPort = con.GetHostPort();
    //        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
    //        string fromEmailId = hostPort["fromEmail"].ToString();
    //        try
    //        {
    //            saveResponse = saveRequest.Add(member.AgencyId, member.AgencyId, source);
    //        }
    //        catch (Exception ex)
    //        {
    //            saveResponse.Status = new WSStatus();
    //            saveResponse.Status.Category = "SPHD";
    //            saveResponse.Status.Description = "Details Not Saved";
    //            saveResponse.Status.StatusCode = "4";
    //            saveResponse.HotelQueueId = "0";
    //            Audit.AddAuditHotelBookingAPI(EventType.AddPendingHotelBookingDetail, Severity.High, member.AgencyId, "Add Pending Hotel Booking Details failed: " + ex.Message + " Stack Trace = " + ex.StackTrace, ipAddr);
    //            SendErrorMail("while saving Pending Hotel booking", member, ex, "Error returned while saving Pending Hotel booking, Error Description : ");
    //            return saveResponse;
    //        }
    //        return saveResponse;
    //    }
    //    else
    //    {
    //        resStatus.Category = "SPHD";
    //        resStatus.Description = "Validation Failed";
    //        resStatus.StatusCode = "3";
    //        saveResponse.Status = resStatus;
    //        Audit.AddAuditHotelBookingAPI(EventType.AddPendingHotelBookingDetail, Severity.High, 0, " AddPending Hotel Booking failed and saveResponse.Status.Description=" + saveResponse.Status.Description, ipAddr);
    //        return saveResponse;
    //    }
    //}

    //[WebMethod]
    //[SoapHeader("Credential")]
    //public WSUpdatePendingHotelBookingResponse UpdatePendingHotelBookingDetail(WSUpdatePendingHotelBookingRequest updateRequest)
    //{
    //    Member member = new Member();
    //    WSUpdatePendingHotelBookingResponse updateResponse = new WSUpdatePendingHotelBookingResponse();
    //    WSStatus resStatus = new WSStatus();
    //    member = WSValidate(Credential);
    //    string ipAddr = this.Context.Request.UserHostAddress;
    //    if (member != null)
    //    {
    //        ConfigurationSystem con = new ConfigurationSystem();
    //        Hashtable hostPort = con.GetHostPort();
    //        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
    //        string fromEmailId = hostPort["fromEmail"].ToString();
    //        try
    //        {
    //            updateResponse = updateRequest.Update(member.AgencyId);
    //        }
    //        catch (Exception ex)
    //        {
    //            updateResponse.Status = new WSStatus();
    //            updateResponse.Status.Category = "UPHD";
    //            updateResponse.Status.Description = "Details Not Updated";
    //            updateResponse.Status.StatusCode = "4";
    //            updateResponse.HotelQueueId = "0";
    //            Audit.AddAuditHotelBookingAPI(EventType.UpdatePendingHotelBookingDetail, Severity.High, member.AgencyId, "Update Pending Hotel Booking Details failed: " + ex.Message + " Stack Trace = " + ex.StackTrace, ipAddr);
    //            SendErrorMail("while updating Pending booking", member, ex, "Error returned while updating Pending Hotel booking, Error Description : ");
    //            return updateResponse;
    //        }
    //        return updateResponse;
    //    }
    //    else
    //    {
    //        resStatus.Category = "UPHD";
    //        resStatus.Description = "Validation Failed";
    //        resStatus.StatusCode = "3";
    //        updateResponse.Status = resStatus;
    //        Audit.AddAuditHotelBookingAPI(EventType.UpdatePendingHotelBookingDetail, Severity.High, 0, " Update Pending Hotel Booking failed and updateResponse.Status.Description=" + updateResponse.Status.Description, ipAddr);
    //        return updateResponse;
    //    }
    //}

    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetStatusCodeListResponse GetStatusCodeList(WSGetStatusCodeListRequest request)
    {
        UserMaster member = new UserMaster();
        WSGetStatusCodeListResponse response = new WSGetStatusCodeListResponse();
        WSStatus resStatus = new WSStatus();
        member = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (member != null)
        {
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            try
            {
                string states = HotelCity.GetIANStateList(request.CountryCode);
                response.StateList = states;
            }
            catch (Exception ex)
            {
                response.Status = new WSStatus();
                response.Status.Category = "GSL";
                response.Status.Description = "List Not Loaded";
                response.Status.StatusCode = "4";
                Audit.AddAuditHotelBookingAPI(EventType.GetStateListForIAN, Severity.High, (int)member.ID, "B2C-Hotel-Get States List failed: " + ex.Message + " Stack Trace = " + ex.StackTrace, ipAddr);
                SendErrorMail("while getting state list", member, ex, "Error returned while getting state list, Error Description : ");
                return response;
            }
            return response;
        }
        else
        {
            resStatus.Category = "UPHD";
            resStatus.Description = "Validation Failed";
            resStatus.StatusCode = "3";
            response.Status = resStatus;
            Audit.AddAuditHotelBookingAPI(EventType.GetStateListForIAN, Severity.High, 0, "B2C-Hotel-Get States List failed and updateResponse.Status.Description=" + response.Status.Description, ipAddr);
            return response;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSAddHotelBookingResponse AddHotelBookingDetail(WSAddHotelBookingRequest saveRequest)
    {

        //Trace.TraceInformation("CozmoB2C.AddHotelBookingDetail entered. PaymentId=" + saveRequest.PaymentId); //commented by brahmam code optimization
        UserMaster member = new UserMaster();
        WSAddHotelBookingResponse saveResponse = new WSAddHotelBookingResponse();
        WSStatus resStatus = new WSStatus();
        ApiCustomerType customerType;
        string authValue = string.Empty;
        member = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (member != null)
        {
            HotelSearchResult[] mseResults;
            HotelSearchResult hotelResult;
            mseResults = (HotelSearchResult[])HttpContext.Current.Cache[saveRequest.SessionId + "-ResultList"];
            if (mseResults != null && mseResults.Length > 0)
            {
                hotelResult = WSGetCancellationPolicyRequest.GetSelectedSearchResult(saveRequest.HotelIndex, mseResults);
            }
            else
            {
                throw new ArgumentException("Hotelresult not loaded from Cache, This may be because cache data has expired");
            }

            HotelBookingSource source = hotelResult.BookingSource;
            if (Credential.SiteName != null && Credential.SiteName != "")
            {
                customerType = ApiCustomerType.WhiteLabel;
                authValue = Credential.SiteName;
            }
            else if (Credential.AccountCode != null && Credential.AccountCode != "")
            {
                customerType = ApiCustomerType.B2B2B;
                authValue = Credential.AccountCode;
            }
            else
            {
                customerType = ApiCustomerType.BookingApi;
                authValue = Credential.UserName;
            }
            if (source == HotelBookingSource.IAN)
            {
                saveRequest.PaymentId = "";
                saveRequest.PaySource = CT.BookingEngine.PaymentGatewaySource.IANSource;
            }
            saveRequest.Source = (WLHotelBookingSource)Enum.Parse(typeof(WLHotelBookingSource), source.ToString()); //Commented by brahmam because in WLHotelBookingSource t
            saveRequest.AgencyId = member.AgentId;
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            try
            {
                saveResponse = saveRequest.Add(authValue, customerType,(int)member.ID);
            }
            catch (Exception ex)
            {
                Audit.AddAuditHotelBookingAPI(EventType.AddHotelAPIBookingDetail, Severity.High, (int)member.ID, "B2C-Hotel-Add Hotel Trip Details failed: " + ex.Message + " Stack Trace = " + ex.StackTrace, ipAddr);
                SendErrorMail("while saving Hotel booking", member, ex, " Hotel Booking Save Failed. Response Status Description : " + saveResponse.Status.Description);
            }
            return saveResponse;
        }
        else
        {
            resStatus.Category = "SHD";
            resStatus.Description = "Validation Failed";
            resStatus.StatusCode = "2";
            saveResponse.Status = resStatus;
            Audit.AddAuditHotelBookingAPI(EventType.AddHotelAPIBookingDetail, Severity.High, 0, "B2C-Hotel-saveRequest.Add Hotel Booking failed and saveResponse.Status.Description=" + saveResponse.Status.Description, ipAddr);
            return saveResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSAddPendingHotelBookingResponse AddPendingHotelBookingDetail(WSAddPendingHotelBookingRequest saveRequest)
    {
        UserMaster member = new UserMaster();
        WSAddPendingHotelBookingResponse saveResponse = new WSAddPendingHotelBookingResponse();
        WSStatus resStatus = new WSStatus();
        member = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (member != null)
        {
            HotelSearchResult[] mseResults;
            HotelSearchResult hotelResult;
            mseResults = (HotelSearchResult[])HttpContext.Current.Cache[saveRequest.SessionId + "-ResultList"];
            if (mseResults != null && mseResults.Length > 0)
            {
                hotelResult = WSGetCancellationPolicyRequest.GetSelectedSearchResult(saveRequest.HotelIndex, mseResults);
            }
            else
            {
                throw new ArgumentException("Hotelresult not loaded from Cache, This may be because cache data has expired");
            }
            HotelBookingSource source = hotelResult.BookingSource;
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            try
            {
                saveResponse = saveRequest.Add((int)member.ID, member.AgentId, source);
            }
            catch (Exception ex)
            {
                saveResponse.Status = new WSStatus();
                saveResponse.Status.Category = "SPHD";
                saveResponse.Status.Description = "Details Not Saved";
                saveResponse.Status.StatusCode = "4";
                saveResponse.HotelQueueId = "0";
                Audit.AddAuditHotelBookingAPI(EventType.AddPendingHotelBookingDetail, Severity.High, (int)member.ID, "B2C-Hotel-Add Pending Hotel Booking Details failed: " + ex.Message + " Stack Trace = " + ex.StackTrace, ipAddr);
                SendErrorMail("while saving Pending Hotel booking", member, ex, "Error returned while saving Pending Hotel booking, Error Description : ");
                return saveResponse;
            }
            return saveResponse;
        }
        else
        {
            resStatus.Category = "SPHD";
            resStatus.Description = "Validation Failed";
            resStatus.StatusCode = "3";
            saveResponse.Status = resStatus;
            Audit.AddAuditHotelBookingAPI(EventType.AddPendingHotelBookingDetail, Severity.High, 0, "B2C-Hotel- AddPending Hotel Booking failed and saveResponse.Status.Description=" + saveResponse.Status.Description, ipAddr);
            return saveResponse;
        }
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public WSUpdatePendingHotelBookingResponse UpdatePendingHotelBookingDetail(WSUpdatePendingHotelBookingRequest updateRequest)
    {
        UserMaster member = new UserMaster();
        WSUpdatePendingHotelBookingResponse updateResponse = new WSUpdatePendingHotelBookingResponse();
        WSStatus resStatus = new WSStatus();
        member = WSValidate(Credential);
        string ipAddr = this.Context.Request.UserHostAddress;
        if (member != null)
        {
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
            string fromEmailId = hostPort["fromEmail"].ToString();
            try
            {
                updateResponse = updateRequest.Update((int)member.ID);
            }
            catch (Exception ex)
            {
                updateResponse.Status = new WSStatus();
                updateResponse.Status.Category = "UPHD";
                updateResponse.Status.Description = "Details Not Updated";
                updateResponse.Status.StatusCode = "4";
                updateResponse.HotelQueueId = "0";
                Audit.AddAuditHotelBookingAPI(EventType.UpdatePendingHotelBookingDetail, Severity.High, (int)member.ID, "Update Pending Hotel Booking Details failed: " + ex.Message + " Stack Trace = " + ex.StackTrace, ipAddr);
                SendErrorMail("while updating Pending booking", member, ex, "B2C-Hotel-Error returned while updating Pending Hotel booking, Error Description : ");
                return updateResponse;
            }
            return updateResponse;
        }
        else
        {
            resStatus.Category = "UPHD";
            resStatus.Description = "Validation Failed";
            resStatus.StatusCode = "3";
            updateResponse.Status = resStatus;
            Audit.AddAuditHotelBookingAPI(EventType.UpdatePendingHotelBookingDetail, Severity.High, 0, "B2C-Hotel- Update Pending Hotel Booking failed and updateResponse.Status.Description=" + updateResponse.Status.Description, ipAddr);
            return updateResponse;
        }
    }


    [WebMethod]
    [SoapHeader("Credential")]
    public WSGetActiveHotelDealsResponse GetActiveHotelDeals()
    {
        Trace.TraceInformation("HotelBookingAPI.GetActiveHotelDeals entered.");
        UserMaster member = new UserMaster();
        member = WSValidate(Credential);
        WSGetActiveHotelDealsResponse response = new WSGetActiveHotelDealsResponse();
        response.Status = new WSStatus();
        response.HotelDeals = new List<WSHotelDeal>();
        if (member != null)
        {
            try
            {
                string whereString = " where isActive=1";
                HotelDeal hotelDeal = new HotelDeal();
                List<HotelDeal> hotelDeals = new List<HotelDeal>();
                int dealsCount = hotelDeal.GetManageDealCount(whereString, "");
                hotelDeals = hotelDeal.GetAllHotelDeals(1, dealsCount, dealsCount, whereString, "");
                response.HotelDeals = LoadHotelDeals(hotelDeals);
                if (response.HotelDeals.Count <= 0)
                {
                    response.Status.Category = "GHD";
                    response.Status.StatusCode = "03";
                    response.Status.Description = "No Deal found.";
                    Audit.AddAuditHotelBookingAPI(EventType.GetActiveHotelDeals, Severity.High, (int)member.ID, "B2C-Hotel-No City found.", "");
                    return response;
                }
            }
            catch (BookingEngineException ex)
            {
                response.Status.Category = "GHD";
                response.Status.StatusCode = "04";
                response.Status.Description = "GetActiveHotelDeals Failed. Technical Fault 01";
                Audit.AddAuditHotelBookingAPI(EventType.GetActiveHotelDeals, Severity.High, (int)member.ID, " B2C-Hotel-Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            catch (ArgumentException ex)
            {
                response.Status.Category = "GHD";
                response.Status.StatusCode = "05";
                response.Status.Description = "Error Returned. Error Description: " + ex.Message;
                Audit.AddAuditHotelBookingAPI(EventType.GetActiveHotelDeals, Severity.High, (int)member.ID, "B2C-Hotel- Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            catch (Exception ex)
            {
                response.Status.Category = "GHD";
                response.Status.StatusCode = "06";
                response.Status.Description = "Technical Fault 02.";
                Audit.AddAuditHotelBookingAPI(EventType.GetActiveHotelDeals, Severity.High, (int)member.ID, " B2C-Hotel-Error: " + ex.Message + " Stack Trace : " + ex.StackTrace, "");
                return response;
            }
            response.Status.Category = "GHD";
            response.Status.StatusCode = "01";
            response.Status.Description = "Successful";
            Audit.AddAuditHotelBookingAPI(EventType.GetActiveHotelDeals, Severity.High, (int)member.ID, "B2C-Hotel-GetActiveHotelDeals Successful", "");
            return response;
        }
        else
        {
            response.Status.Category = "GHD";
            response.Status.StatusCode = "02";
            response.Status.Description = "login failed";
            Audit.AddAuditHotelBookingAPI(EventType.GetActiveHotelDeals, Severity.High, 0, "B2C-Hotel-Login Failed for Member for GetActiveHotelDeals Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return response;
        }
    }
    #endregion

    #region Private Method

    private static UserMaster WSValidate(AuthenticationData credential)
    {
        UserMaster member = new UserMaster();
        HotelBookingAPI bApi = new HotelBookingAPI();
        if (credential.SiteName != null && credential.SiteName != "")
        {
            //string ipAddr = this.Context.Request.UserHostAddress;
            try
            {
                member = bApi.GetMemberBySiteName(credential.SiteName);
                //Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.Low, member.AgencyId, "Credential Succeed for site " + credential.SiteName, "");
            }
            catch (Exception ex)
            {
                Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.High, 0, "B2C-Hotel-Credential Failed " + credential.SiteName + " Site is not configure for whitelabel " + ex.Message, "");
                return null;
            }
        }
        else if (credential.AccountCode != null && credential.AccountCode != "")
        {
            try
            {
                member = bApi.GetMemberByAccountCode(credential.AccountCode);
            }
            catch (Exception ex)
            {
                Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.High, 0, "B2C-Hotel-Credential Failed AccountCode " + credential.AccountCode + " is not found " + ex.Message, "");
                return null;
            }

        }
        else
        {
            try
            {
                //commented by brahmam
               // member = UserMaster.Authenticate(credential.UserName, credential.Password);
            }
            catch (Exception ex)
            {
                Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.High, 0, "B2C-Hotel-Member.Authenticate Failed for User :" + credential.UserName + "  " + ex.Message, "0");
            }
            if (member == null)
            {
                Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.High, 0, "B2C-Hotel-Credential Failed for username " + credential.UserName, "0");
                return null;
            }
            else
            {
                Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.Low, (int)member.ID, "B2C-Hotel-Credential Succeed for username " + credential.UserName, "0");
            }
        }
        return member;
    }
    /// <summary>
    /// To maintain accounts in case of ticketing
    /// </summary>
    /// <param name="source"></param>
    /// <param name="trackId"></param>
    /// <param name="amount"></param>
    /// <param name="agencyId"></param>
    /// <param name="memberId"></param>
    /// <param name="isLCC"></param>
    /// <returns></returns>
    private int AcceptPayment(WSPaymentGatewaySource source, string trackId, decimal amount, int agencyId, int memberId, WSGuest guest, bool isLCC)
    {

        PaymentDetails pd = new PaymentDetails();
        decimal creditCardCharge = (decimal)AccountsDetail.GetCreditCardCharge((CT.AccountingEngine.PaymentGatewaySource)source);
        decimal amountToBeCredited = Math.Round((amount * 100) / (creditCardCharge + 100));
        decimal creditCardChargedAmount = amount;

        try
        {
            //int agencyTypeId = Agency.GetAgencyTypeId(agencyId); commented by brahmam we have directly agenttype is there 
            AgentMaster agent=new AgentMaster(agencyId);
            int agencyTypeId = agent.AgentType;

            #region Making PaymentDetail object

            pd.Amount = amountToBeCredited;
            pd.RemainingAmount = amountToBeCredited;
            pd.OurBankDetailId = AccountsDetail.GetOurBankDetailIdBySource((int)source);
            //Commented by brahmam
            //BankAccount bd = new BankAccount();
            //bd.Load(pd.OurBankDetailId);
            //pd.BankName = bd.BankName;
            //pd.Branch = bd.BranchName;
            pd.ModeOfPayment = CT.AccountingEngine.PaymentMode.CreditCard;
            pd.Invoices = new List<InvoiceCollection>();
            pd.Status = CT.AccountingEngine.PaymentStatus.Accepted;
            pd.CreatedBy = memberId;
            pd.LastModifiedBy = memberId;
            pd.AgencyId = agencyId;
            pd.Remarks = "Pay By Credit Card. TrackId: " + trackId + ", Lead Guest Name: " + guest.FirstName + " " + guest.LastName;
            if (agencyTypeId == 1)
            {
                pd.IsLCC = true;
            }
            else
            {
                pd.IsLCC = isLCC;
            }
            pd.PaymentDate = DateTime.Now;


            #endregion

            #region Making LedgerTransaction object

            LedgerTransaction ledgerTxn = new LedgerTransaction();
            //Commented by brahmam
            //Ledger ledger = new Ledger();
            //ledger.Load(agencyId);
            //ledgerTxn.LedgerId = ledger.LedgerId;
            ledgerTxn.ReferenceType = ReferenceType.PaymentRecieved;
            ledgerTxn.Amount = Convert.ToDecimal(pd.Amount);
            if (agencyTypeId == 1)
            {
                ledgerTxn.IsLCC = true;
            }
            else
            {
                ledgerTxn.IsLCC = isLCC;
            }

            NarrationBuilder objNarration = new NarrationBuilder();

            if (pd.Remarks.Trim().Length > 0)
            {
                objNarration.Remarks = pd.Remarks;
            }
            else
            {
                objNarration.Remarks = "Payment Done";
            }
            ledgerTxn.Narration = objNarration;
            ledgerTxn.Notes = "Payment Done";
            ledgerTxn.Date = DateTime.UtcNow;
            ledgerTxn.CreatedBy = memberId;

            #endregion


            //commented by brahmam
            //#region Saving PaymentDetail and LedgerTransaction objects and updating agency balance

            //using (TransactionScope updateTransaction = new TransactionScope())
            //{
            //    pd.Save();
            //    ledgerTxn.ReferenceId = pd.PaymentDetailId;
            //    ledgerTxn.Save();
            //    if (!isLCC)
            //    {
            //        if (agencyTypeId != 1)
            //        {
            //            Ledger.UpdateCurrentBalance(pd.AgencyId, pd.Amount, memberId);
            //        }
            //        else
            //        {
            //            AgentMaster.UpdateLCCBalance(pd.AgencyId, pd.Amount, memberId);
            //        }
            //    }
            //    else
            //    {
            //        AgentMaster.UpdateLCCBalance(pd.AgencyId, pd.Amount, memberId);
            //    }
            //    updateTransaction.Complete();
            //}

            //#endregion

            string filePathForLogging = System.Configuration.ConfigurationManager.AppSettings["PaymentLog"];

            string detail = "Payment is done for track Id : " + trackId + " for agencyId: " + agencyId + " of amount= " + amountToBeCredited + " (" + creditCardChargedAmount + "Charged from Credit Card) on " + DateTime.Now + "\n";

            System.IO.File.AppendAllText(filePathForLogging, detail);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Account, Severity.High, memberId, "B2C-Hotel-Error during recording entry in paymentDetail and ledger transaction fail of agency: " + agencyId + " for amount = " + amount + ", due to: " + ex.Message + ex.StackTrace, "");
        }
        return pd.PaymentDetailId;
    }

    private bool WSSearchInput(WSHotelSearchRequest request)
    {
        DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
        if (request.CheckInDate < currentDate)
        {
            statusDescription = "Invalid checkIn date " + request.CheckInDate.ToString() + ". CheckIn date must be greater then today date.";
            statusCode = "03";
            return false;
        }
        if (request.CheckInDate > request.CheckOutDate)
        {
            statusDescription = "Invalid dated entered. CheckIn date " + request.CheckInDate.ToString() + " should be less then CheckOut date " + request.CheckOutDate.ToString() + ".";
            statusCode = "04";
            return false;
        }
        if (request.CityReference == null || (request.CityReference != null && request.CityReference == ""))
        {
            statusDescription = "City Name should not be null or blank.";
            statusCode = "05";
            return false;
        }
        if (request.IsDomestic.ToString() == "")
        {
            statusDescription = "IsDomestic value should not be empty.";
            statusCode = "06";
            return false;
        }
        if (request.NoOfRooms <= 0)
        {
            statusDescription = "You should request for at least one room.";
            statusCode = "07";
            return false;
        }
        if (request.RoomGuest == null || request.RoomGuest.Length <= 0)
        {
            statusDescription = "RoomGuest array should not be null or of length zero.";
            statusCode = "08";
            return false;
        }
        else if (request.RoomGuest != null && request.RoomGuest.Length > 0)
        {
            for (int i = 0; i < request.RoomGuest.Length; i++)
            {
                if (request.RoomGuest[i].NoOfAdults <= 0)
                {
                    statusDescription = "There should be atleast one adult in each room.";
                    statusCode = "09";
                    return false;
                }
                if (request.RoomGuest[i].NoOfChild > 0)
                {
                    if (request.RoomGuest[i].ChildAge == null)
                    {
                        statusDescription = "Child age should not be null when child count is greater then one.";
                        statusCode = "10";
                        return false;
                    }
                    else if (request.RoomGuest[i].ChildAge != null && request.RoomGuest[i].ChildAge.Count != request.RoomGuest[i].NoOfChild)
                    {
                        statusDescription = "Every child age should be entered.";
                        statusCode = "11";
                        return false;
                    }
                    else if (request.RoomGuest[i].ChildAge != null)
                    {
                        for (int j = 0; j < request.RoomGuest[i].NoOfChild; j++)
                        {
                            if (request.RoomGuest[i].ChildAge[j] > 18)
                            {
                                statusDescription = "Child age should not be greater then 18 years.";
                                statusCode = "12";
                                return false;
                            }
                        }
                    }
                }
            }
        }
        if (!request.IsDomestic && request.CityId <= 0)
        {
            statusDescription = "We need city id for international hotels as provided in the city list";
            statusCode = "07";
            return false;
        }
        return true;
    }

    private bool WSGetHotelDetailsInput(WSGetHotelDetailRequest request)
    {
        if (request.Index <= 0)
        {
            statusDescription = "Index value should not be Zero or less.";
            statusCode = "03";
            return false;
        }
        else if (request.SessionId == null || request.SessionId == "")
        {
            statusDescription = "SessionId should not be null.";
            statusCode = "04";
            return false;
        }
        return true;
    }

    private bool WSGetCancellationPolicyInput(WSGetCancellationPolicyRequest request)
    {
        if (request.Index <= 0)
        {
            statusDescription = "Index value should not be Zero or less.";
            statusCode = "03";
            return false;
        }
        else if (request.SessionId == null || request.SessionId == "")
        {
            statusDescription = "SessionId can not be null or empty String.";
            statusCode = "04";
            return false;
        }
        else if (request.RatePlanCode == null || request.RatePlanCode == "")
        {
            statusDescription = "RatePlanCode can not be null or empty String.";
            statusCode = "05";
            return false;
        }
        else if (request.RoomTypeCode == null || request.RoomTypeCode == "")
        {
            statusDescription = "RoomTypeCode can not be null or empty String.";
            statusCode = "06";
            return false;
        }
        else if (request.NoOfRooms <= 0)
        {
            statusDescription = "NoOfRooms field should be greater then or equal to one.";
            statusCode = "07";
            return false;
        }
        return true;
    }

    private bool WSBookInput(WSBookRequest request, HotelSearchResult searchResult)
    {
        try
        {
            //#region child age validation at the time of booking
            //int[] childAge=new int[5];
            //int j = 0;
            //for (int i = 0; i < request.Guest.Length; i++)
            //{                
            //    if (request.Guest[i].GuestType == WSHotelGuestType.Child)
            //    {
            //        childAge[j] = request.Guest[i].Age;
            //        j++;
            //    }
            //}
            //j = 0;
            //for (int i = 0; i < searchResult.RoomGuest.Length; i++)
            //{
            //    for (int k = 0; k < searchResult.RoomGuest[i].noOfChild; k++)
            //    {
            //        if (searchResult.RoomGuest[i].childAge[k]!=childAge[j])
            //        {
            //            statusDescription = "Child Age must be same as the time of searching";
            //            return false;
            //        }
            //        j++;
            //    }
            //}
            //#endregion
            int totalGuestCount = 0;
            for (int i = 0; i < searchResult.RoomGuest.Length; i++)
            {
                totalGuestCount += searchResult.RoomGuest[i].noOfAdults + searchResult.RoomGuest[i].noOfChild;
            }
            if (request.SessionId == null || request.SessionId == "")
            {
                statusDescription = "Session Id cannot be blank or null";
                statusCode = "03";
                return false;
            }
            //if(request.Guest.Length != totalGuestCount)
            //{
            //    statusDescription = "All room guests are not provided.";
            //    statusCode = "56";
            //    return false;
            //}
            if (request.Index <= 0)
            {
                statusDescription = "Index value should not be Zero or less.";
                statusCode = "04";
                return false;
            }
            if (request.NoOfRooms <= 0)
            {
                statusDescription = "Number of rooms should be greater then Zero";
                statusCode = "05";
                return false;
            }
            if (request.RoomCodes == null)
            {
                statusDescription = "RoomType Code cannot be left blank or null";
                statusCode = "06";
                return false;
            }
            if (request.Guest == null || request.Guest.Length <= 0)
            {
                statusDescription = "At lease one Guest information should be filled";
                statusCode = "07";
                return false;
            }
            else
            {
                Regex regex = new Regex(@"^[aA-zZ. ]*$");
                Regex emailRegex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                Regex numericRegex = new Regex(@"^[0-9]*$");
                Regex countryCodeRegex = new Regex(@"^[0-9+]*$");
                int leadGuestCount = 0;
                int currentRoomIndex = 0;
                bool isRoomLeadPassenger = false;
                for (int i = 0; i < request.Guest.Length; i++)
                {
                    if (request.Guest[i].RoomIndex == currentRoomIndex && !isRoomLeadPassenger && request.Guest[i].LeadGuest == true)
                    {
                        isRoomLeadPassenger = true;
                        currentRoomIndex += 1;
                    }
                    else
                    {
                        isRoomLeadPassenger = false;
                    }
                    if (request.Guest[i].LeadGuest.ToString().Trim() == "")
                    {
                        statusDescription = "Guest Type must be entered correct.";
                        statusCode = "08";
                        return false;
                    }
                    if (request.Guest[i].RoomIndex < 0)
                    {
                        statusDescription = "RoomIndex should be entered correct.";
                        statusCode = "09";
                        return false;
                    }
                    else if (request.Guest[i].GuestType == WSHotelGuestType.Child && (request.Guest[i].Age > 18))
                    {
                        statusDescription = "Child age should not be greater then eighteen years.";
                        statusCode = "16";
                        return false;
                    }
                    if (request.Guest[i].LeadGuest)
                    {
                        leadGuestCount++;
                    }
                    if (leadGuestCount > 1)
                    {
                        statusDescription = "Lead Guest count should only be one.";
                        statusCode = "10";
                        return false;
                    }
                    if (isRoomLeadPassenger && !(searchResult.BookingSource == HotelBookingSource.Desiya && i > 0))
                    {
                        if (request.Guest[i].FirstName == null || request.Guest[i].FirstName.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": First name should not be null or empty string.";
                            statusCode = "11";
                            return false;
                        }
                        else if (request.Guest[i].LastName == null || request.Guest[i].LastName.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Last name should not be null or empty string.";
                            statusCode = "12";
                            return false;
                        }
                        else if (!(regex.IsMatch(request.Guest[i].FirstName.Trim()) && regex.IsMatch(request.Guest[i].LastName.Trim())))
                        {
                            statusDescription = "Guest No " + (i + 1) + ": either first name or last name is not valid.";
                            statusCode = "13";
                            return false;
                        }
                        else if (request.Guest[i].FirstName.Trim().Length > 10 && searchResult.BookingSource == HotelBookingSource.IAN)
                        {
                            statusDescription = "Guest No " + (i + 1) + ": First name should be less than 11 characters.";
                            statusCode = "11";
                            return false;
                        }
                        else if (request.Guest[i].LastName.Trim().Length > 10 && searchResult.BookingSource == HotelBookingSource.IAN)
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Last name  should be less than 11 characters.";
                            statusCode = "12";
                            return false;
                        }
                        if ((request.Guest[i].GuestType == WSHotelGuestType.Child))
                        {
                            statusDescription = "Child should not be lead Guest of a room.";
                            statusCode = "14";
                            return false;
                        }
                    }
                    if (request.Guest[i].LeadGuest)
                    {
                        if (request.Guest[i].Email == null || request.Guest[i].Email.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Email Id should not be null or empty string.";
                            statusCode = "17";
                            return false;
                        }
                        else if (!emailRegex.IsMatch(request.Guest[i].Email.Trim()))
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Email Id entered is in invalid format.";
                            statusCode = "18";
                            return false;
                        }
                        else if (request.Guest[i].Countrycode == null || request.Guest[i].Countrycode.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Countrycode should not be null or empty string.";
                            statusCode = "19";
                            return false;
                        }
                        else if (!countryCodeRegex.IsMatch(request.Guest[i].Countrycode.Trim()))
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Country code should take only numeric values.";
                            statusCode = "20";
                            return false;
                        }
                        else if (request.Guest[i].Areacode == null || request.Guest[i].Areacode.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Area Code should not be null or empty string.";
                            statusCode = "21";
                            return false;
                        }
                        //else if (!numericRegex.IsMatch(request.Guest[i].Areacode.Trim()))
                        //{
                        //    statusDescription = "Guest No " + (i + 1) + ": Area code should take only numeric values.";
                        //    statusCode = "22";
                        //    return false;
                        //}
                        else if (request.Guest[i].Phoneno == null || request.Guest[i].Phoneno.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Phone number should not be null or empty string.";
                            statusCode = "23";
                            return false;
                        }
                        else if (!numericRegex.IsMatch(request.Guest[i].Phoneno.Trim()))
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Phone number should take only numeric values.";
                            statusCode = "24";
                            return false;
                        }
                        else if (request.Guest[i].Addressline1 == null || request.Guest[i].Addressline1.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": Address Line one should not be empty or null.";
                            statusCode = "25";
                            return false;
                        }
                        else if (request.Guest[i].City == null || request.Guest[i].City.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": City should not be null or empty string.";
                            statusCode = "26";
                            return false;
                        }
                        else if (request.Guest[i].State == null || request.Guest[i].State.Trim() == "")
                        {
                            statusDescription = "Guest No " + (i + 1) + ": State should not be null or empty string.";
                            statusCode = "28";
                            return false;
                        }
                        else if (request.Guest[i].Zipcode != null && request.Guest[i].Zipcode.Length > 0)
                        {
                            if (!numericRegex.IsMatch(request.Guest[i].Zipcode.Trim()))
                            {
                                statusDescription = "Guest No " + (i + 1) + ": Zipcode should have only numenic values.";
                                statusCode = "29";
                                return false;
                            }
                        }
                    }
                }
            }
            if (searchResult.BookingSource != HotelBookingSource.IAN)
            {
                if (request.PaymentInfo == null)
                {
                    statusDescription = "Payment information cannot be left null";
                    statusCode = "30";
                    return false;
                }
                else if (request.PaymentInfo.PaymentModeType.ToString().Trim() == "" || request.PaymentInfo.PaymentModeType.ToString().Trim() == "0")
                {
                    statusDescription = "PaymentModeType cannot be empty string or of value Zero.";
                    statusCode = "31";
                    return false;
                }
                else if (request.PaymentInfo.PaymentModeType == WSPaymentModeType.CreditCard)
                {
                    if (request.PaymentInfo.PaymentId == null || request.PaymentInfo.PaymentId.Trim() == "")
                    {
                        statusDescription = "PaymentId should not be empty string or null";
                        statusCode = "32";
                        return false;
                    }
                    if (request.PaymentInfo.Amount <= 0)
                    {
                        statusDescription = "Payment Amount should not be less then or equal to zero";
                        statusCode = "33";
                        return false;
                    }
                    if (request.PaymentInfo.PaymentGateway.ToString().Trim() == "" || request.PaymentInfo.PaymentGateway.ToString().Trim() == "0")
                    {
                        statusDescription = "PaymentGateway source should not be empty string or of value Zero.";
                        statusCode = "34";
                        return false;
                    }
                }
            }
            if (request.HotelCode == null || request.HotelCode == "")
            {
                statusDescription = "HotelCode cannot be blank or null";
                statusCode = "35";
                return false;
            }
            if (request.HotelName == null || request.HotelName == "")
            {
                statusDescription = "HotelName cannot be blank or null";
                statusCode = "36";
                return false;
            }
            if (request.RoomDetails == null || request.RoomDetails.Length <= 0)
            {
                statusDescription = "RoomDetails array cannot be null or of length zero";
                statusCode = "48";
                return false;
            }
        }
        catch (Exception Ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.Low, 1, Ex.Message.ToString() + "|" + Ex.StackTrace.ToString(), "");
        }
        return true;
    }

    private bool WSGetHotelBookingInput(WSGetHotelBookingRequest request)
    {
        if (request.BookingId == null || request.BookingId == "")
        {
            statusDescription = "BookingId cannot be blank or null";
            statusCode = "03";
            return false;
        }
        return true;
    }

    private bool WSSendHotelChangeRequestInput(WSSendHotelChangeRequest request)
    {
        if (request.BookingId == null || request.BookingId == "")
        {
            statusDescription = "BookingId cannot be blank or null";
            statusCode = "03";
            return false;
        }
        if (request.Remarks == null || request.Remarks == "")
        {
            statusDescription = "Remarks cannot be blank or null";
            statusCode = "04";
            return false;
        }
        if (request.RequestType.ToString() == "")
        {
            statusDescription = "RequestType cannot be empty string";
            statusCode = "05";
            return false;
        }
        return true;
    }

    private bool WSGetHotelChangeRequestStatusInput(WSGetHotelChangeRequestStatusRequest request)
    {
        if (request.ChangeRequestId <= 0) //request.ChangeRequestId == null || 
        {
            statusDescription = "ChangeRequestId cannot be less then or equeal to zero or null";
            statusCode = "03";
            return false;
        }
        return true;
    }

    private bool WSGetDestinationCityListInput(WSGetDestinationCityListRequest request)
    {
        if (request.CountryName == null || request.CountryName == "")
        {
            statusDescription = "CountryName cannot be blank or null";
            statusCode = "03";
            return false;
        }
        return true;
    }

    private UserMaster GetMemberBySiteName(string siteName)
    {
        UserPreference pref = new UserPreference();
        int memberId = 0;
        try
        {
            memberId = pref.GetMemberIdBySiteName(siteName);
        }
        catch (Exception ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.High, 0, "B2C-Hotel-GetMemberIdBySiteName Failed: " + ex.Message, "");
        }
        if (memberId > 0)
        {
            return new UserMaster(memberId);
        }
        else
        {
            return null;
        }
    }

    private UserMaster GetMemberByAccountCode(string accountCode)
    {
        UserPreference pref = new UserPreference();
        UserMaster member = new UserMaster();
        try
        {
            //commented by brahmam
           // member.LoadByAccounCode(accountCode);
            return member;
        }
        catch (Exception ex)
        {
            Audit.AddAuditHotelBookingAPI(EventType.Login, Severity.High, 0, "B2C-Hotel-GetMemberByAccountCode Failed: " + ex.Message, "");
            return null;
        }
    }

    /// <summary>
    /// Method to send error mail
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="member"></param>
    private void SendErrorMail(string subject, UserMaster member, Exception ex, string message)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        Hashtable hostPort = con.GetHostPort();
        string errorNotificationMailingId = hostPort["ErrorNotificationMailingId"].ToString();
        string fromEmailId = hostPort["fromEmail"].ToString();
        if (errorNotificationMailingId != null && errorNotificationMailingId.Trim() != string.Empty)
        {
            try
            {
                string mailMessage = string.Empty;
                mailMessage = "Member Name: " + member.FirstName + "" + member.LastName;                 //member.FullName;
                mailMessage += "\n AgencyId: " + member.AgentId.ToString();
                mailMessage += message;
                mailMessage += "\n\n\n\nException: " + ex.Message;
                mailMessage += "\n\nStack trace: " + ex.StackTrace;
                Email.Send(fromEmailId, errorNotificationMailingId, "B2C-Hotel-Error in Hotel Booking API " + subject, mailMessage);
            }
            catch (Exception excep)
            {
                Audit.Add(EventType.Email, Severity.High, (int)member.ID, "B2C-Hotel-Fail in sending mail " + subject + excep.Message + " StackTrace :" + excep.StackTrace, "");
            }
        }
    }

    private decimal GetTotalPrice(WSRate totalRate)
    {
        decimal total = 0;
        total = totalRate.TotalRate + totalRate.TotalTax;
        return total;
    }

    private List<WSHotelDeal> LoadHotelDeals(List<HotelDeal> hotelDeals)
    {
        List<WSHotelDeal> apiHotelDeals = new List<WSHotelDeal>();
        foreach (HotelDeal hotelDeal in hotelDeals)
        {
            WSHotelDeal apiHotelDeal = new WSHotelDeal();
            apiHotelDeal.DealDesriptionDoc = hotelDeal.DealDesriptionDoc;
            apiHotelDeal.DealId = hotelDeal.DealId;
            apiHotelDeal.DealMoreData = new List<WSHotelDealData>();
            foreach (HotelDealData hotelDealData in hotelDeal.DealMoreData)
            {
                WSHotelDealData apiDealData = new WSHotelDealData();
                apiDealData.Price = hotelDealData.Price;
                apiDealData.RoomType = hotelDealData.RoomType;
                apiDealData.ValidFrom = hotelDealData.ValidFrom;
                apiDealData.ValidUpto = hotelDealData.ValidUpto;
                apiHotelDeal.DealMoreData.Add(apiDealData);
            }
            apiHotelDeal.DealTitle = hotelDeal.DealTitle;
            apiHotelDeal.Description = hotelDeal.Description;
            string imagePath = ConfigurationSystem.CMSImageConfig["hotelDealImgPathForServer"] + "/";
            apiHotelDeal.HotelImage = new List<string>();
            for (int i = 0; i < hotelDeal.HotelImage.Count; i++)
            {
                if (hotelDeal.HotelImage != null && hotelDeal.HotelImage[0] != "")
                {
                    apiHotelDeal.HotelImage.Add(imagePath + hotelDeal.HotelImage[0]);
                }
            }
            apiHotelDeal.HotelLocation = hotelDeal.HotelLocation;
            apiHotelDeal.HotelName = hotelDeal.HotelName;
            apiHotelDeal.HotelProfile = hotelDeal.HotelProfile;
            apiHotelDeal.HotelRating = hotelDeal.HotelRating;
            apiHotelDeal.InAndAround = hotelDeal.InAndAround;
            apiHotelDeal.Includes = hotelDeal.Includes;
            apiHotelDeal.IsActive = hotelDeal.IsActive;
            apiHotelDeal.IsFeatured = hotelDeal.IsFeatured;
            apiHotelDeal.SpecialAttractions = hotelDeal.SpecialAttractions;
            apiHotelDeals.Add(apiHotelDeal);
        }
        return apiHotelDeals;
    }

    /// <summary>
    /// Serializes the given object.
    /// </summary>
    /// <typeparam name="T">The type of the object to be serialized.</typeparam>
    /// <param name="obj">The object to be serialized.</param>
    /// <returns>String representation of the serialized object.</returns>
    protected string Serialize<T>(T obj)
    {
        XmlSerializer xs = null;
        StringWriter sw = null;
        try
        {
            xs = new XmlSerializer(typeof(T));
            sw = new StringWriter();
            xs.Serialize(sw, obj);
            sw.Flush();
            return sw.ToString();
        }
        catch { }
        finally
        {
            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }
        }
        return "Error in Serialisation";
    }
    #endregion

    //Gta Price validation 
    [WebMethod]
    [SoapHeader("Credential")]
    public bool GtaPiceValidate(WSHotelSearchRequest request, string hotelCode, string roomTypeCode, decimal totNetFare)
    {
        Trace.TraceInformation("HotelBookingAPI.Search entered. City Reference : " + request.CityReference + ", CheckIn date : " + request.CheckInDate.ToString("dd/MM/yyyy") + ", Checkout date : " + request.CheckOutDate.ToString("dd/MM/yyyy"));
        UserMaster member = new UserMaster();
        //validating and loading member from credential
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        if (member != null)
        {
            if (!(WSSearchInput(request)))
            {
                WSHotelSearchResponse errorResponse = new WSHotelSearchResponse();
                WSStatus status = new WSStatus();
                status.Category = "SR";
                status.Description = statusDescription;
                status.StatusCode = statusCode;
                errorResponse.Status = status;
                errorResponse.Result = new WSHotelResult[0];
                Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, (int)member.ID, "B2C-Hotel-Input parameter is incorrect: " + statusDescription, "");
            }
            bool isMultiRoom = false;
            HotelRequest mseRequest = new HotelRequest();

            #region Building Request
            //user preference
            WLHotelService wlHotelService = new WLHotelService();
            wlHotelService.Credential = Credential;
            WSUserPreference userPreference = wlHotelService.GetPreferences((int)member.ID); //insted of member.LoginName i changed memberID
            //Request for the hotel sources available
            mseRequest.Sources = new List<string>();
            bool isInternationalSearchAllowed = userPreference.IanInternationAllowed || userPreference.GtaAllowed || userPreference.HotelBedsAllowed || userPreference.TouricoAllowed;
            bool isDomesticSearchAllowed = userPreference.DesiyaAllowed || userPreference.IanDomesticAllowed;
            mseRequest.Sources.Add("GTA");   //HardCoded By brahmam testing purpose only
            mseRequest.AvailabilityType = AvailabilityType.Confirmed;
            mseRequest.StartDate = request.CheckInDate.Date;
            mseRequest.EndDate = request.CheckOutDate.Date;
            mseRequest.PassengerNationality = request.PassengerNationality;
            mseRequest.PassengerCountryOfResidence = request.PassengerCountryOfResidence;
            if (request.HotelName == null)
                mseRequest.HotelName = "";
            else
                mseRequest.HotelName = request.HotelName;
            mseRequest.IsDomestic = request.IsDomestic;
            mseRequest.NoOfRooms = request.NoOfRooms;
            mseRequest.Rating = WSHotelSearchRequest.GetWSHotelRating(request.Rating);
            mseRequest.RoomGuest = new RoomGuestData[request.NoOfRooms];
            for (int i = 0; i < request.NoOfRooms; i++)
            {
                mseRequest.RoomGuest[i] = new RoomGuestData();
                mseRequest.RoomGuest[i].noOfAdults = request.RoomGuest[i].NoOfAdults;
                mseRequest.RoomGuest[i].noOfChild = request.RoomGuest[i].NoOfChild;
                List<int> childAge = new List<int>();
                if (request.RoomGuest[i].ChildAge != null && request.RoomGuest[i].ChildAge.Count > 0)
                {
                    foreach (int age in request.RoomGuest[i].ChildAge)
                    {
                        childAge.Add(age);
                    }
                }
                mseRequest.RoomGuest[i].childAge = childAge;
            }
            mseRequest.ExtraCots = 0;
            mseRequest.SearchByArea = false;
            if (!request.IsDomestic && request.NoOfRooms > 1)
            {
                isMultiRoom = true;
            }
            mseRequest.IsMultiRoom = isMultiRoom;
            //City code and Name for the hotel search
            if (request.IsDomestic)
            {
                mseRequest.CityId = HotelCity.GetCityIdFromCityName(request.CityReference);
            }
            else
            {
                mseRequest.CityId = request.CityId;
            }
            HotelCity hc = HotelCity.Load(request.CityId);
            mseRequest.CityCode = hc.GTACode;
            mseRequest.CityName = request.CityReference;
            mseRequest.CountryName = request.CountryName;
            mseRequest.RequestMode = HotelRequestMode.BookingAPI;
            mseRequest.MinRating = 0;
            switch (request.Rating)
            {
                case WSHotelRatingInput.All:
                    mseRequest.MaxRating = 5;
                    break;
                case WSHotelRatingInput.FiveStarOrLess:
                    mseRequest.MaxRating = 5;
                    break;
                case WSHotelRatingInput.FourStarOrLess:
                    mseRequest.MaxRating = 4;
                    break;
                case WSHotelRatingInput.ThreeStarOrLess:
                    mseRequest.MaxRating = 3;
                    break;
                case WSHotelRatingInput.TwoStarOrLess:
                    mseRequest.MaxRating = 2;
                    break;
                case WSHotelRatingInput.OneStarOrLess:
                    mseRequest.MaxRating = 1;
                    break;
            }
            #endregion
            HotelSearchResult[] hotelSearchResult = new HotelSearchResult[0];
            CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
            LoginInfo loginInfo = AgentLoginInfo;

            //for VAT IMPLEMENTATION
            mseRequest.LoginCountryCode = loginInfo.LocationCountryCode;
            try
            {
                mseRequest.CountryCode = Country.GetCountryCodeFromCountryName(request.CountryName);
            }
            catch { }

            gtaApi.AgentCurrency = loginInfo.Currency;
            gtaApi.AgentExchangeRates = loginInfo.AgentExchangeRates;
            gtaApi.AgentDecimalPoint = loginInfo.DecimalValue;
            //For VAT IMPLEMENTATION
            try
            {
                Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);

                if (activeSources != null && activeSources.Count > 0)
                {
                    gtaApi.SourceCountryCode = activeSources[HotelBookingSource.GTA.ToString()];
                }
                else
                {
                    gtaApi.SourceCountryCode = "AE";// TODO Ziyad
                }
            }
            catch
            {
                gtaApi.SourceCountryCode = "AE";
            }

            DOTWCountry dotw = new DOTWCountry();
            Dictionary<string, string> Countries = dotw.GetAllCountries();
            mseRequest.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[request.PassengerNationality]);
            hotelSearchResult = gtaApi.GetHotelAvailability(mseRequest, 0, null, hotelCode, roomTypeCode.Split('|')[5].ToString());
            decimal oldPrice = totNetFare;
            decimal newPrice = 0;
            if (hotelSearchResult.Length > 0)
            {
                for (int i = 0; i < request.NoOfRooms; i++)
                {
                    for (int j = 0; j < hotelSearchResult[0].RoomDetails.Length; j++)
                    {
                        if (roomTypeCode == hotelSearchResult[0].RoomDetails[j].RoomTypeCode)
                        {
                            newPrice += hotelSearchResult[0].RoomDetails[j].SellingFare;
                            break;
                        }
                    }
                }
                if (oldPrice != newPrice)
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, 0, "B2C-Hotel-Price has been revised for this Hotel. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
                    return false;
                }
            }
            return true;
        }
        else
        {
            Audit.AddAuditHotelBookingAPI(EventType.HotelSearch, Severity.High, 0, "B2C-Hotel-Login Failed for Member for Search Method. \nSiteName : " + Credential.SiteName + "\nUserName : " + Credential.UserName + "\nAccountCode : " + Credential.AccountCode, "");
            return false;
        }
    }


    [WebMethod]
    [SoapHeader("Credential")]
    public bool CancelBooking(int bookingId, string data, string custEmail)
    {
        try
        {
            UserMaster member = new UserMaster();
            //validating and loading member from credential
            member = WSValidate(Credential);
            AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
            LoginInfo loginfo = AgentLoginInfo;
            int loggedMemberId = Convert.ToInt32(loginfo.UserID);
            int requestTypeId = 4;
            AgentMaster agency = null;
            ServiceRequest serviceRequest = new ServiceRequest();
            BookingDetail booking = new BookingDetail();
            booking = new BookingDetail(bookingId);
            HotelItinerary itineary = new HotelItinerary();
            HotelRoom room = new HotelRoom();
            Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].ProductTypeId == (int)ProductType.Hotel)
                {
                    itineary.Load(products[i].ProductId);
                    itineary.Roomtype = room.Load(products[i].ProductId);
                    break;
                }
            }

            agency = new AgentMaster(booking.AgencyId);
            serviceRequest.BookingId = bookingId;
            serviceRequest.ReferenceId = itineary.Roomtype[0].RoomId;
            serviceRequest.ProductType = ProductType.Hotel;

            serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
            serviceRequest.Data = data;
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;

            serviceRequest.CreatedBy = Convert.ToInt32(loginfo.UserID);
            //serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
            serviceRequest.IsDomestic = itineary.IsDomestic;

            serviceRequest.PaxName = itineary.Roomtype[0].PassenegerInfo[0].Firstname + itineary.Roomtype[0].PassenegerInfo[0].Lastname;
            serviceRequest.Pnr = itineary.ConfirmationNo;
            serviceRequest.Source = (int)itineary.Source;
            serviceRequest.StartDate = itineary.StartDate;
            serviceRequest.SupplierName = itineary.HotelName;
            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
            serviceRequest.ReferenceNumber = itineary.BookingRefNo;
            serviceRequest.PriceId = itineary.Roomtype[0].PriceId;
            //serviceRequest.AgencyId = (int)agency.ID;
            serviceRequest.AgencyId = loginfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
            serviceRequest.ItemTypeId = InvoiceItemTypeId.HotelBooking;
            serviceRequest.DocName = "";
            serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
            serviceRequest.LastModifiedOn = DateTime.Now;
            serviceRequest.CreatedOn = DateTime.Now;
            //Added by shiva
            serviceRequest.IsDomestic = true;
            serviceRequest.AgencyTypeId = Agencytype.Cash;
            serviceRequest.RequestSourceId = RequestSource.BookingAPI;
            serviceRequest.Save();
            // Setting booking details status 
            if (requestTypeId == 4)
            {
                booking.SetBookingStatus(BookingStatus.InProgress, loggedMemberId);
            }
            else
            {
                booking.SetBookingStatus(BookingStatus.AmendmentInProgress, loggedMemberId);
            }

            BookingHistory bh = new BookingHistory();
            bh.BookingId = bookingId;
            bh.Remarks = "Request sent to " + serviceRequest.RequestType.ToString() + " of the hotel booking.";

            if (requestTypeId == 4)
            {
                bh.EventCategory = EventCategory.HotelCancel;
            }
            else
            {
                bh.EventCategory = EventCategory.HotelAmendment;
            }
            bh.CreatedBy = (int)loginfo.UserID;
            bh.Save();

            //Sending email.
            Hashtable table = new Hashtable();
            table.Add("agentName", agency.Name);
            table.Add("hotelName", itineary.HotelName);
            table.Add("confirmationNo", itineary.ConfirmationNo);

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            UserMaster bookedBy = new UserMaster(itineary.CreatedBy);
            AgentMaster bookedAgency = new AgentMaster(itineary.AgencyId);
            toArray.Add(bookedBy.Email);
            toArray.Add(bookedAgency.Email1);
            toArray.Add(custEmail);
            string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["HOTEL_CANCEL_MAIL"]).Split(';');
            foreach (string cnMail in cancelMails)
            {
                toArray.Add(cnMail);
            }
            toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]); 

            //string message = "Your request for cancelling hotel <b>" + itineary.HotelName + " </b> is under process. Confirmation No:(" + itineary.ConfirmationNo + ")";
            string message = ConfigurationManager.AppSettings["HOTEL_CANCEL_REQUEST"];
            try
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Hotel)Request for cancellation. Confirmation No:(" + itineary.ConfirmationNo + ")", message, table);
            }
            catch{  }

            return true;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelBook, Severity.High, 1, "Exception in the Hotel Change Request. Message:" + ex.Message, "");
            return false;
        }
        
    }

    [WebMethod]
    [SoapHeader("Credential")]
    public DataTable GetProductPromotions(int productId, DateTime bookingDate, DateTime travelDate, string propertyCode, decimal minTranxAmount, int minPaxCount, int minRoomCount, string city, string source, int minNights)
    {
        DataTable dtPromotions = new DataTable("Promotions");
        try
        {
            UserPreference pref = new UserPreference();
            int memberId = pref.GetMemberIdBySiteName(Credential.SiteName);

            dtPromotions = PromoMaster.GetProductPromotions(productId, bookingDate, travelDate, propertyCode, minTranxAmount, minPaxCount, minRoomCount, city, source, memberId, minNights);
            dtPromotions.TableName = "Promotions";
        }
        catch
        {

        }
        return dtPromotions;
    }
    //Dotw blocked rooms
    [WebMethod]
    [SoapHeader("Credential")]
    public bool BlockRooms(WSBookRequest request)
    {
        bool blocked = false;
        UserMaster member = new UserMaster();
        //Validating member from credentials
        member = WSValidate(Credential);
        AgentLoginInfo = UserMaster.GetB2CUser((int)member.ID);
        HotelSearchResult[] mseResults;
        HotelSearchResult hotelResult;
        WSStatus status = new WSStatus();
        if (member != null)
        {
            try
            {
                #region Cache Search Result
                mseResults = (HotelSearchResult[])HttpContext.Current.Cache[request.SessionId + "-ResultList"];
                if (mseResults != null && mseResults.Length > 0)
                {
                    hotelResult = WSGetCancellationPolicyRequest.GetSelectedSearchResult(request.Index, mseResults);
                    //hotelResult = WSBookRequest.GetSelectedSearchResult(request, mseResults, (int)member.ID); //price validate not required
                }
                else
                {
                    Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Hotelresult not loaded from Cache, This may be because cache data has expired", "");
                    throw new ArgumentException("Hotelresult not loaded from Cache, This may be because cache data has expired");
                }
                #endregion

                #region if (hotelResult == null)
                if (hotelResult == null)
                {
                    try
                    {
                        Audit.AddAuditHotelBookingAPI(EventType.HotelBook, Severity.High, (int)member.ID, "B2C-Hotel-Technical Fault 05:Hotel Search Results not found, any of HotelCode, HotelName or Index is incorrect", "");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    return false;
                }
                #endregion
                try
                {
                    HotelItinerary itineary = WSBookRequest.BuildHotelItinerary(request, member, hotelResult, AgentLoginInfo);
                    CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(request.SessionId);
                    blocked = dotw.BlockRooms(itineary, false);
                }
                catch (Exception ex)
                {
                    SendErrorMail("Book Method Failed, due to Exception", member, ex, "exception returned from DOTW.GetBooking Error Message: could not block rooms : " + " for Hotel " + request.HotelName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.DOTWBooking, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetBooking Error Message: could not block rooms|" + DateTime.Now + " for Hotel " + request.HotelName + "Exception :" + ex.Message, "1");
                return false;
            }
        }
        return blocked;
    }
    //[WebMethod]
    //[SoapHeader("Credential")]
    //public WSUserPreference GetPreferences()
    //{
    //    WSUserPreference userPref = new WSUserPreference();
    //    UserPreference pref = new UserPreference();
    //    int UserMasterId;
    //    try
    //    {

    //        UserMasterId = pref.GetMemberIdBySiteNameForHotel(Credential.SiteName);
    //        AgentLoginInfo = UserMaster.GetB2CUser(UserMasterId);
    //        userPref.Load(UserMasterId);
    //        userPref.AgentCurrency = AgentLoginInfo.Currency;
    //        userPref.AgentDecimalPoint = AgentLoginInfo.DecimalValue;
    //        userPref.AgentEmail = AgentLoginInfo.AgentEmail;
    //        userPref.AgentID = AgentLoginInfo.AgentId;
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Site Name is not valid: " + ex.Message, "");
    //        throw new ArgumentException("Site Name is not valid");
    //    }
    //    return userPref;
    //}
}
