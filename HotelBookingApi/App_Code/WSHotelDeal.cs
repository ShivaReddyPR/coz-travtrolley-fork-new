using System;
using System.Collections.Generic;

/// <summary>
/// Summary description for WSHotelDeal
/// </summary>
public class WSHotelDeal
{
    #region Private Fields

    private int dealId;
    private string dealTitle;
    private string hotelName;
    private int hotelRating;
    private string hotelLocation;
    private string description;
    private string includes;
    private string specialAttractions;
    private bool isActive;
    List<WSHotelDealData> dealMoreData;
    string hotelProfile;
    List<string> hotelImage; // contains images seperated by |
    string inAndAround;
    string dealDesriptionDoc;   // Upload document for deal Description     
    bool isFeatured;
    /// <summary>
    /// Field having possible file extensions
    /// </summary>
    private string[] fileExt ={ "jpg", "jpeg", "jpe", "jfif", "bmp", "gif", "tiff", "tif", "png", "dib" };

    #endregion

    #region Public Properties

    /// <summary>
    /// Deal Id of the HotelDeal
    /// </summary>
    public int DealId
    {
        get { return dealId; }
        set { dealId = value; }
    }

    /// <summary>
    /// Title of the HotelDeal
    /// </summary>
    public string DealTitle
    {
        get { return dealTitle; }
        set { dealTitle = value; }
    }

    /// <summary>
    /// Name of the Hotel or Hotel Chain for which the deal has been created
    /// </summary>
    public string HotelName
    {
        get
        {
            return hotelName;
        }
        set
        {
            hotelName = value;
        }
    }

    /// <summary>
    /// Star rating of the Hotel
    /// </summary>
    public int HotelRating
    {
        get
        {
            return hotelRating;
        }
        set
        {
            hotelRating = value;
        }
    }

    /// <summary>
    /// Location or city in which Hotel is situated
    /// </summary>
    public string HotelLocation
    {
        get
        {
            return hotelLocation;
        }
        set
        {
            hotelLocation = value;
        }
    }

    /// <summary>
    /// Hotel/deal description
    /// </summary>
    public string Description
    {
        get
        {
            return description;
        }
        set
        {
            description = value;
        }
    }

    /// <summary>
    /// Facilities Available in Hotel
    /// </summary>
    public string Includes
    {
        get
        {
            return includes;
        }
        set
        {
            includes = value;
        }
    }

    public string SpecialAttractions
    {
        get
        {
            return specialAttractions;
        }
        set
        {
            specialAttractions = value;
        }
    }

    /// <summary>
    /// Status of the Deal
    /// </summary>
    public bool IsActive
    {
        get
        {
            return isActive;
        }
        set
        {
            isActive = value;
        }
    }

    /// <summary>
    /// Time/Date from which Deal will be valid
    /// </summary>
    public List<WSHotelDealData> DealMoreData
    {
        get
        {
            return dealMoreData;
        }
        set
        {
            dealMoreData = value;
        }
    }

    /// <summary>
    /// Time/Date the deal will be valid upto
    /// </summary>
    public string HotelProfile
    {
        get
        {
            return hotelProfile;
        }
        set
        {
            hotelProfile = value;
        }
    }

    public List<string> HotelImage
    {
        get
        {
            return hotelImage;
        }
        set
        {
            hotelImage = value;
        }
    }

    public string InAndAround
    {
        get
        {
            return inAndAround;
        }
        set
        {
            inAndAround = value;
        }
    }

    public string DealDesriptionDoc
    {

        get
        {
            return dealDesriptionDoc;
        }
        set
        {
            dealDesriptionDoc = value;
        }
    }
    public bool IsFeatured
    {
        get { return isFeatured; }
        set { isFeatured = value; }
    }
    #endregion

    public WSHotelDeal()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

public struct WSHotelDealData
{
    public DateTime ValidFrom;
    public DateTime ValidUpto;
    public String RoomType;
    public Double Price;
}

