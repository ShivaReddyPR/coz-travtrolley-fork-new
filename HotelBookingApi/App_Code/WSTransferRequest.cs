using System;
using System.Data;
using CT.BookingEngine;


[Serializable]
public class WSTransferRequest
{
    private bool immediateConfirmationOnly;
    private string itemName;
    private string itemCode;
    private string cityName;
    private DateTime trasnferDate;
    private int numberOfPassengers;
    private string preferredLanguage;
    private string alternateLanguage;
    private string pickUpCityCode;
    private string pickUpCode;
    private string pickUpPointCode;
    private string dropOffCityCode;
    private string dropOffCode;
    private string dropOffPointCode;


    public bool ImmediateConfirmationOnly
    {
        get { return immediateConfirmationOnly; }
        set { immediateConfirmationOnly = value; }
    }
    public string ItemName
    {
        get { return itemName; }
        set { itemName = value; }
    }
    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
    public string CityName
    {
        get { return cityName; }
        set { cityName = value; }
    }
    public DateTime TrasnferDate
    {
        get { return trasnferDate; }
        set { trasnferDate = value; }
    }
    public int NumberOfPassengers
    {
        get { return numberOfPassengers; }
        set { numberOfPassengers = value; }
    }
    public string PreferredLanguage
    {
        get { return preferredLanguage; }
        set { preferredLanguage = value; }
    }
    public string AlternateLanguage
    {
        get { return alternateLanguage; }
        set { alternateLanguage = value; }
    }
    public string PickUpCityCode
    {
        get { return pickUpCityCode; }
        set { pickUpCityCode = value; }
    }
    public string PickUpCode
    {
        get { return pickUpCode; }
        set { pickUpCode = value; }
    }
    public string PickUpPointCode
    {
        get { return pickUpPointCode; }
        set { pickUpPointCode = value; }
    }
    public string DropOffCityCode
    {
        get { return dropOffCityCode; }
        set { dropOffCityCode = value; }
    }
    public string DropOffCode
    {
        get { return dropOffCode; }
        set { dropOffCode = value; }
    }
    public string DropOffPointCode
    {
        get { return dropOffPointCode; }
        set { dropOffPointCode = value; }
    }
    public static void UpdatePrice(ref TransferSearchResult[] mseResult, int agencyId)
    {
        PriceAccounts price = new PriceAccounts();
        DataTable dtAllAgentsMarkup = new DataTable();
        if (agencyId > 0)
        {
            dtAllAgentsMarkup = UpdateMarkup.GetAllMarkup(agencyId, (int)ProductType.Transfers);
        }
        for (int i = 0; i < mseResult.Length; i++)
        {
            for (int count = 0; count < mseResult[i].Vehicles.Length; count++)
            {
                #region B2CAgentMarkup calculations
                //price = mseResult[i].Price;
                DataView dv = dtAllAgentsMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2C') AND SourceId='" + mseResult[i].Source.ToString() + "'";
                DataTable dtAgentMarkup = dv.ToTable();

                if (dtAgentMarkup != null && dtAgentMarkup.Rows.Count > 0)
                {
                    decimal agtB2CMarkUp = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Markup"]);

                    mseResult[i].Vehicles[count].PriceInfo.B2CMarkupType = dtAgentMarkup.Rows[0]["MarkupType"].ToString();
                    mseResult[i].Vehicles[count].PriceInfo.B2CMarkupValue = agtB2CMarkUp;

                    if (Convert.ToString(dtAgentMarkup.Rows[0]["MarkupType"]) == "P")
                    {
                        mseResult[i].Vehicles[count].PriceInfo.B2CMarkup = (Convert.ToDecimal(mseResult[i].Vehicles[count].PriceInfo.Markup + mseResult[i].Vehicles[count].ItemPrice) * (agtB2CMarkUp / 100m));
                    }
                    else
                    {
                        mseResult[i].Vehicles[count].PriceInfo.B2CMarkup = agtB2CMarkUp;
                    }
                }

                #endregion
            }
        }
    }

}
   

