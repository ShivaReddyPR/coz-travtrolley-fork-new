﻿using ICSharpCode.SharpZipLib.GZip;
using System;
//using System.Collections.Generic;
using System.IO;
//using System.Linq;
using System.Net;
//using System.Text;
//using System.Threading.Tasks;

namespace ReligareInsurance
{
    [Serializable]

    public class GZipHttpWebResponse : WebResponse

    {

        private Stream m_decompressedStream;

        private string _contentType;

        private long _contentLength;

        private string _contentEncoding;

        private Uri _responseUri;

        private WebHeaderCollection _httpResponseHeaders;



        //          /// <summary>

        //          /// default constructor from WebResponse

        //          /// </summary>

        //          /// <param name="serializationInfo"></param>

        //          /// <param name="streamingContext"></param>

        //          protected GZipHttpWebResponse(

        //                SerializationInfo serializationInfo,

        //                StreamingContext streamingContext

        //                ) : base (serializationInfo, streamingContext)

        //          {

        //          }

        //          

        public override long ContentLength

        {

            set

            {

                _contentLength = value;

            }



            get

            {

                return _contentLength;

            }

        }





        public override string ContentType

        {

            set

            {

                _contentType = value;

            }



            get

            {

                return _contentType;

            }

        }



        public override WebHeaderCollection Headers

        {

            get

            {

                return _httpResponseHeaders;

            }

        }



        public string ContentEncoding

        {

            set

            {

                _contentEncoding = value; ;

            }



            get

            {

                return _contentEncoding;

            }

        }



        public GZipHttpWebResponse(HttpWebResponse response)

        {

            try

            {

                _contentType = response.ContentType;

                _contentLength = response.ContentLength;

                _contentEncoding = response.ContentEncoding;

                _responseUri = response.ResponseUri;

                _httpResponseHeaders = response.Headers;

                if (_contentEncoding == "gzip")

                {

                    // Decompress response stream with GZip

                    const int BUF_SIZE = 16384;

                    Stream compressedStream = new GZipInputStream(response.GetResponseStream(), BUF_SIZE);

                    m_decompressedStream = new MemoryStream();



                    byte[] writeData = new byte[BUF_SIZE];

                    int size = compressedStream.Read(writeData, 0, BUF_SIZE);

                    while (size > 0)

                    {

                        m_decompressedStream.Write(writeData, 0, size);

                        size = compressedStream.Read(writeData, 0, BUF_SIZE);

                    }

                    m_decompressedStream.Seek(0, SeekOrigin.Begin);

                    _contentEncoding = "";

                    _httpResponseHeaders.Remove("Content-Encoding");

                    _contentLength = m_decompressedStream.Length;

                    // We have decompressed the entire response, so we no longer need the compressed version.

                    response.Close();

                }

                else

                {

                    m_decompressedStream = response.GetResponseStream();

                }

            }

            catch (Exception)

            {

                // We got an exception processing the response. Clean up.

                response.Close();

                Close();

                throw;

            }

        }



        ~GZipHttpWebResponse()

        {

            Close();

        }



        public override Stream GetResponseStream()

        {

            return m_decompressedStream;

        }



        public override void Close()

        {

            if (m_decompressedStream != null)

            {

                m_decompressedStream.Close();

                m_decompressedStream = null;

            }

        }



        public override Uri ResponseUri { get { return _responseUri; } }

    }
}
