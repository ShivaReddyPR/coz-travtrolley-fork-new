﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;

namespace ReligareInsurance
{
   public class Relationofproposer
    {
        #region private members
        private const long NEW_RECORD = -1;
        int _relationid;
        string _code;
        string _relation;
        long _created_by;
        long _id;
        #endregion
        #region Public properties
        public int RELATION_ID
        { get { return _relationid; }
        set { _relationid = value; }
        }
        public string CODE
        { get { return _code; }
            set { _code = value; }
        }
        public string RELATION
        { get { return _relation; }
            set { _relation = value; }
        }
       
        public long CREATED_BY
        {
            get { return _created_by; }
            set { _created_by = value; }
        }
        public long ID
        {
            get
            {
                return _id;
            }
            set { _id = value; }
        }
        #endregion
        #region Constructors
        public Relationofproposer()
        {
            _id = NEW_RECORD;
        }
        public Relationofproposer(long id)
        {


            _id = id;
            GetDetails(id);
        }
        #endregion
        #region Methods
        public void Save()
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[6];
                parameters[0] = new SqlParameter("@relation_id", _id);
                parameters[1] = new SqlParameter("@code", _code);
                parameters[2] = new SqlParameter("@relation", _relation);
                parameters[3] = new SqlParameter("@created_by", _created_by);
                parameters[4] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                parameters[4].Direction = ParameterDirection.Output;
                parameters[5] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                parameters[5].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("usp_ReligareProposerRelationMaster_add_update", parameters);
                string messageType = Utility.ToString(parameters[4].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameters[5].Value);
                    if (message != string.Empty) throw new Exception(message);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetAdditionalServiceMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("usp_ReligareProposerRelationMaster_Details").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
         DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@relation_id", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_ReligareProposerRelationMaster_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["RELATION_ID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["RELATION_ID"]);
                _code = Utility.ToString(dr["CODE"]);
                _relation = Utility.ToString(dr["RELATION"]);
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
