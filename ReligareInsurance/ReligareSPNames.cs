﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReligareInsurance
{
   public static  class ReligareSPNames
    {
        // Religare Quotation Procs list
        public const string GetReligareProductDetails = "Usp_GetReligareProductDetails";
        public const string GetReligareProductTypes = "usp_CT_T_Religare_ProductTypes";
        public const string GetReligareTravellerDetails = "usp_Get_ReligareTravellerDetails";
        public const string GetReligareSumInsuredRangeId = "usp_Get_ReligareSumInsuredRangeId";
        public const string Get_ReligareInsQuestions = "usp_Get_ReligareInsQuestions";
        public const string GetEducationReligareInsQuestions = "usp_GetEducationReligareInsQuestions";
        public const string GetOPCoverReligareInsQuestions = "usp_GetOPCoverReligareInsQuestions";
        public const string GetVisaNationalities = "usp_GetReligareNationalities";
        public const string GetRelationOfProposer = "usp_GetRelationOfProposer";
        public const string GETReligareDiscountMaster = "usp_Get_ReligareDiscountMaster";
        public const string GetReligareSumInsuredDetails = "usp_Get_Religaresum_InsuredDetails";
        
        // Religare Header Procs list
        public const string GetReligareCityStateCodes = "GetReligareCityStateCodes";
        public const string InsertReligarHeader = "usp_ReligareHeaderDetailsInsert";
        public const string GetHeaderId = "usp_getHeaderId";
        public const string GetReligareHeaderDetails = "GetReligareHeaderDetails";
        public const string GetReligareHeaderQueue = "usp_GetReligareHeaderQueue";
        public const string GetReligareInsuranceChangeRequestList = "usp_GetReligareInsuranceChangeRequestList";
        public const string UpdateReligareHeaderStatus = "Usp_UpdateReligareHeaderStatus";
        public const string GetReligareHeaderId = "Usp_GetReligareHeaderId";
        public const string GetInvoiceNoBytranxHeaderId = "usp_GetInvoiceNoBytranxHeaderId";
        public const string GetInsurancePlanList = "usp_GetReligarePlanList";

        // Religare Passenger Procs list
        public const string InsertPassengerDetails = "usp_ReligarePassengerDetailsInsert";
        
        // Religare Questions Procs list
        public const string InsertQuestionaryDetails = "usp_ReligarePassengerQuestionsDetailsInsert";    

    }
}
