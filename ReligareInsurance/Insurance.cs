﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReligareInsurance
{
    public class InsuranceMaster
    {
        #region private properties
        string travellingId;
        string tripType;
        DateTime? startDate;
        DateTime? endDate;
        int days;
        string ped;
        int travellers;
        int travellingAgeId;
        string mobileNo;
        string email;
        string sumInsured;
        string rangeId;
        string maxTripPeriod;

        public string MaxTripPeriod
        {
            get { return maxTripPeriod; }
            set { maxTripPeriod = value; }
        }
        public string RangeId
        {
            get { return rangeId; }
            set { rangeId = value; }
        }
        public string TravellingId
        {
            get { return travellingId; }
            set { travellingId = value; }
        }
        public string TripType
        {
            get { return tripType; }
            set { tripType = value; }
        }
        public DateTime? StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime? EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public int Days
        {
            get { return days; }
            set { days = value; }
        }
        public string Ped
        {
            get { return ped; }
            set { ped = value; }
        }
        public int Travellers
        {
            get { return travellers; }
            set { travellers = value; }
        }
        public int TravellingAgeId
        {
            get { return travellingAgeId; }
            set { travellingAgeId = value; }
        }
        public string MobileNo
        {
            get { return mobileNo; }
            set { mobileNo = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string SumInsured
        {
            get { return sumInsured; }
            set { sumInsured = value; }
        }
        #endregion

  
        public static DataTable GetInsuranceTypes()
        {
            try
            {
                return DBGateway.ExecuteQuery(@"usp_CT_T_Religare_ProductTypes").Tables[0];
            }
            catch { throw; }
        }
        public static DataTable GetProductDetails(ref string productId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@productId", productId);
                return DBGateway.FillDataTableSP(@"Usp_GetReligareProductDetails", parameters);
            }
            catch (Exception ex) { throw; }
        }
        public static DataTable GetTravellerAgeDetails(ref string insuredId,ref int ped,ref int term)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@SUM_INSURED_ID", insuredId);
                parameters[1] = new SqlParameter("@ped", ped);
                parameters[2] = new SqlParameter("@term", term);
                return DBGateway.FillDataTableSP(@"usp_Get_ReligareTravellerDetails", parameters);
            }
            catch (Exception ex) { throw; }
        }
        public static string GetSumInsuredRangeID(ref string insuredId, ref int ped, ref int term,ref int detailsID)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[4];
                parameters[0] = new SqlParameter("@SUM_INSURED_DETAIL_ID", detailsID);
                parameters[1] = new SqlParameter("@SUM_INSURED_ID", insuredId);
                parameters[2] = new SqlParameter("@ped", ped);
                parameters[3] = new SqlParameter("@term", term);
                return DBGateway.FillDataTableSP(@"usp_Get_ReligareSumInsuredRangeId", parameters).Rows[0]["RANGE_ID"].ToString();
            }
            catch (Exception ex) { throw; }
        }
        public static DataTable GetQuestions(ref string productId,ref string typeId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@PRODUCT_ID", productId);
                parameters[1] = new SqlParameter("@typeId", typeId);
                return DBGateway.FillDataTableSP(@"usp_Get_ReligareInsQuestions", parameters);
            }
            catch (Exception ex) { throw; }
        }
        public static DataTable GetEducationQuestions(ref string productId, ref string typeId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@PRODUCT_ID", productId);
                parameters[1] = new SqlParameter("@typeId", typeId);
                return DBGateway.FillDataTableSP(@"usp_GetEducationReligareInsQuestions", parameters);
            }
            catch (Exception ex) { throw; }
        }
        public static DataTable GetOptionalCoverQuestions(ref string productId, ref string typeId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@PRODUCT_ID", productId);
                parameters[1] = new SqlParameter("@typeId", typeId);
                return DBGateway.FillDataTableSP(@"usp_GetOPCoverReligareInsQuestions", parameters);
            }
            catch (Exception ex) { throw; }
        }
        public static DataTable GetVisaNationalities( )
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1]; 
                return DBGateway.FillDataTableSP(@"usp_GetVisaNationalities", parameters);
            }
            catch (Exception ex) { throw; }
        }
        public static DataTable Get_Relations(string status)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@status", status);
                return DBGateway.FillDataTableSP(@"usp_GetRelationOfProposer", parameters);
            }
            catch (Exception ex) { throw; }
        }
        public static string GetDiscountDetails(ref int travellerCount)
        {
            try
            {                
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@travellerCount", travellerCount);
                return DBGateway.FillDataTableSP(@"usp_Get_ReligareDiscountMaster", parameters).Rows[0]["DISCOUNT_PERCENTAGE"].ToString();
               
            }
            catch { throw; }

        }
    }
    public class ProposerDetails
    {
        #region Variables
        string nationalityId;
        string passportNo;
        string title;
        string firstName;
        string lastName;
        DateTime? dob;
        string addressLine1;
        string addressLine2;
        string email;
        string mobileNo;
        string pincode;
        string city;
        string state;
        string nomineeName;
        string purposeOfVisit;
        string residenceProof;
        string proofDetails;
        string relation;
        #endregion

        public string NationalityId
        {
            get { return nationalityId; }
            set { nationalityId = value; }
        }
        public string PassportNo
        {
            get { return passportNo; }
            set { passportNo = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public string AddressLine1
        {
            get { return addressLine1; }
            set { addressLine1 = value; }
        }
        public string AddressLine2
        {
            get { return addressLine2; }
            set { addressLine2 = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string MobileNo
        {
            get { return mobileNo; }
            set { mobileNo = value; }
        }
        public string Pincode
        {
            get { return pincode; }
            set { pincode = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        public string NomineeName
        {
            get { return nomineeName; }
            set { nomineeName = value; }
        }
        public string PurposeOfVisit
        {
            get { return purposeOfVisit; }
            set { purposeOfVisit = value; }
        }
        public string ResidenceProof
        {
            get { return residenceProof; }
            set { residenceProof = value; }
        }
        public string ProofDetails
        {
            get { return proofDetails; }
            set { proofDetails = value; }
        }
        public DateTime? Dob
        {
            get { return dob; }
            set { dob = value; }
        }
        public string Relation
        {
            get { return relation; }
            set { relation = value; }
        }
    }
  
}

