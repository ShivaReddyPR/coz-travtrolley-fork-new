﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;

namespace ReligareInsurance
{
   public class ReligareProductType
    {
        #region private members
        private const long NEW_RECORD = -1;
        int _productid;
        string _productname;
        long _created_by;
        private long _id;
        string _status;
        #endregion
        #region Public properties
        public int PRODUCTID
        {
            get { return _productid; }
            set { _productid = value; }
        }
        public string PRODUCTNAME
        {
            get { return _productname; }
            set { _productname = value; }
        }
        public long CREATED_BY
        {
            get { return _created_by; }
            set { _created_by = value; }
        }
        public long ID
        {
            get
            {
                return _id;
            }
            set { _id = value; }
        }
        public string STATUS
        { get { return _status; }
          set { _status = value; }
        }
        #endregion
        #region Constructors
        public ReligareProductType()
        {
            _id = NEW_RECORD;
        }
        public ReligareProductType(long id)
        {


            _id = id;
            GetDetails(id);
        }
        #endregion
        #region Methods
        public void Save()
        {
            try
            {
                SqlParameter[] productlist= new SqlParameter[5];
                productlist[0] = new SqlParameter("@productid", _id);
                productlist[1] = new SqlParameter("@productname", _productname);
                productlist[2] = new SqlParameter("@createdby", _created_by);
                productlist[3] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                productlist[3].Direction = ParameterDirection.Output;
                productlist[4] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                productlist[4].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("usp_ReligareProductTypeMaster_add_update", productlist);
                string messageType = Utility.ToString(productlist[3].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(productlist[4].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
         void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
         DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@productid", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_ReligareProductTypeMaster_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["PRODUCTID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["PRODUCTID"]);
                _productname = Utility.ToString(dr["PRODUCTNAME"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetAdditionalServiceMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("usp_GetReligareProductTypeMasterDetails").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

    }
}
