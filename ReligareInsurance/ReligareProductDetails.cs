﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;

namespace ReligareInsurance
{
    public class ReligareProductDetails
    {
        #region private members
        private const long NEW_RECORD = -1;
        int _detailsid;
        int _product_type_id;
        string _productid;
        string _productname;
        long _created_by;
        long _id;
        string _trip_type;

        #endregion

        #region Public Properties
        public int DETAILSID
        {
            get { return _detailsid; }
            set { _detailsid = value; }
        }
        public int PRODUCT_TYPE_ID
        {
            get { return _product_type_id; }
            set { _product_type_id = value; }
        }
        public string PRODUCTID
        {
            get { return _productid; }
            set { _productid = value; }
        }
        public string PRODUCTNAME
        {
            get { return _productname; }
            set { _productname = value; }
        }


        public long CREATED_BY
        {
            get { return _created_by; }
            set { _created_by = value; }
        }
        public long ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Trip_Type
        {
            get { return _trip_type; }
            set { _trip_type = value; }
        }

        #endregion
        #region Constructors
        public ReligareProductDetails()
        {
            _id = NEW_RECORD;
        }
        public ReligareProductDetails(long id)
        {


            _id = id;
            GetDetails(id);
        }
        #endregion
        #region Methods

        
        void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
         DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@detailsid", id);
                DataSet dsResult = DBGateway.ExecuteQuery("USP_ReligareProductDetails_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["DETAIL_ID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
         void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["DETAIL_ID"]);
                _product_type_id = Utility.ToInteger(dr["PRODUCT_TYPE_ID"]);
                _productid = Utility.ToString(dr["PRODUCT_ID"]);
                _productname = Utility.ToString(dr["PRODUCT_NAME"]);
                _trip_type = Utility.ToString(dr["Trip_Type"]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetList()
        {
            try
            { 
             return DBGateway.ExecuteQuery("usp_ReligareProductTypeDetails_Getlist").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Save()
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[8];
                parameters[0] = new SqlParameter("@detailid", _id);
                parameters[1] = new SqlParameter("@productTyep_id", _product_type_id);
                parameters[2] = new SqlParameter("@productid", _productid);
                parameters[3] = new SqlParameter("@productName", _productname);
                parameters[4] = new SqlParameter("@created_by", _created_by);
                parameters[5] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                parameters[5].Direction = ParameterDirection.Output;
                parameters[6] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                parameters[6].Direction = ParameterDirection.Output;
                parameters[7] = new SqlParameter("@Trip_Type", _trip_type);
                DBGateway.ExecuteNonQuerySP("usp_ReligareProductDetails_add_update", parameters);
                string messageType = Utility.ToString(parameters[5].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameters[6].Value);
                    if (message != string.Empty) throw new Exception(message);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetAdditionalServiceMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("usp_GetReligareMasterProductDetails").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

    }
}
